.. -
.. * #%L
.. * Agrosyst
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2013 - 2014 INRA
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Déploiement
===========

Cette documentation détaille le déploiement en production de l'application Agrosyst.

Avant de monté de version votre instance d'Agrosyst, sauvegardez la version actuelle de la base de données::

pg_dump -U agrosyst agrosyst > agrosyst-prod-yyyymmmdd.sql

Base de données
---------------

La base de données de production tourne sur un serveur PostgreSQL (au moins dans sa version 9.0).

Voici les commandes à utiliser pour créer la base de données::

  su postgres
  createuser agrosyst -P
  createdb agrosyst -O agrosyst

Dans le cas d'une base de données existante, il faudra récuperer et injecter un dump dans cette base de données::

  psql -U agrosyst agrosyst < agrosyst-dump-yyyymmdd.sql

ou

  pg_restore -v -U agrosyst -d "agrosyst" agrosyst-dump-yyyymmdd.dump


Mise à jour de la base de données
---------------------------------

Le schéma de base de données est automatiquement mis à jour via des scripts de migration sql ou java.
Les scripts de migration peuvent être retrouvés pour les scripts sql dans le dossier ``agrosyst-services/src/main/resources/db.migration``
et pour les scripts java dans le dossier ``agrosyst-services/src/main/java/fr.inra.agrosyst/api/entities/migration``.

historiquement avant la version 1.1
Des scripts de migration sql etaient disponibles dans le dossier ``agrosyst-services/src/main/sql``.

Ces derners devaient être passé manuillement en utilisant la commande suivante::

  psql -U agrosyst agrosyst < migration-0.12.3.sql

Les scripts sont transactionnels. Soit la migration s'effectue sans emcombre, soit elle ne s'effectue pas du tou

Avant de passer le script, sauvegardez la version actuelle de la base de données.

Configuration de l'application
------------------------------

L'archive WAR d'Agrosyst embarque une configuration par défaut qui n'est PAS optimale pour un serveur de production.

Il est possible de surcharger tous les éléments configurable dans le fichier ``/etc/agrosyst-<context>.properties``

Pour pouvoir supporter plusieurs instances sur une même JVM, le nom de fichier contient le nom de contexte web.
C'est à dire que si l'application est déployée en tant que ``http://localhost:8080/azerty/``, le fichier de configuration doit se nommer ``agrosyst-azerty.properties``.
Autre exemple, si l'application est déployée en tant que ``http://localhost:8080/agrosyst-demo/``, le fichier de configuration doit se nommer ``agrosyst-agrosyst-demo.properties`` (notez la répétition de "agrosyst-").
Enfin, si le contexte ne porte pas de nom (cas ``http://localhost:8080/``), le fichier de configuration doit se nommer ``agrosyst-ROOT.properties``.

Base de données
~~~~~~~~~~~~~~~

Exemple de configuration de la base de données postgresql::

  hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
  jakarta.persistence.jdbc.driver=org.postgresql.Driver
  jakarta.persistence.jdbc.url=jdbc:postgresql:agrosyst
  jakarta.persistence.jdbc.user=agrosyst
  jakarta.persistence.jdbc.password=agrosyst
  hibernate.hbm2ddl.auto=validate

Pour plus de détails sur les options de configuration, reportez-vous à la `page de configuration <config.html>`_.

SMTP
~~~~

L'application se connecte à un serveur SMTP externe pour envoyer les mails. Par défaut, ce serveur
est ``localhost`` sur le port ``25``.

Pour modifier l'adresse de ce serveur, il faut modifier les propriétés suivantes::

  agrosyst.services.email.smtp_host=stmp.inra.fr
  agrosyst.services.email.smtp_port=25

Pour modifier l'adresse de destination des feedback utilisateur, il faut modifier la configuration:;

  agrosyst.services.email.feedback=agrosyst-commits@list.forge.codelutin.com

Enfin, pour les liens contenu dans les mails envoyés, il faut préciser l'adresse de l'application
via la configuration suivante::

  agrosyst.services.application.base_url=http://demo.codelutin.com/agrosyst-stable

Tomcat
------

L'archive WAR doit être déployée dans un conteneur web tomcat 7 (ou equivalent).

Le WAR doit-être déposé ou lié par un lien symbolique dans le dossier webapps.
(on suppose dans la suite que le context est celui par défaut : ``agrosyst-web``)

Modifier le fichier ``server.xml`` pour décoder les adresses en unicode (tomcat <= 7) en ajoutant le parametre ``?nn="UTF-8"`` pour le connecteur AJP::

  <Connector port="8009" protocol="AJP/1.3" redirectPort="8443" URIEncoding="UTF-8" />

Changer la default configuration pour les "post" afin de permettre un post de plus de 2Mo
  sed -i 's/redirectPort="8443"/maxPostSize="52428800"\n               redirectPort="8443"/g' /usr/local/tomcat/conf/server.xml

Pour tomcat >= 8.5 modifier le fichier `context.xml`` pour replacer Rfc6265CookieProcessor par LegacyCookieProcessor voir: https://tomcat.apache.org/migration-85.html
  sed -i 's/<\/Context>/    <CookieProcessor className="org.apache.tomcat.util.http.LegacyCookieProcessor" \/>\n<\/Context>\n/g' /usr/local/tomcat/conf/context.xml


Apache
------

Nous allons ensuite créer un host virtuel apache pour le lier à tomcat via le protocol AJP.

Le module apache ``proxy_ajp`` doit être activé (debian)::

  a2enmod proxy_ajp

Créer ensuite le fichier ``/etc/apache2/sites-available/01-agrosyst``::

  <VirtualHost _default_:80>
    ServerAdmin admin@inrae.fr
    
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/agrosyst-access.log combined
    ErrorLog ${APACHE_LOG_DIR}/agrosyst-error.log
    
    ProxyPass / ajp://localhost:8009/agrosyst-web/
    ProxyPassReverse / ajp://localhost:8009/agrosyst-web/
    
  </VirtualHost>

Acticez le nouveau site virtuel::

  a2ensite 01-agrosyst

Redémarrez apache::

  /etc/init.d/apache2 restart

L'application devrait répondre correctement sur l'adresse définie pour le serveur apache.
