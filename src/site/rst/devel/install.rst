.. -
.. * #%L
.. * Agrosyst
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2013 - 2014 INRA
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Installation locale
===================


Construction depuis les sources
-------------------------------

Maven
~~~~~

Vous devez disposer de `Maven <http://maven.apache.org/>`_ (au moins dans sa version 3.0.0) pour
pouvoir builder le projet.

Le projet utilise un groupe privé sur le dépôt nexus de CodeLutin. Il faut renseigner dans votre
fichier ``$HOME/.m2/settings.xml`` les informations de connexion à ce dépot::

  <server>
    <id>agrosyst-nexus-group</id>
    <username>agrosyst</username>
    <password>ecophyto2018</password>
  </server>

Pour des raisons de sécurité, il est conseillé d'encoder ce mot de passe. Un page d'aide à
l'encodage des mots de passe est disponible sur le `site de maven <http://maven.apache.org/guides/mini/guide-encryption.html>`_.


Code source
~~~~~~~~~~~

Le code source est disponible sur un dépôt `GIT <https://gitlab.nuiton.org/inra/agrosyst/>`_.

Pour récupérer le code source, il suffit de taper la commande suivante::

  git clone https://gitlab.nuiton.org/inra/agrosyst.git

Les informations de connexion sont celle correspond au compte créé sur la `forge CodeLutin <https://forge.codelutin.com/projects/agrosyst>`_.

Une fois le code source récupéré, il peut être compilé et packagé via maven::

  mvn install


Lancement
~~~~~~~~~

Lors de la compilation du projet, maven créé une archive war dans le dossier ''agrosyst/target''
qu'il est possible de déployer dans un conteneur web tel que `Tomcat <http://tomcat.apache.org/>`_.

Une autre solution plus simple est de lancer l'application directement avec maven et
un conteneur tomcat embarqué. Dans le module ``agrosyst-web``, démarrez le serveur via
la commande::

  mvn tomcat7:run

L'application sera ensuite accessible à l'adresse : `http://localhost:8080/agrosyst-web/ <http://localhost:8080/agrosyst-web/>`_.


Configuration
~~~~~~~~~~~~~

Consultez la `page de configuration <config.html>`_ pour configurer l'application.
