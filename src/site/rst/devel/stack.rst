.. -
.. * #%L
.. * Agrosyst
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2013 - 2014 INRA
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Stack technique
===============


Java
----

*Agrosyst* est développé sur la plateforme Java. Le JDK Java est nécessaire dans sa version 6 au minimum pour pouvoir
compiler et exécuter le projet. Toutefois, Code Lutin  préconise l'utilisation de la version 8 du JDK.
Le JDK Java peut être téléchargé sur le site d'`Oracle <http://www.oracle.com/technetwork/java/javase/downloads/>`_.


Maven
-----

Le builder de projet utilisé est `Apache Maven <http://maven.apache.org/>`_. Son rôle est de gérer le cycle de
construction et de déploiement de l'application. L'utilisation de Maven 3.0.5 (ou ultérieure) est recommandée.


Tomcat
------

L'application supporte le standard Servlet 2.5 / JSP 2.1. L'utilisation
d'un serveur respectant ces standards doit être utilisé pour déployer
l'application. `Tomcat <http://tomcat.apache.org/>`_ 6 supporte ces standards. Toutefois, Code Lutin recommande
d'utiliser Tomcat en version 7.


Frameworks Web
--------------

L'application Web sera développée sur le framework MVC `Struts 2 <http://struts.apache.org/development/2.x/>`_ en
utilisant les actions en Java et les vues en JSP. L'utilisation des taglib sera limitée à celle définie dans le core du
framework struts pour ne pas compliquer le code HTML.
`Nuiton-js <http://maven-site.nuiton.org/nuiton-js/>`_ sera utilisé pour inclure des librairies javascript et CSS
externes gérées par maven (sans avoir à la committer dans le projet).


Persistance
-----------

Le framework de persistance `ToPIA <http://maven-site.nuiton.org/topia/>`_ sera utilisé pour :

  * générer le code de persistance depuis le modèle UML ;
  * fournir une API de manipulation de cette persistance pour les services métier ;
  * être indépendant de la technologie de persistance utilisé.

Le code est généré sur le framework `Hibernate <http://www.hibernate.org/>`_, mais les services n'auront une
dépendance que sur le framework ToPIA (le code Hibernate est entièrement géré et
généré, il n'est donc pas à développer ni à maintenir).


Gestion de la transaction
-------------------------

La gestion de la transaction n'est pas encore définie. Elle dépendra de l'architecture
définitive de l'application:

  * architecture client/serveur avec un serveur déporté ;
  * architecture locale où les contrôleur struts utilisent directement les services.

La gestion de la transaction pourrait s'orienter vers :

  * Spring-tx ;
  * Nuiton-web (TopiaTransactionFilter) ;
  * etc ...


Sécurité
--------

La gestion de la sécurité est gérée par le framework `Apache Shiro <http://shiro.apache.org/>`_.
Toutefois, la mise en place de la sécurité dépendra fortement du modèle de données
et de la gestion de la sécurité dans le reste de l'application.


Journalisation
--------------

`Plus de détails sur la journalisation <log.html>`_.


Cartographie
------------

Les fonds de carte seront probablement fournis par l'IGN selon un accord avec l'INRA.
L'affichage d'informations par dessus les tuiles se fera par le biais de la bibliothèque javascript `Leaflet <http://leafletjs.com/>`_.

À noter que la librairie `Open Layers <https://openlayers.org/>`_ pourrait-être une alternative

Recherche
---------

La fonctionnalité de recherche présentes sot :

  * recherche ciblée sur certains champs (filtre) ;
  * navigation en entonnoir ;

Suivant le besoin exprimé, la solution retenue pourra être :

  * Requêtage SQL ;
  * Indexation sur `Apache lucene <http://lucene.apache.org>`_ ;
  * Indexation sur `Apache Solr <http://lucene.apache.org/solr/>`_ (facetisation, répartition de charge, ...) ;
  * ...
