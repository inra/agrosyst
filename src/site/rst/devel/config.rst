.. -
.. * #%L
.. * Agrosyst
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2013 - 2014 INRA
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/gpl-3.0.html>.
.. * #L%
.. -

Configuration
=============

L'application se configure via le fichier de configuration ``agrosyst-<context>.properties``.

Pour pouvoir supporter plusieurs instances sur une même JVM, le nom de fichier contient le nom de contexte web.
C'est à dire que si l'application est déployée en tant que ``http://localhost:8080/azerty/``, le fichier de configuration doit se nommer ``agrosyst-azerty.properties``.
Autre exemple, si l'application est déployée en tant que ``http://localhost:8080/agrosyst-demo/``, le fichier de configuration doit se nommer ``agrosyst-agrosyst-demo.properties`` (notez la répétition de "agrosyst-").
Enfin, si le contexte ne porte pas de nom (cas ``http://localhost:8080/``), le fichier de configuration doit se nommer ``agrosyst-ROOT.properties``.

Son emplacement dépend du système d'exploitation::

  * windows: ``%APPDATA%/agrosyst.properties``
  * linux: ``$HOME/.config/agrosyst.properties``
  * max os: ``$HOME/Library/Application Support/agrosyst.properties``



Configuration pour PostgreSQL
-----------------------------

Dans le fichier de configuration d'Agrosyst, surchargez la configuration de la base de données de
la façon suivante::

  hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
  jakarta.persistence.jdbc.driver=org.postgresql.Driver
  jakarta.persistence.jdbc.url=jdbc:postgresql:agrosyst
  jakarta.persistence.jdbc.user=agrosyst
  jakarta.persistence.jdbc.url=agrosyst
  hibernate.hbm2ddl.auto=validate

Configuration du pool de connexion à la base de données
-------------------------------------------------------

  //HIKARICP POOL
  hibernate.connection.provider_class=org.hibernate.hikaricp.internal.HikariCPConnectionProvider
  hibernate.hikari.minimumIdle=5
  hibernate.hikari.maximumPoolSize=20
  hibernate.hikari.idleTimeout=600000

  //C3P0 POOL
  hibernate.c3p0.min_size=5        // Minimum size of C3P0 connection pool
  hibernate.c3p0.max_size=20       // Maximum size of C3P0 connection pool
  hibernate.c3p0.timeout=600       // Maximum idle time for C3P0 connection pool
  hibernate.c3p0.max_statements=50 // Maximum size of C3P0 statement cache

Configuration du cache applicatif
---------------------------------

  agrosyst.services.businessCaching.enabled=true
  agrosyst.services.businessCaching.duration=5       // minutes (concervation en cache de données issue principalement de référentiels)
  agrosyst.services.businessCaching.shortDuration=30 // seconds (concervation des rôles utilisateur)
