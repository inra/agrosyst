#!/bin/bash

DB_DIR=/home/postgresql-12
DB=inra-agrosyst-latest-272
sudo docker run \
  --name postgres-12-${DB} \
  --restart always \
  --shm-size=2g \
  -v ${DB_DIR}/${DB}:/var/lib/postgresql/data \
  -e POSTGRES_DB=${DB} \
  -e POSTGRES_USER=agrosystuserprod \
  -e POSTGRES_PASSWORD=ecophyto2018 \
  -p 5433:5432 \
  -d postgres:12
