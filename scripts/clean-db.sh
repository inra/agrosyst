#!/bin/bash

DB_DIR=/home/postgresql-12
DB=inra-agrosyst-latest
sudo docker stop postgres-12-${DB}
sudo rm ${DB_DIR}/${DB} -rf
sudo docker start postgres-12-${DB}
