# Dockerization serveur de demo-test (Agrosyst)

## Installer Docker

Suivre la documentation https://docs.docker.com/engine/install/

## Ajouter les utilisateurs au groupe Docker

Pour chaque utilisateur autorisé à utiliser Docker :

    usermod -a -G docker athimel

## Installer docker-compose

Suivre la documentation https://docs.docker.com/compose/install/

## Création du dossier d'installation (stack)

    mkdir -p /opt/docker/agrosyst-demo-test

On travaillera dans ce dossier maintenant

    cd /opt/docker/agrosyst-demo-test

Création des sous-dossiers :

    mkdir receipe
    mkdir config

## Mise en place de la recette pour la construction de l'image Agrosyst

    mkdir -p receipe/app

Copier dans le dossier `receipe/app` le fichier `server-ajp.xml` fourni ainsi que le fichier `Dockerfile` tel que :

```dockerfile
FROM tomcat:8.5-jdk8    
MAINTAINER Arnaud Thimel <thimel@codelutin.com>

# As tomcat8.5 use Rfc6265CookieProcessor as default we must specified to use LegacyCookieProcessor see: https://tomcat.apache.org/migration-85.html
RUN sed -i 's/<\/Context>/    <CookieProcessor className="org.apache.tomcat.util.http.LegacyCookieProcessor" \/>\n<\/Context>\n/g' /usr/local/tomcat/conf/context.xml

# Installe un server.xml compatible AJP (connecteur)
COPY ./server-ajp.xml /usr/local/tomcat/conf/server.xml

ENV BASE_URL="https://nexus.nuiton.org/nexus/content/repositories/agrosyst-release/fr/inra/agrosyst/agrosyst-web/"
ARG VERSION

RUN echo "Download and verify version ${VERSION}" \
    && wget -q ${BASE_URL}/${VERSION}/agrosyst-web-${VERSION}.war \
    && wget -q ${BASE_URL}/${VERSION}/agrosyst-web-${VERSION}.war.sha1 \
    && echo " agrosyst-web-${VERSION}.war" >> agrosyst-web-${VERSION}.war.sha1 \
    && sha1sum -c agrosyst-web-${VERSION}.war.sha1 \
    && rm agrosyst-web-${VERSION}.war.sha1

RUN rm -fr /usr/local/tomcat/webapps/ROOT* \
    && mv agrosyst-web-${VERSION}.war /usr/local/tomcat/webapps/demo-test.war

EXPOSE 8009
```

## Installation des fichiers de configuration.

Il y a 2 fichiers à placer dans le dossier `config` :
  - agrosyst-demo-test.properties
  - agrosyst-demo-test-log4j.xml

NB : Ces fichiers sont potentiellement à adapter

## Création de la stack

Le dernier élément à créer est le fichier `docker-compose.yml`.
Dans le cas de démo-test il est simple car il suffit de déployer le service de l'application (pas d'autre service à déployer).

```yml
version: '3.8'

services:

 app:
    build:
      context: ./receipe/app
      args:
        VERSION: 2.63 
    volumes:
     - /etc/localtime:/etc/localtime:ro
     - /etc/timezone:/etc/timezone:ro
     - /opt/tomcat-commons/setenv.sh:/usr/local/tomcat/bin/setenv.sh:ro
     - ./config/agrosyst-demo-test.properties:/etc/agrosyst-default.properties
     - ./config/agrosyst-demo-test-log4j.xml:/etc/agrosyst-default-log4j.xml:ro
     - ./logs:/opt/tomcat8/logs/agrosyst-demo-test
    ports:
     - 8309:8009
    restart: always
```

NB : On voit que le port exposé est le port `8309` qu'il faudra ensuite configurer dans la conf Apache2

## Configuration Apache2/modJK

Il faut ajouter le point de montage JK dans `/etc/apache2/mods-enabled/jk.conf`:

```
    JKMount /demo-test/* loadbalancer-demo-test
```

On indique ici que toutes les requêtes `/demo-test/*` doivent être envoyées au loadbalancer nommé `loadbalancer-demo-test`.

Celui-ci est défini dans le fichier `/etc/libapache2-mod-jk/workers-ecoinfo.properties` :

```properties
worker.list=[...], loadbalancer-demo-test

worker.demo-test.port=8309
worker.demo-test.host=localhost
worker.demo-test.type=ajp13

worker.demo-test.lbfactor=1

worker.loadbalancer-demo-test.type=lb
worker.loadbalancer-demo-test.balance_workers=demo-test
```

Recharger la conf avec la commande :

    sudo /etc/init.d/apache2 reload

## Démarrage  de la stack

Démarrage :

    docker-compose up -d --build

Arrêt :

    docker-compose down

Déploiement d'une nouvelle version :
 - modifier la version dans le `docker-compose.yml`
 - construire la nouvelle version : `docker-compose build`
 - redémarrer : `docker-compose down && docker-compose down up -d`
