# AGROSYST [![build status](https://gitlab.nuiton.org/codelutin/agrosyst/badges/develop/build.svg)](https://gitlab.nuiton.org/codelutin/agrosyst/commits/develop) [![coverage report](https://gitlab.nuiton.org/codelutin/agrosyst/badges/develop/coverage.svg)](https://gitlab.nuiton.org/codelutin/agrosyst/commits/develop)

AGROSYST pour le plan ECOPHYTO 2018

# Gestion de projet

Retrouvez le suivi des tickets et inscrivez-vous aux listes de discussion sur notre forge Redmine : https://forge.codelutin.com/projects/agrosyst


## Configuration de l'application

L'archive WAR d'Agrosyst embarque une configuration par défaut qui n'est PAS optimale pour un serveur de production.

Il est possible de surcharger tous les éléments configurables dans le fichier ``agrosyst-<context>.properties``

Son emplacement dépend du système d'exploitation :
  * windows: ``%APPDATA%/agrosyst.properties``
  * linux: ``$HOME/.config/agrosyst.properties``
  * max os: ``$HOME/Library/Application Support/agrosyst.properties``

Pour pouvoir supporter plusieurs instances sur une même JVM, le nom de fichier contient le nom de contexte web.
C'est-à-dire que si l'application est déployée en tant que ``http://localhost:8080/azerty/``, le fichier de configuration doit se nommer ``agrosyst-azerty.properties``.
Autre exemple, si l'application est déployée en tant que ``http://localhost:8080/agrosyst-demo/``, le fichier de configuration doit se nommer ``agrosyst-agrosyst-demo.properties`` (notez la répétition de "agrosyst-").
Enfin, si le contexte ne porte pas de nom (cas ``http://localhost:8080/``), le fichier de configuration doit se nommer ``agrosyst-ROOT.properties``.

### Ensemble des paramètres configurables de l'application :

Attention à bien changer les variables suivantes : 

- `jakarta.persistence.url`
- `jakarta.persistence.jdbc.user` et `jakarta.persistence.jdbc.password`
- `agrosyst.services.application.base_url`
- `agrosyst.services.email.smtp_host` et `agrosyst.services.email.port`
- `agrosyst.services.log.file.location`
- `agrosyst.web.jwtSecret` (on peut mettre n'importe quelle valeur)

Et, à moins d'en avoir besoin, on peut se passer des variables suivantes : 

- `agrosyst.web.hazelcast*`
- `agrosyst.services.jmsUrl`
- `hibernate.hbm2ddl.auto`

```
agrosyst.services.demoMode=false       // paramètre déprécié le mode demo n'est plus maintenu
agrosyst.web.list.results.per.page=10  // nombre d'élément à afficher sur les listes

//TÉLÉCHARMENT DE CONTENUE
agrosyst.services.file.encoding=UTF_8
agrosyst.web.upload.attachmentsMaxSize=10240000
agrosyst.web.upload.globalMaxSize=51200000
agrosyst.web.upload.allowedExtensions=gif,jpeg,jpg,png,zip,pdf,doc,xls,odt,docx,xlsx,ppt,pptx,txt,csv

//BANNER HAUT DE PAGE GAUCHE
agrosyst.web.banner.text=INRAE DEMO TEST
agrosyst.web.banner.style=info // Error, warning, info
        
//CARTOGRAPHIE IGN
agrosyst.web.ign.keyjs=null // Clef permettant d'acéder à la cartographie IGN

//SÉCURITÉ
agrosyst.services.security.tokenStorageEnabled=false //false true:stockage des tokens en base de données (si déployé sur plusieurs instances et non sicky session); false: stockage en mémoire (plus performant mais réservé aux mono-instances ou sticky session)
agrosyst.web.sessionTimeout=7200 //durée en s de la session http, focée à illimitée pour les utilisateur ayant le rôle administrateur

//HIBERNATE BASE DE DONNÉES
jakarta.persistence.jdbc.driver=org.postgresql.Driver
hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
jakarta.persistence.jdbc.url=jdbc:postgresql://postgresql:5432/inra-agrosyst-latest
jakarta.persistence.jdbc.user=agrosyst
jakarta.persistence.jdbc.url=****
hibernate.hbm2ddl.auto=validate

// Le pool de connexion à la base de données
// Code Lutin recomande l'utilisation d'HIKARICP POOL plûtot que C3P0
//HIKARICP POOL
hibernate.connection.provider_class=org.hibernate.hikaricp.internal.HikariCPConnectionProvider
hibernate.hikari.minimumIdle=5
hibernate.hikari.maximumPoolSize=20
hibernate.hikari.idleTimeout=600000
        
//C3P0 POOL
hibernate.c3p0.min_size=5        // Minimum size of C3P0 connection pool
hibernate.c3p0.max_size=20       // Maximum size of C3P0 connection pool
hibernate.c3p0.timeout=600       // Maximum idle time for C3P0 connection pool
hibernate.c3p0.max_statements=50 // Maximum size of C3P0 statement cache

//MIGRATION ne pas modifier ces paramètres
topia.service.migration=org.nuiton.topia.flyway.TopiaFlywayServiceImpl
topia.service.migration.useModelVersion=false

//EMAIL
//pour les liens contenu dans les mails envoyés, il faut préciser l'adresse de l'application via la configuration suivante::
agrosyst.services.application.base_url=https://agrosyst.fr

agrosyst.web.email.contact=agrosyst-devel@list.forge.codelutin.com
agrosyst.services.email.enabled=true // L'envoi de mail est-il activé ou non ? Par défaut, oui
agrosyst.services.email.smtp_host=localhost
agrosyst.services.email.smtp_port=25
agrosyst.services.email.from=no-reply+agrosyst@inra.fr
agrosyst.services.email.feedbac=agrosyst-commits@list.forge.codelutin.com
agrosyst.services.email.support=agrosyst-support@inra.fr

#LOGGING chemin ver le fichier de configuration des logs
agrosyst.services.log.file.location=/usr/local/agrosyst/agrosyst-log4j.conf

//CONFIGURATION DU POOL DE THREADS
agrosyst.services.asyncImmediatePoolSize=8         // nombre de threads pouvant être exécuter en paralèle pour les tâches planiffiée (actuellement seuls les exports de performances sont gérés)

//CACHE APPLICATIF
agrosyst.services.businessCaching.enabled=true
agrosyst.services.businessCaching.duration=5       // minutes (concervation en cache de données issue principalement de référentiels)
agrosyst.services.businessCaching.shortDuration=30 // seconds (concervation des rôles utilisateur)

agrosyst.services.referential.acta_dosage_spc_complet.id_groupe_culture_zones_cultivees=428

#URL D'ACCÈS À l'API de DephyGraph
agrosyst.web.dephyGraphApiUrl= null
#URL de redirection vers DephyGraph (si non renseigné on utilise celle définie pour agrosyst.web.dephyGraphApiUrl
agrosyst.web.dephyGraphUrl= null

//URL DE MISE À JOUR DES RÉFÉRENTIELS
agrosyst.web.maaReferentialUrl=https://alim.api.agriculture.gouv.fr/ift/api/produits-doses-reference

//PARAMÈTRE METIER D'AGROSYST
agrosyst.services.species.graftsupport.codesection=26
agrosyst.services.species.graftsupport.utilisation=PG
agrosyst.services.species.graftslone.typevarietal=CLO
agrosyst.services.referential.ref_materiel_outil.default_code_value=OUTI999999999
agrosyst.services.referential.ref_materiel_traction.default_code_value=TRAC999999999
agrosyst.services.referential.ref_materiel_irrigation.default_code_value=IRRI999999999
agrosyst.services.referential.ref_materiel_automoteur.default_code_value=AUTO999999999
agrosyst.services.lowerCampaignBound=2000
agrosyst.services.upperCampaignBound=2999

// EXPORTS ASYNCHRONES
agrosyst.web.domainsExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des domaines
agrosyst.web.plotsExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des parcelles
agrosyst.web.growingPlansExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des dispositifs
agrosyst.web.growingSystemsExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des systèmes de culture
agrosyst.web.effectiveCropCyclesExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des interventions culturales
agrosyst.web.effectiveMeasurementsExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des mesures et observations
agrosyst.web.managementModesExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des modèles de gestion
agrosyst.web.decisionRulesExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des règles de décision
agrosyst.web.practicedSystemsExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des systèmes synthétisés
agrosyst.web.reportGrowingSystemsExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des bilans de campagne (échelle régionale)
agrosyst.web.reportRegionalsExportAsyncThreshold=5 // seuil de bascule en asynchone sur l'export des bilans de campagne (échelle système de culture)
agrosyst.services.downloadableRetentionDays=7 // durée (jours) de rétention en base des fichiers générés

// TÂCHES EN ARRIÈRE-PLAN
agrosyst.services.cronJobsEnabled=true // Autorise ou non les jobs automatiques

// SYNCHRO MULTI-INSTANCES
agrosyst.services.jmsUrl= // Adresse du serveur JMS. Exemple : `tcp://localhost:10002`. La synchro sera désactivée s'il n'y a aucune valeur

// STATELESS
agrosyst.web.jwtSecret= // Clé pour la signature des tokens JWT. C'est une chaîne de caractère arbitraire. Dès que cette chaîne change, les token JWT générés ne seront plus valides et les utilisateurs seront donc tous déconnectés de l'application ;
agrosyst.web.jwtRefreshSeconds=300 // Durée (en secondes) au bout de laquelle un token JWT est automatiquement rafraîchi

// CACHE DISTRIBUÉ
agrosyst.web.hazelcastPort= // Port sur lequel le cache partagé sera exposé. Par défaut ce port est 5701
agrosyst.web.hazelcastOutboundPorts= // Liste des ports autorisés en sortie pour la synchro vers les autres membres.
agrosyst.web.hazelcastMembers= // Liste des membres de ce cache distribué. Si cette propriété n'est pas valuée, il n'y a pas de cache distribué. Exemple: tangara4:5701,zimzoum:15463

// IPMWORKS
ipmworks.sectors=1, // liste des ids de types de conduite à afficher
ipmworks.typeConduitesIds=1, // liste des ids de types de conduite à afficher

agrosyst.web.allowedRestRequestOrigins=http://localhost:8080,http://localhost:5173
```

## Configuration du logger

    <?xml version="1.0" encoding="UTF-8"?>
    <Configuration packages="fr.inra.agrosyst" strict="true">

      <Appenders>
        <RollingFile name="File"
                   fileName="../agrosyst.log"
                   filePattern="../agrosyst_%d{yyyy-MM-dd}.log">
          <PatternLayout pattern="%d{HH:mm:ss} [%t] %-5p %c{1}:%L - %m%n"/>
          <Policies>
            <TimeBasedTriggeringPolicy interval="1"/>
          </Policies>
        </RollingFile>
      </Appenders>

      <Loggers>
        <Logger name="fr.inra.agrosyst" level="info"/>
        <Logger name="fr.inra.agrosyst.services.AgrosystConfigurationHelper" level="info"/>
        <Logger name="fr.inra.agrosyst.services.referential.ImportServiceImpl" level="debug"/>
        <logger name="fr.inra.agrosyst.services.report.ReportServiceImpl" level="info"/>
        <logger name="fr.inra.agrosyst.web.actions"  level="error"/>
        <Logger name="org.nuiton.topia.persistence.internal.HibernateProvider" level="info"/>
        <Logger name="org.nuiton.topia.persistence.util.TopiaUtil" level="error"/>
        <Logger name="fr.inra.agrosyst.web" level="info"/>

        <Root level="warn">
          <AppenderRef ref="File" />
        </Root>
      </Loggers>
    </Configuration>

## Configuration de Tomcat

Version minimum de tomcat: 8

Il faut modifier le fichier server.xml pour autoriser la soumission de formulaires HTML de plus de 2Mo
    
    sed -i 's/redirectPort="8443"/maxPostSize="52428800"\n               redirectPort="8443"/g' server.xml
    
À partir de tomcat 8.5, il faut modifier le fichier context.xml et ajouter la ligne suivante à l'intérieur de la balise `<Context\>\</Context\>` :

    <CookieProcessor className="org.apache.tomcat.util.http.LegacyCookieProcessor"/>

voir [https://tomcat.apache.org/migration-85.html](https://tomcat.apache.org/migration-85.html) pour plus de détails.

## Configuration de JETTY


Si l'application est déployée sur JETTY, il est nécessaire de modifier la valeur de l'attribut "maxFormContentSize" pour lui assigner une valeur supérieure à celle par défaut (200000)
La valeur 5000000 fonctionne.

Voir la documentation suivante :
<http://www.eclipse.org/jetty/documentation/current/setting-form-size.html>

Dans le cas où on utilise Jetty (recommandé en local), il n'est pas nécessaire d'avoir de Tomcat ni de serveur Apache. Dans IntelliJ, pour ce faire, il suffit de créer une nouvelle configuration d'exécution. En commande (champ "Run en anglais"), renseigner `clean jetty:run`, et dans les Java Options : 

- décocher "Inherit from Settings"
- utiliser un JDK correspondant à la version de Java d'Agrosyst actuellement en vigueur et ajouter dans les VM Options : `-Dorg.eclipse.jetty.server.Request.maxFormContentSize=5000000 -Djetty.port=8080`

## Configuration d'Apache

Création d'un host virtuel apache pour le lier à tomcat via le protocol AJP.

Le module apache ``proxy_ajp`` doit être activé (debian) :

    a2enmod proxy_ajp

Créer ensuite le fichier ``/etc/apache2/sites-available/01-agrosyst`` :

      <VirtualHost _default_:80>
        ServerAdmin admin@inra.fr
        
        LogLevel warn
        CustomLog ${APACHE_LOG_DIR}/agrosyst-access.log combined
        ErrorLog ${APACHE_LOG_DIR}/agrosyst-error.log
        
        ProxyPass / ajp://localhost:8009/agrosyst-web/
        ProxyPassReverse / ajp://localhost:8009/agrosyst-web/
        
      </VirtualHost>

Activez le nouveau site virtuel :

    a2ensite 01-agrosyst

Redémarrez apache :

    /etc/init.d/apache2 restart

L'application devrait répondre correctement sur l'adresse définie pour le serveur apache.

## Installer un certificat

Pour l'API MAA, il est possible que le certificat ne soit pas validable par le JDK, auquel cas il faut l'ajouter
manuellement. Pour cela, ouvrir Firefox/Chrome/... à l'URL concernée et télécharger le certificat. 
Ou alors, utilisez la ligne de commande suivante :

```bash
echo | openssl s_client -servername alim.agriculture.gouv.fr -connect alim.agriculture.gouv.fr:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > [cheminDuFichierOùEnregistrerLeCertificat]
```

Ensuite il faut l'importer avec la commande suivante :

```bash
${DOSSIER_JDK}/bin/keytool -import -alias agrigouvfr -keystore ${DOSSIER_JDK}/lib/security/cacerts -file [cheminVersLeCertificat]
```

Un mot de passe est demandé, par défaut c'est `changeit`

## Configuration de Posgresql

Agrosyst utilise l'extension 'unaccent' de Postgresql
Cette extension doit être activée simplement de cette façon:

```
CREATE EXTENSION unaccent;
```

## Participer au projet


### Contexte:


Chez Code Lutin nous utilisons GIT avec le workflow `gitflow`.
La documentation suivante décrit les étapes essentielles à respecter :
<http://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html>

### Récupération des sources du projet

    git clone https://gitlab.nuiton.org/inra/agrosyst.git

### Initialisation de gitflow

    git flow init

Répondre aux questions de la façon suivante :

    Which branch should be used for bringing forth production releases?
    Branch name for production releases: [] master

    Which branch should be used for integration of the "next release"?
    Branch name for "next release" development: [develop]

    How to name your supporting branch prefixes?
    Feature branches? [feature/] 
    Release branches? [release/] 
    Hotfix branches? [hotfix/] 
    Support branches? [support/]
    Version tag prefix? []

### Travailler sur une branche

Lorsque qu'un ticket est traité il est recommandé de le faire sur une branche dédiée
La commande de création d'une branche est

    git flow feature start nom_de_la_branche

Nous avons comme usage pour le nom de la branche de commencer par le numéro de ticket puis une description de celui-ci.
Le tout en caractères minuscules avec les mots séparés entres eux par le caractère '-'.

Exemple :


    git flow feature start 9036-migrate-svn-to-git


À présent vous êtes sur la branche nouvellement créée. Celle-ci évoluera de façon autonome à la branche principale (develop)


Vous avez fait des modifications et vous souhaitez les commiter.


la commande `git status` vous liste les différences entre votre dépôt local vos fichiers.


Si vous devez vous mettre à jour avec le dépôt distant, utilisez la commande `git pull`.


Si vous devez ajouter des fichiers utilisez la commande `git add`.


Pour commiter vos changements sur votre dépôt local utilisez la commande `git commit`


Lorsque vous souhaitez pousser vos modifications sur le dépôt distant la commande est `git push`.


Si la branche distante n'existe pas elle sera créée grace à la commande `git push --set-upstream origin feature/nom_de_la_branche`

Lorsque votre ticket est terminé et que vous souhaitez le pousser sur la branche 'develop' la commande est `git flow feature finish` suivi d'un `git push`,
vous vous retrouvez alors directement sur la branche 'develop'.

### Code Source

Pour récupérer le code source, il suffit de taper la commande suivante :

    git clone https://gitlab.nuiton.org/inra/agrosyst.git

Une fois le code source récupéré, il peut être compilé et packagé via maven :

    mvn clean install
  
### Lancement

Lors de la compilation du projet, maven créé une archive war dans le dossier ''agrosyst/target''
qu'il est possible de déployer dans un conteneur web tel que Tomcat http://tomcat.apache.org.

Une autre solution plus simple est de lancer l'application directement avec maven et
un conteneur tomcat embarqué. Dans le module ``agrosyst-web``, démarrez le serveur via
la commande :

    mvn clean verify org.codehaus.cargo:cargo-maven3-plugin:run
ou

    mvn clean jetty:run

/!\ attention, en fonction de l'une ou l'autre des commandes exécutées le nom du fichier de configuration recherché sera différent. Vérifier les logs et rechercher 'Load config file name' pour le connaitre.

L'application sera ensuite accessible à l'adresse : http://localhost:8080/agrosyst-web/

### Topia

La persistence de données se base sur le framework ToPia http://topia.nuiton.org/

L'édition du modèle de données se fait en utilisant ArgoUML. Ce projet est hébergé ici: https://github.com/argouml-tigris-org/argouml

La version 0.35.1 d'ArgoUML est téléchargeable ici: https://github.com/argouml-tigris-org/argouml/releases/download/VERSION_0_35_1/ArgoUML-0.35.1.tar.gz

Une fois l'archive extraite, il est conseillé de lancer ArgoUML avec cette commande (en étant dans le répertoire nouvellement extrait):

    java -jar argouml.jar

Le projet ArgoUML pour Agrosyst est ``agrosyst-api/src/main/xmi/agrosyst-model.zargo``

### VSCode x Vue3 nouvelle App

Prettier est mis par défaut comme formatteur de code dans le nouveau projet front Vue3.

Pour l'activer par défaut dans VSCoce, il faut installer l'extension Prettier dans VSCode et ajouter ces paramètres à votre setting.json :

```json
{
    ...
    "editor.formatOnSave": true,
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    ...
}
```

il est recommandé d'installer l'extension Vue officiel dans VSCode également.
