package fr.inra.agrosyst.api.services.edaplos;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.services.AgrosystService;

import java.io.InputStream;

/**
 * Edaplos service.
 * To persist eDaplos datas
 *
 * @author eancelet@orleans.inra.fr
 */
public interface EdaplosService extends AgrosystService {

    String storeEdaplosFile(InputStream inputStream, String fileName, String contentType);

    /**
     * return the result of Edaplos parsing
     * @param edaplosFileId the Edaplos file's ID
     * @return the result of Edaplos parsing
     */
    EdaplosParsingResult validEdaplosData(String edaplosFileId);

    /**
     * Import parse data
     *
     * @param edaplosFileId the Edaplos file's ID
     * @return import result as class name, result
     */
    EdaplosParsingResult importEdaplos(String edaplosFileId);

    /**
     * Export edaplos report as CSV.
     *
     * @param edaplosFileId the Edaplos file's ID
     * @return input stream
     */
    InputStream exportCSVEdaplosReport(String edaplosFileId);

    /**
     * return the result of Edaplos parsing
     * @param inputStream the Edaplos file
     * @return the result of Edaplos parsing
     */
    @Deprecated
    EdaplosParsingResult validEdaplosData(InputStream inputStream, String fileName);

    /**
     * Import parse data
     *
     * @param inputStream the Edaplos file
     * @return import result as class name, result
     */
    @Deprecated
    EdaplosParsingResult importEdaplos(InputStream inputStream, String fileName);

    /**
     * Export edaplos report as CSV.
     *
     * @param inputStream the Edaplos file
     * @return input stream
     */
    @Deprecated
    InputStream exportCSVEdaplosReport(InputStream inputStream, String fileName);

    void sendExceptionFeedbackEmail(Exception exception, String context, String fileName);

    void sendExceptionFeedbackEmail(String edaplosFileId, Exception exception, String context);

    RefLocation searchCommune(String postCode, String researchedCommuneName);
}
