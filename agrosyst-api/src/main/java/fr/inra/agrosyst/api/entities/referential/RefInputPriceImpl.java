/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

package fr.inra.agrosyst.api.entities.referential;

import fr.inra.agrosyst.api.entities.PriceUnit;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serial;
import java.util.function.Function;

public abstract class RefInputPriceImpl extends RefInputPriceAbstract {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 1L;
    
    // refs #9696
    public final static Function<Pair<PriceUnit, PriceUnit>, Double> GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE = input -> {
        if (input.equals(Pair.of(PriceUnit.EURO_T, PriceUnit.EURO_T))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_T, PriceUnit.EURO_Q))) return 0.1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_T, PriceUnit.EURO_KG))) return 0.001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_T, PriceUnit.EURO_G))) return 0.000001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_T, PriceUnit.EURO_MG))) return 0.000000001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_Q, PriceUnit.EURO_Q))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_Q, PriceUnit.EURO_T))) return 10d;
        else if (input.equals(Pair.of(PriceUnit.EURO_Q, PriceUnit.EURO_KG))) return 0.01d;
        else if (input.equals(Pair.of(PriceUnit.EURO_Q, PriceUnit.EURO_G))) return 0.00001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_Q, PriceUnit.EURO_MG))) return 0.00000001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG, PriceUnit.EURO_KG))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG, PriceUnit.EURO_L))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG, PriceUnit.EURO_T))) return 1000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG, PriceUnit.EURO_Q))) return 100d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG, PriceUnit.EURO_G))) return 0.001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG, PriceUnit.EURO_MG))) return 0.000001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_G, PriceUnit.EURO_G))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_G, PriceUnit.EURO_T))) return 1000000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_G, PriceUnit.EURO_Q))) return 100000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_G, PriceUnit.EURO_KG))) return 1000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_G, PriceUnit.EURO_MG))) return 0.001d;
        else if (input.equals(Pair.of(PriceUnit.EURO_MG, PriceUnit.EURO_MG))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_MG, PriceUnit.EURO_G))) return 1000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_MG, PriceUnit.EURO_KG))) return 1000000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_MG, PriceUnit.EURO_Q))) return 100000000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_MG, PriceUnit.EURO_T))) return 1000000000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HL_WINE, PriceUnit.EURO_HL_WINE))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HL_WINE, PriceUnit.EURO_KG_GRAPE))) return 0.006375d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG_GRAPE, PriceUnit.EURO_KG_GRAPE))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG_GRAPE, PriceUnit.EURO_HL_WINE))) return 156.86d;
        else if (input.equals(Pair.of(PriceUnit.EURO_L, PriceUnit.EURO_L))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_L, PriceUnit.EURO_KG))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_L, PriceUnit.EURO_M3))) return 1000d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HL, PriceUnit.EURO_HL))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HL, PriceUnit.EURO_L))) return 100d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HL, PriceUnit.EURO_M3))) return 10d;
        else if (input.equals(Pair.of(PriceUnit.EURO_M3, PriceUnit.EURO_M3))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_M3, PriceUnit.EURO_L))) return 0.001d;
        
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_HA))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_DEG_PURE_ALCOHOL, PriceUnit.EURO_DEG_PURE_ALCOHOL))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_PLANT, PriceUnit.EURO_PLANT))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_POT, PriceUnit.EURO_POT))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_T_SUGAR, PriceUnit.EURO_T_SUGAR))) return 1d;
        else if (input.equals(Pair.of(PriceUnit.EURO_UNITE, PriceUnit.EURO_UNITE))) return 1d;
            
            // EURO_HA is default value so it must be ignored
        else if (input.equals(Pair.of(PriceUnit.EURO_T, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_Q, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_G, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_MG, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HL_WINE, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_KG_GRAPE, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_L, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HL, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_M3, PriceUnit.EURO_HA))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_T))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_Q))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_KG))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_G))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_MG))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_HL_WINE))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_KG_GRAPE))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_L))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_HL))) return 0d;
        else if (input.equals(Pair.of(PriceUnit.EURO_HA, PriceUnit.EURO_M3))) return 0d;
        
        else {
            return null;
        }
    };

}
