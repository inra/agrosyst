package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PracticedPerennialCropCycleDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -678103284308819955L;

    protected PracticedPerennialCropCycle practicedPerennialCropCycle;

    protected List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos;

    protected List<String> croppingPlanSpeciesIds;

    protected List<PracticedCropCycleSpeciesDto> speciesDto;

    protected String croppingPlanEntryName;

    public List<PracticedCropCyclePhaseDto> getCropCyclePhaseDtos() {
        return cropCyclePhaseDtos;
    }

    public void setCropCyclePhaseDtos(List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos) {
        this.cropCyclePhaseDtos = cropCyclePhaseDtos;
    }

    public void addCropCyclePhaseDtos(PracticedCropCyclePhaseDto cropCyclePhaseDto) {
        if (cropCyclePhaseDtos == null) {
            cropCyclePhaseDtos = new ArrayList<>();
        }
        cropCyclePhaseDtos.add(cropCyclePhaseDto);
    }

    public List<String> getCroppingPlanSpeciesIds() {
        return croppingPlanSpeciesIds;
    }

    public void setCroppingPlanSpeciesIds(List<String> croppingPlanSpeciesIds) {
        this.croppingPlanSpeciesIds = croppingPlanSpeciesIds;
    }

    public void addCroppingPlanSpeciesIds(String croppingPlanSpeciesId) {
        if (croppingPlanSpeciesIds == null) {
            croppingPlanSpeciesIds = new ArrayList<>();
        }
        croppingPlanSpeciesIds.add(croppingPlanSpeciesId);
    }

    public List<PracticedCropCycleSpeciesDto> getSpeciesDto() {
        return speciesDto;
    }

    public void setSpeciesDto(List<PracticedCropCycleSpeciesDto> speciesDto) {
        this.speciesDto = speciesDto;
    }

    public void addSpeciesDto(PracticedCropCycleSpeciesDto species) {
        if (speciesDto == null) {
            speciesDto = new ArrayList<>();
        }
        speciesDto.add(species);
    }

    public PracticedPerennialCropCycle getPracticedPerennialCropCycle() {
        return practicedPerennialCropCycle;
    }

    public void setPracticedPerennialCropCycle(PracticedPerennialCropCycle practicedPerennialCropCycle) {
        this.practicedPerennialCropCycle = practicedPerennialCropCycle;
    }

    public String getCroppingPlanEntryName() {
        return croppingPlanEntryName;
    }

    public void setCroppingPlanEntryName(String croppingPlanEntryName) {
        this.croppingPlanEntryName = croppingPlanEntryName;
    }
}
