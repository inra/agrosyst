package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2019 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Objects;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformancePlotExecutionContext {
    
    protected final Plot plot;
    protected Plot anonymizePlot;
    
    protected String its;
    protected String irs;
    
    protected Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages;
    
    public PerformancePlotExecutionContext(Plot plot) {
        this.plot = plot;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformancePlotExecutionContext that = (PerformancePlotExecutionContext) o;
        return plot.equals(that.plot);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(plot);
    }
}
