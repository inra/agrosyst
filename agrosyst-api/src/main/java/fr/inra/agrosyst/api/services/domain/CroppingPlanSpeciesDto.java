package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CompagneType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Getter
@Setter
public class CroppingPlanSpeciesDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -3018093993881165475L;

    protected String topiaId;
    protected String code;
    protected boolean validated;
    protected Integer speciesArea;
    // from RefEspece
    protected String speciesId;
    protected String speciesEspece;//Libelle_espece_botanique
    protected String speciesQualifiant;
    protected String speciesTypeSaisonnier;
    protected String speciesDestination;
    protected String profil_vegetatif_BBCH;
    protected String code_espece_botanique;
    protected String code_qualifiant_AEE;
    protected String code_type_saisonier;
    protected String code_destination_aee;
    protected String destinationId;
    
    // from RefVariete
    protected String varietyId;
    protected String varietyLibelle;
    protected String edaplosUnknownVariety;
    
    // il s’agit de cultures qui accompagnent la culture principale, elles ne sont pas récoltées et pas valorisées mais rentre dans les coûts de production.
    // Ce terme est utilisé sur les cultures assolées. Exemple, en grandes cultures, un mélange de maïs, trèfle et ray grass utilisé en inter rang.
    // culture couvert: Idem mais sur les cultures perenne. Exemple, en Vigne, un semis d'orge utilisé pour enherber les rangs entre les vignes.
    // Ces cultures peuvent être semées en même temps ou non que la culture principale.
    protected CompagneType compagne;
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CroppingPlanSpeciesDto other = (CroppingPlanSpeciesDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }
}
