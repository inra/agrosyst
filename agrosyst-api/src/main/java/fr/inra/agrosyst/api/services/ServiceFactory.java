package fr.inra.agrosyst.api.services;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Service factory interface used to get new service instance (local or remote).
 * 
 * @author Eric Chatellier
 */
public interface ServiceFactory {

    /**
     * Build new service instance depending on factory implementation.
     * 
     * @param clazz service interface
     * @return service implementation
     */
    <E extends AgrosystService> E newService(Class<E> clazz);
    
    <E extends AgrosystService, F extends AgrosystService> F newExpectedService(Class<E> clazz, Class<F> expectedClazz);
    
    /**
     * Create new {@code clazz} instance and inject necessary properties (Service, Config, DAO...)
     * 
     * @param clazz implementation class
     * @return clazz instance
     */
    <I> I newInstance(Class<I> clazz);

}
