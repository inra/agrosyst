package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Cycle phase dto corresponding to CropCyclePhaseImpl.
 * Defined just to not manipulate Impl in code.
 * 
 * @author Eric Chatellier
 */
@Getter
@Setter
public class PracticedCropCyclePhaseDto implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -7924215166844492360L;

    protected String topiaId;

    protected CropCyclePhaseType type;

    protected Integer duration;

    protected List<PracticedInterventionDto> interventions;

    public void addIntervention(PracticedInterventionDto interventionDto) {
        if (interventions == null) {
            interventions = new ArrayList<>();
        }
        interventions.add(interventionDto);
    }

}
