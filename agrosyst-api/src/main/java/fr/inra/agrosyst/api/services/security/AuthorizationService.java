package fr.inra.agrosyst.api.services.security;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.users.UserDto;

import java.io.InputStream;
import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public interface AuthorizationService extends AgrosystService {

    boolean isAdminFromUserId(String userId);

    boolean isIsDataProcessorFromUserId(String userId);

    void dropComputedPermissionsFromUserId(String userId);

    String checkComputedPermissionsFromUserId(String userId);

    String checkComputedPermissionsFromUserId(String userId, String doAsUserId);

    List<UserRoleDto> getUserRoles(String userId);

    List<UserRoleDto> getEntityRoles(RoleType roleType, String entityCode);

    List<UserRoleEntityDto> searchPossibleEntities(RoleType roleType, String termRaw);

    List<UserRoleEntityDto> searchEntities(RoleType roleType, String termRaw, Integer campaign);

    /**
     * Set roles to the user with id the userId given has parameter. Previous roles are removed.
     * @param userId
     * @param userRoles
     */
    void saveUserRoles(String userId, List<UserRoleDto> userRoles);

    void saveEntityUserRoles(RoleType roleType, String entityCode, List<UserRoleDto> roles);

    List<UserDto> getDomainResponsibles(String domainCode);

    List<UserDto> getGrowingPlanResponsibles(String growingPlanCode);

    /**
     * Import rules files
     * @param userFileStream
     * @return
     */
    ImportResult importRoles(InputStream userFileStream);

    /**
     * Add role to the user with id the given userId
     * @param userId user Id
     * @param userRoles roles to add
     */
    void addUserRoles(String userId, List<UserRoleDto> userRoles);

    long countPermissions();
    
    void copyUserRoles(String fromUserId, List<String> toUserIds);
    
    boolean isInMaintenanceMode();
    
    String getMaintenanceModeMessage();
    
    boolean mustDisconnetAllUsers();
    
    void setMaintenanceMode(boolean active, String message, boolean disconnectAllUsers);
    
    void reloadConfig();

    void periodicCheckForComputedPermissionsDeletion();
    
    void markAsDirtyForAllUsers();
}
