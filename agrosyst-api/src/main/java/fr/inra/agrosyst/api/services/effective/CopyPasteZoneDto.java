package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CropCyclePhaseType;

/**
 * Represent crops on zones
 * Created by davidcosse on 01/07/14.
 */
public class CopyPasteZoneDto {

    /**
     * crop part
     */
    protected String croppingPlanEntryId;

    protected String croppingPlanEntryName;

    /**
     * Phase part
     */
    protected String phaseId;

    protected CropCyclePhaseType phaseType;

    /**
     * Node part
     */
    protected String nodeId;

    protected Integer rank;

    protected long nbInterventions;

    protected long nbIntermediateCropInterventions;

    /**
     * Recipient zone part
     */
    protected String zoneId;

    protected String zoneName;

    protected double zoneArea;

    /**
     * Plot part
     */
    protected String plotName;

    protected double plotArea;

    /**
     *
     * Domain part
     */
    protected String domainName;

    protected Integer campaign;

    /**
     *
     * Growing System part
     */
    protected String growingSystemName;

    /**
     * Growing Plan Name
     */
    protected String growingPlanName;


    public String getPhaseId() {
        return phaseId;
    }

    public void setPhaseId(String phaseId) {
        this.phaseId = phaseId;
    }

    public CropCyclePhaseType getPhaseType() {
        return phaseType;
    }

    public void setPhaseType(CropCyclePhaseType phaseType) {
        this.phaseType = phaseType;
    }

    public String getCroppingPlanEntryId() {
        return croppingPlanEntryId;
    }

    public void setCroppingPlanEntryId(String croppingPlanEntryId) {
        this.croppingPlanEntryId = croppingPlanEntryId;
    }

    public String getCroppingPlanEntryName() {
        return croppingPlanEntryName;
    }

    public void setCroppingPlanEntryName(String croppingPlanEntryName) {
        this.croppingPlanEntryName = croppingPlanEntryName;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getPlotName() {
        return plotName;
    }

    public void setPlotName(String plotName) {
        this.plotName = plotName;
    }

    public double getZoneArea() {
        return zoneArea;
    }

    public void setZoneArea(double zoneArea) {
        this.zoneArea = zoneArea;
    }

    public double getPlotArea() {
        return plotArea;
    }

    public void setPlotArea(double plotArea) {
        this.plotArea = plotArea;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public Integer getCampaign() {
        return campaign;
    }

    public void setCampaign(Integer campaign) {
        this.campaign = campaign;
    }

    public String getGrowingSystemName() {
        return growingSystemName;
    }

    public void setGrowingSystemName(String growingSystemName) {
        this.growingSystemName = growingSystemName;
    }

    public String getGrowingPlanName() {
        return growingPlanName;
    }

    public void setGrowingPlanName(String growingPlanName) {
        this.growingPlanName = growingPlanName;
    }

    public long getNbInterventions() {
        return nbInterventions;
    }

    public void setNbInterventions(long nbInterventions) {
        this.nbInterventions = nbInterventions;
    }

    public long getNbIntermediateCropInterventions() {
        return nbIntermediateCropInterventions;
    }

    public void setNbIntermediateCropInterventions(long nbIntermediateCropInterventions) {
        this.nbIntermediateCropInterventions = nbIntermediateCropInterventions;
    }
}
