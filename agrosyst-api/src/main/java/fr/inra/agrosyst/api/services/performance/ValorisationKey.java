package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class ValorisationKey {
    
    protected final String JOIN = "_";
    protected final String IGNORE = "";
    protected final String codeEspeceBotanic;
    protected final String codeQualifiantAee;
    protected final String codeDestination;
    protected final boolean isOrganic;
    
    public ValorisationKey(String codeEspeceBotanic, String codeQualifiantAee, String codeDestination, boolean isOrganic) {
        this.codeEspeceBotanic = StringUtils.trimToEmpty(codeEspeceBotanic);
        this.codeQualifiantAee = StringUtils.trimToEmpty(codeQualifiantAee);
        this.codeDestination = StringUtils.trimToEmpty(codeDestination);
        this.isOrganic = isOrganic;
    }
    
    public String getKeyLevel0() {
        return codeEspeceBotanic + JOIN + codeQualifiantAee + JOIN + codeDestination + JOIN + isOrganic;
    }
    
    public String getKeyLevel1() {
        return codeEspeceBotanic + JOIN + IGNORE + JOIN + codeDestination + JOIN + isOrganic;
    }
    
    public String getKeyLevel2() {
        return IGNORE + JOIN + IGNORE + JOIN + codeDestination + JOIN + isOrganic;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValorisationKey that = (ValorisationKey) o;
        return isOrganic == that.isOrganic &&
                codeEspeceBotanic.equals(that.codeEspeceBotanic) &&
                codeQualifiantAee.equals(that.codeQualifiantAee) &&
                codeDestination.equals(that.codeDestination);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(codeEspeceBotanic, codeQualifiantAee, codeDestination, isOrganic);
    }
}
