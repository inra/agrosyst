package fr.inra.agrosyst.api.services;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;

/**
 * Agrosyst common filter base.
 *
 * Contains:
 * <ul>
 *   <li>navigation context (can be null)
 *   <li>requested page
 *   <li>requested result per page
 * </ul>
 *
 * @author Eric Chatellier
 */
public abstract class AgrosystFilter implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 2871244551416220532L;

    /** Constant to get all result. */
    public static final int ALL_PAGE_SIZE = -1;

    /** Default result count. */
    public static final int DEFAULT_PAGE_SIZE = 10;

    /** Optional navigation context. */
    protected NavigationContext navigationContext;

    /** Requested page index. */
    protected int page;

    /** Result per page. */
    protected int pageSize = DEFAULT_PAGE_SIZE;

    protected Collection<String> selectedIds;

    public NavigationContext getNavigationContext() {
        return navigationContext;
    }

    public void setNavigationContext(NavigationContext navigationContext) {
        this.navigationContext = navigationContext;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    
    public void setAllPageSize() {
        this.pageSize = ALL_PAGE_SIZE;
    }

    public Collection<String> getSelectedIds() {
        return selectedIds;
    }

    public void setSelectedIds(Collection<String> selectedIds) {
        this.selectedIds = selectedIds;
    }
}
