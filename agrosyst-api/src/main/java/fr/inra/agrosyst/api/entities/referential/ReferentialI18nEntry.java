package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;

public class ReferentialI18nEntry implements Serializable {
    public static final String PROPERTY_LANG = "lang";

    public static final String PROPERTY_TRADKEY = "tradkey";

    public static final String PROPERTY_TRADUCTION = "traduction";
    @Serial
    private static final long serialVersionUID = 8695428223494628595L;
    
    public static Collection<String> getAllProperties() {
        return Arrays.asList(PROPERTY_LANG, PROPERTY_TRADKEY, PROPERTY_TRADUCTION);
    }

    protected String lang;
    protected String tradkey;
    protected String traduction;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getKey() {
        return tradkey;
    }

    public void setKey(String key) {
        this.tradkey = key;
    }

    public String getTradkey() {
        return tradkey;
    }

    public void setTradkey(String tradkey) {
        this.tradkey = tradkey;
    }

    public String getTraduction() {
        return traduction;
    }

    public void setTraduction(String traduction) {
        this.traduction = traduction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TradRefVivant that = (TradRefVivant) o;
        return lang.equals(that.lang) &&
                tradkey.equals(that.tradkey) &&
                traduction.equals(that.traduction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lang, tradkey, traduction);
    }
}
