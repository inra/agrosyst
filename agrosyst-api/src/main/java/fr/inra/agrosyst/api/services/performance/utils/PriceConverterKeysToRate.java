package fr.inra.agrosyst.api.services.performance.utils;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class PriceConverterKeysToRate implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 1253868028010238287L;

    private final Map<String, PriceConverterKeyToRate> priceConverterKeysToRate = new HashMap<>();
    
    protected String getPriceConverterKey(String codeDestination, PriceUnit priceUnit) {
        return codeDestination + priceUnit;
    }
    
    public PriceConverterKeysToRate(List<RefHarvestingPriceConverter> converters) {
        for (RefHarvestingPriceConverter converter : converters) {
            String codeDestination = converter.getCode_destination_A();
            PriceUnit priceUnit = converter.getPriceUnit();
    
            PriceConverterKeyToRate priceConverterKeyToRate = new PriceConverterKeyToRate(codeDestination, priceUnit, converter);
            priceConverterKeysToRate.put(getPriceConverterKey(codeDestination, priceUnit), priceConverterKeyToRate);
        }
    }
    
    public Optional<Double> getPriceConverterRate(String codeDestination, PriceUnit priceUnit) {
        final PriceConverterKeyToRate priceConverterKeyToRate = priceConverterKeysToRate.get(getPriceConverterKey(codeDestination, priceUnit));
        Optional<Double> result = priceConverterKeyToRate != null ?  Optional.of(priceConverterKeyToRate.conversionRate) : Optional.empty();
        return result;
    }
    
    private static class PriceConverterKeyToRate implements Serializable{
        @Serial
        private static final long serialVersionUID = -3024599987143332874L;
    
        final String codeDestination;
        final PriceUnit priceUnit;
        final double conversionRate;
    
        public PriceConverterKeyToRate(String codeDestination, PriceUnit priceUnit, RefHarvestingPriceConverter converter) {
            this.codeDestination = codeDestination;
            this.priceUnit = priceUnit;
            this.conversionRate = converter.getConvertionRate();
        }
    }
    
    
}
