package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author cosse
 */
@Getter
@Setter
public class ToolsCouplingDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -6341638291682562472L;

    protected String topiaId;

    protected String code;

    protected String toolsCouplingName;

    protected String tractorId;

    protected String tractorKey;

    protected HashSet<String> equipementsIds;

    protected HashSet<String> equipementsKeys;

    protected Double flow;

    protected Double workforce;

    protected String comment;

    protected Double workRate;

    protected MaterielWorkRateUnit workRateUnit;

    protected MaterielTransportUnit materielTransportUnit;

    protected boolean manualIntervention;

    protected Double transitVolume;

    protected Collection<RefInterventionAgrosystTravailEDI> mainsActions;

    protected Double boiledQuantity;

    protected boolean antiDriftNozzle;
    
}
