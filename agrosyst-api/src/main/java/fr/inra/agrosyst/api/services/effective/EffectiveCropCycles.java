package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.FormatUtils;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleSpecies;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.Equipments;
import fr.inra.agrosyst.api.services.itk.Itk;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.i18n.I18n;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EffectiveCropCycles {

    public static final Function<EffectiveCropCycleConnection, EffectiveCropCycleConnectionDto> CROP_CYCLE_CONNECTION_TO_DTO = input -> {
        EffectiveCropCycleConnectionDto result = new EffectiveCropCycleConnectionDto();
        // can be null for connection to BEFORE NODE
        if (input.getSource() != null) {
            result.setSourceId(Entities.ESCAPE_TOPIA_ID.apply(input.getSource().getTopiaId()));
        }
        result.setTargetId(Entities.ESCAPE_TOPIA_ID.apply(input.getTarget().getTopiaId()));
        String label = "";
        String intermediateCroppingPlanEntryName = "";
        if (input.getIntermediateCroppingPlanEntry() != null) {
            result.setIntermediateCroppingPlanEntryId(input.getIntermediateCroppingPlanEntry().getTopiaId());
            label += "<b>CI</b><span class='hover-infos'>" + input.getIntermediateCroppingPlanEntry().getName() + "</span>";
            intermediateCroppingPlanEntryName = input.getIntermediateCroppingPlanEntry().getName();
        }
        result.setLabel(label);
        result.setIntermediateCroppingPlanEntryName(intermediateCroppingPlanEntryName);
        result.setEdaplosIssuerId(input.getEdaplosIssuerId());
        return result;
    };

    public static final Function<EffectiveCropCycleNode, EffectiveCropCycleNodeDto> CROP_CYCLE_NODE_TO_DTO = input -> {
        EffectiveCropCycleNodeDto result = new EffectiveCropCycleNodeDto();
        result.setNodeId(Entities.ESCAPE_TOPIA_ID.apply(input.getTopiaId()));
        result.setX(input.getRank());
        result.setEdaplosIssuerId(input.getEdaplosIssuerId());
        if (input.getCroppingPlanEntry() != null) {
            result.setCroppingPlanEntryId(input.getCroppingPlanEntry().getTopiaId());
            result.setLabel(input.getCroppingPlanEntry().getName());
        }
        return result;
    };

    public static final Function<EffectivePerennialCropCycle, EffectivePerennialCropCycleDto> PERENNIAL_CROP_CYCLE_TO_DTO = input -> {
        EffectivePerennialCropCycleDto result = new EffectivePerennialCropCycleDto();
        result.setTopiaId(input.getTopiaId());
        result.setPlantingYear(input.getPlantingYear());
        result.setPlantingDensity(input.getPlantingDensity());
        result.setPlantingInterFurrow(input.getPlantingInterFurrow());
        result.setPlantingSpacing(input.getPlantingSpacing());
        result.setPlantingDeathRate(input.getPlantingDeathRate());
        result.setPlantingDeathRateMeasureYear(input.getPlantingDeathRateMeasureYear());
        result.setPollinator(input.isPollinator());
        result.setPollinatorPercent(input.getPollinatorPercent());
        result.setPollinatorSpreadMode(input.getPollinatorSpreadMode());
        result.setWeedType(input.getWeedType());
        result.setVineFrutalForm(input.getVineFrutalForm());
        result.setOrchardFrutalForm(input.getOrchardFrutalForm());
        result.setOtherCharacteristics(input.getOtherCharacteristics());
        result.setCroppingPlanEntryId(input.getCroppingPlanEntry().getTopiaId());
        result.setCroppingPlanEntryName(input.getCroppingPlanEntry().getName());
        result.setFoliageHeight(input.getFoliageHeight());
        result.setFoliageThickness(input.getFoliageThickness());
        if (input.getOrientation() != null) {
            result.setOrientationId(input.getOrientation().getTopiaId());
            result.setOrientationLabel(input.getOrientation().getReference_label());
        }
        return result;
    };

    public static final Function<EffectiveSeasonalCropCycle, EffectiveSeasonalCropCycleDto> SEASONNAL_CROP_CYCLE_TO_DTO = input -> {
        EffectiveSeasonalCropCycleDto result = new EffectiveSeasonalCropCycleDto();
        result.setTopiaId(input.getTopiaId());
        return result;
    };

    public static final Function<EffectiveCropCyclePhase, EffectiveCropCyclePhaseDto> CROP_CYCLE_PHASE_TO_DTO = input -> {
        EffectiveCropCyclePhaseDto result = new EffectiveCropCyclePhaseDto();
        result.setTopiaId(input.getTopiaId());
        result.setDuration(input.getDuration());
        result.setType(input.getType());
        result.setEdaplosIssuerId(input.getEdaplosIssuerId());
        return result;
    };

    public static EffectiveInterventionDto getDtoForEffectiveIntervention(
            EffectiveIntervention input,
            ReferentialTranslationMap translationMap,
            Domain domain) {
        EffectiveInterventionDto result = new EffectiveInterventionDto(domain.getTopiaId());
        result.setTopiaId(input.getTopiaId());
        result.setName(input.getName());
        result.setComment(input.getComment());
        result.setStartInterventionDate(input.getStartInterventionDate());
        result.setEndInterventionDate(input.getEndInterventionDate());
        result.setIntermediateCrop(input.isIntermediateCrop());
        result.setSpatialFrequency(input.getSpatialFrequency());
        result.setOtherToolSettings(input.getOtherToolSettings());
        result.setWorkRate(input.getWorkRate());
        result.setProgressionSpeed(input.getProgressionSpeed());
        result.setInvolvedPeopleCount(input.getInvolvedPeopleCount());
        result.setTransitCount(input.getTransitCount());
        result.setType(input.getType());
        result.setEdaplosIssuerId(input.getEdaplosIssuerId());
        result.setWorkRateUnit(input.getWorkRateUnit());
        result.setTransitVolume(input.getTransitVolume());
        result.setTransitVolumeUnit(input.getTransitVolumeUnit());
        result.setNbBalls(input.getNbBalls());

        List<SpeciesStadeDto> stadeDtos = null;
        if (input.getSpeciesStades() != null) {
            stadeDtos = input.getSpeciesStades().stream()
                    .map(s -> getDtoForEffectiveSpeciesStade(s, translationMap))
                    .collect(Collectors.toList());
        }
        result.setSpeciesStadesDtos(stadeDtos);

        Set<String> toolCouplingCodes = null;
        if (input.getToolCouplings() != null) {
            toolCouplingCodes = input.getToolCouplings()
                    .stream()
                    .map(Equipments.GET_TOOLS_COUPLING_CODE)
                    .collect(Collectors.toCollection(Sets::newLinkedHashSet));
        }
        result.setToolsCouplingCodes(toolCouplingCodes);

        return result;
    }

    public static SpeciesStadeDto getDtoForEffectiveSpeciesStade(EffectiveSpeciesStade input, ReferentialTranslationMap translationMap) {
        SpeciesStadeDto result = new SpeciesStadeDto();
        result.setTopiaId(input.getTopiaId());

        if (input.getCroppingPlanSpecies() != null) {
            CroppingPlanSpecies cps = input.getCroppingPlanSpecies();
            result.setSpeciesCode(cps.getCode());

            RefEspece refEspece = cps.getSpecies();
            ReferentialEntityTranslation refEspeceTranslation = translationMap.getEntityTranslation(cps.getTopiaId());
            String libelleEspeceBotanique = refEspeceTranslation.getPropertyTranslation(
                    RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, refEspece.getLibelle_espece_botanique());

            result.setSpeciesName(libelleEspeceBotanique);

            String varietyLabel;
            RefVariete variety = cps.getVariety();
            if (variety != null) {
                varietyLabel = variety.getLabel();
            } else {
                if (StringUtils.isNotBlank(cps.getEdaplosUnknownVariety())) {
                    varietyLabel = I18n.l(translationMap.getLocale(), "fr.inra.agrosyst.api.services.edaplos.unknownVariety", cps.getEdaplosUnknownVariety());
                } else {
                    varietyLabel = "";
                }
            }
            result.setVarietyName(varietyLabel);
            String speciesVarietyName = FormatUtils.getSpeciesVarietyName(libelleEspeceBotanique, Collections.singleton(varietyLabel), "");
            result.setSpeciesAndVarietyName(speciesVarietyName);

            CroppingPlanEntry croppingPlanEntry = cps.getCroppingPlanEntry();
            CroppingPlans.translateCroppingPlanEntry(croppingPlanEntry, translationMap);
            String speciesAndVarietiesNames = FormatUtils.getCroppingPlanEntrySpecies(croppingPlanEntry);
            result.setMixSpeciesName(speciesAndVarietiesNames);
        }
        result.setStadeMin(Itk.getDtoForRefStadeEdi(input.getMinStade(), translationMap));
        result.setStadeMax(Itk.getDtoForRefStadeEdi(input.getMaxStade(), translationMap));
        return result;
    }

    public static final Function<CroppingPlanEntry, EffectiveCropCycleModelDto> CROPPING_PLAN_ENTRY_TO_DTO = input -> {
        boolean isCompagne = CollectionUtils.emptyIfNull(input.getCroppingPlanSpecies()).stream().map(CroppingPlanSpecies::getCompagne).anyMatch(Objects::nonNull);
        EffectiveCropCycleModelDto result = new EffectiveCropCycleModelDto();
        result.setCroppingPlanEntryId(input.getTopiaId());
        result.setLabel(input.getName());
        result.setIntermediate(input.getType() == CroppingEntryType.INTERMEDIATE);
        result.setMixSpecies(input.isMixSpecies());
        result.setMixVariety(input.isMixVariety());
        result.setMixCompanion(isCompagne);
        result.setCatchCrop(input.getType() == CroppingEntryType.CATCH);
        result.setType(input.getType());
        return result;
    };

    public static final Function<EffectiveCropCycleSpecies, String> GET_CROP_CYCLE_PERENNIAL_SPECIES_CODE = input -> input.getCroppingPlanSpecies().getCode();

    public static void fillInterventionEspeceTranslations(
            EffectiveIntervention effectiveIntervention,
            ReferentialTranslationMap translationMap,
            BiConsumer<Collection<String>, ReferentialTranslationMap> translationFiller) {
        Set<String> refEspeceIds = effectiveIntervention.getSpeciesStades().stream()
                .map(EffectiveSpeciesStade::getCroppingPlanSpecies)
                .filter(Objects::nonNull)
                .map(s -> s.getSpecies().getTopiaId())
                .collect(Collectors.toSet());
        translationFiller.accept(refEspeceIds, translationMap);
    }

    public static void fillInterventionStadeTranslationMap(
            EffectiveIntervention effectiveIntervention,
            ReferentialTranslationMap translationMap,
            BiConsumer<Collection<String>, ReferentialTranslationMap> translationFiller) {
        Set<String> topiaIds = new HashSet<>();
        effectiveIntervention.getSpeciesStades().forEach(stade -> {
            if (stade.getMinStade() != null) {
                topiaIds.add(stade.getMinStade().getTopiaId());
            }
            if (stade.getMaxStade() != null) {
                topiaIds.add(stade.getMaxStade().getTopiaId());
            }
        });
        translationFiller.accept(topiaIds, translationMap);
    }

}
