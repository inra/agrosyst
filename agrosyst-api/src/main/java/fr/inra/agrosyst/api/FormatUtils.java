package fr.inra.agrosyst.api;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.i18n.I18n;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isNumeric;

public class FormatUtils {

    public static String getSpeciesName(Collection<CroppingPlanSpecies> croppingPlanSpecies, String separator) {
        String speciesNames = "";
        if (CollectionUtils.isNotEmpty(croppingPlanSpecies)) {
            speciesNames = croppingPlanSpecies.stream()
                    .map(CroppingPlanSpecies::getSpecies)
                    .map(RefEspece::getLibelle_espece_botanique_Translated)
                    .distinct()
                    .sorted()
                    .collect(Collectors.joining(separator));
        }
        return speciesNames;
    }

    public static String getVarietyName(Collection<CroppingPlanSpecies> croppingPlanSpecies, String separator) {
        String varietyName = "";
        if (CollectionUtils.isNotEmpty(croppingPlanSpecies)) {
            varietyName = croppingPlanSpecies.stream()
                    .map(species -> {
                        String varietyLabel = null;
                        if (species.getVariety() != null) {
                            varietyLabel = species.getVariety().getLabel();
                        }
                        if (StringUtils.isBlank(varietyLabel) && StringUtils.isNotBlank(species.getEdaplosUnknownVariety())) {
                            varietyLabel = I18n.t("fr.inra.agrosyst.api.services.edaplos.unknownVariety", species.getEdaplosUnknownVariety());
                        }
                        return varietyLabel;
                    })
                    .filter(Objects::nonNull)
                    .distinct()
                    .sorted()
                    .collect(Collectors.joining(separator));
        }
        return varietyName;
    }

    public static String getSpeciesVarietyName(RefEspece espece, Collection<String> varietyLabels, String separator) {
        String speciesName = espece != null ? espece.getLibelle_espece_botanique_Translated() : "";
        return getSpeciesVarietyName(speciesName, varietyLabels, separator);
    }

    public static String getSpeciesVarietyName(String libelleEspeceBotanique, Collection<String> varietyLabels, String separator) {
        String speciesName = libelleEspeceBotanique;
        String varietiesLabels = varietyLabels.stream()
                .filter(Objects::nonNull)
                .distinct()
                .sorted()
                .collect(Collectors.joining(separator));
        if (StringUtils.isNotBlank(varietiesLabels)) {
            speciesName += " (" + varietiesLabels + ")";
        }
        return speciesName;
    }

    public static String getInterventionSpeciesName(CroppingPlanEntry croppingPlanEntry,
                                                    Collection<String> interventionSpeciesCodes,
                                                    String separator) {
        String speciesNames = "";
        if (croppingPlanEntry != null && CollectionUtils.isNotEmpty(croppingPlanEntry.getCroppingPlanSpecies())) {
            Set<CroppingPlanSpecies> interventionSpecies = croppingPlanEntry.getCroppingPlanSpecies().stream()
                    .filter(croppingPlanSpecies -> interventionSpeciesCodes.contains(croppingPlanSpecies.getCode()))
                    .collect(Collectors.toSet());
            speciesNames = getSpeciesName(interventionSpecies, separator);
        }
        return speciesNames;
    }

    public static String getInterventionVarietiesName(CroppingPlanEntry croppingPlanEntry,
                                                      Collection<String> interventionSpeciesCodes,
                                                      String separator) {
        String varietyName = "";
        if (croppingPlanEntry != null && CollectionUtils.isNotEmpty(croppingPlanEntry.getCroppingPlanSpecies())) {
            Set<CroppingPlanSpecies> interventionSpecies = croppingPlanEntry.getCroppingPlanSpecies().stream()
                    .filter(croppingPlanSpecies -> interventionSpeciesCodes.contains(croppingPlanSpecies.getCode()))
                    .collect(Collectors.toSet());
            varietyName = getVarietyName(interventionSpecies, separator);
        }
        return varietyName;
    }

    public static String getCroppingPlanEntrySpecies(CroppingPlanEntry croppingPlanEntry) {
        MultiValuedMap<RefEspece, String> varietiesBySpecies = new HashSetValuedHashMap<>();
        for (CroppingPlanSpecies croppingPlanSpecies : CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies())) {
            RefEspece species = croppingPlanSpecies.getSpecies();
            RefVariete variety = croppingPlanSpecies.getVariety();
            if (variety != null) {
                varietiesBySpecies.put(species, variety.getLabel());
            } else if (StringUtils.isNotBlank(croppingPlanSpecies.getEdaplosUnknownVariety())) {
                varietiesBySpecies.put(species, croppingPlanSpecies.getEdaplosUnknownVariety());
            }
        }
        return varietiesBySpecies.keySet().stream()
                .map(espece -> FormatUtils.getSpeciesVarietyName(espece, varietiesBySpecies.get(espece), ";"))
                .distinct()
                .sorted()
                .collect(Collectors.joining(" / "));
    }

    public static Pair<Integer, String> parseIdTraitementIdProduit(String objectId) {
        var ids = objectId.split("_");
        var idTraitement = isNumeric(ids[ids.length - 1]) ? Integer.parseInt(ids[ids.length - 1]) : 0;
        var idProduit = String.join("_", Arrays.copyOf(ids, ids.length - 1));
        return Pair.of(idTraitement, idProduit);
    }
}
