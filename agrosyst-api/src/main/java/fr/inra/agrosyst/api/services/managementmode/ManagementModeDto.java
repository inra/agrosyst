package fr.inra.agrosyst.api.services.managementmode;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by davidcosse on 24/01/14.
 */
public class ManagementModeDto {

    /**
     * Topia id of planned Management Mode
     */
    protected String plannedManagementModeId;

    /**
     * TopiaId of Observed Management Mode
     */
    protected String observedManagementModeId;

    /**
     * growing System common for both ManagmentModes
     */
    protected GrowingSystemDto growingSystem;

    public String getPlannedManagementModeId() {
        return plannedManagementModeId;
    }

    public void setPlannedManagementModeId(String plannedManagementModeId) {
        this.plannedManagementModeId = plannedManagementModeId;
    }

    public String getObservedManagementModeId() {
        return observedManagementModeId;
    }

    public void setObservedManagementModeId(String observedManagementModeId) {
        this.observedManagementModeId = observedManagementModeId;
    }

    public GrowingSystemDto getGrowingSystem() {
        return growingSystem;
    }

    public void setGrowingSystem(GrowingSystemDto growingSystem) {
        this.growingSystem = growingSystem;
    }

    public String getDtoId() {
        String dtoId;
        if (StringUtils.isNotBlank(plannedManagementModeId) && StringUtils.isNotBlank(observedManagementModeId)) {
            dtoId = plannedManagementModeId + observedManagementModeId;
        } else if (StringUtils.isNotBlank(plannedManagementModeId)){
            dtoId = plannedManagementModeId;
        } else if (StringUtils.isNotBlank(observedManagementModeId)){
            dtoId = observedManagementModeId;
        } else {
            dtoId = null;
        }
        return dtoId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        String dtoId = getDtoId();
        result = prime * result + ((dtoId == null) ? 0 : dtoId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        String dtoId = getDtoId();
        ManagementModeDto other = (ManagementModeDto) obj;
        if (dtoId == null) {
            return other.getDtoId() == null;
        } else return dtoId.equals(other.getDtoId());
    }
}
