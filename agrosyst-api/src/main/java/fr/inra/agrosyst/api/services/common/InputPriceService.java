package fr.inra.agrosyst.api.services.common;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainFuelInput;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainManualWorkforceInput;
import fr.inra.agrosyst.api.entities.DomainMechanizedWorkforceInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public interface InputPriceService extends AgrosystService {
    
    Function<String, Set<FertiMinElement>> OBJECT_ID_TO_FERTI_MIN_ELEMENTS = objectId -> {
        // categ=109 forme=Granule n=16,000000 p2O5=null k2O=null bore=null calcium=null fer=null manganese=null molybdene=null mgO=6,000000 oxyde_de_sodium=10,000000 sO3=null cuivre=null zinc=null
        Set<FertiMinElement> valuatedElements = new HashSet<>();
        
        String[] elements = objectId.split(" ");
        for (int elemRank = 2; elemRank < elements.length; elemRank++) {
            String elemToValue = elements[elemRank];
            String[] elemValue = elemToValue.split("=");
            String value = elemValue[1];
            if (!value.contentEquals("null") && !value.contentEquals("0.000000")){
                FertiMinElement elem = ReferentialService.FERTI_MIN_ELEMENT_NAME_TO_ENUM.apply(elemValue[0]);
                valuatedElements.add(elem);
            }
        }
        
        return valuatedElements;
    };
    
    Function<String, Map<FertiMinElement, Double>> OBJECT_ID_TO_FERTI_MIN_ELEMENTS_QUANTITY = objectId -> {
        Map<FertiMinElement, Double> multFactorForElement = new HashMap<>();
        // categ=109 forme=Granule n=16,000000 p2O5=null k2O=null bore=null calcium=null fer=null manganese=null molybdene=null mgO=6,000000 oxyde_de_sodium=10,000000 sO3=null cuivre=null zinc=null
        String[] elementRes = objectId.replace(',', '.').split(" ");
        for (int i=2; i < elementRes.length; i++) {
            String elementName =  elementRes[i].split("=")[0];
            String elementQuantityStr = elementRes[i].split("=")[1];
            FertiMinElement fertiMinElement = ReferentialService.FERTI_MIN_ELEMENT_NAME_TO_ENUM.apply(elementName);
            Double elementQuantity = elementQuantityStr.contentEquals("null")  ? 0.0 : Double.parseDouble(elementQuantityStr);
            multFactorForElement.put(fertiMinElement, elementQuantity);
        }
        return multFactorForElement;
    };
    
    Function<RefFertiMinUNIFA, String> GET_REF_FERTI_MIN_UNIFA_OBJECT_ID = input -> {
        String objectId = "categ=%d forme=%s n=%f p2O5=%f k2O=%f bore=%f calcium=%f fer=%f manganese=%f molybdene=%f mgO=%f oxyde_de_sodium=%f sO3=%f cuivre=%f zinc=%f";
        objectId = String.format(objectId, input.getCateg(),
                input.getForme(),
                input.getN(),
                input.getP2O5(),
                input.getK2O(),
                input.getBore(),
                input.getCalcium(),
                input.getFer(),
                input.getManganese(),
                input.getMolybdene(),
                input.getMgO(),
                input.getOxyde_de_sodium(),
                input.getsO3(),
                input.getCuivre(),
                input.getZinc());
        objectId = objectId.replace(',', '.');
        objectId = StringUtils.stripAccents(objectId);
        return objectId;
    };
    
    Function<DomainMineralProductInputDto, String> GET_REF_FERTI_MIN_UNIFA_DTO_OBJECT_ID = input -> {
        String objectId = "categ=%d forme=%s n=%f p2O5=%f k2O=%f bore=%f calcium=%f fer=%f manganese=%f molybdene=%f mgO=%f oxyde_de_sodium=%f sO3=%f cuivre=%f zinc=%f";
        objectId = String.format(objectId, input.getCateg(),
                input.getForme(),
                input.getN(),
                input.getP2o5(),
                input.getK2o(),
                input.getBore(),
                input.getCalcium(),
                input.getFer(),
                input.getManganese(),
                input.getMolybdene(),
                input.getMgo(),
                input.getOxyde_de_sodium(),
                input.getSo3(),
                input.getCuivre(),
                input.getZinc());
        objectId = objectId.replace(',', '.');
        objectId = StringUtils.stripAccents(objectId);
        return objectId;
    };
    
    Function<RefSubstrate, String> GET_REF_SUBSTRAT_OBJECT_ID = substrate -> {
        String result = StringUtils.stripAccents(substrate.getCaracteristic1() + " - " + substrate.getCaracteristic2());
        return result;
    };
    
    Function<RefPot, String> GET_REF_POT_OBJECT_ID = pot -> {
        String result = StringUtils.stripAccents(pot.getCaracteristic1());
        return result;
    };
    
    Function<RefActaTraitementsProduit, String> GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID = input -> String.format(
            "%s_%d",
            input.getId_produit(),
            input.getId_traitement()
    );
    
    Function<RefFertiOrga, String> GET_REF_FERTI_ORGA_OBJECT_ID = RefFertiOrga::getIdtypeeffluent;
    
    Function<RefOtherInput, String> GET_REF_OTHER_INPUT_OBJECT_ID = refOtherInput -> String.format(
            "%s - %s - %s - %s",
            refOtherInput.getInputType_c0(),
            Strings.emptyToNull(refOtherInput.getCaracteristic1()),
            Strings.emptyToNull(refOtherInput.getCaracteristic2()),
            Strings.emptyToNull(refOtherInput.getCaracteristic3())
    );
    
    BiFunction<CroppingPlanEntry, Boolean, String> GET_CROP_SEED_OBJECT_ID = (cropSeed, organic) -> String.format(
            "%s - %b",
            cropSeed.getTopiaId(),
            organic
    );

    BiFunction<RefEspece, RefVariete, String> GET_SPECIES_SEED_OBJECT_ID = (cropSeed, cropVarety) -> String.format(
            "%s - %s - %s - %s",
            cropSeed.getCode_espece_botanique(),
            cropSeed.getCode_qualifiant_AEE(),
            cropSeed.getCode_destination_AEE(),
            cropVarety != null ? cropVarety.getLabel() : "none"
    );

    BiFunction<CroppingPlanSpecies, InputPriceDto, String> GET_SPECIES_DTO_SEED_OBJECT_ID = (cropSeed, priceDto) -> {
        final RefEspece species = cropSeed.getSpecies();
        final RefVariete variety = cropSeed.getVariety();
        return String.format(
            "%s - %s - %s - %s - %b - %b - %b - %s - %b",
                species.getCode_espece_botanique(),
                species.getCode_qualifiant_AEE(),
                species.getCode_destination_AEE(),
                variety != null ? variety.getLabel() : "",
                priceDto.isChemicalTreatment(),
                priceDto.isBiologicalSeedInoculation(),
                priceDto.isIncludedTreatment(),
                priceDto.getSeedType(),
                priceDto.isOrganic());
    };

    Function<Integer, String> GET_IRRIGATION_OBJECT_ID = campaign -> String.format(
            "%d",
            campaign
    );

    Function<Integer, String> GET_FUEL_OBJECT_ID = campaign -> String.format(
            "%d",
            campaign
    );

    Function<Integer, String> GET_MANUAL_WORKFORCE_OBJECT_ID = campaign -> String.format(
            "%d",
            campaign
    );

    Function<Integer, String> GET_MECHANIZED_WORKFORCE_OBJECT_ID = campaign -> String.format(
            "%d",
            campaign
    );

    Function<AbstractDomainInputStockUnit, String> GET_INPUT_OBJECT_ID = input -> {
        String result = null;
        if (input instanceof DomainMineralProductInput inputImpl) {
            RefFertiMinUNIFA product = inputImpl.getRefInput();
            result = GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(product);
        } else if (input instanceof DomainOrganicProductInput inputImpl) {
            RefFertiOrga organicProduct = inputImpl.getRefInput();
            result = GET_REF_FERTI_ORGA_OBJECT_ID.apply(organicProduct);
        } else if (input instanceof DomainPhytoProductInput inputImpl) {
            RefActaTraitementsProduit phytoProduct = inputImpl.getRefInput();
            result = GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(phytoProduct);
        } else if (input instanceof DomainOtherInput inputImpl) {
            result = GET_REF_OTHER_INPUT_OBJECT_ID.apply(inputImpl.getRefInput());
        } else if (input instanceof DomainSubstrateInput inputImpl) {
            final RefSubstrate substrate = inputImpl.getRefInput();
            result = GET_REF_SUBSTRAT_OBJECT_ID.apply(substrate);
        } else if (input instanceof DomainPotInput inputImpl) {
            final RefPot refPot = inputImpl.getRefInput();
            result = GET_REF_POT_OBJECT_ID.apply(refPot);
        } else if (input instanceof DomainSeedLotInput domainSeedLotInput) {
            result = GET_CROP_SEED_OBJECT_ID.apply(domainSeedLotInput.getCropSeed(), domainSeedLotInput.isOrganic());
        } else if (input instanceof DomainSeedSpeciesInput domainSeedSpeciesInput) {
            final RefEspece species = domainSeedSpeciesInput.getSpeciesSeed().getSpecies();
            RefVariete variety = domainSeedSpeciesInput.getSpeciesSeed().getVariety();
            result = GET_SPECIES_SEED_OBJECT_ID.apply(species, variety);
        } else if (input instanceof DomainIrrigationInput) {
            final int campaign = input.getDomain().getCampaign();
            result = GET_IRRIGATION_OBJECT_ID.apply(campaign);
        } else if (input instanceof DomainFuelInput) {
            final int campaign = input.getDomain().getCampaign();
            result = GET_FUEL_OBJECT_ID.apply(campaign);
        }else if (input instanceof DomainManualWorkforceInput) {
            final int campaign = input.getDomain().getCampaign();
            result = GET_MANUAL_WORKFORCE_OBJECT_ID.apply(campaign);
        }else if (input instanceof DomainMechanizedWorkforceInput) {
            final int campaign = input.getDomain().getCampaign();
            result = GET_MECHANIZED_WORKFORCE_OBJECT_ID.apply(campaign);
        }
        return result;
    };

    Function<InputType, InputPriceCategory> INPUT_TYPE_TO_PRICE_TYPE = inputType -> {
        InputPriceCategory result = null;
        if (InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.equals(inputType)) {
            result = InputPriceCategory.MINERAL_INPUT;
        } else if (InputType.EPANDAGES_ORGANIQUES.equals(inputType)) {
            result = InputPriceCategory.ORGANIQUES_INPUT;
        } else if (InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES.equals(inputType)) {
            result = InputPriceCategory.PHYTO_TRAITMENT_INPUT;
        } else if (InputType.AUTRE.equals(inputType)) {
            result = InputPriceCategory.OTHER_INPUT;
        } else if (InputType.SUBSTRAT.equals(inputType)) {
            result = InputPriceCategory.SUBSTRATE_INPUT;
        } else if (InputType.POT.equals(inputType)) {
            result = InputPriceCategory.POT_INPUT;
        } else if (InputType.SEMIS.equals(inputType) || InputType.PLAN_COMPAGNE.equals(inputType)) {
            result = InputPriceCategory.SEEDING_INPUT;
        } else if (InputType.TRAITEMENT_SEMENCE.equals(inputType)) {
            result = InputPriceCategory.PHYTO_TRAITMENT_INPUT;
        } else if (InputType.IRRIGATION.equals(inputType)) {
            result = InputPriceCategory.IRRIGATION_INPUT;
        } else if (InputType.CARBURANT.equals(inputType)) {
            result = InputPriceCategory.FUEL_INPUT;
        } else if (InputType.LUTTE_BIOLOGIQUE.equals(inputType)) {
            result = InputPriceCategory.BIOLOGICAL_CONTROL_INPUT;
        } else if (InputType.MAIN_OEUVRE_MANUELLE.equals(inputType)) {
            result = InputPriceCategory.MAIN_OEUVRE_MANUELLE_INPUT;
        } else if (InputType.MAIN_OEUVRE_TRACTORISTE.equals(inputType)) {
            result = InputPriceCategory.MAIN_OEUVRE_TRACTORISTE_INPUT;
        }
        return result;
    };

    String getRefFertiMinUnifaDisplayName(Language language, RefFertiMinUNIFA input);

    Function<RefFertiOrga, String> GET_REF_FERTI_ORGA_DISPLAY_NAME = input -> {
        String displayName = input.getLibelle();
        return displayName;
    };

    Function<RefActaTraitementsProduit, String> GET_REF_ACTA_TRAITEMENTS_PRODUIT_DISPLAY_NAME = input -> {
        String displayName = input.getNom_produit();
        return displayName;
    };
    
    void deleteHarvestingPrices(Set<HarvestingPrice> harvestingPricesToRemoves);

    void createOrUpdateHarvestingPrices(List<HarvestingPrice> harvestingPricesToCreateOrUpdate);
    
    List<HarvestingPrice> getPracticedSystemPrices(PracticedSystem practicedSystem);
    
    /**
     * ProductPriceSummary:
     *  - other user lowerPrice,
     *  - other user higherPrice,
     *  - other user medianPrice,
     *  - other user averagePrice,
     *  - other user countedPrices,
     *  - refPrice by Unit
     * /*\ does not load harvesting prices indications
     *
     * @param filter        pojo base on price and domain input that contain requirement attribut to load refInputPrice and user prices
     * @param excludeDomain the domain topiaId requesting for price indications (to not load it's own prices)
     * @return              the price's ProductPriceSummary
     *
     */
    ProductPriceSummary loadPriceIndications(InputPriceFilter filter, String excludeDomain);
    
    void deleteInputPrice(InputPrice price);
    
    /**
     * Persist domain input price, including seeding and harvesting price
     * @param inputPrice the inputPrice to persiste, (null safe)
     * @return the persisted price or null
     */
    InputPrice persistInputPrice(InputPrice inputPrice);
    
    /**
     * Get InputPrice, SeedPrice or HarvestingPrice instance according to inputPriceCategory
     *
     * @param inputPriceCategory InputPriceCategory
     * @return new InputPrice instance not persisted yet
     */
    InputPrice getNewPriceInstance(InputPriceCategory inputPriceCategory);
    
    /**
     * Bind priceDto
     *
     * @param domain the domain's price belong to
     * @param inputPriceDto the price DTO
     * @param price the entity price to bind to
     * @param priceObjectId the price ObjectId
     * @param harvestingPracticedSystem For harvesting price, the practicedSystem associated to.
     * @param harvestingZone For harvesting price, the zone associated to.
     * @return the price not updated yet
     */
    InputPrice bindDtoToPrice(
            Domain domain,
            InputPriceDto inputPriceDto,
            InputPrice price,
            String priceObjectId,
            Optional<PracticedSystem> harvestingPracticedSystem,
            Optional<Zone> harvestingZone);

    /**
     * @param domain the domain's price belong to
     * @param price the entity price to bind to
     * @param priceObjectId the price ObjectId
     * @param harvestingPracticedSystem For harvesting price, the practicedSystem associated to.
     * @param harvestingZone For harvesting price, the zone associated to.
     * @return a new price not persisted yet
     */
    InputPrice bindToNewPrice(
            Domain domain,
            InputPrice price,
            String priceObjectId,
            Optional<PracticedSystem> harvestingPracticedSystem,
            Optional<Zone> harvestingZone);
}
