package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class ImportResult implements Serializable {

    @Serial
    private static final long serialVersionUID = -6928906011743267261L;

    protected int created;
    protected int updated;
    protected int deleted;
    protected int unactivated;
    protected int ignored;
    protected long duration;
    protected List<String> errors = new ArrayList<>();
    protected final List<String> warnings = new ArrayList<>();

    public void incCreated() {
        created++;
    }

    public void incUpdated() {
        updated++;
    }

    public void incDeleted() {
        deleted++;
    }

    public void incUnactivated() {
        unactivated++;
    }

    public int getUnactivated() {
        return unactivated;
    }

    public void setUnactivated(int unactivated) {
        this.unactivated = unactivated;
    }

    public void incIgnored() {
        ignored++;
    }

    public int getCreated() {
        return created;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public int getUpdated() {
        return updated;
    }

    public void setUpdated(int updated) {
        this.updated = updated;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getIgnored() {
        return ignored;
    }

    public void setIgnored(int ignored) {
        this.ignored = ignored;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error) {
        errors.add(error);
    }
    
    public void addWarning(String warning) {
        warnings.add(warning);
    }
    
    public List<String> getWarnings() {
        return warnings;
    }
    
    
    public boolean hasWarning() {
        return !warnings.isEmpty();
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void resetCounterForError() {
        created = 0;
        updated = 0;
        deleted = 0;
        unactivated = 0;
        ignored = 0;
    }

    @Override
    public String toString() {
        String result = MoreObjects.toStringHelper(this)
                .add("created", created)
                .add("updated", updated)
                .add("deleted", deleted)
                .add("unactivated", unactivated)
                .add("ignored", ignored)
                .add("duration", duration + "ms")
                .add("errors", errors.size())
                .add("warnings", warnings.size())
                .toString();
        return result;
    }

}
