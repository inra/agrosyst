package fr.inra.agrosyst.api.services.managementmode;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.managementmode.SectionImpl;

import java.io.Serial;
import java.util.List;

public class SectionDto extends SectionImpl {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 145456166997276994L;

    protected String bioAgressorTopiaId;

    /** Utility field for display purpose. */
    protected String bioAgressorLabel;

    protected List<StrategyDto> strategiesDto;

    public String getBioAgressorTopiaId() {
        return bioAgressorTopiaId;
    }

    public void setBioAgressorTopiaId(String bioAgressorTopiaId) {
        this.bioAgressorTopiaId = bioAgressorTopiaId;
    }

    public List<StrategyDto> getStrategiesDto() {
        return strategiesDto;
    }

    public void setStrategiesDto(List<StrategyDto> strategiesDto) {
        this.strategiesDto = strategiesDto;
    }

    public String getBioAgressorLabel() {
        return bioAgressorLabel;
    }

    public void setBioAgressorLabel(String bioAgressorLabel) {
        this.bioAgressorLabel = bioAgressorLabel;
    }
}
