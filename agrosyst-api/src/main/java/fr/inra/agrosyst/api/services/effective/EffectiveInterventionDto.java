package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.export.InterventionDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
public class EffectiveInterventionDto implements InterventionDto {

    @Serial
    private static final long serialVersionUID = -6966560537841395005L;

    protected String topiaId;

    /**
     * Nom de l'attribut en BD : name
     */
    protected String name;

    /**
     * Nom de l'attribut en BD : comment
     */
    protected String comment;

    /**
     * Nom de l'attribut en BD : startInterventionDate
     */
    protected LocalDate startInterventionDate;

    /**
     * Nom de l'attribut en BD : intermediateCrop
     */
    protected boolean intermediateCrop;

    /**
     * Nom de l'attribut en BD : otherToolSettings
     */
    protected String otherToolSettings;

    /**
     * Nom de l'attribut en BD : workRate
     */
    protected Double workRate;

    /**
     * Nom de l'attribut en BD : progressionSpeed
     */
    protected Integer progressionSpeed;

    /**
     * Nom de l'attribut en BD : involvedPeopleCount
     */
    protected Double involvedPeopleCount;

    /**
     * Nom de l'attribut en BD : endInterventionDate
     */
    protected LocalDate endInterventionDate;

    /**
     * Nom de l'attribut en BD : spatialFrequency
     */
    protected double spatialFrequency;

    /**
     * Nom de l'attribut en BD : transitCount
     */
    protected int transitCount;

    /**
     * Nom de l'attribut en BD : transitVolume
     */
    protected Double transitVolume;

    /**
     * Nom de l'attribut en BD : nbBalls
     */
    protected Integer nbBalls;

    /**
     * Nom de l'attribut en BD : edaplosIssuerId
     */
    protected String edaplosIssuerId;

    /**
     * Nom de l'attribut en BD : type
     */
    protected AgrosystInterventionType type;

    /**
     * Nom de l'attribut en BD : workRateUnit
     */
    protected MaterielWorkRateUnit workRateUnit;

    /**
     * Nom de l'attribut en BD : transitVolumeUnit
     */
    protected MaterielTransportUnit transitVolumeUnit;

    protected final String domainId;

    protected Set<String> toolsCouplingCodes;

    protected String fromCropCode;

    protected Boolean originalIntermediateStatus;
    
    protected Collection<AbstractActionDto> actionDtos;

    @Getter
    protected List<SpeciesStadeDto> speciesStadesDtos;

    public EffectiveInterventionDto(String domainId) {
        this.domainId = domainId;
        this.setTopiaId(EffectiveCropCycleService.NEW_INTERVENTION_PREFIX + UUID.randomUUID());
    }

    public void setIntermediateCrop(boolean intermediateCrop) {
        this.intermediateCrop = intermediateCrop;
        this.originalIntermediateStatus = intermediateCrop;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EffectiveInterventionDto other = (EffectiveInterventionDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }

    @Override
    public double getTemporalFrequencyOrTransitCount() {
        return transitCount;
    }

    @Override
    public double getSpatialFrequency() {
        double result = this.spatialFrequency;
        return result;
    }
}
