package fr.inra.agrosyst.api.services.common;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceImpl;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import lombok.Getter;
import lombok.Setter;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

// PriceDto
@Setter
@Getter
public class PriceDto extends HarvestingPriceImpl implements Serializable {
    
    public static final Function<PriceDto, HarvestingPrice> PRICE_DTO_TO_PRICE = input -> {
        HarvestingPrice result = new HarvestingPriceImpl();
        Binder<PriceDto, HarvestingPrice> binder = BinderFactory.newBinder(PriceDto.class, HarvestingPrice.class);
        binder.copy(input, result);
        return result;
    };
    @Serial
    private static final long serialVersionUID = -5106334574759279061L;
    
    /**
     * Nom de l'attribut en BD : includedTreatment
     */
    protected boolean includedTreatment;
    
    /**
     * Nom de l'attribut en BD : chemicalTreatment
     */
    protected boolean chemicalTreatment;
    
    /**
     * Nom de l'attribut en BD : biologicalTreatment
     */
    protected boolean biologicalTreatment;
    
    /**
     * Nom de l'attribut en BD : seedType
     */
    protected SeedType seedType;
    
    // attributes for SeedingProductPrice
    /**
     * Nom de l'attribut en BD : productType
     */
    protected ProductType productType;
    
    // campaigns
    protected String campaigns;
    
    // topiaId or code
    protected String cropIdentifier;//todo dcosse 23/04/18 rename it
    
    protected HashMap<String, Double> quantityByCodeEspeceBotaniqueCodeQualifantAEE;
    
    protected int nbRefPrices;
    
    protected String fromPriceKey;
    
    public void setQuantityByCodeEspeceBotaniqueCodeQualifantAEE(Map<String, Double> quantityByCodeEspeceBotaniqueCodeQualifantAEE) {
        this.quantityByCodeEspeceBotaniqueCodeQualifantAEE = new HashMap<>(quantityByCodeEspeceBotaniqueCodeQualifantAEE);
    }
}
