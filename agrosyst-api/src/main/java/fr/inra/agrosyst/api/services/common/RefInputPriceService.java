package fr.inra.agrosyst.api.services.common;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInputAndCampaigns;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.Scenario;
import fr.inra.agrosyst.api.services.performance.ScenarioFilter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public interface RefInputPriceService extends AgrosystService {
    
    RefCampaignsInputPricesByDomainInputAndCampaigns getRefInputPricesForCampaignsByInput(
            Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<Integer> campaigns);
    
    RefCampaignsInputPricesByDomainInput getRefInputPricesForCampaignByInput(
            Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Integer campaign);
    
    RefCampaignsInputPricesByDomainInput getRefInputPricesForCampaignByInput(
            RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput,
            Integer campaign
    );
    
    RefCampaignsInputPricesByDomainInput getRefInputPricesForCampaignsByInput(
            RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput,
            Collection<Integer> campaign);
    
    RefScenariosInputPricesByDomainInput getRefInputPricesForScenariosByInput(
            Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStocksByTypes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<String> scenarioCodes);
    
    Optional<RefPrixCarbu> getFuelRefPriceForCampaign(int campaign);
    
    Collection<RefPrixCarbu> getFuelRefPriceForCampaigns(Collection<Integer> campaigns);
    
    /**
     *
     * @param scenarioFilter use to filter on scenario label
     * @return PaginationResult of couple scenarioCode and scenarioLabel
     */
    PaginationResult<Scenario> getScenarioLabelByScenarioCode(ScenarioFilter scenarioFilter);
    
    Collection<Scenario> getScenariosForCodes(Collection<String> scenarioCodes);
}
