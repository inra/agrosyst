package fr.inra.agrosyst.api.services.input;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputType;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.io.Serializable;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public abstract class AbstractInputUsageDto implements Serializable {
    
    @Serial
    private static final long serialVersionUID = -7087722098340949696L;
    
    /**
     * Nom de l'attribut en BD : inputType
     */
    @NonNull
    protected InputType inputType;
    /**
     * Nom de l'attribut en BD : productName
     */
    protected String productName;
    
    /**
     * id in database
     */
    protected String inputUsageTopiaId;

    protected String code;
    
    /**
     * Nom de l'attribut en BD : comment
     */
    protected String comment;
    
    /**
     * Nom de l'attribut en BD : qtMin
     */
    protected Double qtMin;
    
    /**
     * Nom de l'attribut en BD : qtAvg
     */
    protected Double qtAvg;
    
    /**
     * Nom de l'attribut en BD : qtMed
     */
    protected Double qtMed;
    
    /**
     * Nom de l'attribut en BD : qtMax
     */
    protected Double qtMax;
    
    
    public Optional<String> getInputUsageTopiaId(){
        return Optional.ofNullable(inputUsageTopiaId);
    }
    
    public Optional<String> getProductName() {
        return Optional.ofNullable(productName);
    }
}
