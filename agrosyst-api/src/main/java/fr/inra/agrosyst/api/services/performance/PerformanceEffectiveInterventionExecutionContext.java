package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020  - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
public class PerformanceEffectiveInterventionExecutionContext extends PerformanceInterventionContext {

    protected final EffectiveIntervention intervention;

    protected final CroppingPlanEntry croppingPlanEntry;

    protected Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> legacyRefDosageForPhytoInputs = new HashMap<>();// used for ITF

    // use t display yeald Average for destination/unit
    protected Map<Pair<RefDestination, YealdUnit>, Double> yealdAveragesByDestinations = new HashMap<>();

    private String outputInterventionYealdAverage;

    @Setter
    private Set<RefEspece> refEspeces;

    public PerformanceEffectiveInterventionExecutionContext(
            EffectiveIntervention effectiveIntervention,
            Integer domainCampaign,
            ToolsCoupling toolsCoupling,
            List<AbstractAction> actions,
            CroppingPlanEntry croppingPlanEntry,
            PriceConverterKeysToRate priceConverterKeysToRate,
            List<HarvestingPrice> harvestingPrices,
            MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode) {
        super(
                effectiveIntervention != null ? effectiveIntervention.getTopiaId() : DUMMY,
                effectiveIntervention != null ? effectiveIntervention.getName() : DUMMY,
                Sets.newHashSet(domainCampaign),
                toolsCoupling,
                actions,
                priceConverterKeysToRate,
                harvestingPrices,
                allSubstancesActivesCommissionEuropeenneByAmmCode);

        this.intervention = effectiveIntervention;
        // main or intermediate one
        this.croppingPlanEntry = croppingPlanEntry;
    }

    public void addProductWithoutAutoconsume(double value) {
        realProductWithoutAutoconsume += value;
    }

    public void addProductWithAutoconsume(Double value) {
        if (value != null) {
            realProductWithAutoconsume += value;
        }
    }

    public void addStandardisedProductWithoutAutoconsume(Double value) {
        if (value != null) {
            standardisedProductWithoutAutoconsume += value;
        }
    }

    public void addStandardisedProductWithAutoconsume(Double value) {
        if (value != null) {
            standardisedProductWithAutoconsume += value;
        }
    }

    public void addExpenses(Double value) {
        if (value != null) {
            operatingExpenses += value;
        }
    }

    public void addStandardisedExpenses(Double value) {
        if (value != null) {
            standardisedOperatingExpenses += value;
        }
    }

    public void setInterventionSpecies(List<CroppingPlanSpecies> interventionSpecies) {
        this.interventionSpecies = interventionSpecies;
    }

    public void setInterventionYealdAveragesTrad(String outputInterventionYealdAverage) {
        this.outputInterventionYealdAverage = outputInterventionYealdAverage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformanceEffectiveInterventionExecutionContext that = (PerformanceEffectiveInterventionExecutionContext) o;
        return intervention.equals(that.intervention);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intervention);
    }

}
