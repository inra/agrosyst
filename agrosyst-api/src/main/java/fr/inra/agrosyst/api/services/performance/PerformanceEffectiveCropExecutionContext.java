package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPrices;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformanceEffectiveCropExecutionContext extends AbstractPerformanceCropContext {

    protected CroppingPlanEntry crop;
    protected CroppingPlanEntry seasonalPreviousCrop;
    protected CroppingPlanEntry intermediateCrop;

    // for perennial
    protected EffectivePerennialCropCycle perennialCropCycle;

    // for seasonnal
    protected EffectiveCropCycleConnection connection;
    
    protected Set<PerformanceEffectiveInterventionExecutionContext> interventionExecutionContexts;

    protected int rank;
    protected long nbSeasonalRank;

    // income prices
    protected List<HarvestingValorisationPrices> harvestingValorisationPrices;
    
    protected Set<RefEspece> refEspeces;

    protected Map<TraitementProduitWithCroppingPlanEntry, ReferenceDoseDTO> legacyDoseForProducts;

    protected RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop;
    protected RefCultureEdiGroupeCouvSol intermediateSpeciesMaxCouvSolForCrop;

    protected WeedType weedType;// get it from perennial crop cycle
    
    // use to display yeald Average for destination/unit
    protected Map<Pair<RefDestination, YealdUnit>, List<Double>> interventionsYealds = new HashMap<>();
    protected Map<Pair<RefDestination, YealdUnit>, List<Double>> intermediateInterventionsYealds = new HashMap<>();
    // intermediate + main yeald Averages
    protected Map<Pair<RefDestination, YealdUnit>, Double> mainCropYealds = null;
    protected Map<Pair<RefDestination, YealdUnit>, Double> intermediateCropYealds = null;

    public PerformanceEffectiveCropExecutionContext() {
    }
    
    public PerformanceEffectiveCropExecutionContext(CroppingPlanEntry crop, EffectivePerennialCropCycle perennialCropCycle, Set<RefEspece> refEspeces, WeedType weedType, RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop) {
        this.crop = crop;
        this.perennialCropCycle = perennialCropCycle;
        this.refEspeces = refEspeces;
        this.weedType = weedType;
        this.speciesMaxCouvSolForCrop = speciesMaxCouvSolForCrop;
    }
    
    public PerformanceEffectiveCropExecutionContext(CroppingPlanEntry crop, CroppingPlanEntry intermediateCrop, CroppingPlanEntry previousCrop, int rank, Set<RefEspece> refEspeces, RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop, RefCultureEdiGroupeCouvSol intermediateSpeciesMaxCouvSolForCrop) {
        this.crop = crop;
        this.intermediateCrop = intermediateCrop;
        this.seasonalPreviousCrop = previousCrop;
        this.rank = rank;
        this.refEspeces = refEspeces;
        this.speciesMaxCouvSolForCrop = speciesMaxCouvSolForCrop;
        this.intermediateSpeciesMaxCouvSolForCrop = intermediateSpeciesMaxCouvSolForCrop;
    }

    public void addInterventionYealdAverage(Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage) {
        for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> forDestinationAndUnitYealdAverage : interventionYealdAverage.entrySet()) {
            Pair<RefDestination, YealdUnit> forDestinationAndUnit = forDestinationAndUnitYealdAverage.getKey();
            List<Double> interventionsYealdsValues = interventionsYealds.computeIfAbsent(forDestinationAndUnit, k -> new ArrayList<>());
            interventionsYealdsValues.add(forDestinationAndUnitYealdAverage.getValue());
        }
    }

    public void addIntermediateInterventionYealdAverage(Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage) {
        for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> forDestinationAndUnitYealdAverage : interventionYealdAverage.entrySet()) {
            Pair<RefDestination, YealdUnit> forDestinationAndUnit = forDestinationAndUnitYealdAverage.getKey();
            List<Double> interventionsYealdsValues = intermediateInterventionsYealds.computeIfAbsent(forDestinationAndUnit, k -> new ArrayList<>());
            interventionsYealdsValues.add(forDestinationAndUnitYealdAverage.getValue());
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformanceEffectiveCropExecutionContext that = (PerformanceEffectiveCropExecutionContext) o;
        return rank == that.rank &&
                crop.equals(that.crop) &&
                Objects.equals(perennialCropCycle, that.perennialCropCycle) &&
                Objects.equals(connection, that.connection);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(crop, perennialCropCycle, connection, rank);
    }
}
