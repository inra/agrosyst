package fr.inra.agrosyst.api;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.hibernate.boot.model.FunctionContributions;
import org.hibernate.dialect.PostgreSQLDialect;
import org.hibernate.query.sqm.produce.function.FunctionParameterType;
import org.hibernate.type.BasicTypeRegistry;
import org.hibernate.type.StandardBasicTypes;

public class PostgreSQL10CustomDialect extends PostgreSQLDialect {

    @Override
    public void initializeFunctionRegistry(FunctionContributions functionContributions) {
        super.initializeFunctionRegistry(functionContributions);
        BasicTypeRegistry basicTypeRegistry = functionContributions.getTypeConfiguration().getBasicTypeRegistry();
        functionContributions.getFunctionRegistry().patternDescriptorBuilder("to_complex_i18n_key", "to_complex_i18n_key(?1)")
                .setInvariantType(basicTypeRegistry.resolve(StandardBasicTypes.STRING))
                .setExactArgumentCount(1)
                .setParameterTypes(FunctionParameterType.STRING)
                .register();
        functionContributions.getFunctionRegistry().patternDescriptorBuilder("to_simple_i18n_key", "to_simple_i18n_key(?1)")
                .setInvariantType(basicTypeRegistry.resolve(StandardBasicTypes.STRING))
                .setExactArgumentCount(1)
                .setParameterTypes(FunctionParameterType.STRING)
                .register();
    }
}
