package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpecies;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.itk.Itk;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;

import java.util.function.Function;

public class PracticedSystems {

    public static final Function<PracticedCropCycleConnection, PracticedCropCycleConnectionDto> CROP_CYCLE_CONNECTION_TO_DTO = input -> {
        PracticedCropCycleConnectionDto result = new PracticedCropCycleConnectionDto();
        result.setSourceId(Entities.ESCAPE_TOPIA_ID.apply(input.getSource().getTopiaId()));
        result.setTargetId(Entities.ESCAPE_TOPIA_ID.apply(input.getTarget().getTopiaId()));
        result.setIntermediateCroppingPlanEntryCode(input.getIntermediateCroppingPlanEntryCode());
        result.setCroppingPlanEntryFrequency(input.getCroppingPlanEntryFrequency());
        result.setNotUsedForThisCampaign(input.isNotUsedForThisCampaign());
        return result;
    };

    public static final Function<PracticedCropCycleNode, PracticedCropCycleNodeDto> CROP_CYCLE_NODE_TO_DTO = input -> {
        PracticedCropCycleNodeDto result = new PracticedCropCycleNodeDto();
        result.setNodeId(Entities.ESCAPE_TOPIA_ID.apply(input.getTopiaId()));
        result.setX(input.getRank());
        result.setY(input.getY());
        result.setEndCycle(input.isEndCycle());
        result.setSameCampaignAsPreviousNode(input.isSameCampaignAsPreviousNode());
        result.setCroppingPlanEntryCode(input.getCroppingPlanEntryCode());
        result.setInitNodeFrequency(input.getInitNodeFrequency());
        return result;
    };
    public static final Function<PracticedCropCycleSpecies, String> GET_CROP_CYCLE_PERENNIAL_SPECIES_CODE = PracticedCropCycleSpecies::getCroppingPlanSpeciesCode;

    public static final Function<RefVariete, CropCycleGraftDto> REF_VARIETE_TO_GRAPH_DTO = input -> {
        CropCycleGraftDto result = null;
        if (input != null) {
            result = new CropCycleGraftDto();
            result.setTopiaId(input.getTopiaId());
            result.setLabel(input.getLabel());
            result.setValue(input.getLabel());
        }
        return result;
    };

    public static final Function<RefClonePlantGrape, CropCycleGraftDto> REF_CLONE_TO_GRAPH_DTO = input -> {
        CropCycleGraftDto result = null;
        if (input != null) {
            result = new CropCycleGraftDto();
            result.setTopiaId(input.getTopiaId());
            String label = String.format("%d, %d (%s)", input.getCodeClone(), input.getAnneeAgrement(), input.getOrigine());
            result.setLabel(label);
            result.setValue(label);
        }
        return result;
    };

    public static SpeciesStadeDto getDtoForPracticedSpeciesStade(PracticedSpeciesStade input, ReferentialTranslationMap translationMap) {
        SpeciesStadeDto result = new SpeciesStadeDto();
        result.setTopiaId(input.getTopiaId());

        if (input.getSpeciesCode() != null) {
            result.setSpeciesCode(input.getSpeciesCode());
            //XXX ymartel 2019/10/18 : be careful, cannot get SpeciesName here... Need to request on DAO ! @see PracticedExportXls#exportSpeciesStadeFields for example
        }
        result.setStadeMin(Itk.getDtoForRefStadeEdi(input.getStadeMin(), translationMap));
        result.setStadeMax(Itk.getDtoForRefStadeEdi(input.getStadeMax(), translationMap));
        return result;
    }
}
