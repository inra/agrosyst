package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.CattleDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.ToolsCouplingDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Practiced system service.
 *
 * @author Eric Chatellier
 */
public interface PracticedSystemService extends AgrosystService {

    String NEW_INTERVENTION_PREFIX = "NEW-INTERVENTION-";

    String NEW_NODE_PREFIX = "new-node-";

    /**
     * Return the practicedSystem matching the given TopiaId key if null a new empty instance is created.
     *
     * @param practicedSystemTopiaId the PracticedSystem's TopiaId can be null
     * @return the entity
     */
    PracticedSystem getPracticedSystem(String practicedSystemTopiaId);

    /**
     * Return the list of Plots filtered according the given filter.
     *
     * @param filter the Plot filter
     * @return the filtered plots
     */
    PaginationResult<PracticedSystem> getFilteredPracticedSystems(PracticedSystemFilter filter);

    /**
     * Return a list of Plot's DTOs filtered according the given filter.
     * @param filter the Plot filter
     * @return the filtered dto plots
     */
    PaginationResult<PracticedSystemDto> getFilteredPracticedSystemsDto(PracticedSystemFilter filter);

    Set<String> getFilteredPracticedSystemIds(PracticedSystemFilter filter);

    //boolean areCampaignsValid(String campaigns);

    List<String> getToolsCouplingsFromGrowingSystemAndCampaigns(String growingSystemId, String campaigns);
    
    List<String> getToolsCouplingsCodesFromDomainAndCampaigns(String domainCode, Integer targetedCampaign);

    /**
     * Return all Cropping Plan Entry code present on domain related to the growing system that match the given growing system id and for the given campaigns.
     * @param growingSystemId the growing system's id
     * @param campaigns the domain campaigns
     * @return all Cropping Plan Entry code for the growing system's domain and for the given campaigns
     */
    List<String> getCropCodesFromGrowingSystemIdForCampaigns(String growingSystemId, String campaigns);

    /**
     * Return all Cropping Plan Entry code present on domains related to the domain that match the given domain id and for the given campaigns.
     * @param domainId the domain's id
     * @param campaigns the domain campaigns
     * @return all Cropping Plan Entry code for the growing system's domain and for the given campaigns
     */
    List<String> getCropCodesFromDomainIdForCampaigns(String domainId, String campaigns);

    /**
     * Retourne les modeles des cultures.
     */
    Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> getCropCycleModelMap(String growingSystemId, boolean includeIntermediate);

    Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> getIpmworksCropCycleModelMap(String domainId);

    Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(String growingSystemId, String campaigns);

    Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant(String growingSystemId, String campaigns);

    Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds2(String domainId, String campaigns);

    Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant2(String domainId, String campaigns);

    /**
     * Create or update practicedPerennialCropCycle.
     *
     * @param practicedSystem                 The practicedPerennialCropCycle's PracticedSystem
     * @param practicedPerennialCropCycleDtos All the PracticedPerennialCropCycleDtos
     * @param practicedSeasonalCropCycleDtos  All the PracticedSeasonalCropCycleDto
     * @return practiced system
     */
    PracticedSystem createOrUpdatePracticedSystem(PracticedSystem practicedSystem,
                                                  List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos,
                                                  List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos);

    boolean isConnectionMissingInCropCycles(Collection<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos);

    Map<AgrosystInterventionType, List<ToolsCouplingDto>> getToolsCouplingModelForInterventionTypes(String growingSystemId, String campaigns, Set<AgrosystInterventionType> interventionTypes);

    /**
     *
     * @param cycleId practicedSeasonalCropCycle id, if null a new instance is created
     * @return the PracticedSeasonalCropCycle matching the given cycleId
     */
    PracticedSeasonalCropCycle getPracticedSeasonalCropCycle(String cycleId);

    List<Equipment> getEquipmentsForInterventionPracticedSystem(PracticedSystem practicedSystem, Domain domain);

    /**
     * Return All practicedPerennialCropCycle related to the PracticedSystem
     *
     * @param practicedSystemId the PracticedSystem's topiaId where to find the practicedPerennialCropCycle
     * @return All practicedPerennialCropCycle
     */
    List<PracticedPerennialCropCycleDto> getAllPracticedPerennialCropCycles(String practicedSystemId);

    /**
     * Return All PracticedSeasonalCropCycle related to the PracticedSystem
     *
     * @param practicedSystemId the PracticedSystem's topiaId where to find the PracticedSeasonalCropCycle
     * @return All PracticedSeasonalCropCycle
     */
    List<PracticedSeasonalCropCycleDto> getAllPracticedSeasonalCropCycles(String practicedSystemId);

    /**
     * Méthode qui charge la liste des espèces disponibles pour le cycle passé en paramètre et remplit les Dto avec les
     * informations déjà saisie dans ce cycle
     *
     * @param croppingPlanEntryCode l'identifiant de la culture à traiter
     * @param cycle                 le cycle pérenne
     * @param campaigns             la liste des campagnes concernées (issues du système synthétisé)
     * @return la liste complète des espèces complétée des informations liées au cycle
     */
    List<PracticedCropCycleSpeciesDto> getCropCyclePerennialSpecies(String croppingPlanEntryCode, PracticedPerennialCropCycle cycle, String campaigns);

    /**
     * return the name of the cropping plan
     *
     * @param croppingPlanEntryCode the cropping plan's code, if null a new instance is created but not persisted yed
     * @return the cropping plan's name
     */
    String getCroppingPlanEntryName(String croppingPlanEntryCode);

    /**
     * Return practicedPerennialCropCycle with the given cycleId
     * @param cycleId if of cycle
     * @return the practicedPerennialCropCycle found
     */
    PracticedPerennialCropCycle getPracticedPerennialCropCycle(String cycleId);

    /**
     * Duplicate practiced system with id practicedSystemId on the target growing system with id the growingSystemId given has parameter.
     * It's possible to duplicate a practiced system if the targeted growing system is same as origin one
     * or if it's domain code is same as origin one
     * Error is throw up if crop can not be found on targeted domain
     *
     * @param fromPracticedSystemId practiced system id
     * @param toGrowingSystemId growing system id
     * @return duplicated practiced system
     */
    PracticedSystem duplicatePracticedSystemWithUsages(String fromPracticedSystemId, String toGrowingSystemId);
    
    /**
     * Unactivate or reactivate the given practiced System
     *
     * @param practicedSystemIds :   Practiced System topiaId to unactivate/reactivate
     * @param activate           :   if "true", reactivate practiced systems, else unactivate.
     */
    void inactivatePracticedSystem(List<String> practicedSystemIds, boolean activate);

    /**
     * return the Domain code from the domain related to the growing system with id as growingSystemId
     * @param growingSystemId growing system Id
     * @return the DomainKeys
     */
    String getDomainCode(String growingSystemId);
    
    List<PracticedSystem> getAllValidatedForGivenIds(List<String> practicedSystemIds);
    
    List<PracticedSystem> getAllUnValidatedForGivenIds(List<String> practicedSystemIds);
    
    /**
     * Set valid true to practiced system
     * @param practicedSystemIds The practiced system ids to valid
     * @param validate validate or not
     * @return if success or not for practiced system
     */
    List<OperationStatus> validate(List<String> practicedSystemIds, boolean validate);

    /**
     * Export cycle
     * @param practicedSystemIds cycle id's to export
     * @return exported cycles
     */
    ExportResult exportPracticedSystemsAsXls(Collection<String> practicedSystemIds);

    void exportPracticedSystemsAsXlsAsync(Collection<String> practicedSystemIds);

    List<GrowingSystem> getGrowingSystemsForNewPracticedSystem(GrowingSystemFilter growingSystemFilter, NavigationContext navigationContext);

    boolean isActivated(PracticedSystem practicedSystem);

    boolean areActivated(Collection<PracticedSystem> practicedSystems);

    Collection<CattleDto> getAllCampaignsGrowingSystemRelatedCattles(String growingSystemId, Set<Integer> campaigns);

    Map<String, DomainInputDto> getDomainInputs(String growingSystemId, String campaigns);

    List<Integer> getGrowingSystemCampaignsFromId(String practicedSystemTopiaId, NavigationContext navigationContext);

    Collection<CattleDto> getAllCampaignsPracticedSystemRelatedCattles(String practicedId, LinkedHashSet<Integer> campaigns);
}
