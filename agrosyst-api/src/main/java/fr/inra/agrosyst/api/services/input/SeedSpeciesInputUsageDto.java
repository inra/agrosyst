package fr.inra.agrosyst.api.services.input;
/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class SeedSpeciesInputUsageDto extends AbstractInputUsageDto {
    
    @Serial
    private static final long serialVersionUID = -1511655668975482100L;
    
    @NonNull
    protected final DomainSeedSpeciesInputDto domainSeedSpeciesInputDto;
    
    @NonNull
    SeedPlantUnit usageUnit;
    
    /*
    species code for usage on practiced system or topiaId on effective
     */
    @NonNull
    protected String speciesIdentifier;
    
    protected final List<PhytoProductInputUsageDto> seedProductInputDtos;
    
    protected final Double deepness;
    

    protected String varietyLibelle;
    ////species.speciesEspece + (species.speciesQualifiant ? ', ' + species.speciesQualifiant : '') + (species.speciesTypeSaisonnier ? ', ' + species.speciesTypeSaisonnier : '') + (species.speciesDestination ? ', ' + species.speciesDestination : '')
    protected String speciesLibelle;
    
    public Optional<Collection<PhytoProductInputUsageDto>> getSeedProductInputDtos() {
        return Optional.ofNullable(seedProductInputDtos);
    }
}
