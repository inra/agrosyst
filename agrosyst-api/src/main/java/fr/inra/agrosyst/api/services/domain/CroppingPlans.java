package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.practiced.CropCycleModelDto;
import org.nuiton.util.StringUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class CroppingPlans {

    public static CroppingPlanSpeciesDto getDtoForCroppingPlanSpecies(CroppingPlanSpecies input, ReferentialTranslationMap translationMap) {
        CroppingPlanSpeciesDto result = new CroppingPlanSpeciesDto();
        result.setTopiaId(input.getTopiaId());
        result.setCode(input.getCode());
        RefEspece species = input.getSpecies();

        ReferentialEntityTranslation refEspeceTranslation = translationMap.getEntityTranslation(species.getTopiaId());
        String especeBotanique = refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, species.getLibelle_espece_botanique());
        String qualifiantAEE = refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_QUALIFIANT__AEE, species.getLibelle_qualifiant_AEE());
        String typeSaisonnier = refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE, species.getLibelle_type_saisonnier_AEE());
        String destination = refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_DESTINATION__AEE, species.getLibelle_destination_AEE());

        result.setSpeciesArea(input.getSpeciesArea());
        result.setSpeciesId(species.getTopiaId());
        result.setSpeciesEspece(especeBotanique);
        result.setSpeciesQualifiant(qualifiantAEE);
        result.setSpeciesTypeSaisonnier(typeSaisonnier);
        result.setSpeciesDestination(destination);
        result.setProfil_vegetatif_BBCH(species.getProfil_vegetatif_BBCH());
        result.setCode_espece_botanique(species.getCode_espece_botanique());
        result.setCode_qualifiant_AEE(species.getCode_qualifiant_AEE());
        result.setCode_destination_aee(species.getCode_destination_AEE());
        result.setCode_type_saisonier(species.getCode_type_saisonnier_AEE());
        RefVariete variety = input.getVariety();
        if (variety != null) {
            result.setVarietyId(variety.getTopiaId());
            result.setVarietyLibelle(variety.getLabel());
        }
        RefDestination refDdestination = input.getDestination();
        if (refDdestination != null) {
            result.setDestinationId(refDdestination.getTopiaId());
        }
        result.setCompagne(input.getCompagne());
        result.setEdaplosUnknownVariety(input.getEdaplosUnknownVariety());
        result.setValidated(input.isValidated());
        return result;
    }

    public static CroppingPlanSpeciesDto getMinimalDtoForCroppingPlanSpecies(CroppingPlanSpecies input) {
        CroppingPlanSpeciesDto result = new CroppingPlanSpeciesDto();
        result.setTopiaId(input.getTopiaId());
        result.setCode(input.getCode());
        RefEspece species = input.getSpecies();
        result.setSpeciesArea(input.getSpeciesArea());
        result.setSpeciesId(species.getTopiaId());
        result.setCode_destination_aee(species.getCode_destination_AEE());
        result.setProfil_vegetatif_BBCH(species.getProfil_vegetatif_BBCH());
        result.setCode_espece_botanique(species.getCode_espece_botanique());
        result.setCode_qualifiant_AEE(species.getCode_qualifiant_AEE());
        RefVariete variety = input.getVariety();
        if (variety != null) {
            result.setVarietyId(variety.getTopiaId());
            result.setVarietyLibelle(variety.getLabel());
        }
        result.setCompagne(input.getCompagne());
        result.setEdaplosUnknownVariety(input.getEdaplosUnknownVariety());
        result.setValidated(input.isValidated());
        return result;
    }

    static final Binder<CroppingPlanEntry, CroppingPlanEntryDto> CPE_TO_DTO_BINDER = BinderFactory.newBinder(CroppingPlanEntry.class, CroppingPlanEntryDto.class);

    public static CroppingPlanEntryDto getDtoForCroppingPlanEntry(
            CroppingPlanEntry input,
            ReferentialTranslationMap translationMap) {
        CroppingPlanEntryDto result = new CroppingPlanEntryDto();

        CPE_TO_DTO_BINDER.copyExcluding(input, result,
                CroppingPlanEntry.PROPERTY_DOMAIN,
                CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);

        List<CroppingPlanSpecies> species = Optional.ofNullable(input.getCroppingPlanSpecies()).orElse(new ArrayList<>());
        result.setSpecies(new ArrayList<>(species.stream().map(s -> getDtoForCroppingPlanSpecies(s, translationMap)).collect(Collectors.toList())));
        boolean hasCompanion = result.getSpecies().stream().anyMatch(s -> Objects.nonNull(s.getCompagne()));
        result.setMixCompanion(hasCompanion);

        result.setColor(STRING_TO_COLOR.apply(input.getName()));

        return result;
    }

    public static void translateCroppingPlanEntry(CroppingPlanEntry croppingPlanEntry, ReferentialTranslationMap translationMap) {
        if(croppingPlanEntry.isCroppingPlanSpeciesNotEmpty()) {
            croppingPlanEntry.getCroppingPlanSpecies().stream().filter(Objects::nonNull).forEach(cps -> {
                RefEspece species = cps.getSpecies();
                bindTranslationToRefEspece(translationMap, species);
            });
        }
    }

    public static void bindTranslationToRefEspece(ReferentialTranslationMap translationMap, RefEspece species) {
        ReferentialEntityTranslation refEspeceTranslation = translationMap.getEntityTranslation(species.getTopiaId());
        species.setLibelle_espece_botanique_Translated(refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, species.getLibelle_espece_botanique()));
        species.setLibelle_qualifiant_AEE_Translated(refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_QUALIFIANT__AEE, species.getLibelle_qualifiant_AEE()));
        species.setLibelle_type_saisonnier_AEE_Translated(refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE, species.getLibelle_type_saisonnier_AEE()));
        species.setLibelle_destination_AEE_Translated(refEspeceTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_DESTINATION__AEE, species.getLibelle_destination_AEE()));
    }

    public static final java.util.function.Function<String, String> STRING_TO_COLOR = input -> {
        String hash = StringUtil.encodeSHA1(input);
        String result = "#" + hash.substring(7, 13);
        return result;
    };

    public static final java.util.function.Function<CroppingPlanSpecies, String> GET_SPECIES_CODE = CroppingPlanSpecies::getCode;
    
    public static final Predicate<CroppingPlanEntryDto> IS_ENTRY_INTERMEDIATE = CroppingPlanEntryDto::isIntermediate;

    public static final Predicate<CropCycleModelDto> IS_INTERMEDIATE = CropCycleModelDto::isIntermediate;

    public static final Predicate<CropCycleModelDto> IS_NOT_INTERMEDIATE = IS_INTERMEDIATE.negate();

}
