package fr.inra.agrosyst.api.services.context;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public interface NavigationContextService extends AgrosystService {

    List<Integer> getAllCampaigns();

    PaginationResult<Network> getAllNetworks(int nbElements);

    PaginationResult<DomainDto> getAllDomainsForCampaign(Set<Integer> campaigns, Set<String> networkIds, int nbElements);

    PaginationResult<GrowingPlanDto> getAllGrowingPlansForDomains(Set<Integer> campaigns, Set<String> domainIds, Set<String> networkIds, int nbElements);

    PaginationResult<GrowingSystemDto> getAllGrowingSystemsForGrowingPlans(Set<Integer> campaigns, Set<String> domainIds, Set<String> growingPlansIds, Set<String> networkIds, int nbElements);

    NavigationContext verify(NavigationContext navigationContext);

    NavigationContext verify(NavigationContext navigationContext, TopiaEntity newEntity);

    Map<String, String> getNetworks(Set<String> networksIds, int maxCount);

    Map<String, String> getDomains(Set<String> domainsIds, int maxCount);

    Map<String, String> getGrowingPlans(Set<String> growingPlansIds, int maxCount);

    Map<String, String> getGrowingSystems(Set<String> growingSystemsIds, int maxCount);

}
