/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.api.services.report;

import fr.inra.agrosyst.api.SortOrder;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.AgrosystFilter;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * Filtre de la liste des bilans de campagne.
 */
@Getter
@Setter
public class ReportRegionalFilter extends AgrosystFilter {
    
    @Serial
    private static final long serialVersionUID = -4313162601138025801L;
    
    protected String name;
    
    protected Integer campaign;
    
    protected String network;
    
    protected Sector sector;
    
    protected SortedColumn sortedColumn;
    
    protected SortOrder sortOrder = SortOrder.ASC;
}
