package fr.inra.agrosyst.api.services.security;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.users.UserDto;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UserRoleDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 6913557761704438621L;

    protected String topiaId;
    protected RoleType type;

    protected UserDto user;
    protected UserRoleEntityDto entity;

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public RoleType getType() {
        return type;
    }

    public void setType(RoleType type) {
        this.type = type;
    }

    public UserRoleEntityDto getEntity() {
        return entity;
    }

    public void setEntity(UserRoleEntityDto entity) {
        this.entity = entity;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("topiaId", topiaId)
                .add("type", type)
                .add("user", user)
                .add("entity", entity).toString();
    }

}
