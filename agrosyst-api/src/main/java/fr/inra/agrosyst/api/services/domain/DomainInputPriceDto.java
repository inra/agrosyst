package fr.inra.agrosyst.api.services.domain;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.PriceType;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Data
@AllArgsConstructor
public class DomainInputPriceDto {
    
    /**
     * Nom de l'attribut en BD : objectId
     */
    protected String objectId;
    
    /**
     * Nom de l'attribut en BD : displayName
     */
    protected String displayName;
    
    /**
     * Nom de l'attribut en BD : price
     */
    protected Double price;
    
    /**
     * Nom de l'attribut en BD : sourceUnit
     */
    protected String sourceUnit;
    
    /**
     * Nom de l'attribut en BD : sourceUnitLabel
     */
    protected String sourceUnitLabel;
    
    /**
     * Nom de l'attribut en BD : type
     */
    protected PriceType type;
    
    /**
     * Nom de l'attribut en BD : priceUnit
     */
    protected PriceUnit priceUnit;
    
    /**
     * Nom de l'attribut en BD : category
     */
    protected InputPriceCategory category;
    
    // seedPrice
    
    /**
     * Nom de l'attribut en BD : biologicalSeedInoculation
     */
    protected boolean biologicalSeedInoculation;
    
    /**
     * Nom de l'attribut en BD : chemicalTreatment
     */
    protected boolean chemicalTreatment;
    
    /**
     * Nom de l'attribut en BD : includedTreatment
     */
    protected boolean includedTreatment;
    
    /**
     * Nom de l'attribut en BD : organic
     */
    protected boolean organic;
    
    /**
     * Nom de l'attribut en BD : seedType
     */
    protected SeedType seedType;
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainInputPriceDto that = (DomainInputPriceDto) o;
        return biologicalSeedInoculation == that.biologicalSeedInoculation && chemicalTreatment == that.chemicalTreatment && includedTreatment == that.includedTreatment && organic == that.organic && objectId.equals(that.objectId) && sourceUnit.equals(that.sourceUnit) && category == that.category && seedType == that.seedType;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(objectId, sourceUnit, category, biologicalSeedInoculation, chemicalTreatment, includedTreatment, organic, seedType);
    }
}
