package fr.inra.agrosyst.api.services.plot;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.domain.PlotDto;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author David Cossé : cosse@codelutin.com
 */
public interface PlotService extends AgrosystService {

    /**
     * Return the plot with reference, the given TopiaId key.
     * If plot does not exists a new one is created, not persisted yet
     * @param plotTopiaId the Plots's TopiaId
     * @return the topiaID
     */
    Plot getPlot(String plotTopiaId);

    /**
     * Create or update the plot
     * @param plot the plot to save
     * @param domainId the plot domain's id
     * @param locationId RefLocation id, may be null
     * @param growingSystemId Growing System id, may be null
     * @param selectedPlotZoningIds all plotZonings, may be null
     * @param selectedSolId Ground id
     * @param selectedSurfaceTextureId surface Texture id, may be null
     * @param selectedSubSoilTextureId Sub soil texture id, may be null
     * @param selectedSolDepthId sol Depth id from RefSolProfondeurIndigo, may be null
     * @param solHorizons plot all sol Horizon, may be null
     * @param zones All plot'szones, at least one is required
     * @param adjacentElementIds all adjacent element ids, may be null
     * @return the persisted plot
     */
    Plot createOrUpdatePlot(Plot plot, String domainId, String locationId, String growingSystemId,
                            Collection<String> selectedPlotZoningIds, String selectedSolId,
                            String selectedSurfaceTextureId, String selectedSubSoilTextureId,
                            String selectedSolDepthId, List<SolHorizonDto> solHorizons,
                            Collection<Zone> zones, List<String> adjacentElementIds);

    /**
     * Find all plots for a specific growing system.
     * 
     * @param growingSystem growing plan
     * @return growing sytem's plot
     */
    List<Plot> findAllByGrowingSystem(GrowingSystem growingSystem);

    /**
     * Find all free plot and plots linked to current system.
     * 
     * @param growingSystem growing system
     * @param growingPlanId growing plan id
     * @return all available plots for current growing system
     */
    List<PlotDto> findAllFreeAndGrowingSystemPlots(GrowingSystem growingSystem, String growingPlanId);

    /**
     * Retourne les parcelles libres (non affectée à un système de culture) liées au dispositif
     * choisit.
     * 
     * @param growingPlanTopiaId growing plan id
     * @return plots list
     */
    List<Plot> getFreePlotForGrowingPlan(String growingPlanTopiaId);

    /**
     * Duplicate plot for extend domain process.
     * Do not commit transaction.
     * 
     * @param extendContext extend context
     * @param clonedDomain domain to attache plot to
     * @param clonedGrowingSystem growingSystem to attache plot to, null able
     * @param plot plot
     * @return cloned plot
     */
    Plot extendPlot(ExtendContext extendContext, Domain clonedDomain, GrowingSystem clonedGrowingSystem, Plot plot);

    /**
     * Update Plots relationship with a growingSystem
     * @param growingSystem the growing system
     * @param plotIds All plots TopiaIds related with the growingSystem.
     */
    void updatePlotsGrowingSystemRelationship(GrowingSystem growingSystem,
                                              Collection<String> plotIds);

    /**
     * Find all plot for a specific domain.
     * 
     * @param domain domain
     * @return all domain's plots
     */
    List<Plot> findAllForDomain(Domain domain);

    /**
     * Find all plot's zone.
     * 
     * @param plot plot
     * @return zones
     */
    List<Zone> getPlotZones(Plot plot);

    /**
     * Get zones with map of plot's zones topiaIds with boolean set to true, if zone is used or not into the soft.
     *
     * @param plotTopiaId plot topia id
     * @return
     */
    UsageList<Zone> getZonesAndUsages(String plotTopiaId);

    /**
     * Get all plots related to given code.
     * 
     * @param plotCode plot code
     * @return related plots
     */
    LinkedHashMap<Integer, String> getRelatedPlots(String plotCode);

    /**
     * Get all zones related to given code.
     * 
     * @param zoneCode zone code
     * @return related zones
     */
    LinkedHashMap<Integer, String> getRelatedZones(String zoneCode);

    /**
     * Duplicate given plot topia id and return duplicated plot topia id.
     * 
     * @param topiaId plot topia id to duplicate
     * @return duplicated plot topia id
     */
    Plot duplicatePlot(String topiaId);
    
    /**
     * Active or unactive plot according to the given parameter.
     * @param plotTopiaIds plot id to unactivate
     * @param isToActivated activate or unactive plot requested status
     */
    void updateActivatePlotStatus(List<String> plotTopiaIds, boolean isToActivated);

    boolean validMergingPlots(List<String> plotTopiaIds);

    /**
     * Merge given plot id into a single plot.
     * 
     * Remaining plot is plot with bigger surface.
     * 
     * @param plotTopiaIds
     * @return
     */
    Plot mergePlots(List<String> plotTopiaIds);

    /**
     * Retourne la liste des observations et mesures associées à la parcelle.
     * 
     * @param plotTopiaId plot topia id
     * @return plot's measurement
     */
    List<MeasurementSession> getPlotMeasurementSessions(String plotTopiaId);

    /**
     * Retourne la liste des autres zones attachées au domaine de la zone dont l'identifiant
     * et l'identifiant de zone passée en paramètre.
     *
     * @param zoneId identifiant de la zone
     * @return les autres zones du domaine de la zone dont l'identifiant est passé en paramètre.
     */
    List<Zone> getZonesWithoutCycle(String zoneId);

    List<PlotDto> getPlots(Collection<String> plotIds);

    /**
     * Export selected effective plots id as excel sheet.
     * 
     * @param plotIds zone ids
     * @return stream
     */
    ExportResult exportPlotsAsXls(Collection<String> plotIds);

    void exportPlotsAsXlsAsync(Collection<String> plotIds);

    /**
     * return plots that target the given growingSystem
     * @param growingSystem The growingSystem targeted bay the plots
     * @return All plots targeted the given growingSystem.
     */
    List<Plot> getAllGrowingSystemPlot(GrowingSystem growingSystem);

}
