package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import fr.inra.agrosyst.api.services.AgrosystService;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Referentiel service.
 *
 * @author Eric Chatellier
 */
public interface ImportService extends AgrosystService {

    /**
     * Import des materiels tracteurs au format csv.
     *
     * @param contentStream content stream
     * @return import result (counts)
     */
    ImportResult importMaterielTracteursCSV(InputStream contentStream);

    /**
     * Import des materiels automoteurs au format csv.
     *
     * @param contentStream content stream
     * @return import result (counts)
     */
    ImportResult importMaterielAutomoteursCSV(InputStream contentStream);

    /**
     * Import des materiels outils au format csv.
     *
     * @param contentStream content stream
     * @return import result (counts)
     */
    ImportResult importMaterielOutilsCSV(InputStream contentStream);

    /**
     * Import des materiels migration au format csv.
     *
     * @param contentStream content stream
     * @return import result (counts)
     */
    ImportResult importMaterielIrrigationCSV(InputStream contentStream);

    /**
     * Import des communes
     *
     * @param communesAndPostCodesStream stream contenant le CSV des communes et dles ocdes postaux (source OSM)
     * @return import result (counts)
     */
    ImportResult importCommuneFranceCSV(InputStream communesAndPostCodesStream);

    ImportResult importCommuneEuropeCSV(FileInputStream stream);

    ImportResult importDepartmentShapes(InputStream stream);

    /**
     * Import des sols arvalis.
     *
     * @param solsStream    sols arvalis stream
     * @param regionsStream region name to region code stream
     * @return import result (counts)
     */
    ImportResult importSolArvalisCSV(InputStream solsStream, InputStream regionsStream);

    /**
     * Import des statuts juridiques.
     *
     * @param statusStream fichier des status
     * @return import result (counts)
     */
    ImportResult importLegalStatusCSV(InputStream statusStream);

    ImportResult importEspeces(InputStream stream);

    ImportResult importVarietesGeves(InputStream stream);

    ImportResult importVarietesPlantGrape(InputStream stream);

    ImportResult importClonesPlantGrape(InputStream stream);

    ImportResult importEspecesToVarietes(InputStream stream);

    /**
     * Import du référentiel OTEX au format csv.
     *
     * @param contentStream content stream
     * @return import result (counts)
     */
    ImportResult importOtexCSV(InputStream contentStream);

    /**
     * Import du référentiel
     *
     * @param contentStream content stream
     * @return import result (counts)
     */
    ImportResult importOrientationEdiCSV(InputStream contentStream);


    ImportResult importInterventionAgrosystTravailEdiCSV(InputStream contentStream);

    ImportResult importStadesEdiCSV(InputStream contentStream);

    /**
     * Import du référentiel sol texture geppa.
     *
     * @param stream flux de lecture
     * @return import result
     */
    ImportResult importSolTextureGeppa(InputStream stream);

    /**
     * Import du référentiel zonage parcelle edi.
     *
     * @param stream flux de lecture
     * @return import result
     */
    ImportResult importZonageParcelleEdi(InputStream stream);

    /**
     * Import du référentiel sol profondeur indigo.
     *
     * @param stream flux de lecture
     * @return import result
     */
    ImportResult importSolProfondeurIndigo(InputStream stream);

    /**
     * Import du référentiel sol caracteristiques indigo.
     *
     * @param stream flux de lecture
     * @return import result
     */
    ImportResult importSolCarateristiquesIndigo(InputStream stream);

    /**
     * Import du référentiel UnitesEDI
     *
     * @param stream flux de lecture
     * @return import result
     */
    ImportResult importUniteEDI(InputStream stream);

    /**
     * Import du référentiel FertiMinUNIFA
     *
     * @param contentStream flux de lecture
     * @return import result
     */
    ImportResult importFertiMinUNIFA(InputStream contentStream);

    /**
     * Import du référentiel Adventices.
     *
     * @param contentStream flux de lecture
     * @return import result
     */
    ImportResult importAdventices(InputStream contentStream);

    ImportResult importAgsAmortissement(InputStream contentStream);

    /**
     * Import du référentiel NuisiblesEDI.
     *
     * @param contentStream flux de lecture
     * @return import result
     */
    ImportResult importNuisiblesEDI(InputStream contentStream);

    /**
     * Import du référentiel ferti Orga.
     *
     * @param contentStream flux de lecture
     * @return import result
     */
    ImportResult importFertiOrga(InputStream contentStream);

    /**
     * Import du référentiel station meteo.
     *
     * @param contentStream contentStream flux de lecture
     * @return import result
     */
    ImportResult importStationMeteo(InputStream contentStream);

    ImportResult importGesCarburants(InputStream stream);

    ImportResult importGesEngrais(InputStream stream);

    ImportResult importGesPhyto(InputStream stream);

    ImportResult importGesSemences(InputStream stream);

    ImportResult importNrjCarburants(InputStream stream);

    ImportResult importNrjEngrais(InputStream stream);

    ImportResult importNrjPhyto(InputStream stream);

    ImportResult importNrjSemences(InputStream stream);

    ImportResult importNrjGesOutils(InputStream stream);

    ImportResult importMesure(InputStream stream);

    ImportResult importSupportOrganeEDI(InputStream stream);

    ImportResult importStadeNuisibleEDI(InputStream stream);

    ImportResult importTypeNotationEDI(InputStream stream);

    ImportResult importValeurQualitativeEDI(InputStream stream);

    ImportResult importUnitesQualifiantEDI(InputStream stream);

    ImportResult importActaTraitementsProduits(InputStream stream);

    ImportResult importActaTraitementsProduitsCateg(InputStream stream);

    ImportResult importActaSubstanceActive(InputStream stream);

    ImportResult importProtocoleVgObs(InputStream stream);

    ImportResult importElementVoisinage(InputStream stream);

    ImportResult importRcesoRulesGroundWater(InputStream stream);

    ImportResult importRcesoFuzzySetGroundWater(InputStream stream);

    ImportResult importRcesoCaseGroundWater(InputStream stream);

    ImportResult importPhytoSubstanceActiveIphy(InputStream stream);

    ImportResult importRcesuRunoffPotRulesParc(InputStream stream);

    ImportResult importTypeAgriculture(InputStream stream);

    ImportResult importActaDosageSpc(InputStream stream);

    ImportResult importActaGroupeCultures(InputStream stream);

    ImportResult importSaActaIphy(InputStream stream);

    ImportResult importTraitSdC(InputStream stream);

    ImportResult importCouvSolAnnuelle(InputStream stream);

    ImportResult importCultureEdiGroupeCouvSol(InputStream stream);

    ImportResult importCouvSolPerenne(InputStream stream);

    ImportResult importZoneClimatiqueIphy(InputStream stream);

    ImportResult importRefActaDosageSpcRoot(InputStream stream);

    ImportResult importRefActaDosageSaRoot(InputStream stream);

    ImportResult importRefActaProduitRoot(InputStream stream);

    ImportResult importRefEdaplosTraitementProduit(InputStream stream);

    /**
     * @param url     user define URL
     * @param apiKey  api authorisation key
     * @param klass   targeted imported class
     * @param apiUser user authentication
     * @param apiPwd  user password
     * @return csv import analyse result
     */
    InputStream getImportSolFromApiToCSVResult(String url, String apiKey, Class<?> klass, String apiUser, String apiPwd) throws AgrosystImportException;

    ImportResult importRefDestination(InputStream stream);

    ImportResult importRefQualityCriteria(InputStream stream);

    ImportResult importRefHarvestingPrice(InputStream stream);

    ImportResult importRefSpeciesToSector(InputStream stream);

    ImportResult importRefHarvestingPriceConverter(InputStream stream);

    ImportResult importRefStrategyLever(InputStream stream);

    ImportResult importRefAnimalType(InputStream stream);

    ImportResult importRefMarketingDestination(InputStream stream);

    ImportResult importRefInternventionTypeItemInputEdi(InputStream stream);

    ImportResult importRefQualityCriteriaClass(InputStream stream);

    ImportResult importRefPrixCarbuCSV(InputStream stream);

    ImportResult importRefPrixFertiMinCSV(InputStream stream);

    ImportResult importRefPrixFertiOrgaCSV(InputStream input);

    ImportResult importRefPrixPhytoCSV(InputStream input);

    ImportResult importRefPrixEspeceCSV(InputStream input);

    ImportResult importRefPrixIrrigCSV(InputStream stream);

    ImportResult importRefPrixAutreCSV(InputStream stream);

    ImportResult importRefInputUnitPriceUnitConverterCSV(InputStream stream);

    ImportResult importRefEspeceOtherToolsCSV(InputStream stream);

    ImportResult importRefCattleAnimalTypesCSV(InputStream stream, List<RefAnimalType> animalTypes);

    ImportResult importRefCattleRationAlimentsCSV(InputStream stream);

    ImportResult importRefCiblesAgrosystGroupesCiblesMAA(InputStream contentStream);

    ImportResult importRefMAABiocontrole(InputStream contentStream);

    ImportResult importRefMAADosesRefParGroupeCible(InputStream contentStream);

    ImportResult importRefGroupeCibleTraitement(InputStream contentStream);

    void importMAAReferentialsFromApiAsync(int campagne, Integer numeroAmmIdMetier);

    Map<Class<? extends ReferentialEntity>, ImportResult> importMAAReferentialsFromApi(int campagne, Integer numeroAmmIdMetier);

    void updateActaTraitementProduitsTraitement();

    void handleMAAResponseForImport(String jsonData,
                                    ImportResult refActaTraitementsProduitImportResult,
                                    ImportResult refMAADosesRefParGroupeCibleImportResult,
                                    ImportResult refMAADBioControleImportResult,
                                    int campagne,
                                    Map<String, Integer> idTraitementsByCode,
                                    Collection<RefGroupeCibleTraitement> refMAACodeTraitementToHistoricCodeTraitements,
                                    Collection<RefActaProduitRoot> refActaProduitRoots,
                                    RefCountry refCountry);

    ImportResult importRefFeedbackRouter(InputStream contentStream);

    ImportResult importRefSubstrate(InputStream contentStream);

    ImportResult importRefPrixSubstrate(InputStream contentStream);

    ImportResult importRefPot(InputStream contentStream);

    ImportResult importRefPrixPot(InputStream contentStream);

    ImportResult importCountriesCSV(InputStream stream);

    ImportResult importTraduction(Class<? extends ReferentialI18nEntry> klass, InputStream stream);

    ImportResult importRefOtherInputCSV(InputStream contentStream);

    ImportResult importRefSubstancesActivesCommissionEuropeenneCSV(InputStream contentStream);

    ImportResult importRefCompositionSubstancesActivesParNumeroAMM(InputStream content);

    ImportResult importRefPhrasesRisqueEtClassesMentionDangerParAMM(InputStream content);

    ImportResult importRefConversionUnitesQSA(InputStream content);

    ImportResult importRefCorrespondanceMaterielOutilsTS(InputStream content);

    ImportResult importRefSeedUnits(InputStream statusStream);
}
