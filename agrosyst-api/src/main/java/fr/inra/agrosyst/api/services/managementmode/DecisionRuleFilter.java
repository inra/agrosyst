package fr.inra.agrosyst.api.services.managementmode;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;

import java.io.Serial;

public class DecisionRuleFilter extends GrowingSystemFilter {

    /** serialVersionUID */
    @Serial
    private static final long serialVersionUID = -7557071227081313172L;

    protected String decisionRuleName;

    protected AgrosystInterventionType interventionType;

    public String getDecisionRuleName() {
        return decisionRuleName;
    }

    public void setDecisionRuleName(String decisionRuleName) {
        this.decisionRuleName = decisionRuleName;
    }

    public AgrosystInterventionType getInterventionType() {
        return interventionType;
    }

    public void setInterventionType(AgrosystInterventionType interventionType) {
        this.interventionType = interventionType;
    }
}
