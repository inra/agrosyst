package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoCaseGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoFuzzySetGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoRulesGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesuRunoffPotRulesParc;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class Referentials {

    public static final Function<String, String> NORMALISE = input -> {

        final String replace = input == null ? "" : input.toLowerCase();

        return Normalizer.normalize(replace, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .replaceAll("[^a-z0-9]", "");
    };

    public static final Function<RefLocation, String> GET_LOCATION_CODE_INSEE = RefLocation::getCodeInsee;

    public static final Function<RefSolArvalis, String> GET_SOL_ARVALIS_NATURAL_ID = input ->
            String.format("%s=%s", RefSolArvalis.PROPERTY_ID_TYPE_SOL, input.getId_type_sol());

    public static final Function<RefLegalStatus, String> GET_LEGAL_STATUS_NATURAL_ID = input ->
            String.format("%s=%d", RefLegalStatus.PROPERTY_CODE__INSEE, input.getCode_INSEE());

    public static final Function<RefSeedUnits, String> GET_REF_SEED_UNITS_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s",
                    RefSeedUnits.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefSeedUnits.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefSeedUnits.PROPERTY_SEED_PLANT_UNIT, input.getSeedPlantUnit());

    public static final Function<RefEspece, String> GET_ESPECE_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%s",
                    RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, input.getCode_type_saisonnier_AEE(),
                    RefEspece.PROPERTY_CODE_DESTINATION__AEE, input.getCode_destination_AEE()
            );

    public static final Function<RefVarieteGeves, String> GET_VARIETE_GEVES_NATURAL_ID = input ->
            String.format("%s=%d", RefVarieteGeves.PROPERTY_NUM__DOSSIER, input.getNum_Dossier());

    public static final Function<RefVarietePlantGrape, String> GET_VARIETE_PLANT_GRAPE_NATURAL_ID = input ->
            String.format("%s=%d", RefVarietePlantGrape.PROPERTY_CODE_VAR, input.getCodeVar());

    public static final Function<RefClonePlantGrape, String> GET_CLONE_PLANT_GRAPE_NATURAL_ID = input ->
            String.format("%s=%d", RefClonePlantGrape.PROPERTY_CODE_CLONE, input.getCodeClone());

    public static final Function<RefEspeceToVariete, String> GET_ESPECE_TO_VARIETE_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI, input.getCode_espece_edi(),
                    RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL, input.getCode_espece_autre_referentiel()
            );

    public static final Function<RefOTEX, String> GET_OTEX_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefOTEX.PROPERTY_CODE__OTEX_70_POSTES, input.getCode_OTEX_70_postes()
            );

    public static final Function<RefOrientationEDI, String> GET_ORIENTATION_EDI_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefOrientationEDI.PROPERTY_REFERENCE_CODE, input.getReference_code()
            );

    public static final Function<RefInterventionAgrosystTravailEDI, String> GET_INTERVENTION_AGROSYST_TRAVAIL_EDI_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, input.getReference_code()
            );

    public static final Function<RefStadeEDI, String> GET_STADE_EDI_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefStadeEDI.PROPERTY_AEE, input.getAee()
            );

    public static final Function<RefMateriel, String> GET_MATERIEL_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%s %s=%f",
                    RefMateriel.PROPERTY_TYPE_MATERIEL1, input.getTypeMateriel1(),
                    RefMateriel.PROPERTY_TYPE_MATERIEL2, input.getTypeMateriel2(),
                    RefMateriel.PROPERTY_TYPE_MATERIEL3, input.getTypeMateriel3(),
                    RefMateriel.PROPERTY_TYPE_MATERIEL4, input.getTypeMateriel4(),
                    RefMateriel.PROPERTY_UNITE_PAR_AN, input.getUniteParAn()
            );

    public static final Function<RefMaterielIrrigation, String> GET_MATERIEL_IRRIGATION_NATURAL_ID = GET_MATERIEL_NATURAL_ID::apply;

    public static final Function<RefMaterielTraction, String> GET_MATERIEL_TRACTION_NATURAL_ID = GET_MATERIEL_NATURAL_ID::apply;

    public static final Function<RefMaterielAutomoteur, String> GET_MATERIEL_AUTOMOTEUR_NATURAL_ID = GET_MATERIEL_NATURAL_ID::apply;

    public static final Function<RefMaterielOutil, String> GET_MATERIEL_OUTIL_NATURAL_ID = GET_MATERIEL_NATURAL_ID::apply;

    public static final Function<RefSolTextureGeppa, String> GET_SOL_TEXTURE_GEPPA_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefSolTextureGeppa.PROPERTY_ABBREVIATION_CLASSES_TEXTURALES__GEPAA, input.getAbbreviation_classes_texturales_GEPAA()
            );

    public static final Function<RefParcelleZonageEDI, String> GET_PARCELLE_ZONAGE_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefParcelleZonageEDI.PROPERTY_CODE_ENGAGEMENT_PARCELLE, input.getCode_engagement_parcelle()
            );


    public static final Function<RefSolProfondeurIndigo, String> GET_SOL_PROFONDEUR_INDIGO_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefSolProfondeurIndigo.PROPERTY_CLASSE_DE_PROFONDEUR__INDIGO, input.getClasse_de_profondeur_INDIGO()
            );


    public static final Function<RefSolCaracteristiqueIndigo, String> GET_SOL_CARACTERISTIQUES_INDIGO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefSolCaracteristiqueIndigo.PROPERTY_ABBREVIATION__INDIGO, input.getAbbreviation_INDIGO(),
                    RefSolCaracteristiqueIndigo.PROPERTY_CLASSE_DE_PROFONDEUR__INDIGO, input.getClasse_de_profondeur_INDIGO()
            );


    public static final Function<RefUniteEDI, String> GET_UNITES_EDI_NATURAL_ID = input ->
            String.format(
                    "%s=%d",
                    RefUniteEDI.PROPERTY_REFERENCE_ID, input.getReference_id()
            );

    public static final Function<RefFertiMinUNIFA, String> GET_FERTI_MIN_UNIFA_NATURAL_ID_WITH_NULL = input -> {
        String result = String.format("%s=%d %s=%s", RefFertiMinUNIFA.PROPERTY_CATEG, input.getCateg(), RefFertiMinUNIFA.PROPERTY_FORME, input.getForme());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_N, input.getN());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_P2_O5, input.getP2O5());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_K2_O, input.getK2O());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_BORE, input.getBore());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_FER, input.getFer());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_CALCIUM, input.getCalcium());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_MANGANESE, input.getManganese());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_MOLYBDENE, input.getMolybdene());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_MG_O, input.getMgO());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, input.getOxyde_de_sodium());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_S_O3, input.getsO3());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_CUIVRE, input.getCuivre());
        result += String.format(" %s=%f", RefFertiMinUNIFA.PROPERTY_ZINC, input.getZinc());
        return result;
    };

    public static final Function<RefAdventice, String> GET_ADVENTICES_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefAdventice.PROPERTY_IDENTIFIANT, input.getIdentifiant()
            );

    public static final Function<RefAgsAmortissement, String> GET_AGS_AMORTISSEMENT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%f %s=%f",
                    RefAgsAmortissement.PROPERTY_TYPE__MATERIEL, input.getType_Materiel(),
                    RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT1, input.getDonneesAmortissement1(),
                    RefAgsAmortissement.PROPERTY_DONNEES_AMORTISSEMENT2, input.getDonneesAmortissement2()
            );

    public static final Function<RefNuisibleEDI, String> GET_NUISIBLES_EDI_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefNuisibleEDI.PROPERTY_REFERENCE_ID, input.getReference_id()
            );

    public static final Function<RefFertiOrga, String> GET_FERTI_ORGA_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefFertiOrga.PROPERTY_IDTYPEEFFLUENT, input.getIdtypeeffluent()
            );

    public static final Function<RefStationMeteo, String> GET_STATION_METEO_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefStationMeteo.PROPERTY_AFFECTATION, input.getAffectation()
            );

    public static final Function<RefGesCarburant, String> GET_GES_CARBURANTS_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefGesCarburant.PROPERTY_LIBELLE_CARBURANT, input.getLibelle_carburant()
            );

    public static final Function<RefGesEngrais, String> GET_GES_ENGRAIS_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefGesEngrais.PROPERTY_TYPE_ENGRAIS, input.getType_engrais()
            );

    public static final Function<RefGesPhyto, String> GET_GES_PHYTO_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefGesPhyto.PROPERTY_INTITULE, input.getIntitule()
            );

    public static final Function<RefGesSemence, String> GET_GES_SEMENCES_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefGesSemence.PROPERTY_CODE_CULTURE, input.getCode_culture()
            );


    public static final Function<RefNrjCarburant, String> GET_NRJ_CARBURANTS_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefNrjCarburant.PROPERTY_LIBELLE_CARBURANT, input.getLibelle_carburant()
            );

    public static final Function<RefNrjEngrais, String> GET_NRJ_ENGRAIS_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefNrjEngrais.PROPERTY_TYPE_ENGRAIS, input.getType_engrais()
            );


    public static final Function<RefNrjPhyto, String> GET_NRJ_PHYTO_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefNrjPhyto.PROPERTY_INTITULE, input.getIntitule()
            );


    public static final Function<RefNrjSemence, String> GET_NRJ_SEMENCES_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefNrjSemence.PROPERTY_CODE_CULTURE, input.getCode_culture()
            );


    public static final Function<RefNrjGesOutil, String> GET_NRJ_GES_OUTILS_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefNrjGesOutil.PROPERTY_CODE_TYPE_MATERIEL, input.getCode_type_materiel()
            );

    public static final Function<RefMesure, String> GET_MESURE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s",
            RefMesure.PROPERTY_CATEGORIE_DE_MESURE, input.getCategorie_de_mesure(),
            RefMesure.PROPERTY_TYPE_VARIABLE_MESUREE, input.getType_variable_mesuree(),
            RefMesure.PROPERTY_VARIABLE_MESUREE, input.getVariable_mesuree());

    public static final Function<RefSupportOrganeEDI, String> GET_SUPPORT_ORGANE_EDI_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefSupportOrganeEDI.PROPERTY_REFERENCE_ID, input.getReference_id());

    public static final Function<RefStadeNuisibleEDI, String> GET_STADE_NUISIBLE_EDI_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefStadeNuisibleEDI.PROPERTY_REFERENCE_ID, input.getReference_id());

    public static final Function<RefTypeNotationEDI, String> GET_TYPE_NOTATION_EDI_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefTypeNotationEDI.PROPERTY_REFERENCE_ID, input.getReference_id());

    public static final Function<RefValeurQualitativeEDI, String> GET_VALEUR_QUALITATIVE_EDI_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefValeurQualitativeEDI.PROPERTY_REFERENCE_ID, input.getReference_id());

    public static final Function<RefUnitesQualifiantEDI, String> GET_UNITES_QUALIFIANT_EDI_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefUnitesQualifiantEDI.PROPERTY_REFERENCE_ID, input.getReference_id());

    public static final Function<RefActaTraitementsProduit, String> GET_ACTA_TRAITEMENTS_PRODUITS_NATURAL_ID = input -> String.format(
            "%s=%s %s=%d %s=%s",
            RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, input.getId_produit(),
            RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, input.getId_traitement(),
            RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, input.getRefCountry()
    );

    public static final Function<RefActaTraitementsProduitsCateg, String> GET_ACTA_TRAITEMENTS_PRODUITS_CATEG_NATURAL_ID = input -> String.format(
            "%s=%d",
            RefActaTraitementsProduitsCateg.PROPERTY_ID_TRAITEMENT, input.getId_traitement()
    );

    public static final Function<RefActaSubstanceActive, String> GET_ACTA_SUBSTANCE_ACTIVE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefActaSubstanceActive.PROPERTY_ID_PRODUIT, input.getId_produit(),
            RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA, NORMALISE.apply(input.getNom_commun_sa())
    );

    public static final Function<RefMAADosesRefParGroupeCible, String> GET_MAA_DOSES_REF_PAR_GROUPE_CIBLE_NATURAL_ID =
            input -> String.format(
                    "%s=%s %s=%s %s=%s %s=%s %s=%s",
                    RefMAADosesRefParGroupeCible.PROPERTY_CAMPAGNE, input.getCampagne(),
                    RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM, input.getCode_amm(),
                    RefMAADosesRefParGroupeCible.PROPERTY_CODE_CULTURE_MAA, input.getCode_culture_maa(),
                    RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA, input.getCode_traitement_maa(),
                    RefMAADosesRefParGroupeCible.PROPERTY_CODE_GROUPE_CIBLE_MAA, input.getCode_groupe_cible_maa()
            );

    public static final Function<RefMAABiocontrole, String> GET_MAA_BIOCONTROLE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefMAABiocontrole.PROPERTY_CODE_AMM, input.getCode_amm(),
            RefMAABiocontrole.PROPERTY_CAMPAGNE, input.getCampagne()
    );

    public static final Function<RefProtocoleVgObs, String> GET_PROTOCOLE_VG_OBS_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%s %s=%s",
            RefProtocoleVgObs.PROPERTY_PROTOCOLE_CODE_PROTOCOLE, input.getProtocole_code_protocole(),
            RefProtocoleVgObs.PROPERTY_LIGNE_CODE_LIGNE_PROTOCOLE, input.getLigne_code_ligne_protocole(),
            RefProtocoleVgObs.PROPERTY_RELEVE_NO_RELEVE, input.getReleve_no_releve(),
            RefProtocoleVgObs.PROPERTY_RELEVE_QUALIFIANT_UNITE_MESURE_CODE, input.getReleve_qualifiant_unite_mesure_code(),
            RefProtocoleVgObs.PROPERTY_CLASSE_NO_CLASSE, input.getClasse_no_classe()
    );

    public static final Function<RefElementVoisinage, String> GET_ELEMENT_VOISINAGE_NATURAL_ID = input -> String.format(
            "%s=%d",
            RefElementVoisinage.PROPERTY_ID_REF_INFRASTRUCTURE, input.getId_ref_infrastructure()
    );

    public static final Function<RefRcesoRulesGroundWater, String> GET_RCESO_RULES_GROUND_WATER_NATURAL_ID = input -> String.format(
            "%s=%d %s=%d %s=%s %s=%s %s=%s %s=%s",
            RefRcesoRulesGroundWater.PROPERTY_CASE_NUMBER, input.getCaseNumber(),
            RefRcesoRulesGroundWater.PROPERTY_RULE_NUMBER, input.getRuleNumber(),
            RefRcesoRulesGroundWater.PROPERTY_FSET_ORGC, input.getFset_orgc(),
            RefRcesoRulesGroundWater.PROPERTY_FSET_DEPTH, input.getFset_depth(),
            RefRcesoRulesGroundWater.PROPERTY_FSET_KOC, input.getFset_koc(),
            RefRcesoRulesGroundWater.PROPERTY_FSET_DT50, input.getFset_dt50()
    );

    public static final Function<RefRcesoFuzzySetGroundWater, String> GET_RCESO_FUZZYSET_GROUND_WATER_NATURAL_ID = input -> String.format(
            "%s=%d %s=%s %s=%s",
            RefRcesoFuzzySetGroundWater.PROPERTY_CASE_NUMBER, input.getCaseNumber(),
            RefRcesoFuzzySetGroundWater.PROPERTY_VARIABLES, input.getVariables(),
            RefRcesoFuzzySetGroundWater.PROPERTY_FUZZY_SET, input.getFuzzySet()
    );

    public static final Function<RefRcesoCaseGroundWater, String> GET_RCESO_CASE_GROUND_WATER_NATURAL_ID = input -> String.format(
            "%s=%d %s=%s %s=%s %s=%s",
            RefRcesoCaseGroundWater.PROPERTY_CLIMATE, input.getClimate(),
            RefRcesoCaseGroundWater.PROPERTY_APPLICATION_PERIOD, input.getApplicationPeriod(),
            RefRcesoCaseGroundWater.PROPERTY_SURFACE_TEXTURE, input.getSurface_texture(),
            RefRcesoCaseGroundWater.PROPERTY_SUBSOIL_TEXTURE, input.getSubsoil_texture()
    );

    public static final Function<RefRcesuRunoffPotRulesParc, String> GET_RCESU_RUNOFF_POT_RULES_PARC_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%s",
            RefRcesuRunoffPotRulesParc.PROPERTY_SOIL_TEXTURE, input.getSoil_texture(),
            RefRcesuRunoffPotRulesParc.PROPERTY_SLOPE, input.getSlope().name(),
            RefRcesuRunoffPotRulesParc.PROPERTY_BATTANCE, input.isBattance(),
            RefRcesuRunoffPotRulesParc.PROPERTY_HYDROMORPHISMS, input.isHydromorphisms()
    );

    public static final Function<RefPhytoSubstanceActiveIphy, String> GET_PHYTO_SUBSTANCE_ACTIVE_IPHY_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefPhytoSubstanceActiveIphy.PROPERTY_NOM_SA, NORMALISE.apply(input.getNom_sa())
    );

    public static final Function<RefTypeAgriculture, String> GET_TYPE_AGRICULTURE_NATURAL_ID = input -> String.format(
            "%s=%d",
            RefTypeAgriculture.PROPERTY_REFERENCE_ID, input.getReference_id()
    );

    public static final Function<RefActaDosageSPC, String> GET_ACTA_DOSAGE_SPC_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%e %s=%s",
            RefActaDosageSPC.PROPERTY_ID_PRODUIT, input.getId_produit(),
            RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, input.getId_traitement(),
            RefActaDosageSPC.PROPERTY_ID_CULTURE, input.getId_culture(),
            RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR, input.getDosage_spc_valeur(),
            RefActaDosageSPC.PROPERTY_DOSAGE_SPC_COMMENTAIRE, input.getDosage_spc_commentaire()
    );

    public static final Function<RefActaGroupeCultures, String> GET_ACTA_GROUPE_CULTURES_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefActaGroupeCultures.PROPERTY_ID_CULTURE, input.getId_culture(),
            RefActaGroupeCultures.PROPERTY_ID_GROUPE_CULTURE, input.getId_groupe_culture()
    );

    public static final Function<RefSaActaIphy, String> GET_SA_ACTA_IPHY_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefSaActaIphy.PROPERTY_NOM_SA_ACTA, NORMALISE.apply(input.getNom_sa_acta())
    );

    public static final Function<RefTraitSdC, String> GET_TRAIT_SDC_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefTraitSdC.PROPERTY_ID_TRAIT, input.getId_trait()
    );

    public static final Function<RefCultureEdiGroupeCouvSol, String> GET_CULTURE_EDI_GROUP_COUV_SOL_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%s",
            RefCultureEdiGroupeCouvSol.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
            RefCultureEdiGroupeCouvSol.PROPERTY_CODE_QUALIFIANT_AEE, input.getCode_qualifiant_aee(),
            RefCultureEdiGroupeCouvSol.PROPERTY_CODE_TYPE_SAISONNIER_AEE, input.getCode_type_saisonnier_aee(),
            RefCultureEdiGroupeCouvSol.PROPERTY_CODE_DESTINATION_AEE, input.getCode_destination_aee()
    );

    public static final Function<RefCouvSolAnnuelle, String> GET_COUV_SOL_ANNUELLE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%s",
            RefCouvSolAnnuelle.PROPERTY_VITESSE_COUV, input.getVitesseCouv(),
            RefCouvSolAnnuelle.PROPERTY_PERIODE_SEMIS, input.getPeriodeSemis(),
            RefCouvSolAnnuelle.PROPERTY_DEBUT_INTER, input.getDebut_inter(),
            RefCouvSolAnnuelle.PROPERTY_FIN_INTER, input.getFin_inter()
    );

    public static final Function<RefCouvSolPerenne, String> GET_COUV_SOL_PERENNE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%s",
            RefCouvSolPerenne.PROPERTY_VITESSE_COUV, input.getVitesseCouv(),
            RefCouvSolPerenne.PROPERTY_PHASE, input.getPhase(),
            RefCouvSolPerenne.PROPERTY_DEBUT_INTER, input.getDebut_inter(),
            RefCouvSolPerenne.PROPERTY_FIN_INTER, input.getFin_inter()
    );

    public static final Function<RefDepartmentShape, String> GET_DEPARTMENT_SHAPE_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefDepartmentShape.PROPERTY_DEPARTMENT, input.getDepartment()
    );

    public static final Function<RefZoneClimatiqueIphy, String> GET_ZONE_CLIMATIQUE_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefZoneClimatiqueIphy.PROPERTY_DEPARTEMENT, input.getDepartement()
    );

    public static final Function<RefActaDosageSpcRoot, String> GET_REF_ACTA_DOSAGE_SPC_NATURAL_ID = input -> String.format(
            "%s=%s %s=%d %s=%d %s=%s %s=%s",
            RefActaDosageSpcRoot.PROPERTY_ID_PRODUIT, input.getIdProduit(),
            RefActaDosageSpcRoot.PROPERTY_ID_TRAITEMENT, input.getIdTraitement(),
            RefActaDosageSpcRoot.PROPERTY_ID_CULTURE, input.getId_culture(),
            RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_VALEUR, input.getDosage_spc_valeur(),
            RefActaDosageSpcRoot.PROPERTY_DOSAGE_SPC_COMMENTAIRE, input.getDosage_spc_commentaire()
    );

    public static final Function<RefActaDosageSaRoot, String> GET_REF_ACTA_DOSAGE_SA_NATURAL_ID = input -> String.format(
            "%s=%s %s=%d %s=%d %s=%s %s=%s",
            RefActaDosageSaRoot.PROPERTY_ID_PRODUIT, input.getIdProduit(),
            RefActaDosageSaRoot.PROPERTY_ID_TRAITEMENT, input.getIdTraitement(),
            RefActaDosageSaRoot.PROPERTY_ID_CULTURE, input.getId_culture(),
            RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_VALEUR, input.getDosage_sa_valeur(),
            RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_COMMENTAIRE, input.getDosage_sa_commentaire()
    );

    public static final Function<RefActaProduitRoot, String> GET_REF_ACTA_PRODUIT_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s",
            RefActaProduitRoot.PROPERTY_ID_PRODUIT, input.getIdProduit(),
            RefActaProduitRoot.PROPERTY_AMM, input.getAmm(),
            RefActaProduitRoot.PROPERTY_REF_COUNTRY, input.getRefCountry()
    );

    public static final Function<RefEdaplosTypeTraitement, String> GET_REF_EDAPLOS_TYPE_TRAITEMENT_NATURAL_ID =
            input -> String.format(
                    "%s=%s %s=%s",
                    RefEdaplosTypeTraitement.PROPERTY_REFERENCE_CODE, input.getReferenceCode(),
                    RefEdaplosTypeTraitement.PROPERTY_CODE_TRAITEMENT, input.getCodeTraitement()
            );

    public static final Function<RefDestination, String> GET_DESTINATION_NATURAL_ID =
            input -> String.format(
                    "%s=%s",
                    RefDestination.PROPERTY_CODE_DESTINATION__A, input.getCode_destination_A()
            );

    public static final Function<RefQualityCriteria, String> GET_QUALITYCRITERIA_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefQualityCriteria.PROPERTY_CODE, input.getCode()
            );

    public static final Function<RefHarvestingPrice, String> GET_REF_HARVESTING_PRICE_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%b %s=%s %s=%d %s=%s %s=%d",
                    RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, input.getCode_destination_A(),
                    RefHarvestingPrice.PROPERTY_ORGANIC, input.isOrganic(),
                    RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, input.getMarketingPeriod(),
                    RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE, input.getMarketingPeriodDecade(),
                    RefHarvestingPrice.PROPERTY_PRICE_UNIT, input.getPriceUnit(),
                    RefHarvestingPrice.PROPERTY_CAMPAIGN, input.getCampaign()
            );

    public static final Function<RefHarvestingPrice, String> GET_REF_HARVESTING_PRICE_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%b %s=%s %s=%d %s=%s %s=%s ",
                    RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, input.getCode_destination_A(),
                    RefHarvestingPrice.PROPERTY_ORGANIC, input.isOrganic(),
                    RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, input.getMarketingPeriod(),
                    RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE, input.getMarketingPeriodDecade(),
                    RefHarvestingPrice.PROPERTY_PRICE_UNIT, input.getPriceUnit(),
                    RefHarvestingPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario()
            );

    public static final Function<RefHarvestingPriceConverter, String> GET_REF_HARVESTING_PRICE_CONVERTER_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s",
                    RefHarvestingPriceConverter.PROPERTY_CODE_DESTINATION__A, input.getCode_destination_A(),
                    RefHarvestingPriceConverter.PROPERTY_YEALD_UNIT, input.getYealdUnit(),
                    RefHarvestingPriceConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefSpeciesToSector, String> GET_REF_SPECIES_TO_SECTOR_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s",
                    RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefSpeciesToSector.PROPERTY_SECTOR, input.getSector()
            );

    public static final Function<RefStrategyLever, String> GET_REF_STRATEGIE_LEVER_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefStrategyLever.PROPERTY_CODE, input.getCode()
            );

    public static final Function<RefAnimalType, String> GET_REF_ANIMAL_TYPE_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefAnimalType.PROPERTY_ANIMAL_TYPE, input.getAnimalType(),
                    RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS, input.getAnimalPopulationUnits()
            );

    public static final Function<RefMarketingDestination, String> GET_REF_MARKETING_DESTINATION_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefMarketingDestination.PROPERTY_SECTOR, input.getSector(),
                    RefMarketingDestination.PROPERTY_MARKETING_DESTINATION, input.getMarketingDestination()
            );

    public static final Function<RefInterventionTypeItemInputEDI, String> GET_REF_INTERVENTION_TYPE_ITEM_INPUT_EDI_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefInterventionTypeItemInputEDI.PROPERTY_INPUT_EDI_LABEL, input.getInputEdiTypeCode()
            );

    public static final Function<RefQualityCriteriaClass, String> GET_REF_QUALITY_CRITERIA_CLASS_NATURAL_ID = input ->
            String.format(
                    "%s=%s",
                    RefQualityCriteriaClass.PROPERTY_CODE, input.getCode()
            );

    public static final Function<RefPrixCarbu, String> GET_REF_PRIX_CARBU_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%d %s=%s",
                    RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixCarbu, String> GET_REF_PRIX_CARBU_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixEspece, String> GET_REF_PRIX_ESPECE_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%d %s=%b %s=%s %s=%b",
                    RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefPrixEspece.PROPERTY_SEED_TYPE, input.getSeedType(),
                    RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
                    RefPrixEspece.PROPERTY_TREATMENT, input.isTreatment(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit(),
                    RefPrixEspece.PROPERTY_ORGANIC, input.isOrganic()
            );

    public static final Function<RefPrixEspece, String> GET_REF_PRIX_ESPECE_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%s %s=%b %s=%s %s=%b",
                    RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefPrixEspece.PROPERTY_SEED_TYPE, input.getSeedType(),
                    RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
                    RefPrixEspece.PROPERTY_TREATMENT, input.isTreatment(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit(),
                    RefPrixEspece.PROPERTY_ORGANIC, input.isOrganic()
            );

    public static final Function<RefPrixFertiMin, String> GET_REF_PRIX_FERTI_MIN_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%d %s=%s",
                    RefPrixFertiMin.PROPERTY_CATEG, input.getCateg(),
                    RefPrixFertiMin.PROPERTY_FORME, input.getForme(),
                    RefPrixFertiMin.PROPERTY_ELEMENT, input.getElement(),
                    RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixFertiMin, String> GET_REF_PRIX_FERTI_MIN_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%s %s=%s",
                    RefPrixFertiMin.PROPERTY_CATEG, input.getCateg(),
                    RefPrixFertiMin.PROPERTY_FORME, input.getForme(),
                    RefPrixFertiMin.PROPERTY_ELEMENT, input.getElement(),
                    RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixFertiOrga, String> GET_REF_PRIX_FERTI_ORGA_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%d %s=%b %s=%s",
                    RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT, input.getIdTypeEffluent(),
                    RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
                    RefPrixFertiOrga.PROPERTY_ORGANIC, input.isOrganic(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixFertiOrga, String> GET_REF_PRIX_FERTI_ORGA_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%b %s=%s",
                    RefPrixFertiOrga.PROPERTY_ID_TYPE_EFFLUENT, input.getIdTypeEffluent(),
                    RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
                    RefPrixFertiOrga.PROPERTY_ORGANIC, input.isOrganic(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixPhyto, String> GET_REF_PRIX_PHYTO_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%d %s=%d %s=%s",
                    RefPrixPhyto.PROPERTY_ID_PRODUIT, input.getId_produit(),
                    RefPrixPhyto.PROPERTY_ID_TRAITEMENT, input.getId_traitement(),
                    RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixPhyto, String> GET_REF_PRIX_PHYTO_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%d %s=%s %s=%s",
                    RefPrixPhyto.PROPERTY_ID_PRODUIT, input.getId_produit(),
                    RefPrixPhyto.PROPERTY_ID_TRAITEMENT, input.getId_traitement(),
                    RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit());

    public static final Function<RefPrixIrrig, String> GET_REF_PRIX_IRRIG_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%d %s=%s",
                    RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixIrrig, String> GET_REF_PRIX_IRRIG_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixAutre, String> GET_REF_PRIX_AUTRE_CAMPAIGN_NATURAL_ID = input ->
            String.format(
                    "%s=%d %s=%s",
                    RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit()
            );

    public static final Function<RefPrixAutre, String> GET_REF_PRIX_AUTRE_SCENARIO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
                    RefInputPrice.PROPERTY_UNIT, input.getUnit());

    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%s %s=%s %s=%s %s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_PHYTO_PRODUCT_UNIT, input.getPhytoProductUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_CAPACITY_UNIT, input.getCapacityUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_MINERAL_PRODUCT_UNIT, input.getMineralProductUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_ORGANIC_PRODUCT_UNIT, input.getOrganicProductUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_SEED_PLANT_UNIT, input.getSeedPlantUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_POT_INPUT_UNIT, input.getPotInputUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_SUBSTRATE_INPUT_UNIT, input.getSubstrateInputUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_CAPACITY_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_CAPACITY_UNIT, input.getCapacityUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_MINERAL_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_MINERAL_PRODUCT_UNIT, input.getMineralProductUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_ORGANIC_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_ORGANIC_PRODUCT_UNIT, input.getOrganicProductUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );


    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_SEED_PLAN_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_SEED_PLANT_UNIT, input.getSeedPlantUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_PHYTO_PRODUCT_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_PHYTO_PRODUCT_UNIT, input.getPhytoProductUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_POT_INPUT_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_POT_INPUT_UNIT, input.getPotInputUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefInputUnitPriceUnitConverter, String> GET_REF_INPUT_UNIT_PRICE_UNIT_SUBSTRATE_INPUT_UNIT_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s",
                    RefInputUnitPriceUnitConverter.PROPERTY_SUBSTRATE_INPUT_UNIT, input.getSubstrateInputUnit(),
                    RefInputUnitPriceUnitConverter.PROPERTY_PRICE_UNIT, input.getPriceUnit()
            );

    public static final Function<RefEspeceOtherTools, String> GET_REF_ESPECE_OTHERS_TOOLS_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s",
                    RefEspeceOtherTools.PROPERTY_CODE_ESPECE_BOTANIQUE_OTHER, input.getCodeEspeceBotaniqueOther(),
                    RefEspeceOtherTools.PROPERTY_CODE_QUALIFIANT_AEEOTHER, input.getCodeQualifiantAEEOther(),
                    RefEspeceOtherTools.PROPERTY_CODE_TYPE_SAISONNIER_AEEOTHER, input.getCodeTypeSaisonnierAEEOther()
            );

    public static final Function<RefCattleAnimalType, String> GET_CATTLE_ANIMAL_TYPE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefCattleAnimalType.PROPERTY_REF_ANIMAL_TYPE, input.getAnimalType(),
            RefCattleAnimalType.PROPERTY_ANIMAL_TYPE, input.getRefAnimalType().getAnimalType());

    public static final Function<RefCattleRationAliment, String> GET_CATTLE_RATION_ALIMENT_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefCattleRationAliment.PROPERTY_ALIMENT_TYPE, input.getAlimentType(),
            RefCattleRationAliment.PROPERTY_ALIMENT_UNIT, input.getAlimentUnit()
    );

    public static final Function<RefCiblesAgrosystGroupesCiblesMAA, String> GET_CIBLES_AGROSYST_GROUPES_CIBLE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID, input.getCible_edi_ref_id(),
            RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA, input.getCode_groupe_cible_maa()
    );

    public static final Function<RefGroupeCibleTraitement, String> GET_GROUPE_CIBLE_TRAITEMENT_NATURAL_ID = input -> String.format(
            "%s=%d %s=%s",
            RefGroupeCibleTraitement.PROPERTY_ID_TRAITEMENT, input.getId_traitement(),
            RefGroupeCibleTraitement.PROPERTY_CODE_GROUPE_CIBLE_MAA, Strings.nullToEmpty(input.getCode_groupe_cible_maa())
    );

    public static final Function<RefFeedbackRouter, String> GET_FEEDBACK_ROUTER_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%b",
            RefFeedbackRouter.PROPERTY_FEEDBACK_CATEGORY, input.getFeedbackCategory(),
            RefFeedbackRouter.PROPERTY_SECTOR, input.getSector(),
            RefFeedbackRouter.PROPERTY_TYPE_DEPHY, input.getTypeDEPHY(),
            RefFeedbackRouter.PROPERTY_ACTIVE, input.isActive()
    );

    public static final Function<RefSubstrate, String> GET_REF_SUBSTRATE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefSubstrate.PROPERTY_CARACTERISTIC1, input.getCaracteristic1(),
            RefSubstrate.PROPERTY_CARACTERISTIC2, input.getCaracteristic2()
    );

    public static final Function<RefPrixSubstrate, String> GET_REF_PRIX_SUBSTRATE_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%d %s=%s %s=%s",
            RefPrixSubstrate.PROPERTY_CARACTERISTIC1, input.getCaracteristic1(),
            RefPrixSubstrate.PROPERTY_CARACTERISTIC2, input.getCaracteristic2(),
            RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
            RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
            RefInputPrice.PROPERTY_UNIT, input.getUnit()
    );

    public static final Function<RefPot, String> GET_REF_POT_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefPot.PROPERTY_CARACTERISTIC1, input.getCaracteristic1()
    );

    public static final Function<RefPrixPot, String> GET_REF_PRIX_POT_NATURAL_ID = input -> String.format(
            "%s=%s %s=%d %s=%s %s=%s",
            RefPrixPot.PROPERTY_CARACTERISTIC1, input.getCaracteristic1(),
            RefInputPrice.PROPERTY_CAMPAIGN, input.getCampaign(),
            RefInputPrice.PROPERTY_CODE_SCENARIO, input.getCode_scenario(),
            RefInputPrice.PROPERTY_UNIT, input.getUnit()
    );

    public static final Function<RefCountry, String> GET_REF_COUNTRY_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefCountry.PROPERTY_TRIGRAM, input.getTrigram()
    );

    public static final Function<RefOtherInput, String> GET_REF_OTHER_INPUT_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s %s=%s %s=%s",
            RefOtherInput.PROPERTY_INPUT_TYPE_C0, input.getInputType_c0(),
            RefOtherInput.PROPERTY_CARACTERISTIC1, Strings.nullToEmpty(input.getCaracteristic1()),
            RefOtherInput.PROPERTY_CARACTERISTIC2, Strings.nullToEmpty(input.getCaracteristic2()),
            RefOtherInput.PROPERTY_CARACTERISTIC3, Strings.nullToEmpty(input.getCaracteristic3())
    );

    public static final Function<ReferentialI18nEntry, String> GET_REF_I18N_ENTRY_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            ReferentialI18nEntry.PROPERTY_LANG, input.getLang(),
            ReferentialI18nEntry.PROPERTY_TRADKEY, input.getTradkey()
    );

    public static final Function<RefSubstancesActivesCommissionEuropeenne, String> GET_REF_SUBSTANCES_ACTIVES_COMMISSION_EUROPEENNE_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefSubstancesActivesCommissionEuropeenne.PROPERTY_ID_SA, input.getId_sa()
    );

    public static final Function<RefCompositionSubstancesActivesParNumeroAMM, String> GET_REF_COMPOSITION_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM, input.getNumero_AMM(),
            RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ID_SA, input.getId_sa()
    );

    public static final Function<RefPhrasesRisqueEtClassesMentionDangerParAMM, String> GET_REF_PHRASES_RISQUES_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE__AMM, input.getCode_AMM(),
            RefPhrasesRisqueEtClassesMentionDangerParAMM.PROPERTY_CODE_INFO, input.getCode_info()

    );

    public static final Function<RefConversionUnitesQSA, String> GET_REF_CONVERSION_UNITES_QSA_NATURAL_ID = input -> String.format(
            "%s=%s %s=%s",
            RefConversionUnitesQSA.PROPERTY_UNITE_SA, input.getUnite_sa(),
            RefConversionUnitesQSA.PROPERTY_UNITE_SAISIE, input.getUnite_saisie()
    );

    public static final Function<RefCorrespondanceMaterielOutilsTS, String> GET_REF_CORRESPONDANCE_MATERIEL_OUTILS_TS_NATURAL_ID = input -> String.format(
            "%s=%s",
            RefCorrespondanceMaterielOutilsTS.PROPERTY_TYPE_MATERIEL_1, input.getType_materiel_1()
    );

    public static final BioAgressorParentType ADVENTICE = new BioAgressorParentType(BioAgressorType.ADVENTICE, BioAgressorType.PLANTE_PARASITE, BioAgressorType.PLANTES_ENVAHISSANTES_ORIGINE_EXOTIQUE);
    public static final BioAgressorParentType GENERIQUE = new BioAgressorParentType(BioAgressorType.GENERIQUE);
    public static final BioAgressorParentType MALADIE = new BioAgressorParentType(BioAgressorType.MALADIE, BioAgressorType.MALADIE_PHYSIOLOGIQUE, BioAgressorType.VIRUS);
    public static final BioAgressorParentType RAVAGEUR = new BioAgressorParentType(BioAgressorType.RAVAGEUR);
    public static final BioAgressorParentType ACTION_SUR_LE_METABOLISME = new BioAgressorParentType(BioAgressorType.ACTION_SUR_LE_METABOLISME);
    public static final BioAgressorParentType AUXILIAIRE_BIOLOGIQUE = new BioAgressorParentType(BioAgressorType.AUXILIAIRE_BIOLOGIQUE);

    public static List<BioAgressorParentType> getTreatmentTargetBioAgressorParentTypes() {
        return Arrays.asList(ADVENTICE, GENERIQUE, MALADIE, RAVAGEUR);
    }

    public static List<BioAgressorParentType> getAllBioAgressorParentTypes() {
        return Arrays.asList(ACTION_SUR_LE_METABOLISME, ADVENTICE, AUXILIAIRE_BIOLOGIQUE, GENERIQUE, MALADIE, RAVAGEUR);
    }
}
