package fr.inra.agrosyst.api.services.security;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.PlotDto;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.performance.PerformanceDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemDto;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 * @since 0.8
 */
public interface AnonymizeService extends AgrosystService {

    String anonymize(String clear);

    Domain checkForDomainAnonymization(Domain domain);

    Collection<Domain> checkForDomainsAnonymization(Collection<Domain> domains);

    PaginationResult<Domain> checkForDomainsAnonymization(PaginationResult<Domain> domains);

    Function<Domain, DomainDto> getDomainToDtoFunction(boolean includeResponsibles);

    Map<String,String> getDomainsAsMap(Iterable<Domain> domains);

    List<GeoPoint> checkForGeoPointAnonymization(List<GeoPoint> result);

    Plot checkForPlotAnonymization(Plot plot);

    List<Plot> checkForPlotsAnonymization(List<Plot> plots);

    Function<Plot, PlotDto> getPlotToDtoFunction();

    Zone checkForZoneAnonymization(Zone zone);

    Function<Zone, ZoneDto> getZoneToDtoFunction(boolean includePlot, boolean allowUnreadable);

    GrowingPlan checkForGrowingPlanAnonymization(GrowingPlan growingPlan);

    Function<GrowingPlan, GrowingPlanDto> getGrowingPlanToDtoFunction(boolean includeResponsibles);

    Map<String,String> getGrowingPlansAsMap(Iterable<GrowingPlan> growingPlans);

    GrowingSystem checkForGrowingSystemAnonymization(GrowingSystem growingSystem);

    Function<GrowingSystem, GrowingSystemDto> getGrowingSystemToDtoFunction();

    PracticedSystem checkForPracticedSystemAnonymization(PracticedSystem practicedSystem);

    Function<PracticedSystem, PracticedSystemDto> getPracticedSystemToDtoFunction(boolean allowUnreadable);

    ManagementMode checkForManagementModeAnonymization(ManagementMode managementMode);

    Function<PracticedPlot,PracticedPlotDto> getPracticedPlotToDtoFunction();

    PaginationResult<Performance> checkForPerformancesAnonymization(PaginationResult<Performance> result);

    Function<Performance,PerformanceDto> getPerformanceToDtoFunction(int subItemsLimit);
}
