package fr.inra.agrosyst.api.services.users;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.AgrosystUser;

import java.util.function.Function;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class Users {

    public static final Function<AgrosystUser, UserDto> TO_USER_DTO = input -> {
        UserDto result;
        if (input == null) {
            result = null;
        } else {
            result = new UserDto();
            result.setTopiaId(input.getTopiaId());
            result.setFirstName(input.getFirstName());
            result.setLastName(input.getLastName());
            result.setEmail(input.getEmail());
            result.setOrganisation(input.getOrganisation());
            result.setPhoneNumber(input.getPhoneNumber());
            result.setBanner(input.getBanner());
            result.setItEmail(input.getItEmail());
            result.setUserLang(input.getUserLang());
            result.setActive(input.isActive());
            result.setAcceptedCharter(charterVersionToDtoBoolean(input.getCharterVersion()));
            result.setLastMessageReadDate(input.getLastMessageReadDate());
        }
        return result;
    };

    public static final int CURRENT_CHART_VERSION = 2;

    public static boolean charterVersionToDtoBoolean(Integer charterVersion) {
        return charterVersion != null && charterVersion == CURRENT_CHART_VERSION;
    }

}
