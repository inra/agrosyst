package fr.inra.agrosyst.api.services.report;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.VitiPestMaster;
import fr.inra.agrosyst.api.entities.report.YieldInfo;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import lombok.Getter;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class ReportGrowingSystemCollections implements Serializable {
    
    @Serial
    private static final long serialVersionUID = -7646840617182985154L;
    
    public static final Sector NONE_DEPHY_EXPE = null;
    
    // pour les cultures assolées autre que DEPHY-EXPE
    // La filière vaut null
    
    // pour DEPHY-EXPE on a autant de rubriques cultures assollées que de filières sélectionnées pour les filières suivantes:
    // CULTURES_TROPICALES
    // GRANDES_CULTURES
    // HORTICULTURE
    // MARAICHAGE
    // POLYCULTURE_ELEVAGE
    private final Map<Sector, List<CropPestMaster>> cropAdventiceMastersBySector = new HashMap<>();//   Maîtrise des ravageurs
    private final Map<Sector, List<CropPestMaster>> cropDiseaseMastersBySector = new HashMap<>();//     Maîtrise des maladies
    private final Map<Sector, List<CropPestMaster>> cropPestMastersBySector = new HashMap<>();//        Maîtrise des adventices
    
    private final Map<Sector, List<VerseMaster>> verseMastersBySector = new HashMap<>();//              Maîtrise de la verse
    private final Map<Sector, List<FoodMaster>> foodMastersBySector = new HashMap<>();//                Maîtrise de l’alimentati
    private Map<Sector, List<YieldLoss>> yieldLossesBySector = null;//                 Rendement et qualité
    
    // specifique filière ARBORICULTURE
    private List<ArboCropAdventiceMaster> arboCropAdventiceMasters;
    private List<ArboCropPestMaster> arboCropDiseaseMasters;
    private List<ArboCropPestMaster> arboCropPestMasters;
    
    // spécifique filière VITICULTURE
    private List<VitiPestMaster> vitiDiseaseMasters;
    private List<VitiPestMaster> vitiPestMasters;

    private Collection<YieldInfo> yieldInfos;
    
    public void addNoneDephyExpeCropAdventiceMasters(List<CropPestMaster> cropAdventiceMasters) {
        cropAdventiceMastersBySector.put(NONE_DEPHY_EXPE, cropAdventiceMasters);
    }
    
    public void addDephyExpeCropAdventiceMasters(Sector sector, List<CropPestMaster> adventiceMasters) {
        cropAdventiceMastersBySector.put(sector, adventiceMasters);
    }
    
    public void addNoneDephyExpeCropDiseaseMasters(List<CropPestMaster> cropDiseaseMasters) {
        cropDiseaseMastersBySector.put(NONE_DEPHY_EXPE, cropDiseaseMasters);
    }
    
    public void addDephyExpeCropDiseaseMasters(Sector sector, List<CropPestMaster> diseaseMasters) {
        cropDiseaseMastersBySector.put(sector, diseaseMasters);
    }
    
    public void addNoneDephyExpeCropPestMasters(List<CropPestMaster> cropPestMasters) {
        cropPestMastersBySector.put(NONE_DEPHY_EXPE, cropPestMasters);
    }
    
    public void addDephyExpeCropPestMasters(Sector sector, List<CropPestMaster> pestMasters) {
        cropPestMastersBySector.put(sector, pestMasters);
    }
    
    public void addNoneDephyExpeVerseMasters(List<VerseMaster> verseMaster) {
        verseMastersBySector.put(NONE_DEPHY_EXPE, verseMaster);
    }
    
    public void addDephyExpeVerseMasters(Sector sector, List<VerseMaster> verseMasters) {
        verseMastersBySector.put(sector, verseMasters);
    }
    
    public void addNoneDephyExpeFoodMasters(List<FoodMaster> foodMaster) {
        foodMastersBySector.put(NONE_DEPHY_EXPE, foodMaster);
    }
    
    public void addDephyExpeFoodMasters(Sector sector, List<FoodMaster> foodMasters) {
        foodMastersBySector.put(sector, foodMasters);
    }
    
    public void addVitiFoodMasters(List<FoodMaster> foodMaster) {
        // can use same map as DEPHY
        foodMastersBySector.put(Sector.VITICULTURE, foodMaster);
    }
    
    public void addArboFoodMasters(List<FoodMaster> foodMaster) {
        // can use same map as DEPHY
        foodMastersBySector.put(Sector.ARBORICULTURE, foodMaster);
    }

    protected Map<Sector, List<YieldLoss>> getOrCreateYieldLoss(){
        if (yieldLossesBySector == null) {
            yieldLossesBySector = new HashMap<>();
        }
        return yieldLossesBySector;
    }
    public void addDephyExpeYieldLosses(Sector sector, List<YieldLoss> yieldLosses) {
        getOrCreateYieldLoss().put(sector, yieldLosses);
    }
    
    public void addNoneDephyExpeYieldLosses(List<YieldLoss> yieldLoss) {
        getOrCreateYieldLoss().put(NONE_DEPHY_EXPE, yieldLoss);
    }
    
    public void addVitiYieldLosses(List<YieldLoss> yieldLoss) {
        getOrCreateYieldLoss().put(Sector.VITICULTURE, yieldLoss);
    }
    
    public void addArboYieldLosses(List<YieldLoss> yieldLoss) {
        getOrCreateYieldLoss().put(Sector.ARBORICULTURE, yieldLoss);
    }
    
    public void setArboAdventiceMasters(List<ArboCropAdventiceMaster> arboAdventiceMasters) {
        arboCropAdventiceMasters = arboAdventiceMasters;
    }
    
    public void setArboDiseaseMasters(List<ArboCropPestMaster> arboDiseaseMasters) {
        arboCropDiseaseMasters = arboDiseaseMasters;
    }
    
    public void setArboPestMasters(List<ArboCropPestMaster> arboPestMaster) {
        arboCropPestMasters = arboPestMaster;
    }
    
    // spécifique filière VITICULTURE
    public void setVitiDiseaseMasters(List<VitiPestMaster> vitiDiseaseMasters) {
        this.vitiDiseaseMasters = vitiDiseaseMasters;
    }
    
    public void setVitiPestMasters(List<VitiPestMaster> vitiPestMasters) {
        this.vitiPestMasters = vitiPestMasters;
    }

    public void setYieldInfos(Collection<YieldInfo> yieldInfos) {
        this.yieldInfos = yieldInfos;
    }
}
