package fr.inra.agrosyst.api.services.generic;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.AgrosystService;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;
import java.util.Map;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public interface GenericEntityService extends AgrosystService {

    PaginationResult<?> listEntities(Class<?> klass, GenericFilter filter);
    PaginationResult<?> listEntitiesFromString(String className, GenericFilter filter);

    Map<String, Long> countEntities(Class<?>... classes);
    Map<String, Long> countEntitiesFromString(List<String> classesList);

    List<String> getProperties(Class<?> klass);
    List<String> getPropertiesFromString(String className);

    void unactivateEntities(Class<?> klass, List<String> entityIds, boolean activate);
    void unactivateEntitiesFromString(String className, List<String> entityIds, boolean activate);

}
