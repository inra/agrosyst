package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import lombok.Getter;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
public class PerformanceGrowingSystemExecutionContext {
    
    protected final Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem;
    // can be Pair.of(null,null) in case there are no growing systems for
    // the current zone
    
    protected Set<PerformancePlotExecutionContext> plotContexts;// for effective
    
    protected Set<PerformancePracticedSystemExecutionContext> practicedSystemExecutionContexts;
    
    protected Map<String, MultiValuedMap<String, RefHarvestingPrice>> harvestingScenarioPricesByKey;// used into IndicatorStandardisedGrossIncome
    
    protected final String irs;
    protected final String its;
    
    /**
     * For practiced system usage
     */
    public PerformanceGrowingSystemExecutionContext(
            Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem,
            Set<PerformancePracticedSystemExecutionContext> practicedSystemExecutionContexts,
            Map<String, MultiValuedMap<String, RefHarvestingPrice>> harvestingScenarioPricesByKey,
            Set<Network> irs, Set<Network> its) {

        this.growingSystemAndAnonymizeGrowingSystem = growingSystemAndAnonymizeGrowingSystem;
        this.practicedSystemExecutionContexts = practicedSystemExecutionContexts;
        this.harvestingScenarioPricesByKey = harvestingScenarioPricesByKey;
        
        this.its = its.stream()
                .sorted(Comparator.comparing(Network::getName))
                .map(Network::getName)
                .collect(Collectors.joining(", "));
        this.irs = irs.stream()
                .sorted(Comparator.comparing(Network::getName))
                .map(Network::getName)
                .collect(Collectors.joining(", "));
    }
    
    /**
     * For effective usage
     */
    public PerformanceGrowingSystemExecutionContext(
            Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem,
            Set<Network> irs,
            Set<Network> its) {
    
        this.growingSystemAndAnonymizeGrowingSystem = growingSystemAndAnonymizeGrowingSystem;
        this.plotContexts = new HashSet<>();
    
    
        this.its = its == null ? "" : its.stream()
                .sorted(Comparator.comparing(Network::getName))
                .map(Network::getName)
                .collect(Collectors.joining(", "));
        this.irs = irs == null ? "" : irs.stream()
                .sorted(Comparator.comparing(Network::getName))
                .map(Network::getName)
                .collect(Collectors.joining(", "));
    }
    
    public Optional<GrowingSystem> getGrowingSystem() {
        return growingSystemAndAnonymizeGrowingSystem.getLeft();
    }
    
    public Optional<GrowingSystem> getAnonymizeGrowingSystem() {
        return growingSystemAndAnonymizeGrowingSystem.getRight();
    }
    
    public void addPlot(PerformancePlotExecutionContext plotContext) {
        this.plotContexts.add(plotContext);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformanceGrowingSystemExecutionContext that = (PerformanceGrowingSystemExecutionContext) o;
        if (growingSystemAndAnonymizeGrowingSystem.equals(that.growingSystemAndAnonymizeGrowingSystem)) return true;
        return growingSystemAndAnonymizeGrowingSystem.getLeft().equals(that.growingSystemAndAnonymizeGrowingSystem.getLeft());
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(growingSystemAndAnonymizeGrowingSystem.getLeft());
    }
}
