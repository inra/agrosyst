package fr.inra.agrosyst.api;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Locale;

public enum Language {
    FRENCH("fra", "Français", "French", "Français", Locale.FRANCE),
    ENGLISH("eng", "Anglais", "English", "English", Locale.ENGLISH),
    GERMAN("deu", "Allemand", "German", "Deutsch", Locale.GERMAN),
    DANISH("dan", "Danois", "Danish", "Dansk", new Locale.Builder().setLanguage("da").setRegion("DK").build()),
    FINNISH("fin", "Finnois", "Finnish", "Suomen kieli", new Locale.Builder().setLanguage("fi").setRegion("FI").build()),
    SPANISH("spa", "Espagnol", "Spanish", "Español", new Locale.Builder().setLanguage("es").setRegion("ES").build()),
    GREEK("gre", "Grec", "Greek", "Ελληνικά", new Locale.Builder().setLanguage("el").setRegion("GR").build()),
    ITALIAN("ita", "Italien", "Italian", "Italiano", Locale.ITALY),
    DUTCH("nld", "Néerlandais", "Dutch", "Nederlands", new Locale.Builder().setLanguage("nl").setRegion("NL").build()),
    POLISH("pol", "Polonais", "Polish", "Polski", new Locale.Builder().setLanguage("pl").setRegion("PL").build()),
    PORTUGUESE("por", "Portugais", "Portuguese", "Português", new Locale.Builder().setLanguage("pt").setRegion("PT").build()),
    SERBIAN("srb", "Serbe", "Serbian", "српски језик", new Locale.Builder().setLanguage("sr").setRegion("SR").build()),
    SLOVENIAN("slv", "Slovène", "Slovenian", "Slovenščina", new Locale.Builder().setLanguage("sl").setRegion("SL").build());

    public static Language fromTrigram(String trigram) {
        Language language = switch (trigram) {
            case "eng" -> Language.ENGLISH;
            case "deu" -> Language.GERMAN;
            case "dan" -> Language.DANISH;
            case "fin" -> Language.FINNISH;
            case "spa" -> Language.SPANISH;
            case "gre" -> Language.GREEK;
            case "ita" -> Language.ITALIAN;
            case "nld" -> Language.DUTCH;
            case "pol" -> Language.POLISH;
            case "por" -> Language.PORTUGUESE;
            case "srb" -> Language.SERBIAN;
            case "slv" -> Language.SLOVENIAN;
            default -> Language.FRENCH;
        };
        return language;
    }

    private final String trigram;
    private final String frenchName;
    private final String englishName;
    private final String nativeName;
    private final Locale locale;

    Language(String trigram, String frenchName, String englishName, String nativeName, Locale locale) {
        this.trigram = trigram;
        this.frenchName = frenchName;
        this.englishName = englishName;
        this.nativeName = nativeName;
        this.locale = locale;
    }

    public String getTrigram() {
        return trigram;
    }

    public String getFrenchName() {
        return frenchName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public String getNativeName() {
        return nativeName;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getNames() {
        return String.format("%s (%s)", frenchName, nativeName);
    }

    public String getLocalNameWithEnglishName() {
        return String.format("%s (%s)", nativeName, englishName);
    }
}
