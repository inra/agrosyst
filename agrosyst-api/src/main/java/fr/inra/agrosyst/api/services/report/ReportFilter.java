/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.api.services.report;

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.AgrosystFilter;

import java.io.Serial;
import java.util.Set;

/**
 * Filtre interne à la page d'edition des bilans campagne / échelle système de culture.
 */
public class ReportFilter extends AgrosystFilter {

    @Serial
    private static final long serialVersionUID = -4502400178509520246L;

    protected Integer campaign;

    protected String domainId;

    protected String growingSystemId;

    protected Set<Sector> sectors;

    public Integer getCampaign() {
        return campaign;
    }

    public void setCampaign(Integer campaign) {
        this.campaign = campaign;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getGrowingSystemId() {
        return growingSystemId;
    }

    public void setGrowingSystemId(String growingSystemId) {
        this.growingSystemId = growingSystemId;
    }

    public void setSectors(Set<Sector> sectors) {
        this.sectors = sectors;
    }

    public Set<Sector> getSectors() {
        return sectors;
    }
}
