package fr.inra.agrosyst.api.services.network;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.Network;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class NetworkGraph implements Serializable {

    @Serial
    private static final long serialVersionUID = 734183498915994008L;
    protected static final String GROWING_SYSTEM_AS_A_NETWORK_ID = "growing-system_" + UUID.randomUUID();

    protected List<List<SmallNetworkDto>> networks;
    protected Set<NetworkConnectionDto> connections;

    public NetworkGraph() {
        networks = new ArrayList<>();
        connections = Sets.newHashSet();
    }

    protected int baseRow = 0;

    public List<List<SmallNetworkDto>> getNetworks() {
        return networks;
    }

    public void setNetworks(List<List<SmallNetworkDto>> networks) {
        this.networks = networks;
    }

    public Set<NetworkConnectionDto> getConnections() {
        return connections;
    }

    public void setConnections(Set<NetworkConnectionDto> connections) {
        this.connections = connections;
    }

    public void addNetwork(Network network) {
        SmallNetworkDto dto = new SmallNetworkDto(network);
        int level = findLevel(dto);
        if (level == -1) {
            level = baseRow;
            addNetworkToLevel(level, dto);
        }
        Collection<Network> parents = network.getParents();
        if (parents != null) {
            for (Network parent : parents) {
                SmallNetworkDto parentDto = new SmallNetworkDto(parent);
                int parentLevel = findLevel(parentDto);
                if (parentLevel == -1) { // Non présent
                    parentLevel = level - 1;
                    addNetworkToLevel(parentLevel, parentDto);
                } else { // Déjà présent
                    if (parentLevel < level) {
                        // Nothing to do, parent is already before in hierarchy
                    } else {
                        parentLevel = level - 1;
                        moveNetworkToLevel(parentLevel, parentDto.getId());
                    }
                }
                addConnection(network.getTopiaId(), parent.getTopiaId());
            }
        }
    }

    public void addGrowingSystem(String name, Set<Network> parents) {
        SmallNetworkDto dto = new SmallNetworkDto(GROWING_SYSTEM_AS_A_NETWORK_ID, name);
        int level = findLevel(dto);
        if (level == -1) {
            level = baseRow;
            addNetworkToLevel(level, dto);
        }
        if (parents != null) {
            for (Network parent : parents) {
                SmallNetworkDto parentDto = new SmallNetworkDto(parent);
                int parentLevel = findLevel(parentDto);
                if (parentLevel == -1) { // Non présent
                    parentLevel = level - 1;
                    addNetworkToLevel(parentLevel, parentDto);
                } else { // Déjà présent
                    if (parentLevel < level) {
                        // Nothing to do, parent is already before in hierarchy
                    } else {
                        parentLevel = level - 1;
                        moveNetworkToLevel(parentLevel, parentDto.getId());
                    }
                }
                addConnection(GROWING_SYSTEM_AS_A_NETWORK_ID, parent.getTopiaId());
            }
        }
    }

    protected void addNetworkToLevel(int level, SmallNetworkDto network) {
        if (networks.isEmpty()) {
            networks.add(new ArrayList<>());
        }
        if (level <= -1) {
            level = 0;
            baseRow++;
            networks.add(0, new ArrayList<>());
        }
        while (level >= networks.size()) {
            baseRow++;
            networks.add(0, new ArrayList<>());
        }
        networks.get(level).add(network);
    }

    protected void moveNetworkToLevel(int level, final String networkId) {
        int wasAtLevel = findLevel(networkId);
        List<SmallNetworkDto> networksAtLevel = networks.get(wasAtLevel);
        java.util.Optional<SmallNetworkDto> first = networksAtLevel.stream().filter(smallNetworkDto -> Objects.equals(smallNetworkDto, new SmallNetworkDto(networkId, null))).findFirst();
        SmallNetworkDto foundNetwork;

        if (first.isPresent()) {
            foundNetwork = first.get();
            networksAtLevel.remove(foundNetwork);
            addNetworkToLevel(level, foundNetwork);
        }

        Iterable<NetworkConnectionDto> allParents = connections.stream().filter(input -> networkId.equals(input.getSourceId())).collect(Collectors.toList());
        for (NetworkConnectionDto connectionDto : allParents) {
            moveNetworkToLevel(level - 1, connectionDto.getTargetId());
        }
    }

    public void addConnection(String sourceId, String targetId) {
        NetworkConnectionDto connection = new NetworkConnectionDto(sourceId, targetId);
        this.connections.add(connection);
    }

    protected int findLevel(String networkId) {
        SmallNetworkDto network = new SmallNetworkDto(networkId, null);
        return findLevel(network);
    }

    protected int findLevel(final SmallNetworkDto network) {
        int level = 0;
        for (List<SmallNetworkDto> networkLevel : networks) {
            Optional<SmallNetworkDto> optional = Iterables.tryFind(networkLevel, smallNetworkDto -> Objects.equals(smallNetworkDto, network));
            if (optional.isPresent()) {
                return level;
            }
            level++;
        }
        return -1;
    }

}
