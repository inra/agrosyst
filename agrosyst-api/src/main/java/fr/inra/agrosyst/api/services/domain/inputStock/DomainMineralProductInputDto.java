package fr.inra.agrosyst.api.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MineralProductUnit;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@SuperBuilder(toBuilder = true)
public class DomainMineralProductInputDto extends DomainInputDto {
    @Serial
    private static final long serialVersionUID = -5824327702558015951L;
    
    // tradeName = (label) Type de produit fertilisant
    
    @NonNull
    MineralProductUnit usageUnit;
    
    // Forme de l'engrais
    @NonNull
    String forme;
    
    @NonNull
    String type_produit;

    String type_produit_Translated;

    String formeTranslated;
    
    String refInputId;
    
    // SDN (Simulateur de Defences Naturel de Plantes)
    boolean phytoEffect;
    
    // catégorie
    int categ;
    
    // elements
    double n;
    double p2o5;
    double k2o;
    double bore;
    double calcium;
    double fer;
    double manganese;
    double molybdene;
    double mgo;
    double oxyde_de_sodium;
    double so3;
    double cuivre;
    double zinc;
    
    public Optional<String> getRefInputId(){
        return Optional.ofNullable(refInputId);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DomainMineralProductInputDto that = (DomainMineralProductInputDto) o;
        return phytoEffect == that.phytoEffect && categ == that.categ && Double.compare(that.n, n) == 0 && Double.compare(that.p2o5, p2o5) == 0 && Double.compare(that.k2o, k2o) == 0 && Double.compare(that.bore, bore) == 0 && Double.compare(that.calcium, calcium) == 0 && Double.compare(that.fer, fer) == 0 && Double.compare(that.manganese, manganese) == 0 && Double.compare(that.molybdene, molybdene) == 0 && Double.compare(that.mgo, mgo) == 0 && Double.compare(that.oxyde_de_sodium, oxyde_de_sodium) == 0 && Double.compare(that.so3, so3) == 0 && Double.compare(that.cuivre, cuivre) == 0 && Double.compare(that.zinc, zinc) == 0 && usageUnit == that.usageUnit && forme.equals(that.forme) && type_produit.equals(that.type_produit);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), refInputId, usageUnit, phytoEffect, categ, forme, type_produit, n, p2o5, k2o, bore, calcium, fer, manganese, molybdene, mgo, oxyde_de_sodium, so3, cuivre, zinc);
    }
}
