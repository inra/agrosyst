package fr.inra.agrosyst.api.services.domain;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2016 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.services.AgrosystService;

import java.util.List;

/**
 * Domain service.
 *
 * @author Yannick Martel (martel@©odelutin.com)
 */
public interface GroundService extends AgrosystService {

    String NEW_GROUND_PREFIX = "NEW-GROUND-";

    /**
     * Find all grounds.
     *
     * @return all grounds
     */
    List<Ground> getAllGrounds();

    /**
     * Find all grounds for a domain
     *
     * @param domain    :   domain we want grounds
     * @return all domain grounds
     */
    List<Ground> getAllGroundsByDomain(Domain domain);

    /**
     * Create a new (empty) Ground entity. the returned instance is not persisted yet.
     *
     * @return the newly created instance
     */
    Ground newGround();

    /**
     * Find a specific ground from identifier.
     *
     * @param groundId ground id
     * @return domain
     */
    Ground getGround(String groundId);

    /**
     * Find domain ground by refSolArvalis.
     *
     * @param domain            domain we search ground
     * @param refSolArvalis     specific refSolArvalis for the ground
     * @return random domain for code
     */
    Ground getDomainByDomainAndRefSolArvalis(Domain domain, RefSolArvalis refSolArvalis);

    /**
     * @param ground    ground
     * @return persisted ground
     */
    Ground createOrUpdateGround(Ground ground);
}
