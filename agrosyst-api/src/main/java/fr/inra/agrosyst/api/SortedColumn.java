package fr.inra.agrosyst.api;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Cossé David : cosse@codelutin.com
 */
public enum SortedColumn {
    // domains
    DOMAIN,
    CAMPAIGN,
    MAIN_CONTACT,
    RESPONSIBLE,
    DEPARTEMENT,
    SIRET,
    
    // growing plans
    GROWING_PLAN,
    
    // networks
    NETWORK,
    
    // growing-systems
    GROWING_SYSTEM,
    DEPHY_NUMBER,
    
    // decision-rules
    DECISION_RULE,
    
    // report-regionals && report-growing-systems
    REPORT,
    
    // effective-crop-cycles && measurement
    ZONE,
    PLOT,
    CROP,
    
    // practiced-systems
    PRACTICED_SYSTEM,
    
    // practiced-plots-list (RAS)
    
    // performances
    PERFORMANCE,
    DATE
    
}
