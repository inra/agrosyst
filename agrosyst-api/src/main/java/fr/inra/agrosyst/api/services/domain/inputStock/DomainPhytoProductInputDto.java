package fr.inra.agrosyst.api.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@SuperBuilder(toBuilder = true)
public class DomainPhytoProductInputDto extends DomainInputDto {
    
    @Serial
    private static final long serialVersionUID = -8043700641813073579L;
    
    @NonNull
    String refInputId;
    
    @NonNull
    PhytoProductUnit usageUnit;
    
    /**
     * Only set for Product with AMM.
     * With AMM : chemical treatment and biological control product
     * Without AMM: chemical treatment
     */
    String ammCode;

    ProductType productType;
    
    public Optional<String> getAmmCode() {
        return Optional.ofNullable(ammCode);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DomainPhytoProductInputDto that = (DomainPhytoProductInputDto) o;
        return refInputId.equals(that.refInputId) && usageUnit == that.usageUnit && Objects.equals(productType,that.productType) && Objects.equals(ammCode, that.ammCode) && key.equals(that.key);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), refInputId, productType, usageUnit, ammCode, key);
    }
}
