package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serial;
import java.io.Serializable;
import java.util.LinkedHashSet;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Getter
public class MineralProductType implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    protected final int categ;
    protected final String label;
    protected final String translatedLabel;
    protected final LinkedHashSet<MineralProductShape> shapesWithTranslations;

    public MineralProductType(Integer categ, Pair<String, String> labelWithTranslation, Pair<String, String> shapeWithTranslation) {
        this.categ = categ;
        this.label = labelWithTranslation.getLeft();
        this.translatedLabel = labelWithTranslation.getRight();
        this.shapesWithTranslations = new LinkedHashSet<>();
        addShape(shapeWithTranslation);
    }

    public void addShape(Pair<String, String> shape) {
        if (shape != null) {
            shapesWithTranslations.add(new MineralProductShape(shape.getLeft(), shape.getRight()));
        }
    }
    public record MineralProductShape(String shape, String translatedShape) {}

}
