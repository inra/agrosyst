package fr.inra.agrosyst.api.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class InputPriceDto implements Serializable {
    
    @Serial
    private static final long serialVersionUID = -9198051877568959433L;
    /**
     * Nom de l'attribut en BD : objectId
     */
    String objectId;
    
    /**
     * Nom de l'attribut en BD : displayName
     */
    @NonNull
    String displayName;
    
    /**
     * Nom de l'attribut en BD : category
     */
    @NonNull
    InputPriceCategory category;
    
    /**
     * Nom de l'attribut en BD : sourceUnit
     */
    String sourceUnit;
    
    /**
     * price topiaId
     */
    String topiaId;
    
    /**
     * Nom de l'attribut en BD : phytoProductType
     */
    ProductType phytoProductType;
    
    /**
     * Nom de l'attribut en BD : seedType
     */
    SeedType seedType;
    
    /**
     * Nom de l'attribut en BD : price
     */
    Double price;

    /**
     * Nom de l'attribut en BD : priceUnit
     */
    PriceUnit priceUnit;
    
    // seed Price
    
    /**
     * Nom de l'attribut en BD : biologicalSeedInoculation
     */
    boolean biologicalSeedInoculation;
    
    /**
     * Nom de l'attribut en BD : chemicalTreatment
     */
    boolean chemicalTreatment;
    
    /**
     * Nom de l'attribut en BD : includedTreatment
     */
    boolean includedTreatment;
    
    /**
     * Nom de l'attribut en BD : organic
     */
    boolean organic;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InputPriceDto that = (InputPriceDto) o;
        return biologicalSeedInoculation == that.biologicalSeedInoculation && chemicalTreatment == that.chemicalTreatment && includedTreatment == that.includedTreatment && organic == that.organic && Objects.equals(objectId, that.objectId) && displayName.equals(that.displayName) && priceUnit == that.priceUnit && category == that.category && Objects.equals(sourceUnit, that.sourceUnit) && phytoProductType == that.phytoProductType && seedType == that.seedType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(objectId, displayName, priceUnit, category, sourceUnit, phytoProductType, seedType, biologicalSeedInoculation, chemicalTreatment, includedTreatment, organic);
    }
}
