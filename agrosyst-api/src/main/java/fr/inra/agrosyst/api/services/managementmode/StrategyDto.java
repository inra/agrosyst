package fr.inra.agrosyst.api.services.managementmode;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.managementmode.StrategyImpl;

import java.io.Serial;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class StrategyDto extends StrategyImpl {

    /** serialVersionUID */
    @Serial
    private static final long serialVersionUID = -4017408368632657577L;

    protected List<String> cropIds;

    protected Set<String> decisionRuleIds;

    public Set<String> getDecisionRuleIds() {
        return decisionRuleIds;
    }

    public void setDecisionRuleIds(Set<String> decisionRuleIds) {
        this.decisionRuleIds = decisionRuleIds;
    }

    public List<String> getCropIds() {
        return cropIds;
    }

    public void setCropIds(List<String> cropIds) {
        this.cropIds = cropIds;
    }

    public void addCropIds(CroppingPlanEntry crop) {
        if (this.cropIds == null) {
            this.cropIds = new LinkedList<>();
        }
        this.cropIds.add(crop.getTopiaId());
    }
}
