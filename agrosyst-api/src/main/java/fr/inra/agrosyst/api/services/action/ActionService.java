package fr.inra.agrosyst.api.services.action;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedDuplicateCropCyclesContext;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author David Cossé
 */
public interface ActionService extends AgrosystService {

    String NEW_ACTION_PREFIX = "NEW-ACTION-";
    String DEFAULT_DESTINATION_NAME = "A compléter"; // TODO kmorin translate

    static boolean IS_WINE_SPECIES_FOUND(
            Map<String, Set<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Collection<String> speciesCode) {

        if (CollectionUtils.isEmpty(speciesCode)) return false;

        boolean isWineSpeciesFound = speciesCode.stream().anyMatch(sc ->
                CollectionUtils.emptyIfNull(speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee.get(sc))
                        .stream()
                        .map(Pair::getKey)
                        .anyMatch(ReferentialService.WINE::equals));

        return isWineSpeciesFound;
    }

    static boolean IS_WINE_SPECIES_FOUND_(
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Collection<String> speciesCode) {

        if (CollectionUtils.isEmpty(speciesCode)) return false;

        boolean isWineSpeciesFound = speciesCode.stream().anyMatch(sc ->
                CollectionUtils.emptyIfNull(speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee.get(sc))
                        .stream()
                        .map(Pair::getKey)
                        .anyMatch(ReferentialService.WINE::equals));

        return isWineSpeciesFound;
    }

    static boolean IS_WINE_SPECIES_FOUND(Collection<CroppingPlanSpecies> targetedSpeciesStadesSpeciesCodes) {
        boolean wineSpeciesFound = targetedSpeciesStadesSpeciesCodes
                .stream()
                .anyMatch(cps -> (cps.getSpecies().getCode_espece_botanique().equals(ReferentialService.WINE)));
        return wineSpeciesFound;
    }

    Map<HarvestingActionValorisation, String> getHarvestingActionValorisationsActions(Collection<HarvestingActionValorisation> valorisations);

    void removeActionsAndUsagesDeps(Collection<AbstractAction> actionsToRemove);

    void duplicatePracticedActionsAndUsage(
            PracticedDuplicateCropCyclesContext duplicateContext,
            PracticedIntervention practicedIntervention,
            PracticedIntervention interventionClone);

    /**
     * migrate action species to targeted species.
     *
     * @param actionDtos                                all actionDtos to process migration treatment. If there are some SeedSpeciesInputUsage there DomainSeedSpeciesInput will be change for new one if possible
     * @param targetedSpeciesByCode                     targeted species mapped with there code
     * @param fromSpeciesCodeToSpeciesCode              species codes that can replace orginial one if not found
     * @param fromCropCode                              the original code from action species's crop
     * @param toCropCode                                the targeted code for action species's crop
     * @param interventionIntermediateStatusChange      true if from the original intervention to the new one, the intermediate crop is different
     * @param targetedSpeciesStadeDtos                  species stades for the migrated intervention
     * @param sectorByCodeEspeceBotaniqueCodeQualifiant use to validate HarvestingAction:YealdAverage
     * @param domainInputStockUnits                     targeted domain Inputs by keys
     */
    Map<AbstractActionDto, Boolean> migrateEffectiveActionsSpeciesToTargetedSpecies(
            Collection<AbstractActionDto> actionDtos,
            Map<String, CroppingPlanSpecies> targetedSpeciesByCode,
            Map<String, String> fromSpeciesCodeToSpeciesCode,
            String fromCropCode,
            String toCropCode,
            Boolean interventionIntermediateStatusChange,
            List<SpeciesStadeDto> targetedSpeciesStadeDtos,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            Map<String, List<AbstractDomainInputStockUnit>> domainInputStockUnits,
            List<CroppingPlanSpecies> fromZoneSpecies);

    Collection<HarvestingAction> getHarvestingActionForGrowingSystem(GrowingSystem growingSystem);

    void updateAllValorisations(Collection<HarvestingAction> harvestingAction);

    /**
     * Update Actions related to the given PracticedIntervention
     *
     * @param intervention                                      The intervention to affect to the actions
     * @param actionDtos                                        ActionDtos related to the intervention
     * @param domainInputStockUnits                             domain's input
     * @param practicedSystem                                   the practicedSystem to add actions
     * @param speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee nullable, use to validate HarvestinAction
     * @param sectorByCodeEspeceBotaniqueCodeQualifiant         nullable, use to validate HarvestinAction
     * @param speciesStadesDtos                                 nullable, use to validate SeedingAction
     */
    void createOrUpdatePracticedInterventionActionAndUsages(
            PracticedIntervention intervention,
            Collection<AbstractActionDto> actionDtos,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            PracticedSystem practicedSystem,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos);

    void createOrUpdateEffectiveInterventionActionAndUsages(
            EffectiveIntervention intervention,
            Collection<AbstractActionDto> actionDtos,
            Collection<AbstractDomainInputStockUnit> domainInputStockUnits,
            Zone zone,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos,
            CroppingPlanEntry interventionCrop,
            boolean isIntermediateIntervention);

    Collection<AbstractActionDto> loadPracticedActionsAndUsages(
            PracticedIntervention intervention);

    List<AbstractActionDto> loadEffectiveActionsAndUsages(
            EffectiveIntervention intervention);

    List<String> validActionDtosAndUsages(
            Collection<AbstractActionDto> actionDtos,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            List<SpeciesStadeDto> speciesStadesDtos,
            boolean isIntermediateIntervention);

    void searchMissingActionAndUsageInput(
            AbstractActionDto actionDto,
            Map<String, DomainInputDto> domainInputDtoByCode,
            String toDomainId);

    AbstractActionDto cloneActionAndUsages(
            AbstractActionDto actionDto,
            Map<String, DomainInputDto> domainInputDtoByCode,
            String toDomainId);
}
