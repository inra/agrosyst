package fr.inra.agrosyst.api.services.users;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import org.nuiton.util.pagination.PaginationResult;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Service to manage users
 *
 * @author cosse
 */
public interface UserService extends AgrosystService {

    String AGROSYST_USER = "AGROSYST_USER";

    boolean isEmailInUse(String email, String currentUserId);

    boolean isUserActive(String email);

    boolean isValidEmail(String email);

    UserDto createUser(UserDto user, String password);

    UserDto getCurrentUser();

    UserDto getUser(String topiaId);

    UserDto updateUser(UserDto user, String password);

    void unactivateUsers(Set<String> topiaIds, boolean activate);

    PaginationResult<UserDto> getFilteredUsers(UserFilter userFilter, boolean includeRoles);

    /**
     * Return all Users with there first name or last name as there name contains the research term.
     * @param research The researched term.
     * @param nbResult The maximum number if return result
     * @return All the users with there first name or last name as there name contains the research term.
     */
    List<UserDto> getNameFilteredActiveUsers(String research, Integer nbResult);


    /**
     * return the number of users
     * @return The number of users
     * @param active optional active filter (may be null)
     */
    long getUsersCount(Boolean active);

    /**
     * Generate a password reminder token and sends the reminder email
     *
     * @param email
     * @param next
     * @return {@code true} if email was found and email sent
     */
    boolean askForPasswordReminder(String email, String next);

    UserDto preparePasswordChange(String token, String userId);

    UserDto updatePassword(String token, String userId, String password);

    FeedbackContext sendFeedback(FeedbackContext feedbackContext);

    /**
     * Import user file from string.
     * 
     * @param userFileStream user file stream
     * @return import result
     */
    ImportResult importUsers(InputStream userFileStream);

    void acceptCharter();

    Optional<LocalDateTime> getLastMessageReadDate(String userId);

    void readInfoMessages();

}
