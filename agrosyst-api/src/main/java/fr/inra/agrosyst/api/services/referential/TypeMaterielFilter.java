package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MaterielType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * Filtre utilisé pour obtenir les différents types de materiels disponibles (par entonoir) dans
 * le but de sélectionner un matériel.
 *
 * @author Eric Chatellier
 */
@Getter
@Setter
public class TypeMaterielFilter implements Serializable {
    
    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = 6066756882384276462L;
    
    protected MaterielType type;

    protected String typeMateriel1;
    
    protected String typeMateriel2;
    
    protected String typeMateriel3;
    
    protected String typeMateriel4;
    
    protected double uniteParAn;

    protected String typeMateriel1_Translated;

    protected String typeMateriel2_Translated;

    protected String typeMateriel3_Translated;

    protected String typeMateriel4_Translated;

    protected String unite_Translated;
    
    protected Boolean filterOnManualTool;

    protected boolean filterEdaplos;
    
    public TypeMaterielFilter() {}
    
    public TypeMaterielFilter(boolean isManualTools) {
        this.filterOnManualTool = isManualTools;
    }
    
    public TypeMaterielFilter(boolean isManualTools, String typeMateriel1) {
        this(isManualTools);
        this.typeMateriel1 = typeMateriel1;
    }
    
    public TypeMaterielFilter(boolean isManualTools, String typeMateriel1, String typeMateriel2) {
        this(isManualTools, typeMateriel1);
        this.typeMateriel2 = typeMateriel2;
    }
    
    public TypeMaterielFilter(boolean isManualTools, String typeMateriel1, String typeMateriel2, String typeMateriel3) {
        this(isManualTools, typeMateriel1, typeMateriel2);
        this.typeMateriel3 = typeMateriel3;
    }
    
    public TypeMaterielFilter(boolean isManualTools, String typeMateriel1, String typeMateriel2, String typeMateriel3, String typeMateriel4) {
        this(isManualTools, typeMateriel1, typeMateriel2, typeMateriel3);
        this.typeMateriel4 = typeMateriel4;
    }
    
    public Boolean isFilterOnManualTool() {
        return filterOnManualTool;
    }

    public Boolean isFilterEdaplos() {
        return filterEdaplos;
    }


    @Override
    public String toString() {
        return "TypeMaterielFilter{" +
                "type=" + type +
                ", typeMateriel1='" + typeMateriel1 + '\'' +
                ", typeMateriel2='" + typeMateriel2 + '\'' +
                ", typeMateriel3='" + typeMateriel3 + '\'' +
                ", typeMateriel4='" + typeMateriel4 + '\'' +
                ", uniteParAn=" + uniteParAn + '\'' +
                ", filterOnManualTool=" + filterOnManualTool +
                ", filterEdaplos=" + filterEdaplos +
                '}';
    }
}
