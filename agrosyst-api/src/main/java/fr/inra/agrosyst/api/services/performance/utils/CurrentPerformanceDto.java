package fr.inra.agrosyst.api.services.performance.utils;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
public class CurrentPerformanceDto {

    protected String topiaId;

    protected long topiaVersion;

    protected Date topiaCreateDate = new Date();

    /**
     * Nom de l'attribut en BD : name
     */
    protected String name;

    /**
     * Nom de l'attribut en BD : practiced
     */
    protected boolean practiced;

    /**
     * Nom de l'attribut en BD : updateDate
     */
    protected OffsetDateTime updateDate;

    /**
     * Nom de l'attribut en BD : scenarioCodes
     */
    protected Collection<String> scenarioCodes;

    /**
     * Nom de l'attribut en BD : nbTasks
     */
    protected long nbTasks;

    /**
     * Nom de l'attribut en BD : domains
     */
    protected Collection<Domain> domains;

    /**
     * Nom de l'attribut en BD : plots
     */
    protected Collection<Plot> plots;

    /**
     * Nom de l'attribut en BD : zones
     */
    protected Collection<Zone> zones;

    /**
     * Nom de l'attribut en BD : author
     */
    protected AgrosystUser author;

    /**
     * Nom de l'attribut en BD : computeStatus
     */
    protected PerformanceState computeStatus;

    /**
     * Nom de l'attribut en BD : indicatorFilter
     */
    protected Collection<IndicatorFilter> indicatorFilter;

    /**
     * Nom de l'attribut en BD : exportType
     */
    protected ExportType exportType;

    /**
     * Nom de l'attribut en BD : growingSystems
     */
    protected Collection<GrowingSystem> growingSystems;

    public boolean isPersisted() {
        return StringUtils.isNoneBlank(topiaId);
    }
}
