package fr.inra.agrosyst.api.services.measurement;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface MeasurementService extends AgrosystService {

    /**
     * Find zone by topia id.
     * 
     * @param zoneTopiaId zone id
     * @return zone
     */
    Zone getZone(String zoneTopiaId);

    /**
     * Get all measurement session for zone
     * @param zone zone
     * @return sessions
     */
    List<MeasurementSession> getZoneMeasurementSessions(Zone zone);

    /**
     * Update measurement sessions for zone.
     * 
     * @param zone zone
     * @param sessions sessions
     */
    void updateMeasurementSessions(Zone zone, Collection<MeasurementSession> sessions);

    /**
     * Recupère les cultures en places sur la zone en fonction des cycles de cultures réalisés.
     * 
     * @param zone zone
     * @return main cropping plan entries
     */
    Set<CroppingPlanEntry> getZoneCroppingPlanEntries(Zone zone);

    List<VariableType> findAllVariableTypes(MeasurementType measurementType);

    /**
     * Find all variable.
     * 
     * @param measurementType measurement category
     * @param variableType variable type (can be empty)
     * @return variables
     */
    List<RefMesure> findAllVariables(MeasurementType measurementType, VariableType variableType);

    List<RefSupportOrganeEDI> findAllSupportOrganeEDI();

    /**
     * Retourne les entites de produit commercial en retournant un sous ensemble unique
     * sur le champs nom_commun_sa.
     *
     * @Deprecated: utiliser le référentiel RefCompositionSubstancesActivesParNumeroAMM en remplacement de RefActaSubstanceActive
     * 
     * @return
     */
    @Deprecated
    List<RefActaSubstanceActive> findDistinctSubstanceActives();

    List<RefAdventice> findAllAdventices();

    List<RefStadeEDI> findAllStadeEdi(String cropFamily, String vegetativeProfile);

    List<String> findAllProtocoleVgObsLabels();

    List<String> findAllProtocoleVgObsPests(ProtocoleVgObsFilter filter);

    List<String> findAllProtocoleVgObsStades(ProtocoleVgObsFilter filter);

    List<String> findAllProtocoleVgObsSupports(ProtocoleVgObsFilter filter);

    List<String> findAllProtocoleVgObsObservations(ProtocoleVgObsFilter filter);

    List<String> findAllProtocoleVgObsQualitatives(ProtocoleVgObsFilter filter);

    List<String> findAllProtocoleVgObsUnits(ProtocoleVgObsFilter filter);

    List<RefProtocoleVgObs> findAllProtocoleVgObsQualifiers(ProtocoleVgObsFilter filter);

    /**
     * Recherche les nuisibles EDI utilisées pour les observations hors protocole vgobs.
     * 
     * @return les nuisibles
     */
    List<BioAgressorType> findAllEdiPestTypes();

    /**
     * Recherche les nuisibles EDI utilisées pour les observations hors protocole vgobs.
     * 
     *
     * @param pestType filtre suivant le type
     * @return nuisible edi
     */
    List<RefNuisibleEDI> findAllEdiPests(BioAgressorType pestType);

    /**
     * Retourne l'ensemble des stades EDI utilisé pour les observation hors protcole vgobs.
     * 
     * @return les stades nuisibles
     */
    List<RefStadeNuisibleEDI> findAllEdiPestStades();

    /**
     * Recherche les unités définies dans le referentiel protocole vgbos, mais utilisé hors
     * protocole vgobs.
     * 
     * @return les unités
     */
    List<String> findAllVgObsUnits();

    /**
     * Retourne les types de notations EDI utilisés pour les observations hors protcole vgobs.
     * 
     * @return tous les types de notations EDI
     */
    List<RefTypeNotationEDI> findAllEdiNotations();

    /**
     * Retourne les valeurs qualifiants EDI utilisés pour les observations hors protcole vgobs.
     * 
     * @return valeur qualifiant EDI
     */
    List<RefValeurQualitativeEDI> findAllEdiQualitatives();

    /**
     * Retourne les unites qualifaint edi utilisés pour les observations hors protcole vgobs.
     * 
     * @return toutes les unités qualifiants edi
     */
    List<RefUnitesQualifiantEDI> findAllEdiQualifiantUnits();

    /**
     * Export selected effective Measurments id as excel sheet.
     * 
     * @param zoneIds zone ids
     * @return the export result
     */
    ExportResult exportEffectiveMeasurementsAsXls(Collection<String> zoneIds);

    void exportEffectiveMeasurementsAsXlsAsync(Collection<String> effectiveCropCycleIds);

}
