package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.io.Serial;

public class RefPrixFertiMinImpl extends RefPrixFertiMinAbstract {
    
    @Serial
    private static final long serialVersionUID = -4724790568176744642L;
    
    /**
     * No ObjectId to return this is the reference price of only one element.
     * For Fertimin intrant reference price's it mus be the some of all elements
     *
     * @return null
     */
    @Override
    public String getObjectId() {
        return null;
    }
} //RefPrixFertiMinImpl
