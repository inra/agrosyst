package fr.inra.agrosyst.api.services.action;
/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.input.PotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SubstrateInputUsageDto;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.util.Collection;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class OtherActionDto extends ActionWithOtherProductInputUsagesDto {
    @Serial
    private static final long serialVersionUID = -1565020736288158296L;
    
    protected Collection<SubstrateInputUsageDto> substrateInputUsageDtos;
    
    protected Collection<PotInputUsageDto> potInputUsageDtos;
    
    public Optional<Collection<SubstrateInputUsageDto>> getSubstrateInputUsageDtos() {
        return Optional.ofNullable(substrateInputUsageDtos);
    }
    
    public Optional<Collection<PotInputUsageDto>> getPotInputUsageDtos() {
        return Optional.ofNullable(potInputUsageDtos);
    }
}
