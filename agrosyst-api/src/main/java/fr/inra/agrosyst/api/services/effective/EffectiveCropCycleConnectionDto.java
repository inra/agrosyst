package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
public class EffectiveCropCycleConnectionDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -4993144996707356076L;

    protected String topiaId;

    protected String edaplosIssuerId;

    protected String sourceId;

    protected String targetId;

    protected String intermediateCroppingPlanEntryId;

    protected String label;
    protected String intermediateCroppingPlanEntryName;

}
