package fr.inra.agrosyst.api.services.security;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UserRoleEntityDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -8398311239355356838L;

    protected String identifier;
    protected String label;
    protected Integer campaign;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getCampaign() {
        return campaign;
    }

    public void setCampaign(Integer campaign) {
        this.campaign = campaign;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("identifier", identifier)
                .add("label", label)
                .add("campaign", campaign).toString();
    }
}
