package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Aliment;
import fr.inra.agrosyst.api.entities.Cattle;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.Ration;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.WeatherStation;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Domain service.
 *
 * @author Eric Chatellier
 */
public interface DomainService extends AgrosystService {
    
    String NEW_CROP_PREFIX = "NEW-CROP-";
    String NEW_SPECIES_PREFIX = "NEW-CROP-";
    
    String NEW_EQUIPMENT = "NEW-EQUIPMENT-";
    
    /**
     * Create a new (empty) Domain entity. the returned instance is not persisted yet.
     *
     * @return the newly created instance
     */
    Domain newDomain();

    /**
     * Create a new (empty) GPSData entity. the return instance is not persited yed.
     *
     * @return the newly created instance
     */
    GeoPoint newGpsData();

    /**
     * Creates a new (empty) {@link WeatherStation}.
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     *
     * @return the newly created {@link WeatherStation} instance
     */
    WeatherStation newWeatherStation();
    
    /**
     * Create new instance of domain materiel.
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     *
     * @return new domain materiel instance
     */
    Equipment newMateriel();
    
    /**
     * Create sol instance.
     *
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     * @return new sol instance
     */
    Ground newSol();
    
    /**
     * Create ToolsCoupling instance.
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     *
     * @return new ToolsCoupling instance
     */
    ToolsCoupling newToolsCoupling();
    
    /**
     * Create Aliment instance
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     * @return Aliment instance not persisted yet
     */
    Aliment newAliment();
    
    /**
     * Create Ration instance
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     * @return Ration instance not persisted yet
     */
    Ration newRation();
    
    /**
     * Create Cattle instance
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     * @return Cattle instance not persisted yet
     */
    Cattle newCattle();
    
    /**
     * Create LivestockUnit instance
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     * @return LivestockUnit instance not persisted yet
     */
    LivestockUnit newLivestockUnit();
    
    /**
     * Create CroppingPlanEntry instance
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     * @return CroppingPlanEntry instance not persisted yet
     */
    CroppingPlanEntry newCroppingPlanEntry();
    
    /**
     * Create CroppingPlanSpecies instance
     * <p>
     * This returned instance is not persisted yet.
     * </p>
     * @return CroppingPlanSpecies instance not persisted yet
     */
    CroppingPlanSpecies newCroppingSpecies();
    
    /**
     * Find a specific domain from identifier.
     *
     * @param domainId domain id
     * @return domain
     */
    Domain getDomain(String domainId);
    
    List<DomainDto> getDomains(Collection<String> domainIds);
    
    /**
     * Find random domain by code.
     *
     * @param domainCode domain code
     * @return random domain for code
     */
    DomainDto getActiveOrUnactiveDomainForCode(String domainCode);
    
    /**
     * Find all domain.
     *
     * @return all domains
     */
    List<Domain> getAllDomains();
    
    /**
     * Get domain related to current domain.
     * Related domain got same duplication code.
     *
     * @param domainCode the code identifying the domain
     * @return related domains
     */
    LinkedHashMap<Integer, String> getRelatedDomains(String domainCode);
    
    /**
     * Return the given domain persisted with it's related components
     * If a given collection parameters is null, it will be ignored
     * if it is empty, all elements from persisted ones will be removed.
     *
     * @param domain                the domain to save
     * @param locationId            the ref location id
     * @param legalStatusId         the ref legal status id
     * @param geoPoints             GPS coordinate center
     * @param croppingPlanDtos      Crop DTOs
     * @param otex18code            otex18
     * @param otex70code            otex70
     * @param grounds               the domain grounds
     * @param equipments            the domain equipments
     * @param toolsCouplings        the domain tools couping including manual intervention
     * @param livestockUnits        the live stock unit
     * @param domainInputStockUnits the input stock unit
     * @param harvestingPriceDtos   the harvestingPriceDtos
     * @return the persisted domain
     */
    Domain createOrUpdateDomain(
            Domain domain,
            String locationId,
            String legalStatusId,
            List<GeoPoint> geoPoints,
            List<CroppingPlanEntryDto> croppingPlanDtos,
            Integer otex18code,
            Integer otex70code,
            List<Ground> grounds,
            List<Equipment> equipments,
            List<ToolsCoupling> toolsCouplings,
            List<LivestockUnit> livestockUnits,
            Collection<DomainInputDto> domainInputStockUnits,
            List<HarvestingPriceDto> harvestingPriceDtos);

    Domain createOrUpdateDomainWithoutCommit(
            Domain domain,
            String locationId,
            String legalStatusId,
            List<GeoPoint> geoPoints,
            List<CroppingPlanEntryDto> croppingPlanDtos,
            Integer otex18code,
            Integer otex70code,
            List<Ground> grounds,
            List<Equipment> equipments,
            List<ToolsCoupling> toolsCouplings,
            List<LivestockUnit> livestockUnits,
            Collection<DomainInputDto> domainInputStockUnits,
            List<HarvestingPriceDto> harvestingPriceDtos);

    /**
     * Fix Double format, replace ',' by '.'
     * Remove duplicate values
     * from an invalid json format as:
     * {"zbb_zbd__":46,"zcj_g99__":32.5,"zdg_zni__":2,"zdg_zni__":2.5}
     *
     * @param json
     * @return
     */
    Map<String, Double> safelyConvertDomainSpeciesToAreaJson(String json);

    String sanitizedDomainSpeciesToAreaJson(String json);

    /**
     * Find domain matching user filter and navigation context.
     *
     * @param filter custom user filter
     * @return matching domains
     */
    PaginationResult<Domain> getFilteredDomains(DomainFilter filter);
    
    /**
     * Find domain matching user filter and navigation context.
     *
     * @param filter custom user filter
     * @return matching domains
     */
    PaginationResult<DomainDto> getFilteredDomainsDto(DomainFilter filter);
    
    
    PaginationResult<DomainSummaryDto> getFilteredDomainSummariesDto(DomainFilter filter);
    
    List<Domain> getDomainWithName(String name);
    
    Domain getDomainForCampaign(String code, Integer campaign);
    
    /**
     * Find domain ids matching user filter and navigation context.
     *
     * @param filter custom user filter
     * @return matching domain ids
     */
    Set<String> getFilteredDomainIds(DomainFilter filter);
    
    /**
     * Unactivate or reactivate the given domains.
     *
     * @param domainIds domains topiaId to unactivate
     */
    void unactivateDomains(List<String> domainIds, boolean activate);
    
    
    void deleteDomain(String domainId) throws DomainDeletionException;
    
    /**
     *
     * @param domain the domain to look for campagins
     * @return the campaigns related to the active curent domain
     */
    List<Integer> getCampaignsForDomain(Domain domain);
    
    
    /**
     * Extend domain for specified campaign.
     *
     * @param domainTopiaId  domain to duplicate
     * @param extendCampaign extend domain campaign
     * @return duplicated domain
     */
    Domain extendDomain(String domainTopiaId, int extendCampaign) throws DomainExtendException;

    /**
     * Extend the closest domain from the given campaign to it, and its related entities (without commiting)
     *
     * @param domainTopiaId  domain to duplicate
     * @param extendCampaign extend domain campaign
     * @return duplicated domain
     */
    Domain extendDomainNoCommit(String domainTopiaId, int extendCampaign) throws DomainExtendException;
    
    /**
     *
     * Export selected domains id as excel sheet.
     */
    ExportResult exportDomainAsXls(Collection<String> domainIds);
    
    void exportDomainAsXlsAsync(Collection<String> domainIds);
    
    /**
     * Check if a domain already exists if specified name.
     *
     * @param domainName domain name to check
     * @return {@code true} if a domain already exists with that name
     */
    boolean checkDomainExistence(String domainName);
    
    /**
     * return the domain code associated to the growing system with topiaId the growingSystemId parameters.
     * @param growingSystemId The id of the growing system
     * @return the domain related to the Growing System
     */
    String getDomainCodeForGrowingSystem(String growingSystemId);
    
    /**
     * Do not use this method to get writable domains, this is only usable on DecisionRule creation !
     * See <a href="https://forge.codelutin.com/issues/4439#note-6">...</a>
     * @return get active writable domains for DecisionRule
     * @param domainFilter filter
     */
    List<Domain> getActiveWritableDomainsForDecisionRuleCreation(DomainFilter domainFilter);

    /**
     * return all domains TopiaId related to the given GrowingSystem id for the given campaigns range.
     * @param gsID the GrowingSystem id
     * @param campaigns the required campaigns
     * @return all domains TopiaId
     */
    List<String> getDomainIdsForGrowingSystemAndCampaigns(String gsID, Set<Integer> campaigns, boolean includeCropsFromInactiveDomains);

    /**
     * return domain TopiaId related to the given GrowingSystem id.
     * @param gsID the GrowingSystem id
     * @return domain TopiaId
     */
    String getDomainIdForGrowingSystem(String gsID);

    /**
     * return all domains TopiaId related to the given domain id for the given campaigns range.
     * @param domainId the Domain id
     * @param campaigns the required campaigns
     * @return all domains TopiaId
     */
    List<String> getDomainIdsForDomainAndCampaigns(String domainId, Set<Integer> campaigns, boolean includeCropsFromInactiveDomains);

    /**
     * Return the number of domains
     *
     * @return The number of domains
     * @param active optional active filter (may be null)
     */
    long getDomainsCount(Boolean active);
    /**
     * Do validate the current domain state
     *
     * @param domainId the identifier of the domain to validate
     * @return the validated domain
     */
    Domain validateAndCommit(String domainId);

    /**
     * return GeoPoint for domain with id the given domainId
     * Data are anonymize if user has no right to see them
     * @param domainId domain'id
     * @return all geopoints for the given domainId
     */
    List<GeoPoint> getGeoPoints(String domainId);

    /**
     *
     * @param domainId domain'id
     * @return All Ground for domain with id the given domainId
     */
    List<Ground> getGrounds(String domainId);

    /**
     * If usage is required please use getDomainInputDtoUsageByTypes()
     * @param domainId domain'id
     * @return All input stock units for domain with id the given domainId
     */
    List<DomainInputDto> getInputStockUnits(String domainId);

    /**
     *
     * @param domainId domain'id
     * @return All equipements for domain with id the given domainId
     */
    List<Equipment> getEquipments(String domainId);

    List<Equipment> getTranslatedEquipmentsForDomain(Domain domain);

    List<Equipment> getEquipmentsForDomain(Domain domain);

    boolean isDefaultEdaplosEquipmentForDomain(List<Equipment> equipments);
    
    
    List<Equipment> getEquipmentsForDomainCodeAndCampaigns(String code, Set<Integer> campaigns);

    /**
     *
     * @param domainId domain'id
     * @return All ToolsCoupling for domain with id the given domainId
     */
    List<ToolsCoupling> getToolsCouplings(String domainId);

    List<ToolsCoupling> getToolsCouplingsForDomain(Domain domain);

    List<ToolsCoupling> getToolsCouplingsForAllCampaignsDomain(Domain domain);

    /**
     * The tools coupling UsageList from domain with id the given domainId.
     * @param domainId domain'id
     * @return All ToolsCoupling and there usage status for domain with id the given domainId
     */
    UsageList<ToolsCoupling> getToolsCouplingAndUsage(String domainId);
    
    /**
     * list of ToolsCoupling's Codes for domains with code the given code and for given campaigns.
     * @param code domain's code
     * @param campaigns required campaigns for domain, can not be null or empty
     * @return List of ToolsCoupling's Codes
     */
    List<String> getToolsCouplingCodeForDomainsAndCampaigns(String code, Set<Integer> campaigns);
    
    /**
     * List of ToolsCouplings for domains with code the given code and for given campaigns.
     * @param code domain's code
     * @param campaigns required campaigns for domain, can not be null or empty
     * @return List of ToolsCouplings
     */
    List<ToolsCoupling> getToolsCouplingsForDomainCodeAndCampaigns(String code, Set<Integer> campaigns);

    /**
     * Copy equipments and optionally tools couplings and manuals interventions from domain the given fromDomain to domain the given toDomains list
     *
     * @param fromDomain           source domain Id
     * @param toDomains            target domains Ids
     * @param includeToolCouplings true if tools coupling have to be include
     * @param equipmentsToCopy     list of source domain equipments
     * @param toolsCouplingsToCopy list of tools couplings
     * @return the result message
     */
    List<String> copyTools(String fromDomain, List<String> toDomains, Boolean includeToolCouplings, List<Equipment> equipmentsToCopy, List<ToolsCoupling> toolsCouplingsToCopy);

    /**
     * Copy equipments and optionally tools couplings and manuals interventions from domain the given fromDomain to domain the given toDomains list
     *
     * @param fromDomain  source domain Id
     * @param inputIds    source domain input Ids
     * @param toDomainIds target domains Ids
     * @param forceCopy   true: the domain input will be added to the targeted domain regardless it if the same input exists
     * @return the result message
     */
    List<String> copyInputs(String fromDomain, List<String> inputIds, List<String> toDomainIds, boolean forceCopy);

    /**
     * The CroppingPlanEntryDto UsageList from domain with id the given domainId.
     * @param domainId domain'id
     * @return All CroppingPlanEntryDto and there usage status for domain with id the given domainId
     */
    UsageList<CroppingPlanEntryDto> getCroppingPlanEntryDtoAndUsage(String domainId);
    
    Map<InputType, UsageList<DomainInputDto>> getDomainInputDtoUsageByTypes(String domainId);

    List<DomainInputDto> getDomainInputDtoForType(String domainId, String growingSystemId, InputType inputType);

    /**
     * List of CroppingPlan's Codes for domains with code the given code and for given campaigns.
     *
     * @param code domain's code
     * @param campaigns required campaigns for domain, can not be null or empty
     * @return list of CroppingPlan's Codes
     */
    List<String> getCroppingPlanCodeForDomainsAndCampaigns(String code, Set<Integer> campaigns);
    
    
    List<CroppingPlanEntry> getCroppingPlanEntriesForDomainCodeAndCampaigns(String domainCode, Set<Integer> campaigns);
    
    
    Map<String, List<CroppingPlanSpecies>> getCroppingPlanSpeciesForDomainAndCampaignsByCropCode(String domainCode, Set<Integer> campaigns);
    
    /**
     * Get the list of cropping plan (assolement) of the given domain containing only main
     * crops (not intermediate). This list is read only and no element can be added to it.
     *
     * @param domainId domain's id
     * @return All cropping plans (assolement) of the given domain containing only main
     * crops (not intermediate).
     */
    List<CroppingPlanEntryDto> getCroppingPlanDtos(String domainId);
    
    /**
     * simply get Domain crop
     * @param domain the domain crops belong to
     * @return the crop
     */
    List<CroppingPlanEntry> getCroppingPlanEntriesForDomain(Domain domain);
    
    
    List<CroppingPlanEntry> getCroppingPlanEntriesForAllCampaignsDomain(Domain domain);
    
    /**
     * return only the crop itself, not the related domain and species
     * @param domainId domain where crops are loaded
     * @return return only the crop itself, not the related domain and species
     */
    List<CroppingPlanEntry> getCroppingPlansBodies(String domainId);
    
    /**
     * All CroppingPlanSpecies Ids and there usage status for domain with Id the given domainId
     * @param domainId domain's id
     * @return All CroppingPlanSpecies Ids and there usage status
     */
    Map<String, Boolean> getCroppingPlanSpeciesUsage(String domainId);
    
    /**
     * Charge les couples cultures/especes à partir du code de l'espèce et d'un Set de campagnes
     *
     * @param croppingPlanEntryCode code de la culture
     * @param campaignsSet          liste des campagnes
     * @return une Pair&lt;Culture, Map&lt;code espece, Espece&gt;&gt;
     */
    Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>> getEntryAndSpeciesFromCode(
            String croppingPlanEntryCode, Set<Integer> campaignsSet);
    
    
    Map<String, Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>>> findSpeciesByCropBySpeciesCodeForCropCodesOnCampaigns(List<String> croppingPlanEntryCodes, Set<Integer> campaignsSet);
    
    /**
     * Past the given crops and there species
     * - If past to same domain same campaign
     * - crops and species are copied, there "code" is remove.
     * - If past to same domain but on different campaign
     * - if crop exist (same code) a new one is created without relation ship between original crop and it
     * - if crop doesn't exists, the crop is created with same "code" parameter as relationship between original crops and pasted ones are keep.
     * _ if past to other domains
     * - crops and species are copied, there "code" is remove.
     *
     * @param fromDomain domain topiaId from where crops were copied
     * @param toDomains  to domains topiaIds to past crops
     * @param crops      CroppingPlanEntryDto to past
     * @return List of "domain name - campaign"
     */
    List<String> pasteCrops(String fromDomain, List<String> toDomains, List<CroppingPlanEntryDto> crops);

    /**
     *
     * @param domainId the domain topiadId
     * @return Maps of usageList by class name
     *         LivestockUnit.class.getName(), livestockUsageList
     *         Cattle.class.getName(), cattleUsageList
     */
    Map<String, UsageList> getUsedLivestocksAndCattles(String domainId);
    
    /**
     * Get domain'plots total area.
     *
     * @param domainId domain id
     * @return total area
     */
    double getDomainSAUArea(String domainId);
    
    List<LivestockUnit> loadLivestockUnitsForDomainId(String domainId);
    
    List<LivestockUnit> loadLivestockUnitsForDomainCode(String domainCode, Set<Integer> campaigns);
    
    Integer getCampaignForDomain(String domainId);


    Map<String, String> getCountries();

    RefCountry getFranceRefCountry();

    String getDefaultCountryTopiaId();

    Domain setPracticedPlotsForDomain(Domain domain, Collection<PracticedPlot> practicedPlots);

    RefCountry getCountry();

}

