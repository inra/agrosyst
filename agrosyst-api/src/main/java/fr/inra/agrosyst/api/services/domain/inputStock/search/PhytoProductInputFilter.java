package fr.inra.agrosyst.api.services.domain.inputStock.search;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;

/**
 * Filter on
 *   RefActaTraitementsProduitsCateg.action (AgrosystInterventionType)
 *   RefActaTraitementsProduitsCateg.type_produit (ProductType)
 *   RefActaTraitementsProduit.nom_produit
 *   RefActaTraitementsProduit.code_AMM
 *   RefActaSubstanceActive.nom_commun_sa
 *
 * @author Cossé David : cosse@codelutin.com
 *
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
public class PhytoProductInputFilter implements Serializable {

    @Serial
    private static final long serialVersionUID = 8443469679063911029L;

    /*
      RefActaTraitementsProduitsCateg.action
          LUTTE_BIOLOGIQUE
          SEMIS
          APPLICATION_DE_PRODUITS_PHYTOSANITAIRES
     */
    @NonNull
    AgrosystInterventionType action;

    String countryTopiaId;

    /*
       For seedLot the species RefEspece.topiaId
     */
    Collection<String> refEspeceIds;
    /*
      RefActaTraitementsProduitsCateg.type_produit
     */
    ProductType typeProduit;//SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION

    Boolean notEqualsTypeProduit;

    /*
      product trade name
      RefActaTraitementsProduit.nom_produit
     */
    String nomProduitTerm;

    /*
     * Exact product name from selected element
     */
    String nomProduit;

    Boolean isWithAmmCode;

    /*
      RefActaTraitementsProduit.code_AMM
     */
    String codeAMM;

    /*
      RefActaSubstanceActive.nom_commun_sa
     */
    String activeSubstancesTerm;

    /*
      exact RefActaSubstanceActive.nom_commun_sa, from selected element
     */
    String activeSubstance;

    boolean ipmworks = false;

}
