package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.MaterielType;

import java.io.Serial;
import java.io.Serializable;

/**
 * MaterielDTO used to edit {@code Materiel} using struts.
 * 
 * @author Eric Chatellier
 */
public class MaterielDto implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -2638995441618947327L;

    protected String topiaId;

    /**
     * Key that allow the binding of material loaded from a JSP when no topiaId are defined on it.
     */
    protected String hashKey;

    protected String name;

    protected String description;

    protected boolean materielETA;

    protected boolean weakenedMaterial;

    protected boolean homemadeMaterial;

    protected boolean jerryRigged;

    protected String materielTopiaId;

    protected MaterielType materielType;

    protected String typeMateriel1;

    protected String typeMateriel2;

    protected String typeMateriel3;

    protected String typeMateriel4;

    protected double uniteParAn;

    protected String unite;

    protected String unitTranslated;
    
    protected String performanceUnite;
    
    protected Double performance;

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Strings.emptyToNull(description);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMaterielETA() {
        return materielETA;
    }

    public void setMaterielETA(boolean materielETA) {
        this.materielETA = materielETA;
    }

    public String getMaterielTopiaId() {
        return materielTopiaId;
    }

    public void setMaterielTopiaId(String materielTopiaId) {
        this.materielTopiaId = materielTopiaId;
    }

    public MaterielType getMaterielType() {
        return materielType;
    }

    public void setMaterielType(MaterielType materielType) {
        this.materielType = materielType;
    }

    public String getTypeMateriel1() {
        return typeMateriel1;
    }

    public void setTypeMateriel1(String typeMateriel1) {
        this.typeMateriel1 = typeMateriel1;
    }

    public String getTypeMateriel2() {
        return typeMateriel2;
    }

    public void setTypeMateriel2(String typeMateriel2) {
        this.typeMateriel2 = typeMateriel2;
    }

    public String getTypeMateriel3() {
        return typeMateriel3;
    }

    public void setTypeMateriel3(String typeMateriel3) {
        this.typeMateriel3 = typeMateriel3;
    }

    public String getTypeMateriel4() {
        return typeMateriel4;
    }

    public void setTypeMateriel4(String typeMateriel4) {
        this.typeMateriel4 = typeMateriel4;
    }

    public double getUniteParAn() {
        return uniteParAn;
    }

    public void setUniteParAn(double uniteParAn) {
        this.uniteParAn = uniteParAn;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

    public String getHashKey() {
        return hashKey;
    }

    public void setHashKey(String hashKey) {
        this.hashKey = hashKey;
    }

    public String getPerformanceUnite() {
        return performanceUnite;
    }

    public void setPerformanceUnite(String performanceUnite) {
        this.performanceUnite = performanceUnite;
    }

    public Double getPerformance() {
        return performance;
    }

    public void setPerformance(Double performance) {
        this.performance = performance;
    }

    public boolean isWeakenedMaterial() {
        return weakenedMaterial;
    }

    public void setWeakenedMaterial(boolean weakenedMaterial) {
        this.weakenedMaterial = weakenedMaterial;
    }

    public boolean isHomemadeMaterial() {
        return homemadeMaterial;
    }

    public void setHomemadeMaterial(boolean homemadeMaterial) {
        this.homemadeMaterial = homemadeMaterial;
    }

    public boolean isJerryRigged() {
        return jerryRigged;
    }

    public void setJerryRigged(boolean jerryRigged) {
        this.jerryRigged = jerryRigged;
    }

    public void setUniteTranslated(String unitTranslated) {
        this.unitTranslated = unitTranslated;
    }

    public String getUnitTranslated() {
        return unitTranslated;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MaterielDto other = (MaterielDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }
}
