package fr.inra.agrosyst.api.services.security;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public interface AuthenticationService extends AgrosystService {

    /**
     * Tente d'authentifier l'utilisateur et charge une instance d'AuthenticatedUser
     *
     * @param email    The user's email
     * @param password The user's password
     * @return the users session detail
     */
    AuthenticatedUser login(String email, String password);

    /**
     * Charge une nouvelle instance de AuthenticatedUser à partir de la base de données pour l'utilisateur actuellement
     * connecté.
     *
     * @return une nouvelle fraîchement créée
     * @since 2.64
     */
    AuthenticatedUser reloadAuthenticatedUser();

    AuthenticatedUser getAuthenticatedUserFromUserId(String userId, String sid);

}
