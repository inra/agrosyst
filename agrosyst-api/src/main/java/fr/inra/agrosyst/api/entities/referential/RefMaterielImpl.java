package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.io.Serial;

public abstract class RefMaterielImpl extends RefMaterielAbstract {

    @Serial
    private static final long serialVersionUID = 1L;

    protected String typeMateriel1_Translated;

    protected String typeMateriel2_Translated;

    protected String typeMateriel3_Translated;

    protected String typeMateriel4_Translated;

    protected String unite_Translated;

    @Override
    public void setTypeMateriel1_Translated(String libelle) {
        typeMateriel1_Translated = libelle;
    }

    @Override
    public void setTypeMateriel2_Translated(String libelle) {
        typeMateriel2_Translated = libelle;
    }

    @Override
    public void setTypeMateriel3_Translated(String libelle) {
        typeMateriel3_Translated = libelle;
    }

    @Override
    public void setTypeMateriel4_Translated(String libelle) {
        typeMateriel4_Translated = libelle;
    }

    @Override
    public void setUnite_Translated(String libelle) {
        unite_Translated = libelle;
    }

    @Override
    public String getTypeMateriel1_Translated() {
        return StringUtils.firstNonBlank(typeMateriel1_Translated, typeMateriel1);
    }

    @Override
    public String getTypeMateriel2_Translated() {
        return StringUtils.firstNonBlank(typeMateriel2_Translated, typeMateriel2);
    }

    @Override
    public String getTypeMateriel3_Translated() {
        return StringUtils.firstNonBlank(typeMateriel3_Translated, typeMateriel3);
    }

    @Override
    public String getTypeMateriel4_Translated() {
        return StringUtils.firstNonBlank(typeMateriel4_Translated, typeMateriel4);
    }

    @Override
    public String getUnite_Translated() {
        return StringUtils.firstNonBlank(unite_Translated, unite);
    }
} //RefMaterielImpl
