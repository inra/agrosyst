package fr.inra.agrosyst.api.services.action;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPriceSummary;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@SuperBuilder(toBuilder = true)
public class HarvestingPriceDto extends InputPriceDto {
    
    @Serial
    private static final long serialVersionUID = 98096620760942709L;
    
    String valorisationId;

    String destinationId;

    String destination;

    Boolean isOrganic;

    Double yealdAverage;

    YealdUnit yealdUnit;

    String practicedSystemId;

    String practicedSystemName;

    String zoneId;

    String zoneName;

    String domainId;
    
    String actionId;

    String growingSystemName;

    String speciesCode;

    String campaign;

    HarvestingValorisationPriceSummary averageReferencePricesForValorisations;

    HarvestingValorisationPriceSummary userSummaryPriceForValorisations;
    
    Collection<PriceUnit> allowedPriceUnitsForPrice;
    
    public Optional<String> getPracticedSystemId() {
        return Optional.ofNullable(practicedSystemId);
    }
    
    public Optional<String> getZoneId() {
        return Optional.ofNullable(zoneId);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HarvestingPriceDto that = (HarvestingPriceDto) o;
        return valorisationId.equals(that.valorisationId) && Objects.equals(practicedSystemId, that.practicedSystemId) && Objects.equals(zoneId, that.zoneId) && Objects.equals(domainId, that.domainId) && Objects.equals(actionId, that.actionId);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), valorisationId, practicedSystemId, zoneId, domainId, actionId);
    }
}
