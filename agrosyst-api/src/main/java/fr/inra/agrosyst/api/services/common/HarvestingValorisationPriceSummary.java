package fr.inra.agrosyst.api.services.common;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PriceUnit;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author David Cossé
 */
public class HarvestingValorisationPriceSummary implements Serializable {

    @Serial
    private static final long serialVersionUID = 0L;

    protected Double lowerPrice;

    protected Double higherPrice;

    protected int countedPrices;

    protected Double medianPrice;

    protected Double averagePrice;

    protected HashMap<Integer, Integer> nbPricesByCampaigns;

    protected PriceUnit priceUnit;

    protected final Map<String, String> sources = new HashMap<>();

    public Double getLowerPrice() {
        return lowerPrice;
    }

    public void setLowerPrice(Double lowerPrice) {
        this.lowerPrice = lowerPrice;
    }

    public Double getHigherPrice() {
        return higherPrice;
    }

    public void setHigherPrice(Double higherPrice) {
        this.higherPrice = higherPrice;
    }

    public int getCountedPrices() {
        return countedPrices;
    }

    public void setCountedPrices(int countedPrices) {
        this.countedPrices = countedPrices;
    }

    public Double getMedianPrice() {
        return medianPrice;
    }

    public void setMedianPrice(Double medianPrice) {
        this.medianPrice = medianPrice;
    }

    public Double getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(Double averagePrice) {
        this.averagePrice = averagePrice;
    }

    public Map<Integer, Integer> getNbPricesByCampaigns() {
        return nbPricesByCampaigns;
    }

    public void setNbPricesByCampaigns(Map<Integer, Integer> nbPricesByCampaigns) {
        this.nbPricesByCampaigns = new HashMap<>(nbPricesByCampaigns);
    }

    public PriceUnit getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(PriceUnit priceUnit) {
        this.priceUnit = priceUnit;
    }

    public Map<String, String> getSources() {
        return sources;
    }

    public void addSources(Map<String, String> sources) {
        this.sources.putAll(sources);
    }
}
