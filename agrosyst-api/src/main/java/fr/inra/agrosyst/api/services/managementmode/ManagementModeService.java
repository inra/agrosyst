package fr.inra.agrosyst.api.services.managementmode;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCrop;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

public interface ManagementModeService extends AgrosystService {

    Function<DecisionRuleCrop, String> GET_CROP_CODE = DecisionRuleCrop::getCroppingPlanEntryCode;

    String HISTORY_CROP_IDS_SEPARATOR = ", ";

    static Map<SectionType, BioAgressorType[]> bioAgressorTypeBySectionType() {
        Map<SectionType, BioAgressorType[]> result = new HashMap<>();
        result.put(SectionType.ADVENTICES, Referentials.ADVENTICE.getBioAgressorTypes().toArray(new BioAgressorType[0]));
        result.put(SectionType.MALADIES, Referentials.MALADIE.getBioAgressorTypes().toArray(new BioAgressorType[0]));
        result.put(SectionType.RAVAGEURS, Referentials.RAVAGEUR.getBioAgressorTypes().toArray(new BioAgressorType[0]));
        return result;
    }

    /**
     * Return the DecisionRules according the given filter.
     *
     * @param filter The DecisionRule filter
     * @return the decisionRules
     */
    PaginationResult<DecisionRulesDto> getFilteredDecisionRules(DecisionRuleFilter filter);

    /**
     * Return the DecisionRule ids according the given filter.
     *
     * @param filter The DecisionRule filter
     * @return the decisionRule ids
     */
    Set<String> getFilteredDecisionRuleIds(DecisionRuleFilter filter);

    /**
     * New decision rule instance.
     *
     * @return new decision rule instance
     */
    DecisionRule newDecisionRule();

    /**
     * Find decision rule instance by id.
     *
     * @param decisionRuleTopiaId topia id
     * @return decision rule
     */
    Optional<DecisionRule> getDecisionRule(String decisionRuleTopiaId);

    /**
     * Return an exact copie of the rule
     *
     * @param decisionRuleCode the code of the rule
     * @return the copy
     */
    DecisionRule getLastDecisionRuleVersion(String decisionRuleCode);

    /**
     * Find all cropping plan entries for domains with code as given in parameter
     * @param domainCode
     * @return
     */
    List<CroppingPlanEntry> getDomainCodeCroppingPlanEntries(String domainCode);

    /**
     * Find all cropping plan entries corresponding to
     *
     * @param growingSystemTopiaId growing system topia id
     * @return cropping plan entries
     */
    List<CroppingPlanEntry> getGrowingSystemCroppingPlanEntries(String growingSystemTopiaId);

    List<String> getCropIdsForGrowingSystemId(String growingSystemTopiaId);

    List<String> loadCropsNames(Set<String> croppingPlanEntryIds);

    /**
     * Update decision rule.
     */
    DecisionRule createOrUpdateDecisionRule(DecisionRule decisionRule, String domainCode, String bioAgressorTopiaId, List<DecisionRuleCrop> decisionRuleCrops);

    /**
     * Create decision rule with params given has parameters
     */
    DecisionRule createNewDecisionRule(
            AgrosystInterventionType interventionType,
            String growingSystemTopiaId,
            BioAgressorType bioAgressorType,
            String codeGroupeCibleMaa, String bioAgressorTopiaId,
            List<String> cropIds,
            String name);

    /**
     * @param decisionRuleCode the code of the source decisionRule
     * @param versionReason    the reason to create this new version
     * @return the created decisionRule
     */
    DecisionRule createNewDecisionRuleVersion(String decisionRuleCode, String versionReason);

    /**
     * Return the ManagementModes according the given filter.
     *
     * @param managementModeFilter The ManagementMode filter
     * @return the managementModes
     */
    PaginationResult<ManagementModeDto> getFilteredManagementModeDtos(ManagementModeFilter managementModeFilter);

    Set<String> getFilteredManagementModeIds(ManagementModeFilter managementModeFilter);

    /**
     * Create new management mode instance for creation.
     *
     * @return management mode instance
     */
    ManagementMode newManagementMode();

    /**
     * Find management mode instance from id.
     *
     * @param managementModeTopiaId topia id
     * @return management mode instance
     */
    ManagementMode getManagementMode(String managementModeTopiaId);

    /**
     * Update management mode.
     *
     * @param managementMode       management mode to update
     * @param growingSystemTopiaId growing system id
     * @param sections             section dto
     * @return updated management mode
     */
    ManagementMode createOrUpdateManagementMode(ManagementMode managementMode, String growingSystemTopiaId, Collection<SectionDto> sections);

    void createManagementModeFromReportGrowingSystem(ReportGrowingSystem reportGrowingSystem);

    /**
     * Get growing system's decision rules.
     *
     * @param growingSystemTopiaId growing system id
     * @return decision rules
     */
    Collection<DecisionRule> getGrowingSystemDecisionRules(String growingSystemTopiaId);

    /**
     * Find management mode by growing system.
     *
     * @param growingSystem growing system
     * @return management mode
     */
    ManagementMode getManagementModeForGrowingSystem(GrowingSystem growingSystem);

    Collection<ManagementMode> getManagementModeForGrowingSystemIds(Collection<String> growingSystemIds);

    Optional<String> getObservedManagementModeIdForGrowingSystemId(String growingSystemId);

    /**
     * Get decision rules version with his related rule's topidId liked to the given rule code
     *
     * @param code the rule code
     * @return the list of version for the rules related to the code.
     */
    List<DecisionRule> getRelatedDecisionRules(String code);

    /**
     * Return the categories associated with the GrowingSystem with same topiaId as given in parameter.
     *
     * @param growingSystemId GrowingSystem topiaId
     * @return
     */
    List<ManagementModeCategory> getAvailableManagementModeCategories(String growingSystemId);

    /**
     * Return ManagementMode related to growingSystem given as parameter
     * @param growingSystem the GrowingSystem to witch ManagementModes or attached
     * @return all related ManagementMode
     */
    List<ManagementMode> getRelatedManagementModes(GrowingSystem growingSystem);

    /**
     *
     * @param growingSystemId
     * @param category
     * @return
     */
    ManagementMode copyManagementMode(String growingSystemId, ManagementModeCategory category, String mainChangesFromPlanned, String changeReasonFromPlanned);

    List<GrowingSystem> getGrowingSystemsForManagementMode(NavigationContext navigationContext);

    List<GrowingSystem> getAvailableGsForDuplication(String growingSystemId, NavigationContext navigationContext);

    void unactivateDecisionRules(List<String> decisionRuleIds, boolean activate);

    DecisionRule duplicateDecisionRule(String decisionRuleIds);

    ManagementMode duplicateManagementModes(String plannedManagementModeTopiaId, String observedManagementMode, String duplicateManagementModeGrowingSystemId);

    /**
     * return all decision Rules from topiaIds given as parameters
     * @param decisionRulesIds
     * @return
     */
    List<DecisionRule> getAllDecisionRules(List<String> decisionRulesIds);

    ExportResult exportManagementModesAsXls(Collection<String> growingSystemIds);

    void exportManagementModesAsXlsAsync(Collection<String> growingSystemIds);

    ExportResult exportDecisionRulesAsXls(Collection<String> decisionRuleIds);

    void exportDecisionRulesAsXlsAsync(Collection<String> decisionRuleIds);

    List<RefStrategyLever> loadRefStrategyLevers(Sector sector, String growingSystemTopiaId, SectionType sectionType, StrategyType strategyType);

    List<RefStrategyLever> loadRefStrategyLeversForTerm(Sector sector, String growingSystemTopiaId, SectionType sectionType, StrategyType strategyType, String term);

    void removeManagementModes(Set<String> managementModeIds);

    PaginationResult<ManagementModeDto> loadWritableManagementModesForGrowingSystemIds(List<String> growingSystemIds);

    boolean isDecisionRuleActive(DecisionRule decisionRule);
}
