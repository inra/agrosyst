package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.BioAgressorType;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class BioAgressorParentType implements Serializable {
    
    @Serial
    private static final long serialVersionUID = -930321630539209700L;
    private final BioAgressorType parent;
    private final List<BioAgressorType> bioAgressorTypes;

    public BioAgressorParentType(BioAgressorType... bioAgressorTypes) {
        this.parent = bioAgressorTypes[0];
        this.bioAgressorTypes = Arrays.asList(bioAgressorTypes);
    }

    public BioAgressorType getParent() {
        return parent;
    }

    public List<BioAgressorType> getBioAgressorTypes() {
        return bioAgressorTypes;
    }
}
