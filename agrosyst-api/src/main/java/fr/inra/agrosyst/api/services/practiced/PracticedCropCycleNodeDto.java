package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class PracticedCropCycleNodeDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 6211894776472547970L;

    protected String nodeId;

    protected String croppingPlanEntryCode;
    protected String label;

    protected int x;
    protected int y;

    protected boolean endCycle;
    protected boolean sameCampaignAsPreviousNode;
    protected String type = "NODE"; // TODO AThimel 24/10/13 Remplacer par une enum (NODE_BEFORE, POINT_BEFORE, NODE)

    protected Double initNodeFrequency;

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getCroppingPlanEntryCode() {
        return croppingPlanEntryCode;
    }

    public void setCroppingPlanEntryCode(String croppingPlanEntryCode) {
        this.croppingPlanEntryCode = croppingPlanEntryCode;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isEndCycle() {
        return endCycle;
    }

    public void setEndCycle(boolean endCycle) {
        this.endCycle = endCycle;
    }

    public boolean isSameCampaignAsPreviousNode() {
        return sameCampaignAsPreviousNode;
    }

    public void setSameCampaignAsPreviousNode(boolean sameCampaignAsPreviousNode) {
        this.sameCampaignAsPreviousNode = sameCampaignAsPreviousNode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setInitNodeFrequency(Double initNodeFrequency) {
        this.initNodeFrequency = initNodeFrequency;
    }

    public Double getInitNodeFrequency() {
        return initNodeFrequency;
    }

}
