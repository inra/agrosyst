package fr.inra.agrosyst.api.services.growingplan;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public interface GrowingPlanService extends AgrosystService {

    /**
     * Create a new growing plan not persisted yet
     * @return the newly created Growing Plan
     */
    GrowingPlan newGrowingPlan();

    /**
     * Find all growing plan for a domain.
     *
     * @param domain domain
     * @return domain's gronwing plans
     */
    List<GrowingPlan> findAllByDomain(Domain domain);

    /**
     * Find a specific growingPlan from identifier.
     *
     * @param growningPlanId growingPlan id
     * @return growingPlan
     */
    GrowingPlan getGrowingPlan(String growningPlanId);

    /**
     * Update or create a GrowingPlan.
     *
     * @param dispositif growingPlan to update
     * @return growingPlan
     */
    GrowingPlan createOrUpdateGrowingPlan(GrowingPlan dispositif);

    /**
     * Find growingPlans matching user filter and navigation context.
     *
     * @param filter custom user filter
     * @return paginated list of growing plans matching the given filter
     */
    PaginationResult<GrowingPlan> getFilteredGrowingPlans(GrowingPlanFilter filter);

    /**
     * Find growingPlans matching user filter and navigation context.
     *
     * @param filter custom user filter
     * @return paginated list of growing plans DTO matching the given filter
     */
    PaginationResult<GrowingPlanDto> getFilteredGrowingPlansDto(GrowingPlanFilter filter);

    /**
     * Find growingPlans matching user filter and navigation context.
     *
     * @param filter custom user filter
     * @return The growing plan ids
     */
    Set<String> getFilteredGrowingPlanIds(GrowingPlanFilter filter);

    /**
     * Unactivate the given growingPlans.
     *
     * @param growingPlanIds growingPlans ids to unactivate
     * @param activate       activate
     */
    void unactivateGrowingPlans(List<String> growingPlanIds, boolean activate);

    /**
     * Duplicate growing plan from id and associate if with given domain id.
     * Copy associated growing systems if needed.
     *
     * @param growingPlanId           growing plan to duplicate
     * @param duplicateDomainId       associate duplicated growing plan to domain
     * @param duplicateGrowingSystems duplicate growing systems option
     */
    GrowingPlan duplicateGrowingPlan(String growingPlanId, String duplicateDomainId, boolean duplicateGrowingSystems);

    /**
     * Duplicate growing plan without committing transaction.
     *
     * @param extendContext           domain extends context
     * @param clonedDomain            cloned domain to attach growing plan to
     * @param growingPlan             plan to clone
     * @param duplicateGrowingSystems also duplicate growing plan's growing systems
     * @return duplicated growing plan
     */
    GrowingPlan duplicateGrowingPlan(ExtendContext extendContext, Domain clonedDomain, GrowingPlan growingPlan, boolean duplicateGrowingSystems);

    /**
     * Get growingPlan related to current growingPlan.
     * Related growingPlan got same duplication code.
     *
     * @param growingPlanCode the code identifying this growingPlan
     * @return related growingPlans
     */
    LinkedHashMap<Integer, String> getRelatedGrowingPlans(String growingPlanCode);

    /**
     * Return the number of growingPlans
     *
     * @return The number of growingPlan
     * @param active optional active filter (may be null)
     */
    long getGrowingPlansCount(Boolean active);

    /**
     * Do validate the current growing plan state
     *
     * @param growingPlanId the identifier of the growing plan to validate
     * @return the validated growing plan
     */
    GrowingPlan validateAndCommit(String growingPlanId);

    /**
     * Retourne les systemes de cultures associé au dispositifs.
     * 
     * @param growingPlanId growing plan id
     * @return growing plan's growing systems
     */
    List<GrowingSystem> getGrowingPlanGrowingSystems(String growingPlanId);

    List<GrowingPlan> getGrowingPlanWithName(String growingPlanName);

    ExportResult exportGrowingPlanAsXls(Collection<String> growingPlanIds);

    void exportGrowingPlanAsXlsAsync(Collection<String> growingPlanIds);

    GrowingPlan getOrCreateDefaultGrowingPlanForDomain(Domain domain);

    GrowingPlan getDefaultGrowingPlanForDomain(Domain domain);
}
