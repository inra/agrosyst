package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesuRunoffPotRulesParc;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.i18n.I18n;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformanceZoneExecutionContext {
    
    protected Pair<Optional<GrowingSystem>, Optional<GrowingSystem>> growingSystemAndAnonymizeGrowingSystem;
    // Optional.empty in case the zone is
    // not related
    // to a
    // growingSystem
    protected PerformancePlotExecutionContext plotContext;
    protected Pair<Zone, Zone> zoneAndAnonymizeZone;
    protected EffectiveSeasonalCropCycle seasonalCropCycle;
    protected List<EffectivePerennialCropCycle> perennialCropCycles;
    protected RefRcesuRunoffPotRulesParc rcesuRunoffPotRulesParc = null;// will be set when required
    
    protected Set<PerformanceEffectiveCropExecutionContext> performanceCropContextExecutionContexts;
    //protected Map<String, List<RefHarvestingPrice>> harvestingStandardizedPricesByKey;
    
    protected String irs;
    protected String its;
    
    protected Map<Pair<RefDestination, YealdUnit>, List<Double>> zoneYealds = new HashMap<>();
    protected Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald = new HashMap<>();
    
    // do not expose it to not use by error as it it only used into this class
    @Getter
    @Setter(AccessLevel.PRIVATE)
    private Set<CroppingPlanEntry> cropsUsedIntoTheCurrentIndicator = new HashSet<>();
    
    @Getter
    @Setter(AccessLevel.PRIVATE)
    private Class<?> currentIndicatorClass;
    private final List<HarvestingPrice> harvestingPrices;

    public PerformanceZoneExecutionContext(
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            Pair<Zone, Zone> zoneAndAnonymizeZone,
            PerformancePlotExecutionContext plotContext,
            EffectiveSeasonalCropCycle seasonalCropCycle,
            List<EffectivePerennialCropCycle> perennialCropCycle,
            List<HarvestingPrice> harvestingPrices) {
        
        this.growingSystemAndAnonymizeGrowingSystem = growingSystemContext.getGrowingSystemAndAnonymizeGrowingSystem();
        this.plotContext = plotContext;
        this.zoneAndAnonymizeZone = zoneAndAnonymizeZone;
        this.seasonalCropCycle = seasonalCropCycle;
        this.perennialCropCycles = perennialCropCycle;
    
        this.irs = growingSystemContext.getIrs();
        this.its = growingSystemContext.getIts();
        this.harvestingPrices = harvestingPrices;
    }

    public Zone getZone() {
        return zoneAndAnonymizeZone.getLeft();
    }

    public Zone getAnonymizeZone() {
        return zoneAndAnonymizeZone.getRight();
    }
    
    public Optional<GrowingSystem> getGrowingSystem() {
        return growingSystemAndAnonymizeGrowingSystem.getLeft();
    }
    
    public Optional<GrowingSystem> getAnonymizeGrowingSystem() {
        return growingSystemAndAnonymizeGrowingSystem.getRight();
    }

    public Plot getPlot() {return plotContext.getPlot();}

    public Plot getAnonymizePlot() {return plotContext.getAnonymizePlot();}
    
    public void addCropConcernByIndicator(Class<?> indicatorClass, CroppingPlanEntry croppingPlanEntry) {
        if (currentIndicatorClass == null || currentIndicatorClass != indicatorClass) {
            currentIndicatorClass = indicatorClass;
            cropsUsedIntoTheCurrentIndicator.clear();
        }
        cropsUsedIntoTheCurrentIndicator.add(croppingPlanEntry);
    }
    
    public String getZoneSpeciesNames() {
        List<String> speciesNames = cropsUsedIntoTheCurrentIndicator.stream()
                .filter(CroppingPlanEntry::isCroppingPlanSpeciesNotEmpty)
                .map(CroppingPlanEntry::getCroppingPlanSpecies)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(CroppingPlanSpecies::getSpecies)
                .filter(Objects::nonNull)
                .map(RefEspece::getLibelle_espece_botanique_Translated)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        return String.join(", ", speciesNames);
    }
    
    public String getZoneVarietiesNames() {
        List<String> varietiesNames = cropsUsedIntoTheCurrentIndicator.stream()
                .filter(CroppingPlanEntry::isCroppingPlanSpeciesNotEmpty)
                .map(CroppingPlanEntry::getCroppingPlanSpecies)
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(species -> {
                    String varietyLabel = null;
                    if (species.getVariety() != null) {
                        varietyLabel = species.getVariety().getLabel();
                    }
                    if (StringUtils.isBlank(varietyLabel) && StringUtils.isNotBlank(species.getEdaplosUnknownVariety())) {
                        varietyLabel = I18n.t("fr.inra.agrosyst.api.services.edaplos.unknownVariety", species.getEdaplosUnknownVariety());
                    }
                    return varietyLabel;
                })
                .filter(Objects::nonNull)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        return String.join(", ", varietiesNames);
    }
    
    public void addCropYealdAverage(Map<Pair<RefDestination, YealdUnit>, Double> yealdAverageForDestinationsAndUnits) {
        for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> yealdAverageForDestinationAndUnit : yealdAverageForDestinationsAndUnits.entrySet()) {
            Double value = yealdAverageForDestinationAndUnit.getValue();
            if (value != null) {
                Pair<RefDestination, YealdUnit> destinationAndUnit = yealdAverageForDestinationAndUnit.getKey();
                List<Double> yealds = zoneYealds.computeIfAbsent(destinationAndUnit, k -> new ArrayList<>());
                yealds.add(value);
            }
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformanceZoneExecutionContext that = (PerformanceZoneExecutionContext) o;
        return zoneAndAnonymizeZone.equals(that.zoneAndAnonymizeZone);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(zoneAndAnonymizeZone);
    }

    public List<HarvestingPrice> getHarvestingPrices() {
        return harvestingPrices;
    }
}
