package fr.inra.agrosyst.api.services.performance;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.BinaryStream;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.domain.PlotDto;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.plot.PlotFilter;
import fr.inra.agrosyst.api.services.plot.ZoneFilter;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Performance service.
 * 
 * @author Eric Chatellier
 */
public interface PerformanceService extends AgrosystService {

    PaginationResult<PerformanceDto> getFilteredPerformances(PerformanceFilter performanceFilter, int subItemsLimit);

    Performance newPerformance();

    Performance getPerformance(String performanceTopiaId);

    void saveState(String performanceId, PerformanceState state);

    BinaryStream downloadPerformances(String performanceTopiaId);

    PaginationResult<PlotDto> getPlotsDto(PlotFilter plotFilter);

    Set<String> getPlotIds(PlotFilter plotFilter);

    PaginationResult<ZoneDto> getZonesDto(ZoneFilter zoneFilter);

    Set<String> getZoneIds(ZoneFilter zoneFilter);

    Map<PracticedSystem, PracticedSeasonalCropCycle> getPracticedSeasonalCropCyclesByPracticedSystems(List<PracticedSystem> practicedSystems);

    Map<PracticedSystem, List<PracticedPerennialCropCycle>> getPracticedPerennialCropCyclesByPracticedSystems(List<PracticedSystem> practicedSystems);

    List<PracticedSystem> getPracticedSystemsForGrowingSystems(List<GrowingSystem> growingSystems);

    /**
     * Suppression d'une performance.
     * 
     * @param performanceIds performance id to delete
     */
    void deletePerformance(Collection<String> performanceIds);
    
    Performance createPerformanceToDb(List<String> filteredDomainIds);
    
    PerformanceState getPerformanceStatus(String performanceId);

    /**
     * Compute performance and generate performance file.
     * This method is called by {@code PerformanceThread}.
     *
     * @param performanceId performanceId to generate file
     */
    String generatePerformanceFile(String performanceId) throws Exception;

    Map<RefMateriel, RefAgsAmortissement> getDeprecationRateByEquipments(List<ToolsCoupling> toolsCouplings);
    
    List<PerformanceStatistics> getDbPerformancesStatistics(String performanceId);

    List<String> getQSAIndicatorClassNames();

    PriceAndUnit loadHarvestingPriceWithUnitRefPriceForValorisation(Optional<String> optionalPracticedSystemCampaigns,
                                                                    Optional<PriceUnit> priceUnit,
                                                                    String interventionId,
                                                                    HarvestingActionValorisation valorisation,
                                                                    Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);

    Performance createperformance(
            String name,
            List<String> domainIds,
            List<String> growingSystemIds,
            List<String> plotIds,
            List<String> zoneIds,
            List<IndicatorFilter> indicatorFilters,
            Set<String> scenarioCodes,
            boolean exportToDb,
            boolean isPracticed);

    Performance updatePerformance(
            String topiaId,
            String name,
            List<String> domainIds,
            List<String> growingSystemIds,
            List<String> plotIds,
            List<String> zoneIds,
            List<IndicatorFilter> indicatorFilters,
            Set<String> scenarioCodes,
            boolean exportToDb);

    void cancelPerformance(String performanceId);
}
