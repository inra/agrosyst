package fr.inra.agrosyst.api.services.network;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.GrowingSystemsIndicator;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class NetworkIndicators implements Serializable {

    @Serial
    private static final long serialVersionUID = -4917760932760266557L;

    protected long domainsCount;
    protected long activeDomainsCount;

    protected long growingPlansCount;
    protected long activeGrowingPlansCount;

    protected long subNetworksCount;
    protected long activeSubNetworksCount;

    protected List<GrowingSystemsIndicator> growingSystems;

    public long getDomainsCount() {
        return domainsCount;
    }

    public void setDomainsCount(long domainsCount) {
        this.domainsCount = domainsCount;
    }

    public long getActiveDomainsCount() {
        return activeDomainsCount;
    }

    public void setActiveDomainsCount(long activeDomainsCount) {
        this.activeDomainsCount = activeDomainsCount;
    }

    public long getGrowingPlansCount() {
        return growingPlansCount;
    }

    public void setGrowingPlansCount(long growingPlansCount) {
        this.growingPlansCount = growingPlansCount;
    }

    public long getActiveGrowingPlansCount() {
        return activeGrowingPlansCount;
    }

    public void setActiveGrowingPlansCount(long activeGrowingPlansCount) {
        this.activeGrowingPlansCount = activeGrowingPlansCount;
    }

    public long getSubNetworksCount() {
        return subNetworksCount;
    }

    public void setSubNetworksCount(long subNetworksCount) {
        this.subNetworksCount = subNetworksCount;
    }

    public long getActiveSubNetworksCount() {
        return activeSubNetworksCount;
    }

    public void setActiveSubNetworksCount(long activeSubNetworksCount) {
        this.activeSubNetworksCount = activeSubNetworksCount;
    }

    public List<GrowingSystemsIndicator> getGrowingSystems() {
        return growingSystems;
    }

    public void setGrowingSystems(List<GrowingSystemsIndicator> growingSystems) {
        this.growingSystems = growingSystems;
    }
}
