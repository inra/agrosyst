package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Sector;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 16/02/16.
 */
public class DestinationContext {

    private final Map<String, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifiant;
    private final Map<String, List<RefDestination>> destinationsByDestinationLabels;
    private final Map<String, RefQualityCriteria> qualityCriteria;
    private final Map<String, List<RefQualityCriteriaClass>> qualityCriteriaClasses;
    private final Map<String, List<Pair<String, String>>> allCodeEspeceBotaniqueCodeQualifantBySpeciesCode;
    private final Map<String, List<Sector>> allSectorByCodeEspeceBotaniqueCodeQualifiant;


    public DestinationContext(
            Map<Pair<String, String>,
                    List<Sector>> sectorsByCodeEspeceBotanique0,
            List<RefDestination> destinations,
            List<RefQualityCriteria> qualityCriteria,
            List<RefQualityCriteriaClass> qualityCriteriaClasses,
            Map<String, List<Pair<String, String>>> allCodeEspeceBotaniqueCodeQualifantBySpeciesCode,
            Map<String, List<Sector>> allSectorByCodeEspeceBotaniqueCodeQualifiant) {
        this.sectorsByCodeEspeceBotaniqueCodeQualifiant = getSectorByCodeEspceBotaniqueCodeQualifiantMap(sectorsByCodeEspeceBotanique0);
        this.destinationsByDestinationLabels = setDestinationsByLabels(destinations);
        this.qualityCriteria = Maps.uniqueIndex(qualityCriteria, Entities.GET_TOPIA_ID::apply);
        this.allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = allCodeEspeceBotaniqueCodeQualifantBySpeciesCode;
        this.allSectorByCodeEspeceBotaniqueCodeQualifiant = allSectorByCodeEspeceBotaniqueCodeQualifiant;
        this.qualityCriteriaClasses = new HashMap<>();
        for (RefQualityCriteriaClass qualityCriteriaClass : qualityCriteriaClasses) {
            List<RefQualityCriteriaClass> classesForRefQualityCriteria = this.qualityCriteriaClasses.computeIfAbsent(qualityCriteriaClass.getRefQualityCriteriaCode(), k -> Lists.newArrayList());
            classesForRefQualityCriteria.add(qualityCriteriaClass);
        }
    }

    public DestinationContext() {
        sectorsByCodeEspeceBotaniqueCodeQualifiant = new HashMap<>();
        destinationsByDestinationLabels = new HashMap<>();
        qualityCriteria = new HashMap<>();
        qualityCriteriaClasses = new HashMap<>();
        allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = new HashMap<>();
        allSectorByCodeEspeceBotaniqueCodeQualifiant = new HashMap<>();
    }

    private Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiantMap(Map<Pair<String, String>, List<Sector>> sectorsByCodeEspeceBotanique0) {
        Map<String, List<Sector>> sectorsByCodeEspeceBotaniqueCodeQualifiant_ = new HashMap<>();
        if (sectorsByCodeEspeceBotanique0 != null) {
            for (Map.Entry<Pair<String, String>, List<Sector>> codeEspeceBotCodeQualiSector : sectorsByCodeEspeceBotanique0.entrySet()) {
                Pair<String, String> codes = codeEspeceBotCodeQualiSector.getKey();
                List<Sector> sectors = codeEspeceBotCodeQualiSector.getValue();
                String key = codes.getLeft()  + Strings.nullToEmpty(codes.getRight());
                sectorsByCodeEspeceBotaniqueCodeQualifiant_.put(key, sectors);
            }
        }
        return sectorsByCodeEspeceBotaniqueCodeQualifiant_;
    }

    protected Map<String, List<RefDestination>> setDestinationsByLabels(List<RefDestination> destinations) {
        Map<String, List<RefDestination>> destinationsByLabel = new HashMap<>();
        for (RefDestination destination : destinations) {
            String label = destination.getDestination_Translated();
            List<RefDestination> destinationsWithSameLabel = destinationsByLabel.computeIfAbsent(label, k -> Lists.newArrayList());
            destinationsWithSameLabel.add(destination);
        }
        return destinationsByLabel;
    }

    public Map<String, List<RefDestination>> getDestinationsByDestinationLabels() {
        return destinationsByDestinationLabels;
    }

    public Map<String, RefQualityCriteria> getQualityCriteria() {
        return qualityCriteria;
    }

    public Map<String, List<RefQualityCriteriaClass>> getQualityCriteriaClasses() {
        return qualityCriteriaClasses;
    }

    public Map<String, List<Sector>> getSectorsByCodeEspeceBotaniqueCodeQualifiant() {
        return sectorsByCodeEspeceBotaniqueCodeQualifiant;
    }

    public Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCode() {
        return allCodeEspeceBotaniqueCodeQualifantBySpeciesCode;
    }

    public Map<String, List<Sector>> getAllSectorByCodeEspeceBotaniqueCodeQualifiant() {
        return allSectorByCodeEspeceBotaniqueCodeQualifiant;
    }
}
