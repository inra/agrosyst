package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.io.Serial;

public class RefInterventionAgrosystTravailEDIImpl extends RefInterventionAgrosystTravailEDIAbstract {

    @Serial
    private static final long serialVersionUID = 1L;

    private String reference_label_Translated;
    public void setReference_label_Translated(String libelle) {
        reference_label_Translated = libelle;
    }

    public String getReference_label_Translated() {
        return StringUtils.firstNonBlank(reference_label_Translated, this.reference_label);
    }
} //RefInterventionAgrosystTravailEDIImpl
