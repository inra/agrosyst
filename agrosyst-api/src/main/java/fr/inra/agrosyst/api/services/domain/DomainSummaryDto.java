package fr.inra.agrosyst.api.services.domain;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class DomainSummaryDto implements Serializable {

    private static final Log LOGGER = LogFactory.getLog(DomainSummaryDto.class);
    @Serial
    private static final long serialVersionUID = -3492393611179727580L;
    
    protected final String topiaId;
    protected final String name;
    protected final int campaign;
    protected boolean hasValidFieldProperties;
    protected boolean hasValidEquipments;
    protected boolean hasValidCrops;
    protected boolean hasValidIputs;
    protected boolean hasValidPrices;
    protected List<CroppingSystem> croppingSystemsAndManagements = new ArrayList<>();
    protected boolean hasPerformanceReadyToDownload;

    public record CroppingSystem(
            String practicedSystemId,
            String practicedSystemName,
            boolean filled
    ) implements Serializable {}

}
