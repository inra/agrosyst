package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

@Getter
@EqualsAndHashCode
public class CropWithSpecies {

    // a crop that is representative for all campaigns
    protected final CroppingPlanEntry croppingPlanEntry;
    // All species that can be found on the crops with same code over practiced system campaigns
    protected final Set<CroppingPlanSpecies> croppingPlanSpecies;
    
    public CropWithSpecies(CroppingPlanEntry croppingPlanEntry) {
        this.croppingPlanEntry = croppingPlanEntry;
        this.croppingPlanSpecies = new HashSet<>();
    }
    
    public CropWithSpecies(CroppingPlanEntry croppingPlanEntry, Set<CroppingPlanSpecies> croppingPlanSpecies) {
        this.croppingPlanEntry = croppingPlanEntry;
        this.croppingPlanSpecies = croppingPlanSpecies == null ? new HashSet<>() : croppingPlanSpecies;
    }
}
