package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.io.Serial;

public class RefEspeceImpl extends RefEspeceAbstract {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 1L;

    private String libelle_espece_botanique_Translated;
    private String libelle_qualifiant_AEE_Translated;
    private String libelle_type_saisonnier_AEE_Translated;
    private String libelle_destination_AEE_Translated;

    @Override
    public void setLibelle_espece_botanique_Translated(String libelle){
        libelle_espece_botanique_Translated = libelle;;
    };

    @Override
    public void setLibelle_qualifiant_AEE_Translated(String libelle){
        libelle_qualifiant_AEE_Translated = libelle;
    };

    @Override
    public void setLibelle_type_saisonnier_AEE_Translated(String libelle){
        libelle_type_saisonnier_AEE_Translated = libelle;
    };

    @Override
    public void setLibelle_destination_AEE_Translated(String libelle){
        libelle_destination_AEE_Translated = libelle;
    };

    @Override
    public String getLibelle_espece_botanique_Translated(){
        return StringUtils.firstNonBlank(libelle_espece_botanique_Translated, this.libelle_espece_botanique);
    };

    @Override
    public String getLibelle_qualifiant_AEE_Translated(){
        return StringUtils.firstNonBlank(libelle_qualifiant_AEE_Translated, this.libelle_qualifiant_AEE);
    };

    @Override
    public String getLibelle_type_saisonnier_AEE_Translated(){
        return StringUtils.firstNonBlank(libelle_type_saisonnier_AEE_Translated, this.libelle_type_saisonnier_AEE);
    };

    @Override
    public String getLibelle_destination_AEE_Translated(){
        return StringUtils.firstNonBlank(libelle_destination_AEE_Translated, this.libelle_destination_AEE);
    };

}
