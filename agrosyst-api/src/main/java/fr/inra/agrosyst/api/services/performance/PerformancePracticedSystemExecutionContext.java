package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesuRunoffPotRulesParc;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformancePracticedSystemExecutionContext {

    protected final Pair<PracticedSystem, PracticedSystem> practicedSystemAndAnonymizePracticedSystem;
    protected final PracticedSeasonalCropCycle seasonalCropCycle;
    protected final List<PracticedPerennialCropCycle> practicedPerennialCropCycles;
    protected final PracticedPlot practicedPlot;
    protected final RefRcesuRunoffPotRulesParc rcesuRunoffPotRulesParc = null;// will be set when required
    protected final long seasonalNbCampaign;

    @Getter
    protected final List<HarvestingPrice> harvestingPrices;

    protected final MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode;
    protected final MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM;

    @Setter
    protected Map<PracticedCropCycleConnection, Double> cumulativeFrequenciesByConnection;
    protected Map<String, CropWithSpecies> cropByCodeWithSpecies;
    protected Map<String, CropWithSpecies> cropByCodeWithSpeciesFallback;

    protected final Collection<String> codeAmmBioControle;

    protected final RefCampaignsInputPricesByDomainInput practicedSystemInputRefPricesByDomainInput;

    protected Set<PracticedCropPath> practicedCropPaths;// set when building seasonal cycle context
    protected Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts;

    public PerformancePracticedSystemExecutionContext(Pair<PracticedSystem, PracticedSystem> practicedSystemAndAnonymizePracticedSystem,
                                                      PracticedSeasonalCropCycle seasonalCropCycle,
                                                      long seasonalNbCampaign,
                                                      List<PracticedPerennialCropCycle> practicedPerennialCropCycles,
                                                      PracticedPlot practicedPlot,
                                                      List<HarvestingPrice> harvestingPrices,
                                                      Collection<String> codeAmmBioControle,
                                                      RefCampaignsInputPricesByDomainInput practicedSystemInputRefPricesByDomainInput,
                                                      MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode,
                                                      MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM) {
        this.practicedSystemAndAnonymizePracticedSystem = practicedSystemAndAnonymizePracticedSystem;
        this.seasonalCropCycle = seasonalCropCycle;
        this.seasonalNbCampaign = seasonalNbCampaign;
        this.practicedPerennialCropCycles = practicedPerennialCropCycles;
        this.practicedPlot = practicedPlot;
        this.harvestingPrices = harvestingPrices;
        this.codeAmmBioControle = codeAmmBioControle;
        this.practicedSystemInputRefPricesByDomainInput = practicedSystemInputRefPricesByDomainInput;
        this.allSubstancesActivesCommissionEuropeenneByAmmCode = allSubstancesActivesCommissionEuropeenneByAmmCode;
        this.refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM = refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM;
    }

    public PracticedSystem getPracticedSystem() {
        return practicedSystemAndAnonymizePracticedSystem.getLeft();
    }

    public PracticedSystem getAnonymizePracticedSystem() {
        return practicedSystemAndAnonymizePracticedSystem.getRight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformancePracticedSystemExecutionContext that = (PerformancePracticedSystemExecutionContext) o;
        if (practicedSystemAndAnonymizePracticedSystem.equals(that.practicedSystemAndAnonymizePracticedSystem))
            return true;
        return practicedSystemAndAnonymizePracticedSystem.getLeft().equals(that.practicedSystemAndAnonymizePracticedSystem.getLeft());
    }

    @Override
    public int hashCode() {
        return Objects.hash(practicedSystemAndAnonymizePracticedSystem.getLeft());
    }

}

