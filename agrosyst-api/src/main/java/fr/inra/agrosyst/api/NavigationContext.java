/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.api;


import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import org.apache.commons.lang3.StringUtils;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Cette classe sert de conteneur pour les filtres de navigation dans l'IHM
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class NavigationContext implements Serializable {
    
    @Serial
    private static final long serialVersionUID = -9075131452675769558L;
    private final HashSet<Integer> campaigns = new HashSet<>();
    private final HashSet<String> networks = new HashSet<>();
    private final HashSet<String> domains = new HashSet<>();
    private final HashSet<String> growingPlans = new HashSet<>();
    private final HashSet<String> growingSystems = new HashSet<>();

    public NavigationContext() {
        
    }
    
    public NavigationContext(Set<Integer> campaigns, Set<String> networks, Set<String> domains, Set<String> growingPlans, Set<String> growingSystems) {
        if (campaigns != null) this.campaigns.addAll(campaigns);
        if (networks != null) {
            networks.forEach(id -> this.networks.add(StringUtils.remove(id, Network.class.getName())));
        }
        if (domains != null) {
            domains.forEach(id -> this.domains.add(StringUtils.remove(id, Domain.class.getName())));
        }
        if (growingPlans != null) {
            growingPlans.forEach(id -> this.growingPlans.add(StringUtils.remove(id, GrowingPlan.class.getName())));
        }
        if (growingSystems != null) {
            growingSystems.forEach(id -> this.growingSystems.add(StringUtils.remove(id, GrowingSystem.class.getName())));
        }
    }
    
    /**
     * Create new navigation context instance by coping other navigation context values.
     * 
     * @param other other navigation context to copy
     */
    public NavigationContext(NavigationContext other) {
        this();
        campaigns.addAll(other.campaigns);
        other.networks.forEach(id -> this.networks.add(StringUtils.remove(id, Network.class.getName())));
        other.domains.forEach(id -> this.domains.add(StringUtils.remove(id, Domain.class.getName())));
        other.growingPlans.forEach(id -> this.growingPlans.add(StringUtils.remove(id, GrowingPlan.class.getName())));
        other.growingSystems.forEach(id -> this.growingSystems.add(StringUtils.remove(id, GrowingSystem.class.getName())));
    }

    public int getCampaignsCount() {
        return campaigns.size();
    }

    public Set<Integer> getCampaigns() {
        return campaigns;
    }

    public int getNetworksCount() {
        return networks.size();
    }

    public Set<String> getNetworks() {
        HashSet<String> minIds = new LinkedHashSet<>(networks);
        networks.clear();
        minIds.forEach(minId -> networks.add(StringUtils.prependIfMissing(minId, Network.class.getName())));
        return networks;
    }

    public int getDomainsCount() {
        return domains.size();
    }

    public Set<String> getDomains() {
        HashSet<String> minIds = new LinkedHashSet<>(domains);
        domains.clear();
        minIds.forEach(minId -> domains.add(StringUtils.prependIfMissing(minId, Domain.class.getName())));
        return domains;
    }
    
    public int getGrowingPlansCount() {
        return growingPlans.size();
    }

    public Set<String> getGrowingPlans() {
        HashSet<String> minIds = new LinkedHashSet<>(growingPlans);
        growingPlans.clear();
        minIds.forEach(minId -> growingPlans.add(StringUtils.prependIfMissing(minId, GrowingPlan.class.getName())));
        return growingPlans;
    }
    
    public int getGrowingSystemsCount() {
        return growingSystems.size();
    }

    public Set<String> getGrowingSystems() {
        HashSet<String> minIds = new LinkedHashSet<>(growingSystems);
        growingSystems.clear();
        minIds.forEach(minId -> growingSystems.add(StringUtils.prependIfMissing(minId, GrowingSystem.class.getName())));
        return growingSystems;
    }
    
}
