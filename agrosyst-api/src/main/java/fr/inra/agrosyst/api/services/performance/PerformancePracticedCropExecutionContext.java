package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformancePracticedCropExecutionContext extends AbstractPerformanceCropContext {
    
    // because species can be different over the campaigns
    // the following pairs contains all species found for the given crop for the practiced campaigns
    protected CropWithSpecies cropWithSpecies;
    protected CropWithSpecies intermediateCropWithSpecies;

    // for perennial
    protected PracticedPerennialCropCycle practicedPerennialCropCycle;

    // for seasonnal
    protected Set<PerformancePracticedInterventionExecutionContext> interventionExecutionContexts;

    protected CroppingPlanEntry seasonalPreviousCrop;
    protected PracticedCropCycleConnection connection;
    protected int rank;
    protected double cummulativeFrequencyForCrop;
    protected Map<PracticedCropCycleConnection, Double> cumulativeFrequenciesByConnection;
    protected long nbSeasonalRank;
    protected boolean isNotUsedForThisCampaign;
    
    protected Set<RefEspece> refEspeces;

    protected Map<TraitementProduitWithSpecies, ReferenceDoseDTO> doseForProducts;

    protected RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop;// IPHY (disable for the moment)
    protected RefCultureEdiGroupeCouvSol intermediateSpeciesMaxCouvSolForCrop;

    protected WeedType weedType;

    // use to display yeald Average for destination/unit
    protected Map<Pair<RefDestination, YealdUnit>, List<Double>> interventionsYealds = new HashMap<>();
    protected Map<Pair<RefDestination, YealdUnit>, List<Double>> intermediateInterventionsYealds = new HashMap<>();
    // intermediate + main yeald Averages
    protected Map<Pair<RefDestination, YealdUnit>, Double> mainCropYealds = null;
    protected Map<Pair<RefDestination, YealdUnit>, Double> intermediateCropYealds = null;

    // contructor for seasonal
    public PerformancePracticedCropExecutionContext(CropWithSpecies cropWithSpecies,
                                                    CroppingPlanEntry seasonalPreviousCrop,
                                                    PracticedCropCycleConnection connection,
                                                    int rank,
                                                    CropWithSpecies intermediateCrop,
                                                    Set<RefEspece> refEspeces,
                                                    Map<PracticedCropCycleConnection, Double> cumulativeFrequenciesByConnection,
                                                    Double cummulativeFrequencyForCrop,
                                                    long nbSeasonalRank,
                                                    RefCultureEdiGroupeCouvSol speciesMaxCouvSolForCrop,
                                                    RefCultureEdiGroupeCouvSol intermediateSpeciesMaxCouvSolForCrop) {
        
        this.cropWithSpecies = cropWithSpecies;
        this.seasonalPreviousCrop = seasonalPreviousCrop;
        this.connection = connection;
        this.rank = rank;
        this.intermediateCropWithSpecies = intermediateCrop;
        this.refEspeces = refEspeces;
        this.cumulativeFrequenciesByConnection = cumulativeFrequenciesByConnection;
        this.cummulativeFrequencyForCrop = cummulativeFrequencyForCrop;
        this.nbSeasonalRank = nbSeasonalRank;
        this.speciesMaxCouvSolForCrop = speciesMaxCouvSolForCrop;
        this.intermediateSpeciesMaxCouvSolForCrop = intermediateSpeciesMaxCouvSolForCrop;
        this.isNotUsedForThisCampaign = connection.isNotUsedForThisCampaign();
    }

    // constructor for perennial
    public PerformancePracticedCropExecutionContext(CropWithSpecies cropWithSpecies,
                                                    PracticedPerennialCropCycle practicedPerennialCropCycle,
                                                    Set<RefEspece> refEspeces,
                                                    WeedType weedType) {
        this.cropWithSpecies = cropWithSpecies;
        this.practicedPerennialCropCycle = practicedPerennialCropCycle;
        this.refEspeces = refEspeces;
        this.weedType = weedType;
    }
    
    public void addInterventionYealdAverage(Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage) {
        for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> forDestinationAndUnitYealdAverage : interventionYealdAverage.entrySet()) {
            Pair<RefDestination, YealdUnit> forDestinationAndUnit = forDestinationAndUnitYealdAverage.getKey();
            List<Double> interventionsYealdsValues = interventionsYealds.computeIfAbsent(forDestinationAndUnit, k -> new ArrayList<>());
            interventionsYealdsValues.add(forDestinationAndUnitYealdAverage.getValue());
        }
    }

    public void addIntermediateInterventionYealdAverage(Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage) {
        for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> forDestinationAndUnitYealdAverage : interventionYealdAverage.entrySet()) {
            Pair<RefDestination, YealdUnit> forDestinationAndUnit = forDestinationAndUnitYealdAverage.getKey();
            List<Double> interventionsYealdsValues = intermediateInterventionsYealds.computeIfAbsent(forDestinationAndUnit, k -> new ArrayList<>());
            interventionsYealdsValues.add(forDestinationAndUnitYealdAverage.getValue());
        }
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformancePracticedCropExecutionContext that = (PerformancePracticedCropExecutionContext) o;
        return rank == that.rank &&
                cropWithSpecies.getCroppingPlanEntry().equals(that.cropWithSpecies.getCroppingPlanEntry()) &&
                Objects.equals(practicedPerennialCropCycle, that.practicedPerennialCropCycle) &&
                Objects.equals(connection, that.connection);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(cropWithSpecies.getCroppingPlanEntry(), practicedPerennialCropCycle, connection, rank);
    }
}
