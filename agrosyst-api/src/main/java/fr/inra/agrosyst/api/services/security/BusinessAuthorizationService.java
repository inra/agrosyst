package fr.inra.agrosyst.api.services.security;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import org.apache.commons.lang3.tuple.Pair;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public interface BusinessAuthorizationService extends AuthorizationService {

    boolean isAdmin();

    boolean isIsDataProcessor();

    boolean isDomainWritable(String domainId);

    boolean areDomainPlotsEditable(String domainId);

    boolean isDomainAdministrable(String domainCode);

    boolean isGrowingPlanWritable(String growingPlanId);

    boolean isGrowingPlanAdministrable(String growingPlanCode);

    boolean isGrowingSystemWritable(String growingSystemId);

    boolean isGrowingSystemAdministrable(String growingSystemCode);

    boolean isNetworkWritable(String networkId);

    boolean isPracticedSystemWritable(String practicedSystemId);

    boolean isDecisionRuleWritable(String decisionRuleId);

    boolean isManagementModeWritable(String managementModeId);

    boolean isManagementModeReadable(String managementModeId);

    // https://forge.codelutin.com/issues/4439
    boolean isDecisionRuleReadable(String decisionRuleId);

    boolean isZoneWritable(String zoneId);

    void domainCreated(Domain domain);
    
    void edaplosDomainCreated(Domain domain);
    
    void growingPlanCreated(GrowingPlan growingPlan);

    void growingSystemCreated(GrowingSystem growingSystem);

    void networkCreated(Network network);

    void networkUpdated(Network network);

    void checkIsAdmin() throws AgrosystAccessDeniedException;

    boolean shouldAnonymizeDomain(String domainId, boolean allowUnreadable);

    Pair<Boolean, Boolean> getShouldAnonymizeCanReadDomain(String domainId, boolean allowUnreadable);
//    boolean shouldAnonymizeDomainByCode(String domainCode);

    boolean shouldAnonymizeGrowingPlan(String growingPlanId, boolean allowUnreadable);

    Pair<Boolean, Boolean> getShouldAnonymizeCanReadGrowingPlan(String growingPlanId, boolean allowUnreadable);
//    boolean shouldAnonymizeGrowingPlanByCode(String growingPlanCode);

    void checkDomainReadable(String domainId) throws AgrosystAccessDeniedException;

    void checkGrowingPlanReadable(String growingPlanId) throws AgrosystAccessDeniedException;

    boolean isGrowingSystemReadable(String growingSystemId);
    void checkGrowingSystemReadable(String growingSystemId) throws AgrosystAccessDeniedException;

    void checkPracticedSystemReadable(String practicedSystemId) throws AgrosystAccessDeniedException;

    void checkDecisionRuleReadable(String decisionRuleId) throws AgrosystAccessDeniedException;

    void checkManagementModeReadable(String managementModeId) throws AgrosystAccessDeniedException;

    void checkEffectiveCropCyclesReadable(String zoneId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdateDomain(String domainId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdateGrowingPlan(String growingPlanId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdateGrowingSystem(String growingSystemId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdateNetwork(String networkId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdatePracticedSystem(String practicedSystemTopiaId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdateDecisionRule(String decisionRuleId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdateManagementMode(String managementModeId) throws AgrosystAccessDeniedException;

    void checkCreateOrUpdateEffectiveCropCycles(String zoneId) throws AgrosystAccessDeniedException;

    boolean isDomainValidable(String domainId);
    boolean isGrowingPlanValidable(String growingPlanId);
    boolean isGrowingSystemValidable(String growingSystemId);
    boolean isPracticedSystemValidable(String practicedSystemId);

    void checkValidateDomain(String domainId) throws AgrosystAccessDeniedException;
    void checkValidateGrowingPlan(String growingPlanId) throws AgrosystAccessDeniedException;
    void checkValidateGrowingSystem(String growingSystemId) throws AgrosystAccessDeniedException;
    void checkValidatePracticedSystem(String practicedSystemId) throws AgrosystAccessDeniedException;

    boolean areAttachmentsAddableOrDeletable(String objectReferenceId);

    void checkDeleteAttachment(String attachmentMetadataId);
    void checkAddAttachment(String objectReferenceId);
    void checkReadAttachment(String attachmentMetadataId);

    boolean isPerformanceWritable(String performanceId);
    void checkPerformanceReadable(String performanceId) throws AgrosystAccessDeniedException;
    void checkCreateOrUpdatePerformance(String performanceId) throws AgrosystAccessDeniedException;

    boolean isPlotWritable(String plotId);
    void checkPlotReadable(String plotId) throws AgrosystAccessDeniedException;
    void checkCreateOrUpdatePlot(String plotId) throws AgrosystAccessDeniedException;

    boolean isPracticedPlotWritable(String practicedPlotId);
    void checkPracticedPlotReadable(String practicedPlotId) throws AgrosystAccessDeniedException;
    void checkCreateOrUpdatePracticedPlot(String practicedPlotId, String practicedSystemId) throws AgrosystAccessDeniedException;

    void checkReportRegionalReadable(String reportRegionalTopiaId) throws AgrosystAccessDeniedException;
    void checkCreateOrUpdateReportRegional(String reportRegionalId) throws AgrosystAccessDeniedException;

    void checkDeleteReportRegional(String reportRegionalId);

    boolean isReportRegionalWritable(String reportRegionalTopiaId);
    void reportRegionalCreated(ReportRegional reportRegional);

    void checkReportGrowingSystemReadable(String reportGrowingSystemId) throws AgrosystAccessDeniedException;
    void checkCreateOrUpdateReportGrowingSystem(String reportGrowingSystemId) throws AgrosystAccessDeniedException;
    boolean isReportGrowingSystemWritable(String reportRegionalTopiaId);
    void checkDeleteReportGrowingSystem(String reportGrowingSystemId);

}
