package fr.inra.agrosyst.api.services.action;
/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class TillageActionDto extends AbstractActionDto {
    @Serial
    private static final long serialVersionUID = -7216367112203286789L;
    
    /**
     * Nom de l'attribut en BD : tillageDepth
     */
    protected Double tillageDepth;
    
    /**
     * Nom de l'attribut en BD : otherSettingTool
     */
    protected String otherSettingTool;
    
}
