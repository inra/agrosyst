package fr.inra.agrosyst.api.services.users;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.security.RoleType;
import org.immutables.value.Value;

import java.util.Locale;
import java.util.Optional;
import java.util.Set;

@Value.Immutable
public interface AuthenticatedUser {

    String getTopiaId();

    String getFirstName();

    String getLastName();

    String getEmail();

    Set<RoleType> getRoles();

    boolean isAcceptedCharter();

    Optional<String> getBanner();

    Optional<String> getItEmail();

    Language getLanguage();

    /**
     * Identifiant de la "session" pour l'authentification utilisateur en cours . Une "session" débute au moment où
     * l'utilisateur se connecte avec l'action de Login et se termine à l'expiration ou la révocation du cookie/token.
     */
    String getSid();

    default boolean isAdmin() {
        boolean result = getRoles().contains(RoleType.ADMIN);
        return result;
    }

    default boolean isISDataProcessor() {
        boolean result = getRoles().contains(RoleType.IS_DATA_PROCESSOR);
        return result;
    }

    default Locale getLocale() {
        return getLanguage().getLocale();
    }
}
