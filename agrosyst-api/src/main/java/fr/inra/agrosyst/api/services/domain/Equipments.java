package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class Equipments {

    public static final Function<ToolsCoupling, ToolsCouplingDto> TOOLS_COUPLING_TO_TOOLS_COUPLING_DTO = toolsCoupling -> {
        ToolsCouplingDto toolsCouplingDto = new ToolsCouplingDto();
        toolsCouplingDto.setTopiaId(toolsCoupling.getTopiaId());
        toolsCouplingDto.setCode(toolsCoupling.getCode());
        toolsCouplingDto.setToolsCouplingName(toolsCoupling.getToolsCouplingName());
        toolsCouplingDto.setMainsActions(toolsCoupling.getMainsActions());
        toolsCouplingDto.setWorkforce(toolsCoupling.getWorkforce());
        toolsCouplingDto.setComment(toolsCoupling.getComment());
        toolsCouplingDto.setWorkRate(toolsCoupling.getWorkRate());
        toolsCouplingDto.setWorkRateUnit(toolsCoupling.getWorkRateUnit());
        toolsCouplingDto.setManualIntervention(toolsCoupling.isManualIntervention());
        toolsCouplingDto.setTransitVolume(toolsCoupling.getTransitVolume());
        toolsCouplingDto.setBoiledQuantity(toolsCoupling.getBoiledQuantity());
        toolsCouplingDto.setAntiDriftNozzle(toolsCoupling.isAntiDriftNozzle());

        if (toolsCoupling.getTractor() != null) {
            toolsCouplingDto.setTractorId(toolsCoupling.getTractor().getTopiaId());
        }

        Collection<Equipment> equipments = toolsCoupling.getEquipments();
        HashSet<String> equipmentsIds = Sets.newHashSet();
        if (equipments != null && !equipments.isEmpty()) {
            Map<String, Equipment> indexedEquipments = Maps.uniqueIndex(equipments, Entities.GET_TOPIA_ID::apply);
            equipmentsIds.addAll(indexedEquipments.keySet());
            Equipment equipment = equipments.iterator().next();
            if (equipment.getRefMateriel() instanceof RefMaterielOutil) {
                MaterielTransportUnit materielTransportUnit = ((RefMaterielOutil)equipment.getRefMateriel()).getDonneesTransport2Unite();
                toolsCouplingDto.setMaterielTransportUnit(materielTransportUnit);
            }
        }
        toolsCouplingDto.setEquipementsKeys(equipmentsIds);

        return toolsCouplingDto;
    };

    public static final Function<ToolsCoupling, String> GET_TOOLS_COUPLING_CODE = ToolsCoupling::getCode;

    public static Set<String> getRefMaterielTopiaIds(Collection<ToolsCoupling> toolsCouplings) {
        Set<String> topiaIds = new HashSet<>();
        toolsCouplings.forEach(tc -> {
            Collection<Equipment> equipments = tc.getEquipments();
            if (equipments != null) {
                equipments.forEach(e -> topiaIds.add(e.getRefMateriel().getTopiaId()));
            }
        });
        return topiaIds;
    }

}
