package fr.inra.agrosyst.api.services.common;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 30/05/17.
 */
@Getter
public class LoadedPricesResult {
    
    @Getter
    protected Map<HarvestingPrice, RefHarvestingPrice> prices = new HashMap<>();
    protected List<HarvestingPriceDto> harvestingValorisationPrices = new ArrayList<>();


    public void setPrices(Map<HarvestingPrice, RefHarvestingPrice> prices) {
        this.prices = new HashMap<>(prices);
    }

    public void setHarvestingValorisationPrices(List<HarvestingPriceDto> harvestingValorisationPrices) {
        if (CollectionUtils.isNotEmpty(harvestingValorisationPrices)) {
            this.harvestingValorisationPrices.addAll(harvestingValorisationPrices);
        }
    }
    
}
