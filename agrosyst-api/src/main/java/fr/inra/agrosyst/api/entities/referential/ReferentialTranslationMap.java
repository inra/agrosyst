package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReferentialTranslationMap {

    protected final Language language;

    protected final Map<String, ReferentialEntityTranslation> map = new HashMap<>();

    public ReferentialTranslationMap(Language language) {
        this.language = language;
    }

    public Language getLanguage() {
        return language;
    }

    public Locale getLocale() {
        return language.getLocale();
    }

    public ReferentialEntityTranslation getEntityTranslation(String topiaId) {
        return map.getOrDefault(topiaId, new ReferentialEntityTranslation(topiaId));
    }

    public void setEntityTranslation(String topiaId, ReferentialEntityTranslation entityTranslation) {
        map.put(topiaId, entityTranslation);
    }

}
