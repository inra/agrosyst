package fr.inra.agrosyst.api.services.action;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.io.Serializable;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class AbstractActionDto implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 5822493839500263483L;
    
    /**
     * RefInterventionAgrosystTravailEDI Id
     */
    @NonNull
    protected String mainActionId;
    
    /**
     * RefInterventionAgrosystTravailEDI.reference_code
     */
    @NonNull
    protected String mainActionReference_code;
    
    /**
     * RefInterventionAgrosystTravailEDI.intervention_agrosyst
     */
    @NonNull
    protected AgrosystInterventionType mainActionInterventionAgrosyst;
    
    /**
     * Id in database
     */
    protected String topiaId;
    
    /**
     * RefInterventionAgrosystTravailEDI.reference_label
     */
    protected String mainActionReference_label;
    
    /**
     * Nom de l'attribut en BD : comment
     */
    protected String comment;
    
    /**
     * Nom de l'attribut en BD : toolsCouplingCode
     */
    protected String toolsCouplingCode;
    
    /**
     * Reference to practicedIntervention
     */
    protected String practicedInterventionId;
    
    /**
     * reference to effectiveIntervention
     */
    protected String effectiveInterventionId;
    
    
    public Optional<String> getTopiaId() {
        return Optional.ofNullable(topiaId);
    }
    
    public Optional<String> getToolsCouplingCode() {
        return Optional.ofNullable(toolsCouplingCode);
    }
    
    public Optional<String> getPracticedInterventionId(){
        return Optional.ofNullable(practicedInterventionId);
    }
    
    public Optional<String> getEffectiveInterventionId() {
        return Optional.ofNullable(effectiveInterventionId);
    }
    
    public Optional<String> getComment() {
        return Optional.ofNullable(comment);
    }
    
    public Optional<String> getMainActionReference_label() {
        return Optional.ofNullable(mainActionReference_label);
    }
    
}

