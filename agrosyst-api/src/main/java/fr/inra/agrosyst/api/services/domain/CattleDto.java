package fr.inra.agrosyst.api.services.domain;
/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2020 INRAE, 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Cattle;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class CattleDto implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 6495570576122697462L;
    
    protected final String code;
    
    // livestockUnit.refAnimalType.animalType + ' / ' + cattle.animalType.animalType
    protected final String label;
    
    
    public CattleDto(Cattle cattle, RefAnimalType liveStockUnitAnimalType) {
        this.code = cattle.getCode();
        this.label = liveStockUnitAnimalType.getAnimalType() + " / " + cattle.getAnimalType().getAnimalType();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CattleDto cattleDto = (CattleDto) o;
        return code.equals(cattleDto.code);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
