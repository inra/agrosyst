package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingEntryType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * Un model représente un model de culture pouvant servir à:
 * - creer un noeud dans un cycle pluriannuel de culture assolées
 * - choisir une culture dans un cycle pluriannuel de culture pérennes.
 * Cela permet de définir une cuture à un rang donné.
 * 
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Getter
@Setter
public class CropCycleModelDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -9206223811128306689L;

    protected String croppingPlanEntryCode;
    protected String label;
    protected boolean intermediate;
    protected Double croppingPlanSellingPrice;
    protected boolean mixSpecies;
    protected boolean mixVariety;
    protected boolean mixCompanion;
    protected boolean catchCrop;

    protected CroppingEntryType type;
}
