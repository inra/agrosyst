package fr.inra.agrosyst.api.services.action;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.io.Serializable;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class QualityCriteriaDto implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 1568633137116762381L;
    
    protected String topiaId;
    
    /**
     * Nom de l'attribut en BD : quantitativeValue
     */
    protected Double quantitativeValue;
    
    /**
     * Nom de l'attribut en BD : binaryValue
     */
    protected Boolean binaryValue;
    
    /**
     * BI Id of refQualityCriteria
     */
    @NotNull
    protected String refQualityCriteriaId;
    
    /**
     * BD Id of refQualityCriteriaClass
     */
    protected String refQualityCriteriaClassId;
    
    public Optional<String> getRefQualityCriteriaClassId() {
        return Optional.ofNullable(refQualityCriteriaClassId);
    }
    
    public Optional<String> getTopiaId() {
        return Optional.ofNullable(topiaId);
    }
}
