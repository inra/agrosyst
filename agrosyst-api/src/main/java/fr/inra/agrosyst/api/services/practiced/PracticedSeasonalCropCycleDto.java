package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PracticedSeasonalCropCycleDto implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -1697195465174706710L;

    protected String topiaId;

    protected List<PracticedCropCycleNodeDto> cropCycleNodeDtos;

    protected List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos;

    public List<PracticedCropCycleNodeDto> getCropCycleNodeDtos() {
        return cropCycleNodeDtos;
    }

    public void addNode(PracticedCropCycleNodeDto node) {
        if (cropCycleNodeDtos == null) {
            cropCycleNodeDtos = new ArrayList<>();
        }
        cropCycleNodeDtos.add(node);
    }

    public void addConnection(PracticedCropCycleConnectionDto connection) {
        if (cropCycleConnectionDtos == null) {
            cropCycleConnectionDtos = new ArrayList<>();
        }
        cropCycleConnectionDtos.add(connection);
    }

}
