package fr.inra.agrosyst.api.services.managementmode;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.managementmode.Section;
import fr.inra.agrosyst.api.entities.managementmode.Strategy;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ManagementModes {

    public static final Binder<Strategy, StrategyDto> STRATEGY_STRATEGY_DTO_BINDER = BinderFactory.newBinder(Strategy.class, StrategyDto.class);
    public static final Binder<SectionDto, Section> SECTION_DTO_SECTION_BINDER = BinderFactory.newBinder(SectionDto.class, Section.class);
    public static final Binder<Section, SectionDto> SECTION_SECTION_DTO_BINDER = BinderFactory.newBinder(Section.class, SectionDto.class);
    public static final Function<Section, SectionDto> SECTION_TO_DTO = new Function<>() {
        @Override
        public SectionDto apply(Section input) {
            SectionDto result = new SectionDto();
            SECTION_SECTION_DTO_BINDER.copyExcluding(input, result,
                    Section.PROPERTY_BIO_AGRESSOR,
                    Section.PROPERTY_STRATEGIES);
            if (input.getBioAgressor() != null) {
                result.setBioAgressorTopiaId(input.getBioAgressor().getTopiaId());
                result.setBioAgressorLabel(input.getBioAgressor().getLabel());
            }
            if (input.getStrategies() != null) {
                result.setStrategiesDto(input.getStrategies().stream().map(STRATEGY_TO_DTO).collect(Collectors.toList()));
            }
            return result;
        }
    };
    
    public static final Function<Strategy, StrategyDto> STRATEGY_TO_DTO = input -> {
        StrategyDto result = new StrategyDto();
        STRATEGY_STRATEGY_DTO_BINDER.copyExcluding(input, result,
                Strategy.PROPERTY_CROPS,
                Strategy.PROPERTY_RULES);
        Collection<CroppingPlanEntry> crops = input.getCrops();
        if (CollectionUtils.isNotEmpty(crops)) {
            for (CroppingPlanEntry crop : crops) {
                result.addCropIds(crop);
            }
        }
        if (input.getRules() != null) {
            result.setDecisionRuleIds(Sets.newHashSet(Collections2.transform(input.getRules(), Entities.GET_TOPIA_ID::apply)));
        }
        return result;
    };

}
