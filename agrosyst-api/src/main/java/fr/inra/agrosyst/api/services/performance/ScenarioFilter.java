package fr.inra.agrosyst.api.services.performance;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.AgrosystFilter;

import java.io.Serial;

/**
 * Filter containing data used to filter domains list
 *
 * @author David Cossé
 */
public class ScenarioFilter extends AgrosystFilter {
    
    @Serial
    private static final long serialVersionUID = -1311277113556925585L;
    
    protected String scenarioName;

    protected Boolean active;
    
    public String getScenarioName() {
        return scenarioName;
    }
    
    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }
    
    public Boolean getActive() {
        return active;
    }
    
    public void setActive(Boolean active) {
        this.active = active;
    }
}
