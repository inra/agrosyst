package fr.inra.agrosyst.api.services.plot;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.SolHorizon;

import java.util.function.Function;

public class Plots {

    public static final Function<SolHorizon, SolHorizonDto> SOL_HORIZON_TO_DTO = input -> {
        SolHorizonDto result = new SolHorizonDto();
        result.setTopiaId(input.getTopiaId());
        result.setLowRating(input.getLowRating());
        result.setStoniness(input.getStoniness());
        result.setComment(input.getComment());
        if (input.getSolTexture() != null) {
            result.setSolTextureId(input.getSolTexture().getTopiaId());
        }
        return result;
    };
}
