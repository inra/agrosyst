package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformancePracticedDomainExecutionContext {

    // new
    // previous
    protected Pair<Domain, Domain> domainAndAnonymizeDomain;

    protected final Map<InputType, List<AbstractDomainInputStockUnit>> inputs;

    protected final RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput;

    protected final RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput;

    private final double manualWorkforceCost;
    private final double mechanizedWorkforceCost;
    protected final Optional<RefPrixCarbu> optionalRefFuelPricePerLiter;

    protected final List<HarvestingPrice> harvestingPrices;
    protected final MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey;

    protected Map<String, ToolsCoupling> toolsCouplingByCode;
    protected MultiKeyMap<Object, CroppingPlanEntry> cropByCampaignAndCode;
    @Getter
    protected Map<String, Set<Integer>> campaignsForCropCode;
    protected Map<String, CroppingPlanSpecies> speciesByCode;
    @Getter
    protected Map<String, Set<Integer>> campaignsForSpeciesCropCode;
    protected Map<String, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrops;// IPHY (disable for the moment)
    protected Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments;
    protected Collection<EquipmentUsageRange> equipmentUsageRanges;
    protected Map<Equipment, List<RefMateriel>> relatedEquipmentRefMateriels;

    private Set<RefEspece> domainRefEspeces = new HashSet<>();

    // Débit de chantier
    protected Map<ToolsCoupling, Pair<Double, MaterielWorkRateUnit>> toolsCouplingWorkRates = new HashMap<>();

    protected Set<PerformanceGrowingSystemExecutionContext> performanceGrowingSystemExecutionContexts = new HashSet<>();

    protected final Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> allDomainSubstancesByAmm;

    protected Double domainIndicatorSurfaceUTH;

    public PerformancePracticedDomainExecutionContext(Map<InputType, List<AbstractDomainInputStockUnit>> inputs,
                                                      Pair<Domain, Domain> domainAndAnonymizeDomain,
                                                      RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput,
                                                      RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
                                                      double manualWorkforceCost,
                                                      double mechanizedWorkforceCost,
                                                      Optional<RefPrixCarbu> optionalRefFuelPricePerLiter,
                                                      List<HarvestingPrice> harvestingPrices,
                                                      MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey,
                                                      Map<String, ToolsCoupling> toolsCouplingByCode,
                                                      Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments,
                                                      Collection<EquipmentUsageRange> equipmentUsageRanges,
                                                      MultiKeyMap<Object, CroppingPlanEntry> cropByCampaignAndCode,
                                                      Map<String, CroppingPlanSpecies> speciesByCode,
                                                      Map<String, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrops,
                                                      Map<String, Set<Integer>> campaignsForCropCode,
                                                      Map<String, Set<Integer>> campaignsForSpeciesCropCode,
                                                      Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> allDomainSubstancesByAmm) {
        this.inputs = inputs;
        this.refInputPricesForCampaignsByInput = refInputPricesForCampaignsByInput;
        this.domainAndAnonymizeDomain = domainAndAnonymizeDomain;
        this.refScenariosInputPricesByDomainInput = refScenariosInputPricesByDomainInput;
        this.refScenarioHarvestingPricesByValorisationKey = refScenarioHarvestingPricesByValorisationKey;
        this.manualWorkforceCost = manualWorkforceCost;
        this.mechanizedWorkforceCost = mechanizedWorkforceCost;
        this.optionalRefFuelPricePerLiter = optionalRefFuelPricePerLiter;
        this.harvestingPrices = harvestingPrices;
        this.toolsCouplingByCode = toolsCouplingByCode;
        this.deprecationRateByEquipments = deprecationRateByEquipments;
        this.equipmentUsageRanges = equipmentUsageRanges;
        this.cropByCampaignAndCode = cropByCampaignAndCode;
        this.speciesByCode = speciesByCode;
        this.speciesMaxCouvSolForCrops = speciesMaxCouvSolForCrops;
        this.campaignsForCropCode = campaignsForCropCode;
        this.campaignsForSpeciesCropCode = campaignsForSpeciesCropCode;
        this.allDomainSubstancesByAmm = allDomainSubstancesByAmm;
    }

    public PerformancePracticedDomainExecutionContext(Map<InputType, List<AbstractDomainInputStockUnit>> inputs,
                                                      Pair<Domain, Domain> domainAndAnonymizeDomain,
                                                      RefCampaignsInputPricesByDomainInputAndCampaigns refInputPricesForCampaignsByInput,
                                                      RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
                                                      double manualWorkforceCost,
                                                      double mechanizedWorkforceCost,
                                                      Optional<RefPrixCarbu> optionalRefFuelPricePerLiter,
                                                      List<HarvestingPrice> harvestingPrices,
                                                      MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey,
                                                      Map<String, ToolsCoupling> toolsCouplingByCode,
                                                      Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments,
                                                      Collection<EquipmentUsageRange> equipmentUsageRanges,
                                                      MultiKeyMap<Object, CroppingPlanEntry> cropByCampaignAndCode,
                                                      Map<String, CroppingPlanSpecies> speciesByCode,
                                                      Map<String, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrops,
                                                      Map<String, Set<Integer>> campaignsForCropCode,
                                                      Map<String, Set<Integer>> campaignsForSpeciesCropCode) {
        this(inputs, domainAndAnonymizeDomain, refInputPricesForCampaignsByInput, refScenariosInputPricesByDomainInput,
                manualWorkforceCost, mechanizedWorkforceCost, optionalRefFuelPricePerLiter, harvestingPrices, refScenarioHarvestingPricesByValorisationKey,
                toolsCouplingByCode, deprecationRateByEquipments, equipmentUsageRanges, cropByCampaignAndCode,
                speciesByCode, speciesMaxCouvSolForCrops, campaignsForCropCode, campaignsForSpeciesCropCode,
                Map.of());
    }

    public Domain getDomain() {
        return domainAndAnonymizeDomain.getLeft();
    }

    public Domain getAnonymiseDomain() {
        return domainAndAnonymizeDomain.getRight();
    }

    public Pair<Double, MaterielWorkRateUnit> getToolsCouplingWorkRate(ToolsCoupling toolsCoupling) {
        return toolsCouplingWorkRates.get(toolsCoupling);
    }

    public void setToolsCouplingWorkRate(ToolsCoupling toolsCoupling, Pair<Double, MaterielWorkRateUnit> workRate) {
        toolsCouplingWorkRates.put(toolsCoupling, workRate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformancePracticedDomainExecutionContext that = (PerformancePracticedDomainExecutionContext) o;
        if (Objects.equals(domainAndAnonymizeDomain, that.domainAndAnonymizeDomain)) return true;
        return Objects.equals(domainAndAnonymizeDomain.getLeft(), that.domainAndAnonymizeDomain.getLeft());
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainAndAnonymizeDomain.getLeft());
    }

    public List<HarvestingPrice> getHarvestingprices() {
        return this.harvestingPrices;
    }
}
