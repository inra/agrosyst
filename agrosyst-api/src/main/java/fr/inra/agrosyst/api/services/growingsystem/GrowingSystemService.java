package fr.inra.agrosyst.api.services.growingsystem;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.MarketingDestinationObjective;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.practiced.OperationStatus;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public interface GrowingSystemService extends AgrosystService {

    /**
     * Return the Growing Systems according the given filter.
     *
     * @param filter The Growing System filter
     * @return The growing systems
     */
    PaginationResult<GrowingSystem> getFilteredGrowingSystems(GrowingSystemFilter filter);

    /**
     * Return the Growing Systems according the given filter.
     *
     * @param filter The Growing System filter
     * @return The growing systems
     */
    PaginationResult<GrowingSystemDto> getFilteredGrowingSystemsDto(GrowingSystemFilter filter);

    /**
     * Return the Growing System ids according the given filter.
     *
     * @param filter The Growing System filter
     * @return The growing system ids
     */
    Set<String> getFilteredGrowingSystemIds(GrowingSystemFilter filter);

    /**
     * Return Growing Systems according it's id
     * @param growingSystemIds
     * @return
     */
    List<GrowingSystemDto> getGrowingSystems(Collection<String> growingSystemIds);

    /**
     * Find growing system from topia id.
     *
     * @param growingSystemId growing system id
     * @return growing system
     */
    GrowingSystem getGrowingSystem(String growingSystemId);

    /**
     * Create a new (empty) GrowingSystem entity. The returned instance is not persisted yet.
     *
     * @return the newly created instance
     */
    GrowingSystem newGrowingSystem();

    /**
     * Create or Update a GrowingSystem
     *
     * @param growingSystem           The GrowingSystem to create or update.
     * @param growingSystemNetworkIds The networks's growing system.
     * @param typeAgricultureTopiaId type agriculture topia id
     * @param marketingDestinationsBySector
     * @return the GrowingSystem
     */
    GrowingSystem createOrUpdateGrowingSystem(GrowingSystem growingSystem, List<String> growingSystemNetworkIds,
                                              Collection<String> selectedPlotsIds, String typeAgricultureTopiaId, List<String> growingSystemCharacteristicIds, Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector);

    /**
     * Duplicate a growing system without committing transaction.
     *
     * @param extendContext     extend context
     * @param clonedGrowingPlan growing plan to attach growing system to
     * @param growingSystem     growing system to clone
     * @return duplicated growing system
     */
    GrowingSystem duplicateGrowingSystem(ExtendContext extendContext, GrowingPlan clonedGrowingPlan, GrowingSystem growingSystem);

    /**
     * Unactivate the given growingSystems.
     *
     * @param growingSystemsIds growingSystems ids to unactivate
     * @param activate          activate
     */
    void unactivateGrowingSystems(List<String> growingSystemsIds, boolean activate);

    /**
     * Create a growing System and it's growingPlan
     * @param domainId the domainId the growing system belong to
     * @param practicedSystemName the practiced system name as it is also the growing system one
     * @param growingSystemSector the sector
     * @param ipmWorksId the networkId
     * @param typeAgricultureId the type of agriculture
     * @param marketingDestinationsBySector the marketing destination by sector
     * @return a new growing system
     */
    GrowingSystem createDefaultIpmworksGrowingSystem(
            String domainId,
            String practicedSystemName,
            Sector growingSystemSector,
            String ipmWorksId,
            String typeAgricultureId,
            Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector
    );

    /**
     * Find all growing system for a specific growing plan.
     *
     * @param growingPlan growing plan
     * @return growing plan's growing systems
     */
    List<GrowingSystem> findAllByGrowingPlan(GrowingPlan growingPlan);

    /**
     * Find all growing system for a specific network.
     *
     * @param network the network
     * @return network's growing systems
     */
    List<GrowingSystem> findAllByNetwork(Network network);

    /**
     * Find all growing system for a specific domain.
     *
     * @param domain the domain
     * @return the growing systems's
     */
    List<GrowingSystem> findAllActiveByDomainForPlotEdit(Domain domain);

    /**
     * Get growingSystem related to current growingSystem.
     * Related growingSystem got same duplication code.
     *
     * @param growingSystemCode the code identifying the growingSystem
     * @return related growingSystems
     */
    LinkedHashMap<Integer, String> getRelatedGrowingSystemIdsByCampaigns(String growingSystemCode);
    
    /**
     * Return the last available actice Growing System related to the given campaigns
     * @param growingSystemCode the code of the growing system searched
     * @param practicedSystemCampaigns the growing system valid campaigns
     * @return Pair of campaign, growingSystem
     */
    GrowingSystem getLastGrowingSystemForCampaigns(String growingSystemCode, Set<Integer> practicedSystemCampaigns);
    
    /**
     * Return the number of growingSystems
     *
     * @return The number of growing systems
     * @param active optional active filter (may be null)
     */
    long getGrowingSystemsCount(Boolean active);


    /**
     * Do validate the current growing system state
     *
     * @param growingSystemIds the identifiers of the growing system to validate
     * @param validate validate or not for growingSystem
     * @return the validated growing systems
     */
    List<OperationStatus> validate(List<String> growingSystemIds, boolean validate);

    /**
     * Retournes tous les types d'agricultures.
     * 
     * @return les types d'agriculture.
     */
    List<RefTypeAgriculture> findAllTypeAgricultures();
    

    /**
     * Recupère la liste des systèmes pratiqués lié à systeme de culture via son code de prolongation.
     * 
     * @param growingSystemCode growing system code
     * @return practiced systems list
     */
    List<PracticedSystem> getPracticedSystems(String growingSystemCode);

    /**
     * Retourne la liste des modes de gestion associés au système de culture.
     * 
     * @param growingSystemId growing system id 
     * @return management modes
     */
    List<ManagementMode> getManagementModes(String growingSystemId);

    /**
     * Return campaigns for growing systems matching the given code
     * @param growingSystemCode Growing system's code
     * @return List of campaigns
     */
    List<Integer> getGrowingSystemCampaigns(String growingSystemCode);

    /**
     * Return all growing systems related to the practiced system's domain withing optional navigationContext criteria, if null no filter is done
     * @param practicedSystemId practiced system id
     * @param navigationContext navigation context may be null,
     * @return all growing systems according filter
     */
    List<GrowingSystem> getAvailableGsForDuplication(String practicedSystemId, NavigationContext navigationContext);

    /**
     * Return growing system matching same name and campaign
     * @param growingSystemName growing system name
     * @param campaign campaign
     * @return All growing system matching filter
     */
    List<GrowingSystem> getGrowingSystemWithNameAndCampaign(String growingSystemName, Integer campaign);

    /**
     * Growing system's xls stream for the given growing system id
     * @param growingSystemIds growing system id
     * @return growing system data export.
     */
    ExportResult exportGrowingSystemsAsXls(Collection<String> growingSystemIds);

    void exportGrowingSystemAsXlsAsync(Collection<String> growingSystemIds);

    Sector getGrowingSystemSector(String growingSystemId);

    Boolean isOrganicGrowingSystem(String growingSystemId);

    boolean isMissingTypeAgriculture(GrowingSystem growingSystem);

    String getMissingTypeAgricultureTopiaId();

    Map<Sector, List<MarketingDestinationObjective>> loadMarketingDestinationObjective(GrowingSystem growingSystem);

    Map<Sector, List<MarketingDestinationObjective>> loadMarketingDestinationObjectivesForSectors(Set<Sector> sectors);

    List<Integer> getGrowingSystemCampaignsFromId(String growingSystemId, NavigationContext navigationContext);

    List<CroppingPlanEntry> getCropForGrowingSystemId(String growingSystemId);
    
    List<GrowingSystem> getAllUnValidatedForGivenIds(List<String> growingSystemIds);
    
    List<GrowingSystem> getAllValidatedForGivenIds(List<String> growingSystemIds);

    GrowingSystem updateGrowingSystem(GrowingSystem growingSystem);

}
