package fr.inra.agrosyst.api.services.referential;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;

import java.io.Serial;
import java.io.Serializable;

public class RefFertiMinUNIFADto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1745208458204588528L;
    /**
     * Nom de l'attribut en BD : categ
     */
    protected Integer categ;

    /**
     * Nom de l'attribut en BD : type_produit
     */
    protected String type_produit;

    /**
     * Nom de l'attribut en BD : codeprod
     */
    protected String codeprod;

    /**
     * Nom de l'attribut en BD : forme
     */
    protected String forme;

    /**
     * Nom de l'attribut en BD : n
     */
    protected Double n;

    /**
     * Nom de l'attribut en BD : p2O5
     */
    protected Double p2O5;

    /**
     * Nom de l'attribut en BD : k2O
     */
    protected Double k2O;

    /**
     * Nom de l'attribut en BD : bore
     */
    protected Double bore;

    /**
     * Nom de l'attribut en BD : calcium
     */
    protected Double calcium;

    /**
     * Nom de l'attribut en BD : fer
     */
    protected Double fer;

    /**
     * Nom de l'attribut en BD : manganese
     */
    protected Double manganese;

    /**
     * Nom de l'attribut en BD : molybdene
     */
    protected Double molybdene;

    /**
     * Nom de l'attribut en BD : mgO
     */
    protected Double mgO;

    /**
     * Nom de l'attribut en BD : oxyde_de_sodium
     */
    protected Double oxyde_de_sodium;

    /**
     * Nom de l'attribut en BD : sO3
     */
    protected Double sO3;

    /**
     * Nom de l'attribut en BD : cuivre
     */
    protected Double cuivre;

    /**
     * Nom de l'attribut en BD : zinc
     */
    protected Double zinc;

    /**
     * Nom de l'attribut en BD : source
     */
    protected String source;

    /**
     * Nom de l'attribut en BD : active
     */
    protected boolean active;

    protected String topiaId;

    public RefFertiMinUNIFADto(RefFertiMinUNIFA product) {
        setCateg(product.getCateg());
        setType_produit(product.getType_produit());
        setCodeprod(product.getCodeprod());
        setForme(product.getForme());
        setN(product.getN());
        setP2O5(product.getP2O5());
        setK2O(product.getK2O());
        setBore(product.getBore());
        setCalcium(product.getCalcium());
        setFer(product.getFer());
        setManganese(product.getManganese());
        setMolybdene(product.getMolybdene());
        setMgO(product.getMgO());
        setOxyde_de_sodium(product.getOxyde_de_sodium());
        setsO3(product.getsO3());
        setCuivre(product.getCuivre());
        setZinc(product.getZinc());
        setSource(product.getSource());
        setActive(product.isActive());
        setTopiaId(product.getTopiaId());
    }

    public Integer getCateg() {
        return categ;
    }

    public void setCateg(Integer categ) {
        this.categ = categ;
    }

    public String getType_produit() {
        return type_produit;
    }

    public void setType_produit(String type_produit) {
        this.type_produit = type_produit;
    }

    public String getCodeprod() {
        return codeprod;
    }

    public void setCodeprod(String codeprod) {
        this.codeprod = codeprod;
    }

    public String getForme() {
        return forme;
    }

    public void setForme(String forme) {
        this.forme = forme;
    }

    public Double getN() {
        return n;
    }

    public void setN(Double n) {
        this.n = n == 0 ? null : n;
    }

    public Double getP2O5() {
        return p2O5;
    }

    public void setP2O5(Double p2O5) {
        this.p2O5 = p2O5 == 0 ? null : p2O5;
    }

    public Double getK2O() {
        return k2O;
    }

    public void setK2O(Double k2O) {
        this.k2O = k2O == 0 ? null : k2O;
    }

    public Double getBore() {
        return bore;
    }

    public void setBore(Double bore) {
        this.bore = bore == 0 ? null : bore;
    }

    public Double getCalcium() {
        return calcium;
    }

    public void setCalcium(Double calcium) {
        this.calcium = calcium == 0 ? null : calcium;
    }

    public Double getFer() {
        return fer;
    }

    public void setFer(Double fer) {
        this.fer = fer == 0 ? null : fer;
    }

    public Double getManganese() {
        return manganese;
    }

    public void setManganese(Double manganese) {
        this.manganese = manganese == 0 ? null : manganese;
    }

    public Double getMolybdene() {
        return molybdene;
    }

    public void setMolybdene(Double molybdene) {
        this.molybdene = molybdene == 0 ? null : molybdene;
    }

    public Double getMgO() {
        return mgO;
    }

    public void setMgO(Double mgO) {
        this.mgO = mgO == 0 ? null : mgO;
    }

    public Double getOxyde_de_sodium() {
        return oxyde_de_sodium;
    }

    public void setOxyde_de_sodium(Double oxyde_de_sodium) {
        this.oxyde_de_sodium = oxyde_de_sodium == 0 ? null : oxyde_de_sodium;
    }

    public Double getsO3() {
        return sO3;
    }

    public void setsO3(Double sO3) {
        this.sO3 = sO3 == 0 ? null : sO3;
    }

    public Double getCuivre() {
        return cuivre;
    }

    public void setCuivre(Double cuivre) {
        this.cuivre = cuivre == 0 ? null : cuivre;
    }

    public Double getZinc() {
        return zinc;
    }

    public void setZinc(Double zinc) {
        this.zinc = zinc == 0 ? null : zinc;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }
}
