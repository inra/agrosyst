package fr.inra.agrosyst.api.services.security;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;

public class AgrosystRuntimeException extends RuntimeException {
    
    @Serial
    private static final long serialVersionUID = -1041901029406865731L;
    
    public AgrosystRuntimeException() {
    }

    public AgrosystRuntimeException(String s) {
        super(s);
    }

    public AgrosystRuntimeException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AgrosystRuntimeException(Throwable throwable) {
        super(throwable);
    }

    public AgrosystRuntimeException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
