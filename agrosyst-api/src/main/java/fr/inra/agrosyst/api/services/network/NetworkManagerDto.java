package fr.inra.agrosyst.api.services.network;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.users.UserDto;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author cosse
 *
 */
public class NetworkManagerDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -5534468574503050340L;

    protected String topiaId;
    
    protected LocalDate fromDate;
    
    protected LocalDate toDate;
    
    protected Boolean active;
    
    protected UserDto user;

    public NetworkManagerDto() {

    }
    
    public NetworkManagerDto(String topiaId, LocalDate fromDate, LocalDate toDate, Boolean active, UserDto userDto) {
        this.topiaId = topiaId;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.active = active;
        this.user = userDto;
    }

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NetworkManagerDto other = (NetworkManagerDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }
}
