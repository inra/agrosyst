package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.plot.SolHorizonDto;
import org.nuiton.util.pagination.PaginationResult;

import java.util.List;

/**
 * Practiced plot service.
 * 
 * @author Eric Chatellier
 */
public interface PracticedPlotService extends AgrosystService  {

    PaginationResult<PracticedPlot> getFilteredPracticedPlots(PracticedPlotFilter filter);

    /**
     * return the practiced plot matching the given filter parameters
     * @param filter filter por practiced plot
     * @return the practiced plot matching filter
     */
    PaginationResult<PracticedPlotDto> getFilteredPracticedPlotsDto(PracticedPlotFilter filter);

    /**
     * Return the instance of plot for the given practicedPlotTopiaId, if null a new instance is created.
     * @param practicedPlotTopiaId the id of requested practiced plot, if null a new instance is created
     * @return a practiced plot
     */
    PracticedPlot getPracticedPlot(String practicedPlotTopiaId);

    /**
     * Create or update the given practiced plot
     * @param practicedPlot the practiced plot to create or update, it must have the following attributes not null: name, waterFlowDistance, bufferStrip, maxSlope
     * @param practicedSystemTopiaId the id of the practiced system related to the practiced plot (can not be null)
     * @param locationTopiaId the id of location (can not be null)
     * @param selectedSurfaceTextureId the related surface texture
     * @param selectedSubSoilTextureId the related sub surface texture
     * @param selectedSolDepthId the sol depth id
     * @param selectedPlotZoningIds  the ids of related zones (can not be null)
     * @param solHorizons  the sol horizon ids
     * @param adjacentElementIds the adjacent elements ids
     * @return the persisted Practiced Plots
     */
    PracticedPlot createOrUpdatePracticedPlot(PracticedPlot practicedPlot, String practicedSystemTopiaId,
                                              String locationTopiaId, String selectedSurfaceTextureId,
                                              String selectedSubSoilTextureId, String selectedSolDepthId,
                                              List<String> selectedPlotZoningIds,
                                              List<SolHorizonDto> solHorizons,
                                              List<String> adjacentElementIds);

    /**
     * Return Practiced Systems not related to Practiced Plot
     * @param navigationContext the navigation context
     * @return all practiced systems without practiced plot
     */
    List<PracticedSystem> getPracticedSystemsWithoutPracticedPlot(NavigationContext navigationContext);

}
