package fr.inra.agrosyst.api.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.FuelUnit;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.IrrigationUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public interface DomainInputStockUnitService extends AgrosystService {

    String KEY_SEPARATOR = ";";

    String IRRIGATION_INPUT_NAME = "Eau d’irrigation";

    String DEPRECATED_REF_OTHER_INPUT_ID = "fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000";

    static String getMineralInputKey(RefFertiMinUNIFA refInput, boolean isPhytoEffect, MineralProductUnit usageUnit) {
        String result = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refInput) + KEY_SEPARATOR + isPhytoEffect + KEY_SEPARATOR + usageUnit;
        return result;
    }

    static String getMineralInputDtoKey(DomainMineralProductInputDto dto, MineralProductUnit usageUnit) {
        String result = InputPriceService.GET_REF_FERTI_MIN_UNIFA_DTO_OBJECT_ID.apply(dto) + KEY_SEPARATOR + dto.isPhytoEffect() + KEY_SEPARATOR + usageUnit;
        return result;
    }

    static String getOrganicProductInputKey(
            String idtypeeffluent,
            OrganicProductUnit usageUnit,
            double n,
            double p2O5,
            double k2O,
            Double caO,
            Double mgO,
            Double s,
            boolean organic) {

        String pattern = "Idtypeeffluent=%s;usageUnit=%s;n=%f;p2O5=%f;k2O=%f;caO=%f;mgO=%f;s=%f;organic=%b";
        String result = String.format(pattern, idtypeeffluent, usageUnit, n, p2O5, k2O, caO, mgO, s, organic);
        result = result.replace(',', '.');
        result = StringUtils.stripAccents(result);
        return result;
    }

    static String getPotInputKey(RefPot refInput, PotInputUnit usageUnit) {
        String result = StringUtils.stripAccents(refInput.getCaracteristic1()) + KEY_SEPARATOR + usageUnit;
        return result;
    }

    static String getSubstratInputKey(RefSubstrate refInput, SubstrateInputUnit usageUnit) {
        String result = StringUtils.stripAccents(refInput.getCaracteristic1()) + KEY_SEPARATOR + StringUtils.stripAccents(refInput.getCaracteristic2()) + KEY_SEPARATOR + usageUnit;
        return result;
    }

    static String getOtherInputKey(RefOtherInput refInput, OtherProductInputUnit usageUnit) {

        String result = StringUtils.stripAccents(Strings.nullToEmpty(refInput.getInputType_c0())) + KEY_SEPARATOR +
                StringUtils.stripAccents(Strings.nullToEmpty(refInput.getCaracteristic1())) + KEY_SEPARATOR +
                StringUtils.stripAccents(Strings.nullToEmpty(refInput.getCaracteristic2())) + KEY_SEPARATOR +
                StringUtils.stripAccents(Strings.nullToEmpty(refInput.getCaracteristic3())) + KEY_SEPARATOR +
                usageUnit;

        return result;
    }

    static String getIrrigInputKey(IrrigationUnit usageUnit) {
        return usageUnit.name();
    }

    static String getFuelInputKey(FuelUnit usageUnit) {
        return usageUnit.name();
    }

    static String getManualWorkforceInputKey(Domain domain) {
        return domain.getCode() + KEY_SEPARATOR + domain.getCampaign() + KEY_SEPARATOR + InputType.MAIN_OEUVRE_MANUELLE.name();
    }

    static String getMechanizedWorkforceInputKey(Domain domain) {
        return domain.getCode() + KEY_SEPARATOR + domain.getCampaign() + KEY_SEPARATOR + InputType.MAIN_OEUVRE_TRACTORISTE.name();
    }

    static String getPhytoInputKey(RefActaTraitementsProduit refInput, PhytoProductUnit usageUnit) {
        String result = refInput.getId_produit() + KEY_SEPARATOR + refInput.getId_traitement() + KEY_SEPARATOR + usageUnit;
        return result;
    }

    static String getLotCropSeedInputKey(CroppingPlanEntry crop, boolean organic, SeedPlantUnit usageUnit) {
        String result = crop.getCode() + KEY_SEPARATOR + organic + KEY_SEPARATOR + usageUnit;
        return result;
    }

    static String getLotSpeciesInputKey(
            String code_espece_botanique,
            String code_qualifiant_aee,
            boolean biologicalSeedInoculation,
            boolean chemicalTreatment,
            boolean organic,
            String vLabel,
            SeedPlantUnit usageUnit,
            SeedType seedType) {


        if (seedType == null) {
            seedType = SeedType.SEMENCES_CERTIFIEES;
        }
        List<String> keyElements = Lists.newArrayList(
                code_espece_botanique,
                code_qualifiant_aee,
                Boolean.toString(biologicalSeedInoculation),
                Boolean.toString(chemicalTreatment),
                Boolean.toString(organic),
                vLabel,
                usageUnit.name(),
                seedType.name());
        String result = String.join(KEY_SEPARATOR, keyElements);
        return result;
    }


    /**
     * Validate One Input item
     *
     * @param domainInput input item
     * @return validation status
     */
    DomainInputDto validateDomainInputStockUnit(AbstractDomainInputStockUnit domainInput);

    /**
     * Create, update or remove inputs from the given domain.
     * Inputs present on domain but not part of domainInputs will be removed if not used on ITKs
     *
     * @param domain             the domain inputs belong to, if null nothing is done on domain input, if empty collection, all not used inputs will be removed
     * @param domainInputs       inputs to create or update
     * @param cropsByOriginalIds crops used on the domain map by there topiaId or a generated Id for new ones
     */
    void createOrUpdateDomainInputStock(
            Domain domain,
            Collection<DomainInputDto> domainInputs,
            Map<String, CropPersistResult> cropsByOriginalIds);

    /**
     * Load domain input from database and transform it to DTO
     *
     * @param domainId the domain TopiaId that belong to inputs
     * @return the domain input DTOs
     */
    Collection<DomainInputDto> loadDomainInputStockDtos(String domainId);

    /**
     * Load domain input from database and transform it to DTO
     *
     * @param domainId  the domain TopiaId that belong to inputs
     * @param inputType the input type that belon to inputs
     * @return the domain input DTOs
     */
    List<DomainInputDto> loadDomainInputStockDtos(String domainId, InputType inputType);

    /**
     * Duplicate the given domain to the toDomain if possible
     *
     * @param fromDomain          domain tha belong to given inputs
     * @param toDomain            domain to duplicate inputs
     * @param extendCroppingPlans persisted crops from extend domaine
     */
    void duplicateDomainInputStocks(
            Domain fromDomain,
            Domain toDomain,
            Collection<CroppingPlanEntry> extendCroppingPlans);

    DomainSeedLotInput cloneDomainSeedLotInput(
            Domain fromDomain,
            Domain toDomain,
            DomainSeedLotInput domainSeedLotInput,
            Map<String, CroppingPlanEntry> extendCroppingPlanByCodes,
            Map<String, CroppingPlanSpecies> extendCroppingPlanSpeciesByCodes);

    DomainSeedLotInput cloneAndPersistDomainSeedLotInput(
            Domain fromDomain,
            Domain toDomain,
            DomainSeedLotInput domainSeedLotInput,
            Map<String, CroppingPlanEntry> extendCroppingPlanByCodes,
            Map<String, CroppingPlanSpecies> extendCroppingPlanSpeciesByCodes);

    AbstractDomainInputStockUnit cloneDomainInputExceptSeedLot(Domain toDomain,
                                                               InputType inputType,
                                                               AbstractDomainInputStockUnit sourceInputStockUnit,
                                                               boolean isSameDomainCode);

    List<String> copyInputStocks(String fromDomain, Collection<String> inputIds, Collection<String> toDomainIds, boolean forceCopy);

    AbstractDomainInputStockUnit copyInputStock(String fromDomainId, String inputId, String toDomainId);

    DomainPhytoProductInput duplicateAndAddPhytoProductToDomainSeedSpecies(
            Domain domain,
            DomainSeedSpeciesInput clonedDomainSeedSpeciesInput,
            DomainPhytoProductInput phytoInputModel,
            boolean preserveInputCode);

    /**
     * remove all inputs related to the given domain
     *
     * @param domain the domain from to remove inputs
     * @throws fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException: if an input remain used
     */
    void deleteDomainInputStockUnit(Domain domain);

    /**
     * load AbstractDomainInputStockUnit entities that belong to the given domain by input types
     *
     * @param domain the domain inputs belong to
     * @return the inputs entities
     */
    Map<InputType, List<AbstractDomainInputStockUnit>> loadDomainInputStock(Domain domain);

    Map<String, AbstractDomainInputStockUnit> loadDomainInputStockByIds(Domain domain);

    Map<String, List<AbstractDomainInputStockUnit>> loadDomainInputStockByIKeys(Domain domain);

    Map<String, AbstractDomainInputStockUnit> loadDomainInputStockByCodes(Domain domain);

    Map<String, DomainInputDto> loadDomainInputStockDtoByCodes(String domainId);

    <I extends AbstractDomainInputStockUnit> I createOrUpdateDomainInput(I input);

    DomainIrrigationInput newDomainIrrigationInput(Domain domain);

    String getDomainSeedSpeciesInputName(
            CroppingPlanSpecies cps,
            SeedType seedType,
            Map<SeedType, String> seedTypeTranslations,
            boolean biologicalSeedInoculation,
            boolean chemicalTreatment,
            Locale locale);

    String getDomainSeedLotName(
            CroppingPlanEntry croppingPlanEntry,
            SeedType seedType,
            Map<SeedType, String> seedTypeTranslations,
            boolean biologicalSeedInoculation,
            boolean chemicalTreatment,
            Locale locale);

    Optional<DomainSeedLotInput> getLotForId(String lotId);

    Collection<SeedPlantUnit> getSeedPlanUnits(List<String> speciesIds);
}
