package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;

import java.util.Collection;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public record DomainReferentialInputs(
        // inputDtos that collection belong to
        Collection<DomainInputDto> domainInputDtos,
        ImmutableMap<String, RefFertiOrga> refFertiOrgaByIds,
        ImmutableMap<String, RefActaTraitementsProduit> refActaTraitementsProduitByIds,
        ImmutableMap<String, RefPot> refPotByIds,
        ImmutableMap<String, RefSubstrate> refSubstrateByIds,
        ImmutableMap<String, RefFertiMinUNIFA> refFertiMinUnifaByIds,
        ImmutableMap<String, RefOtherInput> refOtherInputByIds
) {
}
