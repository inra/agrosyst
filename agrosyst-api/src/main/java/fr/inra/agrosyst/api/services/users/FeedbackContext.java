package fr.inra.agrosyst.api.services.users;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.referential.FeedbackCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Getter
@Setter
public class FeedbackContext {
    
    private AuthenticatedUser agrosystUser;
    
    // L’utilisateur indique si sa question porte sur un dispositif DEPHY FERME, DEPHY EXPE ou Hors DEPHY.
    // L’utilisateur ne peut choisir qu’un seul type de dispositif.
    // Si l’utilisateur sélectionne dans le champ 1 ‘DEPHY FERME’ alors le champ 'isNotNetworkIr' apparait.
    private final TypeDEPHY dephyType;
    // Ce champ permet à l’utilisateur,
    // lorsqu’il le coche, d’indiquer qu’il a des questions sur un dispositif de type DEPHY FERME, mais qu’il ne dépend pas d’un Ingénieur Territorial.
    private final boolean isNotNetworkIrChecked;
    // Si l’utilisateur coche dispositif DEPHY FERME, et isNotNetworkIr = false, alors itEmail apparait.
    // Obligatoire et avec contrôle de la saisie (validité de l’adresse mail).
    private final String itEmail;
    // Filière(s) concernées
    private final List<Sector> sectorTypes;
    
    private final FeedbackCategory category;
    private final String feedback;
    
    private final byte[] screenshotData;
    
    private FeedbackAttachment attachment;

    private String env;
    private String location;
    private String locationTitle;
    private String requested;
    private String referer;
    
    // retour
    private Set<String> feedbackRecipients;
    private FeedbackStatus feedbackStatus;
    private String messageDateTime;
    
    public FeedbackContext(List<Sector> sectorTypes, TypeDEPHY dephyType, FeedbackCategory category, String feedback,
                           String itEmail, boolean isNotNetworkIrChecked, byte[] screenshotData) {
        this.sectorTypes = sectorTypes;
        this.dephyType = dephyType;
        this.category = category;
        this.feedback = feedback;
        this.itEmail = itEmail;
        this.isNotNetworkIrChecked = isNotNetworkIrChecked;
        this.screenshotData = screenshotData;
    }
    
    public void addAttachment(byte[] data, String name, String type) {
        this.attachment = new FeedbackAttachment(data, name, type);
    }
    
    public void addTechnicalDetails(
            String env,
            String location,
            String locationTitle,
            String requested,
            String referer) {
        
        this.env = env;
        this.location = location;
        this.locationTitle = locationTitle;
        this.requested = requested;
        this.referer = referer;
    }
    
    /**
     *
     * @param toMails must not be null
     * @param ccMails must not be null;
     */
    public void setFeedbackRecipients(List<String> toMails, List<String> ccMails) {
        feedbackRecipients = CollectionUtils.union(toMails, ccMails).stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
    }
    
    public enum FeedbackStatus {
        SUCCESS,
        FAILED,
        NOT_SEND
    }

    @AllArgsConstructor
    @Getter
    public static class FeedbackAttachment {
        protected byte[] data;
        protected String name;
        protected String type;
    }
}
