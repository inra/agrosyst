package fr.inra.agrosyst.api.services.practiced;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.inra.agrosyst.api.entities.PhytoProductUnit;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Kevin Morin
 */
public class ReferenceDoseDTO implements Serializable {
    
    @Serial
    private static final long serialVersionUID = 3913862083440288333L;
    protected String topiaId;

    protected PhytoProductUnit unit;

    protected Double value;

    public ReferenceDoseDTO(String topiaId, PhytoProductUnit unit, Double value) {
        this.topiaId = topiaId;
        this.unit = unit;
        this.value = value;
    }

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public PhytoProductUnit getUnit() {
        return unit;
    }

    public void setUnit(PhytoProductUnit unit) {
        this.unit = unit;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
