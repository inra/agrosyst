package fr.inra.agrosyst.api.services.common;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class GrowingSystemsIndicator implements Serializable {

    @Serial
    private static final long serialVersionUID = -881041442589116062L;

    protected TypeDEPHY typeDephy;
    protected Sector sector;
    protected long count;
    protected long active;
    protected long validated;

    public GrowingSystemsIndicator() {
        // total
    }

    public GrowingSystemsIndicator(TypeDEPHY typeDephy) {
        this.typeDephy = typeDephy;
    }

    public GrowingSystemsIndicator(Sector sector) {
        this.sector = sector;
    }

    public TypeDEPHY getTypeDephy() {
        return typeDephy;
    }

    public Sector getSector() {
        return sector;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getActive() {
        return active;
    }

    public void setActive(long active) {
        this.active = active;
    }

    public long getValidated() {
        return validated;
    }

    public void setValidated(long validated) {
        this.validated = validated;
    }
}
