package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.practiced.CropCycleGraftDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.time.LocalDate;

@Getter
@Setter
public class EffectiveCropCycleSpeciesDto extends CroppingPlanSpeciesDto {

    @Serial
    private static final long serialVersionUID = -3406917281269769835L;

    protected boolean plantsCertified;
    protected LocalDate overGraftDate;

    protected String croppingPlanSpeciesId;

    protected CropCycleGraftDto graftSupport;
    protected CropCycleGraftDto graftClone;

    public EffectiveCropCycleSpeciesDto() {
    }

    public EffectiveCropCycleSpeciesDto(CroppingPlanSpeciesDto parent) {
        this.croppingPlanSpeciesId = parent.getTopiaId();
        this.code = parent.getCode();

        this.speciesId = parent.getSpeciesId();
        this.speciesEspece = parent.getSpeciesEspece();
        this.speciesQualifiant = parent.getSpeciesQualifiant();
        this.speciesTypeSaisonnier = parent.getSpeciesTypeSaisonnier();
        this.speciesDestination = parent.getSpeciesDestination();
        this.code_destination_aee = parent.getCode_destination_aee();

        this.varietyId = parent.getVarietyId();
        this.varietyLibelle = parent.getVarietyLibelle();
        this.edaplosUnknownVariety = parent.getEdaplosUnknownVariety();
        this.profil_vegetatif_BBCH = parent.getProfil_vegetatif_BBCH();
        this.code_espece_botanique = parent.getCode_espece_botanique();
        this.compagne = parent.getCompagne();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((croppingPlanSpeciesId == null) ? 0 : croppingPlanSpeciesId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EffectiveCropCycleSpeciesDto other = (EffectiveCropCycleSpeciesDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }
}
