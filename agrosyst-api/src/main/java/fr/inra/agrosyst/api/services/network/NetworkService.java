package fr.inra.agrosyst.api.services.network;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkManager;
import fr.inra.agrosyst.api.services.AgrosystService;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * @author cosse
 */
public interface NetworkService extends AgrosystService {

    /**
     * Return the Networks according the given filter.
     *
     * @param filter The Network filter
     * @return the networks
     */
    PaginationResult<Network> getFilteredNetworks(NetworkFilter filter);

    /**
     * Return the Networks ids according the given filter.
     *
     * @param filter The Network filter
     * @return the network ids
     */
    Set<String> getFilteredNetworkIds(NetworkFilter filter);

    /**
     * Return the Network matching with the network topiaId given has parameter.
     *
     * @param networkTopiaId the network topiaId
     * @return the network
     */
    Network getNetwork(String networkTopiaId);

    /**
     * Return a new not persisted Network.
     *
     * @return the network
     */
    Network newNetwork();

    /**
     * Return a new not persisted Network Manager.
     *
     * @return the network Manager
     */
    NetworkManager newNetworkManager();

    /**
     * Update the given network
     *
     * @param network            the network to update
     * @param networkManagerDtos the network managers DTO
     * @param parentsIds         List of network's parent's TopiaIds
     * @return the updated network
     */
    Network createOrUpdateNetwork(Network network, Collection<NetworkManagerDto> networkManagerDtos, List<String> parentsIds);

    /**
     * Return all the network with there name looking as the researched term.
     *
     * @param research         The researched term
     * @param nbResult         The maximum number if return result
     * @param exclusions       The identifiers of the networks to exclude
     * @param selfNetworkId    The identifier of the current network (in case editing a network)
     * @param onNetworkEdition If the current request should consider only networks without SdC children or not
     * @param onlyResponsibleNetwork only return network from user's network (and sub networks)
     * @return All the network with there name looking as the researched term.
     */
    LinkedHashMap<String, String> searchNameFilteredActiveNetworks(String research, Integer nbResult,
                                                                   Set<String> exclusions, String selfNetworkId,
                                                                   boolean onNetworkEdition, boolean onlyResponsibleNetwork);

    Set<String> findNetworksByName(String name, String excludeNetworkId);

    NetworkGraph buildFullNetworkGraph();

    NetworkGraph buildNetworkGraph(String fromNetworkId);

    NetworkGraph buildGrowingSystemAndNetworkGraph(String growingSystemName, Set<String> parentNetworkIds);

    /**
     * Unactivate or reactivate the given networks.
     *
     * @param networkIds networkIds topiaId to unactivate
     * @param activate   activate insteadof unactivate
     */
    void unactivateNetworks(List<String> networkIds, boolean activate);

    /**
     * return the number of networks
     *
     * @return The number of networks
     * @param active optional active filter (may be null)
     */
    long getNetworksCount(Boolean active);
    
    NetworkIndicators getIndicators(String networkId);

    List<Network> getNetworkWithName(String networkName);

    List<Network> getNetworksForUserManager(String userId);
}
