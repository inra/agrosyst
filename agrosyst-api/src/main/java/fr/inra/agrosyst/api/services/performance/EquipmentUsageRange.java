package fr.inra.agrosyst.api.services.performance;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import lombok.Getter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author David Cossé (Code Lutin)
 */
@Getter
public class EquipmentUsageRange {
    
    private static final Log LOGGER = LogFactory.getLog(EquipmentUsageRange.class);
    
    /**
     *  unit found in DB:
     *  km
     *  heure
     *  Voy
     *  jours
     *  m3
     *  bal
     *  T/an
     *  ha
     *  T
     *  ha/an
     *  h
     *  he
     *  qx
     */
    protected final String HA = "ha";
    protected final String H = "h";
    protected final String HE= "he";
    protected final String HEURE = "heure";
    protected final String VOY = "voy";
    protected final String BAL = "bal";
    
    protected final RefMateriel refMateriel;
    
    protected double lowestValueByHa = Double.MAX_VALUE;
    protected double highestValueByHa = -Double.MAX_VALUE;
    
    protected double lowestValueByH = Double.MAX_VALUE;
    protected double highestValueByH = -Double.MAX_VALUE;
    
    protected double lowestValueByVoy = Double.MAX_VALUE;
    protected double highestValueByVoy = -Double.MAX_VALUE;
    
    protected double lowestValueByBall = Double.MAX_VALUE;
    protected double highestValueByBall = -Double.MAX_VALUE;
    
    public void pushValue(double value, String unit) {
        Preconditions.checkNotNull(unit);
        unit = unit.trim().toLowerCase();
        switch (unit) {
            case HA:
                lowestValueByHa = Math.min(value, lowestValueByHa);
                highestValueByHa = Math.max(value, highestValueByHa);
                break;
            case H:
            case HEURE:
            case HE:
                lowestValueByH = Math.min(value, lowestValueByH);
                highestValueByH = Math.max(value, highestValueByH);
                break;
            case VOY:
                lowestValueByVoy = Math.min(value, lowestValueByVoy);
                highestValueByVoy = Math.max(value, highestValueByVoy);
                break;
            case BAL:
                lowestValueByBall = Math.min(value, lowestValueByBall);
                highestValueByBall = Math.max(value, highestValueByBall);
                break;
            default:
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn("Unitée d'utilisation du matériel non reconnue pour le calcul de l'utilisation annuelle d'un matériel");
                }
                // nothing to do
                break;
        }
    }
    
    public EquipmentUsageRange(RefMateriel refMateriel) {
        this.refMateriel = refMateriel;
        String unit = refMateriel.getUnite().trim().toLowerCase();
        if (HA.contentEquals(unit)) {
            this.lowestValueByHa = refMateriel.getUniteParAn();
            this.highestValueByHa = refMateriel.getUniteParAn();
        } else if (H.contentEquals(unit) || HEURE.contentEquals(unit) || HE.contentEquals(unit)) {
            this.lowestValueByH = refMateriel.getUniteParAn();
            this.highestValueByH = refMateriel.getUniteParAn();
        } else if(VOY.contentEquals(unit)) {
            this.lowestValueByVoy = refMateriel.getUniteParAn();
            this.highestValueByVoy = refMateriel.getUniteParAn();
        } else if (BAL.contentEquals(unit)){
            this.lowestValueByBall = refMateriel.getUniteParAn();
            this.highestValueByBall = refMateriel.getUniteParAn();
        }
    }
}
