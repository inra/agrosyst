package fr.inra.agrosyst.api.services.plot;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.AgrosystFilter;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

public class PlotFilter extends AgrosystFilter {
    
    @Serial
    private static final long serialVersionUID = -3436026948049713196L;
    
    protected List<String> domainIds = new ArrayList<>();

    protected List<String> growingSystemIds = new ArrayList<>();

    protected Boolean active;

    public List<String> getDomainIds() {
        return domainIds;
    }

    public void setDomainIds(List<String> domainIds) {
        this.domainIds = domainIds;
    }

    public List<String> getGrowingSystemIds() {
        return growingSystemIds;
    }

    public void setGrowingSystemIds(List<String> growingSystemIds) {
        this.growingSystemIds = growingSystemIds;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
