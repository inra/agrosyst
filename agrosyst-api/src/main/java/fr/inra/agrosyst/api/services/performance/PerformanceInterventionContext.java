package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, Code Lutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.CarriageAction;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.TillageAction;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.topia.persistence.TopiaEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformanceInterventionContext {

    public static final String DUMMY = "DUMMY";

    private static final String REFERENCES_DOSAGE_UI = "Dose pour '(%s-%s):%s %s'";

    private final String interventionId;
    private final String interventionName;
    private final Set<Integer> practicedOrDomainCampaigns;

    // used in fuel consumption
    private final ToolsCoupling toolsCoupling;// nullable
    private Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel;

    private final List<AbstractAction> actions;

    protected List<CroppingPlanSpecies> interventionSpecies;

    private Double interventionPsci;

    protected final Optional<BiologicalControlAction> optionalBiologicalControlAction;
    protected final Optional<CarriageAction> optionalCarriageAction;
    protected final Optional<HarvestingAction> optionalHarvestingAction;
    protected final Optional<IrrigationAction> optionalIrrigationAction;
    protected final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction;
    protected final Optional<OrganicFertilizersSpreadingAction> optionalOrganicFertilizersSpreadingAction;
    protected final Optional<List<OtherAction>> optionalOtherActions;
    protected final Optional<PesticidesSpreadingAction> optionalPesticidesSpreadingAction;
    protected final Optional<SeedingActionUsage> optionalSeedingActionUsage;
    protected final Optional<TillageAction> optionalTillageAction;
    protected final Optional<List<MaintenancePruningVinesAction>> optionalMaintenancePruningVinesActions;

    private Map<String, RefActaTraitementsProduit> refIdProduitToProducts;


    private final Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement = new HashMap<>();

    private final List<String> targetedIftRefDosageForPhytoInputs = new ArrayList<>();
    private final List<String> legacyReferencesDosagesUserInfos = new ArrayList<>();

    @Getter
    private final PriceConverterKeysToRate priceConverterKeysToRate;

    // Used for Indicator Semi Net Margin
    private double realEquipmentsExpenses = 0.0d;
    private double standardisedEquipmentsExpenses = 0.0d;

    // used for Indicator Gross Income & Indicator Semi Net Margin
    private double realGrossMarginWithoutAutoconsume = 0.0d;
    private double realGrossMarginWithAutoconsume = 0.0d;
    private double standardisedGrossMarginWithoutAutoconsume = 0.0d;
    private double standardisedGrossMarginWithAutoconsume = 0.0d;

    // used for Indicator Gross Income, computed on IndicatorGrossIncome
    protected double realProductWithoutAutoconsume = 0.0d;
    protected double realProductWithAutoconsume = 0.0d;
    // used for Indicator Gross Income, computed on IndicatorStandardisedGrossIncome
    protected double standardisedProductWithoutAutoconsume = 0.0d;
    protected double standardisedProductWithAutoconsume = 0.0d;

    protected double operatingExpenses = 0.0d;
    protected double standardisedOperatingExpenses = 0.0d;

    private Double fuelConsumption;
    private double fuelPricePerLiter;
    private double manualWorkforceCost;
    private double mechanizedWorkforceCost;
    private double fuelExpense;//fuelPricePerLiter * fuelConsumption;

    // used for Indicator Fuel Consumption & Indicator Mechanised Work Time
    private Double toolsCouplingWorkingTime;
    // used for Indicator Manual Workforce Expenses
    private Double manualWorkTime = 0.0d;
    // used for Indicator Mechanized Workforce Expenses
    private Double mechanizedWorkTime = 0.0d;
    // used for IndicatorUTH
    private Double totalWorkTime;

    // used for Indicator Total Workforce Expenses
    private Pair<Double, Double> manualWorkforceExpenses; // real - standardised
    // used for Indicator Total Workforce Expenses
    private Pair<Double, Double> mechanizedWorkforceExpenses; // real - standardised

    // used for Indicator Direct Margin
    private Pair<Double, Double> semiNetMarginWithoutAutoconsume; // real - standardised
    private Pair<Double, Double> semiNetMarginWithAutoconsume; // real - standardised

    // used for Indicator Direct Margin
    private Pair<Double, Double> totalWorkforceExpenses; // real - standardised

    // used for "Produit brut réel sans/avec l’autoconsommations"
    private List<HarvestingPrice> harvestingPrices;

    private Map<String, Double> cuQuantityByInputUsage = new HashMap<>();
    private Map<String, Double> sO3QuantityByInputUsage = new HashMap<>();
    private Map<String, Double> sQuantityByInputUsage = new HashMap<>();

    private Double fertiCuQuantity = null;
    private Double fertiSQuantity = null;
    private Double phytoCuQuantity = null;
    private Double phytoCuQuantityHts = null;
    private Double phytoSQuantity = null;
    private Double phytoSQuantityHts = null;

    // cuivre
    private List<UsageQsaResult> usageFertiCuQuantities = new ArrayList<>();
    private List<UsageQsaResult> usagePhytoCuQuantities = new ArrayList<>();
    private List<UsageQsaResult> usagePhytoCuQuantitiesHts = new ArrayList<>();
    // soufre
    private List<UsageQsaResult> usageFertiSQuantities = new ArrayList<>();
    private List<UsageQsaResult> usagePhytoSQuantities = new ArrayList<>();
    private List<UsageQsaResult> usagePhytoSQuantitiesHts = new ArrayList<>();

    private Map<HarvestingActionValorisation, PriceAndUnit> averageRefSellingPriceByValorisations = new HashMap<>();

    protected final MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode;

    protected Map<Pair<AbstractAction, AbstractInputUsage>, Double[]> mineralFertilizationAmountsByInput;
    protected Map<Pair<AbstractAction, AbstractInputUsage>, Double[]> organicFertilizationAmountsByInput;

    protected Map<AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages = new HashMap<>();

    protected Map<Pair<AbstractAction, AbstractPhytoProductInputUsage>, Double> neonicotinoidAmounts = new HashMap<>();
    protected Map<Pair<AbstractAction, AbstractPhytoProductInputUsage>, Double> soilAppliedHerbicideAmounts = new HashMap<>();

    /**
     * @param interventionId                                    not null
     * @param interventionName                                  not null
     * @param practicedOrDomainCampaigns                        not null
     * @param toolsCoupling                                     may be null
     * @param actions                                           all intervention actions
     * @param allSubstancesActivesCommissionEuropeenneByAmmCode Substances Actives Commission Européenne
     */
    public PerformanceInterventionContext(
            String interventionId,
            String interventionName,
            Set<Integer> practicedOrDomainCampaigns,
            ToolsCoupling toolsCoupling,
            List<AbstractAction> actions,
            PriceConverterKeysToRate priceConverterKeysToRate,
            List<HarvestingPrice> harvestingPrices,
            MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode) {

        this.interventionId = interventionId;
        this.interventionName = interventionName;
        this.practicedOrDomainCampaigns = practicedOrDomainCampaigns;
        this.toolsCoupling = toolsCoupling;
        this.actions = actions;
        this.priceConverterKeysToRate = priceConverterKeysToRate;
        this.harvestingPrices = harvestingPrices;
        this.allSubstancesActivesCommissionEuropeenneByAmmCode = allSubstancesActivesCommissionEuropeenneByAmmCode;

        BiologicalControlAction biologicalControlAction = null;
        CarriageAction carriageAction = null;
        HarvestingAction harvestingAction = null;
        IrrigationAction irrigationAction = null;
        List<MaintenancePruningVinesAction> maintenancePruningVinesActions = null;
        MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = null;
        OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction = null;
        List<OtherAction> otherActions = null;
        PesticidesSpreadingAction pesticidesSpreadingAction = null;
        SeedingActionUsage seedingActionUsage = null;
        TillageAction tillageAction = null;

        Collection<AbstractAction> contextActions = CollectionUtils.emptyIfNull(actions);
        for (AbstractAction action : contextActions) {

            AgrosystInterventionType agrosystInterventionType = action.getMainAction().getIntervention_agrosyst();
            switch (agrosystInterventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                        mineralFertilizersSpreadingAction = (MineralFertilizersSpreadingAction) action;
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ->
                        pesticidesSpreadingAction = (PesticidesSpreadingAction) action;
                case AUTRE -> {
                    otherActions = ListUtils.defaultIfNull(otherActions, new ArrayList<>());
                    otherActions.add((OtherAction) action);
                }
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER -> {
                    maintenancePruningVinesActions = ListUtils.defaultIfNull(maintenancePruningVinesActions, new ArrayList<>());
                    maintenancePruningVinesActions.add((MaintenancePruningVinesAction) action);
                }
                case EPANDAGES_ORGANIQUES ->
                        organicFertilizersSpreadingAction = (OrganicFertilizersSpreadingAction) action;
                case IRRIGATION -> irrigationAction = (IrrigationAction) action;
                case LUTTE_BIOLOGIQUE -> biologicalControlAction = (BiologicalControlAction) action;
                case RECOLTE -> harvestingAction = (HarvestingAction) action;
                case SEMIS -> {
                    if (action instanceof SeedingActionUsage) {
                        seedingActionUsage = (SeedingActionUsage) action;
                    }
                }
                case TRANSPORT -> carriageAction = (CarriageAction) action;
                case TRAVAIL_DU_SOL -> tillageAction = (TillageAction) action;
            }
        }

        this.optionalBiologicalControlAction = Optional.ofNullable(biologicalControlAction);
        this.optionalCarriageAction = Optional.ofNullable(carriageAction);
        this.optionalHarvestingAction = Optional.ofNullable(harvestingAction);
        this.optionalIrrigationAction = Optional.ofNullable(irrigationAction);
        this.optionalMaintenancePruningVinesActions = Optional.ofNullable(maintenancePruningVinesActions);
        this.optionalMineralFertilizersSpreadingAction = Optional.ofNullable(mineralFertilizersSpreadingAction);
        this.optionalOrganicFertilizersSpreadingAction = Optional.ofNullable(organicFertilizersSpreadingAction);
        this.optionalOtherActions = Optional.ofNullable(otherActions);
        this.optionalPesticidesSpreadingAction = Optional.ofNullable(pesticidesSpreadingAction);
        this.optionalSeedingActionUsage = Optional.ofNullable(seedingActionUsage);
        this.optionalTillageAction = Optional.ofNullable(tillageAction);
    }

    public void addValorisationRefPrice(HarvestingActionValorisation valorisation, PriceAndUnit averageRefSellingPriceForPriceUnit) {
        this.averageRefSellingPriceByValorisations.put(valorisation, averageRefSellingPriceForPriceUnit);
    }

    public void addLegacyReferencesDosagesUserInfos(String refDoseUi) {
        if (StringUtils.isNotBlank(refDoseUi)) {
            legacyReferencesDosagesUserInfos.add(refDoseUi);
        }
    }

    public List<String> getTargetIftRefDoseForProductDisplay() {
        return new ArrayList<>(targetedIftRefDosageForPhytoInputs.stream().filter(Objects::nonNull).toList());
    }

    public void addAllTargetedIftfDosageUiForPhytoInputs(Collection<String> refDoseUis) {
        targetedIftRefDosageForPhytoInputs.addAll(refDoseUis);
    }

    public void addAllTargetedIftRefDosageForPhytoUsages(Map<? extends AbstractInputUsage, UsagePerformanceResult> refDoseByUsages) {
        this.refVintageTargetIftDoseByUsages.putAll(refDoseByUsages);
    }

    public void addTraitementProduitCategByIdTraitements(Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> ratpToCateg) {
        this.traitementProduitCategByIdTraitement.putAll(ratpToCateg);
    }

    public void setRefActaTraitementsProduits(Set<RefActaTraitementsProduit> actaTraitementProducts_) {
        Set<RefActaTraitementsProduit> actaTraitementProducts = new HashSet<>(CollectionUtils.emptyIfNull(actaTraitementProducts_));
        Map<String, RefActaTraitementsProduit> refIdProduitToProducts = Maps.uniqueIndex(actaTraitementProducts, TopiaEntity::getTopiaId);
        this.refIdProduitToProducts = refIdProduitToProducts;
    }

    public Pair<Boolean, PriceAndUnit> getValorisationRefPrice(HarvestingActionValorisation valorisation) {
        if (averageRefSellingPriceByValorisations.containsKey(valorisation)) {
            PriceAndUnit value = averageRefSellingPriceByValorisations.get(valorisation);
            return Pair.of(true, value);
        }
        return Pair.of(false, null);
    }

    public void addUsageFertiCuQuantity(AbstractAction action, MineralProductInputUsage mineralProductInputUsage, double quantity) {
        usageFertiCuQuantities.add(new UsageQsaResult(action, mineralProductInputUsage, quantity));
    }

    public void addUsagePhytoCuQuantity(AbstractAction action, AbstractPhytoProductInputUsage usage, double quantity) {
        usagePhytoCuQuantities.add(new UsageQsaResult(action, usage, quantity));
    }

    public void addUsagePhytoCuQuantityHts(AbstractAction action, AbstractPhytoProductInputUsage usage, double quantity) {
        usagePhytoCuQuantitiesHts.add(new UsageQsaResult(action, usage, quantity));
    }

    public void addUsageFertiSQuantity(AbstractAction action, AbstractInputUsage inputUsage, double quantity) {
        usageFertiSQuantities.add(new UsageQsaResult(action, inputUsage, quantity));
    }

    public void addUsagePhytoSQuantity(AbstractAction action, AbstractPhytoProductInputUsage usage, double quantity) {
        usagePhytoSQuantities.add(new UsageQsaResult(action, usage, quantity));
    }

    public void addUsagePhytoSQuantityHts(AbstractAction action, AbstractPhytoProductInputUsage usage, double quantity) {
        usagePhytoSQuantitiesHts.add(new UsageQsaResult(action, usage, quantity));
    }
}
