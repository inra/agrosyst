package fr.inra.agrosyst.api.services.growingsystem;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class GrowingSystemDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 4363593785606956890L;

    protected String topiaId;
    protected String code;
    protected String name;
    protected String dephyNumber;
    protected Sector sector;
    protected boolean validated;
    protected boolean active;
    protected GrowingPlanDto growingPlan;
    protected Map<String, String> networks;

    // information calculée
    protected boolean userCanValidate;

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDephyNumber() {
        return dephyNumber;
    }

    public void setDephyNumber(String dephyNumber) {
        this.dephyNumber = dephyNumber;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public GrowingPlanDto getGrowingPlan() {
        return growingPlan;
    }

    public void setGrowingPlan(GrowingPlanDto growingPlan) {
        this.growingPlan = growingPlan;
    }

    public boolean isUserCanValidate() {
        return userCanValidate;
    }

    public void setUserCanValidate(boolean userCanValidate) {
        this.userCanValidate = userCanValidate;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public Map<String, String> getNetworks() {
        return networks;
    }

    public void setNetworks(Map<String, String> networks) {
        this.networks = networks;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GrowingSystemDto other = (GrowingSystemDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }
}
