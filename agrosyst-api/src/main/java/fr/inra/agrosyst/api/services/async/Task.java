package fr.inra.agrosyst.api.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;

import java.util.UUID;

/**
 * Représente un traitement, vraisemblablement asynchone
 *
 */
public interface Task {

    /**
     * Identifiant unique de la tâche
     */
    UUID getTaskId();

    /**
     * Sa description
     */
    String getDescription();

    /**
     * L'identifiant de l'utilisateur ayant déclenché la tâche
     */
    String getUserId();

    /**
     * L'utilisateur ayant déclenché la tâche s'il y en a un
     */
    String getUserEmail();

    /**
     * Indique si la tâche doit être mis en file ou pas.
     */
    default boolean mustBeQueued() {
        return false;
    }

    /**
     * Si la tâche doit être visible dans l'UI de gestion des tâches ou si c'est une tâche de fond.
     */
    default boolean isVisible() {
        return true;
    }

    default String stringify() {
        return MoreObjects.toStringHelper(this)
                .add("taskId", getTaskId())
                .add("description", getDescription())
                .add("authorId", getUserId())
                .add("authorEmail", getUserEmail())
                .add("visible", isVisible())
                .toString();
    }

}
