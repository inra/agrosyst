package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Getter
@Setter
public class PerformanceStatistics implements Serializable {
    
    @Serial
    private static final long serialVersionUID = -8902863627361580757L;

    protected LocalDateTime endAtTime;

    protected String performanceId;
    
    protected String startAt;
    
    protected String endAt;
    
    protected long nbTotalTask;
    
    protected long nbTaskPerformed;
    
    protected long nbPracticedPracticed;
    
    protected long nbEffectiveSdc;
    
    protected String performanceState;
}
