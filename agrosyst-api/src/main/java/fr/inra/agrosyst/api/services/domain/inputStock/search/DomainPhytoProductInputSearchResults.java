package fr.inra.agrosyst.api.services.domain.inputStock.search;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.referential.ProductType;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author Cossé David : cosse@codelutin.com
 *
 * Return the filters used to get phyto product
 *
 */

public record DomainPhytoProductInputSearchResults(
    /*
        in case there is only one result
     */
    String refActaTraitementsProduitId,
    String idProduit,
    Integer idTraitement,
    String key,

    /*
       in case there is one product
     */
    Set<PhytoProductUnit> phytoProductUnits,
    /*
        all product types matching the search
     */
    List<ProductType> productTypes,
    /*
        all traNames matching search
     */
    List<String> productName,
    /*
        all amm codes matching search
     */
    List<String> code_AMMs,
    /*
        all active substances matching search
     */
    List<String> activeSubstances) implements Serializable {}
