package fr.inra.agrosyst.api.entities;
/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class DomainSeedLotInputImpl extends DomainSeedLotInputAbstract {

    @Serial
    private static final long serialVersionUID = 4121134741274571568L;
    
    @Override
    public void setInputPrice(InputPrice inputPrice) {
        this.seedPrice = (SeedPrice) inputPrice;
    }
    
    @Override
    public InputPrice getInputPrice() {
        return this.seedPrice;
    }
    
}
