package fr.inra.agrosyst.api.services.action;
/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.YealdUnit;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class HarvestingActionValorisationDto implements Serializable {
    @Serial
    private static final long serialVersionUID = -116169287537324593L;
    
    protected String topiaId;
    
    /**
     * Nom de l'attribut en BD : speciesCode
     */
    @NonNull
    protected String speciesCode;
    
    @NonNull
    protected String destinationId;
    
    protected String destinationName;
    
    /**
     * Nom de l'attribut en BD : yealdUnit
     */
    @NonNull
    protected YealdUnit yealdUnit;
    
    /**
     * Nom de l'attribut en BD : salesPercent
     */
    protected int salesPercent;
    
    /**
     * Nom de l'attribut en BD : selfConsumedPersent
     */
    protected int selfConsumedPersent;
    
    /**
     * Nom de l'attribut en BD : noValorisationPercent
     */
    protected int noValorisationPercent;
    
    /**
     * Nom de l'attribut en BD : yealdMin
     */
    protected Double yealdMin;
    
    /**
     * Nom de l'attribut en BD : yealdMax
     */
    protected Double yealdMax;
    
    /**
     * Nom de l'attribut en BD : yealdAverage
     */
    protected double yealdAverage;
    
    /**
     * Nom de l'attribut en BD : yealdMedian
     */
    protected Double yealdMedian;
    
    /**
     * Nom de l'attribut en BD : beginMarketingPeriod
     */
    protected int beginMarketingPeriod;
    
    /**
     * Nom de l'attribut en BD : beginMarketingPeriodDecade
     */
    protected int beginMarketingPeriodDecade;
    
    /**
     * Nom de l'attribut en BD : beginMarketingPeriodCampaign
     */
    protected int beginMarketingPeriodCampaign;
    
    /**
     * Nom de l'attribut en BD : endingMarketingPeriod
     */
    protected int endingMarketingPeriod;
    
    /**
     * Nom de l'attribut en BD : endingMarketingPeriodDecade
     */
    protected int endingMarketingPeriodDecade;
    
    /**
     * Nom de l'attribut en BD : endingMarketingPeriodCampaign
     */
    protected int endingMarketingPeriodCampaign;
    
    /**
     * Nom de l'attribut en BD : organicCrop
     */
    protected boolean organicCrop;
    
    protected Collection<QualityCriteriaDto> qualityCriteriaDtos;
    
    protected HarvestingPriceDto priceDto;
    
    public Optional<String> getTopiaId() {
        return Optional.ofNullable(topiaId);
    }
    
    public Optional<HarvestingPriceDto> getPriceDto() {
        return Optional.ofNullable(priceDto);
    }
    
    public Optional<Collection<QualityCriteriaDto>> getQualityCriteriaDtos() {
        return Optional.ofNullable(qualityCriteriaDtos);
    }
}
