package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * Entité hors ToPIA permettant de stocker des groupes d'identifiants. {@link #cachekey} représente la clé du groupe.
 * {@link #ref} est une des valeurs du groupe.
 */
public class ComputedCache implements Serializable {

    @Serial
    private static final long serialVersionUID = -2579724665083428669L;

    protected String cachekey;
    protected String ref;

    public String getCachekey() {
        return cachekey;
    }

    public void setCachekey(String cachekey) {
        this.cachekey = cachekey;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComputedCache that = (ComputedCache) o;
        return cachekey.equals(that.cachekey) &&
                ref.equals(that.ref);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cachekey, ref);
    }

}
