package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Indicator on based on
 *
 * // practiced
 *
 *   Domain
 *     Crop
 *     Species
 *     ToolsCouplings
 *
 *     GrowingSystem
 *
 *       loop over indicators
 *
 *         PracticedSystem
 *
 *           cycles -> nodes -> connections (cumulativeFrequencies)-> interventions -> actions -> inputs
 *                                interventions
 *
 *
 *    loop over indicators scaled to domain
 *    loop over indicators reset result for practiced
 *
 * // effective
 *
 *   Domain
 *     Crop
 *     ToolsCouplings
 *
 *     GrowingSystem
 *
 *       Plot
 *
 *         Zone
 *
 *           loop over indicators
 *
 *       loop over indicators compute scale for CC and reset it
 *       loop over indicators compute for asked zone if some
 *       loop over indicators reset result for plot
 *
 * @author Cosse David cosse@codelutin.com
 */
@Getter
public class PerformanceGlobalExecutionContext {

    protected final RefSolTextureGeppa defaultRefSolTextureGeppa;
    protected final RefSolProfondeurIndigo defaultSolDepth;
    protected final RefLocation defaultLocation;// Paris
    protected final Map<String, String> groupesCiblesParCode;

    protected final Map<RefActaTraitementsProduit, List<RefActaSubstanceActive>> activeSubstancesByProducts = new HashMap<>();
    protected final Map<RefActaTraitementsProduit, RefActaTraitementsProduitsCateg> traitementProduitCategByIdTraitement = new HashMap<>();

    // gross Income, Standardized Gross Income
    
    protected List<RefPrixEspece> refPrixEspeces = new ArrayList<>();
    // standardized Operating Expenses
    protected List<RefPrixPhyto> refPrixPhytos = new ArrayList<>();

    // used into IndicatorStandardiedOperatingExpenses
    protected final Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByPhytoProductUnit = new HashMap<>();
    protected final Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByMineralProductUnit = new HashMap<>();
    protected final Map<OrganicProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByOrganicProductUnit = new HashMap<>();
    protected final Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingProductUnit = new HashMap<>();
    protected final Map<SubstrateInputUnit, List<RefInputUnitPriceUnitConverter>> convertersBySubstrateUnit = new HashMap<>();
    protected final Map<PotInputUnit, List<RefInputUnitPriceUnitConverter>> convertersByPotUnit = new HashMap<>();

    //PA a (unité unique) : pour le moment, l’utilisateur ne peut saisir un prix que dans une seule unité :
    //Euros/ha. Mais il faut prévoir à termes que l’utilisateur puisse saisir d’autres unités et donc adopter le
    //même fonctionnement que pour tous les autres intrants. Donnée saisie par l’utilisateur.
    protected final Map<OtherProductInputUnit, List<RefInputUnitPriceUnitConverter>> convertersByOtherProductUnit = new HashMap<>();
    
    public PerformanceGlobalExecutionContext(RefSolTextureGeppa defaultRefSolTextureGeppa,
                                             RefSolProfondeurIndigo defaultSolDepth,
                                             RefLocation defaultLocation,
                                             Map<String, String> groupesCiblesParCode) {
        this.defaultRefSolTextureGeppa = defaultRefSolTextureGeppa;
        this.defaultSolDepth = defaultSolDepth;
        this.defaultLocation = defaultLocation;
        this.groupesCiblesParCode = groupesCiblesParCode;
    }

    public void addActiveSubstancesByProducts(Map<RefActaTraitementsProduit, List<RefActaSubstanceActive>> activeSubstancesByProducts) {
        this.activeSubstancesByProducts.putAll(activeSubstancesByProducts);
    }
}
