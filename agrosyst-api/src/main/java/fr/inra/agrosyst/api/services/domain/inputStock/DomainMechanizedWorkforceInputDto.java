package fr.inra.agrosyst.api.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
import java.util.Objects;

@Value
@SuperBuilder(toBuilder = true)
public class DomainMechanizedWorkforceInputDto extends DomainInputDto {

    @Serial
    private static final long serialVersionUID = 5631077189366547012L;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DomainMechanizedWorkforceInputDto that = (DomainMechanizedWorkforceInputDto) o;
        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }
}
