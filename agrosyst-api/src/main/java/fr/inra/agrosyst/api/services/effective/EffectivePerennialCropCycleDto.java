package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.OrchardFrutalForm;
import fr.inra.agrosyst.api.entities.PollinatorSpreadMode;
import fr.inra.agrosyst.api.entities.VineFrutalForm;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleSpecies;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
public class EffectivePerennialCropCycleDto implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -6277623855806594633L;

    protected String topiaId;

    /**
     * Nom de l'attribut en BD : plantingYear
     */
    protected Integer plantingYear;

    /**
     * Nom de l'attribut en BD : plantingDensity
     */
    protected Double plantingDensity;

    /**
     * Nom de l'attribut en BD : plantingInterFurrow
     */
    protected Integer plantingInterFurrow;

    /**
     * Nom de l'attribut en BD : plantingSpacing
     */
    protected Integer plantingSpacing;

    /**
     * Nom de l'attribut en BD : plantingDeathRate
     */
    protected Double plantingDeathRate;

    /**
     * Nom de l'attribut en BD : plantingDeathRateMeasureYear
     */
    protected Integer plantingDeathRateMeasureYear;

    /**
     * Nom de l'attribut en BD : pollinator
     */
    protected boolean pollinator;

    /**
     * Nom de l'attribut en BD : pollinatorPercent
     */
    protected Double pollinatorPercent;

    /**
     * Nom de l'attribut en BD : otherCharacteristics
     */
    protected String otherCharacteristics;

    /**
     * Nom de l'attribut en BD : foliageHeight
     */
    protected Double foliageHeight;

    /**
     * Nom de l'attribut en BD : foliageThickness
     */
    protected Double foliageThickness;

    /**
     * Nom de l'attribut en BD : pollinatorSpreadMode
     */
    protected PollinatorSpreadMode pollinatorSpreadMode;

    /**
     * Nom de l'attribut en BD : weedType
     */
    protected WeedType weedType;

    /**
     * Nom de l'attribut en BD : orchardFrutalForm
     */
    protected OrchardFrutalForm orchardFrutalForm;

    /**
     * Nom de l'attribut en BD : vineFrutalForm
     */
    protected VineFrutalForm vineFrutalForm;

    /**
     * Nom de l'attribut en BD : species
     */
    protected Collection<EffectiveCropCycleSpecies> species;

    protected String croppingPlanEntryId;
    protected String croppingPlanEntryName;

    protected String orientationId;
    protected String orientationLabel;

    protected List<EffectiveCropCycleSpeciesDto> speciesDtos;

    protected List<EffectiveCropCyclePhaseDto> phaseDtos;

    public void addPhase(EffectiveCropCyclePhaseDto phaseDto) {
        if (phaseDtos == null) {
            phaseDtos = new ArrayList<>();
        }
        phaseDtos.add(phaseDto);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    public void addSpeciesDto(EffectiveCropCycleSpeciesDto speciesDto) {
        if (speciesDtos == null) {
            speciesDtos = new ArrayList<>();
        }
        speciesDtos.add(speciesDto);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EffectivePerennialCropCycleDto other = (EffectivePerennialCropCycleDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }
}
