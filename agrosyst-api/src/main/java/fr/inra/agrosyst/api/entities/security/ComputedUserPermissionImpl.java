package fr.inra.agrosyst.api.entities.security;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Objects;

import java.io.Serial;

public class ComputedUserPermissionImpl extends ComputedUserPermissionAbstract {

    @Serial
    private static final long serialVersionUID = 7017232072909011251L;

    @Override
    public boolean equals(Object obj) {
        // AThimel 08/10/13 Override Topia behavior to make it work without topiaId
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ComputedUserPermission other)) {
            return false;
        }
        if (getTopiaId() == null || other.getTopiaId() == null) {
            // Permissions are the same if they have the same type+object+action
            return Objects.equal(getType(), other.getType())
                    && Objects.equal(getObject(), other.getObject())
                    && Objects.equal(getAction(), other.getAction());
        }
        boolean result = getTopiaId().equals(other.getTopiaId());
        return result;
    }

    @Override
    public int hashCode() {
        int result = action;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (object != null ? object.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ComputedUserPermissionImpl{" +
                "topiaId='" + topiaId + '\'' +
                ", userId='" + userId + '\'' +
                ", type=" + type +
                ", object='" + object + '\'' +
                ", action=" + action +
                ", dirty=" + dirty +
                '}';
    }

} //ComputedUserPermissionImpl
