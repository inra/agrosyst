package fr.inra.agrosyst.api.services.performance.utils;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

public final class PhytoProductUnitConverter {

    private static final Map<Pair<PhytoProductUnit, PhytoProductUnit>, Double> PHYTO_PRODUCT_UNIT_CONVERTER =
            ImmutableMap.<Pair<PhytoProductUnit, PhytoProductUnit>, Double>builder()
                    .put(Pair.of(PhytoProductUnit.L_HA, PhytoProductUnit.L_HL), 0.1d)//     PSCI_PHYTO * (Dose appliquée/Dose_Ref)/10
                    .put(Pair.of(PhytoProductUnit.L_HL, PhytoProductUnit.L_HA), 1d)//       PSCI_PHYTO * (Dose appliquée/Dose_Ref) * Volume_Bouillie
                    .put(Pair.of(PhytoProductUnit.L_HL, PhytoProductUnit.L_HL), 10d)//      PSCI_PHYTO * (Dose appliquée * Volume Bouillie)/(Dose_Ref * 10)
                    .put(Pair.of(PhytoProductUnit.KG_HA, PhytoProductUnit.KG_HL), 0.1d)//   PSCI_PHYTO * (Dose appliquée/Dose_Ref) /10
                    .put(Pair.of(PhytoProductUnit.KG_HL, PhytoProductUnit.KG_HA), 1d)//     PSCI_PHYTO * (Dose appliquée /Dose_Ref) * Volume_Bouillie
                    .put(Pair.of(PhytoProductUnit.KG_HL, PhytoProductUnit.KG_HL), 10d)//    PSCI_PHYTO * (Dose appliquée * Volume Bouillie)/(Dose_Ref * 10)
                    .put(Pair.of(PhytoProductUnit.G_HA, PhytoProductUnit.G_HL), 0.1d)//     PSCI_PHYTO * (Dose appliquée/Dose_Ref) /10
                    .put(Pair.of(PhytoProductUnit.G_HL, PhytoProductUnit.G_HA), 1d)//       PSCI_PHYTO * (Dose appliquée /Dose_Ref) * Volume_Bouillie
                    .put(Pair.of(PhytoProductUnit.G_HL, PhytoProductUnit.G_HL), 10d)//      PSCI_PHYTO * (Dose appliquée * Volume Bouillie)/(Dose_Ref * 10)
                    .put(Pair.of(PhytoProductUnit.G_HA, PhytoProductUnit.KG_HA), 1 / 1000d)// PSCI_PHYTO * (Dose appliquée/Dose_Ref)/1000
                    .put(Pair.of(PhytoProductUnit.KG_HA, PhytoProductUnit.G_HA), 1000d)//   PSCI_PHYTO * (Dose appliquée/Dose_Ref)*1000
                    .put(Pair.of(PhytoProductUnit.G_HA, PhytoProductUnit.KG_HL), 1 / 10000d)// PSCI_PHYTO * (Dose appliquée/Dose_Ref)/(1000*10)
                    .put(Pair.of(PhytoProductUnit.KG_HA, PhytoProductUnit.G_HL), 10d)//    PSCI_PHYTO * (Dose appliquée/Dose_Ref)*1000/10 :dose saisie exprimée
                    //                                                                  par Ha (donc pas besoin de prendre en compte le volume de bouillie réel)
                    .put(Pair.of(PhytoProductUnit.G_HL, PhytoProductUnit.KG_HA), 1 / 1000d)// PSCI_PHYTO * (Dose appliquée /Dose_Ref) * Volume_Bouillie/1000
                    .put(Pair.of(PhytoProductUnit.KG_HL, PhytoProductUnit.G_HA), 1000d)//   PSCI_PHYTO * (Dose appliquée /Dose_Ref) *1000* Volume_Bouillie
                    .put(Pair.of(PhytoProductUnit.L_HA, PhytoProductUnit.KG_HA), 1.0d)
                    .put(Pair.of(PhytoProductUnit.ML_HA, PhytoProductUnit.KG_HA), 1 / 1000d)
                    .build();


    public static double getUnitConversionRatio(PhytoProductUnit appliedPhytoProductUnit, PhytoProductUnit unitDoseRefMaaUnit) {
        return PHYTO_PRODUCT_UNIT_CONVERTER.getOrDefault(Pair.of(appliedPhytoProductUnit, unitDoseRefMaaUnit), 0.0d);
    }

}
