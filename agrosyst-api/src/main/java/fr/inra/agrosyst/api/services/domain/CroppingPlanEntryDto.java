package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.DoseType;
import fr.inra.agrosyst.api.entities.EstimatingIftRules;
import fr.inra.agrosyst.api.entities.IftSeedsType;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.ListUtils;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Getter
@Setter
public class CroppingPlanEntryDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -2200338126273122174L;

    protected String topiaId;
    protected String code;
    protected boolean validated;
    protected boolean mixSpecies;
    protected boolean mixVariety;
    protected boolean mixCompanion;

    protected String name;
    protected CroppingEntryType type;
    protected Double sellingPrice;
    protected ArrayList<CroppingPlanSpeciesDto> species;

    protected String color;
    protected String typeImporterValue;

    // decisionnel
    protected Double yealdAverage;
    protected YealdUnit yealdUnit;
    protected Double biocontrolIFT;

    protected Double averageIFT;
    protected EstimatingIftRules estimatingIftRules;
    protected IftSeedsType iftSeedsType;
    protected DoseType doseType;

    // Prairie temporaire
    protected Boolean temporaryMeadow;
    
    // Prairie pâturée
    protected boolean pasturedMeadow;
    
    // Prairie fauchée
    protected boolean mowedMeadow;

    public void addSpecies(CroppingPlanSpeciesDto speciesDto) {
        if (species == null) {
            species = new ArrayList<>();
        }
        species.add(speciesDto);
    }

    public List<CroppingPlanSpeciesDto> getSpecies() {
        return ListUtils.emptyIfNull(species);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CroppingPlanEntryDto other = (CroppingPlanEntryDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }

    public boolean isIntermediate() {
        return CroppingEntryType.INTERMEDIATE.equals(type);
    }

    public boolean isCatchCrop() {
        return CroppingEntryType.CATCH.equals(type);
    }
}

