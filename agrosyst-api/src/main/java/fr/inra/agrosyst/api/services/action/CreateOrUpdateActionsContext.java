package fr.inra.agrosyst.api.services.action;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by davidcosse on 12/04/16.
 */
public class CreateOrUpdateActionsContext {

    protected final String practicedSystemCampaigns;
    // returned results
    // action or mapped by ids that can be a topiaId or an other generated id. This is done
    // for inputs to be able to find there actionToSave.
    private final Map<String, AbstractAction> savedActionsByOriginalIds = new HashMap<>();
    private final Map<String, HarvestingActionValorisation> savedActionValorisationsByOriginalIds = new HashMap<>();

    // constructor attributes
    private final Collection<AbstractAction> actionsToSave;
    protected final PracticedIntervention practicedIntervention;
    protected final EffectiveIntervention effectiveIntervention;
    protected final Set<String> speciesCodes;
    
    //  save actionToSave process attributes
    private Map<String, AbstractAction> actionFromDbByIds = new HashMap<>();
    private AbstractAction dbActionToUpdate;
    private AbstractAction currentActionToSave;
    
    // for harvesting action validation
    private final Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    private final Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant;
    
    public CreateOrUpdateActionsContext(Collection<AbstractAction> actionsToSave,
                                        Collection<AbstractAction> actionsFromDb,
                                        PracticedIntervention practicedIntervention,
                                        EffectiveIntervention effectiveIntervention,
                                        Set<String> speciesCodes,
                                        String practicedSystemCampaigns,
                                        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                                        Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant) {
        this.actionsToSave = actionsToSave;
        if (actionsFromDb != null) {
            actionFromDbByIds = Maps.newHashMap(Maps.uniqueIndex(actionsFromDb, Entities.GET_TOPIA_ID::apply));
        }
        this.practicedIntervention = practicedIntervention;
        this.effectiveIntervention = effectiveIntervention;
        this.speciesCodes = speciesCodes;
        this.practicedSystemCampaigns = practicedSystemCampaigns;
        this.speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        this.sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant;
    }

    public void addSavedAction(String originalId, AbstractAction action) {
        savedActionsByOriginalIds.put(originalId, action);
    }

    public AbstractAction getDbActionToUpdate() {
        return dbActionToUpdate;
    }

    public void setDbActionToUpdate(AbstractAction dbActionToUpdate) {
        this.dbActionToUpdate = dbActionToUpdate;
    }

    public Map<String, AbstractAction> getSavedActionsByOriginalIds() {
        return savedActionsByOriginalIds;
    }

    public Collection<AbstractAction> getActionsToSave() {
        return actionsToSave;
    }

    public PracticedIntervention getPracticedIntervention() {
        return practicedIntervention;
    }

    public EffectiveIntervention getEffectiveIntervention() {
        return effectiveIntervention;
    }

    public Map<String, AbstractAction> getActionFromDbByIds() {
        return actionFromDbByIds;
    }

    public Set<String> getSpeciesCodes() {
        return speciesCodes;
    }

    public void setCurrentActionToSave(AbstractAction actionToSave) {
        this.currentActionToSave = actionToSave;
    }

    public AbstractAction getCurrentActionToSave() {
        return currentActionToSave;
    }

    public void addSavedValorisationsByOriginalIds(String id, HarvestingActionValorisation valorisationByOriginalId) {
        this.savedActionValorisationsByOriginalIds.put(id, valorisationByOriginalId);
    }

    public Map<String, HarvestingActionValorisation> getSavedActionValorisationsByOriginalIds() {
        return savedActionValorisationsByOriginalIds;
    }

    public String getPracticedSystemCampaigns() {
        return practicedSystemCampaigns;
    }

    public Map<String, List<Pair<String, String>>> getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee() {
        return speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    }

    public Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant() {
        return sectorByCodeEspceBotaniqueCodeQualifiant;
    }
}
