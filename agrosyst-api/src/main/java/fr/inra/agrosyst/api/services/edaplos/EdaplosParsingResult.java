package fr.inra.agrosyst.api.services.edaplos;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EdaplosParsingResult implements Serializable {

    @Serial
    private static final long serialVersionUID = -2465365207118298415L;
    private boolean isInterventionsWithoutDestination = false;
    
    protected final List<EdaplosResultLog> errorMessages = new ArrayList<>();

    protected final List<EdaplosResultLog> warningMessages = new ArrayList<>();

    protected final List<EdaplosResultLog> infoMessages = new ArrayList<>();
    
    protected final Set<String> campaigns = new HashSet<>();

    protected EdaplosParsingStatus edaplosParsingStatus = EdaplosParsingStatus.SUCCESS;

    protected String filename;

    protected String documentIdentification;

    protected String documentTypeCode;

    protected String issuerIdentification;

    protected String issuerName;

    protected String softwareName;

    protected String softwareVersion;
    
    public void addDomainCampaigns(int campaign) {
        campaigns.add(Integer.toString(campaign));
    }
    
    public void addInfoMessage(String message, String currentPath) {
        infoMessages.add(new EdaplosResultLog(EdaplosResultLog.EdaplosResultLevel.INFO, message, currentPath));
    }

    public void addWarningMessage(String message, String currentPath) {
        warningMessages.add(new EdaplosResultLog(EdaplosResultLog.EdaplosResultLevel.WARNING, message, currentPath));
    }

    public void addErrorMessage(String message, String currentPath) {
        this.setEdaplosParsingStatus(EdaplosParsingStatus.FAIL);
        errorMessages.add(new EdaplosResultLog(EdaplosResultLog.EdaplosResultLevel.ERROR, message, currentPath));
    }
    
    public void addInterventionsWithoutDestinationWarningMessage() {
        if (!this.isInterventionsWithoutDestination) {
            this.isInterventionsWithoutDestination = true;
            warningMessages.add(new EdaplosResultLog(EdaplosResultLog.EdaplosResultLevel.WARNING, "La destination de la récolte est manquante sur toutes les interventions de récolte car elle n'est pas encore gérée dans eDaplos. La destination 'À compléter' a été mise par défaut et devra être modifiée systématiquement après import.", ""));
        }
    }
    
    public List<EdaplosResultLog> getMessages() {
        List<EdaplosResultLog> messages = new ArrayList<>(errorMessages);
        messages.addAll(warningMessages);
        messages.addAll(infoMessages);
        return messages;
    }

    public void addMessage(EdaplosResultLog log) {
        if (log.level == EdaplosResultLog.EdaplosResultLevel.ERROR) {
            errorMessages.add(log);
        } else if (log.level == EdaplosResultLog.EdaplosResultLevel.WARNING) {
            warningMessages.add(log);
        } else {
            infoMessages.add(log);
        }
    }
    
    public String getStudiedCampaigns() {
        return String.join(", ", campaigns);
    }

    public List<EdaplosResultLog> getErrorMessages() {
        return errorMessages;
    }

    public List<EdaplosResultLog> getWarningMessages() {
        return warningMessages;
    }

    public List<EdaplosResultLog> getInfoMessages() {
        return infoMessages;
    }

    public EdaplosParsingStatus getEdaplosParsingStatus() {
        return edaplosParsingStatus;
    }

    public void setEdaplosParsingStatus(EdaplosParsingStatus edaplosParsingStatus) {
        this.edaplosParsingStatus = edaplosParsingStatus;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public String getDocumentIdentification() {
        return documentIdentification;
    }

    public void setDocumentIdentification(String documentIdentification) {
        this.documentIdentification = documentIdentification;
    }

    public String getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(String documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getIssuerIdentification() {
        return issuerIdentification;
    }

    public void setIssuerIdentification(String issuerIdentification) {
        this.issuerIdentification = issuerIdentification;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getSoftwareName() {
        return softwareName;
    }

    public void setSoftwareName(String softwareName) {
        this.softwareName = softwareName;
    }

    public String getSoftwareVersion() {
        return softwareVersion;
    }

    public void setSoftwareVersion(String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }
}
