package fr.inra.agrosyst.api.services.domain.inputStock;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputType;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;
import java.io.Serializable;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class DomainInputDto implements Serializable {
    
    @Serial
    private static final long serialVersionUID = -450718609342131116L;
    
    @NonNull
    final InputType inputType;
    
    /**
     * Product name define by user
     */
    @NonNull
    final String inputName;

    /**
     * Only used if domain input does not exist and must be create
     */
    protected String domainId;
    protected String toCopyFromInputId;
    protected String toCopyToDomainId;

    protected String key;
    
    /**
     * Product trade name from referential
     * Nom commercial du produit
     */
    final String tradeName;

    final String tradeNameTranslated;

    /**
     * Can be null when created but not persisted yet
     *
     */
    final String topiaId;

    final String code;
    
    final InputPriceDto price;
    
    public DomainInputDto(@NonNull InputType inputType,
                          @NonNull String productName,
                          @NonNull String tradeName,
                          String translatedTradeName,
                          String topiaId,
                          String code,
                          InputPriceDto priceDto) {
        this.inputType = inputType;
        this.inputName = productName;
        this.tradeNameTranslated = translatedTradeName;
        this.topiaId = topiaId;
        this.code = code;
        this.price = priceDto;
        this.tradeName = tradeName;
    }
    
    public Optional<String> getTradeName() {
        return Optional.ofNullable(tradeName);
    }

    public Optional<String> getTopiaId() {
        return Optional.ofNullable(topiaId);
    }
    
    public Optional<InputPriceDto> getPrice() {
        return Optional.ofNullable(price);
    }

    public Optional<String> getDomainId() {
        return Optional.ofNullable(domainId);
    }
    public Optional<String> getToCopyFromInputId() {
        return Optional.ofNullable(toCopyFromInputId);
    }
    public Optional<String> getToCopyToDomainId() {
        return Optional.ofNullable(toCopyToDomainId);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainInputDto that = (DomainInputDto) o;
        return inputType == that.inputType;
    }

}
