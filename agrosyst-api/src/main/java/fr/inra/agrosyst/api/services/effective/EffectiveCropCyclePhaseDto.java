package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EffectiveCropCyclePhaseDto implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -5760233450105721205L;

    protected String topiaId;
    /**
     * Nom de l'attribut en BD : duration
     */
    protected Integer duration;

    /**
     * Nom de l'attribut en BD : edaplosIssuerId
     */
    protected String edaplosIssuerId;

    /**
     * Nom de l'attribut en BD : type
     */
    protected CropCyclePhaseType type;

    protected List<EffectiveInterventionDto> interventions;

    public void addIntervention(EffectiveInterventionDto interventionDto) {
        if (interventions == null) {
            interventions = new ArrayList<>();
        }
        interventions.add(interventionDto);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EffectiveCropCyclePhaseDto other = (EffectiveCropCyclePhaseDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }
}
