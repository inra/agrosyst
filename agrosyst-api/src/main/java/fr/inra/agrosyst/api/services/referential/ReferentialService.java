package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.referential.DestinationContext;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShape;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.search.DomainPhytoProductInputSearchResults;
import fr.inra.agrosyst.api.services.domain.inputStock.search.PhytoProductInputFilter;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.practiced.RefStadeEdiDto;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;

import java.io.InputStream;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

/**
 * Referential service.
 *
 * @author Eric Chatellier
 */
public interface ReferentialService extends AgrosystService {

    String DEFAULT_FALLBACK_TOPIA_ID = "_0000000-0000-0000-0000-000000000000";
    String DEFAULT_REF_ACTA_TRAITEMENTS_PRODUIT_ID = RefActaTraitementsProduit.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    String DEFAULT_REF_OTHER_INPUT_ID = RefOtherInput.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    String DEFAULT_REF_POT_ID = RefPot.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    String DEFAULT_REF_SUBSTRAT_ID = RefSubstrate.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    String DEFAULT_REF_FERTI_ORGA_ID = RefFertiOrga.class.getName() + DEFAULT_FALLBACK_TOPIA_ID;

    String VARIETE_GEVES = "geves";

    int ORGANIC_REF_TYPE_CONDUITE_REFERENCE_ID = 19948;
    int MISSING_REF_TYPE_CONDUITE_REFERENCE_ID = 0;

    String WINE = "ZMO";

    String N = "n";
    String P2_O5 = "p2O5";
    String K2_O = "k2O";
    String BORE = "bore";
    String CALCIUM = "calcium";
    String FER = "fer";
    String MANGANESE = "manganese";
    String MOLYBDENE = "molybdene";
    String MG_O = "mgO";
    String OXYDE_DE_SODIUM = "oxyde_de_sodium";
    String S_O3 = "sO3";
    String CUIVRE = "cuivre";
    String ZINC = "zinc";

    Function<String, FertiMinElement> FERTI_MIN_ELEMENT_NAME_TO_ENUM = value -> {
        FertiMinElement res = null;
        if (N.contentEquals(value)) {
            res = FertiMinElement.N;
        } else if (P2_O5.contentEquals(value)) {
            res = FertiMinElement.P2_O5;
        } else if (K2_O.contentEquals(value)) {
            res = FertiMinElement.K2_O;
        } else if (BORE.contentEquals(value)) {
            res = FertiMinElement.BORE;
        } else if (CALCIUM.contentEquals(value)) {
            res = FertiMinElement.CALCIUM;
        } else if (FER.contentEquals(value)) {
            res = FertiMinElement.FER;
        } else if (MANGANESE.contentEquals(value)) {
            res = FertiMinElement.MANGANESE;
        } else if (MOLYBDENE.contentEquals(value)) {
            res = FertiMinElement.MOLYBDENE;
        } else if (MG_O.contentEquals(value)) {
            res = FertiMinElement.MG_O;
        } else if (OXYDE_DE_SODIUM.contentEquals(value)) {
            res = FertiMinElement.OXYDE_DE_SODIUM;
        } else if (S_O3.contentEquals(value)) {
            res = FertiMinElement.S_O3;
        } else if (CUIVRE.contentEquals(value)) {
            res = FertiMinElement.CUIVRE;
        } else if (ZINC.contentEquals(value)) {
            res = FertiMinElement.ZINC;
        }
        return res;
    };

    static Integer getRefDoseMaxCampaign(Collection<RefMAADosesRefParGroupeCible> doseRefs) {
        int maxCampaign = doseRefs.stream()
                .map(RefMAADosesRefParGroupeCible::getCampagne)
                .filter(Objects::nonNull)
                .max(Integer::compareTo)
                .orElse(-1);
        return maxCampaign;
    }

    boolean isValidRefFertiMinProduct(RefFertiMinUNIFA product);

    /**
     * Retourne les valeurs des champs 'typeMateriel1' utilisé par les materiels groupé par
     * catégorie de materiel.
     * les petits matériels ne sont pas retournés
     *
     * @return values
     * @see RefMateriel#PROPERTY_TYPE_MATERIEL1
     */
    Map<MaterielType, List<Pair<String, String>>> getWithoutPetitMaterielTypeMateriel1List(boolean filterEdaplos);

    /**
     * Retourne les valeurs des champs 'typeMateriel1' utilisé par les petits materiels
     * Seuls les petits matériels sont retournés
     *
     * @return values
     * @see RefMateriel#PROPERTY_TYPE_MATERIEL1
     */
    Map<MaterielType, List<Pair<String, String>>> getPetitMaterielTypeMateriel1List();

    List<String> getRefFertiMinUnifTypeProduitForCateg(Integer categ);

    /**
     * Retourne les valeurs des champs 'typeMateriel2' utilisé par les materiels.
     *
     * @param filter filter
     * @return values
     * @see RefMateriel#PROPERTY_TYPE_MATERIEL2
     */
    List<Pair<String, String>> getTypeMateriel2List(TypeMaterielFilter filter);

    /**
     * Retourne les valeurs des champs 'typeMateriel3' utilisé par les materiels.
     *
     * @param filter filter
     * @return values
     * @see RefMateriel#PROPERTY_TYPE_MATERIEL3
     */
    List<Pair<String, String>> getTypeMateriel3List(TypeMaterielFilter filter);

    /**
     * Retourne les valeurs des champs 'typeMateriel4' utilisé par les materiels.
     *
     * @param filter filter
     * @return values
     * @see RefMateriel#PROPERTY_TYPE_MATERIEL4
     */
    List<Pair<String, String>> getTypeMateriel4List(TypeMaterielFilter filter);

    /**
     * Retourne les valeurs des champs 'typeMateriel4' utilisé par les materiels.
     *
     * @param filter filter
     * @return values
     * @see RefMateriel#PROPERTY_UNITE
     * @see RefMateriel#PROPERTY_UNITE_PAR_AN
     */
    Map<String, List<MaterielDto>> getMaterielUniteMap(TypeMaterielFilter filter);

    /**
     * Find materiel from topia id.
     *
     * @param materielTopiaId materiel topia id
     * @return a materiel
     */
    RefMateriel getMateriel(String materielTopiaId);

    /**
     * Return l'ensemble des régions utilisées par les sols arvalis.
     *
     * @return ensemble des regions
     */
    Map<Integer, String> getSolArvalisRegions();

    /**
     * Get ref sol arvalis filtered by domain region.
     *
     * @param regionCode region
     * @return sol arvalis for region
     */
    List<RefSolArvalis> getSolArvalis(Integer regionCode);


    List<RefEspece> findSpecies(String filter);

    /**
     * Valid that variety with same spece botanique code as the given one exists within GEVES or PLANT_GRAP referential
     *
     * @param variete               variete to valid
     * @param code_espece_botanique code to look for
     * @return true if valid
     */
    boolean validVarietesFromCodeEspeceEdi(RefVariete variete, String code_espece_botanique);

    RefEspece getSpecies(String speciesId);

    List<RefVariete> getVarietes(String speciesId, String filter);

    /**
     * Get a specific location from its identifier.
     *
     * @param refLocationId location id
     * @return RefLocationArvalis
     */
    RefLocation getRefLocation(String refLocationId);

    /**
     * Find all RefCommuneValues according to the filter
     *
     * @param filter filter
     * @return all RefCommuneValues
     */
    List<RefLocation> getActiveCommunes(String filter);

    /**
     * Find all RefCommuneValues according to the filter
     *
     * @param filter       filter
     * @param refCountryId topiaId du pays
     * @return all RefCommuneValues
     */
    List<RefLocation> getActiveCommunes(String filter, String refCountryId);

    /**
     * Get all Otex18 codes and wording.
     *
     * @return all Otex18 codes and wording.
     */
    Map<Integer, String> getAllActiveOtex18Code();

    /**
     * Get all Otex 70 codes filtered from the otex18code code.
     *
     * @param otex18code the otex 18 code to filter on.
     * @return all RefOtex filtered from the given filter.
     */
    Map<Integer, String> getAllActiveCodeOtex70ByOtex18code(Integer otex18code);

    /**
     * Find all RefOrientationEDI.
     *
     * @return all orientation EDI
     */
    List<RefOrientationEDI> getAllReferentielEDI();

    /**
     * Find all species with :
     * - colonne D (nom Section), valeur "Porte-greffes"
     * - "varietes PlantGrape" : colonne C (Utilisation), valeur "PG"
     * and name starting with 'term'.
     *
     * @param speciesId refEspeceId
     * @param term      search term
     * @return all matching varieties
     */
    List<RefVariete> getGraftSupports(String speciesId, String term);

    /**
     * Find all varieties with :
     * - "variétes Geves" : colonne P (Type Variétal), valeur "CLO"
     * - "Clones PlanGrape" : afficher à l'utilisateur le code clone AnnéeAgrement Origne
     * and name starting with 'term'.
     *
     * @param speciesId refEspeceId to search plant grape for
     * @param varietyId species's variety id
     * @param term      search term
     * @return all matching varieties
     */
    List<RefClonePlantGrape> getGraftClones(String speciesId, String varietyId, String term);

    /**
     * Find all active RefInterventionAgrosystTravailEDI
     *
     * @return all active RefInterventionAgrosystTravailEDI
     */
    List<RefInterventionAgrosystTravailEDI> getAllActiveAgrosystActions();

    /**
     * Find all active RefInterventionAgrosystTravailEDI according the interventionType given as parameter
     *
     * @param interventionType The action type to filter on.
     * @return All active RefInterventionAgrosystTravailEDI according the interventionType given as parameter.
     */
    List<RefInterventionAgrosystTravailEDI> getAllActiveAgrosystActions(AgrosystInterventionType interventionType);

    /**
     * Recuperation de tous les parcelles zonages actif.
     *
     * @return all active RefParcelleZonageEDI
     */
    List<RefParcelleZonageEDI> getAllActiveParcelleZonage();

    /**
     * Get all active sol textures.
     *
     * @return all active textures
     */
    List<RefSolTextureGeppa> getAllActiveSolTextures();

    List<RefSolTextureGeppa> getAllNotFrenchActiveSolTextures();

    /**
     * Get all active sol profondeurs.
     *
     * @return all active profondeurs
     */
    List<RefSolProfondeurIndigo> getAllActiveSolProfondeurs();

    /**
     * Get all active sol caracteristiques.
     *
     * @return all active sol caracteristiques
     */
    List<RefSolCaracteristiqueIndigo> getAllActiveSolCaracteristiques();

    /**
     * return all categories and ProductTypes from the actives RefFertiMinUNIFA
     *
     * @return all categories and ProductTypes from the actives RefFertiMinUNIFA
     */
    List<MineralProductType> getAllActiveMineralProductTypes();

    /**
     * Find all active RefFertiMinUnifa filter by category and shape.
     *
     * @param categ           The Fertilizer category
     * @param fertilizerShape the fertelizer shape
     * @return all RefFertiMinUnifa matching the condition.
     */
    List<RefFertiMinUNIFA> getAllActiveRefFertiMinUnifaByCategAndShape(Integer categ, String fertilizerShape);

    /**
     * Find all Actives RefFertiOrga
     *
     * @return All Actives RefFertiOrga
     */
    List<RefFertiOrga> getAllActiveOrganicProductTypes();

    /**
     * Return all actives RefStadeEDI with vegetativeProfile matching the one given in parameters.
     *
     * @param vegetativeProfile the vegetativeProfile to filter on
     * @return All actives RefStadeEDI with vegetativeProfile matching the one given in parameters.
     */
    List<RefStadeEdiDto> getRefStadesEdi(Integer vegetativeProfile);


    /**
     * return the treatment code and name from the Acta treatment referential.
     *
     * @return the treatment code and name
     */
    Map<String, String> getActaTreatementCodesAndNames();

    /**
     * return the list of ACTA treatment product type ordered by AgrosystInterventionType
     *
     * @return the map of ACTA treatment product type by AgrosystInterventionType
     */
    Map<AgrosystInterventionType, List<ProductType>> getAllActiveActaTreatmentProductTypes();

    /**
     * Return all active element de voisinage.
     *
     * @return all active element de voisinage
     */
    List<RefElementVoisinage> getAllActiveElementVoisinages();

    /**
     * Get all bio agressors depending on type ordered by name.
     *
     * @param category category
     * @return all bio agressors
     */
    List<RefBioAgressor> getTreatmentTargets(BioAgressorType category);

    /**
     * Return all active System Growing Characteristics including the characteristic types.
     *
     * @return all {@link RefTraitSdC}
     */
    List<RefTraitSdC> getAllActiveGrowingSystemCharacteristics();

    /**
     * Return all Active ActaTraitementsProduit according the given product type
     *
     * @param interventionType intervention type
     * @param productType      the product type to filter on
     * @return a list of traitement produit
     */
    List<RefActaTraitementsProduit> getActaTraitementsProduits(AgrosystInterventionType interventionType, ProductType productType);

    /**
     * Get RefBioAgressor with id the given id
     *
     * @param bioAgressorId RefBioAgressor id
     * @return the RefBioAgressor  with id the given id
     */
    RefBioAgressor getBioAgressor(String bioAgressorId);

    /**
     * Return RefDepartmentShape from the given departmentCode if found.
     *
     * @param departmentCode departmentCode where country shape can be found
     * @return RefDepartmentShape if found
     */
    RefDepartmentShape getDepartmentShape(String departmentCode);

    /**
     * @return Map of communeSite indexed by station Id
     */
    Map<String, String> getAllRefStationMeteoMap();

    /**
     * Create or update the given product
     *
     * @param product product to save
     * @return persisted product
     */
    RefFertiMinUNIFA createOrUpdateRefMineralProductToInput(RefFertiMinUNIFA product);

    /**
     * Create or update a RefFertiMinUNIFA product according the given DTO
     *
     * @param productDto productDto dta use for RefFertiMinUNIFA
     * @return persisted product
     */
    RefFertiMinUNIFA createOrUpdateRefMineralProductInput(DomainMineralProductInputDto productDto);

    /**
     * get CroppingPlanSpecies code by key.
     * The key is made of two parts (part 2 is optional)
     * part 1: the RefEspece id
     * part 2: (if there is a RefVariety) "_" + RefVariety Id
     *
     * @param allCroppingPlanSpecies the croppingPalnEntry where species come from
     * @return map of key to croppingPlanSpecies code
     */
    Map<String, String> getCroppingPlanSpeciesCodeByRefEspeceAndVarietyKey(Collection<CroppingPlanSpecies> allCroppingPlanSpecies);

    /**
     * return all destinations and qualityCriteria matching the given parameters
     *
     * @param speciesCodes      the species code to filter on
     * @param wineValorisations the valorizations to filter on for destination wine case, null safe
     * @param growingSystemId   optional
     * @param campaigns         optional
     * @param zoneTopiaId       optional
     * @return all destinations
     */
    DestinationContext getDestinationContext(Set<String> speciesCodes, Set<WineValorisation> wineValorisations, String growingSystemId, String campaigns, String zoneTopiaId);

    Map<Pair<String, String>, List<Sector>> getSectorsByCodeEspeceBotaniqueCodeQualifiantAEE(List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE);

    List<Pair<String, String>> getAllCodeEspeceBotaniqueCodeQualifantForDomainIds(Set<String> domainIds);

    Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Set<String> domainIds);

    /**
     * To get full list of sector you need to include values for (codeEspeceBotanique + "_ " + codeQualifiantAee) and (codeEspeceBotanique + "_ " + null)
     */
    Map<String, List<Sector>> getSectorsByCodeEspeceBotanique_CodeQualifiantAEE(List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE);

    LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForPracticedSystemValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            List<RefHarvestingPrice> refHarvestingPrices, Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);

    LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForPracticedSystemValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);

    LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            List<RefHarvestingPrice> refHarvestingPrices,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);

    LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);

    Map<String, Set<Pair<String, String>>> getCodeEspBotCodeQualiBySpeciesCode(Set<String> speciesCodes);

    EnumMap<Sector, List<RefMarketingDestination>> getRefMarketingDestinationsBySectorForSectors(Set<Sector> sectors);

    List<RefTraitSdC> getTraitSdcForIdsIn(List<String> growingSystemCharacteristicIds);

    RefTypeAgriculture getRefTypeAgricultureWithId(String typeAgricultureId);

    List<RefTypeAgriculture> getAllTypeAgricultures();

    List<RefTypeAgriculture> getIpmWorksTypeAgricultures();

    List<RefStrategyLever> loadRefStrategyLeversForTerm(Sector sector, SectionType sectionType, StrategyType strategyType, String term);

    List<RefStrategyLever> loadRefStrategyLevers(String growingSystemTopiaId, SectionType sectionType, StrategyType strategyType);

    List<RefAnimalType> getAllActiveRefAnimalTypes();

    List<RefAnimalTypeDto> getAllTranslatedActiveRefAnimalTypes();

    List<RefLegalStatus> getAllRefActiveLegalStatus();

    /**
     * Find all agressor from specified type.
     *
     * @param bioAgressorType bio agressor type
     * @return bio agressors
     */
    List<RefBioAgressor> getBioAgressors(Collection<BioAgressorType> bioAgressorType);

    /**
     * Find all agressor from specified type.
     *
     * @param bioAgressorType bio agressor type
     * @param sectors         (optionnal) sector filter
     * @return bio agressors
     */
    List<RefBioAgressor> getBioAgressors(Collection<BioAgressorType> bioAgressorType, Collection<Sector> sectors);

    /**
     * Export all materiel as CSV.
     *
     * @return xslx export ex stream
     */
    ExportResult exportAllMaterielXlsx();

    /**
     * Export all materiel as CSV.
     *
     * @return xlsx export ex stream
     */
    ExportResult exportAllSpeciesXslx();

    Collection<EquipmentUsageRange> getEquipmentlUsageRangeForEquipments(Collection<RefMateriel> materiels);

    List<RefCattleAnimalTypeDto> getAllTranslatedActiveRefCattleAnimalTypes();

    List<RefCattleRationAlimentDto> getAllTranslatedActiveRefCattleRationAliments();

    List<RefSubstrateDto> getAllActiveRefSubstrates();

    List<RefPotDto> getAllActiveRefPots();

    List<RefOtherInput> getAllActiveRefOtherInputs();

    ExportResult exportAllActiveRefOtherInputsToXlsx();

    ExportResult exportAllActiveRefSubstratesToXlsx();

    /**
     * If several species are specified, order by ‘L/HA’, ‘KG/ha’, ‘G/HA’, ‘L/HL’, ‘KG/HL’, ‘G/HL’
     *
     * @param refInputId   is the ID of the RefActaTraitementsProduit
     * @param refEspeceIds is a list of ID from RefEspece#topiaId
     * @return the computed dose
     */
    ReferenceDoseDTO computeReferenceDoseForIFT(String refInputId, Collection<String> refEspeceIds);

    /**
     * If several species are specified, order by ‘L/HA’, ‘KG/ha’, ‘G/HA’, ‘L/HL’, ‘KG/HL’, ‘G/HL’
     *
     * @param refInputId   is the ID from RefActaTraitementsProduits#id
     * @param refEspeceIds is a list of ID from RefEspece#topiaId
     * @return the computed dose
     */
    ReferenceDoseDTO computeLocalizedReferenceDoseForIFTLegacy(
            String refInputId,
            Set<String> refEspeceIds);

    /**
     * @param refProductId  the refActaTraitementsProduit id
     * @param refEspeceIds  the refEspece TopiaId
     * @param targetIds     the refCiblesAgrosystGroupesCiblesMAA cible_edi_ref_id or code_groupe_cible_maa
     * @param groupesCibles the phytoProductTarget codeGroupeCibleMaa
     * @return ReferenceDoseDTO
     */
    ReferenceDoseDTO computeReferenceDoseForIFTCibleNonMillesime(String refProductId,
                                                                 Collection<String> refEspeceIds,
                                                                 Collection<String> targetIds,
                                                                 Collection<String> groupesCibles);

    /**
     * @param refInputId    the refActaTraitementsProduit id
     * @param refEspeceIds  the refEspece TopiaId
     * @param targetIds     the refCiblesAgrosystGroupesCiblesMAA cible_edi_ref_id or code_groupe_cible_maa
     * @param groupesCibles the phytoProductTarget codeGroupeCibleMaa
     * @return ReferenceDoseDTO
     */
    ReferenceDoseDTO computeLocalizedReferenceDoseForIFCCibleNonMillesime(
            String refInputId,
            Set<String> refEspeceIds,
            Set<String> targetIds,
            Set<String> groupesCibles);

    ReferenceDoseDTO computeReferenceDoseForIFCCibleMillesime(
            String refInputId,
            Set<String> refEspeceIds,
            Set<String> bioAgressorIdentifiantOrReference_id,
            Set<String> groupesCibles,
            Integer campaign);

    ReferenceDoseDTO computeReferenceDoseForGivenProductAndActaSpecies(RefActaTraitementsProduit phytoProduct, Collection<RefEspece> refEspeces);

    /**
     * @param bioAgressorIdentifiantOrReferenceIds les identifiants des cibles EDI
     * @param groupesCibles                        les groupes cible déclaré dans UI ou associés à une cible
     * @param doseRefsSupplierByTargetId           fonction de trie des doses CodeAMM | CultureMAA | TraitementMAA
     * @return la dose de référence s'il y en a une
     */
    Optional<RefMAADosesRefParGroupeCible> computeMaaRefDose(
            Collection<String> bioAgressorIdentifiantOrReferenceIds,
            Collection<String> groupesCibles,
            Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> doseRefsSupplierByTargetId);

    Map<String, Boolean> getGroupesCiblesCodesAndGeneric(
            String property,
            String bioAgressorEdiIdsOrGroupesCible);

    List<RefCiblesAgrosystGroupesCiblesMAA> getRefCiblesAgrosystGroupesCiblesMAAS(String property, String bioAgressorEdiIdsOrGroupesCible);

    ExportResult exportPhytoAndLutteBioActaTraitementProduitsXlsx();

    InputStream exportCiblesGroupesCiblesMaaCSV();

    Collection<String> getCodeAmmBioControle(int campagne);

    Collection<String> getCodeAmmBioControle(String campagnes);

    List<GroupeCibleDTO> getGroupesCibles();

    Map<String, String> getGroupesCiblesParCode();

    Map<String, BioAgressorType> getGroupesCiblesReferenceParamParCode();

    Map<AgrosystInterventionType, String> getAgrosystInterventionTypeTranslationMap();

    <E extends ReferentialEntity> List<E> getAllActiveReferentialEntities(Class<E> clazz);

    RefPot loadRefPot(String refPotId);

    DomainReferentialInputs getDomainReferentialInputs(Collection<DomainInputDto> domainInputDtos);

    Collection<RefDestination> loadDestinationsForIds(Set<String> destinationIds);

    Collection<RefQualityCriteria> loadRefQualityCriteriaForIds(Set<String> qualityCriteriaRefIds);

    Collection<RefQualityCriteriaClass> loadRefQualityCriteriaClassForIds(Set<String> qualityCriteriaRefQualityCriteriaClassIds);

    Collection<RefInputUnitPriceUnitConverter> loadAllRefInputUnitPriceUnitConverter();

    RefStationMeteo findRefStationMeteoByTopiaId(String topiaId);

    DomainPhytoProductInputSearchResults getDomainPhytoProductInputSearchResults(PhytoProductInputFilter phytoProductInputFilter);

    DomainPhytoProductInputSearchResults getDomainBiologicalControlInputsSearchResults(PhytoProductInputFilter phytoProductInputFilter);

    RefInterventionAgrosystTravailEDI getRefInterventionAgrosystTravailEDI(String manActionId);

    RefDestination getDestination(String topiaId);

    RefQualityCriteria loadQualityCriteriaForId(String refQualityCriteriaId);

    RefDestination loadDestinationForId(String destinationId);

    Map<BioAgressorType, String> getTranslatedBioAgressorType();

    InputStream matchMaaWithFrenchMaa(InputStream contentStream);

    List<RefEspece> getAllActiveRefEspeceWithGivenCodeEspeceBotanique(Set<String> codeEspeceBotaniques);

    List<RefCompositionSubstancesActivesParNumeroAMM> getDomainRefCompositionSubstancesActivesParNumeroAMM(Domain domain);

    Map<String, Double> getCoeffsConversionVersKgHa();

    MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> getSubstancesActivesCommissionEuropeenneByAmmCodeForDomain(Domain domain);

    MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> getRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM(Domain domain);

    Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> findRefCorrespondanceMaterielOutilsTSForTools(Set<RefMateriel> materiels);

    List<SeedPlantUnit> getSeedUnitsForSpecies(List<RefEspece> species);

    /**
     * Find all RefFertiOrga including inactive ones if includeInactive i s true
     *
     * @return All Actives RefFertiOrga
     */
    Object getAllOrganicProductTypes(boolean includeInactive);

    Map<HarvestingActionValorisationDto, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisationDtos(
            Collection<HarvestingActionValorisationDto> valorisationDtos,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);
}
