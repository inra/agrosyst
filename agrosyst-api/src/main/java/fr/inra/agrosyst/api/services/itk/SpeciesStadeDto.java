package fr.inra.agrosyst.api.services.itk;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.practiced.RefStadeEdiDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author David Cossé
 */
@Getter
@Setter
public class SpeciesStadeDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 3998306410823259558L;

    private String topiaId;

    private String speciesCode;
    private String speciesName;
    private String varietyName;
    private String speciesAndVarietyName;
    private String mixSpeciesName;

    private RefStadeEdiDto stadeMin;
    private RefStadeEdiDto stadeMax;
}
