package fr.inra.agrosyst.api.services.common;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by davidcosse on 01/06/16.
 */
public class HarvestingValorisationPrices implements Serializable {

    @Serial
    private static final long serialVersionUID = 0L;

    protected Map<String, HarvestingValorisationPriceSummary> refHarvestingPrices;
    protected Map<String, HarvestingValorisationPriceSummary> userPriceSummaryByValorisations;

    public HarvestingValorisationPrices(
            Map<String, HarvestingValorisationPriceSummary> medianReferencePricesForValorisations,
            Map<String, HarvestingValorisationPriceSummary> userPriceSummaryByValorisations) {
        
        this.refHarvestingPrices = new HashMap<>(medianReferencePricesForValorisations);
        this.userPriceSummaryByValorisations = new HashMap<>(userPriceSummaryByValorisations);
    }

    public Map<String, HarvestingValorisationPriceSummary> getRefHarvestingPrices() {
        return refHarvestingPrices;
    }

    public void setRefHarvestingPrices(Map<String, HarvestingValorisationPriceSummary> refHarvestingPrices) {
        this.refHarvestingPrices = new HashMap<>(refHarvestingPrices);
    }

    public Map<String, HarvestingValorisationPriceSummary> getUserPriceSummaryByValorisations() {
        return userPriceSummaryByValorisations;
    }

    public void setUserPriceSummaryByValorisations(Map<String, HarvestingValorisationPriceSummary> userPriceSummaryByValorisations) {
        this.userPriceSummaryByValorisations = new HashMap<>(userPriceSummaryByValorisations);
    }
}
