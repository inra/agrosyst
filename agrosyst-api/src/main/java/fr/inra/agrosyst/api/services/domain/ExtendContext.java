package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GrowingSystem;

import java.util.HashMap;
import java.util.Map;

/**
 * Pour cloner une hierachie d'object, il faut conserver les associations
 * entre les objects initiaux et les clones.
 * 
 * Par exemple, {@code DomainService} clone les sols des domaines, et une fois
 * dans {@code PlotService}, il faut reutiliser les {@code Sol} déjà clonés et non les cloner à
 * nouveau.
 * 
 * Cet objet concerve des maps de clone entre service.
 * 
 * @author chatellier
 */
public class ExtendContext {

    /** Use extends context with extends code but do only duplication. */
    protected final boolean duplicateOnly;

    /** Les sols lié au domaine */
    protected final Map<Ground, Ground> groundCache = new HashMap<>();

    /** Les systemes de cultures lié au dispositifs. */
    protected final Map<GrowingSystem, GrowingSystem> growingSystemCache = new HashMap<>();

    public ExtendContext() {
        this(false);
    }

    public ExtendContext(boolean duplicateOnly) {
        this.duplicateOnly = duplicateOnly;
    }

    public boolean isDuplicateOnly() {
        return duplicateOnly;
    }

    public Map<Ground, Ground> getGroundCache() {
        return groundCache;
    }
    
    public void addGround(Ground from, Ground to) {
        groundCache.put(from, to);
    }
    public Map<GrowingSystem, GrowingSystem> getGrowingSystemCache() {
        return growingSystemCache;
    }
    
    public void addGrowingSystem(GrowingSystem from, GrowingSystem to) {
        growingSystemCache.put(from, to);
    }
    
}
