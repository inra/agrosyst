package fr.inra.agrosyst.api.services.generic;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.AgrosystFilter;

import java.io.Serial;
import java.util.Map;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class GenericFilter extends AgrosystFilter {

    @Serial
    private static final long serialVersionUID = -714575173262651636L;

    protected Map<String, String> propertyNamesAndValues;

    protected Boolean active;

    public Map<String, String> getPropertyNamesAndValues() {
        return propertyNamesAndValues;
    }

    public void setPropertyNamesAndValues(Map<String, String> propertyNamesAndValues) {
        this.propertyNamesAndValues = propertyNamesAndValues;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
