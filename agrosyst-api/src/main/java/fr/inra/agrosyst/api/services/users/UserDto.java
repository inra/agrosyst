package fr.inra.agrosyst.api.services.users;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.RoleType;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UserDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 9074960836078511518L;

    protected String topiaId;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected String organisation;
    protected String phoneNumber;
    protected String banner;
    protected boolean active;
    protected Set<RoleType> roles;
    protected boolean acceptedCharter;
    protected LocalDateTime lastMessageReadDate;
    protected String itEmail;
    protected String userLang;

    /**
     * Identifiant de la "session" en cours si le DTO est utilisé pour une authentification utilisateur. Une "session"
     * débute au moment où l'utilisateur se connecte avec l'action de Login et se termine à l'expiration ou la
     * révocation du cookie.
     */
    protected String sid;

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganisation() {
        return organisation;
    }

    public LocalDateTime getLastMessageReadDate() {
        return lastMessageReadDate;
    }

    public void setLastMessageReadDate(LocalDateTime lastMessageReadDate) {
        this.lastMessageReadDate = lastMessageReadDate;
    }
    
    public String getItEmail() {
        return itEmail;
    }
    
    public void setItEmail(String itEmail) {
        this.itEmail = itEmail;
    }
    
    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getUserLang() {
        return userLang;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<RoleType> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleType> roles) {
        this.roles = roles;
    }

    public boolean isAcceptedCharter() {
        return acceptedCharter;
    }

    public void setAcceptedCharter(boolean acceptedCharter) {
        this.acceptedCharter = acceptedCharter;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserDto other = (UserDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }

    public boolean isAdmin() {
        boolean result = roles != null && roles.contains(RoleType.ADMIN);
        return result;
    }

    public boolean isISDataProcessor() {
        boolean result = roles != null && roles.contains(RoleType.IS_DATA_PROCESSOR);
        return result;
    }

}
