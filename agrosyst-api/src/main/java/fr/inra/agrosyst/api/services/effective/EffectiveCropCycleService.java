package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.CattleDto;
import fr.inra.agrosyst.api.services.domain.ToolsCouplingDto;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * Service de gestion des interventions culturales (et des cycles liés à une parcelle).
 *
 * @author Eric Chatellier
 */
public interface EffectiveCropCycleService extends AgrosystService {

    String NEW_INTERVENTION_PREFIX = "NEW-INTERVENTION-";

    String NEW_NODE_PREFIX = "new-node-";

    Function<ToolsCoupling, String> GET_TOOLS_COUPLING_BY_CODE = input -> Strings.nullToEmpty(input.getCode());

    /**
     * Recherche paginée de la liste des zones des parcelles.
     *
     * @param filter filtre de pagination
     * @return plot list
     */
    PaginationResult<Zone> getFilteredZones(EffectiveZoneFilter filter);

    PaginationResult<ZoneDto> getFilteredZonesDto(EffectiveZoneFilter filter);

    /**
     * Recherche paginée de la liste des zones des parcelles avec les infos sur cultures et interventions.
     *
     * @param filter filtre de pagination
     * @return plot list
     */
    PaginationResult<ZoneDto> getFilteredZonesAndCroppingPlanInfosDto(EffectiveZoneFilter filter);

    String getCroppingPlanInfoForZone(Collection<Zone> zones);

    /**
     * Recherche de la liste des ids des zones des parcelles avec les infos sur cultures et interventions.
     *
     * @param filter filtre de pagination
     * @return plot id list
     */
    Set<String> getFilteredZonesAndCroppingPlanInfoIds(EffectiveZoneFilter filter);

    /**
     * Find zone by topia id.
     *
     * @param zoneTopiaId zone topia id
     * @return zone instance
     */
    Zone getZone(String zoneTopiaId);

    /**
     * Find zone's domain cropping plan entries.
     *
     * @param zone zone
     * @return zone's domain cropping plan entries
     */
    List<CroppingPlanEntry> getZoneCroppingPlanEntries(Zone zone);

    List<CroppingPlanEntry> getZoneCroppingPlanEntriesWithoutDomain(Zone zone);

    /**
     * Find cropping plan entry of last node defined for previous campaign.
     *
     * @param zoneTopiaId zone topia id
     * @return previous campaign last cropping plan
     */
    CroppingPlanEntry getPreviousCampaignCroppingPlanEntry(String zoneTopiaId);

    /**
     * Get all zone's perennial crop cycles (with phases, species, and interventions).
     *
     * @param zoneTopiaId zoneTopiaId
     * @return zone's perennial crop cycles
     */
    List<EffectivePerennialCropCycleDto> getAllEffectivePerennialCropCyclesDtos(String zoneTopiaId);

    /**
     * Get all plot's seasonal crop cycles (with nodes, connections, and interventions).
     *
     * @param zoneTopiaId zoneTopiaId
     * @return zone's seasonal crop cycles
     */
    List<EffectiveSeasonalCropCycleDto> getAllEffectiveSeasonalCropCyclesDtos(String zoneTopiaId);

    /**
     * Update zone's crop cycles.
     *
     * @param zoneId                          the identifier of the zone on which the crop cycle is about
     * @param effectiveSeasonalCropCycleDtos  seasonal crop cycles
     * @param effectivePerennialCropCycleDtos perennial crop cycles
     */
    void updateEffectiveCropCycles(String zoneId,
                                   List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycleDtos,
                                   List<EffectivePerennialCropCycleDto> effectivePerennialCropCycleDtos);

    Map<AgrosystInterventionType, List<ToolsCouplingDto>> getToolsCouplingModelForInterventionTypes(String zoneTopiaId, Set<AgrosystInterventionType> interventionTypes);

    void duplicateEffectiveCropCycles(String fromZoneId, String toZoneId);

    List<ZoneDto> getZones(Collection<String> zoneIds);

    ExportResult exportEffectiveCropCyclesAsXls(Collection<String> effectiveCropCycleIds);

    void exportEffectiveCropCyclesAsXlsAsync(Collection<String> effectiveCropCycleIds);

    /**
     * @param zoneTopiaId the zone from where the copy is done
     * @return an object containing all zone where interventions can be copied.
     */
    CopyPasteZoneByCampaigns getAvailableZonesForCopy(String zoneTopiaId);

    /**
     * Copy interventions to the given zone crops
     *
     * @param zones            define the intervention targeted crop
     * @param interventionDtos interventions to copy
     * @return true if complete migration (inputs, doses), false otherwise
     */
    boolean copyInterventions(List<TargetedZones> zones, List<EffectiveInterventionDto> interventionDtos);

    Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(String zoneId);

    Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Zone zone);

    Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant(String zoneId);

    Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant(Zone zone);

    Collection<CattleDto> getCattleForDomain(String domainId);

    EffectiveIntervention getEffectiveInterventionWithGivenId(String effectiveInterventionId);

    Pair<Integer, Integer> togglePlotOrZoneActiveStatus(List<String> zoneId, boolean plot, boolean activation);
}
