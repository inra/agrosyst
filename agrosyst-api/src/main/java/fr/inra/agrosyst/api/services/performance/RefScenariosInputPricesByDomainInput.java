package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;

import java.util.Map;
import java.util.Optional;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public record RefScenariosInputPricesByDomainInput(
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> mineralRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> speciesRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> fertiOrgaRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> phytoRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> irrigRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> fuelRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> otherRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> potRefPriceByScenarioForInput,
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> substrateRefPriceByScenarioForInput) {}
