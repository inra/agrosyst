package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

public class ReferentialEntityTranslation {

    protected final String topiaId;
    protected final Map<String, String> translationMap = new HashMap<>();

    public ReferentialEntityTranslation(String topiaId) {
        this.topiaId = topiaId;
    }

    public String getTopiaId() {
        return topiaId;
    }

    public void setPropertyTranslation(String property, String translation) {
        translationMap.put(property, translation);
    }

    public void setPropertyTranslationFromObject(String property, Object translation) {
        if (translation instanceof String) {
            translationMap.put(property, (String)translation);
        }
    }

    public String getPropertyTranslation(String property, String orDefault) {
        return translationMap.getOrDefault(property, orDefault);
    }
}
