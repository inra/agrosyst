package fr.inra.agrosyst.api.services.common;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.io.Serial;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class InputPriceFilter extends InputPriceDto {
    
    @Serial
    private static final long serialVersionUID = 6047608578932594389L;
    
    /**
     * RefInputPrice's targeted campaign
     */
    @NonNull
    Integer campaign;
    
    /**
     * DomainSeedSpeciesInput -> speciesSeed (CroppingPlanSpecies) -> species (RefEspece) -> code_espece_botanique
     */
    String code_espece_botanique;
    
    /**
     * DomainSeedSpeciesInput -> speciesSeed (CroppingPlanSpecies) -> species (RefEspece) -> code_qualifiant_AEE
     */
    String code_qualifiant_AEE;
    
}
