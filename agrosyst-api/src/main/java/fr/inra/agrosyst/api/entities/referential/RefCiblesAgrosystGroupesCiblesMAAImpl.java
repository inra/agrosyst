package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.io.Serial;

public class RefCiblesAgrosystGroupesCiblesMAAImpl extends RefCiblesAgrosystGroupesCiblesMAAAbstract {

    @Serial
    private static final long serialVersionUID = 1L;
    private String cible_edi_ref_label_Translated;
    private String groupe_cible_maa_Translated;

    public String getCible_edi_ref_label_Translated() {
        return StringUtils.firstNonBlank(cible_edi_ref_label_Translated, cible_edi_ref_label);
    }

    public void setCible_edi_ref_label_Translated(String cible_edi_ref_label_Translated) {
        this.cible_edi_ref_label_Translated = cible_edi_ref_label_Translated;
    }

    public String getGroupe_cible_maa_Translated() {
        return StringUtils.firstNonBlank(groupe_cible_maa_Translated, groupe_cible_maa);
    }

    public void setGroupe_cible_maa_Translated(String groupe_cible_maa_Translated) {
        this.groupe_cible_maa_Translated = groupe_cible_maa_Translated;
    }
} //RefCiblesAgrosystGroupesCiblesMAAImpl
