package fr.inra.agrosyst.api.services.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;

import java.io.InputStream;
import java.util.List;

/**
 * Referentiel service.
 *
 * @author Eric Chatellier
 */
public interface ExportService extends AgrosystService {

    /**
     * Export selected entity ids as csv.
     * 
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportSolArvalisCSV(Iterable<String> entityIds);

    /**
     * Export selected entity ids as csv.
     * 
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportOrientationEdiCSV(Iterable<String> entityIds);
    
    /**
     * Export des materiels tracteurs au format csv.
     *
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportMaterielTracteursCSV(Iterable<String> entityIds);

    /**
     * Export des materiels automoteurs au format csv.
     *
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportMaterielAutomoteursCSV(Iterable<String> entityIds);

    /**
     * Export des materiels outils au format csv.
     *
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportMaterielOutilsCSV(Iterable<String> entityIds);

    /**
     * Export des materiels migration au format csv.
     *
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportMaterielIrrigationCSV(Iterable<String> entityIds);

    /**
     * Import des statuts juridiques.
     *
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportLegalStatusCSV(Iterable<String> entityIds);

    InputStream exportEspeces(Iterable<String> entityIds);

    InputStream exportVarietesGeves(Iterable<String> entityIds);

    InputStream exportVarietesPlantGrape(Iterable<String> entityIds);

    InputStream exportClonesPlantGrape(Iterable<String> entityIds);

    InputStream exportEspecesToVarietes(Iterable<String> entityIds);

    /**
     * Export du référentiel OTEX au format csv.
     * 
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportOtexCSV(Iterable<String> entityIds);

    InputStream exportInterventionAgrosystTravailEdiCSV(Iterable<String> entityIds);
    
    InputStream exportStadesEdiCSV(Iterable<String> entityIds);

    /**
     * Export du référentiel sol texture geppa.
     * 
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportSolTextureGeppa(Iterable<String> entityIds);

    /**
     * Export du référentiel zonage parcelle edi.
     * 
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportZonageParcelleEdi(Iterable<String> entityIds);

    /**
     * Export du référentiel sol profondeur indigo.
     * 
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportSolProfondeurIndigo(Iterable<String> entityIds);

    /**
     * Export du référentiel sol caracteristiques indigo.
     * 
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportSolCarateristiquesIndigo(Iterable<String> entityIds);

    /**
     * Export du référentiel unitesEDI.
     *
     * @param entityIds entity to export
     * @return csv export ex stream
     */
    InputStream exportUniteEDI(Iterable<String> entityIds);

    /**
     * Export du référentiel FertiMinUNIFA
     * @param entityIds entity to export
     * @return csv export
     */
    InputStream exportFertiMinUNIFA(Iterable<String> entityIds);
    
    /**
     * Export du référentiel Adventices.
     * @param entityIds entity to export
     * @return csv export
     */
    InputStream exportAdventices(Iterable<String> entityIds);
    
    InputStream exportAgsAmortissement(Iterable<String> entityIds);

    /**
     * Export du référentiel NuisiblesEDI.
     * @param entityIds entity to export
     * @return csv export
     */
    InputStream exportNuisiblesEDI(Iterable<String> entityIds);

    /**
     * Export du référentiel Ferti orga.
     * @param entityIds entity to export
     * @return csv export
     */
    InputStream exportFertiOrga(Iterable<String> entityIds);

    /**
     * Export du référentiel station meteo.
     * @param entityIds entity to export
     * @return csv export
     */
    InputStream exportStationMeteo(Iterable<String> entityIds);

    InputStream exportGesCarburants(Iterable<String> entityIds);

    InputStream exportGesEngrais(Iterable<String> entityIds);

    InputStream exportGesPhyto(Iterable<String> entityIds);

    InputStream exportGesSemences(Iterable<String> entityIds);

    InputStream exportNrjCarburants(Iterable<String> entityIds);

    InputStream exportNrjEngrais(Iterable<String> entityIds);

    InputStream exportNrjPhyto(Iterable<String> entityIds);

    InputStream exportNrjSemences(Iterable<String> entityIds);

    InputStream exportNrjGesOutils(Iterable<String> entityIds);
    
    InputStream exportMesure(Iterable<String> entityIds);
    
    InputStream exportSupportOrganeEDI(Iterable<String> entityIds);
    
    InputStream exportStadeNuisibleEDI(Iterable<String> entityIds);
    
    InputStream exportTypeNotationEDI(Iterable<String> entityIds);
    
    InputStream exportValeurQualitativeEDI(Iterable<String> entityIds);
    
    InputStream exportUnitesQualifiantEDI(Iterable<String> entityIds);

    InputStream exportActaTraitementsProducts(Iterable<String> entityIds);
    
    InputStream exportActaSubstanceActive(Iterable<String> entityIds);
    
    InputStream exportProtocoleVgObs(Iterable<String> entityIds);
    
    InputStream exportElementVoisinage(Iterable<String> entityIds);

    InputStream exportPhytoSubstanceActiveIphy(Iterable<String> entityIds);

    InputStream exportTypeAgriculture(Iterable<String> entityIds);

    InputStream exportActaDosageSpc(Iterable<String> entityIds);

    InputStream exportActaGroupeCultures(Iterable<String> entityIds);
    
    InputStream exportSaActaIphy(Iterable<String> entityIds);

    InputStream exportTraitSdC(Iterable<String> entityIds);

    InputStream exportCultureEdiGroupeCouvSol(Iterable<String> entityIds);

    InputStream exportCouvSolPerenne(Iterable<String> entityIds);

    InputStream exportCouvSolAnnuelle(Iterable<String> entityIds);

    InputStream exportActaTraitementsProductsCateg(List<String> entityIds);

    InputStream exportZoneClimatiqueIphy(List<String> entityIds);

    InputStream exportDestination(List<String> entityIds);

    InputStream exportQualityCriteria(List<String> entityIds);

    InputStream exportRefHarvestingPrice(List<String> entityIds);

    InputStream exportRefSpeciesToSector(List<String> entityIds);

    InputStream exportRefHarvestingPriceConverter(List<String> entityIds);

    InputStream exportRefStrategyLever(List<String> entityIds);

    ExportResult exportActiveRefStrategyLeverXlsx();

    ExportResult exportActiveRefBioAggressorsXlsx();

    InputStream exportRefAnimalType(List<String> entityIds);

    InputStream exportRefMarketingDestination(List<String> entityIds);

    InputStream exportRefWorkItemInputEdi(List<String> entityIds);
    
    InputStream exportRefQualityCriteriaClass(List<String> entityIds);

    InputStream exportRefActaDosageSaRoot(List<String> entityIds);

    InputStream exportRefActaProduitRoot(List<String> entityIds);

    InputStream exportRefActaDosageSpcRoot(List<String> entityIds);

    InputStream exportRefPrixCarbuCSV(List<String> entityIds);

    InputStream exportRefPrixEspeceCSV(List<String> entityIds);

    InputStream exportRefPrixFertiMinCSV(List<String> entityIds);

    InputStream exportRefPrixFertiOrgaCSV(List<String> entityIds);

    InputStream exportRefPrixPhytoCSV(List<String> entityIds);

    InputStream exportRefPrixIrrigCSV(List<String> entityIds);

    InputStream exportRefPrixAutreCSV(List<String> entityIds);

    InputStream exportRefInputUnitPriceUnitConverterCSV(List<String> entityIds);

    InputStream exportRefEspeceOtherToolsCSV(List<String> entityIds);

    InputStream exportRefEdaplosTypeTraitement(List<String> entityIds);

    InputStream exportRefCattleAnimalType(List<String> entityIds, List<RefAnimalType> animalTypes);

    InputStream exportRefCattleRationAliment(List<String> entityIds);

    InputStream exportRefMAADosesRefParGroupeCible(List<String> entityIds);

    InputStream exportRefMAABiocontrole(List<String> entityIds);

    InputStream exportRefCiblesAgrosystGroupesCiblesMAA(List<String> entityIds);
    
    InputStream exportRefFeedbackRouter(List<String> entityIds);

    InputStream exportRefSubstrate(List<String> entityIds);

    InputStream exportRefPrixSubstrate(List<String> entityIds);

    InputStream exportRefPot(List<String> entityIds);

    InputStream exportRefPrixPot(List<String> entityIds);

    InputStream exportRefGroupeCibleTraitement(List<String> entityIds);

    InputStream exportRefCountry(List<String> entityIds);

    InputStream exportReferentialI18Entry(Class<? extends ReferentialI18nEntry> klass);

    InputStream exportRefLocation(List<String> entityIds);
    
    InputStream exportRefOtherInput(List<String> entityIds);

    InputStream exportRefSubstancesActivesCommissionEuropeenne(List<String> entityIds);
    
    InputStream exportRefCompositionSubstancesActivesParNumeroAMM(List<String> entityIds);

    InputStream exportRefPhrasesRisqueEtClassesMentionDangerParAMM(List<String> entityIds);

    InputStream exportRefConversionUnitesQSA(List<String> entityIds);

    InputStream exportRefCorrespodanceMaterielOutilsTS(List<String> entityIds);

    InputStream exportRefSeedUnits(Iterable<String> entityIds);
}
