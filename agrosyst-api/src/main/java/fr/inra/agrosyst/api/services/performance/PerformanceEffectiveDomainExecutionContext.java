package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.BasicPlot;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
/*
 * @author Cosse David cosse@codelutin.com
 */
public class PerformanceEffectiveDomainExecutionContext {

    protected final Pair<Domain, Domain> domainAndAnonymizeDomain;

    protected final Iterable<ToolsCoupling> toolsCouplings;
    protected final List<CroppingPlanEntry> crops;
    protected final List<CroppingPlanSpecies> croppingPlanSpecies;
    protected final Map<CroppingPlanEntry, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrops;// IPHY (disable for the moment)

    protected final RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput;

    protected final RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput;

    protected final Collection<String> codeAmmBioControle;
    protected final MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey;// used into IndicatorStandardisedGrossIncome

    protected final Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments;

    protected final Collection<EquipmentUsageRange> equipmentUsageRanges;

    protected final Optional<RefPrixCarbu> optionalRefFuelPricePerLiter;

    protected final double fuelPricePerLiter;
    private final double manualWorkforceCost;
    private final double mechanizedWorkforceCost;
    protected final List<HarvestingPrice> harvestingPrices;

    protected final Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> allDomainSubstancesByAmm;
    protected final MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode;
    protected final MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM;

    // Débit de chantier
    protected Map<ToolsCoupling, Pair<Double, MaterielWorkRateUnit>> toolsCouplingWorkRates = new HashMap<>();
    protected Map<Optional<GrowingSystem>, PerformanceGrowingSystemExecutionContext> growingSystemContextByGrowingSystems = new HashMap<>();
    protected Map<BasicPlot, Set<PerformanceZoneExecutionContext>> plotZoneContext = new HashMap<>();

    protected Double domainIndicatorSurfaceUTH;

    public PerformanceEffectiveDomainExecutionContext(Pair<Domain, Domain> domainAndAnonymizeDomain,
                                                      Iterable<ToolsCoupling> toolsCouplings,
                                                      Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments,
                                                      Collection<EquipmentUsageRange> equipmentUsageRanges,
                                                      List<CroppingPlanEntry> crops,
                                                      List<CroppingPlanSpecies> croppingPlanSpecies,
                                                      Map<CroppingPlanEntry, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrops,
                                                      RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput,
                                                      RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
                                                      MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey,
                                                      double fuelPricePerLiter,
                                                      double manualWorkforceCost,
                                                      double mechanizedWorkforceCost,
                                                      Collection<String> codeAmmBioControle,
                                                      Optional<RefPrixCarbu> optionalRefFuelPricePerLiter,
                                                      List<HarvestingPrice> harvestingPrices,
                                                      Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> allDomainSubstancesByAmm,
                                                      MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode,
                                                      MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM) {
        this.domainAndAnonymizeDomain = domainAndAnonymizeDomain;
        this.toolsCouplings = toolsCouplings;
        this.deprecationRateByEquipments = deprecationRateByEquipments;
        this.equipmentUsageRanges = equipmentUsageRanges;
        this.crops = crops;
        this.croppingPlanSpecies = croppingPlanSpecies;
        this.speciesMaxCouvSolForCrops = speciesMaxCouvSolForCrops;
        this.refInputPricesForCampaignsByInput = refInputPricesForCampaignsByInput;
        this.refScenariosInputPricesByDomainInput = refScenariosInputPricesByDomainInput;
        this.harvestingPrices = harvestingPrices;
        this.codeAmmBioControle = codeAmmBioControle;
        this.fuelPricePerLiter = fuelPricePerLiter;
        this.manualWorkforceCost = manualWorkforceCost;
        this.mechanizedWorkforceCost = mechanizedWorkforceCost;
        this.refScenarioHarvestingPricesByValorisationKey = refScenarioHarvestingPricesByValorisationKey;
        this.optionalRefFuelPricePerLiter = optionalRefFuelPricePerLiter;
        this.allDomainSubstancesByAmm = allDomainSubstancesByAmm;
        this.allSubstancesActivesCommissionEuropeenneByAmmCode = allSubstancesActivesCommissionEuropeenneByAmmCode;
        this.refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM = refPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM;
    }

    public PerformanceEffectiveDomainExecutionContext(Pair<Domain, Domain> domainAndAnonymizeDomain,
                                                      Iterable<ToolsCoupling> toolsCouplings,
                                                      Map<RefMateriel, RefAgsAmortissement> deprecationRateByEquipments,
                                                      Collection<EquipmentUsageRange> equipmentUsageRanges,
                                                      List<CroppingPlanEntry> crops,
                                                      List<CroppingPlanSpecies> croppingPlanSpecies,
                                                      Map<CroppingPlanEntry, RefCultureEdiGroupeCouvSol> speciesMaxCouvSolForCrops,
                                                      RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput,
                                                      RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
                                                      MultiValuedMap<String, RefHarvestingPrice> refScenarioHarvestingPricesByValorisationKey,
                                                      double fuelPricePerLiter,
                                                      double manualWorkforceCost,
                                                      double mechanizedWorkforceCost,
                                                      Collection<String> codeAmmBioControle,
                                                      Optional<RefPrixCarbu> optionalRefFuelPricePerLiter,
                                                      List<HarvestingPrice> harvestingPrices) {
        this(domainAndAnonymizeDomain, toolsCouplings, deprecationRateByEquipments, equipmentUsageRanges, crops,
                croppingPlanSpecies, speciesMaxCouvSolForCrops, refInputPricesForCampaignsByInput,
                refScenariosInputPricesByDomainInput, refScenarioHarvestingPricesByValorisationKey, fuelPricePerLiter,
                manualWorkforceCost, mechanizedWorkforceCost, codeAmmBioControle, optionalRefFuelPricePerLiter,
                harvestingPrices, Map.of(), new HashSetValuedHashMap<>(), new HashSetValuedHashMap<>());
    }

    public Domain getDomain() {
        return domainAndAnonymizeDomain.getLeft();
    }

    public Domain getAnonymiseDomain() {
        return domainAndAnonymizeDomain.getRight();
    }

    public Pair<Double, MaterielWorkRateUnit> getToolsCouplingWorkRate(ToolsCoupling toolsCoupling) {
        return toolsCouplingWorkRates.get(toolsCoupling);
    }

    public void setToolsCouplingWorkRate(ToolsCoupling toolsCoupling, Pair<Double, MaterielWorkRateUnit> workRate) {
        toolsCouplingWorkRates.put(toolsCoupling, workRate);
    }

    public void addZoneContext(PerformanceZoneExecutionContext performanceZoneExecutionContext) {
        PerformancePlotExecutionContext plotContext = performanceZoneExecutionContext.getPlotContext();
        Set<PerformanceZoneExecutionContext> plotZoneContexts = plotZoneContext.computeIfAbsent(plotContext.getAnonymizePlot(), k -> new HashSet<>());
        plotZoneContexts.add(performanceZoneExecutionContext);
    }

    public void addGrowingSystemContext(PerformanceGrowingSystemExecutionContext growingSystemContext) {
        growingSystemContextByGrowingSystems.put(growingSystemContext.getGrowingSystem(), growingSystemContext);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformanceEffectiveDomainExecutionContext that = (PerformanceEffectiveDomainExecutionContext) o;
        if (domainAndAnonymizeDomain.equals(that.domainAndAnonymizeDomain)) return true;
        return domainAndAnonymizeDomain.getLeft().equals(that.domainAndAnonymizeDomain.getLeft());
    }

    @Override
    public int hashCode() {
        return Objects.hash(domainAndAnonymizeDomain.getLeft());
    }
}
