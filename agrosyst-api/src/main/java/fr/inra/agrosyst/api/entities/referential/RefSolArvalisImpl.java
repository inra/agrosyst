package fr.inra.agrosyst.api.entities.referential;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.inra.agrosyst.api.services.referential.ImportStatus;
import org.apache.commons.lang3.StringUtils;

import java.io.Serial;
import java.util.Objects;

public class RefSolArvalisImpl extends RefSolArvalisAbstract {

    @Serial
    private static final long serialVersionUID = 3991937933089189944L;

    public static final String PROPERTY_STATUS = "status";

    public static final String PROPERTY_AGROSYST_ID = "agrosystId";

    // For Import API
    protected String agrosystId;
    protected ImportStatus status;
    protected String nom_referenciel_pedologique_francais;
    protected String sol_nom_Translated;
    protected String sol_texture_Translated;
    protected String sol_calcaire_Translated;
    protected String sol_profondeur_Translated;
    protected String sol_hydromorphie_Translated;
    protected String sol_pierrosite_Translated;


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;

        if (o == null) {
            return false;
        }

        if (o instanceof RefSolArvalis that) {
    
            String idTypeSol = that.getId_type_sol();
            String nom = that.getSol_nom();
            String calcaire = that.getSol_calcaire();
            String hydromorphie = that.getSol_hydromorphie();
            String pierrosite = that.getSol_pierrosite();
            String profondeur = that.getSol_profondeur();
            String texture = that.getSol_texture();
            String region = that.getSol_region();
            return checkEquals(idTypeSol, nom, calcaire, hydromorphie, pierrosite, profondeur, texture, region);
        } else {
            return false;
        }
    }

    protected boolean checkEquals(String idTypeSol, String nom, String calcaire, String hydromorphie, String pierrosite, String profondeur, String texture, String region) {
        boolean result =  (Objects.equals(this.id_type_sol, idTypeSol))
                && (this.sol_nom != null ? this.sol_nom.equalsIgnoreCase(nom) : null == nom)
                && (this.sol_calcaire != null ? this.sol_calcaire.equalsIgnoreCase(calcaire) : null == calcaire)
                && (this.sol_hydromorphie != null ? this.sol_hydromorphie.equalsIgnoreCase(hydromorphie) : null == hydromorphie)
                && (this.sol_pierrosite != null ? this.sol_pierrosite.equalsIgnoreCase(pierrosite) : null == pierrosite)
                && (this.sol_profondeur != null ? this.sol_profondeur.equalsIgnoreCase(profondeur) : null == profondeur)
                && (this.sol_texture != null ? this.sol_texture.equalsIgnoreCase(texture) : null == texture)
                && (this.sol_region != null ? this.sol_region.equalsIgnoreCase(region) : null == region);
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + (null == id_type_sol ? 0 : id_type_sol.hashCode());
        return hash;
    }

    public String getAgrosystId() {
        return agrosystId;
    }

    public void setAgrosystId(String agrosystId) {
        this.agrosystId = agrosystId;
    }

    public ImportStatus getStatus() {
        return status;
    }

    public void setStatus(ImportStatus status) {
        this.status = status;
    }

    public String getRegion() {
        return this.sol_region;
    }

    public void setRegion(String region) {
        this.sol_region = region;
    }

    public String getCalcaire() {
        return this.sol_calcaire;
    }

    public void setCalcaire(String calcaire) {
       this.sol_calcaire = calcaire;
    }

    public String getNom_arvalis() {
        return this.sol_nom;
    }

    public void setNom_arvalis(String nom_arvalis) {
        this.sol_nom = nom_arvalis;
    }

    public String getPierrosite() {
        return this.sol_pierrosite;
    }

    public void setPierrosite(String pierrosite) {
        this.sol_pierrosite = pierrosite;
    }

    public String getTexture() {
        return this.sol_texture;
    }

    public void setTexture(String texture) {
        this.sol_texture = texture;
    }

    public String getProfondeur() {
        return this.sol_profondeur;
    }

    public void setProfondeur(String profondeur) {
        this.sol_profondeur = profondeur;
    }

    public String getNom_referenciel_pedologique_francais() {
        return nom_referenciel_pedologique_francais;
    }

    public void setNom_referenciel_pedologique_francais(String nom_referenciel_pedologique_francais) {
        this.nom_referenciel_pedologique_francais = nom_referenciel_pedologique_francais;
    }

    public String getHydromorphie() {
        return this.sol_hydromorphie;
    }

    public void setHydromorphie(String hydromorphie) {
        this.sol_hydromorphie = hydromorphie;
    }

    public String getNom_sol() {
        return this.sol_nom;
    }

    public void setNom_sol(String nom_sol) {
        this.sol_nom = nom_sol;
    }

    @Override
    public void setSol_nom_Translated(String libelle) {
        sol_nom_Translated = libelle;
    }

    @Override
    public void setSol_texture_Translated(String libelle) {
        sol_texture_Translated = libelle;
    }

    @Override
    public void setSol_calcaire_Translated(String libelle) {
        sol_calcaire_Translated = libelle;
    }

    @Override
    public void setSol_profondeur_Translated(String libelle) {
        sol_profondeur_Translated = libelle;
    }

    @Override
    public void setSol_hydromorphie_Translated(String libelle) {
        sol_hydromorphie_Translated = libelle;
    }

    @Override
    public void setSol_pierrosite_Translated(String libelle) {
        sol_pierrosite_Translated = libelle;
    }

    @Override
    public String getSol_nom_Translated() {
        return StringUtils.firstNonBlank(sol_nom_Translated, sol_nom);
    }

    @Override
    public String getSol_texture_Translated() {
        return StringUtils.firstNonBlank(sol_texture_Translated, sol_texture);
    }

    @Override
    public String getSol_calcaire_Translated() {
        return StringUtils.firstNonBlank(sol_calcaire_Translated, sol_calcaire);
    }

    @Override
    public String getSol_profondeur_Translated() {
        return StringUtils.firstNonBlank(sol_profondeur_Translated, sol_profondeur);
    }

    @Override
    public String getSol_hydromorphie_Translated() {
        return StringUtils.firstNonBlank(sol_hydromorphie_Translated, sol_hydromorphie);
    }

    @Override
    public String getSol_pierrosite_Translated() {
        return StringUtils.firstNonBlank(sol_pierrosite_Translated, sol_pierrosite);
    }
}
