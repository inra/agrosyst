package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.export.InterventionDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author cosse
 */
@Getter
@Setter
public class PracticedInterventionDto implements InterventionDto {
    
    @Serial
    private static final long serialVersionUID = -3439249030530587922L;
    
    protected String topiaId;
    
    /**
     * Nom de l'attribut en BD : rank
     */
    protected int rank;
    
    /**
     * Nom de l'attribut en BD : name
     */
    protected String name;
    
    /**
     * Nom de l'attribut en BD : comment
     */
    protected String comment;
    
    /**
     * Nom de l'attribut en BD : startingPeriodDate
     */
    protected String startingPeriodDate;
    
    /**
     * Nom de l'attribut en BD : endingPeriodDate
     */
    protected String endingPeriodDate;
    
    /**
     * Nom de l'attribut en BD : intermediateCrop
     */
    protected boolean intermediateCrop;
    
    /**
     * Nom de l'attribut en BD : tillageDepth
     */
    protected Integer tillageDepth;
    
    /**
     * Nom de l'attribut en BD : otherToolSettings
     */
    protected String otherToolSettings;
    
    /**
     * Nom de l'attribut en BD : workRate
     */
    protected Double workRate;
    
    /**
     * Nom de l'attribut en BD : progressionSpeed
     */
    protected Integer progressionSpeed;
    
    /**
     * Nom de l'attribut en BD : involvedPeopleNumber
     */
    protected Double involvedPeopleNumber;
    
    /**
     * Nom de l'attribut en BD : toolsCouplingCodes
     */
    protected Collection<String> toolsCouplingCodes;
    
    /**
     * Nom de l'attribut en BD : spatialFrequency
     */
    protected double spatialFrequency;
    
    /**
     * Nom de l'attribut en BD : temporalFrequency
     */
    protected double temporalFrequency;
    
    /**
     * Nom de l'attribut en BD : transitVolume
     */
    protected Double transitVolume;
    
    /**
     * Nom de l'attribut en BD : nbBalls
     */
    protected Integer nbBalls;
    
    /**
     * Nom de l'attribut en BD : speciesStades
     */
    protected Collection<PracticedSpeciesStade> speciesStades;
    
    protected List<SpeciesStadeDto> speciesStadesDtos;
    
    /**
     * Nom de l'attribut en BD : practicedCropCyclePhase
     */
    protected PracticedCropCyclePhase practicedCropCyclePhase;
    
    /**
     * Nom de l'attribut en BD : practicedCropCycleConnection
     */
    protected PracticedCropCycleConnection practicedCropCycleConnection;
    
    /**
     * Nom de l'attribut en BD : type
     */
    protected AgrosystInterventionType type;
    
    /**
     * Nom de l'attribut en BD : workRateUnit
     */
    protected MaterielWorkRateUnit workRateUnit;
    
    /**
     * Nom de l'attribut en BD : transitVolumeUnit
     */
    protected MaterielTransportUnit transitVolumeUnit;
    

    // Identifiant du domaine sur lequel porte l'intervention.
    // L'identifiant peut-être soit le TopiaId du domaine soit son code
    // selon qu'il s'agit d'une intervention du réalisé ou synthétisé.
    protected String domainId;

    protected String fromCropCode;
    
    protected Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds;
    
    protected Collection<AbstractActionDto> actionDtos;

    public PracticedInterventionDto() {
        this.topiaId = PracticedSystemService.NEW_INTERVENTION_PREFIX + UUID.randomUUID();
    }

    public void addSpeciesStadeDto(SpeciesStadeDto speciesStadeDto) {
        if (speciesStadesDtos == null) {
            speciesStadesDtos = new ArrayList<>();
        }
        speciesStadesDtos.add(speciesStadeDto);
    }

    public List<SpeciesStadeDto> getSpeciesStadesDtos() {
        if (speciesStadesDtos == null) {
            speciesStadesDtos = new ArrayList<>();
        }
        return speciesStadesDtos;
    }

    public void addToolsCouplingCode(String toolsCouplingCode) {
        if (toolsCouplingCodes == null) {
            toolsCouplingCodes = new ArrayList<>();
        }
        toolsCouplingCodes.add(toolsCouplingCode);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((topiaId == null) ? 0 : topiaId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PracticedInterventionDto other = (PracticedInterventionDto) obj;
        if (topiaId == null) {
            return other.topiaId == null;
        } else return topiaId.equals(other.topiaId);
    }

    @Override
    public double getTemporalFrequencyOrTransitCount() {
        return temporalFrequency;
    }
}
