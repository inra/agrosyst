package fr.inra.agrosyst.api.services.itk;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.practiced.RefStadeEdiDto;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class Itk {
    public static RefStadeEdiDto getDtoForRefStadeEdi(RefStadeEDI refStadeEDI, ReferentialTranslationMap translationMap) {
        RefStadeEdiDto result = null;
        if (refStadeEDI != null) {
            ReferentialEntityTranslation refStadeEdiTranslation = translationMap.getEntityTranslation(refStadeEDI.getTopiaId());
            String col2Translation = refStadeEdiTranslation.getPropertyTranslation(RefStadeEDI.PROPERTY_COLONNE2, refStadeEDI.getColonne2());

            String label = refStadeEDI.getAee   ().substring(6) + " - " + col2Translation;

            result = new RefStadeEdiDto();
            result.setTopiaId(refStadeEDI.getTopiaId());
            result.setLabel(label);
        }
        return result;
    }
}
