package fr.inra.agrosyst.api.services.domain;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.SortOrder;
import fr.inra.agrosyst.api.SortedColumn;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.services.AgrosystFilter;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

/**
 * Filter containing data used to filter domains list
 *
 * @author Eric Chatellier
 */
@Getter
@Setter
public class DomainFilter extends AgrosystFilter {

    @Serial
    private static final long serialVersionUID = 2443705539140359328L;

    protected String domainName;

    protected Boolean active;

    protected DomainType type;
    
    protected String mainContact;
    
    protected String departement;
    
    protected Integer campaign;
    
    protected String responsable;
    
    protected String siret;
    
    protected SortedColumn sortedColumn;
    
    protected SortOrder sortOrder = SortOrder.ASC;
}
