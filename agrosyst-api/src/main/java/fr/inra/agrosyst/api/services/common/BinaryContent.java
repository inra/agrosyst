package fr.inra.agrosyst.api.services.common;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.MediaType;
import lombok.Getter;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Classe qui sert de réceptacle à tout contenu binaire. Chaque instance est composée d'un {@code byte[]} et un
 * {@link MediaType}
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public record BinaryContent(@Getter byte[] bytes, @Getter MediaType type) {

    private static final ImmutableMap<MediaType, String> EXTENSIONS = ImmutableMap.<MediaType, String>builder()
            .put(MediaType.MICROSOFT_EXCEL, "xls")
            .put(MediaType.OOXML_SHEET, "xlsx")
            .put(MediaType.CSV_UTF_8, "csv")
            .put(MediaType.OPENDOCUMENT_SPREADSHEET, "ods")
            .put(MediaType.ZIP, "zip")
            .put(MediaType.PDF, "pdf")
            .build();

    public String getExtension() {
        String extension = EXTENSIONS.get(type());
        Preconditions.checkState(extension != null, "type non supporté: ", type());
        return extension;
    }

    public String computeFileName(String filenameWithoutExtension) {
        String result = String.format("%s.%s", filenameWithoutExtension, getExtension());
        return result;
    }

    public static BinaryContent forMediaType(byte[] content, MediaType mediaType) {
        Preconditions.checkState(EXTENSIONS.containsKey(mediaType), "Le type " + mediaType + " n'est pas déclaré");
        BinaryContent result = new BinaryContent(content, mediaType);
        return result;
    }

    public static BinaryContent forXls(byte[] content) {
        BinaryContent result = forMediaType(content, MediaType.MICROSOFT_EXCEL);
        return result;
    }

    public static BinaryContent forXlsx(byte[] content) {
        BinaryContent result = forMediaType(content, MediaType.OOXML_SHEET);
        return result;
    }

    public static BinaryContent forOds(byte[] content) {
        BinaryContent result = forMediaType(content, MediaType.OPENDOCUMENT_SPREADSHEET);
        return result;
    }

    public static BinaryContent forCsv(byte[] content) {
        BinaryContent result = forMediaType(content, MediaType.CSV_UTF_8);
        return result;
    }

    public static BinaryContent forZip(byte[] content) {
        BinaryContent result = forMediaType(content, MediaType.ZIP);
        return result;
    }

    public static BinaryContent forPdf(byte[] content) {
        BinaryContent result = forMediaType(content, MediaType.PDF);
        return result;
    }

    public InputStream toInputStream() {
        final ByteArrayInputStream result = new ByteArrayInputStream(bytes);
        return result;
    }

}
