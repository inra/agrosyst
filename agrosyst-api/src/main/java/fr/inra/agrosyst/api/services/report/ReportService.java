/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.api.services.report;

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.IftEstimationMethod;
import fr.inra.agrosyst.api.entities.report.PestPressure;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.VitiPestMaster;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.common.ExportResult;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ReportService extends AgrosystService {

    int MILDIOU = 19089;
    int OIDIUM = 19224;
    int VIROSE = 19725;
    int BOTRYTIS = 18536;

    int CITADELLE_FLAVESCENCE = 18721;
    int CITADELLE_GRILLURES = 18722;
    int TORDEUSE_GRAPPE = 999996;

    // Regional
    PaginationResult<ReportRegional> getAllReportRegional(ReportRegionalFilter filter);

    Set<String> getAllReportRegionalIds(ReportRegionalFilter filter);

    ReportRegional getReportRegional(String topiaId);

    List<String> deleteReportRegionals(List<String> reportRegionalIds);

    LinkedHashMap<Integer,String> getRelatedReportRegionals(String code);

    ReportRegional createOrUpdateReportRegional(ReportRegional reportRegional, List<String> reportRegionalNetworkIds, List<PestPressure> diseasePressures, List<PestPressure> pestPressures);
    
    PaginationResult<ReportGrowingSystemDto> getAllReportGrowingSystem(ReportGrowingSystemFilter filter);

    Set<String> getAllReportGrowingSystemIds(ReportGrowingSystemFilter filter);

    LinkedHashMap<Integer, String> getRelatedReportGrowingSystems(String reportRegionalCode);

    ReportGrowingSystem getReportGrowingSystem(String reportGrowingSystemId);

    void deleteReportGrowingSystems(List<String> reportGrowingSystemIds);

    ExportResult exportPdfReportGrowingSystems(ReportExportOption options);

    void exportPdfReportGrowingSystemsAsync(ReportExportOption options);

    ExportResult exportXlsReportRegionals(Collection<String> reportRegionalIds);

    void exportXlsReportRegionalsAsync(Collection<String> reportRegionalIds);

    ExportResult exportXlsReportGrowingSystems(Collection<String> reportGrowingSystemIds);

    void exportXlsReportGrowingSystemsAsync(Collection<String> reportGrowingSystemIds);

    ReportGrowingSystem createOrUpdateReportGrowingSystem(ReportGrowingSystem reportGrowingSystem, String growingSystemId,
                                                          String reportRegionalId, ReportGrowingSystemCollections highlights,
                                                          boolean createManagementMode);

    Map<String, String> getDomainsNameToId(ReportFilter rfilter);

    List<GrowingSystemForReport> getGrowingSystemNameToId(ReportFilter rfilter);

    Map<String, String> getReportRegionalNameToId(ReportFilter rfilter);

    /**
     *
     * @param growingSystemId null safe
     * @return list of cropIds for growing system Id
     */
    List<String> getCropIdsForGrowingSystemId(String growingSystemId);

    List<CropPestMaster> loadCropAdventiceMasters(String reportGrowingSystemId, Sector sector);

    List<CropPestMaster> loadCropDiseaseMasters(String reportGrowingSystemId, Sector sector);

    List<CropPestMaster> loadCropPestMasters(String reportGrowingSystemId, Sector sector);

    List<VerseMaster> loadVerseMasters(String reportGrowingSystemId, Sector sector);

    List<FoodMaster> loadFoodMasters(String reportGrowingSystemId, Sector sector);

    List<YieldLoss> loadYieldLosses(String reportGrowingSystemId, Sector sector);

    List<ArboCropAdventiceMaster> loadArboCropAdventiceMasters(String reportGrowingSystemId);

    List<ArboCropPestMaster> loadArboCropDiseaseMasters(String reportGrowingSystemId);

    List<ArboCropPestMaster> loadArboCropPestMasters(String reportGrowingSystemId);

    List<YieldLoss> loadArboYieldLosses(String reportGrowingSystemId);

    List<VitiPestMaster> loadVitiDiseaseMasters(String reportGrowingSystemId);

    List<VitiPestMaster> loadVitiPestMasters(String reportGrowingSystemId);

    /**
     * Retourne les sections des modeles decisionnel associés au bilans de campagne.
     * @param reportGrowingSystemIds ids
     */
    Set<SectionType> getReportGrowingSystemManagementModeSections(List<String> reportGrowingSystemIds);

    /**
     * Retourne les sections des bilans de campagne.
     * @param reportGrowingSystemIds ids
     */
    Map<Sector, Collection<ReportGrowingSystemSection>> getReportGrowingSystemSections(List<String> reportGrowingSystemIds);

    Set<String> getNetworkIdsUsed(ReportRegional reportRegional);

    String getDefaultArboAdventiceIdForReport();

    /**
     * Pour un bilan de campagne donné, indique les {@link IftEstimationMethod} permises.
     */
    Set<IftEstimationMethod> getIftEstimationMethods(ReportGrowingSystem reportGrowingSystem);

    boolean isFilteredIftEstimations(TypeDEPHY growingSystemDephyType, LocalDateTime reportGrowingSystemCreationDate);

    record GrowingSystemForReport(String topiaId, String label, TypeDEPHY typeDEPHY,
                                      Set<IftEstimationMethod> iftEstimationMethods) {}
}
