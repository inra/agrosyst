package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.measure.Measurement;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycle;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaEntities;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaPersistenceContext;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class Entities {

    public static final Function<TopiaEntity, String> GET_TOPIA_ID = TopiaEntities.getTopiaIdFunction();

    public static final Function<String, String> ESCAPE_TOPIA_ID = input -> {
        int partsSeparatorIndex = input.indexOf('_');
        Preconditions.checkState(partsSeparatorIndex != -1, "The given id is not a TopiaId: " + input);
        String result = input.substring(0, partsSeparatorIndex).replaceAll("[.]", "-");
        result += input.substring(partsSeparatorIndex);
        return result;
    };

    public static final Function<String, String> UNESCAPE_TOPIA_ID = input -> {
        int partsSeparatorIndex = input.lastIndexOf('_');
        String result;
        if (partsSeparatorIndex == -1 || StringUtils.startsWith(input, "new-node-")) {
            result = input;
        } else {
            result = input.substring(0, partsSeparatorIndex).replaceAll("[-]", ".");
            result += input.substring(partsSeparatorIndex);
        }
        return result;
    };

    protected static final Set<Class<? extends TopiaEntity>> IGNORED_ABSTRACT_CLASSES = ImmutableSet.of(
            AbstractDomainInputStockUnit.class,
            AbstractPhytoProductInputUsage.class,
            BasicPlot.class,
            AbstractAction.class,
            Measurement.class,
            PracticedCropCycle.class,
            RefMateriel.class,
            RefBioAgressor.class,
            RefVariete.class
);

    protected static final Predicate<AgrosystEntityEnum> IS_ENTITY_AN_ABSTRACT_ENTITY = input -> !IGNORED_ABSTRACT_CLASSES.contains(input.getContract());

    public static Map<Class<? extends TopiaEntity>, Class<? extends TopiaEntity>> getAllAgrosystTypesAndImplementation() {
        Map<Class<? extends TopiaEntity>, Class<? extends TopiaEntity>> result = new HashMap<>();
        List<AgrosystEntityEnum> contracts = Arrays.asList(AgrosystEntityEnum.getContracts());
        Iterable<AgrosystEntityEnum> filtered = Iterables.filter(contracts, IS_ENTITY_AN_ABSTRACT_ENTITY::test);
        for (AgrosystEntityEnum entityEnum : filtered) {
            Class<? extends TopiaEntity> contract = entityEnum.getContract();
            Class<? extends TopiaEntity> implementation = entityEnum.getImplementation();
            result.put(contract, implementation);
        }
        return result;
    }

    public static <E extends TopiaEntity> String getShortId(E entity, TopiaPersistenceContext persistenceContext) {
        return persistenceContext.getTopiaIdFactory().getRandomPart(entity.getTopiaId());
    }

}
