package fr.inra.agrosyst.api.services.practiced;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.Status;

/**
 * SUCCES by default
 */
public class OperationStatus {

    protected Status status = Status.SUCCES;
    
    protected String comment;
    
    public void failed(String comment) {
        status = Status.FAILED;
        this.comment = comment;
    }
    
    public void success(String comment) {
        status = Status.SUCCES;
        this.comment = comment;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public String getComment() {
        return comment;
    }
}
