package fr.inra.agrosyst.api.services.common;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

import static java.util.Optional.ofNullable;


/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public interface PricesService extends AgrosystService {
    
    Log LOGGER = LogFactory.getLog(PricesService.class);

    PriceUnit DEFAULT_PRICE_UNIT = PriceUnit.EURO_HA;

    PriceUnit WORKFORCE_PRICE_UNIT = PriceUnit.EURO_H;
    List<PriceUnit> WEIGTH_PRICE_UNITS =
            List.of(PriceUnit.EURO_T, PriceUnit.EURO_Q, PriceUnit.EURO_KG, PriceUnit.EURO_G, PriceUnit.EURO_MG);

    List<PriceUnit> WINE_PRICE_UNITS =
            List.of(PriceUnit.EURO_HL_WINE, PriceUnit.EURO_KG_GRAPE);

    List<PriceUnit> VOL_PRICE_UNITS =
            List.of(PriceUnit.EURO_L, PriceUnit.EURO_HL, PriceUnit.EURO_M3);

    List<PriceUnit> SEED_PRICE_UNITS =
            List.of(PriceUnit.EURO_UNITE, PriceUnit.EURO_PLANT);

    List<PriceUnit> DEFAULT_PRICE_UNITS =
            List.of(PriceUnit.EURO_HA, PriceUnit.EURO_UNITE, PriceUnit.EURO_L, PriceUnit.EURO_KG, PriceUnit.EURO_M2, PriceUnit.EURO_T);
    
    BiFunction<CroppingPlanSpecies, RefDestination, String> GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID = (cps, destination) -> {
        
        List<String> objectIdElements =  new ArrayList<>();
        objectIdElements.add(cps.getSpecies().getCode_espece_botanique());
        objectIdElements.add(cps.getSpecies().getCode_qualifiant_AEE());
        objectIdElements.add(cps.getSpecies().getCode_type_saisonnier_AEE());
        objectIdElements.add(cps.getSpecies().getCode_destination_AEE());
        String varietyLabel = "";
        if (cps.getVariety() != null) {
            varietyLabel = cps.getVariety().getLabel();
        }
        objectIdElements.add(varietyLabel);
        objectIdElements.add(destination.getCode_destination_A());
        
        final String result = new Gson().toJson(objectIdElements);
        
        return result;
    };
    
    Function<CroppingPlanSpecies, String> GET_HARVESTING_PRICE_DISPLAY_NAME = (cps) -> {
        final RefEspece refEspece = cps.getSpecies();
        List<String> elements = new ArrayList<>();
        elements.add(refEspece.getLibelle_espece_botanique_Translated());
        if (StringUtils.isNotBlank(refEspece.getLibelle_qualifiant_AEE())) {
            elements.add(refEspece.getLibelle_qualifiant_AEE_Translated());
        }
        if (StringUtils.isNotBlank(refEspece.getLibelle_type_saisonnier_AEE())) {
            elements.add(refEspece.getLibelle_type_saisonnier_AEE_Translated());
        }
        final RefVariete variety = cps.getVariety();
        if (variety != null) {
            elements.add("(" + variety.getLabel() + ")");
        }
        return String.join(" ", elements);
    };

    static HarvestingPriceDto bindHarvestingPriceToHarvestingPriceDto(
            Domain domain,
            PracticedSystem practicedSystem,
            Zone zone,
            HarvestingPrice harvestingPrice,
            String actionId) {

        String growingSystemName = null;
        String zoneId = null;
        String zoneName = null;
        String campaign = null;
        if (zone != null) {
            zoneId = zone.getTopiaId();
            zoneName = zone.getName();
            if (zone.getPlot() != null && zone.getPlot().getGrowingSystem() != null) {
                zoneName = zoneName + " (" + zone.getPlot().getName() + ")";
                growingSystemName = zone.getPlot().getGrowingSystem().getName();
            }
        }
        String practicedSystemId = null;
        String practicedSystemName = null;
        if (practicedSystem != null) {
            practicedSystemId = practicedSystem.getTopiaId();
            practicedSystemName = practicedSystem.getName();
            growingSystemName = practicedSystem.getGrowingSystem() != null ? practicedSystem.getGrowingSystem().getName() : null;
            campaign = practicedSystem.getCampaigns();
        }
        final String domainId = domain.getTopiaId();
        HarvestingActionValorisation valorisation = harvestingPrice.getHarvestingActionValorisation();
        HarvestingPriceDto priceDto = HarvestingPriceDto.builder()
                .topiaId(harvestingPrice.getTopiaId())
                .practicedSystemId(practicedSystemId)
                .zoneId(zoneId)
                .domainId(domainId)
                .actionId(actionId)
                .valorisationId(valorisation.getTopiaId())
                .objectId(harvestingPrice.getObjectId())
                .speciesCode(valorisation.getSpeciesCode())
                .displayName(harvestingPrice.getDisplayName())
                .sourceUnit(harvestingPrice.getSourceUnit())
                .price(harvestingPrice.getPrice())
                .priceUnit(harvestingPrice.getPriceUnit())
                .destinationId(valorisation.getDestination().getTopiaId())
                .destination(valorisation.getDestination().getDestination_Translated())
                .isOrganic(valorisation.isIsOrganicCrop())
                .yealdAverage(valorisation.getYealdAverage())
                .yealdUnit(valorisation.getYealdUnit())
                .category(InputPriceCategory.HARVESTING_ACTION)
                .growingSystemName(growingSystemName)
                .practicedSystemName(practicedSystemName)
                .zoneName(zoneName)
                .campaign(ofNullable(campaign).orElse(Integer.toString(domain.getCampaign())))
                .build();
        return priceDto;
    }
    
    HarvestingPrice newHarvestingPriceInstance();
    
    void persistPrice(HarvestingPrice harvestingPrice);

    HarvestingPrice bindDtoToPrice(
            Domain domain,
            HarvestingActionValorisation valorisationClone, InputPriceDto inputPriceDto,
            HarvestingPrice price,
            String priceObjectId,
            Optional<PracticedSystem> harvestingPracticedSystem,
            Optional<Zone> harvestingZone);

    /**
     * This method is used to retrieve prices for HARVESTING_ACTION_PRICE_CATEGORIE
     */
    LoadedPricesResult getHarvestingPriceResults(
            String domainId);

    List<HarvestingPrice> loadHarvestingPrices(
            String domainTopiaId);

    /**
     * Create or update price from zone or practicedSystem services
     * /!\ this methode does not update the price value, it is done from domain persistence PricesService.updateHarvestingPriceValues()
     */
    void bindZoneOrPracticedHarvestingPrice(
            HarvestingActionValorisationDto valorisationDto,
            HarvestingActionValorisation valorisationEntity,
            HarvestingPrice persistedPrice,
            RefDestination destination,
            CroppingPlanSpecies cps,
            Domain domain,
            PracticedSystem practicedSystem,
            Zone zone, Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            Map<HarvestingActionValorisationDto, List<RefHarvestingPrice>> refHarvestingPricesForValorisationDtos);

    void removeValorisationPrices(Collection<HarvestingActionValorisation> valorisations);

    HarvestingPrice getPriceForValorisation(HarvestingActionValorisation valorisation);
    
    void updateHarvestingPriceValues(List<HarvestingPriceDto> harvestingPriceDtos);

    Map<String, HarvestingValorisationPriceSummary> getEffectiveAverageReferencePricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);

    Map<String, HarvestingValorisationPriceSummary> getPracticedAverageReferencePricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);

    Map<String, HarvestingPrice> getPriceForValorisations(Collection<HarvestingActionValorisation> harvestingActionValorisations);

    void removeHarvestingPrices(Collection<HarvestingPrice> harvestingPrices);

    PriceAndUnit getAverageRefHarvestingPriceForValorisation(HarvestingActionValorisation valorisation,
                                                             Optional<String> optionalPracticedSystemCampaigns,
                                                             Optional<PriceUnit> optionalPriceUnit,
                                                             Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode);
}
