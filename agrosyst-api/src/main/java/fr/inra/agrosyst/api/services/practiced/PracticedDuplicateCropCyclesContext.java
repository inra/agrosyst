package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Context de duplication pour l'ensemble des objets de la graphe "systeme synthétisé".
 *
 * @author Eric Chatellier
 */
@Getter
@Setter
public class PracticedDuplicateCropCyclesContext implements Serializable {

    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = 6608039291236218854L;

    protected boolean withInputUsage = false;

    /**
     * for error messages
     **/
    protected String toDomainName;
    protected String toDomainCode;

    protected boolean isSameGrowingSystemCode;

    /**
     * for practiced System
     */
    protected PracticedSystem practicedSystem;
    protected PracticedSystem practicedSystemClone;
    protected GrowingSystem toGrowingSystem;

    /**
     * for effective
     */
    protected String targetedCampaign;

    protected List<String> targetedSystemCropCodes;

    /**
     * common to PracticedSystems and Effective crop cycle
     */
    protected Map<String, Pair<CroppingPlanEntry, Map<String, CroppingPlanSpecies>>> toSpeciesByCropCode;
    protected Map<String, Map<String, String>> fromSpeciesCodeToSpeciesCodeForCropIdentifier;//crop topiaId for effective crop code for practiced
    protected Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByCodes;

    protected Collection<String> toolsCouplingsCode;

    protected String targetedCropCode;
    protected Boolean intermediateStatusChange;

    public void setTargetedCampaign(Integer targetedCampaign) {
        this.targetedCampaign = String.valueOf(targetedCampaign);
    }

    public void setIsSameDomainCode(boolean isSameGrowingSystemCode) {
        this.isSameGrowingSystemCode = isSameGrowingSystemCode;
    }
}
