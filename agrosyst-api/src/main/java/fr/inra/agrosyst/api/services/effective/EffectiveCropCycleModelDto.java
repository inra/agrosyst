package fr.inra.agrosyst.api.services.effective;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingEntryType;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

/**
 * Un model représente un model de culture pouvant servir à:
 * - creer un noeud dans un cycle pluriannuel de culture assolées
 * - choisir une culture dans un cycle pluriannuel de culture pérennes.
 * 
 * Cela permet de définir une cuture à un rang donné.
 * 
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Getter
@Setter
public class EffectiveCropCycleModelDto implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 5126472555940965611L;

    protected String croppingPlanEntryId;
    protected String label;
    protected boolean intermediate;
    protected boolean mixSpecies;
    protected boolean mixVariety;
    protected boolean mixCompanion;
    protected boolean catchCrop;

    protected CroppingEntryType type;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((croppingPlanEntryId == null) ? 0 : croppingPlanEntryId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EffectiveCropCycleModelDto other = (EffectiveCropCycleModelDto) obj;
        if (croppingPlanEntryId == null) {
            return other.croppingPlanEntryId == null;
        } else return croppingPlanEntryId.equals(other.croppingPlanEntryId);
    }
}
