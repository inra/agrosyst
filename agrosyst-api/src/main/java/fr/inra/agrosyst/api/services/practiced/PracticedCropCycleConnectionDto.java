package fr.inra.agrosyst.api.services.practiced;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Connexion entre deux cutures a deux rang differents pouvant comporter une culture intermédiaire.
 * 
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class PracticedCropCycleConnectionDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1767965109676867979L;

    protected String sourceId;
    protected String targetId;
    protected String intermediateCroppingPlanEntryCode;
    protected String intermediateCropName;
    protected String label;
    protected Double croppingPlanEntryFrequency;
    protected boolean notUsedForThisCampaign;

    protected List<PracticedInterventionDto> interventions;

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getTargetId() {
        return targetId;
    }
    
    public String getIntermediateCroppingPlanEntryCode() {
        return intermediateCroppingPlanEntryCode;
    }

    public void setIntermediateCroppingPlanEntryCode(String intermediateCroppingPlanEntryCode) {
        this.intermediateCroppingPlanEntryCode = intermediateCroppingPlanEntryCode;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getCroppingPlanEntryFrequency() {
        return croppingPlanEntryFrequency;
    }

    public void setCroppingPlanEntryFrequency(Double croppingPlanEntryFrequency) {
        this.croppingPlanEntryFrequency = croppingPlanEntryFrequency;
    }
    
    public List<PracticedInterventionDto> getInterventions() {
        return interventions;
    }

    public void setInterventions(List<PracticedInterventionDto> interventions) {
        this.interventions = interventions;
    }

    public String getIntermediateCropName() {
        return intermediateCropName;
    }

    public void setIntermediateCropName(String intermediateCropName) {
        this.intermediateCropName = intermediateCropName;
    }

    public boolean isNotUsedForThisCampaign() {
        return notUsedForThisCampaign;
    }

    public void setNotUsedForThisCampaign(boolean notUsedForThisCampaign) {
        this.notUsedForThisCampaign = notUsedForThisCampaign;
    }

    public void addIntervention(PracticedInterventionDto interventionDto) {
        if (interventions == null) {
            interventions = new ArrayList<>();
        }
        interventions.add(interventionDto);
    }
}
