package fr.inra.agrosyst.api.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.io.Serial;
import java.util.Objects;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@SuperBuilder(toBuilder = true)
public class DomainOrganicProductInputDto extends DomainInputDto {
    @Serial
    private static final long serialVersionUID = -7433612029500355292L;
    
    // tradeName: (libelle) Type de produit fertilisant
    
    // only variable id par
    @NonNull
    String refInputId;
    
    @NonNull
    OrganicProductUnit usageUnit;
    
    double n;
    
    double p2O5;
    
    double k2O;
    
    Double caO;

    Double mgO;

    Double s;

    boolean organic;
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        DomainOrganicProductInputDto that = (DomainOrganicProductInputDto) o;
        return Double.compare(that.n, n) == 0 && Double.compare(that.p2O5, p2O5) == 0 && Double.compare(that.k2O, k2O) == 0 && organic == that.organic &&
                refInputId.equals(that.refInputId) && usageUnit == that.usageUnit && Objects.equals(caO, that.caO) && Objects.equals(mgO, that.mgO) &&
                Objects.equals(s, that.s) && key.equals(that.key);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(refInputId, usageUnit, n, p2O5, k2O, caO, mgO, s, organic, key);
    }
}
