package fr.inra.agrosyst.api.services.measurement;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Protocol filter.
 * 
 * @author Eric Chatellier
 */
public class ProtocoleVgObsFilter {

    /** Protocole VgObs (colonne B de l’onglet « protocole VgObs »). */
    protected String label;

    /** Nuisible (si protocole VgObs identifié, colonne P de l’onglet « protocole VgObs ». */
    protected String pest;

    /** Stade organisme observé (si protocole VgObs identifié, colonne Q de l’onglet « protocole VgObs ». */
    protected String stade;

    /** Support organe observé (si protocole VgObs identifié, colonne Y de l’onglet « protocole VgObs ». */
    protected String support;

    /** Type de notation (si protocole VgObs identifié, colonne AE de l’onglet « protocole VgObs ». */
    protected String observation;

    /** Valeur qualitative (si protocole VgObs identifié, colonne AS de l’onglet « protocole VgObs ». */
    protected String qualitative;

    /** colonne AJ de l’onglet « protocole VgObs ». */
    protected String unit;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPest() {
        return pest;
    }

    public void setPest(String pest) {
        this.pest = pest;
    }

    public String getStade() {
        return stade;
    }

    public void setStade(String stade) {
        this.stade = stade;
    }

    public String getSupport() {
        return support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getQualitative() {
        return qualitative;
    }

    public void setQualitative(String qualitative) {
        this.qualitative = qualitative;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
