package fr.inra.agrosyst.api.services.performance;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Cosse David cosse@codelutin.com
 */
@Getter
@Setter
public class PerformancePracticedInterventionExecutionContext extends PerformanceInterventionContext {

    @Getter
    protected final PracticedCropCycleConnection practicedCropCycleConnection;

    protected final PracticedIntervention intervention;

    protected final boolean isFictive; // there are no intervention but a not used crop.

    // for practiced system we do nor use the croppingPlanSpecies declared on crop as it can be more over the practiced system campaigns
    protected final CropWithSpecies cropWithSpecies;

    protected Map<TraitementProduitWithSpecies, ReferenceDoseDTO> legacyRefDosageForPhytoInputs = new HashMap<>();// used for ITF

    protected List<String> targetedIftReferencesDosagesUserInfos = new ArrayList<>();

    protected Map<Pair<RefDestination, YealdUnit>, Double> yealdAveragesByDestinations;

    private PerformancePracticedInterventionExecutionContext(
            PracticedIntervention practicedIntervention,
            PracticedCropCycleConnection practicedCropCycleConnection,
            Set<Integer> practicedCampaigns,
            ToolsCoupling toolsCoupling,
            CropWithSpecies cropWithSpecies,
            List<AbstractAction> actions,
            PriceConverterKeysToRate priceConverterKeysToRate,
            List<HarvestingPrice> harvestingPrices, MultiValuedMap<String,
            RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode) {
        super(
                practicedIntervention != null ? practicedIntervention.getTopiaId() : DUMMY,
                practicedIntervention != null ? practicedIntervention.getName() : DUMMY,
                practicedCampaigns,
                toolsCoupling,
                actions,
                priceConverterKeysToRate,
                harvestingPrices,
                allSubstancesActivesCommissionEuropeenneByAmmCode);

        this.intervention = practicedIntervention;
        this.isFictive = practicedIntervention == null;
        this.practicedCropCycleConnection = practicedCropCycleConnection;
        this.cropWithSpecies = cropWithSpecies;
    }

    public static PerformancePracticedInterventionExecutionContext createPerformancePracticedInterventionExecutionContext(
            PracticedIntervention practicedIntervention,
            Set<Integer> practicedCampaigns,
            ToolsCoupling toolsCoupling,
            CropWithSpecies cropWithSpecies,
            List<AbstractAction> actions,
            PriceConverterKeysToRate priceConverterKeysToRate,
            List<HarvestingPrice> harvestingPrices,
            MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode) {

        return new PerformancePracticedInterventionExecutionContext(
                practicedIntervention,
                practicedIntervention.getPracticedCropCycleConnection(),
                practicedCampaigns,
                toolsCoupling,
                cropWithSpecies,
                actions,
                priceConverterKeysToRate,
                harvestingPrices,
                allSubstancesActivesCommissionEuropeenneByAmmCode);
    }

    public static PerformancePracticedInterventionExecutionContext createNotUsedCropPerformancePracticedInterventionExecutionContext(
            PracticedCropCycleConnection connectionWithNotUsedCrop,
            Set<Integer> practicedCampaigns,
            CropWithSpecies cropWithSpecies) {

        return new PerformancePracticedInterventionExecutionContext(
                null,
                connectionWithNotUsedCrop,
                practicedCampaigns,
                null,
                cropWithSpecies,
                null,
                null,
                null,
                null);
    }

    public void addProductWithoutAutoconsume(Double value) {
        realProductWithoutAutoconsume += value;
    }

    public void addProductWithAutoconsume(Double value) {
        realProductWithAutoconsume += value;
    }

    public void addStandardisedProductWithoutAutoconsume(double value) {
        standardisedProductWithoutAutoconsume += value;
    }

    public void addStandardisedProductWithAutoconsume(Double value) {
        standardisedProductWithAutoconsume += value;
    }

    public void addExpenses(Double value) {
        operatingExpenses += value;
    }

    public void addStandardisedExpenses(Double value) {
        standardisedOperatingExpenses += value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PerformancePracticedInterventionExecutionContext that = (PerformancePracticedInterventionExecutionContext) o;
        return intervention.equals(that.intervention);
    }

    @Override
    public int hashCode() {
        return Objects.hash(intervention);
    }


}
