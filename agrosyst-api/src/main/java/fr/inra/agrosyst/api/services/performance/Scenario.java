package fr.inra.agrosyst.api.services.performance;

/*
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

/**
 * Performance scenario
 *
 * @author David Cossé
 */
public record Scenario(String code, String label) implements Serializable {

    public static final Comparator<Scenario> LABEL_SCENARIO_COMPARATOR = (o1, o2) -> {
        String label1 = Strings.nullToEmpty(o1.label());
        String label2 = Strings.nullToEmpty(o2.label());
        int result = label1.toLowerCase().compareTo(label2.toLowerCase());
        if (0 == result) {
            result = o1.code().compareTo(o2.code());
        }
        return result;
    };

    @Serial
    private static final long serialVersionUID = -7870749557696556490L;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Scenario scenario = (Scenario) o;
        return Objects.equals(code, scenario.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
