Il s'agit du module qui contient le modèle de données pour Agrosyst. Il contient
le modèle des entités, de quoi récupérer le rootContext, les données des configurations
les templates pour les emails et les modèles pour ces emails.
