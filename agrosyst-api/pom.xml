<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  Agrosyst :: API
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2024 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>fr.inra</groupId>
    <artifactId>agrosyst</artifactId>
    <version>3.6-SNAPSHOT</version>
  </parent>

    <groupId>fr.inra.agrosyst</groupId>
    <artifactId>agrosyst-api</artifactId>
    <packaging>jar</packaging>

    <name>Agrosyst :: API</name>
    <description>Modèle de données, entités, API des services</description>

    <properties>
        <license.licenseResolver>file://${project.parent.basedir}/src/license</license.licenseResolver>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.nuiton.topia</groupId>
            <artifactId>topia-persistence</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate.orm</groupId>
            <artifactId>hibernate-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nuiton</groupId>
            <artifactId>nuiton-utils</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.errorprone</groupId>
            <artifactId>error_prone_annotations</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
        <dependency>
            <groupId>org.immutables</groupId>
            <artifactId>value</artifactId>
        </dependency>
        <dependency>
            <groupId>org.nuiton.i18n</groupId>
            <artifactId>nuiton-i18n</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sun.istack</groupId>
            <artifactId>istack-commons-runtime</artifactId>
        </dependency>
        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>24.1.0</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <scope>runtime</scope>
        </dependency>

    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>${project.basedir}/src/main/resources</directory>
            </resource>
            <resource>
                <directory>${project.build.directory}/generated-sources/models</directory>
                <includes>
                    <include>*.objectmodel</include>
                    <include>*.properties</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.nuiton.eugene</groupId>
                <artifactId>eugene-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generate-entities</id>
                        <phase>generate-sources</phase>
                        <configuration>
                            <inputs>zargo</inputs>
                            <!-- Corresponding to extracted package from zargo file -->
                            <fullPackagePath>fr.inra.agrosyst.api.entities</fullPackagePath>
                            <defaultPackage>fr.inra.agrosyst.api.entities</defaultPackage>
                            <templates>
                                org.nuiton.topia.templates.EntityTransformer,
                                org.nuiton.topia.templates.EntityHibernateMappingGenerator,
                                org.nuiton.eugene.java.JavaEnumerationTransformer,
                                org.nuiton.topia.templates.EntityEnumTransformer
                            </templates>
                        </configuration>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
