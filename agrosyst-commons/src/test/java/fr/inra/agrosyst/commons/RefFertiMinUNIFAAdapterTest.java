package fr.inra.agrosyst.commons;

/*-
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAImpl;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RefFertiMinUNIFAAdapterTest {

    @Test
    public void testSerializeProductImpl() {

        RefFertiMinUNIFAImpl mineralProduct = new RefFertiMinUNIFAImpl();
        mineralProduct.setCateg(101);
        mineralProduct.setForme("Granulé");
        mineralProduct.setN(2.1);


        AgrosystGsonSupplier supplier = new AgrosystGsonSupplier();
        Gson gson = supplier.get();

        String json = gson.toJson(mineralProduct);
        Assertions.assertTrue(json.contains("\"forme\":\"Granulé\""));
        Assertions.assertTrue(json.contains("\"n\":2.1"));
        Assertions.assertFalse(json.contains("\"p2O5\":0.0"));

    }

    @Test
    public void testSerializeProduct() {

        RefFertiMinUNIFA mineralProduct = new RefFertiMinUNIFAImpl();
        mineralProduct.setCateg(101);
        mineralProduct.setForme("Granulé");
        mineralProduct.setN(2.1);


        AgrosystGsonSupplier supplier = new AgrosystGsonSupplier();
        Gson gson = supplier.get();

        String json = gson.toJson(mineralProduct);
        Assertions.assertTrue(json.contains("\"forme\":\"Granulé\""));
        Assertions.assertTrue(json.contains("\"n\":2.1"));
        Assertions.assertFalse(json.contains("\"p2O5\":0.0"));

    }
}
