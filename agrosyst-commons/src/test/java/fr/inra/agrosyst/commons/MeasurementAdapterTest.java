package fr.inra.agrosyst.commons;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.inra.agrosyst.api.entities.measure.Measure;
import fr.inra.agrosyst.api.entities.measure.MeasureImpl;
import fr.inra.agrosyst.api.entities.measure.MeasureType;
import fr.inra.agrosyst.api.entities.measure.Measurement;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementSessionImpl;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.measure.Observation;
import fr.inra.agrosyst.api.entities.measure.ObservationImpl;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Iterator;

/**
 * Unit test class for {@link fr.inra.agrosyst.commons.gson.MeasurementAdapter}.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class MeasurementAdapterTest {

    @Test
    public void testSerializeDeserialize() {
        MeasurementSession measurementSession = new MeasurementSessionImpl();
        measurementSession.setStartDate(LocalDate.now());

        Measure measure1 = new MeasureImpl();
        measure1.setMeasurementType(MeasurementType.PLANTE);
        measure1.setMeasureType(MeasureType.STANDARD_DEVIATION);

        Observation observation1 = new ObservationImpl();
        observation1.setMeasurementType(MeasurementType.STADE_CULTURE);
        observation1.setCropNumberObserved(2);
        observation1.setComment("comment for observation 1");

        measurementSession.addMeasurements(measure1);
        measurementSession.addMeasurements(observation1);

        Gson gson = new AgrosystGsonSupplier().get();

        String measurementSessionAsJsonString = gson.toJson(measurementSession);


        MeasurementSession measurementSessionDeserialized = gson.fromJson(measurementSessionAsJsonString, MeasurementSessionImpl.class);

        Assertions.assertEquals(2, measurementSessionDeserialized.getMeasurements().size());

        Iterator<Measurement> iterator = measurementSessionDeserialized.getMeasurements().iterator();
        Assertions.assertEquals(measure1.getMeasureType(), ((Measure) iterator.next()).getMeasureType());

        Assertions.assertEquals(2, ((Observation) iterator.next()).getCropNumberObserved().intValue());
    }
}
