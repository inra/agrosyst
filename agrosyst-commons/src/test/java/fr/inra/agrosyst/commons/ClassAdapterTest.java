package fr.inra.agrosyst.commons;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleImpl;
import fr.inra.agrosyst.commons.gson.ClassAdapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class ClassAdapterTest {

    @Test
    public void testClassSerialisationWithoutAdapter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        Assertions.assertThrows(UnsupportedOperationException.class, () -> {
            String json = gson.toJson(PracticedPerennialCropCycleImpl.class);
            gson.fromJson(json, Class.class);
        });
    }

    @Test
    public void testClassSerialisationWithAdapter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Class.class, new ClassAdapter());
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(PracticedPerennialCropCycleImpl.class);

        Class<?> aClass = gson.fromJson(json, Class.class);
        Assertions.assertNotNull(aClass);
        Assertions.assertEquals(PracticedPerennialCropCycleImpl.class, aClass);
    }

    protected class SomeTestClass {
        protected Class someClass;

        public SomeTestClass() {
        }

        public SomeTestClass(Class<?> someClass) {
            this.someClass = someClass;
        }

        public Class getSomeClass() {
            return someClass;
        }

        public void setSomeClass(Class someClass) {
            this.someClass = someClass;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SomeTestClass that = (SomeTestClass) o;
    
            return Objects.equals(someClass, that.someClass);
        }

        @Override
        public int hashCode() {
            return someClass != null ? someClass.hashCode() : 0;
        }

    }

    @Test
    public void testClassSerialisationWithinAnObject() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Class.class, new ClassAdapter());
        Gson gson = gsonBuilder.create();

        SomeTestClass src = new SomeTestClass(EffectivePerennialCropCycleImpl.class);
        String json = gson.toJson(src);

        SomeTestClass dest = gson.fromJson(json, SomeTestClass.class);
        Assertions.assertNotNull(dest);
        Assertions.assertEquals(src, dest);
    }

}
