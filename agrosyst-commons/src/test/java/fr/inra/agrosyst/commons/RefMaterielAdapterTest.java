package fr.inra.agrosyst.commons;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.EquipmentImpl;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurImpl;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationImpl;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutilImpl;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTractionImpl;
import fr.inra.agrosyst.commons.gson.RefMaterielAdapter;
import fr.inra.agrosyst.commons.gson.TopiaEntityAdapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class RefMaterielAdapterTest {

    @Test
    public void testRefMaterielSerialisationWithoutAdapter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        String json = gson.toJson(new RefMaterielAutomoteurImpl());

        Assertions.assertThrows(RuntimeException.class, () ->
            gson.fromJson(json, RefMateriel.class)
        );
    }

    @Test
    public void testRefMaterielSerialisationWithAdapter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(RefMateriel.class, new RefMaterielAdapter());
        Gson gson = gsonBuilder.create();

        RefMaterielAutomoteurImpl src = new RefMaterielAutomoteurImpl();
        src.setCodeEDI("azethj");
        String json = gson.toJson(src);

        RefMateriel mat = gson.fromJson(json, RefMateriel.class);
        Assertions.assertNotNull(mat);
        Assertions.assertTrue(mat instanceof RefMaterielAutomoteur);
        RefMaterielAutomoteur materielAutomoteur = (RefMaterielAutomoteur) mat;
        Assertions.assertEquals("azethj", materielAutomoteur.getCodeEDI());
    }

    @Test
    public void testRefMaterielSerialisationWithAdapterAllTypes() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(RefMateriel.class, new RefMaterielAdapter());
        Gson gson = gsonBuilder.create();

        Assertions.assertEquals(RefMaterielAutomoteurImpl.class, gson.fromJson(gson.toJson(new RefMaterielAutomoteurImpl()), RefMateriel.class).getClass());
        Assertions.assertEquals(RefMaterielIrrigationImpl.class, gson.fromJson(gson.toJson(new RefMaterielIrrigationImpl()), RefMateriel.class).getClass());
        Assertions.assertEquals(RefMaterielOutilImpl.class, gson.fromJson(gson.toJson(new RefMaterielOutilImpl()), RefMateriel.class).getClass());
        Assertions.assertEquals(RefMaterielTractionImpl.class, gson.fromJson(gson.toJson(new RefMaterielTractionImpl()), RefMateriel.class).getClass());
    }

    /**
     * Cas d'un equipement qui n'a pas de materiel (autre).
     */
    @Test
    public void testEquipementAutre() {
        Equipment equipment = new EquipmentImpl();
        equipment.setTopiaId(UUID.randomUUID().toString());
        equipment.setName("Test");
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Equipment.class, new TopiaEntityAdapter(EquipmentImpl.class));
        gsonBuilder.registerTypeAdapter(RefMateriel.class, new RefMaterielAdapter());
        Gson gson = gsonBuilder.create();
        
        String json = gson.toJson(equipment);
        Equipment equipment1 = gson.fromJson(json, Equipment.class);
        
        Assertions.assertNotNull(equipment1);
        Assertions.assertEquals(equipment,equipment1);
        
    }
}
