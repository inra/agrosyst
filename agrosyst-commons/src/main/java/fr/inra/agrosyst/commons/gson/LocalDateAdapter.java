/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.commons.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalQuery;

public class LocalDateAdapter extends AbstractJavaTimeAdapter<LocalDate> {

    @Override
    public JsonElement serialize(LocalDate src, Type typeOfSrc, JsonSerializationContext context) {
        String dateFormatAsString = ISO_8601_FORMAT.format(LocalDateTime.of(src, LocalTime.MIDNIGHT));
        return new JsonPrimitive(dateFormatAsString);
    }

    public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        if (!(json instanceof JsonPrimitive)) {
            throw new JsonParseException("The date should be a string value");
        }
        LocalDate date = deserializeToDate(json, LocalDate::from);
        if (typeOfT == LocalDate.class) {
            return date;
        } else {
            throw new IllegalArgumentException(getClass() + " cannot deserialize to " + typeOfT);
        }
    }

    @Override
    protected LocalDate deserializeToDate(JsonElement json, TemporalQuery<LocalDate> query) {
        String stringDate = json.getAsString();
        if (stringDate.matches(ISO_8601_REGEX)) {
            try {
                // XXX: pour moi c'est un hack
                // on recoit de javascript une date en UTC : 2018-05-30T22:00:00.000Z alors
                // que l'utilsateur a saisit 31/04/2018
                // mais le code correct en bas convertit donc la date en 30/05/2018 au lieu de 31/05/2018
                // on ne devrait pas avoir de "zone" ici
                Instant instant = Instant.parse(stringDate); // parses UTC
                LocalDateTime ofInstant = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                return ofInstant.toLocalDate();

                // code qui devrait être correct
                //return LocalDateTime.parse(json.getAsString(), ISO_8601_FORMAT).toLocalDate();
            } catch (DateTimeParseException e) {
                throw new JsonSyntaxException(json.getAsString(), e);
            }
        } else {
            return null;
        }

    }
}
