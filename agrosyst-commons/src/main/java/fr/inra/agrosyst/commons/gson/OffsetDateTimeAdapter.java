/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.commons.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalQuery;

public class OffsetDateTimeAdapter extends AbstractJavaTimeAdapter<OffsetDateTime> {

    public JsonElement serialize(OffsetDateTime src, Type typeOfSrc, JsonSerializationContext context) {
        String dateFormatAsString = DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(src);
        return new JsonPrimitive(dateFormatAsString);
    }

    @Override
    protected OffsetDateTime deserializeToDate(JsonElement json, TemporalQuery<OffsetDateTime> query) {
        String stringDate = json.getAsString();
        if (stringDate.matches(ISO_8601_REGEX)) {
            try {
                return DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(json.getAsString(), query);
            } catch (DateTimeParseException e) {
                throw new JsonSyntaxException(json.getAsString(), e);
            }
        } else {
            return null;
        }
    }

    public OffsetDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        if (!(json instanceof JsonPrimitive)) {
            throw new JsonParseException("The date should be a string value");
        }
        OffsetDateTime date = deserializeToDate(json, OffsetDateTime::from);
        if (typeOfT == OffsetDateTime.class) {
            return date;
        } else {
            throw new IllegalArgumentException(getClass() + " cannot deserialize to " + typeOfT);
        }
    }
}
