package fr.inra.agrosyst.commons.gson;

/*-
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.BiologicalControlActionDto;
import fr.inra.agrosyst.api.services.action.CarriageActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionDto;
import fr.inra.agrosyst.api.services.action.IrrigationActionDto;
import fr.inra.agrosyst.api.services.action.MaintenancePruningVinesActionDto;
import fr.inra.agrosyst.api.services.action.MineralFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.OrganicFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.OtherActionDto;
import fr.inra.agrosyst.api.services.action.PesticidesSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.SeedingActionUsageDto;
import fr.inra.agrosyst.api.services.action.TillageActionDto;

import java.lang.reflect.Type;

/**
 * @author Geoffroy Gley David : gley@codelutin.com
 */
public class ActionDtoAdapter implements JsonSerializer<AbstractActionDto>, JsonDeserializer<AbstractActionDto> {

    protected static final String PROPERTY_INTERVENTION_TYPE = "mainActionInterventionAgrosyst";

    @Override
    public AbstractActionDto deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        AbstractActionDto result = null;
        if (json.isJsonObject() && json.getAsJsonObject().has(PROPERTY_INTERVENTION_TYPE)) {

            String type = json.getAsJsonObject().get(PROPERTY_INTERVENTION_TYPE).getAsString();

            AgrosystInterventionType interventionType = AgrosystInterventionType.valueOf(type);
            result = switch (interventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                        context.deserialize(json, MineralFertilizersSpreadingActionDto.class);
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES ->
                        context.deserialize(json, PesticidesSpreadingActionDto.class);
                case AUTRE -> context.deserialize(json, OtherActionDto.class);
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER -> context.deserialize(json, MaintenancePruningVinesActionDto.class);
                case EPANDAGES_ORGANIQUES -> context.deserialize(json, OrganicFertilizersSpreadingActionDto.class);
                case IRRIGATION -> context.deserialize(json, IrrigationActionDto.class);
                case LUTTE_BIOLOGIQUE -> context.deserialize(json, BiologicalControlActionDto.class);
                case RECOLTE -> context.deserialize(json, HarvestingActionDto.class);
                case SEMIS -> context.deserialize(json, SeedingActionUsageDto.class);
                case TRANSPORT -> context.deserialize(json, CarriageActionDto.class);
                case TRAVAIL_DU_SOL -> context.deserialize(json, TillageActionDto.class);
            };
        }
        return result;
    }

    @Override
    public JsonElement serialize(AbstractActionDto src, Type type, JsonSerializationContext context) {
        return context.serialize(src, src.getClass());
    }
}
