package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.BiologicalControlActionImpl;
import fr.inra.agrosyst.api.entities.action.CarriageActionImpl;
import fr.inra.agrosyst.api.entities.action.HarvestingActionImpl;
import fr.inra.agrosyst.api.entities.action.IrrigationActionImpl;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesActionImpl;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionImpl;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionImpl;
import fr.inra.agrosyst.api.entities.action.OtherActionImpl;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionImpl;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.TillageActionImpl;

import java.lang.reflect.Type;

public class ActionAdapter implements JsonSerializer<AbstractAction>, JsonDeserializer<AbstractAction> {

    protected static final String PROPERTY = "actionType";

    @Override
    public JsonElement serialize(AbstractAction src, Type typeOfSrc, JsonSerializationContext context) {
        JsonElement result = context.serialize(src, src.getClass());
        JsonObject jsonObject = result.getAsJsonObject();
        jsonObject.addProperty(PROPERTY, src.getMainAction().getIntervention_agrosyst().name());
        return result;
    }

    @Override
    public AbstractAction deserialize(JsonElement json, Type typeOfT,
                                      JsonDeserializationContext context) throws JsonParseException {
        AbstractAction result = null;
        if (json.isJsonObject() && json.getAsJsonObject().has(PROPERTY)) {

            String type = json.getAsJsonObject().get(PROPERTY).getAsString();

            AgrosystInterventionType interventionType = AgrosystInterventionType.valueOf(type);
            result = switch (interventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> context.deserialize(json, MineralFertilizersSpreadingActionImpl.class);
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> context.deserialize(json, PesticidesSpreadingActionImpl.class);
                case AUTRE -> context.deserialize(json, OtherActionImpl.class);
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER -> context.deserialize(json, MaintenancePruningVinesActionImpl.class);
                case EPANDAGES_ORGANIQUES -> context.deserialize(json, OrganicFertilizersSpreadingActionImpl.class);
                case IRRIGATION -> context.deserialize(json, IrrigationActionImpl.class);
                case LUTTE_BIOLOGIQUE -> context.deserialize(json, BiologicalControlActionImpl.class);
                case RECOLTE -> context.deserialize(json, HarvestingActionImpl.class);
                case SEMIS -> context.deserialize(json, SeedingActionUsage.class);
                case TRANSPORT -> context.deserialize(json, CarriageActionImpl.class);
                case TRAVAIL_DU_SOL -> context.deserialize(json, TillageActionImpl.class);
            };
        }

        return result;
    }
}
