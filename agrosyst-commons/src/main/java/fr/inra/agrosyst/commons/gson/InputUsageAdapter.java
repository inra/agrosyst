package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;

import java.lang.reflect.Type;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class InputUsageAdapter implements JsonSerializer<AbstractInputUsage>, JsonDeserializer<AbstractInputUsage> {
    
    protected static final String PROPERTY_INPUT_TYPE = AbstractInputUsage.PROPERTY_INPUT_TYPE;
    protected static final String PROPERTY_DOMAIN_SEED_LOT_INPUT = SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT;
    
    @Override
    public AbstractInputUsage deserialize(
            JsonElement json,
            Type typeOfT,
            JsonDeserializationContext context) throws JsonParseException {
        
        AbstractInputUsage result = null;
        if (json.isJsonObject() && json.getAsJsonObject().has(PROPERTY_INPUT_TYPE)) {
            
            final JsonObject asJsonObject = json.getAsJsonObject();
            String type = json.getAsJsonObject().get(PROPERTY_INPUT_TYPE).getAsString();
            
            InputType inputType = InputType.valueOf(type);
            result = switch (inputType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> context.deserialize(json, MineralProductInputUsage.class);
                case EPANDAGES_ORGANIQUES -> context.deserialize(json, OrganicProductInputUsage.class);
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> context.deserialize(json, PesticideProductInputUsage.class);
                case LUTTE_BIOLOGIQUE -> context.deserialize(json, BiologicalProductInputUsage.class);
                case SEMIS, PLAN_COMPAGNE -> asJsonObject.has(PROPERTY_DOMAIN_SEED_LOT_INPUT) ? context.deserialize(json, SeedLotInputUsage.class) : context.deserialize(json, SeedSpeciesInputUsage.class);
                case AUTRE -> context.deserialize(json, OtherProductInputUsage.class);
                case SUBSTRAT -> context.deserialize(json, SubstrateInputUsage.class);
                case POT -> context.deserialize(json, PotInputUsage.class);
                default -> throw new UnsupportedOperationException("Unsupported input usage type: " + inputType);
            };
        }
        
        return result;
    }
    
    @Override
    public JsonElement serialize(AbstractInputUsage src, Type typeOfSrc, JsonSerializationContext context) {
    
        return context.serialize(src, src.getClass());
    }
    
}
