/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.commons.gson;

import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;

public abstract class AbstractJavaTimeAdapter<T extends TemporalAccessor> implements JsonSerializer<T>, JsonDeserializer<T> {

    protected static final DateTimeFormatter ISO_8601_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    protected static final String ISO_8601_REGEX = "^([+-]?\\d{4}(?!\\d{2}\\b))((-?)((0[1-9]|1[0-2])(\\3([12]\\d|0[1-9]|3[01]))?|W([0-4]\\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\\d|[12]\\d{2}|3([0-5]\\d|6[1-6])))([T\\s]((([01]\\d|2[0-3])((:?)[0-5]\\d)?|24:?00)([.,]\\d+(?!:))?)?(\\17[0-5]\\d([.,]\\d+)?)?([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)?)?)?$";

    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        String dateFormatAsString = ISO_8601_FORMAT.format(src);
        return new JsonPrimitive(dateFormatAsString);
    }

    protected T deserializeToDate(JsonElement json, TemporalQuery<T> query) {
        String stringDate = json.getAsString();
        if (stringDate.matches(ISO_8601_REGEX)) {
            try {
                return ISO_8601_FORMAT.parse(json.getAsString(), query);
            } catch (DateTimeParseException e) {
                throw new JsonSyntaxException(json.getAsString(), e);
            }
        } else {
            return null;
        }

    }
}
