package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.measure.MeasureImpl;
import fr.inra.agrosyst.api.entities.measure.Measurement;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.measure.ObservationImpl;

import java.lang.reflect.Type;

/**
 * JSON serializer / deserializer for {@link Measurement} instances.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class MeasurementAdapter implements JsonSerializer<Measurement>,
        JsonDeserializer<Measurement> {

    @Override
    public JsonElement serialize(Measurement src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src, src.getClass());
    }

    @Override
    public Measurement deserialize(JsonElement json, Type typeOfT,
            JsonDeserializationContext context) throws JsonParseException {
        Measurement result = null;
        if (json.isJsonObject() && json.getAsJsonObject().has(Measurement.PROPERTY_MEASUREMENT_TYPE)) {

            String type = json.getAsJsonObject().get(Measurement.PROPERTY_MEASUREMENT_TYPE).getAsString();

            MeasurementType measurementType = MeasurementType.valueOf(type);
            result = switch (measurementType) {
                case PLANTE, SOL, TRANSFERT_DE_SOLUTES, GES, METEO -> context.deserialize(json, MeasureImpl.class);
                case STADE_CULTURE, NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES, ADVENTICES -> context.deserialize(json, ObservationImpl.class);
            };
        }

        return result;
    }
}
