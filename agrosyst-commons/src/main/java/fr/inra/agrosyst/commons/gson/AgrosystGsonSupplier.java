package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Supplier;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnitAbstract;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryAbstract;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesAbstract;
import fr.inra.agrosyst.api.entities.DomainFuelInput;
import fr.inra.agrosyst.api.entities.DomainFuelInputAbstract;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainIrrigationInputAbstract;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputAbstract;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInputAbstract;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainOtherInputAbstract;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputAbstract;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainPotInputAbstract;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputAbstract;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInputAbstract;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.EquipmentAbstract;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.GeoPointAbstract;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GroundAbstract;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemAbstract;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.LivestockUnitAbstract;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotAbstract;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionAbstract;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionAbstract;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeAbstract;
import fr.inra.agrosyst.api.entities.measure.Measurement;
import fr.inra.agrosyst.api.entities.measure.MeasurementAbstract;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementSessionAbstract;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceAbstract;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleAbstract;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseAbstract;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpecies;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpeciesAbstract;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAImpl;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserAbstract;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.internal.AbstractTopiaEntity;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Map;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class AgrosystGsonSupplier implements Supplier<Gson> {

    protected static final Multimap<Class<?>, String> GSON_EXCLUSIONS = HashMultimap.create();

    /*
     * Add class attributes exclusion to prevent stackoverflow exception during json serialisation.
     */
    static {
        GSON_EXCLUSIONS.put(AbstractActionAbstract.class, AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION);
        GSON_EXCLUSIONS.put(AbstractActionAbstract.class, AbstractAction.PROPERTY_PRACTICED_INTERVENTION);
        GSON_EXCLUSIONS.put(AbstractTopiaEntity.class, TopiaEntity.PROPERTY_TOPIA_VERSION);
        GSON_EXCLUSIONS.put(AgrosystUserAbstract.class, AgrosystUser.PROPERTY_PASSWORD);
        GSON_EXCLUSIONS.put(CroppingPlanEntryAbstract.class, CroppingPlanEntry.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(CroppingPlanSpeciesAbstract.class, CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY);
        GSON_EXCLUSIONS.put(EffectiveInterventionAbstract.class, EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE);
        GSON_EXCLUSIONS.put(EffectiveInterventionAbstract.class, EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE);
        GSON_EXCLUSIONS.put(EffectiveInterventionAbstract.class, EffectiveIntervention.PROPERTY_SPECIES_STADES);
        GSON_EXCLUSIONS.put(EquipmentAbstract.class, Equipment.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(GeoPointAbstract.class, GeoPoint.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(GroundAbstract.class, Ground.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(GrowingSystemAbstract.class, GrowingSystem.PROPERTY_NETWORKS);
        GSON_EXCLUSIONS.put(LivestockUnitAbstract.class, LivestockUnit.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(ManagementModeAbstract.class, ManagementMode.PROPERTY_HISTORICAL);
        GSON_EXCLUSIONS.put(MeasurementAbstract.class, MeasurementAbstract.PROPERTY_MEASUREMENT_SESSION);
        GSON_EXCLUSIONS.put(MeasurementSessionAbstract.class, MeasurementSession.PROPERTY_ZONE);
        GSON_EXCLUSIONS.put(PerformanceAbstract.class, Performance.PROPERTY_AUTHOR);
        GSON_EXCLUSIONS.put(PlotAbstract.class, Plot.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(PracticedCropCycleAbstract.class, PracticedCropCycle.PROPERTY_PRACTICED_SYSTEM);
        GSON_EXCLUSIONS.put(PracticedCropCyclePhaseAbstract.class, PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE);
        GSON_EXCLUSIONS.put(PracticedCropCycleSpeciesAbstract.class, PracticedCropCycleSpecies.PROPERTY_CYCLE);
    
        GSON_EXCLUSIONS.put(AbstractDomainInputStockUnitAbstract.class, AbstractDomainInputStockUnit.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainFuelInputAbstract.class, DomainFuelInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainIrrigationInputAbstract.class, DomainIrrigationInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainMineralProductInputAbstract.class, DomainMineralProductInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainOrganicProductInputAbstract.class, DomainOrganicProductInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainOtherInputAbstract.class, DomainOtherInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainPhytoProductInputAbstract.class, DomainPhytoProductInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainPotInputAbstract.class, DomainPotInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainSeedLotInputAbstract.class, DomainSeedLotInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainSeedSpeciesInput.class, DomainSeedSpeciesInput.PROPERTY_DOMAIN);
        GSON_EXCLUSIONS.put(DomainSubstrateInputAbstract.class, DomainSubstrateInput.PROPERTY_DOMAIN);
    }

    protected static final ExclusionStrategy EXCLUSION_STRATEGY = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            // TODO AThimel 06/08/13 Maybe another Multimap implementation will do the job ?
            Class<?> declaringClass = f.getDeclaringClass();
            String attributeName = f.getName();
            return GSON_EXCLUSIONS.containsEntry(declaringClass, attributeName);
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    public static final java.util.function.Supplier<GsonBuilder> GSON_BUILDER_SUPPLIER = () -> {
        GsonBuilder result = new GsonBuilder();

        // Type adapters : Hibernate proxies
        result.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);

        // Type adapters : base types
        result.registerTypeAdapter(Double.class, new DoubleAdapter());
        result.registerTypeAdapter(Integer.class, new IntegerAdapter());
        // encore quelques java.util.Date (topiaCreateDate)
        result.registerTypeAdapter(Date.class, new DateAdapter());
        result.registerTypeAdapter(Timestamp.class, new DateAdapter());
        result.registerTypeAdapter(java.sql.Date.class, new DateAdapter());
        result.registerTypeAdapter(Class.class, new ClassAdapter());
        result.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter());
        result.registerTypeAdapter(OffsetDateTime.class, new OffsetDateTimeAdapter());
        result.registerTypeAdapter(LocalDate.class, new LocalDateAdapter());
        
        // Type adapters : abstract entities
        result.registerTypeAdapter(RefMateriel.class, new RefMaterielAdapter());
        result.registerTypeAdapter(RefVariete.class, new RefVarieteAdapter());
        result.registerTypeAdapter(AbstractAction.class, new ActionAdapter());

        result.registerTypeAdapter(AbstractActionDto.class, new ActionDtoAdapter());
        result.registerTypeAdapter(DomainInputDto.class, new DomainInputAdapter());
        result.registerTypeAdapter(AbstractInputUsage.class, new InputUsageAdapter());
        
        // Type adapters : all non-abstract entities
        Map<Class<? extends TopiaEntity>, Class<? extends TopiaEntity>> pairs = Entities.getAllAgrosystTypesAndImplementation();
        for (Map.Entry<Class<? extends TopiaEntity>, Class<? extends TopiaEntity>> entry : pairs.entrySet()) {
            Class<? extends TopiaEntity> type = entry.getKey();
            Class<? extends TopiaEntity> implementationClass = entry.getValue();
            TopiaEntityAdapter adapter = new TopiaEntityAdapter(implementationClass);
            result.registerTypeAdapter(type, adapter);
        }
    
        result.registerTypeAdapter(RefFertiMinUNIFA.class, new RefFertiMinUNIFAAdapter());
        result.registerTypeAdapter(RefFertiMinUNIFAImpl.class, new RefFertiMinUNIFAAdapter());
        result.registerTypeAdapter(RefBioAgressor.class, new RefBioAgressorAdapter());
        result.registerTypeAdapter(Measurement.class, new MeasurementAdapter());
        result.registerTypeAdapter(InputPrice.class, new PriceAdapter());

        // General customization
        result.enableComplexMapKeySerialization();

        // Register exclusion strategy
        result.addSerializationExclusionStrategy(EXCLUSION_STRATEGY);
        return result;
    };

    protected static GsonBuilder gsonBuilder;

    protected Gson gson;

    protected static GsonBuilder getGsonBuilder() {
        if (gsonBuilder == null) {
            // Assign only when initialization is finished to be thread-safe
            gsonBuilder = GSON_BUILDER_SUPPLIER.get();
        }
        return gsonBuilder;
    }

    @Override
    public Gson get() {
        if (gson == null) {
            gson = getGsonBuilder().create();
        }
        return gson;
    }

}
