package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;

import java.lang.reflect.Type;

public class RefVarieteAdapter implements JsonSerializer<RefVariete>, JsonDeserializer<RefVariete> {

    @Override
    public JsonElement serialize(RefVariete src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src, src.getClass());
    }

    @Override
    public RefVariete deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RefVariete result;
        JsonObject jsonObject = json.getAsJsonObject();

        //
        if (jsonObject.has(RefVarieteGeves.PROPERTY_NUM__DOSSIER)) {
            result = context.deserialize(json, RefVarieteGeves.class);
        } else if (jsonObject.has(RefVarietePlantGrape.PROPERTY_CODE_VAR)) {
            result = context.deserialize(json, RefVarietePlantGrape.class);
        } else {
            throw new UnsupportedOperationException("Unable to guess which type is the given RefVariete");
        }
        return result;
    }
}
