package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;

import java.lang.reflect.Type;

public class RefBioAgressorAdapter implements JsonSerializer<RefBioAgressor>, JsonDeserializer<RefBioAgressor> {

    @Override
    public JsonElement serialize(RefBioAgressor src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src, src.getClass());
    }

    @Override
    public RefBioAgressor deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RefBioAgressor result;
        JsonObject jsonObject = json.getAsJsonObject();

        //
        if (jsonObject.has(RefNuisibleEDI.PROPERTY_REFERENCE_ID)) {
            result = context.deserialize(json, RefNuisibleEDI.class);
        } else if (jsonObject.has(RefAdventice.PROPERTY_IDENTIFIANT)) {
            result = context.deserialize(json, RefAdventice.class);
        } else {
            throw new UnsupportedOperationException("Unable to guess which type is the given RefBioAgressor");
        }
        return result;
    }
}
