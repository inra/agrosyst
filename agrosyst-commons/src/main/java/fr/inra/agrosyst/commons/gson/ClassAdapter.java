package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Type;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class ClassAdapter implements JsonSerializer<Class<?>>, JsonDeserializer<Class<?>> {

    private static final Log LOGGER = LogFactory.getLog(ClassAdapter.class);

    protected static final String ATTRIBUTE_NAME = "class";
    protected static final String MISSING_ATTRIBUTE_MESSAGE = String.format("Expected property %s is missing", ATTRIBUTE_NAME);

    @Override
    public JsonElement serialize(Class<?> src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.getCanonicalName(), String.class);
//        JsonObject result = new JsonObject();
//        String className = src.getCanonicalName();
//        result.addProperty(ATTRIBUTE_NAME, className);
//        return result;
    }

    @Override
    public Class<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//        JsonObject jsonObject = json.getAsJsonObject();
//        if (!jsonObject.has(ATTRIBUTE_NAME)) {
//            throw new JsonParseException(MISSING_ATTRIBUTE_MESSAGE);
//        }
        String className = json.getAsString();//jsonObject.get(ATTRIBUTE_NAME).getAsString();
        try {
            Class<?> result = Class.forName(className);
            return result;
        } catch (ClassNotFoundException cnfe) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("An exception occurred", cnfe);
            }
            throw new JsonParseException("Class not found: " + className, cnfe);
        }
    }

}
