package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurImpl;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationImpl;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutilImpl;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTractionImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Type;

/**
 * Cet adapter va se baser sur le contenu du JSON pour déterminer quelle est la bonne classe. Chacun des champs utilisés
 * dans cette classe doit être unique (n'exister que sur un des sous-types) et doit être un type primitif afin que la
 * valeur soit toujours présente dans le JSON généré.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class RefMaterielAdapter implements JsonSerializer<RefMateriel>, JsonDeserializer<RefMateriel> {

    private static final Log LOGGER = LogFactory.getLog(RefMaterielAdapter.class);

    @Override
    public JsonElement serialize(RefMateriel src, Type typeOfSrc, JsonSerializationContext context) {
        Class<? extends RefMateriel> srcClass = src.getClass();
        JsonElement result = context.serialize(src, srcClass);
        JsonObject jsonObject = result.getAsJsonObject();
        MaterielType type = null;
        if (src instanceof RefMaterielAutomoteur) {
            type = MaterielType.AUTOMOTEUR;
        } else if (src instanceof RefMaterielIrrigation) {
            type = MaterielType.IRRIGATION;
        } else if (src instanceof RefMaterielTraction) {
            type = MaterielType.TRACTEUR;
        } else if (src instanceof RefMaterielOutil) {
            type = MaterielType.OUTIL;
        }
        if (type != null) {
            jsonObject.addProperty("type", type.name());
        }
        return result;
    }

    @Override
    public RefMateriel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RefMateriel result = null;
        JsonObject jsonObject = json.getAsJsonObject();

        // Try to guess from properties
        if (jsonObject.has(RefMaterielAutomoteurImpl.PROPERTY_PERFORMANCE_COUT_TOTAL_AVEC_CARBURANT_PAR_H)) {
            result = context.deserialize(json, RefMaterielAutomoteurImpl.class);
        } else if (jsonObject.has(RefMaterielIrrigationImpl.PROPERTY_COUT_ENERGIE_PAR_UNITE_DE_TRAVAIL)) {
            result = context.deserialize(json, RefMaterielIrrigationImpl.class);
        } else if (jsonObject.has(RefMaterielOutilImpl.PROPERTY_DONNEES_PUISSANCE1_ADEQUATE)) {
            result = context.deserialize(json, RefMaterielOutilImpl.class);
        } else if (jsonObject.has(RefMaterielTractionImpl.PROPERTY_PNEUS_PAR_UNITE_DE_TRAVAIL)) {
            result = context.deserialize(json, RefMaterielTractionImpl.class);
        }

        // If not found, guess from type
        if (result == null && jsonObject.has("type")) {
            String type = jsonObject.getAsJsonPrimitive("type").getAsString();
            MaterielType materielType = MaterielType.valueOf(type);
            switch (materielType) {
                case AUTOMOTEUR:
                    result = context.deserialize(json, RefMaterielAutomoteurImpl.class);
                    break;
                case IRRIGATION:
                    result = context.deserialize(json, RefMaterielIrrigationImpl.class);
                    break;
                case OUTIL:
                    result = context.deserialize(json, RefMaterielOutilImpl.class);
                    break;
                case TRACTEUR:
                    result = context.deserialize(json, RefMaterielTractionImpl.class);
                    break;
                case AUTRE:
                    // c'est possible, mais il n'y a pas de materiel dans ce cas
                    break;
                default:
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Unsupported 'type': " + materielType);
                    }
                    break;
            }
        }

        return result;
    }

}
