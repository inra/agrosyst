package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.services.referential.RefFertiMinUNIFADto;

import java.lang.reflect.Type;

/**
 * @author David Cossé (Code Lutin)
 */
public class RefFertiMinUNIFAAdapter implements JsonSerializer<RefFertiMinUNIFA> {

    @Override
    public JsonElement serialize(RefFertiMinUNIFA src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(new RefFertiMinUNIFADto(src), RefFertiMinUNIFADto.class);
    }

}
