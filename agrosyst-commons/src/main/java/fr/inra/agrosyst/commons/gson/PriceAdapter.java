package fr.inra.agrosyst.commons.gson;

/*
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.SeedPrice;

import java.lang.reflect.Type;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class PriceAdapter implements JsonSerializer<InputPrice>, JsonDeserializer<InputPrice> {
    
    
    @Override
    public InputPrice deserialize(
            JsonElement json,
            Type typeOfT,
            JsonDeserializationContext context) throws JsonParseException {
        InputPrice result = null;
        if(json.isJsonObject()) {
            final JsonObject jsonObject = json.getAsJsonObject();
            boolean isSeedPrice  = jsonObject.has(SeedPrice.PROPERTY_BIOLOGICAL_SEED_INOCULATION);
            boolean isHarvestingPrice  = jsonObject.has(HarvestingPrice.PROPERTY_PRACTICED_SYSTEM) || jsonObject.has(HarvestingPrice.PROPERTY_ZONE) ;
            if (isSeedPrice) {
                result = context.deserialize(json, SeedPrice.class);
            } else if (isHarvestingPrice) {
                result = context.deserialize(json, HarvestingPrice.class);
            } else {
              result = context.deserialize(json, InputPrice.class);
            }
        }
        return result;
    }
    
    @Override
    public JsonElement serialize(
            InputPrice src,
            Type type,
            JsonSerializationContext context) {
        return context.serialize(src, src.getClass());
    }
}
