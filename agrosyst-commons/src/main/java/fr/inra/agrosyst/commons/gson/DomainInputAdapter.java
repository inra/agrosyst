package fr.inra.agrosyst.commons.gson;

/*-
 * #%L
 * Agrosyst :: Commons
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainFuelInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainIrrigationInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainManualWorkforceInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMechanizedWorkforceInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOrganicProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOtherProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSubstrateInputDto;

import java.lang.reflect.Type;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class DomainInputAdapter implements JsonSerializer<DomainInputDto>, JsonDeserializer<DomainInputDto> {
    
    protected static final String PROPERTY_INPUT_TYPE = AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE;
    
    @Override
    public DomainInputDto deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        DomainInputDto result = null;
        if (json.isJsonObject() && json.getAsJsonObject().has(PROPERTY_INPUT_TYPE)) {
        
            String type = json.getAsJsonObject().get(PROPERTY_INPUT_TYPE).getAsString();
        
            InputType inputType = InputType.valueOf(type);
            result = switch (inputType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> context.deserialize(json, DomainMineralProductInputDto.class);
                case EPANDAGES_ORGANIQUES -> context.deserialize(json, DomainOrganicProductInputDto.class);
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE -> context.deserialize(json, DomainPhytoProductInputDto.class);
                case CARBURANT -> context.deserialize(json, DomainFuelInputDto.class);
                case IRRIGATION -> context.deserialize(json, DomainIrrigationInputDto.class);
                case SEMIS, PLAN_COMPAGNE -> context.deserialize(json, DomainSeedLotInputDto.class);
                case AUTRE -> context.deserialize(json, DomainOtherProductInputDto.class);
                case SUBSTRAT -> context.deserialize(json, DomainSubstrateInputDto.class);
                case POT -> context.deserialize(json, DomainPotInputDto.class);
                case MAIN_OEUVRE_MANUELLE -> context.deserialize(json, DomainManualWorkforceInputDto.class);
                case MAIN_OEUVRE_TRACTORISTE -> context.deserialize(json, DomainMechanizedWorkforceInputDto.class);
                default -> throw new AgrosystTechnicalException(String.format("Type d'intrant '%s' non géré", inputType));
            };
        }
        return result;
    }
    
    @Override
    public JsonElement serialize(DomainInputDto src, Type type, JsonSerializationContext context) {
        return context.serialize(src, src.getClass());
    }
}
