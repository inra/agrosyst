# snapshot docker file, create docker from current git state
# use with:
#  docker build --file Dockerfile -t inra/agrosyst:$(date +%Y%m%d%H%M%S) .

# the following parametters overide the ones present on https://github.com/docker-library/tomcat/blob/5d36a1cb80ddf73f37353460a5b1eb0f7a675779/8.5/jre8/Dockerfile
FROM tomcat:9-jdk21-temurin-noble
MAINTAINER David Cossé <cosse@codelutin.com>

RUN echo 'CATALINA_OPTS="-Xms128M"\n' > /usr/local/tomcat/bin/setenv.sh

# As tomcat8.5 use Rfc6265CookieProcessor as default we must specified to use LegacyCookieProcessor see: https://tomcat.apache.org/migration-85.html
RUN sed -i 's/<\/Context>/    <CookieProcessor className="org.apache.tomcat.util.http.LegacyCookieProcessor" \/>\n<\/Context>\n/g' /usr/local/tomcat/conf/context.xml

# Change default configuration for post to allowed to post more than 2Mo
RUN sed -i 's/redirectPort="8443"/maxPostSize="52428800"\n               redirectPort="8443"/g' /usr/local/tomcat/conf/server.xml

#
# rename war ROOT.war to use agrosyst on / (no context)
#
RUN rm -fr /usr/local/tomcat/webapps/ROOT
COPY ./agrosyst-web/target/agrosyst-web-*.war /usr/local/tomcat/webapps/ROOT.war
