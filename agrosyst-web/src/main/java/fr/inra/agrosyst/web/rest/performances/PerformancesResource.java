package fr.inra.agrosyst.web.rest.performances;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.performance.ActiveSubstance;
import fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses;
import fr.inra.agrosyst.api.entities.performance.Ift;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterImpl;
import fr.inra.agrosyst.api.entities.performance.MineralFertilization;
import fr.inra.agrosyst.api.entities.performance.OrganicFertilization;
import fr.inra.agrosyst.api.entities.performance.OrganicProduct;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.TotalFertilization;
import fr.inra.agrosyst.api.services.common.AttachmentService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.performance.indicators.IndicatorOrganicProducts;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorMineralFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorOrganicFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorTotalFertilization;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorDecomposedOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorActiveSubstanceAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorNeonicotinoidsAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSoilAppliedHerbicidesAmount;
import fr.inra.agrosyst.web.rest.CustomInject;
import fr.inra.agrosyst.web.rest.Secured;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static fr.inra.agrosyst.api.entities.performance.ExportType.DB;
import static fr.inra.agrosyst.web.rest.performances.IndicatorUtils.CONVERT_TO_INDICATORS;
import static fr.inra.agrosyst.web.rest.performances.IndicatorUtils.IFT_INDICATORS_PARAMETER;
import static fr.inra.agrosyst.web.rest.performances.MethodUtils.*;

@Secured
@Path("/performances")
public class PerformancesResource {

    @CustomInject
    private PerformanceService performanceService;

    @CustomInject
    private AttachmentService attachmentService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(PerformanceCreationEditionDto performanceDto) {
        var indicatorFilters = buildIndicatorFilters(performanceDto);
        var performanceCreated = performanceService.createperformance(
                performanceDto.name(),
                performanceDto.domainIds(),
                performanceDto.growingSystemIds(),
                performanceDto.plotIds(),
                performanceDto.zoneIds(),
                indicatorFilters,
                Collections.emptySet(),
                performanceDto.exportToDb(),
                performanceDto.practiced()
        );
        Response response = Response.status(Response.Status.CREATED)
                .entity(buildPerformanceDto(performanceCreated, 0))
                .build();
        return response;
    }

    @PUT
    @Path("/{topiaId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("topiaId") String topiaId, PerformanceCreationEditionDto performanceDto) {
        var indicatorFilters = buildIndicatorFilters(performanceDto);
        var performanceUpdated = performanceService.updatePerformance(
                topiaId,
                performanceDto.name(),
                performanceDto.domainIds(),
                performanceDto.growingSystemIds(),
                performanceDto.plotIds(),
                performanceDto.zoneIds(),
                indicatorFilters,
                Collections.emptySet(),
                performanceDto.exportToDb()
        );// false ignore
        var attachmentCount = attachmentService.getAttachmentMetadatas(performanceUpdated.getTopiaId()).size();
        Response response = Response.ok()
                .entity(buildPerformanceDto(performanceUpdated, attachmentCount))
                .build();
        return response;
    }

    private List<IndicatorFilter> buildIndicatorFilters(PerformanceCreationEditionDto performanceDto) {
        var indicatorFilters = new ArrayList<IndicatorFilter>();
        var does = new ArrayList<DecomposedOperatingExpenses>();
        var ifts = new ArrayList<Ift>();
        var mineralFertilizations = new ArrayList<MineralFertilization>();
        var organicFertilizations = new ArrayList<OrganicFertilization>();
        var totalFertilizations = new ArrayList<TotalFertilization>();
        var sasActiveSubstances = new ArrayList<ActiveSubstance>();
        var organicProducts = new ArrayList<OrganicProduct>();
        for (Indicator indicator : performanceDto.indicators()) {
            indicator.getClazz().ifPresent((clazz) -> {
                if (clazz.equals(IndicatorDecomposedOperatingExpenses.class)) {
                    indicator.getDoe().ifPresent(does::add);
                } else if (clazz.equals(IndicatorMineralFertilization.class)) {
                    indicator.getMineralFertilization().ifPresent(mineralFertilizations::add);
                } else if (clazz.equals(IndicatorOrganicFertilization.class)) {
                    indicator.getOrganicFertilization().ifPresent(organicFertilizations::add);
                } else if (clazz.equals(IndicatorTotalFertilization.class)) {
                    indicator.getTotalFertilization().ifPresent(totalFertilizations::add);
                } else if (clazz.equals(IndicatorActiveSubstanceAmount.class) ||
                        clazz.equals(IndicatorSoilAppliedHerbicidesAmount.class) ||
                        clazz.equals(IndicatorNeonicotinoidsAmount.class)) {
                    indicator.getActiveSubstance().ifPresent(sasActiveSubstances::add);
                } else if (clazz.equals(IndicatorOrganicProducts.class)) {
                    indicator.getOrganicProduct().ifPresent(organicProducts::add);
                } else {
                    var filter = new IndicatorFilterImpl();
                    filter.setClazz(indicator.getClazz().get().getSimpleName());
                    indicator.getHandleParameters().ifPresent((consumer) -> consumer.accept(performanceDto.methods(), filter));
                    indicatorFilters.add(filter);
                }
            });
            indicator.getIft().ifPresent(ifts::add);
            indicator.getClazzResolver().ifPresent((resolver) -> {
                var classes = resolver.apply(performanceDto.methods());
                var filters = classes.stream().map((clazz) -> {
                    var filter = new IndicatorFilterImpl();
                    filter.setClazz(clazz.getSimpleName());
                    indicator.getHandleParameters().ifPresent((consumer) -> consumer.accept(performanceDto.methods(), filter));
                    return filter;
                }).toList();
                indicatorFilters.addAll(filters);
            });
        }
        createDecomposedOperatingExpensesIndicator(performanceDto, does, indicatorFilters);
        createIftIndicators(performanceDto, ifts, indicatorFilters);
        createMineralFertilizationIndicator(mineralFertilizations, indicatorFilters);
        createOrganicFertilizationIndicator(organicFertilizations, indicatorFilters);
        createTotalFertilizationIndicator(totalFertilizations, indicatorFilters);
        createSpecificActiveSubstanceAmountIndicator(performanceDto, sasActiveSubstances, indicatorFilters);
        createOrganicProductsIndicator(organicProducts, indicatorFilters);
        return indicatorFilters;
    }

    private void createDecomposedOperatingExpensesIndicator(PerformanceCreationEditionDto performanceDto, ArrayList<DecomposedOperatingExpenses> does, ArrayList<IndicatorFilter> indicatorFilters) {
        if (!does.isEmpty()) {
            var filter = new IndicatorFilterImpl();
            filter.setClazz(IndicatorDecomposedOperatingExpenses.class.getSimpleName());
            filter.setDoeIndicators(does);
            CHARGES_METHODS_PARAMETER.accept(performanceDto.methods(), filter);
            indicatorFilters.add(filter);
        }
    }

    private void createIftIndicators(PerformanceCreationEditionDto performanceDto, ArrayList<Ift> ifts, ArrayList<IndicatorFilter> indicatorFilters) {
        if (!ifts.isEmpty()) {
            IFT_INDICATORS_PARAMETER.apply(performanceDto.methods()).forEach((clazz) -> {
                var filter = new IndicatorFilterImpl();
                filter.setClazz(clazz.getSimpleName());
                filter.setIfts(ifts);
                indicatorFilters.add(filter);
            });
        }
    }

    private void createMineralFertilizationIndicator(ArrayList<MineralFertilization> mineralFertilizations, ArrayList<IndicatorFilter> indicatorFilters) {
        if (!mineralFertilizations.isEmpty()) {
            var filter = new IndicatorFilterImpl();
            filter.setClazz(IndicatorMineralFertilization.class.getSimpleName());
            filter.setMineralFertilizations(mineralFertilizations);
            indicatorFilters.add(filter);
        }
    }

    private void createOrganicFertilizationIndicator(ArrayList<OrganicFertilization> organicFertilizations, ArrayList<IndicatorFilter> indicatorFilters) {
        if (!organicFertilizations.isEmpty()) {
            var filter = new IndicatorFilterImpl();
            filter.setClazz(IndicatorOrganicFertilization.class.getSimpleName());
            filter.setOrganicFertilizations(organicFertilizations);
            indicatorFilters.add(filter);
        }
    }

    private void createTotalFertilizationIndicator(ArrayList<TotalFertilization> totalFertilizations, ArrayList<IndicatorFilter> indicatorFilters) {
        if (!totalFertilizations.isEmpty()) {
            var filter = new IndicatorFilterImpl();
            filter.setClazz(IndicatorTotalFertilization.class.getSimpleName());
            filter.setTotalFertilizations(totalFertilizations);
            indicatorFilters.add(filter);
        }
    }

    private void createSpecificActiveSubstanceAmountIndicator(
            PerformanceCreationEditionDto performanceDto,
            ArrayList<ActiveSubstance> sasActiveSubstances,
            ArrayList<IndicatorFilter> indicatorFilters) {
        if (!sasActiveSubstances.isEmpty()) {
            var filter = new IndicatorFilterImpl();
            filter.setClazz(IndicatorActiveSubstanceAmount.class.getSimpleName());
            filter.setActiveSubstances(sasActiveSubstances);
            SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER.accept(performanceDto.methods(), filter);
            indicatorFilters.add(filter);
            if (sasActiveSubstances.contains(ActiveSubstance.NEONICOTINOIDES)) {
                IndicatorFilterImpl neonicotinoidesFilter = new IndicatorFilterImpl();
                neonicotinoidesFilter.setClazz(IndicatorNeonicotinoidsAmount.class.getSimpleName());
                SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER.accept(performanceDto.methods(), neonicotinoidesFilter);
                indicatorFilters.add(neonicotinoidesFilter);
            }
            if (sasActiveSubstances.contains(ActiveSubstance.HERBICIDES_RACINAIRES)) {
                var soilAppliedHerbicidesAmountFilter = new IndicatorFilterImpl();
                soilAppliedHerbicidesAmountFilter.setClazz(IndicatorSoilAppliedHerbicidesAmount.class.getSimpleName());
                SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER.accept(performanceDto.methods(), soilAppliedHerbicidesAmountFilter);
                indicatorFilters.add(soilAppliedHerbicidesAmountFilter);
            }
        }
    }

    private void createOrganicProductsIndicator(ArrayList<OrganicProduct> organicProducts, ArrayList<IndicatorFilter> indicatorFilters) {
        if (!organicProducts.isEmpty()) {
            var filter = new IndicatorFilterImpl();
            filter.setClazz(IndicatorOrganicProducts.class.getSimpleName());
            filter.setOrganicProducts(organicProducts);
            indicatorFilters.add(filter);
        }
    }

    private boolean isComputeOnReal(PerformanceCreationEditionDto performanceDto) {
        return performanceDto.methods().contains(Method.PRIX_REELS_CHARGES) || performanceDto.methods().contains(Method.PRIX_REELS_PRODUITS_MARGES);
    }

    private boolean isComputeOnStandardised(PerformanceCreationEditionDto performanceDto) {
        return performanceDto.methods().contains(Method.PRIX_STANDARDISES_MILLESIMES_CHARGES) || performanceDto.methods().contains(Method.PRIX_STANDARDISES_MILLESIMES_PRODUITS_MARGES);
    }

    private PerformanceDto buildPerformanceDto(Performance performance, int attachmentCount) {
        var methods = CONVERT_TO_METHODS(performance.getIndicatorFilter());
        var indicators = CONVERT_TO_INDICATORS(performance.getIndicatorFilter());

        Collection<String> domainsTopiaIds = DaoUtils.getShortenIds(performance.getDomainsTopiaIds(), Domain.class);

        Collection<String> growingSystemsTopiaIds = DaoUtils.getShortenIds(performance.getGrowingSystemsTopiaIds(), GrowingSystem.class);

        Collection<String> plotsTopiaIds = DaoUtils.getShortenIds(performance.getPlotsTopiaIds(), Plot.class);

        Collection<String> zonesTopiaIds = DaoUtils.getShortenIds(performance.getZonesTopiaIds(), Zone.class);

        PerformanceDto performanceDto = new PerformanceDto(
                performance.getTopiaId(),
                performance.getName(),
                performance.isPracticed(),
                methods,
                indicators,
                domainsTopiaIds,
                growingSystemsTopiaIds,
                plotsTopiaIds,
                zonesTopiaIds,
                DB.equals(performance.getExportType()),
                performance.getComputeStatus(),
                attachmentCount
        );
        return performanceDto;
    }

    @GET
    @Path("/{topiaId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response read(
            @PathParam("topiaId") String topiaId) {
        var performance = performanceService.getPerformance(topiaId);
        var attachmentCount = attachmentService.getAttachmentMetadatas(performance.getTopiaId()).size();
        Response response = Response.ok()
                .entity(buildPerformanceDto(performance, attachmentCount))
                .build();
        return response;
    }

}
