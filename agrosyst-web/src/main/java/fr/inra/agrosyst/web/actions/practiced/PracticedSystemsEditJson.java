package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.practiced.CropCycleModelDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Actions json utilisées par l'action {@link PracticedSystemsEditJson}.
 * 
 * Action supportée:
 * - rechargement au changement 'system de culture' et/ou 'campagne'
 * 
 * @author Eric Chatellier
 */
public class PracticedSystemsEditJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(PracticedSystemsEditJson.class);

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 4385936088893971371L;
    public static final String GS_NOT_FOUND = "NOT_FOUND";

    protected transient PracticedSystemService practicedSystemService;
    protected transient GrowingSystemService growingSystemService;
    protected transient DomainInputStockUnitService domainInputStockUnitService;

    protected String growingSystemId;

    protected String campaigns;

    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setDomainInputStockUnitService(DomainInputStockUnitService domainInputStockUnitService) {
        this.domainInputStockUnitService = domainInputStockUnitService;
    }

    public void setGrowingSystemId(String growingSystemId) {
        this.growingSystemId = growingSystemId;
    }

    public void setCampaigns(String campaigns) {
        this.campaigns = campaigns;
    }

    @Override
    @Action("practiced-systems-crops-tools-json")
    public String execute() {
        try {
            Map<String, Object> data = new HashMap<>();
    
            final boolean are_campaigns_valids = CommonService.getInstance().areCampaignsValids(campaigns);
            data.put("areValidCampaigns", are_campaigns_valids);

            if (are_campaigns_valids) {
                Set<Integer> practicedSystemCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(campaigns);
                GrowingSystem practicedSystemSelectedGrowingSystem = growingSystemService.getGrowingSystem(growingSystemId);

                Domain originalDomain = practicedSystemSelectedGrowingSystem.getGrowingPlan().getDomain();
                Domain toDomain = originalDomain;
                if (!practicedSystemCampaigns.contains(originalDomain.getCampaign())) {
                    // changement de SDC pour en prendre un dans la campagne valide
                    GrowingSystem sameCodeGrowingSystemReplacement = growingSystemService.getLastGrowingSystemForCampaigns(practicedSystemSelectedGrowingSystem.getCode(), practicedSystemCampaigns);
                    if (sameCodeGrowingSystemReplacement != null) {
                        toDomain = sameCodeGrowingSystemReplacement.getGrowingPlan().getDomain();
                        data.put("practicedSystemGrowingSystemReplacement", sameCodeGrowingSystemReplacement.getTopiaId());
                    } else {
                        data.put("practicedSystemGrowingSystemReplacement", GS_NOT_FOUND);
                        practicedSystemCampaigns.add(originalDomain.getCampaign());
                        campaigns = CommonService.ARRANGE_CAMPAIGNS_SET.apply(practicedSystemCampaigns);
                    }
                }
                
                // chargement de toutes les cultures pour les systems de cultures et les années
                Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> modelToSpecies =
                        practicedSystemService.getCropCycleModelMap(growingSystemId, true);
                Set<CropCycleModelDto> croppingPlanEntryDtos = modelToSpecies.keySet();

                // definition de la liste de culture principale
                Iterable<CropCycleModelDto> modelMain = Iterables.filter(croppingPlanEntryDtos, CroppingPlans.IS_NOT_INTERMEDIATE::test);
                data.put("practicedSystemMainCropCycleModels", Lists.newArrayList(modelMain)); // force no lazy

                // définition de la liste de culture intermédiaire
                Iterable<CropCycleModelDto> modelIntermediate = Iterables.filter(croppingPlanEntryDtos, CroppingPlans.IS_INTERMEDIATE::test);
                data.put("practicedSystemIntermediateCropCycleModels", Lists.newArrayList(modelIntermediate)); // force no lazy

                // chargement de la map 'code culture' > liste d'espece
                Map<String, List<CroppingPlanSpeciesDto>> croppingPlanEntryCodesToSpecies = new HashMap<>();
                for (Map.Entry<CropCycleModelDto, List<CroppingPlanSpeciesDto>> entry : modelToSpecies.entrySet()) {
                    croppingPlanEntryCodesToSpecies.put(entry.getKey().getCroppingPlanEntryCode(), entry.getValue());
                }
                data.put("croppingPlanEntryCodesToSpecies", croppingPlanEntryCodesToSpecies);
                // code du domaine associé au système de culture
                data.put("domainCode", practicedSystemService.getDomainCode(growingSystemId));

                data.put("sector", growingSystemService.getGrowingSystemSector(growingSystemId));

                data.put("isOrganic", growingSystemService.isOrganicGrowingSystem(growingSystemId));

                NavigationContext navigationContext = getNavigationContext();
                data.put("growingSystemCampaigns", growingSystemService.getGrowingSystemCampaignsFromId(growingSystemId, navigationContext));

                data.put("campaigns", CommonService.ARRANGE_CAMPAIGNS.apply(campaigns));
                
                data.put("cattles", practicedSystemService.getAllCampaignsGrowingSystemRelatedCattles(growingSystemId, CommonService.GET_CAMPAIGNS_SET.apply(campaigns)));

                data.put("domainInputsByCodes", domainInputStockUnitService.loadDomainInputStockDtoByCodes(toDomain.getTopiaId()));
            }
            jsonData = data;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load practiced System crops-tools for growingSystemId '%s' and campaigns '%s'",
                        growingSystemId, campaigns), e);
            }
            return ERROR;
        }

        return SUCCESS;
    }
    
}
