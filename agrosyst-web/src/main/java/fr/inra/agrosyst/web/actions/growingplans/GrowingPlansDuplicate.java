package fr.inra.agrosyst.web.actions.growingplans;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Duplication unique de dispositif.
 * 
 * @author Eric Chatellier
 */
public class GrowingPlansDuplicate extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -4930761732959095035L;

    protected transient GrowingPlanService growingPlanService;

    protected String growingPlanIds;

    protected String duplicateDomainId;

    protected boolean duplicateGrowingSystems;

    /** Duplicated growing plan. */
    protected GrowingPlan growingPlan;

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setGrowingPlanIds(String growingPlanIds) {
        List list = getGson().fromJson(growingPlanIds, List.class);
        if (CollectionUtils.isNotEmpty(list)) {
            this.growingPlanIds = (String) list.getFirst();
        }
    }

    public void setDuplicateDomainId(String duplicateDomainId) {
        this.duplicateDomainId = duplicateDomainId;
    }

    public void setDuplicateGrowingSystems(boolean duplicateGrowingSystems) {
        this.duplicateGrowingSystems = duplicateGrowingSystems;
    }

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "growing-plans-edit-input", "growingPlanTopiaId", "${growingPlan.topiaId}"}),
            @Result(name = ERROR, type = "redirectAction", params = {"actionName", "growing-plans-list"})})
    public String execute() throws Exception {
        growingPlan = growingPlanService.duplicateGrowingPlan(growingPlanIds, duplicateDomainId, duplicateGrowingSystems);
        return SUCCESS;
    }
    
    public GrowingPlan getGrowingPlan() {
        return growingPlan;
    }
}
