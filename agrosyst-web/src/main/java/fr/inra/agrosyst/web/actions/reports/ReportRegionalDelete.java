/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.services.report.ReportService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Suppression des bilans de campagne regionale.
 */
public class ReportRegionalDelete extends AbstractReportAction {
    
    @Serial
    private static final long serialVersionUID = 4124977115726748417L;
    
    protected transient ReportService reportService;

    protected List<String> reportRegionalIds;
    
    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReportRegionalIds(String reportRegionalIds) {
        this.reportRegionalIds = getGson().fromJson(reportRegionalIds, List.class);
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "report-regionals-list"})})
    public String execute() {
        
        // if relatedReportGrowingSystemNames not null it means that delete failed
        List<String> relatedReportGrowingSystemNames = reportService.deleteReportRegionals(reportRegionalIds);
        
        notificationSupport.failedDeleteRegionalReports(reportRegionalIds, relatedReportGrowingSystemNames);
        
        return SUCCESS;
    }
    
}
