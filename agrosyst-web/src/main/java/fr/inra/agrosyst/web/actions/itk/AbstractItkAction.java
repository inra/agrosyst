package fr.inra.agrosyst.web.actions.itk;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.action.CapacityUnit;
import fr.inra.agrosyst.api.entities.action.FertiOrgaUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.YealdCategory;
import fr.inra.agrosyst.api.entities.referential.BioAgressorParentType;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAAbstract;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.referential.GroupeCibleDTO;
import fr.inra.agrosyst.api.services.referential.MineralProductType;
import fr.inra.agrosyst.api.services.referential.RefPotDto;
import fr.inra.agrosyst.api.services.referential.RefSubstrateDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.MultiMapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.t;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public abstract class AbstractItkAction extends AbstractAgrosystAction {

    private static final Log LOGGER = LogFactory.getLog(AbstractItkAction.class);

    @Serial
    private static final long serialVersionUID = -6057182538439163217L;

    protected transient ReferentialService referentialService;
    protected transient ActionService actionService;

    protected transient List<MineralProductType> mineralProductTypes;
    protected transient List<GroupeCibleDTO> groupesCibles;
    protected transient Map<AgrosystInterventionType, List<ProductType>> actaTreatmentProductTypes;
    protected transient MultiValuedMap<String, RefSubstrateDto> substratesByCaracteristic1;
    protected transient List<RefPotDto> refPots;

    protected static final String WRONG_DESTINATION_MESSAGE = "Vous avez une destination de récolte importée par défaut depuis eDaplos, pour la culture %s. Merci de corriger cette destination ainsi que le rendement correspondant (valeur et unité).";
    protected static final String MISSING_TOOLCOUPLING_ON_INTERVENTION_MESSAGE = "Aucune combinaison d'outils ou intervention manuelle n'ont été sélectionnées pour l'une de vos interventions. Ce champs est requis pour le calcul du temps de travail et de consommation de carburant.";

    public final void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setActionService(ActionService actionService) {
        this.actionService = actionService;
    }

    @Override
    protected void initForInput() {

        mineralProductTypes = referentialService.getAllActiveMineralProductTypes();
        groupesCibles = referentialService.getGroupesCibles();
        actaTreatmentProductTypes = referentialService.getAllActiveActaTreatmentProductTypes();
        substratesByCaracteristic1 = MultiMapUtils.newSetValuedHashMap();
        List<RefSubstrateDto> allActiveRefSubstrates = referentialService.getAllActiveRefSubstrates();
        for (RefSubstrateDto substrate : allActiveRefSubstrates) {
            substratesByCaracteristic1.put(substrate.getCharacteristic1Translated(), substrate);
        }
        refPots = referentialService.getAllActiveRefPots();

        super.initForInput();
    }

    ////////////// ITK specific data //////////////

    public Map<YealdCategory, String> getYealdCategories() {
        return getEnumAsMap(YealdCategory.values());
    }

    public Map<CapacityUnit, String> getCapacityUnits() {
        return getEnumAsMap(CapacityUnit.values());
    }

    public List<MineralProductType> getMineralProductTypes() {
        return mineralProductTypes;
    }

    public Map<BioAgressorType, BioAgressorParentType> getTreatmentTargetCategoriesByParent() {
        Map<BioAgressorType, BioAgressorParentType> result = Referentials.getTreatmentTargetBioAgressorParentTypes().stream()
                .collect(Collectors.toMap(BioAgressorParentType::getParent, Function.identity()));
        return result;
    }

    public Map<BioAgressorType, String> getTreatmentTargetCategories() {
        Map<BioAgressorType, String> enumTranslationMap = referentialService.getTranslatedBioAgressorType();
        Map<BioAgressorType, String> treatmentTargetCategories = Referentials.getTreatmentTargetBioAgressorParentTypes().stream()
                .map(BioAgressorParentType::getParent)
                .collect(Collectors.toMap(Function.identity(), enumTranslationMap::get));
        return treatmentTargetCategories;
    }

    public List<GroupeCibleDTO> getGroupesCibles() {
        return groupesCibles;
    }

    public Map<SeedType, String> getSeedTypes() {
        Map<SeedType, String> enumTranslationMap = i18nService.getEnumTranslationMap(SeedType.class);
        return enumTranslationMap;
    }

    public Map<SeedPlantUnit, String> getSeedPlantUnits() {
        return getEnumAsMap(SeedPlantUnit.values());
    }

    public Map<MineralProductUnit, String> getMineralProductUnits() {
        return getEnumAsMap(MineralProductUnit.values());
    }

    public Map<OrganicProductUnit, String> getOrganicProductUnits() {
        return getEnumAsMap(OrganicProductUnit.values());
    }

    public Map<PhytoProductUnit, String> getPhytoProductUnits() {
        return getEnumAsMap(PhytoProductUnit.values());
    }

    public Map<ProductType, String> getProductTypes() {
        return getEnumAsMap(ProductType.values());
    }

    public Map<AgrosystInterventionType, List<ProductType>> getActaTreatmentProductTypes() {
        if (actaTreatmentProductTypes == null) {
            actaTreatmentProductTypes = referentialService.getAllActiveActaTreatmentProductTypes();
        }
        return actaTreatmentProductTypes;
    }

    public Map<FertiOrgaUnit, String> getFertiOrgaUnits() {
        return getEnumAsMap(FertiOrgaUnit.values());
    }

    public Map<InputType, String> getInputTypesLabels() {
        return getEnumAsMap(InputType.values());
    }

    public Map<String, Collection<RefSubstrateDto>> getSubstratesByCaracteristic1() {
        return substratesByCaracteristic1.asMap();
    }

    public Map<SubstrateInputUnit, String> getSubstrateInputUnits() {
        return getEnumAsMap(SubstrateInputUnit.values());
    }

    public List<RefPotDto> getRefPots() {
        return refPots;
    }

    public Map<PotInputUnit, String> getPotInputUnits() {
        return getEnumAsMap(PotInputUnit.values());
    }

    /**
     * @return RefFertiMinUNIFA composition elements.
     * Be careful: any element have to be translated otherwise it will not be return !
     */
    public Map<String, String> getMineralProductElementNamesAndLibelles() {

        Field[] champs = RefFertiMinUNIFAAbstract.class.getDeclaredFields();
        // to keep field ordering
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        for (Field champ : champs) {
            String propertyName = champ.getName();
            String libelle = getText(RefFertiMinUNIFAAbstract.class.getName() + "." + propertyName);
            // only add elements (N, Po5, K2O, ...) that are translated
            if (!libelle.startsWith(RefFertiMinUNIFAAbstract.class.getName())) {// key is translated
                result.put(propertyName, libelle);
            }

        }
        return result;
    }

    public String getDefaultDestinationName() {
        String result = StringUtils.firstNonBlank(t("prices-harvest-table-toComplete"), ActionService.DEFAULT_DESTINATION_NAME);
        return result;
    }

}
