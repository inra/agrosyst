package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotFilter;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class PracticedPlotsListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(PracticedPlotsListJson.class);

    @Serial
    private static final long serialVersionUID = -7042498750101369083L;

    protected transient PracticedPlotService practicedPlotService;
    
    protected transient String filter;
    
    public void setPracticedPlotService(PracticedPlotService practicedPlotService) {
        this.practicedPlotService = practicedPlotService;
    }
    
    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String execute() {
        try {
            PracticedPlotFilter pracfilter = getGson().fromJson(filter, PracticedPlotFilter.class);
            writeListNbElementByPage(PracticedPlotDto.class, pracfilter.getPageSize());
            NavigationContext navigationContext = getNavigationContext();
            pracfilter.setNavigationContext(navigationContext);
            jsonData = practicedPlotService.getFilteredPracticedPlotsDto(pracfilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load PracticedPlot", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
