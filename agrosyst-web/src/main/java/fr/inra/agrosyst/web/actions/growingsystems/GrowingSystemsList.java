package fr.inra.agrosyst.web.actions.growingsystems;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

public class GrowingSystemsList extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -8763014594787954150L;

    protected transient GrowingSystemService growingSystemService;

    protected GrowingSystemFilter growingSystemFilter;

    protected int exportAsyncThreshold;

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    /**
     * Result serialized in json.
     */
    protected PaginationResult<GrowingSystemDto> growingSystems;

    @Override
    public String execute() throws Exception {

        NavigationContext navigationContext = getNavigationContext();
        growingSystemFilter = new GrowingSystemFilter();
        growingSystemFilter.setNavigationContext(navigationContext);
        growingSystemFilter.setPageSize(getListNbElementByPage(GrowingSystemDto.class));
        growingSystemFilter.setActive(Boolean.TRUE);

        growingSystems = growingSystemService.getFilteredGrowingSystemsDto(growingSystemFilter);
        this.exportAsyncThreshold = this.config.getGrowingSystemsExportAsyncThreshold();
        return SUCCESS;
    }

    public String getGrowingSystemsJson() {
        String result = getGson().toJson(growingSystems);
        return result;
    }

    public int getExportAsyncThreshold() {
        return exportAsyncThreshold;
    }

    public GrowingSystemFilter getGrowingSystemFilter() {
        return growingSystemFilter;
    }

}
