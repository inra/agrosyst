package fr.inra.agrosyst.web.actions.performances;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.performance.PerformanceDto;
import fr.inra.agrosyst.api.services.performance.PerformanceFilter;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

public class PerformancesList extends AbstractAgrosystAction {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -1180723133330637375L;

    protected transient PerformanceService performanceService;

    protected boolean practiced;

    protected PerformanceFilter performanceFilter;

    protected PaginationResult<PerformanceDto> performances;

    public void setPracticed(boolean practiced) {
        this.practiced = practiced;
    }

    public boolean isPracticed() {
        return practiced;
    }

    public void setPerformanceService(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }

    @Override
    public String execute() {
        performanceFilter = new PerformanceFilter();
        performanceFilter.setPageSize(getListNbElementByPage(PerformanceDto.class, String.valueOf(practiced)));
        performanceFilter.setActive(Boolean.TRUE);
        performanceFilter.setPracticed(practiced);
        performances = performanceService.getFilteredPerformances(performanceFilter, 10);
        return SUCCESS;
    }

    public PaginationResult<PerformanceDto> getPerformances() {
        return performances;
    }
    
    public PerformanceFilter getPerformanceFilter() {
        return performanceFilter;
    }
}
