package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingStatus;
import fr.inra.agrosyst.api.services.edaplos.EdaplosService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.action.UploadedFilesAware;
import org.apache.struts2.dispatcher.multipart.UploadedFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class DomainsPreImportEdaplos extends AbstractAgrosystAction implements UploadedFilesAware {

    private static final Log LOGGER = LogFactory.getLog(DomainsPreImportEdaplos.class);

    @Serial
    private static final long serialVersionUID = 1L;

    protected File file;

    protected String fileContentType;

    protected String fileFileName;

    protected String edaplosFileId;

    protected transient EdaplosService edaplosService;

    protected EdaplosParsingResult edaplosParsingResults;

    public void setEdaplosService(EdaplosService edaplosService) {
        this.edaplosService = edaplosService;
    }

    @Override
    public String input() throws Exception {
        return super.input();
    }

    @Override
    public String execute() throws Exception {
        if (file != null) {
            try (InputStream stream = new FileInputStream(file)) {
                AuthenticatedUser authenticatedUser = getAuthenticatedUser();
                authorizationService.checkComputedPermissionsFromUserId(authenticatedUser.getTopiaId());

                edaplosFileId = edaplosService.storeEdaplosFile(stream, fileFileName, fileContentType);

                edaplosParsingResults = edaplosService.validEdaplosData(edaplosFileId);

            } catch (AgrosystAccessDeniedException ex) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Can't import eDaplos file", ex);
                }
                throw ex;
            } catch (Exception ex) {
                sendFeedback(ex);
                edaplosParsingResults = getEdaplosParsingErrorResults("L'import eDaplos a échoué, veuillez contacter l'équipe Agrosyst");
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Can't import eDaplos file", ex);
                }
                return ERROR;
            }
        } else {
            edaplosParsingResults = getEdaplosParsingErrorResults("L'import eDaplos a échoué, aucun fichier reçu");
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("No eDaplos file provided");
            }
            return ERROR;
        }
        return SUCCESS;
    }

    protected void sendFeedback(Exception ex) {
        try {
            edaplosService.sendExceptionFeedbackEmail(ex, "Analyse du fichier d'import Edaplos", fileFileName);
        } catch (Exception e) {
            // noting to do
        }
    }

    protected EdaplosParsingResult getEdaplosParsingErrorResults(String message) {
        edaplosParsingResults = new EdaplosParsingResult();
        edaplosParsingResults.addErrorMessage(message, "");
        edaplosParsingResults.setEdaplosParsingStatus(EdaplosParsingStatus.EXCEPTION);
        return edaplosParsingResults;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public void setFileFileName(String fileFileName) {
        this.fileFileName = fileFileName;
    }

    public EdaplosParsingResult getEdaplosParsingResults() {
        return edaplosParsingResults;
    }

    public String getEdaplosFileId() {
        return edaplosFileId;
    }

    @Override
    public void withUploadedFiles(List<UploadedFile> uploadedFiles) {
        if (LOGGER.isDebugEnabled()) {
            Collection<UploadedFile> uploadedFiles1 = CollectionUtils.emptyIfNull(uploadedFiles);
            LOGGER.debug(String.format("L'utilisateur %s à soumis les fichiers Edaplos %s", getAuthenticatedUser().getEmail(), uploadedFiles1.stream().map(UploadedFile::getOriginalName).collect(Collectors.joining())));
        }
    }
}
