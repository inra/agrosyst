package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2016 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import fr.inra.agrosyst.api.services.referential.ImportService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;

/**
 * Created by davidcosse on 03/12/15.
 */
public class RunApiImport extends AbstractAdminAction {
    @Serial
    private static final long serialVersionUID = 1L;

    private static final Log LOGGER = LogFactory.getLog(RunApiImport.class);

    protected transient ImportService importService;

    public void setImportService(ImportService importService) {
        this.importService = importService;
    }

    protected String genericClassName;
    protected Class<?> klass;
    protected String url;

    protected String apiKey;
    protected InputStream inputStream;

    protected String apiUser;
    protected String apiPasswd;

    @Override
    public void validate() {
        if (Strings.isNullOrEmpty(genericClassName)) {
            addFieldError("genericClassName", "Ce champ est obligatoire");
        }

        try {
            klass = Class.forName(genericClassName);
        } catch (ClassNotFoundException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e);
            }
            addFieldError("genericClassName", "Classe non trouvée");
        }

        super.validate();
    }

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "stream", params={"contentType", "text/csv",
                    "inputName", "inputStream", "contentDisposition", "attachment; filename=\"${filename}\""}),
            @Result(name = ERROR, type = "redirectAction", params = {"namespace", "/generic", "actionName",
                    "generic-entities-list", "genericClassName", "${genericClassName}", "importFileError", "false"})
    })
    public String execute() throws Exception {
        checkIsAdmin();
        try {
            inputStream = importService.getImportSolFromApiToCSVResult(
                    StringUtils.trimToNull(url),
                    StringUtils.trimToNull(apiKey),
                    klass,
                    StringUtils.trimToNull(apiUser),
                    StringUtils.trimToNull(apiPasswd));
            if (inputStream == null) {
                notificationSupport.importError("Aucune donnée n'a pu être chargée");
                return ERROR;
            }
        } catch (AgrosystImportException e) {
            LOGGER.error(e.getMessage());
            notificationSupport.importError(e.getMessage());
            return ERROR;
        }

        return SUCCESS;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getGenericClassName() {
        return genericClassName;
    }

    public String getFilename() {
        String filename = "Import_" + StringUtils.substringAfterLast(genericClassName, ".") + "_From_API.csv";
        return filename;
    }

    public void setGenericClassName(String genericClassName) {
        this.genericClassName = genericClassName;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setApiUser(String apiUser) {
        this.apiUser = apiUser;
    }

    public void setApiPasswd(String apiPasswd) {
        this.apiPasswd = apiPasswd;
    }
}
