package fr.inra.agrosyst.web.actions.admin;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

public class MaintenanceModeEdit extends AbstractAdminAction {

    @Serial
    private static final long serialVersionUID = 1L;

    protected boolean active;

    protected String message;

    protected boolean disconnectAllUsers;

    protected int connectedUsersNb;

    @Override
    @Action("maintenance-mode-edit-input")
    public String input() throws Exception {
        checkIsAdmin();
        active = authorizationService.isInMaintenanceMode();
        message = authorizationService.getMaintenanceModeMessage();
        disconnectAllUsers = authorizationService.mustDisconnetAllUsers();
        connectedUsersNb = computeConnectedUsersCount().uniqueUsersCount();
        return super.input();
    }

    @Override
    @Action(results = {
            @Result(type = "redirectAction", params = {"actionName", "maintenance-mode-edit-input"})})
    public String execute() throws Exception {
        checkIsAdmin();

        if (!active) {
            message = null;
            disconnectAllUsers = false;
        } else {
            message = message.replaceAll("(\\n|\\r)+", "<br/>");
        }
        authorizationService.setMaintenanceMode(active, message, disconnectAllUsers);

        return SUCCESS;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isDisconnectAllUsers() {
        return disconnectAllUsers;
    }

    public void setDisconnectAllUsers(boolean disconnectAllUsers) {
        this.disconnectAllUsers = disconnectAllUsers;
    }

    public int getConnectedUsersNb() {
        return connectedUsersNb;
    }

}
