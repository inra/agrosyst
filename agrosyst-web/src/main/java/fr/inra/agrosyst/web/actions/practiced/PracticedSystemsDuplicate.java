package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.exceptions.AgrosystDuplicationException;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.ArrayList;

/**
 * Duplicate single domain.
 *
 * @author Eric Chatellier
 */
public class PracticedSystemsDuplicate extends AbstractJsonAction {
    
    private static final Log LOGGER = LogFactory.getLog(PracticedSystemsDuplicate.class);
    
    @Serial
    private static final long serialVersionUID = 3763987457979325440L;
    
    // not use, just to avoid warning messages
    protected ArrayList<String> practicedSystemIds;
    
    /**
     * Single practiced system to duplicate.
     */
    protected String practicedSystemId;
    
    protected String growingSystemId;
    
    /**
     * Service.
     */
    protected transient PracticedSystemService practicedSystemService;
    
    /**
     * Practiced system.
     */
    protected PracticedSystem practicedSystem;
    
    public void setGrowingSystemId(String growingSystemId) {
        this.growingSystemId = growingSystemId;
    }

    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }

    public void setPracticedSystemId(String practicedSystemId) {
        this.practicedSystemId = practicedSystemId;
    }
    
    @Override
    public String execute() throws Exception {
        if (Strings.isNullOrEmpty(practicedSystemId) || Strings.isNullOrEmpty(growingSystemId)) {
            notificationSupport.practicedSystemNotDuplicable(practicedSystemId, growingSystemId);
            return ERROR;
        }
        
        try {
            practicedSystem = practicedSystemService.duplicatePracticedSystemWithUsages(practicedSystemId, growingSystemId);
            jsonData = practicedSystem.getTopiaId();
        } catch (AgrosystDuplicationException ade) {
            jsonData = ade.getLocalizedMessage();
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("ÉCHEC DE DUPLICATION DU SYSTÈME SYNTÉTHISÉ", ade);
            }
            return ERROR;
        } catch (Exception e) {
            jsonData = e.getLocalizedMessage();
            notificationSupport.practicedSystemNotDuplicable(e.toString());
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("ÉCHEC DE DUPLICATION DU SYSTÈME SYNTHÉTISÉ", e);
            }
            return ERROR;
        }

        notificationSupport.practicedSystemDuplicate();
        return SUCCESS;
        
    }
    
    public PracticedSystem getPracticedSystem() {
        return practicedSystem;
    }
    
    public void setPracticedSystemIds(String practicedSystemIds) {
        this.practicedSystemIds = getGson().fromJson(practicedSystemIds, ArrayList.class);
    }
}
