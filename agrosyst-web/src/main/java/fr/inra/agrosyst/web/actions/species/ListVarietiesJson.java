package fr.inra.agrosyst.web.actions.species;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;

/**
 * Action d'autocompletion des variétés:
 * - list-varieties-json
 * - list-porte-greffes-json
 * - list-clone-greffes-json
 * 
 * @author Eric Chatellier
 */
public class ListVarietiesJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(ListVarietiesJson.class);

    @Serial
    private static final long serialVersionUID = 5088194867807225637L;

    protected transient ReferentialService referentialService;
    
    protected transient String speciesId;
    protected transient String varietyId;
    protected transient String term;
    
    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }
    
    public void setSpeciesId(String speciesId) {
        this.speciesId = speciesId;
    }

    public void setVarietyId(String varietyId) {
        this.varietyId = varietyId;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public String execute() {
        try {
            jsonData = referentialService.getVarietes(speciesId, term);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load varieties for speciesId '%s' and term '%s'",
                        speciesId, term), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action(value="list-graft-support-json")
    public String listGraftSupports() {
        try {
            jsonData = referentialService.getGraftSupports(speciesId, term);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load varieties graft support for term '%s'",
                        term), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action(value="list-graft-clone-json")
    public String listGraftClones() {
        try {
            jsonData = referentialService.getGraftClones(speciesId, varietyId, term);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load clones graft support for speciesId '%s, varietyId '%s and term '%s'",
                        speciesId, varietyId, term), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
