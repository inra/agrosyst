package fr.inra.agrosyst.web.listeners;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA
 * Copyright (C) 2020 INRAE
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.hash.Hashing;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;

import static fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter.AUTHENTICATED_USER;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class AgrosystWebSessionListener implements HttpSessionListener {
    
    private static final Log LOGGER = LogFactory.getLog(AgrosystWebSessionListener.class);

    /**
     * Méthode qui génère un hash permettant d'identifier une session sans utiliser publiquement le sessionId. On fait
     * ça pour ne pas prendre le risque de loguer l'identifiant de session qui permettrait à un utilisateur malveillant
     * d'usurper l'identité d'un autre utilsateur.
     *
     * @param sessionId L'ID de session HTTP
     * @return l'identifiant de session raccourci et public
     */
    private static String obfuscateSessionId(String sessionId) {
        String result = "";
        if (StringUtils.isNotEmpty(sessionId)) {
            // On utilise crc32 au lieu de goodFastHash car le hashage a toujours le même résultat
            result = Hashing.crc32().hashString(sessionId, StandardCharsets.UTF_8).toString();
        }
        return result;
    }

    /**
     * Méthode qui génère un hash permettant d'identifier une session sans utiliser publiquement le sessionId. On fait
     * ça pour ne pas prendre le risque de loguer l'identifiant de session qui permettrait à un utilisateur malveillant
     * d'usurper l'identité d'un autre utilsateur.
     *
     * @param session La session HTTP (ne doit pas être nulle)
     * @return l'identifiant de session raccourci et public
     * @see #obfuscateSessionId(String)
     */
    private static String obfuscateSessionId(HttpSession session) {
        String sessionId = session.getId();
        String result = obfuscateSessionId(sessionId);
        if (session.isNew()) {
            result += " NEW";
        }
        return result;
    }

    public static String getSessionId(HttpServletRequest servletRequest) {
        AuthenticatedUser authenticatedUser = (AuthenticatedUser) servletRequest.getAttribute(AUTHENTICATED_USER);
        String result = Optional.ofNullable(authenticatedUser)
                .map(AuthenticatedUser::getSid)
                .orElse("--------");
        return result;
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        if (LOGGER.isInfoEnabled()) {
            HttpSession session = se.getSession();
            LOGGER.info(String.format("[%s] New HTTP session created", obfuscateSessionId(session)));
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

        HttpSession session = se.getSession();
        String sessionId = obfuscateSessionId(session);

        if (LOGGER.isInfoEnabled()) {
            Date created = new Date(session.getCreationTime());
            Date lastAccessed = new Date(session.getLastAccessedTime());

            String message = String.format("[%s] HTTP session about to be destroyed. Created on %s. Last accessed on: %s.",
                    sessionId, created, lastAccessed);
            LOGGER.info(message);
        }

    }
}
