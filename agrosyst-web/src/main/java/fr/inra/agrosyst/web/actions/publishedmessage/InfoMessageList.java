package fr.inra.agrosyst.web.actions.publishedmessage;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.history.Message;
import fr.inra.agrosyst.api.services.history.MessageFilter;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

/**
 * Created by davidcosse on 14/10/14.
 */
public class InfoMessageList extends AbstractAdminAction {

    @Serial
    private static final long serialVersionUID = 1L;

    protected MessageFilter messageFilter;

    protected PaginationResult<Message> messages;

    @Override
    public String execute() throws Exception {

        messageFilter = new MessageFilter();
        messageFilter.setPageSize(getListNbElementByPage(Message.class));
        messages = messageService.getFilteredMessages(messageFilter);
        return SUCCESS;
    }

    public PaginationResult<Message> getMessages() {
        return messages;
    }

    public MessageFilter getMessageFilter() {
        return messageFilter;
    }
}
