package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class LoadTreatmentTargetsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadTreatmentTargetsJson.class);

    @Serial
    private static final long serialVersionUID = 2286147488098650107L;

    protected transient ReferentialService referentialService;
    
    protected transient BioAgressorType category;

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setCategory(BioAgressorType category) {
        this.category = category;
    }

    @Override
    public String execute() {
        try {
            jsonData = referentialService.getTreatmentTargets(category);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load treatmentTargets for category '%s'", category), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

}
