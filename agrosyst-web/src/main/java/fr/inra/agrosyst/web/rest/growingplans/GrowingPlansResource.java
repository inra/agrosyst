package fr.inra.agrosyst.web.rest.growingplans;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanFilter;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.security.AuthorizationService;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.web.rest.CustomInject;
import fr.inra.agrosyst.web.rest.Secured;
import fr.inra.agrosyst.web.rest.common.PaginationResultDto;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Secured
@Path("/growing-plans")
public class GrowingPlansResource {

    @CustomInject
    private GrowingPlanService growingPlanService;
    @CustomInject
    private AuthorizationService authorizationService;

    private GrowingPlanFilter buildFilter(NavigationContext context, String name, String domainName, Integer campaign,
                                          TypeDEPHY typeDEPHY, String responsable, Boolean active, Set<String> selectedIds, Integer pageNumber,
                                          Integer pageSize) {
        var filter = new GrowingPlanFilter();
        filter.setNavigationContext(context);
        filter.setGrowingPlanName(name);
        filter.setDomainName(domainName);
        filter.setCampaign(campaign);
        filter.setTypeDephy(typeDEPHY);
        filter.setResponsable(responsable);
        filter.setActive(active);
        if (selectedIds != null && !selectedIds.isEmpty()) filter.setSelectedIds(selectedIds);
        if (pageNumber != null) filter.setPage(pageNumber);
        if (pageSize != null) filter.setPageSize(pageSize);
        return filter;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAll(@CookieParam("nav.context") Cookie jsonContext,
                            GrowingPlanRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getName(),
                requestFilter.getDomainName(),
                requestFilter.getCampaign(),
                requestFilter.getTypeDEPHY(),
                requestFilter.getResponsable(),
                requestFilter.getActive(),
                requestFilter.getSelectedIds(),
                requestFilter.getPageNumber(),
                requestFilter.getPageSize());
        var paginationResult = growingPlanService.getFilteredGrowingPlans(filter);
        var growingPlans = paginationResult.getElements().stream()
                .map((gp) -> {
                    List<UserDto> responsibles = authorizationService.getGrowingPlanResponsibles(gp.getCode());
                    var responsiblesAsStr = responsibles.stream().map((r) -> r.getFirstName() + " " + r.getLastName())
                            .collect(Collectors.joining(", "));
                    return convertToDto(gp, responsiblesAsStr);
                })
                .toList();
        return Response.ok()
                .entity(new PaginationResultDto<>(growingPlans, paginationResult.getCount(), paginationResult.getCurrentPage()))
                .build();
    }

    private GrowingPlanDto convertToDto(GrowingPlan gp, String responsibles) {
        var domainName = gp.getDomain() != null ? gp.getDomain().getName() : "";
        var campaign = gp.getDomain() != null ? gp.getDomain().getCampaign() : 0;

        return new GrowingPlanDto(
                StringUtils.remove(gp.getTopiaId(), GrowingPlan.class.getName()),
                gp.getName(),
                domainName,
                campaign,
                gp.getType(),
                responsibles,
                gp.isActive());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/filtered-ids")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIds(@CookieParam("nav.context") Cookie jsonContext,
                            GrowingPlanRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getName(),
                requestFilter.getDomainName(),
                requestFilter.getCampaign(),
                requestFilter.getTypeDEPHY(),
                requestFilter.getResponsable(),
                requestFilter.getActive(),
                null,
                null,
                null);
        var ids = growingPlanService.getFilteredGrowingPlanIds(filter);
        return Response.ok()
                .entity(ids)
                .build();
    }
}
