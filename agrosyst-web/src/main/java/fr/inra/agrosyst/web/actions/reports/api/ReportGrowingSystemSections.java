/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports.api;

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemSection;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Retourne les sections disponibles à l'export pour les bilans de campagnes et les modeles décisionnels.
 */
public class ReportGrowingSystemSections extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(ReportGrowingSystemSections.class);
    @Serial
    private static final long serialVersionUID = 7196727218227998436L;
    
    protected transient ReportService reportService;

    protected List<String> reportGrowingSystemIds;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReportGrowingSystemIds(List<String> reportGrowingSystemIds) {
        this.reportGrowingSystemIds = reportGrowingSystemIds;
    }

    @Override
    public String execute() {
        try {
            Map<String, Object> jsonData = new HashMap<>();

            Map<Sector, Collection<ReportGrowingSystemSection>> reportGrowingSectionSection =
                    reportService.getReportGrowingSystemSections(reportGrowingSystemIds);
            jsonData.put("reportGrowingSystem", reportGrowingSectionSection);

            Set<SectionType> managamentModeSection = reportService.getReportGrowingSystemManagementModeSections(reportGrowingSystemIds);
            jsonData.put("managementMode", managamentModeSection);

            this.jsonData = jsonData;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load ReportGrowingSystemSections", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
