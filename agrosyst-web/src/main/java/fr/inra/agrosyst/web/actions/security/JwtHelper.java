package fr.inra.agrosyst.web.actions.security;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.ImmutableAuthenticatedUser;
import fr.inra.agrosyst.web.AgrosystWebConfig;
import org.apache.commons.lang3.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Classe utilitaire pour la création et lecture des token JWT
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.64
 */
public class JwtHelper {

    private static final int CACHE_EXPIRATION_MINUTES = 5;
    private static final Cache<String, AuthenticatedUser> CACHE = CacheBuilder.newBuilder()
            .expireAfterWrite(CACHE_EXPIRATION_MINUTES, TimeUnit.MINUTES)
            .build();

    protected final String jwtSecret;
    protected final int timeoutSeconds;

    public JwtHelper(AgrosystWebConfig config) {
        this(config.getJwtSecret(), config.getHttpSessionTimeout());
    }

    public JwtHelper(String jwtSecret, int timeoutSeconds) {
        Preconditions.checkArgument(jwtSecret != null, "La config est obligatoire");
        this.jwtSecret = jwtSecret;
        this.timeoutSeconds = timeoutSeconds;
    }

    private Algorithm getJwtSecretAlgorithm() {
        Preconditions.checkState(StringUtils.isNotBlank(jwtSecret), "La clé JWT est manquante");
        Algorithm result = Algorithm.HMAC512(jwtSecret);
        return result;
    }

    public String createJwtToken(AuthenticatedUser userDto) {

//        iss issuer : qui a émis le token
//        sub subject : identifiant unique métier
//        aud audience
//        exp date d'expritration
//        nbf not before
//        iat issued at
//        jti identifiant unique : uuid

        Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, timeoutSeconds);
        Date expiresAt = calendar.getTime();

        Algorithm algorithmHS = getJwtSecretAlgorithm();
        JWTCreator.Builder builder = JWT.create()
                .withIssuer("agrosyst-web")
                .withSubject(userDto.getTopiaId())
                .withIssuedAt(now)
                .withExpiresAt(expiresAt)
                .withJWTId(UUID.randomUUID().toString())
                .withClaim("firstName", userDto.getFirstName())
                .withClaim("lastName", userDto.getLastName())
                .withClaim("email", userDto.getEmail())
                .withClaim("userLang", userDto.getLanguage().getTrigram())
                .withArrayClaim("roles", userDto.getRoles().stream().map(RoleType::name).toArray(String[]::new))
                .withClaim("acceptedCharter", userDto.isAcceptedCharter())
                .withClaim("sid", userDto.getSid());

        userDto.getBanner().ifPresent(v -> builder.withClaim("banner", v));
        userDto.getItEmail().ifPresent(v -> builder.withClaim("itEmail", v));

        String jwtToken = builder.sign(algorithmHS);
        return jwtToken;
    }

    public AuthenticatedUser verifyJwtToken(String jwtToken) {

        AuthenticatedUser result = CACHE.getIfPresent(jwtToken);

        if (result == null) {
            Algorithm algorithmHS = getJwtSecretAlgorithm();
            DecodedJWT verify = JWT.require(algorithmHS)
                    .withIssuer("agrosyst-web")
                    .build()
                    .verify(jwtToken);

            List<RoleType> roles = verify.getClaim("roles")
                    .asList(String.class)
                    .stream()
                    .map(RoleType::valueOf)
                    .collect(Collectors.toList());

            Language language;
            if (verify.getClaim("userLang").isNull()) {
                language = Language.FRENCH;
            } else {
                language = Language.fromTrigram(verify.getClaim("userLang").asString());
            }

            result = ImmutableAuthenticatedUser.builder()
                    .topiaId(verify.getSubject())
                    .firstName(verify.getClaim("firstName").asString())
                    .lastName(verify.getClaim("lastName").asString())
                    .email(verify.getClaim("email").asString())
                    .roles(roles)
                    .isAcceptedCharter(verify.getClaim("acceptedCharter").asBoolean())
                    .language(language)
                    .banner(Optional.ofNullable(verify.getClaim("banner").asString()))
                    .itEmail(Optional.ofNullable(verify.getClaim("itEmail").asString()))
                    .sid(verify.getClaim("sid").asString())
                    .build();

            // On ne met dans le cache que si le token expire dans plus longtemps que l'expiration du cache
            Date expiresAt = verify.getExpiresAt();
            Duration expiresIn = Duration.between(Instant.now(), expiresAt.toInstant());
            long expiresInMinutes = expiresIn.toMinutes();
            if (expiresInMinutes > CACHE_EXPIRATION_MINUTES) {
                CACHE.put(jwtToken, result);
            }
        }

        return result;
    }

    public Date getTokenIssuedAt(String jwtToken) {
        Algorithm algorithmHS = getJwtSecretAlgorithm();
        DecodedJWT verify = JWT.require(algorithmHS)
                .withIssuer("agrosyst-web")
                .build()
                .verify(jwtToken);
        Date result = verify.getIssuedAt();
        return result;
    }

}
