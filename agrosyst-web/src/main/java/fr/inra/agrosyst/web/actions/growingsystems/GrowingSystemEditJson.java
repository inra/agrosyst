package fr.inra.agrosyst.web.actions.growingsystems;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by davidcosse on 25/02/14.
 */
@Setter
public class GrowingSystemEditJson extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 1L;

    private static final Log LOGGER = LogFactory.getLog(GrowingSystemEditJson.class);
    
    protected transient GrowingSystemService growingSystemService;

    protected String practicedSystemId;

    protected Sector sector;

    protected String term;

    @Action("available-growing-system-for-duplication-json")
    public String availableGrowingSystemsForDuplication() {
        try {
            NavigationContext navigationContext = getNavigationContext();
            jsonData = growingSystemService.getAvailableGsForDuplication(practicedSystemId, navigationContext);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load Growing Systems list for duplication for practicedSystemId '%s':",
                        practicedSystemId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("load-marketing-destination-objectives-for-sector-json")
    public String loadMarketingDestinationObjectivesForSector() {
        try {
            Set<Sector> sectorSet = new HashSet<>(Collections.singletonList(sector));
            jsonData = growingSystemService.loadMarketingDestinationObjectivesForSectors(sectorSet);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load Marketing Destination Objectives For Sector '%s':",
                        sector.name()), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("load-growing-system-complete-json")
    public String loadGrowingSystemComplete() {
        try {
            GrowingSystemFilter growingSystemFilter = new GrowingSystemFilter();
            growingSystemFilter.setNavigationContext(getNavigationContext());
            growingSystemFilter.setPageSize(DEFAULT_SEARCH_PAGE_SIZE);
            growingSystemFilter.setActive(Boolean.TRUE);
            growingSystemFilter.setGrowingSystemName(term);
            jsonData = growingSystemService.getFilteredGrowingSystemsDto(growingSystemFilter).getElements();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load Marketing Destination Objectives For Sector '%s':",
                        sector.name()), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
