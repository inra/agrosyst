package fr.inra.agrosyst.web.actions.auth;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.Serial;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Results({
        @Result(name="success", type="redirectAction", params = {"namespace", "/auth", "actionName", "login-input", "email", "${email}", "actionMessage", "${actionMessage}", "next", "${next}" }),
        @Result(name="error", type="redirectAction", params = {"namespace", "/auth", "actionName", "retrieve-password-input", "token", "${token}", "userId", "${userId}"})})
public class RetrievePassword extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 3501789225489989672L;

    protected transient UserService userService;
    
    private String userId;
    private String token;
    private String password;
    private String passwordCheck;
    private UserDto user;
    // return to login
    private String actionMessage;
    private String email;
    private String next;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }
    
    public String getActionMessage() {
        return actionMessage;
    }
    
    public String getEmail() {
        return email;
    }
    
    public String getNext() {
        return next;
    }
    
    public void setNext(String next) {
        this.next = next;
    }
    
    @Override
    @Action("retrieve-password-input")
    public String input() throws Exception {
        initForInput();
        return super.input();
    }

    @Override
    protected void initForInput() {
        user = userService.preparePasswordChange(token, userId);
        if (user == null) {
            addActionError("Token invalide");
        }
    }

    @Override
    public void validate() {
        if (Strings.isNullOrEmpty(password)) {
            addFieldError("password", "Le mot de passe est obligatoire");
        } else if (!password.equals(passwordCheck)) {
            addFieldError("passwordCheck", "Les mots de passe entrés ne correspondent pas");
        }
        if (hasErrors()) {
            initForInput();
        }
    }
    
    @Override
    public String execute() throws Exception {
        UserDto userDto = userService.updatePassword(token, userId, password);
        if (userDto != null) {
            actionMessage = "Mot de passe mis à jour, merci de vous reconnecter";
            email = userDto.getEmail();
            return SUCCESS;
        } else {
            input();
            return INPUT;
        }
    }

    public UserDto getUser() {
        return user;
    }

    public String getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }

}
