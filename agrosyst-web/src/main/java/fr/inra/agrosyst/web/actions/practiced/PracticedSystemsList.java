package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemFilter;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

public class PracticedSystemsList extends AbstractAgrosystAction {
    
    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = 3084229660685255829L;
    
    protected transient PracticedSystemService practicedSystemService;
    
    protected PracticedSystemFilter practicedSystemFilter;
    
    protected int practicedSystemsExportAsyncThreshold;
    
    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }
    
    /**
     * Initial list data.
     */
    protected PaginationResult<PracticedSystemDto> practicedSystems;

    @Override
    public String execute() throws Exception {

        NavigationContext navigationContext = getNavigationContext();
        practicedSystemFilter = new PracticedSystemFilter();
        practicedSystemFilter.setNavigationContext(navigationContext);
        practicedSystemFilter.setPageSize(getListNbElementByPage(PracticedSystemDto.class));
        practicedSystemFilter.setActive(Boolean.TRUE);

        practicedSystems = practicedSystemService.getFilteredPracticedSystemsDto(practicedSystemFilter);

        this.practicedSystemsExportAsyncThreshold = config.getPracticedSystemsExportAsyncThreshold();
        return SUCCESS;
    }

    public PaginationResult<PracticedSystemDto> getPracticedSystems() {
        return practicedSystems;
    }
    
    public PracticedSystemFilter getPracticedSystemFilter() {
        return practicedSystemFilter;
    }
    
    public int getPracticedSystemsExportAsyncThreshold() {
        return practicedSystemsExportAsyncThreshold;
    }
    
}
