package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.LoadedPricesResult;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * @author David Cossé
 */
public class LoadPricesJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadPricesJson.class);

    @Serial
    private static final long serialVersionUID = -1437968191353986009L;

    private transient PricesService pricesService;

    /* Incomming parameters */
    private transient String domainId;
    public void setPricesService(PricesService pricesService) {
        this.pricesService = pricesService;
    }


    @Override
    public String execute() throws Exception {

        try {

            LoadedPricesResult pResult = pricesService.getHarvestingPriceResults(domainId);
            
            jsonData = pResult.getHarvestingValorisationPrices();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load prices for domainId '%s'", domainId), e);
            }
            return ERROR;
        }

        return SUCCESS;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
