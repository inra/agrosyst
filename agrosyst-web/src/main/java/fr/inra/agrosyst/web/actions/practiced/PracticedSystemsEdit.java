package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.domain.CattleDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.practiced.CropCycleModelDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleNodeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.practiced.PracticedInterventionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPerennialCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.web.actions.itk.AbstractItkAction;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Action d'édition d'un système synthétisé.
 * Avec:
 * <ul>
 * <li>l'onglet géneralité
 * <li>la liste des cycles pluriannuels assolés
 * <li>la liste des cycles pluriannuels pérennes
 * <li>l'itinéraire technique
 * </ul>
 *
 * @author Eric Chatellier
 */
@Getter
@Setter
@Results({
        @Result(name="success", type="redirectAction", params = {"namespace", "/practiced", "actionName", "practiced-systems-edit-input", "practicedSystemTopiaId", "${practicedSystemTopiaId}" }),
        @Result(name="error", type="redirectAction", params = {"namespace", "/practiced", "actionName", "practiced-systems-edit-input", "practicedSystemTopiaId", "${practicedSystemTopiaId}" })})
public class PracticedSystemsEdit extends AbstractItkAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(PracticedSystemsEdit.class);
    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = -5696853256365484417L;
    public static final String UNKNOWN = "?";

    protected transient DomainInputStockUnitService domainInputStockUnitService;

    protected transient PracticedSystemService practicedSystemService;

    protected transient GrowingSystemService growingSystemService;

    /**
     * L'id du systeme synthétisé en cours d'edition.
     */
    protected String practicedSystemTopiaId;

    /**
     * L'instance du systeme synthétisé en cours d'edition.
     */
    protected PracticedSystem practicedSystem;

    /**
     * Id du domaine
     */
    protected String domainId;

    protected String domainCode;

    protected Integer domainCampaign;

    protected Sector growingSystemSector;
    protected Boolean organic = null;


    /**
     * Le systeme de culture sélectionné.
     */
    protected String growingSystemTopiaId;

    /**
     * Identifiant du premier système de culture parmis la liste des système de culture/
     * Utilisé lors de la créaction d'un système synthétisé pour le binding angular
     */
    private String defaultGrowingSystemId;

    /**
     * La liste des campaignes.
     */
    protected String campaigns;

    /**
     *
     * La liste des campagnes des systèmes de culture avec le même code
     */
    private List<Integer> gsCampaigns;

    private List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos;
    private String practicedPerennialCropCycleDtosJson;

    private List<RefOrientationEDI> refOrientationEDIs;

    /**
     * Liste des cycles pluriannuels assolé.
     */
    protected List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos;
    protected String practicedSeasonalCropCycleDtosJson;

    /**
     * Contient une map &lt;culture, List&lt;Species&gt;&gt; pour toutes les cultures des systemes de cultures
     * concernés sur les campagnes du systeme synthétisé.
     */
    protected Map<String, List<CroppingPlanSpeciesDto>> practicedSystemCroppingPlanEntryCodesToSpecies;

    /**
     * Les cultures principales disponibles dans le graphique.
     */
    protected List<CropCycleModelDto> practicedSystemMainCropCycleModels;

    /**
     * Les cultures intermédiaires disponible dans le graphique.
     */
    protected List<CropCycleModelDto> practicedSystemIntermediateCropCycleModels;

    protected Collection<CattleDto> cattles = new ArrayList<>();

    /**
     * On conserve le temps de la requête la liste des campagnes avant modification. ImmutableSet pour s'éviter des
     * mauvaises surprises.
     */
    protected ImmutableSet<Integer> campaignsBeforeModification;


    public PracticedSystem getPracticedSystem() {
        // EChatellier 27/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(practicedSystem, PracticedSystemImpl::new);
    }

    @Override
    public void prepare() {
        if (StringUtils.isNotBlank(practicedSystemTopiaId)) {

            authorizationService.checkPracticedSystemReadable(practicedSystemTopiaId);

            practicedSystem = practicedSystemService.getPracticedSystem(practicedSystemTopiaId);
            String psGrowingSystemTopiaId = practicedSystem.getGrowingSystem().getTopiaId();
            if (StringUtils.isNotBlank(growingSystemTopiaId) && !psGrowingSystemTopiaId.contentEquals(growingSystemTopiaId)) {
                if (PracticedSystemsEditJson.GS_NOT_FOUND.contentEquals(growingSystemTopiaId)) {
                    // rollback to valid id
                    growingSystemTopiaId = psGrowingSystemTopiaId;
                }
                GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
                practicedSystem.setGrowingSystem(growingSystem);
                domainCampaign = growingSystem.getGrowingPlan().getDomain().getCampaign();
            }
            gsCampaigns = practicedSystemService.getGrowingSystemCampaignsFromId(practicedSystemTopiaId, getNavigationContext());
            String campaignsString = practicedSystem.getCampaigns();
            if (CommonService.getInstance().areCampaignsValids(campaignsString)) {
                this.campaignsBeforeModification = ImmutableSet.copyOf(CommonService.GET_CAMPAIGNS_SET.apply(campaignsString));
            }
            activated = practicedSystemService.isActivated(practicedSystem);
        } else {
            // j'offre la possibilité de crééer un PracticedCropCycle
            practicedSystem = new PracticedSystemImpl();
        }
    }

    @Override
    @Action("practiced-systems-edit-input")
    public String input() {
        initForInput();

        loadPersistedCycles();

        return INPUT;
    }

    protected void loadPersistedCycles() {
        if (practicedSystem != null && practicedSystem.isPersisted()) {
            authorizationService.checkPracticedSystemReadable(practicedSystemTopiaId);

            readOnly = !authorizationService.isPracticedSystemWritable(practicedSystemTopiaId);
            if (readOnly) {
                notificationSupport.practicedSystemNotWritable();
            }

            practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(practicedSystemTopiaId);
            practicedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(practicedSystemTopiaId);
    
            warnEdaplosDestinationUsage();
            
            populateSeasonalCropCycles(practicedSeasonalCropCycleDtos);
        } else {
            // default to empty lists
            practicedSeasonalCropCycleDtos = new ArrayList<>();
            practicedPerennialCropCycleDtos = new ArrayList<>();
        }
    }
    
    private void warnEdaplosDestinationUsage() {
        List<PracticedCropCycleNodeDto> nodeDtos = practicedSeasonalCropCycleDtos.stream()
                .map(PracticedSeasonalCropCycleDto::getCropCycleNodeDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = practicedSeasonalCropCycleDtos.stream()
                .map(PracticedSeasonalCropCycleDto::getCropCycleConnectionDtos)
                .flatMap(Collection::stream)
                .toList();

        Optional<String> eDaplosDefaultDestination = cropCycleConnectionDtos.stream()
                .map(PracticedCropCycleConnectionDto::getInterventions)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(PracticedInterventionDto::getActionDtos)
                .flatMap(Collection::stream)
                .filter(action -> action instanceof HarvestingAction)
                .filter(Objects::nonNull)
                .map(action -> ((HarvestingAction)action).getValorisations())
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(HarvestingActionValorisation::getDestination)
                .map(RefDestination::getDestination)
                .filter(ActionService.DEFAULT_DESTINATION_NAME::equals)
                .findAny();
    
        List<String> cropLabels = new ArrayList<>();
        if (eDaplosDefaultDestination.isPresent()) {
            Map<String, PracticedCropCycleNodeDto> nodeDtoById = Maps.uniqueIndex(nodeDtos, PracticedCropCycleNodeDto::getNodeId);
            
            Map<Integer, ArrayList<String>> cropNameByRank = new HashMap<>();
            for (PracticedCropCycleConnectionDto cropCycleConnectionDto : cropCycleConnectionDtos) {
                if (CollectionUtils.isEmpty(cropCycleConnectionDto.getInterventions())) {
                    continue;
                }
                Optional<String> defaultDestination = cropCycleConnectionDto.getInterventions().stream()
                        .filter(Objects::nonNull)
                        .map(PracticedInterventionDto::getActionDtos)
                        .flatMap(Collection::stream)
                        .filter(action -> action instanceof HarvestingAction)
                        .filter(Objects::nonNull)
                        .map(action -> ((HarvestingAction)action).getValorisations())
                        .flatMap(Collection::stream)
                        .filter(Objects::nonNull)
                        .map(HarvestingActionValorisation::getDestination)
                        .map(RefDestination::getDestination)
                        .filter(ActionService.DEFAULT_DESTINATION_NAME::equals)
                        .findAny();

                if (defaultDestination.isPresent()) {
                    PracticedCropCycleNodeDto nodeDto = nodeDtoById.get(cropCycleConnectionDto.getTargetId());
                    int rank = nodeDto.getX() + 1;
                    ArrayList<String> cropNamesForRank = cropNameByRank.computeIfAbsent(rank, k -> new ArrayList<>());
                    String cropLabel = StringUtils.isNotBlank(cropCycleConnectionDto.getIntermediateCroppingPlanEntryCode()) ? String.format(cropCycleConnectionDto.getIntermediateCropName() +  " (CI Rang %d) ", rank) : String.format(nodeDto.getLabel() +  " (Rang %d) ", rank);
                    cropNamesForRank.add(cropLabel);
                }
            }
            if (!cropNameByRank.isEmpty()) {
                final List<Integer> odoredRank = cropNameByRank.keySet().stream().sorted(Integer::compareTo).toList();
                for (Integer rank : odoredRank) {
                    List<String> cropNames = cropNameByRank.get(rank);
                    cropNames.sort(String::compareTo);
                    cropLabels.addAll(cropNames);
                }
            }
            
        }
    
        List<String> perennialCropNames = new ArrayList<>();
        for (PracticedPerennialCropCycleDto practicedPerennialCropCycleDto : practicedPerennialCropCycleDtos) {
            if (CollectionUtils.isEmpty(practicedPerennialCropCycleDto.getCropCyclePhaseDtos())) {
                continue;
            }
            Optional<String> defaultDestination = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().stream()
                    .map(PracticedCropCyclePhaseDto::getInterventions)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(PracticedInterventionDto::getActionDtos)
                    .flatMap(Collection::stream)
                    .filter(action -> action instanceof HarvestingAction)
                    .filter(Objects::nonNull)
                    .map(action -> ((HarvestingAction)action).getValorisations())
                    .flatMap(Collection::stream)
                    .filter(Objects::nonNull)
                    .map(HarvestingActionValorisation::getDestination)
                    .map(RefDestination::getDestination)
                    .filter(ActionService.DEFAULT_DESTINATION_NAME::equals)
                    .findAny();
            if (defaultDestination.isPresent()) {
                perennialCropNames.add(practicedPerennialCropCycleDto.getCroppingPlanEntryName());
            }
        }
        if (!perennialCropNames.isEmpty()) {
            perennialCropNames.sort(String::compareTo);
            cropLabels.addAll(perennialCropNames);
        }
    
        if (!cropLabels.isEmpty()) {
            notificationSupport.warning(String.format(WRONG_DESTINATION_MESSAGE, String.join(", ", cropLabels)));
        }

    }
    
    protected void checkRights() {
        authorizationService.checkPracticedSystemReadable(practicedSystemTopiaId);
        readOnly = !authorizationService.isPracticedSystemWritable(practicedSystemTopiaId);
        if (readOnly) {
            notificationSupport.practicedSystemNotWritable();
        }
    }

    protected List<PracticedSeasonalCropCycleDto> loadPersistedSeasonalCycles(String practicedSystemTopiaId) {
        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos;
        if(StringUtils.isNotBlank(practicedSystemTopiaId)) {
            checkRights();
            practicedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(practicedSystemTopiaId);
            populateSeasonalCropCycles(practicedSeasonalCropCycleDtos);
        } else {
            practicedSeasonalCropCycleDtos = new ArrayList<>();
        }
        return practicedSeasonalCropCycleDtos;
    }

    protected List<PracticedPerennialCropCycleDto> loadPersistedPerennialCycles(String practicedSystemTopiaId) {
        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos;
        if(StringUtils.isNotBlank(practicedSystemTopiaId)) {
            checkRights();
            practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(practicedSystemTopiaId);
        } else {
            practicedPerennialCropCycleDtos = new ArrayList<>();
        }
        return practicedPerennialCropCycleDtos;
    }

    protected void populateSeasonalCropCycles(List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos) {

        if (practicedSystemIntermediateCropCycleModels != null && practicedSeasonalCropCycleDtos != null) {

            for (PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto : practicedSeasonalCropCycleDtos) {

                List<PracticedCropCycleConnectionDto> connections = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();
                // valuation du label des connexions
                if (connections != null) {
                    for (PracticedCropCycleConnectionDto connection : connections) {
                        final String intermediateExpectedCode = connection.getIntermediateCroppingPlanEntryCode();
                        String label = "";
                        if (StringUtils.isNotBlank(intermediateExpectedCode)) {
                            Optional<CropCycleModelDto> optional = practicedSystemIntermediateCropCycleModels.stream()
                                    .filter(cropCycleVueModelDto -> intermediateExpectedCode.equals(cropCycleVueModelDto.getCroppingPlanEntryCode()))
                                    .findAny();
                            if (optional.isPresent()) {
                                if (connection.getCroppingPlanEntryFrequency() != null) {
                                    label = connection.getCroppingPlanEntryFrequency().intValue() + "%<br/>" +
                                            "<span class='hover-infos'>" + optional.get().getLabel() + "</span>";
                                } else {
                                    label = "<span class='hover-infos'>" + optional.get().getLabel() + "</span>";
                                }
                            }
                        } else {
                            if (connection.getCroppingPlanEntryFrequency() != null) {
                                label += connection.getCroppingPlanEntryFrequency().intValue() + "%";
                            }
    
                        }
                        connection.setLabel(label);
                        
                    }
                }

            }

        }

    }
    
    @Override
    protected void initForInput() {
        PracticedSystem practicedSystem = getPracticedSystem();

        GrowingSystem growingSystem = null;

        if (practicedSystem.isPersisted()) {
            // case it is persisted
            growingSystem = practicedSystem.getGrowingSystem();
        } else if (StringUtils.isNotBlank(growingSystemTopiaId)) {
            // error validation on creation case
            growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
        }

        if (growingSystem != null) {
            // warning, practicedSystem's growingSystem must always be part of growingSystems set
            // even not selected be navigation context
            growingSystemTopiaId = growingSystem.getTopiaId();
            growingSystemSector = growingSystem.getSector();
            organic = growingSystem.getTypeAgriculture() == null ? null : growingSystem.getTypeAgriculture().getReference_id() == ReferentialService.ORGANIC_REF_TYPE_CONDUITE_REFERENCE_ID;
            Domain domain = growingSystem.getGrowingPlan().getDomain();
            domainCode = domain.getCode();
            domainCampaign = domain.getCampaign();
            domainId = domain.getTopiaId();
            campaigns = practicedSystem.getCampaigns() == null ? domainCampaign.toString() : practicedSystem.getCampaigns();
            cattles = practicedSystemService.getAllCampaignsPracticedSystemRelatedCattles(practicedSystem.getTopiaId(), CommonService.GET_CAMPAIGNS_SET.apply(campaigns));
        }
        
        // chargement des référentiels
        refOrientationEDIs = referentialService.getAllReferentielEDI();

        // initialisation des données chargés en json autrement sur la page
        if (!Strings.isNullOrEmpty(growingSystemTopiaId)) {
            // chargement des toutes les cultures pour les systems de cultures et les années
            Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> modelToSpecies =
                    practicedSystemService.getCropCycleModelMap(growingSystemTopiaId, true);
            Set<CropCycleModelDto> croppingPlanEntryDtos = modelToSpecies.keySet();

            // definition de la liste de culture principale
            practicedSystemMainCropCycleModels = croppingPlanEntryDtos.stream().filter(CroppingPlans.IS_NOT_INTERMEDIATE).collect(Collectors.toList()); // force no lazy

            // définition de la liste de culture intermédiaire
            practicedSystemIntermediateCropCycleModels = croppingPlanEntryDtos.stream().filter(CroppingPlans.IS_INTERMEDIATE).collect(Collectors.toList()); // force no lazy

            // chargement de la map 'code culture' > liste d'espece
            practicedSystemCroppingPlanEntryCodesToSpecies = new HashMap<>();
            for (Map.Entry<CropCycleModelDto, List<CroppingPlanSpeciesDto>> entry : modelToSpecies.entrySet()) {
                practicedSystemCroppingPlanEntryCodesToSpecies.put(entry.getKey().getCroppingPlanEntryCode(), entry.getValue());
            }

        }

        super.initForInput();
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(getPracticedSystem().getTopiaId())) {
            validateGrowingSystemForNewPracticedSystem();
        } else {
            growingSystemTopiaId = getPracticedSystem().getGrowingSystem().getTopiaId();
        }

        Domain domain = null;
        Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds;
        if (StringUtils.isNotBlank(growingSystemTopiaId)) {
            GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
            domain = growingSystem.getGrowingPlan().getDomain();
            domainInputStockUnitByIds = domainInputStockUnitService.loadDomainInputStockByIds(domain);
        } else {
            domainInputStockUnitByIds = new HashMap<>();
        }

        if (practicedSystem.isPersisted() && !activated) {
            addActionError(getText("practicedSystem-error-inactive-action-message"));
        }

        if (StringUtils.isBlank(practicedSystem.getName())) {
            addFieldError("practicedSystem.name", getText(REQUIRED_FIELD));
            addActionError(getText("practicedSystem-error-name-action-message"));
        }

        boolean areValidcampaigns = validateCampaigns(practicedSystem);

        List<String> requiredCropcycleCode = new ArrayList<>();

        // utilisé pour la validation des copiers/coller
        Map<String, List<Pair<String, String>>> allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = null;
        Map<String, List<Sector>> allsectorByCodeEspceBotaniqueCodeQualifiant = null;

        if (areValidcampaigns) {
            allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCode();
            allsectorByCodeEspceBotaniqueCodeQualifiant = getSectorByCodeEspceBotaniqueCodeQualifiant();
        }

        practicedPerennialCropCycleDtos = validatePerennialCropCycles(
                practicedSystemTopiaId,
                requiredCropcycleCode,
                practicedPerennialCropCycleDtosJson,
                allCodeEspeceBotaniqueCodeQualifantBySpeciesCode,
                allsectorByCodeEspceBotaniqueCodeQualifiant,
                domainInputStockUnitByIds);

        practicedSeasonalCropCycleDtos = validateSeasonalCropCycles(
                practicedSystemTopiaId,
                requiredCropcycleCode,
                practicedSeasonalCropCycleDtosJson,
                allCodeEspeceBotaniqueCodeQualifantBySpeciesCode,
                allsectorByCodeEspceBotaniqueCodeQualifiant,
                domainInputStockUnitByIds);

        validateSeriesOfCampaigns(areValidcampaigns, requiredCropcycleCode);

        validToolCouplingsOnInterventions();

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                String email = getAuthenticatedUser().getEmail();
                LOGGER.error(String.format("For user email:" + email + " Pour le synthétisé: %s", (practicedSystemTopiaId == null ? "Nouveau" : practicedSystemTopiaId)));
                LOGGER.error(String.format("For user email:" + email + ":validate, action errors : growingSystemTopiaId:'%s', campaigns '%s' -> %s", growingSystemTopiaId, practicedSystem.getCampaigns(), getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + email + ":validate, fields errors : growingSystemTopiaId:'%s', campaigns '%s' -> %s", growingSystemTopiaId, practicedSystem.getCampaigns(), getFieldErrors().toString()));
            }
            if (!areValidcampaigns && (domain != null || practicedSystem.getGrowingSystem() != null)) {
                int campaign = domain != null ? domain.getCampaign() : practicedSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign();
                practicedSystem.setCampaigns(String.valueOf(campaign));
            }
            initForInput();
        }

    }

    protected Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant() {
        return practicedSystemService.getSectorByCodeEspceBotaniqueCodeQualifiant(growingSystemTopiaId, practicedSystem.getCampaigns());
    }

    protected Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCode() {
        return practicedSystemService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(growingSystemTopiaId, practicedSystem.getCampaigns());
    }

    protected void validateGrowingSystemForNewPracticedSystem() {
        if (StringUtils.isBlank(growingSystemTopiaId)) {
            addFieldError("growingSystemTopiaId", "Le système de culture est requis !");
        }
    }

    private void validToolCouplingsOnInterventions() {
        List<PracticedInterventionDto> sInterventions = practicedPerennialCropCycleDtos.stream()
                .map(PracticedPerennialCropCycleDto::getCropCyclePhaseDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(PracticedCropCyclePhaseDto::getInterventions)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();
        List<PracticedInterventionDto> pInterventions = practicedSeasonalCropCycleDtos.stream()
                .map(PracticedSeasonalCropCycleDto::getCropCycleConnectionDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(PracticedCropCycleConnectionDto::getInterventions)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();
        if (CollectionUtils.union(sInterventions, pInterventions).stream()
                .anyMatch(intervention -> intervention.getToolsCouplingCodes().isEmpty())) {
            String text = getText("practicedSystem.itks.itk.intervention.missingToolsCouplings");
            notificationSupport.warning(text);
        }
    }

    protected boolean validateCampaigns(PracticedSystem practicedSystem) {
        String campaigns = practicedSystem.getCampaigns();
        if (StringUtils.isBlank(campaigns)) {
            addFieldError("practicedSystem.campaigns", "La série de campagne est obligatoire");
        }

        boolean validcampaigns = false;
        if (StringUtils.isNotBlank(campaigns)) {
            validcampaigns = CommonService.getInstance().areCampaignsValids(campaigns);
            if (!validcampaigns) {
                String format = "La série de campagne doit être composée d'années séparées par des virgules (,), " +
                        "espaces ( ) ou point-virgules (;). Les campagnes doivent être contenues entre %d et %d.";
                Pair<Integer, Integer> bounds = CommonService.getInstance().getCampaignsBounds();
                addFieldError("practicedSystem.campaigns", String.format(format, bounds.getLeft(), bounds.getRight()));
            }

        }
        return validcampaigns;
    }

    protected List<PracticedPerennialCropCycleDto> validatePerennialCropCycles(
            String practicedSystemTopiaId,
            List<String> requiredCropcycleCode,
            String practicedPerennialCropCycleDtosJson,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = null;
        try {
            practicedPerennialCropCycleDtos = convertPracticedPerennialCropCycleDtosJson(practicedPerennialCropCycleDtosJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("failed to load json values for practicedPerennialCropCycleDtosJson", ex);
            }
            addActionError("Exception:" + ex + "\n/!\\ Désolé les données saisies depuis votre dernier enregistrement concernant vos cycles de cultures pérennes n'ont pu être récupérées !");
        }

        // in case of exception during Gson deserialization then load back persisted data so user doesn't lose every thing;
        if (practicedPerennialCropCycleDtos == null) {
            practicedPerennialCropCycleDtos = loadPersistedPerennialCycles(practicedSystemTopiaId);
        } else if (!practicedPerennialCropCycleDtos.isEmpty()) {
            double solOccupationPercent = 0d;
            // validate perennial cycle
            for (PracticedPerennialCropCycleDto practicedPerennialCropCycleDto : practicedPerennialCropCycleDtos) {
                final PracticedPerennialCropCycle cropCycle = practicedPerennialCropCycleDto.getPracticedPerennialCropCycle();
                if (cropCycle.getSolOccupationPercent() < 0) {
                    addActionError("Le pourcentage dans la sole du SdC d'une culture pérenne ne peut être négatif");
                }
                solOccupationPercent += cropCycle.getSolOccupationPercent();
                if(cropCycle.getWeedType() == null) {
                    addActionError("Un cycle de culture perenne doit avoir un type d'enherbement de sélectionnée");
                }

                final Double plantingDensity = cropCycle.getPlantingDensity();
                if (plantingDensity != null && plantingDensity < 0) {
                    addActionError("Un cycle de culture pérenne doit avoir une densité de plantation positive");
                }

                String cropCycleCode = cropCycle.getCroppingPlanEntryCode();
                if (Strings.isNullOrEmpty(cropCycleCode)) {
                    addActionError("Un cycle de culture pérenne doit avoir une culture de sélectionnée");
                    addFieldError("croppingPlanEntry", "Champ obligatoire");
                } else {
                    requiredCropcycleCode.add(cropCycleCode);
                }

                validatePhases(
                        practicedPerennialCropCycleDto,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspeceBotaniqueCodeQualifiant,
                        domainInputStockUnitByIds);
            }

            if (solOccupationPercent > 100d) {
                addActionError("Le pourcentage dans la sole du SdC d'une culture pérenne ne peut être > 100");
            }
            if (solOccupationPercent == 0d) {
                addActionError("Le pourcentage dans la sole du SdC d'une culture pérenne ne peut être égal à 0");
            }
        }
        return practicedPerennialCropCycleDtos;
    }

    private void validatePhases(PracticedPerennialCropCycleDto practicedPerennialCropCycleDto,
                                Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                                Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
                                Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = practicedPerennialCropCycleDto.getCropCyclePhaseDtos();
        if (CollectionUtils.isEmpty(cropCyclePhaseDtos)) {
            addActionError("Un cycle de culture pérenne doit avoir une phase de pleine production");
        } else {
            validatePhasesTypes(cropCyclePhaseDtos);
            validatePhasesInterventions(
                    practicedPerennialCropCycleDto,
                    cropCyclePhaseDtos,
                    speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                    sectorByCodeEspceBotaniqueCodeQualifiant,
                    domainInputStockUnitByIds);
        }
    }

    private void validatePhasesTypes(List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos) {
        int instCount = 0, montCount = 0, pleiCount = 0, declCount = 0;
        for (PracticedCropCyclePhaseDto cropCyclePhaseDto : cropCyclePhaseDtos) {
            if (cropCyclePhaseDto.getType() != null) {
                switch (cropCyclePhaseDto.getType()) {
                    case INSTALLATION -> instCount++;
                    case MONTEE_EN_PRODUCTION -> montCount++;
                    case PLEINE_PRODUCTION -> pleiCount++;
                    case DECLIN_DE_PRODUCTION -> declCount++;
                    default -> {
                    }
                }
            }
        }

        if (instCount > 1) {
            addActionError("Un cycle de culture pérenne ne peut avoir qu'une seule phase d'installation");
        }
        if (montCount > 1) {
            addActionError("Un cycle de culture pérenne ne peut avoir qu'une seule phase de montée en production");
        }
        if (pleiCount == 0) {
            addActionError("Un cycle de culture pérenne doit avoir une phase de pleine production");
        } else if (pleiCount > 1) {
            addActionError("Un cycle de culture pérenne ne peut avoir qu'une seule phase de pleine production");
        }
        if (declCount > 1) {
            addActionError("Un cycle de culture pérenne ne peut avoir qu'une seule phase de déclin de production");
        }
    }

    private void validatePhasesInterventions(
            PracticedPerennialCropCycleDto practicedPerennialCropCycleDto,
            List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        for (PracticedCropCyclePhaseDto cropCyclePhaseDto : cropCyclePhaseDtos) {
                validatePerennialInterventions(cropCyclePhaseDto,
                        practicedPerennialCropCycleDto.getCroppingPlanEntryName(),
                        cropCyclePhaseDto.getType(),
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        domainInputStockUnitByIds);
        }
    }

    protected List<PracticedSeasonalCropCycleDto> validateSeasonalCropCycles(
            String practicedSystemTopiaId,
            List<String> requiredCropcycleCode,
            String practicedSeasonalCropCycleDtosJson,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = null;
        try {
            practicedSeasonalCropCycleDtos = convertPracticedSeasonalCropCycleDtosJson(practicedSeasonalCropCycleDtosJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("failed to load json values for practicedSeasonalCropCycleDtosJson", ex);
            }
            addActionError("Exception:" + ex + "\n/!\\ Désolé les données saisies depuis votre dernier enregistrement concernant vos cycles de cultures saisonniers n'ont pu être récupérées !");
        }

        if (practicedSeasonalCropCycleDtos == null) {
            practicedSeasonalCropCycleDtos = loadPersistedSeasonalCycles(practicedSystemTopiaId);
        } else if (!practicedSeasonalCropCycleDtos.isEmpty()) {
            for (PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto : practicedSeasonalCropCycleDtos) {

                Map<String, PracticedCropCycleNodeDto> nodesById = validateNodes(requiredCropcycleCode, practicedSeasonalCropCycleDto);

                if (nodesById == null) break;

                validateConnections(
                        practicedSeasonalCropCycleDto,
                        nodesById,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        domainInputStockUnitByIds);

            }
        }
        return practicedSeasonalCropCycleDtos;
    }

    private Map<String, PracticedCropCycleNodeDto> validateNodes(List<String> requiredCropcycleCode, PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto) {
        Map<String, PracticedCropCycleNodeDto> nodesById = new HashMap<>();
        Integer firstRank = null;
        // gestion des proportions de départ de noeud de rang 1
        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = practicedSeasonalCropCycleDto.getCropCycleNodeDtos();
        if (CollectionUtils.isNotEmpty(cropCycleNodeDtos)) {
            // verification de la somme des proportions
            List<PracticedCropCycleNodeDto> nullFrequencyNodes = new ArrayList<>();
            double sum = 0.0;
            for (PracticedCropCycleNodeDto cropCycleNodeDto : cropCycleNodeDtos) {
                nodesById.put(cropCycleNodeDto.getNodeId(), cropCycleNodeDto);
                firstRank= firstRank == null ? cropCycleNodeDto.getX() : (firstRank < cropCycleNodeDto.getX() ? firstRank : cropCycleNodeDto.getX());
                if (cropCycleNodeDto.getX() == 0) {
                    if (cropCycleNodeDto.getInitNodeFrequency() == null) {
                        nullFrequencyNodes.add(cropCycleNodeDto);
                    } else {
                        sum += cropCycleNodeDto.getInitNodeFrequency();
                    }
                }
                requiredCropcycleCode.add(cropCycleNodeDto.getCroppingPlanEntryCode());
            }
            if (firstRank == null || firstRank != 0) {
                addActionError("Le premier noeud doit se situer sur le premier rang");
            }

            if (sum > 100) {
                addActionError("La somme des fréquences initiales des cultures de rang 1 dépassent 100%");
                return null;
            }

            // affectation du pourcentage restant si nécéssaire
            // TODO: 06/02/17  devrait être fait côté service
            double remaining = 100.0 - sum;
            if (!nullFrequencyNodes.isEmpty() && remaining > 0) {
                int size = nullFrequencyNodes.size();
                for (PracticedCropCycleNodeDto connection : nullFrequencyNodes) {
                    double value = remaining / size;
                    value = Math.floor(value * 100.0) / 100.0;
                    connection.setInitNodeFrequency(value);
                }
                // XXX total may not be 100% here due to ceil
            }
        }
        return nodesById;
    }

    private void validateConnections(
            PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto,
            Map<String, PracticedCropCycleNodeDto> nodesById,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        // gestion des proportions des connections
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();
        if (CollectionUtils.isNotEmpty(cropCycleConnectionDtos)) {

            Multimap<String, PracticedCropCycleConnectionDto> connectionsBySource = HashMultimap.create();

            for (PracticedCropCycleConnectionDto cropCycleConnectionDto : cropCycleConnectionDtos) {

                PracticedCropCycleNodeDto source = nodesById.get(cropCycleConnectionDto.getSourceId());
                PracticedCropCycleNodeDto target = nodesById.get(cropCycleConnectionDto.getTargetId());

                validateNodeSourceTarget(source, target);

                validateSeasonalInterventions(
                        cropCycleConnectionDto, target.getLabel(), source.getLabel(),
                        target.getX() + 1,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        domainInputStockUnitByIds);

                connectionsBySource.put(cropCycleConnectionDto.getSourceId(), cropCycleConnectionDto);
            }

            validateConnectionFrequencies(connectionsBySource, nodesById);
        }
    }

    private void validateNodeSourceTarget(PracticedCropCycleNodeDto source, PracticedCropCycleNodeDto target) {
        if(source.getX() == target.getX()){
            addActionError("Deux noeuds reliés entre eux ne peuvent être sur le même rang");
        } else if (source.getX()>target.getX()) {
            if (!source.isEndCycle()) {
                addActionError("Un retour sur cycle ne peut-être créé qu'à partir d'un noeud fin de cycle");
            }
            if (target.getX() != 0) {
                addActionError("Un retour sur cycle ne peut se faire que sur le premier noeud du cycle");
            }
        }
    }

    protected void validateSeriesOfCampaigns(boolean areCurrentCampaignsAreValid, List<String> requiredCropCycleCodes) {
        if (growingSystemTopiaId != null && areCurrentCampaignsAreValid) {
            String campaigns = practicedSystem.getCampaigns();
            List<String> availableCroppingPlanCodes = getCropCodesForCampaigns();
            boolean allCropCycleFound = new HashSet<>(availableCroppingPlanCodes).containsAll(requiredCropCycleCodes);
            if (!allCropCycleFound) {
                if (StringUtils.isBlank(practicedSystemTopiaId)) {
                    addActionError("Cette série de campagne n'est pas valide pour ce système synthétisé car vous avez supprimé une campagne dont vous utilisez des éléments " +
                            "(ex. : les cultures, les matériels) dans le dessin de la rotation, la description des cultures pérennes ou les itinéraires techniques.");
                    addFieldError("practicedSystem.campaigns", "Série de campagnes agricoles %s non valide, précédente série %s !");
                } else {
                    Set<Integer> userSetCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(campaigns);
                    String removedCampaigns = UNKNOWN;
                    int nbCampaignRemoved = 0;

                    if (this.campaignsBeforeModification != null) {
                        Set<Integer> previousCampaigns = new LinkedHashSet<>(this.campaignsBeforeModification);
                        nbCampaignRemoved = previousCampaigns.size();
                        previousCampaigns.removeAll(userSetCampaigns);
                        removedCampaigns = CommonService.ARRANGE_CAMPAIGNS_SET.apply(previousCampaigns);
                    }

                    String pluralise = nbCampaignRemoved > 1 ? "les campagnes" : "la campagne";
                    String pluralise1 = nbCampaignRemoved > 1 ? "les campagnes supprimées sont :": "la campagne supprimée est :";
                    addActionError(
                            String.format("Cette série de campagne '%s' n'est pas valide pour ce système synthétisé car vous avez supprimé %s '%s' " +
                                            "dont vous utilisez des éléments " +
                                            "(ex. : les cultures, les matériels) dans le dessin de la rotation, la description des cultures pérennes ou les itinéraires techniques.",
                                    campaigns, pluralise, removedCampaigns));
                    addFieldError("practicedSystem.campaigns",
                            String.format("Série de campagnes agricoles '%s' non valide, %s '%s' !", campaigns, pluralise1, removedCampaigns));

                }
            }
        }
    }

    protected List<String> getCropCodesForCampaigns() {
        return practicedSystemService.getCropCodesFromGrowingSystemIdForCampaigns(growingSystemTopiaId, practicedSystem.getCampaigns());
    }

    protected void validateSeasonalInterventions(
            PracticedCropCycleConnectionDto cropCycleConnectionDto,
            String cropLabel,
            String previousCropLabel,
            int cropRank,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        List<PracticedInterventionDto> interventionDtos = cropCycleConnectionDto.getInterventions();
        String interventionGeneralInfos = String.format(
                "sur la culture assolée '%s' précedent '%s' (Rang %d)", Strings.nullToEmpty(cropLabel), Strings.nullToEmpty(previousCropLabel), cropRank);
        validateInterventions(
                interventionDtos,
                interventionGeneralInfos,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                domainInputStockUnitByIds);
    }

    protected void validatePerennialInterventions(
            PracticedCropCyclePhaseDto cropCyclePhaseDto,
            String cropLabel,
            CropCyclePhaseType phaseType,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        List<PracticedInterventionDto> interventionDtos = cropCyclePhaseDto.getInterventions();
        String interventionGeneralInfos = String.format(
                "sur la culture pérenne '%s' (%s)", Strings.nullToEmpty(cropLabel), phaseType != null ? AgrosystI18nService.getEnumTraductionWithDefaultLocale(phaseType) : "");
        validateInterventions(
                interventionDtos,
                interventionGeneralInfos,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                domainInputStockUnitByIds);
    }

    protected void validateInterventions(
            List<PracticedInterventionDto> interventionDtos,
            String interventionContextInfo,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEsepceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {

        // TODO dCossé: Add translation
        if (CollectionUtils.isNotEmpty(interventionDtos)) {
            List<String> availableToolsCouplingCodes;
            String campaigns = practicedSystem.getCampaigns();
            boolean isToolsCouplingsCodeLoadable = false;
            if (StringUtils.isNotBlank(growingSystemTopiaId)) {
                if (StringUtils.isNotBlank(campaigns)) {
                    boolean validCampaigns = CommonService.getInstance().areCampaignsValids(campaigns);
                    if (validCampaigns) {
                        isToolsCouplingsCodeLoadable = true;
                    }
                }
            }
            if (isToolsCouplingsCodeLoadable) {
                availableToolsCouplingCodes = practicedSystemService.getToolsCouplingsFromGrowingSystemAndCampaigns(growingSystemTopiaId, campaigns);
            } else {
                availableToolsCouplingCodes = Lists.newArrayListWithCapacity(0);
            }

            for (PracticedInterventionDto interventionDto : interventionDtos) {
                List<String> interventionErrorMessages = new ArrayList<>();

                if (interventionDto.getToolsCouplingCodes() != null) {
                    if (!new HashSet<>(availableToolsCouplingCodes).containsAll(interventionDto.getToolsCouplingCodes())) {
                        interventionErrorMessages.add(String.format("- Les combinaisons d'outils sélectionnées n'existent pas pour la série de campagnes agricoles '%s' ", campaigns));
                    }
                }

                if (Strings.isNullOrEmpty(interventionDto.getName())) {
                    interventionErrorMessages.add("- Renseigner le nom de l'intervention");
                }
                if (interventionDto.getType() == null) {
                    interventionErrorMessages.add("- Renseigner le type de l'intervention");
                }

                if (Strings.isNullOrEmpty(interventionDto.getStartingPeriodDate())) {
                    interventionErrorMessages.add("- Renseigner la date de début de l'intervention");
                }
                if (Strings.isNullOrEmpty(interventionDto.getEndingPeriodDate())) {
                    interventionErrorMessages.add("- Renseigner la date de fin de l'intervention");
                }

                Collection<AbstractActionDto> actionDtos = interventionDto.getActionDtos();

                final Collection<String> actionDtoValidationResult = actionService.validActionDtosAndUsages(
                        actionDtos,
                        domainInputStockUnitByIds,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEsepceBotaniqueCodeQualifiant,
                        interventionDto.getSpeciesStadesDtos(),
                        interventionDto.isIntermediateCrop()
                );

                interventionErrorMessages.addAll(actionDtoValidationResult);

                if (CollectionUtils.isNotEmpty(actionDtoValidationResult)) {

                    String interventionName = StringUtils.isBlank( interventionDto.getName()) ? "?" :  interventionDto.getName();
                    String interventionGeneralInfos = String.format("Intervention '%s' %s&nbsp:<br>", interventionName, interventionContextInfo);
                    String messages = StringUtils.join(interventionErrorMessages, "<br>");
                    addActionError(interventionGeneralInfos + messages);

                } else if (!interventionErrorMessages.isEmpty()) {
                    String messages = StringUtils.join(interventionErrorMessages, "<br>");
                    addActionError(messages);
                }
            }
        }
    }

    private void validateConnectionFrequencies(Multimap<String, PracticedCropCycleConnectionDto> connectionsBySource, Map<String, PracticedCropCycleNodeDto> nodesById) {
        for (String source : connectionsBySource.keySet()) {
            Collection<PracticedCropCycleConnectionDto> connections = connectionsBySource.get(source);
            List<PracticedCropCycleConnectionDto> noFrequencyConnections =
                    connections.stream()
                            .filter(practicedCropCycleConnectionDto -> practicedCropCycleConnectionDto.getCroppingPlanEntryFrequency() == null)
                            .toList();

            double sum = 0d;
            for (PracticedCropCycleConnectionDto connection : connections) {
                Double freq = connection.getCroppingPlanEntryFrequency();
                if (freq != null) {
                    sum += freq;
                }
            }

            double remaining = 100.0 - sum;
            if (!noFrequencyConnections.isEmpty() && remaining > 0) {
                int size = noFrequencyConnections.size();
                double totalValue = 0d;
                double value = remaining / size;
                value = Math.floor(value * 100.0) / 100.0;
                for (int i = 0; i <= size-2; i++) {
                    PracticedCropCycleConnectionDto connection = noFrequencyConnections.get(i);
                    connection.setCroppingPlanEntryFrequency(value);
                    totalValue += value;
                }
                PracticedCropCycleConnectionDto lastConnection = noFrequencyConnections.get(size-1);
                lastConnection.setCroppingPlanEntryFrequency(100.0 - totalValue);
                remaining = 0;
            }

            if (remaining > 1 || remaining < -1) {// 1% tolerance (99 - 101)
                PracticedCropCycleNodeDto sourceNode = nodesById.get(source);
                addActionError("La somme des fréquences des connexions sortantes pour la culture '" + sourceNode.getLabel() + "'" +
                        " doit être égale à 100%");
            }
        }

    }

    @Override
    public String execute() throws Exception {
        PracticedSystem practicedSystem = getPracticedSystem();
        // can define domain only during create action
        if (StringUtils.isBlank(practicedSystem.getTopiaId())) {
            GrowingSystem growingSystem = getGrowingSystemForNewPracticedSystem();
            getPracticedSystem().setGrowingSystem(growingSystem);
        }

        removeEmptySeasonalCropCycle();

        if (practicedSystemService.isConnectionMissingInCropCycles(practicedSeasonalCropCycleDtos)) {
            notificationSupport.warning("Une ou plusieurs connexions ont été créées à l'enregistrement pour boucler votre rotation.");
        }

        practicedSystem = practicedSystemService.createOrUpdatePracticedSystem(
                practicedSystem,
                practicedPerennialCropCycleDtos,
                practicedSeasonalCropCycleDtos
        );

        setPracticedSystemTopiaId(practicedSystem.getTopiaId());
        notificationSupport.practicedSystemSaved(practicedSystem);

        return SUCCESS;
    }

    protected GrowingSystem getGrowingSystemForNewPracticedSystem() {
        return growingSystemService.getGrowingSystem(growingSystemTopiaId);
    }

    /**
     * The last seasonal crop cycle can be empty has one is always created if it's case then it is removed.
     */
    protected void removeEmptySeasonalCropCycle() {
        if (practicedSeasonalCropCycleDtos != null && !practicedSeasonalCropCycleDtos.isEmpty()) {
            List<PracticedSeasonalCropCycleDto> pseccCopy = Lists.newArrayList(practicedSeasonalCropCycleDtos);
            for (PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto : pseccCopy) {
                if (practicedSeasonalCropCycleDto.getCropCycleNodeDtos() == null || practicedSeasonalCropCycleDto.getCropCycleNodeDtos().isEmpty()) {
                    practicedSeasonalCropCycleDtos.remove(practicedSeasonalCropCycleDto);
                }
            }
        }
    }

    protected List<PracticedPerennialCropCycleDto> convertPracticedPerennialCropCycleDtosJson(String json) {
        Type type = new TypeToken<List<PracticedPerennialCropCycleDto>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }

    protected List<PracticedSeasonalCropCycleDto> convertPracticedSeasonalCropCycleDtosJson(String json) {
        Type type = new TypeToken<List<PracticedSeasonalCropCycleDto>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }

    public Map<PracticedSystemSource, String> getSources() {
        return getEnumAsMap(PracticedSystemSource.values());
    }

    
    public String getCampaigns() {
        String result = "";
        if (practicedSystem != null) {
            result = practicedSystem.getCampaigns();
        }
        return getGson().toJson(result);
    }
    
    // used to include action jsp
    public Map<AgrosystInterventionType, String> getAgrosystInterventionTypes() {
        return referentialService.getAgrosystInterventionTypeTranslationMap();
    }

}
