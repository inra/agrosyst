package fr.inra.agrosyst.web.actions.growingsystems;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.Set;

/**
 * Action de recuperation de la liste des ids des systèmes de culture (AJAX).
 * 
 * @author Kevin Morin
 */
public class GrowingSystemIdsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(GrowingSystemIdsJson.class);
    @Serial
    private static final long serialVersionUID = -7016435760281542630L;
    
    protected transient GrowingSystemService growingSystemService;

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    protected boolean fromNavigationContextChoose = false;
    protected String filter;
    protected Set<Integer> selectedCampaigns = Sets.newHashSet();
    protected Set<String> selectedNetworks = Sets.newHashSet();
    protected Set<String> selectedDomains = Sets.newHashSet();
    protected Set<String> selectedGrowingPlans = Sets.newHashSet();

    public void setFromNavigationContextChoose(boolean fromNavigationContextChoose) {
        this.fromNavigationContextChoose = fromNavigationContextChoose;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setSelectedCampaigns(Set<Integer> selectedCampaigns) {
        this.selectedCampaigns = selectedCampaigns;
    }

    public void setSelectedNetworks(Set<String> selectedNetworks) {
        this.selectedNetworks = selectedNetworks;
    }

    public void setSelectedDomains(Set<String> selectedDomains) {
        this.selectedDomains = selectedDomains;
    }

    public void setSelectedGrowingPlans(Set<String> selectedGrowingPlans) {
        this.selectedGrowingPlans = selectedGrowingPlans;
    }

    @Override
    public String execute() {
        try {
            GrowingSystemFilter growingSystemFilter = getGson().fromJson(filter, GrowingSystemFilter.class);
            NavigationContext navigationContext;
            if (fromNavigationContextChoose) {
                navigationContext = new NavigationContext(selectedCampaigns, selectedNetworks,selectedDomains, selectedGrowingPlans, null);
            } else {
                navigationContext = getNavigationContext();
            }
            growingSystemFilter.setNavigationContext(navigationContext);

            jsonData = growingSystemService.getFilteredGrowingSystemIds(growingSystemFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load growing systen ids", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }
}
