package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;

import java.io.Serial;
import java.util.List;

/**
 * Export selected domains into XLSX file.
 *
 * @author Eric Chatellier
 */
public class DomainsExport extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = -1754426647907130114L;

    protected transient DomainService domainService;

    protected List<String> domainIds;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setDomainIds(String domainIds) {
        this.domainIds = getGson().fromJson(domainIds, List.class);
    }

    @Override
    protected ExportResult computeExportResult() {
        ExportResult result = domainService.exportDomainAsXls(domainIds);
        return result;
    }
}
