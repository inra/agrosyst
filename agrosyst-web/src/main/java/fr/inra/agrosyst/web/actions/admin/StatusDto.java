package fr.inra.agrosyst.web.actions.admin;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class StatusDto {

    protected String version;

    protected String revision;

    protected String buildDate;

    protected String encoding;

    protected String jvmName;

    protected String javaVersion;

    protected String memoryAllocated;

    protected String memoryUsed;

    protected String memoryFree;

    protected String memoryMax;

    protected double loadAverage;

    protected int availableProcessors;

    protected String runningSince;

    protected String uptime;

    protected long duration;

    protected String currentDate;

    protected String currentTimeZone;

    protected long computedPermissions = -1;

    protected int activeSessions = -1;

    protected int connectedUsers = -1;

    protected int runningTasks = -1;

    protected int pendingTasks = -1;

    protected String instance;

    protected String jmsPing;

    protected int threadCount;

    protected List<String> threadNames;

}
