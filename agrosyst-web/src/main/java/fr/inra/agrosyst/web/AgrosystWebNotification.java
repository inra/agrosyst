package fr.inra.agrosyst.web;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

public class AgrosystWebNotification implements Serializable {

    @Serial
    private static final long serialVersionUID = 1650671999712757336L;

    protected String text;

    public AgrosystWebNotification(String text) {
        this.text = text;
    }

    public static AgrosystWebNotification of(String text, Object... args) {
        return new AgrosystWebNotification(String.format(text, args));
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgrosystWebNotification that = (AgrosystWebNotification) o;

        return Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        return result;
    }

    @Override
    public String toString() {
        return "AgrosystWebNotification{" +
                "text='" + text + '\'' +
                '}';
    }

}
