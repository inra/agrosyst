package fr.inra.agrosyst.web.actions.ipmworks.domains;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.FrostProtectionType;
import fr.inra.agrosyst.api.entities.HosesPositionning;
import fr.inra.agrosyst.api.entities.IrrigationSystemType;
import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.PompEngineType;
import fr.inra.agrosyst.api.entities.SolWaterPh;
import fr.inra.agrosyst.api.entities.Stoniness;
import fr.inra.agrosyst.api.entities.WaterFlowDistance;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DomainsEdit extends fr.inra.agrosyst.web.actions.domains.DomainsEdit {

    private static final Log LOGGER = LogFactory.getLog(DomainsEdit.class);

    protected transient GrowingPlanService growingPlanService;
    protected transient PracticedPlotService practicedPlotService;

    /** Liste de toutes les sol texture geppa disponibles. */
    protected List<RefSolTextureGeppa> solTextures;

    /** Liste de toutes les sol profondeur disponibles. */
    protected List<RefSolProfondeurIndigo> solProfondeurs;

    /** Caracteristiques des sols. */
    protected List<RefSolCaracteristiqueIndigo> solCaracteristiques;

    /** Referentiel des elements de voisinage. */
    protected List<RefElementVoisinage> adjacentElements;

    /** Element de voisinage sélectionnés. */
    protected List<String> adjacentElementIds;

    protected List<PracticedPlot> practicedPlot;

    @Override
    protected void initForInput() {
        super.initForInput();
        solTextures = referentialService.getAllNotFrenchActiveSolTextures();
        solProfondeurs = referentialService.getAllActiveSolProfondeurs();
        solCaracteristiques = referentialService.getAllActiveSolCaracteristiques();
        adjacentElements = referentialService.getAllActiveElementVoisinages();

        if (domain.isPracticedPlotNotEmpty()) {
            domain.getPracticedPlot().forEach(practicedPlot -> {
                solTextures.stream()
                        .filter(texture -> texture.getTopiaId().equals(practicedPlot.getSurfaceTexture().getTopiaId()))
                        .findFirst()
                        .ifPresent(practicedPlot::setSurfaceTexture);
                solProfondeurs.stream()
                        .filter(profondeur -> profondeur.getTopiaId().equals(practicedPlot.getSolDepth().getTopiaId()))
                        .findFirst()
                        .ifPresent(practicedPlot::setSolDepth);
            });
        }
    }

    @Override
    @Action(results = {
            @Result(type = "redirectAction", params = {"actionName", "domains-edit-input", "domainTopiaId", "${domain.topiaId}"})})
    public String execute() throws Exception {
        String result = super.execute();

        if (SUCCESS.equals(result)) {
            growingPlanService.getOrCreateDefaultGrowingPlanForDomain(domain);
        }

        return result;
    }

    @Override
    protected void createOrUpdateDomain() {
        domain = domainService.createOrUpdateDomainWithoutCommit(
                domain,
                locationTopiaId,
                legalStatusId,
                geoPoints,
                croppingPlans,
                otex18,
                otex70,
                grounds,
                equipments,
                toolsCouplings,
                livestockUnits,
                inputStockUnits,
                harvestingPriceDtos
        );
        domain = domainService.setPracticedPlotsForDomain(domain, practicedPlot);
        domainService.validateAndCommit(domain.getTopiaId());
    }

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setPracticedPlotService(PracticedPlotService practicedPlotService) {
        this.practicedPlotService = practicedPlotService;
    }

    public Map<MaxSlope, String> getMaxSlopes() {
        return getEnumAsMap(MaxSlope.values());
    }

    public Map<WaterFlowDistance, String> getWaterFlowDistances() {
        return getEnumAsMap(WaterFlowDistance.values());
    }

    public void setLocationTopiaId(String locationTopiaId) {
        this.locationTopiaId = locationTopiaId;
    }

    public String getLocationTopiaId() {
        return locationTopiaId;
    }

    protected record StoninessDTO(double value, String label) {
        public double getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }

    public Map<Stoniness, StoninessDTO> getSolStoninesses() {
        Map<Stoniness, String> translationMap = i18nService.getEnumTranslationMap(Stoniness.class);
        Map<Stoniness, StoninessDTO> result = new LinkedHashMap<>();
        translationMap.forEach((key, value) -> result.put(key, new StoninessDTO(key.getStoninessValue(), value)));
        return result;
    }

    public Map<IrrigationSystemType, String> getIrrigationSystemTypes() {
        return getEnumAsMap(IrrigationSystemType.values());
    }

    public Map<PompEngineType, String> getPompEngineTypes() {
        return getEnumAsMap(PompEngineType.values());
    }

    public Map<HosesPositionning, String> getHosesPositionnings() {
        return getEnumAsMap(HosesPositionning.values());
    }

    public Map<FrostProtectionType, String> getFrostProtectionTypes() {
        return getEnumAsMap(FrostProtectionType.values());
    }

    public Map<SolWaterPh, String> getSolWaterPhs() {
        return getEnumAsMap(SolWaterPh.values());
    }

    public List<RefSolTextureGeppa> getSolTextures() {
        return solTextures;
    }

    public List<RefSolProfondeurIndigo> getSolProfondeurs() {
        return solProfondeurs;
    }

    public List<RefSolCaracteristiqueIndigo> getSolCaracteristiques() {
        return solCaracteristiques;
    }

    public Map<ZoneType, String> getZoneTypes() {
        return getEnumAsMap(ZoneType.values());
    }

    public List<RefElementVoisinage> getAdjacentElements() {
        return adjacentElements;
    }

    public List<String> getAdjacentElementIds() {
        return adjacentElementIds;
    }

    public void setAdjacentElementIds(List<String> adjacentElementIds) {
        this.adjacentElementIds = adjacentElementIds;
    }

    public void setPracticedPlot(String practicedPlot) {
        Type type = new TypeToken<List<PracticedPlot>>() {
        }.getType();
        this.practicedPlot =  getGson().fromJson(practicedPlot, type);
    }
}
