package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class LoadRefInterventionAgrosystTravailEdisJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadRefInterventionAgrosystTravailEdisJson.class);
    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 5842359927618631165L;

    protected transient ReferentialService referentialService;

    protected transient AgrosystInterventionType agrosystInterventionType;
    
    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    @Override
    public String execute() {
        try {
            if (agrosystInterventionType == null) {
                jsonData = referentialService.getAllActiveAgrosystActions();
            } else {
                jsonData = referentialService.getAllActiveAgrosystActions(agrosystInterventionType);
            }
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load RefInterventionAgrosystTravailEdis", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public void setAgrosystInterventionType(AgrosystInterventionType agrosystInterventionType) {
        this.agrosystInterventionType = agrosystInterventionType;
    }
}
