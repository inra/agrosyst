package fr.inra.agrosyst.web.actions.generic;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.HashedValue;
import fr.inra.agrosyst.api.services.generic.GenericFilter;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class GenericEntitiesList extends AbstractAdminAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(GenericEntitiesList.class);

    @Serial
    private static final long serialVersionUID = -2256628821672426679L;

    protected String genericClassName;

    protected List<String> properties;

    protected String entitiesJson;

    protected boolean importFileError;

    public void setImportFileError(boolean importFileError) {
        this.importFileError = importFileError;
    }

    public void setGenericClassName(String genericClassName) {
        this.genericClassName = genericClassName;
    }

    @Override
    public void prepare() throws Exception {
        super.prepare();

        if (importFileError) {
            notificationSupport.error("ÉCHEC : %s", "format de fichier incorrect");
            importFileError = false;
        }

        try {
            Class<?> klass = Class.forName(genericClassName);
            properties = service.getPropertiesFromString(genericClassName);

            GenericFilter filter = new GenericFilter();
            filter.setPageSize(getListNbElementByPage(klass));

            PaginationResult<?> entities = service.listEntities(klass, filter);

            if (klass.isEnum()) {
                properties.add("Traduction");
                for (Object entity : entities.getElements()) {
                    Map<String, String> entityMap = (Map<String, String>) entity;
                    String name = entityMap.get("Valeur");
                    String traduction = getText(klass.getName() + "." + name);
                    entityMap.put("Traduction", traduction);
                }
            }

            entitiesJson = getGson().toJson(entities);
        } catch (ClassNotFoundException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Un exception occured", e);
            }
            properties = new ArrayList<>();
        }
    }

    @Override
    public String execute() throws Exception {
        checkIsAdmin();
        return SUCCESS;
    }

    public String getGenericClassName() {
        return genericClassName;
    }

    public String getEntitiesJson() {
        return entitiesJson;
    }

    public List<String> getProperties() {
        List<String> copy = new ArrayList<>();
        if (properties != null) {
            copy.addAll(properties);
            copy.remove("active"); // active will be managed separately
            copy.remove("refCountry");
        }
        return copy;
    }

    public int getPropertiesSize() {
        int result = 0;
        if (properties != null) {
            result = properties.size();
        }
        result++; // column active
        return result;
    }

    public boolean isReferential() {
        boolean result = genericClassName.startsWith("fr.inra.agrosyst.api.entities.referential.Ref") ||
                genericClassName.startsWith("fr.inra.agrosyst.api.entities.referential.TradRef") ||
                genericClassName.startsWith("fr.inra.agrosyst.api.entities.referential.refApi.Ref") ||
                genericClassName.startsWith("fr.inra.agrosyst.services.referential.csv.RefEspeceDto") ||
                genericClassName.startsWith("fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto");
        return result;
    }

    public boolean isTranslation() {
        return genericClassName.startsWith("fr.inra.agrosyst.api.entities.referential.TradRef");
    }

    public boolean isHashedValues() {
        boolean result = HashedValue.class.getName().equals(genericClassName);
        return result;
    }

    public boolean isReferentialOrHashedValues() {
        return isReferential() || isHashedValues();
    }

    public boolean isRefCountry() {
        return properties.contains("refCountry");
    }
}
