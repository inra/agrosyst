package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

/**
 * Creates the new version of a decision rule
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class CreateNewDecisionRuleVersion extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -7237584971917654739L;

    protected transient ManagementModeService managementModeService;

    protected String decisionRuleCode;

    protected String versionReason;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setDecisionRuleCode(String decisionRuleCode) {
        this.decisionRuleCode = decisionRuleCode;
    }

    public void setVersionReason(String versionReason) {
        this.versionReason = versionReason;
    }

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "decision-rules-edit-input", "decisionRuleCode", "${decisionRuleCode}"})})
    public String execute() throws Exception {
        Preconditions.checkState(!Strings.isNullOrEmpty(decisionRuleCode));
        DecisionRule created = managementModeService.createNewDecisionRuleVersion(decisionRuleCode, versionReason);
        notificationSupport.newDecisionRuleCreated(created);
        return SUCCESS;
    }

    public String getDecisionRuleCode() {
        return decisionRuleCode;
    }

}
