package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.edaplos.EdaplosService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;

/**
 * Import eDaplos xml file.
 * 
 * @author Eric Chatellier
 */
public class DomainsExportEdaplos extends AbstractAgrosystAction {

    private static final Log LOGGER = LogFactory.getLog(DomainsExportEdaplos.class);

    @Serial
    private static final long serialVersionUID = 1928633755050334578L;

    protected transient EdaplosService edaplosService;

    protected String edaplosFileId;

    public void setEdaplosService(EdaplosService domainService) {
        this.edaplosService = domainService;
    }

    public void setEdaplosFileId(String edaplosFileId) {
        this.edaplosFileId = edaplosFileId;
    }

    @Override
    @Action(results= {@Result(type="stream", params={"contentType", "text/csv",
            "inputName", "inputStream", "contentDisposition", "attachment; filename=\"${filename}\""})})
    public String execute() throws Exception {
        return SUCCESS;
    }

    public InputStream getInputStream() {
        InputStream inputStream;
        try {
            inputStream = edaplosService.exportCSVEdaplosReport(edaplosFileId);
        } catch (Exception ex) {
            sendFeedback(ex);
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't generate csv", ex);
            }
            throw new AgrosystTechnicalException("Can't create input stream", ex);
        }
        return inputStream;
    }

    protected void sendFeedback(Exception ex) {
        try {
            edaplosService.sendExceptionFeedbackEmail(edaplosFileId, ex, "Export Edaplos");
        } catch (Exception e) {
            // noting to do
        }
    }

    public String getFilename() {
        return "edaplos-result-report.csv";
    }
}
