package fr.inra.agrosyst.web.actions.effective;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class LoadInputStockListJson extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = 1124998190258319281L;

    private static final Log LOGGER = LogFactory.getLog(LoadInputStockListJson.class);

    protected transient DomainService domainService;

    protected transient String domainId;
    
    protected transient String growingSystemId;

    protected transient InputType inputType;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setDomainId(String domainId) {
        if (domainId != null && !domainId.equals("undefined") && !domainId.equals("null")) {
            this.domainId = domainId;
        }
    }
    
    public void setGrowingSystemId(String growingSystemId) {
        if (growingSystemId != null && !growingSystemId.equals("undefined") && !growingSystemId.equals("null")) {
            this.growingSystemId = growingSystemId;
        }
    }

    public void setInputType(InputType inputType) {
        if (inputType != null) {
            this.inputType = inputType;
        }
    }

    @Override
    public String execute() {
        try {

            jsonData = domainService.getDomainInputDtoForType(domainId, growingSystemId, inputType);

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load domains list for type", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }

}
