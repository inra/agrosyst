package fr.inra.agrosyst.web.filters;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2016 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.sitemesh.webapp.SiteMeshFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SiteMeshMonitorFilter extends SiteMeshFilter {

    private static final Log LOGGER = LogFactory.getLog(SiteMeshMonitorFilter.class);

    public static final String ACTION_MONITOR_TEXT = "Action %s %s took %d ms [%d]";

    public void doFilter(ServletRequest rq, ServletResponse rs, FilterChain chain) throws IOException, ServletException {
        // Start monitoring
        long start = System.currentTimeMillis();

        // Execute action
        super.doFilter(rq, rs, chain);

        // Compute execution time
        if (LOGGER.isTraceEnabled()) {
            long end = System.currentTimeMillis();
            HttpServletRequest httpServletRequest = (HttpServletRequest) rq;
            HttpServletResponse httpServletResponse = (HttpServletResponse)rs;
            String requested = httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length());
            int status = -1;
            try {
                Method getStatus = httpServletResponse.getClass().getMethod("getStatus");
                status = (Integer) getStatus.invoke(httpServletResponse);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(e);
                }
            }
            LOGGER.trace(String.format(ACTION_MONITOR_TEXT, httpServletRequest.getMethod(), requested , end - start, status));
        }
    }
}
