package fr.inra.agrosyst.web.actions.growingplans;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanFilter;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.List;
import java.util.Map;

/**
 * Action d'affichage de la liste paginée des dispositifs.
 * 
 * @author Eric Chatellier
 */
public class GrowingPlansList extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -430109839071419528L;

    protected transient GrowingPlanService growingPlanService;
    
    protected transient DomainService domainService;

    /** Init growing plans. */
    protected PaginationResult<GrowingPlanDto> growingPlans;

    /** Domain list (for duplicate option). */
    protected List<Domain> domains;

    protected GrowingPlanFilter growingPlanFilter;

    protected int exportAsyncThreshold = 5;

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    @Override
    public String execute() throws Exception {

        NavigationContext navigationContext = getNavigationContext();
        
        // get init growing plans
        growingPlanFilter = new GrowingPlanFilter();
        growingPlanFilter.setNavigationContext(navigationContext);
        growingPlanFilter.setPageSize(getListNbElementByPage(GrowingPlanDto.class));
        growingPlanFilter.setActive(Boolean.TRUE);
        growingPlans = growingPlanService.getFilteredGrowingPlansDto(growingPlanFilter);

        this.exportAsyncThreshold = this.config.getGrowingPlansExportAsyncThreshold();
        return SUCCESS;
    }

    public PaginationResult<GrowingPlanDto> getGrowingPlans() {
        return growingPlans;
    }

    public List<Domain> getDomains() {
        return domains;
    }

    public Map<TypeDEPHY, String> getTypes() {
        return getEnumAsMap(TypeDEPHY.DEPHY_FERME, TypeDEPHY.DEPHY_EXPE, TypeDEPHY.NOT_DEPHY);
    }

    public GrowingPlanFilter getGrowingPlanFilter() {
        return growingPlanFilter;
    }

    public int getExportAsyncThreshold() {
        return exportAsyncThreshold;
    }
}
