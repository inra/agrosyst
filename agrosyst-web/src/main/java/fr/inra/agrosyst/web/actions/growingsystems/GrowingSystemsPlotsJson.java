package fr.inra.agrosyst.web.actions.growingsystems;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Recupere la liste des parcelles "libre" à la sélection d'un dispositif lors de la creation d'un
 * système de culture.
 * 
 * @author cosse
 */
public class GrowingSystemsPlotsJson extends AbstractJsonAction  {

    @Serial
    private static final long serialVersionUID = 7784391389093373240L;

    private static final Log LOGGER = LogFactory.getLog(GrowingSystemsPlotsJson.class);

    protected transient PlotService plotService;
    
    protected transient String growingPlanTopiaId;

    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    public void setGrowingPlanTopiaId(String growingPlanTopiaId) {
        this.growingPlanTopiaId = growingPlanTopiaId;
    }

    @Override
    public String execute() throws Exception {
        try {
            jsonData = plotService.getFreePlotForGrowingPlan(growingPlanTopiaId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("failed to load plots for growingPlanTopiaId: '%s':",
                        growingPlanTopiaId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
