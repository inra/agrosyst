package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Created by davidcosse on 01/07/14.
 */
public class LoadEffectiveZonesForCopyJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadEffectiveZonesForCopyJson.class);
    @Serial
    private static final long serialVersionUID = -2610482141000789371L;
    
    protected transient String zoneId;

    protected transient EffectiveCropCycleService effectiveCropCycleService;
    
    @Override
    public String execute() throws Exception {
        try {
            jsonData = effectiveCropCycleService.getAvailableZonesForCopy(zoneId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load available zones to copy for zoneId '%s'", zoneId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public EffectiveCropCycleService getEffectiveCropCycleService() {
        return effectiveCropCycleService;
    }

    public void setEffectiveCropCycleService(EffectiveCropCycleService effectiveCropCycleService) {
        this.effectiveCropCycleService = effectiveCropCycleService;
    }
}
