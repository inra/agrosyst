package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeImpl;
import fr.inra.agrosyst.api.entities.managementmode.Section;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.services.common.HistoryItem;
import fr.inra.agrosyst.api.services.common.HistoryType;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModes;
import fr.inra.agrosyst.api.services.managementmode.SectionDto;
import fr.inra.agrosyst.api.services.managementmode.StrategyDto;
import fr.inra.agrosyst.api.services.referential.GroupeCibleDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static org.nuiton.i18n.I18n.t;

/**
 * Management mode edit action.
 * 
 * @author Eric Chatellier
 */
public class ManagementModesEdit extends AbstractAgrosystAction implements Preparable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -2688592598121764478L;

    private static final Log LOGGER = LogFactory.getLog(ManagementModesEdit.class);

    protected static final String DATE_FORMAT = "dd/MM/yyyy";

    protected static final int HISTORY_SIZE_LIMIT = 100;

    protected transient ManagementModeService managementModeService;

    protected transient GrowingSystemService growingSystemService;

    protected transient ReferentialService referentialService;

    /** Management mode id to edit. */
    protected String managementModeTopiaId;

    /** Management mode entity to edit. */
    protected ManagementMode managementMode;

    /** Growing system list (only for edit mode). */
    protected List<GrowingSystem> growingSystems;

    protected Sector growingSystemsSector;

    /** Domain id (of selected growing system). */
    protected String domainTopiaId;

    /** Growing system id (edit mode). */
    protected String growingSystemTopiaId;

    /** Current management mode sections. */
    protected List<SectionDto> sections;
    protected String sectionsJson;

    protected List<GroupeCibleDTO> groupesCibles;

    /** Liste des cultures associées au système de culture sélectionné. */
    protected List<CroppingPlanEntry> croppingPlanEntries;

    /** Liste des regles associés au système de culture sélectionné. */
    protected Collection<DecisionRule> decisionRules;

    protected List<ManagementMode> relatedManagementModes;

    protected List<ManagementModeCategory> availableManagementModeCategories;

    protected List<String> histories = new ArrayList<>();

    protected ManagementModeCategory managementModeCategory;

    protected TypeDEPHY typeDEPHY;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public ManagementMode getManagementMode() {
        // EChatellier 27/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(managementMode, ManagementModeImpl::new);
    }
    
    @Override
    public void prepare() {
        if (StringUtils.isEmpty(managementModeTopiaId)) {
            // Cas de création d'un growingSystem
            managementMode = managementModeService.newManagementMode();
        } else {
            // Cas d'une mise à jour de growingSystem
            managementMode = managementModeService.getManagementMode(managementModeTopiaId);
        }

        if (managementMode.isPersisted()) {
            GrowingSystem growingSystem = managementMode.getGrowingSystem();
            activated = managementMode.isActive() && growingSystem.isActive() && growingSystem.getGrowingPlan().isActive() && growingSystem.getGrowingPlan().getDomain().isActive();
            typeDEPHY = growingSystem.getGrowingPlan().getType();
        }
    }
    
    /**
     * Initialisation de certaines variables pour le premier appel de la page.
     */
    @Override
    @Action("management-modes-edit-input")
    public String input() {

        if (!Strings.isNullOrEmpty(managementModeTopiaId)) {
            authorizationService.checkManagementModeReadable(managementModeTopiaId);
            readOnly = !authorizationService.isManagementModeWritable(managementModeTopiaId);
        }
        if (readOnly) {
            notificationSupport.managementModeNotWritable();
        }

        if (getManagementMode().getGrowingSystem() != null) {
            GrowingSystem growingSystem = managementMode.getGrowingSystem();

            growingSystemTopiaId = growingSystem.getTopiaId();

            typeDEPHY = growingSystem.getGrowingPlan().getType();

        }

        managementModeCategory = managementMode.getCategory();

        sections = getManagementModeSections(managementMode);

        initForInput();

        return INPUT;
    }

    protected List<SectionDto> getManagementModeSections(ManagementMode managementMode) {
        List<SectionDto> sections = null;
        if (managementMode.getSections() != null) {
            Collection<Section> mmSections = managementMode.getSections();
            sections = Lists.newArrayList(Collections2.transform(mmSections, ManagementModes.SECTION_TO_DTO::apply));
        }
        return sections;
    }

    /**
     * Initialisation des listes ou autres données à chaque affichage (premier/erreurs).
     */
    @Override
    protected void initForInput() {
        
        // select combo box
        if (!getManagementMode().isPersisted()) {
            GrowingSystemFilter growingSystemFilter = new GrowingSystemFilter();
            NavigationContext navigationContext = getNavigationContext();
            growingSystemFilter.setNavigationContext(navigationContext);

            growingSystems = managementModeService.getGrowingSystemsForManagementMode(navigationContext);

            histories = new ArrayList<>();
        } else {
            translateHistorical();
        }

        if (StringUtils.isNotBlank(growingSystemTopiaId)) {
            GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
            growingSystemsSector = growingSystem.getSector();
            domainTopiaId = growingSystem.getGrowingPlan().getDomain().getTopiaId();

            croppingPlanEntries = managementModeService.getGrowingSystemCroppingPlanEntries(growingSystemTopiaId);
            decisionRules = managementModeService.getGrowingSystemDecisionRules(growingSystemTopiaId);
            availableManagementModeCategories = managementModeService.getAvailableManagementModeCategories(growingSystemTopiaId);
            relatedManagementModes = managementModeService.getRelatedManagementModes(managementMode.getGrowingSystem());
        } else {
            availableManagementModeCategories = Lists.newArrayList(ManagementModeCategory.values());
        }

        groupesCibles = referentialService.getGroupesCibles();
    }

    protected void translateHistorical() {
        String historical = managementMode.getHistorical();

        if(!Strings.isNullOrEmpty(historical)) {
            Type type = new TypeToken<List<HistoryItem>>() {}.getType();
            List<HistoryItem> items = getGson().fromJson(historical, type);
            int limit = Math.min(items.size(), getHistorySizeLimit());

            histories = Lists.newArrayListWithCapacity(limit);
            DateTimeFormatter simpleFormat = DateTimeFormatter.ofPattern(DATE_FORMAT);

            for (HistoryItem item: Iterables.limit(Lists.reverse(items), limit)) {
                LocalDateTime date = item.getDate();
                String stDate = simpleFormat.format(date);
                List<String> args = item.getArgs();
                String fromCategory = args.getFirst();
                String toCategory = args.getFirst();
                String translatedFromCategories = getText(ManagementModeCategory.class.getName() + "." + fromCategory);
                String translatedToCategories = getText(ManagementModeCategory.class.getName() + "." + toCategory);

                HistoryType historyType = item.getType();

                String format = getText(HistoryType.class.getName() + "." + historyType.name());

                switch (historyType) {
                    case MANAGEMENT_MODE_ADD ->
                        // [toManagementModeCategory]
                            histories.add(String.format(format, stDate, translatedToCategories));
                    case MANAGEMENT_MODE_COPY, MANAGEMENT_MODE_EXTEND ->
                        // [fromManagementModeCategory, toManagementModeCategory]
                            histories.add(String.format(format, stDate, translatedToCategories, translatedFromCategories));
                    case MANAGEMENT_MODE_DUPLICATED -> {
                        // [fromManagementModeCategory, toManagementModeCategory]
                        String fromMMCampagne = args.get(2);
                        String toMMCampagne = args.get(3);
                        histories.add(String.format(format, stDate, translatedToCategories, toMMCampagne, translatedFromCategories, fromMMCampagne));
                    }
                    case SECTION_ADD, SECTION_REMOVE -> {
                        // [SectionType, BioAgressorType, RefBioAgressor]
                        String stSectionType = args.getFirst();
                        String translatedSectionType = getText(SectionType.class.getName() + "." + stSectionType);
                        String stBioAgressorType = args.get(1);
                        String translatedBioAgressorType = Strings.isNullOrEmpty(stBioAgressorType) ? "-" : getText(BioAgressorType.class.getName() + "." + stBioAgressorType);
                        //managementModeService.getBioAgressors()
                        String bioAgressorId = args.get(2);
                        String bioAgressorName = Strings.isNullOrEmpty(bioAgressorId) ? "-" : referentialService.getBioAgressor(bioAgressorId).getLabel();
                        histories.add(String.format(format, stDate, translatedSectionType, translatedBioAgressorType, bioAgressorName));
                    }
                    case STRATEGY_ADD, STRATEGY_REMOVE -> {
                        // [multiannual, Explanation, CroppingPlanEntryTopiaId]
                        String strategyTypeName = args.getFirst();
                        // index 1: explanation
                        String croppingPlanEntryName = "-";
                        String croppingPlanEntryIds = args.get(2);
                        if (StringUtils.isNotBlank(croppingPlanEntryIds)) {
                            String[] cropIds = croppingPlanEntryIds.split(ManagementModeService.HISTORY_CROP_IDS_SEPARATOR);
                            List<String> cropsNames = managementModeService.loadCropsNames(Sets.newHashSet(cropIds));
                            croppingPlanEntryName = StringUtils.join(cropsNames, " - ");
                        }
                        histories.add(String.format(format, stDate, strategyTypeName, croppingPlanEntryName));
                    }
                    case RULE_ADD, RULE_REMOVE -> {
                        // [TopiaId, VersionNumber]
                        String ruleName = "-";
                        String ruleId = args.getFirst();
                        if (StringUtils.isNotBlank(ruleId)) {
                            final Optional<DecisionRule> optionalDecisionRule = managementModeService.getDecisionRule(ruleId);
                            ruleName = optionalDecisionRule.isEmpty() ? t("warning.decisionRule.remove") : optionalDecisionRule.get().getName();
                        }
                        Integer ruleVersion = Integer.valueOf(args.get(1));
                        histories.add(String.format(format, stDate, ruleName, ruleVersion));
                    }
                    default -> {
                    }
                }
            }
        }
    }
    
    @Override
    public void validate() {

        // growing system mandatory only during edit mode
        ManagementMode managementMode = getManagementMode();
        if (!managementMode.isPersisted() && StringUtils.isBlank(growingSystemTopiaId)) {
            addFieldError("growingSystemTopiaId", "Le système de culture est obligatoire !");
        }

        if (managementMode.isPersisted() && !activated) {
            addActionError("Le modèle décisionnel et/ou son système de culture et/ou son dispositif et/ou son domain sont inactifs ");
        }

        growingSystemTopiaId = managementMode.isPersisted() ? managementMode.getGrowingSystem().getTopiaId() : growingSystemTopiaId;

        if (managementMode.getCategory()==null) {
            addFieldError("managementMode.category", "La catégorie est obligatoire !");
        }

        try {
            convertSectionsJson(sectionsJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load decision management modes", ex);
            }
            addActionError("Exception:" + ex + "\n/!\\ Désolé les données saisies depuis votre dernier enregistrement concernant les rubriques n'ont pu être récupérées !");
        }

        if (sections == null) {
            sections = new ArrayList<>();
        }

        List<String> cropIds = null;

        GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
        typeDEPHY = growingSystem.getGrowingPlan().getType();

        for (SectionDto section : sections) {
            if (section.getSectionType() == null) {
                addFieldError("editedSection.sectionType", "Le type de rubrique est obligatoire !");
                addActionError("Le type de rubrique est obligatoire !");
            } else {
                SectionType sectionType = section.getSectionType();

    
                if (TypeDEPHY.DEPHY_FERME.equals(typeDEPHY) && section.getCategoryObjective() == null) {
                    addFieldError("editedStrategy.categoryObjective", "Cette stratégie doit avoir le champ 'Caractérisation des objectifs agronomiques' de défini !");
                    addActionError("Les stratégies doivent avoir le champ 'Caractérisation des objectifs agronomiques' de défini !");
                }
    
                if (TypeDEPHY.DEPHY_FERME.equals(typeDEPHY) && (sectionType == SectionType.ADVENTICES || sectionType == SectionType.MALADIES || sectionType == SectionType.RAVAGEURS)) {
                    if (StringUtils.isEmpty(section.getBioAgressorTopiaId())) {
                        addFieldError("editedStrategy.bioAgressorTopiaId", "Cette rubrique doit avoir un bio-agresseur considéré de défini !");
                        addActionError("Les rubriques de type 'Maitrise des maladies', 'Maitrise des adventices' et 'Maitrise des ravageurs' doivent avoir un bio-agresseur considéré de défini !");
                    }
                }
        
                    List<StrategyDto> strategies = section.getStrategiesDto();
                if (strategies != null){
                    
                    if (cropIds == null) {
                        cropIds = growingSystemTopiaId == null ? new ArrayList<>() : managementModeService.getCropIdsForGrowingSystemId(growingSystemTopiaId);
                    }
                    
                    for (StrategyDto strategyDto : strategies) {
    
                        if (TypeDEPHY.DEPHY_FERME.equals(typeDEPHY) && CollectionUtils.isEmpty(strategyDto.getCropIds())) {
                            addActionError("Une culture est requise sur une stratégie !");
                        }
                        
                        if (CollectionUtils.isNotEmpty(strategyDto.getCropsTopiaIds())) {
                            if (!new HashSet<>(cropIds).containsAll(strategyDto.getCropsTopiaIds())){
                                addActionError("Des cultures non valides ont été détectées sur une stratégie !");
                            }
                        }
    
                        final RefStrategyLever refStrategyLever = strategyDto.getRefStrategyLever();
                        if(refStrategyLever == null) {
                            addActionError("Le levier n'est pas renseigné !");
                            addFieldError("editedStrategy.refStrategyLever","Le levier est obligatoire");
                        }
                        
                    }
                }
            }
        }

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : management mode:'%s' for gs '%s' -> %s", managementModeTopiaId, growingSystemTopiaId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : management mode:'%s' for gs '%s' -> %s", managementModeTopiaId, growingSystemTopiaId, getFieldErrors().toString()));
            }
            initForInput();
        }
    }
    
    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "management-modes-edit-input", "managementModeTopiaId", "${managementMode.topiaId}"})})
    public String execute() throws Exception {

        managementMode = managementModeService.createOrUpdateManagementMode(managementMode, growingSystemTopiaId, sections);
        if (Strings.isNullOrEmpty(managementModeTopiaId)){
            notificationSupport.newManagementModeCreated(managementMode);
        } else {
            notificationSupport.managementModeSaved(managementMode);
        }
        return SUCCESS;
    }

    public void convertSectionsJson(String json) {
        Type type = new TypeToken<List<SectionDto>>() {
        }.getType();
        this.sections = getGson().fromJson(json, type);
    }

    public String getManagementModeTopiaId() {
        return managementModeTopiaId;
    }

    public void setManagementModeTopiaId(String managementModeTopiaId) {
        this.managementModeTopiaId = managementModeTopiaId;
    }

    public String getGrowingSystemTopiaId() {
        return growingSystemTopiaId;
    }

    public void setGrowingSystemTopiaId(String growingSystemTopiaId) {
        this.growingSystemTopiaId = growingSystemTopiaId;
    }

    public String getDomainTopiaId() {
        return domainTopiaId;
    }

    public List<GrowingSystem> getGrowingSystems() {
        return growingSystems;
    }

    public List<SectionDto> getSections() {
        return sections;
    }

    public void setSections(String json) {
        sectionsJson = json;
    }

    public List<CroppingPlanEntry> getCroppingPlanEntries() {
        return croppingPlanEntries;
    }

    public Collection<DecisionRule> getDecisionRules() {
        return decisionRules;
    }

    public List<ManagementModeCategory> getAvailableManagementModeCategories() {
        return availableManagementModeCategories;
    }

    public List<ManagementMode> getRelatedManagementModes() {
        return relatedManagementModes;
    }

    public ManagementModeCategory[] getCategories(){
        return ManagementModeCategory.values();
    }

    public List<String> getHistories() {
        return histories;
    }

    public int getHistorySizeLimit() {
        return HISTORY_SIZE_LIMIT;
    }

    public ManagementModeCategory getManagementModeCategory() {
        return managementModeCategory;
    }

    public Sector getGrowingSystemsSector() {
        return growingSystemsSector;
    }

    public Map<SectionType, BioAgressorType[]> getFilteredBioAgressorTypes() {
        return ManagementModeService.bioAgressorTypeBySectionType();
    }

    public List<GroupeCibleDTO> getGroupesCibles() {
        return groupesCibles;
    }

    public TypeDEPHY getTypeDEPHY() {
        return typeDEPHY;
    }
}
