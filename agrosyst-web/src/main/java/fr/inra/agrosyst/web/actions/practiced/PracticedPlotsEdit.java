package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.BufferStrip;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.FrostProtectionType;
import fr.inra.agrosyst.api.entities.HosesPositionning;
import fr.inra.agrosyst.api.entities.IrrigationSystemType;
import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.PompEngineType;
import fr.inra.agrosyst.api.entities.SolWaterPh;
import fr.inra.agrosyst.api.entities.WaterFlowDistance;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.services.plot.Plots;
import fr.inra.agrosyst.api.services.plot.SolHorizonDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Action de création/edition d'une parcelle type.
 *
 * @author Eric Chatellier
 */
public class PracticedPlotsEdit extends AbstractAgrosystAction implements Preparable {

    @Serial
    private static final long serialVersionUID = -1618279547328450777L;

    private static final Log LOGGER = LogFactory.getLog(PracticedPlotsEdit.class);

    protected transient PracticedPlotService practicedPlotService;

    protected transient PracticedSystemService practicedSystemService;

    protected transient ReferentialService referentialService;

    /** Edited plot id. */
    protected String practicedPlotTopiaId;

    /** Edited plot instance. */
    protected PracticedPlot practicedPlot;

    /** Practiced system topia id. */
    protected String practicedSystemTopiaId;

    protected PracticedSystem practicedSystem;

    /** Practiced system list (create mode). */
    protected List<PracticedSystem> practicedSystems;

    /** Selected location id. */
    protected String locationTopiaId;

    /** Onglet "zonage", liste de tous les zonages. */
    protected List<RefParcelleZonageEDI> parcelleZonages;

    /** Identifiant selectionnées dans la liste {@code parcelleZonages}. */
    protected List<String> selectedPlotZoningIds;

    /** Liste de toutes les sol texture geppa disponibles. */
    protected List<RefSolTextureGeppa> solTextures; 

    /** Identifiant du surface texture sélectionné. */
    protected String selectedSurfaceTextureId;

    /** Identifiant du sous sol texture sélectionné. */
    protected String selectedSubSoilTextureId;

    /** Liste de toutes les sol profondeur disponibles. */
    protected List<RefSolProfondeurIndigo> solProfondeurs;

    /** Identifiant du sol profondeur sélectionné. */
    protected String selectedSolProfondeurId;

    /** Liste des sols horizons. */
    protected List<SolHorizonDto> solHorizons;
    protected String solHorizonsJson;

    /** Caracteristiques des sols. */
    protected List<RefSolCaracteristiqueIndigo> solCaracteristiques;

    /** Referentiel des elements de voisinage. */
    protected List<RefElementVoisinage> adjacentElements;
 
    /** Element de voisinage sélectionnés. */
    protected List<String> adjacentElementIds;

    public void setPracticedPlotService(PracticedPlotService practicedPlotService) {
        this.practicedPlotService = practicedPlotService;
    }

    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    @Override
    public void prepare() {
        practicedPlot = practicedPlotService.getPracticedPlot(practicedPlotTopiaId);

        if (practicedPlot.isPersisted()) {
            authorizationService.checkPracticedPlotReadable(practicedPlotTopiaId);

            this.activated = practicedSystemService.areActivated(practicedPlot.getPracticedSystem());

            readOnly = !authorizationService.isPracticedPlotWritable(practicedPlotTopiaId);
            if (readOnly) {
                notificationSupport.practicedPlotNotWritable();
            }
        }
    }

    @Override
    @Action("practiced-plots-edit-input")
    public String input() throws Exception {
        initForInput();

        if (practicedPlot.getLocation() != null) {
            locationTopiaId = practicedPlot.getLocation().getTopiaId();
        }
        if (practicedPlot.getPlotZonings() != null) {
            selectedPlotZoningIds = practicedPlot.getPlotZonings().stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        }
        if (practicedPlot.getSurfaceTexture() != null) {
            selectedSurfaceTextureId = practicedPlot.getSurfaceTexture().getTopiaId();
        }
        if (practicedPlot.getSubSoilTexture() != null) {
            selectedSubSoilTextureId = practicedPlot.getSubSoilTexture().getTopiaId();
        }
        if (practicedPlot.getSolDepth() != null) {
            selectedSolProfondeurId = practicedPlot.getSolDepth().getTopiaId();
        }
        if (practicedPlot.getSolHorizon() != null) {
            solHorizons = Lists.newArrayList(Collections2.transform(practicedPlot.getSolHorizon(), Plots.SOL_HORIZON_TO_DTO::apply));
        } else {
            solHorizons = new ArrayList<>();
        }
        if (practicedPlot.getAdjacentElements() != null) {
            adjacentElementIds = practicedPlot.getAdjacentElements().stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        } else {
            adjacentElementIds = new ArrayList<>();
        }

        return INPUT;
    }

    
    @Override
    protected void initForInput() {

        if (!practicedPlot.isPersisted()) {
            practicedSystems = practicedPlotService.getPracticedSystemsWithoutPracticedPlot(getNavigationContext());
        } else {
            practicedSystem = IterableUtils.first(practicedPlot.getPracticedSystem());
        }

        parcelleZonages = referentialService.getAllActiveParcelleZonage();
        solTextures = referentialService.getAllActiveSolTextures();
        solProfondeurs = referentialService.getAllActiveSolProfondeurs();
        solCaracteristiques = referentialService.getAllActiveSolCaracteristiques();
        adjacentElements = referentialService.getAllActiveElementVoisinages();
    }

    @Override
    public void validate() {
        // domain
        if (!practicedPlot.isPersisted() && StringUtils.isBlank(practicedSystemTopiaId)) {
            addActionError("Aucun système synthétisé n'est sélectionné !");
        }

        if (practicedPlot.isPersisted() && !practicedSystemService.areActivated(practicedPlot.getPracticedSystem())) {
            addActionError("Le système synthétisé est désactivé !");
        }
        // name
        if (StringUtils.isBlank(practicedPlot.getName())) {
            addFieldError("plot.name", getText(REQUIRED_FIELD));
            addActionError("Le nom de parcelle est non renseigné !");
        }

        try {
            convertSolHorizons(solHorizonsJson);
        } catch (Exception ex) {
            addActionError("Exception:" + ex + "\n/!\\ Désolé les données saisies depuis votre dernier enregistrement concernant les horizons de sol n'ont pu être récupérées !");
            if (practicedPlot.getSolHorizon() != null) {
                solHorizons = Lists.newArrayList(Collections2.transform(practicedPlot.getSolHorizon(), Plots.SOL_HORIZON_TO_DTO::apply));
            } else {
                solHorizons = new ArrayList<>();
            }
        }

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : practicedSystemTopiaId:'%s' -> %s", practicedSystemTopiaId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : practicedSystemTopiaId:'%s' -> %s", practicedSystemTopiaId, getFieldErrors().toString()));
            }
            initForInput();
        }
    }

    @Override
    @Action(results = { @Result(type = "redirectAction", params = { "actionName", "practiced-plots-edit-input", "practicedPlotTopiaId", "${practicedPlot.topiaId}" }) })
    public String execute() throws Exception {
        practicedPlot = practicedPlotService.createOrUpdatePracticedPlot(practicedPlot, practicedSystemTopiaId,
                locationTopiaId, selectedSurfaceTextureId, selectedSubSoilTextureId, selectedSolProfondeurId, selectedPlotZoningIds,
                solHorizons, adjacentElementIds);
        notificationSupport.practicedPlotSaved(practicedPlot);
        return SUCCESS;
    }
    
    public PracticedPlot getPracticedPlot() {
        // AThimel 26/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(practicedPlot, PracticedPlotImpl::new);
    }

    public void convertSolHorizons(String json) {
        Type type = new TypeToken<List<SolHorizonDto>>() {
        }.getType();
        this.solHorizons = getGson().fromJson(json, type);
    }

    public Map<MaxSlope, String> getMaxSlopes() {
        return getEnumAsMap(MaxSlope.values());
    }
    
    public Map<BufferStrip, String> getBufferStrips() {
        return getEnumAsMap(BufferStrip.values());
    }
    
    public Map<WaterFlowDistance, String> getWaterFlowDistances() {
        return getEnumAsMap(WaterFlowDistance.values());
    }

    public String getPracticedPlotTopiaId() {
        return practicedPlotTopiaId;
    }

    public void setPracticedPlotTopiaId(String plotTopiaId) {
        this.practicedPlotTopiaId = plotTopiaId;
    }

    public void setLocationTopiaId(String locationTopiaId) {
        this.locationTopiaId = locationTopiaId;
    }

    public String getLocationTopiaId() {
        return locationTopiaId;
    }

    public List<RefParcelleZonageEDI> getParcelleZonages() {
        return parcelleZonages;
    }

    public void setSelectedPlotZoningIds(List<String> selectedPlotZoningIds) {
        this.selectedPlotZoningIds = selectedPlotZoningIds;
    }

    public List<String> getSelectedPlotZoningIds() {
        return selectedPlotZoningIds;
    }

    public Map<IrrigationSystemType, String> getIrrigationSystemTypes() {
        return getEnumAsMap(IrrigationSystemType.values());
    }

    public Map<PompEngineType, String> getPompEngineTypes() {
        return getEnumAsMap(PompEngineType.values());
    }

    public Map<HosesPositionning, String> getHosesPositionnings() {
        return getEnumAsMap(HosesPositionning.values());
    }

    public Map<FrostProtectionType, String> getFrostProtectionTypes() {
        return getEnumAsMap(FrostProtectionType.values());
    }

    public Map<SolWaterPh, String> getSolWaterPhs() {
        return getEnumAsMap(SolWaterPh.values());
    }

    public List<RefSolTextureGeppa> getSolTextures() {
        return solTextures;
    }

    public List<RefSolProfondeurIndigo> getSolProfondeurs() {
        return solProfondeurs;
    }

    public String getSelectedSurfaceTextureId() {
        return selectedSurfaceTextureId;
    }

    public void setSelectedSurfaceTextureId(String selectedSurfaceTextureId) {
        this.selectedSurfaceTextureId = selectedSurfaceTextureId;
    }

    public String getSelectedSubSoilTextureId() {
        return selectedSubSoilTextureId;
    }

    public void setSelectedSubSoilTextureId(String selectedSubSoilTextureId) {
        this.selectedSubSoilTextureId = selectedSubSoilTextureId;
    }

    public String getSelectedSolProfondeurId() {
        return selectedSolProfondeurId;
    }

    public void setSelectedSolProfondeurId(String selectedSolProfondeurId) {
        this.selectedSolProfondeurId = selectedSolProfondeurId;
    }

    public List<SolHorizonDto> getSolHorizons() {
        return solHorizons;
    }

    public void setSolHorizons(String json) {
        solHorizonsJson = json;
    }
    public List<RefSolCaracteristiqueIndigo> getSolCaracteristiques() {
        return solCaracteristiques;
    }

    public Map<ZoneType, String> getZoneTypes() {
        return getEnumAsMap(ZoneType.values());
    }

    public List<RefElementVoisinage> getAdjacentElements() {
        return adjacentElements;
    }

    public List<String> getAdjacentElementIds() {
        return adjacentElementIds;
    }

    public void setAdjacentElementIds(List<String> adjacentElementIds) {
        this.adjacentElementIds = adjacentElementIds;
    }

    public String getPracticedSystemTopiaId() {
        return practicedSystemTopiaId;
    }

    public void setPracticedSystemTopiaId(String practicedSystemTopiaId) {
        this.practicedSystemTopiaId = practicedSystemTopiaId;
    }

    public PracticedSystem getPracticedSystem() {
        return practicedSystem;
    }

    public List<PracticedSystem> getPracticedSystems() {
        return practicedSystems;
    }
}
