package fr.inra.agrosyst.web.actions.plots;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;

/**
 * Duplication d'une parcelle.
 *
 * @author Eric Chatellier
 */
public class PlotsDuplicate extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 8908279864700466449L;
    
    protected transient PlotService plotService;

    /** Plot id. */
    protected transient String plotTopiaId;
    
    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    public void setPlotTopiaId(String plotTopiaId) {
        this.plotTopiaId = plotTopiaId;
    }

    @Override
    public String execute() throws Exception {
        Plot plot = plotService.duplicatePlot(plotTopiaId);
        jsonData = plot;
        return SUCCESS;
    }
}
