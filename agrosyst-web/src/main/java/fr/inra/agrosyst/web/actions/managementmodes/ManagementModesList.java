package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeDto;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeFilter;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

/**
 * Management modes list action.
 * 
 * @author Eric Chatellier
 */
public class ManagementModesList extends AbstractAgrosystAction {

    private static final Log LOGGER = LogFactory.getLog(ManagementModesList.class);

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -5032928580196001618L;

    protected ManagementModeFilter managementModeFilter;

    protected transient ManagementModeService managementModeService;

    protected PaginationResult<ManagementModeDto> managementModeDtos;

    protected int managementModesExportAsyncThreshold;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    @Override
    public String execute() throws Exception {
        try {
            NavigationContext navigationContext = getNavigationContext();
            managementModeFilter = new ManagementModeFilter();
            managementModeFilter.setNavigationContext(navigationContext);
            managementModeFilter.setPageSize(getListNbElementByPage(ManagementModeDto.class));

            managementModeDtos = managementModeService.getFilteredManagementModeDtos(managementModeFilter);

            this.managementModesExportAsyncThreshold = config.getManagementModesExportAsyncThreshold();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load management modes", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }

    public ManagementModeFilter getManagementModeFilter() {
        return managementModeFilter;
    }

    public PaginationResult<ManagementModeDto> getManagementModeDtos() {
        return managementModeDtos;
    }

    public int getManagementModesExportAsyncThreshold() {
        return managementModesExportAsyncThreshold;
    }
}
