package fr.inra.agrosyst.web.actions.auth;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;

/**
 * @author Sylvain Bavencoff : bavencoff@codelutin.com
 */
public class Charter extends AbstractAgrosystAction {
    
    @Serial
    private static final long serialVersionUID = 1L;

    protected transient UserService userService;

    protected boolean accepted;

    protected String next;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    @Action("charter-input")
    public String input() throws Exception {
        return super.input();
    }

    @Override
    public String execute() throws Exception {
        if (accepted) {
            userService.acceptCharter();
            refreshAuthenticatedUser();
        }

        if (Strings.isNullOrEmpty(next)
                || next.contains(AgrosystWebAuthenticationFilter.AGROSYST_WEB_LOGIN_ACTION)
                || next.contains(AgrosystWebAuthenticationFilter.AGROSYST_WEB_LOGIN_ACTION_INPUT)) {
            next = servletRequest.getContextPath();
        }
        if (next.contains(";jsessionid=")) {
            next = next.substring(0, next.indexOf(";jsessionid="));
        }
        servletResponse.sendRedirect(next);
        return null;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
}
