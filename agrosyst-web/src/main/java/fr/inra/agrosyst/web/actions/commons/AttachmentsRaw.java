package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AttachmentMetadata;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;

import java.io.Serial;
import java.util.List;

/**
 * This is the default action used to upload a file attached to a given object reference ID.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class AttachmentsRaw extends AbstractAgrosystAction {
    
    @Serial
    private static final long serialVersionUID = -2207776619499602254L;
    protected List<AttachmentMetadata> attachmentMetadatas;

    protected String objectReferenceId;
    
    public void setObjectReferenceId(String objectReferenceId) {
        this.objectReferenceId = objectReferenceId;
    }

    @Override
    public String execute() throws Exception {
        attachmentMetadatas = attachmentService.getAttachmentMetadatas(objectReferenceId);
        readOnly = !authorizationService.areAttachmentsAddableOrDeletable(objectReferenceId);
        return SUCCESS;
    }

    public List<AttachmentMetadata> getAttachmentMetadatas() {
        return attachmentMetadatas;
    }

    public String getObjectReferenceId() {
        return objectReferenceId;
    }

}
