package fr.inra.agrosyst.web.actions.reports;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe qui permet de déclencher un export asynchrone des bilans de campagne (échelle système de culture).
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class ReportGrowingSystemExportXlsAsync extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = 8686993981231582811L;

    protected transient ReportService reportService;

    protected List<String> reportGrowingSystemIds;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReportGrowingSystemIds(String reportGrowingSystemIds) {
        this.reportGrowingSystemIds = getGson().fromJson(reportGrowingSystemIds, List.class);
    }

    @Override
    public String execute() throws Exception {
        List<String> safeReportGrowingSystemIds = reportGrowingSystemIds.stream()
                .filter(reportGrowingSystemId -> {
                    try {
                        authorizationService.checkReportGrowingSystemReadable(reportGrowingSystemId);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                })
                .collect(Collectors.toList());

        reportService.exportXlsReportGrowingSystemsAsync(safeReportGrowingSystemIds);
        return SUCCESS;
    }

}
