package fr.inra.agrosyst.web.actions.domains;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.services.domain.DomainDeletionException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Delete selected domains
 * 
 * @author Kevin Morin
 */
public class DomainsDelete extends AbstractAdminAction {

    private static final Log LOGGER = LogFactory.getLog(DomainsDelete.class);
    @Serial
    private static final long serialVersionUID = -5038859523630065675L;
    
    protected List<String> domainIds;

    public void setDomainIds(String domainIds) {
        this.domainIds = getGson().fromJson(domainIds, List.class);
    }

    protected transient DomainService domainService;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "domains-list"}),
            @Result(name = ERROR, type = "redirectAction", params = {"actionName", "domains-list"})})
    public String execute() throws Exception {
        checkIsAdmin();
        String domainId = domainIds.getFirst();
        Domain domain = domainService.getDomain(domainId);
        String domainNameAndCampaign = domain.getName() + " (" + domain.getCampaign() + ")";
        try {
            domainService.deleteDomain(domainId);
            notificationSupport.domainDeleted(domainNameAndCampaign);
        } catch (DomainDeletionException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("For user email:" + getAuthenticatedUser().getEmail() + ":failed to delete domain '" + domainId + "', the domain has practiced systems", e);
            }
            notificationSupport.domainDeletionError("Ce domaine est associé à au moins un système synthétisé pluri-annuel, sa suppression n'est pour le moment pas possible.");
            return ERROR;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":Échec de suppression du domaine '%s' ", domainId), e);
            }
            notificationSupport.domainDeletionError("Erreur lors de la suppression du domaine : " + e.getMessage());
            return ERROR;
        }
        return SUCCESS;
    }
    
}
