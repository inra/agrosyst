package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveZoneFilter;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;

/**
 * Display plots list that may be associated (or not) with crop cycles and cropping plan
 * intervention.
 * 
 * @author Eric Chatellier
 */
public class EffectiveCropCyclesListJson extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -4624926883335820728L;

    private static final Log LOGGER = LogFactory.getLog(EffectiveCropCyclesListJson.class);

    protected transient EffectiveCropCycleService effectiveCropCycleService;

    protected transient String filter;

    protected transient boolean measurement;

    public void setEffectiveCropCycleService(EffectiveCropCycleService effectiveCropCycleService) {
        this.effectiveCropCycleService = effectiveCropCycleService;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setMeasurement(boolean measurement) {
        this.measurement = measurement;
    }

    @Action("effective-crop-cycles-list-json")
    public String effectiveCropCyclesList() throws Exception {
        try {
            EffectiveZoneFilter zoneFilter = getGson().fromJson(filter, EffectiveZoneFilter.class);
            writeListNbElementByPage(ZoneDto.class, String.valueOf(measurement), zoneFilter.getPageSize());
            NavigationContext navigationContext = getNavigationContext();
            zoneFilter.setNavigationContext(navigationContext);
            // Get Cropping Plan Entries and Intervention nb
            jsonData = effectiveCropCycleService.getFilteredZonesAndCroppingPlanInfosDto(zoneFilter);
        } catch(Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to zones and cropping plan info", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }



}
