package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.managementmode.DecisionRuleFilter;
import fr.inra.agrosyst.api.services.managementmode.DecisionRulesDto;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Decision rules list json action.
 * 
 * @author Eric Chatellier
 */
public class DecisionRulesListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(DecisionRulesListJson.class);

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -7454396591234192201L;

    protected transient ManagementModeService managementModeService;

    protected String filter;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String execute() {
        try {
            DecisionRuleFilter decisionRulefilter = getGson().fromJson(filter, DecisionRuleFilter.class);
            writeListNbElementByPage(DecisionRulesDto.class, decisionRulefilter.getPageSize());
            NavigationContext navigationContext = getNavigationContext();
            decisionRulefilter.setNavigationContext(navigationContext);
            jsonData = managementModeService.getFilteredDecisionRules(decisionRulefilter);

        }  catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load decision rules", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
