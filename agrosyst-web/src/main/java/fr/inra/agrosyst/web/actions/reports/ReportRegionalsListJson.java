package fr.inra.agrosyst.web.actions.reports;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.services.report.ReportRegionalFilter;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Action json de liste des bilans de campagne echelle regionale.
 */
public class ReportRegionalsListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(ReportRegionalsListJson.class);
    
    @Serial
    private static final long serialVersionUID = -9151580119720013994L;
    
    /** Service. */
    protected transient ReportService reportService;

    /** Filtre en entrée. */
    protected transient String filter;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String execute() throws Exception {
        try {
            ReportRegionalFilter reportRegionalFilter = getGson().fromJson(filter, ReportRegionalFilter.class);
            writeListNbElementByPage(ReportRegional.class, reportRegionalFilter.getPageSize());
            jsonData = reportService.getAllReportRegional(reportRegionalFilter);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load report regional list", ex);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
