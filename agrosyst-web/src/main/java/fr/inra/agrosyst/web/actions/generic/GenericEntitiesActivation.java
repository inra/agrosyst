package fr.inra.agrosyst.web.actions.generic;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.generic.GenericEntityService;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Désactivation multiple de domaines.
 * 
 * @author Eric Chatellier
 */
public class GenericEntitiesActivation extends AbstractAdminAction {

    private static final Log LOGGER = LogFactory.getLog(GenericEntitiesActivation.class);

    @Serial
    private static final long serialVersionUID = -5821886179843474360L;

    protected transient GenericEntityService genericEntityService;

    protected String genericClassName;
    protected List<String> entityIds;
    protected boolean activate;

    public void setGenericEntityService(GenericEntityService genericEntityService) {
        this.genericEntityService = genericEntityService;
    }

    public void setGenericClassName(String genericClassName) {
        this.genericClassName = genericClassName;
    }

    public String getGenericClassName() {
        return genericClassName;
    }

    public void setEntityIds(String entityIds) {
        this.entityIds = getGson().fromJson(entityIds, List.class);
    }

    public void setActivate(boolean activate) {
        this.activate = activate;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {
            "namespace", "/generic", "actionName", "generic-entities-list", "genericClassName", "${genericClassName}"})})
    public String execute() throws Exception {
        checkIsAdmin();

        try {
            Class<?> klass = Class.forName(genericClassName);
            genericEntityService.unactivateEntities(klass, entityIds, activate);
        } catch (ClassNotFoundException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Un exception occured", e);
            }
        }
        return SUCCESS;
    }

}
