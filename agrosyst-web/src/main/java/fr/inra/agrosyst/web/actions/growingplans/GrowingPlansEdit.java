package fr.inra.agrosyst.web.actions.growingplans;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanImpl;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class GrowingPlansEdit extends AbstractAgrosystAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(GrowingPlan.class);

    @Serial
    private static final long serialVersionUID = 2811409038914984468L;

    protected transient DomainService domainService;

    protected transient GrowingPlanService growingPlanService;

    protected String growingPlanTopiaId;

    protected GrowingPlan growingPlan;

    protected LinkedHashMap<Integer, String> relatedGrowingPlans;

    protected String domainId;

    protected List<GrowingSystem> growingSystems;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setGrowingPlanTopiaId(String growingPlanTopiaId) {
        this.growingPlanTopiaId = growingPlanTopiaId;
    }

    public GrowingPlan getGrowingPlan() {
        // EChatellier 27/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(growingPlan, GrowingPlanImpl::new);
    }

    @Override
    public void prepare() {
        if (StringUtils.isNotBlank(growingPlanTopiaId)) {
            // TODO AThimel 07/10/13 May be included directly in the service ?
            authorizationService.checkGrowingPlanReadable(growingPlanTopiaId);

            readOnly = !authorizationService.isGrowingPlanWritable(growingPlanTopiaId);
            if (readOnly) {
                notificationSupport.growingPlanNotWritable();
            }

            growingPlan = growingPlanService.getGrowingPlan(growingPlanTopiaId);

            activated = growingPlan.isActive() && growingPlan.getDomain().isActive();

        } else {
            growingPlan = growingPlanService.newGrowingPlan();
        }
    }

    @Override
    @Action("growing-plans-edit-input")
    public String input() throws Exception {
        initForInput();
        return INPUT;
    }

    @Override
    protected void initForInput() {
        // warning, growingPlan's domain must always be part of domains set
        // even not selected be navigation context
        if (getGrowingPlan().getTopiaId() != null) {
            relatedGrowingPlans = growingPlanService.getRelatedGrowingPlans(getGrowingPlan().getCode());
            growingSystems = growingPlanService.getGrowingPlanGrowingSystems(getGrowingPlan().getTopiaId());
        }
    }

    @Override
    public void validate() {

        if (growingPlan.isPersisted() && !activated) {
            addActionError("Le disposistif et/ou son domain sont inactifs !");
        }

        // Domaine
        if (StringUtils.isBlank(getGrowingPlan().getTopiaId()) && StringUtils.isBlank(domainId)) {
            addFieldError("domainId", getText(REQUIRED_FIELD));
        }

        // Nom du dispositif
        if (StringUtils.isBlank(growingPlan.getName())) {
            addFieldError("growingPlan.name", getText(REQUIRED_FIELD));
        }

        // Type de dispositif
        if (growingPlan.getType() == null) {
            addFieldError("growingPlan.type", getText(REQUIRED_FIELD));
        }

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                String growingPlanId = getLogEntityId(growingPlan);
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : gp:'%s' -> %s", growingPlanId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : gp:'%s' -> %s", growingPlanId, getFieldErrors().toString()));
            }

            initForInput();
        }

    }

    @Action(results = { @Result(type = "redirectAction", params = {"actionName", "growing-plans-edit-input", "growingPlanTopiaId", "${growingPlan.topiaId}"})})
    @Override
    public String execute() throws Exception {
        // can define domain only during create action
        if (StringUtils.isBlank(getGrowingPlan().getTopiaId())) {
            Domain domain = domainService.getDomain(domainId);
            growingPlan.setDomain(domain);
        }
 
        growingPlan = growingPlanService.createOrUpdateGrowingPlan(growingPlan);
        notificationSupport.growingPlanSaved(growingPlan);

        if (Strings.isNullOrEmpty(growingPlanTopiaId)) {
            navigationContextEntityCreated(growingPlan);
        }

        return SUCCESS;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getDomainId() {
        return domainId;
    }

    /**
     * Get all the type that a a domain could be.
     * 
     * @return all the type
     */
    public Map<TypeDEPHY, String> getTypesDephy() {
        return getEnumAsMap(TypeDEPHY.DEPHY_FERME, TypeDEPHY.DEPHY_EXPE, TypeDEPHY.NOT_DEPHY);
    }

    public LinkedHashMap<Integer, String> getRelatedGrowingPlans() {
        return relatedGrowingPlans;
    }

    public List<GrowingSystem> getGrowingSystems() {
        return growingSystems;
    }

}
