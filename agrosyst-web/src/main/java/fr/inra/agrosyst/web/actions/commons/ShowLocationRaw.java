package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShape;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;

import java.io.Serial;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class ShowLocationRaw extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -3545222983056002193L;

    protected transient ReferentialService referentialService;

    protected Double latitude;
    protected Double longitude;
    protected String locationId;
    protected String name;
    protected String departmentCode;

    protected Double centerLatitude;
    protected Double centerLongitude;

    protected String provider;
    protected RefLocation location;
    protected RefDepartmentShape departmentShape;
    protected String departmentName = "n/c";

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @Override
    public String execute() throws Exception {

        provider = "Esri.WorldImagery";
        if (!Strings.isNullOrEmpty(locationId)) {
            location = referentialService.getRefLocation(locationId);
            departmentCode = location.getDepartement();
        }

        if ((latitude == null || Math.abs(latitude + 8888.8d) < .0001) && (longitude == null || Math.abs(longitude + 8888.8d) < .0001)) {
            location = null;
            latitude = null;
            longitude = null;
        } else {
            centerLatitude = latitude;
            centerLongitude = longitude;
        }

        if (!Strings.isNullOrEmpty(departmentCode)) {
            departmentShape = referentialService.getDepartmentShape(departmentCode);
            if (departmentShape != null) {
                departmentName = getText("departement." + departmentShape.getDepartment());

                // Si pas de coordonnées, on centre sur le centre du département
                if (latitude == null && longitude == null) {
                    centerLatitude = departmentShape.getLatitude();
                    centerLongitude = departmentShape.getLongitude();
                }
            }
        }

        return SUCCESS;
    }

    public Double getCenterLatitude() {
        return centerLatitude;
    }

    public Double getCenterLongitude() {
        return centerLongitude;
    }

    public String getLatitude() {
        return String.format("%.5f", latitude).replaceAll(",", ".");
    }

    public String getLongitude() {
        return String.format("%.5f", longitude).replaceAll(",", ".");
    }

    public String getName() {
        return Strings.emptyToNull(name);
    }

    public String getProvider() {
        return provider;
    }

    public RefLocation getLocation() {
        return location;
    }

    public RefDepartmentShape getDepartmentShape() {
        return departmentShape;
    }

    public String getDepartmentName() {
        return departmentName;
    }

}
