package fr.inra.agrosyst.web.actions.ipmworks;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.Preparable;

/**
 * Action de recuperation de la liste paginées des résumés de domaines pour
 * IPMworks
 * 
 * @author Kevin Morin
 */
public class Index extends AbstractAgrosystAction implements Preparable {

    private String userLocale;

    @Override
    public void prepare() {
        userLocale = i18nService.getLocale().toString();
    }

    public String getUserLocale() {
        return userLocale;
    }
}
