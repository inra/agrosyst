package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.services.measurement.MeasurementService;
import fr.inra.agrosyst.api.services.measurement.ProtocoleVgObsFilter;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;

/**
 * JSON action to retrieve a <code>list</code> of {@link RefMesure} according to a given filter.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class EffectiveMeasurementsEditJson extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -4576065541768079736L;

    private static final Log LOGGER = LogFactory.getLog(EffectiveMeasurementsEditJson.class);

    protected transient MeasurementService measurementService;

    protected MeasurementType measurementType;

    protected VariableType variableType;

    protected String cropFamily;

    protected String vegetativeProfile;

    protected String vgObsFilter;

    protected BioAgressorType pestType;
    
    public void setMeasurementObservationService(MeasurementService measurementService) {
        this.measurementService = measurementService;
    }

    public void setMeasurementType(MeasurementType measurementType) {
        this.measurementType = measurementType;
    }

    public void setVariableType(VariableType variableType) {
        this.variableType = variableType;
    }

    public void setCropFamily(String cropFamily) {
        this.cropFamily = cropFamily;
    }

    public void setVegetativeProfile(String vegetativeProfile) {
        this.vegetativeProfile = vegetativeProfile;
    }

    public void setPestType(BioAgressorType pestType) {
        this.pestType = pestType;
    }

    @Action("effective-measurements-variable-types-json")
    public String listVariableTypes() {
        try {
            jsonData = measurementService.findAllVariableTypes(measurementType);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load measurements-variable-types for measurementType '%s':",
                        measurementType), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-variables-json")
    public String listVariables() {
        try {
            jsonData = measurementService.findAllVariables(measurementType, variableType);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load measurements-variable for measurementType '%s' and variableType '%s':",
                        measurementType, variableType), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("effective-measurements-stades-json")
    public String listStadeEdis() {
        try {
            Preconditions.checkArgument(cropFamily != null ^ vegetativeProfile != null);
            jsonData = measurementService.findAllStadeEdi(cropFamily, vegetativeProfile);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load effective-measurements-stades for cropFamily '%s' and vegetativeProfile '%s':",
                        cropFamily, vegetativeProfile), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public void setVgObsFilter(String vgObsFilter) {
        this.vgObsFilter = vgObsFilter;
    }

    protected ProtocoleVgObsFilter getVgObsFilter() {
        ProtocoleVgObsFilter protocoleVgObsFilter = getGson().fromJson(vgObsFilter, ProtocoleVgObsFilter.class);
        return protocoleVgObsFilter;
    }

    @Action("effective-measurements-vgobs-labels-json")
    public String listVgobsLabels() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsLabels();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-vgobs-labels", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-vgobs-pests-json")
    public String listVgobsPests() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsPests(getVgObsFilter());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effectivemeasurements-vgobs-pests", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-vgobs-stades-json")
    public String listVgobsStades() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsStades(getVgObsFilter());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-vgobs-stades", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-vgobs-supports-json")
    public String listVgobsSupports() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsSupports(getVgObsFilter());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-vgobs-supports", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-vgobs-observations-json")
    public String listVgobsObservations() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsObservations(getVgObsFilter());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-vgobs-observations", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-vgobs-qualitatives-json")
    public String listVgobsQualitatives() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsQualitatives(getVgObsFilter());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-vgobs-qualitatives", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("effective-measurements-vgobs-units-json")
    public String listVgobsUnits() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsUnits(getVgObsFilter());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-vgobs-units", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("effective-measurements-vgobs-qualifiers-json")
    public String listVgobsQualifiers() {
        try {
            jsonData = measurementService.findAllProtocoleVgObsQualifiers(getVgObsFilter());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-vgobs-qualifiers", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-edi-pesttypes-json")
    public String listPestTypes() {
        try {
            jsonData = measurementService.findAllEdiPestTypes();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-pesttypes", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("effective-measurements-edi-pests-json")
    public String listPests() {
        try {
            jsonData = measurementService.findAllEdiPests(pestType);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-pests", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-edi-peststades-json")
    public String listPestStades() {
        try {
            jsonData = measurementService.findAllEdiPestStades();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-peststades", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("effective-measurements-edi-supports-json")
    public String listSupports() {
        try {
            jsonData = measurementService.findAllSupportOrganeEDI();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-supports", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-edi-notations-json")
    public String listNotations() {
        try {
            jsonData = measurementService.findAllEdiNotations();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-notations", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-edi-qualitatives-json")
    public String listQualitatives() {
        try {
            jsonData = measurementService.findAllEdiQualitatives();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-qualitatives", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-edi-qualifierunits-json")
    public String listQualifierUnits() {
        try {
            jsonData = measurementService.findAllEdiQualifiantUnits();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-qualifierunits", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("effective-measurements-edi-units-json")
    public String listUnits() {
        try {
            jsonData = measurementService.findAllVgObsUnits();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load effective-measurements-edi-units", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
