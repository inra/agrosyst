package fr.inra.agrosyst.web.actions.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class GetEntitiesFromRoleTypeJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(GetEntitiesFromRoleTypeJson.class);

    @Serial
    private static final long serialVersionUID = -3616533789132105979L;

    protected transient RoleType roleType;
    protected transient String term;
    
    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public String execute() throws Exception {
        try {
            jsonData = authorizationService.searchPossibleEntities(roleType, term);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load entities for roleType '%s' and term '%s'",
                        roleType, term), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
