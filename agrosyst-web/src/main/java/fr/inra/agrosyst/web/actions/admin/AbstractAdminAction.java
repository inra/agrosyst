package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.AutorisationFR;
import fr.inra.agrosyst.api.entities.BioAgressorCategory;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.BufferStrip;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.DoseType;
import fr.inra.agrosyst.api.entities.EstimatingIftRules;
import fr.inra.agrosyst.api.entities.FrostProtectionType;
import fr.inra.agrosyst.api.entities.GrowingSystemCharacteristicType;
import fr.inra.agrosyst.api.entities.HosesPositionning;
import fr.inra.agrosyst.api.entities.IftSeedsType;
import fr.inra.agrosyst.api.entities.IrrigationSystemType;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.ModalityDephy;
import fr.inra.agrosyst.api.entities.OrchardFrutalForm;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PollinatorSpreadMode;
import fr.inra.agrosyst.api.entities.PompEngineType;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SolWaterPh;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.VineFrutalForm;
import fr.inra.agrosyst.api.entities.WaterFlowDistance;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.Zoning;
import fr.inra.agrosyst.api.entities.action.CapacityUnit;
import fr.inra.agrosyst.api.entities.action.FertiOrgaUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.YealdCategory;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.InvolvedRuleCompliance;
import fr.inra.agrosyst.api.entities.managementmode.CategoryObjective;
import fr.inra.agrosyst.api.entities.managementmode.CategoryStrategy;
import fr.inra.agrosyst.api.entities.managementmode.DamageType;
import fr.inra.agrosyst.api.entities.managementmode.ImplementationType;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.measure.ChemicalElement;
import fr.inra.agrosyst.api.entities.measure.HorizonType;
import fr.inra.agrosyst.api.entities.measure.MeasureType;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.measure.NitrogenMolecule;
import fr.inra.agrosyst.api.entities.measure.ProductivityType;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.CriteriaUnit;
import fr.inra.agrosyst.api.entities.referential.FeedbackCategory;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.PeriodeSemis;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.QualityAttributeType;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefGesCarburant;
import fr.inra.agrosyst.api.entities.referential.RefGesEngrais;
import fr.inra.agrosyst.api.entities.referential.RefGesPhyto;
import fr.inra.agrosyst.api.entities.referential.RefGesSemence;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefNrjCarburant;
import fr.inra.agrosyst.api.entities.referential.RefNrjEngrais;
import fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil;
import fr.inra.agrosyst.api.entities.referential.RefNrjPhyto;
import fr.inra.agrosyst.api.entities.referential.RefNrjSemence;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefSaActaIphy;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnits;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI;
import fr.inra.agrosyst.api.entities.referential.RefUniteEDI;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy;
import fr.inra.agrosyst.api.entities.referential.TradRefDestination;
import fr.inra.agrosyst.api.entities.referential.TradRefDivers;
import fr.inra.agrosyst.api.entities.referential.TradRefIntervention;
import fr.inra.agrosyst.api.entities.referential.TradRefIntrant;
import fr.inra.agrosyst.api.entities.referential.TradRefMateriel;
import fr.inra.agrosyst.api.entities.referential.TradRefSol;
import fr.inra.agrosyst.api.entities.referential.TradRefVivant;
import fr.inra.agrosyst.api.entities.referential.TypeCulture;
import fr.inra.agrosyst.api.entities.referential.VitesseCouv;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.entities.report.DamageLevel;
import fr.inra.agrosyst.api.entities.report.DiseaseAttackRate;
import fr.inra.agrosyst.api.entities.report.FoodIrrigation;
import fr.inra.agrosyst.api.entities.report.GlobalMasterLevelQualifier;
import fr.inra.agrosyst.api.entities.report.GrassingEvolution;
import fr.inra.agrosyst.api.entities.report.GrassingLevel;
import fr.inra.agrosyst.api.entities.report.IftEstimationMethod;
import fr.inra.agrosyst.api.entities.report.InoculumLevel;
import fr.inra.agrosyst.api.entities.report.MasterScale;
import fr.inra.agrosyst.api.entities.report.PestAttackRate;
import fr.inra.agrosyst.api.entities.report.PestMasterLevelQualifier;
import fr.inra.agrosyst.api.entities.report.PressureEvolution;
import fr.inra.agrosyst.api.entities.report.PressureScale;
import fr.inra.agrosyst.api.entities.report.RiskScale;
import fr.inra.agrosyst.api.entities.report.SectorSpecies;
import fr.inra.agrosyst.api.entities.report.StressLevel;
import fr.inra.agrosyst.api.entities.report.YieldLossCause;
import fr.inra.agrosyst.api.entities.report.YieldObjective;
import fr.inra.agrosyst.api.entities.security.PermissionObjectType;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.TrackedEventType;
import fr.inra.agrosyst.api.services.generic.GenericEntityService;
import fr.inra.agrosyst.services.referential.csv.RefEspeceDto;
import fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.Preparable;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public abstract class AbstractAdminAction extends AbstractAgrosystAction implements Preparable {

    @Serial
    private static final long serialVersionUID = -5817371954801847984L;

    private static final Class[] AUTHORITY_BASE_CLASSES = new Class[] {
            AgrosystInterventionType.class,
            AutorisationFR.class,
            BioAgressorCategory.class,
            BioAgressorType.class,
            BufferStrip.class,
            CapacityUnit.class,
            CategoryObjective.class,
            CategoryStrategy.class,
            ChemicalElement.class,
            CriteriaUnit.class,
            CropCyclePhaseType.class,
            CroppingEntryType.class,
            DamageLevel.class,
            DamageType.class,
            DiseaseAttackRate.class,
            DomainType.class,
            DoseType.class,
            EstimatingIftRules.class,
            FeedbackCategory.class,
            FertiMinElement.class,
            FertiOrgaUnit.class,
            FoodIrrigation.class,
            FrostProtectionType.class,
            GlobalMasterLevelQualifier.class,
            GrassingEvolution.class,
            GrassingLevel.class,
            GrowingSystemCharacteristicType.class,
            GrowingSystemCharacteristicType.class,
            HorizonType.class,
            HosesPositionning.class,
            IftEstimationMethod.class,
            IftSeedsType.class,
            ImplementationType.class,
            InoculumLevel.class,
            InvolvedRuleCompliance.class,
            IrrigationSystemType.class,
            ManagementModeCategory.class,
            MasterScale.class,
            MaterielTransportUnit.class,
            MaterielType.class,
            MaterielWorkRateUnit.class,
            MaxSlope.class,
            MeasurementType.class,
            MeasureType.class,
            MineralProductUnit.class,
            ModalityDephy.class,
            NitrogenMolecule.class,
            OrchardFrutalForm.class,
            OrganicProductUnit.class,
            OtherProductInputUnit.class,
            PerformanceState.class,
            PeriodeSemis.class,
            PermissionObjectType.class,
            PestAttackRate.class,
            PestMasterLevelQualifier.class,
            PhytoProductUnit.class,
            PollinatorSpreadMode.class,
            PompEngineType.class,
            PracticedSystemSource.class,
            PressureEvolution.class,
            PressureScale.class,
            PriceUnit.class,
            ProductivityType.class,
            ProductType.class,
            QualityAttributeType.class,
            RiskScale.class,
            RoleType.class,
            SectionType.class,
            Sector.class,
            SectorSpecies.class,
            SeedPlantUnit.class,
            SeedType.class,
            SolWaterPh.class,
            StrategyType.class,
            StressLevel.class,
            TrackedEventType.class,
            TypeCulture.class,
            TypeDEPHY.class,
            VariableType.class,
            VineFrutalForm.class,
            VitesseCouv.class,
            WaterFlowDistance.class,
            WeedType.class,
            WineValorisation.class,
            YealdCategory.class,
            YealdUnit.class,
            YieldLossCause.class,
            YieldObjective.class,
            ZoneType.class,
            Zoning.class
    };

    private static final Class[] REFERENTIAL_BASE_CLASSES = new Class[] {
            RefActaDosageSaRoot.class,
            RefActaDosageSPC.class,
            RefActaDosageSpcRoot.class,
            RefActaGroupeCultures.class,
            RefActaProduitRoot.class,
            RefActaSubstanceActive.class,
            RefActaTraitementsProduit.class,
            RefActaTraitementsProduitsCateg.class,
            RefAdventice.class,
            RefAgsAmortissement.class,
            RefAnimalType.class,
            RefCattleAnimalType.class,
            RefCattleRationAliment.class,
            RefCiblesAgrosystGroupesCiblesMAA.class,
            RefClonePlantGrape.class,
            RefConversionUnitesQSA.class,
            RefCompositionSubstancesActivesParNumeroAMM.class,
            RefCorrespondanceMaterielOutilsTS.class,
            RefCouvSolAnnuelle.class,
            RefCouvSolPerenne.class,
            RefCultureEdiGroupeCouvSol.class,
            RefDestination.class,
            RefEdaplosTypeTraitement.class,
            RefElementVoisinage.class,
            RefEspeceDto.class,
            RefEspeceOtherTools.class,
            RefEspeceToVariete.class,
            RefFeedbackRouter.class,
            RefFertiMinUNIFA.class,
            RefFertiOrga.class,
            RefGesCarburant.class,
            RefGesEngrais.class,
            RefGesPhyto.class,
            RefGesSemence.class,
            RefGroupeCibleTraitement.class,
            RefHarvestingPrice.class,
            RefHarvestingPriceConverter.class,
            RefInputUnitPriceUnitConverter.class,
            RefInterventionAgrosystTravailEDI.class,
            RefInterventionTypeItemInputEDI.class,
            RefLegalStatus.class,
            RefLocationExtendedDto.class,
            RefMAABiocontrole.class,
            RefMAADosesRefParGroupeCible.class,
            RefMarketingDestination.class,
            RefMaterielAutomoteur.class,
            RefMaterielIrrigation.class,
            RefMaterielOutil.class,
            RefMaterielTraction.class,
            RefMesure.class,
            RefNrjCarburant.class,
            RefNrjEngrais.class,
            RefNrjGesOutil.class,
            RefNrjPhyto.class,
            RefNrjSemence.class,
            RefNuisibleEDI.class,
            RefOrientationEDI.class,
            RefOTEX.class,
            RefOtherInput.class,
            RefParcelleZonageEDI.class,
            RefPhrasesRisqueEtClassesMentionDangerParAMM.class,
            RefPhytoSubstanceActiveIphy.class,
            RefPot.class,
            RefPrixAutre.class,
            RefPrixCarbu.class,
            RefPrixEspece.class,
            RefPrixFertiMin.class,
            RefPrixFertiOrga.class,
            RefPrixIrrig.class,
            RefPrixPhyto.class,
            RefPrixPot.class,
            RefPrixSubstrate.class,
            RefProtocoleVgObs.class,
            RefQualityCriteria.class,
            RefQualityCriteriaClass.class,
            RefSaActaIphy.class,
            RefSeedUnits.class,
            RefSolArvalis.class,
            RefSolCaracteristiqueIndigo.class,
            RefSolProfondeurIndigo.class,
            RefSolTextureGeppa.class,
            RefSpeciesToSector.class,
            RefStadeEDI.class,
            RefStadeNuisibleEDI.class,
            RefStationMeteo.class,
            RefStrategyLever.class,
            RefSubstancesActivesCommissionEuropeenne.class,
            RefSubstrate.class,
            RefSupportOrganeEDI.class,
            RefTraitSdC.class,
            RefTypeAgriculture.class,
            RefTypeNotationEDI.class,
            RefUniteEDI.class,
            RefUnitesQualifiantEDI.class,
            RefValeurQualitativeEDI.class,
            RefVarieteGeves.class,
            RefVarietePlantGrape.class,
            RefZoneClimatiqueIphy.class,
            RefCountry.class,
            TradRefVivant.class,
            TradRefSol.class,
            TradRefMateriel.class,
            TradRefDivers.class,
            TradRefIntervention.class,
            TradRefDestination.class,
            TradRefIntrant.class
    };

    protected static final Function<Class, String> GET_CLASS_NAME = Class::getName;

    private static List<String> authorityBaseClassNames;
    private static List<String> referentialBaseClassNames;

    protected Map<String, Long> referentialClasses;

    protected Map<String, Long> authorityClasses;

    protected transient GenericEntityService service;

    public void setService(GenericEntityService service) {
        this.service = service;
    }

    protected void checkIsAdmin() {
        authorizationService.checkIsAdmin();
    }

    protected void sortFromI18n(Class<?>... classes) {
        Arrays.sort(classes, (o1, o2) -> {
            String className1 = o1.getName();
            String className2 = o2.getName();
            String text1 = getText(className1, className1);
            String text2 = getText(className2, className2);
            return text1.compareTo(text2);
        });
    }

    protected static void resetReferentialBaseClassesList() {
        referentialBaseClassNames = new ArrayList<>();
        Iterables.addAll(referentialBaseClassNames, Arrays.stream(REFERENTIAL_BASE_CLASSES).map(GET_CLASS_NAME).collect(Collectors.toList()));
    }

    protected static void resetAuthorityBaseClassesList() {
        authorityBaseClassNames = new ArrayList<>();
        Iterables.addAll(authorityBaseClassNames, Arrays.stream(AUTHORITY_BASE_CLASSES).map(GET_CLASS_NAME).collect(Collectors.toList()));
    }

    @Override
    public void prepare() throws Exception {
        if (referentialBaseClassNames == null) {
            sortFromI18n(REFERENTIAL_BASE_CLASSES);
            resetReferentialBaseClassesList();
        }
        if (authorityBaseClassNames == null) {
            sortFromI18n(AUTHORITY_BASE_CLASSES);
            resetAuthorityBaseClassesList();
        }

        if (authorizationService.isAdmin()) {
            referentialClasses = service.countEntitiesFromString(referentialBaseClassNames);
            authorityClasses = service.countEntitiesFromString(authorityBaseClassNames);
        }

    }

    public Map<String, Long> getReferentialClasses() {
        return referentialClasses;
    }

    public Map<String, Long> getAuthorityClasses() {
        return authorityClasses;
    }

}
