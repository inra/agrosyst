package fr.inra.agrosyst.web.actions.commons;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.components.URL;
import org.apache.struts2.dispatcher.mapper.DefaultActionMapper;
import org.apache.struts2.url.StrutsQueryStringBuilder;
import org.apache.struts2.url.StrutsUrlEncoder;
import org.apache.struts2.views.util.DefaultUrlHelper;
import org.reflections.Reflections;

import java.io.Serial;
import java.lang.reflect.Modifier;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class EndpointsAction extends AbstractAgrosystAction {
    
    @Serial
    private static final long serialVersionUID = 5425103883346009788L;
    private final SortedMap<String, String> endpoints = new TreeMap<>();

    @Override
    public String execute() {

        Reflections reflections = new Reflections("fr.inra.agrosyst.web.actions");
        Set<Class<? extends AbstractAgrosystAction>> actions = reflections.getSubTypesOf(AbstractAgrosystAction.class);
        actions.stream()
                .filter(action -> !Modifier.isAbstract(action.getModifiers()))
                .forEach(action -> {
                    String ipmPref = action.getName().contains("ipmworks") ? "ipmworks_" : "";
                    // compute action name
                    String actionName = StringUtils.removeEnd(action.getSimpleName(), "Action");
                    actionName = StringUtils.uncapitalize(actionName);
                    String actionNameUrl = actionName.replaceAll("([a-z])([A-Z]+)", "$1-$2").toLowerCase();

                    // compute action url
                    String packageName = action.getPackage().getName();
                    String namespace = String.join("/", StringUtils.substringAfter(packageName, "actions.").split("\\."));
                    namespace = StringUtils.prependIfMissing(namespace, "/");
                    String result = computeStrutsUrl(actionNameUrl, namespace);

                    endpoints.put(ipmPref + actionName, result);

                    if (action.getSimpleName().endsWith("Edit")) {
                        actionName += "Input";
                        actionNameUrl += "-input";
                        result = computeStrutsUrl(actionNameUrl, namespace);
                        endpoints.put(ipmPref + actionName, result);
                    }
                });

        return SUCCESS;
    }

    protected String computeStrutsUrl(String simpleName, String namespace) {
        ActionContext context = ActionContext.getContext();
        ActionInvocation invocation = context.getActionInvocation();
        URL url = new URL(invocation.getStack(), servletRequest, null);
        url.setActionMapper(new DefaultActionMapper());
        DefaultUrlHelper helper = new DefaultUrlHelper();
        helper.setQueryStringBuilder(new StrutsQueryStringBuilder(new StrutsUrlEncoder()));
        url.setUrlHelper(helper);
        return url.getUrlProvider().determineActionURL(
                simpleName,
                namespace,
                null,
                servletRequest,
                null,
                null,
                servletRequest.getScheme(),
                true, true, false, false
        );
    }

    public SortedMap<String, String> getEndpoints() {
        return endpoints;
    }
}
