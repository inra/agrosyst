package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * Représentation l'état de validation d'une entité quelconque. Ce bean permet de savoir si l'utilisateur courant a le
 * droit de valider, si l'entité a évolué depuis sa dernière validation ainsi que la date de dernière validation.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 * @since 0.8
 */
public class AgrosystValidationState implements Serializable {

    @Serial
    private static final long serialVersionUID = 5481632474364290629L;

    protected boolean userHasValidationPermission;
    protected boolean dirty; // dirty data means that data has been modified since its last validation
    protected Date lastValidationDate;

    public boolean isValidationPossible() {
        return userHasValidationPermission && dirty;
    }

    public boolean isUserHasValidationPermission() {
        return userHasValidationPermission;
    }

    public void setUserHasValidationPermission(boolean userHasValidationPermission) {
        this.userHasValidationPermission = userHasValidationPermission;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public Date getLastValidationDate() {
        return lastValidationDate;
    }

    public void setLastValidationDate(Date lastValidationDate) {
        this.lastValidationDate = lastValidationDate;
    }

}
