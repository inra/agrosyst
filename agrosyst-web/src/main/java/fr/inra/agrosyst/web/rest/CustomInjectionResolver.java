package fr.inra.agrosyst.web.rest;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import fr.inra.agrosyst.web.AgrosystWebConfig;
import fr.inra.agrosyst.web.AgrosystWebNotificationSupport;
import fr.inra.agrosyst.web.AgrosystWebNotificationSupportDistributed;
import fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter;
import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.ServiceHandle;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.util.Map;
import java.util.Optional;

/**
 * This injection resolver aim to make a bridge between hk2 CDI and Agrosyst legacy CDI.
 */
public class CustomInjectionResolver implements InjectionResolver<CustomInject> {

    @Inject
    private ServletContext servletContext;

    @Context
    private HttpServletRequest httpServletRequest;

    @Override
    public Object resolve(Injectee injectee, ServiceHandle<?> root) {
        if (injectee.getInjecteeClass() != null) {
            AgrosystWebApplicationContext webApplicationContext = getAgrosystApplicationContext();
            Optional<AuthenticatedUser> authenticatedUser = getAuthenticatedUser();
            ServiceContext serviceContext = (ServiceContext) httpServletRequest.getAttribute(ServiceContext.SERVICE_CONTEXT_PARAMETER);

            Object toInject = null;
            if (AgrosystService.class.isAssignableFrom((Class<?>) injectee.getRequiredType())) {
                Class<? extends AgrosystService> clazz = (Class<? extends AgrosystService>) injectee.getRequiredType();
                toInject = serviceContext.newService(clazz);
            } else if (AgrosystWebConfig.class.isAssignableFrom((Class<?>) injectee.getRequiredType())) {
                toInject = webApplicationContext.getWebConfig();
            } else if (AgrosystWebApplicationContext.class.isAssignableFrom((Class<?>) injectee.getRequiredType())) {
                toInject = webApplicationContext;
            } else if (AgrosystWebNotificationSupport.class.isAssignableFrom((Class<?>) injectee.getRequiredType())) {
                AgrosystWebNotificationSupport notificationSupport = getNotificationSupport(webApplicationContext, authenticatedUser);
                toInject = notificationSupport;
            }
            return toInject;
        }
        return null;
    }

    protected AgrosystWebApplicationContext getAgrosystApplicationContext() {
        AgrosystWebApplicationContext applicationContext = (AgrosystWebApplicationContext) servletContext
                .getAttribute(AgrosystWebApplicationContext.APPLICATION_CONTEXT_PARAMETER);
        Preconditions.checkState(applicationContext != null, AgrosystWebApplicationContext.MISSING_APPLICATION_CONTEXT);
        return applicationContext;
    }

    protected Optional<AuthenticatedUser> getAuthenticatedUser() {
        AuthenticatedUser userDto = (AuthenticatedUser) httpServletRequest.getAttribute(AgrosystWebAuthenticationFilter.AUTHENTICATED_USER);
        return Optional.ofNullable(userDto);
    }

    protected AgrosystWebNotificationSupport getNotificationSupport(AgrosystWebApplicationContext webApplicationContext,
                                                                    Optional<AuthenticatedUser> authenticatedUser) {
        if (authenticatedUser.isPresent()) {
            Map<String, AgrosystWebNotificationSupportDistributed> cache = webApplicationContext.getDistributedCache("notifications");
            String userId = authenticatedUser.get().getTopiaId();
            final AgrosystWebNotificationSupportDistributed notificationSupport =
                    Optional.ofNullable(cache.get(userId)).orElseGet(AgrosystWebNotificationSupportDistributed::new);
            notificationSupport.setLanguage(authenticatedUser.get().getLanguage());
            Runnable updater = () -> {
                if (notificationSupport.isEmpty()) {
                    // On fait en sorte de ne pas maintenir un objet en mémoire pour rien s'il n'y a pas de notification
                    cache.remove(userId);
                } else {
                    cache.put(userId, notificationSupport);
                }
            };
            notificationSupport.setUpdater(updater);
            return notificationSupport;
        } else {
            AgrosystWebNotificationSupport result = new AgrosystWebNotificationSupport();
            result.setLanguage(Language.FRENCH);
            return result;
        }
    }

    @Override
    public boolean isConstructorParameterIndicator() {
        return true;
    }

    @Override
    public boolean isMethodParameterIndicator() {
        return true;
    }
}
