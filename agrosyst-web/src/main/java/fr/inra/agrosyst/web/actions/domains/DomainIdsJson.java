package fr.inra.agrosyst.web.actions.domains;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.Set;

/**
 * Action de recuperation de la liste des ids des domaines (AJAX).
 * 
 * @author Kevin Morin
 */
@Getter
@Setter
public class DomainIdsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(DomainIdsJson.class);
    @Serial
    private static final long serialVersionUID = -639287019639333023L;
    
    protected transient DomainService domainService;

    protected boolean fromNavigationContextChoose = false;
    protected String filter;
    protected Set<Integer> selectedCampaigns = Sets.newHashSet();
    protected Set<String> selectedNetworks = Sets.newHashSet();

    @Override
    public String execute() {
        try {
            DomainFilter domainFilter = getGson().fromJson(filter, DomainFilter.class);
            NavigationContext navigationContext;
            if (fromNavigationContextChoose) {
                navigationContext = new NavigationContext(selectedCampaigns, selectedNetworks, null, null, null);
            } else {
                navigationContext = getNavigationContext();
            }
            domainFilter.setNavigationContext(navigationContext);

            jsonData = domainService.getFilteredDomainIds(domainFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load domains list", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }
}
