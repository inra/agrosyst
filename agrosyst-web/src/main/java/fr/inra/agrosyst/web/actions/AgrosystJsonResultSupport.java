package fr.inra.agrosyst.web.actions;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA
 * Copyright (C) 2020 INRAE
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.result.StrutsResultSupport;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serial;
import java.nio.charset.StandardCharsets;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystJsonResultSupport extends StrutsResultSupport {

    @Serial
    private static final long serialVersionUID = 4604320651695670319L;

    private static final Log LOGGER = LogFactory.getLog(AgrosystJsonResultSupport.class);

    protected transient Gson gson;

    public Gson getGson() {
        if (gson == null) {
            gson = new AgrosystGsonSupplier().get();
        }
        return gson;
    }

    @Override
    protected void doExecute(String finalLocation, ActionInvocation invocation) {

        Object jsonData = invocation.getStack().findValue("jsonData");

        String json = getGson().toJson(jsonData);

        // Work-arround for IE to not display download dialog for json result
        // see https://github.com/blueimp/jQuery-File-Upload/issues/1795
        HttpServletRequest servletRequest = (HttpServletRequest) invocation.getInvocationContext().get(HTTP_REQUEST);
        HttpServletResponse servletResponse = (HttpServletResponse) invocation.getInvocationContext().get(HTTP_RESPONSE);

        if (invocation.getResultCode().equals(Action.ERROR)) {
            Integer httpCode = (Integer) invocation.getStack().findValue("httpCode");
            if (httpCode == null) {
                httpCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            }
            servletResponse.setStatus(httpCode);
        }

        servletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
        String acceptHeader = servletRequest.getHeader("accept");
        if (acceptHeader == null || acceptHeader.contains("application/json")) {
            servletResponse.setContentType("application/json");
        } else {
            // IE workaround
            servletResponse.setContentType("text/plain");
        }

        try {
            ServletOutputStream outputStream = servletResponse.getOutputStream();
            byte[] jsonBytes = json.getBytes(StandardCharsets.UTF_8); // On transforme en bytes pour assurer l'encodage
            outputStream.write(jsonBytes);
        } catch (IOException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Unable to write JSON output into Servlet Response");
            }
        }

    }

}
