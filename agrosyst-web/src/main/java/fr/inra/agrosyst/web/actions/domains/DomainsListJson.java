package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.HashSet;
import java.util.Set;

/**
 * Action de recuperation de la liste paginées des domaines (AJAX).
 * 
 * @author Eric Chatellier
 */
public class DomainsListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(DomainsListJson.class);

    @Serial
    private static final long serialVersionUID = 8462808027355013164L;

    protected transient DomainService domainService;

    protected transient boolean fromNavigationContextChoose = false;
    protected transient String filter;
    protected transient Set<Integer> selectedCampaigns = new HashSet<>();
    protected transient Set<String> selectedNetworks = new HashSet<>();
    protected transient Boolean pagination = true;
    
    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setFromNavigationContextChoose(boolean fromNavigationContextChoose) {
        this.fromNavigationContextChoose = fromNavigationContextChoose;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setSelectedCampaigns(Set<Integer> selectedCampaigns) {
        this.selectedCampaigns = selectedCampaigns;
    }

    public void setSelectedNetworks(Set<String> selectedNetworks) {
        this.selectedNetworks = selectedNetworks;
    }

    public void setPagination(Boolean pagination) {
        this.pagination = pagination;
    }

    @Override
    public String execute() {
        try {
            DomainFilter domainFilter = getGson().fromJson(filter, DomainFilter.class);
            NavigationContext navigationContext;
            if (fromNavigationContextChoose) {
                navigationContext = new NavigationContext(selectedCampaigns, selectedNetworks, null, null, null);
                writeListNbElementByPage(DomainDto.class, NAVIGATION_CONTEXT, domainFilter.getPageSize());

            } else {
                navigationContext = getNavigationContext();
                if (!pagination) {
                    domainFilter.setAllPageSize();
                    domainFilter.setPage(0);
                } else {
                    writeListNbElementByPage(DomainDto.class, domainFilter.getPageSize());
                }
            }
            domainFilter.setNavigationContext(navigationContext);

            jsonData = domainService.getFilteredDomainsDto(domainFilter);

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load domains list", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }
}
