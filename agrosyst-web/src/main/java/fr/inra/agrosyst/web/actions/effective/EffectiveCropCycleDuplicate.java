package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

/**
 * @author David Cossé
 */
public class EffectiveCropCycleDuplicate extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 1L;
    
    protected String fromZoneId;

    protected String toZoneId;

    protected EffectiveCropCycleService effectiveCropCycleService;

    public void setEffectiveCropCycleService(EffectiveCropCycleService effectiveCropCycleService) {
        this.effectiveCropCycleService = effectiveCropCycleService;
    }

    public void setFromZoneId(String fromZoneId) {
        this.fromZoneId = fromZoneId;
    }

    public void setToZoneId(String toZoneId) {
        this.toZoneId = toZoneId;
    }

    public String getToZoneId() {
        return toZoneId;
    }

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "effective-crop-cycles-edit-input", "zoneTopiaId", "${toZoneId}"}),
            @Result(name = ERROR, type = "redirectAction", params = {"actionName", "effective-crop-cycles-list"})
    })
    public String execute() {
        if (Strings.isNullOrEmpty(fromZoneId) || Strings.isNullOrEmpty(toZoneId)) {
            notificationSupport.effectiveCropCycleNotDuplicable();
            return ERROR;
        }
        effectiveCropCycleService.duplicateEffectiveCropCycles(fromZoneId, toZoneId);
        notificationSupport.effectiveCropCycleDuplicate();
        return SUCCESS;
    }
}
