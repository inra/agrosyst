package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.services.common.InputPriceFilter;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.search.PhytoProductInputFilter;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.RefAnimalTypeDto;
import fr.inra.agrosyst.api.services.referential.RefCattleAnimalTypeDto;
import fr.inra.agrosyst.api.services.referential.RefCattleRationAlimentDto;
import fr.inra.agrosyst.api.services.referential.RefSubstrateDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.referential.TypeMaterielFilter;
import fr.inra.agrosyst.services.common.EntityUsageService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.collections4.MultiMapUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Permet de gerer les methode json pour l'edition d'un domaine.
 * 
 * @author Eric Chatellier
 */
public class DomainsEditJson extends AbstractJsonAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(DomainsEditJson.class);

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -4134613183262825235L;


    protected transient DomainInputStockUnitService domainInputStockUnitService;
    protected transient DomainService domainService;
    protected transient EntityUsageService entityUsageService;
    protected transient InputPriceService inputPriceService;
    protected transient PlotService plotService;
    protected transient ReferentialService referentialService;



    /** Filter to execute on query. */
    protected transient String filter;

    /** Region for RefSolArvalis actions. */
    protected transient Integer regionCode;

    /** otex18code form edit domain otex code. */
    protected transient Integer otex18code;

    /** Domain name (to check existance). */
    protected transient String domainName;
    
    protected transient String domainId;
    
    protected transient String toolsCouplingCode;
    
    protected transient String materielId;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setEntityUsageService(EntityUsageService entityUsageService) {
        this.entityUsageService = entityUsageService;
    }

    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }
    
    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setInputPriceService(InputPriceService inputPriceService) {
        this.inputPriceService = inputPriceService;
    }

    public void setDomainInputStockUnitService(DomainInputStockUnitService domainInputStockUnitService) {
        this.domainInputStockUnitService = domainInputStockUnitService;
    }

    @Override
    public void prepare() {
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setRegionCode(Integer regionCode) {
        this.regionCode = regionCode;
    }

    public void setOtex18code(Integer otex18code) {
        this.otex18code = otex18code;
    }

    public void setOtex18code(String otex18code) {
        this.otex18code = StringUtils.isBlank(otex18code) ? null : Integer.valueOf(otex18code);
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }
    
    public void setDomainId(String domainId) {
        if (domainId != null && !domainId.equals("undefined") && !domainId.equals("null")) {
            this.domainId = domainId;
        }
    }
    
    @Action("domain-edit-crops-context-json")
    public String getCropContext() {
    
        HashMap<String, Object> cropContext = new HashMap<>();
    
        ArrayList<CroppingPlanEntryDto> croppingPlans;
        HashMap<String, Boolean> croppingPlansUsageMap;
        HashMap<String, Boolean> croppingPlanSpeciesUsageMap;
        
        if (StringUtils.isNotBlank(domainId)) {
            
            UsageList<CroppingPlanEntryDto> croppingPlansUsageList = domainService.getCroppingPlanEntryDtoAndUsage(domainId);
            croppingPlans = new ArrayList<>(croppingPlansUsageList.getElements());
            croppingPlansUsageMap = new HashMap<>(croppingPlansUsageList.getUsageMap());
            croppingPlanSpeciesUsageMap = new HashMap<>(domainService.getCroppingPlanSpeciesUsage(domainId));

        } else {
            
            croppingPlans = new ArrayList<>();
            croppingPlansUsageMap = new HashMap<>();
            croppingPlanSpeciesUsageMap = new HashMap<>();
        }
    
        cropContext.put("croppingPlans", croppingPlans);
        cropContext.put("croppingPlansUsageMap", croppingPlansUsageMap);
        cropContext.put("croppingPlanSpeciesUsageMap", croppingPlanSpeciesUsageMap);
    
        jsonData = cropContext;
        
        return SUCCESS;
    }

    @Action("load-seeding-species-and-products-usages-json")
    public String getSeedingSpeciesAndProductsUsages() {

        if (filter != null) {
            String lotId = getGson().fromJson(filter, String.class);
            Optional<DomainSeedLotInput> optionalDomainSeedLotInput = domainInputStockUnitService.getLotForId(lotId);
            optionalDomainSeedLotInput.ifPresent(
                    domainSeedLotInput -> jsonData = entityUsageService.
                            getSeedingSpeciesAndProductsUsages(domainSeedLotInput.getDomainSeedSpeciesInput()));

        }
        jsonData = ObjectUtils.firstNonNull(jsonData, new HashSet<String>());

        return SUCCESS;
    }

    @Action("load-seeding-species-units-json")
    public String getSeedingSpeciesUnits() {

        List<String> speciesIds = null;
        if (filter != null) {
            speciesIds = getGson().fromJson(filter, List.class);
        }

        jsonData = domainInputStockUnitService.getSeedPlanUnits(speciesIds);

        return SUCCESS;
    }
    
    /**
     * @return A map with for Key the InputType.name and for values all the DomainInputs related to the given domainId parameter for the current InputType
     *         And for key 'UsageMap' : a map of domainInput id and the used status (true if used)
     *
     */
    @Action("domain-edit-input-stock-context-json")
    public String getDomainInputStockContext() {
        try {
            Map<String, Object> inputsUsages = new HashMap<>();
            if (StringUtils.isNotBlank(domainId)) {
                Map<InputType, UsageList<DomainInputDto>> domainInputDtosAndUsages = domainService.getDomainInputDtoUsageByTypes(domainId);
                inputsUsages.put("domainInputsAndUsages", domainInputDtosAndUsages);
                
                jsonData = inputsUsages;
                
            } else {

                inputsUsages.put("domainInputsAndUsages", new HashMap<>());
                jsonData = inputsUsages;
            }
            
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load domain Inputs", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    /**
     * Load equipments, tools couplings, refmateriel
     */
    @Action("domain-edit-equipment-context-json")
    public String getEquipmentContext() {
        try {
            ArrayList<Equipment> equipments;
            ArrayList<ToolsCoupling> toolsCouplings;
            HashMap<String, Boolean> toolsCouplingsUsage;
            if (StringUtils.isNotBlank(domainId)) {
                equipments = new ArrayList<>(domainService.getEquipments(domainId));
                UsageList<ToolsCoupling> toolsCouplingsUsageList = domainService.getToolsCouplingAndUsage(domainId);
                toolsCouplings = new ArrayList<>(toolsCouplingsUsageList.getElements());
                toolsCouplingsUsage = new HashMap<>(toolsCouplingsUsageList.getUsageMap());
    
                if (domainService.isDefaultEdaplosEquipmentForDomain(equipments)) {
                    notificationSupport.defaultMaterialUsed();
                }
            } else {
                equipments = new ArrayList<>();
                toolsCouplings = new ArrayList<>();
                toolsCouplingsUsage = new HashMap<>();
            }
    
            boolean filterEdaplos = false;
            if (filter != null) {
                TypeMaterielFilter typeMaterielFilter = getGson().fromJson(filter, TypeMaterielFilter.class);
                filterEdaplos = typeMaterielFilter.isFilterEdaplos();
            }

            Map<String, Object> equipmentContext = new HashMap<>();
            equipmentContext.put("ManualTools", referentialService.getPetitMaterielTypeMateriel1List());// ici ce n'est pas traduit
            equipmentContext.put(MaterielType.class.getSimpleName(), referentialService.getWithoutPetitMaterielTypeMateriel1List(filterEdaplos));
            equipmentContext.put(Equipment.class.getSimpleName(), equipments);
            equipmentContext.put(ToolsCoupling.class.getSimpleName(), toolsCouplings);
            equipmentContext.put(UsageList.class.getSimpleName(), toolsCouplingsUsage);
    
            jsonData = equipmentContext;
    
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load materiel-type2", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("domain-edit-materiel-type2-json")
    public String listMaterielType2() {
        try {
            TypeMaterielFilter typeMaterielFilter = getGson().fromJson(filter, TypeMaterielFilter.class);
            jsonData = referentialService.getTypeMateriel2List(typeMaterielFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load materiel-type2", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("domain-edit-materiel-type3-json")
    public String listMaterielType3() {
        try {
            TypeMaterielFilter typeMaterielFilter = getGson().fromJson(filter, TypeMaterielFilter.class);
            jsonData = referentialService.getTypeMateriel3List(typeMaterielFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load materiel-type3", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("domain-edit-materiel-type4-json")
    public String listMaterielType4() {
        try {
            TypeMaterielFilter typeMaterielFilter = getGson().fromJson(filter, TypeMaterielFilter.class);
            jsonData = referentialService.getTypeMateriel4List(typeMaterielFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load materiel-type4", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("domain-edit-materiel-unite-json")
    public String listMaterielUnite() {
        try {
            TypeMaterielFilter typeMaterielFilter = getGson().fromJson(filter, TypeMaterielFilter.class);
            jsonData = referentialService.getMaterielUniteMap(typeMaterielFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load MaterielUnite", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("domain-edit-materiel-all-json")
    public String listMaterielAll() {
        try {
            TypeMaterielFilter typeMaterielFilter = getGson().fromJson(filter, TypeMaterielFilter.class);
            jsonData = new ArrayList<>(4);
            ((Collection<Object>) jsonData).add(referentialService.getTypeMateriel2List(typeMaterielFilter));
            ((Collection<Object>) jsonData).add(referentialService.getTypeMateriel3List(typeMaterielFilter));
            ((Collection<Object>) jsonData).add(referentialService.getTypeMateriel4List(typeMaterielFilter));
            ((Collection<Object>) jsonData).add(referentialService.getMaterielUniteMap(typeMaterielFilter));

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load domain-edit-materiel-all", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("domain-edit-materiel-find-materiel")
    public String getMateriel() {
        try {
            jsonData = referentialService.getMateriel(materielId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load materiel for materielId '%s'", materielId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
    @Action("domain-edit-load-plots")
    public String loadPlots() {
        try {
            if (StringUtils.isNotBlank(domainId)) {
                Domain d = domainService.getDomain(domainId);
                jsonData = plotService.findAllForDomain(d);
            } else {
                jsonData = new ArrayList<>();
            }
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load plots for domain with id '%s'", StringUtils.isNotBlank(domainId) ? domainId : "Création"), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("domain-edit-sol-arvalis-list-json")
    public String listSolArvalis() {
        try {
            jsonData = referentialService.getSolArvalis(regionCode);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load sol arvalis", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("get-ref-otex-json")
    public String listRefOtex70() {
        try {
            jsonData = referentialService.getAllActiveCodeOtex70ByOtex18code(otex18code);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load otex 70 for otex18code '%s'", otex18code), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("check-domain-name-json")
    public String checkDomainName() {
        try {
            jsonData = domainService.checkDomainExistence(domainName);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to check valid domain name for name: " + domainName, e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("load-all-active-agrosyst-actions-json")
    public String loadAllActiveAgrosystActions() {
        try {
            jsonData = referentialService.getAllActiveAgrosystActions();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load ref agrosyst action", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("load-livestock-units-actions-json")
    public String loadLivestockUnitsActions() {
        try {
            Map<String, Object> res0 = new HashMap<>();
            res0.put("LivestocksAndCattles",StringUtils.isNotBlank(domainId) ? domainService.getUsedLivestocksAndCattles(domainId) : new HashMap<>());
            List<RefAnimalTypeDto> refAnimalTypes = referentialService.getAllTranslatedActiveRefAnimalTypes();
            List<RefCattleAnimalTypeDto> refCattleAnimalTypes = referentialService.getAllTranslatedActiveRefCattleAnimalTypes();
            List<RefCattleRationAlimentDto> refCattleRationAliments = referentialService.getAllTranslatedActiveRefCattleRationAliments();
            res0.put(RefAnimalType.class.getName(), refAnimalTypes);
            res0.put(RefCattleAnimalType.class.getName(), refCattleAnimalTypes);
            res0.put(RefCattleRationAliment.class.getName(), refCattleRationAliments);
            jsonData = res0;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load ref agrosyst action", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public void setToolsCouplingCode(String toolsCouplingCode) {
        this.toolsCouplingCode = toolsCouplingCode;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }
    
    /**
     *  Load filters if several results to phyto product id and usage unit if 1 result
     *
     *  @return DomainPhytoProductInputSearchResult Json
     *
     *  Traitement phytosanitaires : Intrants de lutte chimique et biocontrôle (Produits avec AMM)
     */
    @Action("load-phyto-products-json")
    public String loadPhytoProducts() {
        try {
            PhytoProductInputFilter phytoProductInputFilter = getGson().fromJson(filter, PhytoProductInputFilter.class);
            phytoProductInputFilter = phytoProductInputFilter.toBuilder().isWithAmmCode(true).build();
            jsonData = referentialService.getDomainPhytoProductInputSearchResults(phytoProductInputFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load phyto products", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Produits Sans AMM : nouveau terme pour désigner les produits de lutte biologique.
     * @see fr.inra.agrosyst.api.entities.AgrosystInterventionType.LUTTE_BIOLOGIQUE
     */
    @Action("load-biological-control-inputs")
    public String loadBiologicalControlInputs() {
        try {
            PhytoProductInputFilter phytoProductInputFilter = getGson().fromJson(filter, PhytoProductInputFilter.class);
            jsonData = referentialService.getDomainBiologicalControlInputsSearchResults(phytoProductInputFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load phyto products", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Load Input type prices
     *
     * @return ProductPriceSummary Json
     */
    @Action("load-input-ref-prices-json")
    public String loadInputRefPrices() {
        try {
            InputPriceFilter inputPriceFilter = getGson().fromJson(filter, InputPriceFilter.class);
            jsonData = inputPriceService.loadPriceIndications(inputPriceFilter, domainId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load input ref prices", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     *  Load ref pots
     *
     *  @return List<RefPot></RefPot> Json
     *
     */
    @Action("load-ref-pots-json")
    public String loadRefPots() {
        try {
            jsonData = referentialService.getAllActiveRefPots();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load ref pots", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Load ref substrates
     *
     * @return MultiValuedMap<String, RefSubstrate> Json
     *
     */
    @Action("load-ref-substrates-json")
    public String loadRefSubstrates() {
        try {
            MultiValuedMap<String, RefSubstrateDto> substratesByCharacteristic1 = MultiMapUtils.newSetValuedHashMap();
            List<RefSubstrateDto> allActiveRefSubstrates = referentialService.getAllActiveRefSubstrates();
            for (RefSubstrateDto substrate : allActiveRefSubstrates) {
                substratesByCharacteristic1.put(substrate.getCharacteristic1Translated(), substrate);
            }
            jsonData = substratesByCharacteristic1.asMap();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load ref substrates", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     *  Load ref other input type
     *
     *  @return List<RefPot></RefPot> Json
     *
     */
    @Action("load-ref-other-inputs-json")
    public String loadRefOtherInputs() {
        try {
            jsonData = referentialService.getAllActiveRefOtherInputs();

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load ref pots", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

}
