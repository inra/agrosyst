package fr.inra.agrosyst.web;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * Classe permettant d'accéder à un cache distribué entre les différentes instances d'Agrosyst. En fonction de la
 * configuration, ce cache sera effectivement distribué ou bien utilisera une simple Map in-memory.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.64
 */
public class AgrosystWebDistributedCacheSupport {

    private static final Log log = LogFactory.getLog(AgrosystWebDistributedCacheSupport.class);

    private static final int MAX_CACHE_EXPIRATION = 12/*h*/ * 60/*m*/ * 60/*s*/;

    private final CacheProvider instance;

    public AgrosystWebDistributedCacheSupport(AgrosystWebConfig webConfig) {
        if (webConfig.getHazelcastMembers().isPresent()) {
            log.info("Démarrage du cache distribué");
            this.instance = new HazelcastCacheProvider(webConfig);
        } else {
            log.info("Pas de cache distribué, on utilise une simple Map in-memory");
            this.instance = new InMemoryCacheProvider();
        }
    }

    public void shutdown() {
        this.instance.shutdown();
    }

    public <K, V> Map<K, V> getCache(String name) {
        Map<K, V> result = this.instance.getCache(name);
        return result;
    }

    public <K, V> Map<K, V> getCacheWithExpiration(String name, int seconds) {
        Map<K, V> result = this.instance.getCacheWithExpiration(name, seconds);
        return result;
    }

    private interface CacheProvider {

        <K, V> Map<K, V> getCache(String name);

        <K, V> Map<K, V> getCacheWithExpiration(String name, int seconds);

        void shutdown();

    }

    /**
     * Cache distribué entre les différentes instances d'Agrosyst
     */
    private static class HazelcastCacheProvider implements CacheProvider {

        protected final HazelcastInstance hazelcastInstance;

        public HazelcastCacheProvider(AgrosystWebConfig config) {
            Optional<String> members = config.getHazelcastMembers();
            Preconditions.checkState(members.isPresent(), "La liste des membres doit être explicite");
            OptionalInt port = config.getHazelcastPort();
            Optional<String> outboundPorts = config.getHazelcastOutboundPorts();
            this.hazelcastInstance = createHazelcastInstance(port, outboundPorts, members.get());
        }

        private HazelcastInstance createHazelcastInstance(OptionalInt port, Optional<String> outboundPorts, String members) {
            Config config = new Config();
            NetworkConfig networkConfig = config.getNetworkConfig();
            port.ifPresent(networkConfig::setPort);
            networkConfig.setPortAutoIncrement(false);
            outboundPorts.ifPresent(networkConfig::addOutboundPortDefinition);

            JoinConfig joinConfig = networkConfig.getJoin();
            joinConfig.getAutoDetectionConfig().setEnabled(false);
            TcpIpConfig tcpIpConfig = joinConfig.getTcpIpConfig();
            tcpIpConfig.setEnabled(true);
            tcpIpConfig.addMember(members);

            HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
            return instance;
        }

        @Override
        public <K, V> Map<K, V> getCache(String name) {
            // Aucun cache ne devrait durer plus de 12h
            this.hazelcastInstance.getConfig()
                    .getMapConfig(name)
                    .setTimeToLiveSeconds(MAX_CACHE_EXPIRATION);
            return this.hazelcastInstance.getMap(name);
        }

        @Override
        public <K, V> Map<K, V> getCacheWithExpiration(String name, int seconds) {
            Preconditions.checkArgument(seconds <= MAX_CACHE_EXPIRATION);
            this.hazelcastInstance.getConfig()
                    .getMapConfig(name)
                    .setTimeToLiveSeconds(seconds);
            Map<K, V> result = this.hazelcastInstance.getMap(name);
            return result;
        }

        @Override
        public void shutdown() {
            this.hazelcastInstance.shutdown();
        }
    }

    /**
     * Dans le cas où aucun cache distribué n'est configuré, on va utiliser un Cache Guava in-memory qui sait gérer l'expiration
     */
    private static class InMemoryCacheProvider implements CacheProvider {

        protected final Map<String, Cache<?,?>> nonDistributedCaches = new LinkedHashMap<>();

        protected <K, V> Cache<K, V> getCacheInstance(String name, OptionalInt timeoutSeconds) {
            final Cache<K, V> cache;
            if (this.nonDistributedCaches.containsKey(name)) {
                cache = (Cache<K, V>) this.nonDistributedCaches.get(name);
            } else {
                if (timeoutSeconds.isEmpty()) {
                    // Aucun cache ne devrait durer plus de 12h
                    cache = CacheBuilder.newBuilder()
                            .expireAfterWrite(MAX_CACHE_EXPIRATION, TimeUnit.SECONDS)
                            .build();
                } else {
                    int seconds = timeoutSeconds.getAsInt();
                    Preconditions.checkArgument(seconds <= MAX_CACHE_EXPIRATION);
                    cache = CacheBuilder.newBuilder()
                            .expireAfterWrite(seconds, TimeUnit.SECONDS)
                            .build();
                }
                this.nonDistributedCaches.put(name, cache);
            }
            return cache;
        }

        @Override
        public <K, V> Map<K, V> getCache(String name) {
            Cache<K, V> cacheInstance = getCacheInstance(name, OptionalInt.empty());
            ConcurrentMap<K, V> result = cacheInstance.asMap();
            return result;
        }

        @Override
        public <K, V> Map<K, V> getCacheWithExpiration(String name, int seconds) {
            Cache<K, V> cacheInstance = getCacheInstance(name, OptionalInt.of(seconds));
            ConcurrentMap<K, V> result = cacheInstance.asMap();
            return result;
        }

        @Override
        public void shutdown() {
            // Nothing to do
        }
    }

}
