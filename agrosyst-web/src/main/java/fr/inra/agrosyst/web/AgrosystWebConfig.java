package fr.inra.agrosyst.web;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.services.AgrosystServiceConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Environment;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;
import org.nuiton.config.ConfigOptionDef;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Web module configuration.
 *
 * @author Eric Chatellier
 */
public class AgrosystWebConfig {

    private static final Log LOGGER = LogFactory.getLog(AgrosystWebConfig.class);

    /**
     * Configuration filename.
     */
    private static final String AGROSYST_DEFAULT_CONF_FILENAME = "agrosyst-default.properties";
    
    public static final String NAVIGATION_CONTEXT_COOKIE_NAME = "nav.context";
    public static final String NB_ELEMENT_BY_PAGE_COOKIE_NAME = "list.nbByPage";

    private final String configFileName;
    
    /**
     * Delegate application config.
     */
    protected ApplicationConfig config;
    
    public AgrosystWebConfig(String configFileName) {
        loadConfig(configFileName);
        this.configFileName = configFileName;
    }
    
    private void loadConfig(String configFileName) {
        try {
            ApplicationConfig defaultConfig = new ApplicationConfig(AGROSYST_DEFAULT_CONF_FILENAME);
            defaultConfig.loadDefaultOptions(WebConfigOption.values());
            defaultConfig.parse();
            if (StringUtils.isNotBlank(configFileName)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Load config file name: " + configFileName);
                }
                Properties flatOptions = defaultConfig.getFlatOptions(false);
                
                config = new ApplicationConfig(flatOptions, configFileName);
                config.parse();
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(Environment.JAKARTA_JDBC_URL + " = " + config.getOption(Environment.JAKARTA_JDBC_URL));
                    LOGGER.debug(Environment.JAKARTA_JDBC_USER + " = " + config.getOption(Environment.JAKARTA_JDBC_USER));
                    LOGGER.debug(Environment.DIALECT + " = " + config.getOption(Environment.DIALECT));
                }
            } else {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("No specific configuration provided, using the default one");
                }
                config = defaultConfig;
            }
        } catch (ArgumentsParserException ex) {
            throw new AgrosystTechnicalException("Can't read configuration", ex);
        }
    }
    
    public String getConfigFileName() {
        return configFileName;
    }
    
    public void reloadConfig() {
        loadConfig(this.configFileName);
    }
    
    public String getApplicationVersion() {
        return config.getOption(WebConfigOption.APPLICATION_VERSION.key);
    }
    
    public String getApplicationBuildDate() {
        return config.getOption(WebConfigOption.APPLICATION_BUILD_DATE.key);
    }
    
    public String getApplicationRevision() {
        return config.getOption(WebConfigOption.APPLICATION_REVISION.key);
    }

    public int getListResultsPerPage() {
        return config.getOptionAsInt(WebConfigOption.LIST_RESULTS_PER_PAGE.key);
    }

    public String getIgnKeyjs() {
        return config.getOption(WebConfigOption.IGN_KEYJS.key);
    }

    public long getUploadAttachmentsMaxSize() {
        return config.getOptionAsLong(WebConfigOption.UPLOAD_ATTACHMENTS_MAX_SIZE.key);
    }

    public long getUploadGlobalMaxSize() {
        return config.getOptionAsLong(WebConfigOption.UPLOAD_GLOBAL_MAX_SIZE.key);
    }
    
    public String getUploadAllowedExtensions() {
        return config.getOption(WebConfigOption.UPLOAD_ALLOWED_EXTENSIONS.key);
    }

    public int getHttpSessionTimeout() {
        return config.getOptionAsInt(WebConfigOption.SESSION_TIMEOUT.key);
    }

    public String getContactEmail() {
        return config.getOption(WebConfigOption.CONTACT_EMAIL.getKey());
    }

    public boolean isDemoModeEnabled() {
        return config.getOptionAsBoolean(WebConfigOption.DEMO_MODE.key);
    }

    public boolean isDephygraphConnexionEnabled() {
        String dephyGraphUrl = config.getOption(WebConfigOption.DEPHYGRAPH_URL.getKey());
        return StringUtils.isNotBlank(dephyGraphUrl);
    }

    public String getInstanceBannerText() {
        return config.getOption(WebConfigOption.INSTANCE_BANNER_TEXT.key);
    }

    public String getInstanceBannerStyle() {
        return config.getOption(WebConfigOption.INSTANCE_BANNER_STYLE.key);
    }

    public int getDomainsExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.DOMAINS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getPlotsExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.PLOTS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getGrowingPlansExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.GROWING_PLANS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getGrowingSystemsExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.GROWING_SYSTEMS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getEffectiveCropCyclesExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.EFFECTIVE_CROP_CYCLES_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getEffectiveMeasurementsExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.EFFECTIVE_MEASUREMENTS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getManagementModesExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.MANAGEMENT_MODES_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getDecisionRulesExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.DECISION_RULES_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getPracticedSystemsExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.PRACTICED_SYSTEMS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getReportGrowingSystemsExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.REPORT_GROWING_SYSTEMS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public int getReportRegionalsExportAsyncThreshold() {
        return config.getOptionAsInt(WebConfigOption.REPORT_REGIONALS_EXPORT_ASYNC_THRESHOLD.key);
    }

    public String getJwtSecret() {
        String key = WebConfigOption.JWT_SECRET.key;
        String result = config.getOption(key);
        Preconditions.checkState(StringUtils.isNotEmpty(result), "La propriété " + key + " est obligatoire");
        return result;
    }

    public int getJwtRefreshSeconds() {
        int result = config.getOptionAsInt(WebConfigOption.JWT_REFRESH_SECONDS.key);
        return result;
    }

    public boolean isSecuredCookies() {
        String baseUrl = config.getOption(AgrosystServiceConfig.ServiceConfigOption.APPLICATION_BASE_URL.getKey());
        // Les cookies doivent être sécurisés dès lors qu'on est en HTTPS
        boolean result = baseUrl != null && baseUrl.startsWith("https://");
        return result;
    }

    public String getBaseUrl() {
        String baseUrl = config.getOption(AgrosystServiceConfig.ServiceConfigOption.APPLICATION_BASE_URL.getKey());
        return baseUrl != null ? baseUrl : "";
    }

    public OptionalInt getHazelcastPort() {
        int option = config.getOptionAsInt(WebConfigOption.HAZELCAST_PORT.key);
        if (option == 0) {
            return OptionalInt.empty();
        }
        return OptionalInt.of(option);
    }

    public Optional<String> getHazelcastOutboundPorts() {
        String option = config.getOption(WebConfigOption.HAZELCAST_OUTBOUND_PORTS.key);
        Optional<String> result = Optional.ofNullable(StringUtils.trimToNull(option));
        return result;
    }

    public Optional<String> getHazelcastMembers() {
        String option = config.getOption(WebConfigOption.HAZELCAST_MEMBERS.key);
        Optional<String> result = Optional.ofNullable(StringUtils.trimToNull(option));
        return result;
    }

    public ApplicationConfig.OptionList getAllowedRestRequestOrigins() {
        return config.getOptionAsList(WebConfigOption.ALLOWED_REST_REQUEST_ORIGINS.key);
    }

    public Sector[] getIpmWorksSectors() {
        List<String> sectorList = config.getOptionAsList(WebConfigOption.IPMWORKS_SECTORS.key).getOption();
        return sectorList.stream()
                .map(Sector::valueOf)
                .toArray(value -> new Sector[sectorList.size()]);
    }

    public LinkedHashMap<String, String> readConfigurationAsMap() throws ArgumentsParserException {
        ApplicationConfig configToPrint = new ApplicationConfig();
        configToPrint.setOptions(config.getFlatOptions());
        configToPrint.setOption(Environment.JAKARTA_JDBC_USER, "*****");
        configToPrint.setOption(Environment.JAKARTA_JDBC_PASSWORD, "*****");
        configToPrint.parse();
        
        for (WebConfigOption key : WebConfigOption.values()) {
            if (configToPrint.getOption(key.getKey()) == null){
                configToPrint.setOption(key.getKey(), key.getDefaultValue());
            }
        }
        
        for (AgrosystServiceConfig.ServiceConfigOption key : AgrosystServiceConfig.ServiceConfigOption.values()) {
            if (configToPrint.getOption(key.getKey()) == null){
                configToPrint.setOption(key.getKey(), key.getDefaultValue());
            }
        }
    
        HashMap<String, String> agrosystKeyValues = new HashMap<>();
        HashMap<String, String> otherKeyValues = new HashMap<>();
        for (String key : configToPrint.getFlatOptions().stringPropertyNames()) {
            String value = configToPrint.getOption(key);
            if (key.matches("agrosyst.*")) {
                
                agrosystKeyValues.put(key, value);
            } else {
                otherKeyValues.put(key,value);
            }
        }
    
        LinkedHashMap<String, String> agrosystKeyValueSortedByKey = agrosystKeyValues.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    
        LinkedHashMap<String, String> otherKeyValuesSortedByKey = otherKeyValues.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    
        agrosystKeyValueSortedByKey.putAll(otherKeyValuesSortedByKey);
        
        return agrosystKeyValueSortedByKey;
    }

    enum WebConfigOption implements ConfigOptionDef {

//        FILENAME(ApplicationConfig.CONFIG_FILE_NAME, AGROSYST_DEFAULT_CONF_FILENAME),
        
        APPLICATION_VERSION("agrosyst.web.application.version", null),

        APPLICATION_BUILD_DATE("agrosyst.web.application.buildDate", null),

        APPLICATION_REVISION("agrosyst.web.application.revision", null),

        LIST_RESULTS_PER_PAGE("agrosyst.web.list.results.per.page", "10"),
        
        IGN_KEYJS("agrosyst.web.ign.keyjs", null),
        
        SESSION_TIMEOUT("agrosyst.web.sessionTimeout", "7200"),

        CONTACT_EMAIL("agrosyst.web.email.contact", "agrosyst-devel@list.forge.codelutin.com"),
        
        DEMO_MODE("agrosyst.services.demoMode", "false"),

        /**
         * Upload config
         */
        UPLOAD_ATTACHMENTS_MAX_SIZE("agrosyst.web.upload.attachmentsMaxSize", "10240000"),
        // check struts.multipart.maxSize=51200000 to be same value
        UPLOAD_GLOBAL_MAX_SIZE("agrosyst.web.upload.globalMaxSize", "51200000"),

        UPLOAD_ALLOWED_EXTENSIONS("agrosyst.web.upload.allowedExtensions", ""),

        INSTANCE_BANNER_TEXT("agrosyst.web.banner.text", null),

        /** Error, warning, info.*/
        INSTANCE_BANNER_STYLE("agrosyst.web.banner.style", null),

        /* Same in AgrosystServiceConfig#ServiceConfigOption */
        DEPHYGRAPH_URL("agrosyst.web.dephyGraphUrl", null),
    
        DOMAINS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.domainsExportAsyncThreshold", "5"),
        PLOTS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.plotsExportAsyncThreshold", "5"),
        GROWING_PLANS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.growingPlansExportAsyncThreshold", "5"),
        GROWING_SYSTEMS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.growingSystemsExportAsyncThreshold", "5"),
        EFFECTIVE_CROP_CYCLES_EXPORT_ASYNC_THRESHOLD("agrosyst.web.effectiveCropCyclesExportAsyncThreshold", "5"),
        EFFECTIVE_MEASUREMENTS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.effectiveMeasurementsExportAsyncThreshold", "5"),
        MANAGEMENT_MODES_EXPORT_ASYNC_THRESHOLD("agrosyst.web.managementModesExportAsyncThreshold", "5"),
        DECISION_RULES_EXPORT_ASYNC_THRESHOLD("agrosyst.web.decisionRulesExportAsyncThreshold", "5"),
        PRACTICED_SYSTEMS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.practicedSystemsExportAsyncThreshold", "5"),
        REPORT_GROWING_SYSTEMS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.reportGrowingSystemsExportAsyncThreshold", "5"),
        REPORT_REGIONALS_EXPORT_ASYNC_THRESHOLD("agrosyst.web.reportRegionalsExportAsyncThreshold", "5"),

        JWT_SECRET("agrosyst.web.jwtSecret", null),
        JWT_REFRESH_SECONDS("agrosyst.web.jwtRefreshSeconds", "300"), // 5 minutes

        HAZELCAST_PORT("agrosyst.web.hazelcastPort", null), // 5701 par défaut
        HAZELCAST_OUTBOUND_PORTS("agrosyst.web.hazelcastOutboundPorts", null),
        HAZELCAST_MEMBERS("agrosyst.web.hazelcastMembers", null),

        // Allowed origins from which rest requests can come (eg IPMWorks)
        ALLOWED_REST_REQUEST_ORIGINS("agrosyst.web.allowedRestRequestOrigins", null),
        IPMWORKS_SECTORS("ipmworks.sectors", "ARBORICULTURE,GRANDES_CULTURES,HORTICULTURE,MARAICHAGE,POLYCULTURE_ELEVAGE,VITICULTURE");

        private final String key;
        private final String defaultValue;
    
        WebConfigOption(String key, String defaultValue) {
            this.key = key;
            this.defaultValue = defaultValue;
        }
    
        @Override
        public String getKey() {
            return key;
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public Class<?> getType() {
            return null;
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public boolean isTransient() {
            return false;
        }

        @Override
        public boolean isFinal() {
            return false;
        }

        @Override
        public void setDefaultValue(String defaultValue) {

        }

        @Override
        public void setTransient(boolean isTransient) {

        }

        @Override
        public void setFinal(boolean isFinal) {

        }
    }
}
