package fr.inra.agrosyst.web.actions.growingsystems;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.conversion.annotations.Conversion;
import com.opensymphony.xwork2.conversion.annotations.TypeConversion;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemCharacteristicType;
import fr.inra.agrosyst.api.entities.GrowingSystemImpl;
import fr.inra.agrosyst.api.entities.MarketingDestinationObjective;
import fr.inra.agrosyst.api.entities.ModalityDephy;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.managementmode.CategoryStrategy;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.PlotDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Action d'edition d'un système de culture.
 *
 * @author Eric Chatellier
 */
@Conversion()
public class GrowingSystemsEdit extends AbstractAgrosystAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(GrowingSystemsEdit.class);

    @Serial
    private static final long serialVersionUID = 2739566488786458389L;

    protected transient DomainService domainService;

    protected transient GrowingSystemService growingSystemService;

    protected transient PlotService plotService;

    protected transient NetworkService networkService;

    protected transient GrowingPlanService growingPlanService;

    protected transient ReferentialService referentialService;

    protected String growingSystemTopiaId;

    /**
     * Edited instance.
     */
    protected GrowingSystem growingSystem;

    protected double domainSAUArea;

    /**
     * To display timeline.
     */
    protected LinkedHashMap<Integer, String> relatedGrowingSystems;

    protected List<PlotDto> availablePlots;

    protected GrowingPlan growingPlan;

    protected String growingPlanTopiaId;

    protected GrowingPlan selectedGrowingPlan; // edit only

    protected List<String> selectedPlotsIds;

    protected List<String> growingSystemNetworkIds;

    protected List<RefTypeAgriculture> typeAgricultures;

    protected String typeAgricultureTopiaId;

    /**
     * List all growing system characteristics
     */
    protected List<RefTraitSdC> growingSystemCharacteristics;

    /**
     * Selected GrowingSystemCharacteristics
     */
    protected List<String> growingSystemCharacteristicIds;

    /**
     * Listes des systemes synthetisés associés au SdC (via le code de prolongation).
     */
    protected List<PracticedSystem> practicedSystems;

    /**
     * Listes des modèles décisionnels associés au SdC.
     */
    protected List<ManagementMode> managementModes;

    /**
     * marketing destination for the growing system sectors
     */
    protected Map<Sector, List<MarketingDestinationObjective>> marketingDestinationsBySector;

    protected String missingTypeAgricultureTopiaId;

    protected static final String PERCENT_FIELD = "La valeur doit être comprise entre 0 et 100";
    protected static final String INCOHERENT_STARTING_AND_ENDING_DATES = "Les dates de début et de fin sont incohérentes";

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }


    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setGrowingSystemCharacteristicIds(List<String> growingSystemCharacteristicIds) {
        this.growingSystemCharacteristicIds = growingSystemCharacteristicIds;
    }

    @Override
    public void prepare() {
        missingTypeAgricultureTopiaId = growingSystemService.getMissingTypeAgricultureTopiaId();

        if (StringUtils.isEmpty(growingSystemTopiaId)) {
            // Cas de création d'un growingSystem
            growingSystem = growingSystemService.newGrowingSystem();
        } else {
            // TODO AThimel 07/10/13 May be included directly in the service ?
            authorizationService.checkGrowingSystemReadable(growingSystemTopiaId);

            // Cas d'une mise à jour de growingSystem
            readOnly = !authorizationService.isGrowingSystemWritable(growingSystemTopiaId);

            growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
            growingPlanTopiaId = growingSystem.getGrowingPlan().getTopiaId();

            activated = growingSystem.isActive() && growingSystem.getGrowingPlan().isActive() && growingSystem.getGrowingPlan().getDomain().isActive();

            if (readOnly || Strings.isNullOrEmpty(growingPlanTopiaId)) {
                notificationSupport.growingSystemNotWritable();
                readOnly = true;
            }

            marketingDestinationsBySector = growingSystemService.loadMarketingDestinationObjective(growingSystem);
        }

        if (!growingSystem.isPersisted() && StringUtils.isNotBlank(growingPlanTopiaId)) {
            GrowingPlan growingPlan = growingPlanService.getGrowingPlan(growingPlanTopiaId);
            growingSystem.setGrowingPlan(growingPlan);
        }
    }

    public GrowingSystem getGrowingSystem() {
        // AThimel 26/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(growingSystem, GrowingSystemImpl::new);
    }

    @Override
    @Action("growing-systems-edit-input")
    public String input() {

        GrowingSystem growingSystem = getGrowingSystem();
        if (growingSystem.getGrowingPlan() != null) {
            growingPlanTopiaId = growingSystem.getGrowingPlan().getTopiaId();
        }
        if (growingSystem.getTypeAgriculture() != null) {
            typeAgricultureTopiaId = growingSystem.getTypeAgriculture().getTopiaId();
        }

        initForInput();

        // initialise la saisie des checkbox de selection
        if (growingSystem.isPersisted()) {
            selectedPlotsIds = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(availablePlots)) {
                for (PlotDto plot : availablePlots) {
                    if (plot.getGrowingSystem() != null && growingSystem.getTopiaId().equals(plot.getGrowingSystem().getTopiaId())) {
                        selectedPlotsIds.add(plot.getTopiaId());
                    }
                }
            }

            if (growingSystemService.isMissingTypeAgriculture(growingSystem)) {
                notificationSupport.error(
                        "Le type de conduite n'est pas renseigné. Merci de préciser le type de conduite dans l'onglet 'Généralités'");
            }
        }

        if (growingSystem.getCharacteristics() != null) {
            growingSystemCharacteristicIds = growingSystem.getCharacteristics().stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        } else {
            growingSystemCharacteristicIds = new ArrayList<>();
        }

        return INPUT;
    }

    @Override
    protected void initForInput() {
        // warning, growingSystem's must always be part of growingPlan set
        // even not selected be navigation context
        GrowingSystem growingSystem = getGrowingSystem();
        if (growingSystem.isPersisted()) {
            // timeline
            relatedGrowingSystems = growingSystemService.getRelatedGrowingSystemIdsByCampaigns(growingSystem.getCode());

            // practiced systems linked to current growing system
            practicedSystems = growingSystemService.getPracticedSystems(growingSystem.getCode());
            
            // modèle décisionnel associé au sdc
            managementModes = growingSystemService.getManagementModes(growingSystem.getTopiaId());
    
            // plots tab
            availablePlots = plotService.findAllFreeAndGrowingSystemPlots(growingSystem, growingPlanTopiaId);
    
            // surface total du domain
            domainSAUArea = domainService.getDomainSAUArea(growingSystem.getGrowingPlan().getDomain().getTopiaId());
            
        } else {
            // plots tab
            if (StringUtils.isNotBlank(growingPlanTopiaId)) {
                selectedGrowingPlan = growingPlanService.getGrowingPlan(growingPlanTopiaId);
                availablePlots = plotService.findAllFreeAndGrowingSystemPlots(null, growingPlanTopiaId);
            }
        }

        typeAgricultures = growingSystemService.findAllTypeAgricultures();
        growingSystemCharacteristics = referentialService.getAllActiveGrowingSystemCharacteristics();
    }

    @Override
    public void validate() {
        // Valid if the growing system :
        //  is attached to a growingPlan
        if (StringUtils.isBlank(growingPlanTopiaId)) {
            addFieldError("growingPlanTopiaId", getText(REQUIRED_FIELD));
        }

        if (growingSystem.isPersisted() && !activated) {
            addActionError("Le systeme de culture et/ou son dispositif et/ou son domain sont inactifs !");
        }

        //  has a name
        if (StringUtils.isBlank(growingSystem.getName())) {
            addFieldError("growingSystem.name", getText(REQUIRED_FIELD));
        }
        //  has startingDate
        LocalDate startingDate = growingSystem.getStartingDate();
        if (startingDate == null) {
            addFieldError("growingSystem.startingDate", getText(REQUIRED_FIELD));
        }

        // Filière
        if (growingSystem.getSector() == null) {
            addFieldError("growingSystem.sector", getText(REQUIRED_FIELD));
        }

        if (Sector.MARAICHAGE == growingSystem.getSector() && growingSystem.getProduction() == null) {
            addFieldError("growingSystem.production", getText(REQUIRED_FIELD));
            addActionError("Le type de production doit être renseigné pour les filières maraîchage");
        }

        if (Sector.HORTICULTURE == growingSystem.getSector() && growingSystem.getProduction() == null) {
            addFieldError("growingSystem.production", getText(REQUIRED_FIELD));
            addActionError("Le type de production doit être renseigné pour les filières horticulture");
        }

        //  has endingDate
        LocalDate endingDate = growingSystem.getEndingDate();
        if (startingDate != null && endingDate != null) {
            if (endingDate.isBefore(startingDate)) {
                addFieldError("growingSystem.startingDate", INCOHERENT_STARTING_AND_ENDING_DATES);
                addFieldError("growingSystem.endingDate", INCOHERENT_STARTING_AND_ENDING_DATES);
            }
        }

        // affectedAreaRate
        Double affectedAreaRate = growingSystem.getAffectedAreaRate();
        if (affectedAreaRate != null) {
            if (affectedAreaRate < 0 || affectedAreaRate > 100) {
                addFieldError("growingSystem.affectedAreaRate", PERCENT_FIELD);
            }
        }

        // affectedWorkForceRate
        Integer affectedWorkForceRate = growingSystem.getAffectedWorkForceRate();
        if (affectedWorkForceRate != null) {
            if (affectedWorkForceRate < 0 || affectedWorkForceRate > 100) {
                addFieldError("growingSystem.affectedWorkForceRate", PERCENT_FIELD);

            }
        }
        // domainsToolsUsageRate
        Integer domainsToolsUsageRate = growingSystem.getDomainsToolsUsageRate();
        if (domainsToolsUsageRate != null) {
            if (domainsToolsUsageRate < 0 || domainsToolsUsageRate > 100) {
                addFieldError("growingSystem.domainsToolsUsageRate", PERCENT_FIELD);
            }
        }

        if (StringUtils.isBlank(typeAgricultureTopiaId)
                || !typeAgricultureTopiaId.startsWith(RefTypeAgriculture.class.getName())
                || typeAgricultureTopiaId.equals(missingTypeAgricultureTopiaId)) {
            addFieldError("typeAgricultureTopiaId", getText(REQUIRED_FIELD));
            addActionError("Le type de conduite est obligatoire");
        }

        if (growingSystem.getGrowingPlan() != null && TypeDEPHY.DEPHY_FERME.equals(growingSystem.getGrowingPlan().getType())) {

            if (growingSystem.getModality() == null) {
                addFieldError("growingSystem.modality", getText(REQUIRED_FIELD));
                addActionError("Le modalité de suivi dans DEPHY est obligatoire");
            }
    
            if (ModalityDephy.DETAILLE.equals(growingSystem.getModality()) && StringUtils.isBlank(growingSystem.getDephyNumber())) {
                addFieldError("growingSystem.dephyNumber", getText(REQUIRED_FIELD));
                addActionError("Le numéro DEPHY est obligatoire");
            }
        }

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                String growingSystemId = getLogEntityId(growingSystem);
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : gs:'%s' -> %s", growingSystemId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : gs:'%s' -> %s", growingSystemId, getFieldErrors().toString()));
            }

            initForInput();
        }
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "growing-systems-edit-input", "growingSystemTopiaId", "${growingSystem.topiaId}"})})
    public String execute() throws Exception {

        growingSystem = growingSystemService.createOrUpdateGrowingSystem(growingSystem, growingSystemNetworkIds, selectedPlotsIds, typeAgricultureTopiaId, growingSystemCharacteristicIds, marketingDestinationsBySector);
        notificationSupport.growingSystemSaved(growingSystem);

        if (Strings.isNullOrEmpty(growingSystemTopiaId)) {
            navigationContextEntityCreated(growingSystem);
        }

        return SUCCESS;
    }

    public void setGrowingSystem(GrowingSystem growingSystem) {
        this.growingSystem = growingSystem;
    }

    public Map<String, Boolean> getSelectedPlotsIds() {
        Map<String, Boolean> result;
        if (selectedPlotsIds == null) {
            result = new HashMap<>();
        } else {
            result = Maps.toMap(selectedPlotsIds, AbstractJsonAction.GET_TRUE::apply);
        }
        return result;
    }

    public List<PlotDto> getAvailablePlots() {
        return availablePlots;
    }

    public String getGrowingSystemTopiaId() {
        return growingSystemTopiaId;
    }

    public void setGrowingSystemTopiaId(String growingSystemTopiaId) {
        this.growingSystemTopiaId = growingSystemTopiaId;
    }

    public String getGrowingPlanTopiaId() {
        return growingPlanTopiaId;
    }

    public void setGrowingPlanTopiaId(String growingPlanTopiaId) {
        this.growingPlanTopiaId = growingPlanTopiaId;
    }

    public void setSelectedPlotsIds(List<String> selectedPlotsIds) {
        this.selectedPlotsIds = selectedPlotsIds;
    }

    public Map<CategoryStrategy, String> getCategoryStrategies() {
        return getEnumAsMap(CategoryStrategy.values());
    }

    public LinkedHashMap<Integer, String> getRelatedGrowingSystems() {
        return relatedGrowingSystems;
    }

    public void setGrowingSystemNetworkIds(List<String> growingSystemNetworkIds) {
        this.growingSystemNetworkIds = growingSystemNetworkIds;
    }

    public List<RefTypeAgriculture> getTypeAgricultures() {
        return typeAgricultures;
    }

    public String getTypeAgricultureTopiaId() {
        return typeAgricultureTopiaId;
    }

    public void setTypeAgricultureTopiaId(String typeAgricultureTopiaId) {
        this.typeAgricultureTopiaId = typeAgricultureTopiaId;
    }

    public List<PracticedSystem> getPracticedSystems() {
        return practicedSystems;
    }

    public List<ManagementMode> getManagementModes() {
        return managementModes;
    }

    /**
     * Attention : le getter a bien le même nom que la variable + get !
     */
    public List<RefTraitSdC> getGrowingSystemCharacteristics() {
        return growingSystemCharacteristics;
    }

    public Map<GrowingSystemCharacteristicType, String> getGrowingSystemCharacteristicTypes() {
        return getEnumAsMap(GrowingSystemCharacteristicType.values());
    }

    public List<String> getGrowingSystemCharacteristicIds() {
        return growingSystemCharacteristicIds;
    }

    public double getDomainSAUArea() {
        return domainSAUArea;
    }

    @TypeConversion(converter = "fr.inra.agrosyst.web.actions.commons.AgrosystStringToDateConverter")
    public void setGrowingSystemStartingDate(LocalDate date) {
        GrowingSystem gs = getGrowingSystem();
        gs.setStartingDate(date);
    }

    @TypeConversion(converter = "fr.inra.agrosyst.web.actions.commons.AgrosystStringToDateConverter")
    public void setGrowingSystemEndingDate(LocalDate date) {
        GrowingSystem gs = getGrowingSystem();
        gs.setEndingDate(date);
    }

    public Map<Sector, List<MarketingDestinationObjective>> getMarketingDestinationsBySector() {
        return marketingDestinationsBySector;
    }

    public void setMarketingDestinationsBySector(String marketingDestinationsBySector) {
        this.marketingDestinationsBySector = convertMarketingDestinationObjectiveJson(marketingDestinationsBySector);
    }

    protected Map<Sector, List<MarketingDestinationObjective>> convertMarketingDestinationObjectiveJson(String json) {
        Type type = new TypeToken<Map<Sector, List<MarketingDestinationObjective>>>() {
        }.getType();

        return getGson().fromJson(json, type);
    }

    public GrowingPlan getSelectedGrowingPlan() {
        return selectedGrowingPlan;
    }

    public Map<TypeDEPHY, String> getTypesDephy() {
        return getEnumAsMap(TypeDEPHY.DEPHY_FERME, TypeDEPHY.DEPHY_EXPE, TypeDEPHY.NOT_DEPHY);
    }

    public String getMissingTypeAgricultureTopiaId() {
        return missingTypeAgricultureTopiaId;
    }
}
