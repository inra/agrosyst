package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by davidcosse on 30/06/14.
 */
public class DomainMaterielsCopy extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 1L;

    private static final Log LOGGER = LogFactory.getLog(DomainMaterielsCopy.class);
    
    protected transient String fromDomain;

    protected transient String toDomains;

    protected transient Boolean includeToolsCouplings;

    protected transient List<Equipment> equipments;
    
    protected transient List<ToolsCoupling> toolsCouplings;

    protected transient DomainService domainService;
    
    @Override
    public String execute() throws Exception {
        try {
            List<String> targetedDomainIds = getTargetedDomainIds(toDomains);
            jsonData = domainService.copyTools(fromDomain, targetedDomainIds, includeToolsCouplings, equipments, toolsCouplings);
            return SUCCESS;
        } catch (IllegalArgumentException e) {
            jsonData = e.getClass().getSimpleName();
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("copy tools from domain '%s' to domains '%s' failed", fromDomain, toDomains), e);
            }
            return ERROR;
        } catch (Exception e) {
            return ERROR;
        }

    }

    public void setFromDomain(String fromDomain) {
        this.fromDomain = fromDomain;
    }

    public void setIncludeToolsCouplings(Boolean includeToolsCouplings) {
        this.includeToolsCouplings = includeToolsCouplings;
    }

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setToDomains(String json) {
        this.toDomains = json;

    }

    protected List<String> getTargetedDomainIds(String json) {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> targetedDomains = getGson().fromJson(json, type);
        return targetedDomains;
    }

    public void setEquipmentsJson(String json) {
        try {
            Type type = new TypeToken<List<Equipment>>() {
            }.getType();
            this.equipments = getGson().fromJson(json, type);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Echec de déserialisation des équipements: '" + json + "' :" + e.getMessage());
            }
        }

    }

    public void setToolsCouplingsJson(String json) {
        try {
            Type type = new TypeToken<List<ToolsCoupling>>() {
            }.getType();
            this.toolsCouplings = getGson().fromJson(json, type);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Echec de déserialisation des combinaisons d'outils: '" + json + "' :" + e.getMessage());
            }
        }
    }
}
