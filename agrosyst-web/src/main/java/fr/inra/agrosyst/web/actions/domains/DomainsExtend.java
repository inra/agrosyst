package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Prolongation d'un domaine.
 * 
 * @author Eric Chatellier
 */
public class DomainsExtend extends AbstractAgrosystAction {

    private static final Log LOGGER = LogFactory.getLog(DomainsExtend.class);

    @Serial
    private static final long serialVersionUID = 2031204987636332755L;

    protected transient DomainService domainService;

    /** Au pluriel car interface communes avec la liste, ne pas modifier. */
    protected String domainIds;

    /** Campagne associée au nouveau domaine. */
    protected int extendCampaign;

    /** Duplicated domain. */
    protected Domain domain;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setDomainIds(String domainIds) {
        List<String> list = getGson().fromJson(domainIds, List.class);
        if (CollectionUtils.isNotEmpty(list)) {
            this.domainIds = list.getFirst();
        }
    }

    public void setExtendCampaign(int extendCampaign) {
        this.extendCampaign = extendCampaign;
    }
    
    
    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "domains-edit-input", "domainTopiaId", "${domain.topiaId}"}),
            @Result(name = ERROR, type = "redirectAction", params = {"actionName", "domains-list"})})
    public String execute() throws Exception {
        try {
            domain = domainService.extendDomain(domainIds, extendCampaign);

            navigationContextEntityCreated(domain);

        } catch (DomainExtendException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("For user email:" + getAuthenticatedUser().getEmail() + ":failed to extends domains '" + domainIds + "' to campaigns: " + extendCampaign + ", the domain must exists for this campaign !", e);
            }
            notificationSupport.domainExtendError(String.format("Ce domaine existe déjà en %d. La prolongation sur cette campagne n'est pas autorisée.",
                    extendCampaign));
            return ERROR;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":Échec de prolongation du domaine '%s' campagne: '%d'", domainIds, extendCampaign), e);
            }
            notificationSupport.domainExtendError("Échec de prolongation du domaine. " + e.getMessage());
            return ERROR;
        }

        return SUCCESS;
    }

    public Domain getDomain() {
        return domain;
    }
}
