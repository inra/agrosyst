package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotFilter;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import lombok.Getter;
import lombok.Setter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

@Getter
@Setter
public class PracticedPlotsList extends AbstractAgrosystAction {

    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = 3084229660685255829L;

    protected PracticedPlotFilter practicedPlotFilter;

    protected transient PracticedPlotService practicedPlotService;

    /**
     * Initial list data.
     */
    protected PaginationResult<PracticedPlotDto> practicedPlots;

    @Override
    public String execute() throws Exception {

        NavigationContext navigationContext = getNavigationContext();
        practicedPlotFilter = new PracticedPlotFilter();
        practicedPlotFilter.setNavigationContext(navigationContext);
        practicedPlotFilter.setPageSize(getListNbElementByPage(PracticedPlotDto.class));

        practicedPlots = practicedPlotService.getFilteredPracticedPlotsDto(practicedPlotFilter);
        return SUCCESS;
    }

}
