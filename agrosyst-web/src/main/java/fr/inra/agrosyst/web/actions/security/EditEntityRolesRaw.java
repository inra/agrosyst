package fr.inra.agrosyst.web.actions.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.security.UserRoleDto;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class EditEntityRolesRaw extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -8587272104576117396L;

    protected transient GrowingSystemService growingSystemService;

    protected RoleType roleType;
    protected String entityCode;
    protected List<UserRoleDto> roles;
    protected List<Integer> availableCampaigns;

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    @Override
    public String execute() throws Exception {

        readOnly = true;
//        boolean loadRoles = true;
        switch (roleType) {
            case DOMAIN_RESPONSIBLE -> readOnly = !authorizationService.isDomainAdministrable(entityCode);

//                if (readOnly) {
//                    loadRoles = !authorizationService.shouldAnonymizeDomainByCode(entityCode);
//                }
            case GROWING_PLAN_RESPONSIBLE -> readOnly = !authorizationService.isGrowingPlanAdministrable(entityCode);

//                if (readOnly) {
//                    loadRoles = !authorizationService.shouldAnonymizeGrowingPlanByCode(entityCode);
//                }
            case GS_DATA_PROCESSOR -> readOnly = !authorizationService.isGrowingSystemAdministrable(entityCode);
            default -> throw new UnsupportedOperationException("Unexpected role type: " + roleType);
        }

//        if (loadRoles) {
            roles = authorizationService.getEntityRoles(roleType, entityCode);
//        }

        if (RoleType.GS_DATA_PROCESSOR.equals(roleType)) {
            availableCampaigns = growingSystemService.getGrowingSystemCampaigns(entityCode);
        } else {
            availableCampaigns = new ArrayList<>();
        }

        return SUCCESS;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public List<UserRoleDto> getRoles() {
        return roles;
    }

    public List<Integer> getAvailableCampaigns() {
        return availableCampaigns;
    }

    public Map<RoleType, String> getRoleTypes() {
        return getEnumAsMap(RoleType.values());
    }

}
