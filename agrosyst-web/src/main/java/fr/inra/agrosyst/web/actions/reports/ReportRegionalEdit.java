/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.referential.BioAgressorParentType;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.entities.report.PestPressure;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.report.ReportRegionalImpl;
import fr.inra.agrosyst.api.services.referential.GroupeCibleDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Edition d'un bilan de campagne.
 */
public class ReportRegionalEdit extends AbstractReportAction implements Preparable {
    
    @Serial
    private static final long serialVersionUID = -6750424949212885687L;
    
    protected ReportRegional reportRegional;

    protected transient ReportService reportService;

    protected transient ReferentialService referentialService;

    protected String reportRegionalTopiaId;

    protected List<String> reportRegionalNetworkIds;

    protected String pestPressuresJson;

    protected String diseasePressuresJson;

    protected Set<String> networkIdsUsed;

    protected List<GroupeCibleDTO> groupesCibles;

    // use LinkedHashMap to keep ordering
    protected LinkedHashMap<Integer, String> relatedReportRegionals;

    public ReportRegional getReportRegional() {
        // AThimel 26/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(reportRegional, ReportRegionalImpl::new);
    }

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setReportRegionalTopiaId(String reportRegionalTopiaId) {
        this.reportRegionalTopiaId = reportRegionalTopiaId;
    }

    public void setReportRegionalNetworkIds(List<String> reportRegionalNetworkIds) {
        this.reportRegionalNetworkIds = reportRegionalNetworkIds;
    }

    @Override
    public void prepare() {
        if (StringUtils.isEmpty(reportRegionalTopiaId)) {
            reportRegional = new ReportRegionalImpl();

            AuthenticatedUser authenticatedUser = getAuthenticatedUser();
            reportRegional.setAuthor(authenticatedUser.getFirstName() + " " + authenticatedUser.getLastName());

            networkIdsUsed = new HashSet<>();
        } else {
            // TODO AThimel 07/10/13 May be included directly in the service ?
            authorizationService.checkReportRegionalReadable(reportRegionalTopiaId);

            reportRegional = reportService.getReportRegional(reportRegionalTopiaId);

            networkIdsUsed = reportService.getNetworkIdsUsed(reportRegional);
        }
    }

    @Override
    protected void initForInput() {
        if (StringUtils.isNotEmpty(reportRegionalTopiaId)) {
            relatedReportRegionals = reportService.getRelatedReportRegionals(reportRegional.getCode());
        }
        groupesCibles = referentialService.getGroupesCibles();
    }

    @Override
    @Action("report-regional-edit-input")
    public String input() throws Exception {

        initForInput();

        if (StringUtils.isNotBlank(reportRegionalTopiaId)) {

            readOnly = !authorizationService.isReportRegionalWritable(reportRegionalTopiaId);
            if (readOnly) {
                notificationSupport.reportRegionalNotWritable();
            }
        }

        return INPUT;
    }

    @Override
    public void validate() {

        if (reportRegional.getCampaign() <= 1900 || reportRegional.getCampaign() >= 2100) {
            addFieldError("reportRegional.campaign", getText(REQUIRED_FIELD));
        }

        if (StringUtils.isBlank(reportRegional.getName())) {
            addFieldError("reportRegional.name", getText(REQUIRED_FIELD));
        }

        if (StringUtils.isBlank(reportRegional.getAuthor())) {
            addFieldError("reportRegional.author", getText(REQUIRED_FIELD));
        }

        if (CollectionUtils.isEmpty(reportRegional.getSectors())) {
            addFieldError("reportRegional.sectors", getText(REQUIRED_FIELD));
        } else {
            if (reportRegional.getSectors().contains(Sector.ARBORICULTURE)) {
                if (CollectionUtils.isEmpty(reportRegional.getSectorSpecies())) {
                    addFieldError("reportRegional.sectorSpecies", getText(REQUIRED_FIELD));
                }
            }
        }

        if (CollectionUtils.isEmpty(reportRegionalNetworkIds)) {
            addFieldError("reportRegionalNetworkIds", getText(REQUIRED_FIELD));
        }

        if (hasErrors()) {
            initForInput();
        }
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "report-regional-edit-input", "reportRegionalTopiaId", "${reportRegional.topiaId}"})})
    public String execute() throws Exception {
        // manage json
        Type collectionType = new TypeToken<List<PestPressure>>(){}.getType();
        List<PestPressure> diseasePressures = getGson().fromJson(diseasePressuresJson, collectionType);
        List<PestPressure> pestPressures = getGson().fromJson(pestPressuresJson, collectionType);
        // save
        reportRegional = reportService.createOrUpdateReportRegional(reportRegional, reportRegionalNetworkIds, diseasePressures, pestPressures);
        notificationSupport.reportRegionalSaved(reportRegional);
        return SUCCESS;
    }

    public void setDiseasePressuresJson(String diseasePressuresJson) {
        this.diseasePressuresJson = diseasePressuresJson;
    }

    public String getDiseasePressuresJson() {
        return diseasePressuresJson;
    }

    public void setPestPressuresJson(String pestPressuresJson) {
        this.pestPressuresJson = pestPressuresJson;
    }

    public String getPestPressuresJson() {
        return pestPressuresJson;
    }

    public LinkedHashMap<Integer, String> getRelatedReportRegionals() {
        return relatedReportRegionals;
    }

    public Set<String> getNetworkIdsUsed() {
        return networkIdsUsed;
    }

    public List<GroupeCibleDTO> getGroupesCibles() {
        return groupesCibles;
    }

    public BioAgressorParentType getDiseaseBioAgressorParentType() {
        return Referentials.MALADIE;
    }

    public BioAgressorParentType getPestBioAgressorParentType() {
        return Referentials.RAVAGEUR;
    }
}
