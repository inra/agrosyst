/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.services.report.ReportService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Suppression des bilan de campagne / echelle system de culture.
 */
public class ReportGrowingSystemDelete extends AbstractReportAction {
    
    @Serial
    private static final long serialVersionUID = 26497581287111148L;
    
    protected transient ReportService reportService;

    protected List<String> reportGrowingSystemIds;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReportGrowingSystemIds(String reportGrowingSystemIds) {
        this.reportGrowingSystemIds = getGson().fromJson(reportGrowingSystemIds, List.class);
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "report-growing-systems-list"})})
    public String execute() {
        reportService.deleteReportGrowingSystems(reportGrowingSystemIds);
        return SUCCESS;
    }
}
