package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Chargement des espèces d'une culture si vraiment l'utilisateur change la culture d'un
 * cycle de cultures pérennes.
 * 
 * @author cosse
 */
public class LoadPracticedPerennialCroppingPlanSpeciesJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadPracticedPerennialCroppingPlanSpeciesJson.class);

    @Serial
    private static final long serialVersionUID = 4680946995049096766L;

    protected transient PracticedSystemService practicedSystemService;

    protected transient String croppingPlanEntryCode;

    protected transient String campaigns;
    
    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }

    public void setCampaigns(String campaigns) {
        this.campaigns = campaigns;
    }

    public void setCroppingPlanEntryCode(String croppingPlanEntryCode) {
        this.croppingPlanEntryCode = croppingPlanEntryCode;
    }

    @Override
    public String execute() throws Exception {
        try {
            jsonData = practicedSystemService.getCropCyclePerennialSpecies(croppingPlanEntryCode, null, campaigns);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failes to load cropCyclePerennialSpeciesDtos for croppingPlanEntryCode '%s' and campaigns '%s'",
                        croppingPlanEntryCode, campaigns), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
