package fr.inra.agrosyst.web.rest.performances;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.services.performance.indicators.IndicatorDirectMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorEquipmentsExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorGrossIncome;
import fr.inra.agrosyst.services.performance.indicators.IndicatorGrossMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorSemiNetMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorStandardisedGrossIncome;
import fr.inra.agrosyst.services.performance.indicators.IndicatorToolUsageTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTotalWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTotalWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTransitCount;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorRefMaxYearCropIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorRefMaxYearTargetIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageCropIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageTargetIFT;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorDecomposedOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorActiveSubstanceAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCMR;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCMRUses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperFertilisationProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperPhytoProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperTotalProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorEnvironmentalRisk;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorEnvironmentalRiskUses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G1;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G2;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G3;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G4;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorLowRiskSubstances;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSubstancesCandidateToSubstitution;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurFertilisationProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurPhytoProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurTotalProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorTotalActiveSubstanceAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorToxicUser;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorToxicUserUses;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

final class MethodUtils {
    final static BiConsumer<Set<Method>, IndicatorFilter> CHARGES_METHODS_PARAMETER = (Set<Method> methods, IndicatorFilter filter) -> {
        filter.setComputeReal(methods.contains(Method.PRIX_REELS_CHARGES));
        filter.setComputeStandardized(methods.contains(Method.PRIX_STANDARDISES_MILLESIMES_CHARGES));
    };

    final static BiConsumer<Set<Method>, IndicatorFilter> PRODUITS_MARGES_METHODS_PARAMETER = (Set<Method> methods, IndicatorFilter filter) -> {
        filter.setComputeReal(methods.contains(Method.PRIX_REELS_PRODUITS_MARGES));
        filter.setComputeStandardized(methods.contains(Method.PRIX_STANDARDISES_MILLESIMES_PRODUITS_MARGES));
        filter.setWithAutoConsumed(methods.contains(Method.AVEC_AUTOCONSOMMATION_PRODUITS_MARGES));
        filter.setWithoutAutoConsumed(methods.contains(Method.SANS_AUTOCONSOMMATION_PRODUITS_MARGES));
    };

    final static BiConsumer<Set<Method>, IndicatorFilter> SUBSTANCES_ACTIVES_METHODS_PARAMETER = (Set<Method> methods, IndicatorFilter filter) -> {
        filter.setWithSeedingTreatment(methods.contains(Method.SA_AVEC_TRAITEMENTS_DE_SEMENCES));
        filter.setWithoutSeedingTreatment(methods.contains(Method.SA_HORS_TRAITEMENTS_DE_SEMENCES));
    };

    final static BiConsumer<Set<Method>, IndicatorFilter> SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER = (Set<Method> methods, IndicatorFilter filter) -> {
        filter.setWithSeedingTreatment(methods.contains(Method.SAS_AVEC_TRAITEMENTS_DE_SEMENCES));
        filter.setWithoutSeedingTreatment(methods.contains(Method.SAS_HORS_TRAITEMENTS_DE_SEMENCES));
    };

    final static BiConsumer<Set<Method>, IndicatorFilter> TEMPS_DE_TRAVAIL_METHODS_PARAMETER = (Set<Method> methods, IndicatorFilter filter) -> {
        filter.setNoDetailed(methods.contains(Method.TEMPS_DE_TRAVAIL_TOTAL));
        filter.setDetailedByMonth(methods.contains(Method.TEMPS_DE_TRAVAIL_PAR_MOIS));
    };

    static Set<Method> CONVERT_TO_METHODS(Collection<IndicatorFilter> filters) {
        if (filters != null) {

            return filters.stream().map((filter) -> {
                var methods = new HashSet<Method>();

                CONVERT_IFT_TO_METHODS(filter, methods);

                CONVERT_SUBSTANCE_ACTIVES_TO_METHODS(filter, methods);

                CONVERT_SUBSTANCE_ACTIVES_SPECIFIQUES_TO_METHODS(filter, methods);

                CONVERT_RECOURS_AUX_MOYENS_BIOLOGIQUES_TO_METHODS(filter, methods);

                CONVERT_PRODUIT_ET_MARGE_TO_METHODS(filter, methods);

                CONVERT_CHARGE_TO_METHODS(filter, methods);

                CONVERT_STATEGIE_AGRONOMIQUE_TO_METHODS(filter, methods);

                CONVERT_FERTILISATION_NPK_TO_METHODS(filter, methods);

                CONVERT_FERTILISATION_AUTRE_TO_METHODS(filter, methods);

                CONVERT_TEMPS_DE_TRAVAIL_TO_METHODS(filter, methods);

                return methods;
            }).flatMap(Set::stream).collect(Collectors.toSet());
        }
        return Collections.emptySet();
    }

    private static void CONVERT_IFT_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        // ift à la cible non millésimé
        if (IndicatorRefMaxYearTargetIFT.class.getSimpleName().equals(filter.getClazz())) {
            methods.add(Method.IFT_A_LA_CIBLE_NON_MILLESIME);
        }
        // ift à la cible millésimé
        if (IndicatorVintageTargetIFT.class.getSimpleName().equals(filter.getClazz())) {
            methods.add(Method.IFT_A_LA_CIBLE_MILLESIME);
        }

        // ift à la culture millésimé
        if (IndicatorVintageCropIFT.class.getSimpleName().equals(filter.getClazz())) {
            methods.add(Method.IFT_A_LA_CULTURE_MILLESIME);
        }
        // ift à la culture non millésimé
        if (IndicatorRefMaxYearCropIFT.class.getSimpleName().equals(filter.getClazz())) {
            methods.add(Method.IFT_A_LA_CULTURE_NON_MILLESIME);
        }
        // ift à l'ancienne
        if (IndicatorLegacyIFT.class.getSimpleName().equals(filter.getClazz())) {
            methods.add(Method.IFT_A_L_ANCIENNE_NON_MILLESIME);
        }
    }

    private static void CONVERT_SUBSTANCE_ACTIVES_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        if (IndicatorTotalActiveSubstanceAmount.class.getSimpleName().equals(filter.getClazz())
                || IndicatorEnvironmentalRisk.class.getSimpleName().equals(filter.getClazz())
                || IndicatorToxicUser.class.getSimpleName().equals(filter.getClazz())
                || IndicatorCMR.class.getSimpleName().equals(filter.getClazz())
                || IndicatorSubstancesCandidateToSubstitution.class.getSimpleName().equals(filter.getClazz())
                || IndicatorLowRiskSubstances.class.getSimpleName().equals(filter.getClazz())
                || IndicatorCopperTotalProduct.class.getSimpleName().equals(filter.getClazz())
                || IndicatorCopperPhytoProduct.class.getSimpleName().equals(filter.getClazz())
                || IndicatorCopperFertilisationProduct.class.getSimpleName().equals(filter.getClazz())
                || IndicatorSulfurTotalProduct.class.getSimpleName().equals(filter.getClazz())
                || IndicatorSulfurPhytoProduct.class.getSimpleName().equals(filter.getClazz())
                || IndicatorSulfurFertilisationProduct.class.getSimpleName().equals(filter.getClazz())
                || IndicatorEnvironmentalRiskUses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorToxicUserUses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorCMRUses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorHRI1.class.getSimpleName().equals(filter.getClazz())
                || IndicatorHRI1_G1.class.getSimpleName().equals(filter.getClazz())
                || IndicatorHRI1_G2.class.getSimpleName().equals(filter.getClazz())
                || IndicatorHRI1_G3.class.getSimpleName().equals(filter.getClazz())
                || IndicatorHRI1_G4.class.getSimpleName().equals(filter.getClazz())) {
            if (Boolean.TRUE.equals(filter.getWithSeedingTreatment())) methods.add(Method.SA_AVEC_TRAITEMENTS_DE_SEMENCES);
            if (Boolean.TRUE.equals(filter.getWithoutSeedingTreatment())) methods.add(Method.SA_HORS_TRAITEMENTS_DE_SEMENCES);
        }
    }

    private static void CONVERT_SUBSTANCE_ACTIVES_SPECIFIQUES_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        if (IndicatorActiveSubstanceAmount.class.getSimpleName().equals(filter.getClazz())) {
            if (Boolean.TRUE.equals(filter.getWithSeedingTreatment())) methods.add(Method.SAS_AVEC_TRAITEMENTS_DE_SEMENCES);
            if (Boolean.TRUE.equals(filter.getWithoutSeedingTreatment())) methods.add(Method.SAS_HORS_TRAITEMENTS_DE_SEMENCES);
        }
    }

    private static void CONVERT_RECOURS_AUX_MOYENS_BIOLOGIQUES_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        // Nothing to do
    }

    private static void CONVERT_PRODUIT_ET_MARGE_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        if (IndicatorGrossIncome.class.getSimpleName().equals(filter.getClazz())
                || IndicatorStandardisedGrossIncome.class.getSimpleName().equals(filter.getClazz())
                || IndicatorGrossMargin.class.getSimpleName().equals(filter.getClazz())
                || IndicatorSemiNetMargin.class.getSimpleName().equals(filter.getClazz())
                || IndicatorDirectMargin.class.getSimpleName().equals(filter.getClazz())) {
            if (Boolean.TRUE.equals(filter.getComputeReal())) methods.add(Method.PRIX_REELS_PRODUITS_MARGES);
            if (Boolean.TRUE.equals(filter.getComputeStandardized())) methods.add(Method.PRIX_STANDARDISES_MILLESIMES_PRODUITS_MARGES);
            if (Boolean.TRUE.equals(filter.getWithAutoConsumed())) methods.add(Method.AVEC_AUTOCONSOMMATION_PRODUITS_MARGES);
            if (Boolean.TRUE.equals(filter.getWithoutSeedingTreatment())) methods.add(Method.SANS_AUTOCONSOMMATION_PRODUITS_MARGES);
        }
    }

    private static void CONVERT_CHARGE_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        if (IndicatorOperatingExpenses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorDecomposedOperatingExpenses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorEquipmentsExpenses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorTotalWorkforceExpenses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorMechanizedWorkforceExpenses.class.getSimpleName().equals(filter.getClazz())
                || IndicatorManualWorkforceExpenses.class.getSimpleName().equals(filter.getClazz())
        ) {
            if (Boolean.TRUE.equals(filter.getComputeReal())) methods.add(Method.PRIX_REELS_CHARGES);
            if (Boolean.TRUE.equals(filter.getComputeStandardized())) methods.add(Method.PRIX_STANDARDISES_MILLESIMES_CHARGES);
        }
    }

    private static void CONVERT_STATEGIE_AGRONOMIQUE_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        // Nothing to do
    }

    private static void CONVERT_FERTILISATION_NPK_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        // Nothing to do
    }

    private static void CONVERT_FERTILISATION_AUTRE_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        // Nothing to do
    }

    private static void CONVERT_TEMPS_DE_TRAVAIL_TO_METHODS(IndicatorFilter filter, HashSet<Method> methods) {
        if (IndicatorTotalWorkTime.class.getSimpleName().equals(filter.getClazz())
                || IndicatorMechanizedWorkTime.class.getSimpleName().equals(filter.getClazz())
                || IndicatorToolUsageTime.class.getSimpleName().equals(filter.getClazz())
                || IndicatorManualWorkTime.class.getSimpleName().equals(filter.getClazz())
                || IndicatorTransitCount.class.getSimpleName().equals(filter.getClazz())
        ) {
            if (Boolean.TRUE.equals(filter.getNoDetailed())) methods.add(Method.TEMPS_DE_TRAVAIL_TOTAL);
            if (Boolean.TRUE.equals(filter.getDetailedByMonth())) methods.add(Method.TEMPS_DE_TRAVAIL_PAR_MOIS);
        }
    }
}
