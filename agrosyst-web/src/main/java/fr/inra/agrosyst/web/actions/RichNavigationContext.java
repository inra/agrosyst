package fr.inra.agrosyst.web.actions;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.context.NavigationContextService;
import fr.inra.agrosyst.api.utils.DaoUtils;

import java.util.Map;
import java.util.function.Function;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class RichNavigationContext {

    protected static final Function<Integer, String> GET_CAMPAIGN_NAME = campaign -> String.format("%d (%d - %d)", campaign, campaign - 1, campaign);

    protected final NavigationContext navigationContext;

    protected final NavigationContextService navigationContextService;

    public RichNavigationContext(NavigationContext navigationContext, NavigationContextService navigationContextService) {
        this.navigationContext = navigationContext;
        this.navigationContextService = navigationContextService;
    }

    public int getCampaignsCount() {
        return navigationContext.getCampaignsCount();
    }

    public Map<Integer, String> getCampaigns() {
        int index = 0;
        int max = Integer.MAX_VALUE;
        Map<Integer, String> result = Maps.newLinkedHashMap();
        for (Integer campaign : navigationContext.getCampaigns()) {
            if (index >= max) {
                break;
            }
            index++;
            result.put(campaign, GET_CAMPAIGN_NAME.apply(campaign));
        }
        return result;
    }

    public int getNetworksCount() {
        return navigationContext.getNetworksCount();
    }

    public Map<String, String> getNetworks() {
        return navigationContextService.getNetworks(navigationContext.getNetworks(), DaoUtils.NO_PAGE_LIMIT);
    }

    public int getDomainsCount() {
        return navigationContext.getDomainsCount();
    }

    public Map<String, String> getDomains() {
        return navigationContextService.getDomains(navigationContext.getDomains(), DaoUtils.NO_PAGE_LIMIT);
    }

    public int getGrowingPlansCount() {
        return navigationContext.getGrowingPlansCount();
    }

    public Map<String, String> getGrowingPlans() {
        return navigationContextService.getGrowingPlans(navigationContext.getGrowingPlans(), DaoUtils.NO_PAGE_LIMIT);
    }

    public int getGrowingSystemsCount() {
        return navigationContext.getGrowingSystemsCount();
    }

    public Map<String, String> getGrowingSystems() {
        return navigationContextService.getGrowingSystems(navigationContext.getGrowingSystems(), DaoUtils.NO_PAGE_LIMIT);
    }

}
