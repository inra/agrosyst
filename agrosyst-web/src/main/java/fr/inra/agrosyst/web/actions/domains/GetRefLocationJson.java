package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;


/**
 * Action d'afficher les information de localisation.
 * 
 * @author David Cossé
 */
public class GetRefLocationJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(GetRefLocationJson.class);

    @Serial
    private static final long serialVersionUID = 1L;

    protected transient ReferentialService referentialService;

    protected transient String locationTopiaId;
    
    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }
    
    public void setLocationTopiaId(String locationTopiaId) {
        this.locationTopiaId = locationTopiaId;
    }

    protected String orNC(String text) {
        String result = MoreObjects.firstNonNull(text, "n/c");
        return result;
    }

    @Override
    public String execute() throws Exception {
        try {
            RefLocation refLocation = referentialService.getRefLocation(locationTopiaId);
    
            String departement = refLocation.getDepartement();
            String key = "departement." + Strings.padStart(departement, 2, '0');
            String departementName = getText(key, "n/c");
            String petiteRegionAgricole = orNC(refLocation.getPetiteRegionAgricoleCode());
            String petiteRegionAgricoleName = orNC(refLocation.getPetiteRegionAgricoleNom());
            int region = refLocation.getRegion();
    
            jsonData = ImmutableMap.of(
                    "departement", departement,
                    "departementName", departementName,
                    "petiteRegionAgricole", petiteRegionAgricole,
                    "petiteRegionAgricoleName", petiteRegionAgricoleName,
                    "region", String.valueOf(region)
            );
            
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Faild to load RefLocation for locationTopiaId '%s':",
                        locationTopiaId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

}
