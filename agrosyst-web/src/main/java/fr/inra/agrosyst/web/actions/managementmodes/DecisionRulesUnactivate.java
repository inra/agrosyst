package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Desactivation multiple de decision rules.
 * 
 * @author Eric Chatellier
 */
public class DecisionRulesUnactivate extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -4930761732959095035L;

    protected transient ManagementModeService managementModeService;

    protected List<String> decisionRuleIds;

    protected boolean activate;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setDecisionRuleIds(String decisionRuleIds) {
        this.decisionRuleIds = getGson().fromJson(decisionRuleIds, List.class);
    }

    public void setActivate(boolean activate) {
        this.activate = activate;
    }

    @Override
    @Action(results = { @Result(type = "redirectAction", params = {
            "actionName", "decision-rules-list" }) })
    public String execute() throws Exception {
        managementModeService.unactivateDecisionRules(decisionRuleIds, activate);
        return SUCCESS;
    }
}
