package fr.inra.agrosyst.web.actions.admin;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.async.AsyncService;
import fr.inra.agrosyst.api.services.async.ScheduledTaskDto;
import fr.inra.agrosyst.api.services.async.TaskFilter;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class AdminTaskAction extends AbstractAdminAction {
    
    @Serial
    private static final long serialVersionUID = 3961688540524235085L;
    
    private AsyncService asyncService;
    
    protected boolean admin;
    
    protected PaginationResult<ScheduledTaskDto> runningAndPendingTasks;
    
    protected TaskFilter filter;
    
    protected transient Boolean pagination = true;
    
    public void setPagination(Boolean pagination) {
        this.pagination = pagination;
    }
    
    public void setAsyncService(AsyncService asyncService) {
        this.asyncService = asyncService;
    }
    
    @Override
    public String execute() throws Exception {
        admin = authorizationService.isAdmin();
        if (admin) {
            filter = new TaskFilter();
            NavigationContext navigationContext = getNavigationContext();
            filter.setNavigationContext(navigationContext);
            filter.setPageSize(getListNbElementByPage(ScheduledTaskDto.class));
            
            runningAndPendingTasks = asyncService.getRunningAndPendingTasks(filter);
        } else {
            throw new AgrosystAccessDeniedException();
        }
        return SUCCESS;
    }
    
    public PaginationResult<ScheduledTaskDto> getRunningAndPendingTasks() {
        return runningAndPendingTasks;
    }
    
    public void setFilter(TaskFilter filter) {
        this.filter = filter;
    }
    
    public TaskFilter getFilter() {
        return filter;
    }
    
    public boolean isAdmin() {
        return admin;
    }
}
