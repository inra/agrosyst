package fr.inra.agrosyst.web.filters;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filtre qui remplace à la volée certaines propriété CSS pour IE 10,11 et Edge.
 */
public class IeEdgeCssFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String userAgent = ((HttpServletRequest) request).getHeader("User-Agent");

        if (userAgent != null && (userAgent.contains("MSIE") || userAgent.contains("Edge") || userAgent.contains("Trident")) ) {
            HtmlResponseWrapper responseWrapper = new HtmlResponseWrapper((HttpServletResponse) response);
            chain.doFilter(request, responseWrapper);
            String content = responseWrapper.getCaptureAsString();

            if (response.getContentType() != null && response.getContentType().contains("text/html")) {
                content = content.replaceAll("(grid-(column|row)\\s*:[^;]*)\\s*/\\s*span\\s*([^;]*);", "$1; -ms-grid-$2-span: $3;");
                content = content.replaceAll("(grid-column|grid-row)\\s*:[^;]*;\\s?", "$0-ms-$0");
                // -ms-grid-columns for IE 10
                // grid-template-columns: 10px repeat(4, 250px 10px)
                // --> -ms-grid-columns: 10px (250px 10px)[4];
                // repeat({{viewPortArray.length||1}}, 1fr)
                content = content.replaceAll("grid-template-columns\\s*:([^;]*;)\\s?", "$0-ms-$0 -ms-grid-columns: $1");
                content = content.replaceAll("(-ms-grid-columns:.*)repeat\\(([^,]*),\\s*1fr\\)(\\s*[^;]*;)", "$1 (1fr)[$2]$3");

                response.getWriter().write(content);
            } else {
                response.getWriter().write(content);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }

}
