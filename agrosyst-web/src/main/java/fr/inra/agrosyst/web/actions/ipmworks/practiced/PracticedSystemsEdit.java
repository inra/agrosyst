package fr.inra.agrosyst.web.actions.ipmworks.practiced;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.practiced.CropCycleModelDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Results({
        @Result(name="success", type="redirectAction", params = {"namespace", "/ipmworks/practiced", "actionName", "practiced-systems-edit-input", "practicedSystemTopiaId", "${practicedSystemTopiaId}" }),
        @Result(name="error", type="redirectAction", params = {"namespace", "/ipmworks//practiced", "actionName", "practiced-systems-edit-input", "practicedSystemTopiaId", "${practicedSystemTopiaId}" })})
public class PracticedSystemsEdit extends fr.inra.agrosyst.web.actions.practiced.PracticedSystemsEdit {

    protected transient DomainService domainService;
    protected transient GrowingPlanService growingPlanService;
    protected transient NetworkService networkService;

    protected String typeAgricultureTopiaId;
    protected String ipmWorksId;

    protected String missingTypeAgricultureTopiaId;
    protected List<RefTypeAgriculture> typeAgricultures;

    @Override
    public void prepare() {
        super.prepare();
        missingTypeAgricultureTopiaId = growingSystemService.getMissingTypeAgricultureTopiaId();
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(typeAgricultureTopiaId)
                || !typeAgricultureTopiaId.startsWith(RefTypeAgriculture.class.getName())
                || typeAgricultureTopiaId.equals(missingTypeAgricultureTopiaId)) {
            addFieldError("typeAgricultureTopiaId", getText(REQUIRED_FIELD));
            addActionError(getText("practicedSystem-error-typeAgricultureTopiaId-action-message"));
        }
        if (StringUtils.isBlank(ipmWorksId)) {
            addFieldError("ipmWorksId", getText(REQUIRED_FIELD));
            addActionError(getText("practicedSystem-error-ipmWorksId-action-message"));
        }
        super.validate();
    }

    @Override
    protected void validateGrowingSystemForNewPracticedSystem() {
        // do nothing
    }

    @Override
    @Action("practiced-systems-edit-input")
    public String input() {

        Domain domain = null;
        if (StringUtils.isNoneBlank(domainId)) {
            domain = domainService.getDomain(domainId);
            campaigns = String.valueOf(domain.getCampaign());
        }

        typeAgricultures = referentialService.getIpmWorksTypeAgricultures();

        GrowingSystem growingSystem = getPracticedSystem().getGrowingSystem();
        if (growingSystem != null) {
            if (growingSystem.getTypeAgriculture() != null) {
                typeAgricultureTopiaId = growingSystem.getTypeAgriculture().getTopiaId();
            }
            typeAgricultureTopiaId = growingSystem.getTypeAgriculture().getTopiaId();
            ipmWorksId = growingSystem.getDephyNumber();
            growingSystemSector = growingSystem.getSector();
        } else if (domain != null){
            growingSystem = getGrowingSystemForNewPracticedSystem(
                    domain.getTopiaId(),
                    domain.getName(),
                    typeAgricultures.get(0));
            growingSystemTopiaId = growingSystem.getTopiaId();
        }

        practicedSystem = getPracticedSystem();
        if (!practicedSystem.isPersisted()) {
            practicedSystem.setCampaigns(campaigns);
            practicedSystem.setGrowingSystem(growingSystem);
        }

        // chargement des toutes les cultures pour les systems de cultures et les années

        Map<CropCycleModelDto, List<CroppingPlanSpeciesDto>> modelToSpecies =
                practicedSystemService.getIpmworksCropCycleModelMap(domainId);
        Set<CropCycleModelDto> croppingPlanEntryDtos = modelToSpecies.keySet();

        // definition de la liste de culture principale
        practicedSystemMainCropCycleModels = croppingPlanEntryDtos.stream().filter(CroppingPlans.IS_NOT_INTERMEDIATE).collect(Collectors.toList()); // force no lazy

        // définition de la liste de culture intermédiaire
        practicedSystemIntermediateCropCycleModels = croppingPlanEntryDtos.stream().filter(CroppingPlans.IS_INTERMEDIATE).collect(Collectors.toList()); // force no lazy

        // chargement de la map 'code culture' > liste d'espece
        practicedSystemCroppingPlanEntryCodesToSpecies = new HashMap<>();
        for (Map.Entry<CropCycleModelDto, List<CroppingPlanSpeciesDto>> entry : modelToSpecies.entrySet()) {
            practicedSystemCroppingPlanEntryCodesToSpecies.put(entry.getKey().getCroppingPlanEntryCode(), entry.getValue());
        }

        return super.input();

    }

    protected Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant() {
        if (practicedSystem.isPersisted()) {
            return super.getSectorByCodeEspceBotaniqueCodeQualifiant();
        }
        return practicedSystemService.getSectorByCodeEspceBotaniqueCodeQualifiant2(domainId, practicedSystem.getCampaigns());
    }

    protected Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCode() {
        if (practicedSystem.isPersisted()) {
            return super.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCode();
        }
        return practicedSystemService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds2(domainId, practicedSystem.getCampaigns());
    }

    protected List<String> getCropCodesForCampaigns() {
        if (practicedSystem.isPersisted()) {
            return super.getCropCodesForCampaigns();
        }
        return practicedSystemService.getCropCodesFromDomainIdForCampaigns(domainId, practicedSystem.getCampaigns());
    }

    @Override
    public String execute() throws Exception {
        PracticedSystem practicedSystem1 = getPracticedSystem();
        if (practicedSystem1.isPersisted()) {
            GrowingSystem growingSystem = practicedSystem1.getGrowingSystem();
            growingSystemTopiaId = growingSystem.getTopiaId();
        }

        if (StringUtils.isNotBlank(growingSystemTopiaId)){
            GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
            growingSystem.setTypeAgriculture(referentialService.getRefTypeAgricultureWithId(typeAgricultureTopiaId));
            growingSystem.setDephyNumber(ipmWorksId);
            growingSystem.setName(practicedSystem1.getName());
            growingSystem.setSector(growingSystemSector);
            growingSystemService.updateGrowingSystem(growingSystem);
        }
        return super.execute();

    }

    protected GrowingSystem getGrowingSystemForNewPracticedSystem(
            String domainId,
            String name,
            RefTypeAgriculture typeAgriculture
    ) {

        GrowingSystem result = growingSystemService.createDefaultIpmworksGrowingSystem(
                domainId,
                name,
                Sector.GRANDES_CULTURES,
                "NEW_GROWING_SYSTEM_IPMWORKS_ID",
                typeAgriculture.getTopiaId(),
                new EnumMap<>(Sector.class));

        return result;
    }

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }

    public String getTypeAgricultureTopiaId() {
        return typeAgricultureTopiaId;
    }

    public void setTypeAgricultureTopiaId(String typeAgricultureTopiaId) {
        this.typeAgricultureTopiaId = typeAgricultureTopiaId;
    }

    public String getIpmWorksId() {
        return ipmWorksId;
    }

    public void setIpmWorksId(String ipmWorksId) {
        this.ipmWorksId = ipmWorksId;
    }

    public String getMissingTypeAgricultureTopiaId() {
        return missingTypeAgricultureTopiaId;
    }

    public List<RefTypeAgriculture> getTypeAgricultures() {
        return typeAgricultures;
    }

    public Map<Sector, String> getGrowingSystemSectors() {
        return i18nService.getEnumAsMap(null, getConfig().getIpmWorksSectors());
    }
}
