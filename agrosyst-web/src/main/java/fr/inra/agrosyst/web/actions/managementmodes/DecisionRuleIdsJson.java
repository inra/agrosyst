package fr.inra.agrosyst.web.actions.managementmodes;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.managementmode.DecisionRuleFilter;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Decision rule ids json action.
 * 
 * @author Kevin Morin
 */
public class DecisionRuleIdsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(DecisionRuleIdsJson.class);
    @Serial
    private static final long serialVersionUID = -1406603544241348610L;
    
    protected transient ManagementModeService managementModeService;

    protected String filter;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String execute() {
        try {
            DecisionRuleFilter decisionRulefilter = getGson().fromJson(filter, DecisionRuleFilter.class);
            NavigationContext navigationContext = getNavigationContext();
            decisionRulefilter.setNavigationContext(navigationContext);
            jsonData = managementModeService.getFilteredDecisionRuleIds(decisionRulefilter);
        }  catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load decision rules", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
