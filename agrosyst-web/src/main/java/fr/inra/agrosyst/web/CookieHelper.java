package fr.inra.agrosyst.web;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.function.Function;

/**
 * Classe utilitaire pour la manipulation des Cookie
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.64
 */
public class CookieHelper {

    protected final HttpServletRequest httpServletRequest;
    protected final HttpServletResponse httpServletResponse;

    protected final String defaultPath;
    protected final boolean secure;

    public CookieHelper(HttpServletRequest httpServletRequest,
                        HttpServletResponse httpServletResponse,
                        AgrosystWebConfig agrosystWebConfig) {
        this.httpServletRequest = httpServletRequest;
        this.httpServletResponse = httpServletResponse;

        String path = httpServletRequest.getServletContext().getContextPath();
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        this.defaultPath = path;
        this.secure = agrosystWebConfig.isSecuredCookies();
    }

    public Optional<String> readCookie(String cookieName) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(cookieName));
        Cookie[] cookies = this.httpServletRequest.getCookies();
        if (cookies == null) {
            return Optional.empty();
        }
        Optional<String> result = Arrays.stream(cookies)
                .filter(c -> cookieName.equals(c.getName()))
                .map(Cookie::getValue)
                .filter(StringUtils::isNotBlank)
                .map(v -> URLDecoder.decode(v, StandardCharsets.UTF_8))
                .findFirst();
        return result;
    }

    public <T> Optional<T> readCookie(String cookieName, Function<String, T> parser) {
        Optional<String> cookieValue = readCookie(cookieName);
        Optional<T> result = cookieValue.map(parser);
        return result;
    }

    public void writeCookie(String cookieName, String cookieValue, OptionalInt maxAge, boolean httpOnly) {
        String encoded = URLEncoder.encode(cookieValue, StandardCharsets.UTF_8);
        Cookie cookie = new Cookie(cookieName, encoded);
        cookie.setHttpOnly(httpOnly);
        cookie.setPath(this.defaultPath);
        cookie.setSecure(this.secure);
        maxAge.ifPresent(cookie::setMaxAge);
        this.httpServletResponse.addCookie(cookie);
    }

    public <T> void writeCookie(String cookieName, T value, Function<T, String> formatter, boolean httpOnly) {
        String cookieValue = formatter.apply(value);
        writeCookie(cookieName, cookieValue, OptionalInt.empty(), httpOnly);
    }

    public <T> void writeCookie(String cookieName, T value, Function<T, String> formatter, int maxAge, boolean httpOnly) {
        String cookieValue = formatter.apply(value);
        writeCookie(cookieName, cookieValue, OptionalInt.of(maxAge), httpOnly);
    }

    public void deleteCookie(String cookieName) {
        // A zero value causes the cookie to be deleted.
        writeCookie(cookieName, "whatever", OptionalInt.of(0), true);
    }

}
