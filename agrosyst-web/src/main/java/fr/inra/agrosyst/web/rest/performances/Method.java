package fr.inra.agrosyst.web.rest.performances;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

enum Method {
    IFT_A_LA_CIBLE_NON_MILLESIME,
    IFT_A_LA_CIBLE_MILLESIME,
    IFT_A_LA_CULTURE_NON_MILLESIME,
    IFT_A_LA_CULTURE_MILLESIME,
    IFT_A_L_ANCIENNE_NON_MILLESIME,

    SA_AVEC_TRAITEMENTS_DE_SEMENCES,
    SA_HORS_TRAITEMENTS_DE_SEMENCES,

    SAS_AVEC_TRAITEMENTS_DE_SEMENCES,
    SAS_HORS_TRAITEMENTS_DE_SEMENCES,

    PRIX_REELS_PRODUITS_MARGES,
    PRIX_STANDARDISES_MILLESIMES_PRODUITS_MARGES,
    AVEC_AUTOCONSOMMATION_PRODUITS_MARGES,
    SANS_AUTOCONSOMMATION_PRODUITS_MARGES,

    PRIX_REELS_CHARGES,
    PRIX_STANDARDISES_MILLESIMES_CHARGES,

    TEMPS_DE_TRAVAIL_PAR_MOIS,
    TEMPS_DE_TRAVAIL_TOTAL
}
