package fr.inra.agrosyst.web;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AgrosystTopiaApplicationContext;
import fr.inra.agrosyst.api.entities.AgrosystTopiaPersistenceContext;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import fr.inra.agrosyst.web.actions.domains.DomainsPreImportEdaplos;
import fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;
import org.apache.struts2.dispatcher.Parameter;
import org.nuiton.util.beans.BeanUtil;

import javax.servlet.http.HttpServletRequest;
import java.beans.PropertyDescriptor;
import java.io.Serial;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Interceptor struts qui inject les parametres de type connu (Session, Service, Config) dans
 * l'action Struts avant son execution.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystWebInterceptor implements Interceptor {

    @Serial
    private static final long serialVersionUID = 2035088485913506328L;

    private static final Log LOGGER = LogFactory.getLog(AgrosystWebInterceptor.class);
    public static final String LANGUAGE_PARAMETER = "lang";

    @Override
    public void init() {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Initializing Agrosyst Web Interceptor");
        }
    }

    protected Optional<AuthenticatedUser> getAuthenticatedUser(ActionInvocation invocation) {
        ActionContext invocationContext = invocation.getInvocationContext();
        HttpServletRequest httpServletRequest = (HttpServletRequest) invocationContext.get(StrutsStatics.HTTP_REQUEST);
        AuthenticatedUser userDto = (AuthenticatedUser) httpServletRequest.getAttribute(AgrosystWebAuthenticationFilter.AUTHENTICATED_USER);
        Optional<AuthenticatedUser> result = Optional.ofNullable(userDto);
        return result;
    }

    protected AgrosystWebNotificationSupport getNotificationSupport(AgrosystWebApplicationContext webApplicationContext,
                                                                    Optional<AuthenticatedUser> authenticatedUser) {
        if (authenticatedUser.isPresent()) {
            Map<String, AgrosystWebNotificationSupportDistributed> cache = webApplicationContext.getDistributedCache("notifications");
            String userId = authenticatedUser.get().getTopiaId();
            final AgrosystWebNotificationSupportDistributed notificationSupport =
                    Optional.ofNullable(cache.get(userId)).orElseGet(AgrosystWebNotificationSupportDistributed::new);
            notificationSupport.setLanguage(authenticatedUser.get().getLanguage());
            Runnable updater = () -> {
                if (notificationSupport.isEmpty()) {
                    // On fait en sorte de ne pas maintenir un objet en mémoire pour rien s'il n'y a pas de notification
                    cache.remove(userId);
                } else {
                    cache.put(userId, notificationSupport);
                }
            };
            notificationSupport.setUpdater(updater);
            return notificationSupport;
        } else {
            AgrosystWebNotificationSupport result = new AgrosystWebNotificationSupport();
            result.setLanguage(Language.FRENCH);
            return result;
        }
    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {

        Object action = invocation.getAction();

        if (action instanceof AbstractAgrosystAction) {

            AgrosystWebApplicationContext webApplicationContext = getAgrosystApplicationContext(invocation);

            Optional<AuthenticatedUser> authenticatedUser = getAuthenticatedUser(invocation);

            ServiceContext serviceContext = newServiceContext(webApplicationContext, authenticatedUser);

            Parameter lang = invocation.getInvocationContext().getParameters().get(LANGUAGE_PARAMETER);
            if (lang.isDefined()) {
                invocation.getInvocationContext().withLocale(Language.fromTrigram(lang.getValue()).getLocale());
            } else {
                authenticatedUser.ifPresent(user -> invocation.getInvocationContext().withLocale(user.getLocale()));
            }

            try {
                Set<PropertyDescriptor> descriptors = BeanUtil.getDescriptors(action.getClass(), BeanUtil.IS_WRITE_DESCRIPTOR);

                // Iterate over writable properties
                for (PropertyDescriptor propertyDescriptor : descriptors) {

                    Class<?> propertyType = propertyDescriptor.getPropertyType();

                    Object toInject = null;

                    if (AgrosystService.class.isAssignableFrom(propertyType)) {
                        Class<? extends AgrosystService> clazz = (Class<? extends AgrosystService>) propertyType;
                        toInject = serviceContext.newService(clazz);

                    } else if (AgrosystWebConfig.class.isAssignableFrom(propertyType)) {
                        toInject = webApplicationContext.getWebConfig();

                    } else if (AgrosystWebApplicationContext.class.isAssignableFrom(propertyType)) {
                        toInject = webApplicationContext;

                    } else if (AgrosystWebNotificationSupport.class.isAssignableFrom(propertyType)) {
                        AgrosystWebNotificationSupport notificationSupport = getNotificationSupport(webApplicationContext, authenticatedUser);
                        toInject = notificationSupport;
                    }

                    if (toInject != null) {
                        if (LOGGER.isTraceEnabled()) {
                            LOGGER.trace("injecting " + toInject + " in action " + action);
                        }
                        propertyDescriptor.getWriteMethod().invoke(action, toInject);
                    }
                }

                String result = invocation.invoke();
                checkForLayoutData((AbstractAgrosystAction) action);
                return result;

            } catch (Exception eee) {
                // rollback transaction only when an exception happen
                // sinon, le serveur devient inutilisable (
                rollbackTransaction(serviceContext);

                String userEmail = authenticatedUser
                        .map(AuthenticatedUser::getEmail)
                        .orElse("?");

                // Make sure exception is logged because of some strange logging behavior
                if (LOGGER.isErrorEnabled()) {
                    String message = String.format("Exception occured for user '%s' during Agrosyst action invocation (action is '%s'): %s",
                            userEmail, action.getClass().getName(), eee.getMessage());
                    LOGGER.error(message, eee);
                }
                throw eee;

            } finally {
                checkClosedPersistenceContext(serviceContext);
            }

        } else {
            // not an action, just process
            return invocation.invoke();
        }
    }

    protected void checkForLayoutData(AbstractAgrosystAction action) {
        // Do not prepare layout data in Json actions
        if (!(action instanceof AbstractJsonAction)) {
            // Only prepare layout data for a "POST" with errors or "GET" requests
            if (action.hasErrors() || "GET".equals(ServletActionContext.getRequest().getMethod())
                    || action instanceof DomainsPreImportEdaplos) { // exception in post
                action.initLayoutData();
            }
        }
    }

    protected AgrosystWebApplicationContext getAgrosystApplicationContext(ActionInvocation invocation) {

        AgrosystWebApplicationContext applicationContext =
                (AgrosystWebApplicationContext) invocation
                        .getInvocationContext()
                        .getApplication()
                        .get(AgrosystWebApplicationContext.APPLICATION_CONTEXT_PARAMETER);

        Preconditions.checkState(applicationContext != null, AgrosystWebApplicationContext.MISSING_APPLICATION_CONTEXT);

        return applicationContext;
    }

    protected ServiceContext newServiceContext(AgrosystWebApplicationContext webApplicationContext,
                                               Optional<AuthenticatedUser> authenticatedUser) {

        ServiceContext result = null;

        final AgrosystTopiaApplicationContext topiaApplicationContext = webApplicationContext.getApplicationContext();
        Preconditions.checkState(topiaApplicationContext != null);

        try {
            result = authenticatedUser.isPresent()
                    ? topiaApplicationContext.newServiceContext(authenticatedUser.get())
                    : topiaApplicationContext.newServiceContext();
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Unable to create local serviceContext", eee);
            }
        }

        Preconditions.checkState(result != null, "ServiceContext not instantiated, check configuration");
        return result;
    }

    protected void checkClosedPersistenceContext(ServiceContext serviceContext) {
        try {
            serviceContext.close();
        } catch (Exception eee) {
            if (LOGGER.isFatalEnabled()) {
                LOGGER.fatal("Unable to close persistence context !", eee);
            }
            throw new AgrosystTechnicalException("Unable to close persistence context !", eee);
        }
    }

    protected void rollbackTransaction(ServiceContext serviceContext) {
        try {
            AgrosystTopiaPersistenceContext persistenceContext = serviceContext.getPersistenceContext(false);
            if (persistenceContext != null) {
                persistenceContext.rollback();
            }
        } catch (Exception eee) {
            if (LOGGER.isFatalEnabled()) {
                LOGGER.fatal("Unable to rollback persistence context !", eee);
            }
            throw new AgrosystTechnicalException("Unable to rollback persistence context !", eee);
        }
    }

    @Override
    public void destroy() {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Destroy Interceptor");
        }
    }

}
