package fr.inra.agrosyst.web.actions.performances;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

public class PerformancesDelete extends AbstractAgrosystAction {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -1180723133330637375L;

    protected transient PerformanceService performanceService;

    protected List performanceIds;

    protected boolean practiced;

    public void setPerformanceIds(String performanceIds) {
        this.performanceIds =  getGson().fromJson(performanceIds, List.class);
    }

    public void setPracticed(boolean practiced) {
        this.practiced = practiced;
    }

    public void setPerformanceService(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "performances-list", "practiced", "${practiced}"})})
    public String execute() {
        performanceService.deletePerformance(performanceIds);
        return SUCCESS;
    }
    
    public boolean isPracticed() {
        return practiced;
    }
}
