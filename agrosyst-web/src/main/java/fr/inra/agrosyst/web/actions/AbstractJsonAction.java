package fr.inra.agrosyst.web.actions;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.Serial;
import java.util.function.Function;

/**
 * Abstract action used to render custom objects as json string using gson directly in response output stream.
 *
 * @author Eric Chatellier
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Results({
        @Result(type = "agrosyst-json", name = "success"),
        @Result(type = "agrosyst-json", name = "error"),
        @Result(type = "agrosyst-json", name = "input")
})
public abstract class AbstractJsonAction extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 2293808209162340936L;

    /**
     * Return true for each value. (Used for javascript selection standard).
     */
    public static final Function<String, Boolean> GET_TRUE = input -> Boolean.TRUE;

    public static final Function<Object, String> TO_STRING = String::valueOf;

    protected static final int DEFAULT_SEARCH_PAGE_SIZE = 50;

    protected transient Object jsonData = null;

    protected Integer httpCode = null;

    /**
     * Method to override to get object data to render as json. Method HAS to be public because result support will use
     * this method.
     *
     * @return object to render as json
     */
    public Object getJsonData() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(jsonData);
        }
        return jsonData;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    @Override
    public String input() throws Exception {
        return INPUT;
    }

}
