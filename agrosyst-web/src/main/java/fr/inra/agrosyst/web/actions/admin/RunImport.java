package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefGesCarburant;
import fr.inra.agrosyst.api.entities.referential.RefGesEngrais;
import fr.inra.agrosyst.api.entities.referential.RefGesPhyto;
import fr.inra.agrosyst.api.entities.referential.RefGesSemence;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefNrjCarburant;
import fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil;
import fr.inra.agrosyst.api.entities.referential.RefNrjPhyto;
import fr.inra.agrosyst.api.entities.referential.RefNrjSemence;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefSaActaIphy;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnits;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI;
import fr.inra.agrosyst.api.entities.referential.RefUniteEDI;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.common.EmailService;
import fr.inra.agrosyst.services.referential.csv.RefEspeceDto;
import fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto;
import fr.inra.agrosyst.services.referential.csv.TypeReferentielCommunes;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.action.UploadedFilesAware;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.dispatcher.multipart.UploadedFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Serial;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Results({
        @Result(type = "redirectAction", name = "input", params = {"namespace", "/generic", "actionName", "generic-entities-list", "genericClassName", "${genericClassName}", "importFileError", "true"})
})
public class RunImport extends AbstractAdminAction implements UploadedFilesAware {
    
    @Serial
    private static final long serialVersionUID = 4130324772446717782L;

    private static final Log LOGGER = LogFactory.getLog(RunImport.class);
    public static final int MAX_DISPLAYED_MESSAGES = 20;
    
    protected transient ImportService importService;
    protected transient EmailService emailService;
    protected transient DomainService domainService;
    protected transient ReferentialService referentialService;

    public void setImportService(ImportService importService) {
        this.importService = importService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }
    
    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }
    
    protected String genericClassName;
    protected Class<?> klass;

    protected File source1;
    protected File source2;

    protected String source1FileName;
    protected String source2FileName;

    protected String source1ContentType;
    protected String source2ContentType;

    protected TypeReferentielCommunes typeReferentielCommune;

    public void setGenericClassName(String genericClassName) {
        this.genericClassName = genericClassName;
    }

    public String getGenericClassName() {
        return genericClassName;
    }

    public void setSource1(File source1) {
        this.source1 = source1;
    }

    public void setSource2(File source2) {
        this.source2 = source2;
    }

    public void setSource1FileName(String source1FileName) {
        this.source1FileName = source1FileName;
    }

    public void setSource2FileName(String source2FileName) {
        this.source2FileName = source2FileName;
    }

    public void setSource1ContentType(String source1ContentType) {
        this.source1ContentType = source1ContentType;
    }

    public void setSource2ContentType(String source2ContentType) {
        this.source2ContentType = source2ContentType;
    }

    public TypeReferentielCommunes getTypeReferentielCommune() {
        return typeReferentielCommune;
    }

    public void setTypeReferentielCommune(TypeReferentielCommunes typeReferentielCommune) {
        this.typeReferentielCommune = typeReferentielCommune;
    }

    protected void required(File... sources) {
        if (sources != null) {
            int index = 0;
            for (File source : sources) {
                index++;
                if (source == null) {
                    addFieldError("source" + index, "Fichier manquant");
                }
            }
        }
    }

    @Override
    public void validate() {
        if (Strings.isNullOrEmpty(genericClassName)) {
            addFieldError("genericClassName", "Ce champ est obligatoire");
        }

        try {
            klass = Class.forName(genericClassName);
        } catch (ClassNotFoundException e) {
            addFieldError("genericClassName", String.format("Classe %s non trouvée", genericClassName));
        }

        if (RefSolArvalis.class.equals(klass)) {
            required(source1, source2);
        } else {
            required(source1);
        }

        super.validate();
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"namespace", "/generic", "actionName", "generic-entities-list", "genericClassName", "${genericClassName}"})})
    public String execute() throws Exception {
        checkIsAdmin();

        ImportResult importResult = null;

        try (FileInputStream stream = new FileInputStream(source1)) {
            if (RefSolArvalis.class.equals(klass)) {
                try (FileInputStream regionsStream = new FileInputStream(source2)) {
                    importResult = importService.importSolArvalisCSV(
                            stream,
                            regionsStream);
                }
            }

            if (RefLocationExtendedDto.class.equals(klass) && TypeReferentielCommunes.REFERENTIEL_COMMUNE_FRANCE == typeReferentielCommune) {
                importResult = importService.importCommuneFranceCSV(stream);
            } else if (RefLocationExtendedDto.class.equals(klass) && TypeReferentielCommunes.REFERENTIEL_COMMUNE_EUROPE == typeReferentielCommune) {
                importResult = importService.importCommuneEuropeCSV(stream);
            }

            if (RefCountry.class.equals(klass)) {
                importResult = importService.importCountriesCSV(stream);
            }

            if (RefCorrespondanceMaterielOutilsTS.class.equals(klass)) {
                importResult = importService.importRefCorrespondanceMaterielOutilsTS(stream);
            }

            if (RefMaterielTraction.class.equals(klass)) {
                importResult = importService.importMaterielTracteursCSV(stream);
            }

            if (RefMaterielIrrigation.class.equals(klass)) {
                importResult = importService.importMaterielIrrigationCSV(stream);
            }

            if (RefMaterielAutomoteur.class.equals(klass)) {
                importResult = importService.importMaterielAutomoteursCSV(stream);
            }

            if (RefMaterielOutil.class.equals(klass)) {
                importResult = importService.importMaterielOutilsCSV(stream);
            }

            if (RefLegalStatus.class.equals(klass)) {
                importResult = importService.importLegalStatusCSV(stream);
            }
    
            if (RefEspeceDto.class.equals(klass)) {
                importResult = importService.importEspeces(stream);
            }

            if (RefVarieteGeves.class.equals(klass)) {
                importResult = importService.importVarietesGeves(stream);
            }

            if (RefEspeceToVariete.class.equals(klass)) {
                importResult = importService.importEspecesToVarietes(stream);
            }

            if (RefOTEX.class.equals(klass)) {
                importResult = importService.importOtexCSV(stream);
            }

            if (RefClonePlantGrape.class.equals(klass)) {
                importResult = importService.importClonesPlantGrape(stream);
            }

            if (RefVarietePlantGrape.class.equals(klass)) {
                importResult = importService.importVarietesPlantGrape(stream);
            }

            if (RefOrientationEDI.class.equals(klass)) {
                importResult = importService.importOrientationEdiCSV(stream);
            }

            if (RefSolTextureGeppa.class.equals(klass)) {
                importResult = importService.importSolTextureGeppa(stream);
            }

            if (RefParcelleZonageEDI.class.equals(klass)) {
                importResult = importService.importZonageParcelleEdi(stream);
            }

            if (RefSolProfondeurIndigo.class.equals(klass)) {
                importResult = importService.importSolProfondeurIndigo(stream);
            }

            if (RefSolCaracteristiqueIndigo.class.equals(klass)) {
                importResult = importService.importSolCarateristiquesIndigo(stream);
            }

            if (RefAdventice.class.equals(klass)) {
                importResult = importService.importAdventices(stream);
            }
    
            if (RefAgsAmortissement.class.equals(klass)) {
                importResult = importService.importAgsAmortissement(stream);
            }

            if (RefNuisibleEDI.class.equals(klass)) {
                importResult = importService.importNuisiblesEDI(stream);
            }

            if (RefFertiMinUNIFA.class.equals(klass)) {
                importResult = importService.importFertiMinUNIFA(stream);
            }

            if (RefFertiOrga.class.equals(klass)) {
                importResult = importService.importFertiOrga(stream);
            }
    
            if (RefInterventionAgrosystTravailEDI.class.equals(klass)) {
                importResult = importService.importInterventionAgrosystTravailEdiCSV(stream);
            }

            if (RefStadeEDI.class.equals(klass)) {
                importResult = importService.importStadesEdiCSV(stream);
            }

            if (RefUniteEDI.class.equals(klass)) {
                importResult = importService.importUniteEDI(stream);
            }

            if (RefStationMeteo.class.equals(klass)) {
                importResult = importService.importStationMeteo(stream);
            }

            if (RefGesCarburant.class.equals(klass)) {
                importResult = importService.importGesCarburants(stream);
            }
            if (RefGesEngrais.class.equals(klass)) {
                importResult = importService.importGesEngrais(stream);
            }
            if (RefGesPhyto.class.equals(klass)) {
                importResult = importService.importGesPhyto(stream);
            }
            if (RefGesSemence.class.equals(klass)) {
                importResult = importService.importGesSemences(stream);
            }
            if (RefNrjCarburant.class.equals(klass)) {
                importResult = importService.importNrjCarburants(stream);
            }
            if (RefNrjPhyto.class.equals(klass)) {
                importResult = importService.importNrjPhyto(stream);
            }
            if (RefNrjSemence.class.equals(klass)) {
                importResult = importService.importNrjSemences(stream);
            }
            if (RefNrjGesOutil.class.equals(klass)) {
                importResult = importService.importNrjGesOutils(stream);
            }
            if (RefMesure.class.equals(klass)) {
                importResult = importService.importMesure(stream);
            }

            if (RefSupportOrganeEDI.class.equals(klass)) {
                importResult = importService.importSupportOrganeEDI(stream);
            }

            if (RefStadeNuisibleEDI.class.equals(klass)) {
                importResult = importService.importStadeNuisibleEDI(stream);
            }
            if (RefTypeNotationEDI.class.equals(klass)) {
                importResult = importService.importTypeNotationEDI(stream);
            }
            if (RefValeurQualitativeEDI.class.equals(klass)) {
                importResult = importService.importValeurQualitativeEDI(stream);
            }
            if (RefUnitesQualifiantEDI.class.equals(klass)) {
                importResult = importService.importUnitesQualifiantEDI(stream);
            }

            if (RefActaSubstanceActive.class.equals(klass)) {
                importResult = importService.importActaSubstanceActive(stream);
            }

            if (RefProtocoleVgObs.class.equals(klass)) {
                importResult = importService.importProtocoleVgObs(stream);
            }

            if (RefElementVoisinage.class.equals(klass)) {
                importResult = importService.importElementVoisinage(stream);
            }

            if (RefPhytoSubstanceActiveIphy.class.equals(klass)) {
                importResult = importService.importPhytoSubstanceActiveIphy(stream);
            }

            if (RefTypeAgriculture.class.equals(klass)) {
                importResult = importService.importTypeAgriculture(stream);
            }

            if (RefActaGroupeCultures.class.equals(klass)) {
                importResult = importService.importActaGroupeCultures(stream);
            }

            if (RefActaDosageSPC.class.equals(klass)) {
                importResult = importService.importActaDosageSpc(stream);
            }

            if (RefSaActaIphy.class.equals(klass)) {
                importResult = importService.importSaActaIphy(stream);
            }
    
            if (RefCultureEdiGroupeCouvSol.class.equals(klass)) {
                importResult = importService.importCultureEdiGroupeCouvSol(stream);
            }

            if (RefCouvSolAnnuelle.class.equals(klass)) {
                importResult = importService.importCouvSolAnnuelle(stream);
            }

            if (RefCouvSolPerenne.class.equals(klass)) {
                importResult = importService.importCouvSolPerenne(stream);
            }

            if (RefActaTraitementsProduit.class.equals(klass)) {
                importResult = importService.importActaTraitementsProduits(stream);
            }

            if (RefActaTraitementsProduitsCateg.class.equals(klass)) {
                importResult = importService.importActaTraitementsProduitsCateg(stream);
            }

            if (RefTraitSdC.class.equals(klass)) {
                importResult = importService.importTraitSdC(stream);
            }

            if (RefZoneClimatiqueIphy.class.equals(klass)) {
                importResult = importService.importZoneClimatiqueIphy(stream);
            }

            if (RefActaDosageSpcRoot.class.equals(klass)) {
                importResult = importService.importRefActaDosageSpcRoot(stream);
            }

            if (RefActaDosageSaRoot.class.equals(klass)) {
                importResult = importService.importRefActaDosageSaRoot(stream);
            }

            if (RefActaProduitRoot.class.equals(klass)) {
                importResult = importService.importRefActaProduitRoot(stream);
            }

            if (RefEdaplosTypeTraitement.class.equals(klass)) {
                importResult = importService.importRefEdaplosTraitementProduit(stream);
            }

            if (RefDestination.class.equals(klass)) {
                importResult = importService.importRefDestination(stream);
            }

            if (RefQualityCriteria.class.equals(klass)) {
                importResult = importService.importRefQualityCriteria(stream);
            }

            if (RefHarvestingPrice.class.equals(klass)) {
                importResult = importService.importRefHarvestingPrice(stream);
            }

            if (RefHarvestingPriceConverter.class.equals(klass)) {
                importResult = importService.importRefHarvestingPriceConverter(stream);
            }

            if (RefSpeciesToSector.class.equals(klass)) {
                importResult = importService.importRefSpeciesToSector(stream);
            }

            if (RefStrategyLever.class.equals(klass)) {
                importResult = importService.importRefStrategyLever(stream);
            }

            if (RefAnimalType.class.equals(klass)) {
                importResult = importService.importRefAnimalType(stream);
            }

            if (RefMarketingDestination.class.equals(klass)) {
                importResult = importService.importRefMarketingDestination(stream);
            }

            if (RefInterventionTypeItemInputEDI.class.equals(klass)) {
                importResult = importService.importRefInternventionTypeItemInputEdi(stream);
            }

            if (RefQualityCriteriaClass.class.equals(klass)) {
                importResult = importService.importRefQualityCriteriaClass(stream);
            }

            if (RefPrixCarbu.class.equals(klass)) {
                importResult = importService.importRefPrixCarbuCSV(stream);
            }

            if (RefPrixEspece.class.equals(klass)) {
                importResult = importService.importRefPrixEspeceCSV(stream);
            }

            if (RefPrixFertiMin.class.equals(klass)) {
                importResult = importService.importRefPrixFertiMinCSV(stream);
            }

            if (RefPrixFertiOrga.class.equals(klass)) {
                importResult = importService.importRefPrixFertiOrgaCSV(stream);
            }

            if (RefPrixPhyto.class.equals(klass)) {
                importResult = importService.importRefPrixPhytoCSV(stream);
            }

            if (RefPrixIrrig.class.equals(klass)) {
                importResult = importService.importRefPrixIrrigCSV(stream);
            }

            if (RefPrixAutre.class.equals(klass)) {
                importResult = importService.importRefPrixAutreCSV(stream);
            }

            if (RefInputUnitPriceUnitConverter.class.equals(klass)) {
                importResult = importService.importRefInputUnitPriceUnitConverterCSV(stream);
            }

            if (RefEspeceOtherTools.class.equals(klass)) {
                importResult = importService.importRefEspeceOtherToolsCSV(stream);
            }

            if (RefCattleAnimalType.class.equals(klass)) {
                importResult = importService.importRefCattleAnimalTypesCSV(stream, referentialService.getAllActiveRefAnimalTypes());
            }

            if (RefCattleRationAliment.class.equals(klass)) {
                importResult = importService.importRefCattleRationAlimentsCSV(stream);
            }

            if (RefCiblesAgrosystGroupesCiblesMAA.class.equals(klass)) {
                importResult = importService.importRefCiblesAgrosystGroupesCiblesMAA(stream);
            }

            if (RefMAADosesRefParGroupeCible.class.equals(klass)) {
                importResult = importService.importRefMAADosesRefParGroupeCible(stream);
            }

            if (RefMAABiocontrole.class.equals(klass)) {
                importResult = importService.importRefMAABiocontrole(stream);
            }
    
            if (RefFeedbackRouter.class.equals(klass)) {
                importResult = importService.importRefFeedbackRouter(stream);
            }

            if (RefSubstrate.class.equals(klass)) {
                importResult = importService.importRefSubstrate(stream);
            }

            if (RefPrixSubstrate.class.equals(klass)) {
                importResult = importService.importRefPrixSubstrate(stream);
            }

            if (RefPot.class.equals(klass)) {
                importResult = importService.importRefPot(stream);
            }

            if (RefPrixPot.class.equals(klass)) {
                importResult = importService.importRefPrixPot(stream);
            }

            if (RefGroupeCibleTraitement.class.equals(klass)) {
                importResult = importService.importRefGroupeCibleTraitement(stream);
                importService.updateActaTraitementProduitsTraitement();
            }

            if (ReferentialI18nEntry.class.isAssignableFrom(klass)) {
                importResult = importService.importTraduction((Class<? extends ReferentialI18nEntry>)klass, stream);
            }
    
            if (RefOtherInput.class.equals(klass)) {
                importResult = importService.importRefOtherInputCSV(stream);
            }

            if (RefSubstancesActivesCommissionEuropeenne.class.equals(klass)) {
                importResult = importService.importRefSubstancesActivesCommissionEuropeenneCSV(stream);
            }
            
            if (RefCompositionSubstancesActivesParNumeroAMM.class.equals(klass)) {
                importResult = importService.importRefCompositionSubstancesActivesParNumeroAMM(stream);
            }

            if (RefPhrasesRisqueEtClassesMentionDangerParAMM.class.equals(klass)) {
                importResult = importService.importRefPhrasesRisqueEtClassesMentionDangerParAMM(stream);
            }

            if (RefConversionUnitesQSA.class.equals(klass)) {
                importResult = importService.importRefConversionUnitesQSA(stream);
            }

            if (RefSeedUnits.class.equals(klass)) {
                importResult = importService.importRefSeedUnits(stream);
            }
        }

        if (importResult != null) {
            
            noticeErrors(importResult);
            
            noticeWarnings(importResult);
    
            noticeSuccess(importResult);
    
            sendEmailReportMessages(importResult);
    
    
        } else if (LOGGER.isWarnEnabled()) {
            LOGGER.warn("Can't find valid export for class " + klass);
        }
        return SUCCESS;
    }
    
    protected void noticeSuccess(ImportResult importResult) {
        if (!importResult.hasErrors()) {
            notificationSupport.importSuccess("RÉUSSITE : Import terminé en %dms. %d créé(s) - %d modifié(s) - %d supprimé(s) - %d ignoré(s)",
                    importResult.getDuration(),
                    importResult.getCreated(),
                    importResult.getUpdated(),
                    importResult.getDeleted(),
                    importResult.getIgnored());
        }
    }
    
    protected void sendEmailReportMessages(ImportResult importResult) {
        try {
            AuthenticatedUser authenticatedUser = getAuthenticatedUser();
            sendImportReferentialReport(authenticatedUser, klass, importResult);
        } catch (Exception e) {
            // noting to do
        }
    }
    
    protected void noticeErrors(ImportResult importResult) {
        List<String> errors = importResult.getErrors();
        if (!errors.isEmpty()) {
            Iterator<String> messageParser = errors.iterator();
            int maxDisplayedMessages = MAX_DISPLAYED_MESSAGES;
            while (messageParser.hasNext() && maxDisplayedMessages > 0) {
                String message = messageParser.next();
                notificationSupport.error("ÉCHEC : %s", StringUtils.normalizeSpace(message));
                maxDisplayedMessages--;
            }
            if (errors.size() - MAX_DISPLAYED_MESSAGES > 0) {
                notificationSupport.error(String.format("et %d autres erreurs. Veuillez consulter votre boîte mail pour plus de détail.", errors.size() - MAX_DISPLAYED_MESSAGES));
            }
        }
    }
    
    protected void noticeWarnings(ImportResult importResult) {
        List<String> warnings = importResult.getWarnings();
        
        if (!warnings.isEmpty()) {
            notificationSupport.warning(String.format("ATTENTION : %d avertissements ont été constatés. Veuillez consulter votre boîte mail pour plus de détail.", warnings.size()));
        }
    }
    
    protected void sendImportReferentialReport(AuthenticatedUser user, Class<?> klass, ImportResult result) {

        if (!result.hasErrors() && !result.hasWarning()) return;
        
        InputStream content = logMessagesToStream(result.getErrors(), result.getWarnings());

        emailService.sendImportReferentialReport(user.getEmail(), klass, result, content);
    }

    protected InputStream logMessagesToStream(List<String> errors, List<String> warnings) {

        ByteArrayInputStream input;

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             Writer writer = new OutputStreamWriter(out, StandardCharsets.UTF_8)) {

            for (String error : errors) {
                writer.write(error + "\n");
            }
    
            for (String warning : warnings) {
                writer.write(warning + "\n");
            }

            writer.close();

            input = new ByteArrayInputStream(out.toByteArray());

        } catch (IOException e) {
            throw new AgrosystTechnicalException("Can't export import result", e);
        }

        return input;
    }

    @Override
    public void withUploadedFiles(List<UploadedFile> uploadedFiles) {
        if (LOGGER.isDebugEnabled()) {
            Collection<UploadedFile> uploadedFiles1 = CollectionUtils.emptyIfNull(uploadedFiles);
            LOGGER.debug(String.format("L'utilisateur %s à envoyé les fichiers d'import %s", getAuthenticatedUser().getEmail(), uploadedFiles1.stream().map(UploadedFile::getOriginalName).collect(Collectors.joining())));
        }
    }
}
