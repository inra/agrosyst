package fr.inra.agrosyst.web.actions.publishedmessage;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

/**
 * Created by davidcosse on 14/10/14.
 */
public class InfoMessageEdit extends AbstractAdminAction {

    @Serial
    private static final long serialVersionUID = 1L;

    protected String title;

    protected String content;

    @Override
    @Action("info-message-edit-input")
    public String input() throws Exception {
        return super.input();
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(title)){
            addActionError("Le titre est obligatoire");
        }
    }

    @Override
    @Action(results = {
            @Result(type = "redirectAction", params = {"actionName", "info-message-list"})})
    public String execute() throws Exception {
        checkIsAdmin();

        if (!Strings.isNullOrEmpty(title)) {
            messageService.publishMessage(title, content);
        }

        return SUCCESS;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
