package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Decision rule edit json actions.
 * 
 * @author Eric Chatellier
 */
public class DecisionRulesEditJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(DecisionRulesEditJson.class);

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 4109060690576341764L;

    protected transient ManagementModeService managementModeService;

    protected transient ReferentialService referentialService;

    /** Domain topia id (for decision-rules-edit-cropping-plan-entries-json). */
    protected String domainCode;

    /** Growing system id (for decision-rules-edit-cropping-plan-entries-json). */
    protected String growingSystemTopiaId;

    protected BioAgressorType bioAgressorType;

    /** Type de bio agresseur (for decision-rules-edit-bio-agressors-json). */
    protected Collection<BioAgressorType> bioAgressorTypes;

    protected String codeGroupeCibleMaa;

    protected String bioAgressorTopiaId;

    protected String decisionRuleName;

    protected AgrosystInterventionType agrosystInterventionType;

    protected String cropIds;
    protected String requiredDecisionRulesIds;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public void setGrowingSystemTopiaId(String growingSystemTopiaId) {
        this.growingSystemTopiaId = growingSystemTopiaId;
    }

    public void setBioAgressorType(BioAgressorType bioAgressorType) {
        this.bioAgressorType = bioAgressorType;
    }

    public void setBioAgressorTypes(String bioAgressorTypes) {
        Type type = new TypeToken<List<BioAgressorType>>() {}.getType();
        this.bioAgressorTypes = getGson().fromJson(bioAgressorTypes, type);
    }

    public void setDecisionRuleName(String decisionRuleName) {
        this.decisionRuleName = decisionRuleName;
    }

    public void setAgrosystInterventionType(AgrosystInterventionType agrosystInterventionType) {
        this.agrosystInterventionType = agrosystInterventionType;
    }

    public void setCropIds(String cropIds) {
        this.cropIds = cropIds;
    }

    public void setRequiredDecisionRulesIds(String requiredDecisionRulesIds) {
        this.requiredDecisionRulesIds = requiredDecisionRulesIds;
    }

    private List<String> convertJsonIdsToStringList(String json) {
        Type type = new TypeToken<List<String>>() {}.getType();
        return getGson().fromJson(json, type);
    }

    public void setCodeGroupeCibleMaa(String codeGroupeCibleMaa) {
        this.codeGroupeCibleMaa = codeGroupeCibleMaa;
    }

    public void setBioAgressorTopiaId(String bioAgressorTopiaId) {
        this.bioAgressorTopiaId = bioAgressorTopiaId;
    }

    /**
     * Récupération de la liste des cultures à la sélection d'un système de culture.
     * 
     * @return SUCCESS
     */
    @Action("decision-rules-edit-cropping-plan-entries-json")
    public String listCroppingPlanEntries() {
        try {
            jsonData = managementModeService.getDomainCodeCroppingPlanEntries(domainCode);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load crops for domain code '%s':",
                        domainCode), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    /**
     * Recuperation de la liste des bio agresseurs filtrée par type.
     * 
     * @return SUCCESS
     */
    @Action("decision-rules-edit-bio-agressors-json")
    public String listBioAgressors() {
        try {
            jsonData = referentialService.getBioAgressors(bioAgressorTypes);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load bioagressors for bioAgressorTypes '%s':", bioAgressorTypes), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("create-decision-rules-edit-json")
    public String createDecisionRules() {
        try {
            List<String> cropIds = convertJsonIdsToStringList(this.cropIds);
            jsonData = managementModeService.createNewDecisionRule(
                    agrosystInterventionType,
                    growingSystemTopiaId,
                    bioAgressorType,
                    codeGroupeCibleMaa,
                    bioAgressorTopiaId,
                    cropIds,
                    decisionRuleName);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to create new décision rules", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("load-required-decision-rules-json")
    public String loadRequiredDecisionRules() {
        try {
            List<String> requiredDecisionRules = convertJsonIdsToStringList(this.requiredDecisionRulesIds);
            jsonData = managementModeService.getAllDecisionRules(requiredDecisionRules);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load decision rules", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
