/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemDto;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemFilter;
import fr.inra.agrosyst.api.services.report.ReportService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

/**
 * Liste des bilans de campagne / échelle Système de culture.
 */
public class ReportGrowingSystemsList extends AbstractReportAction {

    private static final Log LOGGER = LogFactory.getLog(ReportGrowingSystemsList.class);
    @Serial
    private static final long serialVersionUID = -5631153546808063018L;
    
    protected transient ReportService reportService;

    protected PaginationResult<ReportGrowingSystemDto> reportGrowingSystems;

    protected ReportGrowingSystemFilter reportGrowingSystemFilter;

    protected int reportGrowingSystemsExportAsyncThreshold;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public String execute() {
        try {
            NavigationContext navigationContext = getNavigationContext();
            reportGrowingSystemFilter = new ReportGrowingSystemFilter();

            reportGrowingSystemFilter.setNavigationContext(navigationContext);
            reportGrowingSystemFilter.setPageSize(getListNbElementByPage(ReportGrowingSystemDto.class));

            reportGrowingSystems = reportService.getAllReportGrowingSystem(reportGrowingSystemFilter);

            reportGrowingSystemsExportAsyncThreshold = config.getReportGrowingSystemsExportAsyncThreshold();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load report growing system list", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }


    public PaginationResult<ReportGrowingSystemDto> getReportGrowingSystems() {
        return reportGrowingSystems;
    }

    public ReportGrowingSystemFilter getReportGrowingSystemFilter() {
        return reportGrowingSystemFilter;
    }

    public int getReportGrowingSystemsExportAsyncThreshold() {
        return reportGrowingSystemsExportAsyncThreshold;
    }

}
