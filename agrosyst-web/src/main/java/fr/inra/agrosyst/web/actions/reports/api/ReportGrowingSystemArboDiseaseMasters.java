/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports.api;

import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;

import java.io.Serial;
import java.util.List;
import java.util.function.Function;

public class ReportGrowingSystemArboDiseaseMasters extends AbstractReportGrowingSystemData<ArboCropPestMaster> {
    
    @Serial
    private static final long serialVersionUID = 1820326466213090064L;
    
    public Function<String, List<ArboCropPestMaster>> getDataMethod() {
        return reportService::loadArboCropDiseaseMasters;
    }
}
