package fr.inra.agrosyst.web.actions.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.UserFilter;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.Map;

/**
 * User list action.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UsersList extends AbstractAdminAction {

    @Serial
    private static final long serialVersionUID = -4895779258937582288L;

    protected transient UserService userService;

    protected UserFilter usersFilter;

    protected boolean importUsersAndRolesFileError;

    public void setImportUsersAndRolesFileError(boolean importUsersAndRolesFileError) {
        this.importUsersAndRolesFileError = importUsersAndRolesFileError;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * User result list serialized as JSON.
     */
    protected PaginationResult<UserDto> users;

    @Override
    public String execute() throws Exception {
        if (importUsersAndRolesFileError) {
            notificationSupport.usersAndRolesImportFailed("format de fichier incorrect");
            importUsersAndRolesFileError = false;
        }
        checkIsAdmin();
        usersFilter = new UserFilter();
        usersFilter.setActive(true);
        usersFilter.setPageSize(getListNbElementByPage(UserDto.class));
        users = userService.getFilteredUsers(usersFilter, true);
        return SUCCESS;
    }

    public PaginationResult<UserDto> getUsers() {
        return users;
    }

    public Map<RoleType, String> getRoleTypes() {
        return getEnumAsMap(RoleType.values());
    }

    public UserFilter getUsersFilter() {
        return usersFilter;
    }

}
