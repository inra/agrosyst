package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAAbstract;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Geoffroy Gley
 */
public class LoadMineralProductElementJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadMineralProductElementJson.class);

    @Serial
    private static final long serialVersionUID = -4004621338049762000L;

    @Override
    public String execute() {
        try {
            jsonData = getMineralProductElementNamesAndLibelles();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load refFertiMinUNIFA", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    // todo: refactor this method to use AbstractItkAction::getMineralProductElementNamesAndLibelles because this is a duplicated method
    public Map<String, String> getMineralProductElementNamesAndLibelles() {
        Field[] champs = RefFertiMinUNIFAAbstract.class.getDeclaredFields();
        // to keep field ordering
        LinkedHashMap<String, String> result = new LinkedHashMap<>();
        for (Field champ : champs) {
            String propertyName = champ.getName();
            String libelle = getText(RefFertiMinUNIFAAbstract.class.getName() + "." + propertyName);
            // only add elements (N, Po5, K2O, ...) that are translated
            if (!libelle.startsWith(RefFertiMinUNIFAAbstract.class.getName())) {// key is translated
                result.put(propertyName, libelle);
            }
        }
        return result;
    }
}
