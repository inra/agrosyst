package fr.inra.agrosyst.web.actions.practiced;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Kevin Morin
 */

public class LoadReferenceDoseJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadReferenceDoseJson.class);
    @Serial
    private static final long serialVersionUID = -3_566_232_755_453_160_692L;

    @Setter
    protected transient ReferentialService referentialService;
    @Setter
    protected transient DomainService domainService;

    @Setter
    protected transient String refInputId;
    protected transient Set<String> speciesIds;

    @Setter
    protected Integer campaign;

    /**
     * Equivalent de la colonne cible_edi_ref_id de RefCiblesAgrosystGroupesCiblesMAA
     * Identifiant pour les adventices, reference_id pour les nuisibles edi
     */
    protected transient Set<String> targetIds;
    protected transient Set<String> groupesCibles;

    public void setSpeciesIds(String speciesIds) {
        this.speciesIds = getGson().fromJson(speciesIds, new TypeToken<Set<String>>(){}.getType());
    }

    public void setTargetIds(String targetIds) {
        this.targetIds = getGson().fromJson(targetIds, new TypeToken<Set<String>>(){}.getType());
    }

    public void setGroupesCibles(String groupesCibles) {
        this.groupesCibles = getGson().fromJson(groupesCibles, new TypeToken<Set<String>>(){}.getType());
    }

    @Override
    public String execute() throws Exception {
        try {
            ReferenceDoseDTO referenceDoseIFTLegacy =
                    referentialService.computeLocalizedReferenceDoseForIFTLegacy(refInputId, speciesIds);
            ReferenceDoseDTO referenceDoseIFTCibleNonMillesime =
                    referentialService.computeLocalizedReferenceDoseForIFCCibleNonMillesime(refInputId, speciesIds, targetIds, groupesCibles);
            ReferenceDoseDTO referenceDoseIFTCibleMillesime =
                    referentialService.computeReferenceDoseForIFCCibleMillesime(refInputId, speciesIds, targetIds, groupesCibles, campaign);

            Map<String, ReferenceDoseDTO> result = new HashMap<>();
            result.put("IFT_Ancienne", referenceDoseIFTLegacy);
            result.put("IFT_Cible_NonMillesime", referenceDoseIFTCibleNonMillesime);
            result.put("IFT_Cible_Millesime", referenceDoseIFTCibleMillesime);
            jsonData = result;

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load acta Product for refInputId '%s' and refEspeceIds '%s':",
                        refInputId, this.speciesIds), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
}
