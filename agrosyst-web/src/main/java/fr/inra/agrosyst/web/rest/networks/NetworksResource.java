package fr.inra.agrosyst.web.rest.networks;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.web.rest.CustomInject;
import fr.inra.agrosyst.web.rest.Secured;
import fr.inra.agrosyst.web.rest.common.PaginationResultDto;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.stream.Collectors;

@Secured
@Path("/networks")
public class NetworksResource {

    @CustomInject
    private NetworkService networkService;

    private NetworkFilter buildFilter(NavigationContext navigationContext, String name, String manager,
                                      Boolean active, Set<String> selectedIds, Integer pageNumber, Integer pageSize) {
        var filter = new NetworkFilter();
        filter.setNavigationContext(navigationContext);
        filter.setNetworkName(name);
        filter.setNetworkManager(manager);
        filter.setActive(active);
        if (selectedIds != null && !selectedIds.isEmpty()) filter.setSelectedIds(selectedIds);
        if (pageNumber != null) filter.setPage(pageNumber);
        if (pageSize != null) filter.setPageSize(pageSize);
        return filter;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAll(@CookieParam("nav.context") Cookie jsonContext,
                            NetworkRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getName(),
                requestFilter.getResponsable(),
                requestFilter.getActive(),
                requestFilter.getSelectedIds(),
                requestFilter.getPageNumber(),
                requestFilter.getPageSize());

        var paginationResult = networkService.getFilteredNetworks(filter);
        var networks = paginationResult.getElements().stream()
                .map((n) -> {
                    var managersAsStr = n.getManagers().stream().map((m) -> m.getAgrosystUser().getFirstName() + " " + m.getAgrosystUser().getLastName())
                            .collect(Collectors.joining(", "));
                    return new NetworkDto(
                            StringUtils.remove(n.getTopiaId(), Network.class.getName()),
                            n.getName(),
                            managersAsStr,
                            n.isActive());
                })
                .toList();
        Response response = Response.ok()
                .entity(new PaginationResultDto<>(networks, paginationResult.getCount(), paginationResult.getCurrentPage()))
                .build();
        return response;
    }

    @GET
    @Path("/filtered-ids")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIds(@CookieParam("nav.context") Cookie jsonContext,
                            NetworkRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getName(),
                requestFilter.getResponsable(),
                requestFilter.getActive(),
                null,
                null,
                null);
        var ids = networkService.getFilteredNetworkIds(filter);
        Response response = Response.ok()
                .entity(ids)
                .build();
        return response;
    }
}
