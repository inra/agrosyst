package fr.inra.agrosyst.web.actions.context;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Results({
        @Result(type = "agrosyst-json", name = "success"),
        @Result(type = "agrosyst-json", name = "error")
})
public class ContextChooseRaw extends AbstractAgrosystAction implements Preparable {

    @Serial
    private static final long serialVersionUID = 8311330060409844511L;
    
    /**
     * Return true for each value. (Used for javascript selection standard).
     */
    private static final Function<String, Boolean> GET_TRUE = input -> Boolean.TRUE;
    private static final Function<Object, String> TO_STRING = String::valueOf;

    protected transient Object jsonData = null;

    protected ArrayList<Integer> campaignsList;
    protected PaginationResult<DomainDto> domains;
    protected PaginationResult<Network> networks;
    protected PaginationResult<GrowingPlanDto> growingPlans;
    protected PaginationResult<GrowingSystemDto> growingSystems;
    
    protected transient NavigationContext navigationContext;
    protected transient Set<Integer> selectedCampaigns;
    protected transient Set<String> selectedNetworks;
    protected transient Set<String> selectedDomains;
    protected transient Set<String> selectedGrowingPlans;
    protected transient Set<String> selectedGrowingSystems;

    @Override
    public void prepare() {
    }

    public void setSelectedCampaigns(String selectedCampaigns) {
        Set<String> selectedCampaignsAsString = getGson().fromJson(selectedCampaigns, Set.class);
        this.selectedCampaigns = selectedCampaignsAsString.stream()
                .map(Integer::parseInt)
                .collect(Collectors.toSet());
    }

    public void setSelectedNetworks(String selectedNetworks) {
        this.selectedNetworks = getGson().fromJson(selectedNetworks, Set.class);
    }

    public void setSelectedDomains(String selectedDomains) {
        this.selectedDomains = getGson().fromJson(selectedDomains, Set.class);
    }

    public void setSelectedGrowingPlans(String selectedGrowingPlans) {
        this.selectedGrowingPlans = getGson().fromJson(selectedGrowingPlans, Set.class);
    }

    public void setSelectedGrowingSystems(String selectedGrowingSystems) {
        this.selectedGrowingSystems = getGson().fromJson(selectedGrowingSystems, Set.class);
    }

    @Override
    @Action("context-choose-raw-input")
    public String input() throws Exception {
        navigationContext = super.getNavigationContext();

        Set<Integer> campaignForFiltering = navigationContext.getCampaigns();
        Set<String> networksForFiltering = navigationContext.getNetworks();
        Set<String> domainsForFiltering = navigationContext.getDomains();
        Set<String> growingPlansForFiltering = navigationContext.getGrowingPlans();

        campaignsList = new ArrayList<>(navigationContextService.getAllCampaigns());
        networks = navigationContextService.getAllNetworks(getListNbElementByPage(Network.class, NAVIGATION_CONTEXT));
        domains = navigationContextService.getAllDomainsForCampaign(campaignForFiltering, networksForFiltering, getListNbElementByPage(DomainDto.class, NAVIGATION_CONTEXT));
        growingPlans = navigationContextService.getAllGrowingPlansForDomains(campaignForFiltering, domainsForFiltering,
                networksForFiltering, getListNbElementByPage(GrowingPlanDto.class, NAVIGATION_CONTEXT));
        growingSystems = navigationContextService.getAllGrowingSystemsForGrowingPlans(campaignForFiltering, domainsForFiltering,
                growingPlansForFiltering, networksForFiltering, getListNbElementByPage(GrowingSystemDto.class, NAVIGATION_CONTEXT));
        return INPUT;
    }

    @Override
    public String execute() throws Exception {

        NavigationContext newNavigationContext = new NavigationContext();
        if (selectedCampaigns != null) {
            newNavigationContext.getCampaigns().addAll(selectedCampaigns);
        }

        if (selectedNetworks != null) {
            newNavigationContext.getNetworks().addAll(selectedNetworks);
        }

        if (selectedDomains != null) {
            newNavigationContext.getDomains().addAll(selectedDomains);
        }

        if (selectedGrowingPlans != null) {
            newNavigationContext.getGrowingPlans().addAll(selectedGrowingPlans);
        }

        if (selectedGrowingSystems != null) {
            newNavigationContext.getGrowingSystems().addAll(selectedGrowingSystems);
        }

        verifyAndSaveNavigationContext(newNavigationContext);

        return SUCCESS;
    }

    protected Map<String, Boolean> toMap(Set<String> set) {
        Map<String, Boolean> result;
        if (set.isEmpty()) {
            result = new HashMap<>();
        } else {
            result = Maps.toMap(set, GET_TRUE::apply);
        }
        return result;
    }

    public List<String> getCampaigns() {
        List<String> result = new ArrayList<>();
        if (!campaignsList.isEmpty()) {
            Iterable<String> campaignStr = campaignsList.stream().map(TO_STRING).collect(Collectors.toList());
            Iterables.addAll(result, campaignStr);
        }
        return result;
    }

    public Map<String, Boolean> getSelectedCampaigns() {
        Set<Integer> campaigns = navigationContext.getCampaigns();
        Map<String, Boolean> result;
        if (!campaigns.isEmpty()) {
            Iterable<String> campaignStr = campaigns.stream().map(TO_STRING).collect(Collectors.toList());
            result = Maps.toMap(campaignStr, GET_TRUE::apply);
        } else {
            result = new HashMap<>();
        }
        return result;
    }

    public PaginationResult<Network> getNetworks() {
        return networks;
    }

    public Map<String, Boolean> getSelectedNetworks() {
        Set<String> networks = navigationContext.getNetworks();
        Map<String, Boolean> result = toMap(networks);
        return result;
    }

    public PaginationResult<DomainDto> getDomains() {
        return domains;
    }

    public Map<String, Boolean> getSelectedDomains() {
        Set<String> domains = navigationContext.getDomains();
        Map<String, Boolean> result = toMap(domains);
        return result;
    }

    public PaginationResult<GrowingPlanDto> getGrowingPlans() {
        return growingPlans;
    }

    public Map<String, Boolean> getSelectedGrowingPlans() {
        Set<String> dispositifs = navigationContext.getGrowingPlans();
        Map<String, Boolean>  result = toMap(dispositifs);
        return result;
    }

    public PaginationResult<GrowingSystemDto> getGrowingSystems() {
        return growingSystems;
    }

    public Map<String, Boolean> getSelectedGrowingSystems() {
        Set<String> sdcs = navigationContext.getGrowingSystems();
        Map<String, Boolean> result = toMap(sdcs);
        return result;
    }

    public NavigationContext getNavigationContext() {
        return navigationContext;
    }

    public Map<DomainType, String> getDomainTypes() {
        return i18nService.getEnumTranslationMap(DomainType.class);
    }

    public Object getJsonData() {
        return new Object();
    }

    public Map<TypeDEPHY, String> getDephyTypes() {
        return getEnumAsMap(TypeDEPHY.DEPHY_FERME, TypeDEPHY.DEPHY_EXPE, TypeDEPHY.NOT_DEPHY);
    }

}
