package fr.inra.agrosyst.web.actions.generic;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.generic.GenericEntityService;
import fr.inra.agrosyst.api.services.generic.GenericFilter;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.Map;

public class GenericEntitiesListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(GenericEntitiesListJson.class);

    @Serial
    private static final long serialVersionUID = -4267342874222046159L;

    protected transient GenericEntityService service;

    protected transient String genericClassName;

    protected String filter;
    
    public void setService(GenericEntityService service) {
        this.service = service;
    }

    public void setGenericClassName(String genericClassName) {
        this.genericClassName = genericClassName;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }


    @Override
    public String execute() {
        try {
            GenericFilter rawFilter = getGson().fromJson(filter, GenericFilter.class);
            Class<?> klass = Class.forName(genericClassName);
            writeListNbElementByPage(klass, rawFilter.getPageSize());
            PaginationResult<?> paginatedResult = service.listEntitiesFromString(genericClassName, rawFilter);
            if (klass.isEnum()) {
                // cf fr.inra.agrosyst.web.actions.generic.GenericEntitiesList.prepare
                for (Object entity: paginatedResult.getElements()) {
                    Map<String, String> entityMap = (Map<String, String>) entity;
                    String name = entityMap.get("Valeur");
                    String traduction = getText(klass.getName() + "." + name);
                    entityMap.put("Traduction", traduction);
                }
            }
            jsonData = paginatedResult;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Filed to load elements for '%s':",
                        genericClassName), e);
            }
            return ERROR;
        }
    
        return SUCCESS;
    }
}
