package fr.inra.agrosyst.web.actions.reports;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.report.GlobalMasterLevelQualifier;
import fr.inra.agrosyst.api.entities.report.PressureScale;
import fr.inra.agrosyst.api.entities.report.SectorSpecies;
import fr.inra.agrosyst.api.entities.report.YieldLossCause;
import fr.inra.agrosyst.api.entities.report.YieldObjective;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;

import java.io.Serial;
import java.util.Map;

public abstract class AbstractReportAction extends AbstractAgrosystAction {
    
    @Serial
    private static final long serialVersionUID = 1907928248902340739L;
    
    public Map<SectorSpecies, String> getSectorSpecies() {
        return getEnumAsMap(SectorSpecies.values());
    }
    
    public Map<GlobalMasterLevelQualifier, String> getGlobalMasterLevelQualifiers() {
        return getEnumAsMap(GlobalMasterLevelQualifier.values());
    }

    public Map<PressureScale, String> getVitiAdventicePressureScales() {
        return i18nService.enumPressureScaleVitiAdventice();
    }

    public Map<YieldObjective, String> getYieldObjectives() {
        return getEnumAsMap(YieldObjective.values());
    }

    public Map<YieldLossCause, String> getVitiYieldLossCauses() {
        return i18nService.enumYieldLossCauseArboViti();
    }

}
