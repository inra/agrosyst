package fr.inra.agrosyst.web.actions.auth;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class ForgottenPassword extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 2328784341047848966L;
    
    private UserService userService;
    
    private String email;
    private boolean reminderSent;
    
    private String next;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setNext(String next) {
        this.next = next;
    }
    
    public String getNext() {
        return next;
    }
    
    @Override
    @Action("forgotten-password-input")
    public String input() throws Exception {
        return super.input();
    }

    @Override
    public String execute() throws Exception {
        if (StringUtils.isNotBlank(email)) {
            reminderSent = userService.askForPasswordReminder(email, next);
            if (!reminderSent && userService.isValidEmail(email) && !userService.isUserActive(email)) {
                addFieldError("email", "Votre compte est désactivé, merci de contacter un administrateur d'Agrosyst.");
            } else {
                addActionMessage(String.format("Email de récupération du mot de passe envoyé à l'adresse %s, consultez votre boîte mail", email));
                reminderSent = true;
            }
        }
        return INPUT;
    }

    public String getEmail() {
        return email;
    }

    public boolean isReminderSent() {
        return reminderSent;
    }

}
