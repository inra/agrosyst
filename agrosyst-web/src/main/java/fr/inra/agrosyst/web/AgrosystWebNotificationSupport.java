package fr.inra.agrosyst.web;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.users.UserDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.i18n.I18n;

import java.io.Serial;
import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Classe qui gère les notifications. Cette classe est disponible pour toute action et conserve les messages tant qu'ils
 * n'ont pas été consommés.
 */
public class AgrosystWebNotificationSupport implements Serializable {

    @Serial
    private static final long serialVersionUID = -4427471517335097967L;

    protected final LinkedHashSet<AgrosystWebNotification> infoNotifications = new LinkedHashSet<>();

    protected final LinkedHashSet<AgrosystWebNotification> warningNotifications = new LinkedHashSet<>();

    protected final LinkedHashSet<AgrosystWebNotification> errorNotifications = new LinkedHashSet<>();

    protected final LinkedHashSet<AgrosystWebNotification> lightBoxNotifications = new LinkedHashSet<>();

    @Setter
    @Getter
    protected Language language;

    @Override
    public String toString() {
        return "AgrosystWebNotificationSupport{" +
                "infoNotifications=" + infoNotifications +
                ", warningNotifications=" + warningNotifications +
                ", errorNotifications=" + errorNotifications +
                ", lightBoxNotifications=" + lightBoxNotifications +
                '}';
    }

    public boolean isEmpty() {
        boolean result = infoNotifications.isEmpty()
                && warningNotifications.isEmpty()
                && errorNotifications.isEmpty()
                && lightBoxNotifications.isEmpty();
        return result;
    }

    protected void addInfoNotification(AgrosystWebNotification msg) {
        this.infoNotifications.add(msg);
    }

    protected void addWarningNotification(AgrosystWebNotification msg) {
        this.warningNotifications.add(msg);
    }

    protected void addErrorNotification(AgrosystWebNotification msg) {
        this.errorNotifications.add(msg);
    }

    protected void addLightBoxNotification(AgrosystWebNotification msg) {
        this.lightBoxNotifications.add(msg);
    }

    public String getTranslatedText(String messageKey, Object... args) {
        Locale locale = Optional.ofNullable(language).map(Language::getLocale).orElse(Locale.FRENCH);
        String translatedText = I18n.l(locale, messageKey, args);
        return translatedText;
    }

    public void info(String message, Object... args) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message, args);
        addInfoNotification(msg);
    }

    public void error(String message, Object... args) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message, args);
        addErrorNotification(msg);
    }

    public void warning(String message, Object... args) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message, args);
        addWarningNotification(msg);
    }

    public void domainSaved(Domain domain) {
        String text = getTranslatedText("domain.save.confirmation", domain.getName());
        addInfoNotification(new AgrosystWebNotification(text));
    }

    public void domainExtendError(String message) {
        error(message);
    }

    public void domainDeletionError(String message) {
        error(message);
    }

    public void domainDeleted(String domainNameAndCampagne) {
        String text = getTranslatedText("domain.deleted", domainNameAndCampagne);
        addInfoNotification(new AgrosystWebNotification(text));
    }

    public void importSuccess() {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Import terminé.");
        addInfoNotification(msg);
    }

    public void importError(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Erreur d'import du fichier : %s", message);
        addErrorNotification(msg);
    }

    public void growingPlanSaved(GrowingPlan growingPlan) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Dispositif '%s' enregistré", growingPlan.getName());
        addInfoNotification(msg);
    }

    public void growingSystemSaved(GrowingSystem growingSystem) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Système de culture '%s' enregistré", growingSystem.getName());
        addInfoNotification(msg);
    }

    public void importSuccess(String message, long duration, int created, int updated, int deleted, int ignored) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message, duration, created, updated, deleted, ignored);
        addInfoNotification(msg);
    }

    public void practicedSystemSaved(PracticedSystem practicedSystem) {
        String text = getTranslatedText("practicedSystem.save.confirmation", practicedSystem.getName());
        addInfoNotification(new AgrosystWebNotification(text));
    }

    public void networkSaved(Network network) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Réseau '%s' enregistré", network.getName());
        addInfoNotification(msg);
    }

    public void plotSaved(Plot plot) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Parcelle '%s' enregistrée", plot.getName());
        addInfoNotification(msg);
    }

    public void plotActivatedStatus(List<String> plots, boolean activate) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("%d parcelle%s %s", plots.size(), plots.isEmpty() || plots.size() == 1 ? "" : "s",activate ? "activée" : "désactivées");
        addInfoNotification(msg);
    }

    public void plotActivatedStatus(Integer nbUpdated, boolean activate) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("%d parcelle%s %s", nbUpdated, nbUpdated == 0 || nbUpdated == 1 ? "" : "s",activate ? "activée" : "désactivées");
        addInfoNotification(msg);
    }

    public void zoneActivatedStatus(Integer nbUpdated, boolean activate) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("%d zone%s %s", nbUpdated, nbUpdated == 0 || nbUpdated == 1 ? "" : "s",activate ? "activée" : "désactivées");
        addInfoNotification(msg);
    }

    public void plotMerged(Plot plot) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Parcelles fusionnées en '%s'", plot.getName());
        addInfoNotification(msg);
    }

    public void practicedPlotSaved(PracticedPlot practicedPlot) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Parcelle type '%s' enregistrée", practicedPlot.getName());
        addInfoNotification(msg);
    }

    public void userSaved(UserDto user) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Utilisateur '%s %s' enregistré", user.getLastName(), user.getFirstName());
        addInfoNotification(msg);
    }

    public void authenticatedUserSaved(UserDto user) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Profil enregistré");
        addInfoNotification(msg);
    }

    public void decisionRuleSaved(DecisionRule decisionRule) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Règle de décision '%s' enregistrée", decisionRule.getName());
        addInfoNotification(msg);
    }

    public void decisionRuleDuplicated(DecisionRule decisionRule) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Règle de décision '%s' dupliquée", decisionRule.getName());
        addInfoNotification(msg);
    }

    public void managementModeDuplicated() {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Modèle décisionnel dupliqué");
        addInfoNotification(msg);
    }

    public void managementModeDeleted(int nbRemoved) {
        String plural = nbRemoved > 1 ? "s" : "";
        AgrosystWebNotification msg = AgrosystWebNotification.of(String.format("%d modèle%s décisionnel%s supprimé%s", nbRemoved, plural, plural, plural));
        addInfoNotification(msg);
    }

    public void newDecisionRuleCreated(DecisionRule newDecisionRule) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Version %d de la règle de décision '%s' créée",
                newDecisionRule.getVersionNumber(),
                newDecisionRule.getName());
        addInfoNotification(msg);
    }

    public void newManagementModeCreated(ManagementMode managementMode) {
        String txtMsg = managementMode.getCategory() == ManagementModeCategory.PLANNED ?
                "Modèle décisionnel prévu créé" : "Modèle décisionnel constaté créé";
        AgrosystWebNotification msg = AgrosystWebNotification.of(txtMsg);
        addInfoNotification(msg);
    }

    public void managementModeSaved(ManagementMode managementMode) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Modèle décisionnel enregistré");
        addInfoNotification(msg);
    }

    public void culturalInterventionCreated() {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Interventions culturales enregistrées");
        addInfoNotification(msg);
    }

    public void performanceSaved(Performance performance) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Performance enregistrée. Le fichier est en cours de génération...");
        addInfoNotification(msg);
    }

    protected void notWritable(String entityType, String pronom) {
        String message = String.format("Vous n'avez pas les droits nécessaires pour modifier %s %s", pronom, entityType);
        AgrosystWebNotification msg = AgrosystWebNotification.of(message);
        addWarningNotification(msg);
    }
    
    public void defaultMaterialUsed() {
        String message ="Vous avez du matériel par défaut importé depuis eDaplos, merci de compléter ce matériel.";
        AgrosystWebNotification msg = AgrosystWebNotification.of(message);
        addWarningNotification(msg);
    }

    protected void notWritable(String entityType) {
        notWritable(entityType, "ce");
    }

    public void domainNotWritable() {
        notWritable("domaine");
    }

    public void growingPlanNotWritable() {
        notWritable("dispositif");
    }

    public void growingSystemNotWritable() {
        notWritable("système de culture");
    }

    public void networkNotWritable() {
        notWritable("réseau");
    }

    public void practicedSystemNotWritable() {
        notWritable("système synthétisé");
    }

    public void managementModeNotWritable() {
        notWritable("modèle décisionnel");
    }

    public void decisionRuleNotWritable() {
        notWritable("règle de décision", "cette");
    }

    public void effectiveCropCyclesNotWritable() {
        notWritable("cycles réalisés", "ces");
    }

    public void performanceNotWritable() {
        notWritable("performance", "cette");
    }

    public void plotNotWritable() {
        notWritable("parcelle", "cette");
    }

    public void practicedPlotNotWritable() {
        notWritable("parcelle type", "cette");
    }

    public void domainValidated(Domain domain) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Domaine '%s' validé pour la campagne '%d-%d'",
                domain.getName(),
                domain.getCampaign() - 1,
                domain.getCampaign());
        addInfoNotification(msg);
    }

    public void growingSystemValidated(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message);
        addInfoNotification(msg);
    }
    
    public void growingSystemValidatedError(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message);
        addErrorNotification(msg);
    }

    public void practicedSystemValidated(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message);
        addInfoNotification(msg);
    }
    
    public void practicedSystemValidatedError(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(message);
        addErrorNotification(msg);
    }

    public void growingPlanValidated(GrowingPlan growingPlan) {
        int campaign = growingPlan.getDomain().getCampaign();
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Dispositif '%s' validé pour la campagne '%d-%d'",
                growingPlan.getName(),
                campaign - 1,
                campaign);
        addInfoNotification(msg);
    }

    public void userImported(ImportResult result) {
        if(result.hasErrors()) {
            for (String error :result.getErrors()) {
                AgrosystWebNotification msg = AgrosystWebNotification.of(
                    error
                );
                addInfoNotification(msg);
            }
        } else {
            AgrosystWebNotification msg = AgrosystWebNotification.of(
                    "%d utilisateurs créés. %d email envoyés. %d lignes ignorées.",
                    result.getCreated(),
                    result.getCreated(),
                    result.getIgnored());
            addInfoNotification(msg);
        }
    }

    public void userRolesUpdated(ImportResult result) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Rôles de %d utilisateurs mis à jour (%d lignes ignorées).",
                result.getCreated(), result.getIgnored());
        addInfoNotification(msg);
    }

    public void networkImported(ImportResult result) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "%d réseaux mis à jour, %d réseaux créé(s), erreur(s) : %s",
                result.getUpdated(),
                result.getCreated(),
                result.getErrors());
        addInfoNotification(msg);
    }

    public void plotNotMergeable() {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Les parcelles ne peuvent être fusionnées!"
        );
        addWarningNotification(msg);
    }

    public void practicedSystemDuplicate() {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Système synthétisé dupliqué."
        );
        addInfoNotification(msg);
    }

    public void practicedSystemNotDuplicable(String practicedSystemId, String growingSystemId) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Le système synthétisé n'a pu être dupliqué ! " +
                        "Duplication depuis le système synthétisé avec comme identifiant :&nbsp;%s sur le système de culture avec comme identifiant &nbsp;:%s",
                practicedSystemId,
                growingSystemId
        );
        addWarningNotification(msg);
    }

    public void practicedSystemNotDuplicable(String error) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "La duplication du système synthétisé à rencontré un problème: %s" , error
        );
        addWarningNotification(msg);
    }

    public void effectiveCropCycleDuplicate() {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Cycles de cultures dupliqués."
        );
        addInfoNotification(msg);
    }

    public void effectiveCropCycleNotDuplicable() {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Les cycles de cultures n'ont pu être dupliqués!"
        );
        addWarningNotification(msg);
    }

    public void usersImportFailed(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Echec de l'import du fichier des utilisateurs! %s", message
        );
        addErrorNotification(msg);
    }

    public void rulesImportFailed(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of(
                "Echec de l'import du fichier des rôles! %s", message
        );
        addErrorNotification(msg);
    }

    public void usersAndRolesImportFailed(String message) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Echec d'import des utilisateurs et rôles : %s", message);
        addErrorNotification(msg);
    }

    public void eDaplosDataImported(Map<Class, ImportResult> importResult) {
        if (importResult != null && !importResult.isEmpty()){
            ImportResult result = importResult.get(Domain.class);
            AgrosystWebNotification msg = AgrosystWebNotification.of(
                    "%d domain(s) mis à jour, %d domain(s) créé(s)",
                    result.getUpdated(),
                    result.getCreated());
            addInfoNotification(msg);

            result = importResult.get(Plot.class);
            msg = AgrosystWebNotification.of(
                    "%d parcelle(s) mise(s) à jour, %d parcelle(s) créée(s)",
                    result.getUpdated(),
                    result.getCreated());
            addInfoNotification(msg);
        }

    }

    public void reportRegionalSaved(ReportRegional reportRegional) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Bilan de campagne / échelle regionale '%s' enregistré", reportRegional.getName());
        addInfoNotification(msg);
    }

    public void reportRegionalNotWritable() {
        notWritable("bilan de campagne / échelle régional");
    }

    public void reportGrowingSystemSaved(ReportGrowingSystem reportGrowingSystem) {
        AgrosystWebNotification msg = AgrosystWebNotification.of("Bilan de campagne / échelle système de culture '%s' enregistré", reportGrowingSystem.getName());
        addInfoNotification(msg);
    }

    public void reportGrowingSystemNotWritable() {
        notWritable("bilan de campagne / échelle système de culture");
    }

    public void failedDeleteRegionalReports(List<String> reportRegionalIds, List<String> reportGrowingSystemeNames) {
    
        this.lightBoxNotifications.clear();
    
        if (CollectionUtils.isNotEmpty(reportGrowingSystemeNames)) {
            String deleteReportsMessage;
            String part0 = reportRegionalIds.size() == 1 ? "La suppression du bilan de campagne échelle régionale est impossible car il est toujours utilisé " : "La suppression des bilans de campagnes échelle régionale est impossible car ils sont toujours utilisés ";
            String body;
            
            if (reportGrowingSystemeNames.size() == 1) {
                body = "par le bilan de campagne échelle système de culture suivant&nbsp:<ul><li><b>'" + reportGrowingSystemeNames.get(0) + "'</b></li></ul>";
            } else {
                StringBuilder names = new StringBuilder("<ul>");
                for (String reportGrowingSystameName : reportGrowingSystemeNames) {
                    names.append("<li><b>").append(reportGrowingSystameName).append("</b></li>");
                }
                names.append("</ul>");
                
                body = "par les bilans de campagne échelle système de culture suivants&nbsp:" + names;
        
            }
    
            deleteReportsMessage = part0 + body;
            
            AgrosystWebNotification msg = AgrosystWebNotification.of(deleteReportsMessage);

            addLightBoxNotification(msg);
        }

    }

    public Set<AgrosystWebNotification> consumeInfoNotifications() {
        Set<AgrosystWebNotification> result = new LinkedHashSet<>(this.infoNotifications);
        // remove read info messages
        this.infoNotifications.clear();
        return result;
    }

    public Set<AgrosystWebNotification> consumeWarningNotifications() {
        Set<AgrosystWebNotification> result = new LinkedHashSet<>(this.warningNotifications);
        // remove read warning messages
        this.warningNotifications.clear();
        return result;
    }

    public Set<AgrosystWebNotification> consumeErrorNotifications() {
        Set<AgrosystWebNotification> result = new LinkedHashSet<>(this.errorNotifications);
        // remove read error messages
        this.errorNotifications.clear();
        return result;
    }

    public Set<AgrosystWebNotification> consumeLightBoxNotifications() {
        Set<AgrosystWebNotification> result = new LinkedHashSet<>(this.lightBoxNotifications);
        this.lightBoxNotifications.clear();
        return result;
    }

    public boolean isInfoNotificationsEmpty() {
        boolean result = this.infoNotifications.isEmpty();
        return result;
    }

    public boolean isErrorNotificationsEmpty() {
        boolean result = this.errorNotifications.isEmpty();
        return result;
    }

    public boolean isWarningNotificationsEmpty() {
        boolean result = this.warningNotifications.isEmpty();
        return result;
    }
}
