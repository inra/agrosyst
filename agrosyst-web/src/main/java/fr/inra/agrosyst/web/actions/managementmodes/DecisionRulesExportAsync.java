package fr.inra.agrosyst.web.actions.managementmodes;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;
import java.util.List;

/**
 * Classe qui permet de déclencher un export asynchrone des règles de décision.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class DecisionRulesExportAsync extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = 4620374134218137834L;

    protected transient ManagementModeService managementModeService;

    protected List<String> decisionRuleIds;

    public void setDecisionRuleIds(String decisionRuleIds) {
        this.decisionRuleIds = getGson().fromJson(decisionRuleIds, List.class);
    }

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    @Override
    public String execute() {
        managementModeService.exportDecisionRulesAsXlsAsync(decisionRuleIds);
        return SUCCESS;
    }

}
