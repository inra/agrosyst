package fr.inra.agrosyst.web.actions.auth;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import fr.inra.agrosyst.web.converters.LocalDateTimeHelper;
import fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;
import java.time.LocalDateTime;
import java.time.Month;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class Login extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 3750812084600471474L;

    private static final Log LOGGER = LogFactory.getLog(Login.class);

    /**
     * Si l'utilisateur n'a jamais eu de message, on lui affiche tout depuis le début, d'où la date lontaine.
     */
    private static final LocalDateTime DEFAULT_BROADCAST_DATE =
            LocalDateTime.of(2014, Month.JANUARY, 1, 0, 0);

    protected transient UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    protected String email;
    protected String password;
    protected String next;
    protected String actionMessage;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setNext(String next) {
        this.next = next;
    }
    
    public void setActionMessage(String actionMessage) {
        this.actionMessage = actionMessage;
    }
    
    @Override
    @Action("login-input")
    public String input() throws Exception {
        if(StringUtils.isNotBlank(actionMessage)) {
            addActionMessage(actionMessage);
        }
        if (authorizationService.isInMaintenanceMode()) {
            addActionError(authorizationService.getMaintenanceModeMessage());
        }
        return super.input();
    }

    @Override
    public void validate() {
        if (Strings.isNullOrEmpty(email)) {
            addFieldError("email", "Le champ 'email' est obligatoire");
        }
        if (Strings.isNullOrEmpty(password)) {
            addFieldError("password", "Le champ 'password' est obligatoire");
        }
    }

    @Override
    public String execute() throws Exception {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(String.format("[sid=%s] Trying to login with email '%s'", getSessionId(), email));
        }
        String errorMessage = null;

        AuthenticatedUser loginResult = authenticationService.login(email, password);
        if (loginResult != null) {

            if (authorizationService.isInMaintenanceMode() && !authorizationService.isAdminFromUserId(loginResult.getTopiaId())) {
                errorMessage = authorizationService.getMaintenanceModeMessage();
                loginResult = null;
            }

        } else if (userService.isValidEmail(email) && !userService.isUserActive(email)) {
            errorMessage =  "Votre compte est désactivé, merci de contacter un administrateur d'Agrosyst.";
        } else {
            errorMessage =  "Échec de connexion.";
        }

        if (errorMessage != null) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("[sid=%s] Trying to login with email '%s' error message %s", getSessionId(), email, errorMessage));
            }
            addActionError(errorMessage);
            return INPUT;
        } else {

            Preconditions.checkState(loginResult != null);

            if (Strings.isNullOrEmpty(next)
                    || next.contains(AgrosystWebAuthenticationFilter.AGROSYST_WEB_LOGIN_ACTION)
                    || next.contains(AgrosystWebAuthenticationFilter.AGROSYST_WEB_LOGIN_ACTION_INPUT)) {
                next = initNext();
            }
            if (next.contains(";jsessionid=")) {
                next = next.substring(0, next.indexOf(";jsessionid="));
            }

            LocalDateTime lastBroadcastDate = userService.getLastMessageReadDate(loginResult.getTopiaId())
                    .orElse(DEFAULT_BROADCAST_DATE);
            if (next.contains("?")) {
                next += "&";
            } else {
                next += "?";
            }
            next += "lastBroadcastDate=" + LocalDateTimeHelper.urlSafeFormat(lastBroadcastDate);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format("[sid=%s] user email '%s' redirect to %s", loginResult.getSid(), email, next));
            }
            applicationContext.writeAuthenticationCookie(loginResult, getCookieHelper());
            servletResponse.sendRedirect(next);

            return null;
        }
    }

    public String getEmail() {
        return email;
    }

    public String getNext() {
        return next;
    }

    protected String initNext() {
        return servletRequest.getContextPath() + "/";
    }

}
