package fr.inra.agrosyst.web.listeners;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2015 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystWebApplicationListener implements ServletContextListener {

    private static final Log LOGGER = LogFactory.getLog(AgrosystWebApplicationListener.class);

    protected AgrosystWebApplicationContext context;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Initializing Agrosyst Web Module");
        }

        ServletContext servletContext = sce.getServletContext();
        context = new AgrosystWebApplicationContext(servletContext);

        servletContext.setAttribute(AgrosystWebApplicationContext.APPLICATION_CONTEXT_PARAMETER, context);
        context.initApplicationBanners(servletContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Destroy Agrosyst Web Module");
        }

        context.close();
    }
}
