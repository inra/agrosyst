package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveZoneFilter;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * @author David Cossé
 */
public class EffectiveCropCyclesZonePlotActivation extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -5349685986128542627L;

    protected List<String> zoneIds;

    @Setter
    protected boolean plot;

    @Setter
    protected boolean activate;

    @Getter
    protected transient String filter;

    @Setter
    protected transient EffectiveCropCycleService effectiveCropCycleService;

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "effective-crop-cycles-list", "filter", "${filter}"}),
    })
    public String execute() throws Exception {
        Pair<Integer, Integer> plotZoneUpdatedCount = effectiveCropCycleService.togglePlotOrZoneActiveStatus(
                zoneIds,
                plot,
                activate
        );
        if (plot) {
            notificationSupport.plotActivatedStatus(plotZoneUpdatedCount.getLeft(), activate);
        } else {
            if (plotZoneUpdatedCount.getLeft() > 0) {
                notificationSupport.plotActivatedStatus(plotZoneUpdatedCount.getLeft(), activate);
            }
            notificationSupport.zoneActivatedStatus(plotZoneUpdatedCount.getRight(), activate);
        }

        EffectiveZoneFilter filter_ = new EffectiveZoneFilter();
        if (plot) {
            filter_.setPlotState(activate);
        } else {
           filter_.setZoneState(activate);
        }

        this.filter = getGson().toJson(filter_);

        return SUCCESS;
    }


    public void setZoneIds(String zoneIds) {
        this.zoneIds = getGson().fromJson(zoneIds, List.class);
    }

}
