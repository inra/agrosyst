package fr.inra.agrosyst.web.actions.networks;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkImpl;
import fr.inra.agrosyst.api.entities.NetworkManager;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.services.network.NetworkIndicators;
import fr.inra.agrosyst.api.services.network.NetworkManagerDto;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author cosse
 */
public class NetworksEdit extends AbstractAgrosystAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(NetworksEdit.class);

    @Serial
    private static final long serialVersionUID = -4466666585810184117L;

    protected transient NetworkService networkService;

    protected transient UserService userService;

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    protected String networkTopiaId;

    protected Network network;

    protected String parentIds;

    protected List<NetworkManagerDto> managers;
    protected String managersJson;

    protected NetworkIndicators indicators;

    @Override
    public void prepare() {
        if (StringUtils.isBlank(networkTopiaId)) {// new network
            network = networkService.newNetwork();
        } else {
            network = networkService.getNetwork(networkTopiaId);

            activated = network.isActive();
        }
    }

    @Override
    protected void initForInput() {

        indicators = networkService.getIndicators(networkTopiaId);

    }

    @Override
    @Action("networks-edit-input")
    public String input() {
        initForInput();

        if (!StringUtils.isBlank(networkTopiaId)) {

            readOnly = !authorizationService.isNetworkWritable(networkTopiaId);
            if (readOnly) {
                notificationSupport.networkNotWritable();
            }
        }

        managers = getNetworkManagersDtos(network);

        return INPUT;
    }

    protected List<NetworkManagerDto> getNetworkManagersDtos(Network network) {
        List<NetworkManagerDto> managers = new ArrayList<>();

        for (NetworkManager networkManager : network.getManagers()) {
            String userTopiaId = networkManager.getAgrosystUser().getTopiaId();
            UserDto userDto = userService.getUser(userTopiaId);
            NetworkManagerDto result = new NetworkManagerDto(
                    networkManager.getTopiaId(),
                    networkManager.getFromDate(),
                    networkManager.getToDate(),
                    networkManager.isActive(),
                    userDto);
            managers.add(result);
        }

        return managers;
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(network.getName())) {
            addFieldError("name", "Le nom du réseau est obligatoire.");

        } else {
            Set<String> homonymeNetworks = networkService.findNetworksByName(network.getName(), networkTopiaId);
            if (homonymeNetworks != null && !homonymeNetworks.isEmpty()) {
                if (!homonymeNetworks.contains(networkTopiaId)) {
                    addFieldError("name", "Le nom du réseau est déjà utilisé.");
                }
            }
        }

        if (network.isPersisted() && !network.isActive()) {
             addActionError("Vous ne pouvez enregistrer le réseau car il est inactif. Veuillez le réactiver à partir de la liste des réseaux.");
        }

        try {
            managers = convertNetworkManagerDtoJson(managersJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(ex);
            }
            addActionError("Exception:" + ex + "\n/!\\ Désolé les données saisies depuis votre dernier enregistrement concernant les responsables de réseau n'ont pu être récupérées !");
            managers = getNetworkManagersDtos(network);
        }

        if (managers.isEmpty()) {
            addFieldError("user", "Un réseau doit avoir au moins un responsable.");
        }

        try {
            if (researchParentsCycleProcess()) {
                addFieldError("parent", "Cycle détecté, un réseau ne peut avoir lui-même comme parent ou un réseau parent ayant ce réseau comme parent.");
            }
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e);
            }
            addFieldError("parent", "Cycle détecté, un réseau ne peut avoir lui-même comme parent ou un réseau parent ayant ce réseau comme parent.");
        }

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : network:'%s' -> %s", networkTopiaId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : network:'%s' -> %s", networkTopiaId, getFieldErrors().toString()));
            }
            initForInput();
        }
    }

    protected Boolean researchParentsCycleProcess() {
        Boolean result = false;
        List<String> parentIds = getParentTopiaIds();
        for (String parentId : parentIds) {
            Network parent = networkService.getNetwork(parentId);
            result = researchCycle(parent);
        }
        return result;
    }

    protected Boolean researchCycle(Network parent) {
        Boolean result = false;

        if (networkTopiaId.equals(parent.getTopiaId())) {
            result = true;
        } else {
            for (Network grandParent : parent.getParents()) {
                result = researchCycle(grandParent);
            }
        }
        return result;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "networks-edit-input", "networkTopiaId", "${network.topiaId}"})})
    public String execute() throws Exception {
        try {
            network = networkService.createOrUpdateNetwork(network, managers, getParentTopiaIds());
            notificationSupport.networkSaved(network);

            if (Strings.isNullOrEmpty(networkTopiaId)) {
                navigationContextEntityCreated(network);
            }
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to save network '%s', '%s', '%s':", network, managers, parentIds), e);
            }
            return ERROR;
        }

        return SUCCESS;
    }

    public String getNetworkTopiaId() {
        return networkTopiaId;
    }

    public void setNetworkTopiaId(String networkTopiaId) {
        this.networkTopiaId = networkTopiaId;
    }

    public List<String> getParentIds() {
        List<String> result = null;
        try {
            result = getParentTopiaIds();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to deserialize parentIds '%s'", this.parentIds), e);
            }
        }
        return result;
    }

    public void setParentIdsJson(String json) {
        this.parentIds = json;
    }

    protected List<String> getParentTopiaIds() {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> result = getGson().fromJson(this.parentIds, type);
        return result;
    }

    public Network getNetwork() {
        // AThimel 26/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(network, NetworkImpl::new);
    }

    public List<NetworkManagerDto> convertNetworkManagerDtoJson(String json) {
        Type type = new TypeToken<List<NetworkManagerDto>>() {
        }.getType();
        List<NetworkManagerDto>managers = getGson().fromJson(json, type);
        return managers;
    }

    public void setNetwork(Network network) {
        this.network = network;
    }

    public List<NetworkManagerDto> getManagers() {
        return managers;
    }

    public void setNetworkManagerDtoJson(String json) {
        managersJson = json;
    }

    public NetworkIndicators getIndicators() {
        return indicators;
    }

    public Map<TypeDEPHY, String> getTypesDephy() {
        return getEnumAsMap(TypeDEPHY.DEPHY_FERME, TypeDEPHY.DEPHY_EXPE, TypeDEPHY.NOT_DEPHY);
    }

}
