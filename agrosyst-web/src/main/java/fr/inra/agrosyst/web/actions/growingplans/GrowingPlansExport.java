package fr.inra.agrosyst.web.actions.growingplans;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;

import java.io.Serial;
import java.util.List;

/**
 * @author David Cossé
 */
public class GrowingPlansExport extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = 6124361181629908425L;

    protected transient GrowingPlanService growingPlanService;

    protected List<String> growingPlanIds;

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setGrowingPlanIds(String growingPlanIds) {
        this.growingPlanIds = getGson().fromJson(growingPlanIds, List.class);
    }

    @Override
    protected ExportResult computeExportResult() {
        ExportResult result = growingPlanService.exportGrowingPlanAsXls(growingPlanIds);
        return result;
    }
}
