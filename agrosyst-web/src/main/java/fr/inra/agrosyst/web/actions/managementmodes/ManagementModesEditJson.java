package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Management mode edit json actions.
 * 
 * @author Eric Chatellier
 */
public class ManagementModesEditJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(ManagementModesEditJson.class);

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 3260095596557765111L;

    protected transient GrowingSystemService growingSystemService;

    protected transient ManagementModeService managementModeService;

    protected transient ReferentialService referentialService;

    /** Growing system id (for decision-rules-edit-cropping-plan-entries-json). */
    protected String growingSystemTopiaId;

    /** Type de bio agresseur (for management-edit-bio-agressors-json). */
    protected BioAgressorType bioAgressorType;

    protected ManagementModeCategory category;

    protected Sector sector;

    protected SectionType sectionType;

    protected StrategyType strategyType;

    protected String term;

    protected String growingSystemIds;
    
    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setBioAgressorType(BioAgressorType bioAgressorType) {
        this.bioAgressorType = bioAgressorType;
    }

    /**
     * Récupération de la liste des cultures et des règles à la sélection d'un système de culture.
     * 
     * @return SUCCESS
     */
    @Action("management-modes-edit-growing-system-data-json")
    public String listCroppingPlanEntries() {
        try {
            GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemTopiaId);
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            String domainTopiaId = growingPlan.getDomain().getTopiaId();

            List<CroppingPlanEntry> croppingPlanEntries = managementModeService.getGrowingSystemCroppingPlanEntries(growingSystemTopiaId);
            Collection<DecisionRule> decisionRules = managementModeService.getGrowingSystemDecisionRules(growingSystemTopiaId);
            List<ManagementModeCategory> availableManagementModeCategories = managementModeService.getAvailableManagementModeCategories(growingSystemTopiaId);

            Map<String, Object> result = new HashMap<>();
            result.put("croppingPlanEntries", croppingPlanEntries);
            result.put("decisionRules", decisionRules);
            result.put("managementModeCategories", availableManagementModeCategories);
            result.put("domainTopiaId", domainTopiaId);
            result.put("typeDEPHY", growingPlan.getType());
            jsonData = result;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("management-modes-edit-growing-system-data-json failed for growingSystemId '" + growingSystemTopiaId + "' ", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("available-growing-system-for-duplication-json")
    public String availableGrowingSystemsForDuplication() {
        try {
            NavigationContext navigationContext = getNavigationContext();
            jsonData = managementModeService.getAvailableGsForDuplication(growingSystemTopiaId, navigationContext);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("available-growing-system-for-duplication-json failed for growingSystemTopiaId '" + growingSystemTopiaId + "' ", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("management-modes-edit-load-ref-strategy-levers-json")
    public String loadRefStrategyLevers() {
        try {
            jsonData = managementModeService.loadRefStrategyLevers(sector, growingSystemTopiaId, sectionType, strategyType);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("management-modes-edit-load-ref-strategy-levers-json failed for sector '%s', growingSystemId '%s', sectionType '%s', strategyType '%s"
                        , sector, growingSystemTopiaId, sectionType, strategyType), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("management-modes-edit-load-ref-strategy-levers-for-term-json")
    public String loadRefStrategyLeversForTerm() {
        try {
            jsonData = managementModeService.loadRefStrategyLeversForTerm(sector, growingSystemTopiaId, sectionType, strategyType, term);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("management-modes-edit-load-ref-strategy-levers-json failed for sector '%s', growingSystemId '%s', sectionType '%s', strategyType '%s"
                        , sector, growingSystemTopiaId, sectionType, strategyType), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Action("management-modes-edit-load-writable-management-modes-For-growing-system-ids-json")
    public String loadWritableManagementModesForGrowingSystemIds() {
        try {
            List<String> growingSystemIds0 = Lists.newArrayList(getGrowingSystemIds(growingSystemIds));
            jsonData = managementModeService.loadWritableManagementModesForGrowingSystemIds(growingSystemIds0);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("management-modes-edit-load-writable-management-modes-For-growing-system-ids-json failed for growingSystemIds '%s'", growingSystemIds), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public void setGrowingSystemTopiaId(String growingSystemTopiaId) {
        this.growingSystemTopiaId = growingSystemTopiaId;
    }

    public void setCategory(ManagementModeCategory category) {
        this.category = category;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public void setSectionType(SectionType sectionType) {
        this.sectionType = sectionType;
    }

    public void setStrategyType(StrategyType strategyType) {
        this.strategyType = strategyType;
    }

    public void setGrowingSystemIds(String growingSystemIds) {
        this.growingSystemIds = growingSystemIds;
    }

    private Set<String> getGrowingSystemIds(String growingSystemIds) {
        return getSelectedGrowingSystemIds(growingSystemIds);
    }

    public void setTerm(String term) {
        this.term = term;
    }
}
