package fr.inra.agrosyst.web.actions.referential;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.InputStream;
import java.io.Serial;

@Setter
public class SpeciesReferentialDownload extends AbstractExportAction {

    private static final Log LOGGER = LogFactory.getLog(SpeciesReferentialDownload.class);
    @Serial
    private static final long serialVersionUID = 845469068343294643L;
    
    protected transient ReferentialService referentialService;

    protected InputStream inputStream;


    @Override
    protected ExportResult computeExportResult() {
        ExportResult inputStream;
        try {
            inputStream = referentialService.exportAllSpeciesXslx();

        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't generate csv", ex);
            }
            throw new AgrosystTechnicalException("Can't create input stream", ex);
        }
        return inputStream;
    }
}
