package fr.inra.agrosyst.web.actions.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.security.UserRoleDto;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.nuiton.util.StringUtil;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UsersEdit extends AbstractAdminAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(UsersEdit.class);
    @Serial
    private static final long serialVersionUID = 259407617651911081L;

    protected transient UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    protected String userTopiaId;

    protected UserDto user;
    protected boolean newUser;

    protected List<UserRoleDto> userRoles;

    protected String password;
    protected String confirmPassword;

    public void setUserTopiaId(String userTopiaId) {
        this.userTopiaId = userTopiaId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public void setUserRolesJson(String json) {
        Type type = new TypeToken<List<UserRoleDto>>() {
        }.getType();
        this.userRoles = getGson().fromJson(json, type);
    }

    @Override
    public void prepare() throws Exception {
        super.prepare();
        if (Strings.isNullOrEmpty(userTopiaId)) {
            user = new UserDto();
            user.setActive(true);
            userRoles = new ArrayList<>();
            newUser = true;
        } else {
            user = userService.getUser(userTopiaId);
            userRoles = authorizationService.getUserRoles(userTopiaId);
            newUser = false;
        }
    }

    @Override
    @Action("users-edit-input")
    public String input() throws Exception {
        return INPUT;
    }

    @Override
    public void validate() {
        
        UserDto user = getUser();
        String email = user.getEmail();
        if (Strings.isNullOrEmpty(email)) {
            addFieldError("user.email", "Le champ 'Email' est obligatoire");
        } else if (!StringUtil.isEmail(email)){
            addFieldError("user.email", "L'adresse email n'est pas valide");
        }

        if (userService.isEmailInUse(email, userTopiaId)) {
            addFieldError("user.email", "Cette adresse email est déjà utilisée");
        }
        if (newUser) {
            if (Strings.isNullOrEmpty(password)) {
                addFieldError("password", "Le champ 'password' est obligatoire");
            }
            if (Strings.isNullOrEmpty(confirmPassword)) {
                addFieldError("confirmPassword", "Le champ 'confirmPassword' est obligatoire");
            }
        }
        if (!Objects.equal(password, confirmPassword)) {
            addFieldError("confirmPassword", "Les champs 'password' et 'confirmPassword' ne correspondent pas");
        }
        if (Strings.isNullOrEmpty((user.getFirstName()))) {
            addFieldError("user.firstName", "Le champ 'Prénom' est obligatoire");
        }
        if (Strings.isNullOrEmpty((user.getLastName()))) {
            addFieldError("user.lastName", "Le champ 'Nom' est obligatoire");
        }

        // list of rules without empty line
        if (userRoles != null) {
            List<UserRoleDto> validateRules = Lists.newArrayListWithCapacity(userRoles.size());
            for (UserRoleDto userRoleDto : userRoles) {
                if (userRoleDto.getType() != null) {
                    validateRules.add(userRoleDto);
                    if (userRoleDto.getType().equals(RoleType.DOMAIN_RESPONSIBLE) ||
                            userRoleDto.getType().equals(RoleType.GROWING_PLAN_RESPONSIBLE) ||
                            userRoleDto.getType().equals(RoleType.GS_DATA_PROCESSOR) ||
                            userRoleDto.getType().equals(RoleType.NETWORK_RESPONSIBLE) ||
                            userRoleDto.getType().equals(RoleType.NETWORK_SUPERVISOR)) {
                        if (userRoleDto.getEntity() == null || Strings.isNullOrEmpty(userRoleDto.getEntity().getIdentifier())) {
                            addActionError("Les profils ne sont pas complets");
                        }
                    }
                }
            }
            if (!hasErrors()) {
                if (hasErrors()) {
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(String.format("validate, action %s -> %s", UsersEdit.class.getSimpleName(), getActionErrors().toString()));
                        LOGGER.error(String.format("validate, fields errors : '%s' -> %s", UsersEdit.class.getSimpleName(), getFieldErrors().toString()));
                    }

                    initForInput();
                }

                userRoles = validateRules;
            }
        }
        
        if (StringUtils.isNotBlank(user.getItEmail())){
            if (!userService.isValidEmail(user.getItEmail())) {
                addActionError("L'adresse email n'est pas reconnue");
            }
        }


    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {
            "namespace", "/security", "actionName", "users-edit-input", "userTopiaId", "${user.topiaId}"})})
    public String execute() throws Exception {
        checkIsAdmin();

        if (Strings.isNullOrEmpty(userTopiaId)) {
            user = userService.createUser(user, password);
        } else {
            user = userService.updateUser(user, password);
        }
        authorizationService.saveUserRoles(user.getTopiaId(), userRoles);

        notificationSupport.userSaved(user);

        refreshAuthenticatedUser();

        return SUCCESS;
    }

    public UserDto getUser() {
        if (user == null) {
            return new UserDto();
        }
        return user;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public Map<Boolean, String> getTrueFalse() {
        Map<Boolean, String> result = ImmutableMap.of(Boolean.TRUE, "Oui", Boolean.FALSE, "Non");
        return result;
    }

    public List<UserRoleDto> getUserRoles() {
        return userRoles;
    }

    public Map<RoleType, String> getRoleTypes() {
        return getEnumAsMap(RoleType.values());
    }

    public List<Language> getLanguages() {
        return Arrays.asList(Language.values());
    }
}
