package fr.inra.agrosyst.web.actions.growingplans;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;
import java.util.List;

/**
 * Classe qui permet de déclencher un export asynchrone des dispositifs.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class GrowingPlansExportAsync extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -6274263619966528606L;

    protected transient GrowingPlanService growingPlanService;

    protected List<String> growingPlanIds;

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setGrowingPlanIds(String growingPlanIds) {
        this.growingPlanIds = getGson().fromJson(growingPlanIds, List.class);
    }

    @Override
    public String execute() {
        growingPlanService.exportGrowingPlanAsXlsAsync(growingPlanIds);
        return SUCCESS;
    }

}
