package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.TrackedEvent;
import fr.inra.agrosyst.api.entities.security.TrackedEventType;
import fr.inra.agrosyst.api.services.security.TrackedEventFilter;
import fr.inra.agrosyst.api.services.security.TrackerService;
import fr.inra.agrosyst.api.services.security.UserRoleDto;
import fr.inra.agrosyst.api.services.users.Users;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class TrackedEventsList extends AbstractAdminAction {

    @Serial
    private static final long serialVersionUID = 6903878606044840600L;
    
    protected final Map<String, Set<RoleType>> rolesCache = new HashMap<>();

    protected transient TrackerService trackerService;

    public void setTrackerService(TrackerService trackerService) {
        this.trackerService = trackerService;
    }

    protected PaginationResult<TrackedEventDto> events;

    protected TrackedEventFilter filter;

    protected Function<TrackedEvent, @Nullable TrackedEventDto> getEventToDtoFunction() {
        return input -> {
            TrackedEventDto result = new TrackedEventDto();
            result.setType(input.getType());
            String logKey = TrackedEvent.class.getName() + "." + input.getType();
            String logFormat = getText(logKey);
            Type type = new TypeToken<List<String>>() {
            }.getType();


            List<String> argsList = getGson().fromJson(input.getArgs(), type);
            if (TrackedEventType.USER_ACTIVATION.equals(input.getType())) {
                String action = "true".equals(argsList.get(1)) ? "Activation" : "Désactivation";
                argsList = Lists.newArrayList(action, argsList.get(0));
            } else if (TrackedEventType.USER_MOD.equals(input.getType()) || TrackedEventType.USER_MOD_SELF.equals(input.getType())) {
                String action = "true".equals(argsList.get(1)) ? "avec" : "sans";
                argsList = Lists.newArrayList(argsList.get(0), action);
            } else if (TrackedEventType.USER_ROLE_ADD.equals(input.getType()) || TrackedEventType.USER_ROLE_REMOVE.equals(input.getType())) {
                RoleType roleType = RoleType.valueOf(argsList.get(0));
                String role = getText(RoleType.class.getName() + "." + roleType.name());
                argsList.set(0, role);
                if (RoleType.DOMAIN_RESPONSIBLE.equals(roleType)) {
                    logFormat = getText(logKey + ".long");
                    argsList.add(1, "domaine");
                } else if (RoleType.GROWING_PLAN_RESPONSIBLE.equals(roleType)) {
                    logFormat = getText(logKey + ".long");
                    argsList.add(1, "dispositif");
                } else if (RoleType.GS_DATA_PROCESSOR.equals(roleType)) {
                    logFormat = getText(logKey + ".long");
                    argsList.add(1, "système de culture");
                } else if (RoleType.NETWORK_RESPONSIBLE.equals(roleType) || RoleType.NETWORK_SUPERVISOR.equals(roleType)) {
                    logFormat = getText(logKey + ".medium");
                    argsList.add(1, "réseau");
                }
            }
            String log = String.format(logFormat, argsList.toArray());
            result.setDate(input.getDate());
            result.setLog(log);
            result.setAuthor(Users.TO_USER_DTO.apply(input.getAuthor()));
            if (input.getAuthor() != null && result.getAuthor() != null) {
                Set<RoleType> roleTypes = getUserRoles(input.getAuthor().getTopiaId());
                result.getAuthor().setRoles(ImmutableSet.copyOf(roleTypes));
            }
            return result;
        };
    }

    protected Set<RoleType> getUserRoles(String userId) {
        if (!rolesCache.containsKey(userId)) {
            List<UserRoleDto> userRoles = authorizationService.getUserRoles(userId);
            Set<RoleType> roles = userRoles.stream()
                    .map(UserRoleDto::getType)
                    .collect(Collectors.toSet());
            rolesCache.put(userId, roles);
        }
        return rolesCache.get(userId);
    }

    @Override
    public String execute() throws Exception {
        checkIsAdmin();

        final TrackedEventFilter filter = new TrackedEventFilter();
        filter.setPageSize(getListNbElementByPage(TrackedEvent.class));
        PaginationResult<TrackedEvent> trackedEvents = trackerService.list(filter);

        List<TrackedEventDto> dtos = trackedEvents.getElements().stream()
                .map(getEventToDtoFunction())
                .collect(Collectors.toList());

        events = PaginationResult.of(dtos, trackedEvents.getCount(), trackedEvents.getCurrentPage());
        return SUCCESS;
    }

    public PaginationResult<TrackedEventDto> getEvents() {
        return events;
    }


    public Map<TrackedEventType, String> getEventTypes() {
        return getEnumAsMap(TrackedEventType.values());
    }

    public Map<RoleType, String> getRoleTypes() {
        return getEnumAsMap(RoleType.values());
    }

    public void setFilter(TrackedEventFilter filter) {
        this.filter = filter;
    }
}
