package fr.inra.agrosyst.web.rest;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.AgrosystTopiaApplicationContext;
import fr.inra.agrosyst.api.entities.AgrosystTopiaPersistenceContext;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

public class RestTransactionFilter implements Filter {

    private static final Log LOGGER = LogFactory.getLog(RestTransactionFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        AgrosystWebApplicationContext webApplicationContext = getAgrosystApplicationContext(request.getServletContext());
        Optional<AuthenticatedUser> authenticatedUser = getAuthenticatedUser(request);
        ServiceContext serviceContext = newServiceContext(webApplicationContext, authenticatedUser);
        request.setAttribute(ServiceContext.SERVICE_CONTEXT_PARAMETER, serviceContext);
        try {
            chain.doFilter(request, response);
        } catch (Exception eee) {
            // rollback transaction only when an exception happen
            // sinon, le serveur devient inutilisable
            rollbackTransaction(serviceContext);

            // Make sure exception is logged because of some strange logging behavior
            if (LOGGER.isErrorEnabled()) {
                HttpServletRequest httpServletRequest = (HttpServletRequest) request;
                String userEmail = authenticatedUser
                        .map(AuthenticatedUser::getEmail)
                        .orElse("?");
                String message = String.format("Exception occured for user '%s' during Rest endpoint invocation (path is '%s' and method is '%s'): %s",
                        userEmail, httpServletRequest.getContextPath(), httpServletRequest.getMethod(), eee.getMessage());
                LOGGER.error(message, eee);
            }
            throw eee;

        } finally {
            checkClosedPersistenceContext(serviceContext);
        }
    }

    protected AgrosystWebApplicationContext getAgrosystApplicationContext(ServletContext servletContext) {
        AgrosystWebApplicationContext applicationContext = (AgrosystWebApplicationContext) servletContext
                .getAttribute(AgrosystWebApplicationContext.APPLICATION_CONTEXT_PARAMETER);
        Preconditions.checkState(applicationContext != null, AgrosystWebApplicationContext.MISSING_APPLICATION_CONTEXT);
        return applicationContext;
    }

    protected Optional<AuthenticatedUser> getAuthenticatedUser(ServletRequest request) {
        AuthenticatedUser userDto = (AuthenticatedUser) request.getAttribute(AgrosystWebAuthenticationFilter.AUTHENTICATED_USER);
        Optional<AuthenticatedUser> result = Optional.ofNullable(userDto);
        return result;
    }

    protected ServiceContext newServiceContext(AgrosystWebApplicationContext webApplicationContext,
                                               Optional<AuthenticatedUser> authenticatedUser) {

        ServiceContext result = null;

        final AgrosystTopiaApplicationContext topiaApplicationContext = webApplicationContext.getApplicationContext();
        Preconditions.checkState(topiaApplicationContext != null);

        try {
            result = authenticatedUser.isPresent()
                    ? topiaApplicationContext.newServiceContext(authenticatedUser.get())
                    : topiaApplicationContext.newServiceContext();
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Unable to create local serviceContext", eee);
            }
        }

        Preconditions.checkState(result != null, "ServiceContext not instantiated, check configuration");
        return result;
    }

    protected void rollbackTransaction(ServiceContext serviceContext) {
        try {
            AgrosystTopiaPersistenceContext persistenceContext = serviceContext.getPersistenceContext(false);
            if (persistenceContext != null) {
                persistenceContext.rollback();
            }
        } catch (Exception eee) {
            if (LOGGER.isFatalEnabled()) {
                LOGGER.fatal("Unable to rollback persistence context !", eee);
            }
            throw new AgrosystTechnicalException("Unable to rollback persistence context !", eee);
        }
    }

    protected void checkClosedPersistenceContext(ServiceContext serviceContext) {
        try {
            serviceContext.close();
        } catch (Exception eee) {
            if (LOGGER.isFatalEnabled()) {
                LOGGER.fatal("Unable to close persistence context !", eee);
            }
            throw new AgrosystTechnicalException("Unable to close persistence context !", eee);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
