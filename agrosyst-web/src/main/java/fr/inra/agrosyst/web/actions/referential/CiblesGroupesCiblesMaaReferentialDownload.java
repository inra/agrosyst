package fr.inra.agrosyst.web.actions.referential;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CiblesGroupesCiblesMaaReferentialDownload extends AbstractAgrosystAction {

    private static final Log LOGGER = LogFactory.getLog(CiblesGroupesCiblesMaaReferentialDownload.class);
    @Serial
    private static final long serialVersionUID = 2516653646753715542L;
    
    protected transient ReferentialService referentialService;

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    @Override
    @Action(results= {@Result(type="stream", params={"contentType", "text/csv",
            "inputName", "inputStream", "contentDisposition", "attachment; filename=\"${filename}\"; filename*=UTF-8''${urifilename}"})})
    public String execute() throws Exception {
        return SUCCESS;
    }

    public InputStream getInputStream() {
        InputStream inputStream;
        try {
            inputStream = referentialService.exportCiblesGroupesCiblesMaaCSV();
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't generate csv", ex);
            }
            throw new AgrosystTechnicalException("Can't create input stream", ex);
        }
        return inputStream;
    }

    /**
     * For browser as Chrome that need URI encoded file name
     * @return
     * @throws UnsupportedEncodingException
     */
    public String getUrifilename() throws UnsupportedEncodingException {
        return URLEncoder.encode(getFilename(), StandardCharsets.UTF_8).replace("+", "%20");
    }

    public String getFilename() {
        String filename = "Cibles référencés dans Agrosyst au ";
        // date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedString = LocalDate.now().format(formatter);
        filename += formattedString + ".csv";
        return filename;
    }
}
