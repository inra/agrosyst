package fr.inra.agrosyst.web.actions.growingsystems;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;

import java.io.Serial;
import java.util.List;

/**
 * @author David Cossé
 */
public class GrowingSystemsExport extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = 2914458747472187431L;

    protected transient GrowingSystemService growingSystemService;

    protected List<String> growingSystemIds;

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setGrowingSystemIds(String growingSystemIds) {
        this.growingSystemIds = getGson().fromJson(growingSystemIds, List.class);
    }

    @Override
    protected ExportResult computeExportResult() {
        ExportResult result = growingSystemService.exportGrowingSystemsAsXls(growingSystemIds);
        return result;
    }
}
