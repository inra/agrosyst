package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.entities.security.TrackedEventType;
import fr.inra.agrosyst.api.services.users.UserDto;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class TrackedEventDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 5831243047660850086L;

    protected LocalDateTime date;
    protected TrackedEventType type;
    protected UserDto author;
    protected String log;
    protected HashSet<RoleType> authorRoles;

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public TrackedEventType getType() {
        return type;
    }

    public void setType(TrackedEventType type) {
        this.type = type;
    }

    public UserDto getAuthor() {
        return author;
    }

    public void setAuthor(UserDto author) {
        this.author = author;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public void setAuthorRoles(Set<RoleType> authorRoles) {
        this.authorRoles = new HashSet<>(authorRoles);
    }

    public HashSet<RoleType> getAuthorRoles() {
        return authorRoles;
    }
}
