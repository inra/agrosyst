package fr.inra.agrosyst.web.actions.performances.api;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.performance.PerformanceDto;
import fr.inra.agrosyst.api.services.performance.PerformanceFilter;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class PerformancesListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(PerformancesListJson.class);

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -1180723133330637375L;

    protected transient PerformanceService performanceService;

    protected transient String filter;

    protected transient boolean practiced;

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setPerformanceService(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }

    public void setPracticed(boolean practiced) {
        this.practiced = practiced;
    }

    @Override
    public String execute() {
        try {
            PerformanceFilter performanceFilter = getGson().fromJson(filter, PerformanceFilter.class);
            performanceFilter.setPracticed(practiced);
            writeListNbElementByPage(PerformanceDto.class, String.valueOf(practiced), performanceFilter.getPageSize());
            performanceFilter.setActive(Boolean.TRUE);
            jsonData = performanceService.getFilteredPerformances(performanceFilter, 10);

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load performances", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
