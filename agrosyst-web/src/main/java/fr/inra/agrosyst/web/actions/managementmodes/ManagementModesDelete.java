package fr.inra.agrosyst.web.actions.managementmodes;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by davidcosse on 06/02/17.
 */
public class ManagementModesDelete  extends AbstractAgrosystAction {
    
    @Serial
    private static final long serialVersionUID = 5481498707818072304L;
    protected List<String> selectedManagementModesToDelete;

    protected transient ManagementModeService managementModeService;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setSelectedManagementModesToDelete(String selectedManagementModesToDelete) {
        Type type = new TypeToken<List<String>>() {}.getType();
        this.selectedManagementModesToDelete = getGson().fromJson(selectedManagementModesToDelete, type);
    }

    @Override
    @Action(results = { @Result(type = "redirectAction", params = {
            "actionName", "management-modes-list" }) })
    public String execute() throws Exception {
        if (CollectionUtils.isNotEmpty(selectedManagementModesToDelete)) {
            Set<String> managementModeIdsSet = new HashSet<>(selectedManagementModesToDelete);
            managementModeService.removeManagementModes(managementModeIdsSet);
            notificationSupport.managementModeDeleted(managementModeIdsSet.size());
        }
        return SUCCESS;
    }
}
