package fr.inra.agrosyst.web.rest;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import fr.inra.agrosyst.web.AgrosystWebConfig;
import fr.inra.agrosyst.web.actions.security.JwtHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Optional;

import static fr.inra.agrosyst.web.AgrosystWebApplicationContext.TOKEN_COOKIE_NAME;

/**
 * This filter is used to secure JAX-RS REST endpoints.
 * To use it, just add @Secured annotation to class Resource or method of class Resource.
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AgrosystRestAuthenticationFilter implements ContainerRequestFilter {

    private static final Log LOGGER = LogFactory.getLog(AgrosystRestAuthenticationFilter.class);

    @Inject
    private ServletContext servletContext;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Cookie cookie = requestContext.getCookies().get(TOKEN_COOKIE_NAME);
        Optional<String> jwtToken = Optional.ofNullable(cookie).map(Cookie::getValue);

        Optional<AuthenticatedUser> authenticatedUser = validateToken(jwtToken);

        if (authenticatedUser.isEmpty()) {
            abortWithUnauthorized(requestContext);
        }
    }

    private Optional<AuthenticatedUser> validateToken(Optional<String> jwtToken) {
        // Check if the token was issued by the server and if it's not expired
        // Return an empty user if the token is invalid
        AgrosystWebApplicationContext webApplicationContext = (AgrosystWebApplicationContext) servletContext
                .getAttribute(AgrosystWebApplicationContext.APPLICATION_CONTEXT_PARAMETER);
        AgrosystWebConfig webConfig = webApplicationContext.getWebConfig();
        JwtHelper jwtHelper = new JwtHelper(webConfig);
        try {
            return jwtToken.map(jwtHelper::verifyJwtToken);
        } catch (TokenExpiredException tee) {
            LOGGER.warn("Token expiré : " + tee.getMessage());
        } catch (SignatureVerificationException sve) {
            LOGGER.warn("Signature du token invalide : " + sve.getMessage());
        } catch (JWTDecodeException jde) {
            LOGGER.warn("Token invalide : " + jde.getMessage());
        }
        return Optional.empty();
    }

    private void abortWithUnauthorized(ContainerRequestContext requestContext) {
        // Abort the filter chain with a 401 status code response
        // The WWW-Authenticate header is sent along with the response
        requestContext.abortWith(
                Response.status(Response.Status.UNAUTHORIZED)
                        .build()
        );
    }
}
