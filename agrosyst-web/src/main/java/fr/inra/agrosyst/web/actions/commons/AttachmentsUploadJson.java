package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AttachmentMetadata;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.action.UploadedFilesAware;
import org.apache.struts2.dispatcher.multipart.UploadedFile;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Setter
public class AttachmentsUploadJson extends AbstractJsonAction implements UploadedFilesAware {

    private static final Log LOGGER = LogFactory.getLog(AttachmentsUploadJson.class);

    protected transient String objectReferenceId;
    protected transient List<File> files;
    protected transient List<String> filesContentType;
    protected transient List<String> filesFileName;

    @Override
    public String execute() throws Exception {
        try {
            List<AttachmentMetadata> attachmentMetadatas = new ArrayList<>();
            for (int i = 0; i < CollectionUtils.emptyIfNull(files).size(); i++) {
                File file = files.get(i);
                String fileFileName = filesFileName.get(i);
                String fileContentType = filesContentType.get(i);

                AttachmentMetadata attachmentMetadata = attachmentService.addAttachment(
                        objectReferenceId,
                        new FileInputStream(file),
                        fileFileName,
                        fileContentType);

                attachmentMetadatas.add(attachmentMetadata);
            }
            jsonData = attachmentMetadatas;
        } catch (AgrosystAccessDeniedException e) {
            jsonData = "Vous n'avez pas les autorisations nécessaires pour effectuer cette action.";
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load attachment metadata", e);
            }
            return ERROR;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load attachment metadata", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    @Override
    public void withUploadedFiles(List<UploadedFile> uploadedFiles) {
        if (LOGGER.isDebugEnabled()) {
            Collection<UploadedFile> uploadedFiles1 = CollectionUtils.emptyIfNull(uploadedFiles);
            LOGGER.debug(String.format("L'utilisateur %s à envoyé les pièces jointes suivante %s", getAuthenticatedUser().getEmail(), uploadedFiles1.stream().map(UploadedFile::getOriginalName).collect(Collectors.joining())));
        }
    }
}
