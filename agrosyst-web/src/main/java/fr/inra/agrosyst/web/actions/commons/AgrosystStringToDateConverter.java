package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import org.apache.struts2.util.StrutsTypeConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Map;

/**
 * @author David Cossé
 */
public class AgrosystStringToDateConverter extends StrutsTypeConverter {

    protected final DateTimeFormatter simpleDateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Override
    public Object convertFromString(Map context, String[] values, Class toClass) {
        //Parse String to get a date object
        LocalDate result = null;
        if (values != null && values.length == 1) {
            String stDate = values[0];
            if(!Strings.isNullOrEmpty(stDate)) {
                result = LocalDate.parse(values[0], simpleDateFormat);
            }
        }

        return result;
    }

    @Override
    public String convertToString(Map context, Object o) {
        // Get the string from object o
        return o == null ? null : simpleDateFormat.format((TemporalAccessor)o);
    }
}
