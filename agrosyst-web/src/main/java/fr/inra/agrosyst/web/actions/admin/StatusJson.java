package fr.inra.agrosyst.web.actions.admin;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.async.AsyncService;
import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serial;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

public class StatusJson extends AbstractJsonAction {

    protected static final List<String> READABLE_SIZE_UNITS = Arrays.asList("B", "KB", "MB", "GB", "TB", "PB");
    protected static final double ONE_BYTE_AS_DOUBLE = 1024d;
    @Serial
    private static final long serialVersionUID = -5216908457477602159L;
    
    protected AsyncService asyncService;

    protected boolean includeThreadDetails = false;

    public void setAsyncService(AsyncService asyncService) {
        this.asyncService = asyncService;
    }

    public void setIncludeThreadDetails(boolean includeThreadDetails) {
        this.includeThreadDetails = includeThreadDetails;
    }

    protected String asReadableSize(Long bytes) {
        Iterator<String> iterator = READABLE_SIZE_UNITS.iterator();
        double bytesAsDouble = bytes.doubleValue();
        String unit = iterator.next();
        while (bytesAsDouble > ONE_BYTE_AS_DOUBLE) {
            bytesAsDouble /= ONE_BYTE_AS_DOUBLE;
            unit = iterator.next();
        }
        String result = String.format("%.2f%s", bytesAsDouble, unit);
        return result;
    }

    protected String formatDuration(LocalDateTime from, LocalDateTime to) {
        long s = ChronoUnit.SECONDS.between(from, to);
        long m = ChronoUnit.MINUTES.between(from, to);
        long h = ChronoUnit.HOURS.between(from, to);
        long d = ChronoUnit.DAYS.between(from, to);
        String result = String.format("%ds", (s % 60));
        if (m > 0) {
            result = String.format("%dm", (m % 60)) + result;
        }
        if (h > 0) {
            result = String.format("%dh", (h % 24)) + result;
        }
        if (d > 0) {
            result = String.format("%dd", d) + result;
        }
        return result;
    }

    protected void appendMemoryValues(StatusDto builder) {
        // Mémoire : Données brutes
        Runtime runtime = Runtime.getRuntime();
        long freeMemoryOnAllocated = runtime.freeMemory();            // Mémoire libre (par rapport à la mémoire allouée)
        long totalMemory = runtime.totalMemory();                     // Mémoire allouée
        long maxMemory = runtime.maxMemory();                         // Mémoire totale (max)

        // Mémoire : Données déduites
        long usedMemory = totalMemory - freeMemoryOnAllocated;        // Mémoire utilisée (allouée - libre)
        double usedPercent = ((double) usedMemory / maxMemory) * 100d;// Mémoire utilisée en pourcentage du max
        long freeMemory = maxMemory - usedMemory;                     // Mémoire libre (par rapport au max)
        double freePercent = 100d - usedPercent;                      // Mémoire libre en pourcentage du max

        builder.setMemoryAllocated(asReadableSize(totalMemory));
        builder.setMemoryUsed(String.format("%s (%.2f%s)", asReadableSize(usedMemory), usedPercent, "%"));
        builder.setMemoryFree(String.format("%s (%.2f%s)", asReadableSize(freeMemory), freePercent, "%"));
        builder.setMemoryMax(asReadableSize(maxMemory));
    }

    protected void appendAsyncJobsValues(StatusDto builder) {
        Pair<Integer, Integer> runningAndPendingTasks = asyncService.countRunningAndPendingTasks();
        builder.setRunningTasks(runningAndPendingTasks.getLeft());
        builder.setPendingTasks(runningAndPendingTasks.getRight());
    }

    @Override
    public String execute() throws Exception {

        long statusStart = System.currentTimeMillis();

        Future<Void> pingJms = asyncService.pingJms();

        StatusDto result = new StatusDto();

        result.setInstance(ManagementFactory.getRuntimeMXBean().getName());

        result.setVersion(config.getApplicationVersion());
        result.setRevision(config.getApplicationRevision());
        result.setBuildDate(config.getApplicationBuildDate());

        result.setCurrentDate(new Date().toString());
        result.setCurrentTimeZone(TimeZone.getDefault().toString());

        result.setEncoding(System.getProperty("file.encoding"));

        String jvmName = System.getProperty("java.vm.name");
        result.setJvmName(jvmName);

        String javaVersion = System.getProperty("java.version");
        result.setJavaVersion(javaVersion);

        appendMemoryValues(result);

        appendAsyncJobsValues(result);

        int availableProcessors = Runtime.getRuntime().availableProcessors();
        result.setAvailableProcessors(availableProcessors);

        OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
        double systemLoadAverage = os.getSystemLoadAverage();
        result.setLoadAverage(systemLoadAverage);

        LocalDateTime startupTime = AgrosystWebApplicationContext.RUNNING_SINCE;
        String runningSince = startupTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        result.setRunningSince(runningSince);
        result.setUptime(formatDuration(startupTime, LocalDateTime.now()));

        long permissionsCount = authorizationService.countPermissions();
        result.setComputedPermissions(permissionsCount);

        ActiveSessionsAndUsers sessionsAndUsers = computeConnectedUsersCount();
        int sessionsCount = sessionsAndUsers.sessionsCount();
        result.setActiveSessions(sessionsCount);
        int connectedUsersCount = sessionsAndUsers.uniqueUsersCount();
        result.setConnectedUsers(connectedUsersCount);

        if (pingJms.isCancelled()) {
            result.setJmsPing("N/A");
        } else {
            try {
                pingJms.get(1, TimeUnit.SECONDS);
                result.setJmsPing("success");
            } catch (TimeoutException te) {
                result.setJmsPing("timeout");
            }
        }

        List<String> threadNames = Thread.getAllStackTraces()
                .keySet()
                .stream()
                .map(t -> String.format("%s (%d)", t.getName(), t.getId()))
                .collect(Collectors.toList());
        if (this.includeThreadDetails) {
            result.setThreadNames(threadNames);
        }
        result.setThreadCount(threadNames.size());

        long statusEnd = System.currentTimeMillis();

        result.setDuration(statusEnd - statusStart);

        jsonData = result;

        return SUCCESS;
    }

}
