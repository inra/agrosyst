package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.domain.ZoneDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveZoneFilter;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import lombok.Getter;
import lombok.Setter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;

/**
 * Display plots list that may be associated (or not) with crop cycles and cropping plan
 * intervention.
 * 
 * @author Eric Chatellier
 */
@Getter
@Setter
public class EffectiveCropCyclesList extends AbstractAgrosystAction {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 6687115109245346054L;

    protected EffectiveZoneFilter plotFilter;

    protected String filter;

    protected transient EffectiveCropCycleService effectiveCropCycleService;

    /** Liste des zones (point d'entrée des cycles réalisés). */
    protected PaginationResult<ZoneDto> zoneListResult;

    /** Flag to use current zone list as entry point for measurement. */
    protected boolean measurement;

    /** In case of file extension error. */
    protected boolean importFileError;

    protected int exportAsyncThreshold;

    protected int measurementExportAsyncThreshold;

    @Override
    public String execute() throws Exception {
        
        // in case of file type error
        if (importFileError) {
            notificationSupport.importError("Format de fichier incorrect !");
            importFileError = false;
        }

        if (plotFilter == null) {
            plotFilter = new EffectiveZoneFilter();
            NavigationContext navigationContext = getNavigationContext();
            plotFilter.setNavigationContext(navigationContext);
            plotFilter.setZoneState(true);
            plotFilter.setPlotState(true);
        }

        plotFilter.setPageSize(getListNbElementByPage(ZoneDto.class, String.valueOf(measurement)));

        // Get Cropping Plan Entries and Intervention nb
        zoneListResult = effectiveCropCycleService.getFilteredZonesAndCroppingPlanInfosDto(plotFilter);

        exportAsyncThreshold = config.getEffectiveCropCyclesExportAsyncThreshold();
        measurementExportAsyncThreshold = config.getEffectiveMeasurementsExportAsyncThreshold();

        return SUCCESS;
    }

    public void setFilter(String filter) {
        plotFilter = getGson().fromJson(filter, EffectiveZoneFilter.class);
    }
}
