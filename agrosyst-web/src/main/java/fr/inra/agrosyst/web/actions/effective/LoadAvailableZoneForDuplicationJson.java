package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * @author David Cossé
 */
public class LoadAvailableZoneForDuplicationJson extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 1L;

    private static final Log LOGGER = LogFactory.getLog(LoadAvailableZoneForDuplicationJson.class);
    
    protected transient String zoneId;

    protected transient PlotService plotService;
    
    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    @Override
    public String execute() {
        try {
            jsonData = plotService.getZonesWithoutCycle(zoneId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load zones without cycle for zoneId '%s'", zoneId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
