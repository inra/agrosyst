package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

/**
 * Created by davidcosse on 17/02/14.
 */
public class ManagementModesDuplicate extends AbstractAgrosystAction{
    
    @Serial
    private static final long serialVersionUID = 1L;

    protected transient ManagementModeService managementModeService;

    protected String growingSystemId;

    protected String plannedManagementModeId;

    protected String observedManagementModeId;

    protected ManagementMode managementMode;

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setGrowingSystemId(String growingSystemId) {
        this.growingSystemId = growingSystemId;
    }

    public void setPlannedManagementModeId(String plannedManagementModeId) {
        this.plannedManagementModeId = plannedManagementModeId;
    }

    public void setObservedManagementModeId(String observedManagementModeId) {
        this.observedManagementModeId = observedManagementModeId;
    }

    public String getGrowingSystemId() {
        return growingSystemId;
    }

    public ManagementMode getManagementMode() {
        return managementMode;
    }

    @Action(results = {
    @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "management-modes-edit-input", "growingSystemTopiaId", "${growingSystemId}", "managementModeTopiaId", "${managementMode.topiaId}"}),
    @Result(name = ERROR, type = "redirectAction", params = {"actionName", "management-modes-list"})})
    public String execute() throws Exception {
        if(Strings.isNullOrEmpty(growingSystemId)){
            addActionError("Un système de culture doit être défini sur un modèle décisionnel");
            return ERROR;
        }
        managementMode = managementModeService.duplicateManagementModes(plannedManagementModeId, observedManagementModeId, growingSystemId);
        notificationSupport.managementModeDuplicated();
        return SUCCESS;
    }

}
