package fr.inra.agrosyst.web.rest.plots;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.plot.PlotFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.web.rest.CustomInject;
import fr.inra.agrosyst.web.rest.Secured;
import fr.inra.agrosyst.web.rest.common.PaginationResultDto;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Set;

@Secured
@Path("/plots")
public class PlotsResource {

    @CustomInject
    private PerformanceService performanceService;

    private PlotFilter buildFilter(NavigationContext navigationContext,
                                   Set<String> selectedDomains,
                                   Set<String> selectedGrowingSystems,
                                   Boolean active,
                                   Set<String> selectedIds,
                                   Integer pageNumber,
                                   Integer pageSize) {
        var filter = new PlotFilter();
        filter.setNavigationContext(navigationContext);
        filter.setDomainIds(selectedDomains.stream().toList());
        filter.setGrowingSystemIds(selectedGrowingSystems.stream().toList());
        filter.setActive(active);
        if (selectedIds != null && !selectedIds.isEmpty()) filter.setSelectedIds(selectedIds);
        if (pageNumber != null) filter.setPage(pageNumber);
        if (pageSize != null) filter.setPageSize(pageSize);
        return filter;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAll(@CookieParam("nav.context") Cookie jsonContext,
                            PlotRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getSelectedDomains(),
                requestFilter.getSelectedGrowingSystems(),
                requestFilter.getActive(),
                requestFilter.getSelectedIds(),
                requestFilter.getPageNumber(),
                requestFilter.getPageSize());

        var paginationResult = performanceService.getPlotsDto(filter);
        var plots = paginationResult.getElements().stream()
                .map(this::convertToDto)
                .toList();
        return Response.ok()
                .entity(new PaginationResultDto<>(plots, paginationResult.getCount(), paginationResult.getCurrentPage()))
                .build();
    }

    private PlotDto convertToDto(fr.inra.agrosyst.api.services.domain.PlotDto plot) {
        var domainName = plot.getGrowingSystem() != null && plot.getGrowingSystem().getGrowingPlan() != null && plot.getGrowingSystem().getGrowingPlan().getDomain() != null ?
                plot.getGrowingSystem().getGrowingPlan().getDomain().getName() : "";
        var growingPlanName = plot.getGrowingSystem() != null && plot.getGrowingSystem().getGrowingPlan() != null ?
                plot.getGrowingSystem().getGrowingPlan().getName() : "";
        var growingSystemName = plot.getGrowingSystem() != null ? plot.getGrowingSystem().getName() : "";
        var campaign = plot.getGrowingSystem() != null && plot.getGrowingSystem().getGrowingPlan() != null && plot.getGrowingSystem().getGrowingPlan().getDomain() != null ?
                plot.getGrowingSystem().getGrowingPlan().getDomain().getCampaign() : 0;
        return new PlotDto(StringUtils.remove(plot.getTopiaId(), Plot.class.getName()), plot.getName(), domainName, growingPlanName, growingSystemName, campaign,
                plot.isActive()
        );
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/filtered-ids")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIds(@CookieParam("nav.context") Cookie jsonContext,
                            PlotRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getSelectedDomains(),
                requestFilter.getSelectedGrowingSystems(),
                requestFilter.getActive(),
                null,
                null,
                null);
        var ids = performanceService.getPlotIds(filter);
        Collection<String> shortenIds = DaoUtils.getShortenIds(ids, Plot.class);
        return Response.ok()
                .entity(shortenIds)
                .build();
    }
}
