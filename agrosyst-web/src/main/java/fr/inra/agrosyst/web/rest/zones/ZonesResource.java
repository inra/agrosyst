package fr.inra.agrosyst.web.rest.zones;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.plot.ZoneFilter;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.web.rest.CustomInject;
import fr.inra.agrosyst.web.rest.Secured;
import fr.inra.agrosyst.web.rest.common.PaginationResultDto;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Set;

@Secured
@Path("/zones")
public class ZonesResource {

    @CustomInject
    private PerformanceService performanceService;

    private ZoneFilter buildFilter(NavigationContext navigationContext, Set<String> selectedPlots, Boolean active,
                                   Set<String> selectedIds, Integer pageNumber, Integer pageSize) {
        var filter = new ZoneFilter();
        filter.setNavigationContext(navigationContext);
        filter.setPlotIds(selectedPlots.stream().toList());
        filter.setActive(active);
        if (selectedIds != null && !selectedIds.isEmpty()) filter.setSelectedIds(selectedIds);
        if (pageNumber != null) filter.setPage(pageNumber);
        if (pageSize != null) filter.setPageSize(pageSize);
        return filter;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAll(@CookieParam("nav.context") Cookie jsonContext,
                            ZoneRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getSelectedPlots(),
                requestFilter.getActive(),
                requestFilter.getSelectedIds(),
                requestFilter.getPageNumber(),
                requestFilter.getPageSize());

        var paginationResult = performanceService.getZonesDto(filter);
        var zones = paginationResult.getElements().stream()
                .map(this::convertToDto)
                .toList();
        return Response.ok()
                .entity(new PaginationResultDto<>(zones, paginationResult.getCount(), paginationResult.getCurrentPage()))
                .build();
    }

    private ZoneDto convertToDto(fr.inra.agrosyst.api.services.domain.ZoneDto zone) {
        var plotName = zone.getPlot() != null ? zone.getPlot().getName() : "";
        var domainName = zone.getPlot() != null && zone.getPlot().getGrowingSystem() != null && zone.getPlot().getGrowingSystem().getGrowingPlan() != null && zone.getPlot().getGrowingSystem().getGrowingPlan().getDomain() != null ?
                zone.getPlot().getGrowingSystem().getGrowingPlan().getDomain().getName() : "";
        var growingPlanName = zone.getPlot() != null && zone.getPlot().getGrowingSystem() != null && zone.getPlot().getGrowingSystem().getGrowingPlan() != null ?
                zone.getPlot().getGrowingSystem().getGrowingPlan().getName() : "";
        var growingSystemName = zone.getPlot() != null && zone.getPlot().getGrowingSystem() != null ?
                zone.getPlot().getGrowingSystem().getName() : "";
        var campaign = zone.getPlot() != null && zone.getPlot().getGrowingSystem() != null && zone.getPlot().getGrowingSystem().getGrowingPlan() != null && zone.getPlot().getGrowingSystem().getGrowingPlan().getDomain() != null ?
                zone.getPlot().getGrowingSystem().getGrowingPlan().getDomain().getCampaign() : 0;
        return new ZoneDto(
                StringUtils.remove(zone.getTopiaId(), Zone.class.getName()),
                zone.getName(),
                plotName,
                domainName,
                growingPlanName,
                growingSystemName,
                campaign,
                zone.isActive()
        );
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/filtered-ids")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIds(@CookieParam("nav.context") Cookie jsonContext,
                            ZoneRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getSelectedPlots(),
                requestFilter.getActive(),
                null,
                null,
                null);
        var ids = performanceService.getZoneIds(filter);
        Collection<String> shortenIds = DaoUtils.getShortenIds(ids, Zone.class);
        return Response.ok()
                .entity(shortenIds)
                .build();
    }
}
