/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;

import java.io.Serial;
import java.util.List;

/**
 * Export XLS des bilan de campagne / echelle regional.
 */
public class ReportRegionalExportXls extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = 9144347904487976767L;
    
    protected transient ReportService reportService;

    protected List<String> reportRegionalIds;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReportRegionalIds(String reportRegionalIds) {
        this.reportRegionalIds = getGson().fromJson(reportRegionalIds, List.class);
    }

    @Override
    protected ExportResult computeExportResult() {
        ExportResult result = reportService.exportXlsReportRegionals(reportRegionalIds);
        return result;
    }

}
