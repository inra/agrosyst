package fr.inra.agrosyst.web.actions.commons;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class EnumsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(EnumsJson.class);
    @Serial
    private static final long serialVersionUID = -2070861344150517877L;
    
    protected String enumType;

    public void setEnumType(String enumType) {
        this.enumType = enumType;
    }

    @Override
    public String execute() throws Exception {
        try {
            Class<?> enumClass = Class.forName("fr.inra.agrosyst.api.entities." + enumType);
            Preconditions.checkArgument(enumClass.isEnum());
            jsonData = getEnumAsMap(enumClass.getEnumConstants());
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load referential list for class " + enumType, e);
            }
            jsonData = "Failed to load referential list for class " + enumType;
            return ERROR;
        }
        return SUCCESS;
    }
}
