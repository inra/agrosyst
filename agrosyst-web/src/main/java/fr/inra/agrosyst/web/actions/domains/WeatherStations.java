package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.WeatherStation;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;

/**
 * Utility class for {@link WeatherStation} and {@link WeatherStationDto}.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class WeatherStations {

    /**
     * Creates a function that returns a {@link WeatherStationDto} instance from a given {@link WeatherStation}.
     *
     * @param defaultWeatherStationId if this {@link WeatherStation} is the default weather station selected (may be <code>null</code>)
     * @return the function that returns a {@link WeatherStationDto} instance from a given {@link WeatherStation}
     */
    static Function<WeatherStation, WeatherStationDto> getFunctionWeatherStationToDto(final String defaultWeatherStationId) {
        return input -> {
            WeatherStationDto dto = new WeatherStationDto();
            String topiaId = input.getTopiaId();
            dto.setTopiaId(topiaId);
            dto.setRefStationMeteoTopiaId(input.getRefStationMeteo().getTopiaId());
            dto.setComment(input.getComment());
            dto.setData(input.getData());

            if (Strings.isNullOrEmpty(defaultWeatherStationId)) {
                dto.setDefaultSelected(false);
            } else {
                boolean defaultSelected = topiaId.equals(defaultWeatherStationId);
                dto.setDefaultSelected(defaultSelected);
            }

            return dto;
        };
    }

    /**
     * Uses {@link WeatherStationDto} corresponding getter from each
     * {@link WeatherStation} instance setter.
     *
     * @param dto            {@link WeatherStationDto} DTO instance
     * @param weatherStation {@link WeatherStation} to update
     * @param refStationMeteo 
     */
    protected static void dtoToWeatherStation(WeatherStationDto dto, WeatherStation weatherStation, RefStationMeteo refStationMeteo) {
        Preconditions.checkNotNull(dto);
        Preconditions.checkNotNull(weatherStation);
        Preconditions.checkNotNull(refStationMeteo);

        weatherStation.setRefStationMeteo(refStationMeteo);
        weatherStation.setComment(dto.getComment());
        weatherStation.setData(dto.getData());
    }
}
