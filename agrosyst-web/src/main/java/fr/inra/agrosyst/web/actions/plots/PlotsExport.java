package fr.inra.agrosyst.web.actions.plots;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Export XSL d'un ensemble de parcelles.
 *
 * @author Eric Chatellier
 */
public class PlotsExport extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = -1618279547328450777L;

    protected transient PlotService plotService;

    protected List<String> plotTopiaIds;

    public void setJsonPlotTopiaIds(String json) {
        Type type = new TypeToken<List<String>>() {}.getType();
        plotTopiaIds = getGson().fromJson(json, type);
    }

    public void setPlotTopiaIds(List<String> plotTopiaIds) {
        this.plotTopiaIds = plotTopiaIds;
    }

    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    @Override
    protected ExportResult computeExportResult() {
        ExportResult result = plotService.exportPlotsAsXls(plotTopiaIds);
        return result;
    }

}
