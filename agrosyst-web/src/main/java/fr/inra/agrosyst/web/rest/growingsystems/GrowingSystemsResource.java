package fr.inra.agrosyst.web.rest.growingsystems;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.web.rest.CustomInject;
import fr.inra.agrosyst.web.rest.Secured;
import fr.inra.agrosyst.web.rest.common.PaginationResultDto;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;

@Secured
@Path("/growing-systems")
public class GrowingSystemsResource {

    @CustomInject
    private GrowingSystemService growingSystemService;

    private GrowingSystemFilter buildFilter(NavigationContext navigationContext,
                                            Set<String> selectedDomains,
                                            String name,
                                            String domainName,
                                            String growingPlanName,
                                            Integer campaign,
                                            Sector sector,
                                            String dephyNumber,
                                            Boolean active,
                                            Set<String> selectedIds,
                                            Integer pageNumber,
                                            Integer pageSize) {
        var filter = new GrowingSystemFilter();
        var context = mergeNavigationContext(navigationContext, selectedDomains);
        filter.setNavigationContext(context);
        filter.setGrowingSystemName(name);
        filter.setDomainName(domainName);
        filter.setGrowingPlanName(growingPlanName);
        filter.setCampaign(campaign);
        filter.setSector(sector);
        filter.setDephyNumber(dephyNumber);
        filter.setActive(active);
        if (selectedIds != null && !selectedIds.isEmpty()) filter.setSelectedIds(selectedIds);
        if (pageNumber != null) filter.setPage(pageNumber);
        if (pageSize != null) filter.setPageSize(pageSize);
        return filter;
    }

    private NavigationContext mergeNavigationContext(NavigationContext context, Set<String> selectedDomains) {
        NavigationContext navigationContext = new NavigationContext(
                context.getCampaigns(),
                context.getNetworks(),
                selectedDomains != null && !selectedDomains.isEmpty() ? selectedDomains : context.getDomains(),
                context.getGrowingPlans(),
                context.getGrowingSystems()
        );
        return navigationContext;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAll(@CookieParam("nav.context") Cookie jsonContext,
                            GrowingSystemRequestFilter request) throws JsonProcessingException {

        var context = jsonContext != null ?
                new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) :
                new NavigationContext();

        var filter = buildFilter(
                context,
                request.getSelectedDomains(),
                request.getName(),
                request.getDomainName(),
                request.getGorwinPlanName(),
                request.getCampaign(),
                request.getSector(),
                request.getDephyNumber(),
                request.getActive(),
                request.getSelectedIds(),
                request.getPageNumber(),
                request.getPageSize());
        var paginationResult = growingSystemService.getFilteredGrowingSystems(filter);
        var growingSystems = paginationResult.getElements().stream()
                .map(this::convertToDto)
                .toList();
        Response response = Response.ok()
                .entity(new PaginationResultDto<>(growingSystems, paginationResult.getCount(), paginationResult.getCurrentPage()))
                .build();
        return response;
    }

    private GrowingSystemDto convertToDto(GrowingSystem gs) {
        var domainName = gs.getGrowingPlan() != null && gs.getGrowingPlan().getDomain() != null ?
                gs.getGrowingPlan().getDomain().getName() : "";
        var growingPlanName = gs.getGrowingPlan() != null ? gs.getGrowingPlan().getName() : "";
        var campaign = gs.getGrowingPlan() != null && gs.getGrowingPlan().getDomain() != null ?
                gs.getGrowingPlan().getDomain().getCampaign() : 0;
        GrowingSystemDto growingSystemDto = new GrowingSystemDto(
                StringUtils.remove(gs.getTopiaId(), GrowingSystem.class.getName()),
                gs.getName(),
                domainName,
                growingPlanName,
                campaign,
                gs.getSector(),
                gs.getDephyNumber(),
                gs.isActive());
        return growingSystemDto;
    }

    @POST
    @Path("/filtered-ids")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIds(@CookieParam("nav.context") Cookie jsonContext,
                            GrowingSystemRequestFilter request) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                request.getSelectedDomains(),
                request.getName(),
                request.getDomainName(),
                request.getGorwinPlanName(),
                request.getCampaign(),
                request.getSector(),
                request.getDephyNumber(),
                request.getActive(),
                null,
                null,
                null);
        var ids = DaoUtils.getShortenIds(growingSystemService.getFilteredGrowingSystemIds(filter), GrowingSystem.class);
        Response response = Response.ok()
                .entity(ids)
                .build();
        return response;
    }
}
