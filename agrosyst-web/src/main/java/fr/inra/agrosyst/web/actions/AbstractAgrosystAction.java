package fr.inra.agrosyst.web.actions;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.xwork2.ActionSupport;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.DoseType;
import fr.inra.agrosyst.api.entities.EstimatingIftRules;
import fr.inra.agrosyst.api.entities.IftSeedsType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.history.Message;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.services.common.AttachmentService;
import fr.inra.agrosyst.api.services.context.NavigationContextService;
import fr.inra.agrosyst.api.services.history.MessageService;
import fr.inra.agrosyst.api.services.security.AuthenticationService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import fr.inra.agrosyst.web.AgrosystWebConfig;
import fr.inra.agrosyst.web.AgrosystWebNotification;
import fr.inra.agrosyst.web.AgrosystWebNotificationSupport;
import fr.inra.agrosyst.web.CookieHelper;
import fr.inra.agrosyst.web.converters.LocalDateTimeHelper;
import fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter;
import fr.inra.agrosyst.web.listeners.AgrosystWebSessionListener;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.action.ParametersAware;
import org.apache.struts2.action.ServletRequestAware;
import org.apache.struts2.action.ServletResponseAware;
import org.apache.struts2.dispatcher.HttpParameters;
import org.nuiton.topia.persistence.TopiaEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serial;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Toutes les actions Struts doivent hériter de cette classe.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Getter
@Setter
public abstract class AbstractAgrosystAction extends ActionSupport implements ServletRequestAware, ServletResponseAware,
        ParametersAware {
    
    @Serial
    private static final long serialVersionUID = -8765692518009997590L;
    
    protected static final String REQUIRED_FIELD = "common.filedValue.required";
    protected static final String NAVIGATION_CONTEXT = "NavigationContext";
    
    protected static final DecimalFormat df = new DecimalFormat("#.#");
    
    protected static final Log LOGGER = LogFactory.getLog(AbstractAgrosystAction.class);

    protected static final AgrosystLayoutData ERROR_LAYOUT_DATA = new AgrosystLayoutData();
    static {
        ERROR_LAYOUT_DATA.setCurrentUserAnAdmin(false);
        ERROR_LAYOUT_DATA.setCurrentUserFirstName("#Error!");
        ERROR_LAYOUT_DATA.setCurrentUserLastName("#Error!");
        ERROR_LAYOUT_DATA.setNavigationContext(new NavigationContext());
    }

    // Injectés par fr.inra.agrosyst.web.AgrosystWebInterceptor
    protected transient AgrosystWebConfig config;
    protected transient AgrosystWebNotificationSupport notificationSupport;
    protected transient AgrosystWebApplicationContext applicationContext;
    
    protected transient NavigationContextService navigationContextService;
    protected transient BusinessAuthorizationService authorizationService;
    protected transient MessageService messageService;
    protected transient AgrosystI18nService i18nService;
    protected transient AuthenticationService authenticationService;

    protected transient HttpServletRequest servletRequest;
    protected transient HttpServletResponse servletResponse;

    protected transient HttpParameters parameters;

    protected transient RichNavigationContext richNavigationContext;

    protected transient boolean readOnly = false;
    protected transient boolean activated = true;

    protected transient Boolean currentUserAnAdmin;
    protected transient Boolean currentUserAnIsDataProcessor;
    protected transient String currentUserLanguage;
    protected transient String baseUrl;

    /** Attachment management */
    protected transient AttachmentService attachmentService;

    protected transient Long attachmentCount;

    protected String lastBroadcastDate;
    protected List<Message> broacastMessages;

    /**
     * gson (can be used into non json action to initialize json data).
     */
    private transient Gson gson;

    public Gson getGson() {
        if (gson == null) {
            gson = new AgrosystGsonSupplier().get();
        }
        return gson;
    }

    public CookieHelper getCookieHelper() {
        HttpServletRequest servletRequest = getServletRequest();
        CookieHelper result = new CookieHelper(servletRequest, servletResponse, getConfig());
        return result;
    }

    public String toJson(Object element) {
        String result = null;
        try {
            result = getGson().toJson(element);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e, e);
            }
        }
        return result;
    }

    public Set<String> getSelectedGrowingSystemIds(String growingSystemIdsJson) {
        Type type = new TypeToken<Map<String, Boolean>>() {}.getType();
        Map<String, Boolean> managementModeDtos = getGson().fromJson(growingSystemIdsJson, type);
        Set<String> growingSystemIds = new HashSet<>();
        if (managementModeDtos != null) {
            for (Map.Entry<String, Boolean> growingSystemIdToSelected : managementModeDtos.entrySet()) {
                if (growingSystemIdToSelected.getValue()) {
                    growingSystemIds.add(growingSystemIdToSelected.getKey());
                }
            }
        }
        return growingSystemIds;
    }

    @Override
    public void withServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

    @Override
    public void withServletResponse(HttpServletResponse servletResponse) {
        this.servletResponse = servletResponse;
    }

    @Override
    public void withParameters(HttpParameters parameters) {
        this.parameters = parameters;
    }

    public final void setConfig(AgrosystWebConfig config) {
        this.config = config;
    }

    public final void setNavigationContextService(NavigationContextService navigationContextService) { // AThimel 11/10/13 Final to make sure it is not overridden
        this.navigationContextService = navigationContextService;
    }

    public final void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public final void setAuthorizationService(BusinessAuthorizationService authorizationService) { // AThimel 11/10/13 Final to make sure it is not overridden
        this.authorizationService = authorizationService;
    }

    public final void setNotificationSupport(AgrosystWebNotificationSupport notificationSupport) {
        this.notificationSupport = notificationSupport;
    }

    public String getSessionId() {
        return AgrosystWebSessionListener.getSessionId(getServletRequest());
    }

    protected NavigationContext getNavigationContext() {


        // Lecture du cookie
        Optional<NavigationContext> navigationContext = readNavigationContextCookie();

        NavigationContext result = navigationContext.orElseGet(NavigationContext::new);
        return result;
    }

    protected NavigationContext verifyAndSaveNavigationContext(NavigationContext navigationContext) {
        NavigationContext newNavigationContext = navigationContextService.verify(navigationContext);
        writeNavigationContextCookie(newNavigationContext);
        return newNavigationContext;
    }

    protected NavigationContext navigationContextEntityCreated(TopiaEntity newEntity) {
        NavigationContext navigationContext = getNavigationContext();
        NavigationContext newNavigationContext = navigationContextService.verify(navigationContext, newEntity);
        writeNavigationContextCookie(newNavigationContext);
        return newNavigationContext;
    }

    protected Optional<NavigationContext> readNavigationContextCookie() {
        Optional<NavigationContext> result = getCookieHelper().readCookie(
                AgrosystWebConfig.NAVIGATION_CONTEXT_COOKIE_NAME,
                cookieValue -> {
                    NavigationContext parsed = getGson().fromJson(cookieValue, NavigationContext.class);
                    return parsed;
                });
        return result;
    }

    protected void writeNavigationContextCookie(NavigationContext navigationContext) {
        getCookieHelper().writeCookie(AgrosystWebConfig.NAVIGATION_CONTEXT_COOKIE_NAME, navigationContext, getGson()::toJson, false);
    }

    public RichNavigationContext getRichNavigationContext() {
        if (richNavigationContext == null) {
            NavigationContext navigationContext = getNavigationContext();
            richNavigationContext = new RichNavigationContext(navigationContext, navigationContextService);
        }
        return richNavigationContext;
    }

    protected Integer getListNbElementByPage(Class<?> listType) {
        return getListNbElementByPage(listType, "");
    }

    protected Integer getListNbElementByPage(Class<?> listType, String suffix) {

        String cookieName = AgrosystWebConfig.NB_ELEMENT_BY_PAGE_COOKIE_NAME + "." + listType.getSimpleName() + suffix;
        Optional<Integer> value = getCookieHelper().readCookie(cookieName, Integer::valueOf);

        // Si pas de cookie trouvé, création du nb par defaut par défaut
        Integer result = value.orElseGet(getConfig()::getListResultsPerPage);

        return result;
    }

    protected void writeListNbElementByPage(Class<?> listType, int nbElementByPage) {
        writeListNbElementByPage(listType, "", nbElementByPage);
    }

    protected void writeListNbElementByPage(Class<?> listType, String suffix, int nbElementByPage) {
        String cookieName = AgrosystWebConfig.NB_ELEMENT_BY_PAGE_COOKIE_NAME + "." + listType.getSimpleName() + suffix;

        CookieHelper cookieHelper = getCookieHelper();
        if (nbElementByPage == getConfig().getListResultsPerPage()) {
            cookieHelper.deleteCookie(cookieName);
        } else {
            cookieHelper.writeCookie(cookieName, nbElementByPage, String::valueOf, true);
        }
    }


    /**
     * Transform enumeration values into map with i18n value for each enum value.
     *
     * i18n key is fqn.NAME
     *
     * @param <T> generic type
     * @param values values to transform
     * @return map (enum value &gt; i18n text)
     */
    @SafeVarargs
    protected final <T> Map<T, String> getEnumAsMap(T... values) {
        return i18nService.getEnumAsMap(null, values);
    }

    protected void initForInput() {

    }

    /**
     * Return session info notification.
     *
     * Warning : this get method clear resulting collection
     *
     * @return session info notifications
     */
    public Set<AgrosystWebNotification> getInfoNotifications() {
        Set<AgrosystWebNotification> notifications = notificationSupport.consumeInfoNotifications();
        return notifications;
    }

    public boolean isInfoNotificationsEmpty() {
        return notificationSupport.isInfoNotificationsEmpty();
    }
    
    public Set<AgrosystWebNotification> getLightBoxNotifications() {
        Set<AgrosystWebNotification> notifications = notificationSupport.consumeLightBoxNotifications();
        return notifications;
    }

    protected List<Message> loadBroadcastMessages(LocalDateTime fromDate) {
        // Cette méthode est appelée pendant le cycle de rendu, donc pas d'appel DAO possible. C'est pourquoi on ouvre un ServiceContext éphémère.
        try (ServiceContext serviceContext = applicationContext.getApplicationContext().newServiceContext()) {
            MessageService service = serviceContext.newService(MessageService.class);
            List<Message> result = service.getMessagesFromDate(fromDate);
            return result;
        } catch (Exception eee) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Unexpected error in ephemeral context", eee);
            }
            return new LinkedList<>();
        }
    }

    /**
     * Return broadcast messages
     *
     * @return broadcast messages
     */
    public List<Message> getBroadcastMessages() {
        if (broacastMessages == null) {
            broacastMessages = Optional.ofNullable(this.lastBroadcastDate)
                    .map(LocalDateTimeHelper::urlSafeParse)
                    .map(this::loadBroadcastMessages)
                    .orElseGet(LinkedList::new);
        }
        return broacastMessages;
    }

    public boolean isBroadcastMessagesEmpty() {
        return getBroadcastMessages().isEmpty();
    }

    public boolean getIsFirstTimeMessageRead() {
        return !getBroadcastMessages().isEmpty();
    }

    public void setLastBroadcastDate(String lastBroadcastDate) {
        this.lastBroadcastDate = StringUtils.trimToNull(StringUtils.trimToEmpty(lastBroadcastDate).split(", ")[0]);
    }

    /**
     * Return session warning notifications.
     *
     * Warning : this get method clear resulting collection
     *
     * @return session warning notifications
     */
    public Set<AgrosystWebNotification> getWarningNotifications() {
        Set<AgrosystWebNotification> messages = notificationSupport.consumeWarningNotifications();
        return messages;
    }

    public boolean isWarningNotificationsEmpty() {
        return notificationSupport.isWarningNotificationsEmpty();
    }

    /**
     * Return session error notifications.
     *
     * Warning : this get method clear resulting collection
     *
     * @return session error notifications
     */
    public Set<AgrosystWebNotification> getErrorNotifications() {
        Set<AgrosystWebNotification> messages = notificationSupport.consumeErrorNotifications();
        return messages;
    }

    public boolean isErrorNotificationsEmpty() {
        return notificationSupport.isErrorNotificationsEmpty();
    }

    protected Optional<AuthenticatedUser> getAuthenticatedUserOptional() {
        HttpServletRequest servletRequest = getServletRequest();
        AuthenticatedUser userDto = (AuthenticatedUser) servletRequest.getAttribute(AgrosystWebAuthenticationFilter.AUTHENTICATED_USER);
        Optional<AuthenticatedUser> result = Optional.ofNullable(userDto);
        return result;
    }

    protected AuthenticatedUser getAuthenticatedUser() {
        AuthenticatedUser result = getAuthenticatedUserOptional().orElseThrow(() -> new IllegalStateException("AuthenticatedUser manquant"));
        return result;
    }

    public boolean isCurrentUserAnAdmin() {
        if (currentUserAnAdmin == null) {
            currentUserAnAdmin = getAuthenticatedUserOptional()
                    .map(AuthenticatedUser::isAdmin)
                    .orElse(false);
        }
        return currentUserAnAdmin;
    }

    public boolean isCurrentUserAnIsDataProcessor() {
        if (currentUserAnIsDataProcessor == null) {
            currentUserAnAdmin = getAuthenticatedUserOptional()
                    .map(AuthenticatedUser::isISDataProcessor)
                    .orElse(false);
        }
        return currentUserAnIsDataProcessor;
    }

    public String getCurrentUserLanguage() {
        if (currentUserLanguage == null) {
            currentUserLanguage = getAuthenticatedUserOptional()
                    .map(AuthenticatedUser::getLocale)
                    .map(Locale::getLanguage)
                    .orElse(Locale.FRANCE.getLanguage());
        }
        return currentUserLanguage;
    }

    public String getBaseUrl() {
        if (baseUrl == null) {
            baseUrl = getConfig().getBaseUrl();
        }
        return baseUrl;
    }

    public long getAttachmentCount(String codeOrId) {
        if (attachmentCount == null) {
            attachmentCount = attachmentService.getAttachmentMetadatasCount(codeOrId);
        }
        return attachmentCount;
    }

    public AgrosystLayoutData initLayoutData() {

        HttpServletRequest servletRequest = getServletRequest();
        AgrosystLayoutData result;
        String bannerName = null;
        try {

            if (this.layoutData == null) {
                Object locLayoutData = servletRequest.getAttribute("layoutData");
                AgrosystLayoutData instance;
                if (locLayoutData == null) {
                    instance = new AgrosystLayoutData();
                } else {
                    instance = (AgrosystLayoutData)locLayoutData;
                }
                boolean admin = false;
                boolean isDp = false;

                Optional<AuthenticatedUser> authenticatedUser = getAuthenticatedUserOptional();
                if (authenticatedUser.isPresent()) {
                    instance.setCurrentUserLastName(authenticatedUser.get().getLastName());
                    instance.setCurrentUserFirstName(authenticatedUser.get().getFirstName());
                    instance.setCurrentUserItEmail(authenticatedUser.get().getItEmail().orElse(null));
                    instance.setCurrentUserLanguage(authenticatedUser.get().getLanguage());
                    admin = authenticatedUser.get().isAdmin();
                    isDp = authenticatedUser.get().isISDataProcessor();

                    instance.setNavigationContext(getNavigationContext());
                    instance.setCampaigns(getRichNavigationContext().getCampaigns());
                    instance.setNetworks(getRichNavigationContext().getNetworks());
                    instance.setDomains(getRichNavigationContext().getDomains());
                    instance.setGrowingPlans(getRichNavigationContext().getGrowingPlans());
                    instance.setGrowingSystems(getRichNavigationContext().getGrowingSystems());

                    bannerName = authenticatedUser.get().getBanner().orElse(null);

                }
                instance.setCurrentUserAnAdmin(admin);
                instance.setCurrentUserAnIsDataProcessor(isDp);
                // Assign value at the very end to be sure to exception is raised
                this.layoutData = instance;
            }
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Unable to compute layoutData: ", eee);
            }
        } finally {
            result = MoreObjects.firstNonNull(this.layoutData, ERROR_LAYOUT_DATA);

            // banner map
            if (applicationContext != null) {
                Map<String, String[]> bannersMap = applicationContext.getBannersMap();
                if (!bannersMap.containsKey(bannerName)) {
                    bannerName = "Photo_F_Liagre";
                }
                String[] bannerData = bannersMap.get(bannerName);
                if (bannerData != null) {
                    result.setCurrentUserBannerPath(bannerData[0]);
                    result.setCurrentUserBannerMeta(bannerData[1]);
                }
            }

            if (servletRequest != null) {
                servletRequest.setAttribute("layoutData", result);
            }
        }

        return result;
    }

    protected transient AgrosystLayoutData layoutData;

    public AgrosystLayoutData getLayoutData() {
        if (layoutData == null) {
            this.layoutData = initLayoutData();
        }
        return this.layoutData;
    }

    protected String getServletInfo() {
        // AThimel 04/07/14 Note : cannot use servletRequest from AbstractAgrosystAction because maybe it is not yet valued
        HttpServletRequest httpServletRequest = getServletRequest();
        Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        StringBuilder headers = new StringBuilder();
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement();
            headers.append(String.format("[%s=%s] ", name, httpServletRequest.getHeader(name)));
        }

        return String.format("Requested URL is: %s ; headers are: %s", httpServletRequest.getRequestURL(), headers.toString().trim());
    }

    public Map<YealdUnit, String> getYealdUnits() {
        Map<YealdUnit, String> enumTranslationMap = i18nService.getEnumTranslationMap(YealdUnit.class);
        return enumTranslationMap;
    }

    protected HttpServletRequest getServletRequest() {

        if (servletRequest == null) {
            HttpServletRequest httpServletRequest = ServletActionContext.getRequest();
            withServletRequest(httpServletRequest);
        }

        return servletRequest;
    }
    

    // for js, css cache reload
    public String getVersionSuffix() {
        String version = config.getApplicationVersion();
        return String.format("?v=%s", version);
    }

    /**
     * Empty method to prevent log message from struts.
     */
    public void setV(String v) {

    }

    public Map<EstimatingIftRules, String> getEstimatingIftRules() {
        return getEnumAsMap(EstimatingIftRules.values());
    }

    public Map<IftSeedsType, String> getIftSeedsTypes() {
        return getEnumAsMap(IftSeedsType.values());
    }

    public Map<DoseType, String> getDoseTypes() {
        return getEnumAsMap(DoseType.values());
    }

    protected String getLogEntityId(TopiaEntity te) {
        return te != null && te.getTopiaId() != null ? te.getTopiaId() : "?";
    }

    public Pair<Integer, Integer> getCampaignsBounds() {
        return CommonService.getInstance().getCampaignsBounds();
    }

    /**
     * @deprecated since 2.6 : remove this from abstract action
     */
    @Deprecated
    public Map<Sector, String> getSectors() {
        return getEnumAsMap(Sector.values());
    }

    /**
     * @deprecated since 2.6 : remove this from abstract action
     */
    @Deprecated
    public Map<SectionType, String> getSectionTypes() {
        return getEnumAsMap(SectionType.values());
    }

    public boolean isErrors() {
        return hasErrors();
    }

    protected void refreshAuthenticatedUser() {
        Optional<AuthenticatedUser> authenticatedUserOptional = getAuthenticatedUserOptional();
        if (authenticatedUserOptional.isPresent()) {
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info("Demande explicite pour rafraîchir le token");
            }

            AuthenticatedUser authenticatedUser = authenticationService.reloadAuthenticatedUser();
            applicationContext.writeAuthenticationCookie(authenticatedUser, getCookieHelper());
        }
    }

    public record ActiveSessionsAndUsers(int sessionsCount, int uniqueUsersCount) {
        public int getSessionsCount() {
            return sessionsCount;
        }

        public int getUniqueUsersCount() {
            return uniqueUsersCount;
        }
    }

    protected ActiveSessionsAndUsers computeConnectedUsersCount() {
        Map<String, String> sessions = applicationContext.getSessionsCache();
        int activeSessions = sessions.keySet().size(); // Attention, sessions.size() ne fonctionne pas en mode in memory !
        int uniqueUsers = new HashSet<>(sessions.values()).size();
        ActiveSessionsAndUsers result = new ActiveSessionsAndUsers(activeSessions, uniqueUsers);
        return result;
    }

    @Override
    public void addActionError(String anErrorMessage) {
        if (!super.getActionErrors().contains(anErrorMessage)) {
            super.addActionError(anErrorMessage);
        }
    }

    @Override
    public void addActionMessage(String anErrorMessage) {
        if (!super.getActionMessages().contains(anErrorMessage)) {
            super.addActionMessage(anErrorMessage);
        }
    }

    @Override
    public void addFieldError(String fieldName, String errorMessage) {
        if (!super.getFieldErrors().containsKey(fieldName)) {
            super.addFieldError(fieldName, errorMessage);
        }
    }

}
