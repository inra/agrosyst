package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by davidcosse on 11/02/21.
 */
public class DomainCropsCopy extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 1L;
    
    private static final Log LOGGER = LogFactory.getLog(DomainCropsCopy.class);
    
    protected transient String fromDomain;
    
    protected transient String toDomains;
    
    protected String croppingPlansJson;
    
    protected transient DomainService domainService;
    
    
    @Override
    public String execute() throws Exception {
        try {
            List<String> targetedDomainIds = getTargetedDomainIds(toDomains);
            List<CroppingPlanEntryDto> croppingPlans;
            croppingPlans = convertCroppingPlansJson(croppingPlansJson);
            jsonData = domainService.pasteCrops(fromDomain, targetedDomainIds, croppingPlans);
            return SUCCESS;
        } catch (IllegalArgumentException e) {
            jsonData = e.getClass().getSimpleName();
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("copy crops from domain '%s' to domains '%s' failed", fromDomain, toDomains), e);
            }
            return ERROR;
        } catch (Exception e) {
            return ERROR;
        }
        
    }
    
    public void setFromDomain(String fromDomain) {
        this.fromDomain = fromDomain;
    }
    
    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }
    
    public void setToDomains(String json) {
        this.toDomains = json;
    }
    
    public void setCroppingPlansJson(String croppingPlansJson) {
        this.croppingPlansJson = croppingPlansJson;
    }
    
    protected List<String> getTargetedDomainIds(String json) {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> targetedDomains = getGson().fromJson(json, type);
        return targetedDomains;
    }
    
    public List<CroppingPlanEntryDto> convertCroppingPlansJson(String json) {
        try {
            Type type = new TypeToken<List<CroppingPlanEntryDto>>() {
            }.getType();
            return getGson().fromJson(json, type);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Échec de déserialisation des cultures: '" + json + "' :" + ex.getMessage());
            }
        }
        return null;
    }
}
