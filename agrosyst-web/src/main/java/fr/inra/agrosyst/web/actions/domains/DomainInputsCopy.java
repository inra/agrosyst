package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by ggley on 26/09/2022.
 */
public class DomainInputsCopy extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = 5286218067644801016L;

    private static final Log LOGGER = LogFactory.getLog(DomainInputsCopy.class);

    protected transient String fromDomain;

    protected transient String toDomains;

    protected transient String inputs;

    protected transient DomainService domainService;

    protected transient boolean forceCopy;
    
    @Override
    public String execute() throws Exception {
        try {
            List<String> toDomainIds = getToDomainIds(toDomains);
            List<String> inputIds = getInputIds(inputs);
            jsonData = domainService.copyInputs(fromDomain, inputIds, toDomainIds, forceCopy);
            return SUCCESS;
        } catch (IllegalArgumentException e) {
            jsonData = e.getClass().getSimpleName();
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("copy tools from domain '%s' to domains '%s' failed", fromDomain, toDomains), e);
            }
            return ERROR;
        } catch (Exception e) {
            return ERROR;
        }

    }

    public void setFromDomain(String fromDomain) {
        this.fromDomain = fromDomain;
    }

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }
    
    public void setForceCopy(boolean forceCopy) {
        this.forceCopy = forceCopy;
    }
    
    public void setToDomains(String json) {
        this.toDomains = json;
    }

    public void setInputs(String json) {
        this.inputs = json;
    }

    protected List<String> getToDomainIds(String json) {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }

    protected List<String> getInputIds(String json) {
        Type type = new TypeToken<List<String>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }
}
