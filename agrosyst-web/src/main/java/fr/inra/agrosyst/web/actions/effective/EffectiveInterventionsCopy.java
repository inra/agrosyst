package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveInterventionDto;
import fr.inra.agrosyst.api.services.effective.TargetedZones;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by davidcosse on 02/07/14.
 */
public class EffectiveInterventionsCopy extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 1L;
    
    protected transient List<TargetedZones> targetedZones;

    protected transient List<EffectiveInterventionDto> interventionDtos;

    protected transient EffectiveCropCycleService service;
    
    @Override
    public String execute() throws Exception {
        if (targetedZones != null) {

            jsonData = service.copyInterventions(targetedZones, interventionDtos);
            return SUCCESS;

        }
        jsonData = true;
        return ERROR;
    }

    public void setInterventionDtos(String json) {
        Type type = new TypeToken<List<EffectiveInterventionDto>>() {}.getType();
        this.interventionDtos = getGson().fromJson(json, type);
    }

    public void setTargetedZones(String json) {
        Type type = new TypeToken<List<TargetedZones>>() {}.getType();
        this.targetedZones = getGson().fromJson(json, type);
    }

    public void setService(EffectiveCropCycleService service) {
        this.service = service;
    }

}
