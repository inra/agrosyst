package fr.inra.agrosyst.web.actions.commons;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.Preparable;
import org.reflections.Reflections;

import java.io.Serial;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class I18nAction extends AbstractAgrosystAction implements Preparable {
    
    @Serial
    private static final long serialVersionUID = 2931731809232596488L;
    private final SortedMap<String, Map<String, String>> i18n = new TreeMap<>();
    private String userLocale;
    private String defaultLocale;

    @Override
    public void prepare() {
        userLocale = i18nService.getLocale().toString();
        defaultLocale = AgrosystI18nService.getDefaultLocale().toString();
    }

    @Override
    public String execute() {
        // all enum classes
        Reflections reflections = new Reflections("fr.inra.agrosyst.api");
        Set<Class<? extends Enum>> enumClasses = reflections.getSubTypesOf(Enum.class);
        enumClasses.forEach(aEnum -> {
            Map<String, String> translate = new LinkedHashMap<>();
            if (i18nService.isTranslatedInDatabase(aEnum)) {
                Map<? extends Enum, String> enumTranslationMap = i18nService.getEnumTranslationMap(aEnum);
                enumTranslationMap.forEach((key, value) -> translate.put(key.name(), value));
            } else {
                Map<? extends Enum, String> enumAsMap = i18nService.getEnumAsMap(null, aEnum.getEnumConstants());
                enumAsMap.forEach((key, value) -> translate.put(key.name(), value));
            }
            i18n.put(aEnum.getSimpleName(), translate);
        });

        // additional context enums
        Method[] declaredMethods = AgrosystI18nService.class.getDeclaredMethods();
        Arrays.stream(declaredMethods)
            .filter(m -> m.getAnnotation(AgrosystI18nService.EnumContext.class) != null)
            .forEach(m -> {
                AgrosystI18nService.EnumContext annotation = m.getAnnotation(AgrosystI18nService.EnumContext.class);
                Class<? extends Enum> aEnum = annotation.contextEnum();
                String context = annotation.context();

                try {
                    // use invoke because service know how to build enum map specifically
                    Map<? extends Enum, String> enumAsMap = (Map<? extends Enum, String>)m.invoke(i18nService);
                    Map<String, String> translate = new LinkedHashMap<>();
                    enumAsMap.forEach((key, value) -> translate.put(key.name(), value));

                    i18n.put(aEnum.getSimpleName() + "#" + context, translate);
                } catch (IllegalAccessException | InvocationTargetException ex) {
                    throw new AgrosystTechnicalException("Can't translate enum", ex);
                }
            });

        i18n.put("messages", i18nService.getMessages());

        return SUCCESS;
    }

    public SortedMap<String, Map<String, String>> getI18n() {
        return i18n;
    }

    public String getUserLocale() {
        return userLocale;
    }

    public String getDefaultLocale() {
        return defaultLocale;
    }
}
