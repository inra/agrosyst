package fr.inra.agrosyst.web;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.util.Set;

/**
 * Cette classe étend le fonctionnement de {@link AgrosystWebNotificationSupport} mais déclenche une mise à jour grâce à
 * {@link #updater} à chaque modification.
 *
 * @since 2.64
 * @author Arnaud Thimel (Code Lutin)
 */
public class AgrosystWebNotificationSupportDistributed extends AgrosystWebNotificationSupport {

    @Serial
    private static final long serialVersionUID = 3472718857472300995L;

    // Cette implémentation a besoin d'un appel de méthode pour mettre à jour l'instance dans le cache
    private transient Runnable updater;

    public void setUpdater(Runnable updater) {
        this.updater = updater;
    }

    @Override
    protected void addInfoNotification(AgrosystWebNotification msg) {
        super.addInfoNotification(msg);
        this.updater.run();
    }

    @Override
    protected void addWarningNotification(AgrosystWebNotification msg) {
        super.addWarningNotification(msg);
        this.updater.run();
    }

    @Override
    protected void addErrorNotification(AgrosystWebNotification msg) {
        super.addErrorNotification(msg);
        this.updater.run();
    }

    @Override
    protected void addLightBoxNotification(AgrosystWebNotification msg) {
        super.addLightBoxNotification(msg);
        this.updater.run();
    }

    @Override
    public Set<AgrosystWebNotification> consumeInfoNotifications() {
        Set<AgrosystWebNotification> result = super.consumeInfoNotifications();
        this.updater.run();
        return result;
    }

    @Override
    public Set<AgrosystWebNotification> consumeWarningNotifications() {
        Set<AgrosystWebNotification> result = super.consumeWarningNotifications();
        this.updater.run();
        return result;
    }

    @Override
    public Set<AgrosystWebNotification> consumeErrorNotifications() {
        Set<AgrosystWebNotification> result = super.consumeErrorNotifications();
        this.updater.run();
        return result;
    }

    @Override
    public Set<AgrosystWebNotification> consumeLightBoxNotifications() {
        Set<AgrosystWebNotification> result = super.consumeLightBoxNotifications();
        this.updater.run();
        return result;
    }
}
