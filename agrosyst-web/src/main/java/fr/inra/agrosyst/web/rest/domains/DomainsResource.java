package fr.inra.agrosyst.web.rest.domains;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.security.AuthorizationService;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.web.rest.CustomInject;
import fr.inra.agrosyst.web.rest.Secured;
import fr.inra.agrosyst.web.rest.common.PaginationResultDto;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Secured
@Path("/domains")
public class DomainsResource {

    @CustomInject
    private DomainService domainService;
    @CustomInject
    private AuthorizationService authorizationService;

    private DomainFilter buildFilter(NavigationContext navigationContext, String name, Integer campaign, DomainType type,
                                     String responsable, String departement, Boolean active, Set<String> selectedIds,
                                     Boolean withoutContext, Integer pageNumber, Integer pageSize) {
        var filter = new DomainFilter();
        var contextWithoutDomains = new NavigationContext(navigationContext.getCampaigns(), navigationContext.getNetworks(),
                Collections.emptySet(), navigationContext.getGrowingPlans(), navigationContext.getGrowingSystems());
        filter.setNavigationContext(withoutContext != null && withoutContext ? contextWithoutDomains : navigationContext);
        filter.setDomainName(name);
        filter.setCampaign(campaign);
        filter.setType(type);
        filter.setResponsable(responsable);
        filter.setDepartement(departement);
        filter.setActive(active);
        if (selectedIds != null && !selectedIds.isEmpty()) filter.setSelectedIds(selectedIds);
        if (pageNumber != null) filter.setPage(pageNumber);
        if (pageSize != null) filter.setPageSize(pageSize);
        return filter;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readAll(@CookieParam("nav.context") Cookie jsonContext,
                            DomainRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getName(),
                requestFilter.getCampaign(),
                requestFilter.getType(),
                requestFilter.getResponsable(),
                requestFilter.getDepartement(),
                requestFilter.getActive(),
                requestFilter.getSelectedIds(),
                requestFilter.getWithoutContext(),
                requestFilter.getPageNumber(),
                requestFilter.getPageSize());

        var paginationResult = domainService.getFilteredDomains(filter);
        List<DomainDto> domains = paginationResult.getElements().stream()
                .map((d) -> {
                    List<UserDto> responsibles = authorizationService.getDomainResponsibles(d.getCode());
                    var responsiblesAsStr = responsibles.stream().map((r) -> r.getFirstName() + " " + r.getLastName())
                            .collect(Collectors.joining(", "));
                    return new DomainDto(
                            StringUtils.remove(d.getTopiaId(), Domain.class.getName()),
                            d.getName(),
                            d.getCampaign(),
                            d.getType(),
                            responsiblesAsStr,
                            d.getLocation().getDepartement(),
                            d.isActive());
                })
                .toList();
        return Response.ok()
                .entity(new PaginationResultDto<>(domains, paginationResult.getCount(), paginationResult.getCurrentPage()))
                .build();
    }

    @Path("/filtered-ids")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIds(@CookieParam("nav.context") Cookie jsonContext,
                            DomainRequestFilter requestFilter) throws JsonProcessingException {
        var context = jsonContext != null ? new ObjectMapper().readValue(URLDecoder.decode(jsonContext.getValue(), StandardCharsets.UTF_8), NavigationContext.class) : new NavigationContext();
        var filter = buildFilter(
                context,
                requestFilter.getName(),
                requestFilter.getCampaign(),
                requestFilter.getType(),
                requestFilter.getResponsable(),
                requestFilter.getDepartement(),
                requestFilter.getActive(),
                null,
                requestFilter.getWithoutContext(),
                null,
                null);

        var ids = DaoUtils.getShortenIds(domainService.getFilteredDomainIds(filter), Domain.class);
        return Response.ok()
                .entity(ids)
                .build();
    }
}
