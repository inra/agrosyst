/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports.api;

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.services.report.ReportFilter;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.HashMap;
import java.util.Map;

public class ReportGrowingSystemAllCreationContext extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(ReportGrowingSystemAllCreationContext.class);
    @Serial
    private static final long serialVersionUID = 1671521594104613616L;
    
    protected transient ReportService reportService;

    protected transient String filter;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String execute() {
        try {
            ReportFilter rfilter = getGson().fromJson(filter, ReportFilter.class);
            Map<String, Object> data = new HashMap<>();

            NavigationContext navigationContext = getNavigationContext();
            rfilter.setNavigationContext(navigationContext);

            // domains
            data.put(Domain.class.getSimpleName(), reportService.getDomainsNameToId(rfilter));
            // growing systems
            data.put(GrowingSystem.class.getSimpleName(), reportService.getGrowingSystemNameToId(rfilter));
            // report regional
            data.put(ReportRegional.class.getSimpleName(), reportService.getReportRegionalNameToId(rfilter));

            jsonData = data;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load report-growing-system-all-creation-context", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
