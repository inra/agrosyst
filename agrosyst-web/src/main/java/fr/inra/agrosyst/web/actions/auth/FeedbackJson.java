package fr.inra.agrosyst.web.actions.auth;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.referential.FeedbackCategory;
import fr.inra.agrosyst.api.services.users.FeedbackContext;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serial;
import java.util.List;

/**
 * Cette action récupère le formulaire de feedback (en json) et l'envoi par mail.
 */
@Getter
@Setter
public class FeedbackJson extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -6415308969543420930L;

    private static final Log LOGGER = LogFactory.getLog(FeedbackJson.class);

    protected transient UserService userService;
    protected transient String clientAppVersion;

    protected transient String feedback;

    protected transient FeedbackCategory feedbackCategory;
    
    protected transient List<Sector> sectorTypes;
    
    protected transient TypeDEPHY dephyType;
    
    protected transient String itEmail;
    
    protected transient boolean isNotNetworkIr;

    protected transient String env;

    protected transient String location;

    protected transient String locationTitle;

    protected transient String referer;

    protected transient String requested;

    /** Base 64 url encoded data (png format). */
    protected transient String screenshot;

    protected transient String attachment;

    protected transient String attachmentName;

    protected transient String attachmentType;

    public void setIsNotNetworkIr(boolean notNetworkIr) {
        isNotNetworkIr = notNetworkIr;
    }
    
    @Override
    public String execute() throws Exception {
        String envInfo = Strings.nullToEmpty(env);
        envInfo += "Adresse IP : " + servletRequest.getRemoteAddr() + "\n";
        envInfo += "Session ID : " + getSessionId() + "\n";
        envInfo += "Version cliente : " + clientAppVersion + "\n";
        envInfo += "Version web : " + getConfig().getApplicationVersion() + "\n";

        byte[] imgData = convertBase64ToByteArray(screenshot, true);
        FeedbackContext feedbackContext = new FeedbackContext(sectorTypes, dephyType, feedbackCategory, feedback, itEmail, isNotNetworkIr, imgData);
        feedbackContext.addTechnicalDetails(envInfo, location, locationTitle, requested, referer);

        byte[] attachmentData = convertBase64ToByteArray(attachment, false);
        if (attachmentData != null) {
            feedbackContext.addAttachment(attachmentData, attachmentName, attachmentType);
        }

        jsonData = userService.sendFeedback(feedbackContext);
        if (FeedbackContext.FeedbackStatus.SUCCESS.equals(feedbackContext.getFeedbackStatus()) ||
                FeedbackContext.FeedbackStatus.NOT_SEND.equals(feedbackContext.getFeedbackStatus())
        ) {
            return SUCCESS;
        } else {
            return ERROR;
        }
    }
    
    protected byte[] convertBase64ToByteArray(String fullBase64, boolean isImage) {
        byte[] result = null;
        if (fullBase64 != null) {

            try {
                // extract base 64 data
                String base64Data = fullBase64.substring(fullBase64.indexOf(",") + 1);
                result = Base64.decodeBase64(base64Data);

                if (isImage) {
                    // convert to image
                    try (ByteArrayInputStream is = new ByteArrayInputStream(result);
                         ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                        BufferedImage image = ImageIO.read(is);
                        // convert image to byte array
                        ImageIO.write(image, "jpeg", os);
                        result = os.toByteArray();
                    }
                }

            } catch (Exception ex) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Can't convert base64 data to " + (isImage ? "image" : "file"), ex);
                }
            }
        }
        return result;
    }
}
