package fr.inra.agrosyst.web.converters;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.conversion.impl.NumberConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.conversion.TypeConversionException;

import java.lang.reflect.Member;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Map;

/**
 * this class convert a double and integer with local format or standard format
 * To use this converter, add the following lines in your
 * <code>classpath:struts-conversion.properties</code> file :
 *
 * <pre>
 * java.lang.Double=org.nuiton.web.struts2.converters.LocalNumberConverter
 * java.lang.Integer=org.nuiton.web.struts2.converters.LocalNumberConverter
 * </pre>
 *
 * <code>classpath:struts.xml</code> file :
 *
 * <pre>
 * &lt;bean type="com.opensymphony.xwork2.conversion.impl.NumberConverter" class="org.nuiton.web.struts2.converters.LocalNumberConverter" scope="singleton"/&gt;
 * </pre>
 *
 * You may tune this converter to your needs by overriding some methods like {@link #formatNullValue()},
 * {@link #getMinDecimals()} or {@link #getMaxDecimals}
 *
 * @author Sylvain Bavencoff &lt;bavencoff@codelutin.com&gt;
 * @since 1.15
 */
public class LocalNumberConverter extends NumberConverter {

    /** {@link DecimalFormat} to use for parsing/formatting {@code int}s and {@link Integer} instances */
    protected DecimalFormat integerFormat;

    /** {@link DecimalFormat} to use for parsing/formatting {@code double}s and {@link Double} instances */
    protected DecimalFormat doubleFormat;

    protected DecimalFormat getIntegerFormat() {
        if (integerFormat == null) {
            integerFormat = new DecimalFormat("###,##0");
        }
        return integerFormat;
    }

    protected DecimalFormat getDoubleFormat() {
        if (doubleFormat == null) {
            String pattern = "###,##0." + StringUtils.repeat("0", getMinDecimals()) + StringUtils.repeat("#", getMaxDecimals() - getMinDecimals());
            doubleFormat = new DecimalFormat(pattern);
        }
        return doubleFormat;
    }

    @Override
    public Object convertValue(Map<String, Object> context, Object target, Member member, String propertyName, Object value, Class toType) {

        Object result;

        if (toType == String.class && (value instanceof Double || value instanceof Integer)) {
            result = convertToString(value);
        } else if (value instanceof String
                && (toType == Double.class || toType == double.class || toType == Integer.class || toType == int.class)) {
            result = convertToNumber((String) value, toType);
        } else {
            result = super.convertValue(context, target, member, propertyName, value, toType);
        }

        return  result;

    }

    protected String convertToString(Object value) {
        String string;

        if (value == null) {
            // FIXME brendan 27/01/14 this is dead code, already lost 1 hour with it
            string = formatNullValue();
        } else if (value instanceof Integer) {
            string = getIntegerFormat().format(value);
        } else {
            string = getDoubleFormat().format(value);
        }

        return string;

    }

    protected Number convertToNumber(String string, Class toType) {
        Number parsedValue;

        if (isNullValue(string)) {

            if (toType.isPrimitive()) {
                if (toType == double.class) {
                    parsedValue = 0.;
                } else {
                    parsedValue = 0;
                }
            } else {
                parsedValue = null;
            }

        } else {

            char groupingSeparator = getIntegerFormat().getDecimalFormatSymbols().getGroupingSeparator();

            if (Character.isSpaceChar(groupingSeparator)) {
                string = string.replaceAll(" ", groupingSeparator + "");
            }

            try {

                if (toType == Double.class || toType == double.class) {
                    string = string.replace('.', getDoubleFormat().getDecimalFormatSymbols().getDecimalSeparator());
                    parsedValue = getDoubleFormat().parse(string).doubleValue();
                } else {
                    parsedValue = getIntegerFormat().parse(string).intValue();
                }

            } catch (ParseException | NumberFormatException e) {
                throw new TypeConversionException("unable to parse " + string, e);
            }

        }

        return parsedValue;
    }

    /**
     * Result if formatted object is null.
     *
     * If you override this method, you probably must override
     * {@link #isNullValue(String)} too
     */
    protected String formatNullValue() {
        return StringUtils.EMPTY;
    }

    /**
     * Method to know if a given string should be interpreted as "no value".
     */
    protected boolean isNullValue(String string) {
        boolean isNullValue = StringUtils.isEmpty(string);
        return isNullValue;
    }

    protected int getMinDecimals() {
        return 0;
    }

    protected int getMaxDecimals() {
        return 3;
    }

}
