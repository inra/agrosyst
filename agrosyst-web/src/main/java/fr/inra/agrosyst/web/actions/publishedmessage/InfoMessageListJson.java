package fr.inra.agrosyst.web.actions.publishedmessage;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.history.Message;
import fr.inra.agrosyst.api.services.history.MessageFilter;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Created by davidcosse on 14/10/14.
 */
public class InfoMessageListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(InfoMessageListJson.class);

    @Serial
    private static final long serialVersionUID = 1L;

    protected transient String filter;
    
    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String execute() throws Exception {
        try {
            MessageFilter messageFilter = getGson().fromJson(filter, MessageFilter.class);
            writeListNbElementByPage(Message.class, messageFilter.getPageSize());
            jsonData = messageService.getFilteredMessages(messageFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load info messages", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
