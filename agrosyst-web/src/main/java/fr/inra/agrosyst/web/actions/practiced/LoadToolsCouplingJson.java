package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.Objects;
import java.util.Set;

/**
 * @author David Cossé
 */
public class LoadToolsCouplingJson  extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -6352586060492623909L;

    private static final Log LOGGER = LogFactory.getLog(LoadToolsCouplingJson.class);

    protected transient EffectiveCropCycleService effectiveService;
    protected transient PracticedSystemService practicedSystemService;

    protected transient String agrosystInterventionTypes;

    protected transient String growingSystemId;
    protected transient String campaigns;

    protected transient String zoneTopiaId;
    
    public void setEffectiveService(EffectiveCropCycleService effectiveService) {
        this.effectiveService = effectiveService;
    }

    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }

    public void setGrowingSystemId(String growingSystemId) {
        this.growingSystemId = growingSystemId;
    }

    public void setCampaigns(String campaigns) {
        this.campaigns = campaigns;
    }

    public void setZoneTopiaId(String zoneTopiaId) {
        this.zoneTopiaId = zoneTopiaId;
    }

    @Override
    public String execute() {
        try {
            Set<AgrosystInterventionType> agrosystInterventionTypeSet = getAgrosystInterventionTypesSet(this.agrosystInterventionTypes);
            if (Strings.isNullOrEmpty(zoneTopiaId)) {
                jsonData = practicedSystemService.getToolsCouplingModelForInterventionTypes(growingSystemId, campaigns, agrosystInterventionTypeSet);
            } else {
                jsonData = effectiveService.getToolsCouplingModelForInterventionTypes(zoneTopiaId, agrosystInterventionTypeSet);
            }
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load tools copling for (growingSystemId: '%s', campaigns: '%s'),(zoneTopiaId: '%s'),  agrosystInterventionTypes '%s':",
                        growingSystemId, campaigns, zoneTopiaId, agrosystInterventionTypes), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }


    public void setAgrosystInterventionTypes(String json) {
        this.agrosystInterventionTypes = json;
    }

    protected Set<AgrosystInterventionType> getAgrosystInterventionTypesSet(String json) {
        Set<AgrosystInterventionType> result = null;
        if (StringUtils.isNotBlank(json)) {
            Type type = new TypeToken<Set<AgrosystInterventionType>>() {}.getType();
            result = getGson().fromJson(json, type);
            result.removeIf(Objects::isNull);
        }
        return result;
    }

}
