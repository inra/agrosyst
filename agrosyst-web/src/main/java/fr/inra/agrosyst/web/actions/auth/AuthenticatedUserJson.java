package fr.inra.agrosyst.web.actions.auth;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.Serial;

/**
 *
 */
public class AuthenticatedUserJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(AuthenticatedUserJson.class);
    @Serial
    private static final long serialVersionUID = 4566376502470233663L;
    
    protected transient UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute() throws Exception {
        try {
            AuthenticatedUser loginResult = authenticationService.reloadAuthenticatedUser();
            if (authorizationService.isInMaintenanceMode() && !authorizationService.isAdminFromUserId(loginResult.getTopiaId())) {
                jsonData = authorizationService.getMaintenanceModeMessage();
                httpCode = HttpServletResponse.SC_UNAUTHORIZED;
                return ERROR;
            }

            applicationContext.writeAuthenticationCookie(loginResult, getCookieHelper());
            jsonData = loginResult;
            return SUCCESS;

        } catch(Exception e) {
            jsonData = "L'utilisateur n'est pas actif";
            httpCode = HttpServletResponse.SC_UNAUTHORIZED;
            return ERROR;
        }
    }
}
