package fr.inra.agrosyst.web.validators;

/*
 * #%L
 * Nuiton Validator
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.validator.ValidationException;
import fr.inra.agrosyst.api.utils.DataValidator;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validator for French SIRET numbers
 *
 * Siret can be in:
 * <ul>
 * <li>String format: "44211670300038"</li>
 * <li>long, int: 44211670300038</li>
 * <li>Array or Collection of something: [4,4,2,1,1,6,7,0,,3,0,0,0,3,8] or ["442","116","703", "0003", "8"]</li>
 * </ul>
 *
 * @author Jean Couteau - couteau@codelutin.com
 * @since 2.3
 * Validation do the Luhn checksum too
 */
public class FrenchSiretFieldValidator extends NuitonFieldValidatorSupport {

    //SIRET_REGEXP = "[0-9]{3}[\\s]?[0-9]{3}[\\s]?[0-9]{3}[\\s]?[0-9]{5}";
    protected static final Pattern p = Pattern.compile(DataValidator.SIRET_REGEX);

    @Override
    public void validateWhenNotSkip(Object object) throws ValidationException {

        String fieldName = getFieldName();
        Object value = getFieldValue(fieldName, object);

        if (value == null) {
            // no value defined
            return;
        }
        StringBuilder siret;

        if (value.getClass().isArray()) {
            // le siret est stocker dans un tableau, par exemple un byte[]
            siret = new StringBuilder();
            for (int i = 0; i < Array.getLength(value); i++) {
                siret.append(Array.get(value, i));
            }
        } else if (value instanceof Collection<?>) {
            // le siret est stocker dans une collection,
            // ca doit pas arriver souvent :D, mais autant le gerer
            siret = new StringBuilder();
            for (Object o : (Collection<?>) value) {
                siret.append(o);
            }
        } else {
            // sinon dans tous les autres cas (String, int, long, BigInteger ...)
            // on prend le toString
            siret = new StringBuilder(String.valueOf(value));
        }
    
        String siretValue = siret.toString();
        if (StringUtils.isBlank(siretValue)) {
            // no value defined
            return;
        }

        // Remove any space
        siretValue = StringUtils.deleteWhitespace(siretValue);

        Matcher m = p.matcher(siretValue);
        if (!m.matches() || !DataValidator.luhnChecksum(siretValue)) {
            addFieldError(fieldName, object);
        }
    }

    @Override
    public String getValidatorType() {
        return "frenchSiret";
    }

}
