package fr.inra.agrosyst.web.actions.admin;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class AdminPerformancesJson extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 3543659606743731409L;
    
    private PerformanceService performanceService;
    
    protected String performanceId;
    
    @Override
    public String execute() throws Exception {
        if (authorizationService.isAdmin()) {
            jsonData = performanceService.getDbPerformancesStatistics(performanceId);
        } else {
            throw new AgrosystAccessDeniedException();
        }
        return SUCCESS;
    }
    
    public void setPerformanceService(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }
    
    public void setPerformanceId(String performanceId) {
        this.performanceId = performanceId;
    }
}
