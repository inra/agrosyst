package fr.inra.agrosyst.web.actions.admin;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.referential.ReferentialService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MatchFrenchAmmForEuropeanProducts extends AbstractAdminAction {

    private static final Log LOGGER = LogFactory.getLog(MatchFrenchAmmForEuropeanProducts.class);

    protected transient ReferentialService referentialService;
    protected File inputFile;
    protected InputStream resultInputStream;

    @Override
    @Action("match-french-amm-for-european-products-input")
    public String input() throws Exception {
        checkIsAdmin();
        return super.input();
    }

    @Override
    @Action(results= {@Result(type="stream", params={"contentType", "text/csv", "inputName", "resultInputStream" })})
    public String execute() throws Exception {
        checkIsAdmin();
        try (FileInputStream stream = new FileInputStream(inputFile)) {
            resultInputStream = referentialService.matchMaaWithFrenchMaa(stream);
        }
        return SUCCESS;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    public InputStream getResultInputStream() {
        return resultInputStream;
    }
}
