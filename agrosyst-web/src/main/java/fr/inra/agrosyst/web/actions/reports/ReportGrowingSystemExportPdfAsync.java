package fr.inra.agrosyst.web.actions.reports;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.report.ReportExportOption;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Classe qui permet de déclencher un export PDF asynchrone des bilans de campagne (échelle système de culture).
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class ReportGrowingSystemExportPdfAsync extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = 9160804528455812557L;

    protected transient ReportService reportService;

    protected final transient ReportExportOption exportOptions = new ReportExportOption();

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public ReportExportOption getExportOptions() {
        return exportOptions;
    }

    // delegation because not specific to export
    public void setReportGrowingSystemIds(String reportGrowingSystemIds) {
        getExportOptions().setReportGrowingSystemIds(getGson().fromJson(reportGrowingSystemIds, List.class));
    }

    public void setReportGrowingSystemSectionsJson(String reportGrowingSystemSectionsJson) {
        Map<String, Collection<String>> reportGrowingSystemSections = getGson().fromJson(reportGrowingSystemSectionsJson, Map.class);
        reportGrowingSystemSections.entrySet().removeIf(entry -> CollectionUtils.isEmpty(entry.getValue()));
        getExportOptions().setReportGrowingSystemSections(reportGrowingSystemSections);
    }

    @Override
    public String execute() {
        for (String reportGrowingSystemId : exportOptions.getReportGrowingSystemIds()) {
            authorizationService.checkReportGrowingSystemReadable(reportGrowingSystemId);
        }
        reportService.exportPdfReportGrowingSystemsAsync(exportOptions);

        return SUCCESS;
    }

}
