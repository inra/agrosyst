package fr.inra.agrosyst.web.actions.admin;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.services.common.EmailService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.Serial;

/**
 * @author Kevin Morin
 */
@Getter
@Setter
@Results({
        @Result(type = "redirectAction", name = "input", params = {"namespace", "/generic", "actionName", "generic-entities-list", "genericClassName", "${genericClassName}"})
})
public class RunMaaImports extends AbstractAdminAction {

    private static final Log LOGGER = LogFactory.getLog(RunMaaImports.class);
    public static final int MAX_DISPLAYED_MESSAGES = 20;
    @Serial
    private static final long serialVersionUID = -716182286563414398L;
    
    protected transient ImportService importService;
    protected transient EmailService emailService;
    protected transient DomainService domainService;

    protected int campagne;
    protected Integer numeroAmmIdMetier;

    protected String genericClassName;

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"namespace", "/generic", "actionName", "generic-entities-list", "genericClassName", "${genericClassName}"}),
            @Result(name = ERROR, type = "redirectAction", params = {"namespace", "/generic", "actionName", "generic-entities-list", "genericClassName", "${genericClassName}", "importFileError", "false"})
    })
    public String execute() throws Exception {
        checkIsAdmin();
        importService.importMAAReferentialsFromApiAsync(campagne, numeroAmmIdMetier);
        return SUCCESS;
    }
}
