package fr.inra.agrosyst.web.actions.networks;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.services.network.NetworkConnectionDto;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.network.NetworkGraph;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.network.SmallNetworkDto;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.List;
import java.util.Set;

/**
 * Affichage de la liste des réseaux et du graphique des réseaux.
 *
 * @author cosse
 */
public class NetworksList extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 4474047833187143193L;

    protected transient NetworkService networkService;
    
    protected PaginationResult<Network> networks;

    protected NetworkFilter networkFilter;

    protected NetworkGraph networksGraph;
    
    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }
    
    @Override
    public String execute() throws Exception {
        NavigationContext navigationContext = getNavigationContext();
        networkFilter = new NetworkFilter();
        networkFilter.setNavigationContext(navigationContext);
        networkFilter.setActive(Boolean.TRUE);
        networkFilter.setPageSize(getListNbElementByPage(Network.class));

        networks = networkService.getFilteredNetworks(networkFilter);

        // FIXME configure this hard coded value
        // prevent too much data for admins
        if (networks.getCount() < getNetworkDisplayLimit()) {
            networksGraph = networkService.buildFullNetworkGraph();
        }

        return SUCCESS;
    }

    public PaginationResult<Network> getNetworks() {
        return networks;
    }

    public List<List<SmallNetworkDto>> getGraphNetworks() {
        return networksGraph == null ? null : networksGraph.getNetworks();
    }

    public Set<NetworkConnectionDto> getGraphConnections() {
        return networksGraph == null ? null : networksGraph.getConnections();
    }

    public NetworkFilter getNetworkFilter() {
        return networkFilter;
    }

    public long getNetworkDisplayLimit() {
        return 50;
    }
}
