package fr.inra.agrosyst.web.actions.user;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.security.UserRoleDto;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.nuiton.util.StringUtil;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.Serial;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UserAccount extends AbstractAgrosystAction implements Preparable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -6751756402587810749L;

    protected transient AgrosystWebApplicationContext context;

    protected transient UserService userService;

    protected Map<String, String[]> bannerMap;

    protected UserDto user;
    
    protected String password;
    
    protected String confirmPassword;

    protected List<UserRoleDto> userRoles;

    public void setContext(AgrosystWebApplicationContext context) {
        this.context = context;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void prepare() {

        UserDto currentUser = this.userService.getCurrentUser();

        user = new UserDto();
        // make copy to not modify direct object from session
        Binder<UserDto, UserDto> userDtoBinder = BinderFactory.newBinder(UserDto.class);
        userDtoBinder.copy(currentUser, user);

        userRoles = authorizationService.getUserRoles(user.getTopiaId());
    }

    public UserDto getUser() {
        // AThimel 26/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return java.util.Objects.requireNonNullElseGet(user, UserDto::new);
    }

    @Override
    @Action("user-account-input")
    public String input() {
        initForInput();
        return INPUT;
    }

    @Override
    protected void initForInput() {
        bannerMap = context.getBannersMap();
    }

    @Override
    public void validate() {
        
        String email = user.getEmail();
        if (Strings.isNullOrEmpty(email)) {
            addFieldError("user.email", getText("user-account-error-email-required"));
        } else if (!StringUtil.isEmail(email)){
            addFieldError("user.email", getText("user-account-error-email-invalid"));
        }

        if (userService.isEmailInUse(email, user.getTopiaId())) {
            addFieldError("user.email", getText("user-account-error-email-alreadyused"));
        }
        
        if (!Objects.equal(password, confirmPassword)) {
            addFieldError("confirmPassword", getText("user-account-error-password-differents"));
        }
        if (Strings.isNullOrEmpty((user.getFirstName()))) {
            addFieldError("authenticatedUser.firstName", getText("user-account-error-firstName-required"));
        }
        if (Strings.isNullOrEmpty((user.getLastName()))) {
            addFieldError("authenticatedUser.lastName", getText("user-account-error-lastName-required"));
        }
    
        if (StringUtils.isNotBlank(user.getItEmail())){
            if (!userService.isValidEmail(user.getItEmail())) {
                addFieldError("user.itEmail", getText("user-account-error-email-notIt"));
            }
        }
        
        if (hasErrors()) {
            initForInput();
        }
    }

    @Override
    @Action(results = {
            @Result(type = "redirectAction", params = {"actionName", "user-account-input"})})
    public String execute() {
        user = userService.updateUser(user, password);
        refreshAuthenticatedUser();
        notificationSupport.authenticatedUserSaved(user);
        return SUCCESS;
    }

    public Map<String, String[]> getBannerMap() {
        return bannerMap;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public List<UserRoleDto> getUserRoles() {
        return userRoles;
    }

    public Map<RoleType, String> getRoleTypes() {
        return getEnumAsMap(RoleType.values());
    }

    public List<Language> getLanguages() {
        return Arrays.asList(Language.values());
    }

    public boolean isToutesCampagnes(UserRoleDto role) {
        return role.getEntity().getIdentifier() != null && (
                role.getType() == RoleType.DOMAIN_RESPONSIBLE || role.getType() == RoleType.GROWING_PLAN_RESPONSIBLE || (
                        role.getType() == RoleType.GS_DATA_PROCESSOR && role.getEntity().getCampaign() == null
                )
        );
    }

    public boolean isRoleCampagne(UserRoleDto role) {
        return role.getEntity().getIdentifier() != null
                && role.getType() == RoleType.GS_DATA_PROCESSOR
                && role.getEntity().getCampaign() != null;
    }

}
