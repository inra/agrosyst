package fr.inra.agrosyst.web.actions;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.referential.FeedbackCategory;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import org.nuiton.i18n.I18n;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystLayoutData {

    protected boolean currentUserAnAdmin;
    protected boolean currentUserAnIsDataProcessor;
    protected String currentUserLastName;
    protected String currentUserFirstName;
    protected String currentUserBannerPath;
    protected String currentUserBannerMeta;
    protected String currentUserItEmail;
    protected Language currentUserLanguage;

    protected NavigationContext navigationContext;

    protected Map<Integer, String> campaigns;
    protected Map<String, String> networks;
    protected Map<String, String> domains;
    protected Map<String, String> growingPlans;
    protected Map<String, String> growingSystems;

    public void setCurrentUserAnAdmin(boolean currentUserAnAdmin) {
        this.currentUserAnAdmin = currentUserAnAdmin;
    }

    public void setCurrentUserAnIsDataProcessor(boolean currentUserAnIsDataProcessor) {
        this.currentUserAnIsDataProcessor = currentUserAnIsDataProcessor;
    }

    public void setCurrentUserLastName(String currentUserLastName) {
        this.currentUserLastName = currentUserLastName;
    }

    public void setCurrentUserFirstName(String currentUserFirstName) {
        this.currentUserFirstName = currentUserFirstName;
    }

    public boolean isCurrentUserAnAdmin() {
        return currentUserAnAdmin;
    }

    public boolean isCurrentUserAnIsDataProcessor() {
        return currentUserAnIsDataProcessor;
    }

    public String getCurrentUserLastName() {
        return currentUserLastName;
    }

    public String getCurrentUserFirstName() {
        return currentUserFirstName;
    }

    public String getCurrentUserBannerPath() {
        return currentUserBannerPath;
    }

    public void setCurrentUserBannerPath(String currentUserBannerPath) {
        this.currentUserBannerPath = currentUserBannerPath;
    }

    public String getCurrentUserBannerMeta() {
        return currentUserBannerMeta;
    }

    public void setCurrentUserBannerMeta(String currentUserBannerMeta) {
        this.currentUserBannerMeta = currentUserBannerMeta;
    }
    
    public String getCurrentUserItEmail() {
        return currentUserItEmail;
    }

    public void setCurrentUserItEmail(String currentUserItEmail) {
        this.currentUserItEmail = currentUserItEmail;
    }

    public Language getCurrentUserLanguage() {
        return currentUserLanguage;
    }

    public void setCurrentUserLanguage(Language currentUserLanguage) {
        this.currentUserLanguage = currentUserLanguage;
    }

    public void setNavigationContext(NavigationContext navigationContext) {
        this.navigationContext = navigationContext;
    }

    public void setCampaigns(Map<Integer, String> campaigns) {
        this.campaigns = campaigns;
    }

    public void setNetworks(Map<String, String> networks) {
        this.networks = networks;
    }

    public void setDomains(Map<String, String> domains) {
        this.domains = domains;
    }

    public void setGrowingPlans(Map<String, String> growingPlans) {
        this.growingPlans = growingPlans;
    }


    public void setGrowingSystems(Map<String, String> growingSystems) {
        this.growingSystems = growingSystems;
    }

    public int getCampaignsCount() {
        return navigationContext.getCampaignsCount();
    }

    public int getNetworksCount() {
        return navigationContext.getNetworksCount();
    }

    public int getDomainsCount() {
        return navigationContext.getDomainsCount();
    }

    public int getGrowingPlansCount() {
        return navigationContext.getGrowingPlansCount();
    }

    public int getGrowingSystemsCount() {
        return navigationContext.getGrowingSystemsCount();
    }

    public Map<Integer, String> getCampaigns() {
        return campaigns;
    }

    public Map<String, String> getNetworks() {
        return networks;
    }

    public Map<String, String> getDomains() {
        return domains;
    }

    public Map<String, String> getGrowingPlans() {
        return growingPlans;
    }

    public Map<String, String> getGrowingSystems() {
        return growingSystems;
    }
    
    protected List<FeedbackCategoryDto> getFeedbackCategories(FeedbackCategory... feedbackCategories) {
        Locale l = Optional.ofNullable(currentUserLanguage)
                .map(Language::getLocale)
                .orElse(AgrosystI18nService.getDefaultLocale());
        List<FeedbackCategoryDto> result = new ArrayList<>();
        Map<FeedbackCategory, String> res0 = AgrosystI18nService.getEnumAsMap(l,"FeedbackCategory", feedbackCategories);
        for (Map.Entry<FeedbackCategory, String> feedbackCategoryToTrad : res0.entrySet()) {
            String help = I18n.l(l, FeedbackCategory.class.getName() + "." + feedbackCategoryToTrad.getKey().name() + "#help" );
            FeedbackCategoryDto feedbackCategoryDto = new FeedbackCategoryDto(feedbackCategoryToTrad.getKey(), feedbackCategoryToTrad.getValue(), help);
            result.add(feedbackCategoryDto);
        }
        return result;
    }

    public List<FeedbackCategoryDto> getAgrosystFeedbackCategories() {
        return getFeedbackCategories(
                FeedbackCategory.FONCTIONNEMENT_AGROSYST,
                FeedbackCategory.CONSIGNES_SAISIES,
                FeedbackCategory.ETAT_LIEUX_SAISIES,
                FeedbackCategory.REFERENTIELS,
                FeedbackCategory.EVOLUTION,
                FeedbackCategory.GESTION_COMPTE,
                FeedbackCategory.DECALAGE_DONNEES,
                FeedbackCategory.LOCAL_INTRANTS,
                FeedbackCategory.EDAPLOS,
                FeedbackCategory.BUG
        );
    }

    public List<FeedbackCategoryDto> getIpmWorksFeedbackCategories() {
        return getFeedbackCategories(
                FeedbackCategory.FONCTIONNEMENT_AGROSYST,
                FeedbackCategory.CONSIGNES_SAISIES,
                FeedbackCategory.REFERENTIELS,
                FeedbackCategory.EVOLUTION,
                FeedbackCategory.GESTION_COMPTE,
                FeedbackCategory.BUG
        );
    }
    
    protected Map<Sector, String> getSectorTypes(Sector... sectors) {
        Locale l = Optional.ofNullable(currentUserLanguage)
                .map(Language::getLocale)
                .orElse(AgrosystI18nService.getDefaultLocale());
        return AgrosystI18nService.getEnumAsMap(l, null, sectors);
    }

    public Map<Sector, String> getAgrosystSectorTypes() {
        return getSectorTypes(Sector.values());
    }

    public Map<Sector, String> getIpmWorksSectorTypes() {
        return getSectorTypes(
                Sector.ARBORICULTURE,
                Sector.HORTICULTURE,
                Sector.MARAICHAGE,
                Sector.GRANDES_CULTURES,
                Sector.POLYCULTURE_ELEVAGE,
                Sector.VITICULTURE
        );
    }
    
    public Map<TypeDEPHY, String> getDephyTypes() {
        Locale l = Optional.ofNullable(currentUserLanguage)
                .map(Language::getLocale)
                .orElse(AgrosystI18nService.getDefaultLocale());
        return AgrosystI18nService.getEnumAsMap(l,null, TypeDEPHY.DEPHY_FERME, TypeDEPHY.DEPHY_EXPE, TypeDEPHY.NOT_DEPHY);
    }
    
    public String getItEmail() {
        return currentUserItEmail;
    }
}
