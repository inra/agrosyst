package fr.inra.agrosyst.web.actions.admin;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.async.AsyncService;
import fr.inra.agrosyst.api.services.async.ScheduledTaskDto;
import fr.inra.agrosyst.api.services.async.TaskFilter;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class AdminTaskJson extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 3961688540524235085L;
    
    private AsyncService asyncService;
    
    protected String filter;
    
    protected transient Boolean pagination = true;
    
    public void setPagination(Boolean pagination) {
        this.pagination = pagination;
    }
    
    public void setAsyncService(AsyncService asyncService) {
        this.asyncService = asyncService;
    }
    
    @Override
    public String execute() throws Exception {
        if (authorizationService.isAdmin()) {
            try {
                TaskFilter taskFilter = getGson().fromJson(filter, TaskFilter.class);
                if (!pagination) {
                    taskFilter.setAllPageSize();
                    taskFilter.setPage(0);
                } else {
                    writeListNbElementByPage(ScheduledTaskDto.class, taskFilter.getPageSize());
                }
                jsonData = asyncService.getRunningAndPendingTasks(taskFilter);
            } catch (Exception e) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Failed to load domains list", e);
                }
                return ERROR;
            }
        } else {
            throw new AgrosystAccessDeniedException();
        }
        
        return SUCCESS;
    }
    
    public void setFilter(String filter) {
        this.filter = filter;
    }
    
}
