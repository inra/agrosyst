package fr.inra.agrosyst.web.actions.referential;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.referential.ExportService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import lombok.Setter;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Setter
public abstract class ReferentialDownload extends AbstractAgrosystAction {

    protected transient ExportService exportService;

    @Override
    @Action(results= {@Result(type="stream", params={"contentType", "text/csv",
            "inputName", "inputStream", "contentDisposition", "attachment; filename=\"${filename}\"; filename*=UTF-8''${urifilename}"})})
    public String execute() throws Exception {
        return SUCCESS;
    }

    public abstract InputStream getInputStream();

    /**
     * For browser as Chrome that need URI encoded file name
     */
    public String getUrifilename() throws UnsupportedEncodingException {
        return URLEncoder.encode(getFilename(), StandardCharsets.UTF_8).replace("+", "%20");
    }

    public abstract String getFilename();
}
