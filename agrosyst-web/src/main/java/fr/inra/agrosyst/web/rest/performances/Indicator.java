package fr.inra.agrosyst.web.rest.performances;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.ActiveSubstance;
import fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses;
import fr.inra.agrosyst.api.entities.performance.Ift;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.MineralFertilization;
import fr.inra.agrosyst.api.entities.performance.OrganicFertilization;
import fr.inra.agrosyst.api.entities.performance.OrganicProduct;
import fr.inra.agrosyst.api.entities.performance.TotalFertilization;
import fr.inra.agrosyst.services.performance.indicators.IndicatorDirectMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorEquipmentsExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorFuelConsumption;
import fr.inra.agrosyst.services.performance.indicators.IndicatorGrossMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorIrrigation;
import fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorOrganicProducts;
import fr.inra.agrosyst.services.performance.indicators.IndicatorSemiNetMargin;
import fr.inra.agrosyst.services.performance.indicators.IndicatorSurfaceUTH;
import fr.inra.agrosyst.services.performance.indicators.IndicatorToolUsageTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTotalWorkTime;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTotalWorkforceExpenses;
import fr.inra.agrosyst.services.performance.indicators.IndicatorTransitCount;
import fr.inra.agrosyst.services.performance.indicators.IndicatorUTH;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorNumberOfMechanicalWeedingPassages;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorNumberOfPloughingPassages;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorNumberOfTCSPassages;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorTillageType;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.IndicatorUsageOfMechanicalWeeding;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorMineralFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorOrganicFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorTotalFertilization;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorDecomposedOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorActiveSubstanceAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCMR;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCMRUses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperFertilisationProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperPhytoProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperTotalProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorEnvironmentalRisk;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorEnvironmentalRiskUses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G1;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G2;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G3;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorHRI1_G4;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorLowRiskSubstances;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorNeonicotinoidsAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSoilAppliedHerbicidesAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSubstancesCandidateToSubstitution;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurFertilisationProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurPhytoProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorSulfurTotalProduct;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorTotalActiveSubstanceAmount;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorToxicUser;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorToxicUserUses;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;

import static fr.inra.agrosyst.web.rest.performances.IndicatorUtils.PRODUIT_ET_MARGES_PARAMETER;
import static fr.inra.agrosyst.web.rest.performances.MethodUtils.*;

enum Indicator {

    // IFT
    IFT_CHIMIQUE_TOTAL(Ift.TOTAL),
        IFT_CHIMIQUE_TOTAL_HTS(Ift.SUM_HTS),
        IFT_CHIMIQUE_TOTAL_HORS_HERBICIDE(Ift.HH),
        IFT_HERBICIDE(Ift.H),
        IFT_FONGICIDE(Ift.F),
        IFT_INSECTICIDE(Ift.I),
        IFT_TRAITEMENT_DE_SEMENCES(Ift.TS),
        IFT_AUTRES(Ift.A),
    IFT_BIOCONTROLE(Ift.BIO_CONTROL),
    // IFT not implemented
    IFT_CHIMIQUE_TOTAL_HORS_HERBICIDE_HTS, // not implemented
    IFT_TOTAL_AVEC_BIOCONTROLE, // not implemented

    //substances actives
    QSA_TOTALE(IndicatorTotalActiveSubstanceAmount.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    QSA_DANGER_ENVIRONNEMENT(IndicatorEnvironmentalRisk.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    QSA_TOXIQUES_UTILISATEUR(IndicatorToxicUser.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    QSA_CMR(IndicatorCMR.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    QSA_SUBSTANCES_CANDIDATES_SUBSTITUTION(IndicatorSubstancesCandidateToSubstitution.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    QSA_SUBSTANCES_FAIBLE_RISQUE(IndicatorLowRiskSubstances.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    QSA_CUIVRE_TOTAL(IndicatorCopperTotalProduct.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        QSA_CUIVRE_PHYTOSANITAIRE(IndicatorCopperPhytoProduct.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        QSA_CUIVRE_FERTILISATION(IndicatorCopperFertilisationProduct.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    QSA_SOUFRE_TOTAL(IndicatorSulfurTotalProduct.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        QSA_SOUFRE_PHYTOSANITAIRE(IndicatorSulfurPhytoProduct.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        QSA_SOUFRE_FERTILISATION(IndicatorSulfurFertilisationProduct.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    ENVIRONMENTAL_RISK_USES(IndicatorEnvironmentalRiskUses.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    TOXIC_USER_USES(IndicatorToxicUserUses.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    CMR_USES(IndicatorCMRUses.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
    HRI_1(IndicatorHRI1.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        HRI_1_G1(IndicatorHRI1_G1.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        HRI_1_G2(IndicatorHRI1_G2.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        HRI_1_G3(IndicatorHRI1_G3.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),
        HRI_1_G4(IndicatorHRI1_G4.class, SUBSTANCES_ACTIVES_METHODS_PARAMETER),

    //substances actives spécifiques
    // - Sélectionner tous les QSA Herbicides
        QSA_GLYPHOSATE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.GLYPHOSATE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_DICAMBA(IndicatorActiveSubstanceAmount.class, ActiveSubstance.DICAMBA, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_HERBICIDES_RACINAIRES(IndicatorSoilAppliedHerbicidesAmount.class, ActiveSubstance.HERBICIDES_RACINAIRES, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_ACLONIFEN(IndicatorActiveSubstanceAmount.class, ActiveSubstance.ACLONIFEN, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_BEFLUTAMID(IndicatorActiveSubstanceAmount.class, ActiveSubstance.BEFLUTAMID, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_CHLORTOLURON(IndicatorActiveSubstanceAmount.class, ActiveSubstance.CHLORTOLURON, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_DIFLUFENICAN(IndicatorActiveSubstanceAmount.class, ActiveSubstance.DIFLUFENICAN, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_DIMETHENAMID_P(IndicatorActiveSubstanceAmount.class, ActiveSubstance.DIMETHENAMID_P, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_FLORASULAM(IndicatorActiveSubstanceAmount.class, ActiveSubstance.FLORASULAM, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_FLUFENACET(IndicatorActiveSubstanceAmount.class, ActiveSubstance.FLUFENACET, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_ISOPROTURON(IndicatorActiveSubstanceAmount.class, ActiveSubstance.ISOPROTURON, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_ISOXABEN(IndicatorActiveSubstanceAmount.class, ActiveSubstance.ISOXABEN, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_METSULFURON(IndicatorActiveSubstanceAmount.class, ActiveSubstance.METSULFURON, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_PENDIMETHALIN(IndicatorActiveSubstanceAmount.class, ActiveSubstance.PENDIMETHALIN, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_PICOLINAFEN(IndicatorActiveSubstanceAmount.class, ActiveSubstance.PICOLINAFEN, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_PROPOXYCARBAZONE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.PROPOXYCARBAZONE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_PROSULFOCARBE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.PROSULFOCARBE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_S_METOLACHLORE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.S_METOLACHLORE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_SULFOSULFURON(IndicatorActiveSubstanceAmount.class, ActiveSubstance.SULFOSULFURON, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_TRIALLATE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.TRIALLATE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_TRIBENURON(IndicatorActiveSubstanceAmount.class, ActiveSubstance.TRIBENURON, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
    // - Sélectionner tous les QSA Fongicides
        QSA_BOSCALID(IndicatorActiveSubstanceAmount.class, ActiveSubstance.BOSCALID, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_BIXAFEN(IndicatorActiveSubstanceAmount.class, ActiveSubstance.BIXAFEN, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_FLUOPYRAM(IndicatorActiveSubstanceAmount.class, ActiveSubstance.FLUOPYRAM, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_MANCOZEB(IndicatorActiveSubstanceAmount.class, ActiveSubstance.MANCOZEB, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_TEBUCONAZOLE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.TEBUCONAZOLE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
    // - Sélectionner tous les QSA Insecticides
        QSA_LAMBDA_CYHALOTHRINE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.LAMBDA_CYHALOTHRINE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_PHOSMET(IndicatorActiveSubstanceAmount.class, ActiveSubstance.PHOSMET, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
        QSA_NEONICOTINOIDES(IndicatorNeonicotinoidsAmount.class, ActiveSubstance.NEONICOTINOIDES, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_ACETAMIPRIDE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.ACETAMIPRIDE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_CLOTHIANIDINE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.CLOTHIANIDINE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_IMIDACLOPRIDE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.IMIDACLOPRIDE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_THIAMETHOXAM(IndicatorActiveSubstanceAmount.class, ActiveSubstance.THIAMETHOXAM, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),
            QSA_THIACLOPRIDE(IndicatorActiveSubstanceAmount.class, ActiveSubstance.THIACLOPRIDE, SUBSTANCES_ACTIVES_SPECIFIQUES_METHODS_PARAMETER),

    // Recours aux moyens
    RECOURS_AUX_MOYENS_BIOLOGIQUES(IndicatorOrganicProducts.class, OrganicProduct.MOYENS_BIOLOGIQUES),
        RECOURS_AUX_MACRO_ORGANISMES(IndicatorOrganicProducts.class, OrganicProduct.MACRO_ORGANISMES),
        RECOURS_AUX_PRODUITS_BIOTIQUES_SANS_AMM(IndicatorOrganicProducts.class, OrganicProduct.BIOTIQUES_SANS_AMM),
        RECOURS_AUX_PRODUITS_ABIOTIQUES_SANS_AMM(IndicatorOrganicProducts.class, OrganicProduct.ABIOTIQUES_SANS_AMM),

    // Produis et marges
    PRODUIT_BRUT(PRODUIT_ET_MARGES_PARAMETER, PRODUITS_MARGES_METHODS_PARAMETER),
    MARGE_BRUTE(IndicatorGrossMargin.class, PRODUITS_MARGES_METHODS_PARAMETER),
    MARGE_SEMI_NETTE(IndicatorSemiNetMargin.class, PRODUITS_MARGES_METHODS_PARAMETER),
    MARGE_DIRECTE(IndicatorDirectMargin.class, PRODUITS_MARGES_METHODS_PARAMETER),

    // Charges
    CHARGES_OPERATIONELLES_TOTALES(IndicatorOperatingExpenses.class, CHARGES_METHODS_PARAMETER),
        CHARGES_OPERATIONELLES_SEMIS(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.SEMIS),
        CHARGES_OPERATIONELLES_FERTILISATION_MINERALE(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX),
        CHARGES_OPERATIONELLES_EPANDAGE_ORGANIQUE(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.EPANDAGES_ORGANIQUES),
        CHARGES_OPERATIONELLES_PHYTO_AVEC_AMM(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES),
        CHARGES_OPERATIONELLES_PHYTO_SANS_AMM(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.LUTTE_BIOLOGIQUE),
        CHARGES_OPERATIONELLES_TRAITEMENTS_DE_SEMENCE(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.TRAITEMENT_SEMENCE),
        CHARGES_OPERATIONELLES_IRRIGATION(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.IRRIGATION),
        CHARGES_OPERATIONELLES_SUBSTRATS(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.SUBSTRAT),
        CHARGES_OPERATIONELLES_POTS(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.POT),
        CHARGES_OPERATIONELLES_AUTRES(IndicatorDecomposedOperatingExpenses.class, CHARGES_METHODS_PARAMETER, DecomposedOperatingExpenses.AUTRE),
    CHARGES_DE_MECANISATION(IndicatorEquipmentsExpenses.class, CHARGES_METHODS_PARAMETER),
    CHARGES_DE_MAIN_OEUVRE_TOTALES(IndicatorTotalWorkforceExpenses.class, CHARGES_METHODS_PARAMETER),
        CHARGES_DE_MAIN_OEUVRE_TRACTORISTE(IndicatorMechanizedWorkforceExpenses.class, CHARGES_METHODS_PARAMETER),
        CHARGES_DE_MAIN_OEUVRE_MANUELLE(IndicatorManualWorkforceExpenses.class, CHARGES_METHODS_PARAMETER),

    // Strategie agronomique
    TYPE_DE_TRAVAIL_DU_SOL(IndicatorTillageType.class),
    NOMBRE_INTERVENTIONS_DE_LABOUR(IndicatorNumberOfPloughingPassages.class),
    NOMBRE_INTERVENTIONS_DE_TRAVAIL_DU_SOL_HORS_LABOUR(IndicatorNumberOfTCSPassages.class),
    RECOURS_AU_DESHERBAGE_MECANIQUE(IndicatorUsageOfMechanicalWeeding.class),
    NOMBRE_INTERVENTIONS_DE_DESHERBAGE_MECANIQUE(IndicatorNumberOfMechanicalWeedingPassages.class),

    // Fertilisation NPK
    N_TOTAL(IndicatorTotalFertilization.class, TotalFertilization.N),
        N_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.N),
        N_ORGANIQUE(IndicatorOrganicFertilization.class, OrganicFertilization.N),
    P2O5_TOTAL(IndicatorTotalFertilization.class, TotalFertilization.P2O5),
        P2O5_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.P2O5),
        P2O5_ORGANIQUE(IndicatorOrganicFertilization.class, OrganicFertilization.P2O5),
    K2O_TOTAL(IndicatorTotalFertilization.class, TotalFertilization.K2O),
        K2O_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.K2O),
        K2O_ORGANIQUE(IndicatorOrganicFertilization.class, OrganicFertilization.K2O),

    // Fertilisation autre
    CA_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.CA),
    CAO_ORGANIQUE(IndicatorOrganicFertilization.class, OrganicFertilization.CAO),
    MGO_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.MGO),
    MGO_ORGANIQUE(IndicatorOrganicFertilization.class, OrganicFertilization.MGO),
    SO3_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.SO3),
    S_ORGANIQUE(IndicatorOrganicFertilization.class, OrganicFertilization.S),
    B_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.B),
    CU_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.CU),
    FE_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.FE),
    MN_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.MN),
    MO_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.MO),
    NA2O_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.NA2O),
    ZN_MINERAL(IndicatorMineralFertilization.class, MineralFertilization.ZN),

    // Temps de travail
    TEMPS_DE_TRAVAIL_TOTAL(IndicatorTotalWorkTime.class, TEMPS_DE_TRAVAIL_METHODS_PARAMETER),
    TEMPS_DE_TRAVAIL_MECANISE(IndicatorMechanizedWorkTime.class, TEMPS_DE_TRAVAIL_METHODS_PARAMETER),
    TEMPS_UTILISATION_DU_MATERIEL(IndicatorToolUsageTime.class, TEMPS_DE_TRAVAIL_METHODS_PARAMETER),
    TEMPS_DE_TRAVAIL_MANUEL(IndicatorManualWorkTime.class, TEMPS_DE_TRAVAIL_METHODS_PARAMETER),
    NOMBRE_DE_PASSAGES(IndicatorTransitCount.class, TEMPS_DE_TRAVAIL_METHODS_PARAMETER),

    // Performance sociale et consommation
    CONSOMMATION_DE_CARBURANT(IndicatorFuelConsumption.class),
    CONSOMMATION_EAU(IndicatorIrrigation.class),
    SURFACE_PAR_UHT(IndicatorSurfaceUTH.class),
    NOMBRE_UHT_NECESSAIRES(IndicatorUTH.class)
    ;

    final Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz;
    final Function<Set<Method>, List<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>>> clazzResolver;
    final BiConsumer<Set<Method>, IndicatorFilter> handleParameters;
    final DecomposedOperatingExpenses doe;
    final Ift ift;
    final MineralFertilization mineralFertilization;
    final OrganicFertilization organicFertilization;
    final TotalFertilization totalFertilization;
    final ActiveSubstance activeSubstance;
    final OrganicProduct organicProduct;

    Indicator() {
        this.clazz = null;
        this.clazzResolver = null;
        this.handleParameters = null;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = null;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz, BiConsumer<Set<Method>, IndicatorFilter> handleParameters) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = handleParameters;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(
            Function<Set<Method>, List<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>>> clazzResolver,
            BiConsumer<Set<Method>, IndicatorFilter> handleParameters) {
        this.clazz = null;
        this.clazzResolver = clazzResolver;
        this.handleParameters = handleParameters;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz, BiConsumer<Set<Method>, IndicatorFilter> handleParameters, DecomposedOperatingExpenses doe) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = handleParameters;
        this.doe = doe;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Ift ift) {
        this.clazz = null;
        this.clazzResolver = null;
        this.handleParameters = null;
        this.doe = null;
        this.ift = ift;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz, MineralFertilization mineralFertilization) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = null;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = mineralFertilization;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz, OrganicFertilization organicFertilization) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = null;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = organicFertilization;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz, TotalFertilization totalFertilization) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = null;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = totalFertilization;
        this.activeSubstance = null;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz, ActiveSubstance activeSubstance, BiConsumer<Set<Method>, IndicatorFilter> handleParameters) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = handleParameters;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = activeSubstance;
        this.organicProduct = null;
    }

    Indicator(Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator> clazz, OrganicProduct organicProduct) {
        this.clazz = clazz;
        this.clazzResolver = null;
        this.handleParameters = null;
        this.doe = null;
        this.ift = null;
        this.mineralFertilization = null;
        this.organicFertilization = null;
        this.totalFertilization = null;
        this.activeSubstance = null;
        this.organicProduct = organicProduct;
    }

    public Optional<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>> getClazz() {
        return Optional.ofNullable(clazz);
    }

    public Optional<Function<Set<Method>, List<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>>>> getClazzResolver() {
        return Optional.ofNullable(clazzResolver);
    }

    public Optional<BiConsumer<Set<Method>, IndicatorFilter>> getHandleParameters() {
        return Optional.ofNullable(handleParameters);
    }

    public Optional<DecomposedOperatingExpenses> getDoe() {
        return Optional.ofNullable(doe);
    }

    public Optional<Ift> getIft() {
        return Optional.ofNullable(ift);
    }

    public Optional<MineralFertilization> getMineralFertilization() {
        return Optional.ofNullable(mineralFertilization);
    }

    public Optional<OrganicFertilization> getOrganicFertilization() {
        return Optional.ofNullable(organicFertilization);
    }

    public Optional<TotalFertilization> getTotalFertilization() {
        return Optional.ofNullable(totalFertilization);
    }

    public Optional<ActiveSubstance> getActiveSubstance() {
        return Optional.ofNullable(activeSubstance);
    }

    public Optional<OrganicProduct> getOrganicProduct() {
        return Optional.ofNullable(organicProduct);
    }
}
