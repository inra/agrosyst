package fr.inra.agrosyst.web.actions.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.security.UserRoleDto;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class SaveEntityRoles extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -7507420510086126834L;

    private static final Log LOGGER = LogFactory.getLog(SaveEntityRoles.class);

    protected transient RoleType roleType;

    protected transient String entityCode;

    protected transient List<UserRoleDto> roles;

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public void setRolesJson(String rolesJson) {
        Type type = new TypeToken<List<UserRoleDto>>() {
        }.getType();
        this.roles = getGson().fromJson(rolesJson, type);
    }

    @Override
    public String execute() throws Exception {
        try {
            authorizationService.saveEntityUserRoles(roleType, entityCode, roles);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to save roles roleType: '%s' entityCode:'%s'",
                        roleType, entityCode), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

}
