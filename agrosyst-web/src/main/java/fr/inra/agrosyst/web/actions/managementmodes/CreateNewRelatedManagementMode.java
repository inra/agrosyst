package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

/**
 * Created by davidcosse on 25/01/14.
 */
public class CreateNewRelatedManagementMode extends AbstractAgrosystAction {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = 2969368449296333610L;

    protected transient ManagementModeService managementModeService;

    protected String growingSystemTopiaId;

    protected ManagementModeCategory category;

    protected String changeReasonFromPlanned;

    protected String mainChangesFromPlanned;

    protected ManagementMode managementMode;

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "management-modes-edit-input", "growingSystemTopiaId", "${growingSystemTopiaId}", "managementModeTopiaId", "${managementMode.topiaId}"})})
    public String execute() throws Exception {
        Preconditions.checkState(category != null);
        Preconditions.checkState(!Strings.isNullOrEmpty(growingSystemTopiaId));

        ManagementModeCategory newManagementModeCategory = category == ManagementModeCategory.PLANNED ? ManagementModeCategory.OBSERVED : ManagementModeCategory.PLANNED;
        managementMode = managementModeService.copyManagementMode(growingSystemTopiaId, newManagementModeCategory, mainChangesFromPlanned, changeReasonFromPlanned);
        notificationSupport.newManagementModeCreated(managementMode);
        return SUCCESS;
    }

    public String getGrowingSystemTopiaId() {
        return growingSystemTopiaId;
    }

    public void setManagementModeService(ManagementModeService managementModeService) {
        this.managementModeService = managementModeService;
    }

    public void setGrowingSystemTopiaId(String growingSystemTopiaId) {
        this.growingSystemTopiaId = growingSystemTopiaId;
    }

    public void setCategory(ManagementModeCategory category) {
        this.category = category;
    }

    public void setChangeReasonFromPlanned(String changeReasonFromPlanned) {
        this.changeReasonFromPlanned = changeReasonFromPlanned;
    }

    public void setMainChangesFromPlanned(String mainChangesFromPlanned) {
        this.mainChangesFromPlanned = mainChangesFromPlanned;
    }

    public ManagementMode getManagementMode() {
        return managementMode;
    }

    public void setManagementMode(ManagementMode managementMode) {
        this.managementMode = managementMode;
    }


}
