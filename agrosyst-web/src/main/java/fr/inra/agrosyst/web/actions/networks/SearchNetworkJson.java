package fr.inra.agrosyst.web.actions.networks;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.Set;

/**
 * Search network based on network name and specifics options.
 */
public class SearchNetworkJson extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = 4654758167246992232L;

    private static final Log LOGGER = LogFactory.getLog(SearchNetworkJson.class);

    protected transient NetworkService networkService;

    /** Network serch term. */
    protected transient String term;

    /** The identifiers of the networks to exclude. */
    protected transient Set<String> exclusions;

    /** The identifier of the current network (in case editing a network). */
    protected transient String selfNetworkId;

    /** If the current request should consider only networks without SdC children or not. */
    protected transient boolean onNetworkEdition;

    /** Only propose network where user is responsible of. */
    protected transient boolean onlyResponsibleNetwork;
    
    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public void setExclusions(Set<String> exclusions) {
        this.exclusions = exclusions;
    }

    public void setSelfNetworkId(String selfNetworkId) {
        this.selfNetworkId = selfNetworkId;
    }

    public void setOnNetworkEdition(boolean onNetworkEdition) {
        this.onNetworkEdition = onNetworkEdition;
    }

    public void setOnlyResponsibleNetwork(boolean onlyResponsibleNetwork) {
        this.onlyResponsibleNetwork = onlyResponsibleNetwork;
    }

    @Override
    public String execute() {
        try {
            jsonData = networkService.searchNameFilteredActiveNetworks(term, 10, exclusions, selfNetworkId,
                    onNetworkEdition, onlyResponsibleNetwork);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to find network ", ex);
            }
            return ERROR;
        }
        return SUCCESS;
    }

}
