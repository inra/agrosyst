/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemDto;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemFilter;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Liste des bilans de campagne / échelle Système de culture.
 */
public class ReportGrowingSystemsListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(ReportGrowingSystemsListJson.class);

    @Serial
    private static final long serialVersionUID = 8357676200729541321L;

    protected transient ReportService reportService;

    protected transient String filter;
    
    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public String execute() {
        try {
            ReportGrowingSystemFilter rfilter = getGson().fromJson(filter, ReportGrowingSystemFilter.class);
            writeListNbElementByPage(ReportGrowingSystemDto.class, rfilter.getPageSize());
            NavigationContext navigationContext = getNavigationContext();
            rfilter.setNavigationContext(navigationContext);
            jsonData = reportService.getAllReportGrowingSystem(rfilter);

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load report growing system list", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }
}
