package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCrop;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleImpl;
import fr.inra.agrosyst.api.entities.referential.BioAgressorParentType;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.referential.GroupeCibleDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import fr.inra.agrosyst.web.actions.domains.MinimalDomainDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Decision rule edit action.
 *
 * @author Eric Chatellier
 */
@Getter
@Setter
public class DecisionRulesEdit extends AbstractAgrosystAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(DecisionRulesEdit.class);
    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = -6937262808324248162L;

    protected transient ManagementModeService managementModeService;
    protected transient ReferentialService referentialService;
    protected transient DomainService domainService;

    /**
     * Decision rule id to edit.
     */
    protected String decisionRuleTopiaId;

    /**
     * Decision rule entity to edit.
     */
    protected DecisionRule decisionRule;

    /**
     * domain list (only for create mode).
     */
    protected List<Domain> domains;

    /**
     * Domain id (create mode).
     */
    protected String domainCode;

    protected String domainName;

    /**
     * Liste des cultures associées au système de culture sélectionné.
     */
    protected List<CroppingPlanEntry> croppingPlanEntries;

    /**
     * Selected cropping plan entry code.
     */
    protected List<String> croppingCodes;

    protected String decisionRuleCropsJson;

    protected List<GroupeCibleDTO> groupesCibles;

    /**
     * Sous ensemble de bioagresseur pour le {@code bioAgressorType} sélectionné.
     */
    protected List<RefBioAgressor> bioAgressors;

    /**
     * Selected code groupe cible.
     */
    protected String codeGroupeCibleMaa;
    
    /**
     * Selected bio agressor topiaId.
     */
    protected String bioAgressorTopiaId;

    /**
     * Code of the decision rule
     */
    protected String decisionRuleCode;

    /**
     * Related Decision Rules
     */
    protected List<DecisionRule> relatedDecisionRules;
    
    public DecisionRule getDecisionRule() {
        // EChatellier 27/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(decisionRule, DecisionRuleImpl::new);
    }

    @Override
    public void prepare() {
        if (StringUtils.isBlank(decisionRuleTopiaId)) {
            if (StringUtils.isBlank(decisionRuleCode)) {
                // Cas de création d'une règle de décision
                decisionRule = managementModeService.newDecisionRule();
            } else {
                // Cas ou l'on vient de la liste des règles de décision
                decisionRule = managementModeService.getLastDecisionRuleVersion(decisionRuleCode);
                decisionRuleTopiaId = decisionRule.getTopiaId();
            }
        } else {
            // Cas d'une mise à jour d'une règle de décision, on récupère une copie afin de ne pas modifier l'original
            final Optional<DecisionRule> optionalDecisionRule = managementModeService.getDecisionRule(decisionRuleTopiaId);
            this.decisionRule = optionalDecisionRule.orElse(managementModeService.newDecisionRule());
        }

        if (decisionRule.isPersisted()) {
            activated = managementModeService.isDecisionRuleActive(decisionRule);
        }
    }

    /**
     * Initialisation de certaines variables pour le premier appel de la page.
     */
    @Override
    @Action("decision-rules-edit-input")
    public String input() throws Exception {

        if (!Strings.isNullOrEmpty(decisionRuleTopiaId)) {
            authorizationService.checkDecisionRuleReadable(decisionRuleTopiaId);
            readOnly = !authorizationService.isDecisionRuleWritable(decisionRuleTopiaId);
        }
        if (readOnly) {
            notificationSupport.decisionRuleNotWritable();
        }

        if (decisionRule.getBioAgressor() != null) {
            bioAgressorTopiaId = decisionRule.getBioAgressor().getTopiaId();
        }

        initForInput();

        return INPUT;
    }

    /**
     * Initialisation des listes ou autres données à chaque affichage (premier/erreurs).
     */
    @Override
    protected void initForInput() {

        // select combo box
        if (getDecisionRule().isPersisted()) {
            // TODO DCossé 31/01/14 ne pourrais-t-on pas remonter uniquement le nom du domaine ?
            DomainDto domain = domainService.getActiveOrUnactiveDomainForCode(getDecisionRule().getDomainCode());
            domainName = domain.getName();

            croppingPlanEntries = managementModeService.getDomainCodeCroppingPlanEntries(getDecisionRule().getDomainCode());
            relatedDecisionRules = managementModeService.getRelatedDecisionRules(getDecisionRule().getCode());

        } else {
            DomainFilter domainFilter = new DomainFilter();
            NavigationContext navigationContext = getNavigationContext();
            domainFilter.setNavigationContext(navigationContext);
            domainFilter.setActive(Boolean.TRUE);
            domainFilter.setAllPageSize();
            domains = domainService.getActiveWritableDomainsForDecisionRuleCreation(domainFilter);

            croppingPlanEntries = null;
        }

        BioAgressorType bioAgressorType = decisionRule.getBioAgressorType();
        if (bioAgressorType != null) {
            bioAgressors = referentialService.getBioAgressors(Collections.singleton(bioAgressorType));
        }

        groupesCibles = referentialService.getGroupesCibles();
    }

    @Override
    public void validate() {

        if (decisionRule.isPersisted() && !activated) {
            addActionError("La règle de décision est innactive !");
        }

        if (StringUtils.isBlank(decisionRule.getName())) {
            addFieldError("decisionRule.name", "Le nom est obligatoire !");
        }

        if (decisionRule.getInterventionType() == null) {
            addFieldError("decisionRule.interventionType", "Le type d'intervention est obligatoire !");
        }

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                String decisionRuleId = getLogEntityId(decisionRule);
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : decisionRule:'%s' -> %s", decisionRuleId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : decisionRule:'%s' -> %s", decisionRuleId, getFieldErrors().toString()));
            }

            initForInput();
        }
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "decision-rules-edit-input", "decisionRuleTopiaId", "${decisionRule.topiaId}"})})
    public String execute() throws Exception {
        try {
            List<DecisionRuleCrop> decisionRuleCrops = convertSectionsJson(decisionRuleCropsJson);
            decisionRule = managementModeService.createOrUpdateDecisionRule(decisionRule, domainCode, bioAgressorTopiaId, decisionRuleCrops);
            decisionRuleTopiaId = decisionRule.getTopiaId();
            notificationSupport.decisionRuleSaved(decisionRule);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Rule creation exception decisionRule '%s', domainCode '%s', bioAgressorId '%s'",
                        decisionRule, domainCode, bioAgressorTopiaId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public Map<AgrosystInterventionType, String> getAgrosystInterventionTypes() {
        return referentialService.getAgrosystInterventionTypeTranslationMap();
    }
    
    public Collection<MinimalDomainDto> getDomains() {
        Collection<MinimalDomainDto> result = new ArrayList<>();
        Collection<Domain> domainToTransform = CollectionUtils.emptyIfNull(domains);
        domainToTransform.forEach(d ->
                result.add(new MinimalDomainDto(d.getTopiaId(), d.getCode(), d.getName()))
        );
        return result;
    }
    
    public Map<BioAgressorType, String> getTranslatedBioAgressorType() {
        return referentialService.getTranslatedBioAgressorType();
    }
    
    public Map<BioAgressorType, BioAgressorParentType> getBioAgressorTypes() {
        return Referentials.getAllBioAgressorParentTypes().stream()
                .collect(Collectors.toMap(BioAgressorParentType::getParent, Function.identity()));
    }
    
    public List<DecisionRuleCrop> convertSectionsJson(String json) {
        Type type = new TypeToken<List<DecisionRuleCrop>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }
}
