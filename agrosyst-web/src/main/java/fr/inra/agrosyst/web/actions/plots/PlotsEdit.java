package fr.inra.agrosyst.web.actions.plots;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.BufferStrip;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.FrostProtectionType;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HosesPositionning;
import fr.inra.agrosyst.api.entities.IrrigationSystemType;
import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotImpl;
import fr.inra.agrosyst.api.entities.PompEngineType;
import fr.inra.agrosyst.api.entities.SolWaterPh;
import fr.inra.agrosyst.api.entities.WaterFlowDistance;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.plot.Plots;
import fr.inra.agrosyst.api.services.plot.SolHorizonDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Action d'edition (sans création) d'une parcelle.
 *
 * @author cosse
 */
public class PlotsEdit extends AbstractAgrosystAction implements Preparable {

    @Serial
    private static final long serialVersionUID = -4471100957699944974L;

    private static final Log LOGGER = LogFactory.getLog(PlotsEdit.class);

    private static final String INCORRECT_PLOT_AREA = "Surface incorrecte";

    private static final String LOWER_PLOT_AREA = "La surface de la parcelle doit être supérieur ou égale à la surface totale des zones";

    protected transient DomainService domainService;

    protected transient PlotService plotService;

    protected transient GrowingSystemService growingSystemService;
    
    protected transient ReferentialService referentialService;

    /** Edited plot id. */
    protected String plotTopiaId;

    /** Edited plot instance. */
    protected Plot plot;

    /** Related plots. */
    protected LinkedHashMap<Integer, String> relatedPlots;

    /** Plot growings systems select options. */
    protected List<GrowingSystem> growingSystems;

    /** Selected growing system id. */
    protected String growingSystemTopiaId;

    protected boolean changeableGrowingSystem;

    /** Selected location id. */
    protected String locationTopiaId;

    /** Selected domain id (for creation). */
    protected String domainTopiaId;

    /** Selected domain instance. */
    protected Domain domain;

    protected List<Ground> grounds;

    /** Onglet "zonage", liste de tous les zonages. */
    protected List<RefParcelleZonageEDI> parcelleZonages;

    /** Identifiant selectionnées dans la liste {@code parcelleZonages}. */
    protected List<String> selectedPlotZoningIds;

    /** Selected sol id (sol tab). */
    protected String selectedSolId;

    /** Liste de toutes les sol texture geppa disponibles. */
    protected List<RefSolTextureGeppa> solTextures; 

    /** Identifiant du surface texture sélectionné. */
    protected String selectedSurfaceTextureId;

    /** Identifiant du sous sol texture sélectionné. */
    protected String selectedSubSoilTextureId;

    /** Liste de toutes les sol profondeur disponibles. */
    protected List<RefSolProfondeurIndigo> solProfondeurs;

    /** Identifiant du sol profondeur sélectionné. */
    protected String selectedSolProfondeurId;

    /** Liste des sols horizons. */
    protected List<SolHorizonDto> solHorizons;

    /** Caracteristiques des sols. */
    protected List<RefSolCaracteristiqueIndigo> solCaracteristiques;

    /** Zone de la parcelle. */
    protected List<Zone> zones;
    protected UsageList<Zone> zonesUsageList;

    /** Referentiel des elements de voisinage. */
    protected List<RefElementVoisinage> adjacentElements;
 
    /** Elements de voisinage sélectionnés. */
    protected List<String> adjacentElementIds;

    /** Mesures et observation associées à la parcelle. */
    protected List<MeasurementSession> measurementSessions;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    @Override
    public void prepare() {
        plot = plotService.getPlot(plotTopiaId);
        if (plot != null) {
            if (plot.isPersisted()) {
                authorizationService.checkPlotReadable(plotTopiaId);

                readOnly = !authorizationService.isPlotWritable(plotTopiaId);
                if (readOnly) {
                    notificationSupport.plotNotWritable();
                }

                activated = plot.isActive() && plot.getDomain().isActive();
            }

            if (plot.getGrowingSystem() == null) {
                changeableGrowingSystem = true;
            } else {
                growingSystemTopiaId = plot.getGrowingSystem().getTopiaId();
                changeableGrowingSystem = authorizationService.isGrowingSystemWritable(growingSystemTopiaId);
            }
        }
    }

    @Override
    @Action("plots-edit-input")
    public String input() throws Exception {
        initForInput();

        if (plot.getLocation() != null) {
            locationTopiaId = plot.getLocation().getTopiaId();
        }
        if (plot.getPlotZonings() != null) {
            selectedPlotZoningIds = plot.getPlotZonings().stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        }
        if (plot.getGround() != null) {
            selectedSolId = plot.getGround().getTopiaId();
        }
        if (plot.getSurfaceTexture() != null) {
            selectedSurfaceTextureId = plot.getSurfaceTexture().getTopiaId();
        }
        if (plot.getSubSoilTexture() != null) {
            selectedSubSoilTextureId = plot.getSubSoilTexture().getTopiaId();
        }
        if (plot.getSolDepth() != null) {
            selectedSolProfondeurId = plot.getSolDepth().getTopiaId();
        }
        if (plot.getSolHorizon() != null) {
            solHorizons = Lists.newArrayList(Collections2.transform(plot.getSolHorizon(), Plots.SOL_HORIZON_TO_DTO::apply));
        } else {
            solHorizons = new ArrayList<>();
        }
        if (plot.getAdjacentElements() != null) {
            adjacentElementIds = plot.getAdjacentElements().stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        } else {
            adjacentElementIds = new ArrayList<>();
        }
        zones = plotService.getPlotZones(plot);
        zonesUsageList = plotService.getZonesAndUsages(plot.getTopiaId());

        return INPUT;
    }

    
    @Override
    protected void initForInput() {

        if (plot.isPersisted()) {
            domain = plot.getDomain();
            relatedPlots = plotService.getRelatedPlots(plot.getCode());
            measurementSessions = plotService.getPlotMeasurementSessions(plot.getTopiaId());
        } else {
            domain = domainService.getDomain(domainTopiaId);
        }

        growingSystems = growingSystemService.findAllActiveByDomainForPlotEdit(domain);
        if (plot.isPersisted() && plot.getGrowingSystem() != null) {
            if (!growingSystems.contains(plot.getGrowingSystem())) {
                growingSystems.add(plot.getGrowingSystem());
            }
        }
        parcelleZonages = referentialService.getAllActiveParcelleZonage();
        solTextures = referentialService.getAllActiveSolTextures();
        solProfondeurs = referentialService.getAllActiveSolProfondeurs();
        solCaracteristiques = referentialService.getAllActiveSolCaracteristiques();
        adjacentElements = referentialService.getAllActiveElementVoisinages();
        grounds = domainService.getGrounds(domain.getTopiaId());
    }

    @Override
    public void validate() {
        // domain
        if (StringUtils.isBlank(domainTopiaId)) {
            addActionError("domainTopiaId is required");
        }

        // name
        if (StringUtils.isBlank(plot.getName())) {
            addFieldError("plot.name", getText(REQUIRED_FIELD));
        }
        // empty zones
        if (CollectionUtils.isEmpty(zones)) {
            addActionError("La parcelle doit contenir au moins une zone !");
        } else {
            // zone area validation
            double totalZoneArea = 0.0;
            for (Zone zone : zones) {
                totalZoneArea += zone.getArea();
            }
          
            // Check if the sum of zones areas is inferior or equal to the plot area
            // The check includes a margin of error due to the limited precision of double numbers
            double epsilon = 1E-14;
            if ((plot.getArea() < totalZoneArea)
                  && (Math.abs(plot.getArea() - totalZoneArea) > epsilon)) {
                addFieldError("plot.area", INCORRECT_PLOT_AREA);
                addActionError(LOWER_PLOT_AREA);
            }
        }

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                String entityId = getLogEntityId(plot);
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : parcelle:'%s' -> %s", entityId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : parcelle:'%s' -> %s", entityId, getFieldErrors().toString()));
            }

            initForInput();
        }
    }

    @Override
    @Action(results = { @Result(type = "redirectAction", params = { "actionName", "plots-edit-input", "plotTopiaId", "${plot.topiaId}" }) })
    public String execute() throws Exception {
        // Make sure value is not changed
        if (!changeableGrowingSystem) {
            growingSystemTopiaId = null;
            if (plot.getGrowingSystem() != null) {
                growingSystemTopiaId = plot.getGrowingSystem().getTopiaId();
            }
        }
        plot = plotService.createOrUpdatePlot(plot, domainTopiaId, locationTopiaId, growingSystemTopiaId,
                selectedPlotZoningIds, selectedSolId, selectedSurfaceTextureId, selectedSubSoilTextureId,
                selectedSolProfondeurId, solHorizons, zones, adjacentElementIds);
        notificationSupport.plotSaved(plot);
        return SUCCESS;
    }

    public Plot getPlot() {
        // AThimel 26/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(plot, PlotImpl::new);
    }

    public void setDomainTopiaId(String domainTopiaId) {
        this.domainTopiaId = domainTopiaId;
    }

    public Domain getDomain() {
        return domain;
    }

    public List<Ground> getGrounds() {
        return grounds;
    }

    public Map<MaxSlope, String> getMaxSlopes() {
        return getEnumAsMap(MaxSlope.values());
    }
    
    public Map<BufferStrip, String> getBufferStrips() {
        return getEnumAsMap(BufferStrip.values());
    }
    
    public Map<WaterFlowDistance, String> getWaterFlowDistances() {
        return getEnumAsMap(WaterFlowDistance.values());
    }

    public String getPlotTopiaId() {
        return plotTopiaId;
    }

    public void setPlotTopiaId(String plotTopiaId) {
        this.plotTopiaId = plotTopiaId;
    }

    public LinkedHashMap<Integer, String> getRelatedPlots() {
        return relatedPlots;
    }

    public void setLocationTopiaId(String locationTopiaId) {
        this.locationTopiaId = locationTopiaId;
    }

    public String getLocationTopiaId() {
        return locationTopiaId;
    }

    public List<GrowingSystem> getGrowingSystems() {
        return growingSystems;
    }

    public String getGrowingSystemTopiaId() {
        return growingSystemTopiaId;
    }

    public void setGrowingSystemTopiaId(String growingSystemTopiaId) {
        this.growingSystemTopiaId = growingSystemTopiaId;
    }

    public List<RefParcelleZonageEDI> getParcelleZonages() {
        return parcelleZonages;
    }

    public void setSelectedPlotZoningIds(List<String> selectedPlotZoningIds) {
        this.selectedPlotZoningIds = selectedPlotZoningIds;
    }

    public List<String> getSelectedPlotZoningIds() {
        return selectedPlotZoningIds;
    }

    public Map<IrrigationSystemType, String> getIrrigationSystemTypes() {
        return getEnumAsMap(IrrigationSystemType.values());
    }

    public Map<PompEngineType, String> getPompEngineTypes() {
        return getEnumAsMap(PompEngineType.values());
    }

    public Map<HosesPositionning, String> getHosesPositionnings() {
        return getEnumAsMap(HosesPositionning.values());
    }

    public Map<FrostProtectionType, String> getFrostProtectionTypes() {
        return getEnumAsMap(FrostProtectionType.values());
    }

    public Map<SolWaterPh, String> getSolWaterPhs() {
        return getEnumAsMap(SolWaterPh.values());
    }

    public String getSelectedSolId() {
        return selectedSolId;
    }

    public void setSelectedSolId(String selectedSolId) {
        this.selectedSolId = selectedSolId;
    }

    public List<RefSolTextureGeppa> getSolTextures() {
        return solTextures;
    }

    public List<RefSolProfondeurIndigo> getSolProfondeurs() {
        return solProfondeurs;
    }

    public String getSelectedSurfaceTextureId() {
        return selectedSurfaceTextureId;
    }

    public void setSelectedSurfaceTextureId(String selectedSurfaceTextureId) {
        this.selectedSurfaceTextureId = selectedSurfaceTextureId;
    }

    public String getSelectedSubSoilTextureId() {
        return selectedSubSoilTextureId;
    }

    public void setSelectedSubSoilTextureId(String selectedSubSoilTextureId) {
        this.selectedSubSoilTextureId = selectedSubSoilTextureId;
    }

    public String getSelectedSolProfondeurId() {
        return selectedSolProfondeurId;
    }

    public void setSelectedSolProfondeurId(String selectedSolProfondeurId) {
        this.selectedSolProfondeurId = selectedSolProfondeurId;
    }

    public List<SolHorizonDto> getSolHorizons() {
        return solHorizons;
    }

    public void setSolHorizons(String json) {
        Type type = new TypeToken<List<SolHorizonDto>>() {
        }.getType();
        this.solHorizons = getGson().fromJson(json, type);
    }

    public List<RefSolCaracteristiqueIndigo> getSolCaracteristiques() {
        return solCaracteristiques;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(String json) {
        Type type = new TypeToken<List<Zone>>() {
        }.getType();
        this.zones = getGson().fromJson(json, type);
    }

    public UsageList<Zone> getZonesUsageList() {
        return zonesUsageList;
    }

    public Map<ZoneType, String> getZoneTypes() {
        return getEnumAsMap(ZoneType.values());
    }

    public List<RefElementVoisinage> getAdjacentElements() {
        return adjacentElements;
    }

    public List<String> getAdjacentElementIds() {
        return adjacentElementIds;
    }

    public void setAdjacentElementIds(List<String> adjacentElementIds) {
        this.adjacentElementIds = adjacentElementIds;
    }
    
    public List<MeasurementSession> getMeasurementSessions() {
        return measurementSessions;
    }

    public boolean isChangeableGrowingSystem() {
        return changeableGrowingSystem;
    }
}
