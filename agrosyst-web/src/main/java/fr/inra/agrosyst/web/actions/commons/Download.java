package fr.inra.agrosyst.web.actions.commons;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Downloadable;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;
import java.sql.SQLException;

public class Download extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 1864154104471757665L;

    private static final Log LOGGER = LogFactory.getLog(Download.class);

    protected String id;

    protected Downloadable downloadable;

    public void setId(String id) {
        this.id = id;
    }

    public Downloadable getDownloadable() {
        return downloadable;
    }

    @Override
    @Action(results= {
            @Result(type="stream",
                    params={
                        "contentType", "${downloadable." + Downloadable.PROPERTY_MIME_TYPE + "}",
                        "inputName", "inputStream",
                        "contentDisposition", "attachment; filename=\"${downloadable." + Downloadable.PROPERTY_FILE_NAME + "}\""
                    }
            )
    })

    public String execute() throws Exception {
        downloadable = attachmentService.getDownloadable(id);
        return SUCCESS;
    }
    
    public InputStream getInputStream() {
        try {
            return downloadable.getContent().getBinaryStream();
        } catch (SQLException ex) {
            throw new AgrosystTechnicalException("Can't get file content from database", ex);
        }
    }
}
