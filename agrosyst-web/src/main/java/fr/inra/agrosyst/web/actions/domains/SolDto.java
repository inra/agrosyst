package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;
import java.io.Serializable;

/**
 * Sol dto used to edit {@code Sol} in struts (serialized in json).
 * 
 * @author Eric Chatellier
 */
public class SolDto implements Serializable {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -5735897928474989471L;

    protected String topiaId;

    protected String name;

    protected String comment;

    protected double importance;

    protected String solArvalisTopiaId;

    protected String solNom;

    protected String solCalcaire;

    protected String solHydromorphie;

    protected String solPierrosite;

    protected String solProfondeur;

    protected String solTexture;

    protected String solRegion;

    protected int solRegionCode;

    protected String solGrenTopiaId;

    public SolDto() {
        
    }

    public SolDto(String topiaId, String name, String comment, double importance) {
        this.topiaId = topiaId;
        this.name = name;
        this.comment = comment;
        this.importance = importance;
    }

    public String getTopiaId() {
        return topiaId;
    }

    public void setTopiaId(String topiaId) {
        this.topiaId = topiaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getImportance() {
        return importance;
    }

    public void setImportance(double importance) {
        this.importance = importance;
    }

    public String getSolArvalisTopiaId() {
        return solArvalisTopiaId;
    }

    public void setSolArvalisTopiaId(String solArvalisTopiaId) {
        this.solArvalisTopiaId = solArvalisTopiaId;
    }

    public String getSolNom() {
        return solNom;
    }

    public void setSolNom(String solNom) {
        this.solNom = solNom;
    }

    public String getSolCalcaire() {
        return solCalcaire;
    }

    public void setSolCalcaire(String solCalcaire) {
        this.solCalcaire = solCalcaire;
    }

    public String getSolHydromorphie() {
        return solHydromorphie;
    }

    public void setSolHydromorphie(String solHydromorphie) {
        this.solHydromorphie = solHydromorphie;
    }

    public String getSolPierrosite() {
        return solPierrosite;
    }

    public void setSolPierrosite(String solPierrosite) {
        this.solPierrosite = solPierrosite;
    }

    public String getSolProfondeur() {
        return solProfondeur;
    }

    public void setSolProfondeur(String solProfondeur) {
        this.solProfondeur = solProfondeur;
    }

    public String getSolTexture() {
        return solTexture;
    }

    public void setSolTexture(String solTexture) {
        this.solTexture = solTexture;
    }

    public String getSolRegion() {
        return solRegion;
    }

    public void setSolRegion(String solRegion) {
        this.solRegion = solRegion;
    }

    public int getSolRegionCode() {
        return solRegionCode;
    }

    public void setSolRegionCode(int solRegionCode) {
        this.solRegionCode = solRegionCode;
    }

    public String getSolGrenTopiaId() {
        return solGrenTopiaId;
    }

    public void setSolGrenTopiaId(String solGrenTopiaId) {
        this.solGrenTopiaId = solGrenTopiaId;
    }
}
