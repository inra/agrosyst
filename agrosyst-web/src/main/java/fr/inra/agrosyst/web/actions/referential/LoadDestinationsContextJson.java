package fr.inra.agrosyst.web.actions.referential;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.Set;

/**
 * Created by davidcosse on 01/07/14.
 */
public class LoadDestinationsContextJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoadDestinationsContextJson.class);
    @Serial
    private static final long serialVersionUID = -2690053349676146644L;
    
    // for practiced crop cycle
    protected transient String growingSystemId;
    protected transient String campaigns;

    // for effective crop cycle
    protected transient String zoneTopiaId;

    protected transient Set<String> speciesCodes;
    protected transient Set<WineValorisation> wineValorisations;

    protected transient ReferentialService referentialService;

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }
    
    @Override
    public String execute() throws Exception {
        try {
            jsonData = referentialService.getDestinationContext(speciesCodes, wineValorisations, growingSystemId, campaigns, zoneTopiaId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to load DestinationContext for speciesCodes '%s', wineValorisations '%s', growingSystemId '%s', campaigns '%s', zoneTopiaId '%s'",
                        speciesCodes, wineValorisations, growingSystemId, campaigns, zoneTopiaId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }

    public void setSpeciesCodes(String json) {
        Type type = new TypeToken<Set<String>>() {}.getType();
        this.speciesCodes = getGson().fromJson(json, type);
    }

    public void setWineValorisations(String wineValorisations) {
        Type type = new TypeToken<Set<WineValorisation>>() {}.getType();
        this.wineValorisations = getGson().fromJson(wineValorisations, type);
    }

    public void setGrowingSystemId(String growingSystemId) {
        this.growingSystemId = growingSystemId;
    }

    public void setCampaigns(String campaigns) {
        this.campaigns = campaigns;
    }

    public void setZoneTopiaId(String zoneTopiaId) {
        this.zoneTopiaId = zoneTopiaId;
    }
}
