package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.network.NetworkGraph;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;

import java.io.Serial;
import java.util.Set;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class ShowGrowingSystemAndNetworkGraphRaw extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 235930146604038026L;

    protected transient NetworkService networkService;

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }

    protected Set<String> parentNetworkIds;
    protected String growingSystemName;

    public void setParentNetworkIds(Set<String> parentNetworkIds) {
        this.parentNetworkIds = parentNetworkIds;
    }

    public void setGrowingSystemName(String growingSystemName) {
        this.growingSystemName = growingSystemName;
    }

    protected NetworkGraph networkGraph;

    @Override
    public String execute() throws Exception {
        if (parentNetworkIds != null) {
            networkGraph = networkService.buildGrowingSystemAndNetworkGraph(growingSystemName, parentNetworkIds);
        }
        return SUCCESS;
    }

    public NetworkGraph getNetworkGraph() {
        return networkGraph;
    }
}
