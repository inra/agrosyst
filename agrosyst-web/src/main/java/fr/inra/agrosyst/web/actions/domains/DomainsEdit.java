package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainImpl;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.WeatherStation;
import fr.inra.agrosyst.api.entities.Zoning;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOrganicProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOtherProductInputDto;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.utils.DataValidator;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Action d'édition d'un domaine.
 *
 * @author Eric Chatellier
 */
@Getter
@Setter
public class DomainsEdit extends AbstractAgrosystAction implements Preparable {

    protected static final Predicate<CroppingPlanSpeciesDto> HAS_NO_SPECIES = speciesDto -> speciesDto.getSpeciesId() == null;

    private static final Log LOGGER = LogFactory.getLog(DomainsEdit.class);

    @Serial
    private static final long serialVersionUID = 1686182182420156247L;
    
    protected transient ReferentialService referentialService;

    protected transient DomainService domainService;

    protected transient PlotService plotService;

    protected transient PricesService pricesService;
    
    protected Integer campaign;

    protected Domain domain;

    protected String speciesToArea = "";

    // AThimel 18/10/13 Using implementation to make sure order is kept
    protected LinkedHashMap<Integer, String> relatedDomains = new LinkedHashMap<>();

    protected List<GeoPoint> geoPoints;
    protected String geoPointsJson;

    protected String domainTopiaId;

    protected String commune;

    protected String legalStatusId;

    protected List<RefLegalStatus> allRefLegalStatus;

    protected RefLocation location;

    protected String departement;
    protected String petiteRegionAgricole;
    protected String petiteRegionAgricoleName;

    protected String countryTopiaId;
    protected String locationTopiaId;

    protected List<Equipment> equipments;
    protected String equipmentsJson;
    protected List<ToolsCoupling> toolsCouplings;
    protected String toolsCouplingsJson;

    protected Map<Integer, String> solArvalisRegions;

    protected List<Ground> grounds;
    protected String groundsJson;

    protected List<CroppingPlanEntryDto> croppingPlans;
    protected String croppingPlansJson;

    protected UsageList<CroppingPlanEntryDto> croppingPlansUsageList;
    protected Map<String, Boolean> croppingPlanSpeciesUsageMap;

    protected Integer otex18;

    protected Integer otex70;

    protected Map<Integer, String> otex18s;

    protected Map<Integer, String> otex70s = new HashMap<>();

    protected Collection<WeatherStationDto> weatherStations = new ArrayList<>();
    protected String weatherStationsJson;

    protected Map<String, String> allRefStationMeteoIdsAndNames;

    protected List<RefInterventionAgrosystTravailEDI> agrosystActionsFullList;

    protected boolean canEditPlots;

    protected List<LivestockUnit> livestockUnits;
    protected String livestockUnitsJson;

    protected List<DomainInputDto> inputStockUnits;
    protected String inputStockUnitsJson;

    protected String domainSiret;

    protected int plotExportAsyncThreshold;

    protected Map<String, String> countries;

    protected List<HarvestingPriceDto> harvestingPriceDtos;

    //    protected String pricesJson;
    protected String allRegularPricesJson;

    public Domain getDomain() {
        // AThimel 26/06/2013 Fais chier de devoir écrire ça, mais c'est la seule option pour ne pas avoir une grosse dose d'exceptions avec du paramsPrepareParams
        return Objects.requireNonNullElseGet(domain, DomainImpl::new);
    }

    @Override
    public void prepare() {

        if (StringUtils.isEmpty(domainTopiaId)) {
            // Cas de création d'un domain
            domain = domainService.newDomain();
            if (campaign != null) {
                domain.setCampaign(campaign);
            }
            
        } else {

            // TODO AThimel 07/10/13 May be included directly in the service ?
            authorizationService.checkDomainReadable(domainTopiaId);

            // Cas d'une mise à jour de domain
            readOnly = !authorizationService.isDomainWritable(domainTopiaId);
            if (readOnly) {
                notificationSupport.domainNotWritable();
            }

            domain = domainService.getDomain(domainTopiaId);

            activated = domain.isActive();

            speciesToArea = sanitizeSpeciesToAreaJson(domain.getSpeciesToArea());

            if (StringUtils.isBlank(locationTopiaId)) {
                // Cas ou la commune n'a pas étée modifiée
                locationTopiaId = domain.getLocation().getTopiaId();
            }

        }

        if (!StringUtils.isBlank(locationTopiaId)) {
            location = referentialService.getRefLocation(locationTopiaId);
            domain.setLocation(location);

            departement = location.getDepartement();

            petiteRegionAgricole = location.getPetiteRegionAgricoleCode();
            petiteRegionAgricoleName = location.getPetiteRegionAgricoleNom();

            countryTopiaId = location.getRefCountry().getTopiaId();
        } else {
            countryTopiaId = getDefaultCountryTopiaId();
        }

        campaign = domain.getCampaign() == 0 ? null : domain.getCampaign();

        plotExportAsyncThreshold = config.getPlotsExportAsyncThreshold();
    }


    @Override
    public void validate() {

        validateActiveDomain();

        validDomainMainCharacteristics();

        validWeatherStations();

        validEquipments();

        validToolsCouplings();

        validGeoPoints();

        validLiveStock();

        validDomainInputStockUnits();

        validGrounds();

        validCroppingPlans();

        validHarvestingPrices();

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                String domainId = getLogEntityId(domain);
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : domain:'%s' -> %s", domainId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : domain:'%s' -> %s", domainId, getFieldErrors().toString()));
            }

            initForInput();
        }
    }

    protected void validateActiveDomain() {
        if (!domain.isActive()) {
            addActionError(getText("domain-validation-inactive"));
        }
    }

    @Override
    protected void initForInput() {

        allRefLegalStatus = referentialService.getAllRefActiveLegalStatus();
        allRefStationMeteoIdsAndNames = referentialService.getAllRefStationMeteoMap();
        
        solArvalisRegions = referentialService.getSolArvalisRegions();

        otex18s = referentialService.getAllActiveOtex18Code();

        if (domain.getOtex18() != null) {
            otex70s = referentialService.getAllActiveCodeOtex70ByOtex18code(domain.getOtex18().getCode_OTEX_18_postes());
        }


        if (StringUtils.isNotEmpty(domainTopiaId)) {
            relatedDomains = domainService.getRelatedDomains(domain.getCode());
            canEditPlots = authorizationService.areDomainPlotsEditable(domainTopiaId);

        } else {
            canEditPlots = true;
        }
    }

    @Override
    @Action("domains-edit-input")
    public String input() {

        initForInput();

        if (StringUtils.isNotEmpty(domainTopiaId)) {
            geoPoints = domainService.getGeoPoints(domainTopiaId);
            grounds = domainService.getGrounds(domainTopiaId);
            weatherStations = getWeatherStations(domain);
        } else {
            geoPoints = Collections.emptyList();
            grounds = Collections.emptyList();
        }

        return INPUT;
    }

    protected Collection<WeatherStationDto> getWeatherStations(Domain domain) {
        Collection<WeatherStation> weatherStations = domain.getWeatherStations();
        String defaultWeatherStationId = null;

        if (domain.getDefaultWeatherStation() != null) {
            defaultWeatherStationId = domain.getDefaultWeatherStation().getTopiaId();
        }

        return Collections2.transform(weatherStations, WeatherStations.getFunctionWeatherStationToDto(defaultWeatherStationId));
    }


    protected void validDomainMainCharacteristics() {
        // campagne
        if (campaign == null || campaign == 0) {
            addFieldError("campaign", getText(getText(REQUIRED_FIELD)));
            addActionError(getText("domain-validation-noCampaign"));
        } else {
            if (!CommonService.getInstance().areCampaignsValids(Integer.toString(campaign))) {
                addFieldError("campaign", String.format(getText("domain-validation-invalidCampaign"), campaign));
                addActionError(String.format(getText("domain-validation-invalidCampaign"), campaign));
            }
        }

        // nom du domain
        if (StringUtils.isBlank(domain.getName())) {
            addFieldError("domain.name", getText(getText(REQUIRED_FIELD)));
            addActionError(getText("domain-validation-requiredName"));
        }

        // nom de l'interlocuteur principal
        if (StringUtils.isBlank(domain.getMainContact())) {
            addFieldError("domain.mainContact", getText(getText(REQUIRED_FIELD)));
            addActionError(getText("domain-validation-requiredMainContact"));
        }

        // commune
        if (StringUtils.isBlank(locationTopiaId)) {
            addFieldError("commune", getText(getText(REQUIRED_FIELD)));
            addActionError(getText("domain-validation-requiredLocation"));
        }

        // le type de domaine
        if (domain.getType() == null) {
            // we set a default value to avoid npe due to flushing in initForInput();
            domain.setType(DomainType.DOMAINE_EXPERIMENTAL);
            addFieldError("domain.type", getText(getText(REQUIRED_FIELD)));
            addActionError(getText("domain-validation-requiredType"));
        }


        if (StringUtils.isNotBlank(domainSiret) && !DataValidator.isSiretValid(domainSiret)) {
            addActionError(getText("domain-validation-invalidSiret"));
        }

        Double meadowArea = domain.getMeadowArea();
        double sum1 = Optional.ofNullable(domain.getMeadowAlwaysWithGrassArea()).orElse(0d)
                + Optional.ofNullable(domain.getMeadowOtherArea()).orElse(0d);
        double sum2 = Optional.ofNullable(domain.getMeadowOnlyMowedArea()).orElse(0d)
                + Optional.ofNullable(domain.getMeadowOnlyPasturedArea()).orElse(0d)
                + Optional.ofNullable(domain.getMeadowPasturedAndMowedArea()).orElse(0d);
        if (meadowArea != null && (sum1 > 0d && meadowArea != sum1 || sum2 > 0d && meadowArea != sum2)) {
            String errorMessage = getText("domain-validation-invalidMeaowArea");
            addFieldError("domain.meadowArea", errorMessage);
            addActionError(errorMessage);
        }

        // Le nombre de salariés et associés
        final Integer partnersNumber = domain.getPartnersNumber();
        if (partnersNumber != null && partnersNumber < 0) {
            addError("domain-validation-partnersNumber", "domain.partnersNumber");
        }

        final Double operatorWorkForce = domain.getOperatorWorkForce();
        if (operatorWorkForce != null && operatorWorkForce < 0) {
            addError("domain-validation-operatorWorkForce", "domain.operatorWorkForce");
        }

        final Double otherWorkForce = domain.getOtherWorkForce();
        if (otherWorkForce != null && otherWorkForce < 0) {
            addError("domain-validation-otherWorkForce", "domain.otherWorkForce");
        }

        final Double cropsWorkForce = domain.getCropsWorkForce();
        if (cropsWorkForce != null && cropsWorkForce < 0) {
            addError("domain-validation-cropsWorkForce", "domain.cropsWorkForce");
        }

        final Double volunteerWorkForce = domain.getVolunteerWorkForce();
        if (volunteerWorkForce != null && volunteerWorkForce < 0) {
            addError("domain-validation-volunteerWorkForce", "domain.volunteerWorkForce");
        }

        final Double permanentEmployeesWorkForce = domain.getPermanentEmployeesWorkForce();
        if (permanentEmployeesWorkForce != null && permanentEmployeesWorkForce < 0) {
            addError("domain-validation-permanentEmployeesWorkForce", "domain.permanentEmployeesWorkForce");
        }

        final Double temporaryEmployeesWorkForce = domain.getTemporaryEmployeesWorkForce();
        if (temporaryEmployeesWorkForce != null && temporaryEmployeesWorkForce < 0) {
            addError("domain-validation-temporaryEmployeesWorkForce", "domain.temporaryEmployeesWorkForce");
        }

        final Double seasonalWorkForce = domain.getSeasonalWorkForce();
        if (seasonalWorkForce != null && seasonalWorkForce < 0) {
            addError("domain-validation-seasonalWorkForce", "domain.seasonalWorkForce");
        }

        final Double nonSeasonalWorkForce = domain.getNonSeasonalWorkForce();
        if (nonSeasonalWorkForce != null && nonSeasonalWorkForce < 0) {
            addError("domain-validation-nonSeasonalWorkForce", "domain.nonSeasonalWorkForce");
        }

        final Integer nbPlot = domain.getNbPlot();
        if (nbPlot != null && nbPlot < 0) {
            addActionError(getText("domain-validation-nbPlotNegative"));
        }

        final Double furthestPlotDistance = domain.getFurthestPlotDistance();
        if (furthestPlotDistance != null && furthestPlotDistance < 0) {
            addActionError(getText("domain-validation-furthestPlotDistanceNegative"));
        }

        final Double areaAroundHQ = domain.getAreaAroundHQ();
        if (areaAroundHQ != null && areaAroundHQ < 0) {
            addActionError(getText("domain-validation-areaAroundHQ"));
        }
    }

    private void addError(String errorMessageKey, String fieldName) {
        String errorMessage = getText(errorMessageKey);
        addFieldError(fieldName, errorMessage);
        addActionError(errorMessage);
    }

    protected void validWeatherStations() {
        try {
            weatherStations = convertWeatherStationsJson(weatherStationsJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("for domain '" + domain.getTopiaId() + "'", ex);
            }
            addActionError(String.format(getText("domain-validation-loadingWeatherStationsFailed"), ex));
            weatherStations = getWeatherStations(domain);
        }

        // validation des stations méteo
        if (weatherStations != null) {
            List<WeatherStationDto> emptyWeatherStationDtos = new ArrayList<>();
            weatherStations.removeAll(Collections.singleton(null));
            for (WeatherStationDto weatherStationDto : weatherStations) {
                if (StringUtils.isBlank(weatherStationDto.getRefStationMeteoTopiaId())){
                    if (StringUtils.isNotBlank(weatherStationDto.getComment())
                            || StringUtils.isNotBlank(weatherStationDto.getData())) {
                        addActionError(getText("domain-validation-requiredWeatherStationName"));
                    } else {
                        emptyWeatherStationDtos.add(weatherStationDto);
                    }
                }
            }
            weatherStations.removeAll(emptyWeatherStationDtos);
        }
    }

    protected void validEquipments() {
        try {
            equipments = convertEquipmentsJson(equipmentsJson);
            // validation des materiels
            if (equipments != null) {
                equipments.removeAll(Collections.singleton(null));
                for (Equipment equipment : equipments) {
                    if (StringUtils.isBlank(equipment.getName())) {
                        addActionError(getText("domain-validation-requiredEquipmentName"));
                    }

                    final Double realUsage = equipment.getRealUsage();
                    if (realUsage != null && realUsage < 0) {
                        addActionError(getText("domain-validation-realUsageNegative"));
                    }
                }
            }

        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("for domain '" + domainTopiaId + "'", ex);
            }
            addActionError(String.format(getText("domain-validation-loadingEquipmentsFailed"), ex));
            equipments = StringUtils.isBlank(domainTopiaId) ? null: domainService.getEquipments(domainTopiaId);
        }

    }

    protected void validToolsCouplings() {

        if(StringUtils.isNotBlank(toolsCouplingsJson)) {
            try {
                toolsCouplings = convertToolsCouplingsJson(toolsCouplingsJson);
                toolsCouplings.removeAll(Collections.singleton(null));
                for (ToolsCoupling toolsCoupling : toolsCouplings) {
                    Collection<Equipment> tcEquipments = toolsCoupling.getEquipments();

                    String toolsCouplingName = getText("domain-validation-toolsCouplingNameNotSet");
                    if (StringUtils.isBlank(toolsCoupling.getToolsCouplingName())) {
                        addActionError(getText("domain-validation-requiredToolsCouplingName"));
                    } else {
                        toolsCouplingName = toolsCoupling.getToolsCouplingName();
                    }

                    final Double workRate = toolsCoupling.getWorkRate();
                    if (workRate != null && workRate < 0) {
                        addActionError(getText("domain-validation-toolsCouplingWorkRateNegative"));
                    }

                    final Double workforce = toolsCoupling.getWorkforce();
                    if (workforce != null && workforce < 0) {
                        addActionError(getText("domain-validation-toolsCouplingWorkforceNegative"));
                    }

                    if (toolsCoupling.getMainsActions() == null || toolsCoupling.getMainsActions().isEmpty()) {
                        String errorMessage = String.format(getText("domain-validation-requiredActionForToolsCoupling"),toolsCouplingName);
                        addFieldError("editedToolsCoupling-mainsActions", errorMessage);
                        addActionError(errorMessage);
                    }

                    if (!toolsCoupling.isManualIntervention()) {
                        boolean isIrrigationEquipments = false;
                        boolean isNonIrrigationEquipments = false;
                        if (CollectionUtils.isNotEmpty(tcEquipments)) {
                            for (Equipment tcEquipment : tcEquipments) {
                                boolean isIrrigationEquipment = tcEquipment.getRefMateriel() instanceof RefMaterielIrrigation;
                                if (isIrrigationEquipment) {
                                    isIrrigationEquipments = true;
                                } else {
                                    isNonIrrigationEquipments = true;
                                }
                            }
                        }
            
                        Equipment tractor = toolsCoupling.getTractor();
                        // no tractor, can be valid if equipments only contains irrigation tools.
                        if (tractor == null) {
                            if (!isIrrigationEquipments) {
                                String errorMessage = String.format(getText("domain-validation-requiredIrrigationForToolsCoupling"), toolsCouplingName);
                                addFieldError("editedToolsCoupling-mainsActions", errorMessage);
                                addActionError(errorMessage);
                            } else {
                                // if no tractor equipments can only be irrigation type ones.
                                if (CollectionUtils.isNotEmpty(tcEquipments)) {
                                    if (isNonIrrigationEquipments) {
                                        String errorMessage = String.format(getText("domain-validation-invalidIrrigationAssociationForToolsCoupling"), toolsCouplingName);
                                        addFieldError("editedToolsCoupling-mainsActions", errorMessage);
                                        addActionError(errorMessage);
                                    }
                                }
                            }
                        } else {
                            // if tractor is not auto-motor's type, it must be associated to 1+n equipment
                            if (tractor.getRefMateriel() == null || !(tractor.getRefMateriel() instanceof RefMaterielAutomoteur)) {
                                if (CollectionUtils.isEmpty(tcEquipments)) {
                                    addActionError(String.format(getText("domain-validation-missingTractorOnToolsCoupling"), toolsCouplingName));
                                }
                            }
                            if (CollectionUtils.isNotEmpty(tcEquipments)) {
                                if (isIrrigationEquipments && isNonIrrigationEquipments) {
                                    String errorMessage = String.format(getText("domain-validation-invalidIrrigationAssociationForToolsCoupling"), toolsCouplingName);
                                    addFieldError("editedToolsCoupling-mainsActions", errorMessage);
                                    addActionError(errorMessage);
                                }
                            }
                            if (tractor.getRefMateriel() instanceof RefMaterielOutil) {
                                String errorMessage = String.format(getText("domain-validation-invalidEquipmentTypeOnToolsCoupling"), toolsCouplingName);
                                addFieldError("editedToolsCoupling-mainsActions", errorMessage);
                                addActionError(errorMessage);
                            }
                        }
            
                    }

                    if (CollectionUtils.isNotEmpty(tcEquipments)) {
                        Set<Equipment> uniqueEquipments = new HashSet<>(tcEquipments);
                        toolsCoupling.setEquipments(uniqueEquipments);
                    }
                }
                // validation des attelages
            } catch (Exception ex) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("for domain '" + domainTopiaId + "'", ex);
                }
                addActionError(String.format(getText("domain-validation-loadingToolsCouplingsFailed"), ex));
            }
        }
    }

    protected void validGeoPoints() {
        try {
            geoPoints = convertGeoPointsJson(geoPointsJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("for domain '" + domainTopiaId + "'", ex);
            }
            addActionError(String.format(getText("domain-validation-loadingGeopointsFailed"), ex));
        }

        if (geoPoints == null) {
            geoPoints = StringUtils.isBlank(domainTopiaId) ? null : domainService.getGeoPoints(domainTopiaId);
        } else {
            geoPoints.removeAll(Collections.singleton(null));
            List<GeoPoint> emptyGeoPoints = new ArrayList<>();
            for (GeoPoint geoPoint : geoPoints) {
                if (Strings.isNullOrEmpty(geoPoint.getName())) {
                    if (geoPoint.getLatitude() != 0d
                            || geoPoint.getLongitude() != 0d
                            || StringUtils.isNotBlank(geoPoint.getDescription())){
                        addActionError(getText("domain-validation-requiredGeopointName"));
                    } else {
                        emptyGeoPoints.add(geoPoint);
                    }
                }
            }
            geoPoints.removeAll(emptyGeoPoints);
        }
    }

    protected void validLiveStock() {
        livestockUnits = computeLiveStockUnits();

        // validation des ateliers d’élevage
        if (CollectionUtils.isNotEmpty(livestockUnits)) {
            if (livestockUnits.stream().filter(Objects::nonNull).anyMatch(livestockUnit -> livestockUnit.getRefAnimalType() == null)) {
                addActionError(getText("domain-validation-requiredLivestockAnimalType"));
            }
        }
    }

    protected void validDomainInputStockUnits() {
        try {
            inputStockUnits = convertDomainInputStockUnitsJson(inputStockUnitsJson);
            if (inputStockUnits == null) {
                // rien à vérifier
                return;
            }

            for (DomainInputDto domainInput : inputStockUnits) {
                // Vérification du prix
                domainInput.getPrice().ifPresent(inputPriceDto -> {
                    final Double price = inputPriceDto.getPrice();
                    if (price != null && price < 0) {
                        addActionError(getText("domain-validation-inputPriceNegative").formatted(domainInput.getInputName()));
                    }
                });

                // Vérification des éléments composants l'intrant saisis par l'utilisateur
                if (domainInput instanceof DomainMineralProductInputDto mineralInput) {
                    if (mineralInput.getBore() < 0) {
                        addActionErrorForMineralInput("common-element-B");
                    }
                    if (mineralInput.getCalcium() < 0) {
                        addActionErrorForMineralInput("common-element-Ca");
                    }
                    if (mineralInput.getCuivre() < 0) {
                        addActionErrorForMineralInput("common-element-Cu");
                    }
                    if (mineralInput.getFer() < 0) {
                        addActionErrorForMineralInput("common-element-Fe");
                    }
                    if (mineralInput.getManganese() < 0) {
                        addActionErrorForMineralInput("common-element-Mn");
                    }
                    if (mineralInput.getN() < 0) {
                        addActionErrorForMineralInput("common-element-N");
                    }
                    if (mineralInput.getK2o() < 0) {
                        addActionErrorForMineralInput("common-element-K2O");
                    }
                    if (mineralInput.getMgo() < 0) {
                        addActionErrorForMineralInput("common-element-MgO");
                    }
                    if (mineralInput.getMolybdene() < 0) {
                        addActionErrorForMineralInput("common-element-Mo");
                    }
                    if (mineralInput.getOxyde_de_sodium() < 0) {
                        addActionErrorForMineralInput("common-element-Na2O");
                    }
                    if (mineralInput.getP2o5() < 0) {
                        addActionErrorForMineralInput("common-element-P2O5");
                    }
                    if (mineralInput.getSo3() < 0) {
                        addActionErrorForMineralInput("common-element-SO3");
                    }
                    if (mineralInput.getZinc() < 0) {
                        addActionErrorForMineralInput("common-element-Zn");
                    }
                } else if (domainInput instanceof DomainOrganicProductInputDto organicInput) {
                    if (organicInput.getCaO() != null && organicInput.getCaO() < 0) {
                        addActionErrorForOrganicInput("common-element-CaO");
                    }
                    if (organicInput.getK2O() < 0) {
                        addActionErrorForOrganicInput("common-element-K2O");
                    }
                    if (organicInput.getN() < 0) {
                        addActionErrorForOrganicInput("common-element-N");
                    }
                    if (organicInput.getMgO() != null && organicInput.getMgO() < 0) {
                        addActionErrorForOrganicInput("common-element-MgO");
                    }
                    if (organicInput.getP2O5() < 0) {
                        addActionErrorForOrganicInput("common-element-P2O5");
                    }
                    if (organicInput.getS() != null && organicInput.getS() < 0) {
                        addActionErrorForOrganicInput("common-element-S");
                    }
                    break;
                } else if (domainInput instanceof DomainOtherProductInputDto otherInput) {
                    final Double lifetime = otherInput.getLifetime();
                    if (lifetime != null && lifetime < 0) {
                        addActionError(getText("agrosyst-domain-validation-otherInputLifetimeNegative"));
                    }
                }
            }
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("for domain '" + domainTopiaId + "'", ex);
            }
            addActionError(String.format(getText("domain-validation-loadingInputsFailed"), ex));
        }
    }

    private void addActionErrorForMineralInput(final String elementKey) {
        addActionError(getText("agrosyst-domain-validation-mineralInput").formatted(getText(elementKey)));
    }

    private void addActionErrorForOrganicInput(final String elementKey) {
        addActionError(getText("agrosyst-domain-validation-organicInput").formatted(getText(elementKey)));
    }

    protected void validGrounds() {
        try {
            grounds = convertGroundsJson(groundsJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("for domain '" + domainTopiaId + "'", ex);
            }
            addActionError(String.format(getText("domain-validation-loadingGroundsFailed"), ex));
        }
        // validation des sols
        if (grounds == null) {
            grounds = StringUtils.isBlank(domainTopiaId) ? null : domainService.getGrounds(domainTopiaId);
        } else {
            grounds.removeAll(Collections.singleton(null));
            for (Ground ground : grounds) {
                if (ground.getRefSolArvalis() == null) {
                    addActionError(getText("domain-validation-groundRegionRequired"));
                }

                final double importance = ground.getImportance();
                if (importance < 0 || importance > 100) {
                    addActionError(getText("domain-validation-groundImportanceNegative"));
                }
            }
        }
    }

    protected void validCroppingPlans() {
        try {
            croppingPlans = convertCroppingPlansJson(croppingPlansJson);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("for domain '" + domainTopiaId + "'", ex);
            }
            addActionError(String.format(getText("domain-validation-loadingCropsFailed"), ex));
        }

        if (croppingPlans != null) {
            croppingPlans.removeAll(Collections.singleton(null));
            for (CroppingPlanEntryDto croppingPlanEntryDto : croppingPlans) {
                if (StringUtils.isBlank(croppingPlanEntryDto.getName())) {
                    addActionError(getText("domain-validation-cropNameRequired"));
                }
                final Double yealdAverage = croppingPlanEntryDto.getYealdAverage();
                final String cropName = croppingPlanEntryDto.getName() == null ? "?" : croppingPlanEntryDto.getName();
                if (yealdAverage != null && croppingPlanEntryDto.getYealdUnit() == null) {
                    addActionError(String.format(
                            getText("domain-validation-missingCropYieldUnit"),
                            cropName, df.format(yealdAverage)
                        )
                    );
                }

                if (yealdAverage != null && yealdAverage < 0) {
                    addActionError(String.format(
                            getText("domain-validation-cropYieldNegative"),
                            cropName
                    ));
                }

                final Double averageIFT = croppingPlanEntryDto.getAverageIFT();
                if (averageIFT != null && averageIFT < 0) {
                    addActionError(String.format(
                            getText("domain-validation-cropAverageIFTNegative"),
                            cropName
                    ));
                }

                final Double biocontrolIFT = croppingPlanEntryDto.getBiocontrolIFT();
                if (biocontrolIFT != null && biocontrolIFT < 0) {
                    addActionError(String.format(
                            getText("domain-validation-cropBiocontrolIFTNegative"),
                            cropName
                    ));
                }

                croppingPlanEntryDto.getSpecies().removeAll(Collections.singleton(null));
                Iterables.removeIf(croppingPlanEntryDto.getSpecies(), HAS_NO_SPECIES::test);
            }
        }

        if (domain.getSpeciesToArea() != null) {
            String speciesToAreaMap = domainService.sanitizedDomainSpeciesToAreaJson(domain.getSpeciesToArea());
            domain.setSpeciesToArea(speciesToAreaMap);
        }
    }

    protected void validHarvestingPrices() {
        if (harvestingPriceDtos == null) {
            // rien à vérifier
            return;
        }

        for (HarvestingPriceDto harvestingPriceDto : this.harvestingPriceDtos) {
            final Double price = harvestingPriceDto.getPrice();
            if (price != null && price < 0) {
                addActionError(getText("domain-validation-harvestingPriceNegative").formatted(harvestingPriceDto.getDisplayName()));
            }
        }
    }

    @Override
    @Action(results = {
            @Result(type = "redirectAction", params = {"actionName", "domains-edit-input", "domainTopiaId", "${domain.topiaId}"})})
    public String execute() throws Exception {

        propagateWeatherStationsChanges();

        domain.setCampaign(campaign);
        domain.setSiret(StringUtils.trimToNull(StringUtils.replace(domainSiret, " ", "")));

        createOrUpdateDomain();

        notificationSupport.domainSaved(domain);

        if (Strings.isNullOrEmpty(domainTopiaId)) {
            navigationContextEntityCreated(domain);
        }

        return SUCCESS;
    }

    protected void createOrUpdateDomain() {
        domain = domainService.createOrUpdateDomain(
                domain,
                locationTopiaId,
                legalStatusId,
                geoPoints,
                croppingPlans,
                otex18,
                otex70,
                grounds,
                equipments,
                toolsCouplings,
                livestockUnits,
                inputStockUnits,
                harvestingPriceDtos
        );

        if (croppingPlans != null && croppingPlans.stream().anyMatch(croppingPlan -> CollectionUtils.isEmpty(croppingPlan.getSpecies()))) {
            notificationSupport.warning(notificationSupport.getTranslatedText("domain.save.warning.missingSpecies"));
        }
    }

    private List<LivestockUnit> computeLiveStockUnits() {
        List<LivestockUnit> livestockUnits;
        try {
            livestockUnits = convertLivestockUnitsJson(livestockUnitsJson);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("for domain '" + domainTopiaId + "'", e);
            }
            livestockUnits = StringUtils.isBlank(domainTopiaId) ? null : domainService.loadLivestockUnitsForDomainId(domainTopiaId);
        }
        return livestockUnits;
    }

    /**
     * Propagates changes on {@link Domain#getWeatherStations()}.
     */
    protected void propagateWeatherStationsChanges() {
        Collection<WeatherStation> weatherStations = getDomain().getWeatherStations();
        List<WeatherStation> nonDeleted = new ArrayList<>();

        if (weatherStations == null) {
            weatherStations = new ArrayList<>();
            getDomain().setWeatherStations(weatherStations);
        }

        Map<String, WeatherStation> indexWeatherStations = Maps.uniqueIndex(weatherStations, Entities.GET_TOPIA_ID::apply);

        getDomain().setDefaultWeatherStation(null);

        if (this.weatherStations != null) {
            for (WeatherStationDto weatherStationDto : this.weatherStations) {
                String topiaId = weatherStationDto.getTopiaId();
                WeatherStation weatherStation;

                if (Strings.isNullOrEmpty(topiaId)) {
                    weatherStation = domainService.newWeatherStation();
                } else {
                    weatherStation = indexWeatherStations.get(topiaId);
                }

                if (weatherStation != null) {
                    String refStationId = weatherStationDto.getRefStationMeteoTopiaId();
                    RefStationMeteo refStation = referentialService.findRefStationMeteoByTopiaId(refStationId);
                    WeatherStations.dtoToWeatherStation(weatherStationDto, weatherStation, refStation);

                    if (StringUtils.isBlank(topiaId)) {
                        weatherStations.add(weatherStation);
                    }

                    nonDeleted.add(weatherStation);

                    if (LOGGER.isErrorEnabled()) {
                        String domainId = getLogEntityId(getDomain());
                        LOGGER.error(
                                String.format("on domain with id '%s', adding weatherStation : ID : %s, %s",
                                        domainId, weatherStation.getTopiaId(), ReflectionToStringBuilder.toString(weatherStationDto)));
                    }

                    // sets the default weather station
                    if (weatherStationDto.isDefaultSelected()) {
                        getDomain().setDefaultWeatherStation(weatherStation);
                    }
                }
            }
        }

        weatherStations.retainAll(nonDeleted);
    }

    /**
     * Get all the type that a a domain could be.
     *
     * @return all the type
     */
    public Map<DomainType, String> getTypes() {
        return i18nService.getEnumTranslationMap(DomainType.class);
    }

    /**
     * Get all the agrosystInterventionType
     *
     * @return all the agrosystInterventionType
     */
    public Map<AgrosystInterventionType, String> getAgrosystInterventionTypes() {
        Map<AgrosystInterventionType, String> map = referentialService.getAgrosystInterventionTypeTranslationMap();
        return map;
    }

    /**
     * Get all the VulnerableArea that a a domain could be.
     *
     * @return all the VulnerableArea
     */
    public Map<Zoning, String> getZoningValues() {
        return getEnumAsMap(Zoning.values());
    }

    public String getFormatedDepartement() {
        if (StringUtils.isBlank(departement)) {
            return "";
        }
        String depNumber = Strings.padStart(departement, 2, '0');
        String key = "departement." + depNumber;
        String result = getText(key);
        result = result + " (" + depNumber + ")";
        return result;
    }
    
    public String getFormatedPetiteRegionAgricoleName() {
        if (StringUtils.isBlank(petiteRegionAgricoleName)) {
            return "";
        }
        return petiteRegionAgricoleName + " (" + petiteRegionAgricole + ")";
    }

    public Map<String, Boolean> getCroppingPlansUsageMap() {
        if (croppingPlansUsageList != null) {
            return croppingPlansUsageList.getUsageMap();
        } else {
            return new HashMap<>();
        }
    }

    public Map<AgrosystInterventionType, String> getAgrosystInterventionType() {
        return referentialService.getAgrosystInterventionTypeTranslationMap();
    }

    public Map<PriceUnit, String> getPriceUnits() {
        return getEnumAsMap(PriceUnit.values());
    }

    public Map<String, Boolean> getCroppingPlanSpeciesUsageMap() {
        if (croppingPlanSpeciesUsageMap == null) {
            croppingPlanSpeciesUsageMap = new HashMap<>();
        }
        return croppingPlanSpeciesUsageMap;
    }

    public Map<MaterielWorkRateUnit, String> getMaterielWorkRateUnits() {
        return getEnumAsMap(MaterielWorkRateUnit.values());
    }

    public Map<MaterielTransportUnit, String> getMaterielTransportUnits() {
        return getEnumAsMap(MaterielTransportUnit.values());
    }
    
    public List<GeoPoint> convertGeoPointsJson(String gpsDataDtos) {
        Type type = new TypeToken<List<GeoPoint>>() {
        }.getType();
        return getGson().fromJson(gpsDataDtos, type);
    }
    
    
    public Collection<WeatherStationDto> convertWeatherStationsJson(String json) {
        Type type = new TypeToken<List<WeatherStationDto>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }

    public List<LivestockUnit> convertLivestockUnitsJson(String json) {
        List<LivestockUnit> livestockUnits = null;
        if (StringUtils.isNotBlank(json)) {
            Type type = new TypeToken<List<LivestockUnit>>() {
            }.getType();
            livestockUnits = getGson().fromJson(json, type);
        }
        return livestockUnits;
    }

    public List<DomainInputDto> convertDomainInputStockUnitsJson(String json) {
        List<DomainInputDto> domainInputStockUnits = null;
        if (StringUtils.isNotBlank(json)) {
            Type type = new TypeToken<List<DomainInputDto>>() {
            }.getType();
            domainInputStockUnits = getGson().fromJson(json, type);
        }
        return domainInputStockUnits;
    }

    public List<Ground> convertGroundsJson(String json) {
        Type type = new TypeToken<List<Ground>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }
    
    public List<Equipment> convertEquipmentsJson(String json) {
        Type type = new TypeToken<List<Equipment>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }
    
    public List<ToolsCoupling> convertToolsCouplingsJson(String json) {
        Type type = new TypeToken<List<ToolsCoupling>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }
    
    public List<CroppingPlanEntryDto> convertCroppingPlansJson(String json) {
        Type type = new TypeToken<List<CroppingPlanEntryDto>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }
    
    public String getDomainSiret() {
        String result = null;
        if ( StringUtils.isNotBlank(getDomain().getSiret())) {
            String siret = StringUtils.replace(getDomain().getSiret(), " ", "");
            result = siret.substring(0,3);
            result += " " + siret.substring(3,6);
            result += " " + siret.substring(6,9);
            result += " " + siret.substring(9,14);
        }
        return result;
    }

    public Map<String, String> getCountries() {
        if (countries == null) {
            countries = domainService.getCountries();
        }
        return countries;
    }

    public String getDefaultCountryTopiaId() {
        return domainService.getDefaultCountryTopiaId();
    }

    public String getFranceTopiaId() {
        return domainService.getFranceRefCountry().getTopiaId();
    }

    public String getDomainLocationCommune() {
        RefLocation location = domain.getLocation();
        if (location == null) {
            return "";
        }
        if (location.getCodePostal() != null) {
            return location.getCodePostal() + ", " + location.getCommune();
        } else {
            return location.getCommune();
        }
    }

    public void setAllRegularPricesJson(String allRegularPricesJson) {
        this.allRegularPricesJson = allRegularPricesJson;
        this.harvestingPriceDtos = convertAllRegularPricesJson(allRegularPricesJson);
    }

    private List<HarvestingPriceDto> convertAllRegularPricesJson(String json) {
        Type type = new TypeToken<List<HarvestingPriceDto>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }

    private String sanitizeSpeciesToAreaJson(String json) {
        return domainService.sanitizedDomainSpeciesToAreaJson(json);
    }
}
