package fr.inra.agrosyst.web.actions.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.Set;

/**
 * Désactivation multiple d'utilisateurs.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class UsersUnactivate extends AbstractAdminAction {

    @Serial
    private static final long serialVersionUID = 5338924873719324729L;

    protected transient UserService userService;

    protected Set<String> userIds;

    protected boolean activate;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUserIds(String userIds) {
        this.userIds = getGson().fromJson(userIds, Set.class);
    }

    public void setActivate(boolean activate) {
        this.activate = activate;
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {
            "namespace", "/security", "actionName", "users-list"})})
    public String execute() throws Exception {
        checkIsAdmin();
        userService.unactivateUsers(userIds, activate);
        return SUCCESS;
    }

}
