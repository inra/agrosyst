/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.services.report.ReportRegionalFilter;
import fr.inra.agrosyst.api.services.report.ReportService;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.List;

/**
 * Liste des bilans de campagne regionale.
 */
public class ReportRegionalsList extends AbstractReportAction {
    
    @Serial
    private static final long serialVersionUID = -865280158085692199L;
    
    protected ReportRegionalFilter filter;

    protected transient ReportService reportService;

    protected PaginationResult<ReportRegional> reportRegionals;
    
    protected List<String> relatedReportGrowingSystemNames;

    protected int reportRegionalsExportAsyncThreshold;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    @Override
    public String execute() throws Exception {
        filter = new ReportRegionalFilter();
        filter.setNavigationContext(getNavigationContext());
        filter.setPageSize(getListNbElementByPage(ReportRegional.class));

        reportRegionals = reportService.getAllReportRegional(filter);
        reportRegionalsExportAsyncThreshold = config.getReportRegionalsExportAsyncThreshold();
        return SUCCESS;
    }

    public ReportRegionalFilter getFilter() {
        return filter;
    }

    public PaginationResult<ReportRegional> getReportRegionals() {
        return reportRegionals;
    }
    
    public List<String> getRelatedReportGrowingSystemNames() {
        return relatedReportGrowingSystemNames;
    }
    
    public void setRelatedReportGrowingSystemNames(List<String> relatedReportGrowingSystemNames) {
        this.relatedReportGrowingSystemNames = relatedReportGrowingSystemNames;
    }

    public int getReportRegionalsExportAsyncThreshold() {
        return reportRegionalsExportAsyncThreshold;
    }
}
