package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.measure.ChemicalElement;
import fr.inra.agrosyst.api.entities.measure.HorizonType;
import fr.inra.agrosyst.api.entities.measure.MeasureType;
import fr.inra.agrosyst.api.entities.measure.Measurement;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.measure.NitrogenMolecule;
import fr.inra.agrosyst.api.entities.measure.ProductivityType;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.services.measurement.MeasurementService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Action used to list and edit all {@link MeasurementSession} for a given {@link Zone}.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class EffectiveMeasurementsEdit extends AbstractAgrosystAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(EffectiveMeasurementsEdit.class);

    @Serial
    private static final long serialVersionUID = 6018591779575353579L;

    protected transient MeasurementService measurementService;

    protected transient PlotService plotService;

    protected String zoneTopiaId;

    protected Zone zone;

    protected Set<CroppingPlanEntry> croppingPlanEntries;

    protected List<MeasurementSession> measurementSessions;

    protected List<RefSupportOrganeEDI> supportOrganeEdis;

    protected List<RefActaSubstanceActive> substanceActives;

    protected List<RefAdventice> adventices;

    /** Zone related to current zone for navigation. */
    protected LinkedHashMap<Integer, String> relatedZones;

    public void setMeasurementService(MeasurementService measurementService) {
        this.measurementService = measurementService;
    }

    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    public String getZoneTopiaId() {
        return zoneTopiaId;
    }

    public void setZoneTopiaId(String zoneTopiaId) {
        this.zoneTopiaId = zoneTopiaId;
    }

    public Zone getZone() {
        return zone;
    }

    @Override
    public void prepare() {
        zone = measurementService.getZone(zoneTopiaId);

        activated = zone.isActive() && zone.getPlot().isActive() && zone.getPlot().getDomain().isActive()
                && (zone.getPlot().getGrowingSystem() == null || zone.getPlot().getGrowingSystem().isActive());
    }

    @Override
    @Action("effective-measurements-edit-input")
    public String input() throws Exception {
        initForInput();

        measurementSessions = measurementService.getZoneMeasurementSessions(zone);

        return INPUT;
    }

    @Override
    protected void initForInput() {
        croppingPlanEntries = measurementService.getZoneCroppingPlanEntries(zone);
        supportOrganeEdis = measurementService.findAllSupportOrganeEDI();
        substanceActives = measurementService.findDistinctSubstanceActives();
        adventices = measurementService.findAllAdventices();
        relatedZones = plotService.getRelatedZones(zone.getCode());
    }

    public Map<MeasurementType, String> getMeasurementMeasureTypes() {
        return getEnumAsMap(MeasurementType.PLANTE,
                        MeasurementType.SOL,
                        MeasurementType.TRANSFERT_DE_SOLUTES,
                        MeasurementType.GES,
                        MeasurementType.METEO);
    }

    public Map<MeasurementType, String> getMeasurementObservationTypes() {
        return getEnumAsMap(
                        MeasurementType.STADE_CULTURE,
                        MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES,
                        MeasurementType.ADVENTICES);
    }

    public Map<VariableType, String> getVariableTypes() {
        return getEnumAsMap(VariableType.values());
    }

    public Map<MeasureType, String> getMeasureTypes() {
        return getEnumAsMap(MeasureType.values());
    }

    public Map<ProductivityType, String> getProductivityTypes() {
        return getEnumAsMap(ProductivityType.values());
    }

    public Map<ChemicalElement, String> getChemicalElements() {
        return getEnumAsMap(ChemicalElement.values());
    }
    
    public Map<HorizonType, String> getHorizonTypes() {
        return getEnumAsMap(HorizonType.values());
    }
    
    public Map<NitrogenMolecule, String> getNitrogenMolecules() {
        return getEnumAsMap(NitrogenMolecule.values());
    }

    @Override
    public void validate() {
        if (measurementSessions != null) {
            for (MeasurementSession measurementSession : measurementSessions) {
                if (measurementSession.getStartDate() == null) {
                    addActionError("Une serie de mesures doit avoir une date de début !");
                }
                Collection<Measurement> measurements = measurementSession.getMeasurements();
                if (measurements != null) {
                    for (Measurement measurement : measurements) {
                        if (measurement != null) {
                            if (measurement.getMeasurementType() == null) {
                                addActionError("Une mesure ou observation doit avoir un type !");
                            }
                        }
                    }
                }
            }

            if (!activated) {
                addActionError("La zone et/ou sa parcelle et/ou son system de culture et/ou son dispositif et/ou son domain sont inactifs !");
            }
        }
        //measurementSessions
        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : Mesures et observations de la zone:'%s' -> %s", zoneTopiaId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : Mesures et observations de la zone:'%s' -> %s", zoneTopiaId, getFieldErrors().toString()));
            }

            initForInput();
        }
    }

    @Override
    @Action(results = {@Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "effective-measurements-edit-input", "zoneTopiaId", "${zoneTopiaId}"})})
    public String execute() throws Exception {

        measurementService.updateMeasurementSessions(zone, measurementSessions);

        return SUCCESS;
    }

    public List<MeasurementSession> getMeasurementSessions() {
        return measurementSessions;
    }

    public void setMeasurementSessions(String json) {
        Type type = new TypeToken<List<MeasurementSession>>() {}.getType();
        measurementSessions = getGson().fromJson(json, type);
    }

    public Set<CroppingPlanEntry> getCroppingPlanEntries() {
        return croppingPlanEntries;
    }

    public List<RefSupportOrganeEDI> getSupportOrganeEdis() {
        return supportOrganeEdis;
    }

    public List<RefActaSubstanceActive> getSubstanceActives() {
        return substanceActives;
    }

    public List<RefAdventice> getAdventices() {
        return adventices;
    }
    
    public LinkedHashMap<Integer, String> getRelatedZones() {
        return relatedZones;
    }
}
