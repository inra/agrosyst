package fr.inra.agrosyst.web.actions.growingsystems;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.Status;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.practiced.OperationStatus;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.List;

/**
 * Validation d'un SdC.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 0.8
 */
public class ValidateGrowingSystems extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -6811793980746461606L;

    protected transient GrowingSystemService growingSystemService;

    protected List<String> growingSystemIds;
    
    protected boolean validate;

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "growing-systems-list"})})
    public String execute() throws Exception {
    
    
        List<OperationStatus> results = growingSystemService.validate(growingSystemIds, validate);
        
        for (OperationStatus result : results) {
            
            if (Status.SUCCES == result.getStatus()) {
                notificationSupport.growingSystemValidated(result.getComment());
            } else {
                notificationSupport.growingSystemValidatedError(result.getComment());
            }
        }
        return SUCCESS;
    }
    
    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setGrowingSystemIds(String growingSystemIds) {
        this.growingSystemIds = getGson().fromJson(growingSystemIds, List.class);
    }
    
    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
