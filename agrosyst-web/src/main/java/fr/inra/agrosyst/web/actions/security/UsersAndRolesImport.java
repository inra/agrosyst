package fr.inra.agrosyst.web.actions.security;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.admin.AbstractAdminAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.action.UploadedFilesAware;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.dispatcher.multipart.UploadedFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serial;
import java.util.List;

/**
 * Import d'utilisateur par lot.
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Results({
        @Result(type = "redirectAction", name = "input", params = {"namespace", "/security", "actionName", "users-list", "importUsersAndRolesFileError", "true"})
})
public class UsersAndRolesImport extends AbstractAdminAction implements UploadedFilesAware {

    private static final Log LOGGER = LogFactory.getLog(UsersAndRolesImport.class);

    @Serial
    private static final long serialVersionUID = 5338924873719324729L;

    protected transient UserService userService;

    protected File userFile;

    protected File rolesFile;

    public void setRolesFile(File rolesFile) {
        this.rolesFile = rolesFile;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    public void setUserFile(File userFile) {
        this.userFile = userFile;
    }

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"namespace", "/security", "actionName", "users-list"}),
            @Result(name = ERROR, type = "redirectAction", params = {"namespace", "/security", "actionName", "users-list"})
    })
    public String execute() throws Exception {
        boolean errors = false;
        if (userFile != null) {
            checkIsAdmin();
            try {
                ImportResult result = userService.importUsers(new FileInputStream(userFile));
                notificationSupport.userImported(result);
            } catch (Exception ex) {
                errors = true;
                notificationSupport.usersImportFailed(ex.getMessage());
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Can't import user file", ex);
                }
            }
        }
        if (rolesFile != null) {
            checkIsAdmin();
            try {
                ImportResult result = authorizationService.importRoles(new FileInputStream(rolesFile));
                notificationSupport.userRolesUpdated(result);
            } catch (Exception ex) {
                errors = true;
                notificationSupport.rulesImportFailed(ex.getMessage());
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error("Can't import roles file", ex);
                }
            }
        }
        if (errors) {
            return ERROR;
        } else {
            return SUCCESS;
        }
    }

    @Override
    public void withUploadedFiles(List<UploadedFile> uploadedFiles) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("L'utilisateur %s à soumis %d ficher de rôles ou utilisateurs", getAuthenticatedUser().getEmail(), CollectionUtils.emptyIfNull(uploadedFiles).size()));
        }
    }
}
