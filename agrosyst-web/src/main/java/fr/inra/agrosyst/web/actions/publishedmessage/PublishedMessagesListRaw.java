package fr.inra.agrosyst.web.actions.publishedmessage;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.history.Message;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;

import java.io.Serial;
import java.util.ArrayList;

/**
 * Created by davidcosse on 16/10/14.
 */
public class PublishedMessagesListRaw extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 1L;

    protected ArrayList<Message> broadcastMessages;

    @Override
    public String execute() throws Exception {
        broadcastMessages = new ArrayList<>(messageService.getMessagesFromDate(null));
        return SUCCESS;
    }

    @Override
    public ArrayList<Message> getBroadcastMessages() {
        return broadcastMessages;
    }
}
