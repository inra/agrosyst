package fr.inra.agrosyst.web.actions.performances;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.struts2.convention.annotation.Action;

import java.io.Serial;

public class PerformanceFileRequest extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -2052003579744460897L;

    protected transient PerformanceService performanceService;

    protected String performanceId;

    public void setPerformanceService(PerformanceService performanceService) {
        this.performanceService = performanceService;
    }

    @Action("performance-file-request")
    public String listCroppingPlanEntries() {
        jsonData = performanceService.getPerformanceStatus(performanceId);
        return SUCCESS;
    }

    public void setPerformanceId(String performanceId) {
        this.performanceId = performanceId;
    }
}
