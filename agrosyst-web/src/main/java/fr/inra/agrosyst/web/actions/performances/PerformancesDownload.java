package fr.inra.agrosyst.web.actions.performances;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.net.MediaType;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.BinaryStream;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;

/**
 * Téléchargement des fichiers de performances stockés en base de données.
 *
 * Attention:
 *  - jusqu'à la version 2.6.5, les fichiers en base sont en XLS
 *  - depuis ils sont en XLSX
 */
public class PerformancesDownload extends AbstractAgrosystAction {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -2944160203634155778L;

    private static final Log LOGGER = LogFactory.getLog(PerformancesDownload.class);

    @Setter
    protected transient PerformanceService performanceService;

    @Setter
    protected String performanceTopiaId;

    protected transient BinaryStream binaryStream;

    @Override
    @Action(results= {@Result(type="stream", params={"contentType", "${mimetype}",
            "inputName", "inputStream", "contentDisposition", "attachment; filename=\"${filename}\""})})
    public String execute() throws Exception {
        try {
            binaryStream = performanceService.downloadPerformances(performanceTopiaId);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't generate xsl", ex);
            }
            throw new AgrosystTechnicalException("Can't create input stream", ex);
        }
        return SUCCESS;
    }

    public InputStream getInputStream() {
        return binaryStream.getStream();
    }

    public String getMimetype() {
        String mimetype;
        if (binaryStream.getType() == BinaryStream.FileType.XLS) {
            mimetype = "aapplication/vnd.ms-excel";
        } else {
            mimetype = MediaType.OOXML_SHEET.toString();
        }
        return mimetype;
    }

    public String getFilename() {
        String filename = "performance.xls";
        if (binaryStream.getType() == BinaryStream.FileType.XLSX) {
            filename += "x";
        }
        return filename;
    }
}
