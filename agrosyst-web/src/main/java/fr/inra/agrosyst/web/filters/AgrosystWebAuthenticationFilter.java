package fr.inra.agrosyst.web.filters;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA
 * Copyright (C) 2020 INRAE
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.google.common.collect.ImmutableSet;
import fr.inra.agrosyst.api.services.security.AuthenticationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AgrosystServiceConfig;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.web.AgrosystWebApplicationContext;
import fr.inra.agrosyst.web.AgrosystWebConfig;
import fr.inra.agrosyst.web.CookieHelper;
import fr.inra.agrosyst.web.actions.security.JwtHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystWebAuthenticationFilter implements Filter {

    private static final Log LOGGER = LogFactory.getLog(AgrosystWebAuthenticationFilter.class);

    public static final String AGROSYST_WEB_LOGIN_ACTION = "/auth/login.action";
    public static final String AGROSYST_WEB_LOGIN_ACTION_INPUT = "/auth/login-input.action";

    public static final String AGROSYST_WEB_CHARTER_ACTION = "/auth/charter.action";
    public static final String AGROSYST_WEB_CHARTER_ACTION_INPUT = "/auth/charter-input.action";

    public static final String IPMWORKS_WEB_LOGIN_ACTION = "/ipmworks/auth/login.action";
    public static final String IPMWORKS_WEB_LOGIN_ACTION_INPUT = "/ipmworks/auth/login-input.action";

    protected static final Set<String> PUBLIC_URLS = ImmutableSet.of(
            AGROSYST_WEB_LOGIN_ACTION,
            AGROSYST_WEB_LOGIN_ACTION_INPUT,
            "/auth/login-json.action",
            AGROSYST_WEB_CHARTER_ACTION,
            AGROSYST_WEB_CHARTER_ACTION_INPUT,
            "/auth/forgotten-password.action",
            "/auth/forgotten-password-input.action",
            "/auth/forgotten-password-json.action",
            "/auth/retrieve-password.action",
            "/auth/retrieve-password-input.action",
            "/auth/logout.action",
            "/auth/logout-json.action",
            "/auth/legal.action",
            "/commons/help-raw.action",
            "/commons/endpoints.action",
            "/admin/status-json.action",
            IPMWORKS_WEB_LOGIN_ACTION,
            IPMWORKS_WEB_LOGIN_ACTION_INPUT
    );

    protected static final Set<String> STATIC_RESOURCES = ImmutableSet.of(
            "/favicon.ico",
            "/ipmworks/favicon.ico",
            "/js/",
            "/img/",
            "/help/",
            "/font/",
            "/webjars/",
            "/nuiton-js/"
    );

    public static final String AUTHENTICATED_USER = "authenticatedUser";

    protected static final Function<HttpServletRequest, String> GET_FULL_REQUESTED_URI = input -> {
        String contextPath = input.getContextPath();
        String requestURI = input.getRequestURI();
        if (requestURI.startsWith("/context/")
                || requestURI.startsWith(contextPath + "/js/") || requestURI.startsWith(contextPath + "/help/")
                || requestURI.startsWith(contextPath + "/img/") || requestURI.startsWith(contextPath + "/font/")
                || requestURI.startsWith(contextPath + "/webjars/") || requestURI.startsWith(contextPath + "/nuiton-js/")) {
            requestURI = contextPath + "/";
        }
        String queryString = input.getQueryString();
        String result;
        if (queryString == null) {
            result = requestURI;
        } else {
            result = String.format("%s?%s", requestURI, queryString);
        }
        return result;
    };

    protected static final Function<HttpServletRequest, String> GET_REDIRECT_TO_LOGIN_PAGE_URI = input -> {
        String contextPath = input.getContextPath();
        String servletPath = input.getServletPath();
        String result = "";
        if (!"/".equals(contextPath)) {
            result += contextPath;
        }
        if (servletPath.startsWith("/ipmworks")) {
            result += IPMWORKS_WEB_LOGIN_ACTION_INPUT;
        } else {
            result += AGROSYST_WEB_LOGIN_ACTION_INPUT;
        }
        return result;
    };

    protected static final Function<HttpServletRequest, String> GET_REDIRECT_TO_CHARTER_PAGE_URI = input -> {
        String contextPath = input.getContextPath();
        String result;
        if ("/".equals(contextPath)) {
            result = AGROSYST_WEB_CHARTER_ACTION_INPUT;
        } else {
            result = contextPath + AGROSYST_WEB_CHARTER_ACTION_INPUT;
        }
        return result;
    };

    protected AgrosystWebApplicationContext webApplicationContext;

    @Override
    public void init(FilterConfig filterConfig) {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Initializing " + AgrosystWebAuthenticationFilter.class.getName());
        }

        ServletContext servletContext = filterConfig.getServletContext();
        webApplicationContext = (AgrosystWebApplicationContext) servletContext
                .getAttribute(AgrosystWebApplicationContext.APPLICATION_CONTEXT_PARAMETER);
    }

    protected Optional<AuthenticatedUser> refreshIfNecessary(Optional<AuthenticatedUser> authenticatedUserOptional,
                                                   Optional<String> jwtToken,
                                                   JwtHelper jwtHelper,
                                                   CookieHelper cookieHelper) {

        Optional<AuthenticatedUser> result = authenticatedUserOptional;

        if (authenticatedUserOptional.isPresent() && jwtToken.isPresent()) {
            Date issuedAt = jwtHelper.getTokenIssuedAt(jwtToken.get());
            Duration tokenAge = Duration.between(issuedAt.toInstant(), new Date().toInstant());
            int refreshSeconds = webApplicationContext.getWebConfig().getJwtRefreshSeconds();

            if (tokenAge.toSeconds() > refreshSeconds) {
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info(String.format("Token émis depuis plus de %d secondes, on va la rafraîchir", refreshSeconds));
                }
                String userId = authenticatedUserOptional.get().getTopiaId();
                String sid = authenticatedUserOptional.get().getSid();
                result = loadUserDto(userId, sid);
                result.ifPresent(user -> webApplicationContext.writeAuthenticationCookie(user, cookieHelper));
            }
        }

        return result;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest httpServletRequest && response instanceof HttpServletResponse httpServletResponse) {
    
            // Authorize (allow) all domains to consume the content
            List<String> allowedRestRequestOrigins = webApplicationContext.getWebConfig().getAllowedRestRequestOrigins().getOption();
            String origin = httpServletRequest.getHeader("origin");
            if (allowedRestRequestOrigins.contains(origin)) {
                httpServletResponse.addHeader("Access-Control-Allow-Origin", origin);
                httpServletResponse.addHeader("Access-Control-Allow-Credentials", "true");
            }

            String servletPath = httpServletRequest.getServletPath();

            // public action requested
            boolean publicAction = PUBLIC_URLS.contains(servletPath);
            // static resource
            boolean staticResource = STATIC_RESOURCES.stream().anyMatch(servletPath::startsWith);

            boolean publicUrl = publicAction || staticResource;

            boolean jsonRequest = servletPath.endsWith("json.action");

            boolean requestContextWhenNotLogin = httpServletRequest.getRequestURI().startsWith("/context/");
            boolean requestAdminAction = httpServletRequest.getRequestURI().startsWith("/admin/");

            Optional<AuthenticatedUser> authenticatedUser = Optional.empty();
            // S'il ne s'agit pas d'une ressource statique, on charge les informations sur l'utilisateur
            if (!staticResource) {
                try {
                    AgrosystWebConfig webConfig = webApplicationContext.getWebConfig();
                    JwtHelper jwtHelper = new JwtHelper(webConfig);
                    CookieHelper cookieHelper = new CookieHelper(httpServletRequest, httpServletResponse, webConfig);
                    Optional<String> jwtToken = webApplicationContext.readAuthenticationCookie(cookieHelper);
                    authenticatedUser = jwtToken.map(jwtHelper::verifyJwtToken);

                    if (!publicUrl) { // On évite de rafraîchir le token pour les URLs publiques
                        authenticatedUser = refreshIfNecessary(authenticatedUser, jwtToken, jwtHelper, cookieHelper);
                    }

                    authenticatedUser.ifPresent(userDto -> httpServletRequest.setAttribute(AUTHENTICATED_USER, userDto));

                } catch (TokenExpiredException tee) {
                    LOGGER.warn("Token expiré : " + tee.getMessage());
                } catch (SignatureVerificationException sve) {
                    LOGGER.warn("Signature du token invalide : " + sve.getMessage());
                } catch (JWTDecodeException jde) {
                    LOGGER.warn("Token invalide : " + jde.getMessage());
                }
            }

            boolean authenticated = authenticatedUser.isPresent();

            // On vérifie s'il est nécessaire de déconnecter l'utilisateur (mode maintenance)
            if (authenticated && !publicUrl && !authenticatedUser.map(AuthenticatedUser::isAdmin).get()) {
                AgrosystServiceConfig serviceConfig = webApplicationContext.getServiceConfig();
                if (serviceConfig.isMaintenanceModeActive() && serviceConfig.isMaintenanceModeDisconnectAllUsers()) {
                    authenticated = false;
                }
            }

            // check if user has validate Charter
            boolean acceptedCharter = authenticatedUser.isEmpty() || authenticatedUser.map(AuthenticatedUser::isAcceptedCharter).get();

            if (LOGGER.isTraceEnabled()) {
                String message = String.format("Is '%s' [publicUrl=%b] [authenticated=%b] [acceptedCharter=%b]",
                        servletPath, publicUrl, authenticated, acceptedCharter);
                LOGGER.trace(message);
            }

            if (publicUrl) {
                chain.doFilter(request, response);
            } else if (authenticated && acceptedCharter) {
                chain.doFilter(request, response);
            } else if (!authenticated && (jsonRequest || requestContextWhenNotLogin || requestAdminAction)) {
                failWithUnauthorizedError(httpServletResponse);
            } else if (!authenticated) {
                redirectToLoginPage(httpServletRequest, httpServletResponse);
            } else {
                redirectToCharterPage(httpServletRequest, httpServletResponse);
            }

        }
    }

    protected Optional<AuthenticatedUser> loadUserDto(String userId, String sid) {
        try (ServiceContext serviceContext = webApplicationContext.getApplicationContext().newServiceContext()) {

            AuthenticationService authenticationService = serviceContext.newService(AuthenticationService.class);
            AuthenticatedUser newUserDto = authenticationService.getAuthenticatedUserFromUserId(userId, sid);
            return Optional.of(newUserDto);
        } catch (Exception eee) {
            LOGGER.warn("Unable to close serviceContext", eee);
        }
        return Optional.empty();
    }

    private void failWithUnauthorizedError(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Vous n'êtes plus connecté");
    }

    private void redirectToLoginPage(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

        String redirectPageUri = GET_REDIRECT_TO_LOGIN_PAGE_URI.apply(httpServletRequest);

        String requestedUri = GET_FULL_REQUESTED_URI.apply(httpServletRequest);
        String queryString = "?next=" + URLEncoder.encode(requestedUri, StandardCharsets.UTF_8);

        httpServletResponse.sendRedirect(redirectPageUri + queryString);

    }

    private void redirectToCharterPage(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

        String redirectPageUri = GET_REDIRECT_TO_CHARTER_PAGE_URI.apply(httpServletRequest);

        String requestedUri = GET_FULL_REQUESTED_URI.apply(httpServletRequest);
        String queryString = "?next=" + URLEncoder.encode(requestedUri, StandardCharsets.UTF_8);

        httpServletResponse.sendRedirect(redirectPageUri + queryString);
    }

    @Override
    public void destroy() {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Destroying " + AgrosystWebAuthenticationFilter.class.getName());
        }
        // Nothing to do
    }

}
