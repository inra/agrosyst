/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;

import java.io.Serial;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Export XLS des bilan de campagne / echelle system de culture.
 */
public class ReportGrowingSystemExportXls extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = 8686993981231582811L;

    protected transient ReportService reportService;

    protected List<String> reportGrowingSystemIds;

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public void setReportGrowingSystemIds(String reportGrowingSystemIds) {
        this.reportGrowingSystemIds = getGson().fromJson(reportGrowingSystemIds, List.class);
    }

    @Override
    protected ExportResult computeExportResult() {
        List<String> safeReportGrowingSystemIds = reportGrowingSystemIds.stream()
                .filter(reportGrowingSystemId -> {
                    try {
                        authorizationService.checkReportGrowingSystemReadable(reportGrowingSystemId);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                })
                .collect(Collectors.toList());

        ExportResult result = reportService.exportXlsReportGrowingSystems(safeReportGrowingSystemIds);
        return result;
    }

}
