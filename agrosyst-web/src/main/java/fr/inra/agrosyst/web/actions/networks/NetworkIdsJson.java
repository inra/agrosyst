package fr.inra.agrosyst.web.actions.networks;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * @author Kevin Morin
 */
public class NetworkIdsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(NetworkIdsJson.class);
    @Serial
    private static final long serialVersionUID = 2940955765978353344L;
    
    protected transient NetworkService networkService;

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }

    protected String filter;

    @Override
    public String execute() {
        try {
            NetworkFilter networkFilter = getGson().fromJson(filter, NetworkFilter.class);
            jsonData = networkService.getFilteredNetworkIds(networkFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load networks", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

}
