package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemFilter;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class PracticedSystemsListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(PracticedSystemsListJson.class);

    @Serial
    private static final long serialVersionUID = -7042498750101369083L;

    protected transient PracticedSystemService practicedSystemService;

    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }
    
    protected String filter;

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String execute() {
        try {
            PracticedSystemFilter practicedSystemFilter = getGson().fromJson(filter, PracticedSystemFilter.class);
            writeListNbElementByPage(PracticedSystemDto.class, practicedSystemFilter.getPageSize());
            NavigationContext navigationContext = getNavigationContext();
            practicedSystemFilter.setNavigationContext(navigationContext);
            jsonData = practicedSystemService.getFilteredPracticedSystemsDto(practicedSystemFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load practiced Systems", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
}
