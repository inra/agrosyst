package fr.inra.agrosyst.web.actions.domains;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.DomainSummaryDto;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Action de recuperation de la liste paginées des résumés de domaines pour IPMworks
 * 
 * @author Kevin Morin
 */
public class DomainSummaryListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(DomainSummaryListJson.class);
    @Serial
    private static final long serialVersionUID = -3782509535850441876L;
    
    protected transient DomainService domainService;
    protected transient String filter;
    protected transient Boolean pagination = true;
    
    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setPagination(Boolean pagination) {
        this.pagination = pagination;
    }

    @Override
    public String execute() {
        try {
            DomainFilter domainFilter = getGson().fromJson(filter, DomainFilter.class);
            //NavigationContext navigationContext;
            if (!pagination) {
                domainFilter.setAllPageSize();
                domainFilter.setPage(0);
            } else {
                writeListNbElementByPage(DomainSummaryDto.class, domainFilter.getPageSize());
            }
            //domainFilter.setNavigationContext(navigationContext);

            jsonData = domainService.getFilteredDomainSummariesDto(domainFilter);

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load domains list", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }
}
