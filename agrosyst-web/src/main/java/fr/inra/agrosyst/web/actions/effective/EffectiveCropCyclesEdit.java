package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.OrchardFrutalForm;
import fr.inra.agrosyst.api.entities.PollinatorSpreadMode;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.VineFrutalForm;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.referential.CriteriaUnit;
import fr.inra.agrosyst.api.entities.referential.PastureType;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.action.HarvestingActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.CattleDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleModelDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleNodeDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleSpeciesDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycles;
import fr.inra.agrosyst.api.services.effective.EffectiveInterventionDto;
import fr.inra.agrosyst.api.services.effective.EffectivePerennialCropCycleDto;
import fr.inra.agrosyst.api.services.effective.EffectiveSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.web.actions.itk.AbstractItkAction;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class EffectiveCropCyclesEdit extends AbstractItkAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(EffectiveCropCyclesEdit.class);

    /**
     * serialVersionUID.
     */
    @Serial
    private static final long serialVersionUID = 3202026195271703990L;

    protected transient EffectiveCropCycleService effectiveCropCycleService;

    protected transient PlotService plotService;

    protected transient DomainService domainService;
    protected transient DomainInputStockUnitService domainInputStockUnitService;

    protected transient PricesService pricesService;

    /**
     * Topia id de la zone à laquelle sont liés les cycles.
     */
    protected String zoneTopiaId;

    /**
     * Zone à laquelle sont liés les cycles.
     */
    protected Zone zone;
    protected Sector growingSystemSector = null;
    protected Boolean organic = null;

    /**
     * Cycles assolés liés à la parcelle.
     */
    protected List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycles;

    protected String effectiveSeasonalCropCyclesJson;

    /**
     * Cycles pérennes lié à la parcelle.
     */
    protected List<EffectivePerennialCropCycleDto> effectivePerennialCropCycles;
    protected String effectivePerennialCropCyclesJson;

    /**
     * Cultures associées à la zone.
     */
    protected List<CroppingPlanEntry> croppingPlanEntries;
    protected String croppingPlanEntriesJson;

    /**
     * Main cropping plan model entries.
     */
    protected List<EffectiveCropCycleModelDto> effectiveCropCyclesMainModels;

    /**
     * Intermediate cropping plan model entries.
     */
    protected List<EffectiveCropCycleModelDto> effectiveCropCyclesIntermediateModels;

    /**
     * Orientation EDI.
     */
    protected List<RefOrientationEDI> refOrientationEDIs;

    protected String domainId;

    protected int campaign;

    /** Zone related to current zone for navigation. */
    protected LinkedHashMap<Integer, String> relatedZones;

    // utilisé pour la validation des copiers/coller
    private Map<String, List<Pair<String, String>>> allCodeEspeceBotaniqueCodeQualifantBySpeciesCode;
    private Map<String, List<Sector>> allsectorByCodeEspceBotaniqueCodeQualifiant;

    protected Collection<CattleDto> cattles;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }
    
    public void setDomainInputStockUnitService(DomainInputStockUnitService domainInputStockUnitService) {
        this.domainInputStockUnitService = domainInputStockUnitService;
    }
    
    public void setPricesService(PricesService pricesService) {
        this.pricesService = pricesService;
    }

    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }

    public void setEffectiveCropCycleService(EffectiveCropCycleService effectiveCropCycleService) {
        this.effectiveCropCycleService = effectiveCropCycleService;
    }

    @Override
    public void prepare() {
        zone = effectiveCropCycleService.getZone(zoneTopiaId);

        domainId = zone.getPlot().getDomain().getTopiaId();

        campaign = zone.getPlot().getDomain().getCampaign();

        activated = zone.isActive() && zone.getPlot().isActive() && zone.getPlot().getDomain().isActive()
                && (zone.getPlot().getGrowingSystem() == null || zone.getPlot().getGrowingSystem().isActive());
    }

    @Override
    @Action("effective-crop-cycles-edit-input")
    public String input() throws Exception {

        authorizationService.checkEffectiveCropCyclesReadable(zoneTopiaId);
        readOnly = (!authorizationService.isZoneWritable(zoneTopiaId) || domainId == null);
        if (readOnly) {
            notificationSupport.effectiveCropCyclesNotWritable();
        }

        effectivePerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zoneTopiaId);
        effectivePerennialCropCyclesJson = getGson().toJson(CollectionUtils.emptyIfNull(effectivePerennialCropCycles));

        effectiveSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zoneTopiaId);

        // add before node for all seasonal crop cycles
        effectiveSeasonalCropCycles = addNodeBefore(zoneTopiaId, effectiveSeasonalCropCycles);
        effectiveSeasonalCropCyclesJson = getGson().toJson(effectiveSeasonalCropCycles);
    
        warnEdaplosDestinationUsage();

        initForInput();
        return INPUT;
    }
    
    private void warnEdaplosDestinationUsage() {
    
        List<String> cropLabels = new ArrayList<>();
        Map<Integer, Set<String>> cropNameByRank = new HashMap<>();
        
        List<EffectiveCropCycleNodeDto> nodes = effectiveSeasonalCropCycles.stream()
                .map(EffectiveSeasonalCropCycleDto::getNodeDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();

        for (EffectiveCropCycleNodeDto node : nodes) {
            if (node.getInterventions()!=null) {
                
                Collection<AbstractActionDto> harvestingActions = node.getInterventions().stream()
                        .filter(Objects::nonNull)
                        .map(EffectiveInterventionDto::getActionDtos)
                        .filter(Objects::nonNull)
                        .flatMap(Collection::stream)
                        .filter(action -> action instanceof HarvestingActionDto)
                        .toList();

                for (AbstractActionDto harvestingActionDto : harvestingActions) {
                    Set<String> destinationIds = CollectionUtils.emptyIfNull(((HarvestingActionDto) harvestingActionDto).getValorisationDtos())
                            .stream()
                            .filter(Objects::nonNull)
                            .map(HarvestingActionValorisationDto::getDestinationId)
                            .collect(Collectors.toSet());
                    Optional<String> defaultDestination = referentialService.loadDestinationsForIds(destinationIds)
                            .stream()
                            .map(RefDestination::getDestination)
                            .filter(ActionService.DEFAULT_DESTINATION_NAME::equals).findAny();
                    
                    if (defaultDestination.isPresent()) {
                        Optional<String> effectiveInterventionId = harvestingActionDto.getEffectiveInterventionId();
                        if (effectiveInterventionId.isPresent()) {
                            EffectiveIntervention intervention = effectiveCropCycleService.getEffectiveInterventionWithGivenId(effectiveInterventionId.get());
                            String cropLabel = intervention.isIntermediateCrop() ? node.getIntermediateCropLabel() + " (CI Rang %d) " : node.getLabel() + " (Rang %d) ";
                            final int rank = node.getX() + 1;
                            cropLabel = String.format(cropLabel, rank);

                            Set<String> cropNamesForRank = cropNameByRank.computeIfAbsent(rank, k -> new HashSet<>());
                            cropNamesForRank.add(cropLabel);
                        }
                    }
                }
            }

        }
        
        if (!cropNameByRank.isEmpty()) {
            final List<Integer> odoredRank = cropNameByRank.keySet()
                    .stream()
                    .sorted(Integer::compareTo)
                    .toList();
            for (Integer rank : odoredRank) {
                List<String> cropNames = new ArrayList<>(cropNameByRank.get(rank));
                cropNames.sort(String::compareTo);
                cropLabels.addAll(cropNames);
            }
        }
    
        List<String> perennialCropNames = new ArrayList<>();
        for (EffectivePerennialCropCycleDto perennialCropCycle : effectivePerennialCropCycles) {
            if (CollectionUtils.isEmpty(perennialCropCycle.getPhaseDtos())){
                continue;
            }

            Optional<String> defaultDestination = getDefaultDestination(perennialCropCycle);

            if(defaultDestination.isPresent()) {
                perennialCropNames.add(perennialCropCycle.getCroppingPlanEntryName());
            }

            defaultDestination = getDefaultDestination(perennialCropCycle);

            if(defaultDestination.isPresent()) {
                perennialCropNames.add(perennialCropCycle.getCroppingPlanEntryName());
            }
        }

        if (!perennialCropNames.isEmpty()) {
            perennialCropNames.sort(String::compareTo);
            cropLabels.addAll(perennialCropNames);
        }

        if (!cropLabels.isEmpty()) {
            notificationSupport.warning(String.format(WRONG_DESTINATION_MESSAGE, String.join(", ", cropLabels)));
        }
    }
    
    private static Optional<String> getDefaultDestination(EffectivePerennialCropCycleDto perennialCropCycle) {
        Optional<String> defaultDestination = perennialCropCycle.getPhaseDtos().stream()
                .filter(Objects::nonNull)
                .map(EffectiveCropCyclePhaseDto::getInterventions)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(EffectiveInterventionDto::getActionDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .filter(actionDto -> actionDto instanceof HarvestingActionDto)
                .filter(Objects::nonNull)
                .map(actionDto -> ((HarvestingActionDto)actionDto).getValorisationDtos())
                .flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(HarvestingActionValorisationDto::getDestinationName)
                .filter(ActionService.DEFAULT_DESTINATION_NAME::equals)
                .findAny();
        return defaultDestination;
    }
    
    
    protected List<EffectiveSeasonalCropCycleDto> addNodeBefore(String zoneTopiaId, List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycles) {
        CroppingPlanEntry beforeNodeCroppingPlanEntry = effectiveCropCycleService.getPreviousCampaignCroppingPlanEntry(zoneTopiaId);

        EffectiveCropCycleNodeDto previousNodeCroppingPlanEntry = new EffectiveCropCycleNodeDto();
        String beforeNodeLabel = beforeNodeCroppingPlanEntry != null ? beforeNodeCroppingPlanEntry.getName() : "";
        previousNodeCroppingPlanEntry.setLabel(beforeNodeLabel);
        previousNodeCroppingPlanEntry.setNodeId(EffectiveCropCycleNodeDto.NODE_BEFORE_ID);
        previousNodeCroppingPlanEntry.setType(EffectiveCropCycleNodeDto.NODE_BEFORE_ID);

        // fix no yet cycle defined
        if (CollectionUtils.isEmpty(effectiveSeasonalCropCycles)) {
            EffectiveSeasonalCropCycleDto cropCycle = new EffectiveSeasonalCropCycleDto();
            cropCycle.setNodeDtos(new ArrayList<>());
            cropCycle.getNodeDtos().add(previousNodeCroppingPlanEntry);
            cropCycle.setConnectionDtos(new ArrayList<>());
            effectiveSeasonalCropCycles = Lists.newArrayList(cropCycle);
        } else {
            // prepend node into cycles
            for (EffectiveSeasonalCropCycleDto nodedto : effectiveSeasonalCropCycles) {
                nodedto.getNodeDtos().add(0, previousNodeCroppingPlanEntry);
                // link "null" source connection to BEFORE NODE (automatic)
                for (EffectiveCropCycleConnectionDto conndto : CollectionUtils.emptyIfNull(nodedto.getConnectionDtos())) {
                    if (conndto.getSourceId() == null) {
                        conndto.setSourceId(previousNodeCroppingPlanEntry.getNodeId());
                    }
                }
            }
        }

        return effectiveSeasonalCropCycles;
    }

    @Override
    protected void initForInput() {

        if (this.croppingPlanEntries == null) {
            this.croppingPlanEntries = domainId != null ? effectiveCropCycleService.getZoneCroppingPlanEntriesWithoutDomain(zone) : new ArrayList<>();
            croppingPlanEntriesJson = getGson().toJson(this.croppingPlanEntries);
        }

        GrowingSystem growingSystem = zone.getPlot().getGrowingSystem();
        if (growingSystem != null) {
            growingSystemSector = growingSystem.getSector();
            organic = growingSystem.getTypeAgriculture() == null ? null : growingSystem.getTypeAgriculture().getReference_id() == ReferentialService.ORGANIC_REF_TYPE_CONDUITE_REFERENCE_ID;
        }

        // build main and intermediate cropping plan entry map
        effectiveCropCyclesMainModels = new ArrayList<>();
        effectiveCropCyclesIntermediateModels = new ArrayList<>();
        for (CroppingPlanEntry croppingPlanEntry : this.croppingPlanEntries) {
            EffectiveCropCycleModelDto effectiveCropCycleModelDto = EffectiveCropCycles.CROPPING_PLAN_ENTRY_TO_DTO.apply(croppingPlanEntry);
            if (effectiveCropCycleModelDto.isIntermediate()) {
                effectiveCropCyclesIntermediateModels.add(effectiveCropCycleModelDto);
            } else {
                effectiveCropCyclesMainModels.add(effectiveCropCycleModelDto);
            }
        }

        refOrientationEDIs = referentialService.getAllReferentielEDI();
        relatedZones = plotService.getRelatedZones(zone.getCode());

        cattles = effectiveCropCycleService.getCattleForDomain(domainId);

        // prices can be present in case of validation failed
        // in that case do not load prices otherwise it will erase the new created prices
        //loadPricesIfNone(prices);

        //loadPrices(effectiveSeasonalCropCycles, effectivePerennialCropCycles, zoneTopiaId, organic);

        super.initForInput();
    }

    @Override
    public void validate() {

        if (zone == null) {
            addFieldError("zoneTopiaId", "Zone can't be null");
        }

        if (zone != null && !activated) {
            addActionError("La zone et/ou sa parcelle et/ou son system de culture et/ou son dispositif et/ou son domain sont inactifs !");
        }

        try {
            effectiveSeasonalCropCycles = convertEffectiveSeasonalCropCyclesJson(effectiveSeasonalCropCyclesJson);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            addActionError("Exception:" + ex + "\n/!\\ Désolé les données saisies depuis votre dernier enregistrement concernant vos cycles de cultures saisonniers n'ont pu être récupérées !");
        }

        try {
            effectivePerennialCropCycles = convertEffectivePerennialCropCyclesJson(effectivePerennialCropCyclesJson);
        } catch (Exception ex) {
            LOGGER.error(ex.toString());
            addActionError("Exception:" + ex + "\n/!\\ Désolé les données saisies depuis votre dernier enregistrement concernant vos cycles de cultures pérennes n'ont pu être récupérées !");
        }

        //prices = validAndGetPrices();

        if (zone != null) {
            allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = effectiveCropCycleService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(zone);
            allsectorByCodeEspceBotaniqueCodeQualifiant = effectiveCropCycleService.getSectorByCodeEspceBotaniqueCodeQualifiant(zone);
        }
    
        Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds = zone != null ? domainInputStockUnitService.loadDomainInputStockByIds(zone.getPlot().getDomain()) : new HashMap<>();
        
        effectiveSeasonalCropCycles = validateSeasonalCropCycles(
                effectiveSeasonalCropCycles, zoneTopiaId, allCodeEspeceBotaniqueCodeQualifantBySpeciesCode, allsectorByCodeEspceBotaniqueCodeQualifiant, domainInputStockUnitByIds);

        effectivePerennialCropCycles = validatePerennialCropCycles(
                effectivePerennialCropCycles, zoneTopiaId, allCodeEspeceBotaniqueCodeQualifantBySpeciesCode, allsectorByCodeEspceBotaniqueCodeQualifiant, domainInputStockUnitByIds);

        validToolCouplingsOnInterventions();

        if (hasErrors()) {
            if (LOGGER.isErrorEnabled()) {
                String zoneId = getLogEntityId(zone);
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, action errors : cycle du réalisé pour zone:'%s' -> %s", zoneId, getActionErrors().toString()));
                LOGGER.error(String.format("For user email:" + getAuthenticatedUser().getEmail() + ":validate, fields errors : cycle du réalisé pour zone:'%s' -> %s", zoneId, getFieldErrors().toString()));
            }

            initForInput();
        }
    }

    private void validToolCouplingsOnInterventions() {
        List<EffectiveInterventionDto> sInterventions = effectiveSeasonalCropCycles.stream()
                .map(EffectiveSeasonalCropCycleDto::getNodeDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(EffectiveCropCycleNodeDto::getInterventions)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();
        List<EffectiveInterventionDto> pInterventions = effectivePerennialCropCycles.stream()
                .map(EffectivePerennialCropCycleDto::getPhaseDtos)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .map(EffectiveCropCyclePhaseDto::getInterventions)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .toList();
        if (CollectionUtils.union(sInterventions, pInterventions).stream()
                .anyMatch(intervention -> intervention.getToolsCouplingCodes().isEmpty())) {
            notificationSupport.warning(MISSING_TOOLCOUPLING_ON_INTERVENTION_MESSAGE);
        }
    }

    protected List<EffectivePerennialCropCycleDto> validatePerennialCropCycles(
            List<EffectivePerennialCropCycleDto> effectivePerennialCropCycles,
            String zoneTopiaId,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        // can't be null except in case of exception
        if (effectivePerennialCropCycles == null) {
            effectivePerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zoneTopiaId);
        } else {
            for (EffectivePerennialCropCycleDto perennial : effectivePerennialCropCycles) {

                if(Strings.isNullOrEmpty(perennial.getCroppingPlanEntryId())){
                    addActionError("Un cycle de culture pérenne doit avoir une culture de sélectionnée");
                }

                if(perennial.getWeedType() == null) {
                    addActionError("Un cycle de culture pérenne doit avoir un type d'enherbement de sélectionnée");
                }

                // Number of phase for effective crop cycle has been limited to 1.
                if(perennial.getPhaseDtos().size()!=1) {
                    addActionError("Un cycle de culture pérenne doit avoir une phase de déclarée");
                }

                final Double plantingDensity = perennial.getPlantingDensity();
                if (plantingDensity != null && plantingDensity < 0) {
                    addActionError("Un cycle de culture pérenne doit avoir une densité de plantation positive");
                }

                List<EffectiveCropCycleSpeciesDto> allSpecies = perennial.getSpeciesDtos();
                if(allSpecies != null && !allSpecies.isEmpty()) {
                    for (EffectiveCropCycleSpeciesDto speciesDto : allSpecies) {
                        if(Strings.isNullOrEmpty(speciesDto.getCroppingPlanSpeciesId())){
                            addActionError("Une espèce d'un cycle de culture pérenne ne peut-être retrouvée");
                        }
                    }
                }

                for (EffectiveCropCyclePhaseDto phase : perennial.getPhaseDtos()) {
                    if (phase.getType() == null) {
                        addActionError("Une phase d'un cycle de culture pérenne doit être typée");
                    }

                    validatePerennialInterventions(
                            phase, perennial.getCroppingPlanEntryName(), phase.getType(), speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee, sectorByCodeEspceBotaniqueCodeQualifiant, domainInputStockUnitByIds);
                }
            }
        }
        return effectivePerennialCropCycles;
    }

    protected List<EffectiveSeasonalCropCycleDto> validateSeasonalCropCycles(
            List<EffectiveSeasonalCropCycleDto> effectiveSeasonalCropCycles,
            String zoneTopiaId,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        // can't be null except in case of exception
        if (effectiveSeasonalCropCycles == null) {
            effectiveSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zoneTopiaId);
        } else {
            boolean error;
            Set<Integer> xAlreadyDefined = Sets.newHashSet();
            for (EffectiveSeasonalCropCycleDto effectiveSeasonalCropCycleDto : effectiveSeasonalCropCycles) {

                error = validateNodes(
                        xAlreadyDefined,
                        effectiveSeasonalCropCycleDto,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspceBotaniqueCodeQualifiant,
                        domainInputStockUnitByIds);

                if (error) break;

                validateConnections(effectiveSeasonalCropCycleDto);

            }
        }
        return effectiveSeasonalCropCycles;
    }

    private boolean validateNodes(Set<Integer> xAlreadyDefined,
                                  EffectiveSeasonalCropCycleDto effectiveSeasonalCropCycleDto,
                                  Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                                  Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
                                  Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        boolean error = false;
        List<EffectiveCropCycleNodeDto> nodeDtos = effectiveSeasonalCropCycleDto.getNodeDtos();
        for (EffectiveCropCycleNodeDto nodeDto : nodeDtos) {

            // ignore NODE_BEFORE
            if (EffectiveCropCycleNodeDto.NODE_BEFORE_ID.equals(nodeDto.getNodeId())) {
                continue;
            }

            if (xAlreadyDefined.contains(nodeDto.getX())) {
                addActionError("Une succession de cultures assolées en réalisé doit être linéaire (une seule culture par rang).");
                error = true;
                break;
            }
            xAlreadyDefined.add(nodeDto.getX());

            validateSeasonalInterventions(
                    nodeDto.getInterventions(), nodeDto.getLabel(), nodeDto.getX(),
                    speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                    sectorByCodeEspceBotaniqueCodeQualifiant,
                    domainInputStockUnitByIds);
        }
        return error;
    }

    private void validateConnections(EffectiveSeasonalCropCycleDto effectiveSeasonalCropCycleDto) {
        Collection<EffectiveCropCycleConnectionDto> effectiveCropCycleConnectionDtos = CollectionUtils.emptyIfNull(effectiveSeasonalCropCycleDto.getConnectionDtos());
        Set<String> sources = Sets.newHashSet();
        Set<String> targets = Sets.newHashSet();
        for (EffectiveCropCycleConnectionDto connection : effectiveCropCycleConnectionDtos) {
            String sourceId = connection.getSourceId();
            if (StringUtils.isNotBlank(sourceId)) {
                if (sources.contains(sourceId)) {
                    addActionError("Une culture assolée du réalisé ne peut avoir qu'une seule culture précédente");
                    break;
                } else {
                    sources.add(sourceId);
                }
            }
            String targetId = connection.getTargetId();
            if (StringUtils.isNotBlank(targetId)) {
                if (targets.contains(targetId)) {
                    addActionError("Une culture assolée du réalisé ne peut avoir qu'une seule culture suivante");
                    break;
                } else {
                    targets.add(targetId);
                }
            }
        }
    }

    protected void validateSeasonalInterventions(
            List<EffectiveInterventionDto> interventionDtos,
            String cropLabel,
            int cropRank,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        String interventionGeneralInfos = String.format("sur la culture assolée '%s' (Rang %d)", cropLabel, cropRank+1);
        validateInterventions(interventionDtos, interventionGeneralInfos, speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee, sectorByCodeEspceBotaniqueCodeQualifiant, domainInputStockUnitByIds);
    }

    protected void validatePerennialInterventions(
            EffectiveCropCyclePhaseDto phase,
            String cropLabel,
            CropCyclePhaseType phaseType,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        List<EffectiveInterventionDto> interventionDtos = phase.getInterventions();
        String interventionGeneralInfos = String.format("sur la culture pérenne '%s' (%s)", cropLabel, AgrosystI18nService.getEnumTraductionWithDefaultLocale(phaseType));
        validateInterventions(
                interventionDtos, interventionGeneralInfos, speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee, sectorByCodeEspceBotaniqueCodeQualifiant, domainInputStockUnitByIds);
    }

    protected void validateInterventions(
            List<EffectiveInterventionDto> interventionDtos,
            String interventionContextInfo,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds) {
        if (CollectionUtils.isNotEmpty(interventionDtos)) {
            for (EffectiveInterventionDto interventionDto : interventionDtos) {
                List<String> interventionErrorMessages = new ArrayList<>();

                if (interventionDto.getType() == null) {
                    interventionErrorMessages.add("- Renseigner le type de l'intervention");
                }
                if (Strings.isNullOrEmpty(interventionDto.getName())) {
                    interventionErrorMessages.add("- Renseigner le nom de l'intervention");
                }
                if (interventionDto.getStartInterventionDate() == null) {
                    interventionErrorMessages.add("- Renseigner la date de début de l'intervention");
                }
                if (interventionDto.getEndInterventionDate() == null) {
                    interventionErrorMessages.add("- Renseigner la date de fin de l'intervention");
                }
                final Double workRate = interventionDto.getWorkRate();
                if (workRate != null && workRate < 0) {
                    interventionErrorMessages.add("- Le débit de chantier doit être positif");
                }
                final Double involvedPeopleCount = interventionDto.getInvolvedPeopleCount();
                if (involvedPeopleCount != null && involvedPeopleCount < 0) {
                    interventionErrorMessages.add("- Le nombre de personnes mobilisées doit être positif");
                }

                Collection<AbstractActionDto> actionDtos = interventionDto.getActionDtos();

                final Collection<String> actionDtoValidationResult = actionService.validActionDtosAndUsages(
                        actionDtos,
                        domainInputStockUnitByIds,
                        speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                        sectorByCodeEspeceBotaniqueCodeQualifiant,
                        interventionDto.getSpeciesStadesDtos(),
                        interventionDto.isIntermediateCrop()
                );

                if (CollectionUtils.isNotEmpty(actionDtoValidationResult)) {

                    String interventionName = StringUtils.isBlank( interventionDto.getName()) ? "?" :  interventionDto.getName();
                    String interventionGeneralInfos = String.format("Intervention '%s' %s&nbsp:<br>", interventionName, interventionContextInfo);
                    String messages = StringUtils.join(interventionErrorMessages, "<br>");
                    addActionError(interventionGeneralInfos + messages);

                } else if (!interventionErrorMessages.isEmpty()) {
                    String messages = StringUtils.join(interventionErrorMessages, "<br>");
                    addActionError(messages);
                }
            }
        }
    }
    

    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "effective-crop-cycles-edit-input", "zoneTopiaId", "${zoneTopiaId}"})})
    @Override
    public String execute() throws Exception {
        effectiveCropCycleService.updateEffectiveCropCycles(
                zoneTopiaId,
                effectiveSeasonalCropCycles,
                effectivePerennialCropCycles
        );
        notificationSupport.culturalInterventionCreated();
        return SUCCESS;
    }

    public String getZoneTopiaId() {
        return zoneTopiaId;
    }

    public void setZoneTopiaId(String zoneTopiaId) {
        if (Strings.isNullOrEmpty(zoneTopiaId) && LOGGER.isWarnEnabled()) {
            // AThimel 04/07/14 I will try to display "referer" header to understand where the request comes from
            LOGGER.warn("Empty zoneTopiaId. " + getServletInfo());
        }
        this.zoneTopiaId = zoneTopiaId;
    }

    public Zone getZone() {
        return zone;
    }

    public List<EffectiveSeasonalCropCycleDto> convertEffectiveSeasonalCropCyclesJson(String json) {
        Type type = new TypeToken<List<EffectiveSeasonalCropCycleDto>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }

    public List<EffectivePerennialCropCycleDto> convertEffectivePerennialCropCyclesJson(String json) {
        Type type = new TypeToken<List<EffectivePerennialCropCycleDto>>() {
        }.getType();
        return getGson().fromJson(json, type);
    }

    public void setEffectiveSeasonalCropCyclesJson(String json) {
        effectiveSeasonalCropCyclesJson = json;
    }

    public void setEffectivePerennialCropCyclesJson(String json) {
        effectivePerennialCropCyclesJson = json;
    }

    public Map<VineFrutalForm, String> getVineFrutalForms() {
        return getEnumAsMap(VineFrutalForm.values());
    }

    public Map<OrchardFrutalForm, String> getOrchardFrutalForms() {
        return getEnumAsMap(OrchardFrutalForm.values());
    }

    public Map<PollinatorSpreadMode, String> getPollinatorSpreadModes() {
        return getEnumAsMap(PollinatorSpreadMode.values());
    }
    
    public Map<CropCyclePhaseType, String> getPerennialPhaseTypes() {
        return getEnumAsMap(CropCyclePhaseType.values());
    }
    
    public Map<WeedType, String> getWeedTypes() {
        return getEnumAsMap(WeedType.values());
    }
    
    // used to include action jsp
    public Map<AgrosystInterventionType, String> getAgrosystInterventionTypes() {
        Map<AgrosystInterventionType, String> map = referentialService.getAgrosystInterventionTypeTranslationMap();
        return map;
    }
    
    public Map<WineValorisation, String> getWineValorisations() {
        return getEnumAsMap(WineValorisation.values());
    }

    public Map<PriceUnit, String> getPriceUnits() {
        return getEnumAsMap(PriceUnit.values());
    }

    public Map<MaterielWorkRateUnit, String> getMaterielWorkRateUnits() {
        return getEnumAsMap(MaterielWorkRateUnit.values());
    }

    public Map<MaterielTransportUnit, String> getMaterielTransportUnits() {
        return getEnumAsMap(MaterielTransportUnit.values());
    }

    public Map<CriteriaUnit, String> getCriteriaUnits() {
        return getEnumAsMap(CriteriaUnit.values());
    }

    public Map<PastureType, String> getPastureTypes() {
        return getEnumAsMap(PastureType.values());
    }

}
