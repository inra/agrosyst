package fr.inra.agrosyst.web.actions.plots;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Desactivation d'une parcelle.
 *
 * @author Eric Chatellier
 */
public class PlotsUnactivate extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = 4898165393431865345L;
    
    protected transient PlotService plotService;

    /** Plot ids. */
    protected List<String> plotTopiaIds;

    protected transient boolean activate;
    
    public void setPlotService(PlotService plotService) {
        this.plotService = plotService;
    }
    
    public void setActivate(boolean activate) {
        this.activate = activate;
    }

    @Override
    @Action(results = { @Result(type = "redirectAction", params = { "namespace", "/domains", "actionName", "domains-edit-input", "domainTopiaId", "${plot.domain.topiaId}" }) })
    public String execute() throws Exception {
        plotService.updateActivatePlotStatus(plotTopiaIds, activate);
        notificationSupport.plotActivatedStatus(plotTopiaIds, activate);
        return SUCCESS;
    }
    
    public void setJsonPlotTopiaIds(String json) {
        Type type = new TypeToken<List<String>>() {}.getType();
        plotTopiaIds = getGson().fromJson(json, type);
    }
}
