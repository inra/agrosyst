package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.Map;

/**
 * Domain list action.
 *
 * @author Arnaud Thimel (Code Lutin)
 */
public class DomainsList extends AbstractAgrosystAction {
    
    @Serial
    private static final long serialVersionUID = -5828110601098674893L;
    
    protected DomainFilter domainFilter;
    
    protected transient DomainService domainService;
    
    protected PaginationResult<DomainDto> domains;
    
    /**
     * In case of file extension error.
     */
    protected boolean importFileError;
    
    protected int exportAsyncThreshold;
    
    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }
    
    public void setImportFileError(boolean importFileError) {
        this.importFileError = importFileError;
    }
    
    
    @Override
    public String execute() throws Exception {

        // in case of file type error
        if (importFileError) {
            notificationSupport.importError("Format de fichier incorrect !");
            importFileError = false;
        }

        domainFilter = new DomainFilter();
        NavigationContext navigationContext = getNavigationContext();
        domainFilter.setNavigationContext(navigationContext);
        domainFilter.setPageSize(getListNbElementByPage(DomainDto.class));
        domainFilter.setActive(Boolean.TRUE);
        domains = domainService.getFilteredDomainsDto(domainFilter);

        this.exportAsyncThreshold = this.config.getDomainsExportAsyncThreshold();
        return SUCCESS;
    }

    public Map<DomainType, String> getTypes() {
        return i18nService.getEnumTranslationMap(DomainType.class);
    }

    public DomainFilter getDomainFilter() {
        return domainFilter;
    }
    
    public PaginationResult<DomainDto> getDomains() {
        return domains;
    }
    
    public int getExportAsyncThreshold() {
        return exportAsyncThreshold;
    }
    
}
