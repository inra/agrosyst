package fr.inra.agrosyst.web.actions.managementmodes;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.services.managementmode.DecisionRuleFilter;
import fr.inra.agrosyst.api.services.managementmode.DecisionRulesDto;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import lombok.Getter;
import lombok.Setter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.Serial;
import java.util.Map;

/**
 * Decision rule list action.
 * 
 * @author Eric Chatellier
 */
@Getter
@Setter
public class DecisionRulesList extends AbstractAgrosystAction {

    /** serialVersionUID. */
    @Serial
    private static final long serialVersionUID = -8916181887544412258L;

    protected transient ManagementModeService managementModeService;

    protected transient ReferentialService referentialService;

    protected PaginationResult<DecisionRulesDto> decisionRules;

    protected DecisionRuleFilter decisionRuleFilter;

    protected int decisionRulesExportAsyncThreshold;

    @Override
    public String execute() throws Exception {
        NavigationContext navigationContext = getNavigationContext();
        decisionRuleFilter = new DecisionRuleFilter();
        decisionRuleFilter.setNavigationContext(navigationContext);
        decisionRuleFilter.setPageSize(getListNbElementByPage(DecisionRulesDto.class));
        decisionRuleFilter.setActive(Boolean.TRUE);

        decisionRules = managementModeService.getFilteredDecisionRules(decisionRuleFilter);

        this.decisionRulesExportAsyncThreshold = config.getDecisionRulesExportAsyncThreshold();

        return SUCCESS;
    }

    public Map<AgrosystInterventionType, String> getAgrosystInterventionTypes() {
        return referentialService.getAgrosystInterventionTypeTranslationMap();
    }


}
