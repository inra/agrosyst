package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AttachmentMetadata;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;

/**
 * This is the default action used to upload a file attached to a given object reference ID.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class AttachmentsDownload extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 1864154104471757665L;

    private static final Log LOGGER = LogFactory.getLog(AttachmentsDownload.class);

    protected String attachmentTopiaId;

    protected AttachmentMetadata attachmentMetadata;

    public void setAttachmentTopiaId(String attachmentTopiaId) {
        this.attachmentTopiaId = attachmentTopiaId;
    }

    public AttachmentMetadata getAttachmentMetadata() {
        return attachmentMetadata;
    }

    @Override
    @Action(results= {
            @Result(type="stream",
                    params={
                        "contentType", "${attachmentMetadata." + AttachmentMetadata.PROPERTY_CONTENT_TYPE + "}",
                        "inputName", "inputStream",
                        "contentDisposition", "attachment; filename=\"${attachmentMetadata." + AttachmentMetadata.PROPERTY_NAME + "}\""
                    }
            )
    })

    public String execute() throws Exception {
        attachmentMetadata = attachmentService.getAttachmentMetadata(attachmentTopiaId);
        return SUCCESS;
    }
    
    public InputStream getInputStream() {
        InputStream inputStream;
        try {
            inputStream = attachmentService.getAttachmentContent(attachmentTopiaId);
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't download file", ex);
            }
            throw new AgrosystTechnicalException("Can't create input stream", ex);
        }
        return inputStream;
    }
}
