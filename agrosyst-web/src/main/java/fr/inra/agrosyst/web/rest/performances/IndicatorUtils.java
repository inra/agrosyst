package fr.inra.agrosyst.web.rest.performances;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.services.performance.indicators.IndicatorGrossIncome;
import fr.inra.agrosyst.services.performance.indicators.IndicatorOrganicProducts;
import fr.inra.agrosyst.services.performance.indicators.IndicatorStandardisedGrossIncome;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorMineralFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorOrganicFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorTotalFertilization;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorRefMaxYearCropIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorRefMaxYearTargetIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageCropIFT;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageTargetIFT;
import fr.inra.agrosyst.services.performance.indicators.operatingexpenses.IndicatorDecomposedOperatingExpenses;
import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorActiveSubstanceAmount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

final class IndicatorUtils {

    final static Function<Set<Method>, List<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>>> PRODUIT_ET_MARGES_PARAMETER = (Set<Method> methods) -> {
        var classes = new ArrayList<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>>();
        if (methods.contains(Method.PRIX_REELS_PRODUITS_MARGES)) {
            classes.add(IndicatorGrossIncome.class);
        }
        if (methods.contains(Method.PRIX_STANDARDISES_MILLESIMES_PRODUITS_MARGES)) {
            classes.add(IndicatorStandardisedGrossIncome.class);
        }
        return classes;
    };

    final static Function<Set<Method>, List<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>>> IFT_INDICATORS_PARAMETER = (Set<Method> methods) -> {
        var classes = new ArrayList<Class<? extends fr.inra.agrosyst.services.performance.indicators.Indicator>>();
        if (methods.contains(Method.IFT_A_LA_CIBLE_NON_MILLESIME)) {
            classes.add(IndicatorRefMaxYearTargetIFT.class);
        }
        if (methods.contains(Method.IFT_A_LA_CIBLE_MILLESIME)) {
            classes.add(IndicatorVintageTargetIFT.class);
        }
        if (methods.contains(Method.IFT_A_LA_CULTURE_NON_MILLESIME)) {
            classes.add(IndicatorRefMaxYearCropIFT.class);
        }
        if (methods.contains(Method.IFT_A_LA_CULTURE_MILLESIME)) {
            classes.add(IndicatorVintageCropIFT.class);
        }
        if (methods.contains(Method.IFT_A_L_ANCIENNE_NON_MILLESIME)) {
            classes.add(IndicatorLegacyIFT.class);
        }
        return classes;
    };

    static Set<Indicator> CONVERT_TO_INDICATORS(Collection<IndicatorFilter> filters) {
        if (filters != null) {
            var indicators = new HashSet<Indicator>();
            var indicatorsWithClazz = Arrays.stream(Indicator.values())
                    .filter((indicator) -> indicator.getDoe().isEmpty())
                    .filter((indicator) -> indicator.getIft().isEmpty())
                    .filter((indicator) -> indicator.getClazz().isPresent())
                    .collect(Collectors.toSet());
            filters.forEach((filter) -> {
                final String className = filter.getClazz();
                if (IndicatorDecomposedOperatingExpenses.class.getSimpleName().equals(className)) {
                    indicators.addAll(Arrays.stream(Indicator.values())
                            .filter((indicator) -> indicator.getDoe().isPresent())
                            .filter((indicator) -> filter.getDoeIndicators().contains(indicator.getDoe().get()))
                            .collect(Collectors.toSet()));
                } else if (IndicatorLegacyIFT.class.getSimpleName().equals(className) ||
                        IndicatorRefMaxYearTargetIFT.class.getSimpleName().equals(className) ||
                        IndicatorVintageTargetIFT.class.getSimpleName().equals(className) ||
                        IndicatorRefMaxYearCropIFT.class.getSimpleName().equals(className) ||
                        IndicatorVintageCropIFT.class.getSimpleName().equals(className)
                ) {
                    indicators.addAll(Arrays.stream(Indicator.values())
                            .filter((indicator) -> indicator.getIft().isPresent())
                            .filter((indicator) -> filter.getIfts().contains(indicator.getIft().get()))
                            .collect(Collectors.toSet()));
                } else if (IndicatorMineralFertilization.class.getSimpleName().equals(className)) {
                    indicators.addAll(Arrays.stream(Indicator.values())
                            .filter(indicator -> indicator.getMineralFertilization().isPresent())
                            .filter(indicator -> filter.getMineralFertilizations().contains(indicator.getMineralFertilization().get()))
                            .collect(Collectors.toSet()));
                } else if (IndicatorOrganicFertilization.class.getSimpleName().equals(className)) {
                    indicators.addAll(Arrays.stream(Indicator.values())
                            .filter(indicator -> indicator.getOrganicFertilization().isPresent())
                            .filter(indicator -> filter.getOrganicFertilizations().contains(indicator.getOrganicFertilization().get()))
                            .collect(Collectors.toSet()));
                } else if (IndicatorTotalFertilization.class.getSimpleName().equals(className)) {
                    indicators.addAll(Arrays.stream(Indicator.values())
                            .filter(indicator -> indicator.getTotalFertilization().isPresent())
                            .filter(indicator -> filter.getTotalFertilizations().contains(indicator.getTotalFertilization().get()))
                            .collect(Collectors.toSet()));
                } else if (IndicatorActiveSubstanceAmount.class.getSimpleName().equals(className)) {
                    indicators.addAll(Arrays.stream(Indicator.values())
                            .filter(indicator -> indicator.getActiveSubstance().isPresent())
                            .filter(indicator -> filter.getActiveSubstances().contains(indicator.getActiveSubstance().get()))
                            .collect(Collectors.toSet()));
                } else if (IndicatorOrganicProducts.class.getSimpleName().equals(className)) {
                    indicators.addAll(Arrays.stream(Indicator.values())
                            .filter(indicator -> indicator.getOrganicProduct().isPresent())
                            .filter(indicator -> filter.getOrganicProducts().contains(indicator.getOrganicProduct().get()))
                            .collect(Collectors.toSet()));
                } else {
                    indicatorsWithClazz.stream().filter((indicator) -> className.equals(indicator.getClazz().get().getSimpleName()))
                            .findFirst()
                            .ifPresent(indicators::add);
                }
                if (IndicatorGrossIncome.class.getSimpleName().equals(className) ||
                        IndicatorStandardisedGrossIncome.class.getSimpleName().equals(className)) {
                    indicators.add(Indicator.PRODUIT_BRUT);
                }
            });
            return indicators;
        }
        return Collections.emptySet();
    }
}
