package fr.inra.agrosyst.web.actions.referential;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

public class ReferentialsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(ReferentialsJson.class);
    @Serial
    private static final long serialVersionUID = 584317145124524140L;
    
    protected String referential;

    protected transient ReferentialService referentialService;

    public void setReferential(String referential) {
        this.referential = referential;
    }

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    @Override
    public String execute() throws Exception {
        try {
            Class<? extends ReferentialEntity> refClass =
                    (Class<? extends ReferentialEntity>) Class.forName("fr.inra.agrosyst.api.entities.referential." + referential);
            jsonData = referentialService.getAllActiveReferentialEntities(refClass);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load referential list for class " + referential, e);
            }
            jsonData = "Failed to load referential list for class " + referential;
            return ERROR;
        }

        return SUCCESS;
    }
}
