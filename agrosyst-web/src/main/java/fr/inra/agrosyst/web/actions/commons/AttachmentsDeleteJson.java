package fr.inra.agrosyst.web.actions.commons;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * This is the default action used to upload a file attached to a given object reference ID.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class AttachmentsDeleteJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(AttachmentsDeleteJson.class);

    @Serial
    private static final long serialVersionUID = 1864154104471757665L;

    protected transient String attachmentTopiaId;

    public void setAttachmentTopiaId(String attachmentTopiaId) {
        this.attachmentTopiaId = attachmentTopiaId;
    }

    @Override
    public String execute() throws Exception {
        try {
            attachmentService.delete(attachmentTopiaId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Failed to delete attachement with id '%s':", attachmentTopiaId), e);
            }
            return ERROR;
        }
        return SUCCESS;
    }
    
}
