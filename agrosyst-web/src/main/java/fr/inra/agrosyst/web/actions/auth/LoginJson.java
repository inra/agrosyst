package fr.inra.agrosyst.web.actions.auth;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.Serial;

/**
 *
 */
public class LoginJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(LoginJson.class);
    @Serial
    private static final long serialVersionUID = -495238362631537397L;
    
    protected transient UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    protected String email;
    protected String password;

    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws Exception {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info(String.format("[sid=%s] Trying to login with email '%s'", getSessionId(), email));
        }

        if (StringUtils.isEmpty(email)) {
            jsonData = "Le champ 'email' est obligatoire";
            httpCode = HttpServletResponse.SC_EXPECTATION_FAILED;
            return ERROR;
        }

        if (StringUtils.isEmpty(password)) {
            jsonData = "Le champ 'password' est obligatoire";
            httpCode = HttpServletResponse.SC_EXPECTATION_FAILED;
            return ERROR;
        }

        AuthenticatedUser loginResult = authenticationService.login(email, password);
        if (loginResult == null) {
            if (userService.isValidEmail(email) && !userService.isUserActive(email)) {
                jsonData = "Votre compte est désactivé, merci de contacter un administrateur d'Agrosyst.";
            } else {
                jsonData = "Échec de connexion.";
            }
            httpCode = HttpServletResponse.SC_UNAUTHORIZED;
            return ERROR;
        }

        if (authorizationService.isInMaintenanceMode() && !authorizationService.isAdminFromUserId(loginResult.getTopiaId())) {
            jsonData = authorizationService.getMaintenanceModeMessage();
            httpCode = HttpServletResponse.SC_UNAUTHORIZED;
            return ERROR;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("[sid=%s] user email '%s' redirect to %s", loginResult.getSid(), email, "next"));
        }
        applicationContext.writeAuthenticationCookie(loginResult, getCookieHelper());
        jsonData = loginResult;
        return SUCCESS;
    }

}
