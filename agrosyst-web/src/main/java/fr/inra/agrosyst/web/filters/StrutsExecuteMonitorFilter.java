package fr.inra.agrosyst.web.filters;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.net.HttpHeaders;
import fr.inra.agrosyst.web.listeners.AgrosystWebSessionListener;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.dispatcher.filter.StrutsExecuteFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Compute &amp; display execution time
 */
public class StrutsExecuteMonitorFilter extends StrutsExecuteFilter {

    private static final Log LOGGER = LogFactory.getLog(StrutsExecuteMonitorFilter.class);

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        // Start monitoring
        long start = System.currentTimeMillis();

        // Execute action
        super.doFilter(req, res, chain);

        // Execution is over, compute & display execution time
        if (LOGGER.isDebugEnabled()) {
            long end = System.currentTimeMillis();
            HttpServletRequest httpServletRequest = (HttpServletRequest) req;
            HttpServletResponse httpServletResponse = (HttpServletResponse) res;

            String sid = AgrosystWebSessionListener.getSessionId(httpServletRequest);
            String method = httpServletRequest.getMethod();
            String requested = httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length());
            int status = httpServletResponse.getStatus();
            String queryString = httpServletRequest.getQueryString();

            String format = "[sid=%s] %s %s %dms [%d%s]";
            if (StringUtils.isNotEmpty(queryString)) {
                format += " {%s}";
            }
            String location = Optional.ofNullable(httpServletResponse.getHeader(HttpHeaders.LOCATION))
                    .map(l -> " -> " + l)
                    .orElse("");

            String message = String.format(format, sid, method, requested, end - start, status, location, queryString);
            LOGGER.debug(message);
        }
    }

}
