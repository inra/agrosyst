/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.referential.BioAgressorParentType;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.entities.report.ArboAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.IftEstimationMethod;
import fr.inra.agrosyst.api.entities.report.PestMaster;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystemImpl;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.VitiPestMaster;
import fr.inra.agrosyst.api.entities.report.YieldInfo;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.entities.report.YieldLossCause;
import fr.inra.agrosyst.api.entities.report.YieldObjective;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.referential.GroupeCibleDTO;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.report.ReportFilter;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemCollections;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.common.CommonService;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.Preparable;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Edition d'un bilan de campagne.
 */
@Getter
@Setter
public class ReportGrowingSystemEdit extends AbstractReportAction implements Preparable {

    private static final Log LOGGER = LogFactory.getLog(ReportGrowingSystemEdit.class);
    @Serial
    private static final long serialVersionUID = -5620592252923620778L;

    protected transient ReportService reportService;
    protected transient ManagementModeService managementModeService;
    protected transient GrowingSystemService growingSystemService;
    protected transient ReferentialService referentialService;

    protected ReportGrowingSystem reportGrowingSystem;

    // GENERAL

    // filter used to load required entities info
    protected ReportFilter reportFilter;

    // use for creation
    protected String domainId;

    // use only Ids to not load all entities
    protected String growingSystemId;
    protected String reportGrowingSystemId;
    protected String reportRegionalId;

    // name -> Id
    protected Map<String, String> reportRegionalList;

    // true -> will create management mode at recording
    protected boolean createManagementMode;

    protected String observeManagementModeId;

    // use LinkedHashMap to keep ordering
    protected LinkedHashMap<Integer, String> relatedReportGrowingSystems;
    
    protected String iftEstimationMethodName;
    protected IftEstimationMethod iftEstimationMethod;

    // FAIT MARQUANT
    protected final ReportGrowingSystemCollections highlights = new ReportGrowingSystemCollections();
    private List<Sector> reportSectors;

    protected List<GroupeCibleDTO> groupesCibles;

    protected String defaultArboAdventiceId;

    protected Set<IftEstimationMethod> iftEstimationMethods;
    
    protected YieldObjective vitiYieldObjective;
    protected YieldLossCause vitiLossCause1;

    @Override
    public void prepare() {
        if (StringUtils.isEmpty(reportGrowingSystemId)) {
            // creation
            reportGrowingSystem = new ReportGrowingSystemImpl();

            AuthenticatedUser authenticatedUser = getAuthenticatedUser();
            reportGrowingSystem.setAuthor(authenticatedUser.getFirstName() + " " + authenticatedUser.getLastName());

        } else {
            authorizationService.checkReportGrowingSystemReadable(reportGrowingSystemId);

            reportGrowingSystem = reportService.getReportGrowingSystem(reportGrowingSystemId);

            reportFilter = getReportFilterFromReportGrowingSystem(reportGrowingSystem);
            reportRegionalList = reportService.getReportRegionalNameToId(reportFilter);
            iftEstimationMethods = reportService.getIftEstimationMethods(reportGrowingSystem);
            reportGrowingSystem.getVitiYieldObjective();
            reportGrowingSystem.getVitiLossCause1();
        }
    }

    public ReportFilter getReportFilterFromReportGrowingSystem(ReportGrowingSystem reportGrowingSystem) {
        ReportFilter rfilter = new ReportFilter();
        rfilter.setCampaign(reportGrowingSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign());
        rfilter.setGrowingSystemId(reportGrowingSystem.getGrowingSystem().getTopiaId());
        rfilter.setSectors(new HashSet<>(reportGrowingSystem.getSectors()));
        rfilter.setNavigationContext(getNavigationContext());
        return rfilter;
    }

    @Override
    protected void initForInput() {
        if (StringUtils.isNotEmpty(reportGrowingSystemId)) {
            relatedReportGrowingSystems = reportService.getRelatedReportGrowingSystems(reportGrowingSystem.getCode());
        }
        groupesCibles = referentialService.getGroupesCibles();
        defaultArboAdventiceId = reportService.getDefaultArboAdventiceIdForReport();
    }

    @Override
    @Action("report-growing-system-edit-input")
    public String input() {
        initForInput();

        if (reportGrowingSystem.isPersisted()) {
            growingSystemId =  reportGrowingSystem.getGrowingSystem().getTopiaId();
            final ReportRegional reportRegional = reportGrowingSystem.getReportRegional();
            reportRegionalId = reportRegional != null ? reportRegional.getTopiaId() : null;
            observeManagementModeId = managementModeService.getObservedManagementModeIdForGrowingSystemId(growingSystemId).orElse(null);
        }

        if (!StringUtils.isBlank(reportGrowingSystemId)) {

            readOnly = !authorizationService.isReportGrowingSystemWritable(reportGrowingSystemId);
            if (readOnly) {
                notificationSupport.reportGrowingSystemNotWritable();
            }
        }
        return INPUT;
    }

    protected void validateHighlights() {
        GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemId);
        reportGrowingSystem.setGrowingSystem(growingSystem);
        List<String> cropIds = reportService.getCropIdsForGrowingSystemId(growingSystemId);

        validateCropAdventice(cropIds);
        validateCropPest(cropIds);
        validateCropDisease(cropIds);
        validateVerse(cropIds);
        validateFoodMasters(cropIds);
        validateYieldLosses(cropIds, vitiYieldObjective, vitiLossCause1);
        validateArboCropDisease(cropIds);
        validateArboCropPest(cropIds);
        validateArboCropAdventice(cropIds);
        validateVitiDisease();
        validateVitiPest();
        validateVitiAdventice();
    }

    private void validateFoodMasters(List<String> cropIds) {

        List<FoodMaster> allFoodMasters = highlights.getFoodMastersBySector().values().stream()
                .filter(Objects::nonNull)
                .flatMap(Collection::stream).toList();
        String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (cultures assolées), maîtrise de l’alimentation hydrique et minérale: ";
        for (FoodMaster foodMaster : allFoodMasters) {

            filterOnValidGrowingSystemCrops(cropIds, foodMaster);

            if(foodMaster.getCrops().isEmpty()) {
                addActionError(actionErrorMessage + "Culture non renseignée !");
            }
        }
    }

    private void validateYieldLosses(
            List<String> cropIds,
            YieldObjective vitiYieldObjective,
            YieldLossCause vitiLossCause1) {
        
        String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (cultures assolées), Rendement et qualité: ";
        
        if (reportSectors.contains(Sector.VITICULTURE)) {
            if (isDephyFerme()){

                if (vitiYieldObjective == null) {
                    addActionError(actionErrorMessage + "Objectif de rendement. non renseignée !");
                }
            
                boolean causeRequired = vitiYieldObjective != null && YieldObjective.MORE_95 != vitiYieldObjective;
                if (causeRequired && vitiLossCause1 == null) {
                    addActionError(actionErrorMessage + "cause. non renseignée !");
                }
            }
        }
        // the user has not gone to "Faits marquants" tab
        if (highlights.getYieldLossesBySector() == null) return;

        Map<Sector, List<YieldLoss>> yieldLossesBySector = highlights.getYieldLossesBySector();
        List<YieldLoss> allYieldLoss = yieldLossesBySector.values().stream()
                .filter(Objects::nonNull)
                .flatMap(Collection::stream).toList();
        boolean yieldLossRequired = isDephyFerme();
        if (yieldLossRequired && allYieldLoss.isEmpty()) {
            addActionError(actionErrorMessage + " la saisie est obligatoire");
        }
        for (Map.Entry<Sector, List<YieldLoss>> yieldLossBysectors : yieldLossesBySector.entrySet()) {
            Sector sector = yieldLossBysectors.getKey();
            List<YieldLoss> yieldLossForSector = yieldLossBysectors.getValue();
            for (YieldLoss yieldLoss : yieldLossForSector) {
                filterOnValidGrowingSystemCrops(cropIds, yieldLoss);

                if(!Sector.VITICULTURE.equals(sector) && yieldLoss.getCrops().isEmpty()) {
                    addActionError(actionErrorMessage + "Culture non renseignée !");
                }

                if (isNotDephyExpe() && yieldLoss.getYieldObjective() == null) {
                    addActionError(actionErrorMessage + "Objectif de rendement. non renseignée !");
                }

                boolean causeRequired = isDephyFerme()
                        && yieldLoss.getYieldObjective() != null
                        && yieldLoss.getYieldObjective() != YieldObjective.MORE_95;
                if (causeRequired && yieldLoss.getCause1() == null) {
                    addActionError(actionErrorMessage + "cause. non renseignée !");
                }
            }
        }
    }

    private boolean isDephyFerme() {
        GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemId);
        return TypeDEPHY.DEPHY_FERME == growingSystem.getGrowingPlan().getType();
    }

    private boolean isNotDephyExpe() {
        GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemId);
        return TypeDEPHY.DEPHY_EXPE != growingSystem.getGrowingPlan().getType();
    }

    private void validateCropAdventice(List<String> cropIds) {
        List<CropPestMaster> allCropAdventiceMasters = highlights.getCropAdventiceMastersBySector().values().stream()
                .filter(Objects::nonNull)
                .flatMap(Collection::stream).toList();
        String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (cultures assolées), maîtrise des adventices: ";
        for (CropPestMaster cropPestMaster : allCropAdventiceMasters) {

            filterOnValidGrowingSystemCrops(cropIds, cropPestMaster);

            if(cropPestMaster.getCrops().isEmpty()) {
                addActionError(actionErrorMessage + "Culture non renseignée !");
            }

            if(cropPestMaster.getPestMasters().isEmpty()) {
                addActionError(actionErrorMessage + "Adventice. non renseigné !");
            } else if (cropPestMaster.getPestMasters().stream().map(PestMaster::getAgressor).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "Adventice. non renseigné !");
            } else if (isNotDephyExpe() &&
                    cropPestMaster.getPestMasters().stream().map(PestMaster::getPressureScale).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "échelle de pression. non renseignée !");
            } else if (isNotDephyExpe() &&
                    cropPestMaster.getPestMasters().stream().map(PestMaster::getMasterScale).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "échelle de maîtrise. non renseignée !");
            } else if (isNotDephyExpe() &&
                    cropPestMaster.getPestMasters().stream().map(PestMaster::getQualifier).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "qualification du niveau de maîtrise. non renseignée !");
            }
        }
    }
    
    private void validateCropPest(List<String> cropIds) {
        List<CropPestMaster> allCropPestMasters = highlights.getCropPestMastersBySector().values().stream()
                .filter(Objects::nonNull)
                .flatMap(Collection::stream).toList();
        String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (cultures assolées), maîtrise des ravageurs: ";


        for (CropPestMaster cropPestMaster : allCropPestMasters) {

            // maraichage et horticulture : utiliser les échelles avec les entiers (pressureScaleInt et masterScaleInt)
            final Sector sector = cropPestMaster.getSector();
            boolean sectorUsesIntegerValues = sector == Sector.MARAICHAGE || sector == Sector.HORTICULTURE;

            filterOnValidGrowingSystemCrops(cropIds, cropPestMaster);

            if(cropPestMaster.getCrops().isEmpty()) {
                addActionError(actionErrorMessage + "Culture non renseignée !");
            }

            final Collection<PestMaster> pestMasters = cropPestMaster.getPestMasters();
            if(pestMasters.isEmpty()) {
                addActionError(actionErrorMessage + "Ravageur non renseigné !");
            } else if (pestMasters.stream()
                    .anyMatch(pestMaster -> pestMaster.getAgressor() == null && pestMaster.getCodeGroupeCibleMaa() == null)) {
                addActionError(actionErrorMessage + "Ravageur non renseigné !");
            } else if (
                    isNotDephyExpe() &&
                            (sectorUsesIntegerValues && pestMasters.stream().map(PestMaster::getPressureScaleInt).anyMatch(Predicate.isEqual(null))) ||
                            (!sectorUsesIntegerValues && pestMasters.stream().map(PestMaster::getPressureScale).anyMatch(Predicate.isEqual(null)))
            ) {
                addActionError(actionErrorMessage + "échelle de pression. non renseignée !");
            } else if (
                    isNotDephyExpe() &&
                            (sectorUsesIntegerValues && pestMasters.stream().map(PestMaster::getMasterScaleInt).anyMatch(Predicate.isEqual(null))) ||
                            (!sectorUsesIntegerValues && pestMasters.stream().map(PestMaster::getMasterScale).anyMatch(Predicate.isEqual(null)))
            ) {
                addActionError(actionErrorMessage + "échelle de maîtrise. non renseignée !");
            } else if (isNotDephyExpe() && pestMasters.stream().map(PestMaster::getQualifier).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "qualification du niveau de maîtrise. non renseignée !");
            }
        }
    }
    
    private void validateCropDisease(List<String> cropIds) {
        List<CropPestMaster> allCropDiseaseMasters = highlights.getCropDiseaseMastersBySector().values().stream()
                .filter(Objects::nonNull)
                .flatMap(Collection::stream).toList();
        String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (cultures assolées), maîtrise des maladies: ";
        for (CropPestMaster cropPestMaster : allCropDiseaseMasters) {

            filterOnValidGrowingSystemCrops(cropIds, cropPestMaster);

            if(cropPestMaster.getCrops().isEmpty()) {
                addActionError(actionErrorMessage + "Culture non renseignée !");
            }

            if(cropPestMaster.getPestMasters().isEmpty()) {
                addActionError(actionErrorMessage + "Maladie non renseignée !");
            } else if (cropPestMaster.getPestMasters().stream()
                    .anyMatch(pestMaster -> pestMaster.getAgressor() == null && pestMaster.getCodeGroupeCibleMaa() == null)) {
                addActionError(actionErrorMessage + "Maladie non renseignée !");
            } else if (isNotDephyExpe() &&
                    cropPestMaster.getPestMasters().stream().map(PestMaster::getPressureScale).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "échelle de pression. non renseignée !");
            } else if (
                    isNotDephyExpe() &&
                            cropPestMaster.getPestMasters().stream().map(PestMaster::getMasterScale).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "échelle de maîtrise. non renseignée !");
            } else if (
                    isNotDephyExpe() &&
                            cropPestMaster.getPestMasters().stream().map(PestMaster::getQualifier).anyMatch(Predicate.isEqual(null))){
                addActionError(actionErrorMessage + "qualification du niveau de maîtrise. non renseignée !");
            }
        }
    }
    
    private void validateVerse(List<String> cropIds) {
        List<VerseMaster> allCropPestMasters = highlights.getVerseMastersBySector().values().stream()
                .filter(Objects::nonNull)
                .flatMap(Collection::stream).toList();
        String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (cultures assolées), maîtrise de la verse: ";
        for (VerseMaster verseMaster : allCropPestMasters) {

            filterOnValidGrowingSystemCrops(cropIds, verseMaster);

            if(verseMaster.getCrops().isEmpty()) {
                addActionError(actionErrorMessage + "Culture non renseignée !");
            }
        }
    }

    private void validateArboCropDisease(List<String> cropIds) {
        if (reportGrowingSystem.getSectors() != null && reportGrowingSystem.getSectors().contains(Sector.ARBORICULTURE) && (
                reportGrowingSystem.getArboChemicalFungicideIFT() != null ||
                        reportGrowingSystem.getArboBioControlFungicideIFT() != null ||
                        reportGrowingSystem.getArboCopperQuantity() != null ||
                        reportGrowingSystem.getArboDiseaseQualifier() != null ||
                        CollectionUtils.isNotEmpty(highlights.getArboCropDiseaseMasters()
                ))) {

            String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (Arboriculture), Maîtrise des maladies et bactérioses: ";
            if (reportGrowingSystem.getArboDiseaseQualifier() == null) {
                addFieldError("reportGrowingSystem.arboDiseaseQualifier", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Niveau global de maîtrise des maladies non renseignée !");
            }

            if (CollectionUtils.isNotEmpty(highlights.getArboCropDiseaseMasters())) {
                for (ArboCropPestMaster arboCropPestMaster : highlights.getArboCropDiseaseMasters()) {

                    filterOnValidGrowingSystemCrops(cropIds, arboCropPestMaster);

                    if (arboCropPestMaster.getCrops().isEmpty()) {
                        addActionError(actionErrorMessage + "Culture non renseignée !");
                    }
                }
            }
        }
    }

    private void validateArboCropPest(List<String> cropIds) {
        if (reportGrowingSystem.getSectors() != null && reportGrowingSystem.getSectors().contains(Sector.ARBORICULTURE) &&
                (
                        reportGrowingSystem.getArboChemicalPestIFT() != null ||
                                reportGrowingSystem.getArboBioControlPestIFT() != null ||
                                reportGrowingSystem.getArboPestQualifier() != null ||
                                CollectionUtils.isNotEmpty(highlights.getArboCropPestMasters())
                        )) {

            String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (Arboriculture), maîtrise des ravageurs: ";

            if (reportGrowingSystem.getArboPestQualifier() == null) {
                addFieldError("reportGrowingSystem.arboPestQualifier", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Niveau global de maîtrise des ravageurs non renseignée !");
            }

            if (CollectionUtils.isNotEmpty(highlights.getArboCropPestMasters())) {
                for (ArboCropPestMaster arboCropPestMaster : highlights.getArboCropPestMasters()) {

                    filterOnValidGrowingSystemCrops(cropIds, arboCropPestMaster);

                    if (arboCropPestMaster.getCrops().isEmpty()) {
                        addActionError(actionErrorMessage + "Culture non renseignée !");
                    }
                }
            }
        }
    }

    private void validateArboCropAdventice(List<String> cropIds) {
        if (CollectionUtils.isNotEmpty(highlights.getArboCropAdventiceMasters())) {
            String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (Arboriculture), maîtrise des adventices: ";
            for (ArboCropAdventiceMaster arboCropAdventiceMaster : highlights.getArboCropAdventiceMasters()) {

                filterOnValidGrowingSystemCrops(cropIds, arboCropAdventiceMaster);

                if(arboCropAdventiceMaster.getCrops().isEmpty()) {
                    addActionError(actionErrorMessage + "Culture non renseignée !");
                }

                if (isNotDephyExpe()) {
                    if (arboCropAdventiceMaster.getPestMasters().stream().map(ArboAdventiceMaster::getAgressor).anyMatch(Predicate.isEqual(null))) {
                        addActionError(actionErrorMessage + "Adventice non renseignée !");
                    } else if (arboCropAdventiceMaster.getPestMasters().stream().map(ArboAdventiceMaster::getMasterScale).anyMatch(Predicate.isEqual(null))) {
                        addActionError(actionErrorMessage + "échelle de maîtrise non renseignée !");
                    } else if (arboCropAdventiceMaster.getPestMasters().stream().map(ArboAdventiceMaster::getQualifier).anyMatch(Predicate.isEqual(null))) {
                        addActionError(actionErrorMessage + "qualification du niveau de maîtrise non renseignée !");
                    }
                }
            }
        }
    }

    private void validateVitiDisease() {
        if (CollectionUtils.isNotEmpty(highlights.getVitiDiseaseMasters())) {

            String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (Viticulture), maîtrise des maladies: ";

            List<String> diseaseMastersErrorMessages = new ArrayList<>();

            for (VitiPestMaster vitiPestMaster : highlights.getVitiDiseaseMasters()) {

                if (vitiPestMaster.getTreatmentCount() == null) {
                    diseaseMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", Nombre de traitements non renseignée !");
                }
                if (vitiPestMaster.getBioControlFungicideIFT() == null) {
                    diseaseMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", IFT-Fongicide biocontrôle non renseignée !");
                }
                if (vitiPestMaster.getChemicalFungicideIFT() == null) {
                    diseaseMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", IFT-Fongicide (chimique) non renseignée !");
                }
            }

            if (reportGrowingSystem.getVitiDiseaseChemicalFungicideIFT() == null) {
                addFieldError("reportGrowingSystem.vitiDiseaseChemicalFungicideIFT", getText(REQUIRED_FIELD));
                diseaseMastersErrorMessages.add(actionErrorMessage + "IFT-fongicide du système de culture (chimique) non renseignée !");
            }
            if (reportGrowingSystem.getVitiDiseaseBioControlFungicideIFT() == null) {
                addFieldError("reportGrowingSystem.vitiDiseaseBioControlFungicideIFT", getText(REQUIRED_FIELD));
                diseaseMastersErrorMessages.add(actionErrorMessage + "IFT-fongicide biocontrôle du système de culture non renseignée !");
            }
            if (reportGrowingSystem.getVitiDiseaseCopperQuantity() == null) {
                addFieldError("reportGrowingSystem.vitiDiseaseCopperQuantity", getText(REQUIRED_FIELD));
                diseaseMastersErrorMessages.add(actionErrorMessage + "Quantité de cuivre appliquée (Kg Cu/ha) non renseignée !");
            }
            if (reportGrowingSystem.getVitiDiseaseQualifier() == null) {
                addFieldError("reportGrowingSystem.vitiDiseaseQualifier", getText(REQUIRED_FIELD));
                diseaseMastersErrorMessages.add(actionErrorMessage + "Niveau global de maîtrise des maladies non renseignée !");
            }

            for (String diseaseMastersErrorMessage : diseaseMastersErrorMessages) {
                addActionError(diseaseMastersErrorMessage);
            }

            Set<Integer> pestIds = highlights.getVitiDiseaseMasters().stream()
                    .map(VitiPestMaster::getAgressor)
                    .filter(Objects::nonNull)
                    .map(RefNuisibleEDI.class::cast)
                    .map(RefNuisibleEDI::getReference_id)
                    .collect(Collectors.toSet());
            if (!pestIds.containsAll(Arrays.asList(ReportService.MILDIOU, ReportService.OIDIUM, ReportService.VIROSE, ReportService.BOTRYTIS))) {
                addActionError(actionErrorMessage + "Les maladies 'Mildiou', 'Oïdium', 'Virose Black Rot' et 'Botrytis - Pourriture Grise' sont obligatoires");
            }

        }
    }

    private void validateVitiPest() {
        if (CollectionUtils.isNotEmpty(highlights.getVitiPestMasters())) {

            String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (Viticulture), maîtrise des ravageurs: ";

            List<String> pestMastersErrorMessages = new ArrayList<>();

            List<VitiPestMaster> vitiPestMasters = highlights.getVitiPestMasters();

            for (VitiPestMaster vitiPestMaster : vitiPestMasters) {
                if (vitiPestMaster.getTreatmentCount() == null) {
                    pestMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", Nombre de traitements non renseignée !");
                }
                if (vitiPestMaster.getBioControlFungicideIFT() == null) {
                    pestMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", IFT-ravageurs biocontrôle non renseignée !");
                }
                if (vitiPestMaster.getChemicalFungicideIFT() == null) {
                    pestMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", IFT-ravageurs (chimique) non renseignée !");
                }
                if (isNotDephyExpe()) {
                    if (vitiPestMaster.getPressureScale() == null) {
                        pestMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", échelle de pression non renseignée !");
                    }
                    if (vitiPestMaster.getMasterScale() == null) {
                        pestMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", échelle de maîtrise non renseignée !");
                    }
                    if (vitiPestMaster.getQualifier() == null) {
                        pestMastersErrorMessages.add(actionErrorMessage + " " + vitiPestMaster.getAgressor().getLabel() + ", qualification du niveau de maîtrise non renseignée !");
                    }
                }
            }

            for (String pestMastersErrorMessage : pestMastersErrorMessages) {
                addActionError(pestMastersErrorMessage);
            }

            if (reportGrowingSystem.getVitiPestChemicalPestIFT() == null) {
                addFieldError("reportGrowingSystem.vitiPestChemicalPestIFT", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "IFT-ravageurs du système de culture (chimique)  non renseignée !");
            }
            if (reportGrowingSystem.getVitiPestBioControlPestIFT() == null) {
                addFieldError("reportGrowingSystem.vitiPestBioControlPestIFT", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "IFT-ravageurs biocontrôle du système de culture non renseignée !");
            }
            if (reportGrowingSystem.getVitiPestQualifier() == null) {
                addFieldError("reportGrowingSystem.vitiPestQualifier", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Niveau global de maîtrise des ravageurs non renseignée !");
            }

            Set<Integer> pestIds = highlights.getVitiPestMasters().stream()
                    .map(VitiPestMaster::getAgressor)
                    .filter(Objects::nonNull)
                    .map(RefNuisibleEDI.class::cast)
                    .map(RefNuisibleEDI::getReference_id)
                    .collect(Collectors.toSet());
            if (!pestIds.containsAll(Arrays.asList(ReportService.CITADELLE_FLAVESCENCE, ReportService.CITADELLE_GRILLURES, ReportService.TORDEUSE_GRAPPE))) {
                addActionError(actionErrorMessage + "Les ravageurs 'Cicadelle de la flavescence dorée', 'Cicadelle des grillures' et 'Tordeuse(s) de la grappe' sont obligatoires");
            }

        }
    }

    public void validateVitiAdventice() {
        if (isNotDephyExpe() && reportGrowingSystem.getSectors() != null &&
                reportGrowingSystem.getSectors().contains(Sector.VITICULTURE)) {
            String actionErrorMessage = "Bilan des pratiques et État sanitaire des cultures (Viticulture), maîtrise des adventices: ";
            if (reportGrowingSystem.getVitiAdventicePressureScale() == null) {
                addFieldError("reportGrowingSystem.getVitiAdventicePressureScale", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Échelle de pression non renseignée !");
            }
            if (reportGrowingSystem.getVitiAdventiceQualifier() == null) {
                addFieldError("reportGrowingSystem.getVitiAdventiceQualifier", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Résultats obtenus, niveau de maîtrise finale non renseignée !");
            }
            if (reportGrowingSystem.getVitiHerboTreatmentChemical() == null) {
                addFieldError("reportGrowingSystem.getVitiHerboTreatmentChemical", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Nb traitements herbicides chimiques non renseignée !");
            }
            if (reportGrowingSystem.getVitiHerboTreatmentBioControl() == null) {
                addFieldError("reportGrowingSystem.getVitiHerboTreatmentBioControl", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Nb traitements herbicides biocontrol non renseignée !");
            }
            if (reportGrowingSystem.getVitiHerboTreatmentChemicalIFT() == null) {
                addFieldError("reportGrowingSystem.getVitiHerboTreatmentChemicalIFT", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "IFT traitements herbicides chimiques non renseignée !");
            }
            if (reportGrowingSystem.getVitiHerboTreatmentBioControlIFT() == null) {
                addFieldError("reportGrowingSystem.getVitiHerboTreatmentBioControlIFT", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "IFT traitements herbicides biocontrol non renseignée !");
            }
            if (reportGrowingSystem.getVitiSuckeringChemical() == null) {
                addFieldError("reportGrowingSystem.getVitiSuckeringChemical", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Nb epamprage chimiques non renseignée !");
            }
            if (reportGrowingSystem.getVitiSuckeringBioControl() == null) {
                addFieldError("reportGrowingSystem.getVitiSuckeringBioControl", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "Nb epamprage biocontrol non renseignée !");
            }
            if (reportGrowingSystem.getVitiSuckeringChemicalIFT() == null) {
                addFieldError("reportGrowingSystem.getVitiSuckeringChemicalIFT", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "IFT epamprage chimiques non renseignée !");
            }
            if (reportGrowingSystem.getVitiSuckeringBioControlIFT() == null) {
                addFieldError("reportGrowingSystem.getVitiSuckeringBioControlIFT", getText(REQUIRED_FIELD));
                addActionError(actionErrorMessage + "IFT epamprage biocontrol non renseignée !");
            }
        }
    }

    @Override
    public void validate() {

        if (hasFieldErrors()) {
            for (List<String> errors : getFieldErrors().values()) {
                for (String error : errors) {
                    addActionError(error);
                }
            }
        }

        if (!reportGrowingSystem.isPersisted() && reportFilter != null) {

            if (reportFilter.getCampaign() == null) {
                addActionError("Aucune campagne de renseignée !");
                addFieldError("campaign", getText(REQUIRED_FIELD));
            }

            if (reportFilter.getCampaign() != null && !CommonService.getInstance().isCampaignValid(reportFilter.getCampaign())) {
                addActionError("campaign '" + reportFilter.getCampaign() + "' non valide !");
                addFieldError("campaign", getText(REQUIRED_FIELD));
            }

            if (StringUtils.isBlank(growingSystemId)) {
                addActionError("Aucun système de culture sélectionné !");
                addFieldError("selectGrowingSystemError", getText(REQUIRED_FIELD));
            }
        }
    
        if (!StringUtils.isBlank(growingSystemId)) {
            GrowingSystem growingSystem = growingSystemService.getGrowingSystem(growingSystemId);
            if (StringUtils.isBlank(reportRegionalId) && growingSystem != null && TypeDEPHY.DEPHY_EXPE != growingSystem.getGrowingPlan().getType()) {
                addActionError("Aucun bilan de campagne régional sélectionné");
                addFieldError("selectReportRegionalError", getText(REQUIRED_FIELD));
            }
        }

        if (CollectionUtils.isEmpty(reportSectors)) {
            addActionError("Aucune filière sélectionnée !");
            addFieldError("sectorsError", getText(REQUIRED_FIELD));
        } else {
            reportGrowingSystem.setSectors(reportSectors);
        }

        if (StringUtils.isBlank(reportGrowingSystem.getAuthor())) {
            addActionError("Aucun rédacteur pour ce bilan de campagne !");
            addFieldError("authorError", getText(REQUIRED_FIELD));
        }

        validateHighlights();
    
        reportGrowingSystem.setVitiYieldObjective(vitiYieldObjective);
        reportGrowingSystem.setVitiLossCause1(vitiLossCause1);

        if (LOGGER.isErrorEnabled()) {
            String pre = reportGrowingSystem.isPersisted() ? "For reportGrowingSystem with Id '" + reportGrowingSystem.getTopiaId() + "'" : "";

            if (hasFieldErrors()) {
                LOGGER.error("For user email:" + getAuthenticatedUser().getEmail() + ": " + pre + getFieldErrors().toString());
            }
            if (hasActionErrors()) {
                LOGGER.error("For user email:" + getAuthenticatedUser().getEmail() + ": " + pre + getActionErrors().toString());
            }
        }

        if (hasErrors()) {
            initForInput();
        }
    }

    @Override
    @Action(results = {@Result(type = "redirectAction", params = {"actionName", "report-growing-system-edit-input", "reportGrowingSystemId", "${reportGrowingSystemId}"})})
    public String execute() throws Exception {
        reportGrowingSystem = reportService.createOrUpdateReportGrowingSystem(
                reportGrowingSystem,
                growingSystemId,
                reportRegionalId,
                highlights,
                createManagementMode);

        reportGrowingSystemId = reportGrowingSystem.getTopiaId();
        notificationSupport.reportGrowingSystemSaved(reportGrowingSystem);
        return SUCCESS;
    }

    public ReportGrowingSystem getReportGrowingSystem() {
        if (reportGrowingSystem == null) {
            reportGrowingSystem = new ReportGrowingSystemImpl();
        }
        return reportGrowingSystem;
    }

    private void filterOnValidGrowingSystemCrops(List<String> cropIds, FoodMaster foodMaster) {
        Collection<CroppingPlanEntry> crops = foodMaster.getCrops();
        Collection<CroppingPlanEntry> crops0 = crops.stream().filter(crop -> cropIds.contains(crop.getTopiaId())).collect(Collectors.toList());
        foodMaster.setCrops(crops0);
    }
    
    private void filterOnValidGrowingSystemCrops(List<String> cropIds, YieldLoss yieldLoss) {
        Collection<CroppingPlanEntry> crops = yieldLoss.getCrops();
        Collection<CroppingPlanEntry> crops0 = crops.stream().filter(crop -> cropIds.contains(crop.getTopiaId())).collect(Collectors.toList());
        yieldLoss.setCrops(crops0);
    }
    
    private void filterOnValidGrowingSystemCrops(List<String> cropIds, CropPestMaster cropPestMaster) {
        Collection<CroppingPlanEntry> crops = cropPestMaster.getCrops();
        Collection<CroppingPlanEntry> crops0 = crops.stream().filter(crop -> cropIds.contains(crop.getTopiaId())).collect(Collectors.toList());
        cropPestMaster.setCrops(crops0);
    }
    
    private void filterOnValidGrowingSystemCrops(List<String> cropIds, ArboCropPestMaster arboCropPestMaster) {
        Collection<CroppingPlanEntry> crops = arboCropPestMaster.getCrops();
        Collection<CroppingPlanEntry> crops0 = crops.stream().filter(crop -> cropIds.contains(crop.getTopiaId())).collect(Collectors.toList());
        arboCropPestMaster.setCrops(crops0);
    }
    
    private void filterOnValidGrowingSystemCrops(List<String> cropIds, ArboCropAdventiceMaster arboCropPestMaster) {
        Collection<CroppingPlanEntry> crops = arboCropPestMaster.getCrops();
        Collection<CroppingPlanEntry> crops0 = crops.stream().filter(crop -> cropIds.contains(crop.getTopiaId())).collect(Collectors.toList());
        arboCropPestMaster.setCrops(crops0);
    }
    
    private void filterOnValidGrowingSystemCrops(List<String> cropIds, VerseMaster verseMaster) {
        Collection<CroppingPlanEntry> crops = verseMaster.getCrops();
        Collection<CroppingPlanEntry> crops0 = crops.stream().filter(crop -> cropIds.contains(crop.getTopiaId())).collect(Collectors.toList());
        verseMaster.setCrops(crops0);
    }
    
    public void setReportFilter(String json) {
        Type type = new TypeToken<ReportFilter>() {}.getType();
        this.reportFilter = getGson().fromJson(json, type);
    }
    
    public Map<IftEstimationMethod, String> getAllIftEstimationMethods() {
        return getEnumAsMap(IftEstimationMethod.values());
    }
    
    public void setIftEstimationMethod(String iftEstimationMethodName) {
        if (StringUtils.isNotBlank(iftEstimationMethodName)){
            IftEstimationMethod iftEstimationMethod = IftEstimationMethod.valueOf(iftEstimationMethodName);
            reportGrowingSystem.setIftEstimationMethod(iftEstimationMethod);
        }
    }
    
    public void setCreateManagementMode(boolean createManagementMode) {
        this.createManagementMode = createManagementMode;
    }
    
    public void setReportSectors(String json) {
        Type type = new TypeToken<List<Sector>>(){}.getType();
        this.reportSectors = getGson().fromJson(json, type);
    }
    
    // pour les réseaux hors dephy
    public void setCropAdventiceMastersJson(String json) {
        Type type = new TypeToken<List<Map<Sector, List<CropPestMaster>>>>() {}.getType();
        List<Map<Sector, List<CropPestMaster>>> cropMasterBySectorList = getGson().fromJson("[" + json + "]", type);
        setCropMastersJson(cropMasterBySectorList,
                highlights::addDephyExpeCropAdventiceMasters,
                highlights::addNoneDephyExpeCropAdventiceMasters);
    }

    public void setCropDiseaseMastersJson(String json) {
        Type type = new TypeToken<List<Map<Sector, List<CropPestMaster>>>>() {}.getType();
        List<Map<Sector, List<CropPestMaster>>> cropMasterBySectorList = getGson().fromJson("[" + json + "]", type);
        setCropMastersJson(cropMasterBySectorList,
                highlights::addDephyExpeCropDiseaseMasters,
                highlights::addNoneDephyExpeCropDiseaseMasters);
    }

    public void setCropPestMastersJson(String json) {
        Type type = new TypeToken<List<Map<Sector, List<CropPestMaster>>>>() {}.getType();
        List<Map<Sector, List<CropPestMaster>>> cropMasterBySectorList = getGson().fromJson("[" + json + "]", type);
        setCropMastersJson(cropMasterBySectorList,
                highlights::addDephyExpeCropPestMasters,
                highlights::addNoneDephyExpeCropPestMasters);
    }

    public void setVerseMastersJson(String json) {
        Type type = new TypeToken<List<Map<Sector, List<VerseMaster>>>>() {}.getType();
        List<Map<Sector, List<VerseMaster>>> cropMasterBySectorList = getGson().fromJson("[" + json + "]", type);
        setCropMastersJson(cropMasterBySectorList,
                highlights::addDephyExpeVerseMasters,
                highlights::addNoneDephyExpeVerseMasters);
    }

    public void setFoodMastersJson(String json) {
        Type type = new TypeToken<List<Map<Sector, List<FoodMaster>>>>() {}.getType();
        List<Map<Sector, List<FoodMaster>>> cropMasterBySectorList = getGson().fromJson("[" + json + "]", type);
        setCropMastersJson(cropMasterBySectorList,
                highlights::addDephyExpeFoodMasters,
                highlights::addNoneDephyExpeFoodMasters);
    }

    public void setYieldLossesJson(String json) {
        Type type = new TypeToken<List<Map<Sector, List<YieldLoss>>>>() {}.getType();
        List<Map<Sector, List<YieldLoss>>> cropMasterBySectorList = getGson().fromJson("[" + json + "]", type);
        setCropMastersJson(cropMasterBySectorList,
                highlights::addDephyExpeYieldLosses,
                highlights::addNoneDephyExpeYieldLosses);
    }

    protected <T> void setCropMastersJson(List<Map<Sector, List<T>>> cropMasterBySectorList,
                                          BiConsumer<Sector, List<T>> addDephyExpeCropMasters,
                                          Consumer<List<T>> addNonDephyExpeCropMasters) {
        if (cropMasterBySectorList != null) {
            for (Map<Sector, List<T>> cropMasterBySector : cropMasterBySectorList) {
                for (Map.Entry<Sector, List<T>> sectorArrayListEntry : cropMasterBySector.entrySet()) {
                    Sector sector = sectorArrayListEntry.getKey();
                    List<T> cropMasters = sectorArrayListEntry.getValue();
                    if (sector != null) {
                        addDephyExpeCropMasters.accept(sector, cropMasters);
                    } else {
                        addNonDephyExpeCropMasters.accept(cropMasters);
                    }
                }
            }
        }
    }

    //    ARBORICULTURE
    public void setArboCropDiseaseMastersJson(String json) {
        Type type = new TypeToken<List<ArboCropPestMaster>>() {}.getType();
        List<ArboCropPestMaster> arboCropPestMasters = getGson().fromJson(json, type);
        highlights.setArboDiseaseMasters(arboCropPestMasters);
    }
    
    public void setArboCropPestMastersJson(String json) {
        Type type = new TypeToken<List<ArboCropPestMaster>>() {}.getType();
        List<ArboCropPestMaster> arboCropPestMasters = getGson().fromJson(json, type);
        highlights.setArboPestMasters(arboCropPestMasters);
    }
    
    public void setArboCropAdventiceMastersJson(String json) {
        Type type = new TypeToken<List<ArboCropAdventiceMaster>>() {}.getType();
        List<ArboCropAdventiceMaster> arboCropAdventiceMasters = getGson().fromJson(json, type);
        highlights.setArboAdventiceMasters(arboCropAdventiceMasters);
    }
    
    public void setArboFoodMastersJson(String json) {
        Type type = new TypeToken<List<FoodMaster>>() {}.getType();
        List<FoodMaster> foodMasters = getGson().fromJson(json, type);
        if (foodMasters != null) {
            foodMasters.forEach(foodMaster -> foodMaster.setSector(Sector.ARBORICULTURE));
            highlights.addArboFoodMasters(foodMasters);
        }
    }

    public void setArboYieldLossesJson(String json) {
        Type type = new TypeToken<List<YieldLoss>>() {}.getType();
        List<YieldLoss> arboYieldLosses = getGson().fromJson(json, type);
        if (arboYieldLosses != null) {
            arboYieldLosses.forEach(yieldLoss -> yieldLoss.setSector(Sector.ARBORICULTURE));
            highlights.addArboYieldLosses(arboYieldLosses);
        }
    }
    
    // VITICULTURE
    public void setVitiDiseaseMastersJson(String json) {
        Type type = new TypeToken<List<VitiPestMaster>>() {}.getType();
        List<VitiPestMaster> vitiPestMasters = getGson().fromJson(json, type);
        highlights.setVitiDiseaseMasters(vitiPestMasters);
    }
    
    public void setVitiPestMastersJson(String json) {
        Type type = new TypeToken<List<VitiPestMaster>>() {}.getType();
        List<VitiPestMaster> vitiPestMasters = getGson().fromJson(json, type);
        highlights.setVitiPestMasters(vitiPestMasters);
    }
    
    public void setVitiFoodMastersJson(String json) {
        Type type = new TypeToken<List<FoodMaster>>() {}.getType();
        List<FoodMaster> foodMasters = getGson().fromJson(json, type);
        if (foodMasters != null) {
            foodMasters.forEach(foodMaster -> foodMaster.setSector(Sector.VITICULTURE));
            highlights.addVitiFoodMasters(foodMasters);
        }
    }

    public void setYieldInfosJson(String json) {
        Type type = new TypeToken<List<YieldInfo>>() {}.getType();
        List<YieldInfo> yieldInfos = getGson().fromJson("[" + json + "]", type);
        highlights.setYieldInfos(yieldInfos);
    }

    public Map<BioAgressorType, BioAgressorParentType> getTreatmentTargetCategoriesByParent() {
        return Referentials.getTreatmentTargetBioAgressorParentTypes().stream()
                .collect(Collectors.toMap(BioAgressorParentType::getParent, Function.identity()));
    }
}
