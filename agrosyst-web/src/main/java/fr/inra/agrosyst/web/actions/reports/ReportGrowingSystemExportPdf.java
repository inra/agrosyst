/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.web.actions.reports;

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.report.ReportExportOption;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Export PDF des bilan de campagne / echelle system de culture.
 */
public class ReportGrowingSystemExportPdf extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = 9160804528455812557L;
    
    protected transient ReportService reportService;

    protected final transient ReportExportOption exportOptions = new ReportExportOption();

    public void setReportService(ReportService reportService) {
        this.reportService = reportService;
    }

    public ReportExportOption getExportOptions() {
        return exportOptions;
    }

    // delegation because not specific to export
    public void setReportGrowingSystemIds(String reportGrowingSystemIds) {
        getExportOptions().setReportGrowingSystemIds(getGson().fromJson(reportGrowingSystemIds, List.class));
    }

    public void setReportGrowingSystemSectionsJson(String reportGrowingSystemSectionsJson) {
        Map<String, Collection<String>> reportGrowingSystemSections = getGson().fromJson(reportGrowingSystemSectionsJson, Map.class);
        reportGrowingSystemSections.entrySet().removeIf(entry -> CollectionUtils.isEmpty(entry.getValue()));
        getExportOptions().setReportGrowingSystemSections(reportGrowingSystemSections);
    }

    @Override
    protected ExportResult computeExportResult() {
        for (String reportGrowingSystemId : exportOptions.getReportGrowingSystemIds()) {
            authorizationService.checkReportGrowingSystemReadable(reportGrowingSystemId);
        }
        ExportResult result = reportService.exportPdfReportGrowingSystems(exportOptions);

        return result;
    }

}
