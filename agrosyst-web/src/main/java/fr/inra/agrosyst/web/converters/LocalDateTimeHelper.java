package fr.inra.agrosyst.web.converters;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeHelper {

    public static LocalDateTime urlSafeParse(final String str) {
        String decoded = URLDecoder.decode(str, StandardCharsets.UTF_8);
        LocalDateTime result = LocalDateTime.parse(decoded, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        return result;
    }

    public static String urlSafeFormat(final LocalDateTime ldt) {
        String str = ldt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        String result = URLEncoder.encode(str, StandardCharsets.UTF_8);
        return result;
    }

}
