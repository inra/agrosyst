package fr.inra.agrosyst.web;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.opensymphony.xwork2.ActionContext;
import fr.inra.agrosyst.web.actions.admin.RunImport;
import fr.inra.agrosyst.web.actions.commons.AttachmentsUploadJson;
import fr.inra.agrosyst.web.actions.domains.DomainsImportEdaplos;
import fr.inra.agrosyst.web.actions.domains.DomainsPreImportEdaplos;
import fr.inra.agrosyst.web.actions.security.UsersAndRolesImport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.dispatcher.multipart.UploadedFile;
import org.apache.struts2.interceptor.ActionFileUploadInterceptor;

import java.io.Serial;
import java.util.Map;

/**
 * On surcharge {@link org.apache.struts2.interceptor.ActionFileUploadInterceptor} pour aller
 * chercher la taille maximum autorisée et les extensions autorisées dans la configuration.
 *
 * @author kmorin : kmorin@codelutin.com
 * @since 0.8
 */
public class AgrosystWebFileUploadInterceptor extends ActionFileUploadInterceptor {

    @Serial
    private static final long serialVersionUID = 1L;

    private static final Log LOGGER = LogFactory.getLog(AgrosystWebFileUploadInterceptor.class);


    @Override
    protected boolean acceptFile(Object action, UploadedFile file, String filename, String contentType, String inputName) {

        Map<String, Object> application = ActionContext.getContext().getApplication();

        AgrosystWebApplicationContext applicationContext =
                (AgrosystWebApplicationContext) application.get(AgrosystWebApplicationContext.APPLICATION_CONTEXT_PARAMETER);

        AgrosystWebConfig config = applicationContext.getWebConfig();

        setMaximumSize(config.getUploadGlobalMaxSize());

        if (action instanceof AttachmentsUploadJson) {

            setAllowedExtensions(config.getUploadAllowedExtensions());

            setMaximumSize(config.getUploadAttachmentsMaxSize());

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("AttachmentsUpload Upload, allowedTypesSet:");
            }

        } else if ((action instanceof UsersAndRolesImport || action instanceof RunImport)) {

            setAllowedExtensions("csv");

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("UsersAndRolesImport,RunImport Upload");
            }

        } else if(action instanceof DomainsImportEdaplos || action instanceof DomainsPreImportEdaplos) {

            setAllowedExtensions("xml,zip");
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("DomainsImportEdaplos Upload");
            }
        
        } else if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Other Upload, allowedTypesSet");
        }

        boolean result = super.acceptFile(action, file, filename, contentType, inputName);
        return result;

    }

}
