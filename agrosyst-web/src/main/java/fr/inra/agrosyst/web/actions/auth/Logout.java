package fr.inra.agrosyst.web.actions.auth;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class Logout extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = 3750812084600471474L;

    @Override
    @Action(results = { @Result(type = "redirectAction", params = {
            "namespace", "/", "actionName", "index" }) })
    public String execute() throws Exception {
        applicationContext.doLogout(getAuthenticatedUserOptional(), getCookieHelper());
        return SUCCESS;
    }

}
