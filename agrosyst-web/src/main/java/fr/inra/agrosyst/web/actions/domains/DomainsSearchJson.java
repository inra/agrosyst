package fr.inra.agrosyst.web.actions.domains;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;

/**
 * Action qui permet de rechercher des domaines (filtre par nom + pagination).
 * 
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.62
 */
public class DomainsSearchJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(DomainsSearchJson.class);

    @Serial
    private static final long serialVersionUID = -8998970986135363653L;

    @Setter
    protected transient DomainService domainService;

    @Setter
    protected int pageIndex = 0;
    @Setter
    protected int pageSize = DEFAULT_SEARCH_PAGE_SIZE;
    @Setter
    protected String term;

    @Override
    public String execute() {
        try {
            NavigationContext navigationContext = getNavigationContext();

            DomainFilter domainFilter = new DomainFilter();
            domainFilter.setNavigationContext(navigationContext);
            domainFilter.setActive(true);
            if (StringUtils.isNotEmpty(term)) {
                domainFilter.setDomainName(term);
            }
            domainFilter.setPage(pageIndex);
            domainFilter.setPageSize(pageSize);

            jsonData = domainService.getFilteredDomainsDto(domainFilter);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load domains list", e);
            }
            return ERROR;
        }

        return SUCCESS;
    }
}
