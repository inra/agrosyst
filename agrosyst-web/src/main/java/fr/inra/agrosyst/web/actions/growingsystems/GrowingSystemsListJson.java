package fr.inra.agrosyst.web.actions.growingsystems;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.util.HashSet;
import java.util.Set;

public class GrowingSystemsListJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(GrowingSystemsListJson.class);

    @Serial
    private static final long serialVersionUID = -5974287104680221588L;

    protected transient GrowingSystemService growingSystemService;
    
    protected transient boolean fromNavigationContextChoose = false;
    protected transient String filter;
    protected transient Set<Integer> selectedCampaigns = new HashSet<>();
    protected transient Set<String> selectedNetworks = new HashSet<>();
    protected transient Set<String> selectedDomains = new HashSet<>();
    protected transient Set<String> selectedGrowingPlans = new HashSet<>();
    
    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }
    
    public void setFromNavigationContextChoose(boolean fromNavigationContextChoose) {
        this.fromNavigationContextChoose = fromNavigationContextChoose;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public void setSelectedCampaigns(Set<Integer> selectedCampaigns) {
        this.selectedCampaigns = selectedCampaigns;
    }

    public void setSelectedNetworks(Set<String> selectedNetworks) {
        this.selectedNetworks = selectedNetworks;
    }

    public void setSelectedDomains(Set<String> selectedDomains) {
        this.selectedDomains = selectedDomains;
    }

    public void setSelectedGrowingPlans(Set<String> selectedGrowingPlans) {
        this.selectedGrowingPlans = selectedGrowingPlans;
    }

    @Override
    public String execute() {
        try {
            GrowingSystemFilter growingSystemFilter = getGson().fromJson(filter, GrowingSystemFilter.class);

            NavigationContext navigationContext;
            if (fromNavigationContextChoose) {
                navigationContext = new NavigationContext(selectedCampaigns, selectedNetworks, selectedDomains, selectedGrowingPlans, null);
                writeListNbElementByPage(GrowingSystemDto.class, NAVIGATION_CONTEXT, growingSystemFilter.getPageSize());
            } else {
                navigationContext = getNavigationContext();
                writeListNbElementByPage(GrowingSystemDto.class, growingSystemFilter.getPageSize());
            }
            growingSystemFilter.setNavigationContext(navigationContext);

            jsonData = growingSystemService.getFilteredGrowingSystemsDto(growingSystemFilter);

            return SUCCESS;

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Failed to load growing systems ", e);
            }
            return ERROR;
        }
    }
}
