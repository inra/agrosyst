package fr.inra.agrosyst.web.actions.itk;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.domain.ToolsCouplingDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LoadCopyPasteGlobalInfoJson extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = -6352586060492623909L;

    private static final Log LOGGER = LogFactory.getLog(LoadCopyPasteGlobalInfoJson.class);

    protected transient EffectiveCropCycleService effectiveService;
    protected transient PracticedSystemService practicedSystemService;
    protected transient ReferentialService referentialService;

    protected transient String agrosystInterventionTypes;

    // for practiced crop cycle
    protected transient String growingSystemId;
    protected transient String campaigns;

    // for effective crop cycle
    protected transient String zoneTopiaId;

    protected transient Map<AgrosystInterventionType, List<ToolsCouplingDto>> agrosystInterventionTypesToolsCouplingDtos;

    public void setEffectiveService(EffectiveCropCycleService effectiveService) {
        this.effectiveService = effectiveService;
    }

    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }

    public void setGrowingSystemId(String growingSystemId) {
        this.growingSystemId = growingSystemId;
    }

    public void setCampaigns(String campaigns) {
        this.campaigns = campaigns;
    }

    public void setZoneTopiaId(String zoneTopiaId) {
        this.zoneTopiaId = zoneTopiaId;
    }

    @Override
    public String execute() {
        try {
            Set<AgrosystInterventionType> interventionTypes = getAgrosystInterventionTypes(agrosystInterventionTypes);
            Map<String, List<Pair<String, String>>> allCodeEspeceBotaniqueCodeQualifantBySpeciesCode;
            Map<String, List<Sector>> allSectorByCodeEspeceBotaniqueCodeQualifiant;
            Map<String, DomainInputDto> domainInputs;
            if (Strings.isNullOrEmpty(zoneTopiaId)) {
                agrosystInterventionTypesToolsCouplingDtos = practicedSystemService.getToolsCouplingModelForInterventionTypes(growingSystemId, campaigns, interventionTypes);
                allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = practicedSystemService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(growingSystemId, campaigns);
                allSectorByCodeEspeceBotaniqueCodeQualifiant = practicedSystemService.getSectorByCodeEspceBotaniqueCodeQualifiant(growingSystemId, campaigns);
                domainInputs = practicedSystemService.getDomainInputs(growingSystemId, campaigns);
            } else {
                agrosystInterventionTypesToolsCouplingDtos = effectiveService.getToolsCouplingModelForInterventionTypes(zoneTopiaId, interventionTypes);
                allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = effectiveService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(zoneTopiaId);
                allSectorByCodeEspeceBotaniqueCodeQualifiant = effectiveService.getSectorByCodeEspceBotaniqueCodeQualifiant(zoneTopiaId);
                domainInputs = new HashMap<>();
            }

            Map<String, Object> result = new HashMap<>();
            result.put("agrosystInterventionTypesToolsCouplingDtos", agrosystInterventionTypesToolsCouplingDtos);
            result.put("allCodeEspeceBotaniqueCodeQualifantBySpeciesCode", allCodeEspeceBotaniqueCodeQualifantBySpeciesCode);
            result.put("allSectorByCodeEspeceBotaniqueCodeQualifiant", allSectorByCodeEspeceBotaniqueCodeQualifiant);
            result.put("practicedDomainInputStockUnitByCodes", domainInputs);
            jsonData = result;
            return SUCCESS;
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Exception on harvesting price loading, parameters growingSystemId '%s', campaigns '%s', zoneId '%s':",
                        growingSystemId, campaigns, zoneTopiaId), e);
            }
            return ERROR;
        }

    }
    
    public void setAgrosystInterventionTypes(String json) {
        this.agrosystInterventionTypes = json;
    }

    private Set<AgrosystInterventionType> getAgrosystInterventionTypes(String json) {
        Set<AgrosystInterventionType> agrosystInterventionTypes;
        Type type = new TypeToken<Set<AgrosystInterventionType>>(){}.getType();
        agrosystInterventionTypes = getGson().fromJson(json, type);
        return agrosystInterventionTypes;
    }

}
