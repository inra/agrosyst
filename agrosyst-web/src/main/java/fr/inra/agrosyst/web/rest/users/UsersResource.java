package fr.inra.agrosyst.web.rest.users;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.web.rest.Secured;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static fr.inra.agrosyst.web.filters.AgrosystWebAuthenticationFilter.AUTHENTICATED_USER;

@Secured
@Path("/users")
public class UsersResource {

    @Inject
    private HttpServletRequest httpServletRequest;

    @GET
    @Path("/current")
    @Produces(MediaType.APPLICATION_JSON)
    public Response read() {
        var currentUser = (AuthenticatedUser) httpServletRequest.getAttribute(AUTHENTICATED_USER);
        return Response.ok()
                .entity(new UserDto(currentUser.getFirstName(), currentUser.getLastName(), currentUser.isAdmin()))
                .build();
    }
}
