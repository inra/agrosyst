package fr.inra.agrosyst.web.actions.domains;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.edaplos.EdaplosService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.action.UploadedFilesAware;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.dispatcher.multipart.UploadedFile;

import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Import eDaplos xml file.
 * 
 * @author Eric Chatellier
 */
@Results({
        @Result(type = "redirectAction", name = "input", params = {"actionName", "domains-list", "importFileError", "true"})
})
public class DomainsImportEdaplos extends AbstractAgrosystAction implements UploadedFilesAware {

    private static final Log LOGGER = LogFactory.getLog(DomainsImportEdaplos.class);
    @Serial
    private static final long serialVersionUID = -135082559664573570L;
    
    protected transient EdaplosService edaplosService;

    protected String edaplosFileId;

    public void setEdaplosService(EdaplosService domainService) {
        this.edaplosService = domainService;
    }

    public void setEdaplosFileId(String edaplosFileId) {
        this.edaplosFileId = edaplosFileId;
    }

    @Override
    @Action(results = {
            @Result(type = "redirectAction", params = {"actionName", "domains-list"})})
    public String execute() throws Exception {
        try {
            edaplosService.importEdaplos(this.edaplosFileId);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't import", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }


    @Override
    public void withUploadedFiles(List<UploadedFile> uploadedFiles) {
        if (LOGGER.isDebugEnabled()) {
            Collection<UploadedFile> uploadedFiles1 = CollectionUtils.emptyIfNull(uploadedFiles);
            LOGGER.debug(String.format("L'utilisateur %s à envoyé les fichiers Edaplos %s", getAuthenticatedUser().getEmail(), uploadedFiles1.stream().map(UploadedFile::getOriginalName).collect(Collectors.joining())));
        }
    }
}
