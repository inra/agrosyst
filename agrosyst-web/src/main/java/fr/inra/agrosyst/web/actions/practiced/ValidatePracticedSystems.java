package fr.inra.agrosyst.web.actions.practiced;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.Status;
import fr.inra.agrosyst.api.services.practiced.OperationStatus;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * Validation d'un système synthétisé
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 0.11
 */
public class ValidatePracticedSystems extends AbstractAgrosystAction {
    
    @Serial
    private static final long serialVersionUID = -2469399370672464216L;
    
    protected transient PracticedSystemService practicedSystemService;

    protected ArrayList<String> practicedSystemIds;
    
    protected boolean validate;

    @Override
    @Action(results = {
            @Result(name = SUCCESS, type = "redirectAction", params = {"actionName", "practiced-systems-list"})})
    public String execute() throws Exception {
        
    
        List<OperationStatus> results = practicedSystemService.validate(practicedSystemIds, validate);
        
        for (OperationStatus result : results) {
            
            if (Status.SUCCES == result.getStatus()) {
                notificationSupport.practicedSystemValidated(result.getComment());
            } else {
                notificationSupport.practicedSystemValidatedError(result.getComment());
            }
        }
        return SUCCESS;
    }
    
    
    public void setPracticedSystemService(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }
    
    public void setPracticedSystemIds(String practicedSystemIds) {
        this.practicedSystemIds = getGson().fromJson(practicedSystemIds, ArrayList.class);
    }
    
    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
