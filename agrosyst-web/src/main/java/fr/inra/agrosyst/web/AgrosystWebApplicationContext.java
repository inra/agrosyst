package fr.inra.agrosyst.web;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import fr.inra.agrosyst.api.entities.AgrosystTopiaApplicationContext;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AgrosystConfigurationHelper;
import fr.inra.agrosyst.services.AgrosystServiceConfig;
import fr.inra.agrosyst.web.actions.security.JwtHelper;
import fr.inra.agrosyst.web.listeners.AgrosystWebApplicationListener;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.topia.persistence.TopiaConfiguration;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystWebApplicationContext {

    private static final Log LOGGER = LogFactory.getLog(AgrosystWebApplicationContext.class);

    public static final LocalDateTime RUNNING_SINCE = LocalDateTime.now();

    public static final String APPLICATION_CONTEXT_PARAMETER = AgrosystWebApplicationContext.class.getName();

    public static final String CONFIG_FILE_PREFIX = "agrosyst-";
    public static final String CONFIG_FILE_EXT = ".properties";
    public static final String DEFAULT_CONTEXT_NAME = "ROOT";
    
    public static final String MISSING_APPLICATION_CONTEXT =
            AgrosystWebApplicationContext.class.getSimpleName() + " not found. You probably forgot to" +
                    " register " + AgrosystWebApplicationListener.class.getName() + " in your web.xml";

    public static final String TOKEN_COOKIE_NAME = "X-Agrosyst-Token";

    public static final String SEPARATOR = "/";
    
    protected final AgrosystWebConfig webConfig;
    
    protected final AgrosystServiceConfig serviceConfig;
    
    protected final AgrosystTopiaApplicationContext applicationContext;
    
    protected final String configFileName;

    protected final AgrosystWebDistributedCacheSupport distributedCacheSupport;

    /**
     * Banner id =&lt; {banner path, banner copyright}.
     */
    protected final Map<String, String[]> bannersMap = new HashMap<>();
    
    public AgrosystWebApplicationContext(ServletContext servletContext) {

        // L'usage des bundles est plus safe que le ClassPathI18nInitializer pour la compatibilité avec Java 9+
        I18n.init(new DefaultI18nInitializer("agrosyst-web"), Locale.FRANCE);
        
        this.configFileName = getConfigFileName(servletContext);
        
        LOGGER.info("Load config file name:" + this.configFileName);
        
        this.webConfig = new AgrosystWebConfig(configFileName);

        try {
            this.serviceConfig = new AgrosystServiceConfig(configFileName);

            Properties contextProperties = this.serviceConfig.getFlatOptions();
            TopiaConfiguration topiaConfiguration = AgrosystConfigurationHelper.getPersistanceConfiguration(contextProperties);

            this.applicationContext = AgrosystConfigurationHelper.newApplicationContext(this.serviceConfig, topiaConfiguration);

        } catch (Exception eee) {
            throw new AgrosystTechnicalException("An exception occurred", eee);
        }

        this.distributedCacheSupport = new AgrosystWebDistributedCacheSupport(this.webConfig);
    }

    public void reloadConfig() {
        webConfig.reloadConfig();
    }
    
    private String getConfigFileName(ServletContext servletContext) {
        String configFileName = servletContext.getContextPath();
        if (configFileName != null) {
            String baseName = StringUtils.remove(configFileName, SEPARATOR);
            if (StringUtils.isBlank(baseName)) {
                baseName = DEFAULT_CONTEXT_NAME;
            }
            configFileName = StringUtils.join(CONFIG_FILE_PREFIX, baseName, CONFIG_FILE_EXT);
        }
        return configFileName;
    }

    public void initApplicationBanners(ServletContext servletContext) {
        Set<String> resources = servletContext.getResourcePaths("/img/Header-Backgrounds/");
        for (String resource : resources) {
            String extension = FilenameUtils.getExtension(resource);
            String baseName = FilenameUtils.getBaseName(resource);
            if ("jpg".equals(extension)) {
                String metaPath = resource.replace("." + extension, ".txt");

                try (InputStream metaStream = servletContext.getResourceAsStream(metaPath)) {

                    String metaContent = IOUtils.toString(metaStream, Charset.defaultCharset());
                    
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("Adding banner " + baseName + "(" + metaContent + ")");
                    }

                    // make banner path context relative
                    String resourceContextPath = StringUtils.removeStart(resource, "/");
                    bannersMap.put(baseName, new String[]{resourceContextPath, metaContent});
                } catch (IOException ex) {
                    throw new AgrosystTechnicalException("Can't read banner metadata", ex);
                }
            }
        }
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Detecting " + bannersMap.size() + " banners");
        }
    }

    public AgrosystWebConfig getWebConfig() {
        return webConfig;
    }

    public AgrosystServiceConfig getServiceConfig() {
        return serviceConfig;
    }

    public AgrosystTopiaApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public Map<String, String[]> getBannersMap() {
        return bannersMap;
    }

    public void close() {
        if (applicationContext != null && !applicationContext.isClosed()) {
            applicationContext.close();
        }
        if (this.distributedCacheSupport != null) {
            this.distributedCacheSupport.shutdown();
        }
    }

    public <K, V> Map<K, V> getDistributedCache(String name) {
        Map<K, V> result = distributedCacheSupport.getCache(name);
        return result;
    }

    public Map<String, String> getSessionsCache() {
        int timeoutSeconds = webConfig.getHttpSessionTimeout();
        Map<String, String> result = distributedCacheSupport.getCacheWithExpiration("sessions", timeoutSeconds);
        return result;
    }

    public Optional<String> readAuthenticationCookie(CookieHelper cookieHelper) {
        Optional<String> result = cookieHelper.readCookie(TOKEN_COOKIE_NAME);
        return result;
    }

    public void writeAuthenticationCookie(AuthenticatedUser authenticatedUser, CookieHelper cookieHelper) {
        JwtHelper jwtHelper = new JwtHelper(webConfig);

        // On reprend la marge du délai re rafraichissement du token pour être sûr de pas faire expirer le cookie avant le token
        int maxAge = webConfig.getHttpSessionTimeout() + webConfig.getJwtRefreshSeconds();
        cookieHelper.writeCookie(TOKEN_COOKIE_NAME, authenticatedUser, jwtHelper::createJwtToken, maxAge, true);

        // On conserve une trace de la session dans un cache partagé pour des stats
        Map<String, String> sessions = getSessionsCache();
        // On va hasher le userId pour éviter que l'info se trimballe alors que tout ce qui nous intéresse c'est de savoir que c'est le même utilisateur
        String userIdHashed = Hashing.murmur3_128()
                .hashString(authenticatedUser.getTopiaId(), Charsets.UTF_8)
                .toString();
        sessions.put(authenticatedUser.getSid(), userIdHashed);
    }

    public void doLogout(Optional<AuthenticatedUser> authenticatedUser, CookieHelper cookieHelper) {
        cookieHelper.deleteCookie(TOKEN_COOKIE_NAME);

        // Si l'utilisateur est connecté, on peut retirer la trace de se session
        authenticatedUser.map(AuthenticatedUser::getSid).ifPresent(sid -> getSessionsCache().remove(sid));
    }

}
