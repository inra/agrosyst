package fr.inra.agrosyst.web.actions.auth;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.Serial;

/**
 *
 */
public class ForgottenPasswordJson extends AbstractJsonAction {
    
    @Serial
    private static final long serialVersionUID = -2652347612996782319L;
    private UserService userService;
    
    private String email;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String execute() throws Exception {
        if (StringUtils.isNotBlank(email)
                && !userService.askForPasswordReminder(email, "")
                && !userService.isUserActive(email)) {
            jsonData = "Votre compte est désactivé, merci de contacter un administrateur d'Agrosyst.";
            httpCode = HttpServletResponse.SC_UNAUTHORIZED;
            return ERROR;
        }

        jsonData = "Email de récupération du mot de passe envoyé, consultez votre boîte mail";
        return SUCCESS;
    }
}
