package fr.inra.agrosyst.web.rest.growingsystems;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2025 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.web.rest.common.EntityRequestFilter;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class GrowingSystemRequestFilter extends EntityRequestFilter {
    protected Set<String> selectedDomains;
    protected String name;
    protected String domainName;
    protected String gorwinPlanName;
    protected Integer campaign;
    protected Sector sector;
    protected String dephyNumber;
}
