package fr.inra.agrosyst.web.actions.admin;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import fr.inra.agrosyst.api.services.async.AsyncService;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.Serial;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class StopTask extends AbstractAgrosystAction {
    @Serial
    private static final long serialVersionUID = -2744129914890603219L;
    
    private AsyncService asyncService;
    
    protected String taskId;
    
    public void setAsyncService(AsyncService asyncService) {
        this.asyncService = asyncService;
    }
    
    @Action(results = {@Result(type = "redirectAction", params = {
            "namespace", "/admin", "actionName", "admin-task"})})
    @Override
    public String execute() throws Exception {
        if (authorizationService.isAdmin() && StringUtils.isNotBlank(taskId)) {
            asyncService.cancelTask(taskId);
        }
        return SUCCESS;
    }
    
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
