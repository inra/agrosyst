package fr.inra.agrosyst.web.actions.practiced;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet de déclencher un export asynchrone des systèmes synthétisés.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class PracticedSystemsExportAsync extends AbstractJsonAction {

    @Serial
    private static final long serialVersionUID = 4620374134218137834L;

    protected List<String> practicedSystemIds;

    protected transient PracticedSystemService service;

    public void setService(PracticedSystemService service) {
        this.service = service;
    }

    public void setPracticedSystemIds(String practicedSystemIds) {
        this.practicedSystemIds = getGson().fromJson(practicedSystemIds, ArrayList.class);
    }

    @Override
    public String execute() {
        service.exportPracticedSystemsAsXlsAsync(practicedSystemIds);
        return SUCCESS;
    }

}
