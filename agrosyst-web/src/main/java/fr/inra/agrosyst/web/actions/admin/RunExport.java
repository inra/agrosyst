package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefGesCarburant;
import fr.inra.agrosyst.api.entities.referential.RefGesEngrais;
import fr.inra.agrosyst.api.entities.referential.RefGesPhyto;
import fr.inra.agrosyst.api.entities.referential.RefGesSemence;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefNrjCarburant;
import fr.inra.agrosyst.api.entities.referential.RefNrjEngrais;
import fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil;
import fr.inra.agrosyst.api.entities.referential.RefNrjPhyto;
import fr.inra.agrosyst.api.entities.referential.RefNrjSemence;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefSaActaIphy;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnits;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI;
import fr.inra.agrosyst.api.entities.referential.RefUniteEDI;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.referential.ExportService;
import fr.inra.agrosyst.services.referential.csv.RefEspeceDto;
import fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;
import java.util.Collections;
import java.util.List;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class RunExport extends AbstractAdminAction {

    private static final Log LOGGER = LogFactory.getLog(RunExport.class);

    @Serial
    private static final long serialVersionUID = -5821886179843474360L;

    protected transient ExportService exportService;

    public void setImportService(ExportService exportService) {
        this.exportService = exportService;
    }

    protected String genericClassName;
    protected List<String> entityIds;
    protected boolean exportAllEntities;

    public void setGenericClassName(String genericClassName) {
        this.genericClassName = genericClassName;
    }

    public String getGenericClassName() {
        return genericClassName;
    }

    public void setEntityIds(String entityIds) {
        this.entityIds = getGson().fromJson(entityIds, List.class);
    }

    public void setExportAllEntities(boolean exportAllEntities) {
        this.exportAllEntities = exportAllEntities;
    }

    @Override
    @Action(results= {@Result(type="stream", params={"contentType", "text/csv",
            "inputName", "inputStream", "contentDisposition", "attachment; filename=\"${filename}\""})})
    public String execute() throws Exception {
        checkIsAdmin();
        return SUCCESS;
    }
    
    public InputStream getInputStream() {
        InputStream inputStream = null;
        try {
            Class<?> klass = Class.forName(genericClassName);

            if (exportAllEntities) {
                entityIds = null;
            }

            if (RefSolArvalis.class.equals(klass)) {
                inputStream = exportService.exportSolArvalisCSV(entityIds);
            }

            if (RefLocationExtendedDto.class.equals(klass)) {
                inputStream = exportService.exportRefLocation(entityIds);
            }

            if (RefCountry.class.equals(klass)) {
                inputStream = exportService.exportRefCountry(entityIds);
            }

            if (RefCorrespondanceMaterielOutilsTS.class.equals(klass)) {
                inputStream = exportService.exportRefCorrespodanceMaterielOutilsTS(entityIds);
            }

            if (RefMaterielTraction.class.equals(klass)) {
                inputStream = exportService.exportMaterielTracteursCSV(entityIds);
            }

            if (RefMaterielIrrigation.class.equals(klass)) {
                inputStream = exportService.exportMaterielIrrigationCSV(entityIds);
            }

            if (RefMaterielAutomoteur.class.equals(klass)) {
                inputStream = exportService.exportMaterielAutomoteursCSV(entityIds);
            }

            if (RefMaterielOutil.class.equals(klass)) {
                inputStream = exportService.exportMaterielOutilsCSV(entityIds);
            }

            if (RefLegalStatus.class.equals(klass)) {
                inputStream = exportService.exportLegalStatusCSV(entityIds);
            }
    
            if (RefEspeceDto.class.equals(klass)) {
                inputStream = exportService.exportEspeces(entityIds);
            }

            if (RefVarieteGeves.class.equals(klass)) {
                inputStream = exportService.exportVarietesGeves(entityIds);
            }

            if (RefEspeceToVariete.class.equals(klass)) {
                inputStream = exportService.exportEspecesToVarietes(entityIds);
            }

            if (RefOTEX.class.equals(klass)) {
                inputStream = exportService.exportOtexCSV(entityIds);
            }

            if (RefClonePlantGrape.class.equals(klass)) {
                inputStream = exportService.exportClonesPlantGrape(entityIds);
            }

            if (RefVarietePlantGrape.class.equals(klass)) {
                inputStream = exportService.exportVarietesPlantGrape(entityIds);
            }

            if (RefOrientationEDI.class.equals(klass)) {
                inputStream = exportService.exportOrientationEdiCSV(entityIds);
            }

            if (RefSolTextureGeppa.class.equals(klass)) {
                inputStream = exportService.exportSolTextureGeppa(entityIds);
            }

            if (RefParcelleZonageEDI.class.equals(klass)) {
                inputStream = exportService.exportZonageParcelleEdi(entityIds);
            }

            if (RefSolProfondeurIndigo.class.equals(klass)) {
                inputStream = exportService.exportSolProfondeurIndigo(entityIds);
            }

            if (RefSolCaracteristiqueIndigo.class.equals(klass)) {
                inputStream = exportService.exportSolCarateristiquesIndigo(entityIds);
            }
            
            if (RefAdventice.class.equals(klass)) {
                inputStream = exportService.exportAdventices(entityIds);
            }
            
            if (RefAgsAmortissement.class.equals(klass)) {
                inputStream = exportService.exportAgsAmortissement(entityIds);
            }
            
            if (RefNuisibleEDI.class.equals(klass)) {
                inputStream = exportService.exportNuisiblesEDI(entityIds);
            }
            
            if (RefFertiMinUNIFA.class.equals(klass)) {
                inputStream = exportService.exportFertiMinUNIFA(entityIds);
            }
            
            if (RefFertiOrga.class.equals(klass)) {
                inputStream = exportService.exportFertiOrga(entityIds);
            }
            
            if (RefInterventionAgrosystTravailEDI.class.equals(klass)) {
                inputStream = exportService.exportInterventionAgrosystTravailEdiCSV(entityIds);
            }
            
            if (RefStadeEDI.class.equals(klass)) {
                inputStream = exportService.exportStadesEdiCSV(entityIds);
            }
            
            if (RefUniteEDI.class.equals(klass)) {
                inputStream = exportService.exportUniteEDI(entityIds);
            }
            
            if (RefStationMeteo.class.equals(klass)) {
                inputStream = exportService.exportStationMeteo(entityIds);
            }
            
            if (RefGesCarburant.class.equals(klass)) {
                inputStream = exportService.exportGesCarburants(entityIds);
            }
            
            if (RefGesEngrais.class.equals(klass)) {
                inputStream = exportService.exportGesEngrais(entityIds);
            }
            
            if (RefGesPhyto.class.equals(klass)) {
                inputStream = exportService.exportGesPhyto(entityIds);
            }
            
            if (RefGesSemence.class.equals(klass)) {
                inputStream = exportService.exportGesSemences(entityIds);
            }
            
            if (RefNrjCarburant.class.equals(klass)) {
                inputStream = exportService.exportNrjCarburants(entityIds);
            }
            
            if (RefNrjEngrais.class.equals(klass)) {
                inputStream = exportService.exportNrjEngrais(entityIds);
            }

            if (RefNrjPhyto.class.equals(klass)) {
                inputStream = exportService.exportNrjPhyto(entityIds);
            }

            if (RefNrjSemence.class.equals(klass)) {
                inputStream = exportService.exportNrjSemences(entityIds);
            }

            if (RefNrjGesOutil.class.equals(klass)) {
                inputStream = exportService.exportNrjGesOutils(entityIds);
            }

            if (RefMesure.class.equals(klass)) {
                inputStream = exportService.exportMesure(entityIds);
            }

            if (RefSupportOrganeEDI.class.equals(klass)) {
                inputStream = exportService.exportSupportOrganeEDI(entityIds);
            }
            
            if (RefStadeNuisibleEDI.class.equals(klass)) {
                inputStream = exportService.exportStadeNuisibleEDI(entityIds);
            }
            
            if (RefTypeNotationEDI.class.equals(klass)) {
                inputStream = exportService.exportTypeNotationEDI(entityIds);
            }
            
            if (RefValeurQualitativeEDI.class.equals(klass)) {
                inputStream = exportService.exportValeurQualitativeEDI(entityIds);
            }
            
            if (RefUnitesQualifiantEDI.class.equals(klass)) {
                inputStream = exportService.exportUnitesQualifiantEDI(entityIds);
            }

            if (RefActaSubstanceActive.class.equals(klass)) {
                inputStream = exportService.exportActaSubstanceActive(entityIds);
            }

            if (RefProtocoleVgObs.class.equals(klass)) {
                inputStream = exportService.exportProtocoleVgObs(entityIds);
            }
            
            if (RefElementVoisinage.class.equals(klass)) {
                inputStream = exportService.exportElementVoisinage(entityIds);
            }
            
            if (RefPhytoSubstanceActiveIphy.class.equals(klass)) {
                inputStream = exportService.exportPhytoSubstanceActiveIphy(entityIds);
            }

            if (RefTypeAgriculture.class.equals(klass)) {
                inputStream = exportService.exportTypeAgriculture(entityIds);
            }

            if (RefActaGroupeCultures.class.equals(klass)) {
                inputStream = exportService.exportActaGroupeCultures(entityIds);
            }

            if (RefActaDosageSPC.class.equals(klass)) {
                inputStream = exportService.exportActaDosageSpc(entityIds);
            }

            if (RefSaActaIphy.class.equals(klass)) {
                inputStream = exportService.exportSaActaIphy(entityIds);
            }

            if (RefCultureEdiGroupeCouvSol.class.equals(klass)) {
                inputStream = exportService.exportCultureEdiGroupeCouvSol(entityIds);
            }

            if (RefCouvSolAnnuelle.class.equals(klass)) {
                inputStream = exportService.exportCouvSolAnnuelle(entityIds);
            }
            
            if (RefCouvSolPerenne.class.equals(klass)) {
                inputStream = exportService.exportCouvSolPerenne(entityIds);
            }
            
            if (RefActaTraitementsProduit.class.equals(klass)) {
                inputStream = exportService.exportActaTraitementsProducts(entityIds);
            }
            
            if (RefActaTraitementsProduitsCateg.class.equals(klass)) {
                inputStream = exportService.exportActaTraitementsProductsCateg(entityIds);
            }
            
            if (RefTraitSdC.class.equals(klass)) {
                inputStream = exportService.exportTraitSdC(entityIds);
            }
            
            if (RefZoneClimatiqueIphy.class.equals(klass)) {
                inputStream = exportService.exportZoneClimatiqueIphy(entityIds);
            }

            if (RefDestination.class.equals(klass)) {
                inputStream = exportService.exportDestination(entityIds);
            }

            if (RefQualityCriteria.class.equals(klass)) {
                inputStream = exportService.exportQualityCriteria(entityIds);
            }

            if (RefHarvestingPrice.class.equals(klass)) {
                inputStream = exportService.exportRefHarvestingPrice(entityIds);
            }

            if (RefHarvestingPrice.class.equals(klass)) {
                inputStream = exportService.exportRefHarvestingPrice(entityIds);
            }

            if (RefHarvestingPriceConverter.class.equals(klass)) {
                inputStream = exportService.exportRefHarvestingPriceConverter(entityIds);
            }

            if (RefSpeciesToSector.class.equals(klass)) {
                inputStream = exportService.exportRefSpeciesToSector(entityIds);
            }

            if (RefStrategyLever.class.equals(klass)) {
                inputStream = exportService.exportRefStrategyLever(entityIds);
            }

            if (RefAnimalType.class.equals(klass)) {
                inputStream = exportService.exportRefAnimalType(entityIds);
            }

            if (RefMarketingDestination.class.equals(klass)) {
                inputStream = exportService.exportRefMarketingDestination(entityIds);
            }

            if (RefInterventionTypeItemInputEDI.class.equals(klass)) {
                inputStream = exportService.exportRefWorkItemInputEdi(entityIds);
            }

            if (RefQualityCriteriaClass.class.equals(klass)) {
                inputStream = exportService.exportRefQualityCriteriaClass(entityIds);
            }

            if (RefActaDosageSaRoot.class.equals(klass)) {
                inputStream = exportService.exportRefActaDosageSaRoot(entityIds);
            }

            if (RefActaProduitRoot.class.equals(klass)) {
                inputStream = exportService.exportRefActaProduitRoot(entityIds);
            }

            if (RefActaDosageSpcRoot.class.equals(klass)) {
                inputStream = exportService.exportRefActaDosageSpcRoot(entityIds);
            }

            if (RefPrixCarbu.class.equals(klass)) {
                inputStream = exportService.exportRefPrixCarbuCSV(entityIds);
            }

            if (RefPrixEspece.class.equals(klass)) {
                inputStream = exportService.exportRefPrixEspeceCSV(entityIds);
            }

            if (RefPrixFertiMin.class.equals(klass)) {
                inputStream = exportService.exportRefPrixFertiMinCSV(entityIds);
            }

            if (RefPrixFertiOrga.class.equals(klass)) {
                inputStream = exportService.exportRefPrixFertiOrgaCSV(entityIds);
            }

            if (RefPrixPhyto.class.equals(klass)) {
                inputStream = exportService.exportRefPrixPhytoCSV(entityIds);
            }

            if (RefPrixIrrig.class.equals(klass)) {
                inputStream = exportService.exportRefPrixIrrigCSV(entityIds);
            }

            if (RefPrixAutre.class.equals(klass)) {
                inputStream = exportService.exportRefPrixAutreCSV(entityIds);
            }

            if (RefInputUnitPriceUnitConverter.class.equals(klass)) {
                inputStream = exportService.exportRefInputUnitPriceUnitConverterCSV(entityIds);
            }

            if (RefEspeceOtherTools.class.equals(klass)) {
                inputStream = exportService.exportRefEspeceOtherToolsCSV(entityIds);
            }

            if (RefEdaplosTypeTraitement.class.equals(klass)) {
                inputStream = exportService.exportRefEdaplosTypeTraitement(entityIds);
            }

            if (RefCattleAnimalType.class.equals(klass)) {
                inputStream = exportService.exportRefCattleAnimalType(entityIds, Collections.emptyList());
            }

            if (RefCattleRationAliment.class.equals(klass)) {
                inputStream = exportService.exportRefCattleRationAliment(entityIds);
            }

            if (RefMAADosesRefParGroupeCible.class.equals(klass)) {
                inputStream = exportService.exportRefMAADosesRefParGroupeCible(entityIds);
            }

            if (RefMAABiocontrole.class.equals(klass)) {
                inputStream = exportService.exportRefMAABiocontrole(entityIds);
            }

            if (RefCiblesAgrosystGroupesCiblesMAA.class.equals(klass)) {
                inputStream = exportService.exportRefCiblesAgrosystGroupesCiblesMAA(entityIds);
            }
    
            if (RefFeedbackRouter.class.equals(klass)) {
                inputStream = exportService.exportRefFeedbackRouter(entityIds);
            }

            if (RefSubstrate.class.equals(klass)) {
                inputStream = exportService.exportRefSubstrate(entityIds);
            }

            if (RefPrixSubstrate.class.equals(klass)) {
                inputStream = exportService.exportRefPrixSubstrate(entityIds);
            }

            if (RefPot.class.equals(klass)) {
                inputStream = exportService.exportRefPot(entityIds);
            }

            if (RefPrixPot.class.equals(klass)) {
                inputStream = exportService.exportRefPrixPot(entityIds);
            }

            if (RefGroupeCibleTraitement.class.equals(klass)) {
                inputStream = exportService.exportRefGroupeCibleTraitement(entityIds);
            }

            if (ReferentialI18nEntry.class.isAssignableFrom(klass)) {
                inputStream = exportService.exportReferentialI18Entry((Class<? extends ReferentialI18nEntry>)klass);
            }
    
            if (RefOtherInput.class.isAssignableFrom(klass)) {
                inputStream = exportService.exportRefOtherInput(entityIds);
            }

            if (RefSubstancesActivesCommissionEuropeenne.class.isAssignableFrom(klass)) {
                inputStream = exportService.exportRefSubstancesActivesCommissionEuropeenne(entityIds);
            }
            
            if (RefCompositionSubstancesActivesParNumeroAMM.class.isAssignableFrom(klass)) {
                inputStream = exportService.exportRefCompositionSubstancesActivesParNumeroAMM(entityIds);
            }

            if (RefPhrasesRisqueEtClassesMentionDangerParAMM.class.isAssignableFrom(klass)) {
                inputStream = exportService.exportRefPhrasesRisqueEtClassesMentionDangerParAMM(entityIds);
            }

            if (RefConversionUnitesQSA.class.isAssignableFrom(klass)) {
                inputStream = exportService.exportRefConversionUnitesQSA(entityIds);
            }

            if (RefSeedUnits.class.equals(klass)) {
                inputStream = exportService.exportRefSeedUnits(entityIds);
            }

            if (inputStream == null && LOGGER.isWarnEnabled()) {
                LOGGER.warn("Can't find valid export for class " + klass);
            }

        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't generate csv", ex);
            }
            throw new AgrosystTechnicalException("Can't create input stream", ex);
        }
        return inputStream;
    }

    public String getFilename() {
        return "export" + StringUtils.substringAfterLast(genericClassName, ".") + ".csv";
    }
}
