package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ArgumentsParserException;

import java.io.Serial;
import java.util.LinkedHashMap;

/**
 * Action qui permet de visualiser la configuration actuelle via /agrosyst/admin/show-configuration.action
 *
 * @author Cossé David : cosse@codelutin.com
 */
public class ShowConfigurationAction extends AbstractAdminAction {
    
    @Serial
    private static final long serialVersionUID = -6904551238036834982L;
    LinkedHashMap<String, String> configDisplay;
    
    @Override
    public String execute() throws ArgumentsParserException {
        if (authorizationService.isAdmin()) {
            configDisplay = getConfig().readConfigurationAsMap();
            return SUCCESS;
        }
        return SUCCESS;
    }
    
    public LinkedHashMap<String, String> getConfigDisplay() {
        return configDisplay;
    }
}
