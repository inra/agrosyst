package fr.inra.agrosyst.web.actions.admin;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.users.UserService;

import java.io.Serial;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class Home extends AbstractAdminAction {

    @Serial
    private static final long serialVersionUID = 6516792714690014172L;

    protected transient DomainService domainService;

    protected transient GrowingSystemService growingSystemService;

    protected transient GrowingPlanService growingPlanService;

    protected transient NetworkService networkService;

    protected transient UserService userService;

    protected boolean admin;

    protected long domainsCount;
    protected long activeDomainsCount;

    protected long growingSystemsCount;
    protected long activeGrowingSystemsCount;

    protected long growingPlansCount;
    protected long activeGrowingPlansCount;

    protected long networksCount;
    protected long activeNetworksCount;

    protected long usersCount;
    protected long activeUsersCount;
    protected int connectedUsersCount;
    protected int activeSessionsCount;

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute() throws Exception {
        admin = authorizationService.isAdmin();
        boolean isDp = authorizationService.isIsDataProcessor();
        if (!admin && !isDp) {
            throw new AgrosystAccessDeniedException();
        }

        domainsCount = domainService.getDomainsCount(null);
        activeDomainsCount = domainService.getDomainsCount(true);

        growingSystemsCount = growingSystemService.getGrowingSystemsCount(null);
        activeGrowingSystemsCount = growingSystemService.getGrowingSystemsCount(true);

        growingPlansCount = growingPlanService.getGrowingPlansCount(null);
        activeGrowingPlansCount = growingPlanService.getGrowingPlansCount(true);

        networksCount = networkService.getNetworksCount(null);
        activeNetworksCount = networkService.getNetworksCount(true);

        usersCount = userService.getUsersCount(null);
        activeUsersCount = userService.getUsersCount(true);

        ActiveSessionsAndUsers activeSessionsAndUsers = computeConnectedUsersCount();
        activeSessionsCount = activeSessionsAndUsers.sessionsCount();
        connectedUsersCount = activeSessionsAndUsers.uniqueUsersCount();

        return SUCCESS;
    }

    public boolean isAdmin() {
        return admin;
    }

    public Long getDomainsCount() {
        return domainsCount;
    }

    public Long getGrowingSystemsCount() {
        return growingSystemsCount;
    }

    public Long getGrowingPlansCount() {
        return growingPlansCount;
    }

    public Long getNetworksCount() {
        return networksCount;
    }

    public Long getUsersCount() {
        return usersCount;
    }

    public Long getActiveUsersCount() {
        return activeUsersCount;
    }

    public Long getActiveNetworksCount() {
        return activeNetworksCount;
    }

    public Long getActiveGrowingPlansCount() {
        return activeGrowingPlansCount;
    }

    public Long getActiveGrowingSystemsCount() {
        return activeGrowingSystemsCount;
    }

    public Long getActiveDomainsCount() {
        return activeDomainsCount;
    }

    public int getConnectedUsersCount() {
        return connectedUsersCount;
    }

    public int getActiveSessionsCount() {
        return activeSessionsCount;
    }
}
