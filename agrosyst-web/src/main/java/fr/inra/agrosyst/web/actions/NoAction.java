package fr.inra.agrosyst.web.actions;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serial;

/**
 * Le but de cette classe est seulement de permettre d'avoir une action Struts de base pour que les informations soient
 * accessibles depuis le layout.jsp
 *
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class NoAction extends AbstractAgrosystAction {

    @Serial
    private static final long serialVersionUID = -6415308969543420930L;

}
