package fr.inra.agrosyst.web.actions.referential;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.google.gson.reflect.TypeToken;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.web.actions.AbstractJsonAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serial;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

public class BioAgressorsJson extends AbstractJsonAction {

    private static final Log LOGGER = LogFactory.getLog(BioAgressorsJson.class);
    
    @Serial
    private static final long serialVersionUID = -4308281149722961263L;
    
    protected transient ReferentialService referentialService;

    /** Type de bio agresseur. */
    protected transient Set<BioAgressorType> bioAgressorTypes;

    /** (Optional) Filter on sectors. */
    protected transient List<Sector> sectors;

    public void setReferentialService(ReferentialService referentialService) {
        this.referentialService = referentialService;
    }

    public void setBioAgressorTypes(String bioAgressorTypes0) {
        Type type = new TypeToken<Set<BioAgressorType>>() {}.getType();
        this.bioAgressorTypes = getGson().fromJson(bioAgressorTypes0, type);
    }
    public void setSectors(List<Sector> sectors) {
        this.sectors = sectors;
    }

    @Override
    public String execute() {
        try {
            jsonData = referentialService.getBioAgressors(bioAgressorTypes, sectors);
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("bio-agressors-json failed for bioAgressorType '" + bioAgressorTypes + "' ", e);
            }
            return ERROR;
        }
        return SUCCESS;
    }


}
