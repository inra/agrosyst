package fr.inra.agrosyst.web.actions.commons;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.web.actions.AbstractAgrosystAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import java.io.InputStream;
import java.io.Serial;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Classe abstraite pour les actions d'export produisant un {@link ExportResult}.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public abstract class AbstractExportAction extends AbstractAgrosystAction {

    private static final Log LOGGER = LogFactory.getLog(AbstractExportAction.class);
    @Serial
    private static final long serialVersionUID = -4823105448951987123L;
    
    protected transient ExportResult exportResult;

    protected abstract ExportResult computeExportResult();

    @Override
    @Action(results = {@Result(type = "stream", params = {
            "contentType", "${contentType}",
            "inputName", "inputStream",
            "contentDisposition", "attachment; filename=\"${fileName}\"; filename*=UTF-8''${uriFileName}"}
    )})
    public String execute() throws Exception {
        exportResult = computeExportResult();
        return SUCCESS;
    }

    public String getContentType() {
        return exportResult.content().type().toString();
    }

    public InputStream getInputStream() {
        try {
            InputStream inputStream = exportResult.content().toInputStream();
            return inputStream;
        } catch (Exception ex) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Can't generate xsl", ex);
            }
            throw new AgrosystTechnicalException("Can't create input stream", ex);
        }
    }

    public String getFileName() {
        return exportResult.getFileName();
    }

    /**
     * For browser as Chrome that need URI encoded file name
     */
    public String getUriFileName() throws UnsupportedEncodingException {
        String uriFilename = URLEncoder.encode(getFileName(), StandardCharsets.UTF_8).replace("+", "%20");
        return uriFilename;
    }

}
