package fr.inra.agrosyst.web.actions.effective;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.measurement.MeasurementService;
import fr.inra.agrosyst.web.actions.commons.AbstractExportAction;

import java.io.Serial;
import java.util.List;

/**
 * @author David Cossé
 */
public class EffectiveMeasurementsExport extends AbstractExportAction {

    @Serial
    private static final long serialVersionUID = -1612187798771751449L;

    protected transient MeasurementService measurementService;

    protected List<String> selectedEffectiveCropCycleIds;

    public void setSelectedEffectiveCropCycleIds(String selectedEffectiveCropCycleIds) {
        this.selectedEffectiveCropCycleIds = getGson().fromJson(selectedEffectiveCropCycleIds, List.class);
    }

    public void setMeasurementService(MeasurementService measurementService) {
        this.measurementService = measurementService;
    }

    @Override
    protected ExportResult computeExportResult() {
        return measurementService.exportEffectiveMeasurementsAsXls(selectedEffectiveCropCycleIds);
    }

}
