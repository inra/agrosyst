/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Table.getSortableColumnHeader = function() {
  return {
    props: {
      label: String,
      sortKey: String
    },

    inject: ['tableState'],

    computed: {
      filter() {
        return this.tableState.filter;
      }
    },

    template:
      '<sort><header_label>{{label}}</header_label><em :class="{\'fa fa-sort-amount-asc\': filter.sortedColumn == sortKey && filter.sortOrder == \'ASC\',\'fa fa-sort-amount-desc\': filter.sortedColumn == sortKey && filter.sortOrder == \'DESC\',\'fa fa-sort\': filter.sortedColumn != sortKey}"></em></sort>'
  };
};

window.AgrosystVueComponents.Table.getPagination = function() {
  return {
    inject: ['tableState', 'goToPage', 'setPageSize'],

    data() {
      return {
        pageSize: this.tableState.pager.currentPage.pageSize
      }
    },

    computed: {
      pageCount() {
        return Math.floor(this.tableState.pager.count / this.tableState.pager.currentPage.pageSize) + 1;
      },

      currentPage() {
        return this.tableState.pager.currentPage.pageNumber;
      },

      pages() {
        let result = [];
        if (this.pageCount > 1) {
          result.push({ n: 0, display: 1 });
        }
        if (this.currentPage > 4) {
          result.push({ n: this.currentPage - 4, display: '...' });
        }
        for (let i = this.currentPage - 3 ; i <= this.currentPage + 3 ; i++) {
          if (i > 0 && i < this.pageCount - 1) {
            result.push({ n: i, display: i + 1 });
          }
        }
        if (this.currentPage + 5 < this.pageCount) {
          result.push({ n: this.currentPage + 4, display: '...' });
        }
        if (this.pageCount > 1) {
          result.push({ n: this.pageCount - 1, display: this.pageCount });
        }
        return result;
      }
    },

    template:
      '<div class="container"><span v-if="pageCount > 1" class="label">Pages</span><a v-if="pageCount > 1 && currentPage > 0" @click.stop="goToPage(currentPage - 1)">&lt;</a><a v-for="p in pages" @click.stop="goToPage(p.n)" :class="{\'current\': p.n == currentPage}">{{p.display}}</a><a v-if="pageCount > 1 && currentPage < pageCount - 1" @click.stop="goToPage(currentPage + 1)">&gt;</a><span class="label">Par page :</span><input type="number" v-model="pageSize" @input="setPageSize(pageSize)" class="page-size" min="10" max="100" size="3"></div>'
  };
};

window.AgrosystVueComponents.Table.getFooter = function(data) {
  return {
    inject: ['tableState', 'selectAllEntities', 'clearSelection'],

    data() {
      return {
        ...data
      }
    },

    components: {
      AgTablePagination: AgrosystVueComponents.Table.getPagination()
    },

    computed: {
      totalCount() {
        return this.tableState.pager.count;
      },

      selectedEntitiesCount() {
        let count = 0;
        for (const [key, value] of Object.entries(this.tableState.selection)) {
          if (value === true) {
            count += 1;
          }
        }
        return count;
      }
    },

    template:
      '<div class="table-footer"><div class="entity-list-info"><span class="counter">{{ $tc("messages." + entitiesLabel, totalCount) }}</span><span>- <a @click.stop="selectAllEntities()" :class="{\'active\':selectedEntitiesCount < totalCount}">{{ $t("messages." + selectAllLabel) }}</a></span><span>- <a @click.stop="clearSelection()" :class="{\'active\':selectedEntitiesCount > 0}">{{ $t("messages.common-table-unselect-all") }}</a></span><span class="selection">- {{ $tc("messages." + selectionLabel, selectedEntitiesCount) }}</span></div><div class="pagination"><ag-table-pagination></ag-table-pagination></div></div>'
  };
};

window.AgrosystVueComponents.Table.usePaginatedTableMixin = function(initialData, filterData, fetchMethod, fetchIdsMethod) {
  const tableState = Vue.reactive({
    filter: {
      sortedColumn: null,
      sortOrder: null,
      ...filterData
    },
    pager: initialData,
    selection: {},
    pageSelectionToggle: {}
  });

  const refreshTimeoutDelay = 350;
  let refreshTimeout = null;

  function refreshWithDelay() {
    if (refreshTimeout) {
      clearTimeout(refreshTimeout);
      refreshTimeout = null;
    }
    refreshTimeout = setTimeout(() => {
      refreshNow();
    }, refreshTimeoutDelay);
  }

  function refreshNow() {
    if (refreshTimeout) {
      clearTimeout(refreshTimeout);
      refreshTimeout = null;
    }
    fetchMethod();
  }

  function setSort(colName) {
    tableState.filter.sortedColumn = colName;
    if (tableState.filter.sortOrder == 'ASC') {
      tableState.filter.sortOrder = 'DESC';
    } else {
      tableState.filter.sortOrder = 'ASC';
    }
    refreshNow();
  }

  function toggleEntitySelection(topiaId) {
    if (tableState.selection[topiaId] === false) {
      if (tableState.pageSelectionToggle[tableState.pager.currentPage.pageNumber] === true) {
        tableState.pageSelectionToggle[tableState.pager.currentPage.pageNumber] = false;
      }
    }
  }

  function togglePageSelection() {
    const selectionValue = tableState.pageSelectionToggle[tableState.pager.currentPage.pageNumber] === true;
    tableState.pager.elements.forEach(element => {
      tableState.selection[element.topiaId] = selectionValue;
    });
  }

  function goToPage(pageNumber) {
    tableState.filter.page = pageNumber;
    refreshNow();
  }

  function setPageSize(pageSize) {
    tableState.filter.pageSize = pageSize;
    refreshWithDelay();
  }

  function selectAllEntities() {
    fetchIdsMethod();
  }

  function clearSelection() {
    tableState.selection = {};
  }

  Vue.provide('tableState', tableState);
  Vue.provide('goToPage', goToPage);
  Vue.provide('setPageSize', setPageSize);
  Vue.provide('selectAllEntities', selectAllEntities);
  Vue.provide('clearSelection', clearSelection);

  return {
    tableState,
    setSort,
    toggleEntitySelection,
    togglePageSelection,
    refreshWithDelay,
    refreshNow,
    inputErrorClass: 'ng-dirty ng-invalid'
  }
};
