/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Ipmworks.getIpmworksIndexController = function (context) {
  let controller = Vue.createApp({
  })
  .component('IpmworksMap', AgrosystVueComponents.Ipmworks.getIpmworksMap({
    imgFolderUrl: context.imgFolderUrl,
  }))
  .component('IpmworksMainBoard', AgrosystVueComponents.Ipmworks.getIpmworksMainBoard({
    domainCreationURL: context.domainEditURL,
    practicedSystemCreationURL: context.practicedSystemEditURL,
    locale: context.locale,
    baseUrl: context.baseUrl,
  }))
  .component('IpmworksDomainsBoardRow', AgrosystVueComponents.Ipmworks.getIpmworksDomainsBoardRow({
    domainEditURL: context.domainEditURL,
    practicedSystemEditURL: context.practicedSystemEditURL
  }))
  .component('IpmworksPerformanceTable', AgrosystVueComponents.Ipmworks.getIpmworksPerformanceTable({
    fetchEndpoint: context.fetchEndpoint,
    locale: context.locale,
    baseUrl: context.baseUrl,
    performanceDownloadURL: context.performanceDownloadURL,
  }))
  .component('AgSortableColumnHeader', AgrosystVueComponents.Table.getSortableColumnHeader())
  .component('AgTableFooter', window.AgrosystVueComponents.Table.getFooter({
    entitiesLabel: 'generic-performance-table-footer-entities',
    selectionEnabled: false
  }))
  //.use(Oruga.Input); Oruga messes up the layout of the entire page
  .use(AgrosystVueComponents.i18n)
  .use(Oruga.Config, {iconPack: "fa"})
  .use(Oruga.Collapse)
  .use(Oruga.Icon)
  .use(Oruga.Button)
  .use(Oruga.Loading)
  .use(Oruga.Pagination)
  .use(Oruga.Field)
  .use(Oruga.Input);

  // <sort> and <header_label> must not be processed by Vue
  controller.config.compilerOptions.isCustomElement = (tag) => {
    return tag == 'sort' || tag == 'header_label';
  }

  return controller;
}

window.AgrosystVueComponents.Ipmworks.getIpmworksMainBoard = function (context) {
  return {
    template: '#ipmworksMainBoard',
    data() {
      return {
        isOpen: 'domains',
        total: 0,
        currentPage: 1,
        loading: false,
        paginatedDomainSummaries: [],
        filter:"",
        selectedDomain: null,
        domainCreationURL: context.domainCreationURL,
        practicedSystemCreationURL: context.practicedSystemCreationURL,
        performanceCreationURL: context.performanceCreationURL,
        locale: context.locale,
        baseUrl: context.baseUrl,
      }
    },
    created() {
      this.onPageChange(this.currentPage)
    },
    methods : {
      onPageChange(newPage) {
        this.loading = true;
        const data = new FormData();
        data.append(
          "filter",
          JSON.stringify({
            page: newPage - 1,
            active: true,
            sortOrder: "ASC",
            domainName: this.filter,
          })
        );
        return fetch(
          ENDPOINTS.domainSummaryListJson,
          {
            method: "POST",
            body: data,
            credentials: "include",
          }
        )
          .then((response) => {
            if (response.ok) {
              return response.json();
            }
            this.loading = false;
            this.selectedDomain = null;
            this.filter = "";
            throw new Error(response.statusText);
          })
          .then((json) => {
            this.paginatedDomainSummaries = json.elements;
            this.selectedDomain = null;
            this.total = json.count;
            this.loading = false;
          });
      },
      domainIsSelected(domain) {
        if (this.selectedDomain && domain.topiaId === this.selectedDomain.topiaId) {
          this.selectedDomain = null
        }
        else {
          this.selectedDomain = domain;
        }
      },
      newDomain() {
        displayPageLoading();
        window.location = this.domainCreationURL;
      },
      newPracticedSystem() {
        displayPageLoading();
        window.location = this.practicedSystemCreationURL + "?domainId=" + (this.selectedDomain && this.selectedDomain.topiaId);
      },
      newPerformance() {
        displayPageLoading();
        window.location = this.baseUrl + "/nova/#/" + this.locale.split("_")[0] + "/performances/new?practiced=true&frontApp=ipmworks";
      }
    }
  }
};

window.AgrosystVueComponents.Ipmworks.getIpmworksDomainsBoardRow = function (context) {
  return {
    template: '#ipmworksDomainsBoardRow',
    props: ["domain", "expanded"],
    data() {
      return {
        domainEditURL: context.domainEditURL,
        practicedSystemEditURL: context.practicedSystemEditURL
      }
    },
    methods: {
      onSelected() {
        this.$emit("selected", this.domain);
      }
    }
  }
};

window.AgrosystVueComponents.Ipmworks.getIpmworksPerformanceTable = function(context) {

  return {
    template: '#ipmworksPerformanceTable',
    data() {
      let filter = {
        performanceName: null,
        domainName: null,
        growingSystemName: null,
        pageSize: 10
      };

      let pager = {
        elements: [],
        count: 0,
        currentPage: {
          pageSize: 10,
        }
      };

      let performanceTableState = AgrosystVueComponents.Table.usePaginatedTableMixin(
        pager,
        filter,
        this.fetchPerformances.bind(this)
      );

      return {
        locale: context.locale,
        baseUrl: context.baseUrl,
        performanceDownloadURL: context.performanceDownloadURL,
        domainTypes: context.domainTypes,
        ...performanceTableState
      };
    },

    mounted() {
      this.refreshNow();
    },

    methods: {

      buildQueryString() {
        // Do a deep clone of the filter object
        var filter = JSON.parse(JSON.stringify(this.tableState.filter));
        var queryString = 'practiced=true&filter=' + encodeURIComponent(JSON.stringify(filter));
        return queryString;
      },

      fetchPerformances() {
        var queryString = this.buildQueryString();

        displayPageLoading();
        fetch(context.fetchEndpoint, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: queryString
        })
        .then((response) => response.json())
        .then((data) => {
          this.tableState.pager = data;
        })
        .catch(function(response) {
          const message = this.$t('messages.performances-messages-js-domainListLoadingFailed');
          addPermanentError(message, response.status);
        })
        .finally(hidePageLoading);
      },

      displayDate(sDate) {
        let fullDate = new Date(sDate);
        let date = fullDate.getDate().toString().padStart(2, '0') + '/' + fullDate.getMonth().toString().padStart(2, '0') + '/' + fullDate.getFullYear();
        let time = fullDate.getHours().toString().padStart(2, '0') + ":" + fullDate.getMinutes().toString().padStart(2, '0');
        return this.$tc("messages.ipmworks-performance-table-dateFormat", [date, time]);
      }
    }
  }
};

window.AgrosystVueComponents.Ipmworks.getIpmworksMap = function (context) {
    return {
      template: '#ipmworksMap',
      data() {
        return {
          imgFolderUrl: context.imgFolderUrl,
          hubcoachInfo: {
            show: false,
            position: {
              vertical: "bottom: 0px",
              horizontal: "right: 0px",
            },
          },
          map: {
            width: 600,
            height: 443,
          },
          hubcoaches: [
            {
              coords: "423,28,431,32,433,46,426,60,419,74,410,57,404,44,407,34,416,28",
              id: "Hämeenlinna",
              city: "Hämeenlinna",
              partners: [
                {
                  name: "ProAgria",
                  hubcoaches: [
                    {
                      name: "Marja Kallela",
                      email: "marja.kallela@proagria.fi",
                      sectors: ["Outdoor vegetables and ornamentals"],
                    },
                  ],
                },
              ],
              country: "FI",
            },
            {
              coords: "220,95,228,99,230,113,223,127,216,141,207,124,201,111,204,101,213,95",
              id: "Dundee",
              city: "Dundee",
              partners: [
                {
                  name: "JHI",
                  hubcoaches: [
                    {
                      name: "Andrew Christie",
                      email: "Andrew.Christie@hutton.ac.uk",
                      sectors: ["Arable field crops"],
                    },
                    {
                      name: "Gillian Banks",
                      email: "Gill.Banks@hutton.ac.uk",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "UK",
            },
            {
              coords: "177,138,185,142,187,156,180,170,173,184,164,167,158,154,161,144,170,138",
              id: "Carlow",
              city: "Carlow",
              partners: [
                {
                  name: "TEAGASC",
                  hubcoaches: [
                    {
                      name: "Shay Phelan",
                      email: "Shay.Phelan@teagasc.ie",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "IR",
            },
            {
              coords: "388,148,396,152,398,166,391,180,384,194,375,177,369,164,372,154,381,148",
              id: "Minikowo",
              city: "Minikowo",
              partners: [
                {
                  name: "KPODR",
                  hubcoaches: [
                    {
                      name: "Agnieszka Szczepanska",
                      email: "agnieszka.szczepanska@kpodr.pl",
                      sectors: ["Arable field crops"],
                    },
                    {
                      name: "Josip Zubac",
                      email: "josip.zubac@kpodr.pl",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "PL",
            },
            {
              coords: "349,162,357,166,359,180,353,194,346,208,336,191,330,178,333,168,342,162",
              id: "Kleinmachnow",
              city: "Kleinmachnow",
              partners: [
                {
                  name: "JKI",
                  hubcoaches: [
                    {
                      name: "Thomas Rottstock",
                      email: "Thomas.Rottstock@julius-kuehn.de",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "DE",
            },
            {
              coords: "329,185,337,187,342,191,343,201,341,208,329,230,320,214,315,201,317,192,322,187",
              id: "Kleinmachnow2",
              city: "Kleinmachnow",
              network: {
                name: "DIPS",
                contact: {
                  name: "Thomas Rottstock",
                  email: "Thomas.Rottstock@julius-kuehn.de",
                  sector: "All agricultural sectors",
                },
              },
              country: "DE",
            },
            {
              coords: "309,151,317,155,319,169,312,183,305,197,296,180,290,167,293,157,302,151",
              id: "Ovelgönne",
              city: "Ovelgönne",
              partners: [
                {
                  name: "GLZ",
                  hubcoaches: [
                    {
                      name: "Jendrik Holthusen",
                      email: "jendrik.holthusen@gruenlandzentrum.de",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "DE",
            },
            {
              coords: "267,207,261,195,265,181,277,177,287,180,290,194,285,206,277,221",
              id: "Wageningen2",
              city: "Wageningen",
              network: {
                name: "GROEN - AoZ",
                contact: {
                  name: "Geert Kessel",
                  email: "geert.kessel@wur.nl",
                  sector: "Arable field crops",
                },
              },
              country: "NL",
            },

            {
              coords: "281,154,289,158,291,172,288,182,279,203,266,179,262,170,265,160,274,154",
              id: "Wageningen",
              city: "Wageningen",
              partners: [
                {
                  name: "DELPHY",
                  hubcoaches: [
                    {
                      name: "Joris Tielen",
                      email: "j.tielen@delphy.nl",
                      sectors: ["Outdoor vegetables and arable"],
                    },
                  ],
                },
                {
                  name: "WR",
                  hubcoaches: [
                    {
                      name: "Geert Kessel",
                      email: "geert.kessel@wur.nl",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "NL",
            },
            {
              coords: "260,182,268,186,270,200,263,214,256,228,247,211,241,198,244,188,253,182",
              id: "Rumbeke-Beitem",
              city: "Rumbeke-Beitem",
              partners: [
                {
                  name: "INAGRO",
                  hubcoaches: [
                    {
                      name: "Jonathan De Mey",
                      email: "jonathan.demey@inagro.be",
                      sectors: ["Outdoor vegetables and ornamentals"],
                    },
                    {
                      name: "Jolien Claebout",
                      email: "jolien.claerbout@inagro.be",
                      sectors: ["Greenhouse horticulture"],
                    },
                  ],
                },
              ],
              country: "BE",
            },
            {
              coords: "314,105,320,110,320,122,319,133,310,151,301,134,295,121,298,111,307,105",
              id: "Hinnerup",
              city: "Hinnerup",
              partners: [
                {
                  name: "VELAS",
                  hubcoaches: [
                    {
                      name: "Helle Elander",
                      email: "hee@velas.dk",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "DK",
            },
            {
              coords: "333,106,341,111,343,123,338,134,329,152,320,135,319,125,319,111,326,106",
              id: "Grenå",
              city: "Grenå",
              partners: [
                {
                  name: "DL",
                  hubcoaches: [
                    {
                      name: "Mikkel Østerhaab",
                      email: "mm@landboforening.dk",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "DK",
            },
            {
              coords: "195,289,203,293,205,307,198,321,191,335,182,318,176,305,179,295,188,289",
              id: "Villava",
              city: "Villava",
              partners: [
                {
                  name: "INTIA",
                  hubcoaches: [
                    {
                      name: "Noelia Telletxea Senosiain",
                      email: "ntelletxea@intiasa.es",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "ES",
            },
            {
              coords: "126,283,134,287,136,301,129,315,122,329,113,312,107,299,110,289,119,283",
              id: "Pontevedra",
              city: "Pontevedra",
              partners: [
                {
                  name: "FEUGA",
                  hubcoaches: [
                    {
                      name: "Ángela Muñiz",
                      email: "amvarela@feuga.es",
                      sectors: ["Vineyards"],
                    },
                  ],
                },
              ],
              country: "ES",
            },
            {
              coords: "178,373,186,377,188,391,181,405,174,419,165,402,159,389,162,379,171,373",
              id: "Almeria",
              city: "Almeria",
              partners: [
                {
                  name: "COEXPHAL",
                  hubcoaches: [
                    {
                      name: "Eduardo Crisol-Martínez",
                      email: "ecrisol@coexphal.es",
                      sectors: ["Greenhouse horticulture"],
                    },
                  ],
                },
              ],
              country: "ES",
            },
            {
              coords: "108,332,116,336,118,350,111,364,104,378,95,361,89,348,92,338,101,332",
              id: "Lisboa",
              city: "Lisboa",
              partners: [
                {
                  name: "CONSULAI",
                  hubcoaches: [
                    {
                      name: "Barbara Castro",
                      email: "bcastro@consulai.com",
                      sectors: ["Outdoor vegetables", "Vineyards"],
                    },
                  ],
                },
              ],
              country: "PT",
            },
            {
              coords: "327,289,335,293,337,307,330,321,323,335,314,318,308,305,311,295,320,289",
              id: "Pisa",
              city: "Pisa",
              partners: [
                {
                  name: "SSSA",
                  hubcoaches: [
                    {
                      name: "Virginia Bagnoni",
                      email: "virgi.bagnoni@gmail.com",
                      sectors: ["Orchards"],
                    },
                    {
                      name: "Giovanni Pecchoni",
                      email: "g.pecchioni@santannapisa.it",
                      sectors: ["Arable field crops"],
                    },
                  ],
                },
              ],
              country: "IT",
            },
            {
              coords: "380,245,388,249,390,263,383,277,376,291,367,274,361,261,364,251,373,245",
              id: "Maribor",
              city: "Maribor",
              partners: [
                {
                  name: "KGZS MB",
                  hubcoaches: [
                    {
                      name: "Jože Miklavc",
                      email: "joze.miklavc@kmetijski-zavod.si",
                      sectors: ["Arable field crops", "Vineyards", "Orchards"],
                    },
                  ],
                },
              ],
              country: "SI",
            },
            {
              coords: "478,354,486,358,488,372,481,386,474,400,465,383,459,370,462,360,471,354",
              id: "Athènes",
              city: "Athènes",
              partners: [
                {
                  name: "AUA",
                  hubcoaches: [
                    {
                      name: "Kounani Kalliopi",
                      email: "kounanikalliopi@gmail.com",
                      sectors: ["Vineyards"],
                    },
                  ],
                },
              ],
              country: "GR",
            },
            {
              coords: "423,260,431,264,433,278,426,292,419,306,410,289,404,276,407,266,416,260",
              id: "Novi Sad",
              city: "Novi Sad",
              partners: [
                {
                  name: "BIOSENSE",
                  hubcoaches: [
                    {
                      name: "Florian Farkas",
                      email: "florianfarkas@gmail.com",
                      sectors: ["Outdoor vegetables and ornamentals"],
                    },
                  ],
                },
              ],
              country: "RS",
            },
            {
              coords: "249,215,257,219,259,233,252,247,245,261,236,244,230,231,233,221,242,215",
              id: "Paris",
              city: "Paris",
              network: {
                name: "DEPHY",
                contact: {
                  name: "Virginie Brun",
                  email: "virginie.brun@apca.chambagri.fr",
                  sector: "All agricultural sectors",
                },
              },
              country: "FR",
            },
            {
              coords: "220,153,228,157,230,171,223,185,216,199,207,182,201,169,204,159,213,153",
              id: "Birmingham",
              city: "Birmingham",
              network: {
                name: "LEAF",
                contact: {
                  name: "Callum Bennett",
                  email: "Callum.Bennett@leafuk.org",
                  sector: "Arable Field Crops",
                },
              },
              country: "UK",
            },
            {
              coords: "286,249,294,253,296,267,289,281,282,295,273,278,267,265,270,255,279,249",
              id: "Nyon",
              city: "Nyon",
              network: {
                name: "PESTIRED",
                contact: {
                  name: "Sandie Masson",
                  email: "sandie.masson@agroscope.admin.ch",
                  sector: "Arable Field Crops",
                },
              },
              country: "CH",
            },
          ],
        };
      },
      computed: {
        hubcoachesNb() {
          return this.hubcoaches
            .filter((hubcoach) => hubcoach.partners)
            .map((hubcoach) => hubcoach.partners)
            .reduce((acc, partners) => acc.concat(partners), [])
            .reduce((acc, partner) => acc + partner.hubcoaches.length, 0);
        },
        hubcoachInfoPosition() {
          if (this.hubcoachInfo && this.hubcoachInfo.position) {
            return this.hubcoachInfo.position.vertical + '; ' + this.hubcoachInfo.position.horizontal;
          }
        }
      },
      methods: {
        showHubcoachInfo(e, hubcoach) {
          this.hubcoachInfo.position.vertical =
            "bottom: " + (this.map.height - e.layerY + 10) + "px";
          if (e.layerX > this.map.width / 2) {
            this.hubcoachInfo.position.horizontal =
              "right: " + (this.map.width - e.layerX + 5) + "px";
          } else {
            this.hubcoachInfo.position.horizontal =
              "left: " + (e.layerX - 5) + "px";
          }
          this.hubcoachInfo.hubcoach = hubcoach;
          this.hubcoachInfo.show = true;
        },
      },
    }
};
  
