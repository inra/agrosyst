# 'Composants' Vue.js

## Définition / Utilisation des composants

Ce répertoire liste les nouveaux composants basés sur Vue.js pour l'UI d'Agrosyst.
Le contrôleur de chaque composant est rattaché un simple objet javascript (un simili module) pré-déclaré dans
`src/main/webapp/js/agrosyst-app.js`. A titre d'exemple:

```
window.AgrosystVueComponents = {
    Table: {}, // pour les composants en lien avec l'affichage de tableau
    Domains: {} // pour les composants en lien avec la gestion des domaines
};
```

`agrosyst-app.js` est inclus au niveau de `src/main/webapp/WEB-INF/decorators/layout.jsp` donc disponible pour toutes
les JSP.

Si le template du composant n'est pas défini 'inline', il est proposé d'utiliser Struts pour inclure le template
défini dans un fichier HTML séparé. À titre d'exemple, pour utiliser dans une JSP le tableau listant des domaines:

```
<s:include value="/components/domains/domains-table.html"></s:include>
```

## Packaging par wro4j

Les fichiers javascript et CSS sont packagés par [wro4j](https://github.com/wro4j/wro4j).

Les bibliothèques liées à Vue.js sont déclarées dans leur propre groupe dans le fichier `src/main/resources/META-INF/nuiton-js/wro-vuejs.xml`.

Ce fichier wro est référencé au niveau de `src/main/webapp/WEB-INF/wro.xml`.

Le javascript de chaque composant doit être rattaché à son package wro. Par exemple les composants liés à la gestion
des domaines sont à rattacher au groupe `domains` défini dans `src/main/webapp/WEB-INF/wro.xml`:

```
 <group name='domains'>
   <group-ref>...</group-ref>
   <js>/js/domains.js</js>
   <js>/js/prices.js</js>
   <js>/js/entity-roles.js</js>
   <js>/components/domains/domains-list.js</js>
 </group>
```
