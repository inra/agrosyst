/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

window.AgrosystVueComponents.Domains.getDomainCroppingPlansSpeciesDistributionSection = () => {
  return {
    inject: ['context'],

    setup() {},

    props: {
      speciesAreaKey: String,
      speciesArea: Number,
    },

    emits: ['update:drawing'],

    template:'<input type="number" step="0.00001" min=0 :name="\'species-area-\'+speciesAreaKey" v-model="speciesAreaValue" />',

    computed: {

      speciesAreaValue: {
        get() {
          return this.speciesArea;
        },
        set(speciesAreaValue) {
          this.$emit('update:drawing', speciesAreaValue);
        }
      },

    },
  };
}
