/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsLutteBiologiqueModal = function(data) {
  return {
    template: '#domain-inputs-lutte-biologique-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    mounted() {
      if (!this.types.length && !this.products.length) {
        this.fetchTypesAndProducts();
      }
      if (this.tradeName) {
        this.fetchProduct(this.tradeName);
        this.search = this.tradeName;
      }
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },

        types: [],
        products: [],
        productUnits: [],
        objectId: undefined,
        loading: 0,

        priceStepValid: !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit),

        // step définition
        productType: this.selectedInput.productType,
        search: '',
        tradeName: this.selectedInput.tradeName,
        refInputId: this.selectedInput.refInputId,
        inputName: this.selectedInput.inputName,
        usageUnit: this.selectedInput.usageUnit,

        // step prix
        price: this.selectedInput.price ? this.selectedInput.price.price : undefined,
        priceUnit: this.selectedInput.price ? this.selectedInput.price.priceUnit : 'EURO_L'
      };
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },

    validations() {
      return {
        // step définition
        productType: { $autoDirty: true },
        tradeName: { required: VuelidateValidators.required, $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },
      }
    },

    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      phytoProductUnits() { return this.productUnits && this.productUnits.length > 0 ? this.productUnits : Object.keys(this.context.i18n.PhytoProductUnit); },

      // step définition
      isFirstStepValid() {
        return this.isFieldValid('productType') && this.isFieldValid('tradeName') && this.isFieldValid('inputName') &&
          this.isFieldValid('usageUnit');
      },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.priceStepValid;
        }
        return false;
      },

      filteredProducts() {
        if (this.products && this.products.length > 0) {
          return this.search && this.search.length > 0 ? this.products.filter(
            (product) => product.toString().toLowerCase().indexOf(this.search.toLowerCase()) >= 0
          ) : this.products;
        }
        return [];
      }
    },

    watch: {
      selectedInput(newInput) {
        // step définition
        this.productType = newInput.productType;
        this.tradeName = newInput.tradeName;
        this.refInputId = newInput.refInputId;
        this.inputName = newInput.inputName;
        this.usageUnit = newInput.usageUnit;

        // step prix
        this.priceStepValid = !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit);
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = undefined;
        }
        this.v$.$reset();
      }
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      resetForm() {
        this.v$.$reset();
        this.stepProgress.step = 0;
      },

      selectOption(tradeName) {
        this.tradeName = tradeName;
        this.tradeNameChanged(tradeName);
      },

      fetchTypesAndProducts() {
        this.loading++;
        displayPageLoading();

        var filter = { action: 'LUTTE_BIOLOGIQUE', countryTopiaId: this.context.countryTopiaId };
        fetch(this.context.loadBiologicalControlInputs + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.types = data.productTypes;
            // tri par ordre alphabétique de la traduction du type
            this.types = this.types.sort((t1, t2) => this.$t("ProductType." + t1).localeCompare(this.$t("ProductType." + t2)));
            this.products = data.productName;
            this.productUnits = data.phytoProductUnits;
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des types de produit et de la liste des produits';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      productTypeChanged(newProductType) {
        this.fetchProducts(newProductType);
      },

      fetchProducts(productType) {
        this.loading++;
        displayPageLoading();

        var filter = { action: 'LUTTE_BIOLOGIQUE', typeProduit: productType, countryTopiaId: this.context.countryTopiaId };
        fetch(this.context.loadBiologicalControlInputs + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.products = data.productName;
            this.productUnits = data.phytoProductUnits;
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des produits';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      tradeNameChanged(newTradeName) {
        if (newTradeName) {
          this.fetchProduct(newTradeName);
          this.inputName = newTradeName;
        }
      },

      fetchProduct(tradeName) {
        this.loading++;
        displayPageLoading();

        var filter = { action: 'LUTTE_BIOLOGIQUE', nomProduit: tradeName, countryTopiaId: this.context.countryTopiaId };
        fetch(this.context.loadBiologicalControlInputs + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.refInputId = data.refActaTraitementsProduitId;
            this.productType = data.productTypes[0];
            this.productUnits = data.phytoProductUnits;
            if (this.productUnits && this.productUnits.length === 1) {
              this.usageUnit = this.productUnits[0];
            }

            this.objectId = data.idProduit + '_' + data.idTraitement;
          })
          .catch((response) => {
            const message = 'Échec de récupération du produit';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      save() {
        const input = {
          ...this.selectedInput,
          inputType: 'LUTTE_BIOLOGIQUE',

          // step définition
          productType: this.productType,
          tradeName: this.tradeName,
          refInputId: this.refInputId,
          inputName: this.inputName,
          usageUnit: this.usageUnit
        };
        if(!isNaN(parseFloat(this.price)) || this.priceUnit !==undefined) {
          // step prix
          input.price = {
            ...(this.selectedInput.price ? this.selectedInput.price : {}),
            category: 'BIOLOGICAL_CONTROL_INPUT',
            price: this.price,
            priceUnit: this.priceUnit,
            displayName: this.tradeName,
            sourceUnit: this.usageUnit
          }
        } else {
          input.price = undefined;
        }
        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },
    }
  }
}
