/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
//
// Référencé par src/main/webapp/WEB-INF/content/domains/domains-edit-input.jsp
// Fait:
// * Section de prix du formulaire d'un intrant dans le local à intrants

window.AgrosystVueComponents.Domains.getDomainsEditInputInputsModalLightPriceSection = () => {
  return {
    inject: ['context'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    props: {
      price: Number,
      priceUnit: String,
      category: String,
      objectId: String,
      organic: Boolean,
    },

    emits: ['update:price', 'update:priceUnit', 'valid'],

    template: '#domains-edit-input-inputs-modal-light-price-section-template',

    mounted() {
      if (this.objectId) {
        this.fetchPrices();
      }
    },

    data() {
      return {
        prices: undefined,
        refPriceUnit: undefined,
        domainId: this.context.domainId,
        campaign: this.context.campaign,
        campaigns: this.context.relatedDomains,
      }
    },

    validations() {
      return {
        priceValue: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        priceUnitValue: { $autoDirty: true }
      }
    },

    computed: {
      priceValue: {
        get() {
          return this.price;
        },
        set(priceValue) {
          this.$emit('update:price', priceValue);
        }
      },
      priceUnitValue: {
        get() {
          return this.priceUnit;
        },
        set(priceUnitValue) {
          this.$emit('update:priceUnit', priceUnitValue);
        }
      },
    },

    watch: {
      objectId() {
        if (this.objectId) {
          this.fetchPrices();
        }
      },

      campaign(newCampaign) {
        this.domainId = this.campaigns[newCampaign];
        this.fetchPrices();
      },

      priceValue() {
        this.$emit('valid', this.isFieldValid('priceValue') && this.isFieldValid('priceUnitValue') && (!this.price || (this.price && this.priceUnit)));
      },

      priceUnitValue() {
        this.$emit('valid', this.isFieldValid('priceValue') && this.isFieldValid('priceUnitValue') && (!this.price || (this.price && this.priceUnit)));
      },
    },

    methods: {
      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      fetchPrices() {
        displayPageLoading();

        var filter = { campaign: this.campaign, category: this.category, priceUnit: this.priceUnit, objectId: this.objectId };
        if (this.organic !== undefined) {
          filter.isOrganique = this.organic;
        }
        fetch(this.context.loadInputRefPrices + '?domainId=' + this.domainId + '&filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.prices = data;
            const units = Object.keys(this.prices.refInputPriceDtoByUnit);
            this.refPriceUnit = units && units.length ? units[0] : undefined;
            if (this.refPriceUnit === undefined && this.prices.allowedPriceUnits && this.prices.allowedPriceUnits.length === 1) {
              this.refPriceUnit = this.prices.allowedPriceUnits[0];
            }
            if (this.prices.allowedPriceUnits && this.priceUnit && this.prices.allowedPriceUnits.indexOf(this.priceUnit) === -1 ) {
              this.$emit('update:priceUnit', undefined);
            }
            if (this.refPriceUnit !== undefined && !this.priceUnit) {
              this.$emit('update:priceUnit', this.refPriceUnit);
            }
          })
          .catch((response) => {
            const message = 'Échec de récupération des prix de référence pour le type ' + this.category;
            addPermanentError(message, response.status);
          })
          .finally(() => {
            hidePageLoading();
          });
      },
    }
  };
}
