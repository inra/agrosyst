/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsSemisModal = function (data) {
  return {
    template: '#domain-inputs-semis-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean,
      ipmworks: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    mounted() {
      if (!this.croppingPlans.length) {
        this.fetchCroppingPlans();
        this.fetchUsages(this.selectedInput);
        this.fetchSeedLotUnit();
      }
      if (this.products && Object.keys(this.products).length > 0) {
        this.initProductDetails();
      }
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-composition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },
        croppingPlans: [],
        entityUsed : [],
        refPriceUnit: undefined,
        refPricesBySpecies: {},
        refPriceUnitBySpecies: {},
        loading: 0,
        chemicalTreatmentsSwitch:[],
        organicInoculationsSwitch:[],

        // step définition
        croppingPlanId: this.initCroppingPlanId(this.selectedInput),
        croppingPlan: undefined,
        organic: this.initOrganic(this.selectedInput),
        speciesIds: this.initSpeciesId(this.selectedInput),
        seedPlantUnits: [],
        inputName: this.initName(this.selectedInput),
        usageUnit: this.initUsageUnit(this.selectedInput),
        speciesDefaultPriceUnit: this.initDefaultPriceUnit(this.selectedInput),

        // step composition
        selectedSeedTypes: this.initSelectedSeedTypes(this.selectedInput),
        chemicalTreatments: this.initChemicalTreatments(this.selectedInput),
        chemicalTreatmentsSwitch: this.initChemicalTreatmentSwitch(this.selectedInput),
        organicInoculationsSwitch: this.initOrganicInoculationSwitch(this.selectedInput),
        organicInoculations: this.initOrganicInoculations(this.selectedInput),
        products: this.initProducts(this.selectedInput),

        // step prix
        priceBySpecies: this.initPriceBySpecies(this.selectedInput),
        priceForSpecies: this.initPriceForSpecies(this.selectedInput),
        seedCoatedTreatment: this.initIncludedTreatment(this.selectedInput),
        price: this.selectedInput.price ? this.selectedInput.price.price : undefined,
        priceUnit: this.selectedInput.price ? this.selectedInput.price.priceUnit : 'EURO_KG',
        seedingPriceUnits: ['EURO_KG', 'EURO_PLANT', 'EURO_UNITE']
      };
    },

    validations() {
      const validators = {
        // step définition
        croppingPlanId: { required: VuelidateValidators.required, $autoDirty: true },
        organic: { required: VuelidateValidators.required, $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },

        // step composition is created dynamically
        selectedSeedTypes: {},
        chemicalTreatments: {},
        organicInoculations: {},
        priceForSpecies: {},

        // step prix
        seedCoatedTreatment: { $autoDirty: true },
        price: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        priceUnit: { $autoDirty: true }
      };
      this.speciesIds.forEach((speciesId) => {
        validators.selectedSeedTypes[speciesId] = { required: VuelidateValidators.required, $autoDirty: true };
        validators.chemicalTreatments[speciesId] = { required: VuelidateValidators.required, $autoDirty: true };
        validators.organicInoculations[speciesId] = { required: VuelidateValidators.required, $autoDirty: true };
        validators.priceForSpecies[speciesId] = {
          price: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true},
          priceUnit: { $autoDirty: true }
        };
        this.products[speciesId] ? this.products[speciesId].forEach((p) => {
          return p.refInputId !== undefined && p.refInputId !== '' && p.usageUnit !== undefined && p.usageUnit !== '';
        }) : true;
      });

      return validators;
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },


    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      speciesList() {
        return this.croppingPlans
          .filter((croppingPlan) => this.croppingPlanId === croppingPlan.topiaId)
          .map((croppingPlan) => croppingPlan.species.map((specie) => ({ ...specie, croppingPlan: croppingPlan })))
          .reduce((prev, curr) => prev.concat(curr), []);
      },

      seedTypes() {
        // tableau fix pour avoir un ordre déterministe
        return ['SEMENCES_CERTIFIEES', 'SEMENCES_DE_FERME', 'SEMENCES_DE_FERME_ET_CERTIFIEES'];
      },

      phytoProductUnits() { return Object.keys(this.context.i18n.PhytoProductUnit); },
      priceUnits() { return Object.keys(this.context.i18n.PriceUnit); },

      // step définition
      isFirstStepValid() {
        return this.isFieldValid('croppingPlanId') && this.speciesIds.length > 0 && this.isFieldValid('usageUnit') &&
          this.isFieldValid('organic');
      },

      // step composition
      isSecondStepValid() {
        return this.speciesIds.reduce((prev, curr) => {
          return prev && this.isFieldValid('selectedSeedTypes', curr) && prev && this.isFieldValid('chemicalTreatments', curr) && prev && this.isFieldValid('organicInoculations', curr) &&
            (!this.products[curr] || this.products[curr].reduce((prev, curr) => prev && curr.productType && curr.tradeName && curr.inputName, true));
        }, true);
      },

      // step prix
      isLastStepValid() {
        let speciesRes = true;
        this.speciesIds.forEach((speciesId) => {
          let speciesPrice = this.priceForSpecies[speciesId];

          speciesRes &&= !this.priceBySpecies || speciesPrice === undefined || (speciesPrice.price === undefined && speciesPrice.priceUnit === undefined) ||
            (!isNaN(parseFloat(speciesPrice.price)) && parseFloat(speciesPrice.price) >= 0 && speciesPrice.priceUnit !== undefined);

          this.products[speciesId] ? this.products[speciesId].forEach((product) => {
            var speciesProductRes = true;
            // valid product fields
            var productDef = product.refInputId !== undefined && product.refInputId !== ''
              && product.usageUnit !== undefined
              && product.usageUnit !== '';
            // valid product price
            var productPriceRes = (this.seedCoatedTreatment ||
              (!this.seedCoatedTreatment && (isNaN(parseFloat(product.price)) || (!isNaN(parseFloat(product.price)) && product.priceUnit !== undefined))));
            speciesProductRes = speciesProductRes && productDef && productPriceRes;
            speciesRes = speciesRes && speciesProductRes;
          }) : speciesRes;

        });

        return speciesRes && this.isFieldValid('price') && this.isFieldValid('priceUnit') &&
            (this.price === undefined || this.price !== undefined && this.priceUnit !== undefined);
      },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.isSecondStepValid;
        } else if (this.stepProgress.step === 2) {
          return this.isLastStepValid;
        }
        return false;
      }
    },

    watch: {
      selectedInput(newInput) {
        // step définition
        this.croppingPlanId = this.initCroppingPlanId(newInput);
        this.speciesDefaultPriceUnit = this.initDefaultPriceUnit(newInput);
        this.organic = this.initOrganic(newInput);
        this.speciesIds = this.initSpeciesId(newInput);
        this.inputName = this.initName(newInput);
        this.usageUnit = this.initUsageUnit(newInput);

        // step composition
        this.selectedSeedTypes = this.initSelectedSeedTypes(newInput);
        this.chemicalTreatments = this.initChemicalTreatments(newInput);
        this.chemicalTreatmentsSwitch = this.initChemicalTreatmentSwitch(newInput);
        this.organicInoculations = this.initOrganicInoculations(newInput);
        this.organicInoculationsSwitch = this.initOrganicInoculationSwitch(newInput);
        this.products = this.initProducts(newInput);
        if (this.products && Object.keys(this.products).length > 0) {
          this.initProductDetails();
        }

        // step prix
        this.priceBySpecies = this.initPriceBySpecies(newInput);
        this.priceForSpecies = this.priceBySpecies ? this.initPriceForSpecies(newInput) : {};

        this.seedCoatedTreatment = this.initIncludedTreatment(newInput);
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = 'EURO_M3';
        }
        this.v$.$reset();
      },

      croppingPlanId(newId) {
        if (newId) {
          this.croppingPlan = this.foundCroppingPlan(newId);
          this.speciesIds = this.croppingPlan ? this.croppingPlan.species.map((species) => species.topiaId) : [];
        }
      },

      speciesIds(newSpeciesIds) {
        this.cleanSpecies(this.selectedSeedTypes, newSpeciesIds);
        this.cleanSpecies(this.chemicalTreatments, newSpeciesIds);
        this.cleanSpecies(this.organicInoculations, newSpeciesIds);
        this.cleanSpecies(this.products, newSpeciesIds);
        this.cleanSpecies(this.priceForSpecies, newSpeciesIds);
        this.cleanSpecies(this.refPricesBySpecies, newSpeciesIds);
        this.cleanSpecies(this.refPriceUnitBySpecies, newSpeciesIds);

        this.seedPlantUnits = this.fetchSeedLotUnit();

        const existingSpeciesId = Object.keys(this.priceForSpecies);

        if (newSpeciesIds.length === 1) {
          this.priceBySpecies = false;// force to true
        }

        if (this.priceBySpecies) {
          var speciesPriceUnit = this.getSpeciesDefaultPriceUnit(this.usageUnit);
          this.speciesIds.forEach((speciesId) =>
            !this.priceForSpecies[speciesId] ? this.priceForSpecies[speciesId] = { priceUnit: speciesPriceUnit } : this.priceForSpecies[speciesId]);
        }
      },

      usageUnit(newUsageUnit) {
        this.speciesDefaultPriceUnit = this.getSpeciesDefaultPriceUnit(newUsageUnit);
        this.setDefaultPriceUnitIfNoneUserSelected()
      },

      'stepProgress.step'(newStep) {
        if (newStep > 1) {
          if (this.priceBySpecies) {
            const category = this.category();
            this.loadPricesBySpecies(category);
          }
          if (!this.seedCoatedTreatment) {
            this.loadPricesByProduct();
          }
        }
      },

      priceBySpecies(newPriceBySpecies) {
        if (newPriceBySpecies) {
          const category = this.category();
          this.loadPricesBySpecies(category);
        }
      },

      seedCoatedTreatment(newSeedCoatedTreatment) {
        if (this.stepProgress && this.stepProgress.step === 2) {
          this.loadPricesBySpecies(this.category());
        }
        if (!newSeedCoatedTreatment) {
          this.loadPricesByProduct();
        }
      }
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;

        if (this.stepProgress.step === 2 && !this.inputName.length) {

          const speciesNames = this.speciesIds.map((speciesId) => {
            const seedType = this.selectedSeedTypes[speciesId];
            return this.findSpecies(speciesId).speciesEspece + ' ' + this.computeDetails(this.findSpecies(speciesId)) + ' ' + this.$t('SeedType.' + seedType);
          }).join(', ');

          const biologicalSeedInoculation = this.speciesIds.some((speciesId) => this.organicInoculations[speciesId]);
          const chemicalTreatment = this.speciesIds.some((speciesId) => this.chemicalTreatments[speciesId]);
          let suffixe = '';
          if (biologicalSeedInoculation) {
            suffixe += this.$t("messages.domain-edit-inputs-semis-species-input-name-organic-treatment");
          }
          if (chemicalTreatment) {
            suffixe += (suffixe ? ', ' : '') + this.$t("messages.domain-edit-inputs-semis-species-input-name-chemical-treatment");
          }
          if (!biologicalSeedInoculation && !chemicalTreatment) {
            suffixe = this.$t("messages.domain-edit-inputs-semis-species-input-name-no-treatment");
          }
          this.inputName = [this.croppingPlan.name, speciesNames, suffixe].join(', ');
        }
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      isColFieldsDirty(fieldName, speciesId) {
        return this.v$[fieldName][speciesId].$dirty;
      },

      isColFieldsValid(fieldName, speciesId) {
        return !this.v$[fieldName][speciesId].$invalid;
      },

      resetForm() {
        this.v$.$reset();
        this.stepProgress.step = 0;
      },

      cleanSpecies(object, newSpeciesId) {
        Object.keys(object).forEach((speciesId) => {
          if (!newSpeciesId.includes(speciesId)) delete object[speciesId];
        });
      },

      selectAll() {
        this.speciesIds = this.croppingPlan ? this.croppingPlan.species.map((species) => species.topiaId) : [];
      },

      category() {
        const isCompagne = this.croppingPlan.species
          .filter((species) => this.speciesIds.includes(species.topiaId) && species.compagne)
          .length > 0;
        return isCompagne ? 'SEEDING_INPUT' : 'SEEDING_PLAN_COMPAGNE_INPUT';
      },

      loadPricesBySpecies(category) {
        this.croppingPlan.species
          .filter((species) => this.speciesIds.includes(species.topiaId))
          .filter((species) => this.selectedSeedTypes[species.topiaId] !== undefined)
          .forEach((species) => {
            const objectId = [
              species.code_espece_botanique,
              species.code_qualifiant_AEE,
              species.code_destination_AEE,
              species.varietyLibelle ? species.varietyLibelle : '',
              this.chemicalTreatments[species.topiaId],
              this.organicInoculations[species.topiaId],
              this.seedCoatedTreatment,
              this.selectedSeedTypes[species.topiaId],
              this.organic
            ].join(' - ');
            const filter = {
              code_espece_botanique: species.code_espece_botanique,
              code_qualifiant_AEE: species.code_qualifiant_AEE,
              chemicalTreatment: this.chemicalTreatments[species.topiaId],
              biologicalSeedInoculation: this.organicInoculations[species.topiaId],
              includedTreatment: this.seedCoatedTreatment,
              organic: this.organic,
              seedType: this.selectedSeedTypes[species.topiaId]
            };
            this.fetchPrices(category, objectId, filter, (data) => {
              this.refPricesBySpecies[species.topiaId] = data;
              const units = this.refPricesBySpecies[species.topiaId] ? Object.keys(this.refPricesBySpecies[species.topiaId].refInputPriceDtoByUnit) : [];
              this.refPriceUnitBySpecies[species.topiaId] = units && units.length ? units[0] : undefined;
            });
          });
      },

      loadPricesByProduct() {
        if (this.products) {
          Object.keys(this.products)
            .forEach((speciesId) => {
              this.products[speciesId].forEach((product) => {
                this.fetchPrices('SEEDING_TREATMENT_INPUT', product.objectId, {}, (data) => {
                  product.refPrice = data;
                  const units = product.refPrice ? Object.keys(product.refPrice.refInputPriceDtoByUnit) : [];
                  product.refPriceUnit = units && units.length ? units[0] : undefined;
                });
              });
            });
          }
      },

      setDefaultPriceUnitIfNoneUserSelected() {
        if (this.priceBySpecies) {
          this.speciesIds.map((speciesId) => {
            this.priceForSpecies[speciesId].priceUnit =
              this.priceForSpecies[speciesId].price ? this.priceForSpecies[speciesId].priceUnit : this.speciesDefaultPriceUnit;
          })
        } else if (!this.priceUnit) {
          this.priceUnit = this.speciesDefaultPriceUnit;
        }
      },

      getSpeciesDefaultPriceUnit(usageUnit) {
        var speciesDefaultPriceUnit;
        if (usageUnit == 'GRAINES_PAR_HA') {
          speciesDefaultPriceUnit = 'EURO_HA';
        } else if (usageUnit == 'KG_PAR_HA') {
          speciesDefaultPriceUnit = 'EURO_KG';
        } else if (usageUnit == 'PLANTS_PAR_HA') {
          speciesDefaultPriceUnit = 'EURO_PLANT';
        } else if (usageUnit == 'PLANTS_PAR_M2') {
          speciesDefaultPriceUnit = 'EURO_PLANT';
        } else if (usageUnit == 'UNITE_PAR_HA') {
          speciesDefaultPriceUnit = 'EURO_UNITE';
        } else if (usageUnit == 'UNITE_PAR_M2') {
          speciesDefaultPriceUnit = 'EURO_UNITE';
        }
        return speciesDefaultPriceUnit;
      },

      initDefaultPriceUnit(input) {
        const usageUnit = input.usageUnit ? input.usageUnit : 'KG_PAR_HA';
        return this.getSpeciesDefaultPriceUnit(usageUnit);
      },

      initCroppingPlanId(input) {
        return input.cropSeedDto ? input.cropSeedDto.topiaId : undefined
      },

      initOrganic(input) {
        return input.organic !== undefined ? input.organic : false
      },

      initSpeciesId(input) {
        return input.speciesInputs ? input.speciesInputs.map((species) => species.speciesSeedDto.topiaId) : [];
      },

      initName(input) {
        return input.inputName ? input.inputName : '';
      },

      initUsageUnit(input) {
        return input.usageUnit ? input.usageUnit : 'KG_PAR_HA';
      },

      initSelectedSeedTypes(input) {
        return input.speciesInputs ? input.speciesInputs.reduce((prev, curr) => {
          prev[curr.speciesSeedDto.topiaId] = curr.seedType;
          return prev;
        }, {}) : {};
      },

      initChemicalTreatments(input) {
        return input.speciesInputs ? input.speciesInputs.reduce((prev, curr) => {
          prev[curr.speciesSeedDto.topiaId] = curr.chemicalTreatment;
          return prev;
        }, {}) : {};
      },

      initChemicalTreatmentSwitch(input) {
        return input.speciesInputs ? input.speciesInputs.reduce((prev, curr) => {
          prev[curr.speciesSeedDto.topiaId] = curr.chemicalTreatment ? 3 : 1;
          return prev;
        }, {}) : {};
      },

      initOrganicInoculationSwitch(input) {
        return input.speciesInputs ? input.speciesInputs.reduce((prev, curr) => {
          prev[curr.speciesSeedDto.topiaId] = curr.biologicalSeedInoculation ? 3 : 1;
          return prev;
        }, {}) : {};
      },

      initOrganicInoculations(input) {
        return input.speciesInputs ? input.speciesInputs.reduce((prev, curr) => {
          prev[curr.speciesSeedDto.topiaId] = curr.biologicalSeedInoculation;
          return prev;
        }, {}) : {};
      },

      initProducts(input) {
        return input.speciesInputs ? input.speciesInputs.reduce((prev, curr) => {
          const productsBySpecies = curr.speciesPhytoInputDtos && curr.speciesPhytoInputDtos.length ?
            curr.speciesPhytoInputDtos.map((product) => ({
              topiaId: product.topiaId,
              refInputId: product.refInputId,
              productType: product.productType,
              type: product.productType === 'SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION' ? 'ORGANIC_INOCULATION' : 'CHEMICAL_TREATMENT',
              inputName: product.inputName,
              tradeName: product.tradeName,
              usageUnit: product.usageUnit,
              key: product.key,
              objectId: product.key ? product.key.split(';').slice(0, 2).join('_') : undefined,
              price: product.price ? product.price.price : undefined,
              priceUnit: product.price ? product.price.priceUnit : undefined
            })) : [];
          prev[curr.speciesSeedDto.topiaId] = productsBySpecies;
          return prev;
        }, {}) : {};
      },

      initProductDetails() {
        Object.keys(this.products).forEach((id) => {
          this.products[id].forEach((product, index) => {
            this.fetchProductTypes(id, index);
            this.fetchProducts(product.productType, id, index);
          });
        });
      },

      initPriceBySpecies(input) {
        return input.speciesInputs && input.speciesInputs.length > 0 ? (input.speciesInputs.reduce((prev, curr) => prev || (curr.seedPrice && !!curr.seedPrice.price), false)) : false;
      },

      initPriceForSpecies(input) {
        const priceForSpecies = {};
        if (input.speciesInputs) {
          input.speciesInputs.forEach((species) => {
            priceForSpecies[species.speciesSeedDto.topiaId] = {
              price: species.seedPrice ? species.seedPrice.price : undefined,
              priceUnit: species.seedPrice ? species.seedPrice.priceUnit : this.speciesDefaultPriceUnit
            }
          });
        }
        return priceForSpecies;
      },

      displaySeedCoatedTreatment() {
        return this.speciesIds && this.speciesIds.filter((speciesId) => {
          return this.organicInoculations[speciesId] || this.chemicalTreatments[speciesId];
        }).length
      },

      initIncludedTreatment(input) {
        var result = false;
        if (input && input.seedCoatedTreatment !== undefined) {
          result = input.seedCoatedTreatment;
        } else {
          result = this.speciesIds && this.speciesIds.filter((speciesId) => {
             return this.organicInoculations[speciesId] || this.chemicalTreatments[speciesId];
          }).length > 0
        }
        if (this.stepProgress && this.stepProgress.step === 2) {
          this.loadPricesBySpecies(this.category());
        }
        return result;
      },

      fetchCroppingPlans() {
        this.loading++;
        displayPageLoading();

        fetch(this.context.loadCropsContextJson + '?domainId=' + this.context.domainId, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.croppingPlans = data.croppingPlans;
            if (this.croppingPlanId) {
              this.croppingPlan = this.foundCroppingPlan(this.croppingPlanId);
            }
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste de référence des cultures';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      fetchUsages(lot) {
        if (lot && lot.topiaId) {
          this.loading++;

          displayPageLoading();

          fetch(this.context.loadSeedingSpeciesAndProductsUsages + '?filter=' + lot.topiaId, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            }
          })
            .then((response) => response.json())
            .then((data) => {
              this.entityUsed = data;
            })
            .catch((response) => {
              const message = 'Échec de récupération de la liste des usages d\'intrant';
              addPermanentError(message, response.status);
            })
            .finally(() => {
              this.loading--;
              if (!this.loading) {
                hidePageLoading();
              }
            });
        }
      },

      isSpeciesChemicalProductUsed(speciesId) {
        // all the lot is not used
        if (!this.isUsed) {
          return false;
        }
        var products = this.products[speciesId];
        if (products && products.length > 0) {
            let isPresent = products.filter(phyto => phyto.productType !== 'SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION')
            .find(phyto => true);
            return isPresent != undefined;
        }
        return false;
      },

      isSpeciesBiologicalProductUsed(speciesId) {
        // all the lot is not used
        if (!this.isUsed) {
          return false;
        }
        var products = this.products[speciesId];
        if (products && products.length > 0) {
            let isPresent = products.filter(phyto => phyto.productType == 'SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION')
            .find(phyto => true);
            return isPresent != undefined;
        }
        return false;
      },

      isSpeciesUsed(speciesId) {
        // all the lot is not used
        if (!this.isUsed) {
          return false;
        }
        // check for each species
        var currentInput = this.selectedInput && this.selectedInput.speciesInputs ? this.selectedInput.speciesInputs.find(speciesInput => speciesId === speciesInput.speciesSeedDto.topiaId) : undefined;
        return currentInput ? this.isEntityUsed(currentInput.topiaId) : false;
      },

      isEntityUsed(entityId) {
        return this.entityUsed.indexOf(entityId) != -1 ? "disabled" : false ;
      },

      getDiscriminator(value) {
        var disc = 0, i, chr;
        if (value.length === 0) return disc;
        for (i = 0; i < value.length; i++) {
          chr = value.charCodeAt(i) - 64;
          disc += chr;
        }
        return disc;
      },

      computeCropColor(crop) {
        var cropCode = crop.code;
        var disc = this.getDiscriminator(cropCode);
        var hue = Math.abs(parseInt((360 / (26 * 26)) * (disc * 2)));
        var backgroundColor = "hsl(" + hue + ", 50%, 80%)";
        return backgroundColor;
      },

      foundCroppingPlan(id) {
        const values = this.croppingPlans
          .filter((croppingPlan) => croppingPlan.topiaId === id);
        return values.length > 0 ? values[0] : undefined;
      },

      chemicalTreatmentsChanged(id) {
        this.filterProducts(id, 'CHEMICAL_TREATMENT');
      },

      organicInoculationsChanged(id) {
        this.filterProducts(id, 'ORGANIC_INOCULATION');
      },

      filterProducts(id, type) {
        if (this.products[id] && this.products[id].length > 0) {
          this.products[id] = this.products[id].filter((product) => product.type !== type);
        }
      },

      addChemicalTreatment(id) {
        this.addProduct(id, 'CHEMICAL_TREATMENT');
      },

      addOrganicInoculation(id) {
        this.addProduct(id, 'ORGANIC_INOCULATION');
      },

      addProduct(speciesId, type) {
        if (!this.products[speciesId]) {
          this.products[speciesId] = [];
        }
        this.products[speciesId].push({ type: type });
        this.fetchProductTypes(speciesId, this.products[speciesId].length - 1);
      },

      computeFetchProducts(filter, callback) {
        filter.ipmworks = this.frontApp === "ipmworks";
        fetch(this.context.loadPhytoProductsJson + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => callback(data))
          .catch((response) => {
            const message = 'Échec de récupération du produit';
            addPermanentError(message, response.status);
          });
      },

      fetchProductTypes(id, index) {
        var filter = { action: 'SEMIS', countryTopiaId: this.context.countryTopiaId };
        if (!this.products[id][index].productType) {
          filter.typeProduit = 'SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION';
          if (this.products[id][index].type === 'CHEMICAL_TREATMENT') {
            filter.notEqualsTypeProduit = true;
          }
        } else {
          filter.typeProduit = this.products[id][index].productType;
        }

        this.computeFetchProducts(filter, (data) => {
          this.products[id][index].productTypes = data.productTypes;
          if (data.productTypes && data.productTypes.length === 1) {
            this.products[id][index].productType = data.productTypes[0];
          }
          this.products[id][index].products = data.productName;
          this.products[id][index].productUnits = this._sortPhytoProductUnits(data.phytoProductUnits);
        });
      },

      productTypeChanged(productType, id, index) {
        this.fetchProducts(productType, id, index);
      },

      fetchProducts(productType, id, index) {
        var filter = { action: 'SEMIS', typeProduit: productType, countryTopiaId: this.context.countryTopiaId };

        this.computeFetchProducts(filter, (data) => {
          this.products[id][index].products = data.productName;
          this.products[id][index].productUnits = this._sortPhytoProductUnits(data.phytoProductUnits);
        });
      },

      tradeNameChanged(tradeName, id, index) {
        this.products[id][index].inputName = tradeName;
        this.fetchProduct(tradeName, id, index);
      },

      removeProduct(id, index) {
        this.products[id].splice(index, 1);
      },

      fetchProduct(tradeName, id, index) {

        var filter = { action: 'SEMIS', nomProduit: tradeName, countryTopiaId: this.context.countryTopiaId };
        if (!this.products[id][index].productType) {
          filter.typeProduit = 'SEEDINGS_OR_PLANS_BIOLOGICAL_INNOCULATION';
          if (this.products[id][index].type === 'CHEMICAL_TREATMENT') {
            filter.notEqualsTypeProduit = true;
          }
        } else {
          filter.typeProduit = this.products[id][index].productType;
        }

        this.computeFetchProducts(filter, (data) => {
          if (data.productTypes.length === 1) {
            this.products[id][index].productType = data.productTypes[0];
          }
          if (data.phytoProductUnits && data.phytoProductUnits.length === 1) {
            this.products[id][index].usageUnit = data.phytoProductUnits[0];
          }
          this.products[id][index].refInputId = data.refActaTraitementsProduitId;
          this.products[id][index].objectId = data.idProduit + '_' + data.idTraitement;
          this.products[id][index].productUnits = this._sortPhytoProductUnits(data.phytoProductUnits);
          this.products[id][index].key = data.key;
        });
      },

      _sortPhytoProductUnits(phytoProductUnits) {
        if (phytoProductUnits) {
          let result = phytoProductUnits.sort((unit1, unit2) => {
            let unit1Translated = this.$t("PhytoProductUnit." + unit1).toUpperCase();
            let unit2Translated = this.$t("PhytoProductUnit." + unit2).toUpperCase();
            return unit1Translated.localeCompare(unit2Translated);
          });
          return result;
        }
        return [];
      },

      computeDetails(species) {
        const data = [
          species.speciesQualifiant,
          species.speciesTypeSaisonnier,
          species.speciesDestination,
          species.varietyLibelle
        ].filter((value) => value && value.length > 0)
          .join(", ");
        return data ? '(' + data + ')' : '';
      },

      priceBySpeciesChanged() {
        this.speciesIds.forEach((speciesId) => this.priceForSpecies[speciesId] = !this.priceBySpecies ? {} : undefined);
      },

      findSpecies(speciesId) {
        return this.croppingPlan && this.croppingPlan.species ? this.croppingPlan.species.filter((species) => species.topiaId === speciesId)[0] : {};
      },

      fetchPrices(category, objectId, additionalFilter, callback) {
        this.loading++;
        displayPageLoading();

        var filter = { ...additionalFilter, campaign: this.context.campaign, category: category, objectId: objectId };
        fetch(this.context.loadInputRefPrices + '?domainId=' + this.context.domainId + '&filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => callback(data))
          .catch((response) => {
            const message = 'Échec de récupération des prix de référence pour le type ' + category;
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      findSpeciesInDto(speciesId) {
        return this.selectedInput && this.selectedInput.speciesInputs ? this.selectedInput.speciesInputs.filter((species) => species.speciesSeedDto.topiaId === speciesId)[0] : undefined;
      },

      findPhytoInDto(speciesId, refInputId) {
        const species = this.findSpeciesInDto(speciesId);
        return species && species.speciesPhytoInputDtos ? species.speciesPhytoInputDtos.filter((phyto) => phyto.refInputId === refInputId)[0] : undefined;
      },

      filterProductsOnSpecies(speciesId) {
        return this.products[speciesId].map((product) => {
          const phytoInDto = this.findPhytoInDto(speciesId, product.refInputId);
          const newProduct = {
            ...(phytoInDto ? phytoInDto : {}),
            inputType: 'TRAITEMENT_SEMENCE',
            refInputId: product.refInputId,
            topiaId: product.topiaId,
            key: product.key,
            productType: product.productType,
            inputName: product.inputName,
            tradeName: product.tradeName,
            usageUnit: product.usageUnit
          };
          if (!this.seedCoatedTreatment && (!isNaN(parseFloat(product.price)) || product.priceUnit !== undefined)) {
            newProduct.price = {
              ...(phytoInDto && phytoInDto.price ? phytoInDto.price : {}),
              category: 'SEEDING_TREATMENT_INPUT',
              objectId: product.objectId,
              price: product.price,
              priceUnit: product.priceUnit,
              displayName: product.tradeName,
              sourceUnit: product.usageUnit,
              biologicalSeedInoculation: this.organicInoculations[speciesId],
              chemicalTreatment: this.chemicalTreatments[speciesId],
              seedType: this.selectedSeedTypes[speciesId],
              includedTreatment: this.seedCoatedTreatment,
              organic: this.organic
            }
          } else {
            newProduct.price = undefined;
          }
          return newProduct;
        })
      },

      save() {
        const isCompagne = this.croppingPlan.species
          .filter((species) => this.speciesIds.includes(species.topiaId) && species.compagne)
          .length > 0;
        const inputType = isCompagne ? 'PLAN_COMPAGNE' : 'SEMIS';
        const category = isCompagne ? 'SEEDING_PLAN_COMPAGNE_INPUT' : 'SEEDING_INPUT';
        const speciesInputs = this.speciesIds.map((speciesId) => {
          const biologicalSeedInoculation = this.organicInoculations[speciesId];
          const chemicalTreatment = this.chemicalTreatments[speciesId];
          const products = this.products[speciesId] && this.products[speciesId].length ? this.filterProductsOnSpecies(speciesId) : [];
          const seedType = this.selectedSeedTypes[speciesId];
          var speciesNames = this.findSpecies(speciesId).speciesEspece + ' ' + this.computeDetails(this.findSpecies(speciesId)) + ' ' + this.$t('SeedType.' + seedType);
          let suffixe = '';
          if (biologicalSeedInoculation) {
            suffixe += this.$t("messages.domain-edit-inputs-semis-species-input-name-organic-treatment");
          }
          if (chemicalTreatment) {
            suffixe += (suffixe ? ', ' : '') + this.$t("messages.domain-edit-inputs-semis-species-input-name-chemical-treatment");
          }
          if (!biologicalSeedInoculation && !chemicalTreatment) {
            suffixe = this.$t("messages.domain-edit-inputs-semis-species-input-name-no-treatment");
          }
          speciesNames += (suffixe ? ', ' : '') + suffixe;

          const speciesInDto = this.findSpeciesInDto(speciesId);
          const species = {
            ...(speciesInDto ? speciesInDto : {}),
            speciesSeedDto: {
              ...(speciesInDto && speciesInDto.speciesSeedDto ? speciesInDto.speciesSeedDto : {}),
              topiaId: speciesId,
            },
            inputType: inputType,
            inputName: speciesNames,
            seedType: seedType,
            biologicalSeedInoculation: biologicalSeedInoculation,
            chemicalTreatment: chemicalTreatment,
            speciesPhytoInputDtos: products
          };
          if (this.priceBySpecies && this.priceForSpecies[speciesId] !== undefined &&
            (!isNaN(parseFloat(this.priceForSpecies[speciesId].price)) || this.priceForSpecies[speciesId].priceUnit !== undefined)) {
            const displayName = this.findSpecies(speciesId).speciesEspece + ' ' + this.computeDetails(this.findSpecies(speciesId));
            species.seedPrice = {
              ...(speciesInDto && speciesInDto.seedPrice ? speciesInDto.seedPrice : {}),
              category: category,
              includedTreatment: this.seedCoatedTreatment,
              price: this.priceForSpecies[speciesId].price,
              priceUnit: this.priceForSpecies[speciesId].priceUnit,
              displayName: displayName,
              sourceUnit: this.usageUnit
            };
          } else {
            species.seedPrice = undefined;
          }
          return species;
        });

        const input = {
          ...this.selectedInput,
          inputType: inputType,

          // step définition
          cropSeedDto: {
            ...(this.selectedInput.cropSeedDto ? this.selectedInput.cropSeedDto : {}),
            topiaId: this.croppingPlanId
          },
          organic: this.organic,
          usageUnit: this.usageUnit,

          // step composition
          speciesInputs: speciesInputs,

          // step price
          inputName: this.inputName,
          seedCoatedTreatment: this.seedCoatedTreatment
        };

        // step prix
        if (!this.priceBySpecies) {
          if (!isNaN(parseFloat(this.price)) || this.priceUnit !== undefined) {
            input.price = {
              ...(this.selectedInput.price ? this.selectedInput.price : {}),
              category: category,
              includedTreatment: this.seedCoatedTreatment,
              price: this.price,
              priceUnit: this.priceUnit,
              displayName: this.croppingPlan.name,
              sourceUnit: this.usageUnit
            };
          } else {
            input.price = undefined;
          }
        } else {
          input.price = undefined;
        }

        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },

      updateChemicalTreatmentsSwitch(species_topiaId) {
        var value = parseInt(this.chemicalTreatmentsSwitch[species_topiaId], 10); // Convert to an integer
        if (value === 1) {
          this.chemicalTreatments[species_topiaId] = false;
          delete this.products[species_topiaId];
        } else if (value === 2) {
          delete this.chemicalTreatments[species_topiaId];
          delete this.products[species_topiaId];
        } else if (value === 3) {
          this.chemicalTreatments[species_topiaId] = true;
        }
        this.seedCoatedTreatment = this.initIncludedTreatment(null);
      },

      updateOrganicInoculationsSwitch(species_topiaId) {
        var value = parseInt(this.organicInoculationsSwitch[species_topiaId], 10); // Convert to an integer
        if (value === 1) {
          this.organicInoculations[species_topiaId] = false;
          delete this.products[species_topiaId];
        } else if (value === 2) {
          delete this.organicInoculations[species_topiaId];
          delete this.products[species_topiaId];
        } else if (value === 3) {
          this.organicInoculations[species_topiaId] = true;
        }
        this.seedCoatedTreatment = this.initIncludedTreatment(null);
      },

      getSwitchCss(aSwitchList, species_topiaId) {
        var value = parseInt(aSwitchList[species_topiaId], 10); // Convert to an integer
        if (value === 1) {
          return 'tgl-off';
        } else if (value === 2) {
          return 'tgl-def';
        } else if (value === 3) {
          return 'tgl-on';
        }
      },

      fetchSeedLotUnit() {
        if (this.speciesIds && this.speciesIds.length > 0) {
          this.loading++;
          displayPageLoading();
          fetch(this.context.loadSpeciesUnits + '?filter=' + encodeURIComponent(JSON.stringify(this.speciesIds)), {
            method: 'GET',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            }
          })
            .then((response) => response.json())
            .then((data) => {
              this.seedPlantUnits = data;
              return data;
              })
            .catch((response) => {
              const message = 'Échec de récupération des unités de semis';
              addPermanentError(message, response.status);
            })
            .finally(() => {
              this.loading--;
              if (!this.loading) {
                hidePageLoading();
              }
            });
        }
      }

    }
  };
}
