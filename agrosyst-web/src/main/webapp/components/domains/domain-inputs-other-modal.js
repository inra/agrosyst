/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsOtherModal = function (data) {
  return {
    template: '#domain-inputs-other-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },

        otherInputs: [],
        input: undefined,
        prices: undefined,
        refPriceUnit: undefined,

        // step définition
        isMounted: false,
        isDeprecatedInput: this.selectedInput.refInputId ? this.selectedInput.refInputId === 'fr.inra.agrosyst.api.entities.referential.RefOtherInput_0000000-0000-0000-0000-000000000000' : false,
        inputType: this.selectedInput.inputType_c0,
        characteristic1: this.selectedInput.caracteristic1,
        characteristic2: this.selectedInput.caracteristic2,
        characteristic3: this.selectedInput.caracteristic3,
        lifetime: this.selectedInput.lifetime,
        refInputId: this.selectedInput.refInputId,
        inputName: this.selectedInput.inputName,
        inputComment: this.selectedInput.comment,
        usageUnit: this.selectedInput.usageUnit ? this.selectedInput.usageUnit : undefined,

        // liste ordonnée par ordre alphabétique (casse ignorée) selon la traduction.
        otherProductInputUnits: ['KG_M2', 'KG_HA', 'UNITE_M2', 'UNITE_HA', 'M2_HA', 'L_HA', 'L_M2']
            .sort((unit1, unit2) => {
              let unit1Translated = this.$t("OtherProductInputUnit." + unit1).toUpperCase();
              let unit2Translated = this.$t("OtherProductInputUnit." + unit2).toUpperCase();
              return unit1Translated.localeCompare(unit2Translated);
            }),

        // step prix
        price: this.selectedInput.price ? this.selectedInput.price.price : undefined,
        priceUnit: this.selectedInput.price ? this.selectedInput.price.priceUnit : undefined
      };
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },

    mounted() {
      if (!this.otherInputs.length) {
        this.fetchOtherInputs();
      }

      if (this.isDeprecatedInput) {
        this.inputType = undefined;
        this.characteristic1 = undefined;
        this.characteristic2 = undefined;
        this.characteristic3 = undefined;
      }

      this.isMounted = true;

      if (this.inputType && this.characteristic1 && this.characteristic2 && this.characteristic3) {
        const label = this.inputType + ' - ' + this.characteristic1 + ' - ' + this.characteristic2 + ' - ' + this.characteristic3;
        this.objectId = this.$filters.removeAccents(label);
      }
    },

    validations() {
      const validCharacteristic2 = (characteristic) => {
        return this.secondCharacteristics.length === 0 || (characteristic !== undefined && characteristic !== '');
      };

      const validCharacteristic3 = (characteristic) => {
        return this.thirdCharacteristics.length === 0 || (characteristic !== undefined && characteristic !== '');
      };

      const lifetime = (lifetime) => {
        return this.lifetimes.length === 0 || (lifetime !== undefined && lifetime !== '');
      }

      return {
        // step définition
        inputType: { required: VuelidateValidators.required, $autoDirty: true },
        characteristic1: { required: VuelidateValidators.required, $autoDirty: true },
        characteristic2: { required: validCharacteristic2, $autoDirty: true },
        characteristic3: { required: validCharacteristic3, $autoDirty: true },
        lifetime: { required: lifetime, $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },
        inputComment: { $autoDirty: true },

        // step prix
        price: { decimal: VuelidateValidators.decimal, $autoDirty: true },
        priceUnit: { $autoDirty: true }
      }
    },

    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      inputTypes() {
        const types = this.otherInputs.map((input) => input.inputType_c0)
        return [...new Set(types)];
      },

      firstCharacteristics() {
        if (this.inputType) {
          const characteristics = this.otherInputs
            .filter((input) => input.inputType_c0 === this.inputType)
            .filter((input) => input.caracteristic1 != '')
            .map((input) => input.caracteristic1);
          return [...new Set(characteristics)];
        }
        return [];
      },

      secondCharacteristics() {
        if (this.inputType && this.characteristic1) {
          const characteristics = this.otherInputs
            .filter((input) => input.inputType_c0 === this.inputType && input.caracteristic1 === this.characteristic1)
            .filter((input) => input.caracteristic2 !== '')
            .map((input) => input.caracteristic2);
          return [...new Set(characteristics)];
        }
        return [];
      },

      thirdCharacteristics() {
        if (this.inputType && this.characteristic1 && this.characteristic2) {
          const characteristics = this.otherInputs
            .filter((input) => input.inputType_c0 === this.inputType && input.caracteristic1 === this.characteristic1 && input.caracteristic2 === this.characteristic2)
            .filter((input) => input.caracteristic3 !== '')
            .map((input) => input.caracteristic3);
          return [...new Set(characteristics)];
        }
        return [];
      },

      lifetimes() {
        if (this.inputType && this.characteristic1 && this.characteristic2 && this.characteristic3) {
          const lifetimes = this.otherInputs
            .filter((input) => input.inputType_c0 === this.inputType && input.caracteristic1 === this.characteristic1 &&
              input.caracteristic2 === this.characteristic2 && input.caracteristic3 === this.characteristic3)
            .filter((input) => input.lifetime !== '')
            .map((input) => input.lifetime);
          return [...new Set(lifetimes)];
        }
        return [];
      },

      getOtherProductInputUnits() { if (this.usageUnit) { this.otherProductInputUnits.push(this.usageUnit) }; return [...new Set(this.otherProductInputUnits)]; },
      priceUnits() { return Object.keys(this.context.i18n.PriceUnit); },

      // step définition
      isFirstStepValid() {
        return this.isFieldValid('inputType') && this.isFieldValid('characteristic1') && this.isFieldValid('characteristic2') &&
          this.isFieldValid('characteristic3') && this.isFieldValid('lifetime') && this.isFieldValid('inputName') &&
          this.isFieldValid('usageUnit');
      },

      // step prix
      isLastStepValid() { return this.isFieldValid('price') && this.isFieldValid('priceUnit') && !this.price || (this.price && this.priceUnit); },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          if (this.isMounted && this.isDeprecatedInput && this.isFirstStepValid) {
            this.isDeprecatedInput = false;
          }
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.isLastStepValid;
        }
        return false;
      }
    },

    watch: {
      selectedInput(newInput) {
        // step définition
        this.inputType = newInput.inputType_c0;
        this.characteristic1 = newInput.caracteristic1;
        this.characteristic2 = newInput.caracteristic2;
        this.characteristic3 = newInput.caracteristic3;
        this.lifetime = newInput.lifetime;
        this.refInputId = newInput.refInputId;
        this.inputName = newInput.inputName;
        this.usageUnit = newInput.usageUnit;

        // step prix
        this.priceStepValid = !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit);
        this.domainId = this.context.domainId;
        this.campaign = this.context.campaign;
        this.campaigns = this.context.relatedDomains;
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = undefined;
        }
        this.v$.$reset();
      }
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;
      },

      resetForm() {
        this.v$.$reset();
        this.stepProgress.step = 0;
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      fetchOtherInputs() {
        displayPageLoading();

        fetch(this.context.loadRefOtherInputsJson, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.otherInputs = data;
            if (this.inputType && this.characteristic1 && this.characteristic2 && this.characteristic3 && this.lifetime) {
              this.input = this.foundInput(this.inputType, this.characteristic1, this.characteristic2, this.characteristic3, this.lifetime);
            }
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste de référence des autres intrants';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            hidePageLoading();
          });
      },

      inputTypeChanged(newInputType) {
        this.inputType = newInputType;
        const characteristic1 = this.firstCharacteristics.length === 1 ? this.firstCharacteristics[0] : undefined;
        this.characteristic1Changed(characteristic1);
      },

      characteristic1Changed(newCharacteristic1) {
        this.characteristic1 = newCharacteristic1;
        const characteristic2 = this.secondCharacteristics.length === 1 ? this.secondCharacteristics[0] : this.secondCharacteristics.length === 0 ? '' : undefined;
        this.characteristic2Changed(characteristic2);
      },

      characteristic2Changed(newCharacteristic2) {
        this.characteristic2 = newCharacteristic2;
        const characteristic3 = this.thirdCharacteristics.length === 1 ? this.thirdCharacteristics[0] : this.thirdCharacteristics.length === 0 ? '' : undefined;
        this.characteristic3Changed(characteristic3);
      },

      characteristic3Changed(newCharacteristic3) {
        this.characteristic3 = newCharacteristic3;
        const lifetime = this.lifetimes.length === 1 ? this.lifetimes[0] : this.lifetimes.length === 0 ? '' : undefined;
        this.lifetimeChanged(lifetime);
      },

      lifetimeChanged(newLifetime) {
        this.lifetime = newLifetime;
        if (this.inputType !== undefined && this.characteristic1 !== undefined && this.characteristic2 !== undefined && this.characteristic3 !== undefined && this.lifetime !== undefined) {
          this.characteristic1 = this.characteristic1 === '' ? undefined : this.characteristic1
          this.input = this.foundInput(this.inputType, this.characteristic1, this.characteristic2, this.characteristic3, newLifetime);
          this.refInputId = this.input ? this.input.topiaId : undefined;

          const allCharacteristics = [];

          if (!!this.inputType && this.inputType != this.context.i18n["messages"]["common-empty"]) {
            allCharacteristics.push(this.inputType);
          }
          if (!!this.characteristic1 && this.characteristic1 != this.context.i18n["messages"]["common-empty"]) {
            allCharacteristics.push(this.characteristic1);
          }
          if (!!this.characteristic2 && this.characteristic2 != this.context.i18n["messages"]["common-empty"]) {
            allCharacteristics.push(this.characteristic2);
          }
          if (!!this.characteristic3 && this.characteristic3 != this.context.i18n["messages"]["common-empty"]) {
            allCharacteristics.push(this.characteristic3);
          }
          const label = allCharacteristics.join(' - ');

          this.objectId = this.$filters.removeAccents(label);

          this.inputName = label;

          this.usageUnit = this.input.unit;
        }
      },

      foundInput(inputType, characteristic1, characteristic2, characteristic3, lifetime) {
        const inputs = this.otherInputs.filter((input) => input.inputType_c0 === inputType && input.caracteristic1 === characteristic1 &&
          input.caracteristic2 === characteristic2 && input.caracteristic3 === characteristic3 && input.lifetime === lifetime);
        return inputs.length ? inputs[0] : undefined;
      },

      save() {
        const input = {
          ...this.selectedInput,
          inputType: 'AUTRE',

          // step définition
          inputType_c0: this.inputType,
          caracteristic1: this.characteristic1,
          caracteristic2: this.characteristic2,
          caracteristic3: this.characteristic3,
          refInputId: this.refInputId,
          inputName: this.inputName,
          comment: this.inputComment,
          usageUnit: this.usageUnit,
          lifetime: this.lifetime
        };
        if (!isNaN(parseFloat(this.price)) || this.priceUnit !== undefined) {
          // step prix
          input.price = {
            ...(this.selectedInput.price ? this.selectedInput.price : {}),
            category: 'OTHER_INPUT',
            price: this.price,
            priceUnit: this.priceUnit,
            displayName: this.characteristic1 + ' - ' + this.characteristic2 + ' - ' + this.characteristic3,
            sourceUnit: this.usageUnit
          }
        } else {
          input.price = undefined;
        }
        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },
    }
  };
}
