/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

window.AgrosystVueComponents.Domains.getDomainToolsCopyModal = function() {
  return {
    props: {
      toolsCouplingsAndManualInterventions: Array,
      equipments: Array,
      closeModal: Function
    },

    inject: ['context'],

    components: {
      AgTableFooter: window.AgrosystVueComponents.Table.getFooter({
        entitiesLabel: AgrosystVueComponents.Domains.frontApp === 'ipmworks' ? ' common-domain-table-footer-entities' : 'agrosyst-common-domain-table-footer-entities',
        selectAllLabel: AgrosystVueComponents.Domains.frontApp === 'ipmworks' ? 'common-domain-table-footer-selectAll' : 'agrosyst-common-domain-table-footer-selectAll',
        selectionLabel: AgrosystVueComponents.Domains.frontApp === 'ipmworks' ? 'common-domain-table-footer-selection' : 'agrosyst-common-domain-table-footer-selection'
      })
    },

    emits: ['close', 'copy'],

    template: '#domain-tools-copy-modal',

    mounted() {
      this.selectAll();
      this.loadDomains();
    },

    data() {
      let domainsTableState = AgrosystVueComponents.Table.usePaginatedTableMixin(
        {
          count: 0,
          currentPage: 0
        },
        {
          domainName: null,
          campaign: null,
          type: null,
          responsable: null,
          departement: null,
          active: true
        },
        this.loadDomains.bind(this),
        this.selectAllDomains.bind(this)
      );

      return {
        firstStep: 1,
        lastStep: 2,
        activeStep: 1,

        allEquipmentTypes: ['TRACTEUR', 'AUTOMOTEUR', 'OUTIL', 'IRRIGATION'],

        allToolsCouplingsSelected: false,
        allManualInterventionsSelected: false,
        allEquipmentsSelected: false,
        selectedToolsCouplingsIds: [],
        selectedManualInterventionsIds: [],
        selectedEquipmentsIds: [], // this should be a set as well.
        equipmentsInSelectedCouplings: new Set(),

        // paste
        domains: [],
        timer: 0,
        ...domainsTableState
      }
    },

    computed: {
      nonEmptyEquipmentTypes() {
        return this.allEquipmentTypes.filter(equipmentType => this.equipments.filter(equipment => equipment.refMateriel.type === equipmentType).length > 0);
      },

      toolsCouplings() {
        return this.toolsCouplingsAndManualInterventions.filter(toolsCoupling => !toolsCoupling.manualIntervention);
      },

      manualInterventions() {
        return this.toolsCouplingsAndManualInterventions.filter(toolsCoupling => toolsCoupling.manualIntervention);
      },

      domainTypes() {
        return Object.keys(this.context.domainTypes);
      },

      filteredSelectedDomains() {
        return Object.keys(this.tableState.selection).filter(key => !!this.tableState.selection[key]);
      },

      selectedDomainsNb() {
        return Object.values(this.tableState.selection).filter(value => !!value).length;
      },

      // step copy
      isFirstStepValid() {
        return (this.selectedToolsCouplingsIds.length > 0 || this.selectedManualInterventionsIds.length > 0)
            && this.selectedEquipmentsIds.length > 0;
      },

      // step paste
      isLastStepValid() { return this.filteredSelectedDomains.length > 0; },

      isStepValid() {
        if (this.activeStep === this.firstStep) {
          return this.isFirstStepValid;
        } else if (this.activeStep === this.lastStep) {
          return this.isLastStepValid;
        }
        return false;
      },

      nbOfSelectedElements() {
        return this.selectedToolsCouplingsIds.length + this.selectedManualInterventionsIds.length + this.selectedEquipmentsIds.length;
      },

      totalNbOfElements() {
        return this.toolsCouplingsAndManualInterventions.length + this.equipments.length;
      }
    },

    watch: {
      toolsCouplings() {
        this.unselectAll();
        this.selectAll();
      },
      manualInterventions() {
        this.unselectAll();
        this.selectAll();
      },
      equipments() {
        this.unselectAll();
        this.selectAll();
      },

      'tableState.filter.domainName'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.campaign'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.mainContact'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.departement'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.responsable'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.type'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, true);
      },
      'tableState.filter.page'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, false);
      },
      'tableState.filter.pageSize'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, false);
      }
    },

    methods: {
      nextStep() {
        this.activeStep++;
      },

      nbToolsCouplings() {
        return this.selectedToolsCouplingsIds.length;
      },

      nbManualInterventions() {
        return this.selectedManualInterventionsIds.length;
      },

      nbToolsEquipments() {
        return this.selectedEquipmentsIds.length;
      },

      selectAllToolsCouplings() {
        const toolsCouplingsToAdd = this.toolsCouplings.filter((toolsCoupling) => !this.selectedToolsCouplingsIds.includes(toolsCoupling.topiaId))
          .map((toolsCoupling) => toolsCoupling.topiaId);
        this.selectedToolsCouplingsIds = this.selectedToolsCouplingsIds.concat(toolsCouplingsToAdd);
        this.allToolsCouplingsSelected = true;
        this.selectEquipmentsFromSelectedToolsCouplings();
      },

      selectEquipmentsFromSelectedToolsCouplings() {
        const equipmentsToSelectCodes =
          this.getEquipmentsOfSelectedToolsCouplings()
            .map(equipment => equipment.code)
        ;
        const equipmentsToAddIds = this.equipments
          .filter(equipment => equipmentsToSelectCodes.includes(equipment.code))
          .map(equipment => equipment.topiaId)
        ;
        this.selectedEquipmentsIds = this.selectedEquipmentsIds.concat(equipmentsToAddIds);

        equipmentsToAddIds.forEach(equipmentId => this.equipmentsInSelectedCouplings.add(equipmentId));
      },

      getEquipmentsOfSelectedToolsCouplings() {
        // Pour récupérer tous les équipements au niveau des combinaisons d'outils (interventions
        // manuelles exclues), il faut bien penser à récupérer aussi le matériel tracteur.
        let equipments = this.toolsCouplings
          .filter(toolsCoupling => this.selectedToolsCouplingsIds.includes(toolsCoupling.topiaId))
          .flatMap(toolsCoupling => [...toolsCoupling.equipments, toolsCoupling.tractor])
          .filter(equipment => undefined !== equipment);
        return equipments;
      },

      selectEquipmentsFromSelectedManualInterventions() {
        const equipmentsToSelectCodes = this.getEquipmentsOfSelectedManualInterventions()
          .map(equipment => equipment.code)
        ;
        const equipmentsToAdd = this.equipments
          .filter(equipment => equipmentsToSelectCodes.includes(equipment.code))
          .map(equipment => equipment.topiaId)
        ;
        this.selectedEquipmentsIds = this.selectedEquipmentsIds.concat(equipmentsToAdd);
      },

      getEquipmentsOfSelectedManualInterventions() {
        return this.manualInterventions
          .filter(manualIntervention => this.selectedManualInterventionsIds.includes(manualIntervention.topiaId))
          .flatMap(manualIntervention => manualIntervention.equipments)
          .filter(equipment => undefined !== equipment);
      },

      toggleAllToolsCouplings() {
        if (this.allToolsCouplingsSelected || this.isPartiallySelectedToolsCouplings()) {
          this.selectAllToolsCouplings();
        } else {
          this.selectedToolsCouplingsIds = [];
          this.allToolsCouplingsSelected = false;
          this.equipmentsInSelectedCouplings = this.getAllSelectedEquipmentsIds();
        }
      },

      selectAllManualInterventions() {
        const manualInterventionsToAdd = this.manualInterventions.filter((manualIntervention) => !this.selectedToolsCouplingsIds.includes(manualIntervention.topiaId))
          .map((manualIntervention) => manualIntervention.topiaId);
        this.selectedManualInterventionsIds = this.selectedManualInterventionsIds.concat(manualInterventionsToAdd);
        this.allManualInterventionsSelected = true;
        this.selectEquipmentsFromSelectedManualInterventions();
      },

      toggleAllManualInterventions() {
        if (this.allManualInterventionsSelected || this.isPartiallySelectedManualInterventions()) {
          this.selectAllManualInterventions();
        } else {
          this.selectedManualInterventionsIds = [];
          this.allManualInterventionsSelected = false;
          this.equipmentsInSelectedCouplings = this.getAllSelectedEquipmentsIds();
        }
      },

      selectAllEquipments() {
        const equipmentsToAdd = this.equipments.filter((equipment) => !this.selectedEquipmentsIds.includes(equipment.topiaId))
          .map((equipment) => equipment.topiaId);
        this.selectedEquipmentsIds = this.selectedEquipmentsIds.concat(equipmentsToAdd);
        this.allEquipmentsSelected = true;
      },

      toggleAllEquipments() {
        if (this.allEquipmentsSelected || this.isPartiallySelectedEquipments()) {
          this.selectAllEquipments();
        } else {
          // On s'assure qu'on ne décoche pas les équipements qui sont présents dans des
          // combinaisons d'outils ou interventions manuelles sélectionnées
          this.selectedEquipmentsIds = Array.from(this.equipmentsInSelectedCouplings);
          this.allEquipmentsSelected = false;
        }
      },

      selectedToolsCouplingsIdsChanged(toolsCouplingId) {
        // ajout d'une combinaison d'outils
        if (this.selectedToolsCouplingsIds.includes(toolsCouplingId)) {
          if (this.toolsCouplings.length === this.selectedToolsCouplingsIds.length) {
            this.allToolsCouplingsSelected = true;
          }
          this.selectEquipmentsFromSelectedToolsCouplings();
        } else {
          // retrait d'une combinaison d'outils
          this.allToolsCouplingsSelected = false;
          this.equipmentsInSelectedCouplings = this.getAllSelectedEquipmentsIds();
        }
      },

      selectedManualInterventionsIdsChanged(manualInterventionId) {
        // ajout d'une combinaison d'outils
        if (this.selectedManualInterventionsIds.includes(manualInterventionId)) {
          if (this.manualInterventions.length === this.selectedManualInterventionsIds.length) {
            this.allManualInterventionsSelected = true;
          }
          this.selectEquipmentsFromSelectedManualInterventions();
        } else {
          // retrait d'une intervention manuelle
          this.allManualInterventionsSelected = false;
          this.equipmentsInSelectedCouplings = this.getAllSelectedEquipmentsIds();
        }
      },

      getAllSelectedEquipmentsIds() {
        const selectedToolsCouplingsEquipmentsIds = this.getEquipmentsOfSelectedToolsCouplings().map(equipment => equipment.topiaId);
        const selectedManuelInterventionsEquipmentsIds = this.getEquipmentsOfSelectedManualInterventions().map(equipment => equipment.topiaId);

        // Utilisation d'un Set pour s'assurer de n'avoir qu'une seule fois un identifiant de matériel
        return new Set(selectedToolsCouplingsEquipmentsIds.concat(selectedManuelInterventionsEquipmentsIds));
      },

      selectedEquipmentsIdsChanged(equipmentId) {
        // ajout d'une combinaison d'outils
        if (this.selectedEquipmentsIds.includes(equipmentId)) {
          if (this.equipments.length === this.selectedEquipmentsIds.length) {
            this.allEquipmentsSelected = true;
          }
        } else {
          // retrait d'une combinaison d'outils
          this.allEquipmentsSelected = false;
        }
      },

      selectAll() {
        this.selectedToolsCouplingsIds = this.toolsCouplings.map(toolsCoupling => toolsCoupling.topiaId);
        this.selectedManualInterventionsIds = this.manualInterventions.map(manualIntervention => manualIntervention.topiaId);
        this.selectedEquipmentsIds = this.equipments.map(equipment => equipment.topiaId);
        this.equipmentsInSelectedCouplings = this.getAllSelectedEquipmentsIds();

        this.allToolsCouplingsSelected = true;
        this.allManualInterventionsSelected = true;
        this.allEquipmentsSelected = true;
      },

      unselectAll() {
        this.selectedToolsCouplingsIds = [];
        this.selectedManualInterventionsIds = [];
        this.selectedEquipmentsIds = [];
        this.equipmentsInSelectedCouplings = new Set();

        this.allToolsCouplingsSelected = false;
        this.allManualInterventionsSelected = false;
        this.allEquipmentsSelected = false;
      },

      isPartiallySelectedToolsCouplings() {
        const nbToolsCouplingsSelected = this.selectedToolsCouplingsIds.length;
        return nbToolsCouplingsSelected > 0 && nbToolsCouplingsSelected < this.toolsCouplings.length;
      },

      isPartiallySelectedManualInterventions() {
        const nbManualInterventionsSelected = this.selectedManualInterventionsIds.length;
        return nbManualInterventionsSelected > 0 && nbManualInterventionsSelected < this.manualInterventions.length;
      },

      isPartiallySelectedEquipments() {
        const nbEquipmentsSelected = this.selectedEquipmentsIds.length;
        return nbEquipmentsSelected > 0 && nbEquipmentsSelected < this.equipments.length;
      },

      isEquipmentInSelectedCouplings(equipmentId) {
        return this.equipmentsInSelectedCouplings.has(equipmentId);
      },

      loadDomains() {
        displayPageLoading();
        fetch(ENDPOINTS.domainsListJson, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: "filter=" + encodeURIComponent(JSON.stringify(this.tableState.filter)) + "&fromNavigationContextChoose=false"
        })
          .then(response => response.json())
          .then(data => {
            this.domains = data.elements;
            this.tableState.pager = data;
          })
          .catch(function (response) {
            this.domains = null;
            this.tableState.pager = null;
            console.error("Can't get domains list", response);
            addPermanentError(this.$t("messages.domain-list-loading-error"));
          })
          .finally(hidePageLoading);
      },

      selectAllDomains() {
        this.domains.map((domain) => domain.topiaId)
          .forEach((id) => this.tableState.selection[id] = true);
      },

      displayResponsibles(responsibles) {
        if (responsibles) {
          return responsibles.map(responsible => responsible.firstName + " " + responsible.lastName).join(", ");
        }
      },

      filterUpdated(newValue, oldValue, withTimer, resetPage) {
        if (newValue !== oldValue) {
          if (this.timer) {
            clearTimeout(this.timer);
          }
          let applyFilter = () => {
            if (resetPage) {
              this.tableState.filter.page = 0;
            }
            this.loadDomains();
          };
          if (withTimer) {
            this.timer = setTimeout(applyFilter, 350);
          } else {
            applyFilter();
          }
        }
      },

      useDebouncedSubmit() {
        displayPageLoading();
        setTimeout(() => {
          hidePageLoading();
        }, 1000);
      },

      paste() {
        let fromDomain = this.context.domain.topiaId;
        let toOtherDomains = [];
        let toSameDomain = false;
        this.filteredSelectedDomains.forEach(domain => {
          if (domain === fromDomain) {
            toSameDomain = true;
          } else {
            toOtherDomains.push(domain);
          }
        });

        this.processInputsCopy(
          toSameDomain,
          toOtherDomains,
          this.selectedToolsCouplingsIds,
          this.selectedManualInterventionsIds,
          this.selectedEquipmentsIds
        );
      },

      processInputsCopy(
          toSameDomain,
          toOtherDomains,
          selectedToolsCouplingsIds,
          selectedManualInterventionsIds,
          selectedEquipmentsIds
      ) {
        if (toOtherDomains.length > 0 && this.isCopyAllowed(selectedToolsCouplingsIds, selectedManualInterventionsIds, selectedEquipmentsIds)) {
          displayPageLoading();
          const toolsCouplingsToCopy = this.toolsCouplingsAndManualInterventions.filter(toolsCoupling => {
            return selectedToolsCouplingsIds.includes(toolsCoupling.topiaId)
              || selectedManualInterventionsIds.includes(toolsCoupling.topiaId);
          });
          const equipmentsToCopy = this.equipments.filter(equipment => selectedEquipmentsIds.includes(equipment.topiaId));

          const ajaxRequest = "fromDomain=" + encodeURIComponent(this.context.domain.topiaId) +
                            "&toolsCouplingsJson=" + encodeURIComponent(JSON.stringify(toolsCouplingsToCopy)) +
                            "&equipmentsJson=" + encodeURIComponent(JSON.stringify(equipmentsToCopy)) +
                            "&toDomains=" + JSON.stringify(toOtherDomains) +
                            "&includeToolsCouplings=true";
          fetch(ENDPOINTS.domainMaterielsCopy, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: ajaxRequest
          })
          .then(response => response.json())
          .then(data => {
            if (data.length > 0) {
              let domainPluralize = this.$tc("messages.domain-edit-tools-copy-paste-success-domain", data.length);
              let targetedDomain = data.join(", ") + ".";
              let message = this.$t("messages.domain-edit-tools-copy-paste-success", [domainPluralize, targetedDomain]);
              // make sure copy on same domain is done after copy to other domains
              if (toSameDomain) {
                this.toSameDomainCopy(selectedToolsCouplingsIds, selectedManualInterventionsIds, selectedEquipmentsIds);
              }
              addSuccessMessage(message);
              this.close();
            }
          })
          .catch(function(response) {
            let messageKey = "messages.domain-edit-tools-copy-paste-error";
            if (response.data === '"IllegalArgumentException"') {
              messageKey += "-invalid";
            }
            let message = this.$t(messageKey)
            console.error(message, response);
            addPermanentError(message, response.status);
          })
          .finally(() => {
            hidePageLoading();
          });
        } else if (toSameDomain) {
          // make sure copy on same domain is done after copy to other domains
          this.toSameDomainCopy(selectedToolsCouplingsIds, selectedManualInterventionsIds, selectedEquipmentsIds);
        }
      },

      toSameDomainCopy(selectedToolsCouplingsIds, selectedManualInterventionsIds, selectedEquipmentsIds) {
        const toolsCouplingsToCopy = this.toolsCouplingsAndManualInterventions.filter(toolsCoupling => {
          return selectedToolsCouplingsIds.includes(toolsCoupling.topiaId)
            || selectedManualInterventionsIds.includes(toolsCoupling.topiaId);
        });
        const equipmentsToCopy = this.equipments.filter(equipment => selectedEquipmentsIds.includes(equipment.topiaId));

        if (this.isCopyAllowed(selectedToolsCouplingsIds, selectedManualInterventionsIds, selectedEquipmentsIds)) {
          this.useDebouncedSubmit();
          let equipementOrigineToCopy = {};
          equipmentsToCopy.forEach(equipment => {
            let copy = cloneObject(equipment);
            delete copy.topiaId;
            delete copy.code;

            copy.used = false;
            copy.topiaId = "NEW-EQUIPMENT-" + new Date().getTime();
            equipementOrigineToCopy[equipment.topiaId] = copy;
          });

          Object.values(equipementOrigineToCopy).forEach(equipment => {
            equipmentsToCopy.push(equipment);
          });

          if (toolsCouplingsToCopy.length > 0) {
            let tcOriginalToCopy = [];
            toolsCouplingsToCopy.forEach(toolsCoupling => {
              let tcCopy = cloneObject(toolsCoupling);
              delete tcCopy.domain;
              delete tcCopy.topiaId;
              delete tcCopy.code;

              tcCopy.used = false;
              if (toolsCoupling.tractor) {
                tcCopy.tractor = equipementOrigineToCopy[toolsCoupling.tractor.topiaId];
              }
              tcCopy.equipments = [];
              toolsCoupling.equipments.forEach(originalEquipement => {
                tcCopy.equipments.push(equipementOrigineToCopy[originalEquipement.topiaId]);
              });

              tcOriginalToCopy.push(tcCopy);
            });

            tcOriginalToCopy.forEach(toolsCoupling => {
              toolsCouplingsToCopy.push(toolsCoupling);
            });

          }

          let tcPluralize = toolsCouplingsToCopy.length > 1 ? "les combinaisons d'outils ont" : "la combinaison d'outils a";
          let message = (toolsCouplingsToCopy.length > 0) ? "Le matériel / " + tcPluralize + " bien été copié vers ce même domaine, " : "Le matériel a bien été copié vers ce même domaine :";
          addSuccessMessage(message + this.context.domain.name);
        }
      },

      isCopyAllowed(selectedToolsCouplingsIds, selectedManualInterventionsIds, selectedEquipmentsIds) {
        return (selectedToolsCouplingsIds.length > 0 || selectedManualInterventionsIds.length > 0)
          && selectedEquipmentsIds.length > 0;
      },

      close() {
        this.activeStep = 1;
        this.unselectAll();
        this.selectAll();
        this.tableState.selection = {};
        this.closeModal();
      }
    }
  };
}
