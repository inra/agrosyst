/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsWorkforceOperationModal = function (data) {
  return {
    template: '#domain-inputs-workforce-operation-modal',

    props: {
      isOpen: Boolean,
      irrigationInput: Object,
      fuelInput: Object,
      manualWorkforceInput: Object,
      mechanizedWorkforceInput: Object
    },

    inject: ['context'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    emits: ['save', 'close'],

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
    },

    mounted() {
      this.objectId = this.context.campaign;
    },

    data() {
      return {
        objectId: undefined,

        irrigationPrice: this.irrigationInput && this.irrigationInput.price ? this.irrigationInput.price.price : undefined,
        irrigationPriceUnit: this.irrigationInput && this.irrigationInput.price ? this.irrigationInput.price.priceUnit : 'EURO_L',

        fuelPrice: this.fuelInput && this.fuelInput.price ? this.fuelInput.price.price : undefined,
        fuelPriceUnit: this.fuelInput && this.fuelInput.price ? this.fuelInput.price.priceUnit : 'EURO_L',

        manualWorkforcePrice: this.manualWorkforceInput && this.manualWorkforceInput.price ? this.manualWorkforceInput.price.price : undefined,
        manualWorkforcePriceUnit: this.manualWorkforceInput && this.manualWorkforceInput.price ? this.manualWorkforceInput.price.priceUnit : 'EURO_H',

        mechanizedWorkforcePrice: this.mechanizedWorkforceInput && this.mechanizedWorkforceInput.price ? this.mechanizedWorkforceInput.price.price : undefined,
        mechanizedWorkforcePriceUnit: this.mechanizedWorkforceInput && this.mechanizedWorkforceInput.price ? this.mechanizedWorkforceInput.price.priceUnit : 'EURO_H',
      };
    },

    watch: {
      irrigationInput(newInput) {
        this.objectId = this.context.campaign;
        console.log(newInput);
        if (newInput.price) {
          this.irrigationPrice = newInput.price.price;
          this.irrigationPriceUnit = newInput.price.priceUnit;
        } else {
          this.irrigationPrice = undefined;
          this.irrigationPriceUnit = undefined;
        }
        this.v$.$reset();
      },

      fuelInput(newInput) {
        this.objectId = this.context.campaign;
        console.log(newInput);
        if (newInput.price) {
          this.fuelPrice = newInput.price.price;
          this.fuelPriceUnit = newInput.price.priceUnit;
        } else {
          this.fuelPrice = undefined;
          this.fuelPriceUnit = undefined;
        }
        this.v$.$reset();
      },

      manualWorkforceInput(newInput) {
        this.objectId = this.context.campaign;
        console.log(newInput);
        if (newInput.price) {
          this.manualWorkforcePrice = newInput.price.price;
          this.manualWorkforcePriceUnit = newInput.price.priceUnit;
        } else {
          this.manualWorkforcePrice = undefined;
          this.manualWorkforcePriceUnit = undefined;
        }
        this.v$.$reset();
      },

      mechanizedWorkforceInput(newInput) {
        this.objectId = this.context.campaign;
        console.log(newInput);
        if (newInput.price) {
          this.mechanizedWorkforcePrice = newInput.price.price;
          this.mechanizedWorkforcePriceUnit = newInput.price.priceUnit;
        } else {
          this.mechanizedWorkforcePrice = undefined;
          this.mechanizedWorkforcePriceUnit = undefined;
        }
        this.v$.$reset();
      },
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      resetForm() {
        this.v$.$reset();
      },

      save() {
        const inputs = [];
        const iInput = this.createOrEditIrrigationInput();
        if (iInput) {
          inputs.push(iInput);
        }
        const fInput = this.createOrEditFuelInput();
        if (fInput) {
          inputs.push(fInput);
        }
        const maInput = this.createOrEditManualWorkforceInput();
        if (maInput) {
          inputs.push(maInput);
        }
        const meInput = this.createOrEditMechanizedWorkforceInput();
        if (meInput) {
          inputs.push(meInput);
        }
        this.$emit('save', inputs);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },

      createOrEditIrrigationInput() {
        let input = undefined;
        if (!isNaN(parseFloat(this.irrigationPrice)) || this.irrigationPrice === "") {
          input = {
            ...this.irrigationInput,
            inputType: 'IRRIGATION',
            inputName: 'Eau',
            usageUnit: 'MM',
            price: {
              ...(this.irrigationInput.price ? this.irrigationInput.price : {}),
              category: 'IRRIGATION_INPUT',
              price: this.irrigationPrice,
              priceUnit: this.irrigationPriceUnit,
              displayName: 'Eau d’irrigation',
              sourceUnit: 'MM'
            }
          };
        }
        return input;
      },

      createOrEditFuelInput() {
        let input = undefined;
        if (!isNaN(parseFloat(this.fuelPrice)) || this.fuelPrice === "") {
          input = {
            ...this.fuelInput,
            inputType: 'CARBURANT',
            inputName: 'Carburant',
            usageUnit: 'L_HA',
            price: {
              ...(this.fuelInput.price ? this.fuelInput.price : {}),
              category: 'FUEL_INPUT',
              price: this.fuelPrice,
              priceUnit: this.fuelPriceUnit,
              displayName: 'Carburant',
              sourceUnit: 'L_HA'
            }
          };
        }
        return input;
      },

      createOrEditManualWorkforceInput() {
        let input = undefined;
        if (!isNaN(parseFloat(this.manualWorkforcePrice)) || this.manualWorkforcePrice === "") {
          input = {
            ...this.manualWorkforceInput,
            inputType: 'MAIN_OEUVRE_MANUELLE',
            inputName: 'Main d’oeuvre manuelle',
            price: {
              ...(this.manualWorkforceInput.price ? this.manualWorkforceInput.price : {}),
              category: 'MAIN_OEUVRE_MANUELLE_INPUT',
              price: this.manualWorkforcePrice,
              priceUnit: this.manualWorkforcePriceUnit,
              displayName: 'Main d’oeuvre manuelle',
            }
          };
        }
        return input;
      },

      createOrEditMechanizedWorkforceInput() {
        let input = undefined;
        if (!isNaN(parseFloat(this.mechanizedWorkforcePrice)) || this.mechanizedWorkforcePrice === "") {
          input = {
            ...this.mechanizedWorkforceInput,
            inputType: 'MAIN_OEUVRE_TRACTORISTE',
            inputName: 'Main d’oeuvre tractoriste',
            price: {
              ...(this.mechanizedWorkforceInput.price ? this.mechanizedWorkforceInput.price : {}),
              category: 'MAIN_OEUVRE_TRACTORISTE_INPUT',
              price: this.mechanizedWorkforcePrice,
              priceUnit: this.mechanizedWorkforcePriceUnit,
              displayName: 'Main d’oeuvre tractoriste',
            }
          };
        }
        return input;
      }
    },
  };
}
