/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainContextController = function (context) {
  const controller = Vue.createApp({
    data() {
      return {
        domain: context.domain,
        forFrance: context.forFrance,
        frontApp: context.frontApp,
        errors: context.errors,
        types: context.types,
        alreadyExistingName: false,
        countries: context.countries,
        countryTopiaId: context.countryTopiaId,
        franceTopiaId: context.franceTopiaId,
        countryChangeCallback: context.countryChangeCallback,
        showDomainGpsData: false,
        showZoneDetails: false,
        zoningValues: context.zoningValues,
        formatedDepartement: context.formatedDepartement,
        formatedPetiteRegionAgricoleName: context.formatedPetiteRegionAgricoleName,
        showWeatherStations: false
      };
    },
    computed: {
      locationTopiaId() {
        return this.domain.location ? this.domain.location.topiaId : null;
      },
      commune() {
        let commune = "";
        if (this.domain.location) {
          if (!this.franceTopiaId || this.countryTopiaId === this.franceTopiaId) {
            commune += this.domain.location.codePostal + ", ";
          }
          commune += this.domain.location.commune;
        }
        return commune;
      }
    },
    watch: {
      countryTopiaId(newValue) {
        if (this.countryChangeCallback) {
          this.countryChangeCallback(newValue);
        }
        this.domain.location = null;
      }
    },
    methods: {
      checkDomainName() {
        if (!this.domain.topiaId) {
          displayPageLoading();
          fetch(ENDPOINT_DOMAINS_CHECK_DOMAIN_NAME_JSON + "?domainName=" + encodeURIComponent(this.domain.name))
            .then(response => response.json())
            .then(response => {
              this.alreadyExistingName = response;
            })
            .catch(response => {
               const message = "Échec de vérification de la validité du nom de domaine";
               console.error(message, response);
               addPermanentError(message, response.status);
            })
            .finally(hidePageLoading);
        }
      }
    },
    mounted() {
      const self = this;
      //compute commune name auto completion
      $("#commune").autocomplete({
        source : function(request, response) {
          request.countryTopiaId = self.countryTopiaId;
          // On remplace par du POST pour les pbs d'encoding
          $.post(ENDPOINTS.listRefLocationJson, request,
              function(data) {
                if (data) {
                  var autoCompleteField = $("#commune");
                  if (data.length === 0) {
                    autoCompleteField.addClass("empty-autocomplete-result");
                  } else {
                    autoCompleteField.removeClass("empty-autocomplete-result");
                  }
                }
                // On récupère un objet RefLocation brut, qu'on transforme
                var transformedData = $.map(data, function(loc) {
                  if (!self.franceTopiaId || request.countryTopiaId === self.franceTopiaId) {
                    var codePostalAndName = loc.codePostal + ", " + loc.commune;
                    return {
                      label: codePostalAndName,
                      value: codePostalAndName,
                      refLocation: loc
                    };
                  } else {
                    return {
                      label: loc.commune,
                      value: loc.commune,
                      refLocation: loc
                    };
                  }
                });
                response(transformedData);
              }, "json");
        },
        minLength : 1,
        select : function(event, ui) {
          if (ui.item) {
            var location = ui.item.refLocation;
            self.locationTopiaId = location.topiaId;
            $.ajax({
              url : ENDPOINTS.getRefLocationJson,
              data : {
                locationTopiaId : location.topiaId
              },
              async : false,
              dataType : "json",
              success : function(data) {
                self.formatedDepartement = data.departementName + ' (' + data.departement +')';
                self.formatedPetiteRegionAgricoleName = data.petiteRegionAgricoleName + ' (' + data.petiteRegionAgricole +')';
                self.domain.location = location;
              }, error : function(data, status) {
                 console.error("Échec de récupération départements et régions agricoles", status);
                 var message = "Échec de récupération départements et régions agricoles";
                 addPermanentError(message, status);
              }
            });
          }
        }
      });
    }
  })
  .component('DomainGpsDataTable', AgrosystVueComponents.Domains.getGpsDataTable({
    gpsDatas: context.domainGpsData,
    locationId: context.locationTopiaId
  }))
  .component('DomainWeatherStationsTable', AgrosystVueComponents.Domains.getWeatherStationsTable({
    weatherStationSitesIdsAndNames: context.weatherStationSitesIdsAndNames,
    weatherStations: context.weatherStations
  }))
  .use(AgrosystVueComponents.i18n);

  return controller;
}
