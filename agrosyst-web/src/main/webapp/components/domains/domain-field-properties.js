/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainFieldPropertiesListController = function (context) {
  const controller = Vue.createApp({
    data() {
      return {
        countryTopiaId: context.countryTopiaId
      }
    }
  })
  .component('DomainFieldProperties', AgrosystVueComponents.Domains.getDomainFieldProperties(context))
  .component('GenericModal', AgrosystVueComponents.Common.getGenericModal())
  .use(AgrosystVueComponents.i18n)
  .use(Oruga.Config, {iconPack: "fa"})
  .use(Oruga.Icon)
  .use(Oruga.Autocomplete)
  .use(Oruga.Button)
  .use(Oruga.Field)
  .use(Oruga.Input)
  .use(Oruga.Select)
  .use(Oruga.Steps)
  .use(Oruga.Switch)
  .use(Oruga.Modal);

  return controller;
}

window.AgrosystVueComponents.Domains.getDomainFieldProperties = function (context) {
  return {
    template: '#domainFieldProperties',
    data() {
      return {
        domain: context.domain,
        forFrance: context.forFrance,
        frontApp: context.frontApp,
        maxSlopes: context.maxSlopes,
        waterFlowDistances: context.waterFlowDistances,
        irrigationSystemTypes: context.irrigationSystemTypes,
        pompEngineTypes: context.pompEngineTypes,
        hosesPositionnings: context.hosesPositionnings,
        frostProtectionTypes: context.frostProtectionTypes,
        solTextures: context.solTextures,
        solProfondeurs: context.solProfondeurs,
        solStoninesses: context.solStoninesses,
        solWaterPhs: context.solWaterPhs,
        solCaracteristiques: context.solCaracteristiques,
        adjacentElements: context.adjacentElements,
        franceTopiaId: context.franceTopiaId,
        activeStep: 1,
        editedChamp: null,
        adjacentElementIds: {},
      };
    },

    computed: {
      modalTitle() {
        return 'messages.domain-edit-field-properties-' + (this.editedChamp && this.editedChamp._newChamp ? 'ajouter' : 'modifier') + 'champ';
      },
      formInvalid() {
        return !this.editedChamp
         || !this.editedChamp.name
         || !this.editedChamp.location
         || !this.editedChamp.area
         || !this.editedChamp.surfaceTexture
         || !this.editedChamp.solDepth
         || !this.editedChamp.solStoniness && this.editedChamp.solStoniness !== 0;
      },
      inFrance() {
        return this.$parent.countryTopiaId === this.franceTopiaId;
      }
    },

    watch: {
    },

    methods: {
      getCommuneFromLocation(location) {
        let commune = location.commune;
        if (this.inFrance) {
          commune = location.codePostal + ", " + commune;
        }
        return commune;
      },

      addChamp() {
        this.activeStep = 1;
        let location = this.domain.location;
        this.editedChamp = {
          _newChamp: true,
          location,
          commune: this.getCommuneFromLocation(location)
        }
      },

      editChamp(element) {
        this.activeStep = 1;
        const index = this.domain.practicedPlot.indexOf(element);
        if (index >= 0) {
          this.editedChamp = cloneObject(element);
          this.editedChamp._index = index;
          this.editedChamp.commune = this.getCommuneFromLocation(this.editedChamp.location);
        }
      },

      saveChamp() {
        if (!this.formInvalid) {
          if (this.editedChamp._newChamp) {
            this.domain.practicedPlot.push({ ...this.editedChamp, _newChamp: false });
          } else {
            const index = this.editedChamp._index;
            //this.editedChamp._index = null;
            Object.assign(this.domain.practicedPlot[index], this.editedChamp);
          }
          this.editedChamp = null;
        }
      },
  
      deleteChamp(element) {
        if (window.confirm(this.$t("messages.domain-edit-field-properties-delete-confirm"))) {
          let indexOf = this.domain.practicedPlot.indexOf(element);
          this.domain.practicedPlot.splice(indexOf, 1);
          // TODO this.domainEditForm.$setDirty();
        }
      },

      setCountryTopiaId(countryTopiaId) {
        this.countryTopiaId = countryTopiaId;
      },

      refreshLocationsList(locationPartName) {
        if (this.editedChamp) {
          this.editedChamp.isFetchingLocations = true;
          let countryTopiaId = this.$parent.countryTopiaId;
          let self = this;
          return fetch(ENDPOINTS.listRefLocationJson, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
              },
              body: "term=" + encodeURIComponent(locationPartName) + "&countryTopiaId=" + countryTopiaId
            })
            .then(response => response.json())
            .then(data => {
              var transformedData = data.map(loc => {
                if (countryTopiaId === self.franceTopiaId) {
                  var codePostalAndName = loc.codePostal + ", " + loc.commune;
                  return {
                    label: codePostalAndName,
                    value: codePostalAndName,
                    refLocation: loc
                  };
                } else {
                  return {
                    label: loc.commune,
                    value: loc.commune,
                    refLocation: loc
                  };
                }
                return result;
              });
              this.editedChamp.filteredLocations = transformedData;
            })
            .catch(function(response) {
                var message = "Échec de récupération des communes";
                console.error(message, response);
                addPermanentError(message, response.status);
            })
            .finally(() => {
              this.editedChamp.isFetchingLocations = false;
            });
        }
      },

      selectLocation(option) {
        if (!!option) {
          this.editedChamp.location = option.refLocation;
          this.editedChamp.commune = option.value;
        } else {
          this.editedChamp.location = null;
          this.editedChamp.commune = null;
        }
      },

      displayLocation() {
        if (this.editedChamp) {
          displayLocation(this.editedChamp.name, this.editedChamp.latitude, this.editedChamp.longitude, '');
        }
      }
    }
  }
}

