/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

// Référencé par src/main/webapp/WEB-INF/content/domains/domains-edit-input.jsp
// Fait:
// * Gestion du local à intrants
window.AgrosystVueComponents.Domains.getDomainsEditInputInputsController = (context) => {

  let controller = Vue.createApp({
    mounted() {
      $('#tabs-intrants-li').on('click', this.init);
      if ($('#tabs-intrants-li')[0].className === 'selected') {
        this.init();
      }
    },

    beforeDestroy() {
      $('#tabs-intrants-li').off('click', this.init);
    },

    data() {
      return {
        context: context,
        domainInputs: undefined,
        domainInputsUsageMap: undefined,
        selectedInputType: undefined,
        editedInput: undefined,
        isModalActive: false,
        isCopyModalActive: false,
        isWorkforceOperationModalActive: false,
        editedIrrigationInput: undefined,
        editedFuelInput: undefined,
        editedManualWorkforceInput: undefined,
        editedMechanizedWorkforceInput: undefined,
      }
    },

    computed: {
      inputTypes() {
        return [
          "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES",
          "LUTTE_BIOLOGIQUE",
          "APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX",
          "EPANDAGES_ORGANIQUES",
          "SEMIS",
          "AUTRE",
          "POT",
          "SUBSTRAT"
        ];
      }
    },

    methods: {
      init() {
        if (!this.domainInputs) {
          this.fetchInputs();
        }
      },

      sortedInputs() {
        return this.domainInputs
          && Array.from(this.domainInputs)
            .sort((e1, e2) => e1.inputName === undefined ? -1 : e1.inputName.localeCompare(e2.inputName))
          || [];
      },

      showAddButton(inputType) {
        var count = this.domainInputs ? this.domainInputs.filter((domainInput) => domainInput.inputType === inputType).length : 0;
        return (inputType != 'CARBURANT' && inputType != 'IRRIGATION') || count < 1;
      },

      newInput(inputType) {
        this.editedInput = { fKey: this.generateUniqueSerial() };
        this.openModal(inputType);
      },

      generateUniqueSerial() {
        return 'xxxx-xxxx-xxx-xxxx'.replace(/[x]/g, (c) => {
          const r = Math.floor(Math.random() * 16);
          return r.toString(16);
        });
      },

      showMoreInfos(elt) {
        $(elt).parent().siblings('.more-infos').slideToggle();
      },

      editInput(inputType, input) {
        this.editedInput = input;
        this.openModal(inputType);
      },

      openModal(inputType) {
        this.selectedInputType = inputType;
        this.isModalActive = true;
      },

      openWorkforceOperationModal() {
        var index = this.domainInputs.findIndex((input) => input.inputType === 'IRRIGATION');
        this.editedIrrigationInput = index !== -1 ? this.domainInputs[index] : { fKey: this.generateUniqueSerial() };
        index = this.domainInputs.findIndex((input) => input.inputType === 'CARBURANT');
        this.editedFuelInput =  index !== -1 ? this.domainInputs[index] : { fKey: this.generateUniqueSerial() };
        index = this.domainInputs.findIndex((input) => input.inputType === 'MAIN_OEUVRE_MANUELLE');
        this.editedManualWorkforceInput = index !== -1 ? this.domainInputs[index] : { fKey: this.generateUniqueSerial() };
        index = this.domainInputs.findIndex((input) => input.inputType === 'MAIN_OEUVRE_TRACTORISTE');
        this.editedMechanizedWorkforceInput = index !== -1 ? this.domainInputs[index] : { fKey: this.generateUniqueSerial() };
        this.isWorkforceOperationModalActive = true;
      },

      openCopyModal() {
        this.isCopyModalActive = true;
      },

      saveInput(newOrEditedInput) {
        if (newOrEditedInput.topiaId) {
          var index = this.domainInputs.findIndex((input) => input.topiaId === newOrEditedInput.topiaId);
          if (index !== -1) {
            this.domainInputs[index] = newOrEditedInput;
          }
        } else {
          var index = this.domainInputs.findIndex((input) => input.fKey && newOrEditedInput.fKey && input.fKey === newOrEditedInput.fKey);
          if (index !== -1) {
            this.domainInputs[index] = newOrEditedInput;
          } else {
            this.domainInputs.push(newOrEditedInput);
          }
        }
        this.closeModal();
      },

      saveInputs(newOrEditedInputs) {
        newOrEditedInputs.forEach((newOrEditedInput) => {
          this.saveInput(newOrEditedInput);
        });
        this.closeWorkforceOperationModal();
      },

      copyInputs(inputsToCopy) {
        this.domainInputs = this.domainInputs.concat(inputsToCopy);
        this.closeCopyModal();
      },

      closeModal() {
        this.isModalActive = false;
        this.selectedInputType = undefined;
      },

      closeCopyModal() {
        this.isCopyModalActive = false;
      },

      closeWorkforceOperationModal() {
        this.isWorkforceOperationModalActive = false;
      },

      isUsed(inputType, input) {
        return this.domainInputsUsageMap[inputType] && this.domainInputsUsageMap[inputType].usageMap ?
          this.domainInputsUsageMap[inputType].usageMap[input.topiaId] : false;
      },

      deleteInput(deletedInput) {
        var indexOf = this.domainInputs.indexOf(deletedInput);
        if (indexOf != -1) {
          this.domainInputs.splice(indexOf, 1);
        }
      },

      fetchInputs() {
        setTimeout(() => { displayPageLoading() }, 100);

        fetch(context.inputFetchEndpoint + '?domainId=' + context.domainId, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.domainInputsUsageMap = data['domainInputsAndUsages'];
            this.domainInputs = [];
            Object.keys(this.domainInputsUsageMap)
              .forEach((inputType) => this.domainInputs = this.domainInputs.concat(this.domainInputsUsageMap[inputType].elements));
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des intrants';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            setTimeout(() => { hidePageLoading() }, 100);
          });
      }
    }
  })
    .provide('context', context)
    .use(AgrosystVueComponents.i18n)
    .use(Oruga.Config, { iconPack: 'fa', mobileBreakpoint: '480px' })
    .use(Oruga.Modal)
    .use(Oruga.Field)
    .use(Oruga.Input)
    .use(Oruga.Select)
    .use(Oruga.Tooltip)
    .use(Oruga.Icon)
    .use(Oruga.Switch)
    .use(Oruga.Button)
    .use(Oruga.Checkbox)
    .use(Oruga.Radio)
    .use(Oruga.Collapse)
    .use(Oruga.Autocomplete)
    .component('GenericModal', AgrosystVueComponents.Common.getGenericModal())
    .component('DomainsEditInputCopyInputsModal', AgrosystVueComponents.Domains.getDomainsEditInputCopyInputsModal())
    .component('DomainInputsFertilisantsMinerauxModal', AgrosystVueComponents.Domains.getDomainInputsFertilisantsMinerauxModal())
    .component('DomainInputsOtherModal', AgrosystVueComponents.Domains.getDomainInputsOtherModal())
    .component('DomainInputsPhytosanitairesModal', AgrosystVueComponents.Domains.getDomainInputsPhytosanitairesModal())
    .component('DomainInputsWorkforceOperationModal', AgrosystVueComponents.Domains.getDomainInputsWorkforceOperationModal())
    .component('DomainInputsEpandagesOrganiquesModal', AgrosystVueComponents.Domains.getDomainInputsEpandagesOrganiquesModal())
    .component('DomainInputsLutteBiologiqueModal', AgrosystVueComponents.Domains.getDomainInputsLutteBiologiqueModal())
    .component('DomainInputsPotModal', AgrosystVueComponents.Domains.getDomainInputsPotModal())
    .component('DomainInputsSemisModal', AgrosystVueComponents.Domains.getDomainInputsSemisModal())
    .component('DomainInputsSubstrateModal', AgrosystVueComponents.Domains.getDomainInputsSubstrateModal())
    .component('DomainsEditInputInputsModalPriceSection', AgrosystVueComponents.Domains.getDomainsEditInputInputsModalPriceSection())
    .component('DomainsEditInputInputsModalLightPriceSection', AgrosystVueComponents.Domains.getDomainsEditInputInputsModalLightPriceSection());

  controller.config.globalProperties.$filters = AgrosystVueComponents.filters;

  return controller;
};
