/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getToolCouplingEdition = function(data) {
  return {
    data() {
      return {
        editedToolsCoupling: {
          equipments: [],
          mainsActions: [],
          antiDriftNozzle: false,
          manualIntervention: false
        },
        manualWorkRateFlag: false,
        manualTransitVolumeFlag: false,
        workRateUnits: null,
        // available units according to materiel
        workRate_default_unit: [ "H_HA", "HA_H" ],
        workRate_t_h_unit: [ "H_HA", "HA_H", "T_H" ],
        workRate_voy_h_unit: [ "VOY_H", "H_HA", "HA_H" ],
        workRate_bal_h_unit: [ "BAL_H", "H_HA", "HA_H" ],
        alreadyExistingToolsCouplingName: false,
        selectedActionType: '',
        selectedMainAction: {},
        availableActions: {}
      };
    },

    props: ['toolCouplingEditionContext', 'toolCouplingEditionData'],

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal()
    },

    computed: {
      newCoupling() {
        return this.toolCouplingEditionContext && this.toolCouplingEditionContext._newCoupling;
      },

      tractorRequired() {
        return this.editedToolsCoupling && !this.editedToolsCoupling.manualIntervention
          && !this.checkIfIsIrrigationEquipments().isIrrigationEquipments
      },

      tractorsAndAutomoteurs() {
        let tractors = this.toolCouplingEditionContext.equipments.filter(eq => eq.refMateriel.type === 'TRACTEUR' || eq.refMateriel.type === 'AUTOMOTEUR');
        tractors.sort((t1, t2) => t1.name0.localeCompare(t2.name0));
        return tractors;
      },

      irrigationMateriels() {
        let irrigations = this.toolCouplingEditionContext.equipments.filter(eq => eq.refMateriel.type === 'IRRIGATION');
        irrigations.sort((i1, i2) => i1.name0.localeCompare(i2.name0));
        return irrigations;
      },

      outilRequired() {
        return this.editedToolsCoupling && !this.editedToolsCoupling.manualIntervention
            && this.editedToolsCoupling.tractor && this.editedToolsCoupling.tractor.refMateriel.type === 'TRACTEUR';
      },

      outilMateriels() {
        let outils;
        if (this.editedToolsCoupling.manualIntervention) {
          outils = this.toolCouplingEditionContext.equipments.filter(eq => eq.refMateriel.petitMateriel);
        } else {
          outils = this.toolCouplingEditionContext.equipments.filter(eq => eq.refMateriel.type === 'OUTIL');
        }

        outils.sort((o1, o2) => o1.name0.localeCompare(o2.name0));
        return outils;
      },

      showToolsCouplingBoiledQuantity() {
        let actionTypes = this.editedToolsCoupling.mainsActions.map(a => a.intervention_agrosyst);
        return actionTypes.includes("LUTTE_BIOLOGIQUE") || actionTypes.includes("APPLICATION_DE_PRODUITS_PHYTOSANITAIRES");
      },

      showToolsCouplingAntiDriftNozzle() {
        return this.editedToolsCoupling && this.editedToolsCoupling.mainsActions
          .map(a => a.intervention_agrosyst)
          .includes("APPLICATION_DE_PRODUITS_PHYTOSANITAIRES");
      },

      isToolsCouplingValid() {
        return this.editedToolsCoupling
            && this.editedToolsCoupling.mainsActions.length > 0
            && this.editedToolsCoupling.toolsCouplingName
            && (!this.editedToolsCoupling.workRate || this.editedToolsCoupling.workRate >= 0)
            && (!this.editedToolsCoupling.revertWorkRate || this.editedToolsCoupling.revertWorkRate >= 0)
            && (!this.editedToolsCoupling.workforce || this.editedToolsCoupling.workforce >= 0)
            && (!this.tractorRequired || this.editedToolsCoupling.tractor)
            && (!this.outilRequired || this.editedToolsCoupling.equipments && this.editedToolsCoupling.equipments.length > 0);
      }
    },

    watch: {
      toolCouplingEditionContext(newValue, oldValue) {
        if (newValue) {
          if (newValue.toolsCouplingToEdit) {
            this.editedToolsCoupling = newValue.toolsCouplingToEdit;
            this.editedToolsCoupling.availableActionTypes = this._getNewActionTypes();
            this.editedToolsCoupling.mainsActions.forEach(ma => this._updateAvailableActionTypes(ma));
          } else {
            this.workRateUnits = this.workRate_default_unit;
            this.editedToolsCoupling = newValue.toolsCouplingToEdit || {
              equipments: [],
              mainsActions: [],
              antiDriftNozzle: false,
              manualIntervention: newValue.manualIntervention,
              workRate: null,
              workRateUnit: newValue.manualIntervention ? "HA_H" : undefined,
              availableActionTypes: this._getNewActionTypes(),
            };
          }

          // on garde les action dont le type est toujours disponible
          // réinitialisation de l'UI
          this.availableActions = this._getAvailableActions();
          this.selectedMainAction = this.availableActions[0];
          this._initWorkRateToolsCoupling();
          this._checkIfToolsCouplingValid();

          var workRateInfos = this._computeWorkRate();
          this.manualWorkRateFlag = workRateInfos.workRate != this.editedToolsCoupling.workRate
              || workRateInfos.workRateUnit != this.editedToolsCoupling.workRateUnit;
          this.manualTransitVolumeFlag = false;
          if (!this.manualWorkRateFlag) {
            this.manualTransitVolumeFlag = workRateInfos.transitVolume != this.editedToolsCoupling.transitVolume;
          }
        }
      },

      'editedToolsCoupling.toolsCouplingName'(newValue, oldValue) {
        this.alreadyExistingToolsCouplingName = newValue && newValue != oldValue &&
            this.toolCouplingEditionContext.toolCouplingNames
              .some(tc => tc.toUpperCase() === newValue.toUpperCase());
        this._checkIfToolsCouplingValid();
      },

      'editedToolsCoupling.tractor'() {
        // setup workRate values
        if (!this.manualWorkRateFlag){
          var workRateInfos = this._computeWorkRate();
          this.editedToolsCoupling.workRate = workRateInfos.workRate;
          this.editedToolsCoupling.workRateUnit = workRateInfos.workRateUnit;
          this._computeRevertWorkRate();
          if(!this.manualTransitVolumeFlag) {
            this.editedToolsCoupling.transitVolume = workRateInfos.transitVolume;
            this.editedToolsCoupling.transitVolumeUnit = workRateInfos.transitVolumeUnit;
          }
        }
        this._setUpManuallyModifiedFlag();
        this._checkIfToolsCouplingValid();
      },

      'editedToolsCoupling.equipments'() {
        // setup workRate values
        if (!this.manualWorkRateFlag) {
          var workRateInfos = this._computeWorkRate();

          this.editedToolsCoupling.workRate = workRateInfos.workRate;
          this.editedToolsCoupling.workRateUnit = workRateInfos.workRateUnit;

          if (!this.manualTransitVolumeFlag) {
            this.editedToolsCoupling.transitVolume = workRateInfos.transitVolume;
            this.editedToolsCoupling.transitVolumeUnit = workRateInfos.transitVolumeUnit;
          }
          this._computeRevertWorkRate();

        }
        this._setUpManuallyModifiedFlag();
        this._checkIfToolsCouplingValid();
      }
    },

    methods: {
      close() {
        this.toolCouplingEditionContext && this.toolCouplingEditionContext.close(this.toolCouplingEditionContext.toolsCouplingToEditBkp);
      },

      validate() {
        if (this.toolCouplingEditionContext){
          this.toolCouplingEditionContext.callback(this.toolCouplingEditionContext.index, this.editedToolsCoupling);
          this.toolCouplingEditionContext.close(this.toolCouplingEditionContext.toolsCouplingToEditBkp);
        }
      },

      checkIfIsIrrigationEquipments() {
        var isIrrigationEquipments = false;
        var isNonIrrigationEquipments = false;
        if (this.editedToolsCoupling) {
          isIrrigationEquipments = this.editedToolsCoupling.equipments.some(equipment => equipment.refMateriel.type === "IRRIGATION");
          isNonIrrigationEquipments = this.editedToolsCoupling.equipments.some(equipment => equipment.refMateriel.type !== "IRRIGATION");
        }
        var result = {
          isIrrigationEquipments: isIrrigationEquipments,
          isNonIrrigationEquipments: isNonIrrigationEquipments
        };
        return result;
      },

      _updateAvailableActionTypes(mainAction) {
        // This list MUST BE kept in sync with the list of the same name in the function `_addMainActions` in itk.js
        const TYPES_ALLOWING_3_ACTIONS = ['AUTRE', 'ENTRETIEN_TAILLE_VIGNE_ET_VERGER', 'TRAVAIL_DU_SOL'];

        const interventionType = mainAction.intervention_agrosyst;
        if (!TYPES_ALLOWING_3_ACTIONS.includes(interventionType) && interventionType !== 'EMPTY') {
          delete this.editedToolsCoupling.availableActionTypes[interventionType];
        } else {
          // 3 different actions with the same type have been declared
          const typeHas3Actions = TYPES_ALLOWING_3_ACTIONS
              .map(type => interventionType == type && this.editedToolsCoupling.mainsActions.filter(ma => ma.intervention_agrosyst === type).length >= 3)
              .some(Boolean);
          if (typeHas3Actions && interventionType !== 'EMPTY') {
            delete this.editedToolsCoupling.availableActionTypes[interventionType];
          }
        }

        this._sortAvailableActionTypes();
      },

      _sortAvailableActionTypes() {
        let availableActionTypesValues = Object.entries(this.editedToolsCoupling.availableActionTypes);
        availableActionTypesValues.sort((a, b) => {
          return a[1].localeCompare(b[1]);
        });
        this.editedToolsCoupling.availableActionTypes = Object.fromEntries(availableActionTypesValues);
      },

      _getAvailableActions() {
        // on garde les action dont le type est toujours disponible
        let filteredActions = this._getNewAvailableActions().filter(action => {
          let isActionAlreadyPresent = this.editedToolsCoupling.mainsActions.filter(aa => aa.topiaId === action.topiaId).length > 0;
          return action.topiaId === 'aaaaaaa' ? true : this.editedToolsCoupling.availableActionTypes[action.intervention_agrosyst] && !isActionAlreadyPresent;
        });

        // réinitialisation de l'UI
        return filteredActions;
      },

      _checkIfToolsCouplingValid() {
        if (this.editedToolsCoupling) {
          var r0 = this.checkIfIsIrrigationEquipments();
          var isIrrigationEquipments = r0.isIrrigationEquipments;
          var isNonIrrigationEquipments = r0.isNonIrrigationEquipments;

          var commonValidation =
            !this.alreadyExistingToolsCouplingName &&
            (!!this.editedToolsCoupling.tractor || this.editedToolsCoupling.manualIntervention === true || (isIrrigationEquipments === true)) &&
            !!this.editedToolsCoupling.toolsCouplingName &&
            !!this.editedToolsCoupling.toolsCouplingName.trim() &&
            this.editedToolsCoupling.mainsActions !== null &&
            this.editedToolsCoupling.mainsActions.length > 0 &&
            this.editedToolsCoupling.mainsActions[0] &&
            this.editedToolsCoupling.mainsActions[0].intervention_agrosyst !== null;

          // 1 tractor + 1 ou n equipments, equipments must not mix irrigation equipments with other equipments
          var isValidTractor =
            !!this.editedToolsCoupling.tractor &&
            this.editedToolsCoupling.tractor.refMateriel.type === "TRACTEUR" &&
            this.editedToolsCoupling.equipments.length > 0 &&
            (
              (isIrrigationEquipments === true && isNonIrrigationEquipments === false) ||
              (isIrrigationEquipments === false && isNonIrrigationEquipments === true) ||
              (this.editedToolsCoupling.manualIntervention === true)
            );

          // 1 Automotor + 0 ou n equipments, equipments must not mix irrigation equipments with other equipments
          var isValidAutomotor =
            !!this.editedToolsCoupling.tractor &&
            this.editedToolsCoupling.tractor.refMateriel.type === "AUTOMOTEUR" &&
            (
              isIrrigationEquipments === false ||
              (isIrrigationEquipments === true && isNonIrrigationEquipments === false)
            );

          // 0 tractor/Automotor and some irrigation equipments only
          var isValidIrrigationSystem =
            (isNonIrrigationEquipments === false && isIrrigationEquipments === true);

          // no tools coupling to validate for manual intervention
          var isManualIntervention = this.editedToolsCoupling.manualIntervention === true;

          var result = commonValidation && (isValidTractor || isValidAutomotor || isValidIrrigationSystem || isManualIntervention);
          this.isValidToolsCoupling = result;
        } else {
          this.isValidToolsCoupling = false;
        }

      },

      setToolsCouplingName(action) {
        if (!this.editedToolsCoupling.toolsCouplingName && this.editedToolsCoupling.mainsActions.length === 1 && action) {
          this.editedToolsCoupling.toolsCouplingName = action.reference_label_Translated;
          this.editedToolsCoupling.toolsCouplingName0 = unaccent(this.editedToolsCoupling.toolsCouplingName);
        }
      },

      _setUpManuallyModifiedFlag() {
        var computedWorkRate = this._computeWorkRate();

        var workRateManuallyChange;
        var workRateUnitManuallyChange;
        var transitVolumeManuallyChange;

        if (!this.editedToolsCoupling.workRate) {
          workRateManuallyChange = false;
          workRateUnitManuallyChange = false;
          transitVolumeManuallyChange = false;

        } else {
          if (computedWorkRate.workRate) {
            workRateManuallyChange = computedWorkRate.workRate != this.editedToolsCoupling.workRate;
            transitVolumeManuallyChange = computedWorkRate.transitVolume != this.editedToolsCoupling.transitVolume;
          } else {
            workRateManuallyChange = !!this.editedToolsCoupling.workRate;
            transitVolumeManuallyChange = workRateManuallyChange;
          }

          if (computedWorkRate.workRateUnit) {
            workRateUnitManuallyChange = computedWorkRate.workRateUnit != this.editedToolsCoupling.workRateUnit;
          } else {
            workRateUnitManuallyChange = computedWorkRate.workRateUnit != "HA_H";
          }
        }
        this.manualWorkRateFlag = workRateManuallyChange || workRateUnitManuallyChange;
        this.manualTransitVolumeFlag = transitVolumeManuallyChange;
      },

      workRateChange() {
        this._computeRevertWorkRate();
        this._setUpManuallyModifiedFlag();
      },

      reverseWorkRateChange() {
        // set 3 digits after dot
        if (this.editedToolsCoupling.revertWorkRate && this.editedToolsCoupling.revertWorkRate !== 0) {
          this.editedToolsCoupling.workRate = parseInt((1/this.editedToolsCoupling.revertWorkRate) * 1000)/1000;
        } else {
          delete this.editedToolsCoupling.workRate;
        }
        this._setUpManuallyModifiedFlag();
      },

      selectTractor(tractor) {
        //type: 'AUTOMOTEUR'
        if (this.editedToolsCoupling.tractor === tractor) {
          delete this.editedToolsCoupling.tractor;
        } else {
          this.editedToolsCoupling.tractor = tractor;
        }

      },

      _computeRevertWorkRate() {
        // set 3 digits after dot
        if (!!this.editedToolsCoupling.workRate && this.editedToolsCoupling.workRate > 0 &&
           (this.editedToolsCoupling.manualIntervention || this.editedToolsCoupling.workRateUnit == "HA_H" || this.editedToolsCoupling.workRateUnit == "H_HA")) {
          this.editedToolsCoupling.revertWorkRate = parseInt((1/this.editedToolsCoupling.workRate) * 1000)/1000;
        }  else {
          delete this.editedToolsCoupling.revertWorkRate;
        }
      },

      _initWorkRateToolsCoupling() {
        this._computeRevertWorkRate();
        if (this.editedToolsCoupling.manualIntervention) {
          this.workRateUnits = this.workRate_default_unit;
        } else if (this.editedToolsCoupling.tractor && this.editedToolsCoupling.tractor.refMateriel.type == "AUTOMOTEUR") {
          this.workRateUnits = this.editedToolsCoupling.tractor.refMateriel.performanceUnite === "T_H" ? this.workRate_t_h_unit : this.workRate_default_unit;
        } else {
          var equipmentUnite;
          if (this.editedToolsCoupling.equipments.length > 0) {
            equipmentUnite = this.editedToolsCoupling.equipments[0].refMateriel.performanceUnite;
          }
          if (this.editedToolsCoupling.equipments.length > 1){
            // default value
            this.workRateUnits = this.workRate_default_unit;
          } else if (equipmentUnite && equipmentUnite === "VOY_H") {
            this.workRateUnits = this.workRate_voy_h_unit;
            this.editedToolsCoupling.transitVolumeUnit = this.editedToolsCoupling.equipments[0].refMateriel.donneesTransport2Unite;
          } else if (equipmentUnite && equipmentUnite === "BAL_H") {
            this.workRateUnits = this.workRate_bal_h_unit;
          } else {
            // default value
            this.workRateUnits = this.workRate_default_unit;
          }
        }
      },

      _computeWorkRate() {
        var workRateInfos = {};
        if (this.editedToolsCoupling.tractor && this.editedToolsCoupling.tractor.refMateriel.type === "AUTOMOTEUR") {
          // if there is no work rate, then find it from materiel
          workRateInfos.workRateUnit = this.editedToolsCoupling.tractor.refMateriel.performanceUnite;
          this.workRateUnits = workRateInfos.workRateUnit === "T_H" ? this.workRate_t_h_unit : this.workRate_default_unit;
          if (this.workRate_t_h_unit.indexOf(workRateInfos.workRateUnit) == -1) {
            // if the unit is not allowed then the default one is set up.
            workRateInfos.workRateUnit = "H_HA";
          } else {
            workRateInfos.workRate = this.editedToolsCoupling.tractor.refMateriel.performance;
          }
        }

        // case of work rate must be computed from different equipments or from one and an automoteur.
        if ((this.editedToolsCoupling.equipments && this.editedToolsCoupling.equipments.length > 1) ||
          (this.editedToolsCoupling.equipments.length == 1 && workRateInfos.workRate)) {
          var equipments = this.editedToolsCoupling.equipments;
          // from spec: if there are more than one equipment the work rate can be computed only if there unit is ha/h
          var performance =  this._returnLowestEquipmentWorkRate(workRateInfos, equipments);

          if (performance !== null) {
            workRateInfos = performance;
          } else {
            workRateInfos = {};
            workRateInfos.workRate = null;
            workRateInfos.workRateUnit = this.editedToolsCoupling.manualIntervention ? "HA_H" : "H_HA";
          }
          this.workRateUnits = this.workRate_default_unit;

        } else if (this.editedToolsCoupling.equipments.length == 1 && !workRateInfos.workRate) {
          var equipment = this.editedToolsCoupling.equipments[0];
          var performanceUnite = equipment.refMateriel.performanceUnite;
          if (performanceUnite && performanceUnite == "HA_H") {
            workRateInfos.workRate = equipment.refMateriel.performance;
            workRateInfos.workRateUnit = performanceUnite;
            this.workRateUnits = this.workRate_default_unit;
          } else if (performanceUnite && performanceUnite == "VOY_H") {
            workRateInfos.workRate = equipment.refMateriel.performance;
            workRateInfos.workRateUnit = performanceUnite;
            workRateInfos.transitVolumeUnit = equipment.refMateriel.donneesTransport2Unite;
            workRateInfos.transitVolume = equipment.refMateriel.donneesTransport2;
            this.workRateUnits = this.workRate_voy_h_unit;
          } else if (performanceUnite && performanceUnite == "BAL_H") {
            workRateInfos.workRate = equipment.refMateriel.performance;
            workRateInfos.workRateUnit = performanceUnite;
            this.workRateUnits = this.workRate_bal_h_unit;
          } else {
            this.workRateUnits = this.workRate_default_unit;
            workRateInfos.workRate = null;
            workRateInfos.workRateUnit = this.editedToolsCoupling.manualIntervention ? "HA_H" : "H_HA";
          }
        }

        if ((!this.editedToolsCoupling.equipments || this.editedToolsCoupling.equipments.length === 0) && (!this.editedToolsCoupling.tractor)) {
          this.workRateUnits = this.workRate_default_unit;
          workRateInfos = {};
          workRateInfos.workRate = null;
          workRateInfos.workRateUnit = this.editedToolsCoupling.manualIntervention ? "HA_H" : "H_HA";
        }

        return workRateInfos;
      },

      _returnLowestEquipmentWorkRate(automoteurWorkRateInfo, equipments) {
        // the work rate is the lowest one
        var workRateInfos = {};

        // automoteur part
        if(!!automoteurWorkRateInfo && !!automoteurWorkRateInfo.workRateUnit) {
          if ((automoteurWorkRateInfo.workRateUnit == "H_HA" || automoteurWorkRateInfo.workRateUnit == "HA_H" ) &&
                 !!automoteurWorkRateInfo.workRate) {
            // automoteur work rate is valid.
            workRateInfos.workRate = automoteurWorkRateInfo.workRate;
            workRateInfos.workRateUnit = automoteurWorkRateInfo.workRateUnit;
          } else {
            // automoteur work rate is not valid.
            return null;
          }
        }

        // equipment part
        for (var i = 0; i < equipments.length ; i++) {
          var equipment = equipments[i];
          if (!workRateInfos.workRate && (equipment.refMateriel.performanceUnite === "H_HA" || equipment.refMateriel.performanceUnite === "HA_H")) {
            // first true case
            workRateInfos.workRateUnit = equipment.refMateriel.performanceUnite;
            workRateInfos.workRate = equipment.refMateriel.performance;
          } else if (workRateInfos.workRateUnit === equipment.refMateriel.performanceUnite){
            workRateInfos.workRate = workRateInfos.workRate > equipment.refMateriel.performance ? equipment.refMateriel.performance : workRateInfos.workRate;
          } else {
            return null;
          }
        }
        return workRateInfos;
      },

      newActionTypeSelected(userActionType) {
        // on filtre les actions en fonction du type
        this.availableActions = this.availableActions.filter(aa =>
          {
            if (aa.topiaId === 'aaaaaaa') {
              return true;
            }
            return aa.intervention_agrosyst === userActionType;
          });

        // s'il n'y a qu'un choix valide on le sélectionne
        if (this.availableActions.filter(aa => !(aa.topiaId === 'aaaaaaa')).length === 1) {
          this.selectedMainAction = this.availableActions[1];
          this.newActionSelected();
        }
      },

      newActionSelected(uiSelectedMainAction) {
        if (this.selectedMainAction) {
          this.selectedMainAction.newMainAction = true;
          this.editedToolsCoupling.mainsActions.push(this.selectedMainAction);
          this._updateAvailableActionTypes(this.selectedMainAction);
        }
        // dans le cas où l'utilisateur sélectionne directement l'action sans avoir sélectionné de type
        if (uiSelectedMainAction && !this.selectedActionType) {
            uiSelectedMainAction.newMainAction = true;
            this._updateAvailableActionTypes(uiSelectedMainAction);
        }
        this._updateToolsCouplingWithNewActionSelected();
      },

      _updateToolsCouplingWithNewActionSelected() {
        // pour les action manuelles on renseigne le nom de la combinaison d'outils avec le nom de l'action
        if (this.editedToolsCoupling.mainsActions.length > 0 &&
            this.editedToolsCoupling.manualIntervention &&
            (!this.editedToolsCoupling.toolsCouplingName || this.editedToolsCoupling.toolsCouplingName === "")) {
          this.editedToolsCoupling.toolsCouplingName = this.editedToolsCoupling.mainsActions[0].reference_label_Translated;
          this.editedToolsCoupling.toolsCouplingName0 = unaccent(this.editedToolsCoupling.toolsCouplingName);
        }

        // réinitialisation de l'UI
        this.availableActions = this._getAvailableActions();
        this.selectedActionType = null;
        this.selectedMainAction = null;
        // dans le cas où l'utilisateur à choisi manuellement l'action, il faut replacer l'index de la liste sur l'option vide (0)
        if (this.$refs.availableActionTypes) this.$refs.availableActionTypes[0].selected = 'selected';
        if (this.$refs.availableActions) this.$refs.availableActions[0].selected = 'selected';
        this._checkIfToolsCouplingValid();
      },

      _getNewActionTypes() {
        var availableActionTypes = { EMPTY : ''};
        let keys = Object.keys(this.toolCouplingEditionData.agrosystInterventionTypes);
        Object.keys(this.toolCouplingEditionData.agrosystInterventionTypes).map(key => {
          let value = this.toolCouplingEditionData.agrosystInterventionTypes[key];
          availableActionTypes[key] = value;
        })
        return availableActionTypes;
      },

      _getNewAvailableActions() {
        let result = [];
        // add now empty line to ction list
        result.push({topiaId: 'aaaaaaa', reference_label_Translated : '', intervention_agrosyst : ''});
        this.toolCouplingEditionData.agrosystActionsFullList.forEach(action => {
          result.push({
            topiaId: action.topiaId,
            reference_label_Translated: action.reference_label_Translated,
            intervention_agrosyst : action.intervention_agrosyst,
          });
        });
        return result;
      },

      _addActionsWithType(actionType_) {
        this.toolCouplingEditionData.agrosystActionsFullList.forEach(action => {
           var actionType = action.intervention_agrosyst;
           if (actionType === actionType_) {
             // only add if not present
             if (this.availableActions.filter(aa => (aa.topiaId === action.topiaId)).length === 0) {
                this.availableActions.push({
                  topiaId: action.topiaId,
                  reference_label_Translated: action.reference_label_Translated,
                  intervention_agrosyst : action.intervention_agrosyst,
                });
             }
           }
        });
      },

      deleteAction(action) {
        if (action) {
          if (window.confirm(this.$t("messages.domain-edit-tools-coupling-action-delete-confirm"))) {
            var index = this.editedToolsCoupling.mainsActions.indexOf(action);
            if (index != -1) {
              this.editedToolsCoupling.mainsActions.splice(index, 1);
              this.editedToolsCoupling.availableActionTypes[action.intervention_agrosyst] = this.toolCouplingEditionData.agrosystInterventionTypes[action.intervention_agrosyst];
              this._addActionsWithType(action.intervention_agrosyst);
            }
            this._checkIfToolsCouplingValid();
            this._sortAvailableActionTypes();
            this.selectedActionType = null;
            this.$refs.availableActionTypes[0].selected = 'selected';
            this.$refs.availableActions[0].selected = 'selected';
          }
        }
      },

    },
    template: '#domain-tools-coupling'
  }
}
