/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainsToPaste = function(data) {
  return {
    data() {
      let domainsTableState = AgrosystVueComponents.Table.usePaginatedTableMixin(
        null,
        {
          domainName: null,
          campaign: null,
          type: null,
          responsable: null,
          departement: null,
          active: true
        },
        this.loadDomains.bind(this)
      );

      return {
        domainTypes: data.domainTypes,
        domains: null,
        timer: null,
        ...domainsTableState
      };
    },

    props: ['copyDialogContext'],

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      AgTableFooter: window.AgrosystVueComponents.Table.getFooter({
          entitiesLabel: 'generic-domain-table-footer-entities',
          selectAllLabel: 'generic-domain-table-footer-selectAll',
          selectionLabel: 'generic-domain-table-footer-selection',
          selectionEnabled: true
        })
    },

    computed: {
      filteredSelectedDomains() {
        return Object.keys(this.tableState.selection).filter(key => !!this.tableState.selection[key]);
      },

      selectedDomainsNb() {
        return Object.values(this.tableState.selection).filter(value => !!value).length;
      }
    },

    watch: {
      'tableState.filter.domainName'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.campaign'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.mainContact'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.departement'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.responsable'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.type'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, true);
      },
      'tableState.filter.page'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, false);
      },
      'tableState.filter.pageSize'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, false);
      }
    },

    methods: {
      loadDomains() {
        displayPageLoading();
        fetch(ENDPOINTS.domainsListJson, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: "filter=" + encodeURIComponent(JSON.stringify(this.tableState.filter)) + "&fromNavigationContextChoose=false"
        })
        .then(response => response.json())
        .then(data => {
          this.domains = data.elements;
          this.tableState.pager = data;
        })
        .catch(function(response) {
          this.domains = null;
          this.tableState.pager = null;
          console.error("Can't get domains list", response);
          addPermanentError(this.$t("messages.domain-list-loading-error"));
        })
        .finally(hidePageLoading);
      },

      displayResponsibles(responsibles) {
        if (responsibles) {
          return responsibles.map(responsible => responsible.firstName + " " + responsible.lastName).join(", ");
        }
      },

      filterUpdated(newValue, oldValue, withTimer, resetPage) {
        if (newValue !== oldValue) {
          if (this.timer) {
            clearTimeout(this.timer);
          }
          let applyFilter = () => {
            if (resetPage) {
              this.tableState.filter.page = 0;
            }
            this.loadDomains();
          };
          if (withTimer) {
            this.timer = setTimeout(applyFilter, 350);
          } else {
            applyFilter();
          }
        }
      },

      close() {
        this.copyDialogContext && this.copyDialogContext.close();
      },

      paste() {
        if (this.copyDialogContext) {
          this.copyDialogContext.callback(this.filteredSelectedDomains);
          this.copyDialogContext.close();
        }
      }
    },

    mounted() {
      this.loadDomains();
    },

    template: '#domains-to-paste'
  }
}
