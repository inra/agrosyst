/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainCharacteristicsController = function (data) {
  const controller = Vue.createApp({
    data() {
      return {
        domain: data.domain,
        forFrance: data.forFrance,
        legalStatusId: data.domain.legalStatus && data.domain.legalStatus.topiaId,
        allRefLegalStatus: data.allRefLegalStatus,
        domainSiret: data.domainSiret,
        otex18: data.domain.otex18 && data.domain.otex18.code_OTEX_18_postes,
        otex18s: data.otex18s,
        otex70s: data.otex70s,
        otex70: data.domain.otex70 && data.domain.otex70.code_OTEX_70_postes,
        meadowAreaCheckHelpMessage: data.meadowAreaCheckHelpMessage,
        showStatusComment: false,
        showDomain_orientation: false,
        showWorkForce: false,
        showWorkforceComment: false,
        showExperimentalAgriculturalArea: false,
        showGovernmentParticipation: false,
        otex18callback: data.otex18callback
      };
    },
    computed: {
      isFarm() {
        return this.domain.type === 'EXPLOITATION_AGRICOLE' || this.domain.type === 'FERME_DE_LYCEE_AGRICOLE';
      },
      isCompany() {
        let isCompany = false;
        if ((this.domain.type === 'EXPLOITATION_AGRICOLE' || this.domain.type === 'FERME_DE_LYCEE_AGRICOLE')
            && this.domain.legalStatus) {
          isCompany = !!this.domain.legalStatus.societe;
        }
        return isCompany;
      },
      totalWorkForce() {
        let result = ((this.domain.otherWorkForce ? this.domain.otherWorkForce : 0) * 1)
            + ((this.domain.permanentEmployeesWorkForce ? this.domain.permanentEmployeesWorkForce : 0) * 1)
            + ((this.domain.temporaryEmployeesWorkForce ? this.domain.temporaryEmployeesWorkForce : 0) * 1);
        result = Math.round(result * 100) / 100;
        return result;
      },
      surfaceForUMO() {
        var result = 0;
        if (this.domain.usedAgriculturalArea && this.totalWorkForce !== 0) {
          result = this.domain.usedAgriculturalArea / this.totalWorkForce;
        }
        return result;
      }
    },
    watch: {
      otex18(newValue) {
        if (!newValue || newValue === "") {
          delete this.otex70s;
        } else {
          displayPageLoading();
          this.loadOtex70s(newValue);
        }
        this.otex18callback(newValue);
      }
    },
    methods: {
      loadOtex70s(otex18) {
        const self = this;
        return fetch(ENDPOINT_DOMAINS_LIST_OTEX_JSON + "?otex18code=" + otex18)
            .then(response => response.json())
            .then(data => self.otex70s = data)
            .catch(function(response) {
              console.error("Can't get refOtex70", response);
              addPermanentError("Échec de récupération des Otex 70", response.status);

            })
            .finally(hidePageLoading);
      }
    }
  })
  .use(AgrosystVueComponents.i18n);

  return controller;
}
