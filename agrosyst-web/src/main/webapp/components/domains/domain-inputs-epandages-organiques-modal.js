/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsEpandagesOrganiquesModal = function(data) {
  return {
    template: '#domain-inputs-epandages-organiques-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    mounted() {
      if (!this.types.length) {
        this.fetchTypes();
      }
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-composition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },

        types: [],
        selectedType: {},
        objectId: undefined,
        loading: 0,
        priceStepValid: !this.selectedInput.price || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit),

        // step définition
        search: '',
        refInputId: this.selectedInput.refInputId,
        tradeName: this.selectedInput.tradeName,
        inputName: this.selectedInput.inputName,
        usageUnit: this.selectedInput.usageUnit,
        organic: this.selectedInput.organic,

        // step composition
        n: this.selectedInput.n,
        p2O5: this.selectedInput.p2O5,
        k2O: this.selectedInput.k2O,
        caO: this.selectedInput.caO,
        mgO: this.selectedInput.mgO,
        s: this.selectedInput.s,

        // step prix
        price: this.selectedInput.price ? this.selectedInput.price.price : undefined,
        priceUnit: this.selectedInput.price ? this.selectedInput.price.priceUnit : 'EURO_T'
      };
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },

    validations() {
      return {
        // step définition
        tradeName: { required: VuelidateValidators.required, $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },

        // step composition
        n: { required: VuelidateValidators.required, decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        p2O5: { required: VuelidateValidators.required, decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        k2O: { required: VuelidateValidators.required, decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        caO: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        mgO: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        s: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
      }
    },

    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      organicProductUnits() { return Object.keys(this.context.i18n.OrganicProductUnit); }, // todo: this list has to be filtered at runtime see with dcosse
      fertiOrgaUnitLabel() {
        return this.selectedType && this.selectedType.unite_teneur_ferti_orga ?
          this.$t('FertiOrgaUnit.' + this.selectedType.unite_teneur_ferti_orga) : '-';
      },

      // step définition
      isFirstStepValid() { return this.isFieldValid('tradeName') && this.isFieldValid('inputName') && this.isFieldValid('usageUnit'); },

      // step composition
      isSecondStepValid() {
        return this.isFieldValid('n') && this.isFieldValid('p2O5') && this.isFieldValid('k2O') &&
          this.isFieldValid('caO') && this.isFieldValid('mgO') && this.isFieldValid('s');
      },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.isSecondStepValid;
        } else if (this.stepProgress.step === 2) {
          return this.priceStepValid;
        }
        return false;
      },

      filteredTypes() {
        if (this.types && this.types.length > 0) {
          return this.search && this.search.length > 0 ? this.types.filter(
            (option) => option.libelle_Translated.toString().toLowerCase().indexOf(this.search.toLowerCase()) >= 0
          ) : this.types;
        }
        return [];
      }
    },

    watch: {
      selectedInput(newInput) {
        // step définition
        this.refInputId = newInput.refInputId;
        this.tradeName = newInput.tradeName;
        this.inputName = newInput.inputName;
        this.usageUnit = newInput.usageUnit;

        // step composition
        this.n = newInput.n;
        this.p2O5 = newInput.p2O5;
        this.k2O = newInput.k2O;
        this.caO = newInput.caO;
        this.mgO = newInput.mgO;
        this.s = newInput.s;
        this.organic = newInput.organic;

        // step prix
        this.priceStepValid = !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit);
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = undefined;
        }
        this.v$.$reset();
      },

      tradeName(newTradeName) {
        if (newTradeName) {
          this.foundSelectedType(newTradeName);
          this.objectId = this.selectedType.idtypeeffluent;
          this.refInputId = this.selectedType ? this.selectedType.topiaId : undefined;
          this.inputName = newTradeName;
        }
      }
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      resetForm() {
        this.v$.$reset();
        this.search = '';
        this.stepProgress.step = 0;
      },

      fetchTypes() {
        this.loading++;
        displayPageLoading();

        var edaplosPart = '';
        if (this.selectedInput && this.selectedInput.refInputId && this.selectedInput.refInputId.endsWith('_0000000-0000-0000-0000-000000000000')) {
          edaplosPart = '?edaplos=true';
        }
        fetch(this.context.loadFertiOrgaJson + edaplosPart, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.types = data;
            this.foundSelectedType(this.tradeName);
            this.search = this.selectedType ? this.selectedType.libelle_Translated : '';
            if (this.selectedType.idtypeeffluent) {
              this.objectId = this.selectedType.idtypeeffluent;
            }
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des types de fertilisants organiques';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          })
      },

      foundSelectedType(tradeName) {
        var filteredTypes = this.types.filter((type) => type.libelle_Translated === tradeName);
        this.selectedType = filteredTypes && filteredTypes.length ? filteredTypes[0] : {};
      },

      save() {
        const input = {
          ...this.selectedInput,
          inputType: 'EPANDAGES_ORGANIQUES',

          // step définition
          refInputId: this.refInputId,
          tradeName: this.tradeName,
          inputName: this.inputName,
          usageUnit: this.usageUnit,

          // step composition
          n: this.n,
          p2O5: this.p2O5,
          k2O: this.k2O,
          caO: this.caO,
          mgO: this.mgO,
          s: this.s,
          organic: this.organic
        };
        // step prix
        if (!isNaN(parseFloat(this.price)) || this.priceUnit !==undefined) {
          input.price = {
            ...(this.selectedInput.price ? this.selectedInput.price : {}),
            category: 'ORGANIQUES_INPUT',
            price: this.price,
            priceUnit: this.priceUnit,
            displayName: this.tradeName,
            sourceUnit: this.usageUnit
          };
        } else {
          input.price = undefined;
        }
        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },

      selectOption(option) {
        this.tradeName = option ? option.libelle_Translated : undefined;
      }
    },
  };
}
