/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsPhytosanitairesModal = function(data) {
  return {
    template: '#domain-inputs-phytosanitaires-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean,
      ipmworks: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    mounted() {
      if (!this.types.length && !this.products.length) {
        this.fetchTypesProductsAndCodes();
      }
      if (this.tradeName) {
        this.search = this.tradeName;
        this.searchAMM = this.codeAMM;
        this.searchActiveSubstance = this.activeSubstance;
        this.fetchProduct(this.tradeName);
      }
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },

        forFrance: this.context.forFrance,
        types: [],
        products: [],
        codeAMMs: [],
        activeSubstances: [],
        productUnits: [],
        objectId: undefined,
        loading: 0,

        priceStepValid: this.isPriceStepValid(this.selectedInput),

        // step définition
        productType: this.selectedInput ? this.selectedInput.productType : undefined,
        search: '',
        tradeName: this.selectedInput ? this.selectedInput.tradeName : undefined,
        searchAMM: '',
        codeAMM: this.selectedInput ? this.selectedInput.ammCode : undefined,
        searchActiveSubstance: '',
        activeSubstance: this.selectedInput ? this.selectedInput.activeSubstance : undefined,
        refInputId: this.selectedInput ? this.selectedInput.refInputId : undefined,
        inputName: this.selectedInput ? this.selectedInput.inputName : undefined,
        usageUnit: this.selectedInput ? this.selectedInput.usageUnit : undefined,

        // step prix
        price: (this.selectedInput && this.selectedInput.price) ? this.selectedInput.price.price : undefined,
        priceUnit: (this.selectedInput && this.selectedInput.price )? this.selectedInput.price.priceUnit : 'EURO_L'
      }
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },

    validations() {
      const validCodeAMM = (codeAMM) => {
        return this.forFrance ? (codeAMM !== undefined && codeAMM !== '') : true;
      };
      return {
        // step définition
        productType: { $autoDirty: true },
        tradeName: { required: VuelidateValidators.required, $autoDirty: true },
        codeAMM: { required: validCodeAMM, $autoDirty: true },
        activeSubstance: { $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },
      }
    },

    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      phytoProductUnits() { return this.productUnits && this.productUnits.length > 0 ? this.productUnits : Object.keys(this.context.i18n.PhytoProductUnit); },

      // step définition
      isFirstStepValid() {
        return this.isFieldValid('productType') && this.isFieldValid('tradeName') && this.isFieldValid('codeAMM') &&
          this.isFieldValid('inputName') && this.isFieldValid('usageUnit') && this.refInputId;
      },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.priceStepValid;
        }
        return false;
      },

      filteredProducts() {
        if (this.products && this.products.length > 0) {
          return this.search && this.search.length > 0 ? this.products.filter(
            (product) => product.toString().toLowerCase().indexOf(this.search.toLowerCase()) >= 0
          ) : this.products;
        }
        return [];
      },

      filteredCodeAMMs() {
        if (this.codeAMMs && this.codeAMMs.length > 0) {
          return this.searchAMM && this.searchAMM.length > 0 ? this.codeAMMs.filter(
            (code) => code.toString().toLowerCase().indexOf(this.searchAMM.toLowerCase()) >= 0
          ) : this.codeAMMs;
        }
        return [];
      },

      filteredActiveSubstances() {
        if (this.activeSubstances && this.activeSubstances.length > 0) {
          return this.searchActiveSubstance && this.searchActiveSubstance.length > 0 ? this.activeSubstances.filter(
            (sub) => sub.toString().toLowerCase().indexOf(this.searchActiveSubstance.toLowerCase()) >= 0
          ) : this.activeSubstances;
        }
        return [];
      }
    },

    watch: {
      selectedInput(newInput) {
        // step définition
        this.productType = newInput.productType;
        this.tradeName = newInput.tradeName;
        this.refInputId = newInput.refInputId;
        this.inputName = newInput.inputName;
        this.usageUnit = newInput.usageUnit;

        // step prix
        this.priceStepValid = !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit);
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = undefined;
        }
        this.v$.$reset();
      },

      usageUnit() {
        this.treatAllowedPriceUnits();
      }
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;
      },

      resetForm() {
        this.v$.$reset();
        this.stepProgress.step = 0;
        this.search = '';
        this.searchAMM = '';
        this.searchActiveSubstance = '';
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      isPriceStepValid(selectedInput) {
        return !selectedInput || (!selectedInput.price || (!selectedInput.price.price) || (selectedInput.price && !isNaN(parseFloat(selectedInput.price.price)) && selectedInput.price.priceUnit));
      },

      fetchTypesProductsAndCodes() {
        this.loading++;
        var filter = {
          action: 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES',
          nomProduit: this.tradeName,
          typeProduit: this.productType,
          codeAMM: this.codeAMM,
          activeSubstancesTerm: this.activeSubstance,
          countryTopiaId: this.context.countryTopiaId,
          ipmworks: this.ipmworks
        };
        displayPageLoading();
        fetch(this.context.loadPhytoProductsJson + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.types = data.productTypes;
            // tri par ordre alphabétique de la traduction du type
            this.types = this.types.sort((t1, t2) => this.$t("ProductType." + t1).localeCompare(this.$t("ProductType." + t2)));
            this.products = data.productName;
            this.codeAMMs = data.code_AMMs;
            this.activeSubstances = data.activeSubstances;
            this.productUnits = data.phytoProductUnits;
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des types de produit et de la liste des produits';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      productTypeChanged(newProductType) {
        this.fetchProductsAndCodes(newProductType);
      },

      fetchProductsAndCodes(productType) {
        this.loading++;
        displayPageLoading();

        var filter = {
          action: 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES',
          nomProduit: this.tradeName,
          typeProduit: productType,
          codeAMM: this.codeAMM,
          activeSubstancesTerm: this.activeSubstance,
          countryTopiaId: this.context.countryTopiaId,
          ipmworks: this.ipmworks
        };
        fetch(this.context.loadPhytoProductsJson + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.refActaTraitementsProduitId !== undefined) {
              this.tradeName = data.productName[0];
              this.fetchProduct(this.tradeName);
            } else {
              this.products = data.productName;
              this.codeAMMs = data.code_AMMs;
              this.activeSubstances = data.activeSubstances;
              this.productUnits = data.phytoProductUnits;
              if (!this.products.includes(this.search)) {
                this.search = '';
              }
            }
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des produits';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      resetCommonFields() {
        this.selectedInput.productType = undefined;
        this.selectedInput.tradeName = undefined;
        this.selectedInput.activeSubstance = undefined;
        this.selectedInput.refInputId = undefined;
        this.selectedInput.inputName = undefined;
        this.selectedInput.usageUnit = undefined;
        this.searchActiveSubstance = '';
        this.inputName = undefined;
        this.usageUnit = undefined;
        this.search = undefined;
      },

      codeAMMChanged(newCodeAMM) {
        this.selectedInput.ammCode = newCodeAMM;

        var filter = {
          action: 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES',
          codeAMM: newCodeAMM,
          countryTopiaId: this.context.countryTopiaId,
          nomProduit: this.tradeName,
        };
        this.fetchProducts(filter);
      },

      activeSubstanceChanged(newSubstance) {
        var filter = {
          action: 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES',
          nomProduit: this.tradeName,
          codeAMM: this.searchAMM,
          activeSubstance: newSubstance,
          countryTopiaId: this.context.countryTopiaId,
          ipmworks: this.ipmworks
        };
        this.fetchProducts(filter);
      },

      fetchProducts(filter) {
        this.loading++;
        displayPageLoading();

        fetch(this.context.loadPhytoProductsJson + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            if (data.productTypes) this.types = data.productTypes;
            if (data.productTypes && data.productTypes.length === 1) this.productType = data.productTypes[0];
            if (!this.codeAMM) this.codeAMMs = data.code_AMMs;
            if (data.code_AMMs && data.code_AMMs.length === 1) this.codeAMM = data.code_AMMs[0];
            if (!this.activeSubstance) this.activeSubstances = data.activeSubstances;
            this.productUnits = data.phytoProductUnits;
            this.products = data.productName;
            if (data.productName.length === 1) {
              this.tradeName = data.productName[0];
              this.fetchProduct(this.tradeName);
              this.inputName = data.productName[0];
            }
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des produits';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      tradeNameChanged(newTradeName) {
        this.resetCommonFields();
        this.search = newTradeName;
        this.fetchProduct(newTradeName);
        this.inputName = newTradeName;
      },

      fetchProduct(tradeName) {
        this.loading++;
        displayPageLoading();

        var filter = {
          action: 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES',
          nomProduit: tradeName,
          typeProduit: this.productType,
          codeAMM: this.codeAMM,
          activeSubstancesTerm: this.activeSubstance,
          countryTopiaId: this.context.countryTopiaId,
          ipmworks: this.ipmworks
        };
        fetch(this.context.loadPhytoProductsJson + '?filter=' + encodeURIComponent(JSON.stringify(filter)), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.refInputId = data.refActaTraitementsProduitId;
            if (data.productTypes && data.productTypes.length === 1) this.productType = data.productTypes[0];
            if (data.productTypes && data.productTypes.length > 1) {
              this.types = data.productTypes;
            }

            if (data.code_AMMs && data.code_AMMs.length === 1) {
              this.searchAMM = data.code_AMMs[0];
              this.codeAMM = data.code_AMMs[0];
            }

            if (data.code_AMMs && data.code_AMMs.length > 1) {
              this.codeAMMs = data.code_AMMs;
            }

            this.activeSubstances = data.activeSubstances;

            this.productUnits = data.phytoProductUnits;
            if (data.phytoProductUnits && data.phytoProductUnits.length === 1) {
              this.usageUnit = data.phytoProductUnits[0];
            }

            if (data.productName && data.productName.length === 1) {
              this.tradeName = data.productName[0];
              this.search = data.productName[0];
            }

            this.objectId = data.idProduit + '_' + data.idTraitement;
          })
          .catch((response) => {
            const message = 'Échec de récupération du produit';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      treatAllowedPriceUnits() {
        if (this.prices && (this.prices.allowedPriceUnits === undefined || this.prices.allowedPriceUnits.length === 0)) {
          if (this.usageUnit === 'L_HA') {
            this.prices.allowedPriceUnits = ['EURO_L'];
          }
          if (this.usageUnit === 'KG_HA') {
            this.prices.allowedPriceUnits = ['EURO_KG'];
          }
          if (this.usageUnit === 'UNITE_HA') {
            this.prices.allowedPriceUnits = ['EURO_UNITE'];
          }
        }
      },

      save() {
        const input = {
          ...this.selectedInput,
          inputType: 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES',

          // step définition
          productType: this.productType,
          tradeName: this.tradeName,
          refInputId: this.refInputId,
          inputName: this.inputName,
          usageUnit: this.usageUnit,
          ammCode: this.codeAMM
        };

        // step prix
        if (!isNaN(parseFloat(this.price)) || this.priceUnit !==undefined) {
          input.price = {
            ...(this.selectedInput.price ? this.selectedInput.price : {}),
            category: 'PHYTO_TRAITMENT_INPUT',
            price: this.price,
            priceUnit: this.priceUnit,
            displayName: this.tradeName,
            sourceUnit: this.usageUnit
          };
        } else {
          input.price = undefined;
        }

        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },

      selectTradeName(tradeName) {
        this.tradeName = tradeName;
        this.tradeNameChanged(tradeName);
      },

      selectAMM(codeAMM) {
        this.codeAMM = codeAMM;
        this.codeAMMChanged(codeAMM);
      },

      selectActiveSubstance(activeSubstance) {
        this.activeSubstance = activeSubstance;
        this.activeSubstanceChanged(activeSubstance);
      }
    }
  };
}
