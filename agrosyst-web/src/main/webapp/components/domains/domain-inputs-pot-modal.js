/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsPotModal = function(data) {
  return {
    template: '#domain-inputs-pot-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    mounted() {
      if (!this.pots.length) {
        this.fetchPots();
      }
      if (this.characteristic1) {
        this.objectId = this.$filters.removeAccents(this.characteristic1);
      }
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },

        pots: [],
        prices: undefined,
        refPriceUnit: undefined,
        loading: 0,
        forFrance: this.context.forFrance,

        // step définition
        characteristic1: this.selectedInput.caracteristic1,
        characteristic1Translated: this.selectedInput.caracteristic1Translated,
        refInputId: this.selectedInput.refInputId,
        inputName: this.selectedInput.inputName,
        usageUnit: this.selectedInput.usageUnit ? this.selectedInput.usageUnit : 'POTS_M2',

        // step prix
        price: this.selectedInput.price ? this.selectedInput.price.price : undefined,
        priceUnit: this.selectedInput.price ? this.selectedInput.price.priceUnit : 'EURO_UNITE'
      };
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },

    validations() {
      return {
        // step définition
        characteristic1: { required: VuelidateValidators.required, $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },

        // step prix
        price: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        priceUnit: { $autoDirty: true }
      }
    },

    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      potInputUnits() { return Object.keys(this.context.i18n.PotInputUnit); },

      // step définition
      isFirstStepValid() {
        return this.isFieldValid('characteristic1') && this.isFieldValid('inputName') && this.isFieldValid('usageUnit');
      },

      // step prix
      isLastStepValid() { return this.isFieldValid('price') && this.isFieldValid('priceUnit'); },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.isLastStepValid;
        }
        return false;
      }
    },

    watch: {
      characteristic1(newCharacteristic1) {
        const pot = this.pots.filter((pot) => pot.caracteristic1 === newCharacteristic1)[0];
        this.refInputId = pot.topiaId;
        this.objectId = this.$filters.removeAccents(newCharacteristic1);
        this.inputName = newCharacteristic1;
      },

      selectedInput(newInput) {
        // step définition
        this.characteristic1 = newInput.caracteristic1;
        this.characteristic1Translated = newInput.caracteristic1Translated;
        this.refInputId = newInput.refInputId;
        this.inputName = newInput.inputName;
        this.usageUnit = newInput.usageUnit;

        // step prix
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = undefined;
        }
        this.v$.$reset();
      }
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      resetForm() {
        this.v$.$reset();
        this.stepProgress.step = 0;
      },

      fetchPots() {
        this.loading++;
        displayPageLoading();

        fetch(this.context.loadRefPotsJson, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.pots = data;
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste de référence des pots';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
            hidePageLoading();
          });
      },

      save() {
        const input = {
          ...this.selectedInput,
          inputType: 'POT',

          // step définition
          caracteristic1: this.characteristic1,
          caracteristic1Translated: this.characteristic1Translated,
          refInputId: this.refInputId,
          inputName: this.inputName,
          usageUnit: this.usageUnit,


        };
        // step prix
        if(!isNaN(parseFloat(this.price)) || this.priceUnit !==undefined) {
          input.price = {
            ...(this.selectedInput.price ? this.selectedInput.price : {}),
            category: 'POT_INPUT',
            price: this.price,
            priceUnit: this.priceUnit,
            displayName: this.characteristic1,
            sourceUnit: this.usageUnit
          }
        } else {
          input.price = undefined;
        }

        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },
    }
  };
}
