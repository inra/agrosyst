/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
//
// Référencé par src/main/webapp/WEB-INF/content/domains/domains-list.jsp
// Fait:
// * Gestion de la table listant les domaines
// Rest à faire:
// * Gestion des modals pour activer/désactiver des domaines, l'extension de domaines
//

window.AgrosystVueComponents.Domains.getDomainsListController = function (context) {
  // year input validator for Vuelidate
  const mustBeYear = (value) => !VuelidateValidators.helpers.req(value) || value.match(/^[0-9]{4}$/)

  let controller = Vue.createApp({
    /*setup () {
      return { v$: Vuelidate.useVuelidate() }
    },*/

    data() {
      let filter = {
        domainName: null,
        campaign: null,
        mainContact: null,
        type: null,
        responsable: null,
        departement: null,
        siret: null,
        active: null,
        ...context.initialFilter
      };

      let domainsTableState = AgrosystVueComponents.Table.usePaginatedTableMixin(
        context.initialPager, 
        filter,
        this.fetchDomains.bind(this),
        this.fetchDomainIds.bind(this));

      return {
        domainUrl: context.domainUrl,
        domainTypes: context.domainTypes,
        ...domainsTableState
      };
    },

    validations () {
      return {
        tableState: {
          filter: {
            campaign: { mustBeYear }
          }
        }
      }
    },

    computed: {
      isCampainFilterInvalid() {
        return false;//this.v$.tableState.filter.campaign.mustBeYear.$invalid;
      }
    },

    methods: {
      campaignFilterChanged() {
        if (!this.isCampainFilterInvalid) {
          this.refreshWithDelay();
        }
      },

      domainTypeName(domain) {
        return context.i18n.DomainType[domain.type]
      },

      domainResponsables(domain) {
        if (domain.responsibles) {
          return domain.responsibles
            .map((r) => r.firstName + ' ' + r.lastName)
            .join(', ')
        } else {
          return '-';
        }
      },

      buildQueryString() {
        // Do a deep clone of the filter object
        var filter = JSON.parse(JSON.stringify(this.tableState.filter));

        if (this.isCampainFilterInvalid) {
          delete filter.campaign;
        }

        var queryString = 'filter=' + encodeURIComponent(JSON.stringify(filter));

        return queryString;
      },

      fetchDomains() {
        var queryString = this.buildQueryString();

        displayPageLoading();
        fetch(context.fetchEndpoint, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: queryString
        })
        .then((response) => response.json())
        .then((data) => {
          this.tableState.pager = data;
        })
        .catch(function(response) {
          const message = 'Échec de récupération de la liste des domaines';
          addPermanentError(message, response.status);
        })
        .finally(hidePageLoading);
      },

      fetchDomainIds() {
        var queryString = this.buildQueryString();

        displayPageLoading();
        fetch(context.fetchIdsEndpoint, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: queryString
        })
        .then((response) => response.json())
        .then((data) => {
          data.forEach((id) => {
            this.tableState.selection[id] = true;
          })
        })
        .catch(function(response) {
          const message = 'Échec de récupération de la liste des identifiants de domaines';
          addPermanentError(message, response.status);
        })
        .finally(hidePageLoading);
      }
    }
  })
  .component('AgSortableColumnHeader', AgrosystVueComponents.Table.getSortableColumnHeader())
  .component('AgTableFooter', window.AgrosystVueComponents.Table.getFooter({
    entitiesLabel: 'generic-domain-table-footer-entities',
    selectAllLabel: 'generic-domain-table-footer-selectAll',
    selectionLabel: 'generic-domain-table-footer-selection',
    selectionEnabled: true
  }))
  .use(AgrosystVueComponents.i18n);
  //.use(Oruga.Input); Oruga messes up the layout of the entire page

  // <sort> and <header_label> must not be processed by Vue
  controller.config.compilerOptions.isCustomElement = (tag) => {
    return tag == 'sort' || tag == 'header_label';
  }
  return controller;
};
