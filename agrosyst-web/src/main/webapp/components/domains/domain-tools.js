/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainToolsController = function (data) {
  const controller = Vue.createApp({
    data() {
      return {
        domain: data.domain,
        forFrance: data.forFrance,
        frontApp: data.frontApp,
        downloadToolsUrl: data.downloadToolsUrl,

        equipments: null,
        toolsCouplings: null,
        toolsCouplingsInitList: null,
        toolsCouplingsUsageMap: null,

        toolEditionContext: null,
        materielTypesData: {
          materielType1ArrayManualTools: [], // for sorting purpose
          materielType1JsonManualTools: {}, // for edit purpose
          materielType1ArrayWithoutManualTools: [], // for sorting purpose
          materielType1JsonWithoutManualTools: {}
        },

        toolCouplingEditionContext: null,
        toolCouplingEditionData: {
          materielWorkRateUnits: I18N.MaterielWorkRateUnit,
          materielTransportUnits: I18N.MaterielTransportUnit,
          // Types d'actions ...
          agrosystInterventionTypes: I18N.AgrosystInterventionType,
          // ... et actions
          agrosystActionsFullList: null
        },
        isMaterialTabSelected: null,
        copyDialogContext: null,
        isCopyModalActive: false
      };
    },

    computed: {
      /**
       * L'ajout de combinaison d'outils doit être possible s'il y a:
       * - au moins un tracteur et au moins un outils ou un irrigation
       * - ou au moins un automoteur
       * sert à activer la partie 'combinaison d'outils'.
       */
      enableCoupling() {
        var tractorFound = false;
        var automoteurFound = false;
        var equipmentFound = false;
        if (this.equipments) {
          this.equipments.forEach(domainMateriel => {
            if (domainMateriel.refMateriel.type === 'TRACTEUR') {
              tractorFound = true;
            } else if (domainMateriel.refMateriel.type === 'AUTOMOTEUR') {
              automoteurFound = true;
            } else if (domainMateriel.refMateriel.type === 'IRRIGATION' || domainMateriel.refMateriel.type === 'OUTIL') {
              equipmentFound = true;
            }
          });
        }
        return (tractorFound && equipmentFound) || automoteurFound;
      }
    },

    watch: {
    },

    methods: {
      initMaterielToolsCouplings() {
        // materiel without refMateriel
        this.equipments.forEach(equipment => {
          if (!equipment.refMateriel) {
            equipment.refMateriel = { type: 'AUTRE' };
          }
          equipment.name0 = unaccent(equipment.name);
        });

        // init tool coupling
        this.toolsCouplings = [];

        if(this.toolsCouplingsInitList) {
          var usageMap = this.toolsCouplingsUsageMap || {};
          this.toolsCouplingsInitList.forEach(toolsCoupling => {

            toolsCoupling.toolsCouplingName0 = unaccent(toolsCoupling.toolsCouplingName);
            var usedToolsCoupling = usageMap[toolsCoupling.topiaId];
            var localToolsCoupling = cloneObject(toolsCoupling);
            delete localToolsCoupling.tractor;
            localToolsCoupling.equipments = [];
            localToolsCoupling.used = usedToolsCoupling;

            this.equipments.forEach(domainMateriel => {
              // mapping from the toolsCoupling server tractor object
              //   to the js object tractor
              if (toolsCoupling.tractor && domainMateriel.topiaId === toolsCoupling.tractor.topiaId) {
                localToolsCoupling.tractor = domainMateriel;
                if (usedToolsCoupling) {
                  domainMateriel.used = true;
                }
              }

              // mapping from the collection of toolsCoupling server equipments objects
              //   to the js's collection of equipments's objects
              toolsCoupling.equipments.forEach(toolsCouplingEquipment => {
                if (domainMateriel.topiaId === toolsCouplingEquipment.topiaId) {
                  localToolsCoupling.equipments.push(domainMateriel);
                  if (usedToolsCoupling) {
                    domainMateriel.used = true;
                  }
                }
              });
            });

            this.toolsCouplings.push(localToolsCoupling);
          });
        }
      },

      initMaterialTab() {
        if (!this.toolCouplingEditionData.agrosystActionsFullList) {
          displayPageLoading();
          let domainId = this.domain.topiaId && encodeURIComponent(this.domain.topiaId) || null;
          let filter = { filterEdaplos: !this.forFrance };
          return fetch(ENDPOINT_DOMAINS_EDIT_EQUIPMENTS_CONTEXT_JSON + "?domainId=" + domainId + "&filter=" + encodeURIComponent(JSON.stringify(filter)))
            .then(response => response.json())
            .then(data => {
              // clear next
              var DomainListMaterielType1Data = data["MaterielType"];
              var DomainListMaterielType1WithManualToolsData = data["ManualTools"];
              this.equipments = data["Equipment"];
              this.toolsCouplingsInitList = data["ToolsCoupling"];
              this.toolsCouplingsUsageMap = data["UsageList"];

              Object.keys(DomainListMaterielType1Data).forEach(key => {
                DomainListMaterielType1Data[key].forEach(value => {
                  let m = { materielType1: value.left, materielType1Translated: value.right, type: key };
                  if (!this.materielTypesData.materielType1JsonWithoutManualTools[key]) {
                    this.materielTypesData.materielType1JsonWithoutManualTools[key] = {};
                  }
                  this.materielTypesData.materielType1JsonWithoutManualTools[key][value.left] = m;
                  this.materielTypesData.materielType1ArrayWithoutManualTools.push(m);
                });
              });

              Object.keys(DomainListMaterielType1WithManualToolsData).forEach(key => {
                DomainListMaterielType1WithManualToolsData[key].forEach(value => {
                  let m = { materielType1: value.left, materielType1Translated: value.right, type: key };
                  if (!this.materielTypesData.materielType1JsonManualTools[key]) {
                    this.materielTypesData.materielType1JsonManualTools[key] = {};
                  }
                  this.materielTypesData.materielType1JsonManualTools[key][value.left] = m;
                  this.materielTypesData.materielType1ArrayManualTools.push(m);
                });
              });

              this.initMaterielToolsCouplings();

              return fetch(ENDPOINT_LOAD_ALL_ACTIVE_AGROSYST_ACTIONS_JSON)
                .then(response => response.json())
                .then(data => this.toolCouplingEditionData.agrosystActionsFullList = data)
                .catch(function(response) {
                  console.error("Can't load agrosystActionsFullList", response);
                  var message = "Échec de récupération des actions Agrosyst disponible pour la combinaison d'outils";
                  addPermanentError(message, response.status);
                  hidePageLoading();
                });

            })
            .catch(function(response) {
              console.error("Can't load equipments", response);
              var message = "Échec de récupération des équipements";
              addPermanentError(message, response.status);
            })
            .finally(hidePageLoading);

        }
      },

      sortedEquipments(materielType) {
        return this.equipments
              && Array.from(this.equipments)
                .filter(equipement => equipement.refMateriel.type == materielType)
                .sort((e1, e2) => e1.name0.localeCompare(e2.name0))
            || [];
      },

      sortedToolsCouplings(manual) {
        return this.toolsCouplings
              && Array.from(this.toolsCouplings)
                .filter(tc => tc.manualIntervention == manual)
                .sort((tc1, tc2) => tc1.toolsCouplingName0.localeCompare(tc2.toolsCouplingName0))
            || [];
      },

      sortedManualToolsCouplings() {
        return this.toolsCouplings
              && Array.from(this.toolsCouplings)
                .filter(tc => !!tc.manualIntervention)
                .sort((tc1, tc2) => tc1.toolsCouplingName0.localeCompare(tc2.toolsCouplingName0))
            || [];
      },

      toolsCouplingEquipmentNames(toolsCoupling) {
        return toolsCoupling.equipments.map(eq => eq.name).join(", ");
      },

      booleanToYesNo(value) {
        return this.$t('messages.common-' + (value ? 'yes' : 'no'));
      },

      addMateriel(materielType) {
        let editedMateriel = {
          topiaId: "NEW-EQUIPMENT-" + new Date().getTime(),
          materielType: materielType,
          refMateriel: {},
          _newMateriel: true
        };
          //TODO
          //this.addDomainMaterielForm.$setPristine();
          //this.addDomainMaterielForm.$setUntouched();

        this._editMateriel(editedMateriel);
      },

      _editMateriel(editedMateriel, materielType1Row, uniteRow, displayManualTool) {
        this.toolEditionContext = {
          editedMateriel,
          materielType1Row,
          uniteRow,
          displayManualTool,
          startEdit: true,
          callback: this.saveMateriel,
          close: () => this.toolEditionContext = null
        };
      },

      _bindMaterielData(materiel) {
        var materielId = materiel.refMateriel.topiaId;
        var type = materiel.refMateriel.type;

        displayPageLoading();
        return fetch(ENDPOINT_DOMAINS_MATERIELS_FIND_MATERIEL+ "?materielId=" + encodeURIComponent(materielId))
          .then(response => response.json())
          .then(data => {
            materiel.refMateriel = data;
            materiel.refMateriel.type = type;
            //TODO
            //this.addDomainMaterielForm.$setPristine();
          })
          .catch(function(response) {
            console.error("Can't get equipment", response);
            var message = "Échec de récupération du matériel";
            addPermanentError(message, response.status);
          })
          .finally(hidePageLoading);
      },

      saveMateriel(materiel) {
        if (materiel._newMateriel) { // creation
          materiel.name0 = unaccent(materiel.name);
          materiel._newMateriel = false;
          if (materiel.refMateriel.type != 'AUTRE') {
            this._bindMaterielData(materiel);
          }
          this.equipments.push(materiel);

        } else {
          let originalMateriel = this.equipments.find(eq => eq.topiaId === materiel.topiaId);
          // equipment specific
          originalMateriel.materielETA = materiel.materielETA;
          originalMateriel.weakenedMaterial = materiel.weakenedMaterial;
          originalMateriel.homemadeMaterial = materiel.homemadeMaterial;
          originalMateriel.jerryRigged = materiel.jerryRigged;
          originalMateriel.name        = materiel.name;
          originalMateriel.name0       = unaccent(materiel.name);
          originalMateriel.description = materiel.description;
          originalMateriel.realUsage   = materiel.realUsage;

          if (materiel.refMateriel.type != 'AUTRE') {
            this._bindMaterielData(materiel).then(() => originalMateriel.refMateriel = materiel.refMateriel);
          } else {
            // refMateriel specific
            originalMateriel.refMateriel = materiel.refMateriel;
          }
        }
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
        this.$forceUpdate();
      },

      // suppression d'un materiel lors d'un clic sur le bouton de suppression d'un materiel
      deleteMateriel(element) {
        // A confirmation message is display when the user want to delete a material.
        // When deleted a materel, if the materiel is part of a tools coupling it can alse destroy it (if it's a tractor or if it's the last elements of the coupling)
        var toolsCouplingToDelete = [];
        if (!!this.toolsCouplings) {
          this.toolsCouplings.forEach(toolsCoupling => {
            // si c'est un tracteur automoteur
            if (element === toolsCoupling.tractor) {
              toolsCouplingToDelete.push(toolsCoupling);
            }
            // si c'est un equipement du toolCoupling
            if (toolsCoupling.equipments.indexOf(element) != -1) {
              toolsCouplingToDelete.push(toolsCoupling);
            }
          });
        }

        let message = this.$t("messages.domain-edit-tools-materiel-delete-confirm", [ element.name ]);
        if (toolsCouplingToDelete.length > 0) {
          const couplings = toolsCouplingToDelete.map(coupling => " - " + coupling.toolsCouplingName).join("\n");
          message = this.$t("messages.domain-edit-tools-materiel-delete-couplings-confirm", [ couplings ]) + message;
        }

        // Delete materiel and tools coupling if confirm.
        if (window.confirm(message)) {
          // we delete all the concerned tools couplings.
          toolsCouplingToDelete.forEach(toolsCoupling => {
            var indexOf = this.toolsCouplings.indexOf(toolsCoupling);
            this.toolsCouplings.splice(indexOf, 1);
          });

          // The materiel is removed from the domain materiels.
          var indexOf = this.equipments.indexOf(element);
          this.equipments.splice(indexOf, 1);
          (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
        }
      },

      editMateriel(element) {
        this.loadingMateriel = true;

        let filterOnManualTool = !!element.refMateriel.petitMateriel;
        let materielType1Json = filterOnManualTool ?
            this.materielTypesData.materielType1JsonManualTools :
            this.materielTypesData.materielType1JsonWithoutManualTools;

        if (element.refMateriel.type != 'AUTRE') {
          var filter = {
            ...element.refMateriel,
            filterOnManualTool,
          }
          delete this.materielType2Array;
          delete this.materielType3Array;
          delete this.materielType4Array;

          displayPageLoading();

          return fetch(ENDPOINT_DOMAINS_EDIT_MATERIEL_ALL_JSON + "?filter=" + encodeURIComponent(JSON.stringify(filter)))
            .then(response => response.json())
            .then(data => {

              this.materielType2Array = data[0].map(t => t.left);
              this.materielType3Array = data[1].map(t => t.left);
              this.materielType4Array = data[2].map(t => t.left);
              this.uniteArray = [];
              var materiel = cloneObject(element);
              materiel.refMateriel.typeMateriel2 = this.materielType2Array[this.materielType2Array.indexOf(materiel.refMateriel.typeMateriel2)];
              materiel.refMateriel.typeMateriel3 = this.materielType3Array[this.materielType3Array.indexOf(materiel.refMateriel.typeMateriel3)];
              materiel.refMateriel.typeMateriel4 = this.materielType4Array[this.materielType4Array.indexOf(materiel.refMateriel.typeMateriel4)];
              materiel.materielType = element.refMateriel.type;

              let unites = data[3];
              let uniteRow = null;
              Object.keys(unites).forEach(key => {
                let uniteParAn = unites[key][0].uniteParAn;
                let unite = unites[key][0].unite;
                let uniteTranslated =  unites[key][0].unitTranslated;
                let row = { materielTopiaId: key,
                            uniteParAn: uniteParAn,
                            unite: unite,
                            uniteTranslated: uniteTranslated};
                this.uniteArray.push(row);
                if (uniteParAn == materiel.refMateriel.uniteParAn) {
                    uniteRow = row;
                }
              });

              let materielType1Row = materielType1Json[(materiel.refMateriel.type)][(materiel.refMateriel.typeMateriel1)];
              this.loadingMateriel = false;
              this._editMateriel(materiel, materielType1Row, uniteRow, filterOnManualTool);
          })
          .catch(function(response) {
            this.loadingMateriel = false;
            delete this.materielType2Array;
            delete this.materielType3Array;
            delete this.materielType4Array;
            console.error("Can't get all equipments", response);
            var message = "Échec de récupération de l'ensemble des matériels";
            addPermanentError(message, response.status);
          })
          .finally(hidePageLoading);

        } else {
          let materielType1Row = materielType1Json.AUTRE;
          this.loadingMateriel = false;
          this._editMateriel(element, materielType1Row, null, filterOnManualTool);
        }
      },

      addToolsCoupling(manual) {
        this.toolCouplingEditionContext = {
          equipments: this.equipments,
          index : -1,
          manualIntervention: manual,
          _newCoupling: true,
          toolCouplingNames: this.toolsCouplings.map(tc => tc.toolsCouplingName),
          callback: this.saveCoupling,
          close: () => this.toolCouplingEditionContext = null
        };
      },

      editToolsCoupling(toolsCoupling) {
        let toolsCouplingToEdit = cloneObject(toolsCoupling);
        if (toolsCouplingToEdit.manualIntervention && toolsCouplingToEdit.workRate && toolsCouplingToEdit.workRate !== 0){
          // set 3 digits after dot
          toolsCouplingToEdit.revertWorkRate = parseInt((1 / toolsCouplingToEdit.workRate) * 1000)/1000;
        }
        if (toolsCouplingToEdit.tractor) {
          toolsCouplingToEdit.tractor = this.equipments.find(eq => eq.topiaId == toolsCouplingToEdit.tractor.topiaId)
        }
        let selectedEquipments = [];
        toolsCouplingToEdit.equipments.forEach(eq => {
          let selectedEquipment = this.equipments.find(eq2 => eq2.topiaId == eq.topiaId);
          selectedEquipments.push(selectedEquipment);
        });
        toolsCouplingToEdit.equipments = selectedEquipments;

        this.toolCouplingEditionContext = {
          equipments: this.equipments,
          index: this.toolsCouplings.indexOf(toolsCoupling),
          toolsCouplingToEdit,
          toolCouplingNames: this.toolsCouplings.filter(tc => tc != toolsCoupling).map(tc => tc.toolsCouplingName),
          callback: this.saveCoupling,
          close: () => this.toolCouplingEditionContext = null
        };
      },

      saveCoupling(index, editedToolsCoupling) {
        // for value that came from referential and that not get through angular ag-float
        if (editedToolsCoupling.workRate && isNaN(editedToolsCoupling.workRate)) {
          var workRate = editedToolsCoupling.workRate;
          editedToolsCoupling.workRate = parseFloat(workRate.replace(',', '.'));
        }
        if (editedToolsCoupling.transitVolume && isNaN(editedToolsCoupling.transitVolume)) {
          var transitVolume = editedToolsCoupling.transitVolume;
          editedToolsCoupling.transitVolume = parseFloat(transitVolume.replace(',', '.'));
        }

        editedToolsCoupling.toolsCouplingName0 = unaccent(editedToolsCoupling.toolsCouplingName);

        if (index == -1) {
          this.toolsCouplings.push(editedToolsCoupling);
        } else {
          this.toolsCouplings[index] = editedToolsCoupling;
        }
      },

      deleteToolsCoupling(toolsCoupling) {
        var index = this.toolsCouplings.indexOf(toolsCoupling);
        if (index != -1 && window.confirm(this.$t("messages.domain-edit-tools-coupling-delete-confirm"))) {
          this.toolsCouplings.splice(index, 1);
        }
      },

      confirmDomainTargetsToPaste(selectedDomains) {
        var fromDomain = this.domain.topiaId;
        var toOtherDomains = [];
        var toSameDomain = false;
        selectedDomains.forEach(domain => {
          if (domain === fromDomain) {
            toSameDomain = true;
          } else {
            toOtherDomains.push(domain);
          }
        });
        // process copy
        this.processToolsCouplingAndEquipmentsCopy(toSameDomain, toOtherDomains);
      },

      toSameDomainCopy() {
        if (this.equipments && this.equipments.length > 0) {
          var equipementOrigineToCopy = {};
          this.equipments.forEach(equipment => {
            var copy = cloneObject(equipment);
            delete copy.topiaId;
            delete copy.code;

            copy.used = false;
            copy.topiaId = "NEW-EQUIPMENT-" + new Date().getTime();
            equipementOrigineToCopy[equipment.topiaId] = copy;
          });

          Object.values(equipementOrigineToCopy).forEach(equipment => {
            this.equipments.push(equipment);
          });

          if (this.copyDialogContext.includeToolsCouplings && this.toolsCouplings.length > 0) {
            var tcOriginalToCopy = [];
            this.toolsCouplings.forEach(toolsCoupling => {
              var tcCopy = cloneObject(toolsCoupling);
              delete tcCopy.domain;
              delete tcCopy.topiaId;
              delete tcCopy.code;

              tcCopy.used = false;
              if (toolsCoupling.tractor) {
                tcCopy.tractor = equipementOrigineToCopy[toolsCoupling.tractor.topiaId];
              }
              tcCopy.equipments = [];
              toolsCoupling.equipments.forEach(originalEquipement => {
                tcCopy.equipments.push(equipementOrigineToCopy[originalEquipement.topiaId]);
              });

              tcOriginalToCopy.push(tcCopy);
            });

            tcOriginalToCopy.forEach(toolsCoupling => {
              this.toolsCouplings.push(toolsCoupling);
            });

          }

          var tcPluralize = this.toolsCouplings.length > 1 ? "les combinaisons d'outils ont" : "la combinaison d'outils a";
          var message = (this.copyDialogContext.includeToolsCouplings && this.toolsCouplings.length>0) ? "Le matériel / " + tcPluralize + " bien été copiés vers ce même domaine, " : "Le matériel a bien été copiés vers ce même domaine :";
          addSuccessMessage(message + this.domain.name);
          (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
        }
      },

      // copy/past toolsCoupling an tools
      copyTools() {
        // init
        this.filter = {};
        this.filter.active = true;
        // TODO passer les données des différents éléments (combinaisons d'outils, matériels etc) dans le contexte ?
        //      quelle est l'approche pour la copie des intrants ? -> en param du composant : `domains-edit-input-inputs.jsp`
        //      cf domains-edit-input-copy-inputs-modal.js l. 151
        this.copyDialogContext = {
          title: this.$t('messages.domain-edit-cropping-plan-table-copy-paste'),
          type: "TOOLS",
          includeToolsCouplings: true,
          callback: this.confirmDomainTargetsToPaste,
          close: () => this.copyDialogContext = null
        };
      },
      
      openCopyModal() {
        this.isCopyModalActive = true;
      },

      closeCopyModal() {
        this.isCopyModalActive = false;
      },

      // Méthode qui réalise la copie
      processToolsCouplingAndEquipmentsCopy(toSameDomain, toOtherDomains) {
        if (toOtherDomains.length > 0){
          // TODO changer pour utiliser les toolsCoupling et equipments sélectionnés
          var ajaxRequest = "fromDomain=" + encodeURIComponent(this.domain.topiaId) +
                            "&toolsCouplingsJson=" + encodeURIComponent(JSON.stringify(this.toolsCouplings)) +
                            "&equipmentsJson=" + encodeURIComponent(JSON.stringify(this.equipments)) +
                            "&toDomains=" + JSON.stringify(toOtherDomains) +
                            "&includeToolsCouplings=" + this.copyDialogContext.includeToolsCouplings;
          let self = this;
          fetch(ENDPOINTS.domainMaterielsCopy, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: ajaxRequest
          })
          .then(response => response.json())
          .then(data => {
            if (data.length > 0) {
              var domainPluralize = self.$tc("messages.domain-edit-tools-copy-paste-success-domain", data.length);
              var targetedDomain = data.join(", ") + ".";
              var message = self.$t("messages.domain-edit-tools-copy-paste-success", [domainPluralize, targetedDomain]);
              // make sure copy on same domain is done after copy to other domains
              if (toSameDomain) {
                this.toSameDomainCopy();
              }
              addSuccessMessage(message);
            }
          })
          .catch(function(response) {
            let messageKey = "messages.domain-edit-tools-copy-paste-error";
            if (response.data === '"IllegalArgumentException"') {
              messageKey += "-invalid";
            }
            let message = self.$t(messageKey)
            console.error(message, response);
            addPermanentError(message, response.status);
          });
        } else if (toSameDomain) {
          // make sure copy on same domain is done after copy to other domains
          this.toSameDomainCopy();
        }
      },
    }
  })
  .provide('context', data)
  .component('GenericModal', AgrosystVueComponents.Common.getGenericModal())
  .component('DomainsToPaste', AgrosystVueComponents.Domains.getDomainsToPaste({
    domainTypes: data.domainTypes
  }))
  .component('DomainToolsCopyModal', AgrosystVueComponents.Domains.getDomainToolsCopyModal())
  .component('DomainsToolEdition', AgrosystVueComponents.Domains.getToolEdition())
  .component('DomainsToolCouplingEdition', AgrosystVueComponents.Domains.getToolCouplingEdition())
  .use(AgrosystVueComponents.i18n)
  .use(Oruga.Config, { iconPack: "fa", mobileBreakpoint: "480px" })
  .use(Oruga.Autocomplete)
  .use(Oruga.Button)
  .use(Oruga.Checkbox)
  .use(Oruga.Field)
  .use(Oruga.Icon)
  .use(Oruga.Input)
  .use(Oruga.Modal)
  .use(Oruga.Notification)
  .use(Oruga.Radio)
  .use(Oruga.Select)
  .use(Oruga.Switch)
  .use(Oruga.Collapse);

  return controller;
}


