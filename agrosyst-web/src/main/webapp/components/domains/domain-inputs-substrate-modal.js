/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsSubstrateModal = function(data) {
  return {
    template: '#domain-inputs-substrate-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    mounted() {
      if (!this.substrates.length) {
        this.fetchSubstrates();
      }
      this.updateObjectId();
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },

        substrates: [],
        substrate: undefined,
        loading: 0,

        priceStepValid: !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit),

        // step définition
        characteristic1: this.selectedInput.characteristic1,
        characteristic2: this.selectedInput.characteristic2,
        characteristic1Translated: this.selectedInput.characteristic1Translated,
        characteristic2Translated: this.selectedInput.characteristic2Translated,
        refInputId: this.selectedInput.refInputId,
        inputName: this.selectedInput.inputName,
        usageUnit: this.selectedInput.usageUnit,

        // step prix
        price: this.selectedInput.price ? this.selectedInput.price.price : undefined,
        priceUnit: this.selectedInput.price ? this.selectedInput.price.priceUnit : 'EURO_M3',
        objectId: this.selectedInput.price ? this.selectedInput.price.objectId : undefined
      };
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },

    validations() {
      return {
        // step définition
        characteristic1Translated: { required: VuelidateValidators.required, $autoDirty: true },
        characteristic2Translated: { required: VuelidateValidators.required, $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },
      }
    },

    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      firstCharacteristics() { return Object.keys(this.substrates); },
      secondCharacteristics() {
        return this.substrates && this.characteristic1Translated && this.substrates[this.characteristic1Translated] ? this.substrates[this.characteristic1Translated].map((substrate) => substrate.characteristic2Translated) : [];
      },
      substrateInputUnits() {
        if (this.substrate) {
          if (this.substrate.unitType === 'VOLUME') {
            return ['M3_HA', 'L_M2', 'L_POT'];
          }
          if (this.substrate.unitType === 'MASS') {
            return ['KG_HA', 'T_HA', 'G_M2', 'KG_M2', 'G_POT', 'KG_POT'];
          }
        }
        return [];
      },

      // step définition
      isFirstStepValid() {
        return this.isFieldValid('characteristic1Translated') && this.isFieldValid('inputName') && this.isFieldValid('usageUnit');
      },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.priceStepValid;
        }
        return false;
      }
    },

    watch: {
      selectedInput(newInput) {
        // step définition
        this.characteristic1 = newInput.characteristic1;
        this.characteristic1Translated = newInput.characteristic1Translated;
        this.characteristic2 = newInput.characteristic2;
        this.characteristic2Translated = newInput.characteristic2Translated;
        this.updateObjectId();
        this.refInputId = newInput.refInputId;
        this.inputName = newInput.inputName;
        this.usageUnit = newInput.usageUnit;

        // step prix
        this.priceStepValid = !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit);
        this.domainId = this.context.domainId;
        this.campaign = this.context.campaign;
        this.campaigns = this.context.relatedDomains;
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = undefined;
        }
        this.v$.$reset();
      },

      characteristic1Translated() {
        this.characteristic2 = undefined;
        this.characteristic2Translated = undefined;
        this.substrate = undefined;
        this.refInputId = undefined;
        this.objectId = undefined;
        this.inputName = undefined;
        this.usageUnit = undefined;
        this.v$.$reset();
      },

      characteristic2Translated() {
        this.onCharacteristic2Changed()
      },
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      resetForm() {
        this.v$.$reset();
        this.stepProgress.step = 0;
      },

      updateObjectId() {
        if (this.characteristic1 && this.characteristic2) {
          const label = this.characteristic1 + ' - ' + this.characteristic2;
          this.objectId = this.$filters.removeAccents(label);
        }
      },

      fetchSubstrates() {
        this.loading++;
        displayPageLoading();

        fetch(this.context.loadRefSubstratesJson, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.substrates = data;
            if (this.characteristic1Translated && this.characteristic2Translated) {
              this.substrate = this.foundSubstrate(this.characteristic1Translated, this.characteristic2Translated);
              this.characteristic1 = this.substrate ? this.substrate.characteristic1 : undefined;
              this.characteristic2 = this.substrate ? this.substrate.characteristic2 : undefined;
            }
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste de référence des substrats';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          });
      },

      onCharacteristic2Changed() {
        if (this.characteristic2Translated) {
          this.substrate = this.foundSubstrate(this.characteristic1Translated, this.characteristic2Translated);
          this.refInputId = this.substrate ? this.substrate.topiaId : undefined;
          this.characteristic1 = this.substrate ? this.substrate.characteristic1 : undefined;
          this.characteristic2 = this.substrate ? this.substrate.characteristic2 : undefined;
          this.characteristic1Translated = this.substrate ? this.substrate.characteristic1Translated : undefined;
          this.characteristic2Translated = this.substrate ? this.substrate.characteristic2Translated : undefined;
          const objectIdLabel = this.characteristic1 + ' - ' + this.characteristic2;
          this.objectId = this.$filters.removeAccents(objectIdLabel);
          const label = this.characteristic1Translated + ' - ' + this.characteristic2Translated;

          this.inputName = label;
        }
      },

      foundSubstrate(characteristic1Translated, characteristic2Translated) {
        const substrates = this.substrates[characteristic1Translated].filter((substrate) => substrate.characteristic2Translated === characteristic2Translated);
        return substrates.length ? substrates[0] : undefined;
      },

      save() {
        const input = {
          ...this.selectedInput,
          inputType: 'SUBSTRAT',

          // step définition
          characteristic1: this.characteristic1,
          characteristic2: this.characteristic2,
          characteristic1Translated: this.characteristic1Translated,
          characteristic2Translated: this.characteristic2Translated,
          refInputId: this.refInputId,
          inputName: this.inputName,
          usageUnit: this.usageUnit,
        };
        // step prix
        if (!isNaN(parseFloat(this.price)) || this.priceUnit !==undefined) {
          input.price = {
            ...(this.selectedInput.price ? this.selectedInput.price : {}),
            category: 'SUBSTRATE_INPUT',
            price: this.price,
            priceUnit: this.priceUnit,
            displayName: this.characteristic1Translated + ' - ' + this.characteristic2Translated,
            sourceUnit: this.usageUnit
          }
        } else {
          input.price = undefined;
        }
        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },
    }
  };
};
