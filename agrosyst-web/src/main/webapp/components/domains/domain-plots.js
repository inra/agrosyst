/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainPlotsController = function (data) {
  const controller = Vue.createApp({
    data() {
      return {
        domain: data.domain,
        forFrance: data.forFrance,
        frontApp: data.frontApp,
        asyncThreshold: data.plotAsyncThreshold,
        newPlotLink: data.newPlotLink,
        editPlotUrl: data.editPlotUrl,
        editGrowingSystemUrl: data.editGrowingSystemUrl,
        plots: [],
        // {Map} selected plot ids and selection state
        selectedPlots: {},
        allSelectedPlots: {
          selected : false
        },
        filter: {
          active: true
        },
        isInactivePlotSelected: false,
        isActivePlotSelected: false
      };
    },
    computed: {
      filteredPlots() {
        return this.plots.filter(plot => this.filter.active === null || plot.active === this.filter.active)
      },
      selectedPlotsNb() {
        return Object.values(this.selectedPlots).filter(plot => !!plot).length;
      },
      validSelectedPlots() {
        var gsIds = Object.values(this.selectedPlots).filter(gsId => !!gsId);
        if (gsIds.length > 1) {
          var gsIdRef = gsIds[0];
          return gsIds.every(gsId => gsIdRef === gsId);
        }
        return false;
      }
    },

    watch: {
      "filter.active"() {
        this.allSelectedPlots.selected = false;
        this.toggleSelectedPlots();
      },
      "allSelectedPlots.selected"() {
        this.toggleSelectedPlots();
      }
    },

    methods: {
      intiPlotsTab() {
        if (!this.plots.length) {
          displayPageLoading();
          let domainId = !this.domain.topiaId ? null : encodeURIComponent(this.domain.topiaId);
          const self = this;
          return fetch(ENDPOINT_DOMAINS_LOAD_PLOTS + "?domainId=" + domainId)
              .then(response => response.json())
              .then(data => self.plots = data)
              .catch(function(response) {
                console.error("Can't load plots", response);
                addPermanentError("Échec de récupération des parcelles", response.status);

              })
              .finally(hidePageLoading);
        }
      },

      toggleSelectedPlots() {
        this.isInactivePlotSelected = false;
        this.isActivePlotSelected = false;
        Object.keys(this.selectedPlots).forEach(plotId => this.selectedPlots[plotId] = null);
        if (this.allSelectedPlots.selected) {
          this.filteredPlots.forEach(plot => {
            this.selectedPlots[plot.topiaId] = plot.growingSystem ? plot.growingSystem.topiaId : '-';
            this.isActivePlotSelected = plot.active || this.isActivePlotSelected;
            this.isInactivePlotSelected = !plot.active || this.isInactivePlotSelected;
          });
        }
      },
      
      toogleSelectedPlot() {
        this.isActivePlotSelected = false;
        this.isInactivePlotSelected = false;
        this.filteredPlots.forEach(plot => {
          if (this.selectedPlots[plot.topiaId] && plot.active) {
            this.isActivePlotSelected = true;
          } else if (this.selectedPlots[plot.topiaId] && !plot.active) {
            this.isInactivePlotSelected = true;
          }
        });
      },

      _unactivateOrActivatePlot(plotIds, isActivated) {
        const message = this.$tc("messages.domain-edit-plots-confirm-" + (isActivated ? "activate" : "deactivate"), plotIds.length);
        if (window.confirm(message)) {
          displayPageLoading();
          fetch(ENDPOINTS.plotsUnactivate, {
            method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
              },
              body: "jsonPlotTopiaIds=" + JSON.stringify(plotIds) +"&activate=" + isActivated
            })
            .then(response => {
              this.plots.forEach(plot0 => {
                if (plotIds.indexOf(plot0.topiaId) !== -1) {
                  plot0.active = isActivated;
                }
              });
              this.allSelectedPlots = { selected : false };
              this.toggleSelectedPlots();
            })
            .catch(response => {
              console.error("Échec d" + (isActivated ? "'" : "de dés") + "activation de la parcelle", response);
              addPermanentError("Échec d" + (isActivated ? "'" : "de dés") + "activation de la parcelle", response.status);
            })
            .finally(hidePageLoading);
        }
      },

      unactivatePlot(index, plot) {
        var plotId = encodeURIComponent(plot.topiaId);
        this._unactivateOrActivatePlot([ plotId ], false);
      },
      
      activatePlots(isToActivate) {
        let plotsIds = Object.keys(this.selectedPlots).filter(plotId => this.selectedPlots[plotId]);
        if (plotsIds.length > 0) {
          let plotsToChange = plotsIds.map(encodeURIComponent);
          this._unactivateOrActivatePlot(plotsToChange, isToActivate);
        }
      },
      
      activatePlot(index, plot) {
        var plotId = encodeURIComponent(plot.topiaId);
        this._unactivateOrActivatePlot([ plotId ], true);
      },

      exportPlotsXls() {
        let plotsIds = Object.keys(this.selectedPlots).filter(plotId => !!plotId);
        if (plotsIds.length > 0) {
          if (plotsIds.length >= this.asyncThreshold) {
            this.exportPlotsXlsAsync(plotsIds);
            return;
          }
          var data = "jsonPlotTopiaIds=" + encodeURIComponent(JSON.stringify(plotsIds));
          fetch(ENDPOINTS.plotsExport, {
            method: 'POST',
            headers: {
              responseType:'arraybuffer',
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: data
          })
          .then(response => response.blob())
          .then(file => {
            var fileName = "parcelle-export.xlsx";

            var isFirefox = typeof InstallTrigger !== 'undefined';
            var isChromium = window.chrome;
            var winNav = window.navigator;
            var vendorName = winNav.vendor;
            var isOpera = typeof window.opr !== "undefined";
            var isEdge = winNav.userAgent.indexOf("Edge") > -1;
            var isIE = /*@cc_on!@*/false || !!document.documentMode;
            var isIOSChrome = winNav.userAgent.match("CriOS");

            var throwDownloadLink = isIOSChrome ||
              (isChromium !== null && typeof isChromium !== "undefined" && vendorName === "Google Inc." && isOpera === false && isEdge === false) ||
              isFirefox;

            if (throwDownloadLink) {
               // is Google Chrome on IOS
              var url = URL || window.URL || window.webkitURL;
              var downloadLink = angular.element('<a></a>');
              downloadLink.attr('href',url.createObjectURL(file));
              downloadLink.attr('target','_self');
              downloadLink.attr('download', fileName);
              downloadLink[0].click();

            } else if(isEdge || isIE){
                window.navigator.msSaveOrOpenBlob(file, fileName);

            } else {
                var fileURL = URL.createObjectURL(file);
                window.open(fileURL);
            }
          })
          .catch(response => {
            console.error("Échec d'export des parcelles", response);
            addPermanentError("Échec d'export des parcelles", response.status);
          });
        }
      },

      exportPlotsXlsAsync(plotsIds) {
        fetch(ENDPOINTS.plotsExportAsync, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: "jsonPlotTopiaIds=" + encodeURIComponent(JSON.stringify(plotIds))
        })
        .then(response => {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(response => {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
      },

      duplicatePlot(plot) {
        const message = this.$t("messages.domain-edit-plots-confirm-duplicate");
        if (window.confirm(message)) {
          displayPageLoading();
          fetch(ENDPOINTS.plotsDuplicate, {
            method: 'POST',
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
              },
              body: "plotTopiaId=" + encodeURIComponent(plot.topiaId)
            })
            .then(response => response.json())
            .then(data => {
              this.plots.push(data);
              addSuccessMessage(this.$t("messages.domain-edit-plots-duplication-success", [ data.name ]));
            })
            .catch(response => {
              const errorMessage = this.$t("messages.domain-edit-plots-duplication-error");
              console.error(errorMessage, response);
              addPermanentError(errorMessage, response.status);
            })
            .finally(() => {
              hidePageLoading();
          });
        }
      },

      mergePlots() {
        const message = this.$t("messages.domain-edit-plots-confirm-merge");
        const plotsIds = Object.keys(this.selectedPlots);
        if (plotsIds.length > 1 && window.confirm(message)) {
          fetch(ENDPOINTS.plotsMerge, {
          method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: "jsonPlotTopiaIds=" + encodeURIComponent(JSON.stringify(plotsIds))
          })
          .then(response => response.json())
          .then(data => {
            plotsIds.forEach(plotToMerge => {
              let index = this.plots.indexOf(plotToMerge);
              this.plots.splice(index, 1); // on supprime ceux qui ont étés mergés
            })
            if (data) {
              this.plots.push(data);// ajout de la parcelle fusionnée
            }
          })
          .catch(response => {
            const errorMessage = this.$t("messages.domain-edit-plots-merge-error");
            console.error(errorMessage, response);
            addPermanentError(errorMessage, response.status);
          })
          .finally(() => {
            hidePageLoading();
          })
        }
      }
    }
  })
  .component('GenericModal', AgrosystVueComponents.Common.getGenericModal())
  .use(AgrosystVueComponents.i18n)
  .use(Oruga.Config, { iconPack: "fa", mobileBreakpoint: "480px" })
  .use(Oruga.Icon)
  .use(Oruga.Button)
  .use(Oruga.Checkbox)
  .use(Oruga.Field)
  .use(Oruga.Input)
  .use(Oruga.Select)
  .use(Oruga.Modal);

  return controller;
}
