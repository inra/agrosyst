/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

// Référencé par src/main/webapp/WEB-INF/content/domains/domains-edit-input.jsp
// Fait:
// * Modal de copier/coller d'un intrant dans le local à intrants vers un autre domaine
window.AgrosystVueComponents.Domains.getDomainsEditInputCopyInputsModal = () => {
  return {
    inject: ['context'],

    props: {
      domainInputs: Array,
    },

    components: {
      AgTableFooter: window.AgrosystVueComponents.Table.getFooter({
        entitiesLabel: AgrosystVueComponents.Domains.frontApp === 'ipmworks' ? ' common-domain-table-footer-entities' : 'agrosyst-common-domain-table-footer-entities',
        selectAllLabel: AgrosystVueComponents.Domains.frontApp === 'ipmworks' ? 'common-domain-table-footer-selectAll' : 'agrosyst-common-domain-table-footer-selectAll',
        selectionLabel: AgrosystVueComponents.Domains.frontApp === 'ipmworks' ? 'common-domain-table-footer-selection' : 'agrosyst-common-domain-table-footer-selection'
      })
    },

    emits: ['close', 'copy'],

    template: '#domainsEditInputCopyInputsModalTemplate',

    mounted() {
      this.selectAll();
      this.loadDomains();
    },

    data() {
      let domainsTableState = AgrosystVueComponents.Table.usePaginatedTableMixin(
        {
          count: 0,
          currentPage: 0
        },
        {
          domainName: null,
          campaign: null,
          type: null,
          responsable: null,
          departement: null,
          active: true
        },
        this.loadDomains.bind(this),
        this.selectAllDomains.bind(this)
      );

      return {
        firstStep: 1,
        lastStep: 2,
        activeStep: 1,

        // copy
        forceCopy: false,
        selectedInputTypes: [],
        selectedInputIds: [],
        selectedWorkforceAndOperationInput: true,

        // paste
        domains: [],
        timer: 0,
        ...domainsTableState
      }
    },

    computed: {
      filteredDomainInputs() {
        return this.domainInputs ? this.domainInputs.filter((input) => input.topiaId && !input.deprecated) : [];
      },

      filteredWorkforceAndOperationInputs() {
        var result = [];
        this.workforceAndOperationInputTypes.map(inputType => {
          this.domainInputs.filter((input) => input.inputType === inputType)
            .map(input => result.push(input))
          }
        )
        return result;
      },

      inputTypes() {
        return Object.keys(this.context.i18n.InputType)
          .filter((type) => type !== 'TRAITEMENT_SEMENCE' &&
                            type !== 'PLAN_COMPAGNE' &&
                            type !== 'IRRIGATION' &&
                            type !== 'CARBURANT' &&
                            type !== 'MAIN_OEUVRE_MANUELLE' &&
                            type !== 'MAIN_OEUVRE_TRACTORISTE');
      },

      workforceAndOperationInputTypes() {
        return Object.keys(this.context.i18n.InputType)
          .filter((type) => type == 'IRRIGATION' ||
                            type == 'CARBURANT' ||
                            type == 'MAIN_OEUVRE_MANUELLE'  ||
                            type == 'MAIN_OEUVRE_TRACTORISTE');
      },

      domainTypes() {
        return Object.keys(this.context.i18n.DomainType);
      },

      filteredSelectedDomains() {
        return Object.keys(this.tableState.selection).filter(key => !!this.tableState.selection[key]);
      },

      selectedDomainsNb() {
        return Object.values(this.tableState.selection).filter(value => !!value).length;
      },

      // step copy
      isFirstStepValid() { return this.selectedInputIds.length > 0; },

      // step paste
      isLastStepValid() { return this.filteredSelectedDomains.length > 0; },

      isStepValid() {
        if (this.activeStep === this.firstStep) {
          return this.isFirstStepValid;
        } else if (this.activeStep === this.lastStep) {
          return this.isLastStepValid;
        }
        return false;
      }
    },

    watch: {
      domainInputs() {
        this.unselectAll();
        this.selectAll();
      },

      'tableState.filter.domainName'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.campaign'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.mainContact'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.departement'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.responsable'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, true, true);
      },
      'tableState.filter.type'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, true);
      },
      'tableState.filter.page'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, false);
      },
      'tableState.filter.pageSize'(newValue, oldValue) {
        this.filterUpdated(newValue, oldValue, false, false);
      }
    },

    methods: {
      nextStep() {
        this.activeStep++;
      },

      nbElements(inputType) {
        return this.filteredDomainInputs.filter((input) => input.inputType === inputType).length;
      },

      nbWorkforceAndOperationInputTypes() {
        const inputTypes = this.workforceAndOperationInputTypes;
        let count = 0;
        inputTypes.map(inputType => {
          count += this.filteredDomainInputs.filter(
            (input) => input.inputType === inputType).length}
        );
        return count;
      },

      selectedInputTypesChanged(inputType) {
        if (this.selectedInputTypes.includes(inputType)) {
          const inputToAdd = this.filteredDomainInputs.filter(
            (input) => input.inputType === inputType
            && !this.selectedInputIds.includes(input.topiaId)
            && !input.deprecated
            )
            .map((input) => input.topiaId);

          this.selectedInputIds = this.selectedInputIds.concat(inputToAdd);
        } else {
          const inputToRemove = this.filteredDomainInputs.filter(
            (input) => input.inputType === inputType && this.selectedInputIds.includes(input.topiaId))
            .map((input) => input.topiaId);

          this.selectedInputIds = this.selectedInputIds.filter((id) => !inputToRemove.includes(id));
        }
      },

      selectedWorkforceAndOperationInputTypeChanged() {
        const inputIds = this.filteredWorkforceAndOperationInputs.map((input) => input.topiaId);
        if (this.selectedWorkforceAndOperationInput) {
          this.selectedInputIds = inputIds;
        } else {
          this.selectedInputIds = [];
        }
      },

      selectInputType(inputType) {
        const inputToAdd = this.filteredDomainInputs.filter(
          (input) => input.inputType === inputType
                     && !this.selectedInputIds.includes(input.topiaId)
                     && !input.deprecated)
          .map((input) => input.topiaId);
        this.selectedInputIds = this.selectedInputIds.concat(inputToAdd);
        this.selectedInputTypes.push(inputType);
      },

      selectWorkforceAndOperationInputTypes() {
        const inputToAdd = this.workforceAndOperationInputTypes
          .map((input) => input.topiaId);
        this.selectedInputIds = this.selectedInputIds.concat(inputToAdd);
        this.selectedInputTypes.push(inputType);
      },

      selectedWorkforceAndOperationInputIdsChanged(inputId) {
        
        if (this.selectedInputIds.includes(inputId)) {
          const inputAdded = this.filteredWorkforceAndOperationInputs.filter((input) => input.topiaId === inputId)[0];
          const nbInputsOfType = this.filteredWorkforceAndOperationInputs.filter((input) => input.inputType === inputAdded.inputType).length;
          const nbInputsSelectedOfType = this.filteredWorkforceAndOperationInputs.filter((input) => input.inputType === inputAdded.inputType && this.selectedInputIds.includes(input.topiaId)).length;
          if (nbInputsOfType === nbInputsSelectedOfType) {
            this.selectedInputTypes.push(inputAdded.inputType);
          }
        } else {
          const inputRemoved = this.filteredWorkforceAndOperationInputs.filter((input) => input.topiaId === inputId)[0];
          this.selectedInputTypes = this.selectedInputTypes.filter((type) => type !== inputRemoved.inputType);
        }

        var notIncluteids = this.filteredWorkforceAndOperationInputs.filter(input => !this.selectedInputIds.includes(input.topiaId));
        this.selectedWorkforceAndOperationInput = notIncluteids.length === 0;
      },


      selectedInputIdsChanged(inputId) {
        if (this.selectedInputIds.includes(inputId)) {
          const inputAdded = this.filteredDomainInputs.filter((input) => input.topiaId === inputId)[0];
          const nbInputsOfType = this.filteredDomainInputs.filter((input) => input.inputType === inputAdded.inputType).length;
          const nbInputsSelectedOfType = this.filteredDomainInputs.filter((input) => input.inputType === inputAdded.inputType && this.selectedInputIds.includes(input.topiaId)).length;
          if (nbInputsOfType === nbInputsSelectedOfType) {
            this.selectedInputTypes.push(inputAdded.inputType);
          }
        } else {
          const inputRemoved = this.filteredDomainInputs.filter((input) => input.topiaId === inputId)[0];
          this.selectedInputTypes = this.selectedInputTypes.filter((type) => type !== inputRemoved.inputType);
        }
      },

      isHalfSelected(inputType) {
        const inputIds = this.filteredDomainInputs.filter((input) => input.inputType === inputType)
          .map((input) => input.topiaId);
        const isSelected = (inputId) => this.selectedInputIds.includes(inputId);
        return inputIds.some(isSelected) && !inputIds.every(isSelected);
      },

      isHalfSelectedWorkforceAndOperationInputTypes() {
        const inputIds = this.filteredWorkforceAndOperationInputs.map((input) => input.topiaId);
        const isSelected = (inputId) => this.selectedInputIds.includes(inputId);
        var result = inputIds.some(isSelected) && !inputIds.every(isSelected);
        return result;
      },

      selectAll() {
        this.selectedInputTypes = [...new Set(this.filteredDomainInputs.filter((input) => !input.deprecated).map((input) => input.inputType))]
        this.selectedInputIds = this.filteredDomainInputs.map((input) => input.topiaId);
      },

      unselectAll() {
        this.selectedInputTypes = [];
        this.selectedInputIds = [];
      },

      loadDomains() {
        displayPageLoading();
        fetch(ENDPOINTS.domainsListJson, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: "filter=" + encodeURIComponent(JSON.stringify(this.tableState.filter)) + "&fromNavigationContextChoose=false"
        })
          .then(response => response.json())
          .then(data => {
            this.domains = data.elements;
            this.tableState.pager = data;
          })
          .catch(function (response) {
            this.domains = null;
            this.tableState.pager = null;
            console.error("Can't get domains list", response);
            addPermanentError(this.$t("messages.domain-list-loading-error"));
          })
          .finally(hidePageLoading);
      },

      selectAllDomains() {
        this.domains.map((domain) => domain.topiaId)
          .forEach((id) => this.tableState.selection[id] = true);
      },

      displayResponsibles(responsibles) {
        if (responsibles) {
          return responsibles.map(responsible => responsible.firstName + " " + responsible.lastName).join(", ");
        }
      },

      filterUpdated(newValue, oldValue, withTimer, resetPage) {
        if (newValue !== oldValue) {
          if (this.timer) {
            clearTimeout(this.timer);
          }
          let applyFilter = () => {
            if (resetPage) {
              this.tableState.filter.page = 0;
            }
            this.loadDomains();
          };
          if (withTimer) {
            this.timer = setTimeout(applyFilter, 350);
          } else {
            applyFilter();
          }
        }
      },

      useDebouncedSubmit() {
        displayPageLoading();
        setTimeout(() => {
          hidePageLoading();
        }, 1000)
      },

      paste() {
        var fromDomain = this.context.domainId;
        var toOtherDomains = [];
        var toSameDomain = false;
        this.filteredSelectedDomains.forEach(domain => {
          if (domain === fromDomain) {
            toSameDomain = true;
          } else {
            toOtherDomains.push(domain);
          }
        });

        this.processInputsCopy(fromDomain, toSameDomain, toOtherDomains, this.selectedInputIds, this.forceCopy);
      },

      processInputsCopy(fromDomain, toSameDomain, toOtherDomains, inputs, forceCopy) {
        if (toOtherDomains.length > 0) {
          displayPageLoading();
          var ajaxRequest = "fromDomain=" + encodeURIComponent(fromDomain) +
            "&toDomains=" + JSON.stringify(toOtherDomains) +
            "&inputs=" + JSON.stringify(inputs) +
            "&forceCopy=" + forceCopy;
          let self = this;
          fetch(ENDPOINTS.domainInputsCopy, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: ajaxRequest
          })
            .then(response => response.json())
            .then(data => {
              if (data && data.length > 0) {
                var domainPluralize = self.$tc("messages.domain-edit-inputs-copy-paste-success-domain", data.length);
                var targetedDomain = data.join(", ") + ".";
                var message = self.$t("messages.domain-edit-inputs-copy-paste-success", [domainPluralize, targetedDomain]);
                // make sure copy on same domain is done after copy to other domains
                if (toSameDomain) {
                  this.toSameDomainCopy(inputs);
                }
                addSuccessMessage(message);
                this.close();
              }
            })
            .catch(function (response) {
              let messageKey = "messages.domain-edit-inputs-copy-paste-error";
              if (response.data === '"IllegalArgumentException"') {
                messageKey += "-invalid";
              }
              let message = self.$t(messageKey)
              console.error(message, response);
              addPermanentError(message, response.status);
            })
            .finally(() => {
              hidePageLoading();
            });
        } else if (toSameDomain) {
          // make sure copy on same domain is done after copy to other domains
          this.toSameDomainCopy(inputs);
        }
      },

      toSameDomainCopy(inputs) {
        if (this.domainInputs && this.domainInputs.length > 0) {
          this.useDebouncedSubmit();
          var inputsToCopy = [];
          this.domainInputs.filter((input) => inputs.includes(input.topiaId))
            .filter((input) => input.inputType !== 'IRRIGATION')
            .filter((input) => input.inputType !== 'CARBURANT')
            .forEach(input => {
              var copy = { ...input };
              delete copy.topiaId;
              delete copy.code;
              if (copy.price) {
                delete copy.price.topiaId;
              }

              if (copy.price && (copy.price.category === 'SEEDING_INPUT' || copy.price.category === 'SEEDING_PLAN_COMPAGNE_INPUT') &&
                  (copy.price.priceUnit === 'EURO_HA' || copy.price.priceUnit === 'EURO_M3')) {
                delete copy.price.price;
                delete copy.price.priceUnit;
                delete copy.price.topiaId;
              }

              if (copy.speciesInputs) {
                var speciesInputsToCopy = [];
                copy.speciesInputs.forEach(species_ => {
                  var species = { ...species_ };
                  speciesInputsToCopy.push(species);
                  delete species.topiaId;
                  if (species.seedPrice && (species.seedPrice.category === 'SEEDING_INPUT' || species.seedPrice.category === 'SEEDING_PLAN_COMPAGNE_INPUT') &&
                      (species.seedPrice.priceUnit === 'EURO_HA' || species.seedPrice.priceUnit === 'EURO_M3')) {
                    delete species.seedPrice.topiaId;
                    delete species.seedPrice.price;
                    delete species.seedPrice.priceUnit;
                  }
                  if (species.speciesPhytoInputDtos) {
                    var speciesProductsToCopy = [];
                    species.speciesPhytoInputDtos.forEach(product_ => {
                      var product = { ...product_ };
                      delete product.topiaId;
                      if (product.price) {
                        delete product.price.topiaId;
                        delete product.price.price;
                        delete product.price.priceUnit;
                      }
                      speciesProductsToCopy.push(product);
                    })
                    species.speciesPhytoInputDtos = speciesProductsToCopy;
                  }
                  copy.speciesInputs = speciesInputsToCopy;
                });

              }

              copy.used = false;
              copy.topiaId = "NEW-INPUT-" + new Date().getTime();
              inputsToCopy.push(copy);
            });

          this.$emit('copy', inputsToCopy);

          var message = (inputs.length > 1) ? "Les intrants ont bien été copiés vers ce même domaine: " : "L'intrant a bien été copié vers ce même domaine: ";
          addSuccessMessage(message + this.context.domainName + "&nbsp(" + this.context.campaign + ")");
        }
      },

      close() {
        this.activeStep = 1;
        this.unselectAll();
        this.selectAll();
        this.tableState.selection = {};
        this.$emit('close');
      }
    }
  };
}
