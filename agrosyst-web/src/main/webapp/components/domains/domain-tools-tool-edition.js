/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getToolEdition = function(data) {
  return {
    data() {
      return {
        editedMateriel: null,
        materielType1Row: null,
        uniteRow: null,
        displayManualTool: false,
        loadingMateriel: false,
        startEdit: false,
        materielType2Array: [],
        materielType3Array: [],
        materielType4Array: [],
        uniteArray: []
      };
    },

    props: ['toolEditionContext', 'toolEditionData'],

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal()
    },

    computed: {
      newMateriel() {
        return this.editedMateriel && this.editedMateriel._newMateriel;
      },

      materielTypesDisabled() {
        return !this.editedMateriel || !this.editedMateriel.refMateriel
               || this.editedMateriel.refMateriel.type === 'AUTRE';
      },

      materielType1Array() {
        let array = this.displayManualTool ?
            this.toolEditionData.materielType1ArrayManualTools :
            this.toolEditionData.materielType1ArrayWithoutManualTools;

        if (this.editedMateriel && this.editedMateriel.materielType) {
          array = array.filter(materiel => materiel.type == this.editedMateriel.materielType)
            .sort((a, b) => a.materielType1Translated.localeCompare(b.materielType1Translated));
        } else {
          const map = mapBy(array, "type");
          array = this.translateAndOrder(map);
        }

        return array;
      },

      materielType2Disabled() {
        return this.materielTypesDisabled || !this.editedMateriel.refMateriel.typeMateriel1;
      },

      materielType3Disabled() {
        return this.materielTypesDisabled || this.materielType2Array.length < 1
            || !this.editedMateriel.refMateriel.typeMateriel2 && this.editedMateriel.refMateriel.typeMateriel2 != "";
      },

      materielType4Disabled() {
        return this.materielTypesDisabled || this.materielType3Array.length < 1
            || !this.editedMateriel.refMateriel.typeMateriel3 && this.editedMateriel.refMateriel.typeMateriel3 != "";
      },

      isEquipmentValid() {
        return this.editedMateriel && this.editedMateriel.refMateriel
            && (this.editedMateriel.refMateriel.typeMateriel1 || this.editedMateriel.refMateriel.typeMateriel1 === "")
            && (this.editedMateriel.refMateriel.typeMateriel2 || this.editedMateriel.refMateriel.typeMateriel2 === "")
            && (this.editedMateriel.refMateriel.typeMateriel3 || this.editedMateriel.refMateriel.typeMateriel3 === "")
            && (this.editedMateriel.refMateriel.typeMateriel4 || this.editedMateriel.refMateriel.typeMateriel4 === "")
            && (this.materielTypesDisabled || this.uniteRow)
            && (!this.editedMateriel.realUsage || this.editedMateriel.realUsage >= 0.0)
            && this.editedMateriel.name;
      }
    },

    watch: {
      toolEditionContext(newValue, oldValue) {
        if (newValue) {
          this.materielType1Row = newValue.materielType1Row && cloneObject(newValue.materielType1Row);
          this.displayManualTool = newValue.displayManualTool;
          this.uniteRow = newValue.uniteRow && cloneObject(newValue.uniteRow);
          this.editedMateriel = cloneObject(newValue.editedMateriel);
          this.startEdit = newValue.startEdit;
          this.materielType1Selected()
            .then(() => this.materielType2Selected()
              .then(() => this.materielType3Selected()
                .then(() => this.materielType4Selected())))
            .catch(e => e && console.error(e));
        }
      },

      uniteRow(newValue, oldValue) {
        if (newValue !== oldValue) {
          this.uniteSelected(newValue);
        }
      }
    },

    methods: {
      close() {
        this.toolEditionContext && this.toolEditionContext.close();
      },

      validate() {
        if (this.toolEditionContext){
          this.toolEditionContext.callback(this.editedMateriel);
          this.toolEditionContext.close();
        }
      },

      paste() {
        if (this.toolEditionContext) {
          this.toolEditionContext.callback(this.editedMateriel);
          this.toolEditionContext.close();
        }
      },

      displayNullableLabel(label) {
        return label || "<<< " + this.$t("messages.domain-edit-tools-materiel-empty") + " >>>";
      },

      sortByRight(a, b) {
        return a.right.localeCompare(b.right);
      },

      materielType1Selected(clearNextFields) {
        if (this.loadingMateriel || !this.editedMateriel) {
          return Promise.reject();
        }
        if (!this.materielType1Row) {
          this.editedMateriel.refMateriel = {};
          return Promise.reject();
        }
        if (!this.editedMateriel.refMateriel) {
          this.editedMateriel.refMateriel = {};
        }
        this.editedMateriel.refMateriel.type = this.materielType1Row.type;
        this.editedMateriel.refMateriel.typeMateriel1 = this.materielType1Row.materielType1;
        var filter = {
          filterOnManualTool: this.displayManualTool,
          type: this.editedMateriel.refMateriel.type,
          typeMateriel1: this.editedMateriel.refMateriel.typeMateriel1
        };
        displayPageLoading();

        return fetch(ENDPOINT_DOMAINS_EDIT_MATERIEL_TYPE2_JSON + "?filter=" + encodeURIComponent(JSON.stringify(filter)))
          .then(response => response.json())
          .then(data => {
              if (clearNextFields) {
                // clear next
                delete this.editedMateriel.refMateriel.typeMateriel2;
                delete this.editedMateriel.refMateriel.typeMateriel3;
                delete this.editedMateriel.refMateriel.typeMateriel4;
                delete this.uniteRow;
                delete this.editedMateriel.refMateriel.topiaId;
              }
              this.materielType2Array = data.sort((a, b) => this.sortByRight(a, b));
              if (clearNextFields && data.length === 1) {
                this.editedMateriel.refMateriel.typeMateriel2 = data[0].left;
                this.editedMateriel.refMateriel.typeMateriel2Translated = data[0].right;
                this.materielType2Selected(true);
              }
            })
            .catch(function(response) {
              console.error("Can't get type 2 data", response);
              var message = this.$t("messages.domain-edit-tools-materiel-type2-loading-failed");
              addPermanentError(message, response.status);
            })
            .finally(hidePageLoading);
      },

      materielType2Selected(clearNextFields) {
        if (this.loadingMateriel || !this.editedMateriel) {
          return Promise.reject();
        }
        if (this.editedMateriel.refMateriel.typeMateriel2) {
          var filter = this.editedMateriel.refMateriel;
          filter.filterOnManualTool = this.displayManualTool;
          displayPageLoading();

          return fetch(ENDPOINT_DOMAINS_EDIT_MATERIEL_TYPE3_JSON + "?filter=" + encodeURIComponent(JSON.stringify(filter)))
            .then(response => response.json())
            .then(data => {
              if (clearNextFields) {
                // clear next
                delete this.editedMateriel.refMateriel.typeMateriel3;
                delete this.editedMateriel.refMateriel.typeMateriel4;
                delete this.editedMateriel.refMateriel.uniteParAn;
              }
              this.materielType3Array = data.sort((a, b) => this.sortByRight(a, b));
              if (clearNextFields && data.length === 1) {
                this.editedMateriel.refMateriel.typeMateriel3 = data[0].left;
                this.editedMateriel.refMateriel.typeMateriel3Translated = data[0].right;
                this.materielType3Selected(true);
              }
          })
          .catch(function(response) {
            console.error("Can't get type 3 data", response);
            var message = this.$t("messages.domain-edit-tools-materiel-type3-loading-failed");
            addPermanentError(message, response.status);
          })
          .finally(hidePageLoading);
        }
      },

      materielType3Selected(clearNextFields) {
        if (this.loadingMateriel || !this.editedMateriel) {
          return Promise.reject();
        }
        if (this.editedMateriel.refMateriel.typeMateriel3 || this.editedMateriel.refMateriel.typeMateriel3 === "") {
          var filter = this.editedMateriel.refMateriel; // FIXME fax way for now
          filter.filterOnManualTool = this.displayManualTool;
          displayPageLoading();

          return fetch(ENDPOINT_DOMAINS_EDIT_MATERIEL_TYPE4_JSON + "?filter=" + encodeURIComponent(JSON.stringify(filter)))
            .then(response => response.json())
            .then(data => {
              if (clearNextFields) {
                // clear next
                delete this.editedMateriel.refMateriel.typeMateriel4;
                delete this.editedMateriel.refMateriel.uniteParAn;
              }
              this.materielType4Array = data.sort((a, b) => this.sortByRight(a, b));
              if (clearNextFields && data.length == 1) {
                this.editedMateriel.refMateriel.typeMateriel4 = data[0].left;
                this.editedMateriel.refMateriel.typeMateriel4Translated = data[0].right;
                this.materielType4Selected(true);
              }
              if (this.startEdit && data.length == 1) {
                this.editedMateriel.refMateriel.typeMateriel4 = data[0].left;
                this.editedMateriel.refMateriel.typeMateriel4Translated = data[0].right;
              }
          })
          .catch(function(response) {
            console.error("Can't get type 4 data", response);
            var message = "Echec de récupération de la liste des matériels de type 4";
            addPermanentError(message);
          })
          .finally(hidePageLoading);
        }
      },

      materielType4Selected(clearNextFields) {
        if (this.loadingMateriel || !this.editedMateriel) {
          return Promise.reject();
        }
        if (this.editedMateriel.refMateriel.typeMateriel4 || this.editedMateriel.refMateriel.typeMateriel4 === "") {
          var filter = this.editedMateriel.refMateriel; // FIXME fax way for now
          filter.filterOnManualTool = this.displayManualTool;
          displayPageLoading();
          return fetch(ENDPOINT_DOMAINS_EDIT_MATERIEL_UNITE_JSON + "?filter=" + encodeURIComponent(JSON.stringify(filter)))
            .then(response => response.json())
            .then(data => {
              if (clearNextFields) {
                // clear next
                delete this.editedMateriel.refMateriel.uniteParAn;
                delete this.editedMateriel.refMateriel.unite;
              }
              this.uniteArray = [];
              Object.keys(data).forEach(key => {
                let uniteParAn = data[key][0].uniteParAn;
                let unite = data[key][0].unite;
                let uniteTranslated = data[key][0].unitTranslated;
                this.uniteArray.push({
                  materielTopiaId: key,
                  uniteParAn: uniteParAn,
                  unite: unite,
                  uniteTranslated : uniteTranslated
                  });
              });
              this.uniteArray.sort((a, b) => (a.uniteParAn > b.uniteParAn) ? 1 : ((b.uniteParAn > a.uniteParAn) ? -1 : 0));
              if (clearNextFields && this.uniteArray.length == 1) {
                this.uniteRow = this.uniteArray[0];
                this.uniteSelected(this.uniteRow);
              }
              if (this.startEdit) {
                this.uniteRow = this.uniteArray.find((row) => this.editedMateriel.refMateriel.topiaId === row.materielTopiaId);
                this.uniteSelected(this.uniteRow);
              }
          })
          .catch(function(response) {
            console.error("Can't get unite data", response);
            var message = this.$t("messages.domain-edit-tools-materiel-approximative-usage-loading-failed");
            addPermanentError(message, response.status);
          })
          .finally(hidePageLoading);
        }
      },

      uniteSelected(uniteRow) {
        if (uniteRow) { // can be undefined in case of UI update
          this.editedMateriel.refMateriel.topiaId    = uniteRow.materielTopiaId;
          this.editedMateriel.refMateriel.unite      = uniteRow.unite;
          this.editedMateriel.refMateriel.uniteParAn = uniteRow.uniteParAn;
        }
      },

      reverseStartEdit() {
        this.startEdit = !this.startEdit;
      },

      getModalTitle() {
        const titre = this.$t("messages.domain-edit-tools-materiel-title-" + (this.newMateriel ? "add" : "edit"));
        const typeMateriel = this.editedMateriel && this.editedMateriel.materielType ? " " + this.$t("MaterielType." + this.editedMateriel.materielType) : "";
        return titre + typeMateriel;
      },

      translateAndOrder(map) {
        const unordered = Object.keys(map).reduce((obj, key) => {
          const values = map[key].slice();
          values.sort((a, b) => a.materielType1Translated.localeCompare(b.materielType1Translated));
          obj[this.$t("MaterielType." + key)] = values;
          return obj;
        }, {});
        return Object.keys(unordered).sort()
          .reduce((obj, key) => {
            obj[key] = unordered[key];
            return obj;
          }, {});
      },
    },

    template: '#domain-tools-tool'
  }
}
