/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getGpsDataTable = function(data) {
  return {
    data() {
      return {
        gpsDatas: data.gpsDatas,
        locationId: data.locationId
      }
    },

    methods: {
      addGpsData() {
        let gpsData = {};
        this.gpsDatas.push(gpsData);
      },

      removeGpsData(index) {
        this.gpsDatas.splice(index, 1);
      },

      isRequired(gpsData) {
        return gpsData.name || gpsData.longitude || gpsData.latitude;
      },

      displayLocation(gpsData) {
        displayLocation(gpsData.name, gpsData.latitude, gpsData.longitude, this.locationId);
      }
    },

    mounted() {
      if (!this.gpsData) {
        this.gpsData = [];
      }
    },
    template:'#domainCoordinates'
  }
}
