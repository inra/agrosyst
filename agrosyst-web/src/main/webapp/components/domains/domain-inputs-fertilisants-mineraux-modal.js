/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainInputsFertilisantsMinerauxModal = function(data) {
  return {
    template: '#domain-inputs-fertilisants-mineraux-modal',

    props: {
      selectedInputType: String,
      selectedInput: Object,
      isUsed: Boolean
    },

    inject: ['context'],

    emits: ['save', 'close'],

    setup() {
      return { v$: Vuelidate.useVuelidate() }
    },

    components: {
      GenericModal: window.AgrosystVueComponents.Common.getGenericModal(),
      GenericStepProgressBar: window.AgrosystVueComponents.Common.getGenericStepProgressBar(),
    },

    mounted() {
      if (!this.types.length) {
        this.fetchTypes();
      }
      if (!Object.keys(this.elementLabels).length) {
        this.fetchElements();
      }
    },

    data() {
      return {
        steps: [
          this.$t("messages.common-definition"),
          this.$t("messages.common-composition"),
          this.$t("messages.common-price")
        ],
        stepProgress: {
          step: 0
        },

        types: [],
        selectedType: {},
        shapesWithTranslations: [],
        elementLabels: {},
        objectId: undefined,
        loading: 0,

        priceStepValid: !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit),

        // step définition
        firstStepFields: ['categ', 'inputName', 'shapeWithTranslation', 'usageUnit'],
        search: '',
        categ: this.selectedInput.categ,
        tradeName: this.selectedInput.tradeName,
        tradeNameTranslated: this.selectedInput.tradeNameTranslated,
        inputName: this.selectedInput.inputName,
        shapeWithTranslation: {shape:this.selectedInput.forme, translatedShape:this.selectedInput.formeTranslated},
        phytoEffect: this.selectedInput.phytoEffect,
        usageUnit: this.selectedInput.usageUnit,

        // step composition
        secondStepFields: [
          { name: 'n', label: 'N', title: this.$t("messages.common-element-N"), visible: true },
          { name: 'p2O5', label: 'P2O5', title: this.$t("messages.common-element-P2O5"), visible: true },
          { name: 'k2O', label: 'K2O', title: this.$t("messages.common-element-K2O"), visible: true },
          { name: 'bore', label: 'B', title: this.$t("messages.common-element-B"), visible: false },
          { name: 'calcium', label: 'Ca', title: this.$t("messages.common-element-Ca"), visible: false },
          { name: 'fer', label: 'Fe', title: this.$t("messages.common-element-Fe"), visible: false },
          { name: 'manganese', label: 'Mn', title: this.$t("messages.common-element-Mn"), visible: false },
          { name: 'molybdene', label: 'Mo', title: this.$t("messages.common-element-Mo"), visible: false },
          { name: 'mgO', label: 'MgO', title: this.$t("messages.common-element-MgO"), visible: false },
          { name: 'oxyde_de_sodium', label: 'Na2O', title: this.$t("messages.common-element-Na2O"), visible: false },
          { name: 'sO3', label: 'SO3', title: this.$t("messages.common-element-SO3"), visible: false },
          { name: 'cuivre', label: 'Cu', title: this.$t("messages.common-element-Cu"), visible: false },
          { name: 'zinc', label: 'Zn', title: this.$t("messages.common-element-Zn"), visible: false }
        ],
        showAllElements: false,
        n: this.selectedInput.n,
        p2O5: this.selectedInput.p2o5,
        k2O: this.selectedInput.k2o,
        bore: this.selectedInput.bore,
        calcium: this.selectedInput.calcium,
        fer: this.selectedInput.fer,
        manganese: this.selectedInput.manganese,
        molybdene: this.selectedInput.molybdene,
        mgO: this.selectedInput.mgo,
        oxyde_de_sodium: this.selectedInput.oxyde_de_sodium,
        sO3: this.selectedInput.so3,
        cuivre: this.selectedInput.cuivre,
        zinc: this.selectedInput.zinc,

        // step prix
        price: this.selectedInput.price ? this.selectedInput.price.price : undefined,
        priceUnit: this.selectedInput.price ? this.selectedInput.price.priceUnit : 'EURO_KG'
      };
    },

    provide() {
      return {
        stepProgress: this.stepProgress
      }
    },

    validations() {
      return {
        // step définition
        categ: { required: VuelidateValidators.required, $autoDirty: true },
        inputName: { required: VuelidateValidators.required, $autoDirty: true },
        shapeWithTranslation: { required: VuelidateValidators.required, $autoDirty: true },
        usageUnit: { required: VuelidateValidators.required, $autoDirty: true },

        // step composition
        n: { required: VuelidateValidators.required, decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        p2O5: { required: VuelidateValidators.required, decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        k2O: { required: VuelidateValidators.required, decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        bore: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        calcium: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        fer: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        manganese: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        molybdene: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        mgO: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        oxyde_de_sodium: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        sO3: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        cuivre: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
        zinc: { decimal: VuelidateValidators.decimal, minValue: VuelidateValidators.minValue(0), $autoDirty: true },
      }
    },

    computed: {
      selectedInputTypeLabel() { return this.selectedInputType ? this.$t('InputType.' + this.selectedInputType) : '' },

      mineralProductUnits() { return Object.keys(this.context.i18n.MineralProductUnit); }, // todo: this list has to be filtered at runtime see with dcosse

      isFirstStepValid() {
        return this.firstStepFields.map(field => this.isFieldValid(field))
          .reduce((prev, curr) => prev && curr, true);
      },

      isSecondStepValid() {
        return this.secondStepFields.map(field => this.isFieldValid(field.name))
          .reduce((prev, curr) => prev && curr, true);
      },

      isStepValid() {
        if (this.stepProgress.step === 0) {
          return this.isFirstStepValid;
        } else if (this.stepProgress.step === 1) {
          return this.isSecondStepValid;
        } else if (this.stepProgress.step === 2) {
          return this.priceStepValid;
        }
        return false;
      },

      filteredTypes() {
        if (this.types && this.types.length > 0) {
          return this.search && this.search.length > 0 ? this.types.filter(
            (type) => type.translatedLabel.toString().toLowerCase().indexOf(this.search.toLowerCase()) >= 0
          ) : this.types;
        }
        return [];
      }
    },

    watch: {
      'stepProgress.step'(newStep) {
        if (newStep > 1 && this.categ && this.shapeWithTranslation) {
          this.updateObjectId();
        }
      },

      selectedInput(newInput) {
        // step définition
        this.categ = newInput.categ;
        this.tradeName = newInput.tradeName;
        this.tradeNameTranslated = newInput.tradeNameTranslated;
        this.inputName = newInput.inputName;
        this.shapeWithTranslation = newInput.forme;
        this.usageUnit = newInput.usageUnit;

        // step composition
        this.n = newInput.n;
        this.p2O5 = newInput.p2o5;
        this.k2O = newInput.k2o;
        this.bore = newInput.bore;
        this.calcium = newInput.calcium;
        this.fer = newInput.fer;
        this.manganese = newInput.manganese;
        this.molybdene = newInput.molybdene;
        this.mgO = newInput.mgo;
        this.oxyde_de_sodium = newInput.oxyde_de_sodium;
        this.sO3 = newInput.so3;
        this.cuivre = newInput.cuivre;
        this.zinc = newInput.zinc;

        // step prix
        this.priceStepValid = !this.selectedInput.price || (!this.selectedInput.price.price) || (this.selectedInput.price && this.selectedInput.price.price && this.selectedInput.price.priceUnit);
        if (newInput.price) {
          this.price = newInput.price.price;
          this.priceUnit = newInput.price.priceUnit;
        } else {
          this.price = undefined;
          this.priceUnit = undefined;
        }
        this.v$.$reset();
      },

      shapeWithTranslation(newShape) {
        if (newShape === 'Liquide') {
          this.usageUnit = 'L_HA';
        } else {
          this.usageUnit = 'KG_HA';
        }
      },

      categ(newCateg) {
        if (newCateg) {
          this.foundSelectedType(newCateg);
          this.tradeName = this.selectedType ? this.selectedType.label : '';
          this.tradeNameTranslated = this.selectedType ? this.selectedType.translatedLabel : '';
          this.shapesWithTranslations = this.selectedType ? this.selectedType.shapesWithTranslations : [];
          if (this.shapesWithTranslations.length === 1) this.shapeWithTranslation = this.shapesWithTranslations[0];
          this.inputName = this.tradeNameTranslated;
        }
      },
    },

    methods: {
      close() {
        this.$emit('close');
        this.resetForm();
      },

      previousStep() {
        this.stepProgress.step--;
      },

      nextStep() {
        this.stepProgress.step++;
      },

      updateObjectId() {
        this.objectId = "categ=" + this.categ + " " + "forme=" + this.shapeWithTranslation.shape;
        var elements = ['n', 'p2O5', 'k2O', 'bore', 'calcium', 'fer', 'manganese', 'molybdene',
          'mgO', 'oxyde_de_sodium', 'sO3', 'cuivre', 'zinc'];
        for (var i = 0; i < elements.length; i++) {
          var element = elements[i];
          this.objectId += this.$data[element] !== undefined ? " " + element + "=" + this.computeCompositionKey(this.$data[element]) : " " + element + "=0.000000";
        }
      },

      isFieldDirty(fieldName) {
        return this.v$[fieldName].$dirty;
      },

      isFieldValid(fieldName) {
        return !this.v$[fieldName].$invalid;
      },

      fetchTypes() {
        this.loading++;
        displayPageLoading();

        fetch(this.context.loadFertiMineJson, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.types = data;
            this.foundSelectedType(this.categ);
            this.search = this.selectedType ? this.selectedType.translatedLabel : '';
            this.shapesWithTranslations = this.selectedType && this.selectedType.shapesWithTranslations ? this.selectedType.shapesWithTranslations : [];
          })
          .catch((response) => {
            console.error('Échec de récupération de la liste des types de fertilisants organiques', response);
            const message = 'Échec de récupération de la liste des types de fertilisants organiques';
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          })
      },

      fetchElements() {
        this.loading++;
        displayPageLoading();

        fetch(this.context.loadMineralProductElementJson, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          }
        })
          .then((response) => response.json())
          .then((data) => {
            this.elementLabels = data;
          })
          .catch((response) => {
            const message = 'Échec de récupération de la liste des éléments des fertilisants organiques';
            console.error(message, response);
            addPermanentError(message, response.status);
          })
          .finally(() => {
            this.loading--;
            if (!this.loading) {
              hidePageLoading();
            }
          })
      },

      foundSelectedType(categ) {
        var filteredTypes = this.types.filter((type) => type.categ === categ);
        this.selectedType = filteredTypes && filteredTypes.length ? filteredTypes[0] : {};
      },

      computeCompositionKey(element) {
        // convert to string
        element = element + "";
        // split to integer and decimal part
        var part = element.split('.');
        var result = part[0] + ".";
        result += part.length > 1 ? part[1] + ("000000".substr(part[1].length)) : "000000";
        return result;
      },

      save() {
        const input = {
          ...this.selectedInput,
          inputType: 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX',

          // step définition
          categ: this.categ,
          tradeName: this.tradeName,
          tradeNameTranslated: this.tradeNameTranslated,
          inputName: this.inputName,
          forme: this.shapeWithTranslation.shape,
          formeTranslated: this.shapeWithTranslation.translatedShape,
          type_produit: this.tradeName,
          phytoEffect: this.phytoEffect,
          usageUnit: this.usageUnit,

          // step composition
          n: this.n,
          p2o5: this.p2O5,
          k2o: this.k2O,
          bore: this.bore,
          calcium: this.calcium,
          fer: this.fer,
          manganese: this.manganese,
          molybdene: this.molybdene,
          mgo: this.mgO,
          oxyde_de_sodium: this.oxyde_de_sodium,
          so3: this.sO3,
          cuivre: this.cuivre,
          zinc: this.zinc
        };
        if (!isNaN(parseFloat(this.price)) || this.priceUnit !==undefined) {
          // step prix
          input.price = {
            ...(this.selectedInput.price ? this.selectedInput.price : {}),
            category: 'MINERAL_INPUT',
            price: this.price,
            priceUnit: this.priceUnit,
            displayName: this.tradeName + ' ' + this.getDisplayNameFromComposition(),
            sourceUnit: this.usageUnit
          }
        } else {
          input.price = undefined;
        }
        this.$emit('save', input);
        this.resetForm();
        (angular.element("#domainEditForm").scope()).domainEditForm.$setDirty();
      },

      getDisplayNameFromComposition() {
        var results = [];
        var elements = Object.keys(this.elementLabels);
        for (var i = 0; i < elements.length; i++) {
          var element = elements[i];
          if (this.$data[element] !== undefined) {
            results.push(this.elementLabels[element] + "(" + this.$data[element] + "%)");
          }
        }
        return results.join(", ");
      },

      resetForm() {
        this.v$.$reset();
        this.stepProgress.step = 0;
        this.search = '';
      },

      selectOption(option) {
        this.categ = option ? option.categ : undefined;
      }
    },
  };
};
