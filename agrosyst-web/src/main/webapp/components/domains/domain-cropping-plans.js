/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainCroppingPlansController = function (data) {
  const controller = Vue.createApp({
    data() {
      return {
        domain: data.domain,
        downloadSpeciesUrl: data.downloadSpeciesUrl,
        forFrance: data.forFrance,
        frontApp: data.frontApp,
        speciesDistributionMap: {},
        speciesDistribution: speciesDistribution,
        croppingPlans: null,

        yealdUnits: I18N.YealdUnit,
        estimatingIftRules: I18N.EstimatingIftRules,
        iftSeedsTypes: I18N.IftSeedsType,
        doseTypes: I18N.DoseType,
        compagneTypes: I18N.CompagneType,
        totalSpeciesDistributionSurface: 0,

        // surface used by species
        // this.totalSpeciesDistributionSurface,

        cpEntrySelected: null,
        cropSpeciesEdition: {
          edit: false,
          intermediate: false,
          cpes_species: [],
          species: null,
          varieties: [],
          variety: null,
          isFetchingSpecies: false,
          originalCpEntrySpeciesIndex: null,
          autocompleteTypedText: null,
        },

        croppingPlansUsageMap: {},
        selectedCrops: {},
        allCropSelected: { selected: false },
        copyDialogContext: null
      };
    },

    computed: {
      cpEntrySelectedIntermediate() {
        return this.cpEntrySelected && this.cpEntrySelected.type === 'INTERMEDIATE';
      },

      cpEntrySelectedMixed() {
        return this.cpEntrySelected.mixSpecies || this.cpEntrySelected.mixVariety;
      },

      filteredSelectedCrops() {
        return Object.keys(this.selectedCrops).filter(key => !!this.selectedCrops[key]);
      },

      noSelectedCrops() {
        return !Object.values(this.selectedCrops).filter(value => !!value).length;
      },

      isSpeciesValid() {
        if (!this.cropSpeciesEdition.species || this.cropSpeciesEdition.species.speciesArea === undefined) {
          return true;
        } else {
          var value = this.cropSpeciesEdition.species.speciesArea;
          const positiveNumbersOnly = this.cpEntrySelected &&
            (!this.cpEntrySelected.yealdAverage  || this.cpEntrySelected.yealdAverage  >= 0) &&
            (!this.cpEntrySelected.averageIFT    || this.cpEntrySelected.averageIFT    >= 0) &&
            (!this.cpEntrySelected.biocontrolIFT || this.cpEntrySelected.biocontrolIFT >= 0);

          if (undefined === this.compagneTypes[this.cropSpeciesEdition.species.compagne] 
            && this.cpEntrySelected && !this.cpEntrySelected.mixSpecies && !this.cpEntrySelected.mixVariety 
            && !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))) {
            const parsedValue = parseInt(value);
            return 0 <= parsedValue && parsedValue <= 100 && positiveNumbersOnly;
          } else if (this.compagneTypes[this.cropSpeciesEdition.species.compagne] === undefined
              && this.cpEntrySelected && (this.cpEntrySelected.mixSpecies || this.cpEntrySelected.mixVariety)) {
            return true && positiveNumbersOnly;
          } else if (this.compagneTypes[this.cropSpeciesEdition.species.compagne]) {
            return (null === value || undefined === value || 0 === parseInt(value)) && positiveNumbersOnly;
          } else {
            return false;
          }
        }
      }
    },

    watch: {
      'cpEntrySelected.type'(newValue, oldValue) {
        if (newValue === 'INTERMEDIATE' && this.cpEntrySelected.temporaryMeadow === false) {
          this.cpEntrySelected.temporaryMeadow = null;
        }
      },
      'cropSpeciesEdition.species.speciesArea'(newValue, oldValue) {
        if (newValue != oldValue) {
          this.speciesAreForceLock();
        }
      },
      'cpEntrySelected.temporaryMeadow'(newValue, oldValue) {
        if (newValue && this.cpEntrySelected.type === 'INTERMEDIATE') {
          this.cpEntrySelected.type = 'MAIN';
        }
      },
      'cropSpeciesEdition.species.compagne'(newValue, oldValue) {
        if (newValue != oldValue) {
          if (!newValue && this.cropSpeciesEdition.species) {
            this.cropSpeciesEdition.species.compagne = 0;
            this.unLockSpecies();
          } else {
            this.resetSpeciesArea();
          }
        }
      }
    },

    methods: {
      focus(elementId) {
        setTimeout(() => document.getElementById(elementId).focus(), 500);
      },

      highlightAutocomplete(label) {
        if (label) {
          let regex = new RegExp(this.cropSpeciesEdition.autocompleteTypedText, "i");
          return label.replace(regex,"<strong>$&</strong>");
        }
        return label;
      },

      toggleSelectedCrops() {
        if (this.allCropSelected.selected) {
          this.croppingPlans.forEach(crop => {
            if (crop.code) {
              this.selectedCrops[crop.code] = true;
            }
          });
        } else {
          this.selectedCrops = {};
        }
      },

      toggleCropSelection(code) {
        this.selectedCrops[code] = !this.selectedCrops[code];
      },

      editCroppingPlanEntry(element) {
        this.cropSpeciesEdition.species = null;
        this.cpEntrySelected = cloneObject(element);
        if (!this.cpEntrySelected.intermediate) {
          this.cpEntrySelected.intermediate = this.cpEntrySelected.type === 'INTERMEDIATE';
        }
        this.focus('cpe_name');
      },

      editCroppingPlanSpecies(element) {
        this.cropSpeciesEdition.originalCpEntrySpeciesIndex = this.cpEntrySelected.species.indexOf(element);
        this._initResearchFields();
        this.cropSpeciesEdition.intermediate = this.cpEntrySelected.type === 'INTERMEDIATE';
        this.cropSpeciesEdition.species = cloneObject(element);
        this.setLabelForSpecies(this.cropSpeciesEdition.species);

        this.cropSpeciesEdition.varietyLibelle = ''
        if (this.cropSpeciesEdition.species && this.cropSpeciesEdition.species.varietyLibelle) {
          this.cropSpeciesEdition.variety = {
            varietyLibelle: this.cropSpeciesEdition.species.varietyLibelle,
            varietyId: this.cropSpeciesEdition.species.varietyId
          };
          this.cropSpeciesEdition.varietyLibelle = this.cropSpeciesEdition.species.varietyLibelle
        }

        this.cropSpeciesEdition.edit = true;
        this.focus('cpes_species');
      },

      speciesAreForceLock() {
        if (this.cropSpeciesEdition.species) {
          this.cropSpeciesEdition.species.locked = true;
          this.cropSpeciesEdition.species.fLocked = true;
        }
      },

      unLockSpecies() {
        if (this.cropSpeciesEdition.species) {
          this.cropSpeciesEdition.species.locked = false;
          this.cropSpeciesEdition.species.fLocked = false;
        }
      },

      resetSpeciesArea() {
        if (this.cpEntrySelected.mixSpecies || this.cpEntrySelected.mixVariety) {
          if (this.cropSpeciesEdition.species) {
            this.cropSpeciesEdition.species.speciesArea = null;
          }
        } else if (this.cropSpeciesEdition.species && this.cropSpeciesEdition.species.compagne) {
          this.cropSpeciesEdition.species.speciesArea = 0;
          this.speciesAreForceLock();
        }
      },

      isSelectedCroppingPlanEntry(element) {
        return this.cpEntrySelected === element;
      },

      isSelectedCroppingPlanSpecies(element) {
        return this.cropSpeciesEdition.species && this.cropSpeciesEdition.species === element;
      },

      toggleMixSpeciesOrVariety() {
        if (this.cpEntrySelected.species) {
          if (this.cpEntrySelected.mixSpecies || this.cpEntrySelected.mixVariety) {
            this.cpEntrySelected.species.forEach(aSpecies => aSpecies.speciesArea = null);
          } else {
            this.cpEntrySelected.species.forEach(aSpecies => aSpecies.speciesArea = 100 / this.cpEntrySelected.species.length);
          }
        }
      },

      _pushSpeciesToCrop() {
        if (this.cropSpeciesEdition.species) {
          this.cropSpeciesEdition.species.varietyId = "";
          this.cropSpeciesEdition.species.varietyLibelle = "";

          if (this.cropSpeciesEdition.variety) {
            this._bindVarietyData(this.cropSpeciesEdition.variety);
          }

          let speciesToPush = cloneObject(this.cropSpeciesEdition.species);
          if (this.cropSpeciesEdition.originalCpEntrySpeciesIndex != -1) {
            this.cpEntrySelected.species[this.cropSpeciesEdition.originalCpEntrySpeciesIndex] = speciesToPush;
          } else {
            this.cpEntrySelected.species.push(speciesToPush);
          }

          this._recomputeSpeciesAreas();
        }
      },

      _recomputeSpeciesAreas() {
        if (this.cpEntrySelected.mixSpecies || this.cpEntrySelected.mixVariety) {
                    this.cpEntrySelected.species.forEach(aSpecies => aSpecies.speciesArea = null);
        } else {
          let total = 0;
          let totalUnlocked = 0;
          let unlockedSpecies = [];
          if (this.cpEntrySelected) {
            this.cpEntrySelected.species.forEach(aSpecies => {
              let aSpeciesArea = aSpecies.speciesArea ? parseInt(aSpecies.speciesArea) : 0;
              if (aSpecies.locked) {
                total += aSpeciesArea;
              } else {
                unlockedSpecies.push(aSpecies);
                totalUnlocked += aSpeciesArea;
              }
            });
            let remainingSpeciesAreaFactor = totalUnlocked == 0 ? 0 : (100 - total) / totalUnlocked;
            unlockedSpecies.forEach((aSpecies, i) => {
              var speciesArea;
              if (i === unlockedSpecies.length - 1) {
                speciesArea = parseInt(100 - total);
              } else {
                let sSpeciesArea = aSpecies.speciesArea ? parseInt(aSpecies.speciesArea) : 0;
                speciesArea = parseInt(sSpeciesArea * remainingSpeciesAreaFactor);
              }
              aSpecies.speciesArea = speciesArea;
              total += speciesArea;
            });
            this.cpEntrySelected.totalArea = total;
          }
        }
        this.$forceUpdate();
      },

      bindSpeciesData(selectedSpecies) {
        this.cropSpeciesEdition.autocompleteTypedText = null;
        if (selectedSpecies) {
          selectedSpecies.varietyId = "";
          selectedSpecies.varietyLibelle = "";
          Object.assign(this.cropSpeciesEdition.species, selectedSpecies);

          this.cropSpeciesEdition.variety = null;
          this.cropSpeciesEdition.varieties = null;

          this.focus('cpes_variety');
        }

      },

      bindVarietyData(variety) {
        this.cropSpeciesEdition.autocompleteTypedText = null;
        this.cropSpeciesEdition.varieties = [{ speciesId: null, varietyLibelle : "" }];
        if (variety) {
          this.cropSpeciesEdition.variety = variety;
        }
      },

      _bindVarietyData(variety) {
        if (variety && variety.varietyId && variety.varietyId !== null) {
          this.cropSpeciesEdition.species.varietyId = variety.varietyId;
          this.cropSpeciesEdition.species.varietyLibelle = variety.varietyLibelle;
          this.cropSpeciesEdition.species.edaplosUnknownVariety = null;
        } else {
          this.cropSpeciesEdition.species.varietyId = "";
          this.cropSpeciesEdition.species.varietyLibelle = "";
        }
      },

      _clearEditedSpeciesData() {
        this.cropSpeciesEdition.edit = false;

        this.cropSpeciesEdition.species = null;
        this.cropSpeciesEdition.variety = null;
        // this.cropSpeciesEdition.originalCpEntrySpeciesIndex = null;
        this.cropSpeciesEdition.cpes_species = null;
        this.cropSpeciesEdition.varieties = null;
      },

      stopEntrySpeciesEdition() {
        this._pushSpeciesToCrop();
        this._clearEditedSpeciesData();
        var species = this.cpEntrySelected.species || [];
        this.cpEntrySelected.mixCompanion = species.find(sp => sp.compagne && sp.compagne.length > 0) !== undefined;
        if (!this.cpEntrySelected.intermediate
            && this.cpEntrySelected.species.length > 1
            && !this.cpEntrySelected.mixSpecies
            && !this.cpEntrySelected.mixVariety) {
          addPermanentWarning(
            "La culture que vous renseignez contient plusieurs espèces et/ou variétés " +
            "MAIS la case \"Mélange d’espèce/variété\" n'est pas cochée ! " +
            "L'espèce/variété est considérée comme cultivée en PUR !");
        }
      },

      addCroppingPlanEntry(intermediate) {
        var newEntry = {
          intermediate: intermediate,
          type: intermediate ? 'INTERMEDIATE' : 'MAIN',
          name: '',
          code: guid()
        };
        newEntry.species = [];
        this.editCroppingPlanEntry(newEntry);
      },

      stopEditingCroppingPlanEntry() {
        var existingCpEntry = this.croppingPlans.find(cp => cp.code === this.cpEntrySelected.code);
        if (!existingCpEntry) {
          this.croppingPlans.push(this.cpEntrySelected);
        } else {
          Object.assign(existingCpEntry, this.cpEntrySelected);
        }
        this._computeSpeciesDistributionMap(true);
        this.cpEntrySelected = null;
      },

      addCroppingPlanSpecies() {
        var newSpecies = { code: guid() };
        // get last element
        var cpEntrySelectedSpeciesSize = this.cpEntrySelected.species.length;
        if (cpEntrySelectedSpeciesSize > 0) {
          var croppingPlanSpecies = this.cpEntrySelected.species[cpEntrySelectedSpeciesSize - 1];
          if (!croppingPlanSpecies || !croppingPlanSpecies.speciesEspece) {
             this.cpEntrySelected.species.splice((cpEntrySelectedSpeciesSize -1), 1);
          }
        }
        this.editCroppingPlanSpecies(newSpecies);
      },

      confirmDeleteCrop(cpEntry) {
        const message = this.$t("messages.domain-edit-cropping-plan-confirm-crop-deletion");
        if (window.confirm(message)) {
          var indexOf = this.croppingPlans.indexOf(cpEntry);
          this.croppingPlans.splice(indexOf, 1);
          // remove species from speciesDistribution
          cpEntry.species.forEach(croppingPlanSpecies => {
            if (croppingPlanSpecies && croppingPlanSpecies.speciesId) {
              var key = (croppingPlanSpecies.code_espece_botanique + "_" + croppingPlanSpecies.code_qualifiant_AEE + "_" + croppingPlanSpecies.code_type_saisonier + "_" + croppingPlanSpecies.code_destination_aee).toLowerCase();
              key = key.replaceAll('undefined', '');
              delete this.speciesDistribution[key];
              delete this.speciesDistributionMap[key];
            }
          });

          this._computeSpeciesDistributionMap(true);
          this.drawSpeciesGraph();
        }
      },

      _computeSpeciesDistributionMap(checkForEmptySpecies) {
        this.speciesDistributionMap = {};

        this.croppingPlans.forEach(croppingPlanEntry => {
          if (croppingPlanEntry && this.isNotIntermediate(croppingPlanEntry) && croppingPlanEntry.species) {
            var emptySpecies = [];
            croppingPlanEntry.species.forEach(croppingPlanSpecies => {
              if (!croppingPlanSpecies || !croppingPlanSpecies.speciesId) {
                emptySpecies.push(croppingPlanSpecies);
              } else {
                var key = (croppingPlanSpecies.code_espece_botanique + "_" + croppingPlanSpecies.code_qualifiant_AEE + "_" + croppingPlanSpecies.code_type_saisonier + "_" + croppingPlanSpecies.code_destination_aee).toLowerCase();
                key = key.replaceAll('undefined', '');
                this.speciesDistributionMap[key] = {
                  speciesEspece: croppingPlanSpecies.speciesEspece,
                  speciesQualifiant: croppingPlanSpecies.speciesQualifiant,
                  speciesTypeSaisonnier: croppingPlanSpecies.speciesTypeSaisonnier,
                  key: key,
                  color: stringToColor(key)
                };
              }
              if (checkForEmptySpecies) {
                emptySpecies.forEach(toRemove => {
                  var index = croppingPlanEntry.species.indexOf(toRemove);
                  if (index !== -1) {
                    // remove species from crop
                    croppingPlanEntry.species.splice(index, 1);
                    // remove species from domainAreaSpeciesDistribution
                    var key = (toRemove.code_espece_botanique + "_" + toRemove.code_qualifiant_AEE + "_" + toRemove.code_type_saisonier + "_" + toRemove.code_destination_aee).toLowerCase();
                    key = key.replaceAll('undefined', '');
                    delete this.speciesDistribution[key];
                  }
                });
              }
            });
          }
        });

        let totalSpeciesDistributionSurface_ = 0.0;
        Object.keys(this.speciesDistribution).forEach(function (key) {
          var value = this.speciesDistribution[key];
          if (value) {
            if (isNaN(value)) {
              value = value.replaceAll(',', '.');
              let floatValue = parseFloat(value);
              this.speciesDistribution[key] = floatValue;
              totalSpeciesDistributionSurface_ += floatValue;
            } else {
              let floatValue = parseFloat(value);
              totalSpeciesDistributionSurface_ += floatValue;
            }
          }
        });
        this.totalSpeciesDistributionSurface = totalSpeciesDistributionSurface_;

      },

      deleteCroppingPlanSpecies(species) {
        const message = this.$t("messages.domain-edit-cropping-plan-confirm-species-deletion");
        if (window.confirm(message)) {

        }
        var indexOf = this.cpEntrySelected.species.indexOf(species);
        this.cpEntrySelected.species.splice(indexOf, 1);
        this.cropSpeciesEdition.species = null;
        this._computeSpeciesDistributionMap(true);
        this.drawSpeciesGraph();
        this._recomputeSpeciesAreas();
      },

      isNotIntermediate(elem) {
        return elem.type != 'INTERMEDIATE';
      },

      updateSpeciesDistributionMap(userKey, userValue) {
        if (userKey && userValue) {
          if (isNaN(userValue)) {
            let distribKeyValue = userValue.replaceAll(',', '.');
            let floatValue = parseFloat(distribKeyValue);
            this.speciesDistribution[userKey] = floatValue;
          } else {
            let floatValue = parseFloat(userValue);
            this.speciesDistribution[userKey] = floatValue;
          }
        }
        if (userKey && userValue === undefined || userValue === '' || userValue === 0) {
          delete this.speciesDistribution[userKey];
        }
        this._computeSpeciesDistributionMap(false);
        this.drawSpeciesGraph();
      },

      drawSpeciesGraph() {
        var datas = [];
        var backgroundColors = [];
        var labels = [];
        if (this.speciesDistributionMap) {
          Object.keys(this.speciesDistributionMap).forEach(distribKey => {
            let value = this.speciesDistributionMap[distribKey];
            let distribKeyValue = this.speciesDistribution[distribKey];
            if (value && distribKeyValue) {
              let labelTmp = value.speciesEspece;
              labelTmp += value.speciesQualifiant ? " " + value.speciesTypeSaisonnier : "";
              var label = labelTmp + (value.speciesTypeSaisonnier ? " " + value.speciesTypeSaisonnier : "");
              labels.push(label);
              datas.push(distribKeyValue);
              backgroundColors.push(value.color);
            }
          });
        }
        var croppingPlanSpeciesDistributionChartDiv = this.$refs.croppingPlanSpeciesDistributionChart;
        if (typeof(G_vmlCanvasManager) != 'undefined') { // ie IE
          G_vmlCanvasManager.initElement(croppingPlanSpeciesDistributionChartDiv);
        }

        if (croppingPlanSpeciesDistributionChartDiv) {
          //var croppingPlanSpeciesDistributionChartContext = croppingPlanSpeciesDistributionChartDiv.getContext("2d");
          new Chart(croppingPlanSpeciesDistributionChartDiv, {
            type: 'pie',
            data: {
              labels: labels,
              datasets: [{
                data: datas,
                backgroundColor: backgroundColors
              }]
            },
            options: {
              legend: {
                display: false
              }
            }
          });
        }
      },

      initCropTab() {
        if (!this.croppingPlans) {
          displayPageLoading();
          return fetch(ENDPOINT_DOMAINS_EDIT_CROPS_CONTEXT_JSON + "?domainId=" + encodeURIComponent(this.domain.topiaId))
            .then(response => response.json())
            .then(data => {
              this.croppingPlans = data["croppingPlans"];
              this.croppingPlansUsageMap = data["croppingPlansUsageMap"];
              var croppingPlanSpeciesUsageMap = data["croppingPlanSpeciesUsageMap"];
              this.croppingPlanSpeciesUsageList = croppingPlanSpeciesUsageMap || {};

              this._computeSpeciesDistributionMap(false);
              this.drawSpeciesGraph();
              hidePageLoading();
            })
            .catch(function(response) {
                var message = "Échec de récupération des cultures";
                console.error(message, response);
                addPermanentError(message, response.status);
            });
        } else {
          this.drawSpeciesGraph();
        }
      },

      isSpeciesDistributionMapEmpty() {
        return !this.speciesDistributionMap || Object.keys(this.speciesDistributionMap).length <= 0;
      },

      _initResearchFields() {
        this.cropSpeciesEdition.species = null;
        this.cropSpeciesEdition.variety = null;
        // add empty choice
        // species research
        this.cropSpeciesEdition.cpes_species = [{ speciesId: null, speciesEspece : "", label: "" }];
        // cropSpeciesEdition.variety research
        this.cropSpeciesEdition.varieties = [{speciesId: null, varietyLibelle : ""}];
      },

      setLabelForSpecies(species) {
        if (species) {
          let label = species.speciesEspece;
          if (species.speciesQualifiant) {
            label += ', ' + species.speciesQualifiant;
          }
          if (species.speciesTypeSaisonnier) {
            label += ', ' + species.speciesTypeSaisonnier;
          }
          if (species.speciesDestination) {
            label += ', ' + species.speciesDestination;
          }
          species.label = label;
        }
      },

      refreshSpeciesList(speciesPartName) {
        this.cropSpeciesEdition.autocompleteTypedText = speciesPartName;
        this.cropSpeciesEdition.isFetchingSpecies = true;
        return fetch(ENDPOINTS.listSpeciesJson, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: "term=" + encodeURIComponent(speciesPartName)
        })
        .then(response => response.json())
        .then(data => {
          var transformedData = data.map(species => {
            var result = {
              speciesId : species.topiaId,
              speciesEspece : species.libelle_espece_botanique_Translated,
              speciesQualifiant : species.libelle_qualifiant_AEE_Translated,
              speciesTypeSaisonnier : species.libelle_type_saisonnier_AEE_Translated,
              speciesDestination : species.libelle_destination_AEE_Translated,
              code_espece_botanique : species.code_espece_botanique,
              code_qualifiant_AEE : species.code_qualifiant_AEE,
              code_type_saisonier : species.code_type_saisonier,
              code_destination_aee : species.code_destination_aee
            };
            this.setLabelForSpecies(result);
            return result;
          });
          this.cropSpeciesEdition.cpes_species = transformedData;
        })
        .catch(function(response) {
            var message = "Échec de récupération des espèces";
            console.error(message, response);
            addPermanentError(message, response.status);
        })
        .finally(() => {
          this.cropSpeciesEdition.isFetchingSpecies = false;
        });
      },

      refreshVarietiesList(speciesPartName) {
        this.cropSpeciesEdition.autocompleteTypedText = speciesPartName;
        var speciesId;
        if (this.cropSpeciesEdition.species) {
          speciesId  = this.cropSpeciesEdition.species.speciesId;
        }

        if (speciesId) {
          return fetch(ENDPOINTS.listVarietiesJson, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: "term=" + encodeURIComponent(speciesPartName) + "&speciesId=" + encodeURIComponent(speciesId)
          })
          .then(response => response.json())
          .then(data => {
            var transformedData = data.map(variety => {
              return {
                 varietyId: variety.topiaId,
                 varietyLibelle: variety.denomination || variety.variete
              };
            });

            if (transformedData.length === 1) {
              if (this.cropSpeciesEdition.species) {
                 this.cropSpeciesEdition.species.varietyLibelle = transformedData[0].varietyLibelle;
                 this.cropSpeciesEdition.species.varietyId = transformedData[0].varietyId;
                 this.cropSpeciesEdition.variety = transformedData[0];
              }
            }
            this.cropSpeciesEdition.varieties = transformedData;
          })
          .catch(function(response) {
              var message = "Échec de récupération des variétés";
              console.error(message, response);
              addPermanentError(message, response.status);
          });
        }
      },

      // DOMAINE

      toSameDomainCropCopy() {
        if (this.filteredSelectedCrops.length) {
          var cropByCodes = {};
          this.croppingPlans.forEach(crop => { cropByCodes[crop.code] = crop });

          this.filteredSelectedCrops.forEach(cropCode => {
            var crop = cropByCodes[cropCode];
            if (crop) {
              var copiedCrop = cloneObject(crop);
              copiedCrop.species.forEach(cps => {
                cps.topiaId = null;
                cps.code = guid();
                return cps;
              });
              copiedCrop.topiaId = null;
              copiedCrop.code = guid();
              this.croppingPlans.push(copiedCrop);
            }
          });
        }
      },

      processCropsCopy(toSameDomain, toOtherDomains) {
        if (toOtherDomains.length > 0) {
          var selectedCrops = [];
          var cropByCodes = {};
          this.croppingPlans.forEach(crop => cropByCodes[crop.code] = crop);
          this.filteredSelectedCrops.forEach(cropCode => {
            var crop = cropByCodes[cropCode];
            if (crop) {
              selectedCrops.push(crop);
            }
          });
          var ajaxRequest = "fromDomain=" + encodeURIComponent(this.domain.topiaId) +
                            "&toDomains=" + encodeURIComponent(JSON.stringify(toOtherDomains)) +
                            "&croppingPlansJson=" + encodeURIComponent(JSON.stringify(selectedCrops));

          fetch(ENDPOINTS.domainCropsCopy, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: ajaxRequest
          })
          .then(response => response.json())
          .then(data => {
            var domainPluralize = this.$tc("messages.domain-edit-cropping-plan-paste-domain", toOtherDomains.length);
            var tcPluralize = this.$tc("messages.domain-edit-cropping-plan-paste-crops", selectedCrops.length);
            var message = tcPluralize + "nbsp;: " + domainPluralize;

            var targetedDomain = "";
            data.forEach(domainName => targetedDomain += " " + domainName + ",");
            targetedDomain = targetedDomain.substring(0, targetedDomain.length - 1);
            targetedDomain += ".";

            // make sure copy on same domain is done after copy to other domains
            if (toSameDomain) {
              this.toSameDomainCropCopy();
            }
            addSuccessMessage(message + targetedDomain);
          })
          .catch(function(response) {
            var message = "Échec de copie des cultures";
            if (response.data === '"IllegalArgumentException"') {
              message += "&nbsp:Les cultures ne sont pas valides et ne peuvent être copiées.";
            }
            console.error(message, response);
            addPermanentError(message, response.status);
          });
        } else if (toSameDomain) {
          this.toSameDomainCropCopy();
        }
      },

      confirmDomainTargetsToPaste(selectedDomains) {
        var fromDomain = this.domain.topiaId;
        var toOtherDomains = [];
        var toSameDomain = false;
        selectedDomains.forEach(domain => {
          if (domain === fromDomain) {
            toSameDomain = true;
          } else {
            toOtherDomains.push(domain);
          }
        });
        // process copy
        this.processCropsCopy(toSameDomain, toOtherDomains);
      },

      copyCrops() {
        this.copyDialogContext = {
          title: this.$t('messages.domain-edit-cropping-plan-table-copy-paste'),
          type: "CROPS",
          callback: this.confirmDomainTargetsToPaste,
          close: () => this.copyDialogContext = null
        };
      }
    }
  })
  .component('GenericModal', AgrosystVueComponents.Common.getGenericModal())
  .component('DomainsToPaste', AgrosystVueComponents.Domains.getDomainsToPaste({domainTypes: data.domainTypes}))
  .component('DomainCroppingPlansSpeciesDistributionSection', AgrosystVueComponents.Domains.getDomainCroppingPlansSpeciesDistributionSection())
  .use(AgrosystVueComponents.i18n)
  .use(Oruga.Config, { iconPack: "fa", mobileBreakpoint: "480px" })
  .use(Oruga.Autocomplete)
  .use(Oruga.Button)
  .use(Oruga.Checkbox)
  .use(Oruga.Field)
  .use(Oruga.Input)
  .use(Oruga.Modal)
  .use(Oruga.Select);
  controller.config.globalProperties.$filters = AgrosystVueComponents.filters;
  controller.directive('integer', (el, binding) => {
    var val = el.children[0].value;
    var pattern = /^\d+$/;
    if (val && val.length > 1 && !pattern.test(val)) {
      el.children[0].setAttribute("isvalid", "false")
    }
  })
  return controller;
}


