/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainLivestockUnitsController = function (data) {
  const controller = Vue.createApp({
    data() {
      return {
        domain: data.domain,
        livestockUnits: [],
        forFrance: data.forFrance,
        frontApp: data.frontApp,
        editedLivestockUnit: null,
        cattlesToDuplicate: null,
        halfMonths: Array.from(Array(24).keys()),
        loadingLiveStocks: false,
        cattleUsages: [],
        livestockUnitUsages: {}
      }
    },
    computed: {

    },
    watch: {

    },
    methods: {
      filteredRefCattleAnimalTypes(refAnimalType) {
        if (refAnimalType) {
          return this.refCattleAnimalTypes.filter(rcat => rcat.refAnimalType.topiaId === refAnimalType.topiaId)
        }
      },
  
      filteredHalfMonths(bound, before) {
        return this.halfMonths.filter(halfMonth => !bound || (before ? halfMonth <= bound : halfMonth >= bound));
      },
  
      _getTemporaryId() {
        return '_' + generateUUID();
      },
  
      _isTemporaryId(id) {
        return id.startsWith('_');
      },
  
      _updateAlimentAndRationCounts() {
        var lastCattle;
        var lastRation;
        var rationAndAlimentCount = {};
  
        this.editedLivestockUnit.cattles.forEach(cattle => {
          if (!lastCattle || lastCattle.cattleId != cattle.cattleId) {
            lastCattle = cattle;
            lastCattle.cattleAlimentCount = 0;
            lastCattle.cattleRationCount = 0;
            rationAndAlimentCount = {};
          }
          lastCattle.cattleAlimentCount++;
          if (!lastRation || lastRation.rationId != cattle.rationId) {
            lastRation = cattle;
            lastRation.rationAlimentCount = 0;
            lastCattle.cattleRationCount++;
          }
          lastRation.ration = lastCattle.cattleRationCount;
          lastRation.rationAlimentCount++;
  
          rationAndAlimentCount.nbAlimentCount = lastCattle.cattleAlimentCount;
          rationAndAlimentCount.nbCattleRationCount = lastCattle.cattleRationCount;
          cattle.rationAndAlimentCount = rationAndAlimentCount;
        });
      },
  
      getCattlesTooltip(cattles) {
        if (!cattles.length) {
          return;
        }
        return '- ' + cattles.map(c => c.animalType.animalType).join('\n- ');
      },
  
      addLiveStock() {
        this.editedLivestockUnit = {
          index : -1,
          cattles: []
        };
        if (!this.livestockUnits) {
          this.livestockUnits = [];
        }
      },
  
      isMissingRequiredCattleField() {
        return this.editedLivestockUnit && this.editedLivestockUnit.cattles.some(cattle => !cattle.animalType || !cattle.alimentType);
      },
  
      editLivestockUnit(index, livestockUnit) {
        this.editedLivestockUnit = Object.assign({}, livestockUnit);
        var cattles = this.editedLivestockUnit.cattles;
  
        var flattenedCattles = [];
        if (cattles) {
          for (var i in cattles) {
            var cattle = cattles[i];
            var searchedAnimalTypeTopiaId = cattle.animalType && cattle.animalType.topiaId;
            var cattleType = null;
            if (searchedAnimalTypeTopiaId) {
              cattleType = this.refCattleAnimalTypes.find(function(refCattleAnimalType) {
                return refCattleAnimalType.topiaId === searchedAnimalTypeTopiaId;
              });
            }
            for (var j in cattle.rations) {
              var ration = cattle.rations[j];
              for (var k in ration.aliments) {
                var aliment = ration.aliments[k];
                var searchedAlimentTopiaId = aliment.aliment && aliment.aliment.topiaId;
                var alimentType = null;
                if (searchedAlimentTopiaId) {
                  alimentType = this.refCattleRationAliments.find(function(refCattleRationAliment) {
                    return refCattleRationAliment.topiaId === searchedAlimentTopiaId;
                  });
                }
                var row = {
                  cattleId: cattle.topiaId || cattle.cattleId || this._getTemporaryId(),
                  code: cattle.code,
                  rationId: ration.topiaId || ration.rationId || this._getTemporaryId(),
                  alimentId: aliment.topiaId || aliment.alimentId || this._getTemporaryId(),
                  animalType: cattleType,
                  numberOfHeads: cattle.numberOfHeads,
                  ration: Number.parseInt(j, 10) + 1,
                  startingHalfMonth: ration.startingHalfMonth,
                  endingHalfMonth: ration.endingHalfMonth,
                  alimentType: alimentType,
                  alimentQuantity: aliment.quantity,
                  cattleAlimentCount: 0,
                  cattleRationCount: 0,
                  rationAlimentCount: 0
                };
                flattenedCattles.push(row);
              }
            }
          }
        }
        this.editedLivestockUnit.cattles = flattenedCattles;
        this._updateAlimentAndRationCounts();
  
        this.editedLivestockUnit.index = index;
        if (this.editedLivestockUnit.refAnimalType) {
          var searchedTopiaId = this.editedLivestockUnit.refAnimalType.topiaId;
          this.editedLivestockUnit.refAnimalType = this.refAnimalTypes.find(function(refAnimalType) {
            return refAnimalType.topiaId === searchedTopiaId;
          });
        }
      },
  
      addCattle() {
        if (this.editedLivestockUnit && this.editedLivestockUnit.refAnimalType) {
          this.editedLivestockUnit.cattles.push({
            cattleId: this._getTemporaryId(),
            code: null,
            rationId: this._getTemporaryId(),
            alimentId: this._getTemporaryId(),
            ration: 1,
            cattleAlimentCount: 0,
            cattleRationCount: 0,
            rationAlimentCount: 0
          });
          this._updateAlimentAndRationCounts();
        }
      },
  
      addRation(cattle) {
        if (this.editedLivestockUnit) {
          var index = this.editedLivestockUnit.cattles.lastIndexOf(cattle);
          this.editedLivestockUnit.cattles.splice(index + cattle.cattleAlimentCount, 0, {
            cattleId: cattle.cattleId,
            rationId: this._getTemporaryId(),
            alimentId: this._getTemporaryId(),
            animalType: cattle.animalType,
            numberOfHeads: cattle.numberOfHeads,
            ration: cattle.cattleRationCount + 1,
            cattleAlimentCount: 0,
            cattleRationCount: 0,
            rationAlimentCount: 0
          });
          this._updateAlimentAndRationCounts();
        }
      },
  
      addAliment(cattle) {
        if (this.editedLivestockUnit) {
          var index = this.editedLivestockUnit.cattles.lastIndexOf(cattle);
          this.editedLivestockUnit.cattles.splice(index + cattle.rationAlimentCount, 0, {
            cattleId: cattle.cattleId,
            code: cattle.code,
            rationId: cattle.rationId,
            alimentId: this._getTemporaryId(),
            animalType: cattle.animalType,
            numberOfHeads: cattle.numberOfHeads,
            startingHalfMonth: cattle.startingHalfMonth,
            endingHalfMonth: cattle.endingHalfMonth,
            ration: cattle.ration,
            cattleAlimentCount: 0,
            cattleRationCount: 0,
            rationAlimentCount: 0
          });
          this._updateAlimentAndRationCounts();
        }
      },
  
      deleteCattle(cattle) {
        if (this.editedLivestockUnit) {
          this.editedLivestockUnit.cattles = this.editedLivestockUnit.cattles.filter(c => c.cattleId != cattle.cattleId);
          this._updateAlimentAndRationCounts();
        }
      },
  
      deleteRation(cattle) {
        if (this.editedLivestockUnit && (!this.cattleUsages[cattle.cattleId] || cattle.rationAndAlimentCount.nbCattleRationCount > 1)) {
          this.editedLivestockUnit.cattles = this.editedLivestockUnit.cattles.filter(c => c.rationId != cattle.rationId);
          this._updateAlimentAndRationCounts();
        }
      },
  
      deleteAliment(cattle) {
        if (this.editedLivestockUnit && (!this.cattleUsages[cattle.cattleId] || cattle.rationAndAlimentCount.nbAlimentCount > 1)) {
          this.editedLivestockUnit.cattles = this.editedLivestockUnit.cattles.filter(c => c.alimentId != cattle.alimentId);
          this._updateAlimentAndRationCounts();
        }
      },
  
      getDestinationCattleLabel(cattle) {
        var label = "[" + cattle.livestockUnit.refAnimalType.animalType + "] " + (cattle.animalType && cattle.animalType.animalType || "");
        if (cattle.numberOfHeads) {
          label += ' (' + cattle.numberOfHeads + ')';
        }
        return label;
      },
  
      canDuplicateRation() {
        return this.livestockUnits.length > 1 || new Set(this.editedLivestockUnit.cattles.map(cattle => cattle.cattleId)).size > 1;
      },
  
      duplicateRation(cattle) {
        this._chooseRationDestination(this.editedLivestockUnit.cattles.filter(c => c.rationId === cattle.rationId));
      },
  
      duplicateRations(cattle) {
        this._chooseRationDestination(this.editedLivestockUnit.cattles.filter(c => c.cattleId === cattle.cattleId));
      },
  
      _chooseRationDestination(cattles) {
        var cattleIds = cattles.map(cattle => cattle.cattleId);
        this.destinationCattles = [];
        let livestockUnits = Array.from(this.livestockUnits);
        if (!livestockUnits.includes(this.editedLivestockUnit)) {
          livestockUnits.push(this.editedLivestockUnit);
        }
        livestockUnits.forEach(livestockUnit => {
          if (livestockUnit.topiaId === this.editedLivestockUnit.topiaId) {
            this.editedLivestockUnit.cattles.forEach(cattle => {
              if (!cattleIds.includes(cattle.cattleId) && this.destinationCattles.every(c => cattle.cattleId != c.cattleId)) {
                this.destinationCattles.push({...cattle, livestockUnit});
              }
            });
          } else {
            livestockUnit.cattles.forEach(cattle => {
              if (livestockUnit.topiaId != this.editedLivestockUnit.topiaId || !cattleIds.includes(cattle.topiaId)) {
                this.destinationCattles.push({...cattle, livestockUnit});
              }
            });
          }
        });
        this.cattlesToDuplicate = cattles;
      },

      chooseRationDestination() {
        if (this.destinationCattle) {
          let cattle = this.destinationCattle.livestockUnit.cattles.find(cattle => (this.destinationCattle.topiaId || this.destinationCattle.cattleId) === (cattle.topiaId || cattle.cattleId));
          this.cattlesToDuplicate.forEach(ration => {
            if (this.destinationCattle.livestockUnit.topiaId === this.editedLivestockUnit.topiaId) {
              let lastDestinationCattle = this.editedLivestockUnit.cattles
                                                .filter(c => c.cattleId === this.destinationCattle.cattleId)
                                                .sort((c1, c2) => c2.ration - c1.ration)[0];

              if (!lastDestinationCattle.startingHalfMonth && !lastDestinationCattle.endingHalfMonth && !lastDestinationCattle.alimentType) {
                lastDestinationCattle.alimentType = ration.alimentType;
                lastDestinationCattle.alimentQuantity = ration.alimentQuantity;
                lastDestinationCattle.startingHalfMonth = ration.startingHalfMonth;
                lastDestinationCattle.endingHalfMonth = ration.endingHalfMonth;
                lastDestinationCattle.sourceRationId = ration.rationId;

              } else {
                let lastDestinationCattleIndex = this.editedLivestockUnit.cattles.indexOf(lastDestinationCattle) + lastDestinationCattle.rationAlimentCount + 1;
                var rationId;
                var rationIndex;
                if (lastDestinationCattle.sourceRationId === ration.rationId) {
                  rationId = lastDestinationCattle.rationId;
                  rationIndex = lastDestinationCattle.ration;
                } else {
                  rationId = this._getTemporaryId();
                  rationIndex = lastDestinationCattle.ration + 1;
                }
                this.editedLivestockUnit.cattles.splice(lastDestinationCattleIndex, 0, {
                  alimentId: this._getTemporaryId(),
                  alimentQuantity: ration.alimentQuantity,
                  alimentType: ration.alimentType,
                  animalType: lastDestinationCattle.animalType,
                  cattleId: lastDestinationCattle.cattleId,
                  code: this.destinationCattle.code,
                  endingHalfMonth: ration.endingHalfMonth,
                  numberOfHeads: lastDestinationCattle.numberOfHeads,
                  rationId: rationId,
                  sourceRationId: ration.rationId,
                  ration: rationIndex,
                  startingHalfMonth: ration.startingHalfMonth,
                  cattleAlimentCount: 0,
                  cattleRationCount: 0,
                  rationAlimentCount: 0
                });
              }
              this._updateAlimentAndRationCounts();

            } else {
              let cattleRation;
              if (cattle.rations.length === 1
                    && !cattle.rations[0].startingHalfMonth
                    && !cattle.rations[0].endingHalfMonth
                    && !cattle.rations[0].aliments.length) {

                cattleRation = cattle.rations[0];

              } else {
                cattleRation = cattle.rations.find(cattleRation => ration.rationId === cattleRation.rationId);
              }

              if (!cattleRation) {
                cattleRation = {
                  topiaId: null,
                  rationId: ration.rationId,
                  startingHalfMonth: ration.startingHalfMonth,
                  endingHalfMonth: ration.endingHalfMonth,
                  aliments: []
                };
                cattle.rations.push(cattleRation);
              }
              cattleRation.aliments.push({
                topiaId: null,
                alimentId: this._getTemporaryId(),
                aliment: ration.alimentType,
                quantity: ration.alimentQuantity
              });
            }
          });
          this.cattlesToDuplicate = null;
        }
      },

      closeChooseRationDestinationModal() {
        this.cattlesToDuplicate = null;
      },
  
      stopEditLivestockUnit() {
        var livestockUnit = this.editedLivestockUnit;
        var cattles = {};
        for (var i in livestockUnit.cattles) {
          var cattle = livestockUnit.cattles[i];
          var c = cattles[cattle.cattleId];
          if (!c) {
            c = {
              topiaId: this._isTemporaryId(cattle.cattleId) ? null : cattle.cattleId,
              cattleId: cattle.cattleId,
              code: cattle.code,
              animalType: cattle.animalType,
              numberOfHeads: cattle.numberOfHeads,
              rations: {}
            };
            cattles[cattle.cattleId] = c;
          }
          var r = c.rations[cattle.rationId];
          if (!r) {
            r = {
              topiaId: this._isTemporaryId(cattle.rationId) ? null : cattle.rationId,
              rationId: cattle.rationId,
              startingHalfMonth: cattle.startingHalfMonth,
              endingHalfMonth: cattle.endingHalfMonth,
              aliments: []
            };
            c.rations[cattle.rationId] = r;
          }
          r.aliments.push({
            topiaId: this._isTemporaryId(cattle.alimentId) ? null : cattle.alimentId,
            alimentId: cattle.alimentId,
            aliment: cattle.alimentType,
            quantity: cattle.alimentQuantity
          });
        }
        for (var i in cattles) {
          cattles[i].rations = Object.values(cattles[i].rations);
        }
        cattles = Object.values(cattles);
        livestockUnit.cattles = cattles;
  
        if (livestockUnit.refAnimalType) {
          if (livestockUnit.index === -1) {
            this.livestockUnits.push(livestockUnit);
          } else {
            this.livestockUnits[livestockUnit.index] = livestockUnit;
          }
          livestockUnit.index = null;
          this.editedLivestockUnit = null;
  
        } else {
          addPermanentError("Le type d'animaux de l'atelier d’élevage n'est pas renseigné");
        }
      },
  
      cancelEditLivestockUnit() {
        this.editedLivestockUnit = null;
      },
  
      removeLivestockUnit(index) {
        this.livestockUnits.splice(index, 1);
      }
    },
    mounted() {
      this.isLivestockTabSelected = true;

      var domainId = !this.domain.topiaId ? null : encodeURIComponent(this.domain.topiaId);
      this.loadingLiveStocks = true;
      return fetch(ENDPOINT_LOAD_LIVESTOCKS_AND_CATTLES_JSON + "?domainId=" + domainId)
        .then(response => response.json())
        .then(data => {
          var livestocksAndCattles = data["LivestocksAndCattles"];
          if (livestocksAndCattles) {
            var livestockUnitData = livestocksAndCattles["fr.inra.agrosyst.api.entities.LivestockUnit"];
            var cattleData = livestocksAndCattles["fr.inra.agrosyst.api.entities.Cattle"];
          }

          if (livestockUnitData) {
            this.livestockUnits = livestockUnitData.elements;
            this.livestockUnitUsages = livestockUnitData.usageMap || {};
          }

          if (cattleData) {
            this.cattleUsages = cattleData.usageMap;
          }

          this.refAnimalTypes = data["fr.inra.agrosyst.api.entities.referential.RefAnimalType"];
          this.refCattleAnimalTypes = data["fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType"];
          this.refCattleRationAliments = data["fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment"];

          this.loadingLiveStocks = false;
        }).
        catch(function(response) {
          console.error("Can't load Livestock", response);
          var message = "Échec de récupération des ateliers d'élevage";
          addPermanentError(message, response.status);
          this.loadingLiveStocks = false;
        });
    }
  })
  .component('GenericModal', AgrosystVueComponents.Common.getGenericModal())
  .use(AgrosystVueComponents.i18n)
  .use(Oruga.Config, { iconPack: "fa", mobileBreakpoint: "480px" })
  .use(Oruga.Icon)
  .use(Oruga.Button)
  .use(Oruga.Field)
  .use(Oruga.Input)
  .use(Oruga.Select)
  .use(Oruga.Modal);

  return controller;
}
