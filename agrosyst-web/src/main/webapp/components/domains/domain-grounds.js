/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getDomainSolsListController = function (context) {
  const controller = Vue.createApp({
  })
  .component('DomainGrounds', AgrosystVueComponents.Domains.getDomainGrounds(context))
  .component('GenericModal', AgrosystVueComponents.Common.getGenericModal())
  .use(AgrosystVueComponents.i18n)
  .use(Oruga.Config, {iconPack: "fa"})
  .use(Oruga.Icon)
  .use(Oruga.Button)
  .use(Oruga.Field)
  .use(Oruga.Input)
  .use(Oruga.Select)
  .use(Oruga.Switch)
  .use(Oruga.Modal);


    /**angular.forEach(DomainSolArvalisRegions, function(value, key) {
        // parseInt because map converter by gson use objects ? (not sure of that)
        this.domainSolArvalisRegions.push({code: parseInt(key), name: value});
    });

    if (this.domain && this.domain.location) {
        this.defautSolArvalisRegion = this.domain.location.region;
        this.solArvalisRegion = this.defautSolArvalisRegion;
        this.solArvalisRegionSelected(this.solArvalisRegion);
    }**/

  return controller;
}

window.AgrosystVueComponents.Domains.getDomainGrounds = function (context) {
  return {
    template: '#domainGrounds',
    data() {
      return {
        domain: context.domain,
        domainSols: context.domainSols,
        forFrance: context.forFrance,
        domainSolArvalisRegions: context.solArvalisRegions,
        domainSolArvalisArray: null,
        selectedRegion : null,
        selectedTypeSolArvalisId: null,
        editedSol: null
      };
    },

    computed: {
      modalTitle() {
        return 'messages.domain-edit-sols-' + (this.editedSol && this.editedSol._newSoil ? 'ajouter' : 'modifier') + 'sol';
      },
      validationDisabled() {
        return !this.editedSol ||
               !this.selectedRegion ||
               !this.editedSol.refSolArvalis ||
               !this.editedSol.importance ||
               this.editedSol.importance < 0 ||
               this.editedSol.importance > 100
        ;
      }
    },

    watch: {
      'editedSol.refSolArvalis'(newTypeSol, oldTypeSol) {
        if (newTypeSol !== oldTypeSol) {
          this.typeSolArvalisSelected(newTypeSol);
        }
      }
    },

    methods: {
      addSol() {
        this.selectedRegion = this.getDefaultSolArvalisRegion();
        this.solArvalisRegionSelected();
        this.editedSol = {
           refSolArvalis: {},
          _newSoil: true
        }
      },

      getDefaultSolArvalisRegion() {
        if (!this.domain || !this.domain.location) {
          return -1;
        }
        return this.domain.location.region;
      },

      editSol(element) {
        const index = this.domainSols.indexOf(element);
        if (index >= 0) {
          displayPageLoading();
          let solArvalisRegion = parseInt(element.refSolArvalis.sol_region_code);
          return fetch(ENDPOINT_DOMAINS_EDIT_SOL_ARVALIS_LIST_JSON + "?regionCode=" + encodeURIComponent(solArvalisRegion))
            .then(response => response.json())
            .then(data => {
              this.domainSolArvalisArray = data;
              this.selectedRegion = element.refSolArvalis.sol_region_code;
              this.editedSol = cloneObject(element);
              let refId = this.editedSol.refSolArvalis.topiaId;
              this.editedSol.refSolArvalis = this.domainSolArvalisArray.filter(ref => refId === ref.topiaId)[0];
              this.editedSol._index = index;
          })
          .catch(function(response) {
            var message = "Échec de récupération des sols Arvalis";
            console.error(message, response);
            addPermanentError(message, response.status);
          })
          .finally(hidePageLoading);
        }
      },

      saveSol() {
        if (this.editedSol._newSoil) {
          this.domainSols.push({ ...this.editedSol, _newSoil: false });
        } else {
          const index = this.editedSol._index;
          //this.editedSol._index = null;

          Object.assign(this.domainSols[index], this.editedSol);
        }
        this.editedSol = null;
      },

      async solArvalisRegionSelected() {
        if (this.selectedRegion) {
          displayPageLoading();
          try {
            const awaitResponse = await fetch(ENDPOINT_DOMAINS_EDIT_SOL_ARVALIS_LIST_JSON + "?regionCode=" + encodeURIComponent(this.selectedRegion));
            if (awaitResponse.ok) {
              this.domainSolArvalisArray = await awaitResponse.json();
              this.selectedTypeSolArvalisId = "";
              if (this.editedSol && this.editedSol.topiaId) {
                this.editedSol.refSolArvalis = {};
              }
              hidePageLoading();
            }
            else {
              throw new Error(response.statusText);
            }

          } catch(response) {
            var message = "Échec de récupération des sols Arvalis";
            console.error(message, response);
            addPermanentError(message, response.status);
            hidePageLoading();
          };
        }
      },

      typeSolArvalisSelected(selectedTypeSol) {
        if (selectedTypeSol) {
          Object.assign(this.editedSol.refSolArvalis, selectedTypeSol);
          // auto selected type sol name if current sol name is empty
          if (!this.editedSol.name) {
            this.editedSol.name = selectedTypeSol.sol_nom_Translated;
          }
        }
      },
  
      deleteSol(element) {
        if (window.confirm(this.$t("messages.domain-edit-sols-delete-confirm"))) {
          let indexOf = this.domainSols.indexOf(element);
          this.domainSols.splice(indexOf, 1);
          // TODO this.domainEditForm.$setDirty();
        }
      },

      getSoilName(soil) {
        let name = soil.name;
        if (name) {
          name += " ";
        }
        name += "[" + soil.refSolArvalis.sol_nom_Translated + "]";
        return name;
      }
    }
  }
}

