/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
window.AgrosystVueComponents.Domains.getWeatherStationsTable = function(data) {
  return {
    data() {
      return {
        // list of weather station town
        weatherStationSitesIdsAndNames: data.weatherStationSitesIdsAndNames,
        weatherStationSitesIdsAndNamesList: [],
        weatherStations: data.weatherStations || []
      }
    },

    computed: {
      defaultSelectedWeatherStations() {
        return this.weatherStations.filter(ws => ws.defaultSelected);
      }
    },

    methods: {
      setEditedWeatherStationIndex(index) {
        this.editedWeatherStationIndex = index;
      },

      addWeatherStation() {
        const weatherStation = {};
        this.weatherStations.push(weatherStation);
        this.editedWeatherStationIndex = this.weatherStations.indexOf(weatherStation);
      },

      removeWeatherStation(index) {
        if (confirm("Êtes-vous sûr de vouloir supprimer cette station ?")) {
          this.weatherStations.splice(index, 1);
          //TODO this.domainEditForm.$setDirty();
        }
      },

      // disable default on all other weather stations
      selectDefaultWeatherStation(index) {
        for (var i = 0; i < this.weatherStations.length; i++) {
          if (index !== i) {
            this.weatherStations[i].defaultSelected = false;
          }
        }
      }
    },

    mounted() {
      Object.entries(this.weatherStationSitesIdsAndNames).forEach(([key, value]) => {
          var elem = { topiaId: key, name: value };
          this.weatherStationSitesIdsAndNamesList.push(elem);
      });
      if (!this.weatherStations) {
        this.weatherStations = [];
      }
    },

    template: '#domain-weatherstations'
  }
}
