/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Delete selected domains after user confirm.
 */
function unactivateUsers(unactivateDialog, unactivateForm, activate) {
    var selectionCount = JSON.parse(unactivateForm.children("input[name='userIds']").val()).length;
    if (selectionCount > 0) {
        unactivateDialog.dialog({
            resizable: false,
            width: 350,
            modal: true,
            buttons: {
                action: {
                  click: function() {
                    $(this).dialog("close");
                    unactivateForm.attr("action", activate ? ENDPOINTS.usersUnactivate+"?activate=true" : ENDPOINTS.usersUnactivate);
                    unactivateForm.submit();
                  },
                  text: activate ? "Activer" : "Désactiver",
                  'class': 'btn-primary'
                },
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}

/**
 * Import user file after confirm.
 */
function importUsers(importDialog) {
  importDialog.dialog({
      resizable: false,
      width: 500,
      modal: true,
      buttons: {
          action: {
            click: function() {
              $(this).find("form").submit();
            },
            text: "Importer",
            'class': 'btn-primary'
          },
          cancel: {
            click: function() {
              $(this).dialog("close");
            },
            text: 'Annuler',
            'class': 'float-left btn-secondary'
          }
      }
  });
}

/**
 * Contrôlleur de la liste des utilisateurs
 */
AgrosystModule.controller('UsersListController', ['$scope', '$http', '$timeout', '$filter', 'usersInitData',
  function($scope, $http, $timeout, $filter, usersInitData) {
    $scope.users = usersInitData.users.elements;
    $scope.pager = usersInitData.users;
    $scope.roleTypes = $scope.i18n.RoleType;
    $scope.filter = usersInitData.usersFilter;
    //XXX ymartel 2014/02/18 : should be convert Boolean to String for binding until resolution of https://github.com/angular/angular.js/issues/6297
    if (angular.isDefined($scope.filter.active)) {
      $scope.filter.active = $scope.filter.active.toString();
    }

    $scope.selectedUsers = {};
    $scope.selectedUsersEntities = {};
    //$scope.firstSelectedUser;
    //$scope.allSelectedUserActive;

    $scope.refresh = function(newValue, oldValue) {
        if (newValue === oldValue) { return; } // prevent init useless call

        // remove empty value without modifying initial model
        var localFilter = angular.copy($scope.filter);
        if (localFilter.active === '') {
            delete localFilter.active;
        }
        if (localFilter.roleType === '') {
            delete localFilter.roleType;
        }
        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
        $http.post(ENDPOINTS.usersListJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
        then(function(response) {
            $scope.users = response.data.elements;
            $scope.pager = response.data;
        });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.firstName, filter.lastName, filter.email, filter.organisation]',
      function(newValue, oldValue) {
        if (timer) {
            $timeout.cancel(timer);
        }
        timer = $timeout(function(){
          let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
        }, 400);
       }, true);
    // watch without timer
    $scope.$watch('[filter.active, filter.roleType]', function(newValue, oldValue) {
      let page = $scope.filter.page;
      $scope.filter.page = 0;
      if (!page) {
        $scope.refresh(newValue, oldValue);
      }
    }, true);
    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.$watch('selectedUsers', function(newValue, oldValue) {
        // save users entities
        angular.forEach($scope.users, function(elt) {
            if ($scope.selectedUsers[elt.topiaId]) {
                $scope.selectedUsersEntities[elt.topiaId] = elt;
            } else {
                delete $scope.selectedUsersEntities[elt.topiaId];
            }
        });
        // active property and first selected
        $scope.allSelectedUserActive = true;
        angular.forEach($scope.selectedUsersEntities, function(user) {
            $scope.allSelectedUserActive &= user.active;
            $scope.firstSelectedUser = user; // make sense when size() == 1
        });
    }, true);

    // watch with timer
    var timer = false;
    $scope.$watch('[copyRolesDialogObjects.filter.lastName, copyRolesDialogObjects.filter.firstName, copyRolesDialogObjects.filter.email, copyRolesDialogObjects.filter.organisation]',
      function(newValue, oldValue) {
        if (newValue && newValue !== oldValue) {
          if (timer) {
              $timeout.cancel(timer);
          }
          timer = $timeout(function(){
              delete $scope.copyRolesDialogObjects.filter.page;
              $scope.loadCopyRolesUsers();
          }, 350);
        }

       }, true
    );
    // watch without timer
    $scope.$watch('[copyRolesDialogObjects.filter.roleType, copyRolesDialogObjects.filter.active]', function(newValue, oldValue) {
      if (newValue && newValue !== oldValue) {
        $scope.filter.page = 0;
        $scope.loadCopyRolesUsers();
      }
    }, true);
    // watch without timer
    $scope.$watch('[copyRolesDialogObjects.filter.page]', function(newValue, oldValue) {
      if (newValue && newValue !== oldValue) {
        $scope.loadCopyRolesUsers();
      }
    }, true);

    $scope.loadCopyRolesUsers = function() {
      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.copyRolesDialogObjects.filter);
      if (localFilter.active === '') {
          delete localFilter.active;
      }
      if (localFilter.roleType === '') {
          delete localFilter.roleType;
      }
      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
      $http.post(ENDPOINTS.usersListJson, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
          $scope.copyRolesDialogObjects.users = response.data.elements.filter(user => !$scope.selectedUsers[user.topiaId]);
          $scope.copyRolesDialogObjects.pager = response.data;
      });
    };

    // copy/past roles
    $scope.choseUserToCopyRoles = function () {
      // init
      $scope.copyRolesDialogObjects = {
        users: [],
        selectedUsers: {},
        filter: {
          active: true
        },
        pager: {}
      };
      $scope.loadCopyRolesUsers();
      $scope.displayCopyRolesDialog();

    };

    $scope.displayCopyRolesDialog = function() {
      // display daialog
      $("#selectUsersToCopy").dialog({
        resizable: false,
        width: 1100,
        modal: true,
        position: { my: "center top", at: "top+50" },
        buttons: {
          copy: {
            click: function() {
              $(this).dialog("close");
              $scope.copyRolesToUsers();
            },
            text: "Coller",
            'class': 'btn-primary'
          },
          cancel: {
            click: function() {
              $(this).dialog("close");
            },
            text: 'Annuler',
            'class': 'float-left btn-secondary'
          }
        }
      });
    };

    $scope.copyRolesToUsers =  function() {
      var fromSelectedUsers = $filter('toSelectedArray')($scope.selectedUsers)
      var toSelectedUsers = $filter('toSelectedArray')($scope.copyRolesDialogObjects.selectedUsers)
      if (fromSelectedUsers && fromSelectedUsers.length == 1 &&
          toSelectedUsers && toSelectedUsers.length > 0) {
        var fromUser = fromSelectedUsers[0];
        var ajaxRequest = "fromUserId=" + fromUser;
        toSelectedUsers.forEach(userId => ajaxRequest += "&toUserIds=" + userId);

        $http.post(ENDPOINTS.userRolesCopy, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .then(function(response) {
          addSuccessMessage("Les droits ont été ajoutés aux utilisateurs sélectionnés");
        }).catch(function(response) {
          var message = "Échec de copie des rôles";
          console.error(message, response);
          addPermanentError(message, response.status);
        });
      }
    };
}]);

AgrosystModule.controller('UserRolesController', ['$scope', '$window', '$http', '$q', 'userRoles',
    function($scope, $window, $http, $q, userRoles) {
        //$scope.userRoles;
        $scope.roleTypes = $scope.i18n.RoleType;

        $scope.userRoles = userRoles;

        $scope._initSelectedEntitiesForDeletionMap = function() {
            $scope.selectedEntitiesForDeletionMap = {};
            $scope.selectedEntitiesForDeletionCount = 0;

            $scope.userRoles.forEach(r => {
                $scope.selectedEntitiesForDeletionMap[r.topiaId] = {
                    role: r,
                    selected: false
                };
            });
        };

        $scope._initSelectedEntitiesForDeletionMap();

        $scope.selectUserRole = function(userRole) {
            $scope.selectedUserRole = userRole;
            $scope.loadEntities();
            $scope.loadCampaigns();
        };

        $scope.unSelectUserRole = function() {
            delete $scope.selectedUserRole;
        };

        $scope.deleteUserRole = function(confirmDialog, userRole) {
            $scope.confirmDeleteUserRolesMsg = "Êtes-vous sûr de vouloir supprimer ce rôle ?";
            $scope.showUserRolesDeleteConfirmDialog($(confirmDialog)).then(confirm => {
                if (confirm === true) {
                    if ($scope.selectedUserRole === userRole) {
                        delete $scope.selectedUserRole;
                    }
                    delete $scope.selectedEntitiesForDeletionMap[userRole.topiaId];

                    var indexOf = $scope.userRoles.indexOf(userRole);
                    $scope.userRoles.splice(indexOf, 1);
                }
                delete $scope.confirmDeleteUserRolesMsg;
            });
        };

        $scope.deleteSelectedUserRoles = function(confirmDialog) {
            let count = $scope.selectedEntitiesForDeletionCount;
            if (count === 0) {
                return;
            }
            $scope.confirmDeleteUserRolesMsg = "Êtes-vous sûr de vouloir supprimer";
            if (count === 1) {
                $scope.confirmDeleteUserRolesMsg += " le rôle sélectionné ?";
            } else {
                $scope.confirmDeleteUserRolesMsg += " les " + count + " rôles sélectionnés ?";
            }
            $scope.showUserRolesDeleteConfirmDialog($(confirmDialog)).then(confirm => {
                if (confirm === true) {
                    angular.forEach($scope.selectedEntitiesForDeletionMap, (value, key) => {
                        if (value.selected === true) {
                            if ($scope.selectedUserRole === value.role) {
                                delete $scope.selectedUserRole;
                            }
                            var indexOf = $scope.userRoles.indexOf(value.role);
                            $scope.userRoles.splice(indexOf, 1);
                        }
                    });
                    $scope._initSelectedEntitiesForDeletionMap();
                }
                delete $scope.confirmDeleteUserRolesMsg;
            });
        };

        $scope.showUserRolesDeleteConfirmDialog = function(confirmDialog) {
            var deferred = $q.defer();
            confirmDialog.dialog({
                resizable: false,
                width: 480,
                modal: true,
                buttons: {
                    action: {
                      click: function() {
                        $(this).dialog("close");
                        deferred.resolve(true);
                      },
                      text: "Supprimer",
                      'class': 'btn-primary'
                    },
                    cancel: {
                      click: function() {
                        $(this).dialog("close");
                        deferred.resolve(false);
                      },
                      text: 'Annuler',
                      'class': 'float-left btn-secondary'
                    }
                }
            });
            return deferred.promise;
        };

        $scope.isSelectedUserRoleLabelDisabled = function() {
            if (!$scope.selectedUserRole) {
                return false;
            }
            var selectedRoleType = $scope.selectedUserRole.type;
            if (!selectedRoleType) {
                return true;
            }
            var result = selectedRoleType === 'ADMIN' || selectedRoleType === 'IS_DATA_PROCESSOR';
            return result;
        };

        $scope.isSelectedUserRole = function(userRole) {
            return $scope.selectedUserRole === userRole;
        };

        $scope.roleTypeSelected = function() {
            $scope.loadEntities();
        };

        $scope.loadEntities = function() {
            var roleType = $scope.selectedUserRole.type;
            delete $scope.availableEntities;
            if (roleType) {
                if (roleType === 'ADMIN' || roleType === 'IS_DATA_PROCESSOR') {
                    $scope.availableEntities = [];
                } else {
                    var request = "roleType=" + roleType;

                    $http.post(ENDPOINTS.getEntitiesFromRoleTypeJson,
                               request,
                               {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                    ).then(function(response) {
                        if (!response.data || response.data == "null") {
                            $scope.availableEntities = [];
                        } else {
                            $scope.availableEntities = response.data;
                            if ($scope.selectedUserRole.entity && $scope.selectedUserRole.entity.identifier) {
                                var originalCampaign = $scope.selectedUserRole.entity.campaign;
                                angular.forEach($scope.availableEntities, function(entity) {
                                    if (entity.identifier == $scope.selectedUserRole.entity.identifier) {
                                        $scope.selectedUserRole.entity = entity;
                                        $scope.selectedUserRole.entity.campaign = originalCampaign;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        };

        $scope.roleEntitySelected = function() {
            delete $scope.selectedUserRole.entity.campaign;
            $scope.loadCampaigns();
        };

        $scope.loadCampaigns = function() {
            var roleType = $scope.selectedUserRole.type;
            var roleEntity = $scope.selectedUserRole.entity;
            delete $scope.availableEntityCampaigns;
            if (roleType && roleEntity && roleEntity.identifier) {
                if (roleType === 'GS_DATA_PROCESSOR') {
                    var request = "growingSystemCode=" + roleEntity.identifier;

                    $http.post(ENDPOINTS.getGrowingSystemCampaignsJson,
                               request,
                               {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                    ).then(function(response) {
                        if (!response.data || response.data == "null") {
                            $scope.availableEntityCampaigns = [];
                        } else {
                            $scope.availableEntityCampaigns = response.data;
                            if (roleEntity && roleEntity.campaign) {
                                angular.forEach($scope.availableEntityCampaigns, function(campaign) {
                                    if (campaign == roleEntity.campaign) {
                                        roleEntity.campaign = campaign;
                                    }
                                });
                            }
                        }
                    });
                }
            }
        };

        $scope.addUserRole = function() {
            var newUserRole = { entity: {}, user: {} };
            $scope.userRoles.push(newUserRole);

            $scope.selectUserRole(newUserRole);
        };

        $scope.toggleSelectedEntityForDeletion = function(entityId) {
            $scope.selectedEntitiesForDeletionMap[entityId].selected = !$scope.selectedEntitiesForDeletionMap[entityId].selected;
            if ($scope.selectedEntitiesForDeletionMap[entityId].selected === true) {
                $scope.selectedEntitiesForDeletionCount += 1;
            } else {
                $scope.selectedEntitiesForDeletionCount -= 1;
            }
        };
    }
]);
