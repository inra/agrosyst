/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
AgrosystModule.controller('TrackerListController', ['$scope', '$http', '$timeout', 'events', 'eventTypes', 'roleTypes',
    function ($scope, $http, $timeout, events, eventTypes, roleTypes) {
      // la liste des reseaux (utilisé par le ng-repeat)
      $scope.events = events.elements;
      // pager correspondant à la liste actuelle des reseaux affiché
      $scope.pager = events;

      $scope.eventTypes = eventTypes;
      $scope.roleTypes = roleTypes;

      // le filtre pour filtrer la liste
      $scope.filter = {};

      $scope.refresh = function(newValue, oldValue) {
        if (newValue === oldValue) { return; } // prevent init useless call

        // don't send empty active filter
        if ($scope.filter.active === '') {
            delete $scope.filter.active;
        }

        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson($scope.filter));

        $http.post(ENDPOINTS.trackedEventsListJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
        then(function(response) {
            $scope.events = response.data.elements;
            $scope.pager = response.data;
        });
      };

      // watch without timer
      $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);
}]);
