/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

window.isAgConfirmOnExit = true;

// prevent from keeping the loading if the user uses the browser history
window.onunload = function() {
  hidePageLoading();
}

window.AgrosystVueComponents = {
  filters: {},
  i18n: {},
  Table: {},
  Domains: {},
  Sample: {},
  // For generic common components
  Common: {},
  // For ipmworks specific components
  Ipmworks:{}
};

function displayPageLoading() {
  var pageLoading = document.getElementById("pageLoading");
  if (pageLoading) {
    pageLoading.innerHTML = "<div id='pageLoadingDisplayed' class='page-loading'><i class='fa fa-spinner fa-pulse'></i></div>";
  } else {
    // it has been voluntary removed to disable loading indication so we re-enable it for future loading indication
    var pageLoadingParent = document.getElementById("pageLoadingParent");
    if (pageLoadingParent) {
      pageLoadingParent.innerHTML = "<div id='pageLoading'></div>";
    }
  }
  return false;
}

function hidePageLoading() {
  var pageLoading = document.getElementById("pageLoading");
  if (pageLoading) {
    pageLoading.innerHTML = "";
  }
  return false;
}

function pageLoadingForRedirection(e) {
  var target = e.target || e.srcElement;
  // for <a> tags or <a><span> tags
  if (target.tagName === 'A' || (target.parentElement && target.parentElement.tagName === 'A')) {
      var href = target.getAttribute('href') || (target.parentElement && target.parentElement.getAttribute('href'));
      var linkTarget = target.getAttribute('target') || (target.parentElement && target.parentElement.getAttribute('target'));

      // do nothing if it is empty link
      if (href && !linkTarget &&
          href.match(/.action/) &&
          href !== '#' &&
          !href.match(/download.action*/) &&
          !href.match(/-export-*/)) {

          // if page must be open on an other tab, do nt display page loading
          if(!e.ctrlKey) {
            displayPageLoading();
          }

      }
      // unactivated confirm on exit when file downloading is asked by user
      if (href && href.match(/download.action*/)) {
        window.isAgConfirmOnExit = false;
      }
  } else if (target.tagName === 'INPUT' && target.type === 'submit' && (!target.form || target.form.checkValidity())) {
      // if page must be open on an other tab, do nt display page loading
      if(!e.ctrlKey) {
        displayPageLoading();
      }
  }
  return false;
}

// use to disable loading indication
function removePageLoading() {
    var elem = document.getElementById('pageLoading');
    if (elem) {
      elem.parentNode.removeChild(elem);
    }
    return false;
}

if (document.addEventListener) {
    document.addEventListener('click', pageLoadingForRedirection);
    // intercept right click and prevent page loading
    document.addEventListener('contextmenu', function(e) { removePageLoading(); }, false);
} else if (document.attachEvent) {
    document.attachEvent('onclick', pageLoadingForRedirection);
}

var AgrosystModule = angular.module('Agrosyst', ['ngAnimate', 'ui.validate', 'angularLocalStorage', 'angular.jquery']);

/**
 * Directive permettant la confirmation de sortie de page
 * lorsque l'utilisateur possède des modifications en cours dans le formulaire de saisie
 */
AgrosystModule.directive('agConfirmOnExit', function() {
  return {
    link: function($scope, elem, attrs) {
      if (elem[0].tagName.toLowerCase() == 'form') {
        var form = elem[0];
        var formName = form.attributes.name.value;

        // L'utilisateur possède des modifications en cours : demande de confirmation
        window.onbeforeunload = function() {
          storeTabIndex();
          var agConfirmOnExit = window.isAgConfirmOnExit;
          if (!agConfirmOnExit) {
            window.isAgConfirmOnExit = true;
          }
          if ($scope[formName].$dirty && agConfirmOnExit) {
            // disable page loading if change detected
            hidePageLoading();
            return "Les modifications en cours seront perdues.";
          }
        };

        // Lorsque l'utilisateur enregistre ses saisies, le message de confirmation ne doit pas être affiché
        var pristineFct = $scope[formName].$setPristine.bind($scope[formName]);
        var submitFct = function() {
          pristineFct();
        };
        form.addEventListener("submit", submitFct, false); // click
        $(form).on("submit", submitFct); // jquery
      }
    }
  };
});

/**
 * Directive de validation d'un champ en entier.
 */
AgrosystModule.directive('agInteger', function() {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if (/^\d+$/.test(viewValue)) {
                    // it is valid
                    ctrl.$setValidity('integer', true);
                    return Number(viewValue);
                } else if (viewValue.length === 0) {
                    ctrl.$setValidity('integer', true);
                    return "";
                } else {
                    // it is invalid, return undefined (no model update)
                    ctrl.$setValidity('integer', false);
                    return undefined;
                }
            });
        }
    };
});

/**
 * Directive de validation d'un champ float.
 */
AgrosystModule.directive('agFloat', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (/^[-+]?\d*[\.,]?\d*$/.test(viewValue) && viewValue.length > 0) {
          viewValue = Number(viewValue.replace(',', '.'))
          ctrl.$setValidity('float', true);
          return viewValue;
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('float', true);
          return "";
        } else {
          ctrl.$setValidity('float', false);
          return undefined;
        }
      });
    }
  };
});

AgrosystModule.directive('agFloatStrictlyPositive', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        viewValue += "";
        if (/^\d+([\.,]\d+)?$/.test(viewValue)) {
          viewValue = Number(viewValue.replace(',', '.'));
          if (viewValue > 0.0) {
            ctrl.$setValidity('float', true);
            return viewValue;
          } else {
            ctrl.$setValidity('float', false);
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('float', false);
          return undefined;
        } else {
          ctrl.$setValidity('float', false);
          return undefined;
        }
      });
    }
  };
});

AgrosystModule.directive('agFloatPositive', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        viewValue += "";
        if (/^\d+([\.,]\d+)?$/.test(viewValue)) {
          viewValue = Number(viewValue.replace(',', '.'));
          ctrl.$setValidity('float', true);
          return viewValue;
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('float', true);
          return undefined;
        } else {
          ctrl.$setValidity('float', false);
          return undefined;
        }
      });
    }
  };
});

/**
 * Directive de validation d'un champ float.
 */
AgrosystModule.directive('agFloat2dec', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (/^\d+((\.|\,)\d+)?$/.test(viewValue)) {
          viewValue = viewValue.replace(',', '.');
          var valid = true;
          var digit = viewValue.split('.');
          if (digit.length === 2) {
            var digitValue = digit[1];
            if (digitValue.length > 2) {
              valid = false;
            }
          }
          if (valid) {
            ctrl.$setValidity('float', true);
            return viewValue;
          } else {
            ctrl.$setValidity('float', false);
            return undefined;
          }

        } else if (viewValue.length === 0) {
          ctrl.$setValidity('float', true);
          return "";
        } else {
          ctrl.$setValidity('float', false);
          return undefined;
        }
      });
    }
  };
});

/**
 * Directive de validation d'un champ location (negative float).
 */
AgrosystModule.directive('agLocation', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (/^(\-?\d+)((\.|\,)\d+)?$/.test(viewValue)) {
          ctrl.$setValidity('float', true);
          return viewValue.replace(',', '.');
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('float', true);
          return undefined;
        } else {
          ctrl.$setValidity('float', false);
          return undefined;
        }
      });
    }
  };
});

/**
 * Directive de validation d'une série de campagne
 */
AgrosystModule.directive('agCampaignSeries', function() {
  var campaignsBounds0 = null;
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      campaignsBounds0 = scope ? scope.campaignsBounds : campaignsBounds0;
      ctrl.$parsers.unshift(function(viewValue, campaignsBounds) {
        viewValue = viewValue.replace(/\D+/g, ' ').trim();
        var campaigns = viewValue.split(" ");
        if (campaigns && campaigns.length > 0) {
          var intCampaigns = [];
          for (var i=0; i<campaigns.length; i++) {
            var campaign = campaigns[i];
            if (isNaN(campaign)) {
              return undefined;
            }
            var intCampaign = parseInt(campaign);
            if (campaignsBounds0 && campaignsBounds0.left <= intCampaign && intCampaign <= campaignsBounds0.right) {
              intCampaigns.push(intCampaign);
            } else {
              return null;
            }
          }
          return intCampaigns.toString();
        }
      });
    }
  };
});


AgrosystModule.directive('agCampaign', function() {
  var campaignsBounds0 = null;
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      campaignsBounds0 = scope ? scope.campaignsBounds : campaignsBounds0;
      ctrl.$parsers.unshift(function(viewValue, campaignsBounds) {
        if (/^(?:19|20)[0-9]{2}$/.test(viewValue)) {
          var intCampaign = parseInt(viewValue);
          if (campaignsBounds0 && campaignsBounds0.left <= intCampaign && intCampaign <= campaignsBounds0.right) {
            return intCampaign.toString();
          } else {
            return null;
          }
        } else {
          return null;
        }
      });
    }
  };
});

var setValidValorisationPercent = function (sum, scope) {
  var isValid = ((99.0 <= sum) && (sum <= 101.0));
  scope.valorisation._isValidValorisationPercent = isValid;
};

var validGlobalValorisationPercentForSales = function (scope, value) {
  var selfConsumedPersent = parseInt(scope.valorisation.selfConsumedPersent + '');
  var noValorisationPercent = parseInt(scope.valorisation.noValorisationPercent + '');
  var sum = selfConsumedPersent + noValorisationPercent + value;
  setValidValorisationPercent(sum, scope);
};

var validGlobalValorisationPercentForSelfConsumed = function (scope, varFloat) {
  var salesPercent = parseInt(scope.valorisation.salesPercent + '');
  var noValorisationPercent = parseInt(scope.valorisation.noValorisationPercent + '');
  var sum = salesPercent + noValorisationPercent + varFloat;

  setValidValorisationPercent(sum, scope);
};

var validGlobalValorisationPercentForNoValorisation = function (scope, varFloat) {
  var salesPercent = parseInt(scope.valorisation.salesPercent + '');
  var selfConsumedPersent = parseInt(scope.valorisation.selfConsumedPersent + '');
  var sum = salesPercent + selfConsumedPersent + varFloat;

  setValidValorisationPercent(sum, scope);
};

AgrosystModule.directive('agSales', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        viewValue = viewValue + '';
        if (/^\d+$/.test(viewValue)) {
          var value = parseInt(viewValue);
          if (value <= 100) {

            validGlobalValorisationPercentForSales(scope, value);

            ctrl.$setValidity('sales', true);
            return value;
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('sales', true);
          return undefined;
        }
        ctrl.$setValidity('sales', false);
        return undefined;
      });
    }
  };
});

AgrosystModule.directive('agSelfConsumed', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        viewValue = viewValue + '';
        if (/^\d+$/.test(viewValue)) {
          var value = parseInt(viewValue);
          if (value <= 100) {

            validGlobalValorisationPercentForSelfConsumed(scope, value);

            ctrl.$setValidity('self-consumed', true);
            return value;
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('self-consumed', true);
          return undefined;
        }
        ctrl.$setValidity('self-consumed', false);
        return undefined;
      });
    }
  };
});

AgrosystModule.directive('agNoValorisation', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        viewValue = viewValue + '';
        if (/^\d+$/.test(viewValue)) {
          var value = parseInt(viewValue);
          if (value <= 100) {

            validGlobalValorisationPercentForNoValorisation(scope, value);

            ctrl.$setValidity('no-valorisation', true);
            return value;
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('no-valorisation', true);
          return undefined;
        }
        ctrl.$setValidity('no-valorisation', false);
        return undefined;
      });
    }
  };
});

/**
 * Directive de validation d'un champ float.
 */
AgrosystModule.directive('agPercent', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (/^\d+((\.|\,)\d+)?$/.test(viewValue)) {
          var varFloat = parseFloat(viewValue.replace(',', '.'));
          if (varFloat <= 100) {
            ctrl.$setValidity('percent', true);
            return varFloat;
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('percent', true);
          return "";
        }
        ctrl.$setValidity('percent', false);
        return undefined;
      });
    }
  };
});

AgrosystModule.directive('agRequiredPercent', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (/^\d+((\.|\,)\d+)?$/.test(viewValue)) {
          var varFloat = parseFloat(viewValue.replace(',', '.'));
          if (varFloat <= 100) {
            ctrl.$setValidity('percent', true);
            return varFloat;
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('percent', true);
          return undefined;
        }
        ctrl.$setValidity('percent', false);
        return undefined;
      });
    }
  };
});

AgrosystModule.directive('agPercentWithoutZero', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (/^\d+((\.|\,)\d+)?$/.test(viewValue)) {
          var varFloat = parseFloat(viewValue.replace(',', '.'));
          if (varFloat > 0 && varFloat <= 100) {
            ctrl.$setValidity('percent', true);
            return varFloat;
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('percent', true);
          return "";
        }
        ctrl.$setValidity('percent', false);
        return undefined;
      });
    }
  };
});

AgrosystModule.directive('agRequiredPercentWithoutZero', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$parsers.unshift(function(viewValue) {
        if (/^\d+((\.|\,)\d+)?$/.test(viewValue)) {
          var varFloat = parseFloat(viewValue.replace(',', '.'));
          if (varFloat > 0 && varFloat <= 100) {
            ctrl.$setValidity('percent', true);
            return varFloat;
          }
        } else if (viewValue.length === 0) {
          ctrl.$setValidity('percent', true);
          return undefined;
        }
        ctrl.$setValidity('percent', false);
        return undefined;
      });
    }
  };
});
/**
 * Pagination directive.
 */
AgrosystModule.directive('agPagination', ['$timeout', function ($timeout) {
    //var pageSizeLabel = "Page size";
    return {
      priority: 0,
      restrict: 'E',
      scope: {
          pager: '&',
          paginationUpdate: '&',
          labels: '&'
      },
      template:
         '<div class="container"><span class="label" ng-show="pageCount > 1">{{ labels().pages || "Pages" }}</span>'+
         '<a href="" ng-click="clickToPage(pager().currentPage.pageNumber - 1)" ng-show="pageCount > 1 && pager().currentPage.pageNumber > 0">&lt;</a>'+
         '<a href="" ng-click="clickToPage(p.n)" ng-repeat="p in pages"' +
         ' ng-class="p.n == pager().currentPage.pageNumber && \'current\' || \'\'">{{p.display}}</a>'+
         '<a href="" ng-click="clickToPage(pager().currentPage.pageNumber + 1)"'+
         ' ng-show="pageCount > 1 && pager().currentPage.pageNumber < pageCount - 1">&gt;</a>' +
         '<span class="label">{{ labels().byPage || "Par page" }}:</span>' +
         '<input type="number" ng-model="pageSize" class="page-size" min="10" max="100" size="3" />' +
         '</div>',
      replace: true,
      compile: function compile(tElement, tAttrs, transclude) {
        return {
          pre: function preLink($scope, iElement, iAttrs, controller) {

              // use $watch because using ng-repeat on a function cause angular exception
              $scope.pages = [];
              $scope.pageSize = $scope.pager() && $scope.pager().currentPage ? $scope.pager().currentPage.pageSize : 10;
              $scope.$watch('pager()', function(newValue, oldValue) {
                  if (!$scope.pager() || !$scope.pager().currentPage) {
                    return;
                  }
                  $scope.pageCount = Math.ceil($scope.pager().count / $scope.pager().currentPage.pageSize);
                  $scope.pages = [];
                  if ($scope.pageCount > 1) {
                      $scope.pages.push({ n: 0, display: 1 });
                  }
                  if ($scope.pager().currentPage.pageNumber > 4) {
                      $scope.pages.push({ n: $scope.pager().currentPage.pageNumber - 4, display: "..."} );
                  }
                  for (var i = $scope.pager().currentPage.pageNumber - 3 ; i <= $scope.pager().currentPage.pageNumber + 3 ; i++) {
                      if (i > 0 && i < $scope.pageCount - 1) {
                          $scope.pages.push({ n: i, display: i + 1 });
                      }
                  }
                  if ($scope.pager().currentPage.pageNumber + 5 < $scope.pageCount) {
                      $scope.pages.push({ n: $scope.pager().currentPage.pageNumber + 4, display: "..." });
                  }
                  if ($scope.pageCount > 1) {
                      $scope.pages.push({ n: $scope.pageCount - 1, display:$scope.pageCount });
                  }
                  hidePageLoading();
              });

              $scope.clickToPage = function(page) {
                  if ((page >= 0) && (page < $scope.pageCount)) {
                    $scope.paginationUpdate({'page': page, 'pageSize': $scope.pageSize});
                  }
              };

              var timer = false;
              $scope.$watch('pageSize', function(newValue, oldValue) {
                if (newValue) {
                  if (timer) {
                    $timeout.cancel(timer);
                  }
                  timer = $timeout(function() {
                    if ($scope.pager().currentPage) {
                      $scope.paginationUpdate({ 'page': $scope.pager().currentPage.pageNumber, 'pageSize': newValue });
                    }
                  }, 350);
                }
              }, true);
          }
        };
      }
    };
}]);

/**
 * Converti un char en un entier '0' =&gt; 0 et '9' =&gt; 9, et 'A' =&gt; 1 a 'Z' =&gt; 36,
 * les autres caractere sont aussi convertis pour que
 * A|B|C|D|E|F|G|H|I|J
 * K|L|M|N|O|P|Q|R|S|T            z
 * U|V|W|X|Y|Z| | | |
 * -+-+-+-+-+-+-+-+-+
 * 1|2|3|4|5|6|7|8|9|0.
 * Pour les autres c'est un indedermine
 *
 * @param c le caractere qui doit etre converti
 * @return le chiffre
 */
function getDigit(c) {
    let result = 0;
    if (c >= '0' && c <= '9') {
        result = c - '0';
    } else if (c >= 'A' && c <= 'Z') {
        result = (c - 'A' + 1) % 10;
    } else {
        result = (c - 'a' + 1) % 10;
    }
    return result;
}

function luhnChecksum(siret) {

  siret = siret.replace(/\s/g, '');// remove white space
  // on multiple alternativement par 1 ou par 2 les chiffres
  // sachant que le chiffre le plus à droite est multiplié par 1

  // l'offset permet de savoir si le premier chiffre( le plus a gauche) doit etre
  // multiplier par 1 ( offset = 0)
  // multiplier par 2 (offset = 1)
  var sum = 0;
  var offset = (siret.length + 1) % 2;


  for (let i = 0; i < siret.length; i++) {

      // recuperation de la valeur
      let n = getDigit(siret[i]);

      // multiplication par 1 ou 2 en fonction de l'index et de l'offset
      n *= (i + offset) % 2 + 1;

      // si une fois multiplie il est superieur a 9, il faut additionner
      // toutes ces constituante, mais comme il ne peut pas etre superieur
      // a 18, cela revient a retrancher 9
      if (n > 9) {
          n -= 9;
      }

      // on peut directement faire la somme
      sum += n;
  }

  // 3eme phase on verifie que c'est bien un multiple de 10
  var result = sum % 10 == 0;

  return result;
};
/**
 * Directive de validation d'un champ SIRET
 */
AgrosystModule.directive('agSiret', function() {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelController) {
      $($element).inputmask({'mask': '999 999 999 99999'});

      var siretRegex = /^[0-9]{3}[\s]?[0-9]{3}[\s]?[0-9]{3}[\s]?[0-9]{5}$/;

      ngModelController.$formatters.push(function(value) {
        var isValid = typeof value === 'string' && value.match(siretRegex) != null && luhnChecksum(value);
        ngModelController.$setValidity('agSiret', isValid);
        return value;
      });

      ngModelController.$parsers.push(function(value) {
        if (value) {
          var isValid = typeof value === 'string' && value.match(siretRegex) != null && luhnChecksum(value);
          ngModelController.$setValidity('agSiret', isValid);
          return isValid ? value : undefined;
        } else {
          ngModelController.$setValidity('agSiret', true);
          return '';
        }
      });
    }
  };
});

// @see http://ngmodules.org/modules/angular-readmore
AgrosystModule.directive('agReadMore', ['$compile', function($compile) {

    return {
        restrict: 'A',
        scope: true,
        link: function(scope, element, attrs) {

            // start collapsed
            scope.collapsed = false;

            // create the function to toggle the collapse
            scope.toggle = function() {
                scope.collapsed = !scope.collapsed;
            };

            // wait for changes on the text
            attrs.$observe('agReadMore', function(text) {

                // get the length from the attributes
                var maxLength = scope.$eval(attrs.agReadMoreLength) || 50;

                if (text.length > maxLength) {
                    // split the text in two parts, the first always showing
                    var firstPart = String(text).substring(0, maxLength);
                    var secondPart = String(text).substring(maxLength, text.length);

                    // create some new html elements to hold the separate info
                    var firstSpan = $compile('<span>' + firstPart + '</span>')(scope);
                    var secondSpan = $compile('<span ng-if="collapsed">' + secondPart + '</span>')(scope);
                    var moreIndicatorSpan = $compile('<span ng-if="!collapsed">...</span>')(scope);
                    //var lineBreak = $compile('<br ng-if="collapsed">')(scope);
                    var toggleButton = $compile('<span class="read-more-text-toggle" ng-click="toggle()"> {{collapsed ? "(moins)" : "(plus)"}}</span>')(scope);

                    // remove the current contents of the element
                    // and add the new ones we created
                    element.empty();
                    element.append(firstSpan);
                    element.append(secondSpan);
                    element.append(moreIndicatorSpan);
                    //element.append(lineBreak);
                    element.append(toggleButton);
                }
                else {
                    element.empty();
                    element.append(text);
                }
            });
        }
    };
}]);

AgrosystModule.directive('agScrollableTable', function() {
    return {
        link: function(scope, elements, attrs) {
            var tabs = document.getElementsByClassName("tabs");
            elements[0].style.width = (tabs[0].offsetWidth - attrs.agScrollableTable) + "px";

        }
    };
});

/*
 * Animation jQuery slide.
 */
AgrosystModule.animation('.slide-animation', function() {
   return {
     beforeAddClass : function(element, className, done) {
       if (className == 'ng-hide') {
         jQuery(element).slideUp(done);
       }
     },
     removeClass : function(element, className, done) {
       if (className == 'ng-hide') {
         jQuery(element).slideDown(done);
       }
     }
   };
});
AgrosystModule.animation('.fade-animation', function() {
   return {
     enter : function(element, done) {
       jQuery(element).fadeIn(done);
     },
     leave : function(element, done) {
       jQuery(element).fadeOut(done);
     },
     beforeAddClass : function(element, className, done) {
       jQuery(element).fadeOut(done);
     },
     removeClass : function(element, className, done) {
       jQuery(element).fadeIn(done);
     }
   };
});

/**
 * Filtre pour encoder une chaine via 'encodeURIComponent'.
 */
AgrosystModule.filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
});

/**
 * Filtre pour tronquer un texte à une taille (affiche ... si la taille dépasse).
 */
AgrosystModule.filter('truncate', function () {
  return function (text, length, end) {
      if (isNaN(length)) {
          length = 10;
      }
      if (end === undefined) {
          end = "...";
      }
      if (!text || text.length <= length || text.length - end.length <= length) {
          return text;
      }
      else {
          return String(text).substring(0, length-end.length) + end;
      }
  };
});

/**
 * Filtre pour retourner l'unité correspondant à l'input passé en paramêtre..
 */
AgrosystModule.filter('inputUnit', function() {
  /**
  * input: input
  * organicQuantityUnits: liste d'autorité FertilizerQuantityUnit
  */
  return function (input, mineralProductUnits, organicProductUnits, phytoProductUnits) {
    var result;
    if (input.otherProductInputUnit) {
      result = input.otherProductInputUnit;
    } else if (input.mineralProductUnit && mineralProductUnits) {
      result = mineralProductUnits[input.mineralProductUnit];
    } else if (input.organicProductUnit && organicProductUnits) {
      result = organicProductUnits[input.organicProductUnit];
    } else if (input.phytoProductUnit && phytoProductUnits) {
      result = phytoProductUnits[input.phytoProductUnit];
    }
    return result;
  };
});

AgrosystModule.filter('orDash', function () {
  return function (text) {
    return angular.isUndefined(text) || text === '' ? '-' : text;
  };
});

AgrosystModule.filter('NA', function () {
  return function (text) {
    return angular.isUndefined(text) || text === '' ? '#N/A ' : text;
  };
});

/**
 * Affiche l'attribut en paramètre de chaque élément du tableau
 */
AgrosystModule.filter('displayArrayProperties', function () {
    return function (array, attrs, size) {
        if (!size || size > array.length) {
            size = array.length;
        }
        var attrArray = [];
        for (var i = 0 ; i < size ; i++) {
            if (Array.isArray(attrs)) {
                var lineArray = [];
                for (var j=0; j<attrs.length; j++) {
                    var attr = attrs[j];
                    lineArray.push(array[i][attr]);
                }
                if(array[i]['active'] != 'undefined' && array[i]['active'] == false) {
                    lineArray.push("(inactif)");
                }
                attrArray.push(lineArray.join(" "));
            } else {
                attrArray.push(array[i][attrs]);
            }
        }
        return attrArray.join(", ");
    };
});

AgrosystModule.filter('displayMapValues', function () {
    return function (object, size) {
        var keys = Object.keys(object);
        if (!size || size > keys.length) {
            size = keys.length;
        }
        var attrArray = [];
        for (var i = 0 ; i < size ; i++) {
            attrArray.push(object[keys[i]]);
        }
        return attrArray.join(", ");
    };
});

AgrosystModule.filter('displayableList', function () {
    return function (items, map) {
        var array = [];
        angular.forEach(items, function(item) {
            if (map) {
                array.push(map[item]);
            } else {
                array.push(item);
            }
        });
        return array.join(", ");
    };
});

/**
 * Transform une map json {x: true, y:false} en un tableau contenant
 * seulement les élements sélectionnés: [x];
 */
AgrosystModule.filter('toSelectedArray', function() {
    return function(elements) {
        var result = [];
        angular.forEach(elements, function(value, key) {
            if (value) {
                result.push(key);
            }
        });
        return result;
    };
});

/**
 * Compte, parmis une map, combien d'element ont comme valeur 'true';
 */
AgrosystModule.filter('toSelectedLength', function() {
    return function(elements) {
        var count = 0;
        angular.forEach(elements, function(value, key) {
            if (value) {
                ++count;
            }
        });
        return count;
    };
});

AgrosystModule.filter('toSelectedValue', function() {
    return function(elements) {
        var results=[];
        angular.forEach(elements, function(value, key) {
            if (value) {
                results.push(key);
            }
        });
        return results;
    };
});

AgrosystModule.filter('removeAccents', function() {
    return function(text) {
      var textNoAccents = text.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
      return textNoAccents;
    };
});

AgrosystModule.filter('translate', ['$log', function($log) {
    return function(key, bundleName) {
        if (!key) return key;

        var bundle = I18N[bundleName];
        if (!bundle) {
          $log.error("Invalid bundle", bundleName);
          return "### " + key + " ###";
        }

        var display = bundle[key];
        if (!display) {
          $log.warn('Le bundle', bundleName, 'ne contient pas de clé', key);
          return "### " + key + " ###";
        }
        return display;
    };
}]);

AgrosystModule.run(['$rootScope', function($rootScope) {
    $rootScope.i18n = I18N;
    $rootScope.endpoints = ENDPOINTS;
    $rootScope.configuration = {
      debounce : {
        debounce: 700
      },
      httpPostHeader: {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    };
}]);

/* @deprecated since 2.6, use jqdialog */
var _displayConfirmDialog = function(scope, anchor, width_, executeFunction, title) {
  anchor.dialog({
    resizable: false,
    width: width_,
    modal: true,
    title: title,
    close: function( event, ui ) {
      $(this).dialog("destroy");
      executeFunction.cancelChanges();
      scope.$apply();
    },
    buttons: {
      action: {
        click: function() {
          var doNotClose = executeFunction.process();
          if (!doNotClose) {
            $(this).dialog("destroy");
          }
          scope.$apply();
        },
        text: "Ok",
        'class': 'btn-primary'
      },
      cancelChanges: {
        click: function() {
          $(this).dialog("destroy");
          executeFunction.cancelChanges();
          scope.$apply();
        },
        text: 'Annuler',
        'class': 'float-left btn-secondary'
      }
    }
  });
};

/* @deprecated since 2.6, use jqdialog */
var _displayInfoDialog = function (scope, anchor, width_, infoMessage, executeFunction) {
  scope.infoMessage = infoMessage;
  anchor.dialog({
    resizable: false,
    width: width_,
    modal: true,
    buttons: {
      action: {
        click: function() {
          $(this).dialog("destroy");
          executeFunction.process();
          delete scope.infoMessage;
        },
        text: "Valider",
        'class': 'btn-primary'
      }
    }
  });
};

var _recursivelyFindParentTab = function(e) {
  var pe = e ? e.parentElement : null;
  // les onglets ont comme identifiant tab_+ numéro du tab ex: "tab_0"
  if(pe) {
    if (pe && pe.id && ((pe.id).match(/tab_[1234567890]+/g))) {
      return pe;
    } else {
      return _recursivelyFindParentTab(pe);
    }
  } else {
    console.log("FIND TAB ERROR");
    return null;
  }
};

// in case of many validation errors, this methode set up a timeout on event listener.
var _temporaryDisableValidationField = function(scope, timeout) {
  scope.displayFieldsErrorsAlert = false;
  timeout(function(){
     scope.displayFieldsErrorsAlert = true;
  }, 350);
};

// hack to unhide hidden section part
// from the id on the element in error we find the name of the angular variable responsible to show/hide the section part
// and we set it to true as to show
var _unHideParentElement = function(scope, srcE) {
  var id = srcE.id;
  if (id) {
    var showConditionParts = id.split("_");
    if (showConditionParts) {
      var attribut = "show"+showConditionParts[0];
      scope[attribut] = true;
    }
  }
};

// find out the error element tab and select it to display it on screen
var _selectErrorTab = function(scope, srcE) {
  var errorTabElement = _recursivelyFindParentTab(srcE);
  if (errorTabElement) {
    var errorIndex = parseInt(errorTabElement.id.split("_")[1]);
    var selectedTab = $(".tabs-menu li.selected").index();
    if (angular.isDefined(errorIndex) && errorIndex !== selectedTab) {
      if (scope.displayFieldsErrorsAlert) {
        $(".tabs-menu li").get(errorIndex).click();
      }
    }
  }
};

// function call on listened event HTML5 errors
var _errorValidationManager = function(scope, timeout, fct, e) {
  if (e) {
    //         chrome       || mozillla
    var srcE = e.srcElement || e.originalTarget;

    // optional function to execute
    if (fct) {
      fct.process(e);
    }

    _unHideParentElement(scope, srcE);

    _selectErrorTab(scope, srcE);

    _temporaryDisableValidationField(scope, timeout);

    hidePageLoading();

    srcE.focus();

    scope.$apply();
  }
};


var _toggleSelectedEntities = function(selectedEntities, allSelectedEntities, pager, entities) {
  if (allSelectedEntities[pager.currentPage.pageNumber].selected) {
    angular.forEach(entities, function(entity) {
      if (entity.topiaId) {
        selectedEntities[entity.topiaId] = true;
      }
    });
    allSelectedEntities[pager.currentPage.pageNumber].nbSel = pager.elements.length;
  } else {
    angular.forEach(entities, function(entity) {
      if (entity.topiaId) {
        selectedEntities[entity.topiaId] = false;
      }
    });
    allSelectedEntities[pager.currentPage.pageNumber].nbSel = 0;
  }
};

var _toggleSelectedEntity = function(selectedEntities, allSelectedEntities, pager, entityId) {
  if (selectedEntities[entityId]) {
    if (!allSelectedEntities[pager.currentPage.pageNumber]) {
      allSelectedEntities[pager.currentPage.pageNumber] = {selected : false, nbSel : 0};
    }
    let nbSel = allSelectedEntities[pager.currentPage.pageNumber].nbSel + 1;
    allSelectedEntities[pager.currentPage.pageNumber].nbSel = nbSel;
    allSelectedEntities[pager.currentPage.pageNumber].selected = nbSel === pager.elements.length;
  } else {
    if (!allSelectedEntities[pager.currentPage.pageNumber]) {
      allSelectedEntities[pager.currentPage.pageNumber] = {selected : false, nbSel : 0};
    }
    allSelectedEntities[pager.currentPage.pageNumber].selected = false;
    if (allSelectedEntities[pager.currentPage.pageNumber].nbSel > 0) {
      allSelectedEntities[pager.currentPage.pageNumber].nbSel--;
    }
  }
};

var _updatePageSelectedEntities = function(selectedEntities, allSelectedEntities, pager, entities) {
  var nbSel = entities.filter(entity => selectedEntities[entity.topiaId]).length;
  allSelectedEntities[pager.currentPage.pageNumber] = { selected : nbSel == pager.elements.length, nbSel };
};
