/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('ReportGrowingSystemController', ['$scope', '$http', '$timeout', 'ReportGrowingSystemInitData',
  function($scope, $http, $timeout, ReportGrowingSystemInitData) {

    $scope.reportGrowingSystem = ReportGrowingSystemInitData.reportGrowingSystem;
    $scope.campaignsBounds = ReportGrowingSystemInitData.campaignsBounds;
    $scope.reportGrowingSystemId = (!ReportGrowingSystemInitData.reportGrowingSystemId || /^\s*$/.test(ReportGrowingSystemInitData.reportGrowingSystemId)) ? null : ReportGrowingSystemInitData.reportGrowingSystemId;
    $scope.reportRegionalId = (!ReportGrowingSystemInitData.reportRegionalId || /^\s*$/.test(ReportGrowingSystemInitData.reportRegionalId)) ? null : ReportGrowingSystemInitData.reportRegionalId;
    $scope.growingSystemId = (!ReportGrowingSystemInitData.growingSystemId || /^\s*$/.test(ReportGrowingSystemInitData.growingSystemId)) ? null : ReportGrowingSystemInitData.growingSystemId;
    $scope.observeManagementModeId = (!ReportGrowingSystemInitData.observeManagementModeId || /^\s*$/.test(ReportGrowingSystemInitData.observeManagementModeId)) ? null : ReportGrowingSystemInitData.observeManagementModeId;
    $scope.reportFilter = ReportGrowingSystemInitData.reportFilter || {};
    $scope.reportRegionals = ReportGrowingSystemInitData.reportRegionalList || {};
    $scope.campaignsBounds = ReportGrowingSystemInitData.campaignsBounds;
    $scope.dephyType = ReportGrowingSystemInitData.dephyType;
    $scope.isDephyExpe = $scope.dephyType === 'DEPHY_EXPE';
    $scope.isDephyFerme = $scope.dephyType === 'DEPHY_FERME';
    $scope.tDephyType = $scope.dephyType ? I18N.TypeDEPHY[$scope.dephyType] : "Non renseigné";
    $scope.groupesCibles = ReportGrowingSystemInitData.groupesCibles;
    $scope.groupesCiblesByCode = {};
    for (var i = 0 ; i < $scope.groupesCibles.length ; i++) {
      var groupeCible = $scope.groupesCibles[i];
      $scope.groupesCiblesByCode[groupeCible.codeGroupeCibleMaa] = groupeCible;
    }
    $scope.treatmentTargetCategoriesByParent = ReportGrowingSystemInitData.treatmentTargetCategoriesByParent;

    $scope.displayFieldsErrorsAlert = true;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

    $scope.fromGrowingSystemId =  $scope.reportFilter.growingSystemId;
    var fromCampaign = $scope.reportFilter.campaign;
    var fromDomainId = $scope.reportFilter.domainId;

    // init empty data
    $scope.reportGrowingSystem.sectors = $scope.reportGrowingSystem.sectors || [];
    $scope.reportFilter.sectors = $scope.reportGrowingSystem.sectors;

    $scope.loadCreationContext = function() {

      if(angular.isDefined($scope.reportGrowingSystem.topiaId)) {
        return;
      }

      var campaign = Number($scope.reportFilter.campaign);
      if (!!campaign && $scope.campaignsBounds.left <= campaign && campaign <= $scope.campaignsBounds.right) {

        if ($scope.reportFilter.sectors.length === 0) {
          $scope.reportRegionals = null;
          return;
        }

        displayPageLoading();
        $http.post($scope.endpoints.reportGrowingSystemAllCreationContext,
          "filter=" + encodeURIComponent(angular.toJson($scope.reportFilter)),
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          $scope.creationContext = response;
          $scope.domains = response.data.Domain;
          $scope.growingSystems = response.data.GrowingSystem;
          $scope.reportRegionals = response.data.ReportRegional;
          hidePageLoading();
        })
        .catch(function(response) {
         hidePageLoading();
         console.error("Échec de récupération des données nécessaires à la création du bilan de campagne", response);
         addPermanentError("Échec de récupération des données nécessaires à la création du bilan de campagne", response.status);
        });
      }
    };

    // case of validation errors
    if ($scope.reportFilter.campaign) {
      $scope.growingSystemId = $scope.reportFilter.growingSystemId;
      $scope.loadCreationContext();
    }

    $scope.loadCreationContextFromSDC = function() {
      var setGrowingSystemModel = {
        process : function() {
          $scope.growingSystemId = $scope.reportFilter.growingSystemId;
          $scope.fromGrowingSystemId = $scope.reportFilter.growingSystemId;
          $scope.growingSystem = $scope.growingSystems.find(aGrowingSystem => aGrowingSystem.topiaId === $scope.growingSystemId);
          if ($scope.growingSystem) {
            $scope.isDephyExpe = $scope.growingSystem.typeDEPHY === 'DEPHY_EXPE';
            $scope.isDephyFerme = $scope.growingSystem.typeDEPHY === 'DEPHY_FERME';
            $scope.dephyType = $scope.growingSystem.typeDEPHY;
          } else {
            $scope.isDephyExpe = null;
            $scope.isDephyFerme = null;
            $scope.dephyType = null
          }
          $scope.loadCreationContext();
        },
        cancelChanges: function() {
          $scope.reportFilter.growingSystemId = $scope.fromGrowingSystemId;
          fromDomainId = $scope.reportFilter.fromDomainId;
        }
      };

      if ($scope.fromGrowingSystemId && $scope.fromGrowingSystemId !== $scope.reportFilter.growingSystemId) {
        _displayConfirmDialog($scope, $("#confirmStrikingFactImpact"), 400, setGrowingSystemModel);
      } else {
        setGrowingSystemModel.process();
      }

    };

    $scope.loadCreationContextFromCampaign = function() {
      var campaign = Number($scope.reportFilter.campaign);

      var performCampaignChange = {
        process : function() {
          fromCampaign = $scope.reportFilter.campaign;
          fromDomainId = null;
          $scope.fromGrowingSystemId = null;
          $scope.loadCreationContext();
        },
        cancelChanges: function() {
          $scope.reportFilter.campaign = fromCampaign;
        }
      };

      if (!!campaign && $scope.campaignsBounds.left <= campaign && campaign <= $scope.campaignsBounds.right &&
          $scope.reportFilter.growingSystemId && fromCampaign !== $scope.reportFilter.campaign) {

        _displayConfirmDialog($scope, $("#confirmStrikingFactImpact"), 400, performCampaignChange);

      } else {
        performCampaignChange.process();
      }
    };

    $scope.loadCreationContextFromDomain = function() {

      var performDomainChange = {
        process : function() {
          fromDomainId = $scope.reportFilter.domainId;
          $scope.reportFilter.growingSystemId = null;
          $scope.growingSystemId = null;
          $scope.fromGrowingSystemId = null;
          $scope.growingSystems = null;
          $scope.reportRegionals = null;
          $scope.loadCreationContext();
        },
        cancelChanges: function() {
          $scope.reportFilter.domainId = fromDomainId;
        }
      };

      if (fromDomainId && fromDomainId !== $scope.reportFilter.domainId && $scope.reportFilter.growingSystemId) {
        _displayConfirmDialog($scope, $("#confirmStrikingFactImpact"), 400, performDomainChange);
      } else {
        performDomainChange.process();
      }

    };

    // user action (toogle checkbox)

    $scope.selectedSectors = {};
    var _sectorCheckBoxInit = function() {
      if ($scope.reportGrowingSystem && $scope.reportGrowingSystem.sectors) {
        angular.forEach($scope.i18n.Sector, function(trad, sector){
          $scope.selectedSectors[sector] = $scope.reportGrowingSystem.sectors.indexOf(sector) !== -1;
        });
      }
    };

    _sectorCheckBoxInit();

    $scope.toggleSelectedSector = function(sector) {
      if ($scope.reportGrowingSystem.sectors.indexOf(sector) !== -1) {
        if ($scope.reportFilter.growingSystemId) {
          var toggleSelectedSectorModel = {
            process : function() {
              var index = $scope.reportGrowingSystem.sectors.indexOf(sector);
              $scope.reportGrowingSystem.sectors.splice(index, 1);
              $scope.selectedSectors[sector] = false;
              $scope.loadCreationContext();
            },
            cancelChanges: function() {
              $scope.selectedSectors[sector] = true;
            }
          };

          _displayConfirmDialog($scope, $("#confirmStrikingFactImpact"), 400, toggleSelectedSectorModel);
        }
      } else {
        $scope.reportGrowingSystem.sectors.push(sector);
        $scope.selectedSectors[sector] = true;
        $scope.loadCreationContext();
      }
    };

    $scope.getAgressorLabel = function(pestMaster) {
      return (pestMaster.codeGroupeCibleMaa && $scope.groupesCiblesByCode[pestMaster.codeGroupeCibleMaa].groupeCibleMaa || "-")
             + " / " + (pestMaster.agressor && pestMaster.agressor.reference_label || "-");
    };

    // ng-repeat filter
    $scope.isGroupeCibleInCategory = function(category) { return function(groupeCible) {
      return $scope.treatmentTargetCategoriesByParent[category].bioAgressorTypes.includes(groupeCible.referenceParam);
    }};

    $scope.inGroupeCibleBioAgressor = function(pestMaster) { return function(agressor) {
      return !pestMaster || !pestMaster.codeGroupeCibleMaa ||
             $scope.groupesCiblesByCode[pestMaster.codeGroupeCibleMaa].cibleEdiRefCodes.includes(agressor.reference_code || agressor.identifiant);
    }};

    $scope.disableWhenMain = function(aggressor) {
        return !aggressor.main
    }
  }
]);

AgrosystModule.controller('ReportGrowingSystemHighlightController', ['$scope', '$http', 'displayQuestionDialogService', 'displayInfoDialogService', 'ReportGrowingSystemInitData',
  function($scope, $http, displayQuestionDialogService, displayInfoDialogService, ReportGrowingSystemInitData) {

    $scope.forceDisplayAll = false;
    $scope.isDephyExpeAndNotDisplayForced = function() { return $scope.isDephyExpe && !$scope.forceDisplayAll };
    $scope.iftEstimationMethods = ReportGrowingSystemInitData.iftEstimationMethods;

    // contenu de la liste déroulante sur les méthodes d'estimations
    $scope.iftEstimationMethodOptions = [
      {
        value: '',
        label: '',
        enabled: true
      }
    ];

    // ajoute toutes les méthodes connues à la liste déroulante sur les méthodes d'estimations
    Object.keys(ReportGrowingSystemInitData.allIftEstimationMethods)
      .map(function (value) {
        return {
          value: value,
          label: ReportGrowingSystemInitData.allIftEstimationMethods[value],
          enabled: true
        }
      }).forEach(function (option) {
        $scope.iftEstimationMethodOptions.push(option);
      });

    // selon la liste de méthode passée, (dés-)active les options de la liste déroulante
    // sélectionne l'unique option disponible le cas échéant
    $scope.setIftEstimationMethodOptions = function (values) {
      var onlyOneOptionAvailable = values.length === 1;
      var blockEmptyOption = onlyOneOptionAvailable;
      $scope.iftEstimationMethodOptions.forEach(option => {
        if (option.value === '') {
          option.enabled = !blockEmptyOption;
        } else {
          option.enabled = values.includes(option.value);
        }
      });
      if (onlyOneOptionAvailable) {
        $scope.iftEstimationMethod = values[0];
      }
    }

    // lors de la création d'un bilan de culture échelle système de culture, l'utilisateur
    // peut changer le système de culture. Cela fait varier la liste des méthodes autorisées
    // donc on met à jour la liste déroulante en conséquence
    $scope.$watch('growingSystem', function (newValue) {
      if (newValue) {
        $scope.setIftEstimationMethodOptions(newValue.iftEstimationMethods);
      }
    });

    // (dés)active les options de méthode quand on est en modification d'un BDC existant
    if (ReportGrowingSystemInitData.iftEstimationMethods) {
      $scope.setIftEstimationMethodOptions(ReportGrowingSystemInitData.iftEstimationMethods);
    }

    // initialisation de la liste déroulante en modification d'un BDC existant
    if (ReportGrowingSystemInitData.reportGrowingSystem.iftEstimationMethod) {
      $scope.iftEstimationMethod = ReportGrowingSystemInitData.reportGrowingSystem.iftEstimationMethod;
    }

    $scope.isOtherThanArboAndViti = function() {
      return $scope.reportGrowingSystem.sectors.some(sector => sector != "ARBORICULTURE" && sector != "VITICULTURE");
    }

    $scope.onDisplayInfoOk = function() {
        displayInfoDialogService.closeDialog();
    };

    $scope.onDisplayQuestionYes = function() {
        displayQuestionDialogService.closeDialog();
    };

    $scope.onDisplayQuestionNo = function() {
        displayQuestionDialogService.cancelDialog();
    };

    $scope.displayQuestionMessage = function(title, message) {
      $scope.message = message;
      displayQuestionDialogService.setOption("title", title);

      var promise = displayQuestionDialogService.openDialog();
      promise.then(function() {
        delete $scope.message;
      }, function() {
        delete $scope.message;
      });
      return promise; // return initial promise (not then result)
    };

    $scope.displayErrorMessage = function(message) {
      $scope.message = message;
      displayInfoDialogService.setOption("title", "Erreur");
      return displayInfoDialogService.openDialog().then(function() {
        delete $scope.message;
      }, function() {
        delete $scope.message;
      });
    };
  }
]);
