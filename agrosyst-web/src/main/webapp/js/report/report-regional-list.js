/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('ReportRegionalListController', ['$scope', '$http', '$filter', '$timeout', 'ReportRegionalListInitData', 'relatedReportGrowingSystemNames', 'confirmDeleteReportRegionalsDialogService', 'failedDeleteReportGrowingSystemsDialogService', 'reportRegionalsExportAsyncThreshold',
  function($scope, $http, $filter, $timeout, ReportRegionalListInitData, relatedReportGrowingSystemNames, confirmDeleteReportRegionalsDialogService, failedDeleteReportGrowingSystemsDialogService, reportRegionalsExportAsyncThreshold) {
    // liste des bilans affichés
    $scope.reportRegionals = ReportRegionalListInitData.reportRegionals.elements;
    // pagination
    $scope.pager = ReportRegionalListInitData.reportRegionals;
    // filtres actifs
    $scope.filter = ReportRegionalListInitData.filter;
    // rapport sélectionné dans la liste
    $scope.selectedEntities = {};

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined && $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0, 1);
    };

    $scope.asyncThreshold = reportRegionalsExportAsyncThreshold;

    $scope.allSelectedEntities = [];

    $scope.relatedReportGrowingSystemNames = relatedReportGrowingSystemNames;

    $scope.toggleSelectedEntities = function() {
      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.reportRegionals);
    };

    $scope.toggleSelectedEntity = function(entityId) {
      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);
    };

    $scope.clearSelection = function() {
      $scope.selectedEntities = {};
      $scope.allSelectedEntities = [];
    };

    // refresh list when filter change
    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) return;

      var localFilter = angular.copy($scope.filter);
      if (localFilter.campaign === "") {
        delete localFilter.campaign;
      }

      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
      // perform ajax
      $http.post($scope.endpoints.reportRegionalsListJson, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          $scope.reportRegionals = response.data.elements;
          $scope.pager = response.data;
      })
      .catch(function(response) {
        console.error("Can't get report regionals list", response);
        var message = "Échec de récupération des bilans de campagne echelle regionale";
        addPermanentError(message, response.status);
      });
    };

    // refresh list when filter change
    $scope.selectAllEntities = function() {
      var localFilter = angular.copy($scope.filter);
      if (localFilter.campaign === "") {
        delete localFilter.campaign;
      }

      displayPageLoading();
      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
      // perform ajax
      $http.post($scope.endpoints.reportRegionalIdsJson, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          var selectedEntities = {};
          angular.forEach(response.data, function(topiaId) {
            selectedEntities[topiaId] = true;
          });
          Object.assign($scope.selectedEntities, selectedEntities);
      })
      .catch(function(response) {
        console.error("Can't get report regional ids", response);
        var message = "Échec de récupération des ids des bilans de campagne echelle regionale";
        addPermanentError(message, response.status);
      })
      .finally(function () {
        hidePageLoading();
      });
    };

    $scope.$watch('selectedEntities', function(newValue, oldValue) {
      // active property and first selected
      angular.forEach($scope.reportRegionals, function(item) {
        if ($scope.selectedEntities[item.topiaId]) {
          $scope.firstSelectedReport = item; // make sense when size() == 1
        }
      });
    }, true);

    //watch with timer
    var timer = false;
    $scope.$watch('[filter.name, filter.campaign, filter.network]', function(newValue, oldValue) {
        if (timer) {
            $timeout.cancel(timer);
        }
        timer = $timeout(function() {
          let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
        }, 350);
      },
      true
    );
    $scope.$watch('[filter.sector]', function(newValue, oldValue) {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      },
      true
    );

    //watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    // user action
    $scope.reportRegionalDelete = function(deleteFormSelector) {
      confirmDeleteReportRegionalsDialogService.openDialog().then(function() {
        var deleteForm = $(deleteFormSelector);
        deleteForm.attr("action", $scope.endpoints.reportRegionalDelete);
        deleteForm.submit();
      }, function() {});
    };

    $scope.reportRegionalDeleteClose = confirmDeleteReportRegionalsDialogService.closeDialog.bind(confirmDeleteReportRegionalsDialogService);
    $scope.reportRegionalDeleteCancel = confirmDeleteReportRegionalsDialogService.cancelDialog.bind(confirmDeleteReportRegionalsDialogService);

    // user action
    $scope.reportRegionalExportXls = function(exportFormSelector) {
      var exportForm = $(exportFormSelector);
      exportForm.attr("action", $scope.endpoints.reportRegionalExportXls);
      exportForm.submit();
    };

    $scope.asyncReportRegionalExportXls = function() {
      var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

      var data = "reportRegionalIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
      $http.post(
          ENDPOINTS.reportRegionalExportXlsAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };

    // display box
    if ($scope.relatedReportGrowingSystemNames && $scope.relatedReportGrowingSystemNames.length > 0) {

      $scope.deleteReportMessage = $scope.relatedReportGrowingSystemNames[0].text;// it can have only one
      failedDeleteReportGrowingSystemsDialogService.openDialog();
    }

    $scope.failedDeleteReportGrowingSystemsClose = failedDeleteReportGrowingSystemsDialogService.closeDialog.bind(failedDeleteReportGrowingSystemsDialogService);


  }
]);
