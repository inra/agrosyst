/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('VitiPestMastersController', ['$scope', '$http', '$q', '$timeout', 'ReportGrowingSystemInitData', 'addVitiDiseaseDialogService', 'addVitiPestDialogService',
  function($scope, $http, $q, $timeout, ReportGrowingSystemInitData, addVitiDiseaseDialogService, addVitiPestDialogService) {
    $scope.yieldObjectives = ReportGrowingSystemInitData.yieldObjectives;
    // toggle boolean
    $scope.showVitiPestMasters = false;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);
    // data (lazy loading)
    $scope.vitiPestMasters = null;
    // view port index
    $scope.viewPortIndex = 0;
    // required diseases
    var MILDIOU = 19089;
    var OIDIUM = 19224;
    var VIROSE = 19725;
    var BOTRYTIS = 18536;
    $scope.requiredDiseases = [MILDIOU, OIDIUM, VIROSE, BOTRYTIS];
    // required pests
    var CITADELLE_FLAVESCENCE = 18721;
    var CITADELLE_GRILLURES = 18722;
    var TORDEUSE_GRAPPE = 999996;
    $scope.requiredPests = [CITADELLE_FLAVESCENCE, CITADELLE_GRILLURES, TORDEUSE_GRAPPE];

    var updateViewPortArray = function() {
      if ($scope.vitiPestMasters) {
        $scope.maxViewPort = $scope.vitiPestMasters.length;
        $scope.viewPortArray = $scope.vitiPestMasters.slice($scope.viewPortIndex, $scope.viewPortIndex + 3);
      }
    };

    var _loadVitiPestMasters = function() {

      displayPageLoading();
      var endpoint = $scope.isDisease ? $scope.endpoints.reportGrowingSystemVitiDiseaseMasters :
                                        $scope.endpoints.reportGrowingSystemVitiPestMasters;

      return $http.get(endpoint + "?reportGrowingSystemId=" + encodeURIComponent($scope.reportGrowingSystemId))
        .then(function(response) {
          $scope.vitiPestMasters = response.data;
          if ($scope.vitiPestMasters && $scope.vitiPestMasters.length > 0 && !$scope.vitiPestMasters[0].topiaId) {
            var message = $scope.isDisease ? "'Maitrise des maladies' pour la Viticulture" : "'Maîtrise des ravageurs' pour la Viticulture";
            addPermanentWarning("Les données de " + message + " ont été générées automatiquement, vous devez completer les données obligatoires !");
          }
          hidePageLoading();
        })
        .catch(function(response) {
          hidePageLoading();
          console.error("Échec de récupération des données de " + endpoint , response);
          addPermanentError("Échec de récupération des données du tableau " + endpoint + " erreur:" + response.message, response.status);
        });

    };

    // user action : clic toggle
    $scope.loadVitiPestMasters = function() {
      if ($scope.isDisease) {
        $scope.showVitiDiseasePestMasters = !$scope.showVitiDiseasePestMasters;
      } else {
        $scope.showVitiPestMasters = !$scope.showVitiPestMasters;
      }

      if (!$scope.vitiPestMasters) {
        _loadVitiPestMasters()
          .then(updateViewPortArray);
      }
    };

    $scope.init = function(isDisease) {
      $scope.isDisease = isDisease;
      $scope.showVitiPestMasters = false;

      if (isDisease) {
        $scope.vitiPestMasters = ReportGrowingSystemInitData.highlights.vitiDiseaseMasters;
      } else {
        $scope.vitiPestMasters = ReportGrowingSystemInitData.highlights.vitiPestMasters;
      }
      if ($scope.vitiPestMasters) {
        updateViewPortArray();
      }
    };

    // auto load bio agressor select
    var _loadBioAgressors = function() {
      var bioAgressorTypes = $scope.isDisease ? ['MALADIE','VIRUS'] : ['RAVAGEUR'];
      var url = $scope.endpoints.bioAgressorsJson + "?bioAgressorTypes=" + encodeURIComponent(JSON.stringify(bioAgressorTypes)) + "&sectors=VITICULTURE";

      return $http.get(url, {cache: true})
        .then(function(response) {
          $scope.bioAgressors = response.data;
        })
        .catch(function(response) {
          console.error("Échec de chargement des bio agresseurs", response);
          addPermanentError("Échec de chargement des bio agresseurs", response.status);
        });
    };

    // fix object in edited object to match instance in bioAgressors list
    var _fixEditedBioAgressor = function() {
      if ($scope.editedPest.agressor) {
          angular.forEach($scope.bioAgressors, function(bioAgressor) {
            if ($scope.editedPest.agressor.topiaId == bioAgressor.topiaId) {
              $scope.editedPest.agressor = bioAgressor;
            }
          });
      }
    };

    // display box
    var _displayEditPestBox = function() {
      ($scope.isDisease ? addVitiDiseaseDialogService : addVitiPestDialogService).openDialog()
        .then(function() {
          if ($scope.editedPest.$$original) {
            var idx = $scope.vitiPestMasters.indexOf($scope.editedPest.$$original);
            $scope.vitiPestMasters[idx] = $scope.editedPest;
            delete $scope.editedPest.$$original;
          } else {
            $scope.vitiPestMasters.push($scope.editedPest);
          }

          $scope.editedPest = null;
          updateViewPortArray();
        }, function() {
          $scope.editedPest = null;
        });

        _loadBioAgressors()
            .then(_fixEditedBioAgressor);
    };

    // user action
    $scope.addVitiPest = function() {
      $scope.editedPest = {
        agressors: []
      };
      _displayEditPestBox();
    };

    // user action
    $scope.editVitiPest = function(pest) {
      $scope.editedPest = angular.copy(pest);
      $scope.editedPest.$$original = pest;
      if ($scope.editedPest.agressor) {
        $scope.editedPest.changeableDisease =
          $scope.requiredDiseases.indexOf($scope.editedPest.agressor.reference_id) !== -1 ||
          $scope.requiredPests.indexOf($scope.editedPest.agressor.reference_id) !== -1;
      } else {
        $scope.editedPest.changeableDisease = false;
      }
      _displayEditPestBox();
    };

    // user action
    $scope.deleteVitiPest = function(vitipest) {
      var index = $scope.vitiPestMasters.indexOf(vitipest);
      if (index !== -1) {
        $scope.vitiPestMasters.splice(index, 1);
        updateViewPortArray();
      }
    };

    $scope.onDialogOk = function() {
      if(!$scope.editedPest.agressor && !$scope.editedPest.codeGroupeCibleMaa ||
        angular.isUndefined($scope.editedPest.treatmentCount) ||
        angular.isUndefined($scope.editedPest.chemicalFungicideIFT) ||
        angular.isUndefined($scope.editedPest.bioControlFungicideIFT) ||
        (!$scope.isDephyExpe && (
            angular.isUndefined($scope.editedPest.pressureScale) ||
            angular.isUndefined($scope.editedPest.masterScale) ||
            angular.isUndefined($scope.editedPest.qualifier))
        )
      ) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        ($scope.isDisease ? addVitiDiseaseDialogService : addVitiPestDialogService).closeDialog();
      }
    };

    $scope.onDialogCancel = function() {
        ($scope.isDisease ? addVitiDiseaseDialogService : addVitiPestDialogService).cancelDialog();
    };

    // user action : click on button
    $scope.addAgressor = function() {
      $scope.editedPest.agressors.push($scope.selectedAgressor);
      $scope.selectedAgressor = null;
    };

    // user action : click on button
    $scope.removeAgressor = function(agressor) {
      var index = $scope.editedPest.agressors.indexOf(agressor);
      if (index >= 0) {
        $scope.editedPest.agressors.splice(index, 1);
      }
    };

    $scope.moveViewPort = function(inc) {
      $scope.viewPortIndex += inc;
      if ($scope.viewPortIndex + 3 > $scope.maxViewPort) $scope.viewPortIndex = $scope.maxViewPort - 3;
      if ($scope.viewPortIndex < 0) $scope.viewPortIndex = 0;
      updateViewPortArray();
    };

    $scope.isCicadelleFlavescenceDoree = function(vitiPestMaster) {
      return vitiPestMaster && vitiPestMaster.agressor && vitiPestMaster.agressor.reference_id == CITADELLE_FLAVESCENCE;
    }

    $scope.isTordeuseGrappe = function(vitiPestMaster) {
      return vitiPestMaster && vitiPestMaster.agressor && vitiPestMaster.agressor.reference_id == TORDEUSE_GRAPPE;
    }

    $scope.onOpenVitiPestGroupeCible = function(isOpen) {
      $scope._onOpenUiSelect("#vitiPestGroupeCibleField", isOpen)
    }

    $scope.onOpenVitiDiseaseGroupeCible = function(isOpen) {
      $scope._onOpenUiSelect("#vitiDiseaseGroupeCibleField", isOpen)
    }

    $scope.onOpenVitiPestAgressor = function(isOpen) {
      $scope._onOpenUiSelect("#vitiPestAgressor", isOpen)
    }

    $scope.onOpenVitiDiseaseAgressor = function(isOpen) {
      $scope._onOpenUiSelect("#vitiDiseaseAgressor", isOpen)
    }

    $scope._onOpenUiSelect = function(id, isOpen) {
      if (isOpen) {
        var input = document.querySelector(id).getElementsByClassName("select2-input")[0]
        input.focus()
        input.select()
      }
    }
  }
]);

AgrosystModule.controller('VitiAdventiceMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {
    $scope.showVitiAdventiceMasters = false;

    $scope.loadVitiAdventiceMasters = function() {
      $scope.showVitiAdventiceMasters = !$scope.showVitiAdventiceMasters;
    };
  }
]);

AgrosystModule.controller('VitiYieldMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {
    $scope.yieldObjectives = ReportGrowingSystemInitData.yieldObjectives;
    $scope.vitiYieldLossCauses = ReportGrowingSystemInitData.vitiYieldLossCauses;

    $scope.yieldInfo = $scope.reportGrowingSystem.yieldInfos && $scope.reportGrowingSystem.yieldInfos.find(info => info.sector === "VITICULTURE");
    if (!$scope.yieldInfo) {
      $scope.yieldInfo = { sector: "VITICULTURE" };
    }

    $scope.showVitiYields = false;

    $scope.loadVitiYields = function() {
      $scope.showVitiYields = !$scope.showVitiYields;
    };
  }
]);

AgrosystModule.controller('CropVitiFoodMastersController', ['$scope', 'ReportGrowingSystemInitData', 'addVitiFoodDialogService',
  function($scope, ReportGrowingSystemInitData, addVitiFoodDialogService) {
    $scope.setSpecificContext({
      cropMasters: ReportGrowingSystemInitData.highlights.vitiFoodMasters,
      sectionEndpoint: $scope.endpoints.reportGrowingSystemVitiFoodMasters,
      boxCropDiv: addVitiFoodDialogService
    });

    $scope.onCropDialogOk = function() {
      if(!$scope.editedCropObject.crops.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        addVitiFoodDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
        addVitiFoodDialogService.cancelDialog();
    };
  }
]);
