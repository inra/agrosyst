/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('ReportGrowingSystemListController', ['$scope', '$http', '$timeout', '$filter', 'ReportGrowingSystemListInitData',
    'reportGrowingSystemFilter', 'deleteReportGrowingSystemsDialogService', 'exportReportGrowingSystemsDialogService', 'reportGrowingSystemsExportAsyncThreshold',
  function($scope, $http, $timeout, $filter, ReportGrowingSystemListInitData, reportGrowingSystemFilter,
        deleteReportGrowingSystemsDialogService, exportReportGrowingSystemsDialogService, reportGrowingSystemsExportAsyncThreshold) {

    // {Array} elements list
    $scope.reportGrowingSystems = ReportGrowingSystemListInitData.elements;
    // {Object} list pager
    $scope.pager = ReportGrowingSystemListInitData;
    // {Object} User input filter
    $scope.filter = reportGrowingSystemFilter;

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined && $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0,1);
    };

    $scope.asyncThreshold = reportGrowingSystemsExportAsyncThreshold;

    $scope.selectedEntities = {};

    $scope.allSelectedEntities = [];

    $scope.allSelectedEntities[$scope.pager.currentPage.pageNumber] = {selected : false};

    $scope.toggleSelectedEntities = function() {

      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.reportGrowingSystems);

    };

    $scope.toggleSelectedEntity = function(entityId) {

      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);

    };

    $scope.clearSelection = function() {
          $scope.selectedEntities = {};
          $scope.allSelectedEntities = [];
    };

    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) return;

      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.filter);
      if (localFilter.campaign === "") {
        delete localFilter.campaign;
      }

      $http.post($scope.endpoints.reportGrowingSystemsListJson, "filter=" + encodeURIComponent(angular.toJson(localFilter)),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          $scope.reportGrowingSystems = response.data.elements;
          $scope.pager = response.data;
      })
      .catch(function(response) {
         console.error("Échec de récupération des bilans de campagne à l'échelle des systèmes de culture", response);
         addPermanentError("Échec de récupération des bilans de campagne à l'échelle des systèmes de culture", response.status);
      });
    };

    // refresh list when filter change
    $scope.selectAllEntities = function() {
      var localFilter = angular.copy($scope.filter);
      if (localFilter.campaign === "") {
        delete localFilter.campaign;
      }

      displayPageLoading();
      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
      // perform ajax
      $http.post($scope.endpoints.reportGrowingSystemIdsJson, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          var selectedEntities = {};
          angular.forEach(response.data, function(topiaId) {
            selectedEntities[topiaId] = true;
          });
          Object.assign($scope.selectedEntities, selectedEntities);
      })
      .catch(function(response) {
        console.error("Échec de récupération des ids des bilans de campagne à l'échelle des systèmes de culture", response);
                 addPermanentError("Échec de récupération des ids des bilans de campagne à l'échelle des systèmes de culture", response.status);
      })
      .finally(function () {
        hidePageLoading();
      });
    };

    //watch with timer
    var timer = false;
    $scope.$watch('[filter.name, filter.domainName, filter.growingPlanName, filter.growingSystemName, filter.campaign, filter.dephyNumber]',
      function(newValue, oldValue) {
        if (timer) {
          $timeout.cancel(timer);
        }
        timer = $timeout(function() {
          let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
        }, 350);
      },
      true
    );

    //watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.$watch('selectedEntities', function(newValue, oldValue) {
          // active property and first selected
          angular.forEach($scope.reportGrowingSystems, function(item) {
            if ($scope.selectedEntities[item.topiaId]) {
              $scope.firstSelectedReport = item; // make sense when size() == 1
            }
          });
        }, true);

    // user action
    $scope.reportGrowingSystemDelete = function(deleteFormSelector) {
      deleteReportGrowingSystemsDialogService.openDialog().then(function() {
          var deleteForm = $(deleteFormSelector);
          deleteForm.attr("action", $scope.endpoints.reportGrowingSystemDelete);
          deleteForm.submit();
      }, function() {});
    };
    $scope.deleteReportGrowingSystemsClose = deleteReportGrowingSystemsDialogService.closeDialog.bind(deleteReportGrowingSystemsDialogService);
    $scope.deleteReportGrowingSystemsCancel = deleteReportGrowingSystemsDialogService.cancelDialog.bind(deleteReportGrowingSystemsDialogService);

    // user action
    $scope.reportGrowingSystemExportPdf = function(exportFormSelector) {

      var availabilityUrl = $scope.endpoints.reportGrowingSystemSections + "?";
      angular.forEach($scope.selectedEntities, function(value, key) {
        if (value) {
          availabilityUrl += "&reportGrowingSystemIds=" + key;
        }
      });

      var ajaxRequest = "";
      angular.forEach($scope.selectedEntities, function(value, key) {
        if (value) {
          ajaxRequest += "&reportGrowingSystemIds=" + key;
        }
      });
      // perform ajax
      $http.post($scope.endpoints.reportGrowingSystemSections, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .then(function(response) {
          $scope.includeReportRegional = true;
          // filter report section type with availability
          $scope.exportReportSections = {};
          $scope.availableReportGrowingSystemSections = {};
          let reportGrowingSystem = response.data.reportGrowingSystem;
          angular.forEach(reportGrowingSystem, function(sections, sector) {
            $scope.availableReportGrowingSystemSections[sector] = {};
            $scope.exportReportSections[sector] = [];
            angular.forEach($scope.i18n.ReportGrowingSystemSection, function(value, key) {
              // disponible sur au moins un des bilans
              if (sections.indexOf(key) != -1) {
                $scope.availableReportGrowingSystemSections[sector][key] = value;
                $scope.exportReportSections[sector].push(key); // selection par defaut
              }
            });
          });

          // filter section stype with availability
          $scope.availableSectionType = {};
          angular.forEach($scope.i18n.SectionType, function(value, key) {
            if (response.data.managementMode.indexOf(key) != -1) {
              $scope.availableSectionType[key] = value;
            }
          });
          $scope.exportManagementSections = [];
          $scope.exportManagementDisabled = [];
          $scope.managementModeAvailable = response.data.available;
          return exportReportGrowingSystemsDialogService.openDialog();
        })
        .then(function() {

            var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);
            if (selectedForAsyncExport && selectedForAsyncExport.length >= $scope.asyncThreshold) {

              var data = "reportGrowingSystemIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
              data += "&reportGrowingSystemSectionsJson=" + encodeURIComponent(angular.toJson($scope.exportReportSections));
              angular.forEach($scope.exportManagementSections, function(s) {
                data += "&exportOptions.managementSections=" + encodeURIComponent(s);
              });
              data += "&exportOptions.includeReportRegional=" + encodeURIComponent($scope.includeReportRegional);
              $http.post(
                  ENDPOINTS.reportGrowingSystemExportPdfAsync,
                  data,
                  {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                )
                .then(function(response) {
                  addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
                })
                .catch(function(response) {
                  addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
                  console.error("Impossible de lancer l'export asynchrone", response);
                });

            } else {

              var exportForm = $(exportFormSelector);
              // ?includeManagementMode=true
              exportForm.attr("action", $scope.endpoints.reportGrowingSystemExportPdf);
              exportForm.submit();
            }
        })
        .catch(function(response) {
            $scope.exportReportSections = null;
            $scope.exportManagementSections = null;
            $scope.exportManagementDisabled = null;
        });
    };
    $scope.exportReportGrowingSystemsClose = exportReportGrowingSystemsDialogService.closeDialog.bind(exportReportGrowingSystemsDialogService);
    $scope.exportReportGrowingSystemsCancel = exportReportGrowingSystemsDialogService.cancelDialog.bind(exportReportGrowingSystemsDialogService);

    // callback
    var computeDisabledManagementSections = function() {
      $scope.exportManagementDisabled = [];
      let withAdventice = false;
      let withDisease = false;
      let withPest = false;
      let withVerse = false;
      angular.forEach(Object.keys($scope.exportReportSections), function(sector) {
        withAdventice |= $scope.exportReportSections[sector].indexOf("MASTER_ADVENTICE") != -1;
        withDisease |= $scope.exportReportSections[sector].indexOf("MASTER_DISEASE") != -1;
        withPest |= $scope.exportReportSections[sector].indexOf("MASTER_PEST") != -1;
        withVerse |= $scope.exportReportSections[sector].indexOf("MASTER_VERSE") != -1;
      });
      disableSection(withAdventice, "ADVENTICES");
      disableSection(withDisease, "MALADIES");
      disableSection(withPest, "RAVAGEURS");
      disableSection(withVerse, "MAITRISE_DES_DOMMAGES_PHYSIQUES");
    };

    var disableSection = function(withSection, section) {
      if (!withSection) {
        $scope.exportManagementDisabled.push(section);
        let index = $scope.exportManagementSections.indexOf(section);
        if (index != -1) {
          $scope.exportManagementSections.splice(index, 1);
        }
      }
    }

    // user action
    $scope.toggleExportSection = function(sector, section) {
      var index = $scope.exportReportSections[sector].indexOf(section);
      if (index == -1) {
        $scope.exportReportSections[sector].push(section);
      } else {
        $scope.exportReportSections[sector].splice(index, 1);
      }
      computeDisabledManagementSections();
    };

    // user action
    $scope.toggleManagementSection = function(section) {
      var index = $scope.exportManagementSections.indexOf(section);
      if (index == -1) {
        $scope.exportManagementSections.push(section);
      } else {
        $scope.exportManagementSections.splice(index, 1);
      }
    };

    // methode pas très utile, mais impossible à faire coté template :(
    $scope.isNotEmpty = function(obj) {
      return obj && Object.keys(obj).length > 0;
    };

    // user action
    $scope.reportGrowingSystemExportXls = function(exportFormSelector) {
      var exportForm = $(exportFormSelector);
      exportForm.attr("action", $scope.endpoints.reportGrowingSystemExportXls);
      exportForm.submit();
    };

    $scope.asyncReportGrowingSystemExportXls = function() {
      var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

      var data = "reportGrowingSystemIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
      $http.post(
          ENDPOINTS.reportGrowingSystemExportXlsAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };

  }
]);
