/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('ReportRegionalController', ['$scope', '$timeout', 'ReportRegionalInitData',
  function($scope, $timeout, ReportRegionalInitData) {
    $scope.displayFieldsErrorsAlert = true;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);
    // le bilan de campagne en cours d'edition
    $scope.reportRegional = ReportRegionalInitData.reportRegional;
    $scope.reportRegional.networks = ReportRegionalInitData.reportRegionalNetworks;
    $scope.campaignsBounds = ReportRegionalInitData.campaignsBounds;
    $scope.networkIdsUsed = ReportRegionalInitData.networkIdsUsed;
    $scope.groupesCibles = ReportRegionalInitData.groupesCibles;
    $scope.groupesCiblesByCode = {};
    for (var i = 0 ; i < $scope.groupesCibles.length ; i++) {
      var groupeCible = $scope.groupesCibles[i];
      $scope.groupesCiblesByCode[groupeCible.codeGroupeCibleMaa] = groupeCible;
    }
    $scope.diseaseBioAgressorParentType = ReportRegionalInitData.diseaseBioAgressorParentType;
    $scope.pestBioAgressorParentType = ReportRegionalInitData.pestBioAgressorParentType;
  }]);

AgrosystModule.controller('ReportRegionalMainController', ['$scope',
  function($scope) {

    // init empty data
    if ($scope.reportRegional.campaign === 0) $scope.reportRegional.campaign = "";
    $scope.reportRegional.sectors = $scope.reportRegional.sectors || [];
    $scope.reportRegional.sectorSpecies = $scope.reportRegional.sectorSpecies || [];

    // user action (toogle checkbox)
    $scope.toggleSelectedSector = function(sector) {
      var index = $scope.reportRegional.sectors.indexOf(sector);
      if (index == -1) {
        $scope.reportRegional.sectors.push(sector);
      } else {
        $scope.reportRegional.sectors.splice(index, 1);
        if (sector === 'ARBORICULTURE') {
          $scope.reportRegional.sectorSpecies = [];
        }
      }
    };

    // user action (toggle checkbox)
    $scope.toggleSelectedSectorSpecies = function(sectorSpecie) {
      var index = $scope.reportRegional.sectorSpecies.indexOf(sectorSpecie);
      if (index == -1) {
        $scope.reportRegional.sectorSpecies.push(sectorSpecie);
      } else {
        $scope.reportRegional.sectorSpecies.splice(index, 1);
      }
    };
  }
]);

AgrosystModule.controller('ReportRegionalNetworkController', ['$scope', '$window',
  function($scope, $window) {

    // Les réseaux du bilan de campagne
    $scope.reportRegionalNetworks = [];

    // init network
    angular.forEach($scope.reportRegional.networks, function(network) {
      $scope.reportRegionalNetworks.push({
        topiaId : network.topiaId,
        name : network.name
      });
    });

    // user action : click on button
    $scope.removeNetwork = function (network) {
      // only remove last networks added
      var confirm;
      confirm = $window.confirm("Voulez-vous supprimer le réseau "+ network.name + " ?");

      var index = $scope.reportRegionalNetworks.indexOf(network);
      if (index >= 0 && confirm) {
        $scope.reportRegionalNetworks.splice(index, 1);
        $scope.reportEditForm.$setDirty();
      }
    };

    // autocomplete callback
    function setNetwork(topiaId, name) {
      var network = {topiaId: topiaId, name: name};
      $scope.reportRegionalNetworks.push(network);
      $scope.$digest(); // force le refresh
      $("#networks").val("");
    }

    $("#networks").autocomplete({
      source : function(request, response) {

        // only user network
        request.onlyResponsibleNetwork = true;

        // On remplace par du POST pour les pbs d'encoding
        $.post(ENDPOINTS.searchNetworkJson, request, function(data, status, xhr) {
          if (data) {
            var autoCompleteField = $("#networks");
            if (Object.keys(data).length === 0) {
              autoCompleteField.addClass("empty-autocomplete-result");
            } else {
              autoCompleteField.removeClass("empty-autocomplete-result");
            }
          }

          var result = [];
          angular.forEach(data, function(value, key) {
              var network = {
                  label: value,
                  value: value,
                  topiaId: key
                };
              result.push(network);
          });

          response(result);
        }, "json");
      },
      minLength : 0,
      select : function(event, ui) {
        if (ui.item) {
          setNetwork(ui.item.topiaId, ui.item.label);
          $scope.reportEditForm.$setDirty();
          return false;
        }
      }
    });

    // user action
    $scope.displayNetworks = function(reportRegionalName, networks) {
      var networkIds = [];
      angular.forEach(networks, function(network) {
          networkIds.push(network.topiaId);
      });

      displayNetworks(reportRegionalName || "Bilan de campagne", networkIds);
    };
  }
]);

AgrosystModule.controller('ReportRegionalPressureController', ['$scope', '$http', '$window', 'ReportRegionalInitData', 'PestEditDialogService',
  function($scope, $http, $window, ReportRegionalInitData, PestEditDialogService) {

    $scope.reportRegional.diseasePressures = $scope.reportRegional.diseasePressures || [];
    $scope.reportRegional.pestPressures = $scope.reportRegional.pestPressures || [];
    $scope.agressor = {}

    $scope.getGroupeCibleLabel = function(codeGroupeCibleMaa) {
      if (!codeGroupeCibleMaa) {
        return "";
      }
      return $scope.groupesCiblesByCode[codeGroupeCibleMaa].groupeCibleMaa;
    };

    // auto load bio agressor select
    $scope.loadBioAgressors = function() {
      var bioAgressorTypes = ($scope.isDisease ? $scope.diseaseBioAgressorParentType : $scope.pestBioAgressorParentType).bioAgressorTypes;
      return $http.get($scope.endpoints.bioAgressorsJson + "?bioAgressorTypes=" + encodeURIComponent(JSON.stringify(bioAgressorTypes)), {cache: true})
        .then(function(response) {
          $scope.bioAgressors = response.data;
        })
        .catch(function(response) {
          console.error("Échec de chargement des bio agresseurs", response);
          addPermanentError("Échec de chargement des bio agresseurs", response.status);
        });
    };

    // display box
    var _displayEditDiseaseBox = function() {
      var title = "Édition d'" + ($scope.isDisease ? "une maladie" : "un ravageur");
      PestEditDialogService.setOption("title", title);
      PestEditDialogService.openDialog().then(function() {
        // depending on validating maladie or ravageur
        var pressureArray = $scope.isDisease ? $scope.reportRegional.diseasePressures : $scope.reportRegional.pestPressures;
        if ($scope.editedPest._original) {
          var idx = pressureArray.indexOf($scope.editedPest._original);
          pressureArray[idx] = $scope.editedPest;
          delete $scope.editedPest._original;
        } else {
          pressureArray.push($scope.editedPest);
        }
      },function() {
        delete $scope.editedPest;
      });
      $scope.loadBioAgressors().then(_fixEditedBioAgressor);
    };

    // user action
    $scope.onEditDiseaseOk = function() {
      if (!$scope.editedPest.agressors.length && !$scope.editedPest.codeGroupeCibleMaa || !$scope.editedPest.crops) {
        $window.alert("Des informations obligatoires sont requises !");
      } else {
        PestEditDialogService.closeDialog();
      }
    };

    // user action
    $scope.onEditDiseaseCancel = function() {
      PestEditDialogService.cancelDialog();
    };

    // fix object in edited object to match instance in bioAgressors list
    var _fixEditedBioAgressor = function() {
      if ($scope.editedPest.agressors) {
          angular.forEach($scope.bioAgressors, function(bioAgressor) {
            angular.forEach($scope.editedPest.agressors, function(agressor, index) {
              if (agressor.topiaId == bioAgressor.topiaId) {
                $scope.editedPest.agressors[index] = bioAgressor;
              }
            });
          });
      }
    };

    // user action
    $scope.addPestOrDisease = function(isDisease) {
      $scope.editedPest = {
        agressors: []
      };
      $scope.isDisease = isDisease;
      _displayEditDiseaseBox();
    };

    // user action
    $scope.editPestOrDisease = function(pest, isDisease) {
      $scope.editedPest = angular.copy(pest);
      $scope.editedPest._original = pest;
      $scope.isDisease = isDisease;
      _displayEditDiseaseBox();
    };

    // user action
    $scope.deletePestOrDisease = function(index, isDisease) {
      var pressureArray = isDisease ? $scope.reportRegional.diseasePressures : $scope.reportRegional.pestPressures;
      pressureArray.splice(index, 1);
    };

    // user action : click on button
    $scope.removeAgressor = function(agressor) {
      var index = $scope.editedPest.agressors.indexOf(agressor);
      if (index >= 0) {
        $scope.editedPest.agressors.splice(index, 1);
      }
    };

    // user action : click on button
    $scope.addAgressor = function(agressor) {
      $scope.editedPest.agressors.push(agressor);
      $scope.agressor = {}
    };

    // ng-repeat filter
     $scope.isGroupeCibleInCategory = function(isDisease) { return function(groupeCible) {
      return  (isDisease ? $scope.diseaseBioAgressorParentType : $scope.pestBioAgressorParentType).bioAgressorTypes.includes(groupeCible.referenceParam);
    }};

    $scope.inGroupeCibleNotUsedBioAgressor = function(agressor) {
      return $scope.editedPest && !$scope.editedPest.agressors.includes(agressor)
        && (!$scope.editedPest.codeGroupeCibleMaa
          || $scope.groupesCiblesByCode[$scope.editedPest.codeGroupeCibleMaa].cibleEdiRefCodes.includes(agressor.reference_code || agressor.identifiant));
    };

    $scope.disableWhenMain = function(agressor) {
      return !agressor.main
    }

    $scope.onOpenGroupeCible = function(isOpen) {
      $scope._onOpenUiSelect("#groupeCibleField", isOpen)
    }

    $scope.onOpenAggressor = function(isOpen) {
      $scope._onOpenUiSelect("#diseaseAgressor", isOpen)
    }

    $scope._onOpenUiSelect = function(id, isOpen) {
      if (isOpen) {
        var input = document.querySelector(id).getElementsByClassName("select2-input")[0]
        input.focus()
        input.select()
      }
    }
  }
]);
