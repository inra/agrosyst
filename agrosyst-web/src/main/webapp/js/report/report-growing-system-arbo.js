/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('CropArboDiseaseMastersController', ['$scope', '$timeout', 'ReportGrowingSystemInitData',
    'addArboDiseaseCropsDialogService', 'addArboDiseaseDialogService',
  function($scope, $timeout, ReportGrowingSystemInitData, addArboDiseaseCropsDialogService, addArboDiseaseDialogService) {

    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

    $scope.setSpecificContext({
      cropPestMasters: ReportGrowingSystemInitData.highlights.arboCropDiseaseMasters,
      bioAgressorTypes: ReportGrowingSystemInitData.treatmentTargetCategoriesByParent['MALADIE'].bioAgressorTypes,
      getSectors: () => ['ARBORICULTURE'],
      sectionEndpoint: $scope.endpoints.reportGrowingSystemArboDiseaseMasters,
      boxCropDiv: addArboDiseaseCropsDialogService,
      boxDiv: addArboDiseaseDialogService
    });

    $scope.onCropDialogOk = function() {
      if (!$scope.editedCropObject.crops.length || !$scope.editedCropObject.pestMasters.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        addArboDiseaseCropsDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
        addArboDiseaseCropsDialogService.cancelDialog();
    };

    $scope.onPestDialogOk = function() {
      if ((!$scope.editedPestMaster.agressor && !$scope.editedPestMaster.codeGroupeCibleMaa) ||
            (!$scope.isDephyExpe && (!$scope.editedPestMaster.masterScale || !$scope.editedPestMaster.qualifier))) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        addArboDiseaseDialogService.closeDialog();
      }
    };

    $scope.onPestDialogCancel = function() {
        addArboDiseaseDialogService.cancelDialog();
    };
  }
]);

AgrosystModule.controller('CropArboPestMastersController', ['$scope', 'ReportGrowingSystemInitData',
    'addArboPestCropsDialogService', 'addArboPestDialogService',
  function($scope, ReportGrowingSystemInitData, addArboPestCropsDialogService, addArboPestDialogService) {
    $scope.setSpecificContext({
      cropPestMasters: ReportGrowingSystemInitData.highlights.arboCropPestMasters,
      bioAgressorTypes: ReportGrowingSystemInitData.treatmentTargetCategoriesByParent['RAVAGEUR'].bioAgressorTypes,
      getSectors: () => ['ARBORICULTURE'],
      sectionEndpoint: $scope.endpoints.reportGrowingSystemArboPestMasters,
      boxCropDiv: addArboPestCropsDialogService,
      boxDiv: addArboPestDialogService
    });

    $scope.onCropDialogOk = function() {
      if (!$scope.editedCropObject.crops.length || !$scope.editedCropObject.pestMasters.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
          addArboPestCropsDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
        addArboPestCropsDialogService.cancelDialog();
    };

    $scope.onPestDialogOk = function() {
      if((!$scope.editedPestMaster.agressor && !$scope.editedPestMaster.codeGroupeCibleMaa) ||
            (!$scope.isDephyExpe && (!$scope.editedPestMaster.masterScale || !$scope.editedPestMaster.qualifier))) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        addArboPestDialogService.closeDialog();
      }
    };

    $scope.onPestDialogCancel = function() {
        addArboPestDialogService.cancelDialog();
    };
  }
]);

AgrosystModule.controller('CropArboAdventiceMastersController', ['$scope', 'ReportGrowingSystemInitData',
    'addArboAdventiceCropsDialogService', 'addArboAdventiceDialogService',
  function($scope, ReportGrowingSystemInitData, addArboAdventiceCropsDialogService, addArboAdventiceDialogService) {
    $scope.setSpecificContext({
      cropPestMasters: ReportGrowingSystemInitData.highlights.arboCropAdventiceMasters,
      bioAgressorTypes: ReportGrowingSystemInitData.treatmentTargetCategoriesByParent['ADVENTICE'].bioAgressorTypes,
      getSectors: () => ['ARBORICULTURE'],
      sectionEndpoint: $scope.endpoints.reportGrowingSystemArboAvendiceMasters,
      boxCropDiv: addArboAdventiceCropsDialogService,
      boxDiv: addArboAdventiceDialogService
    });

    $scope.onCropDialogOk = function() {
      addArboAdventiceCropsDialogService.closeDialog();
    };

    $scope.onCropDialogCancel = function() {
        addArboAdventiceCropsDialogService.cancelDialog();
    };

    $scope.onPestDialogOk = function() {
      if ($scope.isDephyExpe) {
        $scope.editedPestMaster.agressor = $scope.bioAgressors
            .find(agressor => agressor.identifiant === ReportGrowingSystemInitData.defaultArboAdventiceId);
      }
      if((!$scope.editedPestMaster.agressor && !$scope.editedPestMaster.codeGroupeCibleMaa) ||
            (!$scope.isDephyExpe && (!$scope.editedPestMaster.masterScale || !$scope.editedPestMaster.qualifier))) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        addArboAdventiceDialogService.closeDialog();
      }
    };

    $scope.onPestDialogCancel = function() {
        addArboAdventiceDialogService.cancelDialog();
    };
  }
]);

AgrosystModule.controller('CropArboFoodMastersController', ['$scope', 'ReportGrowingSystemInitData', 'addArboFoodDialogService',
  function($scope, ReportGrowingSystemInitData, addArboFoodDialogService) {
    $scope.setSpecificContext({
      cropMasters: ReportGrowingSystemInitData.highlights.arboFoodMasters,
      sectionEndpoint: $scope.endpoints.reportGrowingSystemArboFoodMasters,
      boxCropDiv: addArboFoodDialogService
    });

    $scope.onCropDialogOk = function() {
      if(!$scope.editedCropObject.crops.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        addArboFoodDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
        addArboFoodDialogService.cancelDialog();
    };
  }
]);

AgrosystModule.controller('CropArboYieldMastersController', ['$scope', 'ReportGrowingSystemInitData', 'addArboYieldDialogService',
  function($scope, ReportGrowingSystemInitData, addArboYieldDialogService) {

    $scope.yieldInfo = $scope.reportGrowingSystem.yieldInfos && $scope.reportGrowingSystem.yieldInfos.find(info => info.sector === "ARBORICULTURE");
    if (!$scope.yieldInfo) {
      $scope.yieldInfo = { sector: "ARBORICULTURE" };
    }

    $scope.setSpecificContext({
      cropMasters: ReportGrowingSystemInitData.highlights.arboYieldLosses,
      sectionEndpoint: $scope.endpoints.reportGrowingSystemArboYieldLosses,
      boxCropDiv: addArboYieldDialogService
    });

    $scope.loadCropMaster(true);

    $scope.onCropDialogOk = function() {
      if (!$scope.editedCropObject.crops.length || !$scope.editedCropObject.yieldObjective) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else if ($scope.isDephyFerme && $scope.editedCropObject.yieldObjective !== 'MORE_95' && !$scope.editedCropObject.cause1) {
        $scope.displayErrorMessage("Il faut préciser la cause !");
      } else {
        addArboYieldDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
        addArboYieldDialogService.cancelDialog();
    };
  }
]);
