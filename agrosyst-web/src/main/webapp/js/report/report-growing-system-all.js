/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('CulturesAssoleesScopeController', ['$scope',
    'addAdventiceCropsCulturesAssoleesDialogService', 'addAdventiceCulturesAssoleesDialogService',
    'addDiseaseCropsCulturesAssoleesDialogService', 'addDiseaseCulturesAssoleesDialogService',
    'addPestCropsCulturesAssoleesDialogService', 'addPestCulturesAssoleesDialogService',
    'addVerseCulturesAssoleesDialogService', 'addFoodCulturesAssoleesDialogService', 'addYieldCulturesAssoleesDialogService',
  function($scope, addAdventiceCropsDialogService, addAdventiceDialogService,
      addDiseaseCropsDialogService, addDiseaseDialogService,
      addPestCropsDialogService, addPestDialogService,
      addVerseDialogService, addFoodDialogService, addYieldDialogService) {
    $scope.addAdventiceCropsDialogService = addAdventiceCropsDialogService;
    $scope.addAdventiceDialogService = addAdventiceDialogService;
    $scope.addDiseaseCropsDialogService = addDiseaseCropsDialogService;
    $scope.addDiseaseDialogService = addDiseaseDialogService;
    $scope.addPestCropsDialogService = addPestCropsDialogService;
    $scope.addPestDialogService = addPestDialogService;
    $scope.addVerseDialogService = addVerseDialogService;
    $scope.addFoodDialogService = addFoodDialogService;
    $scope.addYieldDialogService = addYieldDialogService;
  }
]);

AgrosystModule.controller('CulturesTropicalesScopeController', ['$scope',
    'addAdventiceCropsCulturesTropicalesDialogService', 'addAdventiceCulturesTropicalesDialogService',
    'addDiseaseCropsCulturesTropicalesDialogService', 'addDiseaseCulturesTropicalesDialogService',
    'addPestCropsCulturesTropicalesDialogService', 'addPestCulturesTropicalesDialogService',
    'addFoodCulturesTropicalesDialogService', 'addYieldCulturesTropicalesDialogService',
  function($scope, addAdventiceCropsDialogService, addAdventiceDialogService,
      addDiseaseCropsDialogService, addDiseaseDialogService,
      addPestCropsDialogService, addPestDialogService,
      addFoodDialogService, addYieldDialogService) {
    $scope.sectorPrefix = "CULTURES_TROPICALES";
    $scope.addAdventiceCropsDialogService = addAdventiceCropsDialogService;
    $scope.addAdventiceDialogService = addAdventiceDialogService;
    $scope.addDiseaseCropsDialogService = addDiseaseCropsDialogService;
    $scope.addDiseaseDialogService = addDiseaseDialogService;
    $scope.addPestCropsDialogService = addPestCropsDialogService;
    $scope.addPestDialogService = addPestDialogService;
    $scope.addFoodDialogService = addFoodDialogService;
    $scope.addYieldDialogService = addYieldDialogService;
  }
]);

AgrosystModule.controller('GrandesCulturesScopeController', ['$scope',
    'addAdventiceCropsGrandesCulturesDialogService', 'addAdventiceGrandesCulturesDialogService',
    'addDiseaseCropsGrandesCulturesDialogService', 'addDiseaseGrandesCulturesDialogService',
    'addPestCropsGrandesCulturesDialogService', 'addPestGrandesCulturesDialogService',
    'addVerseGrandesCulturesDialogService', 'addFoodGrandesCulturesDialogService', 'addYieldGrandesCulturesDialogService',
  function($scope, addAdventiceCropsDialogService, addAdventiceDialogService,
      addDiseaseCropsDialogService, addDiseaseDialogService,
      addPestCropsDialogService, addPestDialogService,
      addVerseDialogService, addFoodDialogService, addYieldDialogService) {
    $scope.sectorPrefix = "GRANDES_CULTURES";
    $scope.addAdventiceCropsDialogService = addAdventiceCropsDialogService;
    $scope.addAdventiceDialogService = addAdventiceDialogService;
    $scope.addDiseaseCropsDialogService = addDiseaseCropsDialogService;
    $scope.addDiseaseDialogService = addDiseaseDialogService;
    $scope.addPestCropsDialogService = addPestCropsDialogService;
    $scope.addPestDialogService = addPestDialogService;
    $scope.addVerseDialogService = addVerseDialogService;
    $scope.addFoodDialogService = addFoodDialogService;
    $scope.addYieldDialogService = addYieldDialogService;
  }
]);

AgrosystModule.controller('HorticultureScopeController', ['$scope',
    'addAdventiceCropsHorticultureDialogService', 'addAdventiceHorticultureDialogService',
    'addDiseaseCropsHorticultureDialogService', 'addDiseaseHorticultureDialogService',
    'addPestCropsHorticultureDialogService', 'addPestHorticultureDialogService',
    'addFoodHorticultureDialogService', 'addYieldHorticultureDialogService',
  function($scope, addAdventiceCropsDialogService, addAdventiceDialogService,
      addDiseaseCropsDialogService, addDiseaseDialogService,
      addPestCropsDialogService, addPestDialogService,
      addFoodDialogService, addYieldDialogService) {
    $scope.sectorPrefix = "HORTICULTURE";
    $scope.addAdventiceCropsDialogService = addAdventiceCropsDialogService;
    $scope.addAdventiceDialogService = addAdventiceDialogService;
    $scope.addDiseaseCropsDialogService = addDiseaseCropsDialogService;
    $scope.addDiseaseDialogService = addDiseaseDialogService;
    $scope.addPestCropsDialogService = addPestCropsDialogService;
    $scope.addPestDialogService = addPestDialogService;
    $scope.addFoodDialogService = addFoodDialogService;
    $scope.addYieldDialogService = addYieldDialogService;
  }
]);

AgrosystModule.controller('MaraichageScopeController', ['$scope',
    'addAdventiceCropsMaraichageDialogService', 'addAdventiceMaraichageDialogService',
    'addDiseaseCropsMaraichageDialogService', 'addDiseaseMaraichageDialogService',
    'addPestCropsMaraichageDialogService', 'addPestMaraichageDialogService',
    'addFoodMaraichageDialogService', 'addYieldMaraichageDialogService',
  function($scope, addAdventiceCropsDialogService, addAdventiceDialogService,
      addDiseaseCropsDialogService, addDiseaseDialogService,
      addPestCropsDialogService, addPestDialogService,
      addFoodDialogService, addYieldDialogService) {
    $scope.sectorPrefix = "MARAICHAGE";
    $scope.addAdventiceCropsDialogService = addAdventiceCropsDialogService;
    $scope.addAdventiceDialogService = addAdventiceDialogService;
    $scope.addDiseaseCropsDialogService = addDiseaseCropsDialogService;
    $scope.addDiseaseDialogService = addDiseaseDialogService;
    $scope.addPestCropsDialogService = addPestCropsDialogService;
    $scope.addPestDialogService = addPestDialogService;
    $scope.addFoodDialogService = addFoodDialogService;
    $scope.addYieldDialogService = addYieldDialogService;
  }
]);

AgrosystModule.controller('PolycultureElevageScopeController', ['$scope',
    'addAdventiceCropsPolycultureElevageDialogService', 'addAdventicePolycultureElevageDialogService',
    'addDiseaseCropsPolycultureElevageDialogService', 'addDiseasePolycultureElevageDialogService',
    'addPestCropsPolycultureElevageDialogService', 'addPestPolycultureElevageDialogService',
    'addVersePolycultureElevageDialogService', 'addFoodPolycultureElevageDialogService', 'addYieldPolycultureElevageDialogService',
  function($scope, addAdventiceCropsDialogService, addAdventiceDialogService,
      addDiseaseCropsDialogService, addDiseaseDialogService,
      addPestCropsDialogService, addPestDialogService,
      addVerseDialogService, addFoodDialogService, addYieldDialogService) {
    $scope.sectorPrefix = "POLYCULTURE_ELEVAGE";
    $scope.addAdventiceCropsDialogService = addAdventiceCropsDialogService;
    $scope.addAdventiceDialogService = addAdventiceDialogService;
    $scope.addDiseaseCropsDialogService = addDiseaseCropsDialogService;
    $scope.addDiseaseDialogService = addDiseaseDialogService;
    $scope.addPestCropsDialogService = addPestCropsDialogService;
    $scope.addPestDialogService = addPestDialogService;
    $scope.addVerseDialogService = addVerseDialogService;
    $scope.addFoodDialogService = addFoodDialogService;
    $scope.addYieldDialogService = addYieldDialogService;
  }
]);

AgrosystModule.controller('CropAdventiceMastersDialogController', ['$scope',
  function($scope) {
    $scope.onCropDialogOk = function() {
      if (!$scope.editedCropObject.crops.length ||
        !$scope.editedCropObject.pestMasters.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes");
      } else {
        $scope.addAdventiceCropsDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
      $scope.addAdventiceCropsDialogService.cancelDialog();
    };

    $scope.onPestDialogOk = function() {
      if (!$scope.editedPestMaster.agressor && !$scope.editedPestMaster.codeGroupeCibleMaa) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.pressureScale) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : échelle de pression");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.masterScale) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : échelle de maîtrise");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.qualifier) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : qualification du niveau de maîtrise");
      } else {
        $scope.addAdventiceDialogService.closeDialog();
      }
    };

    $scope.onPestDialogCancel = function() {
      $scope.addAdventiceDialogService.cancelDialog();
    };

    $scope.onOpenGroupeCible = function(isOpen) {
      $scope._onOpenUiSelect("#groupeCibleField", isOpen)
    }

    $scope.onOpenEditedPestBioAgressor = function(isOpen) {
      $scope._onOpenUiSelect("#editedPestBioAgressor", isOpen)
    }

    $scope._onOpenUiSelect = function(id, isOpen) {
      if (isOpen) {
        let input = Array.from(document.getElementsByClassName("ui-dialog")) // récupère tous les dialog, quelque soit le type
            .filter(d => d.style.display != "none")                          // ne garde que le dialog actif
            .map(d => d.querySelector(id))                                   // prend le ui-select ouvert
            .filter(us => us != null)[0]                                     // non nul
            .getElementsByClassName("select2-input")[0]                      // recupere l'input
        input.focus()
        input.select()
      }
    }
  }
]);

AgrosystModule.controller('CropDiseaseMastersDialogController', ['$scope',
  function($scope) {
    $scope.onCropDialogOk = function() {
      if (!$scope.editedCropObject.crops.length || !$scope.editedCropObject.pestMasters.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        $scope.addDiseaseCropsDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
      $scope.addDiseaseCropsDialogService.cancelDialog();
    };

    $scope.onPestDialogOk = function() {
      if (!$scope.editedPestMaster.agressor && !$scope.editedPestMaster.codeGroupeCibleMaa) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.pressureScale) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : échelle de pression");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.masterScale) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : échelle de maîtrise");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.qualifier) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : qualification du niveau de maîtrise");
      } else {
        $scope.addDiseaseDialogService.closeDialog();
      }
    };

    $scope.onPestDialogCancel = function() {
      $scope.addDiseaseDialogService.cancelDialog();
    };

    $scope.onOpenGroupeCible = function(isOpen) {
      $scope._onOpenUiSelect("#groupeCibleField", isOpen)
    }

    $scope.onOpenEditedPestBioAgressor = function(isOpen) {
      $scope._onOpenUiSelect("#editedPestBioAgressor", isOpen)
    }

    $scope._onOpenUiSelect = function(id, isOpen) {
      if (isOpen) {
        let input = Array.from(document.getElementsByClassName("ui-dialog")) // récupère tous les dialog, quelque soit le type
            .filter(d => d.style.display != "none")                          // ne garde que le dialog actif
            .map(d => d.querySelector(id))                                   // prend le ui-select ouvert
            .filter(us => us != null)[0]                                     // non nul
            .getElementsByClassName("select2-input")[0]                      // recupere l'input
        input.focus()
        input.select()
      }
    }
  }
]);

AgrosystModule.controller('CropPestMastersDialogController', ['$scope',
  function($scope) {
    $scope.onCropDialogOk = function() {
      if (!$scope.editedCropObject.crops.length || !$scope.editedCropObject.pestMasters.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        $scope.addPestCropsDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
      $scope.addPestCropsDialogService.cancelDialog();
    };

    $scope.onPestDialogOk = function() {
      if(!$scope.editedPestMaster.agressor && !$scope.editedPestMaster.codeGroupeCibleMaa) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.pressureScale && !$scope.editedPestMaster.pressureScaleInt) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : échelle de pression");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.masterScale && !$scope.editedPestMaster.masterScaleInt) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : échelle de maîtrise");
      } else if (!$scope.isDephyExpe && !$scope.editedPestMaster.qualifier) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes : qualification du niveau de maîtrise");
      } else {
        $scope.addPestDialogService.closeDialog();
      }
    };

    $scope.onPestDialogCancel = function() {
      $scope.addPestDialogService.cancelDialog();
    };

    $scope.onOpenGroupeCible = function(isOpen) {
      $scope._onOpenUiSelect("#groupeCibleField", isOpen)
    }

    $scope.onOpenEditedPestBioAgressor = function(isOpen) {
      $scope._onOpenUiSelect("#editedPestBioAgressor", isOpen)
    }

    $scope._onOpenUiSelect = function(id, isOpen) {
      if (isOpen) {
        let input = Array.from(document.getElementsByClassName("ui-dialog")) // récupère tous les dialog, quelque soit le type
            .filter(d => d.style.display != "none")                          // ne garde que le dialog actif
            .map(d => d.querySelector(id))                                   // prend le ui-select ouvert
            .filter(us => us != null)[0]                                     // non nul
            .getElementsByClassName("select2-input")[0]                      // recupere l'input
        input.focus()
        input.select()
      }
    }
  }
]);

AgrosystModule.controller('CropVerseMastersDialogController', ['$scope',
  function($scope) {
    $scope.onCropDialogOk = function() {
      if (!$scope.editedCropObject.crops.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        $scope.addVerseDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
      $scope.addVerseDialogService.cancelDialog();
    };
  }
]);

AgrosystModule.controller('CropFoodMastersDialogController', ['$scope',
  function($scope) {
    $scope.onCropDialogOk = function() {
      if(!$scope.editedCropObject.crops.length) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else {
        $scope.addFoodDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
      $scope.addFoodDialogService.cancelDialog();
    };
  }
]);

AgrosystModule.controller('CropYieldMastersDialogController', ['$scope',
  function($scope) {
    $scope.onCropDialogOk = function() {
      if(!$scope.editedCropObject.crops.length || (!$scope.editedCropObject.yieldObjective && !$scope.editedCropObject.yieldObjectiveInt)) {
        $scope.displayErrorMessage("Certaines informations requises sont manquantes ou invalides !");
      } else if ($scope.isDephyFerme && $scope.editedCropObject.yieldObjective !== 'MORE_95' && !$scope.editedCropObject.cause1) {
        $scope.displayErrorMessage("Il faut préciser la cause !");
      } else {
        $scope.addYieldDialogService.closeDialog();
      }
    };

    $scope.onCropDialogCancel = function() {
      $scope.addYieldDialogService.cancelDialog();
    };
  }
]);

/**
 * Common crop div controller.
 */
AgrosystModule.controller('CropPestMasterEditController', ['$scope', '$http', '$q',
  function($scope, $http, $q) {

    var _getDiscriminator = function(value) {
      var disc = 0, i, chr;
      if (value.length === 0) return disc;
      for (i = 0; i < value.length; i++) {
        chr = value.charCodeAt(i) - 64;
        disc += chr;
      }
      return disc;
    };

    var _getComputeCropColor = function(crop) {
      var cropCode = crop.code;
      var disc = _getDiscriminator(cropCode);

      var hue = Math.abs(parseInt((360 / (26 * 26)) * (disc * 2)));
      var backgroundColor = "hsl(" + hue + ", 50%, 80%)";
      return backgroundColor;
    };

    $scope.removeSpecies = function(cropPest, cpe, cps) {
      var index = cropPest.species.indexOf(cps);
      if (index !== -1) {
        cropPest.species.splice(index, 1);
      }
      var cpeSpecies = cpe.croppingPlanSpecies;
      var found = false;
      for(var i=0; i < cpeSpecies.length; i++) {
        var cpeS = cpeSpecies[i];
        var speIndex = cropPest.species.indexOf(cpeS);
        if (speIndex !== -1) {
          found = true;
        }
      }
      if (!found) {
        var cropIndex = cropPest.crops.indexOf(cpe);
        if (cropIndex !== -1) {
          cropPest.crops.splice(cropIndex, 1);
        }
      }
      return index;
    };

    $scope.removeCrop = function(cropPest, cpe) {
      var index = cropPest.crops.indexOf(cpe);
      if (index !== -1) {
        cropPest.crops.splice(index, 1);
        // remove crop species
        var cropSpecies = cpe.croppingPlanSpecies;
        for(var i=0; i<cropSpecies.length; i++) {
          var cps = cropSpecies[i];
          $scope.removeSpecies(cropPest, cpe, cps);
        }
      }
      return index;
    };

    $scope.addCrop = function(cropPest, cpe, addAllCrop) {
      var index = cropPest.crops.indexOf(cpe);
      if (index === -1) {
        cropPest.crops.push(cpe);
        if (addAllCrop) {
          $scope.addAllCropSpecies(cropPest, cpe);
        }
      } else if (addAllCrop) {
        $scope.addAllCropSpecies(cropPest, cpe);
      }
    };

    $scope.addOrRemoveCrop = function(cropPest, cpe) {
      if ($scope.removeCrop(cropPest, cpe) === -1) {
        $scope.addCrop(cropPest, cpe, true);
      }
    };

    $scope.addSpecies = function(cropPest, cpe, cps) {
      $scope.addCrop(cropPest, cpe);
      if (cropPest.species.indexOf(cps) === -1) {
        cropPest.species.push(cps);
      }
    };

    $scope.addAllCropSpecies = function(cropPest, cpe) {
      var cropSpecies = cpe.croppingPlanSpecies;
      for (var i=0; i<cropSpecies.length; i++) {
        $scope.addSpecies(cropPest, cpe, cropSpecies[i]);
      }
    };

    $scope.addOrRemoveSpecies = function(cropPest, cpe, cps) {
      if ($scope.removeSpecies(cropPest, cpe, cps) === -1) {
        $scope.addSpecies(cropPest, cpe, cps);
      }
    };

    $scope.loadEditCropContext = function() {
      if ($scope.growingSystemId) {
        displayPageLoading();
        return $http.get($scope.endpoints.growingSystemCrops + "?growingSystemId=" + encodeURIComponent($scope.growingSystemId),{cache: true})
          .then(function(data) {
            $scope.crops = data.data;
            for (var i=0; i < $scope.crops.length; i++) {
              var crop = $scope.crops[i];
              var cropColor = _getComputeCropColor(crop);
              crop.backgroundColor = cropColor;
            }
            hidePageLoading();
          })
          .catch(function(response) {
            hidePageLoading();
            console.error("Échec de récupération des cultures liées au système de culture avec comme id: '" + $scope.reportGrowingSystemId +"'", response);
            addPermanentError("Échec de récupération des cultures liées au système de culture avec comme id: '" + $scope.reportGrowingSystemId +"'", response.status);
          });
      }
    };
}]);

AgrosystModule.controller('CropAllPestMastersController', ['$scope', '$http', '$timeout', '$q',
  function($scope, $http, $timeout, $q) {

    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

    // table visibility
    $scope.showArboDiseaseCropPestMasters = false;
    $scope.showArboPestCropPestMasters = false;
    $scope.showArboAdventiceCropPestMasters = false;

    $scope.showDiseaseCropPestMasters = false;
    $scope.showPestCropPestMasters = false;
    $scope.showAdventiceCropPestMasters = false;

    $scope.showCropPestMasters = false;

    // view port index
    $scope.viewPortIndex = 0;

    // callback
    var updateViewPortArray = function() {
      // transform flat
      $scope.viewPortArray = [];
      if ($scope.cropPestMasters) {
        angular.forEach($scope.cropPestMasters, function(cropPestMaster) {
          if (cropPestMaster.sector === $scope.sectorPrefix) {
            angular.forEach(cropPestMaster.pestMasters, function(pestMaster) {
              pestMaster.$$crop = cropPestMaster; // hack ? non :)
              pestMaster.$$cropspan = 1;
              $scope.viewPortArray.push(pestMaster);
            });
          }
        });
      }

      // move port
      $scope.maxViewPort = $scope.viewPortArray.length;
      $scope.viewPortArray = $scope.viewPortArray.slice($scope.viewPortIndex, $scope.viewPortIndex + 3);
      // fixe colspan
      if ($scope.viewPortArray.length > 2 && $scope.viewPortArray[1].$$crop === $scope.viewPortArray[2].$$crop) {
        $scope.viewPortArray[1].$$cropspan += $scope.viewPortArray[2].$$cropspan;
        $scope.viewPortArray[2].$$cropspan = 0;
      }
      if ($scope.viewPortArray.length > 1 && $scope.viewPortArray[0].$$crop === $scope.viewPortArray[1].$$crop) {
        $scope.viewPortArray[0].$$cropspan += $scope.viewPortArray[1].$$cropspan;
        $scope.viewPortArray[1].$$cropspan = 0;
      }
    };

    $scope.getCropPestMasters = function() {
      let result = {};
      result[$scope.sectorPrefix] = $scope.cropPestMasters;
      return result;
    }

    // called by impl
    $scope.setSpecificContext = function(specific) {
      $scope.specific = specific;
      $scope.cropPestMasters = specific.cropPestMasters;
      if ($scope.cropPestMasters) {
        updateViewPortArray();
      }
      delete specific.cropPestMasters;
    };

    // callback
    var _loadCropPestMasters = function() {
      displayPageLoading();
      let uri = $scope.specific.sectionEndpoint + "?reportGrowingSystemId=" + encodeURIComponent($scope.reportGrowingSystemId);
      if ($scope.sectorPrefix) {
        uri += "&sector=" + encodeURIComponent($scope.sectorPrefix);
      }
      return $http.get(uri)
        .then(function(response) {
          $scope.cropPestMasters = response.data;

          if ($scope.cropPestMasters && $scope.cropPestMasters.length > 0 && !$scope.cropPestMasters[0].topiaId) {
            addPermanentWarning("Ces données ont été générées automatiquement, vous devez completer les données obligatoires !");
          }
          hidePageLoading();
        })
        .catch(function(response) {
          hidePageLoading();
          console.error("Échec de récupération des données de " + $scope.specific.sectionEndpoint , response);
          addPermanentError("Échec de récupération des données de " + $scope.specific.sectionEndpoint +
                         " erreur:" + response.message, response.status);
        });
    };

    // user action : show hide
    $scope.loadCropPestMaster = function(target) {
      if (target && target === 'arboDisease') {
        $scope.showArboDiseaseCropPestMasters = !$scope.showArboDiseaseCropPestMasters;
      } else if (target && target === 'arboPest') {
        $scope.showArboPestCropPestMasters = !$scope.showArboPestCropPestMasters;
      } else if (target && target === 'arboAdventice') {
        $scope.showArboAdventiceCropPestMasters = !$scope.showArboAdventiceCropPestMasters;
      } else if (target && target === 'disease') {
        $scope.showDiseaseCropPestMasters = !$scope.showDiseaseCropPestMasters;
      } else if (target && target === 'pest') {
        $scope.showPestCropPestMasters = !$scope.showPestCropPestMasters;
      } else if (target && target === 'adventice') {
        $scope.showAdventiceCropPestMasters = !$scope.showAdventiceCropPestMasters;
      } else {
        $scope.showCropPestMasters = !$scope.showCropPestMasters;
      }
      if (!$scope.cropPestMasters) {
        if ($scope.reportGrowingSystemId) {
          _loadCropPestMasters()
            .then(updateViewPortArray);
        } else {
          $scope.cropPestMasters = [];
          updateViewPortArray();
        }
      }
    };

    var _showEditCropPestMasterBox = function() {
      // if validation error then use same backup as previous
      $scope.boxShowSpecies = false;

      return $scope.specific.boxCropDiv.openDialog().then(function() {
          if ($scope.cropPestMasters === undefined) {
            $scope.cropPestMasters = [];
          }
          var previousIndex = $scope.cropPestMasters.indexOf($scope.editedCropObject.$$original);
          if (previousIndex === -1) {
            $scope.cropPestMasters.push($scope.editedCropObject);
          } else {
            $scope.cropPestMasters[previousIndex] = $scope.editedCropObject;
          }
          delete $scope.editedCropObject.$$original;

          $scope.reportGrowingSystemEditForm.$setDirty();

          updateViewPortArray();
      }, function() {});
    };

    // specific second box
    var _showAddPestEditLightBox = function() {
      return $scope.specific.boxDiv.openDialog().then(function() {

          var previousIdx = $scope.editedCropObject.pestMasters.indexOf($scope.editedPestMaster.$$original);
          if (previousIdx === -1) {
            $scope.editedCropObject.pestMasters.push($scope.editedPestMaster);
          } else {
            $scope.editedCropObject.pestMasters[previousIdx] = $scope.editedPestMaster;
          }
          delete $scope.editedPestMaster.$$original;

          $scope.reportGrowingSystemEditForm.$setDirty();
          updateViewPortArray();
        }, function() {});
    };

    // user action
    $scope.addCropPestMaster = function() {
      $scope.editedCropObject = {
          sector: $scope.sectorPrefix,
          crops: [],
          species: [],
          pestMasters: []
      };
      $scope.loadEditCropContext()
        .then(_loadBioAgressor)
        .then(_bindLoadedBioAgressor)
        .then(_showEditCropPestMasterBox);
    };

    // user action
    $scope.editPestMasterFromCropPestMaster = function(cropPestMaster, pestMaster) {
      var cropPestMasterCopy = angular.copy(cropPestMaster);
      cropPestMasterCopy.$$original = cropPestMaster;
      cropPestMasterCopy.pestMasters = [];
      var pestMasterEdit;
      angular.forEach(cropPestMaster.pestMasters, function(pestMasterOrig) {
        var pestMasterCopy = angular.copy(pestMasterOrig);
        if (pestMaster === pestMasterOrig) {
          pestMasterEdit = pestMasterCopy;
        }
        cropPestMasterCopy.pestMasters.push(pestMasterCopy);
      });
      $scope.editPestMaster(cropPestMasterCopy, pestMasterEdit);
    };

    var _displayPestBoxAndAddPestEditLightBox = function() {
      _showEditCropPestMasterBox();
      _showAddPestEditLightBox();
    };

    var _bindLoadedCrops = function() {
      if ($scope.crops) {
        for (var i = 0; i < $scope.crops.length; i++) {
          var loadedCrop = $scope.crops[i];
          for (var j = 0; j < $scope.editedCropObject.crops.length; j++) {
            var crop = $scope.editedCropObject.crops[j];
            if (crop.topiaId === loadedCrop.topiaId) {
              $scope.editedCropObject.crops[j] = loadedCrop;
              for (var k = 0; k < $scope.editedCropObject.species.length; k++) {
                var species = $scope.editedCropObject.species[k];
                for (var l =0 ; l < loadedCrop.croppingPlanSpecies.length; l++) {
                  var loadedSpecies = loadedCrop.croppingPlanSpecies[l];
                  if (loadedSpecies.topiaId === species.topiaId) {
                    $scope.editedCropObject.species[k] = loadedSpecies;
                  }
                }
              }
              break;
            }
          }
        }
      }
    };

    var _bindLoadedBioAgressor = function() {
      if ($scope.editedPestMaster && $scope.editedPestMaster.agressor) {
          var agressorId = $scope.editedPestMaster.agressor.topiaId;
          angular.forEach($scope.bioAgressors, function(bioAgressor) {
            if (bioAgressor.topiaId == $scope.editedPestMaster.agressor.topiaId) {
              $scope.editedPestMaster.agressor = bioAgressor;
            }
          });
      }
    };

    // Update bio agressors when bio agressor type change
    var _loadBioAgressor = function() {
      displayPageLoading();
      var url = $scope.endpoints.bioAgressorsJson + "?bioAgressorTypes=" + encodeURIComponent(JSON.stringify($scope.specific.bioAgressorTypes));
      var sectors = $scope.specific.getSectors();
      angular.forEach(sectors, function(sector) {
        url += "&sectors=" + sector;
      });
      return $http.get(url, {cache: true})
        .then(function(response) {
          $scope.bioAgressors = response.data;
          hidePageLoading();
        }).
      catch(function(response) {
        hidePageLoading();
        console.error("Échec de chargement des bio agresseurs", response);
        addPermanentError("Échec de chargement des bio agresseurs", response.status);
      });
    };

    // user action
    $scope.editPestMaster = function(cropPestMaster, pestMaster) {
      $scope.editedCropObject = cropPestMaster;
      if (pestMaster) {
        $scope.editedPestMaster = angular.copy(pestMaster);
        $scope.editedPestMaster.$$original = pestMaster;
      } else {
        $scope.editedPestMaster = {};
      }

      $scope.loadEditCropContext()
        .then(_bindLoadedCrops)
        .then(_loadBioAgressor)
        .then(_bindLoadedBioAgressor)
        .then(_displayPestBoxAndAddPestEditLightBox);
    };

    // user action
    $scope.deleteCropPestMaster = function(cropPestMaster) {
      var idx = $scope.cropPestMasters.indexOf(cropPestMaster);
      $scope.cropPestMasters.splice(idx, 1);
      updateViewPortArray();
    };

    // user action
    $scope.deletePestMaster = function(cropPestMaster, pestMaster) {
      return $scope.displayQuestionMessage("Suppression", "Êtes-vous sûr de vouloir supprimer cet élément ?")
       .then(function() {
           var pestMasters = cropPestMaster.pestMasters;
           var pestMasterIdx = pestMasters.indexOf(pestMaster);
           if (pestMasterIdx !== -1) {
             pestMasters.splice(pestMasterIdx, 1);
           }

           updateViewPortArray();
         }, function() {

         });
    };

    // user action
    $scope.moveViewPort = function(inc) {
      $scope.viewPortIndex += inc;
      if ($scope.viewPortIndex + 3 > $scope.maxViewPort) $scope.viewPortIndex = $scope.maxViewPort - 3;
      if ($scope.viewPortIndex < 0) $scope.viewPortIndex = 0;
      updateViewPortArray();
    };
  }
]);

AgrosystModule.controller('CropAdventiceMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {

    $scope.setSpecificContext({
      cropPestMasters: ReportGrowingSystemInitData.highlights.cropAdventiceMasters,
      bioAgressorTypes: ReportGrowingSystemInitData.treatmentTargetCategoriesByParent['ADVENTICE'].bioAgressorTypes,
      getSectors: () => $scope.reportGrowingSystem.sectors.filter(sector => sector !== 'ARBORICULTURE' && sector !== 'VITICULTURE'),
      sectionEndpoint: $scope.endpoints.reportGrowingSystemCropAdventiceMasters,
      boxCropDiv: $scope.addAdventiceCropsDialogService,
      boxDiv: $scope.addAdventiceDialogService
    });
  }
]);

AgrosystModule.controller('CropDiseaseMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {

    $scope.setSpecificContext({
      cropPestMasters: ReportGrowingSystemInitData.highlights.cropDiseaseMasters,
      bioAgressorTypes: ReportGrowingSystemInitData.treatmentTargetCategoriesByParent['MALADIE'].bioAgressorTypes,
      getSectors: () => $scope.reportGrowingSystem.sectors.filter(sector => sector !== 'ARBORICULTURE' && sector !== 'VITICULTURE'),
      sectionEndpoint: $scope.endpoints.reportGrowingSystemCropDiseaseMasters,
      boxCropDiv: $scope.addDiseaseCropsDialogService,
      boxDiv: $scope.addDiseaseDialogService
    });
  }
]);

AgrosystModule.controller('CropPestMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {

    $scope.setSpecificContext({
      cropPestMasters: ReportGrowingSystemInitData.highlights.cropPestMasters,
      bioAgressorTypes: ReportGrowingSystemInitData.treatmentTargetCategoriesByParent['RAVAGEUR'].bioAgressorTypes,
      getSectors: () => $scope.reportGrowingSystem.sectors.filter(sector => sector !== 'ARBORICULTURE' && sector !== 'VITICULTURE'),
      sectionEndpoint: $scope.endpoints.reportGrowingSystemCropPestMasters,
      boxCropDiv: $scope.addPestCropsDialogService,
      boxDiv: $scope.addPestDialogService
    });
  }
]);

AgrosystModule.controller('CropAllMastersController', ['$scope', '$http', '$q',
  function($scope, $http, $q) {
    // table visibility
    $scope.showCropMasters = false;
    // view port index
    $scope.viewPortIndex = 0;

    // callback
    var updateViewPortArray = function() {
      // transform flat
      $scope.viewPortArray = $scope.cropMasters.slice(); // copy

      // move port
      $scope.maxViewPort = $scope.viewPortArray.length;
      $scope.viewPortArray = $scope.viewPortArray.slice($scope.viewPortIndex, $scope.viewPortIndex + 3);
    };

    // called by impl
    $scope.setSpecificContext = function(specific) {
      $scope.specific = specific;
      $scope.cropMasters = specific.cropMasters;
      if ($scope.cropMasters) {
        updateViewPortArray();
      }
      delete specific.cropMasters;
    };

    // callback
    var _loadCropMasters = function() {
      displayPageLoading();
      let uri = $scope.specific.sectionEndpoint + "?reportGrowingSystemId=" + encodeURIComponent($scope.reportGrowingSystemId);
      if ($scope.sectorPrefix) {
        uri += "&sector=" + encodeURIComponent($scope.sectorPrefix);
      }
      return $http.get(uri)
        .then(function(response) {
          $scope.cropMasters = response.data;
          if ($scope.cropMasters && $scope.cropMasters.length > 0 && !$scope.cropMasters[0].topiaId) {
            addPermanentWarning("Ces données ont été générées automatiquement, vous devez completer les données obligatoires !");
          }
          hidePageLoading();
        })
        .catch(function(response) {
          hidePageLoading();
          console.error("Échec de récupération des données de " + $scope.specific.sectionEndpoint , response);
          addPermanentError("Échec de récupération des données du tableau " + $scope.specific.sectionEndpoint + " erreur:" + response.message, response.status);
        });
    };

    var _showEditCropMasterBox = function() {
      // if validation error then use same backup as previous
      $scope.boxShowSpecies = false;

      return $scope.specific.boxCropDiv.openDialog()
        .then(function() {

          var previousIndex = $scope.cropMasters.indexOf($scope.editedCropObject.$$original);
          if (previousIndex === -1) {
            $scope.cropMasters.push($scope.editedCropObject);
          } else {
            $scope.cropMasters[previousIndex] = $scope.editedCropObject;
          }
          delete $scope.editedCropObject.$$original;

          $scope.reportGrowingSystemEditForm.$setDirty();

          updateViewPortArray();
        }, function() {

        });
    };

    // user action : show hide
    $scope.loadCropMaster = function(init) {
      if (!init){
        $scope.showCropMasters = !$scope.showCropMasters;
      }
      if (!$scope.cropMasters) {
        if ($scope.reportGrowingSystemId) {
          _loadCropMasters()
            .then(updateViewPortArray);
        } else {
          $scope.cropMasters = [];
          updateViewPortArray();
        }
      }
    };

    // user action
    $scope.addCropMaster = function() {
      $scope.editedCropObject = {
          sector: $scope.sectorPrefix,
          crops: [],
          species: []
      };
      $scope.loadEditCropContext()
        .then(_showEditCropMasterBox);
    };

    var _bindLoadedCrops = function() {
      if ($scope.crops) {
        for (var i = 0; i < $scope.crops.length; i++) {
          var loadedCrop = $scope.crops[i];
          for (var j = 0; j < $scope.editedCropObject.crops.length; j++) {
            var crop = $scope.editedCropObject.crops[j];
            if (crop.topiaId === loadedCrop.topiaId) {
              $scope.editedCropObject.crops[j] = loadedCrop;
              for (var k = 0; k < $scope.editedCropObject.species.length; k++) {
                var species = $scope.editedCropObject.species[k];
                for (var l =0 ; l < loadedCrop.croppingPlanSpecies.length; l++) {
                  var loadedSpecies = loadedCrop.croppingPlanSpecies[l];
                  if (loadedSpecies.topiaId === species.topiaId) {
                    $scope.editedCropObject.species[k] = loadedSpecies;
                  }
                }
              }
              break;
            }
          }
        }
      }
    };

    // user action
    $scope.editCropMaster = function(cropMaster) {
      $scope.editedCropObject = angular.copy(cropMaster);
      $scope.editedCropObject.$$original = cropMaster;

      $scope.loadEditCropContext()
        .then(_bindLoadedCrops)
        .then(_showEditCropMasterBox);
    };

    // user action
    $scope.deleteCropMaster = function(cropMaster) {
      var idx = $scope.cropMasters.indexOf(cropMaster);
      $scope.cropMasters.splice(idx, 1);
      updateViewPortArray();
    };

    // user action
    $scope.moveViewPort = function(inc) {
      $scope.viewPortIndex += inc;
      if ($scope.viewPortIndex + 3 > $scope.maxViewPort) $scope.viewPortIndex = $scope.maxViewPort - 3;
      if ($scope.viewPortIndex < 0) $scope.viewPortIndex = 0;
      updateViewPortArray();
    };

    $scope.getCropMasters = function() {
      let result = {};
      result[$scope.sectorPrefix] = $scope.cropMasters;
      return result;
    }
  }
]);

AgrosystModule.controller('CropVerseMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {
    $scope.setSpecificContext({
      cropMasters: ReportGrowingSystemInitData.highlights.verseMasters,
      sectionEndpoint: $scope.endpoints.reportGrowingSystemVerseMasters,
      boxCropDiv: $scope.addVerseDialogService
    });
  }
]);

AgrosystModule.controller('CropFoodMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {
    $scope.setSpecificContext({
      cropMasters: ReportGrowingSystemInitData.highlights.foodMasters,
      sectionEndpoint: $scope.endpoints.reportGrowingSystemFoodMasters,
      boxCropDiv: $scope.addFoodDialogService
    });
  }
]);

AgrosystModule.controller('CropYieldMastersController', ['$scope', 'ReportGrowingSystemInitData',
  function($scope, ReportGrowingSystemInitData) {
    $scope.yieldInfo = $scope.reportGrowingSystem.yieldInfos && $scope.reportGrowingSystem.yieldInfos.find(info => info.sector === $scope.sectorPrefix);
    if (!$scope.yieldInfo) {
      $scope.yieldInfo = { sector: $scope.sectorPrefix };
    }

    $scope.setSpecificContext({
      cropMasters: ReportGrowingSystemInitData.highlights.yieldLosses,
      sectionEndpoint: $scope.endpoints.reportGrowingSystemYieldLosses,
      boxCropDiv: $scope.addYieldDialogService,
    });

    $scope.loadCropMaster(true);
  }
]);
