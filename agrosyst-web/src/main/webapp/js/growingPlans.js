/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


function growingPlansExport(exportForm) {
    var selectionCount = JSON.parse(exportForm.children("input[name='growingPlanIds']").val()).length;
    if (selectionCount >= 1) {
      exportForm.attr("action", ENDPOINTS.growingPlansExport);
      exportForm.submit();
    }
}


AgrosystModule.controller('GrowingPlanController', ['$scope', '$window', '$timeout', '$http', 'growingPlan',
  function ($scope, $window, $timeout, $http, growingPlan) {
    $scope.growingPlan = growingPlan;
    $scope.displayFieldsErrorsAlert = true;
    $scope.latestDomainSearchTerm = '';
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);


    $scope.refreshDomains = function(pattern) {
      // Les réponses aux requêtes XHR n'arrivent pas forcément dans l'ordre. On garde en mémoire la dernière valeur demandée éviter d'afficher un ancien résultat
      $scope.latestDomainSearchTerm = pattern;
      return $http.get($scope.endpoints.domainsSearchJson + "?term=" + encodeURIComponent(pattern))
          .then(function(response) {
            // Si la réponse correspond à la dernière reuqête envoyée, on met à jour la liste
            if (pattern == $scope.latestDomainSearchTerm) {
              $scope.domains = response.data.elements;
            }
          })
          .catch(function(response) {
              var message = "Échec de récupération des domaines";
              console.error(message, response);
              addPermanentError(message, response.status);
          });
    };

    $scope.selectDomain = function(domain) {
      if (domain && domain.topiaId !== $scope.domainId) {
        $scope.domainId = domain.topiaId;
        $scope.growingPlan.domain = domain;
      }
    };
    if ($scope.growingPlan) {
      $scope.selectDomain($scope.growingPlan.domain); // init
    }

  }]
);

function editGrowingPlanResponsibles(growingPlanCode) {
  editEntityRoles("GROWING_PLAN_RESPONSIBLE", growingPlanCode, "Responsables du dispositif");
}

/**
 * Unactivate growing plans after user confirm.
 */
function growingPlansUnactivate(confirmDialog, unactivateForm, activate) {
    var selectionCount = JSON.parse(unactivateForm.children("input[name='growingPlanIds']").val()).length;
    if (selectionCount > 0) {
        confirmDialog.dialog({
            resizable: false,
            width: 350,
            modal: true,
            buttons: {
                action: {
                  click:function() {
                    $(this).dialog("close");
                    unactivateForm.attr("action", activate ? ENDPOINTS.growingPlansUnactivate+"?activate=true" : ENDPOINTS.growingPlansUnactivate);
                    unactivateForm.submit();
                  },
                  text: activate ? "Activer" : "Désactiver",
                  'class': 'btn-primary'
                },
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}
 
