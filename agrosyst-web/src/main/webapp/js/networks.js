/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
AgrosystModule.controller('NetworkMainController', ['$scope', '$http', '$window', '$timeout', 'managers', 'network', 'growingSystemIndicators',
  function($scope, $http, $window, $timeout, managers, network, growingSystemIndicators) {
    $scope.listNetworkManagers = managers;
    $scope.growingSystemIndicators = growingSystemIndicators;
    $scope.currentNetworkManagers=[];
    $scope.showInactivesManagers=false;
    //$scope.origineCurrentEditedManager;
    $scope.network = network;
    $scope.parents=[];
    $scope.nbActiveManagers=0;
    $scope.nbUnactiveManagers=0;
    $scope.alreadyExistingNetwork = false;
    $scope.sectors = $scope.i18n.Sector;
    $scope.displayFieldsErrorsAlert = true;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

    // options des dates pickers
    $scope.dateOptions = {
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy"
    };

    $scope.showAddManagerButton = function(){
      var result = false;
      if ($scope.nbUnactiveManagers > 0){
        result = true;
      }
      return result;
    };

    angular.forEach($scope.network.parents, function(parent){
      var parentObject = {topiaId: parent.topiaId, name: parent.name};
      $scope.parents.push(parentObject);
    });

    $scope.getParentIds = function () {
      var result = [];
      angular.forEach($scope.parents, function (parent) {
        result.push(parent.topiaId);
      });
      return result;
    };

    $scope.removeParent = function (parent) {
      var confirm = $window.confirm("Êtes-vous sûr de vouloir supprimer le parent " + parent.name + " du réseau ?");
      if (confirm) {
        var index = $scope.parents.indexOf(parent);
        if (index >=0 ) {
          $scope.parents.splice(index, 1);
          $scope.networksEditForm.$setDirty();
        }
      }
    };

    angular.forEach($scope.listNetworkManagers, function (networkManager) {

      if (networkManager.fromDate) {
        networkManager.fromDate = new Date(Date.parse(networkManager.fromDate));
      }

      if (networkManager.toDate) {
        networkManager.toDate = new Date(Date.parse(networkManager.toDate));
      }

      $scope.currentNetworkManagers.push(networkManager);
      if(networkManager.active) {
        $scope.nbActiveManagers++;
      } else {
        $scope.nbUnactiveManagers++;
      }
    });

    $scope.fromDate = {
        changeMonth: true,
        changeYear: true
    };

    $scope.toDate = {
        changeMonth: true,
        changeYear: true
    };

    angular.element(document).ready(
      function rolBackUiDateDirty() {
        $scope.networksEditForm.$setPristine();
      }
    );

    $scope.editNetworkManager = function (networkManager) {
      $scope.origineCurrentEditedManager = networkManager;
      $scope.currentEditedManager={};
      $scope.currentEditedManager.fromDate = networkManager.fromDate;
      $scope.currentEditedManager.toDate = networkManager.toDate;
      $scope.currentEditedManager.active = networkManager.active;
      $scope.currentEditedManager.user = networkManager.user;
      $scope.currentEditedManagerUserName = networkManager.user.firstName + " " + networkManager.user.lastName;
      $('#network_user').val($scope.currentEditedManagerUserName);
    };

    $scope.cancelAddedNetworkManarger = function () {
      delete $scope.currentEditedManager;
      delete $scope.currentEditedManagerUserName;
    };

    $scope.getSelectedUsers = function () {
      var result = [];
      angular.forEach($scope.checkedUsers, function (value, key) {
        if(value){
          result.push(key);
        }
      });
      return result;
    };

    $scope.saveAddedNetworkManarger = function () {
      if (angular.isDefined($scope.origineCurrentEditedManager)){
        var index = $scope.currentNetworkManagers.indexOf($scope.origineCurrentEditedManager);
        if (index >= 0) {
          $scope.currentNetworkManagers.splice(index, 1);
        }
      }

      if (!angular.isDefined($scope.currentEditedManager.active)){
        $scope.currentEditedManager.active = true;
      }

      if ($scope.currentEditedManager.fromDate) {
        $scope.currentEditedManager.fromDate = new Date(Date.parse($scope.currentEditedManager.fromDate));
      }

      if ($scope.currentEditedManager.toDate) {
        $scope.currentEditedManager.toDate = new Date(Date.parse($scope.currentEditedManager.toDate));
      }

      if ($scope.currentEditedManager.active) {
        $scope.nbActiveManagers++;
      } else {
        $scope.nbUnactiveManagers++;
      }

      $scope.currentNetworkManagers.push($scope.currentEditedManager);

      $scope.addNetworkManagerForm.$setPristine();
      delete $scope.currentEditedManager;
    };

    $scope.removeAddedNetworkManarger = function (networkManager) {
      var index = currentNetworkManagers.indexOf(networkManager);
      if (index != -1) {
        currentNetworkManagers.splice(index, 1);
        if ($scope.currentEditedManager.active) {
          $scope.nbActiveManagers--;
        } else {
          $scope.nbUnactiveManagers--;
        }
      }
    };

    $scope.addManager = function () {
      $scope.currentEditedManager={};
      $scope.currentEditedManager.active = true;
      $('#network_user').val("");
      if ($scope.networksEditForm) {
        $scope.networksEditForm.$setDirty();
      }
    };

    if ($scope.nbActiveManagers === 0) {
      // if there are no manager yet the edit manager form is open and an empty manager is added
      // as a manager is required for the network.
      $scope.addManager();
      $scope.nbActiveManagers++;
    };


    $scope.showInactives = function () {
      if ($scope.showInactivesManagers) {
        $scope.showInactivesManagers = false;
      } else {
        $scope.showInactivesManagers = true;
      }
      return $scope.showInactivesManagers;
    };

    // display commune name
    function setUser(user) {
      $scope.currentEditedManager.user = user;
      $scope.$digest(); // prise en compte des modifs faite par l'autocomplete
    }

    $scope.isFormValid = function() {
      // marche pas a cause de l'autocomplete :
      //return $scope.addNetworkManagerForm.$valid
      var result = true;
      if (angular.isDefined($scope.currentEditedManager)){
        result = !!$scope.currentEditedManager.user;
      }
      return result;
    };

    $("#network_user").autocomplete(
        {
          source : function(request, response) {
            // On remplace par du POST pour les pbs d'encoding
            $.post(ENDPOINTS.listUserJson, request,
                function(data, status, xhr) {
                  if (data) {
                    var autoCompleteField = $("#network_user");
                    if (Object.keys(data).length === 0) {
                      autoCompleteField.addClass("empty-autocomplete-result");
                    } else {
                      autoCompleteField.removeClass("empty-autocomplete-result");
                    }
                  }
                  // On récupère un objet RefLocation brut, qu'on transforme
                  var transformedData = $.map( data, function( user ) {
                    var userFullName = user.firstName + " " + user.lastName;
                    var result =
                      {
                         label: userFullName,
                         value: userFullName,
                         user: user
                      };
                    return result;
                  });
                  response(transformedData);
                }, "json");
            $scope.networksEditForm.$setDirty();
          },
          minLength : 0,
          select : function(event, ui) {
            if (ui.item) {
              setUser(ui.item.user);
            }
          }
        }
    );

    // display commune name
    function setParent(parentId, parentName) {
      var parentObject = {topiaId: parentId, name: parentName};
      $scope.parents.push(parentObject);
      $scope.$digest(); // force le refresh
      $("#network_parents").val("");
    }

    function lookForCycle (network) {
      var result = false;
      if (network.topiaId === $scope.network.topiaId){
        result = true;
      } else {
        angular.forEach(network.parents, function(parent) {
          if (parent.topiaId === $scope.network.topiaId) {
            result = true;
          }
          if (result === false) {
            angular.forEach(parent.parents, function (grandParent) {
              result = lookForCycle(grandParent);
            });
          }
        });
      }
      return result;
    }

    $("#network_parents").autocomplete(
        {
          source : function(request, response) {

            // On ajoute les identifiants des réseaux déjà sélectionnés
            request.exclusions = [];
            angular.forEach($scope.parents, function(item) {
              request.exclusions.push(item.topiaId);
            });
            request.onNetworkEdition = true;

            // Il faut préciser si on est sur un réseau existant
            request.selfNetworkId = $scope.network.topiaId;

            // On remplace par du POST pour les pbs d'encoding
            $.post(ENDPOINTS.searchNetworkJson, request,
                  function(data, status, xhr) {
                      if (data) {
                        var autoCompleteField = $("#network_parents");
                        if (Object.keys(data).length === 0) {
                          autoCompleteField.addClass("empty-autocomplete-result");
                        } else {
                          autoCompleteField.removeClass("empty-autocomplete-result");
                        }
                      }

                      var result = [];
                      angular.forEach(data, function(value, key) {
                          var network = {
                              label: value,
                              value: value,
                              topiaId: key
                            };
                          result.push(network);
                      });

                      response(result);
                  } , "json");
          },
          minLength : 0,
          select : function(event, ui) {
            if (ui.item) {
              setParent(ui.item.topiaId, ui.item.label);
              $scope.networksEditForm.$setDirty();
              return false;
            }
          }
        }
    );

    $scope.checkNetworkNameAvailable = function() {
      var query = "networkName=" + encodeURIComponent($scope.network.name) + "&networkTopiaId=" + encodeURIComponent($scope.network.topiaId);
      $http.post(
          ENDPOINTS.loadNetworkJson,
          query,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
      ).then(function(response) {
          $scope.alreadyExistingNetwork = response.data.length > 0;
        })
       .catch(function(response) {
         console.error("Échec de récupération des réseaux", response);
         addPermanentError("Échec de récupération des réseaux", response.status);
        });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[network.name]',
      function(newValue, oldValue) {
        if (timer) {
            $timeout.cancel(timer);
        }
        timer = $timeout(function(){
          $scope.checkNetworkNameAvailable();
        }, 500);
       }, true);

}]);

/**
 * Unactive selected network after user confirm.
 */
function networkUnactivate(unactivateDialog, unactivateForm, activate) {
  var selectionCount = JSON.parse(unactivateForm.children("input[name='networkIds']").val()).length;
  if (selectionCount > 0) {
      unactivateDialog.dialog({
          resizable: false,
          width: 350,
          modal: true,
          buttons: {
              action: {
                click: function() {
                  $(this).dialog("close");
                  unactivateForm.attr("action", activate ? ENDPOINTS.networksUnactivate+"?activate=true" : ENDPOINTS.networksUnactivate);
                  unactivateForm.submit();
                },
                text: activate ? "Activer" : "Désactiver",
                'class': 'btn-primary'
              },
              cancel: {
                click: function() {
                  $(this).dialog("close");
                },
                text: 'Annuler',
                'class': 'float-left btn-secondary'
              }
          }
      });
  }
}
