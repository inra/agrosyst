/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

function editDomainResponsibles(domainCode) {
  editEntityRoles("DOMAIN_RESPONSIBLE", domainCode, "Responsables du domaine");
}

/**
 * Unactivate selected domains after user confirm.
 */
function domainChangeActivateStatus(unactivateDialog, unactivateForm, activate) {
    var selectionCount = JSON.parse(unactivateForm.children("input[name='domainIds']").val()).length;
    if (selectionCount > 0) {
        unactivateDialog.dialog({
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                action: {
                  click: function() {
                    $(this).dialog("close");
                    unactivateForm.attr("action", activate ? ENDPOINTS.domainsUnactivate+"?activate=true" : ENDPOINTS.domainsUnactivate);
                    unactivateForm.submit();
                  },
                  text: activate ? "Activer" : "Désactiver",
                  'class': 'btn-primary'
                },
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}

/**
 * Duplicate single selected domain after asking user for new campaign year
 * and optional info.
 */
function domainExtend(extendDialog, extendForm) {
    var selectionCount = JSON.parse(extendForm.children("input[name='domainIds']").val()).length;
    if (selectionCount == 1) {
        extendDialog.dialog({
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                "Prolonger": {
                  click: function () {
                    $(this).dialog("close");
                    extendForm.attr("action", ENDPOINTS.domainsExtend);
                    displayPageLoading();
                    extendForm.submit();
                  },
                  text: 'Prolonger',
                  'class': 'btn-primary'
                },
                "Annuler": {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}

function domainExport(exportForm) {
  var selectedDomainIds = JSON.parse(exportForm.children("input[name='domainIds']").val());
  var selectionCount = selectedDomainIds.length;
  if (selectionCount >= 1) {
    exportForm.attr("action", ENDPOINTS.domainsExport);
    exportForm.submit();
  }
}

function domainDelete(confirmDialog, deleteForm) {
  var selectionCount = JSON.parse(deleteForm.children("input[name='domainIds']").val()).length;
  if (selectionCount == 1) {
    confirmDialog.dialog({
        resizable: false,
        width: 400,
        modal: true,
        buttons: {
            action: {
              click: function() {
                $(this).dialog("close");
                deleteForm.attr("action", ENDPOINTS.domainsDelete);
                deleteForm.submit();
              },
              text: "Supprimer",
              'class': 'btn-primary'
            },
            cancel: {
              click: function() {
                $(this).dialog("close");
              },
              text: 'Annuler',
              'class': 'float-left btn-secondary'
            }
        }
    });
  }
}

AgrosystModule.controller('DomainMainController', ['$scope', '$http', '$timeout', 'Domain', 'campaignsBounds',
  function($scope, $http, $timeout, Domain, campaignsBounds) {
  $scope.domain = Domain;
  $scope.domainId = $scope.domain ? $scope.domain.topiaId : null;
  /*$scope.alreadyExistingName = false;
  $scope.refLegalStatus = refLegalStatus;
  $scope.domainType = $scope.domain.type;
  $scope.otherWorkForce = $scope.domain.otherWorkForce * 1;
  $scope.permanentEmployeesWorkForce = $scope.domain.permanentEmployeesWorkForce * 1;
  $scope.temporaryEmployeesWorkForce = $scope.domain.temporaryEmployeesWorkForce * 1;
  $scope.showZoneDetails = false;
  $scope.domainSiret = domainSiret;*/
  $scope.displayFieldsErrorsAlert = true;
  $scope.campaignsBounds = campaignsBounds;
  $scope.showLivestockUnits = false;

  $scope.halfMonths = [{
      id: 0,
      label: "Janvier 1"
    }, {
      id: 1,
      label: "Janvier 2"
    }, {
      id: 2,
      label: "Février 1"
    }, {
      id: 3,
      label: "Février 2"
    }, {
      id: 4,
      label: "Mars 1"
    }, {
      id: 5,
      label: "Mars 2"
    }, {
      id: 6,
      label: "Avril 1"
    }, {
      id: 7,
      label: "Avril 2"
    }, {
      id: 8,
      label: "Mai 1"
    }, {
      id: 9,
      label: "Mai 2"
    }, {
      id: 10,
      label: "Juin 1"
    }, {
      id: 11,
      label: "Juin 2"
    }, {
      id: 12,
      label: "Juillet 1"
    }, {
      id: 13,
      label: "Juillet 2"
    }, {
      id: 14,
      label: "Août 1"
    }, {
      id: 15,
      label: "Août 2"
    }, {
      id: 16,
      label: "Septembre 1"
    }, {
      id: 17,
      label: "Septembre 2"
    }, {
      id: 18,
      label: "Octobre 1"
    }, {
      id: 19,
      label: "Octobre 2"
    }, {
      id: 20,
      label: "Novembre 1"
    }, {
      id: 21,
      label: "Novembre 2"
    }, {
      id: 22,
      label: "Décembre 1"
    }, {
      id: 23,
      label: "Décembre 2"
    }];

  // listener is on agrosyst-app.js
  document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

  //$scope.location;

  // use to set defautSolArvalisRegion
  if ($scope.domain.location) {
    $scope.location = $scope.domain.location;
  }

  $scope.updateShowLivestockUnits = function(otex18) {
    $scope.showLivestockUnits = otex18 && ["41", "42", "43", "44", "50", "60", "71", "72", "81", "82"].includes(otex18.toString());
  }

  if ($scope.domain && $scope.domain.otex18 && $scope.domain.otex18.code_OTEX_18_postes) {
    $scope.updateShowLivestockUnits($scope.domain.otex18.code_OTEX_18_postes);
  }

}]).directive('agCheckDomainMeadowArea', function() {
  function link (scope, elm, attrs, ctrl) {

    var updateErrorOnMeadowArea = function(viewValue) {
      var domain = scope.domain;
      var sum1 = (domain.meadowAlwaysWithGrassArea || 0) + (domain.meadowOtherArea || 0);
      var sum2 = (domain.meadowOnlyPasturedArea || 0) + (domain.meadowOnlyMowedArea || 0) + (domain.meadowPasturedAndMowedArea || 0);
      var valid = !viewValue || (sum1 === 0 || viewValue === sum1) && (sum2 === 0 || viewValue === sum2);
      ctrl.$setValidity('sum', valid);
      ctrl.$setDirty(true);
      return valid ? viewValue : undefined;
    }

    scope.$watch('domain.meadowAlwaysWithGrassArea', function(value) {
      updateErrorOnMeadowArea(scope.domain.meadowArea);
    });

    scope.$watch('domain.meadowOtherArea', function(value) {
      updateErrorOnMeadowArea(scope.domain.meadowArea);
    });

    scope.$watch('domain.meadowOnlyPasturedArea', function(value) {
      updateErrorOnMeadowArea(scope.domain.meadowArea);
    });

    scope.$watch('domain.meadowOnlyMowedArea', function(value) {
      updateErrorOnMeadowArea(scope.domain.meadowArea);
    });

    scope.$watch('domain.meadowPasturedAndMowedArea', function(value) {
      updateErrorOnMeadowArea(scope.domain.meadowArea);
    });

    ctrl.$parsers.unshift(updateErrorOnMeadowArea);
  };
  return {
    require: 'ngModel',
    link: link
  };
});

AgrosystModule.controller('ImportEdaplosController', ['$scope', 'EDaplosResult',
  function($scope, EDaplosResult) {
    $scope.eDaplosResult = EDaplosResult;
}]);
