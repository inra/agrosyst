/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Onglet 'Cycle de cultures pérennes'.
 */
AgrosystModule.controller('PracticedPerennialCropCycleController', ['$scope', '$rootScope', '$http', '$window', '$timeout', 'PracticedSystemInitData', 'I18nMessages',
  function($scope, $rootScope, $http, $window, $timeout, PracticedSystemInitData, I18nMessages) {
    $scope.messages = I18nMessages;

    // le cycle pérenne en cours d'edition
    //$scope.cycle;
    // unité pour un produit
    //$scope.productPriceUnit;
    // phase to remove use by the lightbox
    //$scope.cropCyclePhaseToRemove;
    // name of the PracticedPerennialCropCycle use by the lightbox
    //$scope.practicedPerennialCropCycleName;
    // liste d'entité avec traduction pour ng-options
    $scope.weedTypes = $scope.i18n.WeedType;
    // liste d'entité avec traduction pour ng-options
    $scope.orchardFrutalForms = $scope.i18n.OrchardFrutalForm;
    // liste d'entité avec traduction pour ng-options
    $scope.vineFrutalForms = $scope.i18n.VineFrutalForm;
    // liste d'entité avec traduction pour ng-options
    $scope.pollinatorSpreadModes = $scope.i18n.PollinatorSpreadMode;
    // liste d'entité avec traduction pour ng-options
    $scope.orientationEDIs = PracticedSystemInitData.orientationEDIs;
    // List des types de produits ACTA par AgrosystInterventionType
    $scope.actaTreatmentProductTypes = PracticedSystemInitData.actaTreatmentProductTypes;

    // options des dates pickers
    $scope.dateOptions = {
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy"
    };

    // function that select the perennial crop to edit
    var validationErrorFunction = {
      process: function(e) {
        // look if perennial crop field error
        if (e && e.target && e.target.id) {
          //"PerennialCropEdition0_croppingPlanEntryField_0"
          var name = e.target.id.match(/PerennialCropEdition[1234567890]+/g);
          if (name) {
            // extract index from id
            var index = Number(name[0].match(/[1234567890]+/g)[0]);
            $scope.cycle = $scope.perennialCropCycles[index];
          }
        }

      }
    };

    $scope.displayFieldsErrorsAlert = true;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, validationErrorFunction), true);

    // formule de calcul de la densité de plantation
    // densité (plantes/ha) = 10000 / ((inter-rang/100)*(espacement/100)).
    var computePlantingDensity = function(interFurrow, spacing) {
      if ((interFurrow === 0)  || (spacing === 0)) {
        return null;
      }
      var result = 10000 / ((interFurrow / 100) * (spacing / 100));
      result = Math.round(result * 100) / 100;
      return result;
    };

    // Calcul auto de la densité de plantation
    $scope.$watch("cycle.practicedPerennialCropCycle.plantingInterFurrow", function(newValue, oldValue) {
      if ($scope.cycle) {
        var oldResult = computePlantingDensity(oldValue, $scope.cycle.practicedPerennialCropCycle.plantingSpacing);
        // on modifie si il n'y a pas eu de saisie utilisateur
        if (!$scope.cycle.practicedPerennialCropCycle.plantingDensity || $scope.cycle.practicedPerennialCropCycle.plantingDensity == oldResult) {
          $scope.cycle.practicedPerennialCropCycle.plantingDensity = computePlantingDensity(newValue, $scope.cycle.practicedPerennialCropCycle.plantingSpacing);
        }
      }
    });

    // Calcul auto de la densité de plantation
    $scope.$watch("cycle.practicedPerennialCropCycle.plantingSpacing", function(newValue, oldValue) {
      if ($scope.cycle) {
        var oldResult = computePlantingDensity($scope.cycle.practicedPerennialCropCycle.plantingInterFurrow, oldValue);
        // on modifie si il n'y a pas eu de saisie utilisateur
        if (!$scope.cycle.practicedPerennialCropCycle.plantingDensity || $scope.cycle.practicedPerennialCropCycle.plantingDensity == oldResult) {
          $scope.cycle.practicedPerennialCropCycle.plantingDensity = computePlantingDensity($scope.cycle.practicedPerennialCropCycle.plantingInterFurrow, newValue);
        }
      }
    });

    var findSelectedCroppingPlanEntryDtoByCode = function(cycle) {
      if (cycle.practicedPerennialCropCycle && cycle.practicedPerennialCropCycle.croppingPlanEntryCode) {
        angular.forEach($scope.croppingPlanModel, function(item) {
          if (cycle.practicedPerennialCropCycle.croppingPlanEntryCode === item.croppingPlanEntryCode) {
            cycle.selectedCroppingPlanEntryDto = item;
          }
        });
      }
    };

    if ($scope.perennialCropCycles) {
      angular.forEach($scope.perennialCropCycles, function(cycle){

        if (cycle.practicedPerennialCropCycle && cycle.practicedPerennialCropCycle.croppingPlanEntryCode) {
          angular.forEach($scope.croppingPlanModel, function(item) {
            if (cycle.practicedPerennialCropCycle.croppingPlanEntryCode === item.croppingPlanEntryCode) {
              cycle.selectedCroppingPlanEntryDto = item;
              cycle.selectedPracticedPerennialCropCycleSolOccupationPercent = cycle.practicedPerennialCropCycle.solOccupationPercent;
            }
          });
        }

      });
    }

    var findSelectedOrientationEDIById = function(cycle) {
      if (cycle.practicedPerennialCropCycle && cycle.practicedPerennialCropCycle.orientation) {
        angular.forEach($scope.orientationEDIs, function(orientation) {
          if (cycle.practicedPerennialCropCycle.orientation.topiaId === orientation.topiaId) {
            cycle.practicedPerennialCropCycle.orientation = orientation;
          }
        });
      }
    };

    // convert date representation to real date objects
    var cropCyclePerennialSpeciesDateConversion = function (cycle) {
      if (cycle.speciesDto && cycle.speciesDto !== null) {
        angular.forEach(cycle.speciesDto, function(cropCyclePerennialSpecies) {
          if (cropCyclePerennialSpecies.overGraftDate) {
            cropCyclePerennialSpecies.overGraftDate = new Date(Date.parse(cropCyclePerennialSpecies.overGraftDate));
          }
       });
      }
    };

    angular.element(document).ready(
      function rolBackUiDateDirty() {
        $scope.practicedSystemForm.$setPristine();
      }
    );

    $scope.countPerennialCropInterventions = function (tmpSum, phase) {
      var phase = angular.isDefined(phase) && phase.interventions ? phase.interventions.length : 0;
      return tmpSum + phase;
    };

    $scope.toggleCycle = function (practicedPerennialCropCycleDto) {
      if ($scope.cycle && $scope.cycle === practicedPerennialCropCycleDto) {
        delete $scope.cycle;
      } else {

        if (practicedPerennialCropCycleDto.cropCyclePhaseDtos) {
          var sum = 0;
          practicedPerennialCropCycleDto.cropCyclePhaseDtos.forEach(async function(phase) {
            sum = $scope.countPerennialCropInterventions(sum, phase)
          })
        }

        $scope.cycle = practicedPerennialCropCycleDto;
        $scope.cycle.nbInterventions = sum;
        findSelectedCroppingPlanEntryDtoByCode($scope.cycle);
        cropCyclePerennialSpeciesDateConversion($scope.cycle);
        findSelectedOrientationEDIById($scope.cycle);
      }
      $rootScope.perennialCropCyclesTabOpened = true;
    };

    // default edited practiced perennial crop cycle.
    if ($scope.perennialCropCycles && $scope.perennialCropCycles.length === 1) {
      $scope.toggleCycle($scope.perennialCropCycles[0]);
      // tab has not been selected so tab has not been opened
      $rootScope.perennialCropCyclesTabOpened = false;
    }

    $scope.enableAutocompleteGraftSupportByIndex = -1;
    $scope.enableAutocompleteGraftSupport = function(index) {
      $scope.enableAutocompleteGraftSupportByIndex = index;
    };

    $scope.autocompleteGraftSupport = function (index, cropCyclePerennialSpeciesItem, term) {
      if(index == $scope.enableAutocompleteGraftSupportByIndex) {
        return $http.post(
          ENDPOINT_GRAFT_SUPPORT_JSON, "speciesId=" + encodeURIComponent(cropCyclePerennialSpeciesItem.speciesId) +"&term=" + encodeURIComponent(term),
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
          then(function(response) {
          var transformedData = $.map( response.data, function( variety ) {
              var displayName = variety.denomination ? variety.denomination : variety.variete;
              var result = {
                   label: displayName,
                   value: displayName,
                   topiaId: variety.topiaId
              };
              return result;
            });

            var transformedDataWithEmptyOption = [];
            transformedDataWithEmptyOption.push({label:'' , topiaId: null});
            angular.forEach(transformedData, function(tfd) {
              transformedDataWithEmptyOption.push(tfd);
            });

            $scope.graftSupports = transformedDataWithEmptyOption;
          }).
          catch(function(response) {
            var message = $scope.messages.graftSupportsLoadingFailed;
            console.error(message, response);
            addPermanentError(message, response.status);
          });
      } else {
        $scope.graftSupports = [];
      }
    };

    $scope.enableGetAutocompleteGraftCloneByIndex = -1;
    $scope.enableGetAutocompleteGraftClone = function(index) {
      $scope.enableGetAutocompleteGraftCloneByIndex = index;
    };
    /**
     * Function that return a specific ui.autocomplete configuration for each value
     * of ng-repeat iterator (xhr call contextualized with current species and
     * variety).
     */
    $scope.getAutocompleteGraftClone = function (index, cropCyclePerennialSpeciesItem, term) {
      if (index === $scope.enableGetAutocompleteGraftCloneByIndex &&
          cropCyclePerennialSpeciesItem.speciesId &&
          cropCyclePerennialSpeciesItem.varietyId) {
        var localRequest = {
          speciesId: cropCyclePerennialSpeciesItem.speciesId,
          varietyId: cropCyclePerennialSpeciesItem.varietyId,
          term: term};
        $.post(
          ENDPOINT_GRAFT_CLONE_JSON, localRequest,function(data, status, xhr) {
            var transformedData = $.map( data, function( clonePlantGrape ) {
                var displayName = clonePlantGrape.codeClone + ', ' +
                    clonePlantGrape.anneeAgrement + ' (' +
                    clonePlantGrape.origine + ')';
                var result = {
                     label: displayName,
                     value: displayName,
                     topiaId: clonePlantGrape.topiaId
                };
                return result;
              });

              var transformedDataWithEmptyOption = [];
              transformedDataWithEmptyOption.push({label:'' , topiaId: null});
              angular.forEach(transformedData, function(tfd) {
                transformedDataWithEmptyOption.push(tfd);
              });

              $scope.graftSupportClones = transformedDataWithEmptyOption;
            }, "json");
      } else {
        $scope.graftSupportClones = [];
      }
    };

    $scope.computeTotalSolOccupationPercent = function() {
      var result = 0;
      angular.forEach($scope.perennialCropCycles, function(cycle){
        if (cycle != $scope.cycle &&cycle.practicedPerennialCropCycle && cycle.practicedPerennialCropCycle.solOccupationPercent) {
          result += parseFloat(cycle.practicedPerennialCropCycle.solOccupationPercent);
        }
      });
      return result;
    };

    // creation d'un nouveau cycle
    $scope.createPracticedPerennialCropCycle = function () {
      $scope.cycle = {};
      $scope.cycle.practicedPerennialCropCycle = {weedType: "PAS_ENHERBEMENT"};
      $scope.cycle.practicedPerennialCropCycle.solOccupationPercent = 100 - $scope.computeTotalSolOccupationPercent();
      $scope.cycle.cropCyclePhaseDtos = [];
      var cropCyclePhase = {duration:0,type: "PLEINE_PRODUCTION"};
      $scope.cycle.cropCyclePhaseDtos.push(cropCyclePhase);
      $scope.perennialCropCycles.push($scope.cycle);
    };

    // ajout d'une phase au cycle en cours d'edition
    $scope.addCropCyclePhase = function(cropCyclePhaseType) {
      if ($scope.cycle.cropCyclePhaseDtos.length >= 4) {
        _displayInfoDialog($scope, $("#addPhaseMaxNumberRaised"), 400, "", { process : function(){} });
      } else {
        $scope.cycle.cropCyclePhaseDtos.push( { type: cropCyclePhaseType } );
      }
    };

    $scope.removeCropCyclePhase = function (cropCyclePhase) {
      var indexOfCropCyclePhaseToRemove = $scope.cycle.cropCyclePhaseDtos.indexOf(cropCyclePhase);

      if (cropCyclePhase.interventions && cropCyclePhase.interventions.length > 0) {
        var impactMessage = $scope.messages.deletePhaseConfirm + ": " + $scope.perennialPhaseTypes[cropCyclePhase.type] + "?"
            + "<ul><li><b>";
        if (cropCyclePhase.interventions.length === 1) {
          impactMessage += $scope.messages.deletePhaseConfirmOne;
        } else {
          impactMessage += $scope.messages.deletePhaseConfirmSeveral.replace('{}', cropCyclePhase.interventions.length);
        }
        impactMessage += "</b></li></ul>";

        $scope.deletePhase = { impactMessage: impactMessage, confirm: true };

        var perennialDeletePhaseModel = {
          process: function() {
            $scope.cycle.cropCyclePhaseDtos.splice(indexOfCropCyclePhaseToRemove, 1);
          },
          cancelChanges: function() {}
        };

        _displayConfirmDialog($scope, $("#confirmDeletePhase"), 1024, perennialDeletePhaseModel);
      } else {
        $scope.cycle.cropCyclePhaseDtos.splice(indexOfCropCyclePhaseToRemove, 1);
      }
    };

    $scope.propagateCropChangeOnPerennialCycle = function () {
      var doValidation = true;
      var impactMessage = $scope._lookForPerennialCropChangeImpact($scope.cycle.cropCyclePhaseDtos, doValidation);
      $scope.cropChange = {impactMessage:impactMessage, confirm: true};

      var perennialCropChangeModel = {
        process: function() {
          $scope.doOrNotCropChange(true);
        },
        cancelChanges: function() {
          $scope.doOrNotCropChange(false);
        }
      };

      if (impactMessage) {
        _displayConfirmDialog($scope, $("#confirmCropChange"), 1024, perennialCropChangeModel);
      } else {
        perennialCropChangeModel.process();
      }
    };

    $scope.doOrNotCropChange = function(confirm) {
      if (confirm && $scope.cycle.selectedCroppingPlanEntryDto && $scope.practicedSystem.campaigns) {
        // load the species
        $http.post(
          ENDPOINTS.loadPracticedPerennialCroppingPlanSpeciesJson,
          "croppingPlanEntryCode=" + $scope.cycle.selectedCroppingPlanEntryDto.croppingPlanEntryCode +
          //"&cycleId=" + cycleId +
          "&campaigns=" + $scope.practicedSystem.campaigns,
          { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
        ).then(function(response) {
          $scope._flushCroppingEntryChanges(response.data);
          $scope.croppingPlanModelLoading = false;
        }).
        catch(function(response) {
          var message = $scope.messages.speciesLoadingFailed;
          console.error(message, response);
          addPermanentError(message, response.status);
        });
      } else {
        if ($scope.cycle.practicedPerennialCropCycle.croppingPlanEntryCode) {
          angular.forEach($scope.croppingPlanModel, function(croppingPlanEntryDto){
            if (croppingPlanEntryDto.croppingPlanEntryCode === $scope.cycle.practicedPerennialCropCycle.croppingPlanEntryCode) {
              $scope.cycle.selectedCroppingPlanEntryDto = croppingPlanEntryDto;
            }
          });
        }
        $scope.croppingPlanModelLoading = false;
      }
    };

//    _manageSpeciesChangeOnSpeciesStades = function(intervention, newCycleSpecies, mapPreviousSpeciesCodeToNewSpeciesCode) {
//      var newInterventionSpeciesStades = [];
//      angular.forEach(intervention.speciesStadesDtos, function(speciesStade){
//        var croppingPlanSpecies = mapPreviousSpeciesCodeToNewSpeciesCode[speciesStade.speciesCode];
//
//        if (croppingPlanSpecies){
//          speciesStade.speciesCode = croppingPlanSpecies.code;
//          newInterventionSpeciesStades.push(speciesStade);
//        }
//      });
//
//      // push original species stade to the intervention if compatible
//      // if not a new species stade is created
//      angular.forEach(newCycleSpecies, function(species){
//        var found = false;
//        angular.forEach(newInterventionSpeciesStades, function(speciesStade){
//          // species stade is compatible, but to be valid we must change his species code
//          found = found === true ? true : speciesStade.speciesCode === species.code;
//        });
//
//        if (!found) {
//          var speciesStade = {speciesCode : species.code};
//          newInterventionSpeciesStades.push(speciesStade);
//        }
//      });
//
//      intervention.speciesStadesDtos = newInterventionSpeciesStades;
//    };

    $scope._lookForPerennialCropChangeImpact = function(phases, doValidation) {
      // for input management
      var result;

      if (doValidation && (!$scope.cycle.selectedCroppingPlanEntryDto || !$scope.practicedSystem.campaigns)) {
        var cropError = !$scope.cycle.selectedCroppingPlanEntryDto ? "<li>" + $scope.messages.noCropSelected + "</li>" : "";
        var campaignError = !$scope.practicedSystem.campaigns ? "<li>" + $scope.messages.noCampaignForPracticed + "</li>" : "";
        result = "<b>" + $scope.messages.cropChangeRefused + ":</b><ul>" + cropError + campaignError + "</ul>";

      } else if (!$scope.croppingPlanModelLoading && phases) {
        angular.forEach(phases, function(phase){
          if (phase.interventions && phase.interventions.length > 0) {
            var nbConcernedInterventions = phase.interventions.length;

            var interMess = (nbConcernedInterventions === 1 ? $scope.messages.forPhaseIntervention
                : $scope.messages.forPhaseInterventions.replace('{}', nbConcernedInterventions)) + " <br><br>";
            if (result) {
              result += "<br />";
            }
            result = "<b>" + $scope.messages.forPhase.replace('{}', $scope.perennialPhaseTypes[phase.type]) + "</b>," + interMess;

            angular.forEach(phase.interventions, function(intervention){
              var nbInterventionSeedingActions = 0;
              angular.forEach(intervention.actions, function(action) {
                if (action.seedingSpecies) {
                  nbInterventionSeedingActions = nbInterventionSeedingActions + 1;
                }
              });

            if (nbInterventionSeedingActions > 0) {
                result += " <li> " + $scope.messages.forIntervention.replace('{}', intervention.name) + ":";
                if (nbInterventionSeedingActions === 1) {
                  result += $scope.messages.forInterventionDefaultValuesForAction;
                } else {
                  result += $scope.messages.forInterventionDefaultValuesForActions;
                }
                result += "</li>";
              }
            });
          }
        });
      }
      return result;
    };

    $scope._doPerennialCropChange = function(phases, newCycleSpecies, newSelectedCroppingPlanEntryCode, newSelectedCroppingPlanEntryLabel) {

      $scope.cycle.speciesDto = newCycleSpecies;
      $scope.cycle.practicedPerennialCropCycle.croppingPlanEntryCode = newSelectedCroppingPlanEntryCode;
      $scope.cycle.croppingPlanEntryName = newSelectedCroppingPlanEntryLabel;

      angular.forEach(phases, function(phase){
        if (phase.interventions && phase.interventions.length > 0) {

          angular.forEach(phase.interventions, function(intervention){

            // make change on species stades
            intervention.speciesStadesDtos = [];
            angular.forEach(newCycleSpecies, function(species){
              var speciesStade = {speciesCode : species.code};
              intervention.speciesStadesDtos.push(speciesStade);
            });

            // make change on seeding actions
            angular.forEach(intervention.actions, function(action) {
              if (action.seedingSpecies) {

                var treatment;
                var biologicalSeedInoculation;
                var quantity;
                var unit;

                // look for default value for new seeding action species (default value = first value found)
                if (action.seedingSpecies) {
                  angular.forEach(action.seedingSpecies, function(seedingSpecies){

                      if (seedingSpecies.treatment) {
                        treatment = true;
                        if (!quantity) {
                          quantity = seedingSpecies.quantity;
                          unit = seedingSpecies.seedPlantUnit;
                        }
                      }
                      if (seedingSpecies.biologicalSeedInoculation) {
                        biologicalSeedInoculation = true;
                        if (!quantity) {
                          quantity = seedingSpecies.quantity;
                          unit = seedingSpecies.seedPlantUnit;
                        }
                      }
                  });

                  // set default values to all species refs #6282
                  action.seedingSpecies = [];
                  angular.forEach(newCycleSpecies, function(species){
                    var seedingSpecies = {speciesCode: species.code,treatment:treatment, biologicalSeedInoculation:biologicalSeedInoculation, quantity:quantity, seedPlantUnit:unit};
                    action.seedingSpecies.push(seedingSpecies);
                  });
                }

              }
            });
          });
        }
      });
    };

    $scope._flushCroppingEntryChanges = function(newCycleSpecies) {
        var newSelectedCroppingPlanEntryCode = $scope.cycle.selectedCroppingPlanEntryDto.croppingPlanEntryCode;
        var newSelectedCroppingPlanEntryLabel = $scope.cycle.selectedCroppingPlanEntryDto.label;
        $scope._doPerennialCropChange($scope.cycle.cropCyclePhaseDtos, newCycleSpecies, newSelectedCroppingPlanEntryCode, newSelectedCroppingPlanEntryLabel);
    };

    $scope.deletePracticedPerennialCropCycle = function (practicedPerennialCropCycle) {
      $scope.practicedPerennialCropCycleName = practicedPerennialCropCycle.croppingPlanEntryName;
      $scope.practicedPerennialCropCycleNbInterventions = practicedPerennialCropCycle.nbInterventions;

      var deletePracticedPerennialCropCycleModel = {
        process: function() {
          var indexOf = $scope.perennialCropCycles.indexOf(practicedPerennialCropCycle);
          $scope.perennialCropCycles.splice(indexOf , 1);
          delete $scope.cycle;
          // default edited practiced perenial crop cycle.
          if ($scope.perennialCropCycles && $scope.perennialCropCycles.length === 1) {
            $scope.toggleCycle($scope.perennialCropCycles[0]);
          }
        },
        cancelChanges: function() {}
      };

      _displayConfirmDialog($scope, $("#confirmRemovedPerennialCycle"), 400, deletePracticedPerennialCropCycleModel);

    };

    // retourne les types de phase disponibles (non actuellement définis)
    $scope.availablePhaseTypes = function() {
      // reverse array to fix float style in ui
      // TODO EC 11/11/13 : Manage the fix in css rather than in js
      var result = Object.keys($scope.perennialPhaseTypes).reverse();
      // On retire de la liste les types déjà utilisés
      if ($scope.cycle && $scope.cycle.cropCyclePhaseDtos){
        angular.forEach($scope.cycle.cropCyclePhaseDtos, function(item) {
          var indexOf = result.indexOf(item.type);
          result.splice(indexOf , 1);
        });
      }
      return result;
    };

    // angular filter to sort phase by enum ordinal
    $scope.orderByPhaseType = function(item) {
      var allKeys = Object.keys($scope.perennialPhaseTypes);
      return allKeys.indexOf(item.type);
    };

    $scope.displayWarningSolOccupationPercent = false;
    // valid that perennial crop has valid solOccupationPercent
    $scope.validSolOccupationPercent = function() {
      var value = $scope.cycle.practicedPerennialCropCycle.solOccupationPercent;
      var result = true;
      if (angular.isDefined(value)) {
        if (value < 0) {
         result = false;
        } else {
         var otherCropSolOccupation = $scope.computeTotalSolOccupationPercent();
         var floatValue = parseFloat(value);
         result = (otherCropSolOccupation + floatValue) <= 100;
        }
      }
      $scope.displayWarningSolOccupationPercent = !result;
    };

    $scope.bindSelectedCropCyclePerennialSpeciesGraftSupport = function(item, cropCyclePerennialSpeciesItem) {
      if (cropCyclePerennialSpeciesItem) {
        if (item.topiaId === null) {
          delete cropCyclePerennialSpeciesItem.graftSupport;
        } else {
          cropCyclePerennialSpeciesItem.graftSupport = item;
        }
      }
      $scope.graftSupports = [{label:'' , topiaId: null}];
    };

    $scope.bindSelectedCropCyclePerennialSpeciesGraftClone = function(item, cropCyclePerennialSpeciesItem) {
      if (cropCyclePerennialSpeciesItem) {
        if (item.topiaId === null) {
          delete cropCyclePerennialSpeciesItem.graftClone;
        } else {
          cropCyclePerennialSpeciesItem.graftClone = item;
        }
      }
      $scope.graftSupportClones = [{label:'' , topiaId: null}];
    };

}]);
