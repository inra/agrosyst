/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 *  Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Affichage de la liste des parcelles types.
 */
AgrosystModule.controller('PracticedPlotsListController', ['$scope', '$http', '$timeout', 'PracticedPlotsInitData', 'practicedPlotFilter',
  function($scope, $http, $timeout, PracticedPlotsInitData, practicedPlotFilter) {
    $scope.practicedPlots = PracticedPlotsInitData.elements;
    $scope.pager = PracticedPlotsInitData;
    $scope.filter = practicedPlotFilter;

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0,1);
    };

    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) { return; } // prevent init useless call

      $http.post(ENDPOINTS.practicedPlotsListJson, "filter=" + encodeURIComponent(angular.toJson($scope.filter)),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
          $scope.practicedPlots = response.data.elements;
          $scope.pager = response.data;
      })
      .catch(function(response) {
         console.error("Échec de récupération des parcelles du synthétisé", response);
         addPermanentError("Échec de récupération des parcelles du synthétisé", response.status);
      });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.practicedPlotName, filter.practicedSystemName, filter.growingSystemName, filter.practicedSystemCampaign, filter.domainName, filter.growingPlanName]',
    function(newValue, oldValue) {
      if (timer) {
        $timeout.cancel(timer);
      }
      timer = $timeout(function(){
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      }, 350);
    }, true);

    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);
}]);


AgrosystModule.controller('PracticedPlotEditController', ['$scope', '$timeout', 'PracticedPlot',
 function ($scope, $timeout, PracticedPlot) {
   // la parcelle en cours d'edition
   $scope.practicedPlot = PracticedPlot;

   $scope.displayFieldsErrorsAlert = true;
   // listener is on agrosyst-app.js
   document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

   //compute commune name auto completion
   $("#commune").autocomplete(
     {
       source : function(request, response) {
         if (request.term && request.term.length >= 1) {
           // On remplace par du POST pour les pbs d'encoding
           $.post(ENDPOINTS.listRefLocationJson, request,
             function(data, status, xhr) {
               if (data) {
                 var autoCompleteField = $("#commune");
                 if (data.length === 0) {
                   autoCompleteField.addClass("empty-autocomplete-result");
                 } else {
                   autoCompleteField.removeClass("empty-autocomplete-result");
                 }
               }
               // On récupère un objet RefLocation brut, qu'on transforme
               var transformedData = $.map( data, function( loc ) {
                 var codePostalAndName = loc.codePostal + ", " + loc.commune;
                 var result =
                   {
                      label: codePostalAndName,
                      value: codePostalAndName,
                      refLocation: loc
                   };
                 return result;
               });
               response(transformedData);
             }, "json");
           } else {
             $("#locationTopiaId").val("");
           }
         $scope.practicedPlotEditForm.$setDirty();
       },
       minLength : 0,
       select : function(event, ui) {
         if (ui.item) {
           $("#locationTopiaId").val(ui.item.refLocation.topiaId);
         }
       }
     }
   );
 }]);

AgrosystModule.controller('PracticedPlotZoningController', ['$scope', 'ParcelleZonages', 'SelectedPlotZoningIds',
 function ($scope, ParcelleZonages, SelectedPlotZoningIds) {
   // l'ensemble des zonages selectionnable
   $scope.parcelleZonages = ParcelleZonages;
   // les topiaId des zonages selectionnés
   $scope.selectedPlotZoningIds = {};

   // set id select in map by default
   angular.forEach(SelectedPlotZoningIds, function(id) {
     $scope.selectedPlotZoningIds[id] = true;
   });
}]);

AgrosystModule.controller('PracticedPlotSolController', ['$scope', 'SelectedSurfaceTextureId',
      'SelectedSubSoilTextureId', 'SelectedSolProfondeurId',
      'SolTextures', 'SolHorizons', 'SolCaracteristiques', 'SolProfondeurs',
 function ($scope, SelectedSurfaceTextureId, SelectedSubSoilTextureId,
     SelectedSolProfondeurId, SolTextures, SolHorizons, SolCaracteristiques, SolProfondeurs) {
   // {String} l'id de la texture geppa de la surface
   $scope.selectedSurfaceTextureId = SelectedSurfaceTextureId ? SelectedSurfaceTextureId : "";
   // {String} l'id de la texture geppa du sous sol
   $scope.selectedSubSoilTextureId = SelectedSubSoilTextureId ? SelectedSubSoilTextureId : "";
   // {String} l'id de la classe de profondeur
   $scope.selectedSolProfondeurId = SelectedSolProfondeurId ? SelectedSolProfondeurId : "";
   // le sol sélectionné dans l'onglet Sol (pour afficher l'aide à la saisie)
   //$scope.selectedSol;
   // la liste des horizon du sol
   $scope.solHorizons = SolHorizons;
   // l'horizon selectionné pour edition
   //$scope.selectSolHorizon;
   // index du sol selectionné dans la liste
   //$scope.selectedSolHorizonIndex;
   // liste des sol textures (pour recuperation du label pour affichage liste des horizons)
   $scope.solTextures = SolTextures;
   // liste des profondeur (pour le calcul de la reserve utile)
   $scope.solProfondeurs = SolProfondeurs;
   // liste des sol carateristiques (pour le calcul de la reserve utile)
   $scope.solCaracteristiques = SolCaracteristiques;

   // called to add a new horizon
   $scope.addSolHorizon = function() {
     // auto select plot texture
     var solHorizon = {
         solTextureId : $scope.selectedSolTextureId
     };
     $scope.solHorizons.push(solHorizon);
     $scope.editSolHorizon(solHorizon, $scope.solHorizons.length - 1);
   };

   // called when editing a line
   $scope.editSolHorizon = function(solHorizon, index) {
     $scope.selectedSolHorizon = solHorizon;
     $scope.selectedSolHorizonIndex = index;
   };

   // suppression d'un sol horizon depuis la liste des sol horizons
   $scope.deleteSolHorizon = function(shIndex) {
     $scope.solHorizons.splice(shIndex, 1);
   };

   // get sol texture label from sol texture topia id
   $scope.solTextureLabel = function(solHorizon) {
     var textureLabel;
     angular.forEach($scope.solTextures, function(solTexture) {
       if (solTexture.topiaId == solHorizon.solTextureId) {
         textureLabel = solTexture.classes_texturales_GEPAA;
       }
     });
     return textureLabel;
   };

   // validate current low rating to be > previous horizon low rating and < next horizon low rating
   $scope.validateHorizonLowRating = function(value) {
     var result = true;
     if (!angular.isDefined(value)) {
       return result;
     }
     var fValue = parseFloat(value);
     if ($scope.selectedSolHorizonIndex > 0) {
       var previousSolHorizon = $scope.solHorizons[$scope.selectedSolHorizonIndex-1];
       result &= fValue > previousSolHorizon.lowRating;
     }
     if ($scope.selectedSolHorizonIndex < $scope.solHorizons.length - 1) {
       var nextSolHorizon = $scope.solHorizons[$scope.selectedSolHorizonIndex+1];
       result &= fValue < nextSolHorizon.lowRating;
     }
     return result;
   };

   // compute sol horizon Thickness depending on other sol horizon
   $scope.computeSolHorizonThickness = function(solHorizon, solHorizonIndex) {
     var result = 0;
     if (solHorizon && solHorizon.lowRating) {
       result = solHorizon.lowRating;
       if (solHorizonIndex > 0) {
         var previousSol = $scope.solHorizons[solHorizonIndex - 1];
         if (previousSol.lowRating) {
           result -= previousSol.lowRating;
         }
       }
     }
     return result;
   };

   // Compute usefull reserve
   // RU réelle = RU de la classe de profondeur du sol choisi * (profondeur saisie / h) * (1-% cailloux)
   // RU de le classe de prondeur : présent dans le référentiel sol_texture (INDIGO)
   // h = epaisseur d'horizon : présents dans le référentiel sol_texture (GEPPA)
   // % cailloux = pierrosité moyenne
   // Correspondance entre INDIGO et GEPPA dans le référentiel sol_texture GEPPA
   var computeUsefullReserveSingle = function(solTextureId, solDepth, solStoniness) {
     var textureAbbr;
     var profondeurDefault;
     var profondeurClass;
     var reserveUtile;
     // recherche de l'abbreviation de texture de la texture selectionée
     angular.forEach($scope.solTextures, function(item) {
       if (item.topiaId == solTextureId) {
         textureAbbr = item.abbreviation_INDIGO;
       }
     });
     // recherche de la classe de profondeur de la profondeur sélectionnée
     angular.forEach($scope.solProfondeurs, function(item) {
       if (item.topiaId == $scope.selectedSolProfondeurId) {
         profondeurClass = item.classe_de_profondeur_INDIGO;
         profondeurDefault = item.profondeur_par_defaut;
       }
     });
     // à partir de ces deux informations, on croise avec un 3eme référentiel
     // pour obtenir la réserve utile
     angular.forEach($scope.solCaracteristiques, function(item) {
       if (item.abbreviation_INDIGO == textureAbbr && item.classe_de_profondeur_INDIGO == profondeurClass) {
         reserveUtile = item.reserve_utile;
       }
     });
     // valeaur par defaut si undefined
     if (!solStoniness) {solStoniness = 0;}
     if (!solDepth) {solDepth = profondeurDefault;}
     if (!reserveUtile) {reserveUtile = 0;}
     // et on fait le cacul
     var usefullReserve;
     if (profondeurDefault) {
         usefullReserve = reserveUtile * (solDepth / profondeurDefault) * (1 - solStoniness / 100);
     } else {
       usefullReserve = 0;
     }
     return usefullReserve;
   };

   // s'il y a des horizon, c'est pas somme des reserves utiles des horizons
   var computeUsefullReserveComplex = function() {
     var sum = 0;
     var index = 0;
     angular.forEach($scope.solHorizons, function(solHorizon) {
       var solHorizonDepth = $scope.computeSolHorizonThickness(solHorizon, index);
       sum += computeUsefullReserveSingle(solHorizon.solTextureId, solHorizonDepth, solHorizon.stoniness);
       index++;
     });
     return sum;
   };

   // chose compute method depending on horizon number
   $scope.computeUsefullReserve = function() {
     var result;
     if ($scope.solHorizons.length > 0) {
       result = computeUsefullReserveComplex();
     } else {
       result = computeUsefullReserveSingle($scope.selectedSurfaceTextureId, $scope.practicedPlot.solMaxDepth, $scope.practicedPlot.solStoniness);
     }
     return Math.round(result*100)/100;
   };
}]);

AgrosystModule.controller('PracticedPlotAdjacentElementsController', ['$scope', 'AdjacentElements', 'AdjacentElementIds',
 function ($scope, AdjacentElements, AdjacentElementIds) {
   // {Array} Element de voisinage du référentiel
   $scope.adjacentElements = AdjacentElements;
   // {Object} Ids des elements de voisinage sélectionnés
   $scope.adjacentElementIds = {};

   // set id select in map by default
   angular.forEach(AdjacentElementIds, function(id) {
     $scope.adjacentElementIds[id] = true;
   });
}]);
