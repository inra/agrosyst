/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Meta controller pour l'edition d'un système synthétisé.
 */
AgrosystModule.controller('PracticedSystemController', ['$scope', '$rootScope', '$timeout', '$http', '$q', 'PracticedSystemInitData', 'I18nMessages',
  function($scope, $rootScope, $timeout, $http, $q, PracticedSystemInitData, I18nMessages) {
    $scope.messages = I18nMessages;
    //$scope.defaultGrowingSystemId = PracticedSystemInitData.defaultGrowingSystemId;
    $scope.frontApp = PracticedSystemInitData.frontApp;
    // code du domaine associé au système de culture utilisé pour identifier le domaine sur quel domaine porte un ITK.
    $scope.domainId = PracticedSystemInitData.domainId;
    // topiaId du domaine
    $scope.domainTopiaId = PracticedSystemInitData.domainTopiaId;
    // le systeme de cultures actuellement sélectionné
    $scope.growingSystemTopiaId = PracticedSystemInitData.growingSystemTopiaId;
    // la liste des cycles pérennes (onglet pérennes)
    $scope.perennialCropCycles = PracticedSystemInitData.perennialCropCycles;
    // la liste des cycles assolé (onglet cycle assolées)
    $scope.seasonalCropCycles = PracticedSystemInitData.seasonalCropCycles;
    // la liste des cultures principale utilisable dans les cycles
    $scope.croppingPlanModel = PracticedSystemInitData.croppingPlanModel || [];
    // la liste des cultures intermédiaire utilisable dans un cpdc assolées
    $scope.intermediateCroppingPlanModel = PracticedSystemInitData.intermediateCroppingPlanModel;
    // map de cultures indexées par code
    $scope.croppingPlanEntriesByCodes = {};
    // association entre les code de cultures et la liste des especes (pour affichage : cycle assolés)
    $scope.croppingPlanEntrySpeciesIndex = PracticedSystemInitData.croppingPlanEntrySpeciesIndex || [];
    // liste d'entité avec traduction pour ng-options
    $scope.perennialPhaseTypes = $scope.i18n.CropCyclePhaseType;
    // use to detect that perennial crop cycle has been edited
    $rootScope.perennialCropCyclesTabOpened = false;
    // use for harvesting action
    //$scope.sector;
    $scope.campaignsBounds = PracticedSystemInitData.campaignsBounds;
    // campaigns of all growing systems with same code
    $scope.gsCampaigns = PracticedSystemInitData.gsCampaigns;
    // to inform user that ps campaigns are found into gs campaigns
    $scope.areCampaignsPartOfGrowingSystemCampaigns = true;
    // selected growing system key on growing system list on practiced system creation (on validation error only)
    $scope.growingSystemSector = PracticedSystemInitData.growingSystemSector;
    $scope.typeAgricultureTopiaId = PracticedSystemInitData.typeAgricultureTopiaId;
    $scope.growingSystemSectors = PracticedSystemInitData.growingSystemSectors;
    $scope.domainCampaign = PracticedSystemInitData.domainCampaign;

    // options des dates pickers
    $scope.dateOptions = {
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy"
    };

    // the practicedSystem
    $scope.practicedSystem = {
      name: PracticedSystemInitData.practicedSystemName,
      comment: PracticedSystemInitData.practicedSystemComment,
      topiaId: PracticedSystemInitData.practicedSystemTopiaId,
      growingSystemTopiaId : $scope.growingSystemTopiaId
    };

    if (PracticedSystemInitData.campaigns) {
      $scope.practicedSystem.campaigns = PracticedSystemInitData.campaigns;
    } else if ($scope.gsCampaigns && $scope.gsCampaigns.length > 0){
      $scope.practicedSystem.campaigns = $scope.gsCampaigns[0].toString();
    }

    // to be able to rollback campaigns
    $scope.lastValidCampaignSeries = $scope.practicedSystem.campaigns;

    // this is to avoid empty line into sources list because of unsuccessfully binding to null
    if (PracticedSystemInitData.practicedSystemSource && PracticedSystemInitData.practicedSystemSource !== null) {
      $scope.practicedSystem.source = PracticedSystemInitData.practicedSystemSource;
    }

    // Initialisation des listes de cycle car c'est plus simple à gerer
    if (!$scope.perennialCropCycles) {
      $scope.perennialCropCycles = [];
    }
    if (!$scope.seasonalCropCycles) {
      $scope.seasonalCropCycles = [];
    }

    angular.forEach($scope.croppingPlanModel, function(croppingPlanEntry) {
      $scope.croppingPlanEntriesByCodes[croppingPlanEntry.croppingPlanEntryCode] = croppingPlanEntry;
    });
    angular.forEach($scope.intermediateCroppingPlanModel, function(croppingPlanEntry) {
      $scope.croppingPlanEntriesByCodes[croppingPlanEntry.croppingPlanEntryCode] = croppingPlanEntry;
    });

    if (!$scope.growingSystemTopiaId && $scope.defaultGrowingSystemId) {
      $scope.growingSystemTopiaId = $scope.defaultGrowingSystemId;
    }

    $scope.getIntCampaigns = function(campaigns) {
      var intCampaigns = [];
      for (var i=0; i < campaigns.length; i++) {
        var campaign = campaigns[i];
        if (isNaN(campaign)) {
          break;
        }
        var intCampaign = parseInt(campaign);
        if ($scope.campaignsBounds.left <= intCampaign && intCampaign <= $scope.campaignsBounds.right) {
          intCampaigns.push(intCampaign);
        }
      }
      return intCampaigns;
    };

    $scope.initSeasonalCropTab = function() {
      angular.element(document).ready(
        function initSeasonalCropTab() {
          $scope.validateSolOccupationPercent();
        }
      );
    };

    $scope.initItkTab = function() {
      angular.element(document).ready(
        function initItkTab() {
          $('#tab_3').scope().initItkTab();
          $scope.validateSolOccupationPercent();
        }
      );
    };

    $scope.setDateOptionYearRange = function() {
      // noting to do for practiced systems
    };

    $scope.validateSolOccupationPercent = function() {
      if ($rootScope.perennialCropCyclesTabOpened && $scope.perennialCropCycles && $scope.perennialCropCycles.length > 0) {
        var result = 0;
        angular.forEach($scope.perennialCropCycles, function(cycle){
          if (cycle.practicedPerennialCropCycle && cycle.practicedPerennialCropCycle.solOccupationPercent) {
            result += parseFloat(cycle.practicedPerennialCropCycle.solOccupationPercent);
          }
        });
        $rootScope.perennialCropCyclesTabOpened = false;
        if (result != 100) {
          window.alert($scope.messages.solOccupationPercentError);
        }
      } else {
        $rootScope.perennialCropCyclesTabOpened = false;
      }
    };

    $scope._processWildCropChange = function(loadedData) {
      //$scope.lastValidCampaignSeries = $scope.practicedSystem.campaigns;

      $scope.practicedSystemGrowingSystemReplacement = loadedData ? loadedData.practicedSystemGrowingSystemReplacement : null;

      if ($scope.practicedSystemGrowingSystemReplacement) {
        // les campagnes sélectionnées ne sont pas valides car aucun système de culture existe pour ses campagne
        // on annule les changement
        $scope.areCampaignsPartOfGrowingSystemCampaigns = false;
        $scope.gsCampaigns = loadedData ? loadedData.growingSystemCampaigns : "";
        $scope.practicedSystem.campaigns = loadedData.campaigns;
        $scope.lockCampagnesChange = true;
        $scope.gsCampaignsValues = $scope.gsCampaigns.join(', ');
        if ($scope.practicedSystem.campaigns) {$(campaignsErrorMessage).fadeIn()};
        return;
      }
      $scope.areCampaignsPartOfGrowingSystemCampaigns = true;
      $(campaignsErrorMessage).fadeOut();

      $scope.croppingPlanModelLoading = true;
      $scope.croppingPlanModel = loadedData ? loadedData.practicedSystemMainCropCycleModels : [];
      $scope.intermediateCroppingPlanModel = loadedData ? loadedData.practicedSystemIntermediateCropCycleModels : [];
      $scope.croppingPlanEntrySpeciesIndex = loadedData ? loadedData.croppingPlanEntryCodesToSpecies : {};
      $scope.domainId = loadedData ? loadedData.domainCode : null;
      $scope.sector = loadedData ? loadedData.sector: null;
      $scope.isOrganic = loadedData ? loadedData.isOrganic : false;
      $scope.gsCampaigns = loadedData ? loadedData.growingSystemCampaigns : "";
      $scope.cattles = loadedData ? loadedData.cattles : "";

      if (!$scope.practicedSystem.campaigns) {
        $scope.practicedSystem.campaigns = $scope.gsCampaigns;
      }

      $scope.croppingPlanEntriesByCodes = $scope.getCroppingPlanByCodes($scope.croppingPlanModel);

      angular.forEach($scope.intermediateCroppingPlanModel, function(croppingPlanEntry) {
        $scope.croppingPlanEntriesByCodes[croppingPlanEntry.croppingPlanEntryCode] = croppingPlanEntry;
      });

      angular.element(document).ready(
        function initCropCycleDiagramDiv() {
          $("#cropCycleDiagramDiv").cropCycleDiagram("models", $scope.croppingPlanModel);
        }
      );
    };

    $scope._confirmCampaignOrGrowingSystemChange = function (loadedData, fromGrowingSystemId, removedCampaigns) {

      // model
      var onConfirmCampaignOrGrowingSystemChange = {
        loadedData: loadedData,
        fromGrowingSystemId: fromGrowingSystemId,

        process : function() {
          $scope.practicedSystemGrowingSystemReplacement = loadedData ? loadedData.practicedSystemGrowingSystemReplacement : null;
          if ($scope.practicedSystemGrowingSystemReplacement) {
             $scope.practicedSystem.growingSystemTopiaId = $scope.practicedSystemGrowingSystemReplacement;
             $scope.growingSystemTopiaId = $scope.practicedSystem.growingSystemTopiaId;
          }
          $scope._processWildCropChange(loadedData);
          var cropCodes = Object.keys($scope.croppingPlanEntriesByCodes);
          $scope._removeSeasonalNodesWithoutCrop(cropCodes);
          $scope._removePerennialCyclesWithoutCrop(cropCodes);
          $scope._migrateInputUsages(loadedData.domainInputsByCodes);

          $scope.lastValidCampaignSeries = $scope.practicedSystem.campaigns;
        },

        cancelChanges : function() {
          // roolback growing systeme
          if (fromGrowingSystemId) {
            $scope.growingSystemTopiaId = fromGrowingSystemId;
          } else {
            // rollback camaigns
            $scope.practicedSystem.campaigns = $scope.lastValidCampaignSeries;
          }
        }
      };

      // view
      var removedCampaignMessage = (removedCampaigns && removedCampaigns.length > 1 ?
          $scope.messages.removeCampaignConfirmSeveral : $scope.messages.removeCampaignConfirmOne) + ": ";
      var confirmTitle = removedCampaigns ? removedCampaignMessage + removedCampaigns.join(', ') : $scope.messages.croppingSystemChangeConfirmTitle;
      $scope.confirmContent = removedCampaigns ? $scope.messages.removeCampaignConfirmContentCampaign : $scope.messages.removeCampaignConfirmContentElements;

      _displayConfirmDialog($scope, $("#confirmRemovedElements"), 600, onConfirmCampaignOrGrowingSystemChange, confirmTitle);
    };

    $scope._removePerennialCyclesWithoutCrop = function(cropCodes) {
      var newPerennialCropCycles = null;
      if ($scope.perennialCropCycles) {
        newPerennialCropCycles = [];
        var perennialCropCycles = $scope.perennialCropCycles;
        for (var i = 0; i < perennialCropCycles.length; i++ ) {
          var perennialCropCycle = perennialCropCycles[i];
          var pperennialCropCycle = perennialCropCycle.practicedPerennialCropCycle;
          var cropCode = pperennialCropCycle.croppingPlanEntryCode;
          if (cropCodes.indexOf(cropCode) !== -1) {
            newPerennialCropCycles.push(perennialCropCycle);
          }
        }
      }
      $scope.perennialCropCycles = newPerennialCropCycles;
    };

    $scope._removeCropNodeFromCycle = function(cropCycleNode) {
      $("#cropCycleDiagramDiv").cropCycleDiagram("removeNode", cropCycleNode.nodeId);
    };

    $scope._removeCropNodesFromCycle = function(cropCycleNodes) {
      angular.forEach(cropCycleNodes, function(cropCycleNode) {
        $scope._removeCropNodeFromCycle(cropCycleNode);
      });
    };

    $scope._removeSeasonalNodesWithoutCrop = function(cropCodes) {
      var nodeToRemoved = [];
      if($scope.seasonalCropCycles) {
        var seasonalCropCycles = $scope.seasonalCropCycles;
        for (var j=0; j < seasonalCropCycles.length; j++) {
          var seasonalCropCycle = seasonalCropCycles[j];
          var cropCycleNodes = seasonalCropCycle.cropCycleNodeDtos;
          if (cropCycleNodes) {
            for (var k=0; k < cropCycleNodes.length; k++) {
              var cropCycleNode = cropCycleNodes[k];
              var cropCode =  cropCycleNode.croppingPlanEntryCode;
              if (cropCodes.indexOf(cropCode) === -1) {
                nodeToRemoved.push(cropCycleNode);
              }
            }
          }
        }
      }
      $scope._removeCropNodesFromCycle(nodeToRemoved);
    };

    $scope._migrateOtherProductInputUsageDtos = function(actionDto, domainInputsDtoByCodes) {
      if (actionDto.otherProductInputUsageDtos) {
        var validOtherProductInputUsageDtos = [];
        angular.forEach(actionDto.otherProductInputUsageDtos, function(otherProductInputUsageDto){
          var domainOtherProductInputCode =  otherProductInputUsageDto.domainOtherProductInputDto.code;
          if (domainInputsDtoByCodes[domainOtherProductInputCode]) {
            var  domainOtherProductInputDto = domainInputsDtoByCodes[domainOtherProductInputCode];
            otherProductInputUsageDto.domainOtherProductInputDto = domainOtherProductInputDto;
            validOtherProductInputUsageDtos.push(otherProductInputUsageDto);
          }
        });
        actionDto.otherProductInputUsageDtos = validOtherProductInputUsageDtos;
      }
    };

    $scope._migrateOrganicProductInputUsageDtos = function(actionDto, domainInputsDtoByCodes) {
      var validOrganicProductInputUsageDtos = [];
      angular.forEach(actionDto.organicProductInputUsageDtos, function(organicProductInputUsageDto){
        var domainOrganicProductInputCode =  organicProductInputUsageDto.domainOrganicProductInputDto.code;
        if (domainInputsDtoByCodes[domainOrganicProductInputCode]) {
          var  domainOrganicProductInputDto = domainInputsDtoByCodes[domainOrganicProductInputCode];
          organicProductInputUsageDto.domainOrganicProductInputDto = domainOrganicProductInputDto;
          validOrganicProductInputUsageDtos.push(organicProductInputUsageDto);
        }
      });
      actionDto.organicProductInputUsageDtos = validOrganicProductInputUsageDtos;
    };

    $scope._migrateIrrigationInputUsageDto = function(actionDto, domainInputsDtoByCodes) {
      var irrigationInputUsageDtoCode =  actionDto.irrigationInputUsageDto.domainIrrigationInputDto.code;
      if (domainInputsDtoByCodes[irrigationInputUsageDtoCode]) {
        var  domainIrrigationInputDto = domainInputsDtoByCodes[irrigationInputUsageDtoCode];
        actionDto.irrigationInputUsageDto.domainIrrigationInputDto = domainIrrigationInputDto;
      } else {
        actionDto.irrigationInputUsageDto = undefined;
      }
    };

    $scope._migrateMineralProductInputUsageDtos = function(actionDto, domainInputsDtoByCodes) {
      var validMineralProductInputUsageDtos = [];
      angular.forEach(actionDto.mineralProductInputUsageDtos, function(mineralProductInputUsageDto){
        var domainMineralProductInputCode =  mineralProductInputUsageDto.domainMineralProductInputDto.code;
        if (domainInputsDtoByCodes[domainMineralProductInputCode]) {
          var  domainMineralProductInputDto = domainInputsDtoByCodes[domainMineralProductInputCode];
          mineralProductInputUsageDto.domainMineralProductInputDto = domainMineralProductInputDto;
          validMineralProductInputUsageDtos.push(mineralProductInputUsageDto);
        }
      });
      actionDto.mineralProductInputUsageDtos = validMineralProductInputUsageDtos;
    };

    $scope._migrateSeedLotInputUsageDtos = function(actionDto, domainInputsDtoByCodes) {
      var validSeedLotInputUsageDtos = [];
      angular.forEach(actionDto.seedLotInputUsageDtos, function(seedLotInputUsageDto){
        var domainSeedLotInputDtoCode =  seedLotInputUsageDto.domainSeedLotInputDto.code;
        if (domainInputsDtoByCodes[domainSeedLotInputDtoCode]) {
          var  domainSeedLotInputDto = domainInputsDtoByCodes[domainSeedLotInputDtoCode];
          seedLotInputUsageDto.domainSeedLotInputDto = domainSeedLotInputDto;
          validSeedLotInputUsageDtos.push(seedLotInputUsageDto);

          if (seedLotInputUsageDto.seedingSpeciesDtos) {
            var speciesInputsDtoByCodes = {};
            if(domainSeedLotInputDto.speciesInputs) {
              angular.forEach(domainSeedLotInputDto.speciesInputs, function(domainSeedSpeciesInput){
                speciesInputsDtoByCodes[domainSeedSpeciesInput.code] = domainSeedSpeciesInput;
              });
            }
            var validSeedingSpeciesDtos = [];
            angular.forEach(seedLotInputUsageDto.seedingSpeciesDtos, function(seedingSpeciesDto){
              var domainSeedSpeciesInputDtoCode =  seedingSpeciesDto.domainSeedSpeciesInputDto.code;

              if (speciesInputsDtoByCodes[domainSeedSpeciesInputDtoCode]) {
                var  domainSeedSpeciesInputDto = speciesInputsDtoByCodes[domainSeedSpeciesInputDtoCode];
                seedingSpeciesDto.domainSeedSpeciesInputDto = domainSeedSpeciesInputDto;
                validSeedingSpeciesDtos.push(seedingSpeciesDto);

                if (seedingSpeciesDto.seedProductInputDtos && seedingSpeciesDto.seedProductInputDtos.length > 0) {

                  var productSpeciesInputsDtoByCodes = {};
                  if(domainSeedSpeciesInputDto.speciesPhytoInputDtos && domainSeedSpeciesInputDto.speciesPhytoInputDtos.length > 0) {
                    angular.forEach(domainSeedSpeciesInputDto.speciesPhytoInputDtos, function(speciesPhytoInputDto){
                      productSpeciesInputsDtoByCodes[speciesPhytoInputDto.code] = speciesPhytoInputDto;
                    });
                  }

                  var validPhytoProductInputUsageDtos = [];
                  angular.forEach(seedingSpeciesDto.seedProductInputDtos, function(phytoProductInputUsageDto){
                    var domainPhytoProductInputCode =  phytoProductInputUsageDto.domainPhytoProductInputDto.code;
                    if (productSpeciesInputsDtoByCodes[domainPhytoProductInputCode]) {
                      var  domainPhytoProductInputDto = productSpeciesInputsDtoByCodes[domainPhytoProductInputCode];
                      phytoProductInputUsageDto.domainPhytoProductInputDto = domainPhytoProductInputDto;
                      validPhytoProductInputUsageDtos.push(phytoProductInputUsageDto);
                    }
                  });
                  seedingSpeciesDto.seedProductInputDtos = validPhytoProductInputUsageDtos;
                }
              }
            });
            actionDto.seedingSpeciesDtos = validSeedingSpeciesDtos;
          }
        }
      });
      actionDto.seedLotInputUsageDtos = validSeedLotInputUsageDtos;
    };

    $scope._migratePhytoProductInputUsageDtos = function(actionDto, domainInputsDtoByCodes) {
      var validPhytoProductInputUsageDtos = [];
      angular.forEach(actionDto.phytoProductInputUsageDtos, function(phytoProductInputUsageDto){
        var domainPhytoProductInputCode =  phytoProductInputUsageDto.domainPhytoProductInputDto.code;
        if (domainInputsDtoByCodes[domainPhytoProductInputCode]) {
          var  domainPhytoProductInputDto = domainInputsDtoByCodes[domainPhytoProductInputCode];
          phytoProductInputUsageDto.domainPhytoProductInputDto = domainPhytoProductInputDto;
          validPhytoProductInputUsageDtos.push(phytoProductInputUsageDto);
        }
      });
      actionDto.phytoProductInputUsageDtos = validPhytoProductInputUsageDtos;
    };

    $scope._migrateInputUsagesFromIntervention = function(intervention, domainInputsDtoByCodes) {
      if (intervention.actionDtos && intervention.actionDtos.length > 0) {

        angular.forEach(intervention.actionDtos, function(actionDto){

          $scope._migrateOtherProductInputUsageDtos(actionDto, domainInputsDtoByCodes);

          if (actionDto.organicProductInputUsageDtos) {
            $scope._migrateOrganicProductInputUsageDtos(actionDto, domainInputsDtoByCodes);
          } else if (actionDto.irrigationInputUsageDto) {
            $scope._migrateIrrigationInputUsageDto(actionDto, domainInputsDtoByCodes);
          } else if (actionDto.mineralProductInputUsageDtos) {
            $scope._migrateMineralProductInputUsageDtos(actionDto, domainInputsDtoByCodes);
          } else if (actionDto.seedLotInputUsageDtos) {
            $scope._migrateSeedLotInputUsageDtos(actionDto, domainInputsDtoByCodes);
          } else if (actionDto.phytoProductInputUsageDtos) {
            $scope._migratePhytoProductInputUsageDtos(actionDto, domainInputsDtoByCodes);
          }

        });
      }
    };

    $scope._migrateInputUsages = function(domainInputsDtoByCodes){
      if($scope.seasonalCropCycles) {
        var seasonalCropCycles = $scope.seasonalCropCycles;
        for (var j=0; j < seasonalCropCycles.length; j++) {
          var seasonalCropCycle = seasonalCropCycles[j];
          var cropCycleConnectionDtos = seasonalCropCycle.cropCycleConnectionDtos;
          if (cropCycleConnectionDtos && cropCycleConnectionDtos.length > 0) {
            angular.forEach(cropCycleConnectionDtos, function(cropCycleConnectionDto){
              if (cropCycleConnectionDto.interventions && cropCycleConnectionDto.interventions.length > 0) {
                angular.forEach(cropCycleConnectionDto.interventions, function(intervention){
                  $scope._migrateInputUsagesFromIntervention(intervention, domainInputsDtoByCodes);
                });
              }
            });
          }
        }
      }
      if ($scope.perennialCropCycles) {
        var perennialCropCycles = $scope.perennialCropCycles;
        for (var i = 0; i < perennialCropCycles.length; i++ ) {
          var perennialCropCycle = perennialCropCycles[i];
          var phaseDtos = perennialCropCycle.cropCyclePhaseDtos;
          if (phaseDtos && phaseDtos.length > 0) {
            angular.forEach(phaseDtos, function(phaseDto){
              if (phaseDto.interventions && phaseDto.interventions.length > 0) {
                angular.forEach(phaseDto.interventions, function(intervention){
                  $scope._migrateInputUsagesFromIntervention(intervention, domainInputsDtoByCodes);
                });
              }
            });
          }
        }
      }
    };

    $scope.getCroppingPlanByCodes = function(cropModel) {
      var croppingPlanEntriesByCodes = [];
      angular.forEach(cropModel,  function(croppingPlanEntry) {
        croppingPlanEntriesByCodes[croppingPlanEntry.croppingPlanEntryCode] = croppingPlanEntry;
      });
      return croppingPlanEntriesByCodes;
    };

    // make xhr call to refresh cropping plan list and tools coupling list
    $scope._updatePracticedSystemWideData = function(fromGrowingSystemId, removedCampaigns) {
      if ($scope.frontApp === 'agrosyst') {
        if ($scope.growingSystemTopiaId && $scope.practicedSystem.campaigns) {
          $http.post(
              ENDPOINT_PRACTICED_SYSTEMS_CROPS_TOOLS_JSON, "growingSystemId=" + $scope.growingSystemTopiaId + "&campaigns=" + $scope.practicedSystem.campaigns,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
          ).then(function(response) {
            if (response.data.areValidCampaigns) {

              if (fromGrowingSystemId || (removedCampaigns && removedCampaigns.length > 0)) {
                $scope._confirmCampaignOrGrowingSystemChange(response.data, fromGrowingSystemId, removedCampaigns);
              } else {
                $scope._processWildCropChange(response.data);
              }

            }
          }).
          catch(function(response) {
            var message = $scope.messages.practicedSystemLoadingFailed;
            console.error(message, response);
            addPermanentError(message, response.status);
          });
          return $q.defer().promise;
        } else {
          if (fromGrowingSystemId || (removedCampaigns && removedCampaigns.length > 0)) {
            $scope._confirmCampaignOrGrowingSystemChange(null, fromGrowingSystemId, removedCampaigns);
          } else {
            $scope._processWildCropChange(null);
          }
          return $q.defer().promise;
        }
      } else {
        return $q.defer().promise;
      }

    };

    $scope.refreshGrowingSystems = function(pattern) {
      // La librairie ui-select ne permet pas de mettre de place holder directement sur le input de recherche donc on l'injecte en JS
      $('#growingSystemTopiaId .ui-select-dropdown input.ui-select-search').attr('placeholder', 'Entrez le nom de votre système de culture s\'il n\'apparait pas dans la liste');
      return $http.get(ENDPOINT_LOAD_GROWING_SYSTEM_COMPLETE_JSON + "?term=" + encodeURIComponent(pattern))
          .then(function(response) {
            $scope.growingSystems = response.data;
          })
          .catch(function(response) {
              var message = "Échec de récupération des systèmes de cultures";
              console.error(message, response);
              addPermanentError(message, response.status);
          });
    };

    $scope.selectedGrowingSystem = null;


    $scope.changeGrowingSystem = function(growingSystem) {

      $scope.croppingPlanModelLoading = true;
      $scope.growingSystemTopiaId = growingSystem.topiaId;

      if (!$scope.practicedSystem.campaigns) {
        $scope.practicedSystem.campaigns  = growingSystem.growingPlan.domain.campaign;
        $scope.domainCampaign = growingSystem.growingPlan.domain.campaign;
      }

      var oldValue = $scope.practicedSystem.growingSystemTopiaId;
      $scope.practicedSystem.growingSystemTopiaId = $scope.growingSystemTopiaId;
      $scope._updatePracticedSystemWideData(oldValue).then(
          function() {
            $scope.croppingPlanModelLoading = false;
          }
        );
    };

    $scope.displayCampaignsErrorWarning = function() {
      $(campaignsErrorMessage).fadeOut();
    };

    $scope.$watch('practicedSystem.campaigns',
      function(newValue, oldValue) {
        // newValue peut-être undefined dans le cas ou le pattern de validation n'est pas validé
        // dans ce cas la valeur est récupéré depuis l'input directement autrement
        // Il serait considéré que toutes les campagnes ont été retirées alors qu'en fait
        // il s'agit d'une campagne qui est en train d'être modifiée et qui n'est pas encore valide ou retirée
        if ($scope.lockCampagnesChange == true) {
          $scope.lockCampagnesChange = false;
          return;
        }
        if (!newValue && $(practicedSystem_campaigns)[0] && $(practicedSystem_campaigns)[0].value) {//
          newValue = $(practicedSystem_campaigns)[0].value;
        }
        if (newValue != $scope.lastValidCampaignSeries) {
          $scope.croppingPlanModelLoading = true;
          var campaigns;
          var removedCampaigns;
          if(newValue) {
            let intCampaigns;
            if (isNaN(newValue)) {
              campaigns = newValue.replace(/\D+/g, ' ').split(' ');
              if (campaigns && campaigns.length > 0) {
                intCampaigns = $scope.getIntCampaigns(campaigns);
              }
            } else {
              intCampaigns = [];
              intCampaigns.push(newValue);
              campaigns = newValue + "";
            }

            // do update only if all campaigns are valides
            if (intCampaigns.length === campaigns.length) {
              var previousCampaigns = $scope.lastValidCampaignSeries ?
                $scope.lastValidCampaignSeries.replace(/\D+/g, ' ').split(' ') : [];
              removedCampaigns = [];
              angular.forEach(previousCampaigns, function(previousCampaign){
                if (intCampaigns.indexOf(parseInt(previousCampaign)) === -1) {
                  removedCampaigns.push(previousCampaign);
                }
              });

              $scope._updatePracticedSystemWideData(null, removedCampaigns).then(
                function() {
                  $scope.croppingPlanModelLoading = false;
                  }
                );
            }

          } else if (oldValue){
            campaigns = oldValue.replace(/\D+/g, ' ').split(' ');
            if (campaigns && campaigns.length > 0) {
              removedCampaigns = [];
              angular.forEach(campaigns, function(previousCampaign){
                removedCampaigns.push(parseInt(previousCampaign));
              });
              $scope._updatePracticedSystemWideData(null, removedCampaigns).then(
                function() {
                    $scope.croppingPlanModelLoading = false;
                  }
                );
            }
          }
        }
    });

    $scope.getItkIdentifierQuery = function() {
      return "growingSystemId=" + $scope.growingSystemTopiaId + "&campaigns=" + $scope.practicedSystem.campaigns;
    };

    $scope._getSpeciesId = function(species) {
      return species.speciesId;
    };

}]);
