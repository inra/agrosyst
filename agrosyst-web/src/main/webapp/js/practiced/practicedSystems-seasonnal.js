/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Onglet 'Cycle de culture assolées'
 */
AgrosystModule.controller('PracticedSeasonalCropCycleNodeController', ['$scope', 'PracticedSystemInitData',
  function($scope, PracticedSystemInitData) {
    // {Object} le noeud sélectionné et en cours d'edition
    //$scope.selectedCropCycleNode;
    // {Object} la connexion selectionnée et en cours d'edition
    //$scope.selectedCropCycleConnection;
    // {Object} la culture intermediaire sélectionnée dans le 'select' (si selectedCropCycleConnection != null)
    //$scope.selectedIntermediateCroppingPlanEntry;
    // {Object} le code de la culture principale (noeud) ou secondaire sélectionnée (select)
    //scope.selectedCroppingPlanEntryCode;
    // status to know if intermediate crop has bean selected
    //$scope.isSelectedIntermediateCroppingPlanEntry;

    // initialise le diagrams js (jquery ready)
    $scope.initChartJs = function() {
      if ($scope.seasonalCropCycles.length > 0) {
        var firstCycle = $scope.seasonalCropCycles[0];
        if ($scope.croppingPlanModel) {
          $("#cropCycleDiagramDiv").cropCycleDiagram("models", $scope.croppingPlanModel);
        }

        let cropCycleNodeDtos = firstCycle.cropCycleNodeDtos;
        if (cropCycleNodeDtos) {
          cropCycleNodeDtos.forEach(node => {
            let model = $scope.croppingPlanModel.find(m => m.croppingPlanEntryCode === node.croppingPlanEntryCode);
            if (model) {
              node.mixSpecies = model.mixSpecies;
              node.mixVariety = model.mixVariety;
              node.mixCompanion = model.mixCompanion;
              node.catchCrop = model.catchCrop;
            }

            node.mixVarietiesAbbreviation = $scope.messages.mixVarietiesAbbreviation;
            node.mixSpeciesAbbreviation = $scope.messages.mixSpeciesAbbreviation;
            node.mixCompanionAbbreviation = $scope.messages.mixCompanionAbbreviation;
            node.catchCropAbbreviation = $scope.messages.catchCropAbbreviation;
          })
        }

        $("#cropCycleDiagramDiv").cropCycleDiagram("data", {
          "nodes": cropCycleNodeDtos,
          "connections": firstCycle.cropCycleConnectionDtos
        });

      } else {
        $("#cropCycleDiagramDiv").cropCycleDiagram("models", $scope.croppingPlanModel);
        $("#cropCycleDiagramDiv").cropCycleDiagram('redraw');
      }
    };

    // creation d'un nouveau diagram
    $scope.createPracticedSeasonalCropCycle = function() {
      if ($scope.croppingPlanModel && $scope.croppingPlanModel.length > 0) {
        $scope.seasonalCropCycles.push({
          cropCycleNodeDtos: [],
          cropCycleConnectionDtos: []
        });
        $("#cropCycleDiagramDiv").cropCycleDiagram("models", $scope.croppingPlanModel);
        $("#cropCycleDiagramDiv").cropCycleDiagram('redraw');
      }
    };

    // util method, reset all
    var clearAll = function() {
      delete $scope.selectedCropCycleNode;
      delete $scope.selectedCropCycleNodeSpecies;
      delete $scope.selectedCropCycleConnection;
      //delete $scope.selectedIntermediateCroppingPlanEntry;
      delete $scope.selectedCroppingPlanEntryCode;
    };

    $scope.deletePracticedSeasonalCropCycleDto = function () {
      // model
      var onConfirmRemovedSeasonalCycle = {
        process : function() {
          // the last one is the current edited
          var lastCycleIndex = $scope.seasonalCropCycles.length - 1;
          var cycle = $scope.seasonalCropCycles[lastCycleIndex];

          angular.forEach(cycle.cropCycleConnectionDtos, function(connections) {
            $scope.deleteCropCycleConnection(connections);
          });

          angular.forEach(cycle.cropCycleNodeDtos, function(node) {
            $scope.deleteCropCycleNode(node);
          });

          $scope.seasonalCropCycles.splice(lastCycleIndex, 1);
          clearAll();
        },
        cancelChanges : function() {}
      };
      // view
      _displayConfirmDialog($scope, $("#confirmRemovedSeasonalCycle"), 400, onConfirmRemovedSeasonalCycle);
    };

    $scope.initSeasonalCropCycleTab = function () {
      clearAll();
    };

    // node selected in chart (called by jquery)
    $scope.setSelectedNode = function(selectedCropCycleNode) {
      clearAll();
      $scope.selectedCropCycleNode = selectedCropCycleNode;

      $scope.selectedCroppingPlanEntryCode = selectedCropCycleNode.croppingPlanEntryCode;

      $scope.$apply(); // force le refresh
    };

    // update chart on node data change
    $scope.updateNode = function() {
      $("#cropCycleDiagramDiv").cropCycleDiagram(
          "updateNode",
          $scope.selectedCropCycleNode.nodeId,
          $scope.selectedCropCycleNode.endCycle,
          $scope.selectedCropCycleNode.sameCampaignAsPreviousNode);
    };

    // delete node
    $scope.deleteCropCycleNode = function(cropCycleNode, lookForImpact) {
      // look for impact
      var seasonalCropCycle = $scope.seasonalCropCycles[0];
      var connectionSource;

      // action to do
      var deleteCropCycleNodeModel = {
        process : function() {
          $("#cropCycleDiagramDiv").cropCycleDiagram("removeNode", cropCycleNode.nodeId);
          clearAll();

          delete $scope.seasonalCropChangeContext;
        },
        cancelChanges : function() {
          delete $scope.seasonalCropChangeContext;
        }
      };

      if (lookForImpact) {
        angular.forEach(seasonalCropCycle.cropCycleConnectionDtos, function(cropCycleConnectionDto){
          if (cropCycleConnectionDto.targetId === cropCycleNode.nodeId) {
            connectionSource = cropCycleConnectionDto;
          }
        });
        var warningMessages = _removeConnectionImpact(connectionSource);
        if (warningMessages) {
          $scope.seasonalCropChangeContext = {warningMessages : warningMessages, cropCycleNode : cropCycleNode};
          _displayConfirmDialog($scope, $("#confirmSeasonalCropChange"), 1024, deleteCropCycleNodeModel);
        } else {
          deleteCropCycleNodeModel.process();
          clearAll();
        }

      } else {
        deleteCropCycleNodeModel.process();
        clearAll();
      }
    };

    // new connection selected on chart (called by jquery)
    $scope.setSelectedConnection = function(selectedCropCycleConnection) {
      if (selectedCropCycleConnection) {
        clearAll();
        $scope.selectedCropCycleConnection = selectedCropCycleConnection;

        if (angular.isDefined($scope.selectedCropCycleConnection.intermediateCroppingPlanEntryCode)) {
            $scope.selectedCroppingPlanEntryCode = $scope.selectedCropCycleConnection.intermediateCroppingPlanEntryCode;
            // search in intermediateCroppingPlanModel collection, item with cropping plan entry code
            angular.forEach($scope.intermediateCroppingPlanModel, function(practicedSystemIntermediateCropCycleModel) {
              if (practicedSystemIntermediateCropCycleModel.croppingPlanEntryCode === $scope.selectedCroppingPlanEntryCode) {
                $scope.selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry = practicedSystemIntermediateCropCycleModel;
              }
            });
        }
        $scope.$apply(); // force le refresh

        if ($scope.seasonalCropCycles[0] && $scope.seasonalCropCycles[0].cropCycleConnectionDtos) {
          let otherFreq = $scope.seasonalCropCycles[0].cropCycleConnectionDtos
          .filter(c => c.targetId !== selectedCropCycleConnection.targetId)
          .filter(c => c.sourceId == selectedCropCycleConnection.sourceId)
          .filter(c => c.croppingPlanEntryFrequency)
          .map(c => c.croppingPlanEntryFrequency)
          .reduce((a, b) => a+b, 0);
          let totalFreq = otherFreq + (selectedCropCycleConnection.croppingPlanEntryFrequency ? selectedCropCycleConnection.croppingPlanEntryFrequency : 0);

          if (totalFreq == 100) {
            const autoFreqAdjust = document.getElementById("autoFreqAdjust");
            if (autoFreqAdjust) {
              autoFreqAdjust.classList.add("fakeButtonDisabled");
            }
          }
        }
      }
    };

    $scope._changeDiagramConnexionInfo = function(cropCycleConnection, croppingPlanEntryCode, cropCycleConnectionLabel) {
      if (croppingPlanEntryCode && angular.isDefined(cropCycleConnectionLabel)) {
          cropCycleConnection.intermediateCroppingPlanEntryCode = croppingPlanEntryCode;
          //cropCycleConnection.label = "<b>CI</b><span class='hover-infos'>" + cropCycleConnectionLabel + "</span>";
          //cropCycleConnection.label = angular.isDefined(cropCycleConnection.croppingPlanEntryFrequency) ? cropCycleConnection.croppingPlanEntryFrequency : "";
          $scope.selectedCroppingPlanEntryCode = croppingPlanEntryCode;
      } else {
          //cropCycleConnection.label = angular.isDefined(cropCycleConnection.croppingPlanEntryFrequency) ? cropCycleConnection.croppingPlanEntryFrequency : "";
          delete cropCycleConnection.intermediateCroppingPlanEntryCode;
          delete $scope.selectedCroppingPlanEntryCode;
      }

      $("#cropCycleDiagramDiv").cropCycleDiagram("setConnectionData", cropCycleConnection.sourceId, cropCycleConnection.targetId, cropCycleConnection);
    };

    // update chart when connection data changes
    $scope.updateConnection = function(cropCycleConnection, croppingPlanItem, autoFreqCompute) {
      if (autoFreqCompute && $scope.seasonalCropCycles[0] && $scope.seasonalCropCycles[0].cropCycleConnectionDtos) {
        let otherFreq = $scope.seasonalCropCycles[0].cropCycleConnectionDtos
        .filter(c => c.targetId !== cropCycleConnection.targetId)
        .filter(c => c.sourceId == cropCycleConnection.sourceId)
        .filter(c => c.croppingPlanEntryFrequency)
        .map(c => c.croppingPlanEntryFrequency)
        .reduce((a, b) => a+b, 0);
        let currentFreq = 100 - otherFreq;
        cropCycleConnection.croppingPlanEntryFrequency = currentFreq;
      }
      var croppingPlanEntryCode;
      var cropCycleConnectionLabel;
      if (croppingPlanItem) {
        croppingPlanEntryCode = croppingPlanItem.croppingPlanEntryCode;
        cropCycleConnectionLabel = croppingPlanItem.label;
      }
      $scope._changeDiagramConnexionInfo(cropCycleConnection, croppingPlanEntryCode, cropCycleConnectionLabel);
    };

    $scope._doUpdateConnection = function (cropCycleConnection, intermediateCrop, matchingSpeciesByCode) {
      if (cropCycleConnection){

        var isFromIntermediateCrop = angular.isDefined(cropCycleConnection.intermediateCroppingPlanEntryCode);

        // update intermediate crop code
        cropCycleConnection.intermediateCroppingPlanEntryCode = $scope._getConnectionIntermediateCroppingPlanEntryCode(intermediateCrop);
        var isToIntermediateCrop = angular.isDefined(cropCycleConnection.intermediateCroppingPlanEntryCode);
        _doConnectionIntermediateCropChange(cropCycleConnection, isFromIntermediateCrop, isToIntermediateCrop, matchingSpeciesByCode);
      }

      var croppingPlanEntryCode;
      var cropCycleConnectionLabel;
      if (cropCycleConnection.selectedIntermediateCroppingPlanEntry) {
        croppingPlanEntryCode = cropCycleConnection.selectedIntermediateCroppingPlanEntry.croppingPlanEntryCode;
        cropCycleConnectionLabel = cropCycleConnection.selectedIntermediateCroppingPlanEntry.label;
      }

      $scope._changeDiagramConnexionInfo(cropCycleConnection, croppingPlanEntryCode, cropCycleConnectionLabel);
    };

    $scope._getConnectionIntermediateCroppingPlanEntryCode = function(intermediateCrop) {
      var intermediateCroppingPlanEntryCode;
      if (intermediateCrop && intermediateCrop.croppingPlanEntryCode) {
        intermediateCroppingPlanEntryCode = intermediateCrop.croppingPlanEntryCode;
      }
      return intermediateCroppingPlanEntryCode;
    };

    $scope._validUpdateConnection = function(cropCycleConnection, intermediateCrop, matchingSpeciesByCode) {
      var warningMessages;
      if (cropCycleConnection){
        var isFromIntermediateCrop = cropCycleConnection.intermediateCroppingPlanEntryCode;
        var isToIntermediateCrop = intermediateCrop && intermediateCrop.croppingPlanEntryCode;
        warningMessages = _getConnectionIntermediateCropChangeImpact(cropCycleConnection, isFromIntermediateCrop, isToIntermediateCrop, matchingSpeciesByCode);
      }
      return warningMessages;
    };

    $scope.setSelectedIntermediateCroppingPlanEntryChange = function() {
      $scope.isSelectedIntermediateCroppingPlanEntry = true;
    };

    // critere de correspondance des especes
    var isMatchingSpeciesForCopyPaste = function(species1, species2) {
      return species1.code_espece_botanique == species2.code_espece_botanique;
    };

    // retourne une map de correspondance entre les anciens code especes et les nouvelles especes
    window._findMatchingSpecies = function(fromSpecies, toSpecies) {
      var matchingSpeciesByCode = {};

      var toSpeciesCopy = angular.copy(toSpecies); // to delete them
      angular.forEach(fromSpecies, function(fSpecies) {

        var matchingSpecies = null;

        for(var i = 0; i < toSpeciesCopy.length; i++){
          var tSpecies = toSpeciesCopy[i];
          if (isMatchingSpeciesForCopyPaste(fSpecies, tSpecies)) {
            matchingSpecies = tSpecies;
            break;
          }
        }

//        not supported by IE11
//        var matchingSpecies = toSpeciesCopy.find(function(tSpecies) {
//          return isMatchingSpeciesForCopyPaste(fSpecies, tSpecies);
//        });
        if (matchingSpecies) {
          matchingSpeciesByCode[fSpecies.code] = matchingSpecies;
          // and remove it to not match twice
          var indexOf = toSpeciesCopy.indexOf(matchingSpecies);
          toSpeciesCopy.splice(indexOf, 1);
        }
      });

      return matchingSpeciesByCode;
    };

    $scope._confirmSelectedIntermediateCroppingPlanEntryChange = function(connection, newCroppingPlanEntry, previousCroppingPlanEntry) {
      var previousCroppingPlanEntrySpecies = previousCroppingPlanEntry ? $scope.croppingPlanEntrySpeciesIndex[previousCroppingPlanEntry.croppingPlanEntryCode] : [];
      var newCroppingPlanEntrySpecies = newCroppingPlanEntry ?  $scope.croppingPlanEntrySpeciesIndex[newCroppingPlanEntry.croppingPlanEntryCode] : [];

      var previousCropCode = previousCroppingPlanEntry ? previousCroppingPlanEntry.croppingPlanEntryCode : null;
      var newCropCode = newCroppingPlanEntry ? newCroppingPlanEntry.croppingPlanEntryCode : null;
      var matchingSpeciesByCode = _findMatchingSpecies(previousCroppingPlanEntrySpecies, newCroppingPlanEntrySpecies);

      $scope.seasonalCropChangeContext = {warningMessages : $scope._validUpdateConnection(connection, newCroppingPlanEntry, matchingSpeciesByCode)};

      var intermediateCroppingPlanEntryChangeModel = {
        newCroppingPlanEntry : newCroppingPlanEntry,
        previousCroppingPlanEntry : previousCroppingPlanEntry,
        next: function() {
          delete $scope.seasonalCropChangeContext;
          delete $scope.isSelectedIntermediateCroppingPlanEntry;
        },
        process: function() {
          $scope._doUpdateConnection(connection, newCroppingPlanEntry, matchingSpeciesByCode);
          // force to use same angular object
          connection.selectedIntermediateCroppingPlanEntry = newCroppingPlanEntry;
          this.next();
        },
        cancelChanges: function() {
          connection.selectedIntermediateCroppingPlanEntry = previousCroppingPlanEntry;
          this.next();
        }
      };

      if ($scope.seasonalCropChangeContext.warningMessages) {
        _displayConfirmDialog($scope, $("#confirmSeasonalCropChange"), 1024, intermediateCroppingPlanEntryChangeModel);
      } else {
        intermediateCroppingPlanEntryChangeModel.process();
      }
    };

    $scope.$watch("selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry", function(newCroppingPlanEntry, previousCroppingPlanEntry) {
      if ($scope.isSelectedIntermediateCroppingPlanEntry) {
        if (newCroppingPlanEntry !== previousCroppingPlanEntry) {
          $scope._confirmSelectedIntermediateCroppingPlanEntryChange($scope.selectedCropCycleConnection, newCroppingPlanEntry, previousCroppingPlanEntry);
        }
      }
    });

    $scope._getNodeFromNodeId = function(researchedNodeId) {
      var result;
      if ($scope.seasonalCropCycles && researchedNodeId) {
        angular.forEach($scope.seasonalCropCycles,function(seasonalCropCycle) {
          var nodes = seasonalCropCycle.cropCycleNodeDtos;
          if (nodes && !result) {
            angular.forEach(nodes, function(node){
              if (!result && (node.nodeId === researchedNodeId)) {
                result = node;
              }
            });
          }
        });
      }
      return result;
    };

    // ATTENTION : déclarée sur la window, utilisée en global
    window._getCroppingPlanEntrySpeciesFromCropCycleConnection = function(cropCycleConnection) {
        var croppingPlanEntrySpecies = [];
        if (cropCycleConnection){
          if(cropCycleConnection.intermediateCroppingPlanEntryCode){
           // create species stades for intermediate crops
           croppingPlanEntrySpecies = $scope.croppingPlanEntrySpeciesIndex[cropCycleConnection.intermediateCroppingPlanEntryCode];
          } else {
           var nodeId = cropCycleConnection.targetId;
           var node = $scope._getNodeFromNodeId(nodeId);
           if (node && node.croppingPlanEntryCode) {
            croppingPlanEntrySpecies = $scope.croppingPlanEntrySpeciesIndex[node.croppingPlanEntryCode];
           }
          }
      }

      return croppingPlanEntrySpecies;
    };

    // ATTENTION : déclarée sur la window, utilisée en global
    window._getCropCycleConnectionInterventions = function(cropCycleConnection) {
      var result = {affected : [], nonAffected : []};

      if (cropCycleConnection){
        if (cropCycleConnection.interventions) {
          var interventions = cropCycleConnection.interventions;
          angular.forEach(interventions, function(intervention){
            if (intervention.intermediateCrop) {
              result.affected.push(intervention);
            } else {
              result.nonAffected.push(intervention);
            }
          });
        }
      }
      return result;
    };

    // ATTENTION : déclarée sur la window, utilisée en global
      window._getInterventionsRelatedToConnection = function(cropCycleConnection) {
      var interventions;
      if (cropCycleConnection) {
        interventions = cropCycleConnection.interventions;
      }
      return interventions;
    };

    // delete current selected connection (not called back by jquery)
    $scope.deleteCropCycleConnection = function(cropCycleConnection, confirmIt) {
      var deleteCropCycleConnectionModel = {
        process : function() {
          // update model
          var firstCycle = $scope.seasonalCropCycles[0];
          var indexOf = firstCycle.cropCycleConnectionDtos.indexOf(cropCycleConnection);
          firstCycle.cropCycleConnectionDtos.splice(indexOf, 1);
          // fire jquery
          $("#cropCycleDiagramDiv").cropCycleDiagram("removeConnection", cropCycleConnection.sourceId, cropCycleConnection.targetId);
          clearAll();
          delete $scope.seasonalCropChangeContext;
        },
        cancelChanges : function() {
          delete $scope.seasonalCropChangeContext;
        }
      };

      var warningMessages = _removeConnectionImpact(cropCycleConnection);
      if (warningMessages && confirmIt) {
        $scope.seasonalCropChangeContext = {warningMessages : warningMessages, cropCycleConnection:cropCycleConnection};
        _displayConfirmDialog($scope, $("#confirmSeasonalCropChange"), 1024, deleteCropCycleConnectionModel);
      } else {
        deleteCropCycleConnectionModel.process();
      }
    };

    // called by jquery when user modify charjs (new nodes, new connections...)
    $scope.setData = function(data) {
      if ($scope.seasonalCropCycles.length > 0) {
        var firstCycle = $scope.seasonalCropCycles[0];
        firstCycle.cropCycleNodeDtos = data.nodes;
        firstCycle.cropCycleConnectionDtos = data.connections;
        $scope.$apply();
        $scope.practicedSystemForm.$setDirty();
      } else {
        $scope.seasonalCropCycles.push({
          cropCycleNodeDtos: [],
          cropCycleConnectionDtos: []
        });
      }
    };

    $scope.toggleGraphInfo = function (hide) {
      if (hide) {
        $('.connection-label > span').show();
        $('#graphInfosShow').hide();
        $('#graphInfosHide').show();
      } else {
        $('.connection-label > span').hide();
        $('#graphInfosHide').hide();
        $('#graphInfosShow').show();
      }
    };
  }]
);
