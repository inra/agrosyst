/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


function practicedSystemsExport(exportForm) {
    var idsCount = JSON.parse(exportForm.children("input[name='practicedSystemIds']").val()).length;
    if (idsCount >= 1) {
      exportForm.attr("action", ENDPOINTS.practicedSystemsExport);
      exportForm.submit();
    }
}

/**
 * Affichage de la liste des systèmes synthétisés.
 */
AgrosystModule.controller('PracticedSystemsListController', ['$scope', '$http', '$timeout', '$filter', 'PracticedSystemsInitData',
'practicedSystemFilter', 'practicedSystemsExportAsyncThreshold',
  function($scope, $http, $timeout, $filter, PracticedSystemsInitData, practicedSystemFilter, practicedSystemsExportAsyncThreshold) {
    // {Array} elements list
    $scope.practicedSystems = PracticedSystemsInitData.elements;
    // {Object} list pager
    $scope.pager = PracticedSystemsInitData;
    // {Object} User input filter
    $scope.filter = practicedSystemFilter;

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0,1);
    }

    //
    $scope.practicedSystemDuplicateObject = {};

    $scope.asyncThreshold = practicedSystemsExportAsyncThreshold;

    $scope.selectedEntities = {};

    $scope.allSelectedEntities = [];

    $scope.duplicationErrorMessageContainer = {};

    $scope.displayDuplicationErrorIfPresent = function(duplicationErrorMessage) {

      hidePageLoading();

      $scope.duplicationErrorMessageContainer.duplicationErrorMessage = duplicationErrorMessage;

      $("#failedDuplicatePracticedSystem").dialog({
        resizable: false,
        width: '80%',
        modal: true,
        position: { my: "center top", at: "top+50" },
        buttons: {
          cancel: {
            click: function() {
              $(this).dialog("close");
              $scope.$apply();
            },
            text: 'Fermer',
            'class': 'btn-primary'
          }
        }
      });
    };

    $scope.toggleSelectedEntities = function() {

      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.practicedSystems);

    };

    $scope.toggleSelectedEntity = function(entityId) {

      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);

    };

    $scope.clearSelection = function() {
          $scope.selectedEntities = {};
          $scope.allSelectedEntities = [];
    };

    // growingSystem for the practiced system to duplicate
    //$scope.practicedSystemDuplicateObject.growingSystemIdForDuplication;
    // practiced System Id to Duplicate
    //$scope.practicedSystemIdToDuplicate;

    //XXX ymartel 2014/02/18 should be convert Boolean to String for binding until resolution of https://github.com/angular/angular.js/issues/6297
    if (angular.isDefined(practicedSystemFilter.active)) {
        $scope.filter.active = practicedSystemFilter.active.toString();
    }
    // {Map} Selected practiced systems
    $scope.selectedEntities = {};
    // {Object} first selected practiced system
    //$scope.firstSelectedPracticedSystem;
    //$scope.allSelectedPracticedSystemActive;
    //$scope.allSelectedPracticedSystemsValidable;
    $scope.selectedPracticedSystemEntities = {};

    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) { return; } // prevent init useless call

      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.filter);
      if (localFilter.practicedSystemCampaign === "") {
        delete localFilter.practicedSystemCampaign;
      }
      if (localFilter.active === '') {
        delete localFilter.active;
      }

      $http.post(ENDPOINTS.practicedSystemsListJson, "filter=" + encodeURIComponent(angular.toJson(localFilter)),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          $scope.practicedSystems = response.data.elements;
          $scope.pager = response.data;
      })
      .catch(function(response) {
         console.error("Échec de récupération des systèmes synthétisés", response);
         addPermanentError("Échec de récupération des systèmes synthétisés", response.status);
      });
    };

    $scope.selectAllEntities = function() {
      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.filter);
      if (localFilter.practicedSystemCampaign === "") {
        delete localFilter.practicedSystemCampaign;
      }
      if (localFilter.active === '') {
        delete localFilter.active;
      }

      displayPageLoading();
      $http.post($scope.endpoints.practicedSystemIdsJson, "filter=" + encodeURIComponent(angular.toJson(localFilter)),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          var selectedEntities = {};
          angular.forEach(response.data, function(topiaId) {
            selectedEntities[topiaId] = true;
          });
          Object.assign($scope.selectedEntities, selectedEntities);
      })
      .catch(function(response) {
         console.error("Échec de récupération des ids des systèmes synthétisés", response);
         addPermanentError("Échec de récupération des ids des systèmes synthétisés", response.status);
      })
      .finally(function() {
        hidePageLoading();
      });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.practicedSystemName, filter.growingSystemName, filter.practicedSystemCampaign, filter.domainName, filter.growingPlanName]',
    function(newValue, oldValue) {
      if (timer) {
        $timeout.cancel(timer);
      }
      timer = $timeout(function(){
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      }, 350);
    }, true);

    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);
    // watch without timer
    $scope.$watch('[filter.active, filter.validated]', function(newValue, oldValue) {
      let page = $scope.filter.page;
      $scope.filter.page = 0;
      if (!page) {
        $scope.refresh(newValue, oldValue);
      }
    }, true);

    $scope.$watch('selectedEntities', function(newValue, oldValue) {
        // active property and first selected
        angular.forEach($scope.practicedSystems, function(item) {
            if ($scope.selectedEntities[item.topiaId]) {
                $scope.selectedPracticedSystemEntities[item.topiaId] = item;
            } else {
                delete $scope.selectedPracticedSystemEntities[item.topiaId];

            }
        });
        // active property and first selected
        $scope.allSelectedPracticedSystemActive = true;
        $scope.allSelectedPracticedSystemsValidable = true;
        angular.forEach($scope.selectedPracticedSystemEntities, function(practicedSystem, practicedSystemId) {
            $scope.allSelectedPracticedSystemActive &= practicedSystem.active;
            $scope.allSelectedPracticedSystemsValidable &= (practicedSystem.validated && practicedSystem.userCanValidate);
            $scope.firstSelectedPracticedSystem = practicedSystem; // make sense when size() == 1
        });
    }, true);

    $scope.pushPracticedSystemDuplication = function() {
      var practicedSystemId = $scope.practicedSystemIdToDuplicate;
      var growingSystemId = $scope.practicedSystemDuplicateObject.growingSystemIdForDuplication;
      $http.post(
          ENDPOINTS.practicedSystemsDuplicate,
          "practicedSystemId=" + encodeURIComponent(practicedSystemId) +
          "&growingSystemId=" + encodeURIComponent(growingSystemId),
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
      ).then(function(response) {
        var action = ENDPOINTS.practicedSystemsEditInput + "?practicedSystemTopiaId=" + encodeURIComponent(response.data);
        displayPageLoading();
        window.location = action;
      }).catch(function(response) {
        $scope.displayDuplicationErrorIfPresent(response.data);
      });
    };

    /**
     * Duplicate practiced system after user confirm.
     */
    $scope.practicedSystemDuplicate = function() {
      var results = $filter('toSelectedValue')($scope.selectedEntities);
      if (results.length == 1) {
        $scope.practicedSystemDuplicateObject = {};
        $scope.practicedSystemIdToDuplicate = results[0];
        $http.post(
            ENDPOINT_AVAILABLE_GROWING_SYSTEM_FOR_PS_DUPLICATION_JSON,
            "practicedSystemId=" + encodeURIComponent($scope.practicedSystemIdToDuplicate),
            {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        ).then(function(response) {
          if (response.data && response.data != "null") {
            $scope.availableGrowingSystemsForDuplication = response.data;
            if ($scope.availableGrowingSystemsForDuplication.length === 1) {
              $scope.practicedSystemDuplicateObject.growingSystemIdForDuplication = $scope.availableGrowingSystemsForDuplication[0].topiaId;
            }
            if (response.data.length > 0) {
              $("#confirmDuplicatePracticedSystem").dialog({
                resizable: false,
                width: 400,
                modal: true,
                buttons: {
                    dupliquer: {
                      click: function () {
                        $(this).dialog("close");
                        displayPageLoading();
                        $scope.pushPracticedSystemDuplication();
                      },
                      text:"Dupliquer",
                      'class': 'btn-primary'
                    },
                    Annuler: {
                      click: function() {
                        $(this).dialog("close");
                      },
                      text:"Annuler",
                      'class': 'float-left btn-secondary'
                    }
                }
              });
            } else {
              $("#confirmDuplicatePracticedSystem").dialog({
                resizable: false,
                width: '80%',
                modal: true,
                position: { my: "center top", at: "top+50" },
                buttons: {
                  cancel: {
                    click: function() {
                      $(this).dialog("close");
                      displayPageLoading();
                      $scope.$apply();
                    },
                    text: 'Annuler',
                    'class': 'float-left btn-secondary'
                  }
                }
              });
            }
          }
        })
        .catch(function(response) {
           console.error("Échec de récupération des systèmes de culture permettant la duplication", response);
           addPermanentError("Échec de récupération des systèmes de culture permettant la duplication", response.status);
        });
      }
    };

    $scope.asyncPracticedSystemsExport = function() {
      var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

      var data = "practicedSystemIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
      $http.post(
          ENDPOINTS.practicedSystemsExportAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };

}]);

/**
 * Unactivate selected practiced system after user confirm.
 */
function practicedSystemUnactivate(unactivateDialog, unactivateForm, activate) {
    var selectionCount = JSON.parse(unactivateForm.children("input[name='practicedSystemIds']").val()).length;
    if (selectionCount > 0) {
        unactivateDialog.dialog({
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                action: {
                  click: function() {
                    $(this).dialog("close");
                    unactivateForm.attr("action", activate ? ENDPOINTS.practicedSystemsUnactivate+"?activate=true" : ENDPOINTS.practicedSystemsUnactivate);
                    unactivateForm.submit();
                  },
                  text: activate ? "Activer" : "Désactiver",
                  'class': 'btn-primary'
                },
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}

function validatePracticedSystems(confirmDialog, form, isToValidate) {
    var selectionCount = JSON.parse(form.children("input[name='practicedSystemIds']").val()).length;
    if (selectionCount > 0) {
        confirmDialog.dialog({
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                action: {
                    click: function() {
                        $(this).dialog("close");
                        form.attr("action", isToValidate ? ENDPOINTS.validatePracticedSystems+"?validate=true" : ENDPOINTS.validatePracticedSystems);
                        form.submit();
                    },
                    text: (isToValidate ? "Valider" : "dévalider"),
                    'class': 'btn-primary'
                },
                cancel: {
                    click: function() {
                        $(this).dialog("close");
                    },
                    text: 'Annuler',
                    'class': 'float-left btn-secondary'
                }
            }
        });
    }
}
