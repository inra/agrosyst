/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Widget pour faire les diagram pluriannuel
 *
 * usage: <div id="toto"></div><script>$(function(){$("#toto").cropCycleDiagram(options)})</script>
 *
 * Les options possibles sont:
 * data: {nodes: [], connections: []},
 * models: [],
 * nodeTemplate: '<div id="%{nodeId}s" class="%{classes}s"><span class="name">%{label}s</span><div class="ep"></div></div>',
 * trash: true,
 * columnWidth: 170,
 * endpointsRadius: 6,
 * nodeClickCallback:
 *
 * data peut-etre une chaine json, on un objet
 *
 * trash peut-être une chaine de caractère contenant du code HTML, ou un sélecteur
 * pour représenter Trash. false, n'affiche pas de Trash
 *
 * nodeClickCallback permet de spécifier une function qui recevra l'évènement lorsqu'un élément sera sélectionné
 *
 * Pour mettre à jour les data vous pouvez faire:
 *   $("#toto").cropCycleDiagram("data", {...});
 *
 */
jQuery.widget("agrosyst.cropCycleDiagram", {
    options: {
        data: {nodes: [], connections: []},
        models: [],
        nodeTemplate: '<div id="%{nodeId}s" class="%{classes}s" title="%{label}s">'
                      + '<div class="name">%{label}s</div>'
                      + '<div class="mvIcon">%{mixVarietiesAbbreviation}s</div>'
                      + '<div class="msIcon">%{mixSpeciesAbbreviation}s</div>'
                      + '<div class="mcIcon">%{mixCompanionAbbreviation}s</div>'
                      + '<div class="sp"></div>'
                      + '<div class="ep"></div>'
                      + '<div class="dIcon">%{catchCropAbbreviation}s</div>'
                      + '</div>',
        connectionTemplate: "<span>%{label}s</span>",
        trash: true,
        columnWidth: 170,
        endpointsRadius: 6,
        nodeClickCallback: '',
        connectionClickCallback: '',
        dataModificationCallback: '',
        messages: {
          intermediate: "CI",
          absentAbbr: "Abs",
          previousCampaign: "Campagne précédente",
          mixVarietiesAbbreviation: "MV",
          mixSpeciesAbbreviation: "ME",
          mixCompanionAbbreviation: "C",
          catchCropAbbreviation: "D",
          freqError: "",
        },
        isCreated: false
    },
    _create: function() {
        var diagram = this;
        this.element.addClass("diagram");

        this.modelsElem = jQuery("<div class='models'></div>").appendTo(this.element);
        this.columnsContainer = jQuery("<div class='columns-container'></div>").appendTo(this.element);
        this.columnsElem = jQuery("<div class='columns'></div>").css("width", "100%").appendTo(this.columnsContainer);

        this.jsPlumb = jsPlumb.getInstance();
        this.jsPlumb.importDefaults({
            // default drag options
            DragOptions: {cursor: 'pointer', zIndex: 2000},
            Endpoints: [["Dot", {radius: this.options.endpointsRadius}], ["Dot", {radius: this.options.endpointsRadius}]],
            ConnectionsDetachable: false
        });

        // detecte l'ajout d une connexion et change sa couleur
        this.jsPlumb.bind("connection", function(conn) {
            var connData = diagram._getConnectionData(conn.sourceId, conn.targetId);
            var notUsedForThisCampaign = false;
            if (connData) {
              notUsedForThisCampaign = connData.notUsedForThisCamp;
            }

            conn.connection.addOverlay([ "Label", {label:"", id:"label", cssClass:"connection-label", events:{
                "click":function(label, evt) {
                    diagram.options.connectionClickCallback && diagram.options.connectionClickCallback(diagram, connData);
                    diagram._setSelected(conn);
                }
            }}]);

            diagram._addConnection(conn.sourceId, conn.targetId, notUsedForThisCampaign);
        });

        // détecte la suppression d'une connexion qui peut provenir de la suppression
        // de la connection ou d'un noeud
        this.jsPlumb.bind("connectionDetached", function(conn) {
            diagram._removeConnection(conn.sourceId, conn.targetId);
        });

        // détecte le click
        this.jsPlumb.bind("click", function(conn) {
            var connData = diagram._getConnectionData(conn.sourceId, conn.targetId);
            diagram.options.connectionClickCallback && diagram.options.connectionClickCallback(diagram, connData);
            diagram._setSelected(conn);
        });

        // permet d'interdire de mettre 2 fois la meme connexion entre les memes noeuds
        this.jsPlumb.bind("beforeDrop", function(info) {
            var result = !diagram._containsConnection(info.sourceId, info.targetId);
            // permet d'interdire une connexion sur le même noeud (boucle)
            result &= info.sourceId != info.targetId;
            return result;
        });

        // Redimensionnement auto de la zone du graphe
        $(".columns-container").width ($(window).width() * 0.8 +'px');
        $(window).resize(function () {
          $(".columns-container").width ($(window).width() * 0.8 +'px');
        });


        this.curColourIndex = 1;
        this.maxColourIndex = 24;

        this._addColumn(this.columnsElem, 1);
        this._addModel(this.modelsElem, this.options.models);

        this.data(this.options.data);

    },
    _setOption: function(key, value) {
        this._super(key, value);
        if (key === "trash") {
            this._addTrash(this.trashElem, this.options.trash);
        } else if (key === "models") {
            this._addModel(this.modelsElem, this.options.models);
        } else {
            this.redraw();
        }
    },
    _setOptions: function(options) {
        this._super(options);
        this.redraw();
    },
    _destroy: function() {
        this.element
                .removeClass("diagram")
                .empty();
    },
    data: function(value) {
        if (value) {
            if (typeof value === "string") {
                value = JSON.parse(value);
            }
            this.options.data = {};
            jQuery.extend(this.options.data, value);

            this.redraw();
        } else {
            return this.options.data;
        }
    },
    _templateNode: function(data) {
        if (data.mixVariety) {
            data.classes = 'mixVariety ' + data.classes;
        }
        if (data.mixSpecies) {
            data.classes = 'mixSpecies ' + data.classes;
        }
        if (data.mixCompanion) {
            data.classes = 'mixCompanion ' + data.classes;
        }
        if (data.catchCrop) {
            data.classes = 'catchCrop ' + data.classes;
        }
        data.mixVarietiesAbbreviation = this.options.messages.mixVarietiesAbbreviation;
        data.mixSpeciesAbbreviation = this.options.messages.mixSpeciesAbbreviation;
        data.mixCompanionAbbreviation = this.options.messages.mixCompanionAbbreviation;
        data.catchCropAbbreviation = this.options.messages.catchCropAbbreviation;
        
        return this._format(this.options.nodeTemplate, data);
    },
    _getNodeId: function(data) {
        var result = data && data.nodeId || "new-node-" + generateUUID();
        return result;
    },
    /**
     * Format la chaine avec les info de data. Si data est un objet il faut que dans
     * le template on retrouve des %{fieldname}s. Si data est un tableau
     * les %s sont convertie dans l'ordre ou on les trouves
     * @param {String} template
     * @param {Object or Array} data
     * @returns {String}
     */
    _format: function(template, data) {
        var index = 0;
        var result = template.replace(/%\{(.*?)\}s/g, function(match, field) {
            return data[field] || data[index++] || "";
        });
        return result;
    },
    /**
     * Return node with nodeId or undefined if not found
     * @param {String} nodeId
     * @returns {NodeData or undefined}
     */
    _getNodeData: function(nodeId) {
        var result;
        for (var i in this.options.data.nodes) {
            if (this.options.data.nodes[i].nodeId === nodeId) {
                result = this.options.data.nodes[i];
                break;
            }
        }
        return result;
    },
    /**
     * Retourne tous les noeuds dans l'ordre
     * @returns {Array} all nodes ordered by position x,y
     */
    _getNodes: function() {
        var result = this.options.data.nodes;
        result.sort(function(a, b) {
            var v;
            if (a.x < b.x) {
                v = -1;
            } else if (a.x > b.x) {
                v = 1;
            } else {
                if (a.y < b.y) {
                    v = -1;
                } else if (a.y > b.y) {
                    v = 1;
                } else {
                    v = 0;
                }
            }
            return v;
        });
        return result;
    },
    /**
     * Ajout d'un nouveau noeud. data est directement utilise sans faire de copie
     * nodeId est ajoute au data. Si le noeud existait deja, les donnees actuelles
     * sont remplacer par les donnees en argument
     * @param {String} nodeId
     * @param {Object} data
     * @returns {undefined}
     */
    _addNode: function(nodeId, data) {
        this._getNodeData(nodeId) && this._removeNode(nodeId); // prevent already present node
        data.nodeId = nodeId;
        this.options.data.nodes.push(data);
        this._validateCatchCrops();
        this.options.dataModificationCallback && this.options.dataModificationCallback('addNode', this.options.data, data);
    },
    /**
     * Met a jour les data d'un noeud avec les valeurs passees dans data, si le noeud
     * n'existe pas rien n'est fait
     * @param {String} nodeId
     * @param {Object} data
     * @returns {undefined}
     */
    _updateNode: function(nodeId, data) {
        var i; // to iterate

        var node = this._getNodeData(nodeId);
        let originalNode = JSON.parse(JSON.stringify(node));
        node && jQuery.extend(node, data);

        var nodeUI = $("#" + nodeId);
        var endpoints = this.jsPlumb.getEndpoints(nodeUI);
        if (node && endpoints) {
            for (i = 0 ; i < endpoints.length ; i++) {
                var endpoint = endpoints[i];
                if (endpoint.isSource) {
                    endpoint.setAnchor(node.endCycle ? "BottomRight" : "RightMiddle");
                }
            }
        }

        if (node) {
          if (node.endCycle) {
            nodeUI.addClass("end-cycle");
          } else {
            nodeUI.removeClass("end-cycle");
          }

          var sameCampaignAsPreviousNode = node.sameCampaignAsPreviousNode;
          if (sameCampaignAsPreviousNode) {
              nodeUI.addClass("same-campaign");
          } else {
              nodeUI.removeClass("same-campaign");
          }

        }
        
        this.jsPlumb.unmakeSource(nodeUI);
        this._makeSource(nodeUI, node);

        let updatedNode = JSON.parse(JSON.stringify(node));
        if (originalNode.x !== updatedNode.x && 
            originalNode.y !== updatedNode.y && 
            originalNode.sameCampaignAsPreviousNode !== originalNode.sameCampaignAsPreviousNode &&
            originalNode.endCycle !== originalNode.endCycle) {

            var connections = this._getConnectionDataForTarget(nodeId);

            if (connections) {
                var connData = {sameCampaignAsPreviousNode : sameCampaignAsPreviousNode};
                for (i = 0 ; i < connections.length ; i++) {
                    var conn = connections[i];
                    this._updateConnection(conn.sourceId, conn.targetId, connData);
                }
            }
        }
    },

    //    Ajouter un message de vigilance de couleur jaune/orange : "Attention, pour une culture dérobée la case "Même campagne agricole que la culture précédente" n'a pas été cochée." Les conditions pour afficher ce message sont :
    //    - qu'il y ait une culture dérobée dans le schéma de rotation
    //    - que case "Même campagne agricole que la culture précédente" n'a pas été cochée pour la culture dérobée OU pour toutes les cultures qui suivent immédiatement la culture dérobée.
    _validateCatchCrop: function(nodeId) {
        if (nodeId) {
            let c_node = this._getNodeData(nodeId);
            if (c_node.sameCampaignAsPreviousNode) {
                return true;
            }
            let currentNodeIndex = c_node.x;
            var connections = this._getConnectionDataForSource(nodeId);
            if (connections) {
                for(let i=0; i < connections.length; i++) {
                    let con = connections[i];
                    if (con && con.targetId) {
                        var nextNode = this._getNodeData(con.targetId);
                        if (nextNode.x > currentNodeIndex) {
                            let c_next_node = this._getNodeData(con.targetId);
                            if (c_next_node.sameCampaignAsPreviousNode) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    },
    _validateCatchCrops: function() {
        var catchCropErrorMessage = I18N.messages["itk-messages-js-catch-crop-same-campaign-as-previous-warning"];
        let catchCropNodes = this.options.data.nodes.filter(n => n.catchCrop);
        if (catchCropNodes) {
            var isError = false;
            for(let i=0; i < catchCropNodes.length; i++) {
                let node = catchCropNodes[i];
                let isCatchCropValid = this._validateCatchCrop(node.nodeId);
                if (!isCatchCropValid) {
                    isError = true;
                    break;
                }
            }
            if (isError) {
                addPermanentWarning(catchCropErrorMessage);
            } else {
                var key = catchCropErrorMessage.replace(/\s+/g, '_').toLowerCase().replace(/[^\w\s!]/gi, '');
                removeInfoMessage(key);
            }
        }
    },

    /**
     * Permet d'indiquer si un noeud est une fin de cycle ou non
     * et si sa culture est successive à la précédente dans la campagne
     * @param nodeId
     */
    updateNode: function(nodeId, endCycle, sameCampaignAsPreviousNode) {
        this._updateNode(nodeId, {endCycle: endCycle, sameCampaignAsPreviousNode: sameCampaignAsPreviousNode});
        var node = this._getNodeData(nodeId);
        if (sameCampaignAsPreviousNode) {
            this._moveOtherNodesFromColumn(node.x);
        } else if(node.catchCrop){
            var connections = this._getConnectionDataForTarget(nodeId);
        }
        this._validateCatchCrops();
    },

    /**
     * Déplace dans la colonne suivante les noeuds de la colonne "index"
     * dont la culture n'est pas successive à une autre sur une même campagne
     * (en décalant tous les noeuds suivants)
     */
    _moveOtherNodesFromColumn: function(index) {
        var self = this;
        var columns = this.columnsElem.children(".column");
        var column = columns.eq(index);

        $.each(column.children(), function(i, nodeUI) {
            var nodeId = nodeUI.id;
            var node = self._getNodeData(nodeId);
            if (node && !node.sameCampaignAsPreviousNode) {
                self._moveNodeToNextColumn(nodeId);
            }
        });
        this.jsPlumb.repaintEverything();
    },

    /**
     * Déplace le noeuds dans la colonne suivante (et les suivants récursivement)
     */
    _moveNodeToNextColumn: function(nodeId) {
        var node = this._getNodeData(nodeId);

        var conns = this._getConnectionDataForSource(nodeId);
        if (conns) {
            for (var i = 0 ; i < conns.length ; i++) {
                var conn = conns[i];
                var nextNode = this._getNodeData(conn.targetId);
                // on arrête la récursion en cas de connexion à rebours
                if (node.x < nextNode.x) {
                    this._moveNodeToNextColumn(conn.targetId);
                }
            }
        }
        var nextColumnIndex = ++node.x;
        var columns = this.columnsElem.children(".column");

        this._addColumn(this.columnsElem, nextColumnIndex);
        var nextColumn = columns.eq(nextColumnIndex);

        var nodeUI = $("#" + nodeId);
        nodeUI.appendTo(nextColumn);
        node.y = $(nodeUI).index(nextColumn);

    },

    /**
     * Supprime un noeud
     * @param {String} nodeId
     * @returns {undefined}
     */
    _removeNode: function(nodeId) {
        var node;
        this.options.data.nodes = this.options.data.nodes.filter(function(e, i, array) {
            var result = e.nodeId !== nodeId;
            if (!result) {
              node = e;
            }
            return result;
        });

        this.options.data.connections = this.options.data.connections.filter(function(e, i, array) {
            // on retient l'element si result est vrai
            var result = !(e.sourceId === nodeId || e.targetId === nodeId);
            return result;
        });
        node && this.options.dataModificationCallback && this.options.dataModificationCallback('removeNode', this.options.data, node);
    },

    _updateConnectionCroppingPlanEntryFrequencyLabel: function(conn) {
        let sourceId = conn.sourceId;
        let targetId = conn.targetId;
        if (conn.croppingPlanEntryFrequency == undefined) {
            let uiFreqElement = document.getElementById("selectedCropCycleConnectionFrequency");
            if (uiFreqElement) {
                conn.croppingPlanEntryFrequency = parseInt(uiFreqElement.value);
            }
        }
        conn.label = conn.croppingPlanEntryFrequency + "%"

        var connUI = this._getUIConnection(sourceId, targetId);
        this._updateConnectionLabel(conn, connUI);
    },

    _setOtherConnectionCroppingPlanEntryFrequency: function(conn) {
        let sourceId = conn.sourceId;
        let targetId = conn.targetId;
        let someOtherConnections = 0;
        let connWithoutFreq = [];

        if(this.options && this.options.data.connections) {
           someOtherConnections = this.options.data.connections
           .filter(c => (c.sourceId == sourceId && c.targetId != targetId))
           .filter(c => (c.croppingPlanEntryFrequency))
           .map(c => c.croppingPlanEntryFrequency)
           .reduce((a, b) => a+b, 0);

           connWithoutFreq = this.options.data.connections
           .filter(c => (c.sourceId == sourceId && c.targetId != targetId))
           .filter(c => (!c.croppingPlanEntryFrequency));
        }
        let totalFreq = conn.croppingPlanEntryFrequency + someOtherConnections;
        let nbFreqNotSet = connWithoutFreq.length;
        if (totalFreq <= 100 && nbFreqNotSet > 0) {
            let averageCropPart = (100 - totalFreq) / nbFreqNotSet;
            // add freq to connexion without one
            connWithoutFreq.map(c => {
                c.croppingPlanEntryFrequency = averageCropPart; 
                c.label=averageCropPart+"%"});
        }
    },

    _setAutoConnectionCroppingPlanEntryFrequency: function(conn) {
        let sourceId = conn.sourceId;
        let targetId = conn.targetId;
        let someOtherConnections = 0;

        if(this.options && this.options.data.connections) {
           someOtherConnections = this.options.data.connections
           .filter(c => (c.sourceId == sourceId && c.targetId != targetId))
           .filter(c => (c.croppingPlanEntryFrequency))
           .map(c => c.croppingPlanEntryFrequency)
           .reduce((a, b) => a+b, 0);
        }

        if (someOtherConnections < 100) {
            let curentConnFreq = 100 - someOtherConnections;
            conn.croppingPlanEntryFrequency = curentConnFreq >= 0 ? curentConnFreq : 0;
            conn.label=curentConnFreq+"%"
        } else {
            conn.croppingPlanEntryFrequency = 0
            conn.label="0%"
            conn.freqError = true;
        }
    },

    _validConnUiCroppingPlanEntryFrequency: function(sourceId) {
        let someConnections = 0;
        let notValuedConnection = 0;

        if(this.options && this.options.data.connections) {
           someConnections = this.options.data.connections
                .filter(c => (c.sourceId == sourceId))
                .filter(c => (c.croppingPlanEntryFrequency))
                .map(c => c.croppingPlanEntryFrequency)
                .reduce((a, b) => a+b, 0);

           notValuedConnection = this.options.data.connections
                 .filter(c => (c.sourceId == sourceId))
                 .filter(c => (c.targetId != sourceId))
                 .filter(c => !c.croppingPlanEntryFrequency).length
        }

        const saveButton = document.getElementById("saveButton");
        const autoFreqAdjust = document.getElementById("autoFreqAdjust");
        if (someConnections != 100 || notValuedConnection) {
            saveButton.classList.add("btn-primary", "disabled");
            if (autoFreqAdjust) {
                // seulement présent lorque l'on est en édition sur une connection
                autoFreqAdjust.classList.remove("disabled");
            }
            this.options.data.connections
            .filter(c => (c.sourceId == sourceId))
            .map(c => {
                c.freqError = true;
                var connUI = this._getUIConnection(c.sourceId, c.targetId);
                this._updateConnectionLabel(c, connUI);
            });
        } else {
            saveButton.classList.remove("disabled");
            if (autoFreqAdjust) {
                // seulement présent lorque l'on est en édition sur une connection
                autoFreqAdjust.classList.add("fakeButtonDisabled");
            }
            this.options.data.connections
            .filter(c => (c.sourceId == sourceId))
            .map(c => {
                c.freqError = false;
                let connUI = this._getUIConnection(c.sourceId, c.targetId);
                this._updateConnectionLabel(c, connUI);
            });
        }
    },

    _validConnectionCroppingPlanEntryFrequency: function(conn) {
        let sourceId = conn.sourceId;
        let someConnections = 0;
        let currentConn = undefined;
        let notValuedConnection = conn.croppingPlanEntryFrequency ? 0 : 1;

        if(this.options && this.options.data.connections) {
            someConnections = this.options.data.connections
                .filter(c => (c.sourceId == sourceId))
                .filter(c => (c.targetId != conn.targetId))
                .filter(c => (c.croppingPlanEntryFrequency))
                .map(c => c.croppingPlanEntryFrequency)
                .reduce((a, b) => a+b, 0);

            currentConn = this.options.data.connections
                .filter(c => (c.sourceId == sourceId))
                .filter(c => (c.targetId == conn.targetId));
            if (currentConn.length > 0) {
                conn = currentConn[0];
            }

            notValuedConnection += this.options.data.connections
                  .filter(c => (c.sourceId == sourceId))
                  .filter(c => (c.targetId != conn.targetId))
                  .filter(c => !(c.croppingPlanEntryFrequency)).length
        }

        let total = someConnections;
        if (conn && conn.croppingPlanEntryFrequency) {
            total = someConnections + conn.croppingPlanEntryFrequency;
        }

        const saveButton = document.getElementById("saveButton");
        const autoFreqAdjust = document.getElementById("autoFreqAdjust");
        if (total != 100 || notValuedConnection) {
            saveButton.classList.add("btn-primary", "disabled");
            if (autoFreqAdjust) {
                // seulement présent lorque l'on est en édition sur une connection
                autoFreqAdjust.classList.remove("fakeButtonDisabled");
            }

            conn.freqError = true;
            var connUI = this._getUIConnection(sourceId, conn.targetId);
            this._updateConnectionLabel(conn, connUI);

            this.options.data.connections
           .filter(c => (c.sourceId == sourceId))
           .map(c => {
                c.freqError = true;
                var connUI = this._getUIConnection(c.sourceId, c.targetId);
                this._updateConnectionLabel(c, connUI);
            });
        } else {
            saveButton.classList.remove("disabled"); 
            if (autoFreqAdjust) {
                // seulement présent lorque l'on est en édition sur une connection
                autoFreqAdjust.classList.add("fakeButtonDisabled");
            }
            this.options.data.connections
               .filter(c => (c.sourceId == sourceId))
               .map(c => {
                    c.freqError = false;
                    let connUI = this._getUIConnection(c.sourceId, c.targetId);
                    this._updateConnectionLabel(c, connUI);
                }
               );
            if (conn) {
                conn.freqError = false;
                let connUI = this._getUIConnection(sourceId, conn.targetId);
                this._updateConnectionLabel(conn, connUI);
            }
        }
    },

    _addConnection: function(sourceId, targetId, notUsedForThisCampaign) {
        var conn = {sourceId: sourceId, targetId: targetId, notUsedForThisCampaign: notUsedForThisCampaign};
        // only if the model is ready
        if (this.options.isCreated) {
            this._setAutoConnectionCroppingPlanEntryFrequency(conn);
            this._setOtherConnectionCroppingPlanEntryFrequency(conn);
            this._validConnectionCroppingPlanEntryFrequency(conn);
            this._validateCatchCrops();
        }

        this._updateSourceNodeSameCampaign(conn);

        // on ajoute que si elle n'existe pas deja
        if (! this._containsConnection(sourceId, targetId)) {
            this.options.data.connections.push(conn);
            this.options.dataModificationCallback && this.options.dataModificationCallback('addConnection', this.options.data, conn);
        }

        var connUI = this._getUIConnection(sourceId, targetId);
        connUI.setPaintStyle(this._getConnectionStyle(conn));
    },
    _removeConnection: function(sourceId, targetId) {
        var sourceNode = this._getNodeData(sourceId);
        if (sourceNode && !sourceNode.sameCampaignAsPreviousNode) {
            var sourceNodeUI = $("#" + sourceId);
            sourceNodeUI.removeClass("same-campaign");
        }

        var conn;
        this.options.data.connections = this.options.data.connections.filter(function(e, i, array) {
            var result = !(e.sourceId === sourceId && e.targetId === targetId);
            if (!result) {
                conn = e;
            }
            return result;
        });
        conn && this.options.dataModificationCallback && this.options.dataModificationCallback('removeConnection', this.options.data, conn);
    },
    _getConnectionData: function(sourceId, targetId) {
        var result;
        for (var c in this.options.data.connections) {
            var conn = this.options.data.connections[c];
            if (conn.sourceId === sourceId && conn.targetId === targetId) {
                result = conn;
                break;
            }
        }
        return result;
    },
    _getConnectionDataForTarget: function(targetId) {
        var result = [];
        for (var c in this.options.data.connections) {
            var conn = this.options.data.connections[c];
            if (conn.targetId === targetId) {
                result.push(conn);
            }
        }
        return result;
    },
    _getConnectionDataForSource: function(sourceId) {
        var result = [];
        for (var c in this.options.data.connections) {
            var conn = this.options.data.connections[c];
            if (conn.sourceId === sourceId) {
                result.push(conn);
            }
        }
        return result;
    },
    _updateConnectionLabel: function(conn, connUI) {
        if (connUI && this.options){
            var overlay = connUI.getOverlay("label");
            if (overlay) {
                let intermediateCropSpan = conn.intermediateCroppingPlanEntryCode ? "<span><b>" + this.options.messages.intermediate + "</b><br></span>" : "";
                if (conn.freqError) {
                    let errorOpenSpan = "<span style='background-color: rgba(249, 2, 2, 0.51);font-weight: bold;'>";
                    let errorInputRequired = "<input required='true' style='width: 0; height: 0; border: none; padding: 0;opacity: 0;'></input><br>";
                    let absSpan = conn.notUsedForThisCampaign ? "<span>" + this.options.messages.absentAbbr + "<br></span>" : "";
                    let uiValue = this._format(this.options.connectionTemplate, conn);
                    let errorCloseSpan = "</span>";
                    //overlay.setLabel("<span>" + this.options.messages.absentAbbr + "<br>"+ overlay.getLabel() +"</span>");
                    overlay.setLabel(errorOpenSpan + errorInputRequired + absSpan + intermediateCropSpan + uiValue + errorCloseSpan);
                } else if (conn.label && conn.notUsedForThisCampaign) {
                    let absSpanOpen = "<span>" + this.options.messages.absentAbbr + "<br>";
                    overlay.setLabel(absSpanOpen + intermediateCropSpan + this._format(this.options.connectionTemplate, conn) +"</span>");
                } else if (conn.label && !conn.notUsedForThisCampaign) {
                    // default one
                    overlay.setLabel(intermediateCropSpan + this._format(this.options.connectionTemplate, conn));
                } else if (conn.notUsedForThisCampaign){
                    overlay.setLabel("<span>" + this.options.messages.absentAbbr + "</span>" + intermediateCropSpan);
                } else {
                    overlay.setLabel("");
                }
            }
            
        }
    },
    _updateConnection: function(sourceId, targetId, data) {
        var conn = this._getConnectionData(sourceId, targetId);
        conn && jQuery.extend(conn, data);

        if (conn) {

            this._updateSourceNodeSameCampaign(conn);

            var connUI = this._getUIConnection(sourceId, targetId);

            if (this.options.isCreated) {
                this._updateConnectionCroppingPlanEntryFrequencyLabel(conn);
                this._setOtherConnectionCroppingPlanEntryFrequency(conn);
                this._validConnectionCroppingPlanEntryFrequency(conn);
            }

            this._updateConnectionLabel(conn, connUI);

            var targetNode = this._getNodeData(targetId);
            conn.sameCampaignAsPreviousNode = targetNode.sameCampaignAsPreviousNode;

            connUI.setPaintStyle(this._getConnectionStyle(conn));

            this.options.dataModificationCallback && this.options.dataModificationCallback('updateConnection', this.options.data, conn);
        }
    },

    /**
     * retourne vrai si cette connexion existe deja
     */
    _containsConnection: function(sourceId, targetId) {
        var result = this.options.data.connections.some(function(element, index, array) {
            return element.sourceId === sourceId && element.targetId === targetId;
        });
        return result;
    },

// helper method to generate a color from a cycle of colors.
    _nextColour: function() {
        var R, G, B;
        R = parseInt(128 + Math.sin((this.curColourIndex * 3 + 0) * 1.3) * 128);
        G = parseInt(128 + Math.sin((this.curColourIndex * 3 + 1) * 1.3) * 128);
        B = parseInt(128 + Math.sin((this.curColourIndex * 3 + 2) * 1.3) * 128);
        this.curColourIndex = this.curColourIndex + 1;
        if (this.curColourIndex > this.maxColourIndex)
            this.curColourIndex = 1;
        return "rgb(" + R + "," + G + "," + B + ")";
    },
// this is the paint style for the connecting lines
    _getConnectionStyle: function(connData) {
        var connectorPaintStyle = {
            lineWidth: 2,
            strokeStyle: "#777777",
            joinstyle: "round",
            outlineColor: "transparent",
            outlineWidth: 5
        };

        if (connData) {
            if (this._getNodeData(connData.targetId).x === 0 && !connData.notUsedForThisCampaign) {
                connectorPaintStyle.strokeStyle = "#BBBBBB";
                connectorPaintStyle.dashstyle = "5 1";
            } else if (connData.sameCampaignAsPreviousNode && !connData.notUsedForThisCampaign) {
                connectorPaintStyle.strokeStyle = "#777777";
                connectorPaintStyle.outlineColor = "#A4DCE9";
                connectorPaintStyle.outlineWidth = 5;
            } else if (connData.notUsedForThisCampaign) {
                connectorPaintStyle.strokeStyle = "#FF2700";
                connectorPaintStyle.outlineColor = "#A4DCE9";
                connectorPaintStyle.outlineWidth = 5;
            } else {
                connectorPaintStyle.strokeStyle = "#777777";
                connectorPaintStyle.outlineColor = "transparent";
                connectorPaintStyle.outlineWidth = 5;
            }
        }
        return connectorPaintStyle;
    },
    _getUIConnection: function(sourceId, targetId) {
        var conns = this.jsPlumb.getConnections({source:sourceId, target:targetId, flat: true});
        return conns[conns.length - 1];
    },
    /**
     * Make e the current selection (class selected add)
     * @param {Connection or JQuery element} e element currently selected
     */
    _setSelected: function(e) {
        // remove all previously selected class
        $(".selected", this.columnsElem).removeClass("selected");
        var allConnections = this.jsPlumb.getConnections();
        for (var c in allConnections) {
            allConnections[c].removeClass("selected");
        }
        // add selected to current selected element
        if (!e.addClass) {
            e = this._getUIConnection(e.sourceId, e.targetId);
        }
        e.addClass("selected");
    },
    /**
     * Genere un element HTML pret a etre ajouter au DOM.
     * Utilise pour la generation d'un point dans la column before
     * @param {Object} modelData toutes les infos pour creer le noeud
     *  (au minimum: nodeId, classes, label)
     * @returns {DOM Node}
     */
    _makePointBefore: function(modelData) {
        var extendedData = jQuery.extend({classes: "node point"}, modelData);
        if (modelData.endCycle) {
            node.addClass("end-cycle");
        }

        this.jsPlumb.makeSource(node, {
            filter: ".ep",
            anchor: "RightMiddle",
            connector: ["Straight"], // ["StateMachine", { curviness: 0 }], //["Flowchart", {stub: [40, 60], gap: 6, cornerRadius: 5, alwaysRespectStubs: false}],
            connectorStyle: this._getConnectionStyle()
        });

        return node;
    },
    /**
     * Genere un element HTML pret a etre ajouter au DOM
     * @param {Object} modelData toutes les infos pour creer le noeud
     *  (au minimum: nodeId, classes, label)
     * @returns {DOM Node}
     */
    _makeNodeBefore: function(modelData) {
        var diagram = this;
        var extendedData = jQuery.extend({classes: "node"}, modelData);
        var node = $(this._templateNode(extendedData));
        if (modelData.endCycle) {
            node.addClass("end-cycle");
        }

        this.options.nodeClickCallback && node.click(this._getNodeData(modelData.nodeId), this.options.nodeClickCallback);
        node.click(node, function(e) {
            diagram._setSelected(e.data);
        });

        this.jsPlumb.makeSource(node, {
            filter: ".ep",
            anchor: "RightMiddle",
            connector: ["Straight"], // ["StateMachine", { curviness: 0 }], //["Flowchart", {stub: [40, 60], gap: 6, cornerRadius: 5, alwaysRespectStubs: false}],
            connectorStyle: this._getConnectionStyle()
        });

        return node;
    },
    /**
     * Genere un element HTML pret a etre ajouter au DOM
     * @param {Object} modelData toutes les infos pour creer le noeud
     *  (au minimum: nodeId, classes, label)
     * @returns {DOM Node}
     */
    _makeNode: function(modelData, first) {
        var diagram = this;

        var extendedData = jQuery.extend({classes: "node"}, modelData);
        var node = $(this._templateNode(extendedData));
        if (modelData.endCycle) {
            node.addClass("end-cycle");
        }
        if (modelData.sameCampaignAsPreviousNode) {
            node.addClass("same-campaign");
        }

        this.options.nodeClickCallback && node.click(this._getNodeData(modelData.nodeId), this.options.nodeClickCallback);
        node.click(node, function(e) {
            diagram._setSelected(e.data);
        });

        this._makeSource(node, modelData);

        this.jsPlumb.makeTarget(node, {
            dropOptions: {hoverClass: "dragHover"},
            anchor: first ? "Bottom" : "LeftMiddle",
            isTarget: true
        });
        return node;
    },
    _makeSource: function(nodeUI, nodeData) {
        this.jsPlumb.makeSource(nodeUI, {
            filter: ".ep",
            anchor: nodeData && nodeData.endCycle ? "BottomRight" : "RightMiddle",
            isSource: true,
            connector: nodeData && nodeData.endCycle ? ["Bezier", { curviness: 80 }] : ["Straight" ], // ["StateMachine", { curviness: 0 }], //["Flowchart", {stub: [40, 60], gap: 6, cornerRadius: 5, alwaysRespectStubs: false}],
            connectorStyle: this._getConnectionStyle()
        });
    },
    /**
     * Remet a jour tous les liens qui sont dans la colonne source et destination
     * @param {Element} source
     * @param {Element} dest
     * @param {String} eventName
     * @returns {undefined}
     */
    _refresh: function(source, dest, eventName) {
        var all = $(source).add(dest);
        all = all.children(".node:not(.ui-sortable-placeholder)");
        var diagram = this;
        all.each(function(i, e) {
            var item = $(e);
            var nodeId = item.attr("id");
            if (nodeId) {
                diagram.jsPlumb.repaint(item);

                var x = item.parent().data("index");
                var y = item.index();
                diagram._updateNode(nodeId, {x: x, y: y});
            }
        });
    },
    /**
     * Ajoute une premiere colonne dans lequel on peut mettre des elements
     * liable mais non deplaceable. Cette colonne ne permet pas l'ajout d'element
     * pas glisser/deplacer.
     * @param {JQuery Element} parent columns element
     */
    _addBeforeColumn: function(parent) {
        var before = parent.children(".before");
        if (before.length === 0) {
            before = $('<div class="before"><div class="title">' + this.options.messages.previousCampaign + '</div></div>')
                    .css({width: this.options.columnWidth, float: "left"})
                    .prependTo(parent);
            before.disableSelection();
        }
        return before;
    },
    /**
     * Ajoute autant de colonne que besoin pour satisfaire min. Si le nombre
     * de colonne convient deja, rien n'est fait
     * @param {JQuery Element} parent
     * @param {Number} minColumn le nombre mini de colonne que doit avoir parent
     * @returns {undefined}
     */
    _addColumn: function(parent, minColumn) {
        var diagram = this;
        if (typeof minColumn !== "number") {
            // minColumn is column element it self
            // we must add one column if minColumn is last, and index is 0 based
            minColumn = $(minColumn).data("index") + 2;
        }
        var columnNumber = parent.children(".column").length;
        var columnBefore = parent.children(".before").length;
        while (columnNumber < minColumn) {
            columnNumber++;
            $(parent).css("min-width", ((columnNumber + columnBefore) * this.options.columnWidth + 20) + "px");
            var column = $('<div class="column"></div>')
                    .css({width: this.options.columnWidth, float: "left"})
                    .appendTo(parent);
            column.data("index", columnNumber - 1); // to have 0 based index
            column.sortable({
//                revert: true, // beautiful effect (transition with new place) // Creates a bug when removing a node
                connectWith: ".column",
                receive: function(event, ui) {
                    diagram._addColumn(parent, this);
                    diagram._refresh(this, ui.sender, "receive");
                },
                // TODO, faire le tri dans les evenements reellement util pour le refresh (a priori stop est necessaire)
                activate: function(event, ui) {
                    diagram._refresh(this, ui.sender, "activate");
                },
                beforeStop: function(event, ui) {
                    diagram._refresh(this, ui.sender, "beforeStop");
                },
                change: function(event, ui) {
                    diagram._refresh(this, ui.sender, "change");
                },
                deactivate: function(event, ui) {
                    diagram._refresh(this, ui.sender, "deactivate");
                },
                out: function(event, ui) {
                    diagram._refresh(this, ui.sender, "out");
                },
                over: function(event, ui) {
                    diagram._refresh(this, ui.sender, "over");
                },
                remove: function(event, ui) {
                    diagram._refresh(this, ui.sender, "remove");
                },
                sort: function(event, ui) {
                    diagram._refresh(this, ui.sender, "sort");
                },
                start: function(event, ui) {
                    diagram._refresh(this, ui.sender, "start");
                },
                stop: function(event, ui) {
                    diagram._refresh(this, ui.sender, "stop");
                    // bug fix when used dropable and sortable
                    $( this ).parent().children().removeClass( "ui-state-default" );
                },
                update: function(event, ui) {
                    diagram._refresh(this, ui.sender, "update");
                }
            });
            column.disableSelection();

            column.droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ":not(.ui-sortable-helper)",
                drop: function(event, ui) {
                    var model = $(ui.draggable);
                    if (model.hasClass("model")) {
                      var x = $(this).data("index");
                      var y = $(this).children().length;
                      // si on a ajoute a la derniere colonne, on en cree une nouvelle
                      diagram._addColumn(parent, this);

                      var nodeId = diagram._getNodeId();

                      var data = jQuery.extend({}, {x: x, y: y}, model.data("info"));
                      diagram._addNode(nodeId, data);

                      diagram.options.isCreated = true;

                      var clone = diagram._makeNode(jQuery.extend({nodeId: nodeId}, model.data("info")), x === 0);
                      clone.appendTo(this);
                    }
                }
            });
        }
    },
    _addTrash: function(trashElem, trashOption) {
        var diagram = this;
        trashElem.empty();
        var trash;
        if (trashOption === true) {
            trash = "<span>Trash</span>";
        } else {
            trash = trashOption;
        }
        if (trash) {
            trash = jQuery(trash).appendTo(trashElem);
            trash.droppable({
                accept: ".node",
                activeClass: "ui-state-highlight",
                drop: function(event, ui) {
                    var item = $(ui.draggable);
                    diagram.removeNode(item);
                }
            });
        }
    },
    _addModel: function(parent, models) {
        parent.empty();
        for (var m in models) {
            var data = jQuery.extend({classes: "model"}, models[m]);
            var elem = $(this._templateNode(data));
            elem.data("info", models[m]);

            elem.draggable({
                appendTo: "#agrosyst-content,#ipmworks-content",
                helper: "clone"
            });

            elem.appendTo(parent);
        }
    },

    _updateSourceNodeSameCampaign: function(conn) {
        var sourceNode = this._getNodeData(conn.sourceId);
        var targetNode = this._getNodeData(conn.targetId);
        var sourceNodeUI = $("#" + conn.sourceId);
        conn.sameCampaignAsPreviousNode = targetNode.sameCampaignAsPreviousNode;
        if (targetNode.sameCampaignAsPreviousNode) {
            sourceNodeUI.addClass("same-campaign");
        } else if (sourceNode && !sourceNode.sameCampaignAsPreviousNode) {
            sourceNodeUI.removeClass("same-campaign");
        }
    },

    redraw: function() {
        // clean all before redraw
        $(".columns .column").empty();

        var nodes = this._getNodes();
        // restauration des noeuds
        for (var n in nodes) {
            var node = nodes[n];
            var nodeElem;
            var before;
            if (node.type === "POINT_BEFORE") {
                nodeElem = this._makePointBefore(node);
                before = this._addBeforeColumn(this.columnsElem); // ensure existance
                before.append(nodeElem);
            } else if (node.type === "NODE_BEFORE") {
                nodeElem = this._makeNodeBefore(node);
                before = this._addBeforeColumn(this.columnsElem); // ensure existance
                before.append(nodeElem);
            } else {
                var x = node.x || 0;
                nodeElem = this._makeNode(node, x === 0);
                this._addColumn(this.columnsElem, x + 2); // ensure capacity
                var column = $(".column", this.columnsElem).eq(x);
                column.append(nodeElem);
            }
        }

        // restauration des connexions
        var connections = this.options.data.connections;
        for (var c in connections) {
            var conn = connections[c];
            var connUI = this.jsPlumb.connect({source: conn.sourceId, target: conn.targetId});
            this._updateConnectionLabel(conn, connUI);

            connUI.setPaintStyle(this._getConnectionStyle(conn));
            this._updateSourceNodeSameCampaign(conn);
        }
        if (connections && connections.length) {
            this.options.isCreated = true;
            this._validateCatchCrops();
        }
    },

    /**
     *
     * @param item Dom element or id
     */
    removeNode: function(item) {
        var diagram = this;
        if (jQuery.type( item ) == "string" ) {
            item = $("#" + item);
        }
        var parent = item.parent();
        item.animate({width: 0, height: 0}, function() {
            let mySet = new Set();
            diagram.options.data.connections
                .filter(c => c.targetId === item.attr("id"))
                .map(c => mySet.add(c.sourceId));
            diagram.jsPlumb.remove(item);
            diagram._refresh(parent);
            diagram._removeNode(item.attr("id"));
            mySet.forEach(c => diagram._validConnUiCroppingPlanEntryFrequency(c));
        });
    },

    removeConnection: function(sourceId, targetId) {
        var conn = this._getUIConnection(sourceId, targetId);
        this.jsPlumb.detach(conn);
        this._validConnUiCroppingPlanEntryFrequency(sourceId);
    },

    setConnectionData: function(sourceId, targetId, connData) {
        this._updateConnection(sourceId, targetId, connData);
        var conn = this._getUIConnection(sourceId, targetId);
        conn.setPaintStyle(this._getConnectionStyle(connData));
    },

    repaintEverything: function() {
        this.jsPlumb.repaintEverything();
    },


    models: function(models) {
      if (models) {
          this.options.models = models;
          this._addModel(this.modelsElem, this.options.models);
      } else {
          return this.options.models;
      }
    }
});
