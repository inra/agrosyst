/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

function entitiesUnactivate(unactivateDialog, unactivateForm, activate) {
    var selectionCount = JSON.parse(unactivateForm.children("input[name='entityIds']").val()).length;
    if (selectionCount > 0) {
        unactivateDialog.dialog({
            resizable: false,
            width: 350,
            modal: true,
            buttons: {
                action: {
                  click: function() {
                    $(this).dialog("close");
                    unactivateForm.attr("action", activate ? ENDPOINTS.genericEntitiesActivation+"?activate=true" : ENDPOINTS.genericEntitiesActivation);
                    unactivateForm.submit();
                  },
                  text : activate ? "Activer" : "Désactiver",
                  'class': 'btn-primary'
                },
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}

/**
 * Duplicate single selected domain after asking user for new campaign year
 * and optional info.
 */
function entitiesExport(exportForm, all) {
    exportForm.find('input[name=exportAllEntities]').val(all);
    exportForm.attr("action", ENDPOINTS.runExport);
    exportForm.submit();
}

function entitiesImport () {
  $("#generic-import-form").dialog({
      resizable: false,
      width: $(window).width() * 0.7,
      modal: true,
      buttons: {
          "Importer": {
            click: function() {
              $("#generic-import-form-submit").trigger('click');
              if (!$("#generic-import-form").find("input:invalid")) {
                $(this).dialog("close");
              }
            },
            text: 'Importer',
            'class': 'btn-primary'
          },
          "Annuler": {
            click: function() {
              $(this).dialog("close");
            },
            text: 'Annuler',
            'class': 'float-left btn-secondary'
          }
      }
  });
}
