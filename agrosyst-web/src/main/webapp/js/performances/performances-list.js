/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Affichage de la liste des performances.
 */
AgrosystModule.controller('PerformancesListController', ['$scope', '$timeout', '$http', 'PerformancesListData',
  function ($scope, $timeout, $http, PerformancesListData) {
    // current list displayed in table
    $scope.performances = PerformancesListData.performances.elements;
    // pager for pagination
    $scope.pager = PerformancesListData.performances;
    // selected crop cycles
    $scope.selectedPerformances = {};
    // first selected instance
    //$scope.firstSelectedPerformance;
    // filter data
    $scope.filter = PerformancesListData.performanceFilter;
    $scope.practiced = PerformancesListData.practiced;

    $scope.refresh = function(newValue, oldValue) {
      if (newValue == oldValue) { return; } // prevent init useless call
      displayPageLoading();
      $http.post($scope.endpoints.performancesListJson, "practiced=" + $scope.practiced + "&filter=" + encodeURIComponent(angular.toJson($scope.filter)),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
          $scope.performances = response.data.elements;
          $scope.pager = response.data;
          hidePageLoading();
      })
      .catch(function(response) {
         hidePageLoading();
         console.error("Échec de récupération de la liste des performances", response);
         addPermanentError("Échec de récupération de la liste des performances", response.status);
      });
    };

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0,1);
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.performanceName, filter.growingSystemName, filter.domainName, filter.plotName, filter.zoneName]',
    function(newValue, oldValue) {
      if (newValue == oldValue) { return; } // prevent useless call
      if (timer) {
        $timeout.cancel(timer);
      }
      timer = $timeout(function(){
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      }, 350);
    }, true);
    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.$watch('selectedPerformances', function(newValue, oldValue) {
      // active property and first selected
      if (newValue == oldValue) { return; } // prevent useless call
      angular.forEach($scope.performances, function(item) {
        if ($scope.selectedPerformances[item.topiaId]) {
          $scope.firstSelectedPerformance = item; // make sense when size() == 1
        }
      });
    }, true);
}]);

/**
 * Delete selected performance after user confirm.
 */
function performanceDelete(deleteDialog, deleteForm) {
  var selectionCount = JSON.parse(deleteForm.children("input[name='performanceIds']").val()).length;
  if (selectionCount > 0) {
    deleteDialog.dialog({
      resizable: false,
      width: 400,
      modal: true,
      buttons: {
        "Supprimer": {
          click: function () {
            $(this).dialog("close");
            deleteForm.attr("action", ENDPOINTS.performancesDelete);
            deleteForm.submit();
          },
          text:"Supprimer",
          'class': 'btn-primary'
        },
        "Annuler": {
          click: function() {
            $(this).dialog("close");
          },
          text:"Annuler",
          'class': 'float-left btn-secondary'
        }
      }
    });
  }
}
