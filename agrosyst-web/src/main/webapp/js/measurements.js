/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller("MeasurementsEditController", ["$scope", "$http", "$q", "$window", "$store", "MeasurementsEditInitData",
  function($scope, $http, $q, $window, $store, MeasurementsEditInitData) {
    // la liste des sessions disponibles
    $scope.measurementSessions = MeasurementsEditInitData.measurementSessions;
    // map enum/trad des types de mesures
    $scope.measurementMeasureTypes = MeasurementsEditInitData.measurementMeasureTypes;
    // map enum/trad des types d'observations
    $scope.measurementObservationTypes = MeasurementsEditInitData.measurementObservationTypes;
    // map enum/trad des types de variables
    $scope.variableTypes = $scope.i18n.VariableType;
    // les especes de la zone
    $scope.croppingPlanEntries = MeasurementsEditInitData.croppingPlanEntries;

    //$scope.editedMeasurementSessionForm = null;
    // la session en cours d'edition
    //$scope.editedMeasurementSession;
    // mesure/observation en cours d'edition
    //$scope.editedMeasurement;
    // les types de variables des mesures (colonne B du ref)
    //$scope.refMesureVariableTypes;
    // les mesures filtrées par le type de variable sélectionné
    //// les unités disponibles pour la mesure selectionnée
    //$scope.refMesureUnits;
    // {Map} Selected measurements
    //$scope.selectedMeasurements;
    // {Boolean} localstorage contains mesure/observation to paste
    //
    // options des dates pickers
    $scope.dateOptions = {
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy"
    };

    angular.element(document).ready(
      function rolBackUiDateDirty() {
        $scope.measurementForm.$setPristine();
      }
    );

    // ajout du meta attribut dans l'ui pour différencier une mesure d'une observation
    angular.forEach($scope.measurementSessions, function(measurementSession) {
      angular.forEach(measurementSession.measurements, function(measurement) {
        if (angular.isUndefined(measurement._measure)) {
          measurement._measure = !(measurement.measurementType === "STADE_CULTURE" ||
          measurement.measurementType === "NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES" ||
          measurement.measurementType === "ADVENTICES");
        }
      });

      // try to parse 'startDate' value as Date object
      if (typeof measurementSession.startDate === "string") {
        measurementSession.startDate = new Date(Date.parse(measurementSession.startDate));
      }
      if (typeof measurementSession.endDate === "string") {
        measurementSession.endDate = new Date(Date.parse(measurementSession.endDate));
      }
    });

    // ajout d'une nouvelle session
    $scope.addMeasurementSession = function() {
      if(!$scope.editedMeasurementSessionForm || $scope.editedMeasurementSessionForm.$valid) {
        var newSession = {
            measurements: []
        };
        $scope.measurementSessions.push(newSession);
        $scope.editMeasurementSession(newSession);
      }
    };

    // edition d'une session
    $scope.editMeasurementSession = function(measurementSession) {
      if(!$scope.editedMeasurementSessionForm || $scope.editedMeasurementSessionForm.$valid) {
        // cleanup
        delete $scope.editedMeasurement;

        // unselect session if already selected
        if ($scope.editedMeasurementSession == measurementSession) {
          delete $scope.editedMeasurementSession;
        } else {
          $scope.editedMeasurementSession = measurementSession;

          // refresh session cropping with one of cropping list (ng-options)
          if ($scope.editedMeasurementSession.croppingPlanEntry) {
            angular.forEach($scope.croppingPlanEntries, function(item) {
              if (item.topiaId == $scope.editedMeasurementSession.croppingPlanEntry.topiaId) {
                $scope.editedMeasurementSession.croppingPlanEntry = item;
              }
            });
          }
        }

        // init local storage pasted value
        $scope.selectedMeasurements = {};
        $scope.pastedMeasurements = !!$store.get("effective-measurements");
      }
    };

    $scope.getSpeciesName = function(measurement) {
      var esp = measurement.croppingPlanSpecies;
      if (esp !== undefined) {
        var res = esp.species.libelle_espece_botanique;
        if (esp.variety !== undefined) {
          if (esp.variety.denomination !== undefined) {
            res += ' (' + esp.variety.denomination + ')';
          } else if (esp.variety.label !== undefined) {
            res += ' (' + esp.variety.label + ')';
          }
        }
        return res;
      }
      return '-';
    };

    // mise à jour de la date de début
    $scope.measurementSessionStartDateUpdated = function(beginDate) {
      // remplissage de la date de fin de session avec la date de début
      if (!$scope.editedMeasurementSession.endDate) {
        $scope.editedMeasurementSession.endDate = beginDate;
      }
    };

    // suppression d'une session
    $scope.deleteMeasurementSession = function(measurementSession) {
      if ($window.confirm("Êtes vous sûr de vouloir supprimer cette session ?")) {
        var index = $scope.measurementSessions.indexOf(measurementSession);
        $scope.measurementSessions.splice(index, 1);
        if (measurementSession == $scope.editedMeasurementSession) {
          delete $scope.editedMeasurementSession;
        }
      }
    };

    // ajout d'une mesure (true) ou observation (false)
    $scope.addMeasurement = function(mesure) {
      var measurement = {
          _measure:mesure,
          active: true};
      $scope.editedMeasurementSession.measurements.push(measurement);
      $scope.editMeasurement(measurement);
    };

    // edition d'une mesure ou observation
    $scope.editMeasurement = function(measurement) {
      $scope.editedMeasurement = measurement;
    };

    // suppression d'une observation
    $scope.deleteMeasurement = function(measurementSession, measurement) {
      if ($window.confirm("Êtes vous sûr de vouloir supprimer cette mesure/observation ?")) {
        var index = measurementSession.measurements.indexOf(measurement);
        measurementSession.measurements.splice(index, 1);
        if (measurement == $scope.editedMeasurement) {
          delete $scope.editedMeasurement;
        }
      }
    };

    // edition d'une mesure dans une serie de mesure
    $scope.editMeasurementSessionMeasurement = function(measurementSession, measurement) {
      $scope.editMeasurementSession(measurementSession);
      $scope.editMeasurement(measurement);
    };

    // copy selected measurement into local storage
    $scope.copyMeasurements = function() {
      var measurements = [];
      angular.forEach($scope.selectedMeasurements, function(value, key) {
        if (value) {
          angular.forEach($scope.editedMeasurementSession.measurements, function(measurement) {
            if (key == measurement.$$hashKey) {
              var copy = angular.copy(measurement);
              // remove all identifiers
              delete copy.topiaId;
              // save copy
              measurements.push(copy);
            }
          });
        }
      });

      $store.set("effective-measurements", measurements);
      $scope.selectedMeasurements = {};
      $scope.pastedMeasurements = true;
    };

    // paste measurements from local storage into current session
    $scope.pasteMeasurements = function() {
      var measurements = $store.get("effective-measurements");
      angular.forEach(measurements, function(item) {
        $scope.editedMeasurementSession.measurements.push(item);
      });
      $store.remove("effective-measurements"); // copy only once ?
      $scope.pastedMeasurements = false;
    };
}]);

AgrosystModule.controller("MeasurementsMeasureController", ["$scope", "$http", "$q", "$window", "$log", "MeasurementsEditInitData",
   function($scope, $http, $q, $window, $log, MeasurementsEditInitData) {
     // les types de variables des mesures (colonne B du ref)
     //$scope.refMesureVariableTypes;
     // les mesures filtrées par le type de variable sélectionné
     //$scope.refMesures;
     // les unités disponibles pour la mesure selectionnée
     //$scope.refMesureUnits;
     // les substances actives
     $scope.substanceActives = MeasurementsEditInitData.substanceActives;

     // listener for event on measurement edition
     $scope.$watch("editedMeasurement", function(event) {

       // seulement pour les mesures
       if ($scope.editedMeasurement && $scope.editedMeasurement._measure) {
         // find correct instance for substanceActives
         if ($scope.editedMeasurement.activeSubstance) {
           angular.forEach($scope.substanceActives, function(elt) {
             if (elt.topiaId == $scope.editedMeasurement.activeSubstance.topiaId) {
               $scope.editedMeasurement.activeSubstance = elt;
             }
           });
         }

         // fire mesure events
         if ($scope.editedMeasurement.measurementType) {
           $log.log("Managing event on mesure", $scope.editedMeasurement.measurementType);
           if ($scope.editedMeasurement.refMesure) {
             $scope.editedMeasurement.refMesureVariableTypeItem = $scope.editedMeasurement.refMesure.type_variable_mesuree;
           }

           $scope.measureTypeChanged()
             //.then($scope.measureVariableTypeChanged())
             .then($scope.selectRefMesure())
             .then(function() {
               console.log("all done");
             });
         }
       }
     });

     // changement du type de mesure
     $scope.measureTypeChanged = function() {
       if ($scope.editedMeasurement.measurementType) {
         return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VARIABLE_TYPES_JSON +
           "?measurementType=" + encodeURIComponent($scope.editedMeasurement.measurementType), {cache:true})
           .then(function(response) {
             $scope.refMesureVariableTypes = response.data;
             // auto selection de la valeur par default si unique
             // necessaire pour les types autres que PLANTE/SOL
             if ($scope.refMesureVariableTypes.length == 1) {
               $scope.editedMeasurement.refMesureVariableTypeItem = $scope.refMesureVariableTypes[0];
             }
           })
           .then($scope.measureVariableTypeChanged())
           .catch(function(response) {
              console.error("Échec de récupération des types de variable", response);
              addPermanentError("Échec de récupération des types de variable", response.status);
           });
       } else {
         return $q.defer().promise;
       }
     };

     // changement du type de variable
     $scope.measureVariableTypeChanged = function() {
       if ($scope.editedMeasurement.measurementType && $scope.editedMeasurement.refMesureVariableTypeItem ||
           ($scope.editedMeasurement.measurementType != 'PLANTE' && $scope.editedMeasurement.measurementType != 'SOL')) {
         var params = "?measurementType=" + encodeURIComponent($scope.editedMeasurement.measurementType);
         if ($scope.editedMeasurement.measurementType == 'PLANTE' || $scope.editedMeasurement.measurementType == 'SOL') {
           params += "&variableType=" + encodeURIComponent($scope.editedMeasurement.refMesureVariableTypeItem);
         }
         return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VARIABLES_JSON + params, {cache:true}).
           then(function(response) {
             $scope.refMesures = response.data;

             // reselect new instance
             if ($scope.editedMeasurement.refMesure) {
               angular.forEach($scope.refMesures, function(item) {
                 if ($scope.editedMeasurement.refMesure.topiaId == item.topiaId) {
                   $scope.editedMeasurement.refMesure = item;
                 }
               });
             }
           })
           .catch(function(response) {
              console.error("Échec de récupération des mesures", response);
              addPermanentError("Échec de récupération des mesures", response.status);
           });
       }
     };

     // choix d'un type de mesure
     $scope.selectRefMesure = function() {
       var deferred = $q.defer();
       setTimeout(function() {
         // time out need to be run inside $scope.$apply for angular
         $scope.$apply(function() {
           // split unit list
           $scope.refMesureUnits = [];
           if ($scope.editedMeasurement.refMesure && $scope.editedMeasurement.refMesure.unite) {
             var unitArray = $scope.editedMeasurement.refMesure.unite.split(',');
             angular.forEach(unitArray, function(unit) {
               $scope.refMesureUnits.push(unit.trim());
               deferred.resolve();
             });
             // auto select unique unit value
             if ($scope.refMesureUnits.length == 1) {
               $scope.editedMeasurement.measureUnit = $scope.refMesureUnits[0];
             }
           }
         });
       }, 10);
       return deferred.promise;
     };
 }]);


AgrosystModule.controller("MeasurementsObservationController", ["$scope", "$http", "$q", "$window", "MeasurementsEditInitData",
   function($scope, $http, $q, $window, MeasurementsEditInitData) {
    // {Array} le referentiel adventices
    $scope.adventices = MeasurementsEditInitData.adventices;
    // {Array} les stades edi applicable a l'adventices selectioné ou l'espece sélectionnée
    //$scope.refStadeEdis;
    // {Array} Option d'affichage de l'interface graphique
    //$scope.displayOption;

    // listener for event on measurement edition
    $scope.$watch("editedMeasurement", function(event) {

      // find correct instance for substanceActives
      if ($scope.editedMeasurement.measuredAdventice) {
        angular.forEach($scope.adventices, function(elt) {
          if (elt.topiaId == $scope.editedMeasurement.measuredAdventice.topiaId) {
            $scope.editedMeasurement.measuredAdventice = elt;
          }
        });
      }

      // find correct instance for croppingPlanSpecies
      if ($scope.editedMeasurement.croppingPlanSpecies && $scope.editedMeasurementSession.croppingPlanEntry) {
        angular.forEach($scope.editedMeasurementSession.croppingPlanEntry.croppingPlanSpecies, function(elt) {
          if (elt.topiaId == $scope.editedMeasurement.croppingPlanSpecies.topiaId) {
            $scope.editedMeasurement.croppingPlanSpecies = elt;
          }
        });
      }

      // seulement pour les observations
      if (!$scope.editedMeasurement._measure) {
        // fire mesure events
        if ($scope.editedMeasurement.measurementType) {
          // can be parallelized
          $q.all($scope.mesurementAdventiceChanged(),
            $scope.measurementSpeciesChanged());
        }
      }
    });

    // nouvelle espece sélectionée
    $scope.measurementSpeciesChanged = function() {
      if ($scope.editedMeasurement.croppingPlanSpecies) {
        return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_STADES_JSON +
            "?vegetativeProfile=" + encodeURIComponent($scope.editedMeasurement.croppingPlanSpecies.species.profil_vegetatif_BBCH), {cache:true})
            .then(function(response) {
              $scope.refStadeEdis = response.data;

              // reselect new instance
              angular.forEach($scope.refStadeEdis, function(item) {
                if ($scope.editedMeasurement.cropStageMin && $scope.editedMeasurement.cropStageMin.topiaId == item.topiaId) {
                  $scope.editedMeasurement.cropStageMin = item;
                }
                if ($scope.editedMeasurement.cropStageAverage && $scope.editedMeasurement.cropStageAverage.topiaId == item.topiaId) {
                  $scope.editedMeasurement.cropStageAverage = item;
                }
                if ($scope.editedMeasurement.cropStageMedium && $scope.editedMeasurement.cropStageMedium.topiaId == item.topiaId) {
                  $scope.editedMeasurement.cropStageMedium = item;
                }
                if ($scope.editedMeasurement.cropStageMax && $scope.editedMeasurement.cropStageMax.topiaId == item.topiaId) {
                  $scope.editedMeasurement.cropStageMax = item;
                }
              });
            }).
            catch(function(response) {
              console.error("Échec de récupération des stades de mesure", response);
              addPermanentError("Échec de récupération des stades de mesure", response.status);
            });
      }
    };

    // nouvelle adventices selectionnée pour la mesure
    $scope.mesurementAdventiceChanged = function() {
      if ($scope.editedMeasurement.measuredAdventice) {
        return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_STADES_JSON +
          "?cropFamily=" + encodeURIComponent($scope.editedMeasurement.measuredAdventice.famille_de_culture), {cache:true})
          .then(function(response) {
            $scope.refStadeEdis = response.data;

            // reselect new instance
            if ($scope.editedMeasurement.adventiceStage) {
              angular.forEach($scope.refStadeEdis, function(item) {
                if ($scope.editedMeasurement.adventiceStage.topiaId == item.topiaId) {
                  $scope.editedMeasurement.adventiceStage = item;
                }
              });
            }
          }).
          catch(function(response) {
            console.error("Échec de récupération des stades de mesure", response);
            addPermanentError("Échec de récupération des stades de mesure", response.status);
          });
      }
    };
 }]);

AgrosystModule.controller("MeasurementsObservationVgObsController", ["$scope", "$http",
  function($scope, $http) {
   // {Array} referentiel protocol vgobs labels
   //$scope.protocoleVgObsLabels;
   // {Array} referentiel protocole vgobs pests
   //$scope.protocoleVgObsPests;
   // {Array} referentiel protocole vgobs stades
   //$scope.protocoleVgObsStades;
   // {Array} referentiel protocole vgobs supports
   //$scope.protocoleVgObsSupports;
   // {Array} referentiel protocole vgobs observations
   //$scope.protocoleVgObsObservations;
   // {Array} referentiel protocole vgobs qualitatives
   //$scope.protocoleVgObsQualitatives;
   // {Array} referentiel protocole vgobs unit
   //$scope.protocoleVgObsUnits;
   // {Array} referentiel protocole vgobs qualifier
   //$scope.protocoleVgObsQualifiers;
   // {Object} selected protocoleVgObs data used as filter for json requests
   $scope.vgObsFilter = {};
   // {Array} referentiel nuisibles edi (type)
   //$scope.ediPestTypes;
   // {String} selected type nuisibles edi
   $scope.ediFilter = {};
   // {Array} referentiel nuisibles edi
   //$scope.ediPests;
   // {Array} referentiel nuisibles stade edi
   //$scope.ediPestStades;
   // {Array} referentiel support organe edi
   //$scope.ediSupportOrganes;
   // {Array} referentiel type notation edi
   //$scope.ediTypeNotations;
   // {Array} referentiel valeurs qualitative edi
   //$scope.ediQualitatives;
   // {Array} referentiel protocoleVgObs (unité)
   //$scope.vgObsUnits;
   // {Array} referentiel qualifiant unites
   //$scope.ediQualifierUnits;

   // nouvelle espece sélectionée
   var loadProtocoleVgObsLabels = function() {
     return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSLABELS_JSON, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsLabels = response.data;
         }).
         catch(function(response) {
           console.error("Échec de récupération des VGOBS", response);
           addPermanentError("Échec de récupération des VGOBS", response.status);
         });
   };

   // chargement de la liste des types de nuisibles
   var loadEdiPestTypes = function() {
     return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIPESTTYPES_JSON, {cache:true})
         .then(function(response) {
           $scope.ediPestTypes = response.data;
         }).
         catch(function(response) {
           console.error("Échec de récupération des données EDI type de pesticide", response);
           addPermanentError("Échec de récupération des données EDI type de pesticide", response.status);
         });
   };

   // load protocoleVgObsLabels on first protocolVgObs selection
   $scope.$watch('editedMeasurement.protocolVgObs',
     function(newValue, oldValue) {
       if (newValue && angular.isUndefined($scope.protocoleVgObsLabels)) {
         loadProtocoleVgObsLabels();
       }
       if (!newValue && angular.isUndefined($scope.ediPestTypes)) {
         loadEdiPestTypes();
       }
   });

   // listener for event on measurement edition
   $scope.$watch("editedMeasurement", function(newValue) {
     if (newValue) {
       // clear/init
       if ($scope.editedMeasurement.protocol) {
         $scope.vgObsFilter = {
             label: $scope.editedMeasurement.protocol.protocole_libelle,
             pest: $scope.editedMeasurement.protocol.ligne_organisme_vivant,
             stade: $scope.editedMeasurement.protocol.ligne_stades_developpement,
             support: $scope.editedMeasurement.protocol.ligne_supports_organes,
             observation: $scope.editedMeasurement.protocol.ligne_supports_organes,
             qualitative: $scope.editedMeasurement.protocol.classe_valeur_qualitative,
             unit: $scope.editedMeasurement.protocol.releve_unite
         };
         $scope.protocoleVgObsLabelChanged();
       } else {
         $scope.vgObsFilter = {};
       }

       if ($scope.editedMeasurement.pest) {
         $scope.ediFilter = {
           pestType: $scope.editedMeasurement.pest.reference_param
         };
         $scope.ediPestTypeChanged();
       } else {
         $scope.ediFilter = {};
       }
     }
   });

   // protocole vgobs label changed
   $scope.protocoleVgObsLabelChanged = function() {
     if ($scope.vgObsFilter.label) {
       var filter = angular.toJson({
         label: $scope.vgObsFilter.label
       });
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSPESTS_JSON + "?vgObsFilter=" + filter, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsPests = response.data;
         })
         .then($scope.protocoleVgObsPestChanged)
         .catch(function(response) {
              console.error("Échec de récupération des protocoles VGOBS", response);
              addPermanentError("Échec de récupération des protocoles VGOBS", response.status);
         });
     }
   };

   // protocole vgobs pest changed
   $scope.protocoleVgObsPestChanged = function() {
     delete $scope.protocoleVgObsStades;
     if ($scope.vgObsFilter.pest || $scope.vgObsFilter.pest === "") {
       var filter = angular.toJson({
         label: $scope.vgObsFilter.label,
         pest: $scope.vgObsFilter.pest
       });
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSSTADES_JSON + "?vgObsFilter=" + filter, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsStades = response.data;

           // auto select single value
           if ($scope.protocoleVgObsStades.length == 1) {
             $scope.vgObsFilter.stade = $scope.protocoleVgObsStades[0];
           }
         })
         .then($scope.protocoleVgObsStadeChanged)
         .catch(function(response) {
           console.error("Échec de récupération des stades VGOBS", response);
           addPermanentError("Échec de récupération des stades VGOBS", response.status);
         });
     }
   };

   // protocole vgobs stade changed
   $scope.protocoleVgObsStadeChanged = function() {
     delete $scope.protocoleVgObsSupports;
     if ($scope.vgObsFilter.stade || $scope.vgObsFilter.stade === "") {
       var filter = angular.toJson({
         label: $scope.vgObsFilter.label,
         pest: $scope.vgObsFilter.pest,
         stade: $scope.vgObsFilter.stade
       });
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSSUPPORTS_JSON + "?vgObsFilter=" + filter, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsSupports = response.data;

           // auto select single value
           if ($scope.protocoleVgObsSupports.length == 1) {
             $scope.vgObsFilter.support = $scope.protocoleVgObsSupports[0];
           }
         })
         .then($scope.protocoleVgObsSupportChanged)
         .catch(function(response) {
            console.error("Échec de récupération des mesures VGOBS", response);
            addPermanentError("Échec de récupération des mesures VGOBS", response.status);
         });
     }
   };

   // protocole vgobs support changed
   $scope.protocoleVgObsSupportChanged = function() {
     delete $scope.protocoleVgObsObservations;
     if ($scope.vgObsFilter.support || $scope.vgObsFilter.support === "") {
       var filter = angular.toJson({
         label: $scope.vgObsFilter.label,
         pest: $scope.vgObsFilter.pest,
         stade: $scope.vgObsFilter.stade,
         support: $scope.vgObsFilter.support
       });
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSOBSERVATIONS_JSON + "?vgObsFilter=" + filter, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsObservations = response.data;

           // auto select single value
           if ($scope.protocoleVgObsObservations.length == 1) {
             $scope.vgObsFilter.observation = $scope.protocoleVgObsObservations[0];
           }
         })
         .then($scope.protocoleVgObsObservationChanged)
         .catch(function(response) {
            console.error("Échec de récupération des observations VGOBS", response);
            addPermanentError("Échec de récupération des observations VGOBS", response.status);
         });
     }
   };

   // protocole vgobs observation changed
   $scope.protocoleVgObsObservationChanged = function() {
     delete $scope.protocoleVgObsQualitatives;
     if ($scope.vgObsFilter.observation || $scope.vgObsFilter.observation === "") {
       var filter = angular.toJson({
         label: $scope.vgObsFilter.label,
         pest: $scope.vgObsFilter.pest,
         stade: $scope.vgObsFilter.stade,
         support: $scope.vgObsFilter.support,
         observation: $scope.vgObsFilter.observation
       });
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSQUALITATIVES_JSON + "?vgObsFilter=" + filter, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsQualitatives = response.data;

           // auto select single value
           if ($scope.protocoleVgObsQualitatives.length == 1) {
             $scope.vgObsFilter.qualitative = $scope.protocoleVgObsQualitatives[0];
           }
         })
         .then($scope.protocoleVgObsQualitativeChanged)
         .catch(function(response) {
            console.error("Échec de récupération des données qualitatives VGOBS", response);
            addPermanentError("Échec de récupération des données qualitatives VGOBS", response.status);
         });
     }
   };

   // protocole vgobs qualitative changed
   $scope.protocoleVgObsQualitativeChanged = function() {
     delete $scope.protocoleVgObsUnits;
     if ($scope.vgObsFilter.qualitative || $scope.vgObsFilter.qualitative === "") {
       var filter = angular.toJson({
         label: $scope.vgObsFilter.label,
         pest: $scope.vgObsFilter.pest,
         stade: $scope.vgObsFilter.stade,
         support: $scope.vgObsFilter.support,
         observation: $scope.vgObsFilter.observation,
         qualitative: $scope.vgObsFilter.qualitative
       });
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSUNITS_JSON + "?vgObsFilter=" + filter, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsUnits = response.data;

           // auto select single value
           if ($scope.protocoleVgObsUnits.length == 1) {
             $scope.vgObsFilter.unit = $scope.protocoleVgObsUnits[0];
           }
         })
         .then($scope.protocoleVgObsUnitChanged)
         .catch(function(response) {
           console.error("Échec de récupération des unitées VGOBS", response);
           addPermanentError("Échec de récupération des unitées VGOBS", response.status);
         });
     }
   };

   // protocole vgobs qualitative changed
   $scope.protocoleVgObsUnitChanged = function() {
     delete $scope.protocoleVgObsQualifiers;
     if ($scope.vgObsFilter.unit || $scope.vgObsFilter.unit === "") {
       var filter = angular.toJson($scope.vgObsFilter);
       filter = encodeURIComponent(filter); // for % fail !
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSQUALIFIERS_JSON + "?vgObsFilter=" + filter, {cache:true})
         .then(function(response) {
           $scope.protocoleVgObsQualifiers = response.data;

           // update instance
           if ($scope.editedMeasurement.protocol) {
             angular.forEach($scope.protocoleVgObsQualifiers, function(item) {
               if (item.topiaId == $scope.editedMeasurement.protocol.topiaId) {
                 $scope.editedMeasurement.protocol = item;
               }
             });
           }

           // auto select single value
           if ($scope.protocoleVgObsQualifiers.length == 1) {
             $scope.editedMeasurement.protocol = $scope.protocoleVgObsQualifiers[0];
           }
         })
         .catch(function(response) {
           console.error("Échec de récupération des qualifiants VGOBS", response);
           addPermanentError("Échec de récupération des qualifiants VGOBS", response.status);
         });
     }
   };

   // protocole vgobs label changed
   $scope.ediPestTypeChanged = function() {
     if ($scope.ediFilter.pestType) {
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIPESTS_JSON + "?pestType=" + $scope.ediFilter.pestType, {cache:true})
         .then(function(response) {
           $scope.ediPests = response.data;

           // update instance
           if ($scope.editedMeasurement.pest) {
             angular.forEach($scope.ediPests, function(item) {
               if (item.topiaId == $scope.editedMeasurement.pest.topiaId) {
                 $scope.editedMeasurement.pest = item;
               }
             });
           }
         })
         .then($scope.ediPestChanged)
         .catch(function(response) {
           console.error("Échec de récupération des mesures pesticice EDI", response);
           addPermanentError("Échec de récupération des mesures pesticice EDI", response.status);
         });
     }
   };

   // edi pest changed
   $scope.ediPestChanged = function() {
     if ($scope.editedMeasurement.pest) {
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIPESTSTADES_JSON, {cache:true})
         .then(function(response) {
           $scope.ediPestStades = response.data;

           // update instance
           if ($scope.editedMeasurement.cropOrganismStage) {
             angular.forEach($scope.ediPestStades, function(item) {
               if (item.topiaId == $scope.editedMeasurement.cropOrganismStage.topiaId) {
                 $scope.editedMeasurement.cropOrganismStage = item;
               }
             });
           }
         })
         .then($scope.ediPestStadeChanged)
         .catch(function(response) {
           console.error("Échec de récupération des stades EDI", response);
           addPermanentError("Échec de récupération des stades EDI", response.status);
         });
     }
   };

   // edi pest stage changed
   $scope.ediPestStadeChanged = function() {
     if ($scope.editedMeasurement.cropOrganismStage) {
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDISUPPORTS_JSON, {cache:true})
         .then(function(response) {
           $scope.ediSupportOrganes = response.data;

           // update instance
           if ($scope.editedMeasurement.cropOrganSupport) {
             angular.forEach($scope.ediSupportOrganes, function(item) {
               if (item.topiaId == $scope.editedMeasurement.cropOrganSupport.topiaId) {
                 $scope.editedMeasurement.cropOrganSupport = item;
               }
             });
           }
         })
         .then($scope.ediSupportChanged)
         .catch(function(response) {
           console.error("Échec de récupération des supports EDI", response);
           addPermanentError("Échec de récupération des supports EDI", response.status);
         });
     }
   };

   // edi pest support changed
   $scope.ediSupportChanged = function() {
     if ($scope.editedMeasurement.cropOrganSupport) {
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDINOTATIONS_JSON, {cache:true})
         .then(function(response) {
           $scope.ediTypeNotations = response.data;

           // update instance
           if ($scope.editedMeasurement.cropNotationType) {
             angular.forEach($scope.ediTypeNotations, function(item) {
               if (item.topiaId == $scope.editedMeasurement.cropNotationType.topiaId) {
                 $scope.editedMeasurement.cropNotationType = item;
               }
             });
           }
         })
         .then($scope.ediNotationChanged)
         .catch(function(response) {
           console.error("Échec de récupération des notations EDI", response);
           addPermanentError("Échec de récupération des notations EDI", response.status);
         });
     }
   };

   // edi notation changed
   $scope.ediNotationChanged = function() {
     if ($scope.editedMeasurement.cropNotationType) {
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIQUALITATIVES_JSON, {cache:true})
         .then(function(response) {
           $scope.ediQualitatives = response.data;

           // update instance
           if ($scope.editedMeasurement.cropQualititiveValue) {
             angular.forEach($scope.ediQualitatives, function(item) {
               if (item.topiaId == $scope.editedMeasurement.cropQualititiveValue.topiaId) {
                 $scope.editedMeasurement.cropQualititiveValue = item;
               }
             });
           }
         })
         .then($scope.ediQualitativeChanged)
         .catch(function(response) {
           console.error("Échec de récupération des données qualitatives EDI", response);
           addPermanentError("Échec de récupération des données qualitatives EDI", response.status);
         });
     }
   };

   // edi qualitative changed
   $scope.ediQualitativeChanged = function() {
     if ($scope.editedMeasurement.cropQualititiveValue) {
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIUNITS_JSON, {cache:true})
         .then(function(response) {
           $scope.vgObsUnits = response.data;
         })
         .then($scope.ediUnitChanged)
         .catch(function(response) {
            console.error("Échec de récupération des unitées EDI", response);
            addPermanentError("Échec de récupération des unités EDI", response.status);
         });
     }
   };

   // edi qualitative changed
   $scope.ediUnitChanged = function() {
     if ($scope.editedMeasurement.unitEDI) {
       return $http.get(ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIQUALIFIERUNITS_JSON, {cache:true})
         .then(function(response) {
           $scope.ediQualifierUnits = response.data;

           // update instance
           if ($scope.editedMeasurement.cropUnitQualifier) {
             angular.forEach($scope.ediQualifierUnits, function(item) {
               if (item.topiaId == $scope.editedMeasurement.cropUnitQualifier.topiaId) {
                 $scope.editedMeasurement.cropUnitQualifier = item;
               }
             });
           }
         })
         .catch(function(response) {
            console.error("Échec de récupération des qualifieurs EDI", response);
            addPermanentError("Échec de récupération des qualifieurs EDI", response.status);
         });
     }
   };
}]);
