/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


AgrosystModule.controller('TasksListController', ['$scope', '$http', '$timeout', 'ListTasksInitData', 'ContextFilterInit', 'taskFilter',
  function($scope, $http, $timeout, ListTasksInitData, ContextFilterInit, taskFilter) {
    $scope.tasks = ListTasksInitData.elements;
    $scope.pager = ListTasksInitData;
    $scope.contextFilter = ContextFilterInit;
    $scope.filter = taskFilter;


    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) { return; } // prevent init useless call

      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.filter);

      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));

      displayPageLoading();
      $http.post(ENDPOINTS.adminTaskJson, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          $scope.tasks = response.data.elements;
          $scope.pager = response.data;
      })
      .catch(function(response) {
        delete $scope.tasks;
        delete $scope.pager;
        console.error("Can't get task list", response);
        var message = "Échec de récupération des tâches";
        addPermanentError(message, response.status);
      })
      .finally(hidePageLoading);
    };

    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.stopTask = function(taskId) {
      var performanceAdminForm = $('#taskStopForm');
      performanceAdminForm.attr("action", ENDPOINTS.stopTask + "?taskId=" + encodeURIComponent(taskId));
      performanceAdminForm.submit();
    };

}]);

AgrosystModule.controller('MaintenanceModeEditController', ['$scope', '$http', 'active', 'disconnectAllUsers', 'connectedUsersNb',
  function($scope, $http, active, disconnectAllUsers, connectedUsersNb) {
    $scope.active = active;
    $scope.disconnectAllUsers = disconnectAllUsers;
    $scope.connectedUsersNb = connectedUsersNb;
  }
]);

AgrosystModule.controller('PerformanceStatisticsListController', ['$scope', '$http', 'performanceStatistics',
  function($scope, $http, performanceStatistics) {
    $scope.performanceStatistics = performanceStatistics;

    $scope.refresh = function(performanceId) {

      var ajaxRequest = "performanceId=" + encodeURIComponent(performanceId);

      $http.post(ENDPOINTS.adminPerformancesJson, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
          var stat = response.data[0];
          angular.forEach($scope.performanceStatistics, function(ps){
            if (ps.performanceId === stat.performanceId) {
              ps.endAt = stat.endAt;
              ps.endAtTime = stat.endAtTime;
              ps.nbTaskPerformed = stat.nbTaskPerformed;
              ps.nbPracticedPracticed = stat.nbPracticedPracticed;
              ps.nbEffectiveSdc = stat.nbEffectiveSdc;
              ps.performanceState = stat.performanceState;
            }
          });
      }).
      catch(function(response) {
        delete $scope.tasks;
        delete $scope.pager;
        console.error("Can't get performance statistics", response);
        var message = "Échec de récupération des statistiques";
        addPermanentError(message, response.status);
      });
    };

    /**
     * Delete selected performance after user confirm.
     */
    $scope.delete = function (perf) {
      var performanceId = perf.performanceId;
      var deleteDialog = $('#confirmDeletePerformances');
      deleteDialog.dialog({
        resizable: false,
        width: 400,
        modal: true,
        buttons: {
          "Supprimer": {
            click: function () {
              $(this).dialog("close");
                displayPageLoading();
                var performanceIds = [];
                performanceIds.push(performanceId);
                var ajaxRequest = "performanceIds=" + encodeURIComponent(angular.toJson(performanceIds));
                $http.post(ENDPOINTS.performancesDelete, ajaxRequest,
                        {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
                then(function(response) {
                  var indexOf = $scope.performanceStatistics.indexOf(perf);
                  $scope.performanceStatistics.splice(indexOf, 1);
                  hidePageLoading();
                  var message = "Performance supprimée";
                  addSuccessMessage(message, response.status);
                }).catch(function(response) {
                     delete $scope.tasks;
                     delete $scope.pager;
                     console.error("Can't delete performance", response);
                     hidePageLoading();
                     var message = "Échec de suppression de la performance";
                     addPermanentError(message, response.status);
                   });;
            },
            text:"Supprimer",
            'class': 'btn-primary'
          },
          "Annuler": {
            click: function() {
              $(this).dialog("close");
            },
            text:"Annuler",
            'class': 'float-left btn-secondary'
          }
        }
      });

    }

}]);
