/* %%Ignore-License
 * Copyright (c) 2008-2015 Institut National de l'Information Geographique et Forestiere (IGN) France.
 * Released under the BSD license.
 */
/*---------------------------------------------------------
 *Nouvelle classe de Leaflet pour supporter les flux WMTS (basée sur L.TileLayer.WMS)
 *New Leaflet's class to support WMTS (based on L.TileLayer.WMS)
 */
L.TileLayer.WMTS = L.TileLayer.extend({

        defaultWmtsParams: {
                service: 'WMTS',
                request: 'GetTile',
                version: '1.0.0',
                layer: '',
                style: '',
                tilematrixSet: '',
                format: 'image/jpeg'
        },

        initialize: function (url, options) { // (String, Object)
                this._url = url;
                var wmtsParams = L.extend({}, this.defaultWmtsParams),
                    tileSize = options.tileSize || this.options.tileSize;
                if (options.detectRetina && L.Browser.retina) {
                        wmtsParams.width = wmtsParams.height = tileSize * 2;
                } else {
                        wmtsParams.width = wmtsParams.height = tileSize;
                }
                for (var i in options) {
                        // all keys that are not TileLayer options go to WMTS params
                        if (!this.options.hasOwnProperty(i) && i!="matrixIds") {
                                wmtsParams[i] = options[i];
                        }
                }
                this.wmtsParams = wmtsParams;
                this.matrixIds = options.matrixIds;
                L.setOptions(this, options);
        },

        onAdd: function (map) {
                L.TileLayer.prototype.onAdd.call(this, map);
        },

        getTileUrl: function (tilePoint, zoom) { // (Point, Number) -> String
                var map = this._map;
                var crs = map.options.crs;
                var tileSize = this.options.tileSize;
                zoom=map.getZoom();
                var ident = this.matrixIds[zoom].identifier;
                var url = L.Util.template(this._url, {s: this._getSubdomain(tilePoint)});
                return url + L.Util.getParamString(this.wmtsParams, url) + "&tilematrix=" + ident + "&tilerow=" + tilePoint.y +"&tilecol=" + tilePoint.x ;
        },

        setParams: function (params, noRedraw) {
                L.extend(this.wmtsParams, params);
                if (!noRedraw) {
                        this.redraw();
                }
                return this;
        }
});

L.tileLayer.wtms = function (url, options) {
        return new L.TileLayer.WMTS(url, options);
};
/* Fin / End
 *---------------------------------------------------------*/
