/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

function editEntityRoles(roleType, entityCode, title) {
    if (roleType && entityCode) {
        var editEntityRoleDiv = $("<div></div>");
        editEntityRoleDiv.html("Chargement ...");
        editEntityRoleDiv.dialog({
            modal : true,
            open : function() {
                $(this).load(ENDPOINTS.editEntityRolesRaw, {
                    roleType : roleType,
                    entityCode : entityCode
                });
            },
            width : $(window).width() * 0.9,
            height : "auto",
            title : title,
            close: function () {
                // need to remove dialog from dom from angular to it fine on second opening
                $(this).dialog('destroy').remove();
            },
            buttons: {
                cancel: {
                    text: "Annuler",
                    'class': 'btn-secondary',
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                save: {
                    text: "Enregistrer",
                    'class': 'btn-primary',
                    click: function() {
                        var dialog = $(this);
                        var request = {
                            roleType: $(this).find("input[name='roleType']").val(),
                            entityCode: $(this).find("input[name='entityCode']").val(),
                            rolesJson: $(this).find("input[name='rolesJson']").val()
                        };
                        $.post(ENDPOINTS.saveEntityRoles, request, function (data, textStatus, jqXHR) {
                            if (textStatus == "success") {
                                addSuccessMessage("Les rôles ont bien été enregistrés");
                                dialog.dialog("close");
                            } else {
                                window.alert("Erreur");
                            }
                        });
                    }
                }
            }
        });
    }
}

AgrosystModule.controller('EditEntityRolesController', ['$scope', '$window', '$http', 'editEntityRolesInitData',
    function($scope, $window, $http, editEntityRolesInitData) {
        // DOMAIN_RESPONSIBLE, GROWING_PLAN_RESPONSIBLE or GS_DATA_PROCESSOR
        $scope.roleType = editEntityRolesInitData.roleType;
        // domain's, growingPlan's or growingSystem's code
        $scope.entityCode = editEntityRolesInitData.entityCode;
        // List<UserRoleDto>
        $scope.roles = editEntityRolesInitData.roles;
        // Map<RoleType, String> : translations
        $scope.roleTypes = editEntityRolesInitData.roleTypes;
        // List<Integer> : non empty only for GS_DATA_PROCESSOR roleType
        $scope.availableCampaigns = editEntityRolesInitData.availableCampaigns;
        // boolean
        $scope.readOnly = editEntityRolesInitData.readOnly;

        // Le rôle actuellement en édition
        //$scope.selectedUserRole;
        // Liste des utilisateurs affectables (la liste sera chargée uniquement à la première utilisation)
        //$scope.availableUsers;
        // Labels du bouton d'ajout en fonction du type de rôle
        $scope.addLabel = {
            DOMAIN_RESPONSIBLE: "Ajouter un responsable domaine",
            GROWING_PLAN_RESPONSIBLE: "Ajouter un responsable dispositif",
            GS_DATA_PROCESSOR: "Ajouter un exploitant système de culture"
        };

        $scope.addUserRole = function() {
            var newUserRole = { entity: {}, user: {}, type: $scope.roleType };
            $scope.roles.push(newUserRole);

            $scope.selectUserRole(newUserRole);
        };

        $scope.deleteUserRole = function(userRole) {
            if ($window.confirm("Êtes-vous sûr de vouloir retirer ce rôle à l'utilisateur ?")) {
                if ($scope.selectedUserRole === userRole) {
                    delete $scope.selectedUserRole;
                }
                var indexOf = $scope.roles.indexOf(userRole);
                $scope.roles.splice(indexOf, 1);
            }
        };

        $scope.selectUserRole = function(userRole) {
            $scope.selectedUserRole = userRole;
            if (!$scope.availableUsers) {
                $scope.loadUsers();
            } else {
                $scope.parseAvailableUsers();
            }
        };

        $scope.unSelectUserRole = function() {
            delete $scope.selectedUserRole;
        };

        $scope.isSelectedUserRole = function(userRole) {
            return $scope.selectedUserRole === userRole;
        };

        $scope.loadUsers = function() {
            delete $scope.availableUsers;
            $http.post(ENDPOINTS.usersListAllJson, "", {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
            ).then(function(response) {
                if (response.data && response.data != "null") {
                    $scope.availableUsers = response.data;
                    $scope.parseAvailableUsers();
                }
            });
        };

        $scope.parseAvailableUsers = function() {
            angular.forEach($scope.availableUsers, function(user) {
                if (!user.groupActive) {
                    user.groupActive = "Utilisateurs " + (user.active ? "" : "in") + "actifs";
                }
                if ($scope.selectedUserRole && $scope.selectedUserRole.user && $scope.selectedUserRole.user.topiaId && $scope.selectedUserRole.user.topiaId == user.topiaId) {
                    $scope.selectedUserRole.user = user;
                }
            });
        };
    }
]);
