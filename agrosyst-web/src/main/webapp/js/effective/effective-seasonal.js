/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Controller d'edition des cycles assolés.
 */
AgrosystModule.controller('EffectiveCropCyclesSeasonnalController', ['$scope',
  function ($scope) {
    // le noeud sélectionné et en cours d'edition
    //$scope.selectedCropCycleNode;
    // la connexion sélectionnée et en cours d'edition
    //$scope.selectedCropCycleConnection;
    // les espèces a afficher pour le noeud (la culture) sélectionné
    //$scope.selectedCropCycleNodeSpecies;
    //
    //$scope.previousSelectedCropCycleConnection = null;

    // initialise le diagrams js (jquery ready)
    $scope.initChartJs = function() {
      if ($scope.seasonalCropCycles.length > 0) {
        var firstCycle = $scope.seasonalCropCycles[0];
        if ($scope.croppingPlanModel) {
          $("#cropCycleDiagramDiv").cropCycleDiagram("models", $scope.croppingPlanModel);
        }
        let nodeDtos = firstCycle.nodeDtos;
        if (nodeDtos) {
          nodeDtos.forEach(node => {
            let model = $scope.croppingPlanModel.find(m => m.croppingPlanEntryId === node.croppingPlanEntryId);
            if (model) {
              node.mixSpecies = model.mixSpecies;
              node.mixVariety = model.mixVariety;
              node.mixCompanion = model.mixCompanion;
              node.catchCrop = model.catchCrop;
            }

          })
        }
        $("#cropCycleDiagramDiv").cropCycleDiagram("data", {
          "nodes": nodeDtos,
          "connections": firstCycle.connectionDtos
        });
      }
    };

    // creation d'un nouveau diagram
    $scope.createEffectiveSeasonalCropCycle = function() {
      $scope.seasonalCropCycles.push({
        nodeDtos: [],
        connectionDtos: []
      });
      $("#cropCycleDiagramDiv").cropCycleDiagram("models", $scope.croppingPlanModel);
      $("#cropCycleDiagramDiv").cropCycleDiagram('redraw');
    };

    // called by jquery when user modify charjs (new nodes, new connections...)
    $scope.setData = function(data) {
      var firstCycle = $scope.seasonalCropCycles[0];
      firstCycle.nodeDtos = data.nodes;
      firstCycle.connectionDtos = data.connections;
      $scope.$apply();
      $scope.effectiveCropCyclesEditForm.$setDirty();
    };

    // util method, reset all
    var clearAll = function() {
      delete $scope.selectedCropCycleNode;
      delete $scope.selectedCropCycleNodeSpecies;
      delete $scope.selectedCropCycleConnection;
    };

    $scope.initSeasonalCropCycleTab = function () {
      clearAll();
      $scope.$apply();
    };

    // node selected in chart (called by jquery)
    $scope.setSelectedNode = function(selectedCropCycleNode) {
      clearAll();
      $scope.selectedCropCycleNode = selectedCropCycleNode;

      // set list of species for the selected node
      if ($scope.croppingPlanEntriesIndex && angular.isDefined($scope.croppingPlanEntriesIndex[selectedCropCycleNode.croppingPlanEntryId])){
          $scope.selectedCropCycleNodeSpecies = $scope.croppingPlanEntriesIndex[selectedCropCycleNode.croppingPlanEntryId].croppingPlanSpecies;
      }

      $scope.$apply(); // force le refresh
    };

    // update chart on node data change
    $scope.updateNode = function() {};

    $scope._removeNodeImpact = function(node) {
      var messages;

      var impactedInterventions = [];

      // look for impact on current node
      var interventions = node.interventions;
      if (interventions) {
        angular.forEach(interventions, function(intervention){
          impactedInterventions.push(intervention);// add standard and intermediate intervention
        });
      }

      // next intermediate crop intervention must also be removed as the connection will be
      var nodes = $scope.seasonalCropCycles[0].nodeDtos;
      var nextNodeIndex = nodes.indexOf(node) + 1;
      var nextNode = nodes[nextNodeIndex];

      if (nextNode) {
        if (nextNode.interventions) {
          angular.forEach(nextNode.interventions, function(intervention){
            if (intervention.intermediateCrop) {// intermediate connection interventions are located into next node and identified as intervention.intermediateCrop
              impactedInterventions.push(intervention);
            }
          });
        }
      }

      var ngInt = impactedInterventions.length;

      if (ngInt > 0) {

        var singMes = "1 intervention sera supprimée.\n\n";
        var plurMes = ngInt + " interventions seront supprimées.\n\n";

        messages = ngInt === 1 ? singMes : plurMes;
        messages = messages + _computeSpeciesChangeImpactOnInterventions(false, impactedInterventions);
      }

      return messages;
    };

    $scope.removeNextNodeIntermediateInterventions = function (cropCycleNode){
      var nextNodeIndex = $scope.seasonalCropCycles[0].nodeDtos.indexOf(cropCycleNode) + 1;
      var nextNode = $scope.seasonalCropCycles[0].nodeDtos[nextNodeIndex];
      if (nextNode) {
        if (nextNode.interventions) {
          var nextNodeInterventions = [];
          angular.forEach(nextNode.interventions, function(intervention){
            if (!intervention.intermediateCrop) {
              nextNodeInterventions.push(intervention);
            }
          });
          nextNode.interventions = nextNodeInterventions;
        }
      }
    };

    // delete node
    $scope.deleteCropCycleNode = function(cropCycleNode) {

      $scope.seasonalCropChangeContext = {confirm : true, cropCycleNode:cropCycleNode};
      $scope.seasonalCropChangeContext.warningMessages = $scope._removeNodeImpact(cropCycleNode);

      var deleteCropCycleNodeModel = {
        process: function() {
          $("#cropCycleDiagramDiv").cropCycleDiagram("removeNode", $scope.seasonalCropChangeContext.cropCycleNode.nodeId);
          $scope.removeNextNodeIntermediateInterventions(cropCycleNode);
          clearAll();
          delete $scope.seasonalCropChangeContext;
        },
        cancelChanges: function() {
          delete $scope.seasonalCropChangeContext;
        }
      };

      if ($scope.seasonalCropChangeContext.warningMessages) {
        _displayConfirmDialog($scope, $("#confirmSeasonalCropChange"), 1024, deleteCropCycleNodeModel);
      } else {
        deleteCropCycleNodeModel.process();
      }
    };

    // new connection selected on chart (called by jquery)
    $scope.setSelectedConnection = function(selectedCropCycleConnection) {
      clearAll();
      $scope.selectedCropCycleConnection = selectedCropCycleConnection;
      if (angular.isDefined($scope.selectedCropCycleConnection.intermediateCroppingPlanEntryId)) {
          // search in intermediateCroppingPlanModel collection, item with cropping plan entry id
          angular.forEach($scope.intermediateCroppingPlanModel, function(effectiveCropCyclesIntermediateModel) {
            if (effectiveCropCyclesIntermediateModel.croppingPlanEntryId == $scope.selectedCropCycleConnection.intermediateCroppingPlanEntryId) {
              $scope.selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry = effectiveCropCyclesIntermediateModel;
            }
          });
      }
      $scope.$apply(); // force le refresh
    };

    $scope._changeDiagramConnexionInfo = function(cropCycleConnection, croppingPlanEntryId, cropCycleConnectionLabel) {
      if (croppingPlanEntryId && angular.isDefined(cropCycleConnectionLabel)) {
          cropCycleConnection.intermediateCroppingPlanEntryId = croppingPlanEntryId;
          cropCycleConnection.label = "<b>CI</b><span class='hover-infos'>" + cropCycleConnectionLabel + "</span>";
      } else {
          delete cropCycleConnection.intermediateCroppingPlanEntryId;
          cropCycleConnection.label = "";
          delete $scope.selectedCropCycleConnection.intermediateCroppingPlanEntryId;
          delete $scope.selectedCropCycleConnection.intermediateCroppingPlanEntryName;
      }

      $("#cropCycleDiagramDiv").cropCycleDiagram("setConnectionData", cropCycleConnection.sourceId, cropCycleConnection.targetId, cropCycleConnection);

    };

    // update chart when connection data changes
    $scope.updateConnection = function(cropCycleConnection, croppingPlanItem) {
      var croppingPlanEntryId;
      var cropCycleConnectionLabel;
      if (croppingPlanItem) {
        croppingPlanEntryId = croppingPlanItem.croppingPlanEntryId;
        cropCycleConnectionLabel = croppingPlanItem.label;
      }
      $scope._changeDiagramConnexionInfo(cropCycleConnection, croppingPlanEntryId, cropCycleConnectionLabel);
    };

    $scope._doUpdateConnection = function (cropCycleConnection, intermediateCrop, matchingSpeciesByCode) {
      if (cropCycleConnection){

        var isFromIntermediateCrop = angular.isDefined(cropCycleConnection.intermediateCroppingPlanEntryId);

        // update intermediate crop code
        var intermediateCroppingPlanEntryId;
        if (intermediateCrop) {
          intermediateCroppingPlanEntryId = intermediateCrop.croppingPlanEntryId;
        }
        cropCycleConnection.intermediateCroppingPlanEntryId = intermediateCroppingPlanEntryId;
        var isToIntermediateCrop = angular.isDefined(cropCycleConnection.intermediateCroppingPlanEntryId);
        _doConnectionIntermediateCropChange(cropCycleConnection, isFromIntermediateCrop, isToIntermediateCrop, matchingSpeciesByCode);
      }

      $scope.updateConnection(cropCycleConnection, cropCycleConnection.selectedIntermediateCroppingPlanEntry);
    };

    $scope._validUpdateConnection = function(cropCycleConnection, intermediateCrop, matchingSpeciesByCode) {
      var warningMessages;
      if (cropCycleConnection){
        var isFromIntermediateCrop = cropCycleConnection.intermediateCroppingPlanEntryId;
        var isToIntermediateCrop = intermediateCrop && intermediateCrop.croppingPlanEntryId;
        warningMessages = _getConnectionIntermediateCropChangeImpact(cropCycleConnection, isFromIntermediateCrop, isToIntermediateCrop, matchingSpeciesByCode);
      }
      return warningMessages;
    };

    // critere de correspondance des especes
    var isMatchingSpeciesForCopyPaste = function(species1, species2) {
      return species1.code_espece_botanique == species2.code_espece_botanique;
    };

    // retourne une map de correspondance entre les anciens code especes et les nouvelles especes
    window._findMatchingSpecies = function(fromSpecies, toSpecies) {
      var matchingSpeciesByCode = {};

      var toSpeciesCopy = angular.copy(toSpecies); // to delete them
      angular.forEach(fromSpecies, function(fSpecies) {

        var matchingSpecies = null;

        for(var i = 0; i < toSpeciesCopy.length; i++){
          var tSpecies = toSpeciesCopy[i];
          if (isMatchingSpeciesForCopyPaste(fSpecies, tSpecies)) {
            matchingSpecies = tSpecies;
            break;
          }
        }
//        not supported by IE11
//        var matchingSpecies = toSpeciesCopy.find(function(tSpecies) {
//          return isMatchingSpeciesForCopyPaste(fSpecies, tSpecies);
//        });
        if (matchingSpecies) {
          matchingSpeciesByCode[fSpecies.code] = matchingSpecies;
          // and remove it to not match twice
          var indexOf = toSpeciesCopy.indexOf(matchingSpecies);
          toSpeciesCopy.splice(indexOf, 1);
        }
      });

      return matchingSpeciesByCode;
    };

    $scope._confirmSelectedIntermediateCroppingPlanEntryChange = function(connection, newCroppingPlanEntry, previousCroppingPlanEntry, isCropRemove) {
      $scope.seasonalCropChangeContext = {confirm : true, isCropRemove:isCropRemove};
      var fromSpecies = [];
      var toSpecies = [];
      if (newCroppingPlanEntry) {
        var nvCroppingPlanEntry = $scope.croppingPlanEntriesIndex[newCroppingPlanEntry.croppingPlanEntryId];
        toSpecies = angular.isDefined(nvCroppingPlanEntry) ? nvCroppingPlanEntry.croppingPlanSpecies : [];
      }
      if (previousCroppingPlanEntry) {
        var prCroppingPlanEntry = $scope.croppingPlanEntriesIndex[previousCroppingPlanEntry.croppingPlanEntryId];
        fromSpecies = angular.isDefined(prCroppingPlanEntry) ? prCroppingPlanEntry.croppingPlanSpecies : [];
      }

      var previousCropCode = previousCroppingPlanEntry ? previousCroppingPlanEntry.code : null;
      var newCropCode = newCroppingPlanEntry ? newCroppingPlanEntry.code : null;
      var matchingSpeciesByCode = _findMatchingSpecies(fromSpecies, toSpecies);

      $scope.seasonalCropChangeContext.warningMessages = $scope._validUpdateConnection(connection, newCroppingPlanEntry, matchingSpeciesByCode);

      var selectedIntermediateCroppingPlanEntryChangeModel = {
        process: function() {
          $scope._doUpdateConnection(connection, newCroppingPlanEntry, matchingSpeciesByCode);
          // force to use same angular object
          connection.selectedIntermediateCroppingPlanEntry = newCroppingPlanEntry;
          if (isCropRemove) {
            // update model
            var firstCycle = $scope.seasonalCropCycles[0];
            var indexOf = firstCycle.connectionDtos.indexOf(connection);
            firstCycle.connectionDtos.splice(indexOf, 1);
            // fire jquery
            $("#cropCycleDiagramDiv").cropCycleDiagram("removeConnection", connection.sourceId, connection.targetId);
            clearAll();
          } else {
            $scope.selectedCropCycleConnection = connection;
          }
          this.next();
        },
        cancelChanges: function() {
          connection.selectedIntermediateCroppingPlanEntry = previousCroppingPlanEntry;
          $scope.selectedCropCycleConnection = connection;
          this.next();
        },
        next: function() {
          delete $scope.seasonalCropChangeContext;
          delete $scope.previousSelectedCropCycleConnection;
        }
      };

      if ($scope.seasonalCropChangeContext.warningMessages) {
        _displayConfirmDialog($scope, $("#confirmSeasonalCropChange"), 1024, selectedIntermediateCroppingPlanEntryChangeModel);
      } else {
        selectedIntermediateCroppingPlanEntryChangeModel.process();
      }

      delete $scope.previousSelectedCropCycleConnection;
    };

    $scope.savePreviousSelectedIntermediateCroppingPlanEntry = function() {
      $scope.previousSelectedCropCycleConnection = true;
    };

    $scope.$watch("selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry", function(newCroppingPlanEntry, previousCroppingPlanEntry) {
      if ($scope.previousSelectedCropCycleConnection) {
        if (newCroppingPlanEntry !== previousCroppingPlanEntry) {
          $scope._confirmSelectedIntermediateCroppingPlanEntryChange($scope.selectedCropCycleConnection, newCroppingPlanEntry, previousCroppingPlanEntry, false);
        }
      }
    });

    $scope._getNodeFromNodeId = function(researchedNodeId) {
      var result;
      if ($scope.seasonalCropCycles && researchedNodeId) {
        angular.forEach($scope.seasonalCropCycles,function(seasonalCropCycle) {
          var nodes = seasonalCropCycle.nodeDtos;
          if (nodes && !result) {
            angular.forEach(nodes, function(node){
              if (!result && (node.nodeId === researchedNodeId)) {
                result = node;
              }
            });
          }
        });
      }
      return result;
    };

    // ATTENTION : déclarée sur la window, utilisée en global
    window._getCroppingPlanEntrySpeciesFromCropCycleConnection = function(cropCycleConnection) {
      var croppingPlanEntrySpecies = [];
      if (cropCycleConnection){
        var croppingPlanEntry;
        if(cropCycleConnection.intermediateCroppingPlanEntryId){
          // create species stades for intermediate crops
          croppingPlanEntry = $scope.croppingPlanEntriesIndex[cropCycleConnection.intermediateCroppingPlanEntryId];
          if (croppingPlanEntry) {
            croppingPlanEntrySpecies = croppingPlanEntry.croppingPlanSpecies;
          }
        } else {
          var nodeId = cropCycleConnection.targetId;
          var node = $scope._getNodeFromNodeId(nodeId);
          if (node && node.croppingPlanEntryId) {
            croppingPlanEntry = $scope.croppingPlanEntriesIndex[node.croppingPlanEntryId];
            if (croppingPlanEntry) {
              croppingPlanEntrySpecies = croppingPlanEntry.croppingPlanSpecies;
            }
          }
        }
      }
      return croppingPlanEntrySpecies;
    };

    // ATTENTION : déclarée sur la window, utilisée en global
    window._getCropCycleConnectionInterventions = function(cropCycleConnection) {
      var result = {affected : [], nonAffected : []};

      if (cropCycleConnection){
        var nodeId = cropCycleConnection.targetId;
        var node = $scope._getNodeFromNodeId(nodeId);
        var interventions = node.interventions;
        if (node && interventions) {
          angular.forEach(interventions, function(intervention){
            if (intervention.intermediateCrop) {
              result.affected.push(intervention);
            } else {
              result.nonAffected.push(intervention);
            }
          });
        }
      }
      return result;
    };

    // ATTENTION : déclarée sur la window, utilisée en global
    window._getInterventionsRelatedToConnection = function(cropCycleConnection) {
      var interventions;
      if (cropCycleConnection) {
        var nodeId = cropCycleConnection.targetId;
        if ($scope.seasonalCropCycles[0] && $scope.seasonalCropCycles[0].nodeDtos) {
          angular.forEach($scope.seasonalCropCycles[0].nodeDtos, function(node){
            if (node.nodeId === nodeId) {
              interventions = node.interventions;
            }
          });
        }
      }
      return interventions;
    };

    // delete current selected connection (not called back by jquery)
    $scope.deleteCropCycleConnection = function(connection) {
      var intermediateCroppingPlanEntry = null;
      if (connection.intermediateCroppingPlanEntryId) {
        intermediateCroppingPlanEntry = $scope.croppingPlanEntriesIndex[connection.intermediateCroppingPlanEntryId];
      }
      $scope._confirmSelectedIntermediateCroppingPlanEntryChange(connection, intermediateCroppingPlanEntry, connection.selectedIntermediateCroppingPlanEntry, true);
    };
}]);
