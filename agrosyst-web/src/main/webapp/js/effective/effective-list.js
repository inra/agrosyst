/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Affichage de la liste des zones.
 */

function effectiveMeasurementsExport(exportForm) {
    var selectionCount = JSON.parse(exportForm.children("input[name='selectedEffectiveCropCycleIds']").val()).length;
    if (selectionCount >= 1) {
      exportForm.attr("action", ENDPOINTS.effectiveMeasurementsExport);
      exportForm.submit();
    }
}

function effectiveCropCyclesExport(exportForm) {
    var selectionCount = JSON.parse(exportForm.children("input[name='selectedEffectiveCropCycleIds']").val()).length;
    if (selectionCount >= 1) {
      exportForm.attr("action", ENDPOINTS.effectiveCropCyclesExport);
      exportForm.submit();
    }
}

AgrosystModule.controller('EffectiveCropCyclesListController', ['$scope', '$http', '$timeout', '$filter', 'EffectiveCropCyclesInitData', 'asyncThreshold', 'measurementAsyncThreshold',
  function ($scope, $http, $timeout, $filter, EffectiveCropCyclesInitData, asyncThreshold, measurementAsyncThreshold) {
    // current list displayed in table
    $scope.effectiveCropCycles = EffectiveCropCyclesInitData.zoneListResult.elements;
    $scope.measurement = EffectiveCropCyclesInitData.measurement;
    // selected crop cycles
    $scope.selectedEntities = {};
    $scope.allSelectedEntities = [];
    $scope.asyncThreshold = asyncThreshold;
    $scope.measurementAsyncThreshold = measurementAsyncThreshold;

    // pager for pagination
    $scope.pager = EffectiveCropCyclesInitData.zoneListResult;
    // filter data
    $scope.filter = EffectiveCropCyclesInitData.plotFilter;
    $scope.filter.zoneState = $scope.filter.zoneState ? "true" : "false";
    $scope.filter.plotState = $scope.filter.plotState ? "true" : "false";

    $scope.relatedDomainIds = [];
    $scope.plotZoneActivationEnable = true;
    $scope.relatedDomainIdZones = {};

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {zoneState : "true", plotState : "true"};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0,1);
    };

    $scope.toggleSelectedEntities = function() {
      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.effectiveCropCycles);
      $scope.relatedDomainIds = [];
      $scope.relatedDomainIdZones = {};
      $scope.effectiveCropCycles.map(zone => {
        let domainId = zone.plot.domain.topiaId;
        if ($scope.selectedEntities[zone.topiaId]){
          if ($scope.relatedDomainIds.indexOf(domainId) == -1) {
            $scope.relatedDomainIds.push(domainId);
          }
          let zoneIds = $scope.relatedDomainIdZones[domainId];
          if (!zoneIds) {
            zoneIds = [];
            $scope.relatedDomainIdZones[domainId] = zoneIds;
          }
          let entityId = zone.topiaId;
          if (zoneIds.indexOf(entityId) == -1) {
            zoneIds.push(entityId);
          }
        }
      });
    };

    $scope.toggleSelectedEntity = function(entity) {
      let entityId = entity.topiaId;
      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);

      let domainId = entity.plot.domain.topiaId;
      let zoneIds = $scope.relatedDomainIdZones[domainId];
      if (!$scope.selectedEntities[entityId]) {
        if (!zoneIds) {
          zoneIds = [];
          $scope.relatedDomainIdZones[domainId] = zoneIds;
        }
        if (zoneIds.indexOf(entityId) == -1) {
          zoneIds.push(entityId);
          if ($scope.relatedDomainIds.indexOf(domainId) == -1) {
            $scope.relatedDomainIds.push(domainId);
          }
        }
      } else if (zoneIds && zoneIds.indexOf(entityId) !== -1) {
        let index = zoneIds.indexOf(entityId);
        zoneIds.splice(index, 1);
        if (zoneIds.length === 0 && $scope.relatedDomainIds.indexOf(domainId) !== -1) {
          let index = $scope.relatedDomainIds.indexOf(domainId);
          $scope.relatedDomainIds.splice(index, 1)
        }
      }
    };

    $scope.clearSelection = function() {
      $scope.selectedEntities = {};
      $scope.allSelectedEntities = [];
      $scope.relatedDomainIds = [];
    };

    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) return; // prevent init useless call

      // don't send empty departement filter
      if ($scope.filter.departement === '') {
        delete $scope.filter.departement;
      }

      if ($scope.filter.zoneState === '') {
        delete $scope.filter.zoneState;
      }

      if ($scope.filter.plotState === '') {
        delete $scope.filter.plotState;
      }

      var ajaxRequest = "measurement=" + $scope.measurement + "&filter=" + encodeURIComponent(angular.toJson($scope.filter));
      $http.post(ENDPOINTS.effectiveCropCyclesListJson, ajaxRequest,
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
          $scope.effectiveCropCycles = response.data.elements;
          $scope.pager = response.data;
      })
      .catch(function(response) {
         console.error("Échec de récupération de cycles de culture du réalisés", response);
         addPermanentError("Échec de récupération de cycles de culture du réalisés", response.status);
      });
  };

  $scope.selectAllEntities = function() {
    // don't send empty departement filter
    if ($scope.filter.departement === '') {
      delete $scope.filter.departement;
    }

    displayPageLoading();
    var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson($scope.filter));
    $http.post($scope.endpoints.effectiveCropCycleIdsJson, ajaxRequest,
            {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
    then(function(response) {
        var selectedEntities = {};
        angular.forEach(response.data, function(topiaId) {
          selectedEntities[topiaId] = true;
        });
        Object.assign($scope.selectedEntities, selectedEntities);
    })
    .catch(function(response) {
       console.error("Échec de récupération des ids de cycles de culture du réalisés", response);
       addPermanentError("Échec de récupération des ids des cycles de culture du réalisés", response.status);
    })
    .finally(function() {
      hidePageLoading();
    });
  };

  //watch with timer
  var timer = false;
  $scope.$watch('[filter.zoneName, filter.plotName, filter.domainName, filter.growingPlanName, filter.growingSystemName, filter.croppingPlanInfo, filter.campaign]',
    function(newValue, oldValue) {
      if (timer) {
          $timeout.cancel(timer);
      }
      timer = $timeout(function(){
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      }, 350);
     }, true);
  // watch without timer
  $scope.$watch('[filter.active]', function(newValue, oldValue) {
    let page = $scope.filter ? $scope.filter.page : 0;
    $scope.filter.page = 0;
    if (!page) {
      $scope.refresh(newValue, oldValue);
    }
  }, true);

  $scope.$watch('[filter.zoneState, filter.plotState]', function(newValue, oldValue) {
    let page = $scope.filter ? $scope.filter.page : 0;
    $scope.filter.page = 0;
    if (!page) {
      $scope.clearSelection();
      $scope.refresh(newValue, oldValue);
    }
  }, true);
  // watch without timer
  $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

  /**
   * Duplicate effective system after user confirm.
   */
  $scope.effectiveDuplicate = function() {
    var results = $filter('toSelectedValue')($scope.selectedEntities);
    if (results.length == 1) {
      $scope.effectiveDuplicateObject = {};
      $scope.effectiveDuplicateObject.fromZoneId = results[0];
      $http.post(
          ENDPOINTS.loadAvailableZoneForDuplicationJson,
          "zoneId=" + encodeURIComponent($scope.effectiveDuplicateObject.fromZoneId),
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
      ).then(function(response) {
        if (response.data && response.data != "null") {
          $scope.availableZonesForDuplication = response.data;
          if ($scope.availableZonesForDuplication.length === 1){
            $scope.effectiveDuplicateObject.toZoneId = $scope.availableZonesForDuplication[0].topiaId;
          }
          if (response.data.length > 0) {
            $("#confirmValidateDuplication").dialog({
              resizable: false,
              width: 400,
              modal: true,
              buttons: {
                dupliquer: {
                  click: function () {
                    $(this).dialog("close");
                    $("#effectiveListForm").attr("action", ENDPOINTS.effectiveCropCycleDuplicate);
                    $("#effectiveListForm").submit();
                  },
                  text:"Dupliquer",
                  'class': 'btn-primary'
                },
                Annuler: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text:"Annuler",
                  'class': 'float-left btn-secondary'
                }
              }
            });
          }  else {
            $("#confirmValidateDuplication").dialog({
              resizable: false,
              width: '80%',
              modal: true,
              position: { my: "center top", at: "top+50" },
              buttons: {
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                    $scope.$apply();
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
              }
            });
          }
        }
      })
      .catch(function(response) {
         console.error("Échec de récupération des zones diponibles", response);
         addPermanentError("Échec de récupération des zones disponibles", response.status);
      });
    }
  };

  $scope.togglePlotOrZoneActiveStatus = function (filter) {
    var results = $filter('toSelectedValue')($scope.selectedEntities);
    if (results.length > 0) {
      displayPageLoading();

      let effectiveListForm = $('#effectiveListForm');
      let selectedZoneIds = $filter('toSelectedArray')($scope.selectedEntities);
      effectiveListForm.attr("action", ENDPOINTS.effectiveCropCyclesZonePlotActivation + "?zoneIds=" + encodeURIComponent(angular.toJson(selectedZoneIds)) + filter);
      effectiveListForm.submit();
    }
  };

  $scope.unActivateZone = function () {
    $scope.togglePlotOrZoneActiveStatus("&plot=false&activate=false");
  };

  $scope.unActivatePlot = function () {
    $scope.togglePlotOrZoneActiveStatus("&plot=true&activate=false");
  };

  $scope.activatePlot = function () {
    $scope.togglePlotOrZoneActiveStatus("&plot=false&activate=true");
  };

  $scope.asyncEffectiveCropCyclesExport = function() {
    var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

    var data = "effectiveCropCycleIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
    $http.post(
        ENDPOINTS.effectiveCropCyclesExportAsync,
        data,
        {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
      )
      .then(function(response) {
        addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
      })
      .catch(function(response) {
        addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
        console.error("Impossible de lancer l'export asynchrone", response);
      });
  };

  $scope.asyncEffectiveMeasurementsExport = function() {
    var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

    var data = "effectiveCropCycleIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
    $http.post(
        ENDPOINTS.effectiveMeasurementsExportAsync,
        data,
        {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
      )
      .then(function(response) {
        addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
      })
      .catch(function(response) {
        addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
        console.error("Impossible de lancer l'export asynchrone", response);
      });
  };
}]);
