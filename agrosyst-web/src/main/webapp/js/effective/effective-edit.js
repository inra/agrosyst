/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Controller d'edition des interventions cultures et cycle associés.
 */
AgrosystModule.controller('EffectiveCropCyclesEditController', ['$scope', 'EffectiveCropCyclesInitData',
  function ($scope, EffectiveCropCyclesInitData) {
    // liste des cycles assolés
    $scope.zoneTopiaId = EffectiveCropCyclesInitData.zoneTopiaId;
    // liste des cycles assolés
    $scope.seasonalCropCycles = EffectiveCropCyclesInitData.seasonalCropCycles;
    // liste des cycles perennes
    $scope.perennialCropCycles = EffectiveCropCyclesInitData.perennialCropCycles;
    // liste des cultures principales
    $scope.croppingPlanModel = EffectiveCropCyclesInitData.croppingPlanModel || [];
    // liste des cultures intermediaires
    $scope.intermediateCroppingPlanModel = EffectiveCropCyclesInitData.intermediateCroppingPlanModel;
    // liste des cultures et espèce
    $scope.zoneCroppingPlanEntries = EffectiveCropCyclesInitData.zoneCroppingPlanEntries;
    // identifiant du domaine sur lequel porte la zone
    $scope.domainId = EffectiveCropCyclesInitData.domainId;
    // map des cultures indexées par topiaId;
    $scope.croppingPlanEntriesIndex = {};
    // map de cultures indexées par code
    $scope.croppingPlanEntriesByCodes = {};
    // campagne du domaine lié au cycle
    $scope.effectiveCampaign = EffectiveCropCyclesInitData.campaign;

    $scope.campaignsBounds = EffectiveCropCyclesInitData.campaignsBounds;

    $scope.domainCampaign = $scope.effectiveCampaign;

    angular.forEach($scope.zoneCroppingPlanEntries, function(croppingPlanEntry) {
      $scope.croppingPlanEntriesIndex[croppingPlanEntry.topiaId] = croppingPlanEntry;
      $scope.croppingPlanEntriesByCodes[croppingPlanEntry.code] = croppingPlanEntry;
    });

    // liste d'autorite : CropCyclePhaseType
    $scope.perennialPhaseTypes = $scope.i18n.CropCyclePhaseType;

    $scope.getItkIdentifierQuery = function() {
      return "zoneTopiaId=" + $scope.zoneTopiaId;
    };

    $scope.initItkTab = function() {
      angular.element(document).ready(
        function initPricesTab() {
          $('#tab_2').scope().initItkTab();
        }
      );
    };

    // options des dates pickers
    $scope.dateOptions = {
        changeMonth: true,
        changeYear: true,
        yearRange: '1900:-0',
        dateFormat: "dd/mm/yy",
        defaultDate: "01/01/" + $scope.effectiveCampaign
    };

    $scope.setDateOptionYearRange = function (intervention) {
      if (intervention && $scope.effectiveCampaign && $scope.dateOptions) {
        var interventionYear = intervention.startInterventionDate && intervention.startInterventionDate.getFullYear()
        var beforeCampaign = parseInt($scope.effectiveCampaign) - 1;
        if (interventionYear && interventionYear < beforeCampaign) {
          beforeCampaign = interventionYear;
        }
        var afterCampaign = parseInt($scope.effectiveCampaign)  + 1;
        if (interventionYear && interventionYear > afterCampaign) {
          afterCampaign = interventionYear;
        }
        var yearRange = beforeCampaign + ":" + afterCampaign;
        $scope.dateOptions = {
          changeMonth: true,
          changeYear: true,
          yearRange: yearRange,
          dateFormat: "dd/mm/yy",
          defaultDate: "01/01/" + $scope.effectiveCampaign,
          onSelect: function(date) {
            // change from date picker
            if (date === $.datepicker.formatDate("dd/mm/yy", intervention.startInterventionDate) && !intervention.endInterventionDate) {
              intervention.endInterventionDate = intervention.startInterventionDate;
            }
          }
        };
      }
    };

    $scope._getSpeciesId = function(agrosystSpecies) {
      return agrosystSpecies.species.topiaId;
    };
}]);
