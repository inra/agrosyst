/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Controller d'edition des cycles pérennes.
 */
AgrosystModule.controller('EffectiveCropCyclesPerennialController', ['$scope', '$http', '$timeout',
  function ($scope, $http, $timeout) {
    // selected cycle for edition
    //$scope.selectedPerennialCropCycle;
    // selection d'une nouvelle culture, dans le select
    //$scope.croppingPlanEntrySelectedChange;
    // id de la culture du cycle pour le model du select
    //$scope.selectedPerennialCropCycle.selectedPerennialCropCycleCroppingPlanEntryId;
    // graft support list with empty value
    $scope.graftSupports = [{label:'' , topiaId: null}];
    // graft clone list with empty value
    $scope.graftSupportClones = [{label:'' , topiaId: null}];

    // function that select the perennial crop to edit
    var validationErrorFunction = {
      process: function(e) {
        // look if perennial crop field error
        if (e && e.target && e.target.id) {
          //"PerennialCropEdition0_croppingPlanEntryField_0"
          var name = e.target.id.match(/PerennialCropEdition[1234567890]+/g);
          if (name) {
            // extract index from id
            var index = Number(name[0].match(/[1234567890]+/g)[0]);
            $scope.selectedPerennialCropCycle = $scope.perennialCropCycles[index];
          }
        }

      }
    };

    if($scope.perennialCropCycles) {
      angular.forEach($scope.perennialCropCycles, function(cycle){
        cycle.selectedPerennialCropCycleCroppingPlanEntryId = cycle.croppingPlanEntryId;
      });
    }

    $scope.displayFieldsErrorsAlert = true;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, validationErrorFunction), true);

    // options des dates pickers
    $scope.initPerennialCropCycleTab = function () {
      $scope.dateOptions.yearRange = '1900:-0';
      $scope.$apply();
    };

    // formule de calcul de la densité de plantation
    // densité (plantes/ha) = 10000 / ((inter-rang/100)*(espacement/100)).
    var computePlantingDensity = function(interFurrow, spacing) {
      if ((interFurrow === 0)  || (spacing === 0)) {
        return null;
      }
      var result = 10000 / ((interFurrow / 100) * (spacing / 100));
      result = Math.round(result * 100) / 100;
      return result;
    };

    // Calcul auto de la densité de plantation
    $scope.$watch("selectedPerennialCropCycle.plantingInterFurrow", function(newValue, oldValue) {
      if ($scope.selectedPerennialCropCycle) {
        var oldResult = computePlantingDensity(oldValue, $scope.selectedPerennialCropCycle.plantingSpacing);
        // on modifie si il n'y a pas eu de saisie utilisateur
        if (!$scope.selectedPerennialCropCycle.plantingDensity || $scope.selectedPerennialCropCycle.plantingDensity == oldResult) {
          $scope.selectedPerennialCropCycle.plantingDensity = computePlantingDensity(newValue, $scope.selectedPerennialCropCycle.plantingSpacing);
        }
      }
    });

    // Calcul auto de la densité de plantation
    $scope.$watch("selectedPerennialCropCycle.plantingSpacing", function(newValue, oldValue) {
      if ($scope.selectedPerennialCropCycle) {
        var oldResult = computePlantingDensity($scope.selectedPerennialCropCycle.plantingInterFurrow, oldValue);
        // on modifie si il n'y a pas eu de saisie utilisateur
        if (!$scope.selectedPerennialCropCycle.plantingDensity || $scope.selectedPerennialCropCycle.plantingDensity == oldResult) {
          $scope.selectedPerennialCropCycle.plantingDensity = computePlantingDensity($scope.selectedPerennialCropCycle.plantingInterFurrow, newValue);
        }
      }
    });

    // create new perennial cycle
    $scope.createEffectivePerennialCropCycle = function() {
      $scope.setDateOptionYearRange();
      var cycle = {
          weedType: "PAS_ENHERBEMENT",
          phaseDtos: [
            {type:"PLEINE_PRODUCTION"}
          ]
      };
      $scope.perennialCropCycles.push(cycle);
      $scope.toogleCycle(cycle);
    };

    $scope.deleteEffectivePerennialCropCycle = function(cycle) {
      var index = $scope.perennialCropCycles.indexOf(cycle);
      $scope.cycleCroppingPlanEntryId=cycle.croppingPlanEntryId;
      $scope.effectivePerennialCropCycleNbInterventions = cycle.nbInterventions;
      if (index != -1) {
        var deleteEffectivePerennialCropCycleModel = {
          process: function() {
            $scope.perennialCropCycles.splice(index, 1);
            delete $scope.selectedPerennialCropCycle;
          },
          cancelChanges: function() {}
        };
        _displayConfirmDialog($scope, $("#confirmRemovedEffectivePerennialCropCycle"), 400, deleteEffectivePerennialCropCycleModel);
      }
    };

    // crop cycle species only contains cropping plan species
    // meta data need to be added for display purpose
    var updateCropCycleSpeciesMetaData = function() {
      angular.forEach($scope.selectedPerennialCropCycle.speciesDtos, function(cropCycleSpecies) {
        var croppingPlanEntry = $scope.croppingPlanEntriesIndex[$scope.selectedPerennialCropCycle.croppingPlanEntryId];
          if (croppingPlanEntry) {
            angular.forEach(croppingPlanEntry.croppingPlanSpecies, function(croppingPlanSpecies) {
              if (croppingPlanSpecies.topiaId == cropCycleSpecies.croppingPlanSpeciesId) {
                cropCycleSpecies.speciesId = croppingPlanSpecies.species.topiaId;
                cropCycleSpecies.speciesEspece = croppingPlanSpecies.species.libelle_espece_botanique_Translated;
                cropCycleSpecies.speciesQualifiant = croppingPlanSpecies.species.libelle_qualifiant_AEE_Translated;
                cropCycleSpecies.speciesTypeSaisonnier = croppingPlanSpecies.species.libelle_type_saisonnier_AEE_Translated;
                cropCycleSpecies.speciesDestination = croppingPlanSpecies.species.libelle_destination_AEE_Translated;
                if (croppingPlanSpecies.variety) {
                  cropCycleSpecies.varietyId = croppingPlanSpecies.variety.topiaId;
                  if (croppingPlanSpecies.variety.variete) {
                    cropCycleSpecies.varietyLibelle = croppingPlanSpecies.variety.variete;
                  } else {
                    cropCycleSpecies.varietyLibelle = croppingPlanSpecies.variety.denomination;
                  }
                }
                cropCycleSpecies.edaplosUnknownVariety = croppingPlanSpecies.edaplosUnknownVariety;
                if (typeof cropCycleSpecies.overGraftDate === "string") {
                    cropCycleSpecies.overGraftDate = new Date(Date.parse(cropCycleSpecies.overGraftDate));
                }
              }
            });
          }
      });
    };

    angular.element(document).ready(
      function rolBackUiDateDirty() {
        $scope.effectiveCropCyclesEditForm.$setPristine();
      }
    );

//    $scope._getPerennialCropCycleCroppingPlanEntry = function(){
//      var croppingPlanEntry;
//      if ($scope.selectedPerennialCropCycle && $scope.selectedPerennialCropCycle.croppingPlanEntryId) {
//        var croppingPlanEntryId = $scope.selectedPerennialCropCycle.croppingPlanEntryId;
//        croppingPlanEntry = $scope.croppingPlanEntriesIndex[croppingPlanEntryId];
//      }
//      return croppingPlanEntry;
//    };

    $scope._getCroppingPlanSpeciesByCode = function (croppingPlanEntry) {
      var croppingPlanSpeciesByCode = {};
      if (croppingPlanEntry && croppingPlanEntry.croppingPlanSpecies) {
        angular.forEach(croppingPlanEntry.croppingPlanSpecies, function(croppingPlanSpecies){
          croppingPlanSpeciesByCode[croppingPlanSpecies.code] = croppingPlanSpecies;
        });
      }
      return croppingPlanSpeciesByCode;
    };

    $scope._getPerennialCropCycleSpecies = function (croppingPlanEntry) {
      var result = [];
      if (croppingPlanEntry.croppingPlanSpecies) {
        angular.forEach(croppingPlanEntry.croppingPlanSpecies, function(croppingPlanSpecies) {
          var cropCycleSpecies = {};
          cropCycleSpecies.croppingPlanSpeciesId = croppingPlanSpecies.topiaId;
          result.push(cropCycleSpecies);
        });
      }
      return result;
    };


//    _manageSpeciesChangeOnSpeciesStades = function(intervention, originalCroppingPlanSpeciesByCode, utilityMaps) {
//      var originalSpeciesStadeCodeToNew = {};
//      var newInterventionSpeciesStades = [];
//      angular.forEach(intervention.speciesStadesDtos, function(speciesStade){
//        var croppingPlanSpecies = originalCroppingPlanSpeciesByCode[speciesStade.speciesCode];
//
//        // get compatible croppingPlanSpecies (same refEspece, same variety)
//        var refEspece = croppingPlanSpecies.species.topiaId;
//        var refVariety = croppingPlanSpecies.variety ? croppingPlanSpecies.variety.topiaId : "";
//        var croppingPlanSpeciesForRefEspeceRefVariety = utilityMaps.croppingPlanSpeciesByRefEspeceRefVariety[refEspece + "_" + refVariety];
//
//        if (croppingPlanSpeciesForRefEspeceRefVariety){
//          originalSpeciesStadeCodeToNew[speciesStade.speciesCode] = croppingPlanSpeciesForRefEspeceRefVariety.code;
//          speciesStade.speciesCode = croppingPlanSpeciesForRefEspeceRefVariety.code;
//          newInterventionSpeciesStades.push(speciesStade);
//        }
//      });
//
//      // push original species stade to the intervention if compatible
//      // if not a new species stade is created
//      angular.forEach(utilityMaps.speciesByCode, function(value, key){
//        var found = false;
//        angular.forEach(newInterventionSpeciesStades, function(speciesStade){
//          // species stade is compatible, but to be valid we must change his species code
//          found === true ? true : speciesStade.speciesCode === key;
//        });
//
//        if (!found) {
//          var speciesStade = {speciesCode : key};
//          newInterventionSpeciesStades.push(speciesStade);
//        }
//      });
//
//      intervention.speciesStadesDtos = newInterventionSpeciesStades;
//      return originalSpeciesStadeCodeToNew;
//    };


    $scope._flushCroppingEntryChanges = function(croppingPlanEntryId) {

      if(croppingPlanEntryId){
        // set the newly selected cropping plan id to the cycle
        $scope.selectedPerennialCropCycle.croppingPlanEntryId = croppingPlanEntryId;

        // the new crop
        var croppingPlanEntry = $scope.croppingPlanEntriesIndex[croppingPlanEntryId];

        if (croppingPlanEntry.topiaId === croppingPlanEntryId) {
          // add the crop species to the phase
          $scope.selectedPerennialCropCycle.speciesDtos = $scope._getPerennialCropCycleSpecies(croppingPlanEntry);

          // change to interventions species stades, actions
          angular.forEach($scope.selectedPerennialCropCycle.phaseDtos, function(phase){
            if (phase.interventions && phase.interventions.length > 0) {

              angular.forEach(phase.interventions, function(intervention){

                // make change on species stades
                intervention.speciesStadesDtos = [];
                angular.forEach(croppingPlanEntry.croppingPlanSpecies, function(species){
                  var speciesStade = {speciesCode : species.code};
                  intervention.speciesStadesDtos.push(speciesStade);
                });

                // make change on seeding actions
                angular.forEach(intervention.actions, function(action) {
                  if (action.seedingSpecies) {

                    var treatment;
                    var biologicalSeedInoculation;
                    var quantity;
                    var unit;

                    // look for default value for new seeding action species (default value = first value found)
                    if (action.seedingSpecies) {
                      angular.forEach(action.seedingSpecies, function(seedingSpecies){

                          if (seedingSpecies.treatment) {
                            treatment = true;
                            if (!quantity) {
                              quantity = seedingSpecies.quantity;
                              unit = seedingSpecies.seedPlantUnit;
                            }
                          }
                          if (seedingSpecies.biologicalSeedInoculation) {
                            biologicalSeedInoculation = true;
                            if (!quantity) {
                              quantity = seedingSpecies.quantity;
                              unit = seedingSpecies.seedPlantUnit;
                            }
                          }
                      });

                      // set default values to all species refs #6282
                      action.seedingSpecies = [];
                      angular.forEach(croppingPlanEntry.croppingPlanSpecies, function(species){

                        var seedingSpecies = {
                          speciesCode: species.code,
                          treatment:treatment,
                          biologicalSeedInoculation:biologicalSeedInoculation,
                          quantity:quantity,
                          seedPlantUnit:unit};

                        action.seedingSpecies.push(seedingSpecies);
                      });
                    }

                  }
                });
              });
            }
          });

          updateCropCycleSpeciesMetaData();
        }
      }
    };

    $scope.countPerennialCropInterventions = function (tmpSum, phase) {
      var phase = angular.isDefined(phase) && phase.interventions ? phase.interventions.length : 0;
      return tmpSum + phase;
    };

    // display or undisplay the selected cycle
    $scope.toogleCycle = function (cycle) {
      if ($scope.selectedPerennialCropCycle && $scope.selectedPerennialCropCycle === cycle) {
        delete $scope.selectedPerennialCropCycle;
      } else {

        if (cycle.phaseDtos) {
          var sum = 0;
          cycle.phaseDtos.forEach(async function(phase) {
            sum = $scope.countPerennialCropInterventions(sum, phase)
          })
        }

        $scope.selectedPerennialCropCycle = cycle;
        $scope.selectedPerennialCropCycle.nbInterventions = sum;
        $scope.selectedPerennialCropCycle.selectedPerennialCropCycleCroppingPlanEntryId = cycle.croppingPlanEntryId;
        updateCropCycleSpeciesMetaData();
      }
    };

    // FIXME autoselect first for now
    if ($scope.perennialCropCycles.length == 1) {
      $scope.toogleCycle($scope.perennialCropCycles[0]);
    }

    $scope._lookForPerennialCropChangeImpact = function(phases) {
      // for input management
      var result;
      if (phases) {
        angular.forEach(phases, function(phase){
          if (phase.interventions && phase.interventions.length > 0) {
            var nbConcernedInterventions = phase.interventions.length;

            var interMess = nbConcernedInterventions === 1 ?
              " 1 intervention est concernée.<br><br>" :
              " " + nbConcernedInterventions + " interventions sont concernées.<br><br>";

            if (!result) {
              //result = "Le changement de culture sera répercuté dans l'onglet 'Interventions culturales'.<br>";
              result = "<b>Pour la phase '" + $scope.perennialPhaseTypes[phase.type] + "' de cette culture</b>," + interMess;

            } else {

              result = result +
                "<br><b>Pour la phase '" +
                $scope.perennialPhaseTypes[phase.type] +
                "' de cette culture'</b>," +
                interMess;

            }


            angular.forEach(phase.interventions, function(intervention){
              var nbInterventionSeedingActions = 0;
              angular.forEach(intervention.actions, function(action) {
                if (action.seedingSpecies) {
                  nbInterventionSeedingActions = nbInterventionSeedingActions + 1;
                }
              });

              if (nbInterventionSeedingActions > 0) {
                if (nbInterventionSeedingActions === 1) {
                  result = result +
                    "<li> Pour l'intervention '" +
                    intervention.name + "'" +
                    " : des valeurs par défaut vont être choisies pour l'action de type semis.<br>" +
                    "&nbsp;&nbsp;&nbsp;" +
                    "Merci de vérifier cette action et les " +
                    "intrants associés.</li>";
                } else {
                  result = result +
                    " <li> Pour l'intervention '"
                    + intervention.name + "'" +
                    " : des valeurs par défaut vont être choisies pour les actions de type semis.<br>" +
                    "&nbsp;&nbsp;&nbsp;" +
                    "Merci de vérifier ces actions et les " +
                    "intrants associés.</li>";
                }
              }
            });
          }
        });
      }
      return result;
    };

    $scope.croppingPlanEntrySelected = function() {
      $scope.croppingPlanEntrySelectedChange = true;
    };

    $scope.doOrNotCropChange = function(confirm, newCroppingPlanEntry, previousCroppingPlanEntry) {
      if (confirm) {
        $scope._flushCroppingEntryChanges(newCroppingPlanEntry);
      } else {
          // revert selection
          $scope.selectedPerennialCropCycle.selectedPerennialCropCycleCroppingPlanEntryId = previousCroppingPlanEntry;
      }
      delete $scope.croppingPlanEntrySelectedChange;
      delete $scope.cropChange;
    };

    $scope.$watch("selectedPerennialCropCycle.selectedPerennialCropCycleCroppingPlanEntryId", function(newCroppingPlanEntry, previousCroppingPlanEntry) {
      if($scope.croppingPlanEntrySelectedChange) {

        $scope.cropChange = {impactMessage: $scope._lookForPerennialCropChangeImpact($scope.selectedPerennialCropCycle.phaseDtos)};

        var cropChangeModel = {
          process: function() {
            $scope._flushCroppingEntryChanges(newCroppingPlanEntry);
            this.next();
          },
          cancelChanges: function() {
            $scope.selectedPerennialCropCycle.selectedPerennialCropCycleCroppingPlanEntryId = previousCroppingPlanEntry;
            this.next();
          },
          next: function() {
            delete $scope.croppingPlanEntrySelectedChange;
            delete $scope.cropChange;
          }
        };

        if ($scope.cropChange.impactMessage) {
          _displayConfirmDialog($scope, $("#confirmCropChange"), 1024, cropChangeModel);
        } else {
          cropChangeModel.process();
        }
      }
    });

    $scope.enableAutocompleteGraftSupportByIndex = -1;
    $scope.enableAutocompleteGraftSupport = function(index) {
      $scope.enableAutocompleteGraftSupportByIndex = index;
    };

    $scope.autocompleteGraftSupport = function (index, cropCyclePerennialSpeciesItem, term) {
      if(index == $scope.enableAutocompleteGraftSupportByIndex) {
        return $http.post(
          ENDPOINT_GRAFT_SUPPORT_JSON, "speciesId=" + encodeURIComponent(cropCyclePerennialSpeciesItem.speciesId) +"&term=" + encodeURIComponent(term),
            {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
            then(function(response) {
            var transformedData = $.map( response.data, function( variety ) {
                var displayName = variety.denomination ? variety.denomination : variety.variete;
                var result = {
                     label: displayName,
                     value: displayName,
                     topiaId: variety.topiaId
                };
                return result;
              });

              var transformedDataWithEmptyOption = [];
              transformedDataWithEmptyOption.push({label:'' , topiaId: null});
              angular.forEach(transformedData, function(tfd) {
                transformedDataWithEmptyOption.push(tfd);
              });
              $scope.graftSupports = transformedDataWithEmptyOption;
            }).
            catch(function(response) {
             console.error("Echec des supports de greffe", response);
             var message = "Echec des supports de greffe";
             addPermanentError(message, response.status);
            });
      } else {
        $scope.graftSupports = [];
      }
    };

    $scope.enableGetAutocompleteGraftCloneByIndex = -1;
    $scope.enableGetAutocompleteGraftClone = function(index) {
      $scope.enableGetAutocompleteGraftCloneByIndex = index;
    };

    /**
     * Function that return a specific ui.autocomplete configuration for each value
     * of ng-repeat iterator (xhr call contextualized with current species and
     * variety).
     */
    $scope.getAutocompleteGraftClone = function (index, cropCyclePerennialSpeciesItem, term) {
      if (index === $scope.enableGetAutocompleteGraftCloneByIndex &&
          cropCyclePerennialSpeciesItem.speciesId &&
          cropCyclePerennialSpeciesItem.varietyId) {
        var localRequest = {
          speciesId: cropCyclePerennialSpeciesItem.speciesId,
          varietyId: cropCyclePerennialSpeciesItem.varietyId,
          term: term};
        $.post(
          ENDPOINT_GRAFT_CLONE_JSON, localRequest,function(data, status, xhr) {
            var transformedData = $.map( data, function( clonePlantGrape ) {
                var displayName = clonePlantGrape.codeClone + ', ' +
                    clonePlantGrape.anneeAgrement + ' (' +
                    clonePlantGrape.origine + ')';
                var result = {
                     label: displayName,
                     value: displayName,
                     topiaId: clonePlantGrape.topiaId
                };
                return result;
              });

              var transformedDataWithEmptyOption = [];
              transformedDataWithEmptyOption.push({label:'' , topiaId: null});
              angular.forEach(transformedData, function(tfd) {
                transformedDataWithEmptyOption.push(tfd);
              });

              $scope.graftSupportClones = transformedDataWithEmptyOption;
            }, "json");
      } else {
        $scope.graftSupportClones = [];
      }
    };

    $scope.bindSelectedCropCyclePerennialSpeciesGraftSupport = function(item, cropCyclePerennialSpeciesItem) {
      if (cropCyclePerennialSpeciesItem) {
        if (item.topiaId === null) {
          delete cropCyclePerennialSpeciesItem.graftSupport;
        } else {
          cropCyclePerennialSpeciesItem.graftSupport = item;
        }
      }
      $scope.graftSupports = [{label:'' , topiaId: null}];
    };

    $scope.bindSelectedCropCyclePerennialSpeciesGraftClone = function(item, cropCyclePerennialSpeciesItem) {
      if (cropCyclePerennialSpeciesItem) {
        if (item.topiaId === null) {
          delete cropCyclePerennialSpeciesItem.graftClone;
        } else {
          cropCyclePerennialSpeciesItem.graftClone = item;
        }
      }
      $scope.graftSupportClones = [{label:'' , topiaId: null}];
    };


    // retourne les types de phase disponibles (non actuellement définis)
    $scope.availablePhaseTypes = function() {
      // reverse array to fix float style in ui
      // TODO EC 11/11/13 : Manage the fix in css rather than in js
      var result = Object.keys($scope.perennialPhaseTypes).reverse();
      // On retire de la liste les types déjà utilisés
      angular.forEach($scope.selectedPerennialCropCycle.phaseDtos, function(item) {
        var indexOf = result.indexOf(item.type);
        result.splice(indexOf , 1);
      });

      return result;
    };

    // Ajout d'une phase
    $scope.addCropCyclePhase = function(cropCyclePhaseType) {
      $scope.selectedPerennialCropCycle.phaseDtos.push( { type: cropCyclePhaseType } );
    };

    // suppression d'une phase au cycle en cours d'edition
    $scope.removeCropCyclePhase = function(cropCyclePhase) {
      var index = $scope.selectedPerennialCropCycle.phaseDtos.indexOf(cropCyclePhase);
      $scope.selectedPerennialCropCycle.phaseDtos.splice(index, 1);
    };

    // angular filter to sort phase by enum ordinal
    $scope.orderByPhaseType = function(item) {
      var allKeys = Object.keys($scope.perennialPhaseTypes);
      return allKeys.indexOf(item.type);
    };
}]);
