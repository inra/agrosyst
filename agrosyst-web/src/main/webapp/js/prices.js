/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Onglets 'Prix'.
 */
AgrosystModule.controller('PricesController', ['$scope', '$http', '$filter', '$q', 'PricesInitData',
  function($scope, $http, $filter, $q, PricesInitData) {

  // The prices model (mandatory)
  //$scope.prices = PricesInitData.prices;
  $scope.loadExclusion = PricesInitData.pricesLoadExclusion;

  // The prices model (optional)
  $scope.readOnly = PricesInitData.readOnly;
  $scope.helpMessage = PricesInitData.helpMessage;
  $scope.campaigns = PricesInitData.campaigns;

  // units for prices
  $scope.priceUnits = $scope.i18n.PriceUnit;

  $scope.yealdUnits = $scope.i18n.YealdUnit;

  $scope.harvestingPriceCategory = Object.keys($scope.i18n.InputPriceCategory)
    .filter((key) => key === "HARVESTING_ACTION")
    .reduce((obj, key) => {
      return Object.assign(obj, {
        [key]: $scope.i18n.InputPriceCategory[key]
      });
    }, {});

  $scope.pricesKeysForActionId = {};

  $scope.pricesIndex = {};
  $scope.valorisationsByIds = new Map();

  $scope.harvestingPricesByActionId = {};

  $scope.mainValorisationPricesByActionId = {};
  $scope.mainValorisationHeaderPricesByActionId = {};

  $scope.allPriceCategories = ["HARVESTING_ACTION"];

  $scope.seedingPricesRefPricesByKey = {};

  $scope.getPriceOrderableValue = function(price) {
    return price.type + '_' + price.category + '_' + price.displayName + '_' + price.sourceUnit;
  };

  var getPriceKey = function (price) {

    var type = price.type ? price.type : "null";// should not be null, required
    var category = 'HARVESTING_ACTION';
    var objectId = price.objectId ? $filter('removeAccents')(price.objectId).normalize('NFD').replace(/[\u0300-\u036f]/g, "").trim().toLowerCase().replace(/\s+/g, '_') : "null";
    var sourceUnit = price.sourceUnit ? price.sourceUnit : "null";
    var valorisationId = price.valorisationId ? price.valorisationId : "null";
    var chemicalTreatment = price.chemicalTreatment ? price.chemicalTreatment : false;
    var biologicalTreatment = price.biologicalTreatment ?  price.biologicalTreatment : false;
    var includedTreatment = price.includedTreatment ? price.includedTreatment : false;
    var productType = price.productType ? price.productType : "null";

    if (price.fromValues) {
      seedType = price.fromValues.seedType ? price.fromValues.seedType : "null";
      chemicalTreatment = price.fromValues.chemicalTreatment ? price.fromValues.chemicalTreatment : false;
      biologicalTreatment = price.fromValues.biologicalTreatment ?  price.fromValues.biologicalTreatment : false;
    }

    var key = type + "_" + category + "_" + objectId + "_" + sourceUnit + "_" + valorisationId + "_" + chemicalTreatment + "_" + biologicalTreatment + "_" + includedTreatment + "_" + productType;

    return key;
  };

  var _getOrCreatePricesForCateg = function() {

    if (!$scope.pricesByCateg) {
      $scope.pricesByCateg = {};
    }

    var pfc = $scope.pricesByCateg['HARVESTING_ACTION'];
    if (!pfc) {
      pfc = [];
      $scope.pricesByCateg['HARVESTING_ACTION'] = pfc;
    }
    return pfc;

  };

  var addOrRemoveHarvestingPrice = function(harvestingPrices, price) {

    var index = harvestingPrices.indexOf(price);

    if (index !== -1) {
      harvestingPrices.splice(index, 1);
    }

    harvestingPrices.push(price);
  };

  var _getFilteredPriceUnitsForPrice = function(price) {
    var filteredPriceUnits = {};

    var pricesPriceUnits = price.allowedPriceUnitsForPrice;

    if (pricesPriceUnits && pricesPriceUnits.length > 0) {
      angular.forEach(pricesPriceUnits, function(pricePriceUnit){
        let puTrad = $scope.priceUnits[pricePriceUnit];
        filteredPriceUnits[pricePriceUnit] = puTrad;
      });

    } else {
      // for harvesting prices, unit are found inside the valorisation
      // #10240 comment #18 user must use the same unit as the one from referential
      if (price.averageReferencePricesForValorisations && price.averageReferencePricesForValorisations.priceUnit) {
        filteredPriceUnits[price.averageReferencePricesForValorisations.priceUnit] = $scope.priceUnits[price.averageReferencePricesForValorisations.priceUnit];
      } else {
        // default value
        filteredPriceUnits.EURO_HA = $scope.priceUnits.EURO_HA;
      }

    }
    return filteredPriceUnits;
  };

  var _pushPrice = function(price) {

    var key = price.topiaId;//angular.isUndefined(specificPriceKey) ? getPriceKey(price) : specificPriceKey;

    price.priceUnits = _getFilteredPriceUnitsForPrice(price);
    price.hasPriceUnits = Object.keys(price.priceUnits).length > 0;

    var priceFound = $scope.pricesIndex[key];

    if (!priceFound) {
      // les prix qui n'ont pas de topiaId sont là uniquement pour l'UI
      if (price.topiaId) {
        if (!$scope.allRegularPrices) {
          $scope.allRegularPrices = [];
        }
        $scope.allRegularPrices.push(price);
      }
    }

    // add or replace to prices by catyegory
    if (!priceFound) {
      var pfc = _getOrCreatePricesForCateg('HARVESTING_ACTION');
      pfc.push(price);
    } else {
      // bind loaded result to current price
      var categ = price.category;
      var pricesForCateg = $scope.pricesByCateg['HARVESTING_ACTION'];
      for (var i = 0 ; i < pricesForCateg.length ; i++) {
        var pfc = pricesForCateg[i];
        var pfcKey = pfc.topiaId;//getPriceKey(pfc);
        if (pfcKey === key) {
          // #11180 copy the price values in the existing object, otherwise it is not the same object anymore
          // and the input seedingProductPricesJson is no longer modified
          price.price = pfc.price;
          price.priceUnit = pfc.priceUnit;
          price.includedTreatment = pfc.includedTreatment;
          Object.assign(pfc, price);
        }
      }
    }

    // add or replace price by key
    $scope.pricesIndex[key] = price;

    return $q.when();
  };

  var _pushHarvestingPrices = function(headerPrices, headerSummaryPrices, valorisationPrices, footerPrices) {

    angular.forEach(headerPrices, function(price) {
      addOrRemoveHarvestingPrice($scope.harvestingPrices, price);
    });

    angular.forEach(headerSummaryPrices, function(price) {
      addOrRemoveHarvestingPrice($scope.harvestingPrices, price);
    });

    angular.forEach(valorisationPrices, function(price) {
      addOrRemoveHarvestingPrice($scope.harvestingPrices, price);
      _pushPrice(price);
    });
    angular.forEach(footerPrices, function(price) {
      addOrRemoveHarvestingPrice($scope.harvestingPrices, price);
    });

  };

  var _getLoadPriceQuery = function(category) {
    var queryElements = [];
    if ($scope.domainId) {queryElements.push("domainId=" + encodeURIComponent($scope.domainId));}
    queryElements.push("campaigns=" + ($scope.practicedSystem ? $scope.practicedSystem.campaigns : $scope.effectiveCampaign ? $scope.effectiveCampaign : $scope.domain.campaign));
    var query = queryElements.join('&');
    return query;
  };

  var _loadPrices = function(categ) {
    var query = _getLoadPriceQuery(categ);
    if (query.length > 0) {
      $scope.loadingPrices = true;

      return $http.post(
        ENDPOINTS.loadPricesJson, query
        ,{headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
      ).then(function(result) {
          if (result && result !== "null") {
            $scope.loadedPricesResult = result.data;
          }
          $scope.loadingPrices = false;
        })
      .catch(function(response) {
        $scope.loadingPrices = false;
        console.error("Échec de récupération des prix", response);
        addPermanentError("Échec de récupération des prix", response.status);
      });

    } else {
      return $q.when();
    }

  };

  $scope.toggleProductDisplayStatus = function(seedingPricesRefPrices) {
    var displayProductPrices = false;
    return;
  }

  var _getPricesForHarvestingAction = function (actionId) {

    var pricesForHarvestingAction = $scope.harvestingPricesByActionId[actionId];
    // createNewOne
    if (!pricesForHarvestingAction) {
      pricesForHarvestingAction = {mainValorisationPrices: [], pricesBySpecesCode: {}};
      $scope.harvestingPricesByActionId[actionId] = pricesForHarvestingAction;
    }
    return pricesForHarvestingAction;

  };

  var _getHarvestingPricesForSpeciesCode = function (pricesForHarvestingAction, speciesCode) {
    var pricesForSpeciesCode = pricesForHarvestingAction.pricesBySpecesCode[speciesCode];
    if (!pricesForSpeciesCode) {
      pricesForSpeciesCode = [];
      pricesForHarvestingAction.pricesBySpecesCode[speciesCode] = pricesForSpeciesCode;
    }
    return pricesForSpeciesCode;
  };

  var _bindPriceToHarvestingPrices = function(price) {

    var actionId = price.actionId;

    var pricesForHarvestingAction = _getPricesForHarvestingAction(actionId);

    var speciesCode = price.speciesCode;

    var pricesForSpeciesCode = _getHarvestingPricesForSpeciesCode(pricesForHarvestingAction, speciesCode);

    var priceKey = price.topiaId;//getPriceKey(price);
    var fPrice = $scope.pricesIndex[priceKey];
    if (fPrice) {
      fPrice.valorisationId = price.valorisationId;
      angular.forEach(pricesForSpeciesCode, function(speciesPrice){
        if (speciesPrice.valorisationId === fPrice.valorisationId) {
          speciesPrice.valorisationId = fPrice.valorisationId;
        }
      });
    }

    if (!fPrice) {
      pricesForSpeciesCode.push(price);
    }

    var valorisation = {
      topiaId:price.valorisationId,

    };
    if (price.averageReferencePricesForValorisations) {
      valorisation.averageRefPrice=price.averageReferencePricesForValorisations.averagePrice;
      valorisation.hasAverageRefPrice=angular.isNumber(price.averageReferencePricesForValorisations.averageRefPrice);
      valorisation.medianUnitRefPrice=price.averageReferencePricesForValorisations.priceUnit;
      valorisation.countedPrices=price.averageReferencePricesForValorisations.countedPrices;
      valorisation.nbPricesByCampaigns=price.averageReferencePricesForValorisations.nbPricesByCampaigns;
    }
    $scope.valorisationsByIds.set(valorisation.topiaId, valorisation);

  };

  var _indexPrices = function() {
    var harvestingPrices = $scope.loadedPricesResult;
    if (harvestingPrices) {
      angular.forEach(harvestingPrices, function(price){
        _bindPriceToHarvestingPrices(price);
        _pushPrice(price);
      });
    }

    _prepareHarvestingPricesForUIDisplay(harvestingPrices, $scope.harvestingPricesByActionId);

    return $q.when();
  };


  var _addReferencesPriceToValorisation = function(valorisationsByIds, prices) {
    angular.forEach(prices, function(price){
      var valorisation = valorisationsByIds.get(price.valorisationId);
      var refPrice = price.averageReferencePricesForValorisations;

      if (refPrice) {
        // for UI
        valorisation.averageRefPrice = refPrice.averagePrice;
        valorisation.hasAverageRefPrice = angular.isNumber(valorisation.averageRefPrice);
        valorisation.medianUnitRefPrice = refPrice.priceUnit;
        if (Object.keys(refPrice.nbPricesByCampaigns).length > 0) {
          var campaignsPricesSummary = "(";
          var separator = ", ";
          angular.forEach(refPrice.nbPricesByCampaigns, function(nbPricesForCampaign, campaign){
            campaignsPricesSummary += campaign + separator;
          });
          campaignsPricesSummary = campaignsPricesSummary.slice(0, campaignsPricesSummary.length - separator.length);
          campaignsPricesSummary += ")";
          valorisation.campaignsPricesSummary = campaignsPricesSummary;
        }

        price.harvestingActionValorisation = valorisation;

      }

    });
  };

  var _addUserPriceToValorisation = function(valorisationsByIds, prices) {
    angular.forEach(prices, function(price){
      var valorisation = valorisationsByIds.get(price.valorisationId);
      var priceDetail = price.userSummaryPriceForValorisations;
      if (priceDetail) {
        valorisation.userPricesMedian = priceDetail.medianPrice;
        valorisation.userNbPricesByCampaigns = priceDetail.nbPricesByCampaigns;
        valorisation.userLowerPrice = priceDetail.lowerPrice;
        valorisation.userHigherPrice = priceDetail.higherPrice;
        valorisation.userPriceUnit = priceDetail.priceUnit;
      }
      price.harvestingActionValorisation = valorisation;
    });
  };

  var createMainValorisationPriceFromValorisation = function(actionPrices, actionId, actionPricesNumber, mainMetaInf, priceUnits) {

    var mainValorisationPrice = {
      actionId: actionId,
      newSerie : actionPrices.mainValorisationPrices.length === 0,
      rowSpan : 1,
      actionPricesNumber : actionPricesNumber,
      mainMetaInf : mainMetaInf,
      yealdAverage : 0.0,
      speciesPrices : {},
      pricesforDestination : [],
      priceUnits : priceUnits
      };

    actionPrices.mainValorisationPrices.push(mainValorisationPrice);

    return mainValorisationPrice;
  };

  var _getHarvestingUIPriceKey = function(price, speciesCode) {
    var key = getPriceKey(price);
    var extraPriceKey = key + "_speciesCode_" + speciesCode;
    return extraPriceKey;
  };

  var _prepareHarvestingPricesForUIDisplay = function(prices, harvestingPricesByActionId) {
    if (prices && harvestingPricesByActionId) {

      $scope.harvestingPrices = [];

      var valorisationsByIds = $scope.valorisationsByIds;

      _addReferencesPriceToValorisation(valorisationsByIds, prices);
      _addUserPriceToValorisation(valorisationsByIds, prices);

      // for action line coloration
      var actionPricesNumber = 0;
      // display: main valorisations

      angular.forEach(harvestingPricesByActionId, function(actionPrices, actionId){
        $scope.mainValorisationPricesByActionId[actionId] = [];

        // use to display or hide species valorisations
        var mainMetaInf = {showSpeciesValorisationPrices : false, actionId : actionId};

        // add action valorisation for species destinations
        var speciesCodes = Object.keys(actionPrices.pricesBySpecesCode);
        var nbSpecies = speciesCodes.length;
        var valorisationPrices = [];
        
        if (nbSpecies === 1) {

          var speciesCode = speciesCodes[0];
          var destinationPricesForSpeciesCode = actionPrices.pricesBySpecesCode[speciesCode];
          if (destinationPricesForSpeciesCode) {
            var nbPricesForSpecies = destinationPricesForSpeciesCode.length;

            for (var i = 0; i < nbPricesForSpecies; i++) {

              var priceModel = destinationPricesForSpeciesCode[i];

              var extraPriceKey = priceModel.topiId;//_getHarvestingUIPriceKey(priceModel, speciesCode);

              if (!$scope.pricesIndex[extraPriceKey]) {

                var price = priceModel;
                price.newSerie = i === 0;
                price.mainMetaInf = mainMetaInf;
                price.speciesMainMetaInf = {rowSpan : nbPricesForSpecies};
                price.headerCropLine = true;
                price.actionPricesNumber = actionPricesNumber;
                price.displayDetails = true;

                valorisationPrices.push(price);

              }
            }
          }

          _pushHarvestingPrices([], [], valorisationPrices, []);

        } else {
          // display an empty line with the species label for mix, then the species destination prices then the summary for mix
          actionPrices.mainValorisationPrices = [];
          var headerPrices = [];
          var headerSummaryPrices = [];
          var valorisationPrices = [];
          var footerPrices = [];

          var headerLinePrice = {
            mainMetaInf : mainMetaInf,
            actionPricesNumber : actionPricesNumber,
            displayName: "",
            newSerie : true,
            headerSummaryLine : true
          };

          headerPrices.push(headerLinePrice);

          var mainPricesByDestinations = {};

          angular.forEach(speciesCodes, function(speciesCode){
            var destinationPricesForSpeciesCode = actionPrices.pricesBySpecesCode[speciesCode];
            if (destinationPricesForSpeciesCode) {
              var nbPricesForSpecies = destinationPricesForSpeciesCode.length;
              for (var i = 0; i < nbPricesForSpecies; i++) {

                var price = destinationPricesForSpeciesCode[i];
                price.newSerie = i === 0;
                price.mainMetaInf = mainMetaInf;
                price.displayDetails = true;
                price.speciesMainMetaInf = {rowSpan : nbPricesForSpecies};
                valorisationPrices.push(price);

                if (i === 0) {
                  headerLinePrice.displayName += " " + price.displayName;
                  headerLinePrice.priceUnits = price.priceUnits;
                }

                var mainPricesByDestination = mainPricesByDestinations[price.destinationId];
                if (!mainPricesByDestination) {
                  mainPricesByDestination = createMainValorisationPriceFromValorisation(
                    actionPrices,
                    actionId,
                    actionPricesNumber,
                    mainMetaInf,
                    price.priceUnits);
                  mainPricesByDestinations[price.destinationId] = mainPricesByDestination;
                }

                mainPricesByDestination.yealdAverage += price.yealdAverage;
                var pricesforDestination = mainPricesByDestination.pricesforDestination;
                pricesforDestination.push(price);
              }
            }
          });

          // if it's a mixed crop then add estimated Mix price
          if (speciesCodes.length > 0) {

            var mainValorisationSpeciesPriceDetailed = {};
            var mainValorisationSpeciesHeaderPriceDetailed = {};
            $scope.mainValorisationPricesByActionId[actionId] = mainValorisationSpeciesPriceDetailed;
            $scope.mainValorisationHeaderPricesByActionId[actionId] = mainValorisationSpeciesHeaderPriceDetailed;

            for (var j = 0; j < actionPrices.mainValorisationPrices.length; j++) {
              var _footerPrice = actionPrices.mainValorisationPrices[j];

              //_mainValorisationPrices.mainMetaInf = mainMetaInf;
              _footerPrice.summaryInf = true;
              var firstDestinationPrice = _footerPrice.pricesforDestination[0];
              _footerPrice.destination = firstDestinationPrice ? firstDestinationPrice.destination : "-";


              if (j === 0) {
                _footerPrice.summaryInfNewLine = true;
                _footerPrice.summaryInfNewLineDestinationCount = actionPrices.mainValorisationPrices.length;
              }

              var mainYealAverage = parseFloat(_footerPrice.yealdAverage);
              var pricesForDestination = _footerPrice.pricesforDestination;


              var headerSummaryPrice = angular.copy(_footerPrice);
              headerSummaryPrice.headerSummaryPrice = true;
              headerSummaryPrice.mainMetaInf = mainMetaInf;
              headerSummaryPrice.displayName = headerLinePrice.displayName;

              _footerPrice.sharedPriceInfos = {price : 0};
//                _footerPrice.price = 0;
              angular.forEach(pricesForDestination, function (price) {
                headerLinePrice.zone = price.zone ? price.zone : undefined;
                headerSummaryPrice.zone = price.zone ? price.zone : undefined;
                headerLinePrice.practicedSystemId = price.practicedSystemId ? price.practicedSystemId : undefined;
                headerSummaryPrice.practicedSystemId = price.practicedSystemId ? price.practicedSystemId : undefined;
                headerLinePrice.zoneId = price.zoneId ? price.zoneId : undefined;
                headerSummaryPrice.zoneId = price.zoneId ? price.zoneId : undefined;
                headerLinePrice.growingSystemName = price.growingSystemName ? price.growingSystemName : undefined;
                headerSummaryPrice.growingSystemName = price.growingSystemName ? price.growingSystemName : undefined;
                headerLinePrice.practicedSystemName = price.practicedSystemName ? price.practicedSystemName : undefined;
                headerSummaryPrice.practicedSystemName = price.practicedSystemName ? price.practicedSystemName : undefined;
                headerLinePrice.zoneName = price.zoneName ? price.zoneName : undefined;
                headerSummaryPrice.zoneName = price.zoneName ? price.zoneName : undefined;
                headerLinePrice.campaign = price.campaign ? price.campaign : undefined;
                headerSummaryPrice.campaign = price.campaign ? price.campaign : undefined;
                var speciesPrice = price.price ? price.price : 0; //price.averageRefPrice;// TODO ADD IT
                var speciesPriceYeald = parseFloat(price.yealdAverage);
                var speciesPricePart = ((speciesPriceYeald * 100.0) / mainYealAverage);
                var speciesPriceDetailed = {id: price.topiaId, speciesPricePart : speciesPricePart, speciesPrice : speciesPrice};
                _footerPrice.speciesPrices[speciesPriceDetailed.id] = speciesPriceDetailed;

                if (speciesPriceDetailed.speciesPricePart && speciesPriceDetailed.speciesPrice) {
                  _footerPrice.sharedPriceInfos.priceUnit = price.priceUnit;
                  _footerPrice.sharedPriceInfos.price += (speciesPriceDetailed.speciesPricePart * speciesPriceDetailed.speciesPrice)/100.0;
                }
                mainValorisationSpeciesPriceDetailed[speciesPriceDetailed.id] = _footerPrice;
                mainValorisationSpeciesHeaderPriceDetailed[speciesPriceDetailed.id] = headerSummaryPrice;
              });

              headerSummaryPrice.sharedPriceInfos = _footerPrice.sharedPriceInfos;
              var uiPrice = headerSummaryPrice.sharedPriceInfos.price === 0 ? '' : headerSummaryPrice.sharedPriceInfos.price;
              headerSummaryPrice.globalPrice = uiPrice;
              headerSummaryPrice.globalPriceUnit = headerSummaryPrice.sharedPriceInfos.priceUnit;

              footerPrices.push(_footerPrice);
              headerSummaryPrices.push(headerSummaryPrice);
            }

            _pushHarvestingPrices(headerPrices, headerSummaryPrices, valorisationPrices, footerPrices);

          }
        }

        actionPricesNumber = actionPricesNumber + 1;

      });
    }

  };

  var _getValorisationFromHarvestingPrices = function(harvestingPricesByActionId) {
    var valorisations = [];
    angular.forEach(harvestingPricesByActionId, function(value, key){
      var pricesForSpeciesCode = value.pricesBySpecesCode;
      angular.forEach(pricesForSpeciesCode, function(prices, speciesCode){
        angular.forEach(prices, function(price) {
          valorisations.push(price.valorisationId);
        });
      });
    });
    return valorisations;
  };

  var _computePriceUnit = function(sourceUnit, category) {
    var priceUnit = "EURO_HA";

    if(sourceUnit){
      if(sourceUnit.match(/^L_/)) {
        priceUnit = "EURO_L";
      } else if (sourceUnit.match(/^G_/)) {
        priceUnit = "EURO_G";
      } else if (sourceUnit.match(/^KG_/)) {
        priceUnit = "EURO_KG";
      } else if (sourceUnit.match(/^T/)) {
        priceUnit = "EURO_T";
      }  else if (sourceUnit.match(/^DI/)) {
        priceUnit= "EURO_DIFFUSEUR";
      }
    }

    return priceUnit;
  };

  var _getPriceModel = function(priceUi) {
    var key = priceUi.topiaId;//getPriceKey(priceUi);
    var p2 = $scope.pricesIndex[key];
    return p2;
  };

  var _loadPricesProcess = function(categ) {
    return _loadPrices(categ).then(_indexPrices);
  };

  var _loadNextCategPrices = function(categIndex) {
    if (categIndex + 1 <= $scope.allPriceCategories.length) {
      _loadPricesProcess($scope.allPriceCategories[categIndex])
        .then(_loadNextCategPrices.bind($scope, categIndex+1));
    }
    return $q.when();
  };

  $scope.setUserPrice = function(price) {
    if (price && price.mainMetaInf) {

      var actionId = price.mainMetaInf.actionId;
      var mainValorisationPrices = $scope.mainValorisationPricesByActionId[actionId];
      var modifiedPrice = mainValorisationPrices[price.topiaId];
      // only if it's a mixed crop
      if (modifiedPrice) {

        modifiedPrice.speciesPrice = parseFloat(price.price);

        // update the destination species price for global valorisation
        var speciesPriceDetailed = modifiedPrice.speciesPrices[price.topiaId];

        if (!isNaN(modifiedPrice.speciesPrice) && isFinite(modifiedPrice.speciesPrice)) {
          speciesPriceDetailed.speciesPrice = modifiedPrice.speciesPrice;
        } else if (price.harvestingActionValorisation && price.harvestingActionValorisation.averageRefPrice) {
          speciesPriceDetailed.speciesPrice = price.harvestingActionValorisation.averageRefPrice;
        } else {
          speciesPriceDetailed.speciesPrice = 0.0;
        }

        // compute new global valorisation price for destination
        modifiedPrice.sharedPriceInfos.price = 0.0;
        angular.forEach(modifiedPrice.speciesPrices, function(speciesPrice){
          modifiedPrice.sharedPriceInfos.price += (speciesPrice.speciesPricePart * speciesPrice.speciesPrice)/100.0;
        });

      }
      //harvestingPrices
      if (!isNaN(price.price) && isFinite(price.price)) {
        if (price.pricesforDestination) {
          angular.forEach(price.pricesforDestination, function(priceForDestination){
            var indexedPrice = $scope.pricesIndex[priceForDestination.topiaId];
            if (indexedPrice) {
              indexedPrice.price = price.price;
            }
          });
        }

        if (price.sharedPriceInfos) {
          price.sharedPriceInfos.price = price.price;
        }
      }

      if (!!price.priceUnit) {
        if (price.pricesforDestination) {
          angular.forEach(price.pricesforDestination, function(priceForDestination){
            var indexedPrice = $scope.pricesIndex[priceForDestination.topiaId];
            if (indexedPrice) {
              indexedPrice.priceUnit = price.priceUnit;
            }
          });
        }

        if (price.sharedPriceInfos) {
          price.sharedPriceInfos.priceUnit = price.priceUnit;
        }
      }

//      var priceModel = _getPriceModel(price);
//      priceModel.price = price.price;
//      priceModel.priceUnit = price.priceUnit;
    }

  };

  $scope.setGlobalPrice = function (price) {
    $scope.harvestingPrices.filter((p) => p.actionId === price.actionId)
      .forEach((p) => {
        if (price.globalPrice && price.globalPrice !== 0) p.price = price.globalPrice;
        if (price.globalPriceUnit) p.priceUnit = price.globalPriceUnit;
        if (p.sharedPriceInfos && price.globalPrice && price.globalPrice !== 0) p.sharedPriceInfos.price = price.globalPrice;
        if (p.sharedPriceInfos && price.globalPriceUnit) p.sharedPriceInfos.priceUnit = price.globalPriceUnit;
      });
  };

  $scope.refreshPrices = function() {

    var onlyNewPrices = _getOrCreatePricesForCateg('HARVESTING_ACTION').length > 0;
    // quick fix
    if (!onlyNewPrices) {
      _loadPricesProcess('HARVESTING_ACTION');
    }

  };

  $scope.initPricesTab = function () {
    delete $scope.editedValorisationPrice;
    $scope.refreshPrices();
  };

}]);
