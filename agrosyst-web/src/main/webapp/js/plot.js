/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2013 - 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

AgrosystModule.controller('PlotEditController', ['$scope', '$timeout', 'plotInitData',
 function ($scope, $timeout, plotInitData) {
   // la parcelle en cours d'edition
   $scope.plot = plotInitData.Plot;
   $scope.campaignsBounds = plotInitData.campaignsBounds;
   $scope.displayFieldsErrorsAlert = true;

   // liste des systèmes de culture
   $scope.growingSystems = plotInitData.growingSystems;
   // liste des systèmes de culture sélectionnables
   $scope.writableGrowingSystems = plotInitData.writableGrowingSystems;

   $scope.displayLocation = function() {
     displayLocation($scope.plot.name, $scope.plot.latitude, $scope.plot.longitude, "");
   };

   // listener is on agrosyst-app.js
   document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

 }]);

AgrosystModule.controller('PlotZoningController', ['$scope', 'plotInitData',
 function ($scope, plotInitData) {
   // l'ensemble des zonages selectionnable
   $scope.parcelleZonages = plotInitData.ParcelleZonages;
   // les topiaId des zonages selectionnés
   $scope.selectedPlotZoningIds = {};

   // set id select in map by default
   angular.forEach(plotInitData.SelectedPlotZoningIds, function(id) {
     $scope.selectedPlotZoningIds[id] = true;
   });
}]);

AgrosystModule.controller('PlotSolController', ['$scope', 'plotInitData',
 function ($scope, plotInitData) {
   // {Array} les sols d'un domaine
   $scope.grounds = plotInitData.Grounds;
   // {String} l'id sol du domaine sélectionné dans l'onglet Sols
   $scope.selectedSolId = plotInitData.SelectedSolId ? plotInitData.SelectedSolId : "";
   // {String} l'id de la texture geppa de la surface
   $scope.selectedSurfaceTextureId = plotInitData.SelectedSurfaceTextureId ? plotInitData.SelectedSurfaceTextureId : "";
   // {String} l'id de la texture geppa du sous sol
   $scope.selectedSubSoilTextureId = plotInitData.SelectedSubSoilTextureId ? plotInitData.SelectedSubSoilTextureId : "";
   // {String} l'id de la classe de profondeur
   $scope.selectedSolProfondeurId = plotInitData.SelectedSolProfondeurId ? plotInitData.SelectedSolProfondeurId : "";
   // le sol sélectionné dans l'onglet Sol (pour afficher l'aide à la saisie)
   //$scope.selectedSol;
   // la liste des horizon du sol
   $scope.solHorizons = plotInitData.SolHorizons;
   // l'horizon selectionné pour edition
   //$scope.selectSolHorizon;
   // index du sol selectionné dans la liste
   //$scope.selectedSolHorizonIndex;
   // liste des sol textures (pour recuperation du label pour affichage liste des horizons)
   $scope.solTextures = plotInitData.SolTextures;
   // liste des profondeur (pour le calcul de la reserve utile)
   $scope.solProfondeurs = plotInitData.SolProfondeurs;
   // liste des sol carateristiques (pour le calcul de la reserve utile)
   $scope.solCaracteristiques = plotInitData.SolCaracteristiques;
   // topiaId of the growing System
   $scope.growingSystemTopiaId = plotInitData.growingSystemTopiaId;

   // called when sol is selected in select
   $scope.updateSelectedSol = function(selectedSolId) {
     angular.forEach($scope.grounds, function(ground) {
       if (ground.topiaId == selectedSolId) {
         $scope.selectedSol = ground;
       }
     });
   };
   $scope.updateSelectedSol($scope.selectedSolId);

   // called to add a new horizon
   $scope.addSolHorizon = function() {
     // auto select plot texture
     var solHorizon = {
         solTextureId : $scope.selectedSolTextureId
     };
     $scope.solHorizons.push(solHorizon);
     $scope.editSolHorizon(solHorizon, $scope.solHorizons.length - 1);
   };

   // called when editing a line
   $scope.editSolHorizon = function(solHorizon, index) {
     $scope.selectedSolHorizon = solHorizon;
     $scope.selectedSolHorizonIndex = index;
   };

   // suppression d'un sol horizon depuis la liste des sol horizons
   $scope.deleteSolHorizon = function(shIndex) {
     $scope.solHorizons.splice(shIndex, 1);
   };

   // get sol texture label from sol texture topia id
   $scope.solTextureLabel = function(solHorizon) {
     var textureLabel;
     angular.forEach($scope.solTextures, function(solTexture) {
       if (solTexture.topiaId == solHorizon.solTextureId) {
         textureLabel = solTexture.classes_texturales_GEPAA;
       }
     });
     return textureLabel;
   };

   // validate current low rating to be > previous horizon low rating and < next horizon low rating
   $scope.validateHorizonLowRating = function(value) {
     var result = true;
     if (!angular.isDefined(value)) {
       return result;
     }
     var fValue = parseFloat(value);
     if ($scope.selectedSolHorizonIndex > 0) {
       var previousSolHorizon = $scope.solHorizons[$scope.selectedSolHorizonIndex-1];
       result &= fValue > previousSolHorizon.lowRating;
     }
     if ($scope.selectedSolHorizonIndex < $scope.solHorizons.length - 1) {
       var nextSolHorizon = $scope.solHorizons[$scope.selectedSolHorizonIndex+1];
       result &= fValue < nextSolHorizon.lowRating;
     }
     return result;
   };

   // compute sol horizon Thickness depending on other sol horizon
   $scope.computeSolHorizonThickness = function(solHorizon, solHorizonIndex) {
     var result = 0;
     if (solHorizon && solHorizon.lowRating) {
       result = solHorizon.lowRating;
       if (solHorizonIndex > 0) {
         var previousSol = $scope.solHorizons[solHorizonIndex - 1];
         if (previousSol.lowRating) {
           result -= previousSol.lowRating;
         }
       }
     }
     return result;
   };

   // Compute usefull reserve
   // RU réelle = RU de la classe de profondeur du sol choisi * (profondeur saisie / h) * (1-% cailloux)
   // RU de le classe de prondeur : présent dans le référentiel sol_texture (INDIGO)
   // h = epaisseur d'horizon : présents dans le référentiel sol_texture (GEPPA)
   // % cailloux = pierrosité moyenne
   // Correspondance entre INDIGO et GEPPA dans le référentiel sol_texture GEPPA
   var computeUsefullReserveSingle = function(solTextureId, solDepth, solStoniness) {
     var textureAbbr;
     var profondeurDefault;
     var profondeurClass;
     var reserveUtile;
     // recherche de l'abbreviation de texture de la texture selectionée
     angular.forEach($scope.solTextures, function(item) {
       if (item.topiaId == solTextureId) {
         textureAbbr = item.abbreviation_INDIGO;
       }
     });
     // recherche de la classe de profondeur de la profondeur sélectionnée
     angular.forEach($scope.solProfondeurs, function(item) {
       if (item.topiaId == $scope.selectedSolProfondeurId) {
         profondeurClass = item.classe_de_profondeur_INDIGO;
         profondeurDefault = item.profondeur_par_defaut;
       }
     });
     // à partir de ces deux informations, on croise avec un 3eme référentiel
     // pour obtenir la réserve utile
     angular.forEach($scope.solCaracteristiques, function(item) {
       if (item.abbreviation_INDIGO == textureAbbr && item.classe_de_profondeur_INDIGO == profondeurClass) {
         reserveUtile = item.reserve_utile;
       }
     });
     // valeaur par defaut si undefined
     if (!solStoniness) {solStoniness = 0;}
     if (!solDepth) {solDepth = profondeurDefault;}
     if (!reserveUtile) {reserveUtile = 0;}
     // et on fait le cacul
     var usefullReserve;
     if (profondeurDefault) {
         usefullReserve = reserveUtile * (solDepth / profondeurDefault) * (1 - solStoniness / 100);
     } else {
       usefullReserve = 0;
     }
     return usefullReserve;
   };

   // s'il y a des horizon, c'est pas somme des reserves utiles des horizons
   var computeUsefullReserveComplex = function() {
     var sum = 0;
     var index = 0;
     angular.forEach($scope.solHorizons, function(solHorizon) {
       var solHorizonDepth = $scope.computeSolHorizonThickness(solHorizon, index);
       sum += computeUsefullReserveSingle(solHorizon.solTextureId, solHorizonDepth, solHorizon.stoniness);
       index++;
     });
     return sum;
   };

   // chose compute method depending on horizon number
   $scope.computeUsefullReserve = function() {
     var result;
     if ($scope.solHorizons.length > 0) {
       result = computeUsefullReserveComplex();
     } else {
       result = computeUsefullReserveSingle($scope.selectedSurfaceTextureId, $scope.plot.solMaxDepth, $scope.plot.solStoniness);
     }
     return Math.round(result*100)/100;
   };
}]);

AgrosystModule.controller('PlotZoneController', ['$scope', 'plotInitData',
 function ($scope, plotInitData) {
  // liste des zones définie sur la parcelle
  $scope.zones = plotInitData.Zones;
  // liste d'autorité type de zone
  $scope.zoneTypes = plotInitData.ZoneTypes;
  // zoneIds's map and boolean for zone used or not.
  $scope.zonesUsageList = angular.isDefined(plotInitData.zonesUsageList) ? plotInitData.zonesUsageList.usageMap : {};
  // {Map} entity usage count for each zone topia id
  //  $scope.zoneUsages = plotInitData.ZoneUsages;
  // current selected zone for edition
  //$scope.selectedZone;

  // add new zone
  $scope.addZone = function() {
    var zone = {
        area: $scope.availableAreaForNewZone(),
        active:true
    };
    $scope.zones.push(zone);
    $scope.editZone(zone);
  };

  // edit zone
  $scope.editZone = function(zone) {
    $scope.selectedZone = zone;
  };

  // listen for area change (change only if no value OR if value is the same as plot area)
  $scope.$watch('plot.area', function(newValue, oldValue) {
    if ($scope.zones.length == 1) {
      var first = $scope.zones[0];
      if (angular.isUndefined(first.area) || first.area == oldValue) {
        first.area = newValue;
      }
    }
  });

  // get availaible free area for new zone creation
  $scope.availableAreaForNewZone = function() {
    var avail = 0;
    if ($scope.plot.area) {
      avail = $scope.plot.area;
      angular.forEach($scope.zones, function(zone) {
        avail -= parseFloat(zone.area);
      });
      avail = Math.round(avail * 10000) / 10000;
    }
    return avail;
  };

  // validate zone area
  $scope.validateZoneSurface = function(value) {
    if (!angular.isDefined(value)) {
      return true; // error float, not validated
    }
    var sum = 0;
    angular.forEach($scope.zones, function(zone) {
      if (zone == $scope.selectedZone) {
        sum += parseFloat(value);
      } else {
        sum += parseFloat(zone.area);
      }
    });
    // Check if the sum of zones areas is superior or equal to the plot area
    // The check includes a margin of error due to the limited precision of double numbers
    var epsilon = parseFloat(1E-14);
    var plotArea = parseFloat($scope.plot.area);
    var result = false;
    if (plotArea >= sum) {
      result = true;
    } else {
      result = (Math.abs(plotArea - sum) <= epsilon);
    }

    return result;
  };

  // suppression d'une zone (si non utilisé dans agrosyst)
  $scope.deleteZone = function(zoneIndex) {
   $("#confirmDeleteZone").dialog({
     resizable: false,
     width: 400,
     modal: true,
     buttons: {
       action: {
         click: function() {
           $(this).dialog("close");
           if ($scope.zones[zoneIndex] == $scope.selectedZone) {
             delete $scope.selectedZone;
           }
           $scope.zones.splice(zoneIndex, 1);
           $scope.$apply();
         },
         text: "Supprimer",
         'class': 'btn-primary'
       },
       cancel: {
         click: function() {
           $(this).dialog("close");
         },
         text: 'Annuler',
         'class': 'float-left btn-secondary'
       }
     }
   });
  };

  //compute commune name auto completion
  $("#commune").autocomplete(
    {
      source : function(request, response) {
        if (request.term && request.term.length >= 1) {
          // On remplace par du POST pour les pbs d'encoding
          $.post(ENDPOINTS.listRefLocationJson, request,
            function(data, status, xhr) {
              if (data) {
                var autoCompleteField = $("#commune");
                if (data.length === 0) {
                  autoCompleteField.addClass("empty-autocomplete-result");
                } else {
                  autoCompleteField.removeClass("empty-autocomplete-result");
                }
              }
              // On récupère un objet RefLocation brut, qu'on transforme
              var transformedData = $.map( data, function( loc ) {
                var codePostalAndName = loc.codePostal + ", " + loc.commune;
                var result =
                  {
                     label: codePostalAndName,
                     value: codePostalAndName,
                     refLocation: loc
                  };
                return result;
              });
              response(transformedData);
            }, "json");
          } else {
            $("#locationTopiaId").val("");
          }
        $scope.plotEditForm.$setDirty();
      },
      minLength : 0,
      select : function(event, ui) {
        if (ui.item) {
          $("#locationTopiaId").val(ui.item.refLocation.topiaId);
        }
      }
    }
  );
}]);

AgrosystModule.controller('PlotAdjacentElementsController', ['$scope', 'plotInitData',
 function ($scope, plotInitData) {
   // {Array} Element de voisinage du référentiel
   $scope.adjacentElements = plotInitData.AdjacentElements;
   // {Object} Ids des elements de voisinage sélectionnés
   $scope.adjacentElementIds = {};

   // set id select in map by default
   angular.forEach(plotInitData.AdjacentElementIds, function(id) {
     $scope.adjacentElementIds[id] = true;
   });
}]);
