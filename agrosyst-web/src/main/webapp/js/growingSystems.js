/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

function growingSystemsExport(exportForm) {
    var idsCount = JSON.parse(exportForm.children("input[name='growingSystemIds']").val()).length;
    if (idsCount >= 1) {
      exportForm.attr("action", ENDPOINTS.growingSystemsExport);
      exportForm.submit();
    }
}

function editGrowingSystemDataProcessors(growingSystemCode) {
  editEntityRoles("GS_DATA_PROCESSOR", growingSystemCode, "Exploitants du système de culture");
}

AgrosystModule.controller('GrowingSystemPlotsController', ['$scope', '$window', '$http', '$timeout','typeAgricultureTopiaId','availablePlots', 'growingSystem', 'growingSystemNetworks', 'domainSAUArea', 'selectedPlotsIds', 'selectedGrowingPlan', 'marketingDestinationsBySector', 'readOnly',
  function ($scope, $window, $http, $timeout, typeAgricultureTopiaId, availablePlots, growingSystem, growingSystemNetworks, domainSAUArea, selectedPlotsIds, selectedGrowingPlan, marketingDestinationsBySector, readOnly) {
    $scope.typeAgricultureTopiaId = typeAgricultureTopiaId;
    $scope.availablePlots = availablePlots;
    $scope.growingSystem = growingSystem;
    $scope.growingSystem.networks = growingSystemNetworks;
    // {double} surface du domain associée au system de culture (can be null)
    $scope.domainSAUArea = domainSAUArea;
    $scope.selectedPlots = selectedPlotsIds;
    $scope.gsNetworks=[];
    $scope.marketingDestinationsBySector = marketingDestinationsBySector || [];
    $scope.flatMarketingDestinationsBySector = [];
    $scope.estimatingIftRules = $scope.i18n.EstimatingIftRules;
    $scope.doseTypes = $scope.i18n.DoseType;
    $scope.iftSeedsTypes = $scope.i18n.IftSeedsType;
    $scope.readOnly = readOnly;
    $scope.displayFieldsErrorsAlert = true;
    $scope.displayFieldsErrorsAlert = true;
    $scope.gardeningProductionType = ['ABRIS', 'HORS_SOL', 'PLEIN_CHAMP'];
    $scope.horticultureProductionType = ['CONTAINER_NURSERY', 'OPEN_GROUND_NURSERY', 'MIX', 'POT_PLANTS'];
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

    $scope.getProductionTypes = function(sector) {
      var result = [];
      if ('MARAICHAGE' === sector) {
        result = $scope.gardeningProductionType;
      } else if ('HORTICULTURE' === sector) {
        result = $scope.horticultureProductionType;
      }
      return result;
    }

    $scope.areAllPlotsSelected = function() {
      return $scope.availablePlots && $scope.availablePlots.every(plot => !plot.active || $scope.selectedPlots[plot.topiaId]);
    }

    $scope.toggleSelectedPlots = function() {
      if ($scope.growingSystem.growingPlan.domain.active) {
        var allSelected = $scope.areAllPlotsSelected();
        angular.forEach($scope.availablePlots, function(plot) {
          if (plot.active) {
            $scope.selectedPlots[plot.topiaId] = !allSelected;
          }
        });
      }
    };

    $scope._setFlatMarketingDestinationsBySector = function(marketingDestinationsBySector) {
      angular.forEach(Object.keys(marketingDestinationsBySector), function(sector) {
        var marketingDestinations = marketingDestinationsBySector[sector];
        angular.forEach(marketingDestinations, function(marketingDestination){
          $scope.flatMarketingDestinationsBySector.push(marketingDestination);
        });
      });
    };

    $scope._setFlatMarketingDestinationsBySector($scope.marketingDestinationsBySector);

    // options des dates picker début et fin d'utilisation
    $scope.dateOptions = {
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd/mm/yy"
    };

    angular.element(document).ready(
      function rolBackUiDateDirty() {
        $scope.growingSystemEditForm.$setPristine();
      }
    );

    angular.forEach($scope.growingSystem.networks, function(network) {
      var gsNetwork = {
        topiaId : network.topiaId,
        name : network.name
      };
      $scope.gsNetworks.push(gsNetwork);
    });

    if (typeof $scope.growingSystem.startingDate  === "string") {
      $scope.growingSystem.startingDate = new Date(Date.parse($scope.growingSystem.startingDate));
    }

    if (typeof $scope.growingSystem.endingDate  === "string") {
      $scope.growingSystem.endingDate = new Date(Date.parse($scope.growingSystem.endingDate));
    }

    $scope.autoSelect = function(marketingDestination) {
      marketingDestination.selectedForGS = marketingDestination.part ? true : marketingDestination.selectedForGS;
    };

    $scope.removeNetwork = function (network) {
      var confirm = $window.confirm("Voulez-vous supprimer le réseau "+ network.name + " ?");
      var index = $scope.gsNetworks.indexOf(network);
      if (index >= 0 && confirm) {
        $scope.gsNetworks.splice(index,1);
        $scope.growingSystemEditForm.$setDirty();
      }
    };

    $scope.growingPlanSelected = function() {
      $.ajax({
        url : ENDPOINTS.growingSystemsPlotsJson,
        data : {
          'growingPlanTopiaId' : $scope.growingPlanTopiaId
        },
        async : false,
        dataType : "json",
        success : function(data) {
          $scope.availablePlots = data;
          $scope.selectedPlots = {};
        },
        error : function(data, status) {
          console.error("Echec de récupération des parcelles");
          var message = "Echec de récupération des parcelles";
          addPermanentError(message, status);
        }
      });
    };

    $scope.refreshGrowingPlans = function(pattern) {
      // La librairie ui-select ne permet pas de mettre de place holder directement sur le input de recherche donc on l'injecte en JS
      $('#growingPlanTopiaId .ui-select-dropdown input.ui-select-search').attr('placeholder', 'Entrez le nom de votre dispositif s\'il n\'apparait pas dans la liste');
      return $http.get($scope.endpoints.growingPlanCompleteJson + "?term=" + encodeURIComponent(pattern))
          .then(function(response) {
            $scope.growingPlans = response.data;
          })
          .catch(function(response) {
              var message = "Échec de récupération des dispositif";
              console.error(message, response);
              addPermanentError(message, response.status);
          });
    };

    $scope.selectGrowingPlan = function(growingPlan) {
      if (growingPlan && growingPlan.topiaId !== $scope.growingPlanTopiaId) {

        $scope.growingPlanTopiaId = growingPlan.topiaId;
        $scope.growingSystem.growingPlan = growingPlan;

        if ($scope.growingSystem.growingPlan.type === 'DEPHY_FERME') {
          $scope.growingSystem.modality = 'DETAILLE';
        }

        $scope.growingPlanSelected();
      }
    };
    $scope.selectedGrowingPlan = selectedGrowingPlan;
    if ($scope.selectedGrowingPlan) {
      $scope.selectGrowingPlan(selectedGrowingPlan); // init
    }

    $scope.propagateSectorChange = function(){
      var found = false;
      for (var i=0; i < $scope.flatMarketingDestinationsBySector.length; i++) {
        var objective = $scope.flatMarketingDestinationsBySector[i];
        if (objective.selectedForGS) {
          found = true;
          break;
        }
      }

      var confirm;
      if (found) {
        confirm = $window.confirm("Attention vos modes de commercialisation vont être modifiés !");
      }

      if (!found || confirm) {
        $.ajax({
          url : ENDPOINT_LOAD_MARKETING_DESTINATION_OBJECTIVES_FOR_SECTOR,
          data : {
            'sector' : $scope.growingSystem.sector
          },
          async : false,
          dataType : "json",
          success : function(data) {
            $scope.marketingDestinationsBySector = data;
            $scope.flatMarketingDestinationsBySector = [];
            $scope._setFlatMarketingDestinationsBySector($scope.marketingDestinationsBySector);
          },
          error : function(data, status) {
             console.error("Can't load marketing destination objective");
             addPermanentError("Échec de récupération des objectifs marketing pour cette filière");
          }
        });
      }

    };

    // autocomplete callback
    function setNetwork(topiaId, name) {
      $scope.gsNetworks.push({topiaId: topiaId, name: name});
      $scope.$digest(); // force le refresh
      $("#networks").val("");
    }

    $("#networks").autocomplete(
        {
          source : function(request, response) {

            // On ajoute les identifiants des réseaux déjà sélectionnés
            request.exclusions = [];
            angular.forEach($scope.gsNetworks, function(item) {
              request.exclusions.push(item.topiaId);
            });
            request.onNetworkEdition = false;

            // On remplace par du POST pour les pbs d'encoding
            $.post(ENDPOINTS.searchNetworkJson, request,
                 function(data, status, xhr) {
                      if (data) {
                        var autoCompleteField = $("#networks");
                        if (Object.keys(data).length === 0) {
                          autoCompleteField.addClass("empty-autocomplete-result");
                        } else {
                          autoCompleteField.removeClass("empty-autocomplete-result");
                        }
                      }

                      var result = [];
                      angular.forEach(data, function(value, key) {
                          var network = {
                              label: value,
                              value: value,
                              topiaId: key
                            };
                          result.push(network);
                      });

                      response(result);
                    }, "json");
          },
          minLength : 0,
          select : function(event, ui) {
            if (ui.item) {
              setNetwork(ui.item.topiaId, ui.item.label);
              $scope.growingSystemEditForm.$setDirty();
              return false;
            }
          }
        }
    );

    $scope.displayNetworks = function(gsName, networks) {
      var networkIds = [];
      angular.forEach(networks, function(network) {
          networkIds.push(network.topiaId);
      });

      displayNetworks(gsName || "SdC", networkIds);
    };
  }]
);

AgrosystModule.controller('GrowingSystemCharacteristicController', ['$scope', 'typeAgricultureTopiaId', 'growingSystemCharacteristics', 'growingSystemCharacteristicIds',
  function ($scope, typeAgricultureTopiaId, growingSystemCharacteristics, growingSystemCharacteristicIds) {
    // {String} Topia id du type de culture sélectionné
    $scope.growingSystemCharacteristics = growingSystemCharacteristics;
    $scope.growingSystemCharacteristicTypes = $scope.i18n.GrowingSystemCharacteristicType;
    $scope.typeAgricultureTopiaId = typeAgricultureTopiaId;

    // {Object} growing system characteristic Ids
    $scope.growingSystemCharacteristicIds = {};

    // set id select in map by default
    angular.forEach(growingSystemCharacteristicIds, function(id) {
      $scope.growingSystemCharacteristicIds[id] = true;
    });

    $scope.selectedCharacteristic = function(item) {
      return !!$scope.growingSystemCharacteristicIds[item.topiaId];
    };
  }]
);

function unactivateGrowingSystems(confirmDialog, unactivateForm, activate) {
    var selectionCount = JSON.parse(unactivateForm.children("input[name='growingSystemIds']").val()).length;
    if (selectionCount > 0) {
        confirmDialog.dialog({
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                action: {
                  click: function() {
                    $(this).dialog("close");
                    unactivateForm.attr("action", activate ? ENDPOINTS.growingSystemsUnactivate+"?activate=true" : ENDPOINTS.growingSystemsUnactivate);
                    unactivateForm.submit();
                  },
                  text: activate ? "Activer" : "Désactiver",
                  'class': 'btn-primary'
                },
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}

function validateGrowingSystems(confirmDialog, form, isToValidate) {
    var selectionCount = JSON.parse(form.children("input[name='growingSystemIds']").val()).length;
    if (selectionCount > 0) {
        confirmDialog.dialog({
            resizable: false,
            width: 400,
            modal: true,
            buttons: {
                action: {
                    click: function() {
                        $(this).dialog("close");
                        form.attr("action", isToValidate ? ENDPOINTS.validateGrowingSystems+"?validate=true": ENDPOINTS.validateGrowingSystems);
                        form.submit();
                    },
                    text: (isToValidate ? "Valider" : "dévalider"),
                    'class': 'btn-primary'
                },
                cancel: {
                    click: function() {
                        $(this).dialog("close");
                    },
                    text: 'Annuler',
                    'class': 'float-left btn-secondary'
                }
            }
        });
    }
}
