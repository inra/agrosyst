/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
  */

/**
 * Generates a GUID string, according to RFC4122 standards.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

function mapSize(map) {
  if (!map) {
    return 0;
  }
  var keys = Object.keys(map);
  return keys.length;
}

function isLocalStorageAvailable() {
  var result;
  try {
    result = 'localStorage' in window && window.localStorage !== null;
  } catch (e) {
    // can happen with IE 11 in desktop mode
    result = false;
  }
  return result;
}

var TAB_INDEX_KEY = "tabIndex";
var FORCE_TAB_INDEX_PROPERTY = "forceTabIndex";
function storeTabIndex() {
  if (isLocalStorageAvailable()) {
    var index = $(".tabs-menu li.selected").index();
    localStorage.setItem(TAB_INDEX_KEY, index);
  }
}

function createTabs($tabsname) {
  $('#'+$tabsname+'-menu li').each(function() {
    $(this).click(function() {
      var tabId = $(this).index();
      var tabContent = $('#'+$tabsname+'-content').children()[tabId];
      if (tabContent !== null) {
        tabContent.style.display = 'block';
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');
        $(this).removeClass('error');
        $(tabContent).siblings("div").css('display','none');
      }
    });
  });

  var anchorInit = window.location.hash;
  if (anchorInit && anchorInit != '#') {
    $(".tabs-menu li").get($(".tabs-content " + anchorInit).index()).click();
  }

  // use local storage to restore the current tab index if:
  // - the user refreshes the page
  // - the previous page is the same as the current one
  // - the tab index is forced
  else if (isLocalStorageAvailable()) {
    var tabIndex = localStorage.getItem(TAB_INDEX_KEY);

    var mustDisplayTab = !document.referrer ||
                         (document.referrer.split(/(\?|#)/g)[0]).replace("-input","") == (location.href.split(/(\?|#)/g)[0]).replace("-input","") ||
                         localStorage.getItem(FORCE_TAB_INDEX_PROPERTY);
    if (tabIndex && mustDisplayTab) {
      if ($(".tabs-menu li").get(tabIndex) !== undefined) {
        $(".tabs-menu li").get(tabIndex).click();
      }
    }

    localStorage.removeItem(TAB_INDEX_KEY);
    localStorage.removeItem(FORCE_TAB_INDEX_PROPERTY);

    $(".timeline a").click(function() {
      localStorage.setItem(FORCE_TAB_INDEX_PROPERTY, true);
    });

    $(document).bind('keydown keyup', function(e) {
      //if the user presses F5 or ctrl+R, force the tab index
      if(e.which === 116 || e.which == 82 && (e.ctrlKey || (window.event && window.event.ctrlKey))) {
        localStorage.setItem(FORCE_TAB_INDEX_PROPERTY, true);
      }
    });
  }
}

function toggleContextElement(element) {
  var contextElementContent = element.parent().find('.context-element-content');
  var contextElementOpener = element.parent().find('.context-element-opener');
  if (contextElementContent.is(':visible') ) {
    contextElementContent.hide();
    contextElementOpener.removeClass('opened');
  } else {
    contextElementContent.show();
    contextElementOpener.addClass('opened');
  }
}

function chooseNavigationContext(entity) {
  var contextSelector = $("#context-selector");
  var contextSelectorParent = contextSelector.parent("div.ui-dialog");

  if (!contextSelectorParent.length || !contextSelectorParent.is(":visible")) {
    contextSelector.html("Chargement...");

    contextSelector.dialog({
      modal: true,
      open: function () {
        $(this).load(ENDPOINT_NAVIGATION_CONTEXT_CHOOSE_INPUT, function( response, status, xhr ) {
            if ( status == "error" ) {
              if (xhr.status == "401") {
                $(this).dialog('destroy').remove();
                addPermanentError("Votre session a expirée, veuillez vous reconnecter", 401)
              } else {
                addPermanentError("Une erreur est survenue",  parseInt(xhr.status))
              }
            } else {
              toggleContextElement($(entity + " .context-element-opener"));
            }
        })
      },
      position: { my: "center top", at: "top+30" },
      width: $(window).width() * 0.9,
      height: $(window).height() * 0.8,
      title: "Contexte de navigation",
      classes:{"ui-dialog": "agrosyst-context"}
    });
  } else {
    $(entity + " .context-element-opener").click();
  }
}

function displayLocation(name, latitude, longitude, locationId) {

  if (!latitude || !longitude) {
    alert("Veuillez saisir une latitude et une longitude");
  } else {
    var nameForTitle = name;
    if (!nameForTitle) {
      nameForTitle = "n/c";
    }

    var showLocationDiv = $( "#show-location" );
    showLocationDiv.html("Chargement...");
    var showLocationUrl = ENDPOINTS.showLocationRaw;
    var locationData = {
            "latitude":latitude,
            "longitude":longitude,
            "name":name,
            "locationId":locationId
    };

    showLocationDiv.dialog({
      modal: true,
      open: function () {
        $(this).load(showLocationUrl, locationData);
      },
      position: { my: "center top", at: "top+30" },
      width: $(window).width() * 0.9,
      height: $(window).height() * 0.9,
      title: "Position GPS de : " + nameForTitle
    });
  }
}

function displayEntityAttachments(objectReferenceId, lbTitle) {
  if (objectReferenceId) {
    var showAttachementDiv = $("<div></div>");
    showAttachementDiv.html("Chargement ...");
    showAttachementDiv.dialog({
        modal : true,
        open : function() {
            $(this).load(ENDPOINTS.attachmentsRaw, {
                objectReferenceId : objectReferenceId
            });
        },
        width : $(window).width() * 0.9,
        height : "auto",
        title : lbTitle == undefined ? "Pièces jointes" : lbTitle,
        close: function () {
          // need to remove dialog from dom from angular to it fine on second opening
          $(this).dialog('destroy').remove();
        },
        buttons: {
          action: {
            click: function() {
              $(this).dialog("close");
            },
            text: "Fermer",
            'class': 'btn-primary'
          }
        }
    });
  }
}

function displayAllPublishedMessages() {
  var showBrocastedMessagesDiv = $("<div></div>");
  showBrocastedMessagesDiv.html("Chargement ...");
  showBrocastedMessagesDiv.dialog({
      position: { my: "center top", at: "top+50" },
      width : $(window).width() * 0.5,
      modal : true,
      open : function() {
          $(this).load(ENDPOINTS.publishedMessagesListRaw);
      },
      title : "Messages de diffusion",
      close: function () {
        // need to remove dialog from dom from angular to it fine on second opening
        $(this).dialog('destroy').remove();
      },
      buttons: {
        action: {
          click: function() {
            $(this).dialog("close");
          },
          text: "Fermer",
          'class': 'btn-primary'
        }
      }
  });
}

var infoMessages = [];

function addInfoMessage(text, type, delay, status) {
    if (status === 401) {
      console.error("Authentification error", status);
      text = "Vous êtes déconnecté ! Afin de ne pas perdre vos données veuillez ouvrir un nouvel onglet de votre navigateur (Ctrl+T) et vous reconnecter.";
      delay = null;
    }
    var textKey = text.replace(/\s+/g, '_').toLowerCase().replace(/[^\w\s!]/gi, '');
    if(infoMessages.indexOf(textKey) !== -1) {
      return;// éviter les doublons
    }
    infoMessages.push(textKey);

    var toastsDivName = `div.fixed-toasts div.toast-group`;
    var toastsDiv = $(toastsDivName);
    if (!toastsDiv.length) {
      $('div.fixed-toasts').append(`<div class="toast-group"></div>`);
      toastsDiv = $(toastsDivName);
    }
    var toast = $(`<div id='${textKey}' class="toast ${type}"><div class="type">&nbsp;</div><div class="message">${text}</div><span class="close-button" title="Fermer" onclick="$(this).parent().stop().fadeOut(); removeInfoMessage('${textKey}');">Fermer</span></div>`);
    toast.hide().prependTo(toastsDiv);
    toast.animate({top: `+=400px`}, 200, function() {
      toast.show();
      toast.animate({top: `-=400px`}, 600);
    });
    if (delay) {
      setTimeout(function(){ toast.fadeOut(); removeInfoMessage(textKey); }, delay);
    }
}

function removeInfoMessage(textKey) {
  var messageIndex = infoMessages.indexOf(textKey);
  if(messageIndex !== -1) {
    infoMessages.splice(messageIndex, 1);
    document.getElementById(textKey).remove();
  }
}

function addInlineMessage(anchor, type, text) {
  $(`${anchor}`).append(`<div class="inline-toasts toasts"></div>`);
  $(`${anchor} .inline-toasts`).append(`<div class="toast-group"></div>`);
  var toastsDiv = $(`${anchor}.inline-toasts div.toast-group`);
  var toast = $(`<div class="toast ${type}"><div class="type">&nbsp;</div><div class="message">${text}</div><span class="close-button" title="Fermer" onclick="$(this).parent().stop().fadeOut();">Fermer</span></div>`);
  toast.hide().prependTo(toastsDiv);
  toast.show();
}

function addPermanentWarning(text) {
  addInfoMessage(text, "warning");
}

function addPermanentError(text, status) {
  addInfoMessage(text, "error", null, status);
}

function addSuccessMessage(text) {
  addInfoMessage(text, "success", 15000);
}

/*
 * On recherche tous les résultats des « &lt;s:actionerror cssClass="send-toast-to-js"/&gt; »,
 * on les cache (cf CSS) et on ajoute des notifications
 */
function addPermanentActionErrors() {
  /* Code HTML typique :
   <ul class="send-toast-to-js">
     <li><span>A</span></li>
     <li><span>B</span></li>
     <li><span>C</span></li>
   </ul>
  */
  var messages = $('ul.send-toast-to-js li span');
  var messagesArray = messages.toArray();
  angular.forEach(messagesArray, function (errorSpan) {
    addPermanentError(errorSpan.innerText);
  });
}

function displayNetworks(growingSystemName, networkIds) {

  if (!networkIds) {
    alert("Aucun réseau");
  } else {
    var showNetworksDiv = $( "#show-networks" );
    showNetworksDiv.html("Chargement...");

    if (networkIds.length > 0) {
      var networksDatas = {
           "growingSystemName" : growingSystemName,
           "parentNetworkIds": networkIds
      };
      $.ajax({
        url : ENDPOINTS.showGrowingSystemAndNetworkGraphRaw,
        data : networksDatas,
        type : "POST",
        async : false,
        traditional : true,
        success : function(data) {
          showNetworksDiv.dialog({
            modal: true,
            open: function () {
              $(this).html(data);
            },
            width: $(window).width() * 0.9,
            height: "auto",
            title: "Hiérarchie des réseaux"
          });
        }
      });
    } else {
      showNetworksDiv.dialog({
        modal: true,
        open: function () {
          $(this).html("Pas de réseaux disponibles");
        },
        width: $(window).width() * 0.9,
        height: "auto",
        title: "Hiérarchie des réseaux"
      });
    }
  }
}

/**
 * Display dialog div and submit internal dialog form.
 */
function displayDialogAndSubmitForm(importDialog) {
    importDialog.dialog({
        resizable: false,
        width: 600,
        modal: true,
        buttons: {
            valid: {
              click: function () {
                $(this).find("form").submit();
                displayPageLoading();
                $(this).dialog("close");
              },
              text: 'Importer',
              'class': 'btn-primary'
            },
            cancel: {
              click: function() {
                $(this).dialog("close");
              },
              text: 'Annuler',
              'class': 'float-left btn-secondary'
            }
        }
    });
}

// Source: http://stackoverflow.com/questions/3426404/create-a-hexadecimal-colour-based-on-a-string-with-jquery-javascript
function stringToColor(str) {
  // str to hash
  var i;
  var hash;
  var colour;
  for (i = 0, hash = 0; i < str.length; hash = str.charCodeAt(i++) + ((hash << 5) - hash));

  // int/hash to hex
  for (i = 0, colour = "#"; i < 3; colour += ("00" + ((hash >> i++ * 8) & 0xFF).toString(16)).slice(-2));

  return colour;
}

// Hide fixed notification message after delay
function hideNotificationsMessage () {
  $("body > .messages-panel .infoMessage li").delay(30000).fadeOut();
  $("body > .messages-panel .warningMessage li").delay(30000).fadeOut();
  $("body > .messages-panel .errorMessage li").delay(50000).fadeOut();
  $("body > .messages-panel .successMessage li").delay(50000).fadeOut();
}

function getBrowserNameAndVersion() {
    var ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    M= M[2]? [M[1], M[2]]:[navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/([\.\d]+)/i)) !== null) M[2]= tem[1];
    return M.join(' ');
}

function getOperatingSystemNameAndVersion() {
  var OSName;
  if (navigator.userAgent.indexOf("Windows NT 10.1")!=-1) OSName="Windows 10.1";
  if (navigator.userAgent.indexOf("Windows NT 10.0")!=-1) OSName="Windows 10";
  if (navigator.userAgent.indexOf("Windows NT 6.3")!=-1) OSName="Windows 8.1";
  if (navigator.userAgent.indexOf("Windows NT 6.2")!=-1) OSName="Windows 8";
  if (navigator.userAgent.indexOf("Windows NT 6.1")!=-1) OSName="Windows 7";
  if (navigator.userAgent.indexOf("Windows NT 6.0")!=-1) OSName="Windows Vista";
  if (navigator.userAgent.indexOf("Windows NT 5.1")!=-1) OSName="Windows XP";
  if (navigator.userAgent.indexOf("Windows NT 5.0")!=-1) OSName="Windows 2000";
  if (navigator.userAgent.indexOf("Mac")!=-1) OSName="Mac/iOS";
  if (navigator.userAgent.indexOf("X11")!=-1) OSName="UNIX";
  if (navigator.userAgent.indexOf("Linux")!=-1) OSName="Linux";
  return OSName;
}

function getEnvInfo() {
  var result = "Navigateur : " + getBrowserNameAndVersion() + "\n";
  result += "OS : " + getOperatingSystemNameAndVersion() + "\n";
  result += "Platform : " + navigator.platform + "\n";
  result += "Résolution : " + screen.width + "x" + screen.height + "\n";
  result += "Taille d'affichage : " + window.innerWidth + "x" + window.innerHeight + "\n";
  result += "Locale : " + navigator.language + "\n";
  return result;
}

$('#isNotNetworkIrBlock').hide();
$('#itEmailBlock').hide();

function checkIsDephyFerme() {
  $('#feedbackCategory option:selected').prop('selected', false);
  var res = $('input[name=dephyType]:checked', '#feedBackForm').val() == 'DEPHY_FERME';
  return res;
}

function showIsDephyFerme() {
  if (checkIsDephyFerme()) {
    $('#isNotNetworkIrBlock').slideDown();
    $("option[value='ETAT_LIEUX_SAISIES']").css('display', 'inherit');
    $("option[value='LOCAL_INTRANTS']").css('display', 'inherit');
  } else {
    $('input[name=isNotNetworkIr]', '#feedBackForm').prop('checked', false);
    $('#isNotNetworkIrBlock').slideUp();
    $('#itEmailBlock').slideUp();
    $("option[value='ETAT_LIEUX_SAISIES']").css('display', 'none');
    $("option[value='LOCAL_INTRANTS']").css('display', 'none')
  }
}

function isForItOnly() {
  var res = $('input[name=dephyType]:checked', '#feedBackForm').val() == 'DEPHY_FERME' && !$('input[name=isNotNetworkIr]', '#feedBackForm').is(':checked')
  return res;
}

function showForItOnly() {
  showIsDephyFerme();
  var res = checkIsDephyFerme() && !$('input[name=isNotNetworkIr]', '#feedBackForm').is(':checked')
  if (res) {
    $('#itEmailBlock').slideDown();
  } else {
    $('#itEmailBlock').slideUp();
  }
  filterCategoriesIfNeeded();
  return res;
}

function filterCategoriesIfNeeded() {
  let sectorTypes = getFeedbackSectors();

  // Masquer la catégorie Fonctionnement d'Agrosyst si la demande ne concerne que la filière
  // grandes cultures ou la filière polyculture élevage, ou les 2.
  // S'il y a d'autres filières, on ne masque pas l'option.
  let hideCategory = sectorTypes.length > 0 &&
    sectorTypes
      .filter(sectorType => sectorType !== 'GRANDES_CULTURES' && sectorType !== 'POLYCULTURE_ELEVAGE')
      .length === 0
  ;

  if (checkIsDephyFerme() && hideCategory) {
    $("option[id='FONCTIONNEMENT_AGROSYST']", "#feedBackForm").hide();
  } else {
    $("option[id='FONCTIONNEMENT_AGROSYST']", "#feedBackForm").show();
  }
}

function mapBy(array, attribute) {
  let map = {};
  array.forEach(el => {
    let items = map[el[attribute]];
    if (!items) {
      map[el[attribute]] = [ el ];
    } else {
      items.push(el);
    }
  });
  return map;
}

function checkRequiredEmail() {
  if (isForItOnly()) {
    $( "it-email" ).prepend( "<span id='email_required' class='required''>*</span>" );
    $("input[name='layoutData.itEmail']", '#feedBackForm').attr('required', true)
  } else {
    $('it-email').each(function(){
      var $this = $(this),
      span = $this.find('span'),
      text = span.text();
      span.remove();
    });
    $("input[name='layoutData.itEmail']", '#feedBackForm').removeAttr('required')
  }
};

function isWellFormattedEmail(email) {
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  var res = regex.test(email);
  return res;
}

function displayWrongEmail() {

  $('#wrongEmail').dialog({
      width: 600,
      modal: true,
      open: function() {},
      buttons: {
        ok: {
          text: "Ok",
          'class': 'btn-secondary',
          click: function() {
            $(this).dialog("close");
          }
        }
      }
  })
}

function emailValidationCallbackAndSend(data, textStatus, jqXHR) {
  if (textStatus !== "success" || !data) {
    displayWrongEmail();
  } else {
    sendFeedBack()
  }
}

function emailValidationCallback(data, textStatus, jqXHR) {
  if (textStatus !== "success" || !data) {
    displayWrongEmail();
  }
}

function getFeedbackSectors() {
  var sectorTypes = [];
  var sectorKeys = Object.keys(I18N.Sector);
  for(var i = 0; i < sectorKeys.length; i++) {
    var key = 'sectorTypes_' + sectorKeys[i];
    var resForKey = $("input[name='"+ key +"']:checked", "#feedBackForm").val();
    if (resForKey) {
      sectorTypes.push(resForKey);
    }
  }
  return sectorTypes;
}

function _valideItEmail(callback) {
  var itEmail = $("input[name='layoutData.itEmail']", '#feedBackForm').val();

  if (isWellFormattedEmail(itEmail)) {

    // sendFeedBack() is managed into the callback function
    var emailValidationObject = {email: itEmail};
    $.post(ENDPOINTS.validEmailJson, emailValidationObject, callback, "json");

  } else {
    // feedback will not be send and dialog not closed.
    displayWrongEmail();
  }

}

function valideItEmail() {
  _valideItEmail(emailValidationCallback);
}

function validateAndSendFeedbackForm(labels) {
  if (!$('input[name=dephyType]:checked', '#feedBackForm').val() && !$('input[name=dephyType]:hidden', '#feedBackForm').val()) {
    window.alert("Aucun dispositif sélectionné");
    return;
  }

  var sectorTypes = getFeedbackSectors();
  if (sectorTypes.length == 0) {
    window.alert(labels.noSector || "Aucune filière sélectionnée");
    return;
  }

  if (!$('select[name=feedbackCategory]', '#feedBackForm').val()) {
      window.alert(labels.noCategory || "Aucune catégorie sélectionnée");
      return;
  }

  if (isForItOnly()) {
    // first we need to validate the email
    _valideItEmail(emailValidationCallbackAndSend);
  } else {
    // at this point the form is valid and can be send
    sendFeedBack();
  }

}

function sendFeedBack() {

    var sectorTypes = getFeedbackSectors();
    var itEmail = $("input[name='layoutData.itEmail']", '#feedBackForm').val();
    var dephyType = $('input[name=dephyType]:checked', '#feedBackForm').val() || $('input[name=dephyType]:hidden', '#feedBackForm').val();
    var feedback = $('textarea[name=feedback]', '#feedBackForm').val();
    var feedbackCategory = $('select[name=feedbackCategory]', '#feedBackForm').val();
    var appVersion = $('input[name=appVersion]', '#feedBackForm').val();
    var feedbackAttachment = $('input[name=feedbackAttachment]', '#feedBackForm')[0].files[0];
    var requestedUrl = $('meta[property=meta-requested]').attr("content");
    var refererUrl = $('meta[property=meta-requested]').attr("content");
    var feedbackScreenshot = $('input[name=feedbackScreenshot]:checked', '#feedBackForm').val();
    var isNotNetworkIrChecked = $('input[name=isNotNetworkIr]', '#feedBackForm').is(':checked');

    var feedbackObject = {
      env: getEnvInfo(),
      feedback: feedback,
      feedbackCategory : feedbackCategory,
      clientAppVersion: appVersion,
      locationTitle: window.document.title,
      location: window.location.href,
      requested: requestedUrl,
      referer: refererUrl,
      sectorTypes: sectorTypes,
      itEmail: itEmail,
      dephyType: dephyType,
      isNotNetworkIr: isNotNetworkIrChecked
    };

    // must be closed first to take screenshot without dialog
    $("#feedBackDialog").dialog( "close" );

    var promises = [];
    if (feedbackScreenshot) {
      // take screenshot
      var promise = new Promise((resolve, reject) => html2canvas(document.body, {
        onrendered: function(canvas) {
          feedbackObject.screenshot = canvas.toDataURL("image/jpeg");
          resolve(feedbackObject);
        }
      }));
      promises.push(promise);
    }
    if (feedbackAttachment) {
      var promise = new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onload = function () {
          feedbackObject.attachment = reader.result;
          feedbackObject.attachmentName = feedbackAttachment.name;
          feedbackObject.attachmentType = feedbackAttachment.type;
          resolve(feedbackObject);
        };
        reader.readAsDataURL(feedbackAttachment);
      });
      promises.push(promise);
    }
    Promise.all(promises).then(() => $.post(ENDPOINTS.feedbackJson, feedbackObject, showFeedBackDialogCallback, "json"));
}

function showFeedBackDialog(labels) {

  $('#feedBackDialog').dialog({
    width: 700,
    minWidth: 700,
    resizable: false,
    modal: true,
    open: function() {
      $("#DEPHY_FERME").prop("checked", true);
      $('#isNotNetworkIrBlock').slideDown();
      $('#itEmailBlock').slideDown();
      $(this).find("textarea[name='feedback']").val("");
    },
    buttons: {
      cancel: {
        text: labels.cancel || "Annuler",
        class: 'btn-secondary',
        click: function() {
          $(this).dialog("close");
        }
      },
      send: {
        text: labels.send || "Envoyer",
        class: 'btn-primary',
        click: function() {
          // extract form params
          validateAndSendFeedbackForm(labels);
        }
      }
    }

  });
}
//TODO
function showFeedBackDialogCallback(data, textStatus, jqXHR) {
  $('#feedbackResult').remove();
  if (textStatus == "success") {
    if (data === null || "FAILED" === data.feedbackStatus) {
      $('#feedbackResultFailed', '#feedBackDialogReturn').show();

    } else if ("SUCCESS" === data.feedbackStatus) {
      var recipientsEmails = "" + data.feedbackRecipients;
      recipientsEmails = recipientsEmails.replace(/,/g, ',</li><li>');
      $('#recipientList', '#feedbackResultSuccess').empty();
      $('#recipientList', '#feedbackResultSuccess').append($("<li class='remove-list-element'>" + recipientsEmails + "</li>"));
      $('#feedbackResultSuccess', '#feedBackDialogReturn').show();

    } else if ("NOT_SEND" === data.feedbackStatus) {
      $('#feedbackResultNotSend', '#feedBackDialogReturn').show();
    }
  } else {
    $('#feedbackResultError', '#feedBackDialogReturn').show();
  }

  $('#feedBackDialogReturn').dialog({
    width: 1024,
      modal: true,
      open: function() {},
      buttons: {
        ok: {
          text: "Ok",
          'class': 'btn-secondary',
          click: function() {
            $(this).dialog("close");
            $('#feedbackResult').remove();
          }
        }
      }
  })
}

/* Detect when Enter key is pressed when user is filling a form */
function detectEnterKeyPressed (bindingScope) {
  $(bindingScope).bind('keydown', function(e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
      var focus = $("*:focus");
      if (!focus.is("textarea") && !focus.is("select") && !focus.hasClass('ui-autocomplete-input')) {

        var parentSubForm = focus.parents(".sub-form");
        if (parentSubForm.length) {
          parentSubForm.find('input.btn-darker:enabled').click();
        }

        var parentDialog = $(".ui-dialog:visible");
        if (parentDialog.length) {
          parentDialog.find('.btn-primary:enabled').click();
        }
      return false;
      }

    }
  });

}

/* Hack to fixe the problem of printing fieldset with Firefox  */
function handlePrintFieldset () {
    // Firefox print debug
    if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){  // if borwser is firefox
        $(window).bind('beforeprint', function(){
            $('fieldset').each(
                function(item)
                {
                    $(this).replaceWith('<div id="fieldsetReplaced'+item+'">'+$(this).html()+'</div>');
                    $('div#fieldsetReplaced'+item).css('display','inline').css('text-align','left');
                }
            );
        });
        $(window).bind('afterprint', function(){
            $('.fieldset').remove();
        });
    }
}

/**
 * Document ready.
 */
$(function () {

//  Struts doesn't like the jQuery 1.4+ format, you can use the traditional format though
//  http://stackoverflow.com/questions/3508550/http-array-parameters-with-struts-2-via-an-ajax-call
  $.ajaxSettings.traditional = true;

  $('.tabs-menu').each( function () {
    var id = $(this).attr('id');
    createTabs (id.substr(0, id.length - 5));
  });

  // Ajout de comportement sur le bandeau du contexte de navigation
  $("#context-panel ul li.icon-campaign").click(function () {
    chooseNavigationContext(".campaign");
  });
  $("#context-panel ul li.icon-networkContext").click(function () {
    chooseNavigationContext(".networkContext");
  });
  $("#context-panel ul li.icon-domain").click(function () {
    chooseNavigationContext(".domain");
  });
  $("#context-panel ul li.icon-growingPlan").click(function () {
    chooseNavigationContext(".growingPlan");
  });
  $("#context-panel ul li.icon-growingSystem").click(function () {
    chooseNavigationContext(".growingSystem");
  });

  // toolitp multiligne pour les elements de classe 'agrosyst-tooltip'
  $(".agrosyst-tooltip").tooltip({
      content: function() {
          return $(this).attr('title').replace(/\n/g, '<br />');
      }
  });

  detectEnterKeyPressed('.tabs-content');

  hideNotificationsMessage();

  checkBrowser();

  handlePrintFieldset();

});

/**
 * Controller de la liste des domaines.
 * Double utilisation possible avec ou sans contexte.
 */
AgrosystModule.controller('DomainsListController', ['$scope', '$http', '$timeout', '$filter', 'ListDomainsInitData', 'ContextFilterInit', 'domainFilter', 'asyncThreshold',
  function($scope, $http, $timeout, $filter, ListDomainsInitData, ContextFilterInit, domainFilter, asyncThreshold) {
    $scope.domains = ListDomainsInitData.elements;
    $scope.pager = ListDomainsInitData;
    $scope.contextFilter = ContextFilterInit;
    $scope.filter = domainFilter;
    $scope.asyncThreshold = asyncThreshold;
    //XXX ymartel 2014/02/18 : should be convert Boolean to String for binding until resolution of https://github.com/angular/angular.js/issues/6297
    if (angular.isDefined(domainFilter.active)) {
        $scope.filter.active = domainFilter.active.toString();
    }

    $scope.selectedEntities = $scope.selectedDomains || {};

    $scope._selectedEntities = {};

    $scope.allSelectedEntities = [];

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0, 1);
    };

    _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.domains);

    $scope.toggleSelectedEntities = function() {
      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.domains);
    };

    $scope.toggleSelectedEntity = function(entityId) {
      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);
    };

    $scope.clearSelection = function() {
      angular.forEach($scope.selectedEntities, (value, key) => delete $scope.selectedEntities[key]);
      $scope._selectedEntities = {};
      $scope.allSelectedEntities = [];
    };

    //$scope.firstSelectedDomain;
    //$scope.allSelectedDomainActive;
    $scope.domainTypes = $scope.i18n.DomainType;

    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) { return; } // prevent init useless call

      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.filter);

      var filterSelected = localFilter.selected;
      if (filterSelected) {
        localFilter.selectedIds = $filter('toSelectedValue')($scope.selectedEntities);
      }
      delete localFilter.selected;

      if (localFilter.campaign === "") {
        delete localFilter.campaign;
      }
      if (localFilter.active === "") {
        delete localFilter.active;
      }

      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
      if ($scope.contextFilter.fromNavigationContextChoose) {
        ajaxRequest += "&fromNavigationContextChoose=true";
      }
      angular.forEach($scope.contextFilter.selectedCampaigns, function(value, key) {
        if (value) {
          ajaxRequest += "&selectedCampaigns=" + key;
        }
      });
      angular.forEach($scope.contextFilter.selectedNetworks, function(value, key) {
        if (value) {
          ajaxRequest += "&selectedNetworks=" + key;
        }
      });
      displayPageLoading();
      $http.post(ENDPOINTS.domainsListJson, ajaxRequest, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' }} )
        .then(function(response) {
          $scope.domains = response.data.elements;
          $scope.pager = response.data;
          _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.domains);
        })
        .catch(function(response) {
          delete $scope.domains;
          delete $scope.pager;
          console.error("Can't get domains list", response);
          var message = "Échec de récupération de la liste des domaines";
          addPermanentError(message, response.status);
        })
        .finally(function() {
          hidePageLoading();
        });
    };

    $scope.selectAllEntities = function() {
      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.filter);
      if (localFilter.campaign === "") {
        delete localFilter.campaign;
      }
      if (localFilter.active === "") {
        delete localFilter.active;
      }

      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
      if ($scope.contextFilter.fromNavigationContextChoose) {
          ajaxRequest += "&fromNavigationContextChoose=true";
      }
      angular.forEach($scope.contextFilter.selectedCampaigns, function(value, key) {
          if (value) {
              ajaxRequest += "&selectedCampaigns=" + key;
          }
      });
      angular.forEach($scope.contextFilter.selectedNetworks, function(value, key) {
          if (value) {
              ajaxRequest += "&selectedNetworks=" + key;
          }
      });

      displayPageLoading();

      $http.post($scope.endpoints.domainIdsJson, ajaxRequest, { headers: {'Content-Type': 'application/x-www-form-urlencoded' }})
        .then(function(response) {
          var selectedEntities = {};
          angular.forEach(response.data, function(topiaId) {
            selectedEntities[topiaId] = true;
          });
          Object.assign($scope.selectedEntities, selectedEntities);
        })
        .catch(function(response) {
          console.error("Can't get domains ids", response);
          var message = "Échec de récupération de la liste des ids des domaines";
          addPermanentError(message, response.status);
        })
        .finally(function() {
          hidePageLoading();
        });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.domainName, filter.siret, filter.campaign, filter.mainContact, filter.responsable, filter.departement, contextFilter.selectedCampaigns, contextFilter.selectedNetworks]',
      function(newValue, oldValue) {
        if (timer) {
          $timeout.cancel(timer);
        }
        timer = $timeout(function() {
          let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
        }, 350);
       }, true);

    // watch without timer
    $scope.$watch('[filter.type, filter.active]', function(newValue, oldValue) {
      let page = $scope.filter.page;
      $scope.filter.page = 0;
      if (!page) {
        $scope.refresh(newValue, oldValue);
      }
    }, true);

    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.$watch('[selectedEntities]', function(newValue, oldValue) {
      // save domain entities
      angular.forEach($scope.domains, function(elt) {
        if ($scope.selectedEntities[elt.topiaId]) {
          $scope._selectedEntities[elt.topiaId] = elt;
        } else {
          delete $scope._selectedEntities[elt.topiaId];
        }
      });
      // active property and first selected
      $scope.allSelectedDomainActive = true;
      angular.forEach($scope._selectedEntities, function(domain, domainId) {
        $scope.allSelectedDomainActive &= domain.active;
        $scope.firstSelectedDomain = domain; // make sense when size() == 1
      });

      $scope.filter.selected = !!$scope.filter.selected && $filter('toSelectedLength')($scope.selectedEntities) > 0;
      if ($scope.filter.selected) {
        $scope.refresh(true);
      }
    }, true);

    $scope.$watch('filter.selected', function(newValue, oldValue) {
      if (newValue != oldValue) {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(true);
        }
      }
    }, true);

    $scope.asyncDomainExport = function() {
      var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

      var data = "domainIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
      $http.post(
          ENDPOINTS.domainsExportAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };
}]);

AgrosystModule.controller('GrowingPlansListController', ['$scope', '$http', '$timeout', '$filter', 'ListGrowingPlansInitData', 'ContextFilterInit', 'growingPlanFilter', 'asyncThreshold',
  function($scope, $http, $timeout, $filter, ListGrowingPlansInitData, ContextFilterInit, growingPlanFilter, asyncThreshold) {
    $scope.growingPlans = ListGrowingPlansInitData.elements;
    $scope.pager = ListGrowingPlansInitData;
    $scope.filter = growingPlanFilter;
    $scope.asyncThreshold = asyncThreshold;
    //XXX ymartel 2014/02/18 : should be convert Boolean to String for binding until resolution of https://github.com/angular/angular.js/issues/6297
    if (angular.isDefined(growingPlanFilter.active)) {
      $scope.filter.active = growingPlanFilter.active.toString();
    }

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0,1);
    };

    $scope.selectedEntities = $scope.selectedGrowingPlans || {};

    $scope._selectedEntities = {};

    $scope.allSelectedEntities = [];

    _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.growingPlans);

    $scope.contextFilter = ContextFilterInit;
    //$scope.firstSelectedGrowingPlan;
    //$scope.allSelectedGrowingPlanActive;
    $scope.dephyTypes = $scope.i18n.TypeDEPHY;

    $scope.toggleSelectedEntities = function() {
      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.growingPlans);
    };

    $scope.toggleSelectedEntity = function(entityId) {
      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);
    };

    $scope.clearSelection = function() {
      angular.forEach($scope.selectedEntities, (value, key) => delete $scope.selectedEntities[key]);
      $scope._selectedEntities = {};
      $scope.allSelectedEntities = [];
    };

    $scope.refresh = function(newValue, oldValue) {
        if (newValue === oldValue) { return; } // prevent init useless call

        // remove empty value without modifying initial model
        var localFilter = angular.copy($scope.filter);

        var filterSelected = localFilter.selected;
        if (filterSelected) {
          localFilter.selectedIds = $filter('toSelectedValue')($scope.selectedEntities);
        }
        delete localFilter.selected;

        if (localFilter.campaign === "") {
          delete localFilter.campaign;
        }
        if (localFilter.active === "") {
          delete localFilter.active;
        }

        // get request filter
        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
        if ($scope.contextFilter.fromNavigationContextChoose) {
            ajaxRequest += "&fromNavigationContextChoose=true";
        }
        angular.forEach($scope.contextFilter.selectedCampaigns, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedCampaigns=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedNetworks, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedNetworks=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedDomains, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedDomains=" + key;
            }
        });

        // perform ajax
        displayPageLoading();
        $http.post(ENDPOINTS.growingPlansListJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
        then(function(response) {
            $scope.growingPlans = response.data.elements;
            $scope.pager = response.data;
            _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.growingPlans);
        }).
        catch(function(response) {
          console.error("Can't get growing plan list", response);
          var message = "Échec de récupération de la liste des dispositifs";
          addPermanentError(message, response.status);
        }).finally(function() {
          hidePageLoading();
        });
    };

    $scope.selectAllEntities = function() {
        // remove empty value without modifying initial model
        var localFilter = angular.copy($scope.filter);
        if (localFilter.campaign === "") {
          delete localFilter.campaign;
        }
        if (localFilter.active === "") {
          delete localFilter.active;
        }

        // get request filter
        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
        if ($scope.contextFilter.fromNavigationContextChoose) {
            ajaxRequest += "&fromNavigationContextChoose=true";
        }
        angular.forEach($scope.contextFilter.selectedCampaigns, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedCampaigns=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedNetworks, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedNetworks=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedDomains, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedDomains=" + key;
            }
        });
        displayPageLoading();
        $http.post($scope.endpoints.growingPlanIdsJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .then(function(response) {
            var selectedEntities = {};
            angular.forEach(response.data, function(topiaId) {
              selectedEntities[topiaId] = true;
            });
            Object.assign($scope.selectedEntities, selectedEntities);
        })
        .catch(function(response) {
          console.error("Can't get growing plan ids", response);
          var message = "Échec de récupération de la liste des ids des dispositifs";
          addPermanentError(message, response.status);
        })
        .finally(function() {
          hidePageLoading();
        });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.growingPlanName, filter.domainName, filter.mainContact, filter.network, filter.campaign, filter.mainContact, contextFilter.selectedCampaigns, contextFilter.selectedNetworks, contextFilter.selectedDomains]',
      function(newValue, oldValue) {
        if (newValue == oldValue) { return; } // prevent useless call
        if (timer) {
            $timeout.cancel(timer);
        }
        timer = $timeout(function(){
          let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
        }, 350);
      }, true);
    // watch without timer
    $scope.$watch('[filter.active, filter.typeDephy]', function(newValue, oldValue) {
      if(newValue !== oldValue) {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      }
    }, true);
    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.$watch('[selectedEntities]', function(newValue, oldValue) {
        // save growing plan entities
        angular.forEach($scope.growingPlans, function(elt) {
            if ($scope.selectedEntities[elt.topiaId]) {
                $scope._selectedEntities[elt.topiaId] = elt;
            } else {
                delete $scope._selectedEntities[elt.topiaId];
            }
        });
        // active property and first selected
        $scope.allSelectedGrowingPlanActive = true;
        angular.forEach($scope._selectedEntities, function(growingPlan) {
            $scope.allSelectedGrowingPlanActive &= growingPlan.active;
            $scope.firstSelectedGrowingPlan = growingPlan; // make sense when size() == 1
        });

        $scope.filter.selected = !!$scope.filter.selected && $filter('toSelectedLength')($scope.selectedEntities) > 0;
        if ($scope.filter.selected) {
          $scope.refresh(true);
        }
    }, true);

    $scope.$watch('filter.selected', function(newValue, oldValue) {
      if (newValue != oldValue) {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(true);
        }
      }
    }, true);

    $scope._loadDomainsForDuplication = function() {
      var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson($scope.filter)) + "&pagination=false";
      displayPageLoading();
      $http.post(ENDPOINTS.domainsListJson, ajaxRequest,
             {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
         $scope.domains = response.data.elements;
         $scope.$broadcast('domainsForDuplicationLoaded', $scope.domains);
      }).
      catch(function(response) {
        delete $scope.domains;
        console.error("Can't get domains list", response.status);
        var message = "Échec de récupération de la liste des domaines";
        addPermanentError(message, response.status);
      }).finally(function() {
        hidePageLoading();
      });
    };

    $scope._growingPlanDuplicate = function() {
      var selectionCount = Object.keys($scope.selectedEntities).length;
      if (selectionCount === 1) {
        var confirmDialog = $('#confirmDuplicateGrowingPlans');
        confirmDialog.dialog({
            resizable: false,
            width: 350,
            modal: true,
            buttons: {
                "Dupliquer": {
                  click : function() {
                    $(this).dialog("close");
                    $(growingplansListForm).attr("action", ENDPOINTS.growingPlansDuplicate);
                    $(growingplansListForm).submit();
                  },
                  text: 'Dupliquer',
                  'class': 'btn-primary'
                },
                "Annuler": {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
      }
    };

    $scope.$on('domainsForDuplicationLoaded', function(event, domains) {
      if (domains) {
       $scope._growingPlanDuplicate();
      }
    });

    /**
    * Duplicate growing plan after user confirm.
    */
    $scope.growingPlansDuplicate = function() {
     if(!$scope.domains) {
       $scope._loadDomainsForDuplication();
     } else {
       $scope._growingPlanDuplicate();
     }
    };

    $scope.asyncGrowingPlansExport = function() {
      var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

      var data = "growingPlanIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
      $http.post(
          ENDPOINTS.growingPlansExportAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };

}]);

AgrosystModule.controller('GrowingSystemsListController', ['$scope', '$timeout', '$filter', 'ListGrowingSystemsInitData', 'ContextFilterInit', '$http', 'growingSystemFilter', 'asyncThreshold',
  function($scope, $timeout, $filter, ListGrowingSystemsInitData, ContextFilterInit, $http, growingSystemFilter, asyncThreshold) {
    $scope.growingSystems = ListGrowingSystemsInitData.elements;
    $scope.pager = ListGrowingSystemsInitData;
    $scope.filter = growingSystemFilter;
    $scope.asyncThreshold = asyncThreshold;
    //XXX ymartel 2014/02/18 should be convert Boolean to String for binding until resolution of https://github.com/angular/angular.js/issues/6297
    if (angular.isDefined(growingSystemFilter.active)) {
        $scope.filter.active = growingSystemFilter.active.toString();
    }

    $scope.selectedEntities = $scope.selectedGrowingSystems || {};

    $scope._selectedEntities = {};

    $scope.allSelectedEntities = [];

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0,1);
    };

    _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.growingSystems);

    $scope.toggleSelectedEntities = function() {
      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.growingSystems);
    };

    $scope.toggleSelectedEntity = function(entityId) {
      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);
    };

    $scope.clearSelection = function() {
      angular.forEach($scope.selectedEntities, (value, key) => delete $scope.selectedEntities[key]);
      $scope._selectedEntities = {};
      $scope.allSelectedEntities = [];
    };

    $scope.contextFilter = ContextFilterInit;
    $scope.sectors = $scope.i18n.Sector;

    $scope.refresh = function(newValue, oldValue) {
        if (newValue === oldValue) { return; } // prevent init useless call

        // remove empty value without modifying initial model
        var localFilter = angular.copy($scope.filter);

        var filterSelected = localFilter.selected;
        if (filterSelected) {
          localFilter.selectedIds = $filter('toSelectedValue')($scope.selectedEntities);
        }
        delete localFilter.selected;

        if (localFilter.campaign === "") {
          delete localFilter.campaign;
        }
        if (localFilter.active === "") {
          delete localFilter.active;
        }
        if (localFilter.validated === "") {
          delete localFilter.validated;
        }

        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
        if ($scope.contextFilter.fromNavigationContextChoose) {
            ajaxRequest += "&fromNavigationContextChoose=true";
        }
        angular.forEach($scope.contextFilter.selectedCampaigns, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedCampaigns=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedNetworks, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedNetworks=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedDomains, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedDomains=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedGrowingPlans, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedGrowingPlans=" + key;
            }
        });

        // perform ajax
        displayPageLoading();
        $http.post(ENDPOINTS.growingSystemsListJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
        then(function(response) {
            $scope.growingSystems = response.data.elements;
            $scope.pager = response.data;
            _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.growingSystems);
        }).
        catch(function(response) {
          console.error("Can't get growing system list", response.status);
          var message = "Échec de récupération des systèmes de culture";
          addPermanentError(message, response.status);
        }).finally(function() {
          hidePageLoading();
        });
    };

    $scope.selectAllEntities = function() {
        // remove empty value without modifying initial model
        var localFilter = angular.copy($scope.filter);
        if (localFilter.campaign === "") {
          delete localFilter.campaign;
        }
        if (localFilter.active === "") {
          delete localFilter.active;
        }
        if (localFilter.validated === "") {
          delete localFilter.validated;
        }

        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
        if ($scope.contextFilter.fromNavigationContextChoose) {
            ajaxRequest += "&fromNavigationContextChoose=true";
        }
        angular.forEach($scope.contextFilter.selectedCampaigns, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedCampaigns=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedNetworks, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedNetworks=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedDomains, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedDomains=" + key;
            }
        });
        angular.forEach($scope.contextFilter.selectedGrowingPlans, function(value, key) {
            if (value) {
                ajaxRequest += "&selectedGrowingPlans=" + key;
            }
        });

        displayPageLoading();
        $http.post($scope.endpoints.growingSystemIdsJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .then(function(response) {
            var selectedEntities = {};
            angular.forEach(response.data, function(topiaId) {
              selectedEntities[topiaId] = true;
            });
            Object.assign($scope.selectedEntities, selectedEntities);
        })
        .catch(function(response) {
          console.error("Can't get growing system ids", response.status);
          var message = "Échec de récupération de la liste des ids des systèmes de culture";
          addPermanentError(message, response.status);
        })
        .finally(function() {
          hidePageLoading();
        });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.growingSystemName, filter.growingPlanName, filter.domainName, filter.network, filter.campaign, filter.dephyNumber, contextFilter.selectedCampaigns, contextFilter.selectedNetworks, contextFilter.selectedDomains, contextFilter.selectedGrowingPlans]',
      function(newValue, oldValue) {
        if (timer) {
            $timeout.cancel(timer);
        }
        timer = $timeout(function(){
          let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
        }, 350);
       }, true);
    // watch without timer
    $scope.$watch('[filter.active, filter.sector, filter.validated]', function(newValue, oldValue) {
      let page = $scope.filter.page;
      $scope.filter.page = 0;
      if (!page) {
        $scope.refresh(newValue, oldValue);
      }
    }, true);
    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.$watch('[selectedEntities]', function(newValue, oldValue) {
        // save growing system entities
        angular.forEach($scope.growingSystems, function(elt) {
            if ($scope.selectedEntities[elt.topiaId]) {
                $scope._selectedEntities[elt.topiaId] = elt;
            } else {
                delete $scope._selectedEntities[elt.topiaId];
            }
        });

        // active property and first selected
        $scope.allSelectedDomainActive = true;
        angular.forEach($scope._selectedEntities, function(domain, domainId) {
            $scope.allSelectedDomainActive &= domain.active;
            $scope.firstSelectedDomain = domain; // make sense when size() == 1
        });

        // active property and first selected
        $scope.allSelectedGrowingSystemActive = true;
        $scope.allSelectedGrowingSystemsValidate = true;
        angular.forEach($scope._selectedEntities, function(growingSystem) {
            $scope.allSelectedGrowingSystemActive &= growingSystem.active;
            $scope.allSelectedGrowingSystemsValidate &= (growingSystem.validated && growingSystem.userCanValidate);
            $scope.firstSelectedGrowingSystem = growingSystem; // make sense when size() == 1
        });

        $scope.filter.selected = !!$scope.filter.selected && $filter('toSelectedLength')($scope.selectedEntities) > 0;
        if ($scope.filter.selected) {
          $scope.refresh(true);
        }
    }, true);

    $scope.$watch('filter.selected', function(newValue, oldValue) {
      if (newValue != oldValue) {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(true);
        }
      }
    }, true);

    $scope.mapSize = function(map) {
      if (!map) {
        return 0;
      }
      var keys = Object.keys(map);
      return keys.length;
    };

    $scope.asyncGrowingSystemsExport = function() {
      var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

      var data = "growingSystemIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
      $http.post(
          ENDPOINTS.growingSystemsExportAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };

}]);

// end of GrowingSystemsListController

AgrosystModule.controller('NetworksListController', ['$scope', '$http', '$timeout', '$filter', 'ListNetworksInitData', 'ContextFilterInit', 'networkFilter',
    function ($scope, $http, $timeout, $filter, ListNetworksInitData, ContextFilterInit, networkFilter) {
      // la liste des reseaux (utilisé par le ng-repeat)
      $scope.networks = ListNetworksInitData.elements;
      // pager correspondant à la liste actuelle des reseaux affiché
      $scope.pager = ListNetworksInitData;
      $scope.contextFilter = ContextFilterInit;
      // map contenant les id des reseaux sélectionnés (peut contenir l'id avec false pour une non selection)
      $scope.selectedEntities = $scope.selectedNetworks || {};
      // map contenant les object des réseaux sélectionné (entierrement)
      $scope._selectedEntities = {};

      $scope.allSelectedEntities = [];

      _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.networks);

      $scope.toggleSelectedEntities = function() {
        _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.networks);
      };

      $scope.toggleSelectedEntity = function(entityId) {
        _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);
      };

      $scope.clearSelection = function() {
        angular.forEach($scope.selectedEntities, (value, key) => delete $scope.selectedEntities[key]);
        $scope._selectedEntities = {};
        $scope.allSelectedEntities = [];
      };

      // L'object représantant le premier reseau sélectionné
      //$scope.firstSelectedNetwork;
      // Vrai si tous les reseaux sélectionné sont actif
      //$scope.allSelectedNetworkActive;
      // le filtre pour filtrer la liste
      $scope.filter = networkFilter;
      //XXX ymartel 2014/02/18 should be convert Boolean to String for binding until resolution of https://github.com/angular/angular.js/issues/6297
      if (angular.isDefined(networkFilter.active)) {
        $scope.filter.active = networkFilter.active.toString();
      }

      $scope.sortColumn = {};

      $scope.changeSort = function(scope) {
        $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

        if($scope.filter == undefined) {
          $scope.filter = {};
        }
        if ($scope.filter.sortedColumn != undefined &&
              $scope.filter.sortedColumn != scope) {
          $scope.sortColumn[$scope.filter.sortedColumn] = false;
        }
        $scope.filter.sortedColumn = scope;
        $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
        $scope.refresh(0,1);
      };

      $scope.getManagers = function (network) {
        var managersNames = "";
        if (network.managers.length !== 0) {
          managersNames = network.managers
            .filter(m => m.active === true)
            .map(m => m.agrosystUser.firstName + " " + m.agrosystUser.lastName)
            .join(", ");
        }
        return managersNames;
      };

      $scope.refresh = function(newValue, oldValue) {
        if (newValue === oldValue) { return; } // prevent init useless call

        // remove empty value without modifying initial model
        var localFilter = angular.copy($scope.filter);

        var filterSelected = localFilter.selected;
        if (filterSelected) {
          localFilter.selectedIds = $filter('toSelectedValue')($scope.selectedEntities);
        }
        delete localFilter.selected;

        if (localFilter.active === "") {
          delete localFilter.active;
        }

        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
        if ($scope.contextFilter.fromNavigationContextChoose) {
            ajaxRequest += "&fromNavigationContextChoose=true";
        }
        displayPageLoading();
        $http.post(ENDPOINTS.networksListJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
        then(function(response) {
            $scope.networks = response.data.elements;
            $scope.pager = response.data;
            _updatePageSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.networks);
        }).
        catch(function(response) {
          console.error("Can't get network list", response);
          var message = "Échec de récupération de la liste des réseaux";
          addPermanentError(message, response.status);
        }).finally(function() {
          hidePageLoading();
        });
      };

      $scope.selectAllEntities = function() {
        // remove empty value without modifying initial model
        var localFilter = angular.copy($scope.filter);
        if (localFilter.active === "") {
          delete localFilter.active;
        }

        displayPageLoading();
        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson(localFilter));
        $http.post($scope.endpoints.networkIdsJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .then(function(response) {
            var selectedEntities = {};
            angular.forEach(response.data, function(topiaId) {
              selectedEntities[topiaId] = true;
            });
            Object.assign($scope.selectedEntities, selectedEntities);
        })
        .catch(function(response) {
          console.error("Can't get network ids", response);
          var message = "Échec de récupération de la liste des ids des réseaux";
          addPermanentError(message, response.status);
        })
        .finally(function() {
          hidePageLoading();
        });
      };

      // watch with timer
      var timer = false;
      $scope.$watch('[filter.networkName, filter.networkManager]',
        function(newValue, oldValue) {
          if (timer) {
              $timeout.cancel(timer);
          }
          timer = $timeout(function(){
            let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
          }, 350);
         }, true);
      // watch without timer
      $scope.$watch('[filter.active]', function(newValue, oldValue) {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      }, true);
      // watch without timer
      $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

      $scope.$watch('selectedEntities', function(newValue, oldValue) {
        // save network entities
        angular.forEach($scope.networks, function(elt) {
            if ($scope.selectedEntities[elt.topiaId]) {
                $scope._selectedEntities[elt.topiaId] = elt;
            } else {
                delete $scope._selectedEntities[elt.topiaId];
            }
        });
        // active property and first selected
        $scope.allSelectedNetworkActive = true;
        angular.forEach($scope._selectedEntities, function(network) {
            $scope.allSelectedNetworkActive &= network.active;
            $scope.firstSelectedNetwork = network; // make sense when size() == 1
        });

        $scope.filter.selected = !!$scope.filter.selected && $filter('toSelectedLength')($scope.selectedEntities) > 0;
        if ($scope.filter.selected) {
          $scope.refresh(true);
        }
    }, true);

    $scope.$watch('filter.selected', function(newValue, oldValue) {
      if (newValue != oldValue) {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(true);
        }
      }
    }, true);
}]);

AgrosystModule.controller('NavigationContextController', ['$scope', '$http', '$filter', 'displayTooManyElementsDialogService', 'ContextFilterInit', 'ListCampaignsInitData',
  function($scope, $http, $filter, displayTooManyElementsDialogService, ContextFilterInit, ListCampaignsInitData) {
    $scope.contextFilter = ContextFilterInit;
    $scope.campaigns = ListCampaignsInitData;
    $scope.selectedNetworks = $scope.contextFilter.selectedNetworks;
    $scope.selectedDomains = $scope.contextFilter.selectedDomains;
    $scope.selectedGrowingPlans = $scope.contextFilter.selectedGrowingPlans;
    $scope.selectedGrowingSystems = $scope.contextFilter.selectedGrowingSystems;

    $scope.clearNavigationContext = function () {
      if ($scope.contextFilter) {
        $scope.contextFilter.selectedCampaigns = {};
        angular.forEach($scope.selectedNetworks, (value, key) => delete $scope.selectedNetworks[key]);
        angular.forEach($scope.selectedDomains, (value, key) => delete $scope.selectedDomains[key]);
        angular.forEach($scope.selectedGrowingPlans, (value, key) => delete $scope.selectedGrowingPlans[key]);
        angular.forEach($scope.selectedGrowingSystems, (value, key) => delete $scope.selectedGrowingSystems[key]);
      }
    };

    $scope.toggleCampaign = function(campaign) {
      $scope.contextFilter.selectedCampaigns[campaign] = !$scope.contextFilter.selectedCampaigns[campaign];
    };

    $scope.toggleCampaigns = function() {
      if ($filter('toSelectedArray')($scope.contextFilter.selectedCampaigns).length == $scope.campaigns.length) {
        $scope.clearSelectedCampaigns();
      } else {
        $scope.selectAllCampaigns();
      }
    };

    $scope.selectAllCampaigns = function() {
      $scope.campaigns.forEach(campaign => $scope.contextFilter.selectedCampaigns[campaign] = true);
    };

    $scope.clearSelectedCampaigns = function() {
      $scope.contextFilter.selectedCampaigns = {};
    };

    $scope.saveNavigationContext = function() {
      /*
       * Quand trop d'éléments sont sélectionnés, le cookie devient trop gros (+ de 4ko), donc les filtres contextuels
       * ne sont pas sauvegardés. Mais ça ne lève pas vraiment d'erreur, il y a juste un warning que l'on peut apercevoir
       * dans la console navigateur. On calcule a priori pour éviter d'envoyer inutilement des données au serveur.
       * D'après les observations, le cookie devient trop grand quand on sélectionne + de 85 éléments.
       */
      const nbElementsSelectionnes =
        Object.keys($scope.contextFilter.selectedCampaigns).filter(k => $scope.contextFilter.selectedCampaigns[k]).length +
        Object.keys($scope.contextFilter.selectedNetworks).filter(k => $scope.contextFilter.selectedNetworks[k]).length +
        Object.keys($scope.contextFilter.selectedDomains).filter(k => $scope.contextFilter.selectedDomains[k]).length +
        Object.keys($scope.contextFilter.selectedGrowingPlans).filter(k => $scope.contextFilter.selectedGrowingPlans[k]).length +
        Object.keys($scope.contextFilter.selectedGrowingSystems).filter(k => $scope.contextFilter.selectedGrowingSystems[k]).length
      ;

      if (nbElementsSelectionnes <= 85) {
        var data = "selectedCampaigns=" + encodeURIComponent(angular.toJson($filter('toSelectedArray')($scope.contextFilter.selectedCampaigns)))
                    + "&selectedNetworks=" + encodeURIComponent(angular.toJson($filter('toSelectedArray')($scope.contextFilter.selectedNetworks)))
                    + "&selectedDomains=" + encodeURIComponent(angular.toJson($filter('toSelectedArray')($scope.contextFilter.selectedDomains)))
                    + "&selectedGrowingPlans=" + encodeURIComponent(angular.toJson($filter('toSelectedArray')($scope.contextFilter.selectedGrowingPlans)))
                    + "&selectedGrowingSystems=" + encodeURIComponent(angular.toJson($filter('toSelectedArray')($scope.contextFilter.selectedGrowingSystems)));

        $http.post(ENDPOINTS.contextChooseRaw, data, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
            .then(function(response) {
              location.reload(); // Il suffit de recharger la page
            })
            .catch(function(response) {
              $("#context-selector").html("Erreur !");
            });
      } else {
        const msg = "Vous ne pouvez pas sélectionner plus de 85 éléments dans les filtres contextuels. Vous avez sélectionné " + nbElementsSelectionnes + " éléments.";
        $scope.displayWarningMessage(msg);
      }
    };

    $scope.onDisplayTooManyElementsOk = function() {
        displayTooManyElementsDialogService.closeDialog();
    };

    $scope.displayWarningMessage = function(message) {
      $scope.message = message;
      displayTooManyElementsDialogService.setOption("title", "Attention");
      return displayTooManyElementsDialogService.openDialog().then(function() {
        delete $scope.message;
      }, function() {
        delete $scope.message;
      });
    };

}]);

AgrosystModule.controller('AttachmentsController', ['$scope', '$window', '$http', 'AttachmentsData', 'fileMaxSize', 'allowedExtensions', 'readOnly', 'i18n',
  function($scope, $window, $http, AttachmentsData, fileMaxSize, allowedExtensions, readOnly, i18n) {
    // current attachments list
    $scope.attachmentMetadatas = AttachmentsData.attachmentMetadatas;
    $scope.readOnly = readOnly === 'true';
    $scope.messages = i18n.messages;
    var updateAttachmentsCount = function() {
      // FIXME echatellier 20131114 : trouver un moyen de le faire plus propre en faisant communiquer les app angular
      angular.element("#attachmentLink").text($scope.attachmentMetadatas.length);
    };

    var fileuploaddone = function(e, data) {
      $scope.attachmentMetadatas = $scope.attachmentMetadatas.concat(data.result);
      updateAttachmentsCount();
    };

    var acceptFileTypes;
    if (!allowedExtensions) {
      acceptFileTypes = ".*";
    } else {
      acceptFileTypes = allowedExtensions.replace(/,/g, "|");
    }
    if (!$scope.readOnly) {
      $scope.options = {
          maxFileSize: fileMaxSize,
          acceptFileTypes: new RegExp("(\.|\/)(" + acceptFileTypes + ")$", "i"),
          autoUpload: true,
          messages: {
            maxNumberOfFiles: 'Nombre maximum de fichiers dépassé !',
            acceptFileTypes: 'Type de fichier non autorisé (' + allowedExtensions + ') !',
            maxFileSize: 'Le fichier est trop gros (max ' + Math.floor(fileMaxSize / 1024000) + 'Mo) !',
            minFileSize: 'Le fichier est trop petit !'
          },
          handleResponse: function (e, data) {
              //code from blueimp.fileupload, but overrides the error message if the request fails
              var files = data.result;
              if (files) {
                  data.scope().replace(data.files, files);

              } else if (data.errorThrown ||
                      data.textStatus === 'error') {
                  console.log(data);
                  data.files[0].error = "Erreur serveur (vérifiez que votre fichier fait moins de " + Math.floor(fileMaxSize / 1024000) + "Mo)";
              }
          },
          done: fileuploaddone
      };
    }

    // cancel pending file upload
    $scope.cancelFileUpload = function(file) {
      file.$cancel();
    };

    // delete file at index
    $scope.deleteAttachment = function(index) {
      if ($window.confirm("Etes vous sûr de vouloir supprimer ce fichier ?")) {
        var file = $scope.attachmentMetadatas[index];
        displayPageLoading();
        $http.post(ENDPOINTS.attachmentsDeleteJson, "attachmentTopiaId=" + encodeURIComponent(file.topiaId),
            {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
          then(function(response) {
            $scope.attachmentMetadatas.splice(index, 1);
            updateAttachmentsCount();
          }).
          catch(function(response) {
            console.error("Échec de suppression des pièces jointes", response.status);
            var message = "Échec de suppression des pièces jointes";
            addPermanentError(message, response.status);
          }).finally(function() {
            hidePageLoading();
          });
      }
    };
}]);

/**
 * Controller de la liste des domaines.
 * Double utilisation possible avec ou sans contexte.
 */
AgrosystModule.controller('InfoMessageListController', ['$scope', '$http', '$timeout', 'ListInfoMessageInitData', 'messageFilter',
  function ($scope, $http, $timeout, ListInfoMessageInitData, messageFilter) {
    $scope.messages = ListInfoMessageInitData.elements;
    $scope.pager = ListInfoMessageInitData;
    $scope.filter = messageFilter;

    //InfoMessageList
    $scope.refresh = function(newValue, oldValue) {
        if (newValue === oldValue) { return; } // prevent init useless call

        // remove empty value without modifying initial model
        //var localFilter = angular.copy($scope.filter);
        var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson($scope.filter));
        displayPageLoading();
        $http.post(ENDPOINTS.infoMessageListJson, ajaxRequest,
                {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
        then(function(response) {
            $scope.messages = response.data.elements;
            $scope.pager = response.data;
        }).
        catch(function(response) {
          console.error("Can't get message list", response);
          var message = "Échec de récupération de la liste des messages d'information";
          addPermanentError(message, response.status);
        }).finally(function() {
          hidePageLoading();
        });
    };

    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

}]);

// recommander un navigateur plus récent que IE9
function checkBrowser() {
  var oldBrowser = false;

  // Mozilla base browser (ex:epiphany)
  if (/Mozilla[\/s](\d+\.\d+)/.test(navigator.userAgent)) {
    var mzVersion = new Number(RegExp.$1); // capture x.x portion and store as a number
    oldBrowser = mzVersion < 4;
  }

  // Firefox
  if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
    var ffVersion = new Number(RegExp.$1); // capture x.x portion and store as a number
    oldBrowser = ffVersion < 21;
  }

  // Internet Explorer
  if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
    var ieVersion = new Number(RegExp.$1); // capture x.x portion and store as a number
    oldBrowser = ieVersion < 10;
  }

  // Opera
  if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
    var oprVersion = new Number(RegExp.$1); // capture x.x portion and store as a number
    oldBrowser = oprVersion < 14;
  }

  // Chrome
  if (/Chrome\/(\d+)\./.test(navigator.userAgent)){
    var chrVersion = new Number(RegExp.$1); // capture x.x portion and store as a number
    oldBrowser = chrVersion < 24;
  }

  // Safari
  if (/Version\/\d+\.\d+.*Safari/.test(navigator.userAgent) && (!/Mozilla[\/s](\d+\.\d+)/.test(navigator.userAgent))){
    var safVersion = new Number(RegExp.$1); // capture x.x portion and store as a number
    oldBrowser = safVersion < 8;
  }

  if (oldBrowser) {
    $('body').prepend("<div class='disclaimer'>Votre navigateur ne supporte pas certaines fonctionnalités essentielles au portail écophyto. Il est recommandé d'utiliser <a href='http://www.browserchoice.eu'>un navigateur plus récent</a></div>");
  }

}

/**
 * Genere un UUID
 * @returns {String}
 */
function generateUUID() {
    var d = Date.now();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
}

function cloneObject(o) {
  return JSON.parse(JSON.stringify(o));
}

function unaccent(text) {
  return text.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
}
