/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
angular.module('ui.switch', [])
.directive('switch', function(){
  return {
    restrict: 'AE',
    replace: true,
    transclude: true,
    template: function(element, attrs) {
      var html = '<div class="';
      if (attrs.struts) {
          html += 'wwgrp '
      }
      if (attrs.class) {
        html += attrs.class;
      }
      html += '">';
      if (attrs.label) {
        html += '<span class="';
        if (attrs.struts) {
            html += 'wwlbl '
        }
        if (attrs.class) {
          html += attrs.class;
        }
        html += '">';
        html += '<label class="label';
        if (attrs.class) {
          html += ' ' + attrs.class;
        }
        html += '">'+ attrs.label + '</label>'
        html += '</span>';
      }
      if (attrs.struts) {
        html += '<span class="wwctrl">';
      }
      if (attrs.help) {
        html += '<span class="tooltip-ng help"><i class="icon float-right fa fa-question-circle" aria-hidden="true"></i><span class="tooltip-hover">' + attrs.help + '</span></span>';
      }
      html += '<span class="switch';
      if (attrs.class) {
        html += ' ' + attrs.class;
      }
      html += '"';
      if (attrs.ngModel) {
        html += ' ng-click="(' + attrs.disabled + ') ? ' + attrs.ngModel + ' : ' + attrs.ngModel + '=!' + attrs.ngModel;
        if (attrs.ngChange) {
          html += '; ' + attrs.ngChange + '()';
        }
        html += '"';
      }
      html +=   ' ng-class="{ checked:' + attrs.ngModel + ', disabled:' + attrs.disabled + ' }"';
      html +=   '>';
      html +=   '<small></small>';
      html +=   '<input type="checkbox"';
      if (attrs.id) {
        html += ' id="' + attrs.id + '"';
      }
      if (attrs.name) {
        html += ' name="' + attrs.name + '"';
      }
      if (attrs.ngModel) {
        html += ' ng-model="' + attrs.ngModel + '"';
      }
      html +=     ' style="display:none" value="true"/>';
      html +=     '<span class="switch-text">'; /*adding new container for switch text*/
      if (attrs.on) {
        html += '<span class="on">' + attrs.on + '</span>'; /*switch text on value set by user in directive html markup*/
      }
      if (attrs.off) {
        html += '<span class="off">' + attrs.off + '</span>'; /*switch text off value set by user in directive html markup*/
      }
      html += '</span>';
      if (attrs.struts) {
        html += '</span>';
      }
      html += '</div>';
      return html;
    }
  }
});
