/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


function decisionRulesExport(exportForm) {
    var idsCount = JSON.parse(exportForm.children("input[name='decisionRuleIds']").val()).length;
    if (idsCount >= 1) {
      exportForm.attr("action", ENDPOINTS.decisionRulesExport);
      exportForm.submit();
    }
}

function managementModesExport(exportForm) {
    var idsCount = Object.values(JSON.parse($(managementModeDtoMap)[0].value)).filter(selected => selected == true).length;
    if (idsCount >= 1) {
      exportForm.attr("action", ENDPOINTS.managementModesExport);
      exportForm.submit();
    }
}

/**
 * Affichage de la liste des mode de gestion.
 */
AgrosystModule.controller('ManagementModesListController', ['$scope', '$http', '$timeout', '$filter', 'ManagementModesInitData', 'managementModeFilter', 'managementModesExportAsyncThreshold',
  function ($scope, $http, $timeout, $filter, ManagementModesInitData, managementModeFilter, managementModesExportAsyncThreshold) {
    $scope.managementModeDtos = ManagementModesInitData.elements;
    $scope.managementModeGrowingSystems = [];
    $scope.pager = ManagementModesInitData;
    $scope.asyncThreshold = managementModesExportAsyncThreshold;
    $scope.filter = managementModeFilter;
    // ManagementModeDto map; key ManagementMode Growing System ,value:ManagementModeDto
    $scope.selectedManagementModes = {};

    $scope.selectedEntities = {};

    $scope._selectedEntities = {};

    $scope.allSelectedEntities = [];

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0, 1);
    };

    $scope._feedUpGrowingSystemList = function() {
      $scope.managementModeGrowingSystems = [];
      angular.forEach($scope.managementModeDtos, function(managementModeDto){
        $scope.managementModeGrowingSystems.push(managementModeDto.growingSystem);
      });
    };

    $scope.toggleSelectedEntities = function() {
      $scope._feedUpGrowingSystemList();
      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.managementModeGrowingSystems);

    };

    $scope.toggleSelectedEntity = function(managementModeDto) {
      var growingSystemId = managementModeDto.growingSystem.topiaId;
      $scope.selectedManagementModes[growingSystemId] = managementModeDto;

      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, growingSystemId);

    };

    $scope.clearSelection = function() {
          $scope.selectedEntities = {};
          $scope._selectedEntities = {};
          $scope.allSelectedEntities = [];
    };

    $scope.managementModeCategories = $scope.i18n.ManagementModeCategory;
    // Object contains attribut for prolongation
    $scope.forManagementModeDuplication = {};
    // list of growing system where can be duplicated the management mode
    //$scope.availableGrowingSystemsForDuplication;
    // growing system where to duplicate management mode
    //$scope.forManagementModeDuplication.growingSystemIdForDuplication;
    // Management mode topiaId for Management Mode to duplicate
    //$scope.forManagementModeDuplication.managementModeId;

    $scope.managementModesDelete = function() {
      var form = $("#managementModesListForm");
      return $http.get(ENDPOINT_MANAGEMENT_MODES_LOAD_WRITABLE_MM_FOR_GS + "?growingSystemIds=" + encodeURIComponent(angular.toJson($scope.selectedEntities)), {cache: false})
        .then(function(response) {
                  $scope.availableManagementModesToDeletes = response.data.elements;
          $scope.deleteSelectionPager = response.data;
          $scope.selectedManagementModesToDelete = [];
          $scope._showDeleteManagementModesLightBox($scope.selectedEntities, form);
        })
        .catch(function(response) {
          console.error("Échec de chargement des modes de gestion disponibles pour les systèmes de cultures sélectionnés", response);
          addPermanentError("Échec de chargement des modes de gestion disponibles pour les systèmes de cultures sélectionnés", response.status);
        });
    };

    $scope.selectManagementModesToDelete = function(managementModeId) {
      var index = $scope.selectedManagementModesToDelete.indexOf(managementModeId);
      if (index === -1) {
        $scope.selectedManagementModesToDelete.push(managementModeId);
      } else {
        $scope.selectedManagementModesToDelete.splice(index, 1);
      }
    };

    $scope._showDeleteManagementModesLightBox = function (selectedEntities, managementModesListForm) {
      $scope.showDeleteManagementModeLB = true;
      var model = {
        process: function() {
          $scope.showDeleteManagementModeLB = false;
          if ($scope.selectedManagementModesToDelete.length >= 1) {
            managementModesListForm.attr("action", ENDPOINTS.managementModesDelete);
            managementModesListForm.submit();
          }
        },
        cancelChanges: function() {
          $scope.showDeleteManagementModeLB = false;
          $scope.selectedManagementModesToDelete = [];
        }
      };
      _displayConfirmDialog($scope, $("#select-managements-modes-light-box"), '95%', model);
    };

    $scope.toManagementModeSelectedLength = function () {
      var count = 0;
      angular.forEach($scope.selectedEntities, function(selectedEntity){
        count += selectedEntity ? 1 : 0;
      });
      return count;
    };

    var _loadAvailableGrowingSystemForManagementModeDuplication = function(growingSystemId) {
      return $http.get(ENDPOINT_AVAILABLE_GROWING_SYSTEM_FOR_MM_DUPLICATION_JSON + "?growingSystemTopiaId=" + encodeURIComponent(growingSystemId), {cache: false})
      .then(function(response) {
        $scope.availableGrowingSystemsForDuplication = response.data;
      })
      .catch(function(response) {
        console.error("Échec de chargement des dispositifs", response);
        addPermanentError("Échec de chargement des dispositifs", response.status);
      });
    };

    var _displayDuplicateManagementModeDialog = function() {
      if ($scope.availableGrowingSystemsForDuplication && $scope.availableGrowingSystemsForDuplication != "null") {
        if ($scope.availableGrowingSystemsForDuplication.length === 1){
          $scope.forManagementModeDuplication.growingSystemIdForDuplication = $scope.availableGrowingSystemsForDuplication[0].topiaId;
        }
        if ($scope.availableGrowingSystemsForDuplication.length > 0) {
          $("#confirmDuplicateManagementMode").dialog({
            resizable: false,
            width: '80%',
            modal: true,
            position: { my: "center top", at: "top+50" },
            buttons: {
              action: {
                click: function() {
                 var managementModesListForm =  $("#managementModesListForm");
                  managementModesListForm.attr("action", ENDPOINTS.managementModesDuplicate);
                  managementModesListForm.submit();
                  $scope.forManagementModeDuplication={};
                  $(this).dialog("close");
                  $scope.$apply();
                },
                text: "Dupliquer",
                'class': 'btn-primary'
              },
              cancel: {
                click: function() {
                  $(this).dialog("close");
                  $scope.$apply();
                  $scope.forManagementModeDuplication={};
                },
                text: 'Annuler',
                'class': 'float-left btn-secondary'
              }
            }
          });
        } else {
          $("#confirmDuplicateManagementMode").dialog({
            resizable: false,
            width: '80%',
            modal: true,
            position: { my: "center top", at: "top+50" },
            buttons: {
              cancel: {
                click: function() {
                  $(this).dialog("close");
                  $scope.$apply();
                  $scope.forManagementModeDuplication={};
                },
                text: 'Annuler',
                'class': 'float-left btn-secondary'
              }
            }
          });
        }
      }
    };

    $scope.showLightBoxDuplicateManagementMode = function() {
      var count = 0;
      var growingSystemId;
      angular.forEach($scope.selectedEntities, function(value, key) {
          if (value) {
              ++count;
              growingSystemId = key;
          }
      });

      if (growingSystemId && count === 1) {
        var managementModeDto = $scope.selectedManagementModes[growingSystemId];
        $scope.forManagementModeDuplication.plannedManagementModeId = managementModeDto.plannedManagementModeId;
        $scope.forManagementModeDuplication.observedManagementModeId = managementModeDto.observedManagementModeId;
        $scope.forManagementModeDuplication.plannedManagementModeSelected = angular.isDefined(managementModeDto.plannedManagementModeId);
        $scope.forManagementModeDuplication.observedManagementModeSelected = angular.isDefined(managementModeDto.observedManagementModeId);

        _loadAvailableGrowingSystemForManagementModeDuplication(growingSystemId)
          .then(_displayDuplicateManagementModeDialog);
      }
    };

    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) { return; } // prevent init useless call

      $http.post(ENDPOINTS.managementModesListJson, "filter=" + encodeURIComponent(angular.toJson($scope.filter)),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
          $scope.managementModeDtos = response.data.elements;
          $scope.pager = response.data;
      }).
      catch(function(response) {
        console.error("Échec de chargement de la liste des modes de gestion", response);
        addPermanentError("Échec de chargement de la liste des modes de gestion", response.status);
      });
    };

    $scope.selectAllEntities = function() {
      displayPageLoading();
      $http.post($scope.endpoints.managementModeIdsJson, "filter=" + encodeURIComponent(angular.toJson($scope.filter)),
              {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
      .then(function(response) {
          var selectedEntities = {};
          angular.forEach(response.data, function(topiaId) {
            selectedEntities[topiaId] = true;
          });
          Object.assign($scope.selectedEntities, selectedEntities);
      })
      .catch(function(response) {
        var message = "Échec de récupération de la liste des ids des modes de gestion";
        console.error(message, response);
        addPermanentError(message, response.status);
      })
      .finally(function() {
        hidePageLoading();
      });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.managementModeName, filter.growingSystemName, filter.domainName, filter.growingPlanName, filter.campaign]',
      function(newValue, oldValue) {
        if (timer) {
          $timeout.cancel(timer);
        }
        timer = $timeout(function(){
          let page = $scope.filter.page;
          $scope.filter.page = 0;
          if (!page) {
            $scope.refresh(newValue, oldValue);
          }
        }, 350);
      }, true);
    // watch without timer
    $scope.$watch('[filter.managementModeCategory]', function(newValue, oldValue) {
      let page = $scope.filter.page;
      $scope.filter.page = 0;
      if (!page) {
        $scope.refresh(newValue, oldValue);
      }
    }, true);
    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.asyncManagementModesExport = function() {
      var data = "managementModeDtoMap=" + encodeURIComponent(angular.toJson($scope.selectedEntities));
      $http.post(
          ENDPOINTS.managementModesExportAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };

}]);

/**
 * Edition d'un mode de gestion.
 */
AgrosystModule.controller('ManagementModesEditController', ['$scope', '$http', '$filter', '$timeout','ManagementModesInitData',
  function ($scope, $http, $filter, $timeout, ManagementModesInitData) {
    // mode de gestion en cours d'edition
    $scope.managementMode = {};
    $scope.managementMode.topiaId = ManagementModesInitData.managementModeTopiaId;
    $scope.managementMode.category = ManagementModesInitData.managementModeCategory;
    $scope.typeDEPHY = ManagementModesInitData.typeDEPHY;
    // Dommage considéré
    $scope.damageTypes = $scope.i18n.DamageType;
    // listes des sections a editer
    $scope.sections = ManagementModesInitData.sections !== null ? ManagementModesInitData.sections : [];

    // identifiant du domain correspondant au système de culture sélectioné
    $scope.domainTopiaId = ManagementModesInitData.domainTopiaId;
    // identifiant du systeme de culture actuellement sélectionné
    $scope.growingSystemTopiaId = ManagementModesInitData.growingSystemTopiaId;
    // liste des cultures associées au système de culture
    $scope.croppingPlanEntries = ManagementModesInitData.croppingPlanEntries;
    // liste des regles de décision du système de culture
    $scope.decisionRules = ManagementModesInitData.decisionRules;
    // Enum des catégories
    $scope.managementModeCategories = $scope.i18n.ManagementModeCategory;
    // Enum des catégories disponibles
    $scope.availableManagementModeCategories = ManagementModesInitData.availableManagementModeCategories;
    // listes des section types
    $scope.sectionTypes = ManagementModesInitData.sectionTypes;
    // map des types de bio agresseurs
    $scope.bioAgressorTypes = $scope.i18n.BioAgressorType;
    // croppingPlanEntries by code
    //$scope.croppingPlanEntriesByCode;
    $scope.displayFieldsErrorsAlert = true;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

    $scope.setCroppingPlanEntriesByCode = function(croppingPlanEntries){
      $scope.croppingPlanEntriesByCode = {};
      if (croppingPlanEntries) {
        angular.forEach(croppingPlanEntries, function(crop){
          $scope.croppingPlanEntriesByCode[crop.code] = crop;
        });
      }
    };

    $scope.setCroppingPlanEntriesByCode($scope.croppingPlanEntries);

    // Update cropping plan entries when growing system change
    $scope._updateCroppingPlanEntries = function(growingSystemTopiaId) {
      if(growingSystemTopiaId){
        $http.post(ENDPOINT_MANAGEMENTMODES_GROWING_SYSTEM_DATA_JSON, "growingSystemTopiaId=" + encodeURIComponent(growingSystemTopiaId),
            {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
        then(function(response) {
          $scope.croppingPlanEntries = response.data.croppingPlanEntries;
          $scope.decisionRules = response.data.decisionRules;
          $scope.availableManagementModeCategories = response.data.managementModeCategories;
          $scope.domainTopiaId = response.data.domainTopiaId;
          $scope.setCroppingPlanEntriesByCode($scope.croppingPlanEntries);
          $scope.typeDEPHY = response.data.typeDEPHY;
          $scope.$emit('cropsChanged');
        })
        .catch(function(response) {
          var message = "Échec de récupération des cultures du système de culture";
          console.error(message, response);
          addPermanentError(message, response.status);
        });
      } else {
       delete $scope.availableManagementModeCategories;
      }
    };

    $scope._removeCropFromStrategies = function(strategies) {
      if (strategies && strategies.length > 0) {
        angular.forEach(strategies, function(strategie){
          strategie.cropIds = [];
        });
      }
    };

    $scope._removeManagementModeRules = function() {
      var sections = $scope.sections;
      if (sections && sections.length > 0) {
        var foundNb = 0;
        angular.forEach(sections, function(section){
          $scope._removeCropFromStrategies(section.strategiesDto);
          $scope._removeCropFromStrategies(section.annualStrategies);
          $scope._removeCropFromStrategies(section.pluriStrategies);
        });
      }
    };

    $scope._growingSystemChange = function(processToDoNext) {
      var sections = $scope.sections;
      if (sections && sections.length > 0) {
        var foundNb = 0;
        angular.forEach(sections, function(section){
          if (section.strategiesDto && section.strategiesDto.length > 0) {
            angular.forEach(section.strategiesDto, function(strategie){
              if (strategie.cropIds && strategie.cropIds.length > 0) {
                foundNb = foundNb + 1;
              }
            });
          }
        });
        if (foundNb > 0) {
          $scope.message = "Attention au moins une culture est utilisée pour les stratégies";

          _displayConfirmDialog($scope, $("#confirm-lightBox"), '50%', processToDoNext);
        } else {
          processToDoNext.process();
        }
      } else {
        processToDoNext.process();
      }
    };

    $scope.$watch('growingSystemTopiaIdToDephy', function(newValue, oldValue) {
      if ($scope.managementMode && !$scope.managementMode.topiaId && newValue !== $scope.newGsValue) {
        var processGrowingSystemChange = {
          process: function() {
            if (newValue) {
              $scope.growingSystemTopiaId = newValue.split(';')[0];
              $scope.typeDEPHY = newValue.split(';')[1];
              $scope.growingSystemSector = newValue.split(';')[2];
              $scope._updateCroppingPlanEntries($scope.growingSystemTopiaId);
              $scope._removeManagementModeRules();

            }
            delete $scope.message;
          },
          cancelChanges: function() {
            $scope.growingSystemTopiaId = oldValue.split(';')[0];
            $scope.typeDEPHY = oldValue.split(';')[1];
            $scope.growingSystemSector = oldValue.split(';')[2];
            delete $scope.message;
          }
        };
        $scope._growingSystemChange(processGrowingSystemChange);
      }
    });

}]);

/**
 * Edition des rubriques d'un mode de gestion.
 */
AgrosystModule.controller('ManagementModesSectionController', ['$scope', '$http', '$window', '$store', '$q', 'ManagementModesInitData',
  function ($scope, $http, $window, $store, $q, ManagementModesInitData) {
    // listes des sections a editer
    //$scope.sections = ManagementModesInitData.sections !== null ? ManagementModesInitData.sections : [];
    // listes des section types
    $scope.sectionTypes = $scope.i18n.SectionType;

    // Constitution de la liste des sectionTypes dans cet ordre particulier : les types ADVENTICES, MALADIES, RAVAGEURS
    // doivent apparaître en premier dans la liste déroulante pour l'utilisateur. Les Object js ne garantissant pas
    // l'ordre, il faut passer par une liste pour constituer l'élément <select>.
    $scope.sectionTypesOrdered = ['ADVENTICES', 'RAVAGEURS', 'MALADIES'];
    for (sectionType of Object.keys($scope.sectionTypes).sort()) {
      if ($scope.sectionTypesOrdered.indexOf(sectionType) == -1) {
        $scope.sectionTypesOrdered.push(sectionType);
      }
    }

    // liste des types de strategies
    $scope.strategyTypes = $scope.i18n.StrategyType;
    // selected section for edition
    //$scope.editedSection;
    // liste des bio agresseur filtrée par le type de la section
    $scope.bioAgressors = [];
    // edited strategy
    //$scope.editedStrategy;
    // Enum des types d'intervention
    $scope.agrosystInterventionTypes = $scope.i18n.AgrosystInterventionType;
    // bioAgressorTypeBySectionType
    $scope.bioAgressorTypeBySectionType = ManagementModesInitData.filteredBioAgressorTypes;
    // Decision Rule for creation
    //$scope.decisionRule;
    // Map of boolean to allow or not to watch decision rules infos.
    $scope.showMoreDecisionRulesInfo = [];
    // collection of available rules
    //$scope.strategyAvailableDecisionRulesMap;
    // section copy for cancel action
    //$scope.originalSection;
    // {object} Copy interventions json object (selected strategies to copy)
    //$scope.selectedStrategies;
    // {boolean} If localstorage contains data that can be copied
    //$scope.pastedStrategies;
    //
    $scope.growingSystemSector = ManagementModesInitData.growingSystemSector;

    $scope.groupesCibles = ManagementModesInitData.groupesCibles;
    $scope.groupesCiblesByCode = {};
    for (var i = 0 ; i < $scope.groupesCibles.length ; i++) {
      var groupeCible = $scope.groupesCibles[i];
      $scope.groupesCiblesByCode[groupeCible.codeGroupeCibleMaa] = groupeCible;
    }

    $scope._buildCroppingPlansEntryByNames = function() {
      $scope.cropNameByIds = {};
      if ($scope.croppingPlanEntries) {
        angular.forEach($scope.croppingPlanEntries, function(item) {
          $scope.cropNameByIds[item.topiaId] = item.name;
        });
      }
    };

    $scope.doCropsChanged = function() {
      delete $scope.cropNameByIds;
    };

    $scope.$parent.$on('cropsChanged', $scope.doCropsChanged);

    // add new section and start edition
    $scope.addSection = function() {
      delete $scope.originalSection;
      $scope.editedSection = {};
      $scope.viewSection($scope.editedSection);
      $scope.managementModesEditForm.$setDirty();
      $scope.originalSection = $scope.editedSection;
    };

    // edit selected section
    $scope.editSection = function(section) {
      $scope.originalSection = section;
      $scope.viewSection(section);
      $scope.editedSection = section;
      $scope.sectionType = $scope.editedSection.sectionType;
    };

    $scope.validateStrategy = function(section, strategy) {
      var result = !!strategy.refStrategyLever;

      if ($scope.typeDEPHY === 'DEPHY_FERME') {
        result &= (strategy.cropIds && strategy.cropIds.length > 0);
      }

      if ($scope.typeDEPHY !== 'DEPHY_FERME') {
        result &= !!strategy.strategyType;
      }

      return result;
    };

    $scope.validateSection = function(section) {

      var result = true;
      if (section) {
        result &= angular.isDefined(section.sectionType) && section.sectionType !== "";
        if ($scope.typeDEPHY === 'DEPHY_FERME' && (section.sectionType === 'ADVENTICES' || section.sectionType === 'MALADIES' || section.sectionType === 'RAVAGEURS')){
          result &= angular.isDefined(section.bioAgressorTopiaId);
        }
        if($scope.typeDEPHY === 'DEPHY_FERME') {
          result &= angular.isDefined(section.categoryObjective);
        }
        if (section.strategiesDto) {
          angular.forEach(section.strategiesDto, function (strategy){
            result &= $scope.validateStrategy(section, strategy);
          });
        }
      }
      return result;
    };

    $scope.viewSection = function(section) {
      $scope.sectionType = section.sectionType;

      if (!section.strategiesDto) {
        section.strategiesDto = [];
      }
      if (section.sectionType == 'ADVENTICES' || section.sectionType == 'MALADIES' || section.sectionType == 'RAVAGEURS') {
        $scope.bioAgressorTypeSelected(section.sectionType);
        if (!section.sdcConcerneBioAgresseur) {
          section.sdcConcerneBioAgresseur = false;
        }
      }
      delete $scope.editedStrategy;

      // reinitialisation de l'objet de selection
      $scope.selectedStrategies = {};
      $scope.pastedStrategies = !!$store.get("management-modes-strategies-" + $scope.domainTopiaId);
    };

    // suppression d'une section
    $scope.removeSection = function() {
      var index = $scope.sections.indexOf($scope.originalSection);

        if (index != -1) {
          if ($window.confirm("Êtes vous sûr de vouloir supprimer cette rubrique ?")) {
            $scope.sections.splice(index, 1);
          }
        }
      delete $scope.editedSection;
      delete $scope.originalSection;
    };

    $scope.sectionTypeChange = function (section, sectionType) {
        // prevent invalid data to be sent to server
        if ($scope.sections.includes(section)) {
          var index = $scope.sections.indexOf(section);
          if (index != -1) {
            $scope.sections.splice(index, 1);
          }
        }
        if (sectionType) {
          $scope.sections.push(section);

          // Pour ces cas, la valeur sdcConcerneBioAgresseur doit être non nulle. Si on trouve null, on met
          // true en valeur par défaut pour être cohérent avec le fonctionnement de l'interface.
          if (['ADVENTICES', 'MALADIES', 'RAVAGEURS'].indexOf(sectionType) > -1) {
            if (!section.sdcConcerneBioAgresseur) {
              section.sdcConcerneBioAgresseur = true;
            }
          }
        }

        if (section.strategiesDto && section.strategiesDto.length > 0) {
          var foundNb = 0;
          angular.forEach(section.strategiesDto, function(strategie){
            if (strategie.refStrategyLever) {
              foundNb = foundNb + 1;
            }
          });
          if (foundNb > 0) {
            $scope.message = "Attention. Un(des) levier(s) associé(s) à la rubrique précédente va(vont) être supprimé(s) des stratégies !";
            var processSectionTypeChange = {
              process: function() {
                $scope.sectionTypeSelected(section, sectionType);
                delete $scope.message;
              },
              cancelChanges: function() {
                $scope.sectionType = section.sectionType;
                delete $scope.message;
              }
            };
            _displayConfirmDialog($scope, $("#confirm-lightBox"), '50%', processSectionTypeChange);
          } else {
            $scope.sectionTypeSelected(section, sectionType);
          }
        } else {
          $scope.sectionTypeSelected(section, sectionType);
        }
    };

    // Update bio agressors when bio agressor type change
    $scope.bioAgressorTypeSelected = function(sectionType) {
      let bioAgressorTypes = $scope.bioAgressorTypeBySectionType[sectionType];
      return $http.get($scope.endpoints.bioAgressorsJson + "?bioAgressorTypes=" + encodeURIComponent(JSON.stringify(bioAgressorTypes)), {cache: true})
        .then(function(response) {
          $scope.bioAgressors = response.data;
          $scope._computeStrategyAvailableDecisionRules($scope.editedSection, $scope.editedStrategy);
        })
        .catch(function(response) {
          console.error("Échec de chargement des bio agresseurs", response);
          addPermanentError("Échec de chargement des bio agresseurs", response.status);
        });
    };

    // new section type selected for section
    $scope.sectionTypeSelected = function(section, newSectionType) {
      if (section) {
        section.sectionType = newSectionType;
        delete section.bioAgressorType;
        delete section.bioAgressorTopiaId;
        delete section.bioAgressorLabel;
        delete $scope.editedStrategy;
        delete $scope.editedSection.codeGroupeCibleMaa;
        delete $scope.editedSection.bioAgressorTopiaId;

        if (section.sectionType == 'ADVENTICES' || section.sectionType == 'MALADIES' || section.sectionType == 'RAVAGEURS') {
          $scope.bioAgressorTypeSelected(section.sectionType);
        }
        $scope._computeStrategyAvailableDecisionRules(section, null);
      }
    };

    // Called when a bio agressor is selected
    $scope.bioAggressorSelected = function(bioAggressorId) {
      angular.forEach($scope.bioAgressors, function(item) {
        if (item.topiaId == bioAggressorId) {
          if (item.adventice) {
            $scope.editedSection.bioAgressorLabel = item.adventice;
          } else {
            $scope.editedSection.bioAgressorLabel = item.reference_label;
          }
        }
      });
    };

    $scope.getCroppingPlanEntryName = function(croppingPlanEntryId) {
      if (!$scope.cropNameByIds) {
        $scope._buildCroppingPlansEntryByNames();
      }
       var name = croppingPlanEntryId ? $scope.cropNameByIds[croppingPlanEntryId] : '-';
      return name;
    };

    $scope.getCroppingPlansEntryNames = function(croppingPlanEntryIds, fullText) {
      var names = "-";
      if (!$scope.cropNameByIds) {
        $scope._buildCroppingPlansEntryByNames();
      }
      if (croppingPlanEntryIds && $scope.cropNameByIds) {
        var cropNames = [];
        var maxIndex = fullText || croppingPlanEntryIds.length < 3 ? croppingPlanEntryIds.length : 2;
        for (var i = 0; i < maxIndex; i ++) {
          var cropId = croppingPlanEntryIds[i];
          cropNames.push($scope.cropNameByIds[cropId]);
        }
        if (maxIndex + 1 < croppingPlanEntryIds.length) {
          cropNames.push("...");
        }
        names = cropNames.join(", ");
      }
      return names;
    };

    $scope.computeMaxSize = function(section) {
      var maxSize = 1;

      section.pluriStrategies = [];
      section.annualStrategies = [];

      angular.forEach(section.strategiesDto, function(item) {
        if (item.implementationType === 'MULTI_ANNUAL_STRATEGY' || item.implementationType === 'LANDSCAPE_LEVEL_MANAGEMENT') {
            section.pluriStrategies.push(item);
        } else {
            section.annualStrategies.push(item);
        }
      });

      if (section.annualStrategies.length > maxSize) {
        maxSize = section.annualStrategies.length;
      }

      section.extraRange = new Array(maxSize - 1);

      section.maxSize = maxSize;
      return section.maxSize;
    };


    $scope.createStrategy = function(section) {
      var strategy = {decisionRuleIds: []};
      $scope.editedSection.strategiesDto.push(strategy);
      $scope.editStrategy(section, strategy);
    };

    // generic add strategy
//    $scope.addStrategy = function() {
//      if($scope.editedSection.strategiesDto.indexOf($scope.editedStrategy) === -1) {
//        $scope.editedSection.strategiesDto.push($scope.editedStrategy);
//      }
//    };

    $scope._computeStrategyAvailableCrops = function(strategy) {
      var strategyCroppingPlanEntries = [];
      if (strategy.cropIds) {
        angular.forEach($scope.croppingPlanEntries, function(crop){
          var index = strategy.cropIds.indexOf(crop.topiaId);
          if (index === -1) {
            strategyCroppingPlanEntries.push(crop);
          }
        });
      } else {
        strategy.cropIds = [];
        strategyCroppingPlanEntries = angular.copy($scope.croppingPlanEntries);
      }
      if (strategyCroppingPlanEntries.length > 1) {
        strategyCroppingPlanEntries.unshift({topiaId: -1, name: '** Toutes cultures **'});
      }

      $scope.strategyCroppingPlanEntries = strategyCroppingPlanEntries;
    };

        $scope.displayLeverUiSelect = function(selected) {
          if (selected) {
            return selected.lever;
          }
        };

    $scope.bindStrategyLeverData = function(selectedStrategyLever) {
      if (selectedStrategyLever) {
        // hack to refresh value and style on the UI
        $('#strategyLever span.select2-chosen').attr("style","");
        $scope.selectedStrategyLever=selectedStrategyLever;
        const content = $scope.getLeverLabel(selectedStrategyLever)
        $('#strategyLever span.select2-chosen')[1].firstChild.textContent=content;
        // end hack
        $scope.refStrategyLeverId = selectedStrategyLever.topiaId;
        $scope.editedStrategy.refStrategyLever = selectedStrategyLever;
        $scope.editedStrategy.strategyType = selectedStrategyLever.strategyType;
      }
    };

    $scope.refreshStrategyLevers = function(strategyLeverPartName) {
      $('#strategyLever .ui-select-dropdown input.ui-select-search').attr('placeholder', 'Entrez le nom d\'un levier s\'il n\'apparait pas dans la liste');

      var query = $scope.growingSystemSector ? ("sector=" + $scope.growingSystemSector) + "&" : "";

      query += "growingSystemTopiaId=" + encodeURIComponent($scope.growingSystemTopiaId);

      if ($scope.editedSection.sectionType) {
        query += "&sectionType=" + $scope.editedSection.sectionType;
      }

      query +="&term=" + encodeURIComponent(strategyLeverPartName);

      return $http.post(
        ENDPOINT_MANAGEMENTMODES_LOAD_REF_STRATEGY_LEVERS_FOR_TERM_JSON, query,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
          then(function(response) {
            $scope.strategyLevers = response.data;
            // auto select the unique value
            if ($scope.strategyLevers.length===1) {
              $scope.bindStrategyLeverData($scope.strategyLevers[0]);
            }
          }).catch(function(response) {
              var message = "Échec de récupération des levier";
              console.error(message, response);
              addPermanentError(message, response.status);
          });
    }

    // edition d'une strategy
    $scope.editStrategy = function(section, strategy) {
      if ($scope.editedStrategy && $scope.editedStrategy === strategy) {
        delete $scope.editedStrategy;
      } else {
        $scope._computeStrategyAvailableCrops(strategy);
        $scope._computeStrategyAvailableDecisionRules(section, strategy);
        $scope.editedStrategy = strategy;
        $scope.selectedStrategyLever = $scope.editedStrategy.refStrategyLever;
        $scope.strategyLevers = [];
        $scope.croppingPlanEntry = null;
      }
    };

    // remove strategy
    $scope.removeStrategy = function(section, index) {
      if ($window.confirm("Êtes vous sûr de vouloir supprimer cette stratégies ?")) {
        if (section.strategiesDto[index] == $scope.editedStrategy) {
          delete $scope.editedStrategy;
        }
        section.strategiesDto.splice(index, 1);
      }
    };

    // retourne les regles disponibles pour la stratégie en cours (même type de bio agresseur,
    // même culture)
    $scope._computeStrategyAvailableDecisionRules = function(section, strategy) {
      if(section) {
        $scope.strategyAvailableDecisionRulesMap = {};
        angular.forEach($scope.decisionRules, function(rule) {
          // strategie pluriannuelle
            if (section.bioAgressorType && rule.bioAgressorType) {
              // un type de bio agresseur défini des deux coté et qui concorde
              if (section.bioAgressorType == rule.bioAgressorType) {
                $scope.strategyAvailableDecisionRulesMap[rule.topiaId] = rule;
              }
            } else if (!section.bioAgressorType && !rule.bioAgressorType){
              // pas de bio agresseur défini des deux coté
              $scope.strategyAvailableDecisionRulesMap[rule.topiaId] = rule;
            }
        });
        // add strategy's decision rule from previous version.
        if (strategy && strategy.decisionRuleIds) {
          var requiredDecisionRules = [];
          angular.forEach(strategy.decisionRuleIds, function(ruleId) {
            if (!$scope.strategyAvailableDecisionRulesMap[ruleId]){
              requiredDecisionRules.push(ruleId);
            }
          });
          if (requiredDecisionRules.length > 0) {
            var requiredDecisionRulesIds = encodeURIComponent(JSON.stringify(requiredDecisionRules));
            return $http.get(ENDPOINT_LOAD_REQUIRED_DECISION_RULES + "?requiredDecisionRulesIds=" + requiredDecisionRulesIds, {cache: false})
              .then(function(response) {
                if (response.data && response.data != "null" && response.data.length>0) {
                  angular.forEach(response.data, function(d){
                    $scope.strategyAvailableDecisionRulesMap[d.topiaId] = d;
                  });
                }
              })
              .catch(function(response) {
                console.error("Échec de chargement des règles de décision", response);
                addPermanentError("Échec de chargement des règles de décision", response.status);
              });
          }
        }
      }
    };

    $scope.addStrategyCrop = function(section, strategy, crop) {
      if (!crop) {
        return;
      }
      // add cropId to the strategy
      if (!strategy.cropIds) {
        strategy.cropIds = [];
      }
      // case of add all crops
      if (crop.topiaId === -1) {
        strategy.cropIds = [];
        angular.forEach($scope.croppingPlanEntries, function(crop){
          strategy.cropIds.push(crop.topiaId);
        });
        $scope.strategyCroppingPlanEntries = [];
      } else {
        strategy.cropIds.push(crop.topiaId);

        // remove available crop from list
        var index = $scope.strategyCroppingPlanEntries.indexOf(crop);
        if (index !== -1) {
          $scope.strategyCroppingPlanEntries.splice(index, 1);
        }
      }
      $scope._computeStrategyAvailableCrops(strategy);
      $scope._computeStrategyAvailableDecisionRules(section, strategy);
    };

    $scope.removeStrategyCrop = function(strategy, cropIndex) {
      strategy.cropIds.splice(cropIndex, 1);
      $scope._computeStrategyAvailableCrops(strategy);
    };

    // return true if a rule is selected by current strategy
    $scope.strategyIsSelectedRule = function(strategy, rule) {
      return $scope.editedStrategy.decisionRuleIds.indexOf(rule.topiaId) >= 0;
    };

    // select a rule when checkbox is clicked
    $scope.strategySelectRule = function(strategy, rule) {
      var index = $scope.editedStrategy.decisionRuleIds.indexOf(rule.topiaId);
      if (index >= 0) {
        $scope.editedStrategy.decisionRuleIds.splice(index, 1);
      } else {
        // A dicision rule has been selected
        // if a decisionRule with same code was selected then it is removed.(toggle between version)
        angular.forEach($scope.editedStrategy.decisionRuleIds, function(ruleId, key){
          var ruleTmp = $scope.strategyAvailableDecisionRulesMap[ruleId];
          if(ruleTmp && ruleTmp.topiaId != rule.topiaId && ruleTmp.code === rule.code){
            //ruleTmp.code;
            index = key;
          }
        });
        if (index != -1) {
          $scope.editedStrategy.decisionRuleIds.splice(index, 1);
        }
        // the decision rule is added
        $scope.editedStrategy.decisionRuleIds.push(rule.topiaId);
      }
      $scope.managementModesEditForm.$setDirty();
    };

    $scope.createNewDecisionRules = function() {

      var query;

      // required attributes
      if ($scope.managementMode.growingSystem) {
        query = "growingSystemTopiaId=" + encodeURIComponent($scope.managementMode.growingSystem.topiaId);
      } else {
        query = "growingSystemTopiaId=" + encodeURIComponent($scope.growingSystemTopiaId);
      }
      query += "&agrosystInterventionType=" + encodeURIComponent($scope.decisionRule.interventionType);
      query += "&decisionRuleName=" + $scope.decisionRule.name;

      // not required attributes
      if ($scope.editedSection) {
        if ($scope.editedSection.bioAgressorType) {
          query += "&bioAgressorType=" + encodeURIComponent($scope.editedSection.bioAgressorType);
        }
        if ($scope.editedSection.bioAgressorTopiaId) {
          query += "&bioAgressorTopiaId=" + encodeURIComponent($scope.editedSection.bioAgressorTopiaId);
        }
      }
      if ($scope.editedStrategy && $scope.editedStrategy.cropIds) {
        query += "&cropIds=" + angular.toJson($scope.editedStrategy.cropIds);
      }

      $http.post(ENDPOINT_CREATE_DECISION_RULE_JSON,query,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
      then(function(response) {
        $scope.strategyAvailableDecisionRulesMap[response.data.topiaId] = response.data;
      }).
      catch(function(response) {
        console.error("Échec de création de la règle de décision", response);
        addPermanentError("Échec de création de la règle de décision", response.status);
      });
    };

    $scope.showLightoxCreateNewDecisionRules = function () {
      $scope.decisionRule = {};
      var createDecisionRuleModelModel = {
        process: function() {
          $scope.createNewDecisionRules();
        },
        cancelChanges: function() {
          delete $scope.decisionRule;
        }
      };
      _displayConfirmDialog($scope, $("#decision-rules-edit-lightbox"), '95%', createDecisionRuleModelModel);
    };

    // copies des strategies sélectionnées dans le presse papier
    $scope.copyStrategies = function() {
      var strategies = [];
      angular.forEach($scope.selectedStrategies, function(value, key) {
        if (value) {
          angular.forEach($scope.editedSection.strategiesDto, function(strategy) {
            if (key == strategy.$$hashKey) {
              var copy = angular.copy(strategy);
              // remove all identifiers
              delete copy.topiaId;
              // save copy
              strategies.push(copy);
            }
          });
        }
      });

      $store.set("management-modes-strategies-" + $scope.domainTopiaId, strategies);
      $scope.selectedStrategies = {};
      $scope.pastedStrategies = true;
    };

    // paste strategies from local storage into current node or phase
    $scope.pasteStrategies = function() {
      var strategies = $store.get("management-modes-strategies-" + $scope.domainTopiaId);
      angular.forEach(strategies, function(item) {
        $scope.editedSection.strategiesDto.push(item);
      });
      $store.remove("management-modes-strategies-" + $scope.domainTopiaId); // copy only once ?
      $scope.pastedStrategies = false;
    };

    $scope.isGroupeCibleInCategory = function(groupeCible) {
      return !$scope.editedSection || !$scope.bioAgressorTypeBySectionType[$scope.editedSection.sectionType] ||
             $scope.bioAgressorTypeBySectionType[$scope.editedSection.sectionType].includes(groupeCible.referenceParam);
    };

    $scope.inGroupeCibleBioAgressor = function(agressor) {
      return !$scope.editedSection|| !$scope.editedSection.codeGroupeCibleMaa ||
             $scope.groupesCiblesByCode[$scope.editedSection.codeGroupeCibleMaa].cibleEdiRefCodes.includes(agressor.reference_code || agressor.identifiant);
    };

    $scope.groupeCibleSelected = function(groupeCible) {
        $scope.editedSection.codeGroupeCibleMaa = groupeCible.codeGroupeCibleMaa;
        delete $scope.editedSection.bioAgressorTopiaId;
    };

    $scope.getLeverLabel = function(strategyLever) {
      if (strategyLever) {
        if (['GRANDES_CULTURES', 'POLYCULTURE_ELEVAGE', 'VITICULTURE', 'ARBORICULTURE'].indexOf($scope.growingSystemSector) > -1) {
          return strategyLever.lever;
        } else {
          return strategyLever.lever + ' (' + $scope.i18n.StrategyType[strategyLever.strategyType] + ')';
        }
      }
    };

    $scope.notDephyExpe = function() {
      return $scope.typeDEPHY !== 'DEPHY_EXPE';
    }

}]);

/**
 * Affichage de la liste des règles de decisions.
 */
AgrosystModule.controller('DecisionRulesListController', ['$scope', '$http', '$timeout', '$filter', 'DecisionRulesInitData', 'decisionRuleFilter', 'decisionRulesExportAsyncThreshold',
  function ($scope, $http, $timeout, $filter, DecisionRulesInitData, decisionRuleFilter, decisionRulesExportAsyncThreshold) {
    $scope.decisionRules = DecisionRulesInitData.elements;
    $scope.pager = DecisionRulesInitData;
    $scope.asyncThreshold = decisionRulesExportAsyncThreshold;

    $scope.sortColumn = {};

    $scope.changeSort = function(scope) {
      $scope.sortColumn[scope]=$scope.sortColumn[scope] === undefined || $scope.sortColumn[scope] === 1 ? 0 : 1;

      if($scope.filter == undefined) {
        $scope.filter = {};
      }
      if ($scope.filter.sortedColumn != undefined &&
            $scope.filter.sortedColumn != scope) {
        $scope.sortColumn[$scope.filter.sortedColumn] = false;
      }
      $scope.filter.sortedColumn = scope;
      $scope.filter.sortOrder = $scope.sortColumn[scope] ? 'DESC' : 'ASC';
      $scope.refresh(0, 1);
    };

    if (decisionRuleFilter) {
        $scope.filter = decisionRuleFilter;
        //XXX ymartel 2014/02/18 should be convert Boolean to String for binding until resolution of https://github.com/angular/angular.js/issues/6297
        if (angular.isDefined(decisionRuleFilter.active)) {
            $scope.filter.active = decisionRuleFilter.active.toString();
        }
    } else {
        $scope.filter = {};
    }

    $scope.selectedEntities = {};

    $scope._selectedEntities = {};

    $scope.allSelectedEntities = [];

    $scope.toggleSelectedEntities = function() {

      _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.decisionRules);

    };

    $scope.toggleSelectedEntity = function(entityId) {

      _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);

    };

    $scope.clearSelection = function() {
          $scope.selectedEntities = {};
          $scope._selectedEntities = {};
          $scope.allSelectedEntities = [];
    };

    //$scope.firstSelectedDecisionRule;
    //$scope.allSelectedDecisionRuleActive;
    $scope.agrosystInterventionTypes = $scope.i18n.AgrosystInterventionType;

    $scope.refresh = function(newValue, oldValue) {
      if (newValue === oldValue) { return; } // prevent init useless call

      // remove empty value without modifying initial model
      var localFilter = angular.copy($scope.filter);
      if (localFilter.active === '') {
        delete localFilter.active;
      }
      return $http.get(ENDPOINTS.decisionRulesListJson + "?filter=" + encodeURIComponent(angular.toJson(localFilter)), {cache: false})
        .then(function(response) {
          $scope.decisionRules = response.data.elements;
          $scope.pager = response.data;
        })
        .catch(function(response) {
          console.error("Échec de chargement de la liste des règles de décision", response);
          addPermanentError("Échec de chargement de la liste des règles de décision", response.status);
        });
    };

    $scope.selectAllEntities = function() {
      var localFilter = angular.copy($scope.filter);
      if (localFilter.active === '') {
        delete localFilter.active;
      }
      displayPageLoading();
      $http.get($scope.endpoints.decisionRuleIdsJson + "?filter=" + encodeURIComponent(angular.toJson(localFilter)), {cache: false})
      .then(function(response) {
          var selectedEntities = {};
          angular.forEach(response.data, function(topiaId) {
            selectedEntities[topiaId] = true;
          });
          Object.assign($scope.selectedEntities, selectedEntities);
      })
      .catch(function(response) {
        console.error("Can't get decision rule ids", response);
        var message = "Échec de récupération de la liste des ids des règles de décision";
        addPermanentError(message, response.status);
      })
      .finally(function() {
        hidePageLoading();
      });
    };

    // watch with timer
    var timer = false;
    $scope.$watch('[filter.decisionRuleName, filter.domainName]', function(newValue, oldValue) {
      if (timer) {
        $timeout.cancel(timer);
      }
      timer = $timeout(function() {
        let page = $scope.filter.page;
        $scope.filter.page = 0;
        if (!page) {
          $scope.refresh(newValue, oldValue);
        }
      }, 350);
    }, true);
    // watch without timer
    $scope.$watch('[filter.active, filter.interventionType]', function(newValue, oldValue) {
      let page = $scope.filter.page;
      $scope.filter.page = 0;
      if (!page) {
        $scope.refresh(newValue, oldValue);
      }
    }, true);
    // watch without timer
    $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

    $scope.$watch('selectedEntities', function(newValue, oldValue) {
      // save growing plan entities
      angular.forEach($scope.decisionRules, function(elt) {
          if ($scope.selectedEntities[elt.topiaId]) {
              $scope._selectedEntities[elt.topiaId] = elt;
          } else {
              delete $scope._selectedEntities[elt.topiaId];
          }
      });
      // active property and first selected
      $scope.allSelectedDecisionRuleActive = true;
      angular.forEach($scope._selectedEntities, function(decisionRule) {
          $scope.allSelectedDecisionRuleActive &= decisionRule.active;
          $scope.firstSelectedDecisionRule = decisionRule; // make sense when size() == 1
      });
    }, true);

    $scope.asyncDecisionRulesExport = function() {
      var selectedForAsyncExport = $filter('toSelectedArray')($scope.selectedEntities);

      var data = "decisionRuleIds=" + encodeURIComponent(angular.toJson(selectedForAsyncExport));
      $http.post(
          ENDPOINTS.decisionRulesExportAsync,
          data,
          {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
        )
        .then(function(response) {
          addSuccessMessage("Votre export a bien été pris en charge, vous le recevrez par e-mail dans quelques instants");
        })
        .catch(function(response) {
          addPermanentError("Une erreur est survenue, veuillez réessayer ultérieurement");
          console.error("Impossible de lancer l'export asynchrone", response);
        });
    };

}])
.directive('autoComplete', function($timeout) {
               return function(scope, iElement, iAttrs) {
                 iElement.bind("keypress", function(e) {
                   scope.showlist = true;
                 })
               };
             })

/**
 * Edition d'une regle de décision.
 */
AgrosystModule.controller('DecisionRulesEditController', ['$scope', '$http', 'DecisionRulesInitData',
  function ($scope, $http, DecisionRulesInitData) {
    // regle de decision en cours d'edition
    $scope.decisionRule = DecisionRulesInitData.decisionRule;

    // identifiant du domaine actuellement sélectionné
    $scope.domainCode = (!DecisionRulesInitData.domainCode || /^\s*$/.test(DecisionRulesInitData.domainCode)) ? null : DecisionRulesInitData.domainCode;
    $scope.domains = DecisionRulesInitData.domains;

    // liste des cultures associées au systeme de culture
    //$scope.croppingPlanEntryCode = DecisionRulesInitData.croppingPlanEntryCode;
    $scope.croppingPlanEntries = angular.isUndefined(DecisionRulesInitData.croppingPlanEntries) ? [] : DecisionRulesInitData.croppingPlanEntries;

    // liste des bio-agresseurs associés au type de bio agresseurs sélectionné
    $scope.bioAgressorTopiaId = DecisionRulesInitData.bioAgressorTopiaId;
    $scope.bioAgressors = DecisionRulesInitData.bioAgressors;
    $scope.bioAgressorTypesByParent = DecisionRulesInitData.bioAgressorTypes;
    $scope.bioAgressorTypes = Object.keys(DecisionRulesInitData.bioAgressorTypes);
    $scope.groupesCibles = DecisionRulesInitData.groupesCibles;
    $scope.groupesCiblesByCode = {};
    for (var i = 0 ; i < $scope.groupesCibles.length ; i++) {
      var groupeCible = $scope.groupesCibles[i];
      $scope.groupesCiblesByCode[groupeCible.codeGroupeCibleMaa] = groupeCible;
    }

    $scope.setCroppingPlanEntriesByCode = function(croppingPlanEntries){
      $scope.croppingPlanEntriesByCode = {};
      if (croppingPlanEntries) {
        angular.forEach(croppingPlanEntries, function(crop){
          $scope.croppingPlanEntriesByCode[crop.code] = crop;
        });
      }
    };

    $scope.setCroppingPlanEntriesByCode($scope.croppingPlanEntries);

    $scope._getDecisionRuleCropByCropCode = function(decisionRule, croppingPlanEntriesByCode) {
      var decisionRuleCropByCropCode = {};
      angular.forEach(decisionRule.decisionRuleCrop, function(decisionRuleCrop){
        var crop = croppingPlanEntriesByCode[decisionRuleCrop.croppingPlanEntryCode];
        if (crop) {
          decisionRuleCropByCropCode[decisionRuleCrop.croppingPlanEntryCode] = decisionRuleCrop;
        }
      });
      return decisionRuleCropByCropCode;
    };

    $scope._getSelectableCropForDecisionRule = function(croppingPlanEntries, decisionRuleCropByCropCode) {
      var decisionRuleCroppingPlanEntries = [];
      angular.forEach(croppingPlanEntries, function(crop){
        var decisionRuleCrop = decisionRuleCropByCropCode[crop.code];
        if (!decisionRuleCrop) {
          decisionRuleCroppingPlanEntries.push(crop);
        }
      });
      return decisionRuleCroppingPlanEntries;
    };

    $scope._computeDecisionRulesAvailableCrops = function(decisionRule) {

      var decisionRuleCroppingPlanEntries = [];
      var croppingPlanEntriesByCode = $scope.croppingPlanEntriesByCode;

      if (decisionRule.decisionRuleCrop && croppingPlanEntriesByCode) {

        var decisionRuleCropByCropCode = $scope._getDecisionRuleCropByCropCode(decisionRule, croppingPlanEntriesByCode);

        decisionRule.decisionRuleCrop = Object.values(decisionRuleCropByCropCode);
        decisionRuleCroppingPlanEntries = $scope._getSelectableCropForDecisionRule($scope.croppingPlanEntries, decisionRuleCropByCropCode);

      } else {

        decisionRule.decisionRuleCrop = [];
        decisionRuleCroppingPlanEntries = angular.copy($scope.croppingPlanEntries);
      }

      $scope.decisionRuleCroppingPlanEntries = decisionRuleCroppingPlanEntries;
    };

    $scope._computeDecisionRulesAvailableCrops($scope.decisionRule);

    $scope.getJson = function(input) {
      input = input || "";
      var result = angular.toJson(input);
      return result;
    };

    $scope.domainSelected = function(domainCode) {
      $scope._updateCroppingPlanEntries(domainCode);
    };

    // Update cropping plan entries when domains change
    $scope._updateCroppingPlanEntries = function(domainCode) {
      return $http.get(ENDPOINT_DECISIONRULES_CROPPING_PLAN_ENTRIES_JSON + "?domainCode=" + encodeURIComponent(domainCode), {cache: false})
        .then(function(response) {
          $scope.croppingPlanEntries = response.data;
          $scope.setCroppingPlanEntriesByCode($scope.croppingPlanEntries);
          $scope._computeDecisionRulesAvailableCrops($scope.decisionRule);
        })
        .catch(function(response) {
          console.error("Échec de chargement des cultures associées à la règle de décision", response);
          addPermanentError("Échec de chargement des cultures associées à la règle de décision", response.status);
        });
    };

    $scope.addDecisionRuleCrop = function(decisionRule, croppingPlanEntry) {
      if (!croppingPlanEntry) {
        return;
      }
      if (!decisionRule.decisionRuleCrop) {
        decisionRule.decisionRuleCrop = [];
      }
      var decisionRuleCrop = {croppingPlanEntryCode:croppingPlanEntry.code};
      decisionRule.decisionRuleCrop.push(decisionRuleCrop);

      var index = $scope.decisionRuleCroppingPlanEntries.indexOf(croppingPlanEntry);
      if (index !== -1) {
        $scope.decisionRuleCroppingPlanEntries.splice(index, 1);
      }
    };

    $scope.removeDecisionRuleCrop = function(decisionRule, index) {
      decisionRule.decisionRuleCrop.splice(index, 1);
      $scope._computeDecisionRulesAvailableCrops(decisionRule);
    };

    // Update bio agressors when bio agressor type change
    $scope.updateBioAgressors = function(bioAgressorType) {
      delete $scope.bioAgressorTopiaId;
      let bioAgressorTypes = $scope.bioAgressorTypesByParent[bioAgressorType] ? $scope.bioAgressorTypesByParent[bioAgressorType].bioAgressorTypes : null;
      if (bioAgressorTypes) {
        return $http.get(ENDPOINT_DECISIONRULES_BIO_AGRESSORS_JSON + "?bioAgressorTypes=" + encodeURIComponent(JSON.stringify(bioAgressorTypes)), {cache: true})
          .then(function(response) {
            $scope.bioAgressors = response.data;
          })
          .catch(function(response) {
            console.error("Échec de mise à jour du bio agresseur", response);
            addPermanentError("Échec de mise à jour du bio agresseur", response.status);
          });
      } else {
        $scope.bioAgressors = [];
      }
    };

    $scope.isGroupeCibleInCategory = function(groupeCible) {
      return !$scope.decisionRule.bioAgressorType || groupeCible.referenceParam === $scope.decisionRule.bioAgressorType;
    };

    $scope.inGroupeCibleBioAgressor = function(agressor) {
      return !$scope.decisionRule.codeGroupeCibleMaa ||
             $scope.groupesCiblesByCode[$scope.decisionRule.codeGroupeCibleMaa].cibleEdiRefCodes.includes(agressor.reference_code || agressor.identifiant);
    };

    CKEDITOR.replace('ckeditorSolution');
    CKEDITOR.instances.ckeditorSolution.on("instanceReady", function(){
      this.document.on("keyup", function(){
        $scope.decisionRulesEditForm.$setDirty();
      });
    });
}]);


/**
 * Unactivate growing plans after user confirm.
 */
function decisionRulesUnactivate(confirmDialog, unactivateForm, activate) {
    var selectionCount = JSON.parse(unactivateForm.children("input[name='decisionRuleIds']").val()).length;
    if (selectionCount > 0) {
        confirmDialog.dialog({
            resizable: false,
            width: 350,
            modal: true,
            buttons: {
                action: {
                  click:function() {
                    $(this).dialog("close");
                    unactivateForm.attr("action", activate ? ENDPOINTS.decisionRulesUnactivate+"?activate=true" : ENDPOINTS.decisionRulesUnactivate);
                    unactivateForm.submit();
                  },
                  text: activate ? "Activer" : "Désactiver",
                  'class': 'btn-primary'
                },
                cancel: {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}

/**
 * Duplicate decision rule after user confirm.
 */
function decisionRulesDuplicate(confirmDialog, duplicateForm) {
    var selectionCount = JSON.parse(duplicateForm.children("input[name='decisionRuleIds']").val()).length;
    if (selectionCount == 1) {
        confirmDialog.dialog({
            resizable: false,
            width: 350,
            modal: true,
            buttons: {
                "Dupliquer": {
                  click : function() {
                    $(this).dialog("close");
                    duplicateForm.attr("action", ENDPOINTS.decisionRulesDuplicate);
                    duplicateForm.submit();
                  },
                  text: 'Dupliquer',
                  'class': 'btn-primary'
                },
                "Annuler": {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
            }
        });
    }
}
