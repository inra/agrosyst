/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
AgrosystModule.controller('ItkPracticedCopyPasteController', ['$scope', '$controller', '$http', '$store', 'ItkInitData',
  function($scope, $controller, $http, $store, ItkInitData) {

    // inherit from common stuff
    $controller('ItkCommonCopyPasteController', {$scope: $scope}); // inheritance

    // todo should be integrated to $store
    window.addEventListener('storage', function (evt){
      if (evt.key === $scope.prefix + "-interventions-" + $scope.domainId) {
        $scope.pastedInterventions = evt.newValue && evt.newValue !== "";
        $scope.$apply();
      }
    });

    // copy selected intervention to local storage
    $scope.copyPracticedInterventions = function() {
      var interventions = [];
      angular.forEach($scope.selectedInterventions, function(value, key) {
        if (value) {
          angular.forEach($scope.selectedPhaseConnectionOrNode.interventions, function(intervention) {
            if (key == intervention.topiaId) {
              // conserve original targeted crop code to be able to compare to pasted one
              var copy = $scope._toNewIntervention(intervention);
              interventions.push(copy);
            }
          });
        }
      });

      $store.set($scope.prefix + "-interventions-" + $scope.domainId , interventions);
      $scope.setSelectedIntervention(null);
      $scope.pastedInterventions = true;
    };

    // paste interventions from local storage into current node or phase
    $scope.pastePracticedInterventions = function() {

      var interventions = $store.get($scope.prefix + "-interventions-" + $scope.domainId);

      if (interventions) {
        $scope._loadCopyPasteGlobalInfo(interventions)
        .then(function() {
          var isFullyMigrated = true;
          angular.forEach(interventions, function(copiedIntervention) {

            var toolsCouplingModels = $scope.agrosystInterventionTypesToolsCouplingDtos[copiedIntervention.type];
            // valid intermediate intervention status
            if (copiedIntervention.intermediateCrop && !$scope.selectedPhaseConnectionOrNode.intermediateCroppingPlanEntryCode) {
              copiedIntervention.intermediateCrop = false;
            }
            copiedIntervention.targetedCroppingPlanEntriesCode = copiedIntervention.intermediateCrop ? $scope.selectedPhaseConnectionOrNode.intermediateCroppingPlanEntryCode : $scope.phaseOrNodeCrop.croppingPlanEntryCode;

            var isInterventionFullyMigrated = $scope._migrateInterventionToSelectedCrop(
              copiedIntervention,
              toolsCouplingModels,
              $scope.practicedDomainInputStockUnitByCodes);

            if (!isInterventionFullyMigrated) {
              isFullyMigrated = false;
            }

          });
          $store.remove($scope.prefix + "-interventions-"+ $scope.domainId); // copy only once ?
          $scope.pastedInterventions = false;

          if (!isFullyMigrated) {
            var warningMessage = "</br><b>" + $scope.messages.copyPasteIncompleteMigration + "</b>";
            addPermanentWarning(warningMessage);
          }

          $scope.practicedSystemForm.$setDirty();
        });
      }

    };

    $scope._replaceUsageDomainInputs = function (actionDtos, practicedDomainInputStockUnitByCodes) {
      let new_actionDtos = [];
      actionDtos.map(a => {
        let isIrrigAction = a.irrigationInputUsageDto !== undefined;
        // mineral
        if(a.mineralProductInputUsageDtos) {
          let new_mineralProductInputUsageDtos = [];
          a.mineralProductInputUsageDtos.map(
            iu => {
              let domainInput = practicedDomainInputStockUnitByCodes[iu.domainMineralProductInputDto.code];
              if (domainInput !== undefined || domainInput !== null) {
                iu.domainMineralProductInputDto = domainInput;
                new_mineralProductInputUsageDtos.push(iu);
              }
            }
          );
          a.mineralProductInputUsageDtos = new_mineralProductInputUsageDtos;
        }
        // irrigationInputUsageDto
        if(a.irrigationInputUsageDto) {
          let domainInput = practicedDomainInputStockUnitByCodes[a.irrigationInputUsageDto.code];
          if (domainInput !== undefined || domainInput !== null) {
            a.irrigationInputUsageDto = domainInput;
          } else {
            a.irrigationInputUsageDto = undefined;
          }
        }
        // other
        if (a.otherProductInputUsageDtos) {
          let new_otherProductInputUsageDtos = [];
          a.otherProductInputUsageDtos.map(
            iu => {
              let domainInput = practicedDomainInputStockUnitByCodes[iu.domainOtherProductInputDto.code];
              if (domainInput !== undefined || domainInput !== null) {
                iu.domainOtherProductInputDto = domainInput;
                new_otherProductInputUsageDtos.push(iu);
              }
            }
          )
          a.otherProductInputUsageDtos = new_otherProductInputUsageDtos;
        }
        // organicProductInputUsageDtos
        if (a.organicProductInputUsageDtos) {
          let new_organicProductInputUsageDtos = [];
          a.organicProductInputUsageDtos.map(
            iu => {
              let domainInput = practicedDomainInputStockUnitByCodes[iu.domainOrganicProductInputDto.code];
              if (domainInput !== undefined && domainInput !== null) {
                iu.domainOrganicProductInputDto = domainInput;
                new_organicProductInputUsageDtos.push(iu);
              }
            }
          )
          a.organicProductInputUsageDtos = new_organicProductInputUsageDtos;
        }
        // phytoProductInputUsageDtos
        if (a.phytoProductInputUsageDtos) {
          let new_phytoProductInputUsageDtos = [];
          a.phytoProductInputUsageDtos.map(
            iu => {
              let domainInput = practicedDomainInputStockUnitByCodes[iu.domainPhytoProductInputDto.code];
              if (domainInput !== undefined && domainInput !== null) {
                iu.domainPhytoProductInputDto = domainInput;
                new_phytoProductInputUsageDtos.push(iu);
              }
            }
          )
          a.phytoProductInputUsageDtos = new_phytoProductInputUsageDtos;
        }
        // PotInputUsageDto
        if (a.potInputUsageDtos) {
          let new_potInputUsageDtos = [];
          a.potInputUsageDtos.map(
            iu => {
              let domainInput = practicedDomainInputStockUnitByCodes[iu.domainPotInputDto.code];
              if (domainInput !== undefined && domainInput !== null) {
                iu.domainPotInputDto = domainInput;
                new_potInputUsageDtos.push(iu);
              }
            }
          )
          a.potInputUsageDtos = new_potInputUsageDtos;
        }
        // substrateInputUsageDtos
        if (a.substrateInputUsageDtos) {
          let new_substrateInputUsageDtos = [];
          a.substrateInputUsageDtos.map(
            iu => {
              let domainInput = practicedDomainInputStockUnitByCodes[iu.domainSubstrateInputDto.code];
              if (domainInput !== undefined && domainInput !== null) {
                iu.domainSubstrateInputDto = domainInput;
                new_substrateInputUsageDtos.push(iu);
              }
            }
          )
          a.substrateInputUsageDtos = new_substrateInputUsageDtos;
        }
        // seedLotInputUsageDtos
        if (a.seedLotInputUsageDtos) {
          let new_seedLotInputUsageDtos = [];
          a.seedLotInputUsageDtos.map(
            iu => {
              // build map domainSpeciesSeedByCode and domainPhytoProductInputDtoByCode as there are not into practicedDomainInputStockUnitByCodes
              let domainSpeciesSeedByCode = {};
              let domainPhytoProductInputDtoByCode = {};

              let domainInput = practicedDomainInputStockUnitByCodes[iu.domainSeedLotInputDto.code];
              if (domainInput !== undefined && domainInput !== null) {
                iu.domainSeedLotInputDto = domainInput;
                new_seedLotInputUsageDtos.push(iu);
                if (domainInput.speciesInputs) {
                  domainInput.speciesInputs.map(
                    di => {
                      domainSpeciesSeedByCode[di.code] = di;
                      if (di.speciesPhytoInputDtos) {
                        di.speciesPhytoInputDtos.map(
                          di => {
                            domainPhytoProductInputDtoByCode[di.code] = di;
                          }
                        )
                      }
                    }
                  )
                }
              }

              if (iu.seedingSpeciesDtos) {
                let new_seedingSpeciesDtos = [];
                iu.seedingSpeciesDtos.map(
                  ssp => {
                    let domainInput = domainSpeciesSeedByCode[ssp.domainSeedSpeciesInputDto.code];
                    if (domainInput !== undefined && domainInput !== null) {
                      ssp.domainSeedSpeciesInputDto = domainInput;
                      new_seedingSpeciesDtos.push(ssp);
                    }

                    if(ssp.seedProductInputDtos) {
                      let new_seedProductInputDtos = [];
                      ssp.seedProductInputDtos.map(
                        iu => {
                          let domainInput = domainPhytoProductInputDtoByCode[iu.domainPhytoProductInputDto.code];
                          if (domainInput !== undefined && domainInput !== null) {
                            iu.domainPhytoProductInputDto = domainInput;
                            new_seedProductInputDtos.push(iu);
                          }
                        }
                      )
                      ssp.seedProductInputDtos = new_seedProductInputDtos;
                    }
                  }
                )
                iu.seedingSpeciesDtos = new_seedingSpeciesDtos;
              }
            }
          )
          a.seedLotInputUsageDtos = new_seedLotInputUsageDtos;
        }
      });
    };

    // practiced
    $scope._migrateInterventionToSelectedCrop = function(copiedIntervention, toolsCouplings, practicedDomainInputStockUnitByCodes) {

      var allActionAndInputsMigrated = true;
      var toolsCouplingMigrated = true;

      var toolsCouplingCodeToToolsCoupling = {};
      if (copiedIntervention.type) {

        // create map: toolsCouplingCode -> toolsCoupling
        angular.forEach(toolsCouplings, function(toolsCoupling) {
          toolsCouplingCodeToToolsCoupling[toolsCoupling.code] = toolsCoupling;
        });

        copiedIntervention.toolsCouplingCodes = $scope._getValidToolsCouplingsCodes(toolsCouplingCodeToToolsCoupling, copiedIntervention.toolsCouplingCodes);

        var targetedSpecies = copiedIntervention.intermediateCrop ? $scope.connectionIntermediateSpecies : $scope.phaseConnectionOrNodeSpecies;

        var fromSpecies = copiedIntervention.previousCroppingPlanEntrySpecies;
        var fromCropCode = copiedIntervention.fromCropCode;
        var toCropCode = copiedIntervention.targetedCroppingPlanEntriesCode;
        var matchingSpeciesBySpeciesCode = _findMatchingSpecies(fromSpecies, targetedSpecies);

        // Create intervention speciesStade for the copied intervention
        // push the existed ones if same species targeted otherwise create new one.
        var speciesStadeMigrationStatus = $scope._tryToMigrateSpeciesStades(
          copiedIntervention.speciesStadesDtos,
          targetedSpecies,
          matchingSpeciesBySpeciesCode,
          copiedIntervention.intermediateCrop);

        copiedIntervention.speciesStadesDtos = speciesStadeMigrationStatus.speciesStades;

        // valid Actions and Inputs according to the given species
        $scope._replaceUsageDomainInputs(copiedIntervention.actionDtos, practicedDomainInputStockUnitByCodes);

        allActionAndInputsMigrated = $scope._migrateActionsAndInputs(copiedIntervention, matchingSpeciesBySpeciesCode, speciesStadeMigrationStatus.status, toCropCode, targetedSpecies);

        $scope._migratePhytoInputDoseIfPossible(copiedIntervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);

        toolsCouplingMigrated = $scope._updateActionsToolsCouplingCode(toolsCouplingCodeToToolsCoupling, copiedIntervention.actions);

        $scope.selectedPhaseConnectionOrNode.interventions.push(copiedIntervention);

      }

      return allActionAndInputsMigrated && toolsCouplingMigrated;
    };

    $scope._getValidToolsCouplingsCodes = function(toolsCouplingCodeToToolsCoupling, toolsCouplings) {
      var toolsCouplingCodes = [];
      // remove not valide tools coupling
      angular.forEach(toolsCouplings, function(code){
        var isValid = toolsCouplingCodeToToolsCoupling[code];
        if (isValid) {
          toolsCouplingCodes.push(code);
        }
      });
      return toolsCouplingCodes;
    };

    // remove unavailable tools codes from actions
    $scope._updateActionsToolsCouplingCode = function(toolsCouplingCodeToToolsCoupling, actions){
      var toolsCouplingMigrated = true;
      if (actions) {
        angular.forEach(actions, function(action){
          if (action.toolsCouplingCode) {
            var isValid = toolsCouplingCodeToToolsCoupling[action.toolsCouplingCode];
            if (!isValid) {
              delete action.toolsCouplingCode;
              toolsCouplingMigrated = false;
            }
          }
        });
      }
      return toolsCouplingMigrated;
    };
  }
]);
