/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
AgrosystModule.controller('ItkEffectiveCopyPasteController', ['$scope', '$controller', '$http', 'ItkInitData', 'CopyPasteItkDialogService',
  function ($scope, $controller, $http, ItkInitData, CopyPasteItkDialogService) {

    // inherit from common stuff
    $controller('ItkCommonCopyPasteController', { $scope: $scope });

    $scope._initCopyPasteInterventions = function () {
      $scope.copyPasteInterventions = {};
      $scope.copyPasteInterventions.ready = false; // TODO DCossé 04/11/15 valider
      $scope.copyPasteInterventions.targetedZoneCampaigns = [];
      $scope.copyPasteInterventions.targetedZoneCampaigns.push(ItkInitData.currentCampaign);
      $scope.copyPasteInterventions.selectedPhasesOrNodes = {};
      $scope.copyPasteInterventions.availableZonesForCampaign = [];
      $scope.copyPasteInterventions.phases = {};
      $scope.copyPasteInterventions.nodes = {};
      $scope.copyPasteInterventions.targetedZones = {};
    };

    $scope.copyEffectiveInterventions = function () {
      if ($scope._getSelectedIntervention().length > 0) {
        $scope._initCopyPasteInterventions();

        var query = "?zoneId=" + encodeURIComponent($scope.zoneTopiaId);
        displayPageLoading();
        $http.get(ENDPOINTS.loadEffectiveZonesForCopyJson + query)
          .then(function (response) {
            hidePageLoading();
            $scope._feedUpCopyPasteInterventionsData(response.data);
          })
          .catch(function (response) {
            hidePageLoading();
            console.error("Can't get effective zones for copy", response);
            var message = "Échec de récupération des zones sur laquelle la copie est possible";
            addPermanentError(message, response.status);
          });

        // blocking call
        $scope.displayCopyInterventionDialog()
          .then($scope.setSelectedIntervention);
      }
    };

    $scope._feedUpCopyPasteInterventionsData = function (data) {
      $scope.copyPasteInterventions.zonesToCopyToByCampaigns = data;
      // create map of targetId -> CopyPasteZoneDto
      $scope.copyPasteInterventions.campaigns = [];
      angular.forEach($scope.copyPasteInterventions.zonesToCopyToByCampaigns.zonesByCampaigns, function (value, key) {
        $scope.copyPasteInterventions.campaigns.push(key);
        angular.forEach(value, function (zone) {
          if (zone.phaseId) {
            $scope.copyPasteInterventions.phases[zone.phaseId] = zone;
          } else if (zone.nodeId) {
            $scope.copyPasteInterventions.nodes[zone.nodeId] = zone;
          }
        });
      });

      $scope.computeAvailableZonesForCampaign();

      $scope.copyPasteInterventions.ready = true;
    };

    $scope.computeAvailableZonesForCampaign = function () {
      $scope.copyPasteInterventions.selectedPhasesOrNodes = {};
      $scope.copyPasteInterventions.availableZonesForCampaign = [];
      if ($scope.copyPasteInterventions.targetedZoneCampaigns && $scope.copyPasteInterventions.zonesToCopyToByCampaigns.zonesByCampaigns) {
        angular.forEach($scope.copyPasteInterventions.targetedZoneCampaigns, function (campaign) {
          var availableZonesForCampaign = $scope.copyPasteInterventions.zonesToCopyToByCampaigns.zonesByCampaigns[campaign];
          angular.forEach(availableZonesForCampaign, function (zone) {
            $scope.copyPasteInterventions.availableZonesForCampaign.push(zone);
          });
        });
      }
    };

    $scope.displayCopyInterventionDialog = function () {
      return CopyPasteItkDialogService.openDialog()
        .then($scope._pasteEffectiveInterventions);
    };
    $scope.onCopyPasteItkDialogOk = CopyPasteItkDialogService.closeDialog.bind(CopyPasteItkDialogService);
    $scope.onCopyPasteItkDialogCancel = CopyPasteItkDialogService.cancelDialog.bind(CopyPasteItkDialogService);

    $scope._pasteEffectiveInterventions = function () {
      var toSameZones = {};
      toSameZones.phases = [];
      toSameZones.nodes = [];

      angular.forEach($scope.copyPasteInterventions.selectedPhasesOrNodes, function (value, key) {
        var copyPasteZoneDto;

        if (value) {
          // target is phase
          var zone;
          copyPasteZoneDto = $scope.copyPasteInterventions.phases[key];
          if (copyPasteZoneDto) {
            if (copyPasteZoneDto.zoneId === $scope.zoneTopiaId) {
              // same node
              toSameZones.phases.push(key);
            } else {
              // other node
              zone = $scope.copyPasteInterventions.targetedZones[copyPasteZoneDto.zoneId];
              if (!zone) {
                zone = $scope._createNewTargetedZone(copyPasteZoneDto.zoneId, copyPasteZoneDto.croppingPlanEntryId);
              }
              zone.phases.push(key);
            }
          } else {
            // target is node
            copyPasteZoneDto = $scope.copyPasteInterventions.nodes[key];
            if (copyPasteZoneDto) {
              if (copyPasteZoneDto.zoneId === $scope.zoneTopiaId) {
                // same node
                toSameZones.nodes.push(key);
              } else {
                // other node
                zone = $scope.copyPasteInterventions.targetedZones[copyPasteZoneDto.zoneId];
                if (!zone) {
                  zone = $scope._createNewTargetedZone(copyPasteZoneDto.zoneId, copyPasteZoneDto.croppingPlanEntryId);
                }
                zone.nodes.push(key);
              }
            }
          }
        }
      });
      // push interventions to the targeted zones
      $scope._sameZoneCopy(toSameZones);
      $scope._otherZonesCopy($scope.copyPasteInterventions.targetedZones);
    };

    $scope._createNewTargetedZone = function (zoneId, croppingPlanEntryId) {
      var zone = {};
      zone.zoneId = zoneId;
      zone.phases = [];
      zone.nodes = [];
      zone.croppingPlanEntryId = croppingPlanEntryId;
      $scope.copyPasteInterventions.targetedZones[zoneId] = zone;
      return zone;
    };

    $scope._sameZoneCopy = function (toSameZones) {
      // copy to same zone
      if (toSameZones.phases.length > 0 || toSameZones.nodes.length > 0) {

        $scope._loadCopyPasteGlobalInfo($scope.copiedInterventions)
          .then(function () {
            var allMigrated = true;
            var displayMessage = false;
            var interventionNames = "";
            angular.forEach($scope.copiedInterventions, function (intervention) {

              var fullInterventionMigrated = $scope._pushToSameZonesInterventions(toSameZones, intervention);
              interventionNames += intervention.name + " - ";
              displayMessage = true;
              if (!fullInterventionMigrated) {
                allMigrated = false;
              }

            });

            if (displayMessage) {

              interventionNames = interventionNames.substring(0, interventionNames.length - 3);
              var pluralize = $scope.copiedInterventions.length > 1 ? "s" : "";
              var successMessage = "Intervention" + pluralize + " copiée" + pluralize + " sur la même zone:" + interventionNames;
              if (allMigrated) {
                addSuccessMessage(successMessage);
              } else {
                var warningMessage = " ATTENTION : les espèces vers lesquelles vous avez collé les interventions diffèrent de celles sur lesquelles portaient initialement ces interventions. Des données ne pourront pas être collées intégralement (doses intrants phytosanitaires/de lutte biologique, actions & intrants de semis, rendements déclarés, combinaisons d’outils). Pensez à aller vérifier/compléter.";
                addPermanentWarning(warningMessage);
              }
            }
          });
      }

    };

    $scope._otherZonesCopy = function (toOtherZones) {
      // transform map to array
      var targetedZones = toOtherZones ? Object.keys(toOtherZones).map(function (key) { return toOtherZones[key]; }) : [];

      // copy to other zones
      if (targetedZones.length > 0) {
        // add from crop code to intervetions
        var currentCropIds = $scope._getCurrentCropCodeIdentifiants();
        angular.forEach($scope.copiedInterventions, function (intervention) {
          intervention.fromCropCode = intervention.intermediateCrop ? currentCropIds.intermediateCropCode : currentCropIds.code;
        });

        displayPageLoading();
        var ajaxRequest = "targetedZones=" + angular.toJson(targetedZones);
        ajaxRequest += "&interventionDtos=" + encodeURIComponent(angular.toJson($scope.copiedInterventions));
        $http.post(ENDPOINTS.effectiveInterventionsCopy, ajaxRequest,
          { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).
          then(function (result) {
            var fullyMigrated = result.data; // true all actions and inputs migrated

            var interventionNames = "";
            angular.forEach($scope.copiedInterventions, function (intervention) {
              interventionNames += intervention.name + " - ";
            });
            interventionNames = interventionNames.substring(0, interventionNames.length - 3);
            var pluralize = interventionNames.length > 1 ? "s" : "";

            var successMessage = "Interventions copiée" + pluralize + " sur d'autres zones:" + interventionNames;
            if (fullyMigrated) {
              addSuccessMessage(successMessage);
            } else {
              var warningMessage = successMessage += "</br><b>ATTENTION : les espèces vers lesquelles vous avez collé les interventions diffèrent de celles sur lesquelles portaient initialement ces interventions. Des données ne pourront pas être collées intégralement (doses intrants phytosanitaires/de lutte biologique, actions & intrants de semis, rendements déclarés, combinaisons d’outils). Pensez à aller vérifier/compléter.</b>";
              addPermanentWarning(warningMessage);
            }
          }).
          catch(function (response) {
            console.error("Echec de la copies des interventions du réalisé", response);
            var message = "Echec de la copies des interventions du réalisé pour les zones sélectionnées";
            addPermanentError(message, response.status);
          })
          .finally(hidePageLoading);
      }
    };

    var _getCroppingPlanSpecies = function (croppingPlanEntries) {
      var phaseSpecies = [];
      angular.forEach(croppingPlanEntries.croppingPlanSpecies, function (species) {
        phaseSpecies.push(species);
      });
      return phaseSpecies;
    };

    // effective
    $scope._pushToTargetedPhasesInterventions = function (targetedPhaseIds, intervention) {

      var allActionsAndInputsMigrated = true;

      var perennialCropCycles = $scope.perennialCropCycles;

      if (perennialCropCycles) {

        var fromSpecies = $scope.phaseConnectionOrNodeSpecies;
        var fromCropIdentifier = $scope._getCurrentCropCodeIdentifiants();

        angular.forEach(targetedPhaseIds, function (targetedPhaseId) {

          var found = false;
          var cycleIndex = 0;
          while (cycleIndex < perennialCropCycles.length && !found) {

            var cycle = perennialCropCycles[cycleIndex++];
            if (cycle.phaseDtos) {

              for (var phaseId = 0; phaseId < cycle.phaseDtos.length; phaseId++) {

                var targetedPhase = cycle.phaseDtos[phaseId];
                if (targetedPhase.topiaId === targetedPhaseId) {
                  // conserve original targeted crop code to be able to compare to pasted one
                  var interventionToCopy = $scope._toNewIntervention(intervention);

                  // if there are some seeding Action, we validate there treatments and inputs
                  interventionToCopy.intermediateCrop = false;
                  var targetedCrop = $scope.croppingPlanEntriesIndex[targetedPhase.croppingPlanEntryId];

                  if (targetedCrop) {

                    var fromCropCode = interventionToCopy.intermediateCrop ? fromCropIdentifier.intermediateCropCode : fromCropIdentifier.code;
                    var toCropCode = targetedCrop.code;
                    var toCropMainSpecies = _getCroppingPlanSpecies(targetedCrop);

                    var speciesStadesMigrationResult = $scope._getInterventionSpeciesStadesMigrationResult(
                      interventionToCopy, fromCropCode, toCropCode, fromSpecies, toCropMainSpecies);

                    interventionToCopy.speciesStadesDtos = speciesStadesMigrationResult.speciesStades;

                    var toCropIntermediateSpecies = [];// aucune pour les phases

                    var toSpecies = $scope._computeSpeciesFromSpeciesStades(interventionToCopy, toCropIntermediateSpecies, toCropMainSpecies);

                    var matchingSpeciesBySpeciesCode = _findMatchingSpecies(fromSpecies, toSpecies);

                    allActionsAndInputsMigrated = $scope._migrateActionsAndInputsSpecies(
                      interventionToCopy, toCropCode, speciesStadesMigrationResult.status,
                      matchingSpeciesBySpeciesCode, toSpecies);

                    targetedPhase.interventions.push(interventionToCopy);
                    found = true;
                    break;
                  } else {
                    allActionsAndInputsMigrated = false;
                  }
                }
              }
            }
          }

        });
      }

      return allActionsAndInputsMigrated;
    };

    // effective
    $scope._pushToTargetedNodesInterventions = function (targetedNodeIds, intervention) {

      var allActionsAndInputsMigrated = true;

      var cycles = $scope.seasonalCropCycles;
      if (cycles) {

        var fromMainSpecies = $scope.phaseConnectionOrNodeSpecies;
        var fromIntermediateSpecies = $scope.connectionIntermediateSpecies;

        angular.forEach(targetedNodeIds, function (targetedNodeId) {

          targetedNodeId = targetedNodeId ? targetedNodeId.replace(/\./g, '-') : '';

          // look for connection with intermediate crop
          var intermediateConnectionsByNodeId = {};
          // les identifiants des noeuds ciblés par une connexion ayant une culture intermédiare
          var intermediateCropTargets = [];

          var targetedNode = null;
          var cycleIndex = 0;
          // on parcours les cycles pour retouver le noeud cible si le cicle du neoud a été trouvé on arrête la recheche
          while (cycleIndex < cycles.length && !targetedNode) {

            var cycle = cycles[cycleIndex++];

            // maps de toutes les connexions avec une culture intermédiaire organisée par cible (targetedNode)
            $scope._mapConnectionsWithNode(cycle.connectionDtos, intermediateConnectionsByNodeId, intermediateCropTargets);

            // on cherche le noeud cible
            var nodeIndex = 0;
            while (nodeIndex < cycle.nodeDtos.length && !targetedNode) {
              var node = cycle.nodeDtos[nodeIndex++];

              // le noeud cible est trouvé
              if (node.nodeId === targetedNodeId) {
                targetedNode = node;

                var toIntermediateSpecies = [];
                var toMainSpecies = [];

                // Règles concernant le copier/coller de cultures
                // CP = culture principale
                // CI = culture intermédiaire

                // source: CI d'une culture assolée
                // cible: Culture assolée sans CI
                // Règle: Copier sur la CP. Cocher toutes les espèces de la CP cible.

                // source: CI d'une culture assolée
                // cible: Culture assolée avec CI
                // Règle: Copier sur la CI.
                //   Si la CI cible a le même code que la CI source,
                //     cocher les espèces cibles qui sont les mêmes que les espèces sources (même refespece)
                //   sinon cocher toutes les espèces de la CI.

                var intermediateCropConnexion = intermediateConnectionsByNodeId[targetedNode.nodeId];
                if (!!intermediateCropConnexion) {
                  // on se souvient de la connexion pointant vers le noeud afin d'en retrouver ses espèces.
                  if (!!!targetedNode.connections) {
                    targetedNode.connections = [];
                  }
                  if (!!!targetedNode.connections[intermediateCropConnexion]) {
                    targetedNode.connections.push(intermediateCropConnexion);
                  }
                }

                // conserve original targeted crop code to be able to compare to pasted one
                var copiedIntervention = $scope._toNewIntervention(intervention);

                if (copiedIntervention.intermediateCrop && !!intermediateCropConnexion) {
                  // Source CI cible CI
                  angular.forEach(targetedNode.connections, function (connection) {
                    var toCroppingPlanEntries = $scope.croppingPlanEntriesIndex[connection.intermediateCroppingPlanEntryId];
                    if (toCroppingPlanEntries) {
                      copiedIntervention.targetedCroppingPlanEntriesId = toCroppingPlanEntries.topiaId;
                      copiedIntervention.targetedCroppingPlanEntriesCode = toCroppingPlanEntries.code;
                      toIntermediateSpecies = toIntermediateSpecies.concat(toCroppingPlanEntries.croppingPlanSpecies);
                    }
                  });

                } else {
                  // Source CI cible CP
                  copiedIntervention.intermediateCrop = false;
                  copiedIntervention.isCropChange = true;
                  var toCroppingPlanEntries = $scope.croppingPlanEntriesIndex[targetedNode.croppingPlanEntryId];
                  if (toCroppingPlanEntries) {
                    copiedIntervention.targetedCroppingPlanEntriesId = toCroppingPlanEntries.topiaId;
                    copiedIntervention.targetedCroppingPlanEntriesCode = toCroppingPlanEntries.code;
                    toMainSpecies = toMainSpecies.concat(toCroppingPlanEntries.croppingPlanSpecies);
                  }
                }

                var matchingSpeciesBySpeciesCode;
                if (intervention.intermediateCrop && copiedIntervention.intermediateCrop) {
                  // cible CI
                  // cocher les espèces cibles qui sont les mêmes que les espèces sources (même refespece)
                  matchingSpeciesBySpeciesCode = _findMatchingSpecies(fromIntermediateSpecies, toIntermediateSpecies);

                } else if (!copiedIntervention.intermediateCrop) {
                  // source CP cible CP
                  // Copier sur la CP. Si la CP cible a le même code que la CP source,
                  // cocher les espèces cibles qui sont les mêmes que les espèces sources (même refespece)
                  // sinon cocher toutes les espèces de la CP cible.
                  matchingSpeciesBySpeciesCode = _findMatchingSpecies(fromMainSpecies, toMainSpecies);
                }

                var speciesStadesDtos = copiedIntervention.speciesStadesDtos;
                var toIntermediateCrop = copiedIntervention.intermediateCrop;
                var targetedSpecies = toIntermediateCrop ? toIntermediateSpecies : toMainSpecies;

                var speciesStadeMigrationStatus = $scope._tryToMigrateSpeciesStades(
                  speciesStadesDtos,
                  targetedSpecies,
                  matchingSpeciesBySpeciesCode,
                  toIntermediateCrop);
                copiedIntervention.speciesStadesDtos = speciesStadeMigrationStatus.speciesStades;

                // if there are some seeding Action, we validate there treatments and inputs
                var fromCropCode = copiedIntervention.fromCropCode;
                var toCropCode = copiedIntervention.targetedCroppingPlanEntriesCode;

                var toSpecies = $scope._computeSpeciesFromSpeciesStades(copiedIntervention, toIntermediateSpecies, toMainSpecies);

                allActionsAndInputsMigrated = $scope._migrateActionsAndInputsSpecies(
                  copiedIntervention, toCropCode, speciesStadeMigrationStatus.status,
                  matchingSpeciesBySpeciesCode, toSpecies);

                $scope._migratePhytoInputDoseIfPossible(copiedIntervention, toIntermediateSpecies, toMainSpecies);

                targetedNode.interventions.push(copiedIntervention);
              }
            }
          }
        });
      }
      return allActionsAndInputsMigrated;
    };

    $scope._mapConnectionsWithNode = function (connectionDtos, intermediateConnectionsByNodeId, intermediateCropTargets) {
      if (connectionDtos) {
        angular.forEach(connectionDtos, function (connection) {
          if (angular.isDefined(connection.intermediateCroppingPlanEntryId)) {
            intermediateConnectionsByNodeId[connection.targetId] = connection;
            intermediateCropTargets.push(connection.targetId);
          }
        });
      }
    };

    $scope._pushToSameZonesInterventions = function (toSameZones, intervention) {
      // look for phase interventions
      var allMigrated = $scope._pushToTargetedPhasesInterventions(toSameZones.phases, intervention);
      // look for nodes intervention
      allMigrated &= $scope._pushToTargetedNodesInterventions(toSameZones.nodes, intervention);

      return allMigrated;
    };

    $scope._migrateActionsAndInputsSpecies = function (intervention, toCropCode, speciesStadeMigrationStatus, matchingSpeciesBySpeciesCode, toSpecies) {
      return $scope._migrateActionsAndInputs(intervention, matchingSpeciesBySpeciesCode, speciesStadeMigrationStatus, toCropCode, toSpecies);
    };
  }
]);
