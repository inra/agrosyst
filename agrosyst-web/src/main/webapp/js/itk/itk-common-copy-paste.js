/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
AgrosystModule.controller('ItkCommonCopyPasteController', ['$scope', '$http', 'ItkInitData',
  function ($scope, $http, ItkInitData) {

    // copy given intervention by removing all topia id
    $scope._toNewIntervention = function (intervention) {
      var copy = angular.copy(intervention);
      copy.topiaId = "NEW-INTERVENTION-" + guid();
      copy.edaplosIssuerId = null;
      copy.allInputsCopied = true; //true if all inputs have action and have been copied to the intervention

      var copiedActions = [];
      angular.forEach(copy.actionDtos, function (action) {
        var copiedAction = angular.copy(action);
        copiedActions.push(copiedAction);
        copiedAction.topiaId = "NEW-ACTION-" + guid();

        if (copiedAction.seedingSpecies) {
          angular.forEach(copiedAction.seedingSpecies, function (seedingSpecie) {
            delete seedingSpecie.topiaId;
          });
        }
        if (copiedAction.valorisationDtos) {
          angular.forEach(copiedAction.valorisationDtos, function (valorisation) {
            valorisation.topiaId = $scope._getNewValorisationId();
            angular.forEach(valorisation.qualityCriteria, function (qualityCriteria) {
              delete qualityCriteria.topiaId;
            });
          });
        }
      });
      copy.actionDtos = copiedActions;

      // treat usage of inputs
      angular.forEach(copy.actionDtos, function (action) {
        if (action.mineralProductInputUsageDtos) $scope._migrateUsages(action.mineralProductInputUsageDtos);
        if (action.phytoProductInputUsageDtos) $scope._migrateUsages(action.phytoProductInputUsageDtos);
        if (action.organicProductInputUsageDtos) $scope._migrateUsages(action.organicProductInputUsageDtos);
        if (action.otherProductInputUsageDtos) $scope._migrateUsages(action.otherProductInputUsageDtos);
        if (action.substrateInputUsageDtos) $scope._migrateUsages(action.substrateInputUsageDtos);
        if (action.potInputUsageDtos) $scope._migrateUsages(action.potInputUsageDtos);
        if (action.seedLotInputUsageDtos) $scope._migrateUsages(action.seedLotInputUsageDtos);
        if (action.irrigationInputUsageDto) action.irrigationInputUsageDto.inputUsageTopiaId = "NEW-USAGE-" + guid();
      });

      angular.forEach(copy.speciesStadesDtos, function (speciesStadesDto) {
        delete speciesStadesDto.topiaId;
      });

      var currentCropIds = $scope._getCurrentCropCodeIdentifiants();
      copy.fromCropId = copy.intermediateCrop ? currentCropIds.intermediateCropId : currentCropIds.id;
      copy.fromCropCode = copy.intermediateCrop ? currentCropIds.intermediateCropCode : currentCropIds.code;
      copy.previousCroppingPlanEntrySpecies = $scope._getSelectedPhaseConnectionOrNodeSpecies(copy.intermediateCrop);
      delete copy.destinationContext;

      return copy;
    };

    $scope._migrateUsages = function (usages) {
      angular.forEach(usages, function (usage) {
        usage.inputUsageTopiaId = "NEW-USAGE-" + guid();
        if (usage.targets && usage.targets.length > 0) {
          $scope._migrateTargets(usage.targets);
        }
        if (usage.seedingSpeciesDtos && usage.seedingSpeciesDtos.length > 0) {
          $scope._migrateSeedingSpecies(usage.seedingSpeciesDtos);
        }
      });
    }

    $scope._migrateTargets = function (targets) {
      angular.forEach(targets, function (target) {
        delete target.topiaId;
      });
    }

    $scope._migrateSeedingSpecies = function (seedingSpecies) {
      angular.forEach(seedingSpecies, function (species) {
        species.inputUsageTopiaId = "NEW-USAGE-" + guid();
        species.productName = species.productName ? species.productName : species.inputName;
        if (species.seedProductInputDtos && species.seedProductInputDtos.length > 0) {
          angular.forEach(species.seedProductInputDtos, function (product) {
            product.inputUsageTopiaId = "NEW-USAGE-" + guid();
            if (product.targets && product.targets.length > 0) {
              $scope._migrateTargets(product.targets);
            }
          });
        }
      });
    }

    $scope._migrateActionsAndInputs = function (intervention, matchingSpeciesBySpeciesCode, speciesStadeMigrationStatus, toCropCode, targetedSpecies) {
      var confirmNeeded = true;
      var toNewActions = true;
      var allMigrated = true;

      var newInterventionActionsResult = $scope._migrateActions(
        intervention,
        intervention.actionDtos,
        matchingSpeciesBySpeciesCode,
        toNewActions,
        confirmNeeded,
        targetedSpecies,
        toCropCode);

      intervention.actionDtos = newInterventionActionsResult.actionDtos;

      angular.forEach(intervention.actionDtos, function (action) {
        if (action.seedLotInputUsageDtos) {
          var filteredUsage = [];
          angular.forEach(action.seedLotInputUsageDtos, function (usage) {
            if (usage.seedingSpeciesDtos && usage.seedingSpeciesDtos.length > 0) {
              var ids = usage.seedingSpeciesDtos.map((species) => species.speciesIdentifier);
              if (targetedSpecies.some((target) => (ids.includes(target.code) || ids.includes(target.topiaId)))) {
                filteredUsage.push(usage);
              } else {
                allMigrated = false;
              }
            } else if (usage.domainSeedLotInputDto.cropSeedDto.code === toCropCode) {
              filteredUsage.push(usage);
            } else {
              allMigrated = false;
            }
          });
          action.seedLotInputUsageDtos = filteredUsage;
        }
      });

      return allMigrated;
    };

    $scope._migratePhytoInputDoseIfPossible = function (intervention, toIntermediateSpecies, toMainSpecies) {
      angular.forEach(intervention.actionDtos, function (action) {
        if (action.mainActionInterventionAgrosyst === "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES" && action.phytoProductInputUsageDtos && action.phytoProductInputUsageDtos.length > 0) {
          angular.forEach(intervention.phytoProductInputUsageDtos, function (usage) {
            var keyParts = usage.domainPhytoProductInputDto.key.split(";");
            var phytoProductId = keyParts[0];
            var idTraitement = keyParts[1];
            var treatedSpecies = angular.isDefined(treatedSpecies) ? treatedSpecies : $scope._getTreatedSpecies(intervention, input, toIntermediateSpecies, toMainSpecies);
            var previousInputPhytoProductUnit = usage.domainPhytoProductInputDto.usageUnit;
            $scope._loadActaReferenceDose(usage, phytoProductId, idTraitement, treatedSpecies).then(function () {
              if (!usage.referenceDose || previousInputPhytoProductUnit != usage.phytoProductUnit) {
                delete usage.qtMin;
                delete usage.qtAvg;
                delete usage.qtMed;
                delete usage.qtMax;
              }
            });
          });
        }
      });
    };
  }
]);
