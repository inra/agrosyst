/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
const messages = {};
messages[USER_LOCALE] = I18N;

if (DEFAULT_LOCALE !== USER_LOCALE) {
    messages[DEFAULT_LOCALE] = I18N;
}

window.AgrosystVueComponents.i18n = VueI18n.createI18n({
    locale: USER_LOCALE,
    fallbackLocale: DEFAULT_LOCALE,
    warnHtmlInMessage: 'off',
    messages,
});
