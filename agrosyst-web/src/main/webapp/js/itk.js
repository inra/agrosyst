/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Onglet 'Itinéraire technique'.
 */
AgrosystModule.controller('ItkController', ['$scope', '$http', '$q', '$window', '$timeout', '$filter', '$store', 'ItkInitData', 'I18nMessages',
  function ($scope, $http, $q, $window, $timeout, $filter, $store, ItkInitData, I18nMessages) {

    $scope.messages = I18nMessages;

    /*********************************************/
    /*  Initialisation : phases ou connections   */
    /*********************************************/

    // Phase ou connexion en cours d'édition
    //$scope.selectedPhaseConnectionOrNode;
    // Association entre une phase ou un noeud et son croppingPlanEntryCode
    //$scope.phaseOrConnectionCroppingPlanEntryCodes;
    // Map d'association entre les id de noeud et les noeud (pour avoir acces au information en plus sur les connexions)
    //$scope.connectionNodeIdToNodeObjectMap;
    // La liste des especes de la connexion ou de la phase (qui depend du CroppingPlanEntry)
    //$scope.phaseConnectionOrNodeSpecies;
    // Map entre le type de phase et sa traduction
    //$scope.perennialPhaseTypes = ItkInitData.perennialPhaseTypes; <-- Définie par le scope parent

    /*********************************************/
    /*  Initialisation : interventions           */
    /*********************************************/

    // Intervention sélectionnée en cours d'édition
    //$scope.editedIntervention;
    // La map des tools coupling code selectionné
    //$scope.editedInterventionSelectedToolCouplings;
    // table with interventions to copy
    //$scope.copiedInterventions;
    // map of selected unselected phases or nodes to copy interventions
    //$scope.copyPasteInterventions.selectedPhasesOrNodes;
    // available zones for copy
    //$scope.copyPasteInterventions.zonesToCopyTo;
    // map of CopyPasteZoneDto ordered by phaseId
    //$scope.copyPasteInterventions.phases;
    // map of CopyPasteZoneDto ordered by nodeId
    //$scope.copyPasteInterventions.nodes;
    // map of TargetedZones ordered by zoneId
    //$scope.copyPasteInterventions.targetedZones;
    // object use to copy-paste interventions
    // unités de voulume par voyage
    $scope.materielTransportUnits = $scope.i18n.MaterielTransportUnit;
    // map toolsCouplingCode to toolsCoupling entity
    var _toolsCouplingCodeToToolsCoupling = {};
    // flag used to detect a work rates's manual change.
    $scope.userSetValues = {
      workRate: false,
      transitVolume: false,
      involvedPeopleNumber: false
    };
    //
    $scope.copyPasteInterventions = {};

    /*********************************************/
    /*  Initialisation : actions                 */
    /*********************************************/

    // Action sélectionnée en cours d'édition
    //$scope.editedAction;
    // Type d'actions (Map Liste d'autorité et traduction)
    $scope.agrosystInterventionTypes = $scope.i18n.AgrosystInterventionType;
    // valorisation des cultures vin
    $scope.wineValorisations = $scope.i18n.WineValorisation;
    // Catégories de rendements
    $scope.yealdCategories = $scope.i18n.YealdCategory;
    // yealdUnits
    $scope.yealdUnits = $scope.i18n.YealdUnit;
    // types de semences
    $scope.seedTypes = $scope.i18n.SeedType;
    // seedPlantUnits
    $scope.seedPlantUnits = $scope.i18n.SeedPlantUnit;
    // Unités de volumes
    $scope.capacityUnits = $scope.i18n.CapacityUnit;
    // Actions Agrosyst
    $scope.agrosystActionsFullIndex = {};
    // use do load destinations for harvesting action
    $scope.sector = ItkInitData.sector;
    $scope.isOrganic = ItkInitData.isOrganic;
    $scope.pastureTypes = $scope.i18n.PastureType;
    $scope.cattles = ItkInitData.cattles;

    $scope.qualitativeValues = ItkInitData.qualitativeValues;

    $scope.defaultDestinationName = ItkInitData.defaultDestinationName;

    $scope.productTypes = ItkInitData.productTypes;

    /*********************************************/
    /*  Initialisation : intrants                */
    /*********************************************/

    // Intrants Agrosyst
    $scope.inputTypesLabels = $scope.i18n.InputType;
    $scope.inputTypesFullList = Object.keys($scope.inputTypesLabels);

    /* Initialisation : intrants minéraux */
    // type de produits fertilisant minéraux
    $scope.mineralProductTypes = ItkInitData.mineralProductTypes;
    $scope.mineralProductTypesIndex = {};
    angular.forEach($scope.mineralProductTypes, function (mineralProductType) {
      $scope.mineralProductTypesIndex[mineralProductType.categ] = mineralProductType;
    });
    // Enum des unités des produits pour les intrants minéraux
    $scope.mineralProductUnits = $scope.i18n.MineralProductUnit;
    // Map des champs de l'objet RefFertiMinUNIFA associé avec leur libéllé
    // Be careful: any element have to be translated otherwise it will not be present !
    $scope.mineralProductElementNamesAndLibelles = ItkInitData.mineralProductElementNamesAndLibelles;
    // info added to js objects are identified with suffix
    $scope.extendedObjectInfos = "_extendedInfos";
    // suffix added to $scope.extendedObjectInfos to identified mineral element addition
    $scope.mineralProductApport = "_apport";

    /* Initialisation : intrants phyto */
    // List<Enum> Sous-ensemble des bio-agresseurs utilisables comme catégories de traitement
    $scope.treatmentTargetCategories = ItkInitData.treatmentTargetCategories;
    $scope.treatmentTargetCategoriesByParent = ItkInitData.treatmentTargetCategoriesByParent;
    $scope.groupesCibles = ItkInitData.groupesCibles;
    $scope.groupesCiblesByCode = {};
    for (var i = 0; i < $scope.groupesCibles.length; i++) {
      var groupeCible = $scope.groupesCibles[i];
      $scope.groupesCiblesByCode[groupeCible.codeGroupeCibleMaa] = groupeCible;
    }
    // liste des produits Acta correspondant à un type de produit Acta
    //$scope.actaProducts;
    // code du produitActa sélectionné.
    //$scope.actaTreatmentCode;
    // Enum des unités des produits pour les intrants phyto
    $scope.phytoProductUnits = $scope.i18n.PhytoProductUnit;
    // List des types de produits ACTA par AgrosystInterventionType
    $scope.actaTreatmentProductTypes = ItkInitData.actaTreatmentProductTypes;

    // Enum des unités des produits pour les intrants organiques
    $scope.organicProductUnits = $scope.i18n.OrganicProductUnit;
    // Enum des unités des teneurs de produits ferti Orga
    $scope.fertiOrgaUnits = $scope.i18n.FertiOrgaUnit;

    $scope.substratesByCaracteristic1 = ItkInitData.substratesByCaracteristic1;
    $scope.substrateInputUnits = $scope.i18n.SubstrateInputUnit;

    $scope.refPots = ItkInitData.refPots;
    $scope.potInputUnits = $scope.i18n.PotInputUnit;
    $scope.seedPlantUnits = $scope.i18n.SeedPlantUnit;

    $scope.otherProductInputUnits = $scope.i18n.OtherProductInputUnit;

    /*********************************************/
    /*  Initialisation : divers                  */
    /*********************************************/
    // stocke le nom de la catégorie de rendement pour les messages d'avertissement
    //$scope.yealdCategory;
    // stocke le nom d'un produit pour les messages d'avertissement
    //$scope.productName;
    // stocke le nom de l'intervention, utilisé pour les messages d'avertissement
    //$scope.interventionName;
    // stocke le type d'action à supprimer
    //$scope.deletedActionType;
    // la liste des combinaisons d'outils utilisable dans les interventions d'itk
    //$scope.toolsCouplings;
    // {object} Copy interventions json object (selected intervention to copy)
    //$scope.selectedInterventions;
    // {boolean} If localstore contains data that can be copied
    //$scope.pastedInterventions;
    // Map de cultures intermediaires pour du cycle assolé du réalisé
    //$scope.effectiveIntermediateCropIdForConnectionId = {};
    // intermediate phaseOrNode Species
    //$scope.connectionIntermediateSpecies;
    // Intermediate crops
    //$scope.connectionIntermediateCrop
    // map des stades de culture par species code
    //$scope.speciesStadeBySpeciesCode;
    // Map de cultures intermediaires pour du cycle assolé du pratiqué
    // Ce préfixe permet d'isoler les choses qui doivent l'être entre pratiqué et réalisé
    $scope.prefix = ItkInitData.prefix;
    // unités de débit de chantier
    $scope.materielWorkRateUnits = $scope.i18n.MaterielWorkRateUnit;
    // unités de critère de qualité des récoltes
    $scope.criteriaUnits = $scope.i18n.CriteriaUnit;
    // available units according to materiel
    $scope.workRateUnits = ["T_H", "H_HA", "HA_H", "VOY_H", "BAL_H"];
    //
    $scope.practicedIntermediateCropCyclesCodes = {};

    $scope.practicedPeriodFormatPattern = /^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])/i;

    const substrateActionCodes = ['AAT27', 'AAT20', 'AAT04', 'AAT35', 'AAT30', 'AAT31', 'AAT28', 'AAT32', 'AAT34', 'AAT29', 'AAT33', 'SEW'];
    const potActionCodes = ['AAT20', 'AAT04'];

    // This list MUST BE kept in sync with the list of the same name in the function `_updateAvailableActionTypes` in domain-tools-coupling-edition.js
    const TYPES_ALLOWING_3_ACTIONS = ['AUTRE', 'ENTRETIEN_TAILLE_VIGNE_ET_VERGER', 'TRAVAIL_DU_SOL'];

    /*********************************************/
    /*  Initialisation : définis sur le parent   */
    /*********************************************/

    if (!$scope.perennialPhaseTypes) {
      console.error("'$scope.perennialPhaseTypes' devrait être défini dans le controlleur parent");
    }
    if (!$scope.getItkIdentifierQuery) {
      console.error("'$scope.getItkIdentifierQuery' devrait être défini dans le controlleur parent");
    }
    if (!$scope.croppingPlanEntrySpeciesIndex && !$scope.croppingPlanEntriesIndex) {
      console.error("'$scope.croppingPlanEntrySpeciesIndex ou $scope.croppingPlanEntriesIndex' devrait être défini dans le controlleur parent");
    }
    if (!$scope.seasonalCropCycles) {
      console.error("'$scope.seasonalCropCycles' devrait être défini dans le controlleur parent");
    }
    if (!$scope.perennialCropCycles) {
      console.error("'$scope.perennialCropCycles' devrait être défini dans le controlleur parent");
    }
    if (!$scope.croppingPlanModel) {
      console.error("'$scope.croppingPlanModel' devrait être défini dans le controlleur parent");
    }

    $scope.updateCroppingPlanIndex = function () {
      $scope.croppingPlanIndex = {};
      angular.forEach($scope.croppingPlanModel, function (croppingPlanEntry) {
        var key = croppingPlanEntry.croppingPlanEntryId || croppingPlanEntry.croppingPlanEntryCode;
        $scope.croppingPlanIndex[key] = croppingPlanEntry;
      });
      angular.forEach($scope.intermediateCroppingPlanModel, function (croppingPlanEntry) {
        var key = croppingPlanEntry.croppingPlanEntryId || croppingPlanEntry.croppingPlanEntryCode;
        $scope.croppingPlanIndex[key] = croppingPlanEntry;
      });
    };

    $scope.updateCroppingPlanIndex();


    // function that select the perennial crop to edit

    $scope.displayFieldsErrorsAlert = true;
    // listener is on agrosyst-app.js
    document.addEventListener('invalid', _errorValidationManager.bind(null, $scope, $timeout, null), true);

    /*********************************************/
    /*           Phases et connections           */
    /*********************************************/

    /* Method called to reset all action edition used variables */
    $scope._resetEditedAction = function () {
      delete $scope.editedAction;
      delete $scope.originalAction;
      delete $scope.actaTreatmentCode;
      delete $scope.refMinUnifas;
      delete $scope.editedValorisation;
      delete $scope.editedSeedingSpeciesAction;
      if ($scope.editedIntervention) {
        $scope._checkAvailableActionAndInputTypes($scope.editedIntervention);
      }
      $scope.seedingActionUsagesToRemove = [];
    };

    $scope._resetEditedUsage = function () {
      if ($scope.editedUsage) {
        delete $scope.editedUsage.inputs;
      }
      delete $scope.editedUsage;
      delete $scope.treatmentTarget;
      delete $scope.treatmentTargets;
    };

    $scope.allSpeciesByCode = {};

    $scope.initItkTab = function () {

      return $http.get(ENDPOINT_LOAD_ALL_ACTIVE_AGROSYST_ACTIONS_JSON, { cache: true })
        .then(function (response) {

          // clear next
          $scope.agrosystActionsFullList = response.data;

          angular.forEach($scope.agrosystActionsFullList, function (agrosystAction) {
            $scope.agrosystActionsFullIndex[agrosystAction.topiaId] = agrosystAction;
          });

          $scope.selectedInterventions = {};
          delete $scope.editedInterventionSelectedToolCouplings;
          if ($scope.practicedSystem) {
            $scope.practicedPhasesAndConnections();
          } else {
            $scope.effectivePhasesAndNodes();
          }

          if (!$scope.$$phase) {
            $scope.$digest();
          }

          hidePageLoading();

        }).
        catch(function (response) {
          var message = $scope.messages.toolscouplingLoadingActionsFailed;
          addPermanentError(message, response.status);
          console.error(message, response);
          hidePageLoading();
        });

    };

    // get phases and nodes of all cycles
    $scope.effectivePhasesAndNodes = function () {
      var phasesAndNodesArray = [];
      $scope.effectiveIntermediateCropIdForConnectionId = {};
      angular.forEach($scope.seasonalCropCycles, function (cycle) {
        angular.forEach(cycle.connectionDtos, function (connection) {
          if (connection.intermediateCroppingPlanEntryId) {
            $scope.effectiveIntermediateCropIdForConnectionId[connection.targetId] = connection.intermediateCroppingPlanEntryId;
          }
        });
        angular.forEach(cycle.nodeDtos, function (node) {
          if (node.type !== "NODE_BEFORE") {
            phasesAndNodesArray.push(node);
            // init effectiveInterventionDtos (for size in table)
            if (!node.interventions) {
              node.interventions = [];
            }
          }
        });
      });

      angular.forEach($scope.perennialCropCycles, function (cycle) {
        angular.forEach(cycle.phaseDtos, function (phase) {
          phasesAndNodesArray.push(phase);
          // init effectiveInterventionDtos (for size in table)
          if (!phase.interventions) {
            phase.interventions = [];
          }
          phase.croppingPlanEntryId = cycle.croppingPlanEntryId;
        });
      });
      $scope.phasesAndNodesArray = phasesAndNodesArray;
    };

    // Phase and connections of all cycles
    $scope.practicedPhasesAndConnections = function () {
      var phasesAndConnectionsArray = [];
      $scope.phaseOrConnectionCroppingPlanEntryCodes = [];
      $scope.connectionNodeIdToNodeObjectMap = {};

      var allConnections = [];

      angular.forEach($scope.seasonalCropCycles, function (cycle) {

        // ordered node by id
        angular.forEach(cycle.cropCycleNodeDtos, function (node) {
          $scope.connectionNodeIdToNodeObjectMap[node.nodeId] = node;
        });

        angular.forEach(cycle.cropCycleConnectionDtos, function (conn) {
          allConnections.push(conn);
          if (conn.intermediateCroppingPlanEntryId) {
            $scope.practicedIntermediateCropCyclesCodes[conn.targetId] = conn.intermediateCroppingPlanEntryCode;
          }
          if (!conn.interventions) {
            conn.interventions = [];
          }

          // pour la connexion, elle porte sur la culture de son noeud cible
          var targetNode = $scope.connectionNodeIdToNodeObjectMap[conn.targetId];
          var nodeCroppingPlanEntryCode = { key: conn, value: targetNode.croppingPlanEntryCode };
          $scope.phaseOrConnectionCroppingPlanEntryCodes.push(nodeCroppingPlanEntryCode);
        });

      });

      // order nodes by rank
      var orderedConnections = [];
      angular.forEach(allConnections, function (connection) {
        var node = $scope.connectionNodeIdToNodeObjectMap[connection.targetId];
        var allSameRankCons = orderedConnections[node.x];
        if (!allSameRankCons) {
          allSameRankCons = [];
        }
        allSameRankCons.push(connection);
        orderedConnections[node.x] = allSameRankCons;
      });
      angular.forEach(orderedConnections, function (connections) {
        if (connections) {
          angular.forEach(connections, function (connection) {
            phasesAndConnectionsArray.push(connection);
          });
        }
      });

      // add phases
      angular.forEach($scope.perennialCropCycles, function (cycle) {
        angular.forEach(cycle.cropCyclePhaseDtos, function (phase) {
          phasesAndConnectionsArray.push(phase);
          if (!phase.interventions) {
            phase.interventions = [];
          }
          if (cycle.practicedPerennialCropCycle) {
            // recuperation de la culture qui est celle du cycle pérenne
            var phaseCroppingPlanEntryCode = { key: phase, value: cycle.practicedPerennialCropCycle.croppingPlanEntryCode };
            $scope.phaseOrConnectionCroppingPlanEntryCodes.push(phaseCroppingPlanEntryCode);

            // association des cultures du cycle
            phase.croppingPlanEntryCode = cycle.practicedPerennialCropCycle.croppingPlanEntryCode;
          }
        });
      });
      $scope.phasesAndConnectionsArray = phasesAndConnectionsArray;
    };

    $scope._setCroppingPlanSpeciesValues = function (croppingPlanSpecies) {
      croppingPlanSpecies.code_espece_botanique = croppingPlanSpecies.species.code_espece_botanique;
      croppingPlanSpecies.code_qualifiant_AEE = croppingPlanSpecies.species.code_qualifiant_AEE;
      croppingPlanSpecies.speciesEspece = croppingPlanSpecies.species.libelle_espece_botanique_Translated;
      croppingPlanSpecies.speciesQualifiant = croppingPlanSpecies.species.libelle_qualifiant_AEE_Translated;
      croppingPlanSpecies.speciesTypeSaisonnier = croppingPlanSpecies.species.libelle_type_saisonnier_AEE_Translated;
      croppingPlanSpecies.speciesDestination = croppingPlanSpecies.species.libelle_destination_AEE_Translated;
      if (croppingPlanSpecies.variety) {
        if (croppingPlanSpecies.variety.variete) {
          croppingPlanSpecies.varietyLibelle = croppingPlanSpecies.variety.variete;
        } else {
          croppingPlanSpecies.varietyLibelle = croppingPlanSpecies.variety.denomination;
        }
      }
    };

    // effective
    $scope._getNodeConnectionOrPhaseCropAndSpeciesContext = function (phaseConnectionOrNode, croppingPlanEntries) {
      var result = {};
      result.connectionIntermediateCrop = null;
      result.phaseOrNodeCrop = null;
      result.phaseConnectionOrNodeSpecies = [];
      result.connectionIntermediateSpecies = [];
      $scope.allSpeciesByCode = {};

      angular.forEach(croppingPlanEntries, function (croppingPlanEntry) {
        var intermediate = $scope.effectiveIntermediateCropIdForConnectionId[phaseConnectionOrNode.nodeId] === croppingPlanEntry.topiaId;
        if (phaseConnectionOrNode.croppingPlanEntryId === croppingPlanEntry.topiaId || intermediate) {
          var croppingPlanSpecies = croppingPlanEntry.croppingPlanSpecies;
          if (angular.isDefined(croppingPlanSpecies)) {
            angular.forEach(croppingPlanSpecies, function (croppingPlanSpecies) {
              $scope._setCroppingPlanSpeciesValues(croppingPlanSpecies);
              $scope.allSpeciesByCode[croppingPlanSpecies.code] = croppingPlanSpecies;
              if (intermediate) {
                result.connectionIntermediateSpecies.push(croppingPlanSpecies);
              } else {
                result.phaseConnectionOrNodeSpecies.push(croppingPlanSpecies);
              }
            });
          } else {
            croppingPlanEntry.croppingPlanSpecies = [];
          }
          if (intermediate) {
            result.connectionIntermediateCrop = croppingPlanEntry;
          } else {
            result.phaseOrNodeCrop = croppingPlanEntry;
          }
        }
      });
      return result;
    };

    $scope._addInterventionProportionOfTreatedSurface = function (intervention) {
      var proportionOfBioTreatedSurface = 0;
      var proportionOfPhytoTreatedSurface = 0;
      if (intervention.actionDtos) {
        angular.forEach(intervention.actionDtos, function (action) {
          var actionType = action.mainActionInterventionAgrosyst;
          if (actionType === "LUTTE_BIOLOGIQUE") {
            proportionOfBioTreatedSurface = proportionOfBioTreatedSurface + action.proportionOfTreatedSurface;
          } else if (actionType === "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES") {
            proportionOfPhytoTreatedSurface = proportionOfPhytoTreatedSurface + action.proportionOfTreatedSurface;
          }
        });
      }
      if (proportionOfBioTreatedSurface > 0) {
        intervention.proportionOfBioTreatedSurface = proportionOfBioTreatedSurface;
      }
      if (proportionOfPhytoTreatedSurface) {
        intervention.proportionOfPhytoTreatedSurface = proportionOfPhytoTreatedSurface;
      }
    };

    $scope._resetEditedIntervention = function () {
      delete $scope.editedIntervention;
      $('#new-intervention').hide();
      $scope._resetEditedAction();
      $scope._resetEditedUsage();

      delete $scope.stadesList;
      delete $scope.editedSpeciesStades;
      delete $scope.loadDestinationsContext;
      delete $scope.speciesStadeBySpeciesCode;
    };

    $scope.isSpeciesQtyAvailable = function(speciesInput) {
      var inputSpeciesCode = speciesInput.speciesSeedDto ? speciesInput.speciesSeedDto.code : '';
      return $scope.editedIntervention.intermediateCrop ? true : $scope.editedIntervention.speciesStadesDtos.find(ss => ss.speciesCode === inputSpeciesCode) !== undefined;
    };

    $scope._createUsageDataForIntervention = function (intervention) {
      let usageData = [];
      if (intervention.usages) {
        usageData = usageData.concat(
          intervention.usages
            .filter(usage => !usage.domainSeedLotInputDto)
            .map(usage => {
              let unit;
              if (usage.domainOtherProductInputDto && usage.domainOtherProductInputDto.usageUnit && $scope.otherProductInputUnits) {
                unit = $scope.otherProductInputUnits[usage.domainOtherProductInputDto.usageUnit];

              } else if (usage.domainMineralProductInputDto && usage.domainMineralProductInputDto.usageUnit && $scope.mineralProductUnits) {
                unit = $scope.mineralProductUnits[usage.domainMineralProductInputDto.usageUnit];

              } else if (usage.domainOrganicProductInputDto && usage.domainOrganicProductInputDto.usageUnit && $scope.organicProductUnits) {
                unit = $scope.organicProductUnits[usage.domainOrganicProductInputDto.usageUnit];

              } else if (usage.domainPhytoProductInputDto && usage.domainPhytoProductInputDto.usageUnit && $scope.phytoProductUnits) {
                unit = $scope.phytoProductUnits[usage.domainPhytoProductInputDto.usageUnit];

              } else if (usage.domainSubstrateInputDto && usage.domainSubstrateInputDto.usageUnit && $scope.substrateInputUnits) {
                unit = $scope.substrateInputUnits[usage.domainSubstrateInputDto.usageUnit];

              } else if (usage.domainPotInputDto && usage.domainPotInputDto.usageUnit && $scope.potInputUnits) {
                unit = $scope.potInputUnits[usage.domainPotInputDto.usageUnit];

              }
              let label;
              if (usage.domainMineralProductInputDto && usage.domainMineralProductInputDto.inputName) {
                label = usage.domainMineralProductInputDto.inputName;
              } else if (usage.domainPhytoProductInputDto && usage.domainPhytoProductInputDto.inputName) {
                label = usage.domainPhytoProductInputDto.inputName;
              } else if (usage.domainOrganicProductInputDto && usage.domainOrganicProductInputDto.inputName) {
                label = usage.domainOrganicProductInputDto.inputName;
              } else if (usage.domainOtherProductInputDto && usage.domainOtherProductInputDto.inputName) {
                label = usage.domainOtherProductInputDto.inputName;
              } else if (usage.domainSubstrateInputDto && usage.domainSubstrateInputDto.inputName) {
                label = usage.domainSubstrateInputDto.inputName;
              } else if (usage.domainPotInputDto && usage.domainPotInputDto.inputName) {
                label = usage.domainPotInputDto.inputName;
              }
              return {
                label: label,
                quantity: usage.qtAvg,
                unit: unit,
                comment: usage.comment
              };
            })
        );

        // lots de semences : on récupère le détail par espèce
        usageData = usageData.concat(
          intervention.usages
            .filter(usage => usage.domainSeedLotInputDto && usage.seedingSpeciesDtos)
            .flatMap(usage => {
              // renseigner usageUnit si n'est pas encore présent au niveau de l'espèce
              usage.seedingSpeciesDtos.forEach(seedingSpeciesDto => {
                if (!seedingSpeciesDto.usageUnit) {
                  seedingSpeciesDto.usageUnit = usage.domainSeedLotInputDto.usageUnit;
                }
              });
              return usage.seedingSpeciesDtos;
            })
            .filter(seedingSpecies => $scope.allSpeciesByCode[seedingSpecies.domainSeedSpeciesInputDto.speciesSeedDto.code] && seedingSpecies.usageUnit)
            .map(seedingSpecies => {
              const species = $scope.allSpeciesByCode[seedingSpecies.domainSeedSpeciesInputDto.speciesSeedDto.code];
              let label = species.speciesEspece;
              if (species.speciesQualifiant) {
                label += " " + species.speciesQualifiant;
              }
              if (species.varietyLibelle) {
                label += " (" + species.varietyLibelle + ")";
              } else if (species.edaplosUnknownVariety) {
                label += " (" + species.edaplosUnknownVariety + ")";
              }
              return {
                label: label,
                quantity: seedingSpecies.qtAvg,
                unit: $scope.seedPlantUnits[seedingSpecies.usageUnit]
              };
            })
        );
      }
      if (intervention.actionDtos) {
        // return summary of harvesting quantity by destination
        var croppingPlanEntry = $scope.getCroppingPlanEntry($scope._getSelectedCroppingPlanEntryIdentifier());
        var isMixVarietyOrSpecies = croppingPlanEntry && (croppingPlanEntry.mixSpecies || croppingPlanEntry.mixVariety);
        var speciesTotalArea;
        if (!isMixVarietyOrSpecies && intervention.speciesStadesDtos && intervention.speciesStadesDtos.length > 1) {
          speciesTotalArea = 0;
          angular.forEach(intervention.speciesStadesDtos, function(speciesStade) {
            var species = $scope.allSpeciesByCode[speciesStade.speciesCode];
            if (species) {
              speciesTotalArea += species.speciesArea ? species.speciesArea : 0;
            }
          });
        }
        if (!speciesTotalArea) {
          speciesTotalArea = 100;
        }
        var harvestingSummary = intervention.actionDtos
          .filter(action => action.mainActionInterventionAgrosyst === "RECOLTE" && action.valorisationDtos && action.valorisationDtos.length > 0)
          .map(action => {
            let valorisations = action.valorisationDtos;
            return valorisations.reduce(function (models, valorisation) {
              let destinationId = valorisation.destinationId;
              let model = models[valorisation.destinationId];
              let quantity = valorisation.yealdAverage;
              if (!isMixVarietyOrSpecies) {
                let speciesStadeNb = intervention.speciesStadesDtos.length;
                let species = $scope.allSpeciesByCode[valorisation.speciesCode];
                let speciesArea = 0;
                if (speciesStadeNb !== 0 && species && $scope.allSpeciesByCode[valorisation.speciesCode].speciesArea) {
                  speciesArea = $scope.allSpeciesByCode[valorisation.speciesCode].speciesArea;
                } else if (speciesStadeNb !== 0 && species) {
                  speciesArea = speciesTotalArea / intervention.speciesStadesDtos.length;
                } else {
                  speciesArea = 0;
                }
                quantity = quantity * speciesArea / speciesTotalArea;
              }
              // let speciesArea = speciesStadeNb !== 0 && species ? $scope.allSpeciesByCode[valorisation.speciesCode].speciesArea || speciesTotalArea / intervention.speciesStadesDtos.length : 0;
              let model0 = {
                destination: destinationId,
                quantity: quantity,
                unit: $scope.yealdUnits[valorisation.yealdUnit]
              }
              if (angular.isUndefined(model)) {
                models[destinationId] = model0;
              } else {
                model.quantity = model.quantity + model0.quantity;
              }
              return models;
            }, {});
          });
        if (harvestingSummary[0]) {
          usageData = Object.values(harvestingSummary[0]).concat(usageData);
        }
      }
      return usageData;
    };

    $scope.$watch("selectedPhaseConnectionOrNode.interventions", function (newValue, oldValue, scope) {
      angular.forEach(newValue, function (intervention) {
        if (intervention.actionDtos) {
          intervention.usages = intervention.actionDtos.map((action) => $scope.getUsageDtos(action))
            .reduce((prev, curr) => prev.concat(curr), []);
        }
        intervention.usageData = $scope._createUsageDataForIntervention(intervention);
      });
    }, true);

    $scope.$watch("editedIntervention", function (newValue, oldValue, scope) {
      if (newValue && newValue.actionDtos) {
        newValue.actionDtos.forEach((action) => {
          action.mainAction = $scope.agrosystActionsFullList.filter((elt) => elt.topiaId === action.mainActionId)[0];
          if (action.otherProductInputUsageDtos) {
            action.otherProductInputUsageDtos.forEach((usage) => {
              usage.mainActionId = action.mainActionId;
            });
          }
        });
      }
    }, true);

    // edit phase or node on effective crop cycle
    $scope.editPhaseOrNode = function (phaseOrNode) {
      if (!$scope.editedInterventionForm.$valid) {
        // do not close if not valid
        return;
      }

      $scope.setSelectedIntervention();
      delete $scope.editedIntervention; // hide selected intervention
      $scope._resetEditedIntervention();

      $scope.selectedPhaseConnectionOrNode = phaseOrNode;

      // load species lists and crops
      var result = $scope._getNodeConnectionOrPhaseCropAndSpeciesContext($scope.selectedPhaseConnectionOrNode, $scope.zoneCroppingPlanEntries);
      $scope.connectionIntermediateCrop = result.connectionIntermediateCrop;
      $scope.phaseOrNodeCrop = result.phaseOrNodeCrop;
      $scope.phaseConnectionOrNodeSpecies = result.phaseConnectionOrNodeSpecies;
      $scope.connectionIntermediateSpecies = result.connectionIntermediateSpecies;

      // convertit les dates des interventions en Date
      angular.forEach($scope.selectedPhaseConnectionOrNode.interventions, function (intervention) {
        intervention.showCompleteActionList = false;
        intervention.showCompleteProductList = false;

        if (angular.isString(intervention.startInterventionDate)) {
          intervention.startInterventionDate = new Date(Date.parse(intervention.startInterventionDate));
        }
        if (angular.isString(intervention.endInterventionDate)) {
          intervention.endInterventionDate = new Date(Date.parse(intervention.endInterventionDate));
        }
        // s'il n'y a pas espèces intermédiaires alors les interventions ne peuvent être affectée à une culture intermédiaire.
        if (!$scope.connectionIntermediateCrop) {
          intervention.intermediateCrop = false;
        }
        $scope._addInterventionProportionOfTreatedSurface(intervention);
      });
      // reinitialisation de l'objet de selection
      $scope.selectedInterventions = {};
      $scope.pastedInterventions = !!$store.get($scope.prefix + "-interventions-" + $scope.domainId);
    };

    $scope._getRemoveSeedingUsagesMessages = function (usagesToRemove) {
      var message;
      if (usagesToRemove) {
        if (usagesToRemove.length === 1) {
          message = $scope.messages.removeSeedingUsages1 + "\n";
        } else if (usagesToRemove.length > 1) {
          message = usagesToRemove.length + $scope.messages.removeSeedingUsagesSeveral + " \n";
        }
      }

      return message;
    };

    $scope._removeUsagesFromInterventions = function (intervention, usagesToRemove) {
      angular.forEach(usagesToRemove, function (usage) {
        var index = intervention.usages.indexOf(usage);
        if (index !== -1) {
          intervention.usages.splice(index, 1);
        }
      });
    };

    $scope._initActionResult = function () {
      var result = {
        treatment: false,
        actionByIds: {}, // action by original topiaid to be able to link new action with there usages,
        actionDtos: [],
        harvestingActionValorisationsToRemovesByActionIds: {},
        done: true // if migration is done and is not cancel by user
      };
      return result;
    };

    $scope.removeExtraHarvestingActionFields = function (action) {
      delete action.mainValorisations;
      delete action.speciesStadeSpecies;
      delete action.speciesValorisations;
      delete action.speciesValorisationsBySpeciesCode;
      delete action.destinationsByDestinationLabels;
      delete action.qualityCriteria;
      delete action.qualityCriteriaUnits;
      delete action.isOrganic;
      delete action.nbSameValorisationSectorFromMainValorisation;
      delete action.valorisationsByMainValorisationIds;
      delete action.selectedWineValorisations;
      delete action.errorMessage;
      delete $scope.isOrganicCrop;

    };

    $scope._removeActionLightBoxAttributes = function (action) {

      delete $scope.mainActionInterventionAgrosyst;
      delete $scope.mainAction;

      delete action.availableActionTypes;
      delete action.availableActions;
      delete action.capacityUnits;
      delete action.seedTypes;
      delete action.yealdUnits;
      delete action.seedPlantUnits;
      delete action.yealdCategories;
      delete action.speciesForSeeding;
      delete action.agrosystActionsFullList;
      delete action.errorMessage;

      $scope.removeExtraHarvestingActionFields(action);
    };

    $scope._bindAction = function (action, isToNewAction) {
      var result;
      if (isToNewAction) {
        result = angular.copy(action);
        result.topiaId = "NEW-ACTION-" + guid();
      } else {
        result = action;
      }
      $scope._removeActionLightBoxAttributes(result);

      return result;
    };

    $scope.getSpeciesLabelForSpecies = function (action, speciesCode) {
      return $scope._getSpeciesLabelForSpecies(action.speciesByCodes.speciesByCode[speciesCode]);
    };

    $scope._getSpeciesLabelForSpecies = function (species, hideVarietyWarning) {
      var speciesLabel = "";
      if (species) {
        speciesLabel += species.speciesEspece;
        if (species.speciesQualifiant) {
          speciesLabel += ' ' + species.speciesQualifiant;
        }
        if (species.varietyLibelle) {
          speciesLabel += ' (' + species.varietyLibelle + ')';
        }
        if (!species.varietyLibelle && species.edaplosUnknownVariety) {
          speciesLabel += ' (';
          if (!hideVarietyWarning) {
            speciesLabel += '<span class="warning-label"><i class="fa fa-warning" aria-hidden="true"></i>' + $scope.messages.unknownVariety + ' "';
          }
          speciesLabel += species.edaplosUnknownVariety;
          if (!hideVarietyWarning) {
            speciesLabel += '"</span>';
          }
          speciesLabel += ')';
        }
      }
      return speciesLabel;
    };

    $scope._getCurrentCampaigns = function () {
      var result = "";
      if ($scope.practicedSystem) {
        result = $scope.practicedSystem.campaigns.replace(/\D+/, " ");
        if (!result) {
          // set temporaly default value
          result = new Date().getFullYear();
        }

      } else {
        var result0 = [];

        var startInterventionDate = null;
        var endInterventionDate = null;

        if ($scope.editedIntervention) {
          startInterventionDate = $scope.editedIntervention && !!Date.parse($scope.editedIntervention.startInterventionDate) ? new Date($scope.editedIntervention.startInterventionDate) : $scope.editedIntervention.startInterventionDate;
          endInterventionDate = !!Date.parse($scope.editedIntervention.endInterventionDate) ? new Date($scope.editedIntervention.endInterventionDate) : $scope.editedIntervention.endInterventionDate;
        }

        // set user selected campaign otherwise set effective default one
        var startingCampaign = startInterventionDate ? startInterventionDate.getFullYear() : $scope.effectiveCampaign;

        result0.push(startingCampaign);

        // set user selected campaign otherwise set effective default one
        var endingCampaign = endInterventionDate ? endInterventionDate.getFullYear() : $scope.effectiveCampaign;


        result0.push(endingCampaign);

        result = result0.join(" ");
      }
      return result;
    };

    $scope._getCurrentCrop = function (intervention) {
      var cpEntryIdentifier;
      if (intervention.intermediateCrop) {
        // effective
        if ($scope.effectiveIntermediateCropIdForConnectionId) {
          cpEntryIdentifier = $scope.effectiveIntermediateCropIdForConnectionId[$scope.selectedPhaseConnectionOrNode.nodeId];
        }
        // practiced
        if (!cpEntryIdentifier) {
          cpEntryIdentifier = $scope.selectedPhaseConnectionOrNode.intermediateCroppingPlanEntryCode;
        }
      } else {
        cpEntryIdentifier = $scope._getSelectedCroppingPlanEntryIdentifier();
      }

      var cpEntry = $scope.getCroppingPlanEntry(cpEntryIdentifier);
      return cpEntry;
    };

    $scope._groupValorisationBySpecies = function (valorisation, valorisationsBySpeciesCode) {
      if (valorisation.yealdAverage && parseFloat(valorisation.yealdAverage) > 0) {
        var valorisationForSpeciesCodes = valorisationsBySpeciesCode[valorisation.speciesCode];
        if (!valorisationForSpeciesCodes) {
          valorisationForSpeciesCodes = [];
          valorisationsBySpeciesCode[valorisation.speciesCode] = valorisationForSpeciesCodes;
        }
        valorisationForSpeciesCodes.push(valorisation);
      }
    };

    $scope._getValorisationsBySpecies = function (migratedAction, destinationInfos, matchingSpeciesForCodeEspeceBotaniqueCodeQualifiant) {
      var valorisationsBySpeciesCode = {};
      angular.forEach(migratedAction.valorisationDtos, function (valorisation) {
        $scope._groupValorisationBySpecies(valorisation, valorisationsBySpeciesCode);
      });
      return valorisationsBySpeciesCode;
    };

    $scope._getNewValorisationId = function () {
      return "NEW-HARVESTING-VALORISATION-" + guid();
    };

    $scope._copyToNewValorisation = function (valorisation) {
      var newValorisation = angular.copy(valorisation);
      newValorisation.topiaId = this._getNewValorisationId();
      return newValorisation;
    };

    $scope._getSpeciesLibelle = function (speciesWithSameCodeEspeceBotanique) {
      var speciesLibelle;
      if (speciesWithSameCodeEspeceBotanique.species && speciesWithSameCodeEspeceBotanique.species.espece) {
        speciesLibelle = speciesWithSameCodeEspeceBotanique.species.espece;
      } else if (speciesWithSameCodeEspeceBotanique.speciesEspece) {
        speciesLibelle = speciesWithSameCodeEspeceBotanique.speciesEspece;
      } else {
        speciesLibelle = "";
      }
      return speciesLibelle;
    };

    $scope._getVarietyLibelle = function (speciesWithSameCodeEspeceBotanique) {
      var varietyLibelle;
      if (speciesWithSameCodeEspeceBotanique.variety && speciesWithSameCodeEspeceBotanique.variety.variete) {
        varietyLibelle = "(" + speciesWithSameCodeEspeceBotanique.variety.variete + ")";
      } else if (speciesWithSameCodeEspeceBotanique.varietyLibelle) {
        varietyLibelle = "(" + speciesWithSameCodeEspeceBotanique.varietyLibelle + ")";
      } else {
        varietyLibelle = "";
      }
      return varietyLibelle;
    };

    $scope._addToWineValorisationsIfWineSpecies = function (speciesWithSameCodeEspeceBotanique, wineValorisations, newValorisation) {
      // réalisé || synthétisé
      if (speciesWithSameCodeEspeceBotanique) {
        var code_espece_botanique = speciesWithSameCodeEspeceBotanique.species ?
          speciesWithSameCodeEspeceBotanique.species.code_espece_botanique :
          speciesWithSameCodeEspeceBotanique.code_espece_botanique;
        if (code_espece_botanique === "ZMO") {
          wineValorisations.push(newValorisation.destination.wineValorisation);
        }
      }
    };

    $scope._setSpeciesToValorisation = function (valorisation, targetedSpecies) {
      var valorisationForTargetedSpecies = $scope._copyToNewValorisation(valorisation);
      valorisationForTargetedSpecies.speciesCode = targetedSpecies.code;
      var speciesLibelle = $scope._getSpeciesLibelle(targetedSpecies);
      var varietyLibelle = $scope._getVarietyLibelle(targetedSpecies);
      var displayName = speciesLibelle + varietyLibelle;
      valorisationForTargetedSpecies.displayName = displayName;
      return valorisationForTargetedSpecies;
    };

    $scope._getTargetedSpeciesCodeToAddValorisationsFromTargetedSpeciesStades = function (speciesStadesDtos) {
      var targetedSpeciesToAddValorisation = [];
      angular.forEach(speciesStadesDtos, function (speciesStade) {
        targetedSpeciesToAddValorisation.push(speciesStade.speciesCode);
      });
      return targetedSpeciesToAddValorisation;
    };

    $scope._getSpeciesByCodes = function (targetedSpecies) {
      var targetedSpeciesByCodes = {};
      angular.forEach(targetedSpecies, function (species) {
        targetedSpeciesByCodes[species.code] = species;
      });
      return targetedSpeciesByCodes;
    };

    $scope._getValorisationByDestinationAndComputeGlobalAverage = function (
      fromMatchingSpeciesCodes, valorisationsBySpeciesCode, matchingSpeciesForCodeEspeceBotaniqueCodeQualifiant) {

      var globalYealdAverageByDestination = {};
      var valorisationsByDestination = {};

      angular.forEach(fromMatchingSpeciesCodes, function (mactchingSpeciesCode) {
        var valorisationForSpecies = valorisationsBySpeciesCode[mactchingSpeciesCode];
        for (var i = 0; i < valorisationForSpecies.length; i++) {
          var valorisation = valorisationForSpecies[i];

          if (!valorisation.yealdAverage || parseFloat(valorisation.yealdAverage) === 0.0) {
            // this valorisation does not target it's species
            continue;
          }

          var globalYealdAverage = globalYealdAverageByDestination[valorisation.destination.uiId];
          globalYealdAverageByDestination[valorisation.destination.uiId] = globalYealdAverage ? globalYealdAverage + valorisation.yealdAverage : valorisation.yealdAverage;

          var speciesWithSameCodeEspeceBotaniques = valorisationsBySpeciesCode[valorisation.speciesCode];

          if (speciesWithSameCodeEspeceBotaniques) {
            var valorisationsForDestination = valorisationsByDestination[valorisation.destination.uiId];
            if (!valorisationsForDestination) {
              valorisationsForDestination = [];
              valorisationsByDestination[valorisation.destination.uiId] = valorisationsForDestination;
            }
            if (valorisationsForDestination.indexOf(valorisation) === -1) {
              valorisationsForDestination.push(valorisation);
            }
          }

        }
      });
      return {
        globalYealdAverageByDestination: globalYealdAverageByDestination,
        valorisationsByDestination: valorisationsByDestination
      };

    };

    $scope._computeValorisationsToOneValorisation = function (valorisations) {
      var newValorisation;
      if (valorisations) {
        newValorisation = angular.copy(valorisations[0]);
        newValorisation.yealdAverage = 0;
        newValorisation.yealdMin = 1000;// only the smaller yealdmin will be save
        newValorisation.yealdMax = 0;
        newValorisation.yealdMedian = 0;
        newValorisation.selfConsumedPersent = 0;
        newValorisation.salesPercent = 0;
        newValorisation.qualityCriteriaDtos = [];
        var qualityCriteriaByRefQualityCriteriaId = {};

        angular.forEach(valorisations, function (valorisation) {
          newValorisation.yealdAverage += !!valorisation.yealdAverage ? valorisation.yealdAverage : 0;
          newValorisation.yealdMin += !!valorisation.yealdMin ? (newValorisation.yealdMin < valorisation.yealdMin ? newValorisation.yealdMin : valorisation.yealdMin) : newValorisation.yealdMin;
          newValorisation.yealdMax += !!valorisation.yealdMax ? (newValorisation.yealdMax > valorisation.yealdMax ? newValorisation.yealdMax : valorisation.yealdMax) : newValorisation.yealdMax;
          newValorisation.yealdMedian = !!valorisation.yealdMedian ? valorisation.yealdMedian : 0;
          newValorisation.selfConsumedPersent += !!valorisation.selfConsumedPersent ? valorisation.selfConsumedPersent : 0;
          newValorisation.salesPercent += !!valorisation.salesPercent ? valorisation.salesPercent : 0;
          if (valorisation.qualityCriteriaDtos) {
            angular.forEach(valorisation.qualityCriteriaDtos, function (qualityCriteria) {
              var qc = qualityCriteriaByRefQualityCriteriaId[qualityCriteria.refQualityCriteriaId];
              if (!qc) {
                qualityCriteriaByRefQualityCriteriaId[qualityCriteria.refQualityCriteriaId] = qualityCriteria;
              }
            });
          }
        });

        newValorisation.qualityCriteriaDtos = Object.values(qualityCriteriaByRefQualityCriteriaId);
      }
      return newValorisation;
    };

    $scope._getValorizedValorisations = function (valorisationsForDestination) {
      var result = [];
      if (valorisationsForDestination) {
        angular.forEach(valorisationsForDestination, function (valorisation) {
          if (valorisation.yealdAverage && parseFloat(valorisation.yealdAverage) > 0.0) {
            result.push(valorisation);
          }
        });
      }
      return result;
    };

    $scope._distributeValorisationsToSpecies = function (targetedSpeciesCodeToAddValorisation,
      targetedSpeciesByCodes,
      valorisationsByDestination,
      globalYealdAverageByDestination,
      wineValorisations,
      migratedSpeciesDestinationValorisation,
      valorisationsBySpeciesCode) {
      angular.forEach(targetedSpeciesCodeToAddValorisation, function (targetedSpeciesCode) {
        var tagetedSpecies = targetedSpeciesByCodes[targetedSpeciesCode];
        if (tagetedSpecies) {

          angular.forEach(valorisationsByDestination, function (valorisationsForDestination, destinationId) {
            //var valorisation = valorisationsForDestination[0];

            var valorisation = $scope._computeValorisationsToOneValorisation($scope._getValorizedValorisations(valorisationsForDestination));
            var speciesWithSameCodeEspeceBotanique = valorisationsBySpeciesCode[valorisation.speciesCode];

            var valorisationForTargetedSpecies = null;
            for (var i = 0; i < speciesWithSameCodeEspeceBotanique.length; i++) {
              var species = speciesWithSameCodeEspeceBotanique[i];
              if (species) {
                valorisationForTargetedSpecies = $scope._setSpeciesToValorisation(valorisation, tagetedSpecies);
                var globalValorisation = globalYealdAverageByDestination[valorisation.destination.uiId];
                valorisationForTargetedSpecies.yealdAverage = globalValorisation / speciesWithSameCodeEspeceBotanique.length;
                $scope._addToWineValorisationsIfWineSpecies(tagetedSpecies, wineValorisations, valorisationForTargetedSpecies);
                migratedSpeciesDestinationValorisation.push(valorisationForTargetedSpecies);
                break;
              }
            }

            if (valorisationForTargetedSpecies === null) {
              valorisationForTargetedSpecies = $scope._setSpeciesToValorisation(valorisation, tagetedSpecies);
              valorisationForTargetedSpecies.yealdAverage = 0.0;
              migratedSpeciesDestinationValorisation.push(valorisationForTargetedSpecies);
            }

          });

        }

      });
    };

    $scope._filterQualityCriteriaForSectorCodeEspeceBotaniqueCodeQualifant = function (valorisation, sectors, targetedSpecies) {
      var allQualityCriteria = valorisation.qualityCriteriaDtos;
      if (allQualityCriteria) {
        var validateAllQualityCriteria = [];

        // practiced effective
        var targetedCode_espece_botanique = targetedSpecies.code_espece_botanique || targetedSpecies.species && targetedSpecies.species.code_espece_botanique;
        var targetedCode_qualifiant_AEE = targetedSpecies.code_qualifiant_AEE || targetedSpecies.species && targetedSpecies.species.code_qualifiant_AEE;

        angular.forEach(allQualityCriteria, function (qualityCriteria) {
          if (qualityCriteria.refQualityCriteria != undefined) {
            var qualityCriteriaSector = qualityCriteria.refQualityCriteria.sector;
            var qualityCriteriaCodeEspeceBotanique = qualityCriteria.refQualityCriteria.code_espece_botanique;
            var qualityCriteriaCodeQUalifiant = qualityCriteria.refQualityCriteria.code_qualifiant_AEE;

            var valid = sectors.indexOf(qualityCriteriaSector) !== -1;
            if (valid && qualityCriteriaCodeEspeceBotanique) {
              valid = targetedCode_espece_botanique === qualityCriteriaCodeEspeceBotanique;
            }
            if (valid && qualityCriteriaCodeEspeceBotanique && qualityCriteriaCodeQUalifiant) {
              valid = targetedCode_qualifiant_AEE === qualityCriteriaCodeQUalifiant;
            }
            if (valid) {
              validateAllQualityCriteria.push(qualityCriteria);
            }
          }
        });
        valorisation.qualityCriteriaDtos = validateAllQualityCriteria;
      }
    };

    $scope._getSectorsForSpecies = function (species, allSectorsByCodeEspeceBotaniqueCodeQualifiant) {
      // effective/practiced
      species = species.species || species;
      var codeEspeceBotanique = species.code_espece_botanique;
      var codeQualifant = species.code_qualifiant_AEE;
      var key = codeEspeceBotanique + "_" + (codeQualifant || "null");
      var sectors = allSectorsByCodeEspeceBotaniqueCodeQualifiant[key];
      return sectors;
    };

    $scope._isValorisationTargetingSpecies = function (valorisation, species, sectors) {

      // si le rendement de la valorisation est à 0 cela signifie que la destination n'était pas destinée à cette espèce
      var valid = (valorisation.yealdAverage && (parseFloat(valorisation.yealdAverage.toFixed(3))) > 0.0) ? 1 : 0;
      valid &= sectors && sectors.indexOf(valorisation.destination.sector) !== -1;
      if (valid && valorisation.destination.code_espece_botanique) {
        // effective practiced
        var speciesCodeEspeceBotanique = species.species ? species.species.code_espece_botanique : species.code_espece_botanique;
        valid = speciesCodeEspeceBotanique === valorisation.destination.code_espece_botanique;
      }
      if (valid && valorisation.destination.code_espece_botanique && valorisation.destination.code_qualifiant_AEE) {
        // effective practiced
        var speciesCodeQualifiantAEE = species.species ? species.species.code_qualifiant_AEE : species.code_qualifiant_AEE;
        valid = speciesCodeQualifiantAEE === valorisation.destination.code_qualifiant_AEE;
      }
      return valid;
    };

    $scope._migrateHarvestingActionToMatchingSpecies = function (intervention, migratedAction, targetedSpecies) {
      // les Destination et Critères de qualités ciblent différents ensembles d'espèce selon leur niveau de précision
      // cas 1: Fillière : toutes les espèces ayant la même flilière
      // cas 2: Fillière + CodeEspeceBotanique : toutes les espèces de la même filière avec le même codeEspeceBotanique
      // cas 3: Fillière + CodeEspèceBotanique + CodeQualifiant : toutes les espèces de la même filière avec le même codeEspeceBotanique, même qualifiant
      var validSpecies = [];
      var validValorisations = [];

      if (!targetedSpecies || targetedSpecies.length === 0) {
        migratedAction.valorisationDtos = validValorisations;
        return;
      }

      var speciesStadeSpeciesCodes = [];
      if (intervention.speciesStadesDtos) {
        intervention.speciesStadesDtos.map(sstade => { speciesStadeSpeciesCodes.push(sstade.speciesCode) });
      }

      var allSectorsByCodeEspeceBotaniqueCodeQualifiant = $scope.allSectorsByCodeEspeceBotaniqueCodeQualifiant;
      var copiedAction = angular.copy(migratedAction);
      $scope._prepareHarvestingActionModel(intervention, copiedAction, targetedSpecies, true);

      angular.forEach(copiedAction.mainValorisations, function (mainValorisation) {
        if (mainValorisation.yealdAverage > 0) {
          var speciesValorisationForMainValorisation = [];
          for (let i = 0; i < targetedSpecies.length; i++) {
            let toSpecies = targetedSpecies[i];

            if (speciesStadeSpeciesCodes.indexOf(toSpecies.code) === -1) continue;

            var sectors = $scope._getSectorsForSpecies(toSpecies, allSectorsByCodeEspeceBotaniqueCodeQualifiant);

            var valid = $scope._isValorisationTargetingSpecies(mainValorisation, toSpecies, sectors);

            if (valid) {
              validSpecies.push(toSpecies);
              $scope._filterQualityCriteriaForSectorCodeEspeceBotaniqueCodeQualifant(mainValorisation, sectors, toSpecies);
              var newValorisation = angular.copy(mainValorisation);
              newValorisation.topiaId = $scope._getNewValorisationId();
              newValorisation.speciesCode = toSpecies.code;
              newValorisation.relativeArea = toSpecies.speciesArea;
              validValorisations.push(newValorisation);
              speciesValorisationForMainValorisation.push(newValorisation);
            }
          };
          var yealdAverageDivider = speciesValorisationForMainValorisation.length;
          angular.forEach(speciesValorisationForMainValorisation, function (speciesValorisation) {
            speciesValorisation.yealdAverage = mainValorisation.yealdAverage / yealdAverageDivider;
          });
        }
      });
      migratedAction.valorisationDtos = validValorisations;
    };

    $scope._addAllWineValorisations = function (wineValorisations, migratedAction) {
      var newWineValorisations = [];
      angular.forEach(wineValorisations, function (wineValorisation) {
        if (newWineValorisations.indexOf(wineValorisation) === -1) {
          newWineValorisations.push(wineValorisation);
        }
      });
      if (migratedAction.wineValorisations) {
        angular.forEach(migratedAction.wineValorisations, function (wineValorisation) {
          if (newWineValorisations.indexOf(wineValorisation) === -1) {
            newWineValorisations.push(wineValorisation);
          }
        });
      }
      return newWineValorisations;
    };

    $scope._migrateHarvestingActionToSpecies = function (intervention, migratedAction, speciesStadesDtos, targetedSpecies, matchingSpeciesBySpeciesCode) {
      if (migratedAction.valorisationDtos) {

        // dans le cas ou les espèces cibles ne correspondent pas exactement aux espèces d'originine des valorisations
        // un traitement particulier doit être fait pour concerver les valorisations qui peuvent être migrées
        var speciesCodes = Object.keys(matchingSpeciesBySpeciesCode);
        var allSpeciesMatch = matchingSpeciesBySpeciesCode && speciesCodes.length > 0 && speciesStadesDtos.length > 0;
        var speciesStadesDtosIndex = 0;
        while (speciesStadesDtosIndex < speciesStadesDtos.length && allSpeciesMatch) {
          var speciesCode = speciesStadesDtos[speciesStadesDtosIndex++].speciesCode;
          allSpeciesMatch = speciesCodes.indexOf(speciesCode) != -1;
        }

        if (!allSpeciesMatch) {

          var wineValorisations = [];
          $scope._migrateHarvestingActionToMatchingSpecies(intervention, migratedAction, targetedSpecies);

          migratedAction.wineValorisations = $scope._addAllWineValorisations(wineValorisations, migratedAction);
        }
      }
    };

    $scope._migrateHarvestingAction = function (isToNewActions, migratedAction, intervention, targetedSpecies, matchingSpeciesBySpeciesCode, result, confirmNeeded) {
      if (isToNewActions) {
        angular.forEach(migratedAction.valorisationDtos, function (valorisation) {
          valorisation.topiaId = $scope._getNewValorisationId();
        });
      }

      $scope._migrateHarvestingActionToSpecies(intervention, migratedAction, intervention.speciesStadesDtos, targetedSpecies, matchingSpeciesBySpeciesCode);

      if (confirmNeeded && Object.keys(result.harvestingActionValorisationsToRemovesByActionIds).length > 0) {
        result.done = $window.confirm($scope.messages.deleteNotMigrateValorisationsConfirm);
      }
    };

    $scope._migrateActions = function (intervention, actions, matchingSpeciesBySpeciesCode, isToNewActions, confirmNeeded, targetedSpecies, toCropCode) {
      var result = $scope._initActionResult();
      if (actions.length > 0) {
        angular.forEach(actions, function (action) {

          var migratedAction = $scope._bindAction(action, isToNewActions);

          // specific treatments for harvesting action
          if (migratedAction.mainActionInterventionAgrosyst === "RECOLTE" && migratedAction.valorisationDtos) {

            $scope._migrateHarvestingAction(isToNewActions, migratedAction, intervention, targetedSpecies, matchingSpeciesBySpeciesCode, result, confirmNeeded);

          }

          if (migratedAction.mainActionInterventionAgrosyst === "SEMIS") {
            var newSeedLotInputUsageDtos = [];
            var seedLotInputUsageDtos = migratedAction.seedLotInputUsageDtos ? migratedAction.seedLotInputUsageDtos : [];
            angular.forEach(seedLotInputUsageDtos, function (usage) {
              if (usage.domainSeedLotInputDto) {
                var valid = toCropCode ? toCropCode === usage.domainSeedLotInputDto.cropSeedDto.code : false;
                if (valid) {
                  newSeedLotInputUsageDtos.push(usage);
                }
              }
            });
            migratedAction.seedLotInputUsageDtos = newSeedLotInputUsageDtos;
          }

          result.actionDtos.push(migratedAction);
          result.actionByIds[action.topiaId] = migratedAction;

        });
      }
      return result;
    };

    $scope._migrateInterventionUsagesAction = function (intervention, originalSpeciesStadesDtos, croppingPlanEntrySpecies, matchingSpeciesBySpeciesCode, isIntermediateCrop, newInterventionActionsResult, toCrop) {
      if (intervention.usages) {
        var speciesStadeMigrationResult = $scope._tryToMigrateSpeciesStades(originalSpeciesStadesDtos, croppingPlanEntrySpecies, matchingSpeciesBySpeciesCode, isIntermediateCrop);
        var forceNewUsage = true;
        var migratedUsages = $scope._migrateUsagesActionAndTargets(newInterventionActionsResult.actionByIds, intervention.usages, speciesStadeMigrationResult, forceNewUsage, toCrop);
        intervention.usages = migratedUsages;
      }
    };

    $scope._addOrReplaceSpeciesStadeToSpeciesStadeBySpeciesCode = function (speciesStades) {
      $scope.speciesStadeBySpeciesCode = {};
      if (speciesStades) {
        angular.forEach(speciesStades, function (speciesStade) {
          $scope.speciesStadeBySpeciesCode[speciesStade.speciesCode] = speciesStade;
        });
      }
    };

    $scope._createAndPushSpeciesStade = function (speciesStades, species, allowCompagneSpecies) {
      if (allowCompagneSpecies || species.compagne === undefined) {// DO not add compagne species as default
        var speciesStade = {speciesCode : species.code};
        speciesStades.push(speciesStade);
      }
    };

    $scope._createMissingSeedLotSpecies = function(intervention, species) {
      var actionDtos = intervention.actionDtos ? intervention.actionDtos : [];
      var seedingActionDtos = actionDtos.filter(
        actionDto => actionDto.seedLotInputUsageDtos && actionDto.seedLotInputUsageDtos.length > 0
      )
      if (seedingActionDtos.length > 0) {
        seedingActionDtos.map(
          seedingActionDto => {
            seedingActionDto.seedLotInputUsageDtos.map(
              seedLotInputUsageDto => {
                var usageUnit = seedLotInputUsageDto.domainSeedLotInputDto.usageUnit;
                var domainSpeciesInputs = seedLotInputUsageDto.domainSeedLotInputDto.speciesInputs;
                var domainSpeciesInput = domainSpeciesInputs.filter(
                  domainSpeciesInput => domainSpeciesInput.speciesSeedDto.code === species.code
                )[0];
                if (domainSpeciesInput) {
                  var seedingSpeciesDto = seedLotInputUsageDto.seedingSpeciesDtos.filter(
                    seedingSpeciesDto => seedingSpeciesDto.domainSeedSpeciesInputDto.speciesSeedDto.code === species.code
                  )[0];
                  if (!seedingSpeciesDto) {
                    seedLotInputUsageDto.seedingSpeciesDtos.push(
                      {
                        domainSeedSpeciesInputDto : domainSpeciesInput,
                        inputType: "SEMIS",
                        productName: domainSpeciesInput.inputName,
                        speciesIdentifier: ($scope.practicedSystem ? domainSpeciesInput.speciesSeedDto.code : domainSpeciesInput.speciesSeedDto.topiaId),
                        usageUnit : usageUnit
                      }
                    );
                  }
                }
              }
            )
          }
        );
      }
    };

    $scope._createSpeciesStadesFromSpecies = function (croppingPlanEntrySpecies, allowCompagneSpecies) {
      var speciesStades = [];
      if (croppingPlanEntrySpecies) {
        angular.forEach(croppingPlanEntrySpecies, function (species) {
          $scope._createAndPushSpeciesStade(speciesStades, species, allowCompagneSpecies);
        });
      }
      return speciesStades;
    };

    $scope._propagateCropChangeOnIntervention = function (isIntermediateCrop, interventions, croppingPlanEntrySpecies, matchingSpeciesBySpeciesCode) {
      $scope._loadCopyPasteGlobalInfo(interventions)
        .then(function () {
          angular.forEach(interventions, function (intervention) {

            // creation des species stades
            var originalSpeciesStadesDtos = angular.copy(intervention.speciesStadesDtos);
            intervention.speciesStadesDtos = $scope._createSpeciesStadesFromSpecies(croppingPlanEntrySpecies, false);

            intervention.intermediateCrop = isIntermediateCrop;

            // for seeding and harvesting Action, species has to be updated
            var confirmNeeded = true;
            var toNewActions = true;
            var toCrop = null; // TODO
            var targetedSpecies = !croppingPlanEntrySpecies ? [] : Object.values(croppingPlanEntrySpecies);
            var newInterventionActionsResult = $scope._migrateActions(
              intervention,
              intervention.actionDtos,
              matchingSpeciesBySpeciesCode,
              toNewActions,
              confirmNeeded,
              targetedSpecies,
              toCrop);// TODO

            intervention.actionDtos = newInterventionActionsResult.actionDtos;

            $scope._migrateInterventionUsagesAction(
              intervention,
              originalSpeciesStadesDtos,
              targetedSpecies,
              matchingSpeciesBySpeciesCode,
              isIntermediateCrop,
              newInterventionActionsResult);

          });
        });
    };

    // ATTENTION : déclarée sur la window, utilisée en global
    window._removeConnectionImpact = function(cropCycleConnection) {
      var messages;
      if (cropCycleConnection) {
        var interventions= _getInterventionsRelatedToConnection(cropCycleConnection);
        if (interventions) {
          var ngInt = interventions.length;

          if (ngInt > 0) {

            var singMes = $scope.messages.interventionDeleted;
            var plurMes = ngInt + " " + $scope.messages.interventionsDeleted;

            messages = (ngInt === 1 ? singMes : plurMes) + "\n\n";
          }
        }
      }
      return messages;
    };

    $scope._getCroppingPlanSpeciesByCode = function (croppingPlanEntrySpecies) {
      var croppingPlanEntrySpeciesByCode;
      if (croppingPlanEntrySpecies && croppingPlanEntrySpecies.length > 0) {
        croppingPlanEntrySpeciesByCode = {};
        angular.forEach(croppingPlanEntrySpecies, function (species) {
          croppingPlanEntrySpeciesByCode[species.code] = species;
        });
      }
      return croppingPlanEntrySpeciesByCode;
    };

    // from seasonal crop cycle, when adding or removing intermediate crop from connection
    // ATTENTION : déclarée sur la window, utilisée en global
    window._doConnectionIntermediateCropChange = function (cropCycleConnection, isFromIntermediateCrop, isToIntermediateCrop, matchingSpeciesBySpeciesCode) {

      // species to set on interventions
      var croppingPlanEntrySpecies = _getCroppingPlanEntrySpeciesFromCropCycleConnection(cropCycleConnection);
      var interventionResults = _getCropCycleConnectionInterventions(cropCycleConnection);

      var ngInt = interventionResults.affected.length;
      if (ngInt > 0) {
        var croppingPlanEntrySpeciesByCode = $scope._getCroppingPlanSpeciesByCode(croppingPlanEntrySpecies);
        $scope._propagateCropChangeOnIntervention(isToIntermediateCrop, interventionResults.affected, croppingPlanEntrySpeciesByCode, matchingSpeciesBySpeciesCode);
      }
    };

    // ATTENTION : déclarée sur la window, utilisée en global
    window._getConnectionIntermediateCropChangeImpact = function (cropCycleConnection, isFromIntermediateCrop, isToIntermediateCrop, matchingSpeciesBySpeciesCode) {

      var messages;

      var interventionResults = _getCropCycleConnectionInterventions(cropCycleConnection);

      var ngInt = interventionResults.affected.length;
      if (ngInt === 1) {
        messages = (isFromIntermediateCrop ? $scope.messages.connectionCropChangeImpactFromIntermediateOne : $scope.messages.connectionCropChangeImpactFromMainOne) + "<br>"
                 + (isToIntermediateCrop ? $scope.messages.connectionCropChangeImpactToIntermediateOne : $scope.messages.connectionCropChangeImpactToMainOne) + "<br><br>";
      } else if (ngInt > 0) {
        messages = (isFromIntermediateCrop ? $scope.messages.connectionCropChangeImpactFromIntermediateSeveral : $scope.messages.connectionCropChangeImpactFromMainSeveral).replaceAll('{}', ngInt) + "<br>"
                 + (isToIntermediateCrop ? $scope.messages.connectionCropChangeImpactToIntermediateSeveral : $scope.messages.connectionCropChangeImpactToMainSeveral) + "<br><br>";
      }
      return messages;
    };

    $scope._removeUsageDoses = function (matchingSpeciesBySpeciesCode, fromSpecies, intervention) {
      var nbMatchingSpecies = matchingSpeciesBySpeciesCode ? Object.keys(matchingSpeciesBySpeciesCode).length : 0;

      if (fromSpecies.length != nbMatchingSpecies.length) {
        angular.forEach(intervention.usages, function (usage) {
          if (usage.biologicalControlAction || usage.pesticidesSpreadingAction) {
            delete usage.qtMin;
            delete usage.qtAvg;
            delete usage.qtMed;
            delete usage.qtMax;
            delete usage.phytoProductUnit;
          }
        });
      }
    };

    $scope._loadInterventionDestinationContext = function (intervention) {
      angular.forEach(intervention.actionDtos, function (action) {
        if (action.mainActionInterventionAgrosyst === "RECOLTE") {
          var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
          var rollbackContext = null;
          $scope._loadDestinationsContext(speciesStadeSpecies, intervention, action, rollbackContext);
        }
      });
    };

    // cette methode ajoute automatiquement les espèces d'une culture intermédiaire
    // car l'utilisateur n'ap pas la main sur la sélection des espèces d'une CI
    // et q'une espèce a pu être ajouté à la culture après création de l'intervention
    $scope._autoAddIntermediateSpeciesStades = function(intervention) {
      var targetedSpecies;

      if (intervention.intermediateCrop) {
        targetedSpecies = $scope.connectionIntermediateSpecies ? $scope.connectionIntermediateSpecies : [];
      }

      intervention.speciesStadesDtos = intervention.speciesStadesDtos ? intervention.speciesStadesDtos : [];

      if (targetedSpecies && targetedSpecies.length > 0) {
        targetedSpecies.map(species => {
          var speciesStade = intervention.speciesStadesDtos.filter(
            speciesStade => speciesStade.speciesCode == species.code
          )
          if(speciesStade.length === 0) {

            $scope._createAndPushSpeciesStade(intervention.speciesStadesDtos, species, false);
            $scope._createMissingSeedLotSpecies(intervention, species)
          }
        });
      }

    };

    $scope.toggleIntermediateSpeciesSelection = function() {
      var originalIntervention = angular.copy($scope.editedIntervention);
      var originalSpeciesStadeBySpeciesCode = angular.copy($scope.speciesStadeBySpeciesCode);
      var originalSpeciesStadesDtos = angular.copy($scope.editedIntervention.speciesStadesDtos);

      $scope.editedIntervention.speciesStadesDtos = [];
      $scope.speciesStadeBySpeciesCode = {};

      var targetedSpecies;
      var fromSpecies;
      var fromCrop;
      var toCrop;

      if ($scope.editedIntervention.intermediateCrop) {
        targetedSpecies = $scope.connectionIntermediateSpecies ? $scope.connectionIntermediateSpecies : [];
        fromSpecies = $scope.phaseConnectionOrNodeSpecies;
        fromCrop = $scope.phaseOrNodeCrop;
        toCrop = $scope.connectionIntermediateCrop;
      } else {
        targetedSpecies = $scope.phaseConnectionOrNodeSpecies ? $scope.phaseConnectionOrNodeSpecies : [];
        fromSpecies = $scope.connectionIntermediateSpecies;
        fromCrop = $scope.connectionIntermediateCrop;
        toCrop = $scope.phaseOrNodeCrop;
      }

      $scope.editedIntervention.speciesStadesDtos = $scope._createSpeciesStadesFromSpecies(targetedSpecies, $scope.editedIntervention.intermediateCrop);
      $scope._addOrReplaceSpeciesStadeToSpeciesStadeBySpeciesCode($scope.editedIntervention.speciesStadesDtos);

      var nextFunction = {
        name: 'TOGGLE_INTERMEDIATE_SPECIES',
        fromCrop: fromCrop,
        toCrop: toCrop,
        fromSpecies: fromSpecies,
        targetedSpecies: targetedSpecies,
        process: function() {
          // migrate actions
          var confirmNeeded = true;
          var toNewActions = false;
          var matchingSpeciesBySpeciesCode = _findMatchingSpecies(fromSpecies, targetedSpecies);

          var newInterventionActionsResult = $scope._migrateActions(
            $scope.editedIntervention,
            $scope.editedIntervention.actionDtos,
            matchingSpeciesBySpeciesCode,
            toNewActions,
            confirmNeeded,
            targetedSpecies,
            toCrop.code);

          $scope.editedIntervention.actionDtos = newInterventionActionsResult.actionDtos;

          $scope._migrateInterventionUsagesAction(
            $scope.editedIntervention,
            originalSpeciesStadesDtos,
            targetedSpecies,
            matchingSpeciesBySpeciesCode,
            $scope.editedIntervention.intermediateCrop,
            newInterventionActionsResult,
            toCrop);

          $scope._loadInterventionDestinationContext($scope.editedIntervention);
        }
      };

      $scope._loadDestinationsContext(targetedSpecies, $scope.editedIntervention, null, null, nextFunction);

    };

    // For praticed System
    // clic sur une ligne du tableau pour editer la liste des interventions
    $scope.editPhaseOrConnection = function (phaseOrConnection) {
      if(!$scope.editedInterventionForm.$valid) {
        // do not close if not valid
        return;
      }

      $scope.setSelectedIntervention();
      delete $scope.editedIntervention; // hide selected intervention
      $('#new-intervention').hide();
      $scope.selectedPhaseConnectionOrNode = phaseOrConnection;

      // initialisation de la liste des espèces correspondant au croppingPlanEntryCode
      // du noeud ou de la connexion sélectionnée
      var croppingPlanEntryCode = null;
      $scope.connectionIntermediateCrop = null;

      angular.forEach($scope.phaseOrConnectionCroppingPlanEntryCodes, function (phaseOrConnectionCroppingPlanEntryCode) {
        if (phaseOrConnectionCroppingPlanEntryCode.key === phaseOrConnection) {
          croppingPlanEntryCode = phaseOrConnectionCroppingPlanEntryCode.value;
          var intermediateCropCode;
          if (phaseOrConnection.intermediateCroppingPlanEntryCode) {
            intermediateCropCode = phaseOrConnection.intermediateCroppingPlanEntryCode;
          }
          $scope.phaseConnectionOrNodeSpecies = $scope.croppingPlanEntrySpeciesIndex[croppingPlanEntryCode];

          $scope.connectionIntermediateSpecies = $scope.croppingPlanEntrySpeciesIndex[intermediateCropCode];
          // s'il n'y a pas espèces intermédiaires alors les interventions ne peuvent être affectée à une culture intermédiaire.
          if (!intermediateCropCode) {
            angular.forEach($scope.selectedPhaseConnectionOrNode.interventions, function (intervention) {
              intervention.intermediateCrop = false;
              $scope._addInterventionProportionOfTreatedSurface(intervention);
            });
          } else {
            $scope.connectionIntermediateCrop = $scope.getCroppingPlanEntry(intermediateCropCode);
          }

          // set speciesByCode
          $scope.allSpeciesByCode = {};
          if ($scope.phaseConnectionOrNodeSpecies) {
            angular.forEach($scope.phaseConnectionOrNodeSpecies, function (species) {
              $scope.allSpeciesByCode[species.code] = species;
            });
          }
          if ($scope.connectionIntermediateSpecies) {
            angular.forEach($scope.connectionIntermediateSpecies, function (species) {
              $scope.allSpeciesByCode[species.code] = species;
            });
          }
        }
      });

      $scope.phaseOrNodeCrop = $scope.getCroppingPlanEntry(croppingPlanEntryCode);

      // reinitialisation de l'objet de selection
      $scope.selectedInterventions = {};
      $scope.pastedInterventions = !!$store.get($scope.prefix + "-interventions-" + $scope.domainId);
    };

    // also called by inheritance
    $scope.setSelectedIntervention = function (selectedIntervention) {
      $scope.allInterventionSelected = { selected: false };
      $scope.selectedInterventions = selectedIntervention || {};
    };


    /*********************************************/
    /*               Interventions               */
    /*********************************************/

    $scope.allInterventionSelected = { selected: false };
    $scope.toggleSelectedInterventions = function () {

      if ($scope.allInterventionSelected.selected) {
        angular.forEach($scope.selectedPhaseConnectionOrNode.interventions, function (intervention) {
          if (intervention.topiaId) {
            $scope.selectedInterventions[intervention.topiaId] = true;
          }
        });
      } else {
        $scope.selectedInterventions = {};
      }

    };

    $scope._getEditedInterventionHarvestingAction = function () {
      var result = null;
      if ($scope.actionsByAgrosystInterventionType) {
        var actions = $scope.actionsByAgrosystInterventionType.RECOLTE;
        if (actions && actions.length > 0) {
          // one harvesting action is allowed by intervention
          result = actions[0];
        }
      }
      return result;
    };

    $scope.$on('destinationContextLoaded', function (event, loadDestinationsContext, rollbackContext, functionToBeDoneNextToLoad, action) {
      if (!action) {
        action = $scope._getEditedInterventionHarvestingAction();
      }
      $scope._addLoadDestinationsContextResultsToAction(loadDestinationsContext, $scope.editedIntervention, action, rollbackContext);

      if (functionToBeDoneNextToLoad) {

        functionToBeDoneNextToLoad.process($scope.editedIntervention, action);

      }

    });

    $scope._getLoadDestinationQuery = function (action, speciesStadeSpecies) {
      var speciesCodes = [];

      if (action) {
        // check if species are wine species
        var speciesBySpeciesCodesAndByCodeEspeceBotaniqueCodeQualifant = $scope._getSpeciesStadeBySpeciesCodeAndByCodeEspeceBotanique(speciesStadeSpecies);
        var speciesByCodeEspeceBotanique = speciesBySpeciesCodesAndByCodeEspeceBotaniqueCodeQualifant.speciesByCodeEspeceBotanique;

        action.isWineSpecies = $scope._doesWineSpeciesPresentFromSpeciesStades(speciesByCodeEspeceBotanique);
        action.wineValorisations = action.isWineSpecies ? action.wineValorisations : [];
        action.selectedWineValorisations = action.isWineSpecies ? action.selectedWineValorisations : {};
      }

      var wineValorisations = action && action.wineValorisations && action.wineValorisations.length > 0 ? action.wineValorisations : null;

      angular.forEach(speciesStadeSpecies, function (species) {
        // add species code to compute distribution
        speciesCodes.push(species.code);
      });

      var query = $scope.getItkIdentifierQuery() + "&speciesCodes=" + angular.toJson(speciesCodes);
      if (wineValorisations) {
        query += "&wineValorisations=" + angular.toJson(wineValorisations);
      }

      return query;
    };

    // action may be null
    $scope._loadDestinationsContext = function (speciesStadeSpecies, intervention, action, rollbackContext, functionToBeDoneNextToLoad) {

      $scope.$broadcast('loadingDestinationContext');
      displayPageLoading();
      $scope.loadDestinationsContext = null;
      var query = $scope._getLoadDestinationQuery(action, speciesStadeSpecies);

      $http.post(
        ENDPOINTS.loadDestinationsContextJson, query,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
      ).then(function (response) {
        if (response.data != "null") {
          $scope.loadDestinationsContext = response.data;
          intervention.destinationContext = response.data;
          $scope.allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = response.data.allCodeEspeceBotaniqueCodeQualifantBySpeciesCode;
          $scope.allSectorsByCodeEspeceBotaniqueCodeQualifiant = response.data.allSectorByCodeEspeceBotaniqueCodeQualifiant;

          $scope.$broadcast('destinationContextLoaded', $scope.loadDestinationsContext, rollbackContext, functionToBeDoneNextToLoad, action);
          hidePageLoading();
        } else {
          hidePageLoading();
          var message = $scope.messages.loadDestinationsContextDataError;
          addPermanentError(message, status);
        }
      }
      ).catch(function (response) {
        var message = $scope.messages.loadDestinationsContextError;
        addPermanentError(message, response.status);
        console.error(message, response);
        hidePageLoading();
      });
    };

    $scope.filterQualityCriteriaOnSpecies = function (action, valorisation) {
      var allQualityCriteriaForValorisation = {};
      if (valorisation.destination) {

        var refQualityCriteriaIds = [];

        angular.forEach(valorisation.qualityCriteriaDtos, function (criteria) {
          refQualityCriteriaIds.push(criteria.refQualityCriteriaId);
        });

        var code_espece_botanique;
        var code_qualifiant_AEE;
        if (valorisation.main) {
          var destination = valorisation.destination;
          code_espece_botanique = destination.code_espece_botanique;
          code_qualifiant_AEE = destination.code_qualifiant_AEE;
        } else {
          var species = action.speciesByCodes.speciesByCode[valorisation.speciesCode];
          code_espece_botanique = species && species.code_espece_botanique;
          code_qualifiant_AEE = species && species.code_qualifiant_AEE;
        }
        angular.forEach(action.qualityCriteria, function (qc) {
          if (refQualityCriteriaIds.indexOf(qc.topiaId) == -1 && qc.sector === valorisation.destination.sector &&
            (!qc.code_espece_botanique || !code_espece_botanique || qc.code_espece_botanique === code_espece_botanique) &&
            (!qc.code_qualifiant_AEE || !code_qualifiant_AEE || qc.code_qualifiant_AEE === code_qualifiant_AEE)
          ) {
            allQualityCriteriaForValorisation[qc.topiaId] = qc;
          }
        });
      }
      return allQualityCriteriaForValorisation;
    };

    $scope.setDestination = function (valorisation, action) {
      var destination = valorisation.destinationsYealdUnits[valorisation.yealdUnit];
      $scope._setValorisationDestination(destination, valorisation, action);
    };

    $scope._autoSelectDestination = function (valorisation, action) {
      var units = Object.keys(valorisation.destinationsYealdUnits);
      var nbUnitsAvalaibles = units.length;
      var valorisationYealdUnit = valorisation.yealdUnit;

      // do not change anything if there are no units available to don't lost details
      valorisation.yealdUnit = nbUnitsAvalaibles > 0 ? null : valorisation.yealdUnit;

      // auto-select unique units
      if (nbUnitsAvalaibles === 1) {
        valorisation.yealdUnit = units[0];
        $scope.setDestination(valorisation, action);
      }

      // use same units if possible
      if (nbUnitsAvalaibles > 1 && valorisation.destination && units.indexOf(valorisationYealdUnit) !== -1) {
        var index = units.indexOf(valorisationYealdUnit);
        valorisation.yealdUnit = units[index];
        $scope.setDestination(valorisation, action);
      }

      valorisation.isSelectUnitsEnable = units.length > 1;
    };

    $scope._filterYealdUnitsOnDestinations = function (valorisation, destinationsByLabel, action) {
      $scope._setDestinationYealdUnits(valorisation, destinationsByLabel);
      $scope._autoSelectDestination(valorisation, action);
    };

    $scope.isDisabledDestination = function (destination, action, valorisation) {
      return destination === 'A compléter' ||
        (!valorisation.destination || valorisation.destination.destination_Translated != destination) &&
        Object.values(action.mainValorisations).map(v => v.destination ? v.destination.destination_Translated : null).includes(destination);
    };

    $scope.setValorisationDestination = function (valorisation, action) {
      if (valorisation.main) {
        var selectedDestinations = valorisation.selectedDestinations;
        if (selectedDestinations && selectedDestinations.length === 1) {
          var selectedDestination = selectedDestinations[0];
          $scope._setValorisationDestination(selectedDestination, valorisation, action);

        } else if (!selectedDestinations && valorisation.destination && action.destinationsByDestinationLabels) {
          valorisation.selectedDestinations = action.destinationsByDestinationLabels[valorisation.destination.destination_Translated];
          selectedDestinations = valorisation.selectedDestinations;
        }
        $scope._filterYealdUnitsOnDestinations(valorisation, selectedDestinations, action);
      }
    };

    $scope._setValorisationDestination = function (selectedDestination, valorisation, action) {
      angular.forEach(action.speciesValorisations, v => {
        if (!v.destination && !valorisation.destination ||
          v.destination && valorisation.destination && v.destination.uiId == valorisation.destination.uiId) {
          v.destination = selectedDestination;
          v.destinationName = v.destination ? v.destination.destination_Translated : "";
        }
      });
      delete action.mainValorisations[valorisation.destination ? valorisation.destination.uiId : null];
      action.mainValorisations[selectedDestination.uiId] = valorisation;

      valorisation.destination = selectedDestination;
      valorisation.destinationId = selectedDestination.uiId;
      valorisation.destinationName = valorisation.destination ? valorisation.destination.destination_Translated : "";

      if (valorisation.yealdUnit && valorisation.yealdUnit !== valorisation.destination.yealdUnit) {
        valorisation.previousYealdUnit = valorisation.yealdUnit;
        $scope.destinationUnitChangeMessage = $scope.messages.destinationUnitChange.replace('{0}', I18N.YealdUnit[valorisation.yealdUnit]).replace('{1}', I18N.YealdUnit[valorisation.destination.yealdUnit]);
        _displayInfoDialog($scope, $("#destinationUnitChange"), 1024, "", { process: function () { } });
      }
      valorisation.yealdUnit = valorisation.destination.yealdUnit;

      valorisation.allQualityCriteriaForValorisation = $scope.filterQualityCriteriaOnSpecies(action, valorisation);
      valorisation.nbQualityCriteriaForValorisation = Object.keys(valorisation.allQualityCriteriaForValorisation).length;
    };

    $scope.getAutoSelectedUniqueDestination = function (action) {
      var autoSelectedDestination;
      var autoSelectedDestinations = action.destinationsLength === 1 ? action.destinationsByDestinationLabels[Object.keys(action.destinationsByDestinationLabels)[0]] : null;
      if (autoSelectedDestinations && autoSelectedDestinations.length === 1) {
        autoSelectedDestination = autoSelectedDestinations[0];
      }
      return autoSelectedDestination;
    };

    $scope._tryFindSameDestination = function (mainValorisation, action) {
      var sameDestination;
      var destinationsWithSameLabels = mainValorisation.destination && action.destinationsLength > 0 ?
        action.destinationsByDestinationLabels[mainValorisation.destination.destination_Translated] ? action.destinationsByDestinationLabels[mainValorisation.destination.destination_Translated] : [] : [];
      if (destinationsWithSameLabels.length > 0) {
        if (mainValorisation.destination.destination_Translated === $scope.defaultDestinationName) {
          for (var i = 0; i < destinationsWithSameLabels.length; i++) {
            var destination = destinationsWithSameLabels[i];
            if (destination.yealdUnit === mainValorisation.destination.yealdUnit) {
              sameDestination = destination;
              break;
            }
          }
          // si aucune destination ne correspond alors une est prise par défaut
          if (!sameDestination) {
            sameDestination = destinationsWithSameLabels[0];
          }
        } else {
          for (var j = 0; j < destinationsWithSameLabels.length; j++) {
            var destination0 = destinationsWithSameLabels[j];
            if (destination0.uiId === mainValorisation.destination.uiId) {
              sameDestination = destination0;
              break;
            }
          }
        }
      }
      return sameDestination;
    };

    $scope._tryFindSameQualityCriteria = function (mainValorisation, action) {
      var newQualityCriteria;

      if (action.qualityCriteria && mainValorisation.qualityCriteriaDtos) {
        newQualityCriteria = [];
        angular.forEach(mainValorisation.qualityCriteriaDtos, function (qualityCriterias) {
          var matchingRefQualityCriteria = action.qualityCriteria[qualityCriterias.refQualityCriteriaId];
          if (matchingRefQualityCriteria) {
            var newQualityCriterias = angular.copy(qualityCriterias);
            newQualityCriterias.refQualityCriteria = matchingRefQualityCriteria;
            newQualityCriteria.push(newQualityCriterias);
          }
        });
      }
      return newQualityCriteria;
    };

    $scope._setDestinationYealdUnits = function (valorisation, destinationsByLabel) {
      valorisation.destinationsYealdUnits = {};

      if (destinationsByLabel) {
        angular.forEach(destinationsByLabel, function (destination) {
          // ajout uniquement des valeurs non null
          if (destination.yealdUnit) {
            valorisation.destinationsYealdUnits[destination.yealdUnit] = destination;
          }
        });
      }
    };

    $scope._propagateDestinationChangesToSpecies = function (action, valorisation) {
      var newSpeciesValorisationsForDestination = [];
      var totalSpeciesYealdAverage = 0.0;
      var destinationId = valorisation.destination.uiId;
      angular.forEach(action.speciesValorisationsBySpeciesCode, function (speciesValorisations) {
        var speciesValorisation = speciesValorisations.find(v => v.destination.uiId == destinationId);
        if (speciesValorisation) {
          speciesValorisation.destination = valorisation.destination;
          speciesValorisation.yealdUnit = valorisation.yealdUnit;
          if (valorisation._forceSpeciesYealdAveragesToMainYealdAverage) {
            speciesValorisation.yealdAverage = valorisation.yealdAverage;
          }

          speciesValorisation.destinationId = speciesValorisation.destination.topiaId;
          speciesValorisation.destinationName = speciesValorisation.destination ? speciesValorisation.destination.destination_Translated : "";
          speciesValorisation.yealdMin = valorisation.yealdMin ? parseFloat(valorisation.yealdMin) : null;
          speciesValorisation.yealdMax = valorisation.yealdMax ? parseFloat(valorisation.yealdMax) : null;
          speciesValorisation.yealdMedian = valorisation.yealdMedian ? parseFloat(valorisation.yealdMedian) : null;

          var targetedSpecies = action.speciesByCodes.speciesByCode[speciesValorisation.speciesCode];
          var isDestinationValidForSpecies = $scope._getIsDestinationValidForSpecies(targetedSpecies, speciesValorisation);
          speciesValorisation.locked = !isDestinationValidForSpecies;

          if (valorisation.qualityCriteriaDtos) {
            const qualityCriteriaToAdd = valorisation.qualityCriteriaDtos
                .filter(qc => !speciesValorisation.qualityCriteriaDtos.map(speciesQc => speciesQc.refQualityCriteriaId).includes(qc.refQualityCriteriaId));
            const refQualityCriteriaIdToRemove = speciesValorisation.qualityCriteriaDtos
                          .filter(speciesQc => !valorisation.qualityCriteriaDtos.map(qc => qc.refQualityCriteriaId).includes(speciesQc.refQualityCriteriaId))
                          .map(speciesQc => speciesQc.refQualityCriteriaId);

            speciesValorisation.qualityCriteriaDtos = speciesValorisation.qualityCriteriaDtos.filter(qc => !refQualityCriteriaIdToRemove.includes(qc.refQualityCriteriaId));
            speciesValorisation.qualityCriteriaDtos = speciesValorisation.qualityCriteriaDtos.concat(qualityCriteriaToAdd)
          }

          if (isDestinationValidForSpecies && action.isMixVarietyOrSpecies) {
            if (angular.isUndefined(speciesValorisation.yealdAverage) || isNaN(speciesValorisation.yealdAveragePart)) {
              newSpeciesValorisationsForDestination.push(speciesValorisation);
            } else {
              var speciesPart = parseFloat(speciesValorisation.yealdAveragePart);
              speciesValorisation.yealdAverage = (speciesPart * parseFloat(valorisation.yealdAverage)) / 100.0;
              totalSpeciesYealdAverage += speciesValorisation.yealdAverage;
            }
          }

        }
      });
      delete valorisation._forceSpeciesYealdAveragesToMainYealdAverage;

      if (newSpeciesValorisationsForDestination.length === 1) {
        var newValorisation = newSpeciesValorisationsForDestination[0];
        newValorisation.yealdAverage = parseFloat(valorisation.yealdAverage);
        newValorisation.yealdAveragePart = 100.0;

      } else if (newSpeciesValorisationsForDestination.length > 0) {
        var defaultSpeciesValorisation = (parseFloat(valorisation.yealdAverage) - totalSpeciesYealdAverage) / newSpeciesValorisationsForDestination.length;
        angular.forEach(newSpeciesValorisationsForDestination, function (newValorisation) {
          newValorisation.yealdAverage = defaultSpeciesValorisation;
          newValorisation.yealdAveragePart = parseFloat((defaultSpeciesValorisation * 100.0) / parseFloat(valorisation.yealdAverage));
        });
      }
    };

    $scope._saveMainValorisationChange = function (valorisation, action) {
      // destination can be change only for main destination then it is throw down to species valorisation
      if (action && valorisation && valorisation.main) {
        if (valorisation.userCreatedValorisation) {
          $scope._createSpeciesValorisationsForMainValorisation(action, valorisation);

        } else if (valorisation.destination) {
          $scope._propagateDestinationChangesToSpecies(action, valorisation);
        }
      }
    };

    $scope._removeValorisations = function (action, valorisationsToRemove) {
      if (valorisationsToRemove === "ALL") {
        $scope._clearEditedHarvestingActionContext(action);
      } else {
        angular.forEach(valorisationsToRemove, function (valorisationToRemove) {
          var destinationIdToRemove = valorisationToRemove.destination.uiId;
          if (action.mainValorisations[destinationIdToRemove]) {
            // remove main one
            delete action.mainValorisations[destinationIdToRemove];
            // remove valorisation for same destination in detailed valorisations
            $scope._removeSpecisesValorisation(action, destinationIdToRemove);

            delete $scope.editedValorisation;
          }
        });
      }
      action.hideDetailedHarvestingActionValorisation = action.hideDetailedHarvestingActionValorisation
        || !Object.keys(action.mainValorisations).length;

    };

    $scope._confirmDeleteValorisations = function (action, valorisationsToRemove, rollbackContext) {
      $scope.nbDestinationsToRemove = valorisationsToRemove === "ALL" ?
        Object.keys(action.mainValorisations).length : valorisationsToRemove.length;

      var deleteValorisationsModel = {
        process: function () {
          $scope._removeValorisations(action, valorisationsToRemove);
        },
        cancelChanges: function () {
          action = rollbackContext.rollBackAction;
          $scope.loadDestinationsContext = rollbackContext.rollBackLoadDestinationContext;
          if ($scope.editedAction) {
            $scope.editedAction = action;
          }
        }
      };

      _displayConfirmDialog($scope, $("#confirmRemovedDestinations"), 400, deleteValorisationsModel);

    };

    $scope._addLoadDestinationsContextResultsToAction = function (loadDestinationsContext, intervention, action, rollbackContext) {
      if (loadDestinationsContext && intervention && action) {

        action.qualityCriteria = loadDestinationsContext.qualityCriteria;

        var qualityCriteriaClasses = loadDestinationsContext.qualityCriteriaClasses;
        if (action.qualityCriteria && qualityCriteriaClasses) {
          angular.forEach(action.qualityCriteria, function (crit) {
            crit.qualityCriteriaClasses = qualityCriteriaClasses[crit.code];
          });
        }

        if (angular.isUndefined(action.mainValorisations)) {
          var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
          $scope._prepareHarvestingActionModel(intervention, action, speciesStadeSpecies);
        }

        action.destinationsByDestinationLabels = loadDestinationsContext.destinationsByDestinationLabels;
        $scope._setDestinationsUiIds(action);
        action.destinationsLength = Object.keys(action.destinationsByDestinationLabels).length;


        // for UI to display correct destination label while valorisation is edited
        if ($scope.editedValorisation && $scope.editedValorisation.selectedDestinations && $scope.editedValorisation.selectedDestinations.length > 0) {
          $scope.editedValorisation.selectedDestinations = action.destinationsByDestinationLabels[$scope.editedValorisation.selectedDestinations[0].destination_Translated];
        }

        var autoSelectedDestination = $scope.getAutoSelectedUniqueDestination(action);

        // if no destination available for the action remove all valorisations
        // otherwise replace previous destination with the new loaded one or with default one if there is one
        if (action.destinationsLength === 0) {

          if (rollbackContext && Object.keys(action.mainValorisations).length > 0) {
            // confirm remove valorisations
            $scope._confirmDeleteValorisations(action, "ALL", rollbackContext);
          } else {
            $scope._clearEditedHarvestingActionContext(action);
          }


        } else {
          if (!action.isMixVarietyOrSpecies) {
            $scope.showDetailedHarvestingActionValorisation(action);
          }

          if (action.mainValorisations) {
            var valorisationsToRemove = [];

            angular.forEach(action.mainValorisations, function (mainValorisation) {

              var loadDestination = $scope._tryFindSameDestination(mainValorisation, action) || autoSelectedDestination;
              if (!loadDestination) {
                valorisationsToRemove.push(mainValorisation);

              } else {
                mainValorisation.destination = loadDestination;
                mainValorisation.selectedDestinationLabel = loadDestination.destination_Translated;
                mainValorisation.selectedDestinations = action.destinationsByDestinationLabels[mainValorisation.selectedDestinationLabel];
                mainValorisation.yealdUnit = loadDestination.yealdUnit;
                mainValorisation.qualityCriteriaDtos = $scope._tryFindSameQualityCriteria(mainValorisation, action);

                // propagate change to species valorisations
                $scope._saveMainValorisationChange(mainValorisation, action);
              }
            });

            if (valorisationsToRemove.length > 0) {
              if (rollbackContext) {
                // confirm remove valorisations
                $scope._confirmDeleteValorisations(action, valorisationsToRemove, rollbackContext);
                action.valorisationDtos = action.speciesValorisations;
              } else {
                $scope._removeValorisations(action, valorisationsToRemove);
                action.valorisationDtos = action.speciesValorisations;
              }
            }
          }
        }
      }
    };

    $scope.setOrganicValorisation = function (action) {
      angular.forEach(action.valorisationDtos, function (valorisation) {
        valorisation.isOrganicCrop = action.isOrganicCrop;
      });
    };

    $scope._getPracticedSystemCampaingsValues = function (practicedSystem) {
      var practicedSystemCampaigns = [];
      if (practicedSystem) {
        var campaigns = practicedSystem.campaigns.replace(/\D+/, " ");
        var allCampaigns = campaigns.split(" ");
        for (var i = 0; i <= allCampaigns.length; i++) {
          var currentCampaign = allCampaigns[i];
          if (!isNaN(currentCampaign)) {
            var intCampaignValue = parseFloat(currentCampaign);
            practicedSystemCampaigns.push(intCampaignValue);
          }
        }
      }
      return practicedSystemCampaigns;
    };

    $scope.validPracticedPeriodFormat = function (period) {
      var valid = false;
      if (period) {
        valid = $scope.practicedPeriodFormatPattern.test(period);
      }
      return valid;
    };

    $scope.validEffectiveDate = function (date) {
      var valid = false;
      if (date) {
        var isDate = (!!Date.parse(date));
        if (isDate) {
          valid = true;
        } else if (angular.isString(date)) {
          // avoid setting date if not complete ex: 3/10/2014 valid but not correct. because of ng-change call.
          if (date.length === 10) {
            var startingDateTime = Date.parse($scope.editedIntervention.startInterventionDate);
            // check if the dateString can be converted to date
            valid = isNaN(startingDateTime) === false;
          }
        }
      }
      return valid;
    };

    $scope._getDecade = function (day0) {
      var day = parseInt(day0);
      var result;
      if (day < 11) {
        result = 1;
      } else if (day < 21) {
        result = 2;
      } else {
        result = 3;
      }
      return result;
    }

    $scope._getPracticedValorisationPediods = function (intervention, practicedSystem) {
      var result = {};

      var valid = $scope.validPracticedPeriodFormat(intervention.startingPeriodDate);
      valid &= $scope.validPracticedPeriodFormat(intervention.endingPeriodDate);

      if (!valid) {
        return;
      }

      var practicedSystemCampaigns = $scope._getPracticedSystemCampaingsValues(practicedSystem);
      result.beginCampaign = practicedSystemCampaigns[0];
      result.beginPeriod = intervention.startingPeriodDate.split('/')[1] - 1;// -1: 'all periods, 0 January, 2 february, ...
      result.beginDecade = $scope._getDecade(intervention.startingPeriodDate.split('/')[0]);

      result.endingCampaign = practicedSystemCampaigns[practicedSystemCampaigns.length - 1];
      result.endingPeriod = intervention.endingPeriodDate.split('/')[1] - 1;
      result.endingDecade = $scope._getDecade(intervention.endingPeriodDate.split('/')[0]);

      return result;
    };

    $scope._getEffectiveValorisationPediods = function (intervention) {
      var result = {};
      var startInterventionDate = !!Date.parse(intervention.startInterventionDate) ? new Date(intervention.startInterventionDate) : intervention.startInterventionDate;
      result.beginCampaign = startInterventionDate.getFullYear();
      result.beginPeriod = startInterventionDate.getMonth(); // same as Calendar 0: January, ...
      result.beginDecade = $scope._getDecade(startInterventionDate.getDate());

      var endInterventionDate = !!Date.parse(intervention.endInterventionDate) ? new Date(intervention.endInterventionDate) : intervention.endInterventionDate;

      result.endingCampaign = endInterventionDate.getFullYear();
      result.endingPeriod = endInterventionDate.getMonth(); // same as Calendar 0: January, ...
      result.endingDecade = $scope._getDecade(endInterventionDate.getDate());

      return result;
    };

    // intevention is required
    // practicedSystem is optional
    $scope._getValorisationPeriods = function (intervention, practicedSystem) {

      var result = null;

      if (practicedSystem) {
        result = $scope._getPracticedValorisationPediods(intervention, practicedSystem);
      } else {
        result = $scope._getEffectiveValorisationPediods(intervention);
      }

      return result;
    };

    $scope._getSpeciesStadesSpeciesLabels = function (speciesStadeSpecies) {
      return speciesStadeSpecies.map(sss => $scope._getSpeciesLabelForSpecies(sss, true)).join(" - ");
    };

    $scope._createMainValorisation = function (intervention, action, userCreatedValorisation, speciesCode) {
      var harvestingActionValorisation = null;

      var speciesStadesSpecies = action.speciesStadeSpecies;
      if (speciesStadesSpecies && speciesStadesSpecies.length > 0) {
        // set default valorisation (first present)
        var mainDestination = $scope.getAutoSelectedUniqueDestination(action);
        var periods = $scope._getValorisationPeriods(intervention, $scope.practicedSystem);

        // main valorisation
        harvestingActionValorisation = {
          topiaId: $scope._getNewValorisationId(),
          main: true,
          //cropCode:cropDescription.code,
          //mainSpeciesLabels : mainSpeciesLabels,
          displayName: action.displayName,
          speciesCode: null,
          destination: mainDestination,
          yealdAverage: 0.0,
          yealdUnit: mainDestination ? mainDestination.yealdUnit : null,
          salesPercent: 100,        // valorisation % commercialisé
          selfConsumedPersent: 0,   // valorisation % autoconsommé
          noValorisationPercent: 0, // valorisation % non valorisé
          qualityCriteria: [],
          detailRequired: false, // for UI to know if detail valorisation for each species must be displayed
          _isValidValorisationPercent: true,
          beginMarketingPeriod: periods.beginPeriod,
          beginMarketingPeriodDecade: periods.beginDecade,
          beginMarketingPeriodCampaign: periods.beginCampaign,
          endingMarketingPeriod: periods.endingPeriod,
          endingMarketingPeriodDecade: periods.endingDecade,
          endingMarketingPeriodCampaign: periods.endingCampaign,
          isOrganicCrop: action.isOrganicCrop,
          userCreatedValorisation: userCreatedValorisation,
          changeMainValorisationTargetAvailable: true,
          _speciesValorisationsToCreate: {}
        };
        if (speciesCode) {
          harvestingActionValorisation._speciesValorisationsToCreate[speciesCode] = true
        } else {
          angular.forEach(speciesStadesSpecies, speciesStade => harvestingActionValorisation._speciesValorisationsToCreate[speciesStade.code] = true);
        }
      }

      return harvestingActionValorisation;
    };

    $scope._computeIsSameSector = function (sectorForSpecies, valorisation) {
      return sectorForSpecies && sectorForSpecies.indexOf(valorisation.destination.sector) !== -1 ? 1 : 0;
    };

    $scope._getSpeciesCodeEspeceBotaniqueCodeQualifiantKey = function (species) {
      var result = null;
      if (species) {
        var codeEspeceBotanique = species.code_espece_botanique;
        var codeQualifant = species.code_qualifiant_AEE;
        var key = codeEspeceBotanique + "_" + (codeQualifant || "null");
        result = [key, codeEspeceBotanique, codeQualifant];
      }
      return result;
    };

    $scope._getIsDestinationValidForSpecies = function (species, valorisation) {
      var valid = false;
      var keyCodeEspeceBCodeQ = $scope._getSpeciesCodeEspeceBotaniqueCodeQualifiantKey(species);
      if (keyCodeEspeceBCodeQ) {
        var sectorForSpecies = $scope.allSectorsByCodeEspeceBotaniqueCodeQualifiant[keyCodeEspeceBCodeQ[0]];
        var destination = valorisation.destination;
        valid = destination
          && sectorForSpecies && sectorForSpecies.indexOf(destination.sector) != -1
          && (!destination.code_espece_botanique || keyCodeEspeceBCodeQ[1] === destination.code_espece_botanique)
          && (!destination.code_qualifiant_AEE || keyCodeEspeceBCodeQ[2] === destination.code_qualifiant_AEE);
      }
      return valid;
    };

    $scope.getValorisationSpecies = function (action) {
      var speciesCodes = Object.keys(action.speciesValorisationsBySpeciesCode);
      return speciesCodes.map(speciesCode => action.speciesByCodes.speciesByCode[speciesCode]);
    };

    $scope._getIsDestinationValidForSpeciesCode = function (speciesByCodes, valorisation) {
      var species = speciesByCodes.speciesByCode[valorisation.speciesCode];
      return $scope._getIsDestinationValidForSpecies(species, valorisation);
    };

    $scope._addSpeciesValorisationToAction = function (action, harvestingActionValorisation, theSpecies, mainValorisation) {
      if (theSpecies) {
        $scope._updateValorisationSpecies(action, harvestingActionValorisation, theSpecies.code);
      }

      var valorisationsForDestination = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
      if (!valorisationsForDestination) {
        valorisationsForDestination = [];
        action.valorisationsByMainValorisationIds[mainValorisation.topiaId] = valorisationsForDestination;
      }
      valorisationsForDestination.push(harvestingActionValorisation);

      if (!action.nbSameValorisationSectorFromMainValorisation[mainValorisation.topiaId]) {
        action.nbSameValorisationSectorFromMainValorisation[mainValorisation.topiaId] = 0;
      }
      if (harvestingActionValorisation.isDestinationValidForSpecies) {
        action.nbSameValorisationSectorFromMainValorisation[mainValorisation.topiaId]++;
      }

      $scope._updateActionSpeciesValorisations(action);
    };

    $scope._setValorisationSpecies = function (action, valorisation, species) {
      valorisation.speciesCode = species.code;
      valorisation.relativeArea = angular.isDefined(species.speciesArea) ?
        species.speciesArea * 100 / action.speciesTotalArea :
        action.speciesTotalArea / action.speciesStadeSpecies.length;
      valorisation.displayName = $scope._getSpeciesLabelForSpecies(species);
    };

    $scope._updateValorisationSpecies = function (action, valorisation, newSpeciesCode, previousSpeciesCode) {
      if (previousSpeciesCode) {
        var speciesValorisationForSpeciesCode = action.speciesValorisationsBySpeciesCode[previousSpeciesCode];
        speciesValorisationForSpeciesCode.splice(speciesValorisationForSpeciesCode.indexOf(valorisation), 1);
      }
      var speciesValorisationForSpeciesCode = action.speciesValorisationsBySpeciesCode[newSpeciesCode];
      if (!speciesValorisationForSpeciesCode) {
        speciesValorisationForSpeciesCode = [];
        action.speciesValorisationsBySpeciesCode[newSpeciesCode] = speciesValorisationForSpeciesCode;
      }
      speciesValorisationForSpeciesCode.push(valorisation);
    };

    $scope._updateActionSpeciesValorisations = function (action) {
      action.speciesValorisations = [];
      angular.forEach(action.speciesValorisationsBySpeciesCode, function (valorisationsForSpeciesCode) {
        action.speciesValorisations = action.speciesValorisations.concat(valorisationsForSpeciesCode);
      });
    };

    $scope._createSpeciesValorisationForMainValorisation = function (action, mainValorisation, theSpecies) {
      var isDestinationValidForSpecies = $scope._getIsDestinationValidForSpecies(theSpecies, mainValorisation);
      var harvestingActionValorisation = $scope._copyToNewValorisation(mainValorisation);
      harvestingActionValorisation.main = false;
      if (theSpecies) {
        $scope._setValorisationSpecies(action, harvestingActionValorisation, theSpecies);
      } else {
        harvestingActionValorisation.userCreatedValorisation = true;
      }
      harvestingActionValorisation.yealdAveragePart = 0.0;
      harvestingActionValorisation.yealdAverage = 0.0;
      harvestingActionValorisation._isValidValorisationPercent = true;

      harvestingActionValorisation.qualityCriteriaDtos = (isDestinationValidForSpecies || action.isMixVariety) ? angular.copy(mainValorisation.qualityCriteriaDtos) : [];
      harvestingActionValorisation.isDestinationValidForSpecies = isDestinationValidForSpecies;
      harvestingActionValorisation.locked = !harvestingActionValorisation.isDestinationValidForSpecies;

      angular.forEach(harvestingActionValorisation.qualityCriteriaDtos, function (qualityCriteria) {
        qualityCriteria.topiaId = null;
      });

      return harvestingActionValorisation;
    };

    $scope._createValorisationForSpecies = function (action, theSpecies) {

      //var sectorForSpecies = $scope._getSectorForSpecies($scope.loadDestinationsContext, theSpecies);

      action.speciesValorisationsBySpeciesCode[theSpecies.code] = [];

      // create new valorisation for each main species destinations
      angular.forEach(action.mainValorisations, function (mainValorisation) {
        // add valorisation for the current species
        var harvestingActionValorisation = $scope._createSpeciesValorisationForMainValorisation(action, mainValorisation, theSpecies);
        $scope._addSpeciesValorisationToAction(action, harvestingActionValorisation, theSpecies, mainValorisation);
      });
    };

    $scope._setSpeciesValorisationYealdAverage = function (action, mainValorisation) {
      if (mainValorisation) {
        var mainYealdAverage = mainValorisation.yealdAverage || 0.0;
        var speciesValorisationsForDestination = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
        var nbSameValorisationSectorFromMainValorisation = action.nbSameValorisationSectorFromMainValorisation[mainValorisation.topiaId];

        var yealdAverage = nbSameValorisationSectorFromMainValorisation === 0 ? mainYealdAverage : (mainYealdAverage / nbSameValorisationSectorFromMainValorisation);
        var yealdAveragePart = parseFloat(((yealdAverage * 100.0) / mainYealdAverage));

        angular.forEach(speciesValorisationsForDestination, function (speciesValorisation) {
          if (!speciesValorisation.isDestinationValidForSpecies) {
            speciesValorisation.yealdAverage = 0.0;
            speciesValorisation.yealdAveragePart = 0.0;

          } else if (action.isMixVarietyOrSpecies) {
            speciesValorisation.yealdAverage = yealdAverage;
            speciesValorisation.yealdAveragePart = yealdAveragePart;

          } else {
            speciesValorisation.yealdAverage = mainYealdAverage;
            speciesValorisation.yealdAveragePart = speciesValorisation.relativeArea || 100;
          }
        });
      }
    };

    $scope._getSectorForSpecies = function (destinationsContext, species) {
      var code_espece_botanique = species.code_espece_botanique || species.species.code_espece_botanique;
      var code_qualifiant = species.species ? species.species.code_qualifiant_AEE : species.code_qualifiant_AEE;
      var key = code_espece_botanique + "_" + (code_qualifiant || '');

      var sectorForSpecies = destinationsContext ? destinationsContext.sectorsByCodeEspeceBotaniqueCodeQualifiant[key] : null;
      return sectorForSpecies;
    };

    $scope._addDetailedHarvestingActionValorisation = function (action, mainValorisation) {
      // for each species we define the same destination
      angular.forEach(action.speciesStadeSpecies, function (species) {
        if (!mainValorisation._speciesValorisationsToCreate || mainValorisation._speciesValorisationsToCreate[species.code]) {
          var harvestingActionValorisation = $scope._createSpeciesValorisationForMainValorisation(action, mainValorisation, species);
          $scope._addSpeciesValorisationToAction(action, harvestingActionValorisation, species, mainValorisation);
        }
      });
      $scope._setSpeciesValorisationYealdAverage(action, mainValorisation);
    };

    $scope.mustDisplayValorisationsBySpecies = function (action) {
      return $scope.hasMainValorisations(action)
        && $scope.hasSeveralSpecies(action)
        && !action.hideDetailedHarvestingActionValorisation;
    };

    $scope.displayValorisationsBySpecies = function (action) {
      return $scope.hasMainValorisations(action)
        && $scope.hasSeveralSpecies(action);
    };

    $scope.showDetailedHarvestingActionValorisation = function (action) {
      action.hideDetailedHarvestingActionValorisation = false;
    };

    $scope.hideDetailedHarvestingActionValorisation = function (action) {
      action.hideDetailedHarvestingActionValorisation = true;
    };

    $scope.hasSeveralSpecies = function (action) {
      return action.speciesStadeSpecies.length > 1;
    };

    $scope.hasMainValorisations = function (action) {
      return !!Object.keys(action.mainValorisations).length;
    };

    $scope.hasSeveralMainValorisations = function (action) {
      return Object.keys(action.mainValorisations).length > 1;
    };

    $scope.areDestinationsForSpecies = function (action) {
      return action.destinationsLength > 0 && (!action.isWineSpecies || action.wineValorisations.length);
    };

    $scope.hasInterventionValidDates = function (intervention) {
      return (intervention.startingPeriodDate || intervention.startInterventionDate)
        && (intervention.endingPeriodDate || intervention.endInterventionDate);
    };

    $scope._createSpeciesValorisationsForMainValorisation = function (action, mainValorisation) {
      delete mainValorisation.userCreatedValorisation;
      $scope._addDetailedHarvestingActionValorisation(action, mainValorisation);
      action.nbDestinationForSpecies = action.nbDestinationForSpecies + 1;
    };

    $scope.addDestination = function (action) {
      if ($scope.loadDestinationsContext) {
        var userCreatedValorisation = true;
        var mainValorisation = $scope._createMainValorisation($scope.editedIntervention, action, userCreatedValorisation);

        if (mainValorisation) {
          action.valorisationsByMainValorisationIds[mainValorisation.topiaId] = [];
          $scope.setValorisationDestination(mainValorisation, action);
          action.mainValorisations[mainValorisation.destination ? mainValorisation.destination.uiId : null] = mainValorisation;
          $scope.editValorisation(mainValorisation, action);
        }
      }

    };

    $scope._initAction = function (intervention, action) {
      // the chosen action type is "RECOLTE" and it's a new action one.
      var speciesStadeSpecies;
      if (action.otherProductInputUsageDtos === undefined && (action.mainActionInterventionAgrosyst != "TRANSPORT" && action.mainActionInterventionAgrosyst != "TRAVAIL_DU_SOL")) {
        action.otherProductInputUsageDtos = [];
      }
      if (action.mainActionInterventionAgrosyst === "SUBSTRAT") {
              action.substrateInputUsageDtos = action.substrateInputUsageDtos === undefined ? [] : action.substrateInputUsageDtos;
      } else if (action.mainActionInterventionAgrosyst === "POT") {
        action.potInputUsageDtos = action.potInputUsageDtos === undefined ? [] : action.potInputUsageDtos;
      } else if (action.mainActionInterventionAgrosyst === "EPANDAGES_ORGANIQUES") {
        action.organicProductInputUsageDtos = action.organicProductInputUsageDtos === undefined ? [] : action.organicProductInputUsageDtos;
      } else if (action.mainActionInterventionAgrosyst === "APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX") {
        action.mineralProductInputUsageDtos = action.mineralProductInputUsageDtos === undefined ? [] : action.mineralProductInputUsageDtos;
      } else if (action.mainActionInterventionAgrosyst === "RECOLTE" && angular.isUndefined(action.mainValorisations)) {
        action.yealdUnits = $scope.yealdUnits;
        action.seedPlantUnits = $scope.seedPlantUnits;

        var rollbackContext = null;
        $scope._addLoadDestinationsContextResultsToAction($scope.loadDestinationsContext, intervention, action, rollbackContext);
        speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
        $scope._prepareHarvestingActionModel(intervention, action, speciesStadeSpecies);
      }

      // the chosen action type is "SEMIS" and it's a new action one.
      // we create a new seedingActionDto with all the intervention's species code.
      else if (action.mainActionInterventionAgrosyst === "SEMIS") {
        action.seedLotInputUsageDtos = action.seedLotInputUsageDtos === undefined ? [] : action.seedLotInputUsageDtos;
        action.seedTypes = $scope.seedTypes;
        action.yealdUnits = $scope.yealdUnits;
        action.seedPlantUnits = $scope.seedPlantUnits;
        action.seedType = 'SEMENCES_CERTIFIEES';

        $scope.seedingActionUsagesToRemove = [];
        speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
        action.speciesForSeeding = speciesStadeSpecies;

        if (angular.isUndefined(action.seedingSpecies)) {
          action.seedingSpecies = [];

          angular.forEach(speciesStadeSpecies, function (specie) {
            var seedingSpeciesAction = {};
            seedingSpeciesAction.speciesCode = specie.code;
            seedingSpeciesAction.speciesId = specie.speciesId || specie.species.topiaId;// practiced  : effective
            action.seedingSpecies.push(seedingSpeciesAction);
          });
        }
      }

      // init APPLICATION_DE_PRODUITS_PHYTOSANITAIRES action
      else if (action.mainActionInterventionAgrosyst === "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES" && angular.isUndefined(action.proportionOfTreatedSurface)) {
        action.phytoProductInputUsageDtos = action.phytoProductInputUsageDtos === undefined ? [] : action.phytoProductInputUsageDtos;
        action.proportionOfTreatedSurface = 100;
        action.antiDriftNozzle = false;

        if (action.toolsCouplingCode) {
          var localToolsCoupling = _toolsCouplingCodeToToolsCoupling[action.toolsCouplingCode];
          if (localToolsCoupling) {
            action.boiledQuantity = localToolsCoupling.boiledQuantity;
            action.antiDriftNozzle = localToolsCoupling.antiDriftNozzle;
          }
        }
      }

      // init LUTTE_BIOLOGIQUE action
      else if (action.mainActionInterventionAgrosyst === "LUTTE_BIOLOGIQUE" && angular.isUndefined(action.proportionOfTreatedSurface)) {
        action.phytoProductInputUsageDtos = action.phytoProductInputUsageDtos === undefined ? [] : action.phytoProductInputUsageDtos;
        action.proportionOfTreatedSurface = 100;
        if (action.toolsCouplingCode) {
          var localToolsCoupling = _toolsCouplingCodeToToolsCoupling[action.toolsCouplingCode];
          if (localToolsCoupling) {
            action.boiledQuantity = localToolsCoupling.boiledQuantity;
          }
        }
      }

      else if (action.mainActionInterventionAgrosyst === "TRANSPORT") {
        action.capacityUnits = $scope.capacityUnits;
      }
    };

    $scope._setUserSetValuesWorkRate = function () {
      $scope.userSetValues.workRate = !!$scope.editedIntervention.workRate;
      // the user change value but if he set the same value as the tools coupling one we consider it has not modified
      if ($scope.userSetValues.workRate &&
        $scope.editedIntervention.toolsCouplingCodes &&
        $scope.editedIntervention.toolsCouplingCodes[0]) {

        var localToolsCoupling = _toolsCouplingCodeToToolsCoupling[$scope.editedIntervention.toolsCouplingCodes[0]];
        if (localToolsCoupling) {
          $scope.userSetValues.workRate = $scope.editedIntervention.workRate !== localToolsCoupling.workRate ||
            $scope.editedIntervention.workRateUnit !== localToolsCoupling.workRateUnit;
        }
      }
    };

    $scope.processWorkRateChange = function () {
      $scope._setUserSetValuesWorkRate();
      $scope._computeSpendingTime($scope.editedIntervention);
    };

    $scope._setUserSetTransitVolume = function () {
      $scope.userSetValues.transitVolume = !!$scope.editedIntervention.transitVolume;
      // the user change value but if he set the same value as the tools coupling one we consider it has not modified
      if ($scope.userSetValues.transitVolume &&
        $scope.editedIntervention.toolsCouplingCodes &&
        $scope.editedIntervention.toolsCouplingCodes[0]) {

        var localToolsCoupling = _toolsCouplingCodeToToolsCoupling[$scope.editedIntervention.toolsCouplingCodes[0]];
        if (localToolsCoupling) {
          $scope.userSetValues.transitVolume = $scope.editedIntervention.transitVolume !== localToolsCoupling.transitVolume ||
            $scope.editedIntervention.transitVolumeUnit !== localToolsCoupling.transitVolumeUnit;
        }
      }
    };

    $scope.processTransitVolumeChange = function () {
      $scope._setUserSetTransitVolume();
      $scope._computeSpendingTime($scope.editedIntervention);
    };

    $scope.processInvolvedPeopleNumberChange = function () {
      var workforce = $scope.editedIntervention.involvedPeopleNumber || $scope.editedIntervention.involvedPeopleCount;
      $scope.userSetValues.involvedPeopleNumber = !!workforce;
      // the user change value but if he set the same value as the tools coupling one we consider it has not modified
      if ($scope.userSetValues.involvedPeopleNumber &&
        $scope.editedIntervention.toolsCouplingCodes &&
        $scope.editedIntervention.toolsCouplingCodes[0]) {

        var localToolsCoupling = _toolsCouplingCodeToToolsCoupling[$scope.editedIntervention.toolsCouplingCodes[0]];
        if (localToolsCoupling) {
          $scope.userSetValues.involvedPeopleNumber = workforce !== localToolsCoupling.workforce;
        }
      }
    };

    $scope.computeSpendingTime = function () {
      $scope._computeSpendingTime($scope.editedIntervention);
    };

    // compute intervention workRate
    $scope._computeSpendingTime = function (intervention) {
      if (intervention) {
        delete $scope.spendingTimeErrorMessage;
        $scope._setupFlag(intervention);

        var error = false;
        var errorMessage;
        var psci;

        if (intervention.transitCount) {
          // effective
          psci = intervention.transitCount * intervention.spatialFrequency;
        }
        if (intervention.temporalFrequency) {
          // practiced
          psci = intervention.spatialFrequency * intervention.temporalFrequency;
        }

        // calculs du temps de travail de l'intervention
        // PSCi effective: = intervention.transitCount * intervention.spatialFrequency
        // PSCi practiced: = intervention.transitCount * intervention.temporalFrequency
        // h/ha : = PSCi x débit de chantier
        // voy/h: = (PSCi x dose produit parmis les doses des intrants de type Engrais/amendement organique) / (débit x m3/voy)
        // bal/h: = (PSCi x débit) / (bal/h)
        // t/h  : = (PSCi x rendement) / débit

        if (!error && angular.isUndefined(psci)) {
          error = true;
          errorMessage = $scope.messages.psciNotComputable;
        }

        if (!intervention.workRate || intervention.workRate === 0) {
          error = true;
          errorMessage = $scope.messages.noWorkRate;
        }

        if (!intervention.workRateUnit) {
          error = true;
          errorMessage = $scope.messages.noWorkRateUnit;
        }

        if (psci === 0) {
          intervention.spendingTime = 0;
        }

        if (!error && psci !== 0) {
          if (intervention.workRateUnit === "H_HA") {
            intervention.spendingTime = psci * intervention.workRate;
          } else if (intervention.workRateUnit === "HA_H") {
            intervention.spendingTime = psci * (1 / intervention.workRate);
          } else if (intervention.workRateUnit === "VOY_H") {
            error = false;
            if (!intervention.transitVolume || intervention.transitVolume === 0) {
              error = true;
              errorMessage = $scope.messages.noTransitVolume;
            } else {
              if (!intervention.transitVolumeUnit) {
                error = true;
                errorMessage = $scope.messages.noTransitVolumeUnit;
              } else {
                if (intervention.type === "EPANDAGES_ORGANIQUES") {
                // find dose from EPANDAGES_ORGANIQUES
                  if (intervention.usages && intervention.usages.length > 0) {
                    var dose;
                    var qtUnit;
                    if (!error) {
                      var organiquesUsages = intervention.usages.filter((usage) => usage.inputType === "EPANDAGES_ORGANIQUES");
                      if (organiquesUsages && organiquesUsages.length > 0) {
                        var compatiblesUsages = organiquesUsages.filter((usage) => (intervention.transitVolumeUnit === "L" && usage.domainOrganicProductInputDto.usageUnit === "KG_HA") ||
                                                                        (intervention.transitVolumeUnit === "M3" && usage.domainOrganicProductInputDto.usageUnit === "M_CUB_HA") ||
                                                                        (intervention.transitVolumeUnit === "T" && usage.domainOrganicProductInputDto.usageUnit === "T_HA"))
                        if (compatiblesUsages && compatiblesUsages.length > 0) {
                          usageFound = true;
                          qtUnit = compatiblesUsages[0].domainOrganicProductInputDto.usageUnit;
                          dose = compatiblesUsages[0].qtAvg;
                          intervention.spendingTime = (psci * dose) / (intervention.workRate * intervention.transitVolume);
                        } else {
                          error = true;
                          errorMessage = $scope.messages.inconsistentUnits;
                        }
                      } else {
                        error = true;
                        errorMessage = $scope.messages.noInputDose;
                      }
                    }
                  } else {
                    error = true;
                    errorMessage = $scope.messages.inputDoseRequired;
                  }
                }
              }
            }
          } else if (intervention.workRateUnit === "BAL_H") {
            error = false;
            // défaut la valeur bal/ha de 10
            if (!intervention.nbBalls) {
              intervention.nbBalls = 10;
            }
            // h/ha: = (PSCi x bal/h) / débit
            if (intervention.workRate !== 0) {
              intervention.spendingTime = (psci * intervention.nbBalls) / intervention.workRate;
            } else {
              error = true;
              errorMessage = $scope.messages.balHaRequired;
            }
          } else if (intervention.workRateUnit === "T_H") {
            error = false;
            if (!intervention.actionDtos || intervention.actionDtos.length === 0) {
              error = true;
              errorMessage = $scope.messages.noHarvestAction;
            } else {
              var actionFound = false;
              angular.forEach(intervention.actionDtos, function (action) {
                if (!error && action.mainActionInterventionAgrosyst === "RECOLTE") {
                  actionFound = true;
                  var harvestingYealdAverage;
                  var harvestingYealdUnit;
                  if (!action.valorisationDtos || action.valorisationDtos.length === 0) {
                    error = true;
                    errorMessage = $scope.messages.noYieldOnHarvestAction;
                  } else {
                    angular.forEach(action.valorisationDtos, function (valorisation) {
                      if (!error) {
                        if (!harvestingYealdAverage) {
                          harvestingYealdAverage = valorisation.yealdAverage;
                          harvestingYealdUnit = valorisation.yealdUnit;
                          if (harvestingYealdUnit === "TONNE_HA" || harvestingYealdUnit === "Q_HA") {
                            if (harvestingYealdUnit === "Q_HA") {
                              // convert yeald in t/ha yeald = (q/ha) x 0.1
                              harvestingYealdAverage = (valorisation.yealdAverage * 0.1);
                            }
                          } else {
                            error = true;
                            errorMessage = $scope.messages.wrongUnitsOnHarvestAction;
                          }
                        } else {
                          if (harvestingYealdUnit === valorisation.yealdUnit) {
                            var newHarvestingYeald;
                            if (harvestingYealdUnit === "Q_HA") {
                              // convert yeald in t/ha yeald = (q/ha) x 0.1
                              newHarvestingYeald = (valorisation.yealdAverage * 0.1);
                            } else {
                              newHarvestingYeald = valorisation.yealdAverage;
                            }
                            harvestingYealdAverage = harvestingYealdAverage > newHarvestingYeald ? newHarvestingYeald : harvestingYealdAverage;
                          } else {
                            error = true;
                            errorMessage = $scop.messages.inconsistentUnitsOnHarvestAction;
                          }
                        }
                      }
                    });
                    // t/h  : = (PSCi x rendement) / débit
                    if (!error && harvestingYealdAverage) {
                      intervention.spendingTime = (psci * harvestingYealdAverage) / intervention.workRate;
                    }
                  }
                }
              });
              if (!actionFound) {
                error = true;
                errorMessage = $scope.messages.noHarvestAction;
              }
            }
          } else {
            error = true;
            errorMessage = $scope.messages.inconsistentWorkrateUnit;
          }
        }
        if (error) {
          delete intervention.spendingTime;
          $scope.spendingTimeErrorMessage = errorMessage;
        }
      }
    };

    $scope.checkInterventionEndDateValidity = function () {
      var customValidity = "";
      var field = $scope.editedInterventionForm.interventionEndDateField;
      var endValid = true;
      if ($scope.editedIntervention && $scope.editedIntervention.endInterventionDate && $scope.editedIntervention.startInterventionDate &&
        $scope.editedIntervention.startInterventionDate > $scope.editedIntervention.endInterventionDate) {
        customValidity = $scope.messages.invalidDates;
        endValid = false;
      }
      document.getElementById("interventionEndDateField").setCustomValidity(customValidity);
      field.$setValidity("afterSartDate", endValid);
      field.$setDirty(field.$dirty || endValid);
    };

    // ne pas déclancher à l'appel initial
    // intervention: required
    // practicedSystem: optional
    $scope.pushPeriodChangeToValorisations = function (intervention, practicedSystem) {

      var newSIDP = intervention.startInterventionDate || intervention.startingPeriodDate;
      var newEIDP = intervention.endInterventionDate || intervention.endingPeriodDate;

      if (($scope.prevSIDP && $scope.prevSIDP.toString() !== newSIDP.toString()) ||
        (newEIDP && $scope.prevEIDP && $scope.prevEIDP.toString() !== newEIDP.toString())) {

        var action = $scope._getEditedInterventionHarvestingAction();

        var pushPeriodChangeToValorisationsModel = {
          name: 'PERIOD_CHANGE',
          intervention: intervention,
          practicedSystem: practicedSystem,
          action: action,
          process: function () {
            if (action) {
              var valorisationPeriods = $scope._getValorisationPeriods(intervention, practicedSystem);
              var actionValorisations = action.valorisationDtos;
              if (actionValorisations && actionValorisations.length > 0) {
                angular.forEach(actionValorisations, function (valorisation) {
                  valorisation.beginMarketingPeriod = valorisationPeriods.beginPeriod;
                  valorisation.beginMarketingPeriodDecade = valorisationPeriods.beginDecade;
                  valorisation.beginMarketingPeriodCampaign = valorisationPeriods.beginCampaign;
                  valorisation.endingMarketingPeriod = valorisationPeriods.endingPeriod;
                  valorisation.endingMarketingPeriodDecade = valorisationPeriods.endingDecade;
                  valorisation.endingMarketingPeriodCampaign = valorisationPeriods.endingCampaign;
                });
              }

              var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
              $scope._prepareHarvestingActionModel(intervention, action, speciesStadeSpecies);
            }
          }
        };

        if (!$scope.loadDestinationsContext) {
          var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
          var rollbackContext = null;
          $scope._loadDestinationsContext(speciesStadeSpecies, intervention, action, rollbackContext, pushPeriodChangeToValorisationsModel);
        } else {
          pushPeriodChangeToValorisationsModel.process();
        }

      } else {
        // when first loading effective date set pistine false, this take off this effect
        if ($scope.effectiveCropCyclesEditForm && $scope.formPristineState) {
          $scope.effectiveCropCyclesEditForm.$setPristine();
        }
      }
    };

    // Préremplir la date de fin d'intervention avec la date de début
    $scope.interventionStartingDateSelected = function () {
      if ($scope.editedIntervention) {
        // practiced
        var valid = $scope.validPracticedPeriodFormat($scope.editedIntervention.startingPeriodDate);

        if ($scope.editedIntervention.startingPeriodDate && valid) {

          if (!$scope.editedIntervention.endingPeriodDate) {
            $scope.editedIntervention.endingPeriodDate = $scope.editedIntervention.startingPeriodDate;
          }

          $scope.pushPeriodChangeToValorisations($scope.editedIntervention, $scope.practicedSystem);
        }

        // effective
        if ($scope.editedIntervention.startInterventionDate) {
          $scope.pushPeriodChangeToValorisations($scope.editedIntervention, $scope.practicedSystem);
        }

        $scope.prevSIDP = angular.copy($scope.editedIntervention.startInterventionDate || $scope.editedIntervention.startingPeriodDate);
        $scope.prevEIDP = angular.copy($scope.editedIntervention.endInterventionDate || $scope.editedIntervention.endingPeriodDate);
      }
    };

    $scope._getSpeciesStadeBySpeciesCode = function (intervention) {
      var speciesStadeBySpeciesCode = {};
      if (intervention && intervention.speciesStadesDtos) {
        angular.forEach(intervention.speciesStadesDtos, function (speciesStade) {
          speciesStadeBySpeciesCode[speciesStade.speciesCode] = speciesStade;
        });
      }
      return speciesStadeBySpeciesCode;
    };

    $scope._getHarvestingActionFromIntervention = function (intervention) {
      for (var i = 0; i < intervention.actionDtos.length; i++) {
        var action = intervention.actionDtos[i];
        if (action.mainActionInterventionAgrosyst === "RECOLTE") {
          // il ne peux y avoir qu'une action de type récolte
          return action;
        }
      }
    };

    $scope._displayNoSelectedToolsCoupling = function() {
      if (Object.keys(_toolsCouplingCodeToToolsCoupling).length &&
          !Object.keys($scope.editedInterventionSelectedToolCouplings).length) {
       $('#warning-no-selected-tools-coupling').slideDown();
      } else {
       $('#warning-no-selected-tools-coupling').slideUp();
      }
    };

    $scope._nexToLoadToolsCouplingProcess = function (intervention, initialLoading) {
      // in all case if the intervention type change we ask the user to select a new toolsCoupling.
      if (!initialLoading && intervention && intervention.toolsCouplingCodes && intervention.toolsCouplingCodes.length === 0) {
        intervention.toolsCouplingCodes = [];
      }
      $scope._computeSpendingTime(intervention);

      // on intialise la map de selection des toolCoupling
      $scope.editedInterventionSelectedToolCouplings = {};
      angular.forEach(intervention.toolsCouplingCodes, function (code) {
        $scope.editedInterventionSelectedToolCouplings[code] = true;
      });

      $scope._displayNoSelectedToolsCoupling();

      $scope._checkAvailableActionAndInputTypes(intervention);

      if (intervention.actionDtos) {
        if (!intervention.destinationContext) {
          var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);

          var harvestingAction = $scope._getHarvestingActionFromIntervention(intervention);

          var rollbackContext = null;
        }
      }

      $scope._addInterventionProportionOfTreatedSurface(intervention);
    };

    $scope._displayIntervention = function(intervention) {
      $scope.editedIntervention = intervention;
      $scope.$emit('displayIntervention');
      return $q.when();
    };

    $scope.editedIntervention = undefined;

    $scope._displayInterventionForm = function() {
      $('#new-intervention').hide();
      $('#warning-no-tools-coupling-available').hide();
      $('#warning-no-selected-tools-coupling').hide();
      $('#new-intervention').slideDown();
    }

    $scope.$on('displayIntervention', $scope._displayInterventionForm);

    $scope._addLoadedToolsCoupling = function (intervention, initialLoading) {
      var response = $scope.toolsCouplingsByType;
      if (response && response.data && response.data != "null") {
        var toolsCouplings = response.data[intervention.type];
        $scope.toolsCouplings = toolsCouplings || [];
        // create map: toolsCouplngCode -> toolsCoupling
        angular.forEach($scope.toolsCouplings, function (toolsCoupling) {
          _toolsCouplingCodeToToolsCoupling[toolsCoupling.code] = toolsCoupling;
        });

        $("#warning-no-selected-tools-coupling").css("color", "red", "invalid");
        var updateTypeAgricultureFeedback = function() {
          if ($("#typeAgricultureField option:selected").val() == "<s:property value='missingTypeAgricultureTopiaId' escapeHtml='false'/>") {
            $("#typeAgricultureField").css("color", "red");
          } else {
            $("#typeAgricultureField").css("color", "inherit");
          }
        };
        $("#typeAgricultureField").change(updateTypeAgricultureFeedback);
        updateTypeAgricultureFeedback();

        if (!Object.keys(_toolsCouplingCodeToToolsCoupling).length) {
         $('#warning-no-tools-coupling-available').addClass('no-after');
         $('#warning-no-tools-coupling-available').slideDown();
        } else {
         $('#warning-no-tools-coupling-available').slideUp();
        }

        if (initialLoading) {
          // setup flag for manually modified work rate
          $scope._setupFlag(intervention);
        }
      } else {
        $scope.toolsCouplings = [];
      }
      return $q.when();
    };

    $('#new-intervention').hide();

    $scope.redirectToToolsCooplingsTab = function() {
       localStorage.setItem("forceTabIndex", true);
       if ($scope.frontApp === 'ipmworks') {
         localStorage.setItem("tabIndex", 4);
       } else {
         localStorage.setItem("tabIndex", 3);
       }

    }

    // selection d'une ligne intervention pour edition
    $scope.editIntervention = function (intervention) {

      // only for effective
      // save it because of date-ui that will set it to false;
      if ($scope.effectiveCropCyclesEditForm) {
        $scope.formPristineState = angular.copy($scope.effectiveCropCyclesEditForm.$pristine);
      }

      if ($scope.editedIntervention && intervention && $scope.editedIntervention === intervention) {
        delete $scope.editedIntervention;
        delete $scope.prevSIDP;
        delete $scope.prevEIDP;
        $('#new-intervention').hide();
        return;
      }

      $scope.prevSIDP = angular.copy(intervention.startInterventionDate || intervention.startingPeriodDate);
      $scope.prevEIDP = angular.copy(intervention.endInterventionDate || intervention.endingPeriodDate);

      $scope.setDateOptionYearRange(intervention);

      $scope._resetEditedIntervention();

      if (!intervention.toolsCouplingCodes) {
        intervention.toolsCouplingCodes = [];
      }

      if (!intervention.speciesStadesDtos) {
        intervention.speciesStadesDtos = $scope._createSpeciesStadesFromSpecies($scope.phaseConnectionOrNodeSpecies, false);
      }

      $scope.speciesStadeBySpeciesCode = $scope._getSpeciesStadeBySpeciesCode(intervention);

      if (intervention.actionDtos) {
        intervention.usages = intervention.actionDtos.map((action) => $scope.getUsageDtos(action))
          .reduce((prev, curr) => prev.concat(curr), []);
      } else {
        intervention.actionDtos = [];
        intervention.usages = [];
      }

      $scope._loadToolsCouplings(intervention).
        then($scope._addLoadedToolsCoupling.bind($scope, intervention, true)).
        then($scope._nexToLoadToolsCouplingProcess.bind($scope, intervention, true)).
        then($scope._displayIntervention.bind($scope, intervention));
    };
    // end editIntervention

    $scope.getUsages = function (intervention) {
      if (intervention && intervention.actionDtos){
        const usages = intervention.actionDtos.map((action) => $scope.getUsageDtos(action));
        return usages && usages.length ? usages.reduce((prev, curr) => prev.concat(curr), []) : [];
      }
    };

    $scope.getUsageDtos = function (action) {
      var usageDtos = [];
      if (action.mineralProductInputUsageDtos) {
        action.mineralProductInputUsageDtos.forEach((usage) => {
          usage.mainActionInterventionAgrosyst = action.mainActionInterventionAgrosyst;
          usage.mainActionReferenceLabel = action.mainActionReference_label;
        });
        usageDtos = usageDtos.concat(action.mineralProductInputUsageDtos);
      }
      if (action.phytoProductInputUsageDtos) {
        action.phytoProductInputUsageDtos.forEach((usage) => {
          usage.mainActionInterventionAgrosyst = action.mainActionInterventionAgrosyst;
          usage.mainActionReferenceLabel = action.mainActionReference_label;
        });
        usageDtos = usageDtos.concat(action.phytoProductInputUsageDtos);
      }
      if (action.organicProductInputUsageDtos) {
        action.organicProductInputUsageDtos.forEach((usage) => {
          usage.mainActionInterventionAgrosyst = action.mainActionInterventionAgrosyst;
          usage.mainActionReferenceLabel = action.mainActionReference_label;
        });
        usageDtos = usageDtos.concat(action.organicProductInputUsageDtos);
      }
      if (action.otherProductInputUsageDtos) {
        action.otherProductInputUsageDtos.forEach((usage) => {
          usage.mainActionInterventionAgrosyst = action.mainActionInterventionAgrosyst;
          usage.mainActionReferenceLabel = action.mainActionReference_label;
        });
        usageDtos = usageDtos.concat(action.otherProductInputUsageDtos);
      }
      if (action.substrateInputUsageDtos) {
        action.substrateInputUsageDtos.forEach((usage) => {
          usage.mainActionInterventionAgrosyst = action.mainActionInterventionAgrosyst;
          usage.mainActionReferenceLabel = action.mainActionReference_label;
        });
        usageDtos = usageDtos.concat(action.substrateInputUsageDtos);
      }
      if (action.potInputUsageDtos) {
        action.potInputUsageDtos.forEach((usage) => {
          usage.mainActionInterventionAgrosyst = action.mainActionInterventionAgrosyst;
          usage.mainActionReferenceLabel = action.mainActionReference_label;
        });
        usageDtos = usageDtos.concat(action.potInputUsageDtos);
      }
      if (action.seedLotInputUsageDtos) {
        action.seedLotInputUsageDtos.forEach((usage) => {
          usage.mainActionInterventionAgrosyst = action.mainActionInterventionAgrosyst;
          usage.mainActionReferenceLabel = action.mainActionReference_label;
        });
        usageDtos = usageDtos.concat(action.seedLotInputUsageDtos);
      }
      return usageDtos;
    };

    // clic sur le bouton "Nouvelle intervention"
    $scope.createIntervention = function () {
      $scope.setDateOptionYearRange();

      // toutes les especes (non définies comme compagne) sont sélectionnées par defaut
      // donc on crée autant de SpeciesStades qu'il y a d'espèces dans la culture
      var allSpeciesStades = $scope._createSpeciesStadesFromSpecies($scope.phaseConnectionOrNodeSpecies, false);

      var newIntervention = {
        topiaId: "NEW-INTERVENTION-" + guid(),
        domainId: $scope.domainId,
        toolsCouplingCodes: [],
        transitCount: 1,
        spatialFrequency: 1,
        temporalFrequency: 1,
        nbBalls: 10,
        speciesStadesDtos: allSpeciesStades,
        actionDtos: []
      };
      $scope.selectedPhaseConnectionOrNode.interventions.push(newIntervention);
      $scope.editIntervention(newIntervention);
      $scope.toolsCouplings = [];
      $scope.userSetValues = {
        workRate: false,
        transitVolume: false,
        involvedPeopleNumber: false
      };
    };

    $scope._getSelectedIntervention = function () {
      $scope.copiedInterventions = [];

      angular.forEach($scope.selectedInterventions, function (value, key) {
        if (value) {
          angular.forEach($scope.selectedPhaseConnectionOrNode.interventions, function (intervention) {
            if (key === intervention.topiaId) {
              var copy = angular.copy(intervention);
              copy.topiaId = "NEW-INTERVENTION-" + guid();
              // save copy
              $scope.copiedInterventions.push(copy);
            }
          });
        }
      });
      return $scope.copiedInterventions;
    };

    // return the action if found otherwise false
    $scope._replaceUsageAction = function (actionByOriginalIds, usage, speciesStadeMigrationStatus) {
      // speciesStadeMigrationStatus is optionnal, if specified then we must validate dosage
      var actionFound = false;
      if (usage.seedingAction) {
        usage.seedingAction = actionByOriginalIds[usage.seedingAction.topiaId];
        actionFound = usage.seedingAction;
      }
      if (usage.biologicalControlAction) {
        if (speciesStadeMigrationStatus && speciesStadeMigrationStatus != "ALL_MIGRATED") {
          delete usage.qtMin;
          delete usage.qtAvg;
          delete usage.qtMed;
          delete usage.qtMax;
          delete usage.phytoProductUnit;
        }
        usage.biologicalControlAction = actionByOriginalIds[usage.biologicalControlAction.topiaId];
        actionFound = usage.biologicalControlAction;
      }
      if (usage.pesticidesSpreadingAction) {
        usage.pesticidesSpreadingAction = actionByOriginalIds[usage.pesticidesSpreadingAction.topiaId];
        actionFound = usage.pesticidesSpreadingAction;
      }
      if (usage.organicFertilizersSpreadingAction) {
        usage.organicFertilizersSpreadingAction = actionByOriginalIds[usage.organicFertilizersSpreadingAction.topiaId];
        actionFound = usage.organicFertilizersSpreadingAction;
      }
      if (usage.mineralFertilizersSpreadingAction) {
        usage.mineralFertilizersSpreadingAction = actionByOriginalIds[usage.mineralFertilizersSpreadingAction.topiaId];
        actionFound = usage.mineralFertilizersSpreadingAction;
      }
      if (usage.otherAction) {
        usage.otherAction = actionByOriginalIds[usage.otherAction.topiaId];
        actionFound = usage.otherAction;
      }
      if (usage.harvestingAction) {
        usage.harvestingAction = actionByOriginalIds[usage.harvestingAction.topiaId];
        actionFound = usage.harvestingAction;
      }
      if (usage.maintenancePruningVinesAction) {
        usage.maintenancePruningVinesAction = actionByOriginalIds[usage.maintenancePruningVinesAction.topiaId];
        actionFound = usage.maintenancePruningVinesAction;
      }
      if (usage.irrigationAction) {
        usage.irrigationAction = actionByOriginalIds[usage.irrigationAction.topiaId];
        actionFound = usage.irrigationAction;
      }
      // TODO FIXIT
      if (usage.seedLotInputUsageDtos) {
        usage.irrigationAction = actionByOriginalIds[usage.irrigationAction.topiaId];
        actionFound = usage.irrigationAction;
      }
      return actionFound;
    };

    $scope._getCurrentCropCodeIdentifiants = function () {
      var result = {};
      var currentCrop;
      // practiced
      if ($scope.phaseOrNodeCrop) {
        currentCrop = $scope.phaseOrNodeCrop;
        result.code = currentCrop.croppingPlanEntryCode;
        result.intermediateCropCode = $scope.connectionIntermediateCrop ? $scope.connectionIntermediateCrop.croppingPlanEntryCode : null;
        result.name = currentCrop.name;
      }

      // effective
      if ($scope.croppingPlanEntriesIndex && $scope.selectedPhaseConnectionOrNode) {
        currentCrop = $scope.croppingPlanEntriesIndex[$scope.selectedPhaseConnectionOrNode.croppingPlanEntryId];
        result.id = currentCrop.topiaId;
        result.code = currentCrop.code;
        result.intermediateCropCode = $scope.connectionIntermediateCrop ? $scope.connectionIntermediateCrop.code : null;
        result.name = currentCrop.name;
      }

      return result;
    };

    $scope._getSpeciesStadesBySpeciesCodes = function (speciesStadesDtos, matchingSpeciesBySpeciesCode) {
      var speciesStadeBySpeciesCode = {};
      angular.forEach(speciesStadesDtos, function (speciesStade) {
        var newSpecies = matchingSpeciesBySpeciesCode[speciesStade.speciesCode];
        if (newSpecies) {
          speciesStade.speciesCode = newSpecies.code;
          speciesStadeBySpeciesCode[speciesStade.speciesCode] = speciesStade;
        }
      });
      return speciesStadeBySpeciesCode;
    };

    // Create intervention speciesStade for the copied intervention
    // push the existed ones if same species targeted otherwise create new one.
    $scope._tryToMigrateSpeciesStades = function (speciesStadesDtos, targetedSpecies, matchingSpeciesBySpeciesCode, toIntermediateCrop) {
      // status: NEW (migration could not be done, new species stades created)
      //         ALL_MIGRATED (all species stades succed to be migrated)
      //         PARTIAL (some species stades succed to be migrated)
      var speciesStadeMigrationStatus = { speciesStades: [], status: "NEW" };
      var speciesStadeBySpeciesCode;
      if (toIntermediateCrop) {
        // cocher toutes les espèces (depuis #12354)
        speciesStadeMigrationStatus.speciesStades = $scope._createSpeciesStadesFromSpecies(targetedSpecies, true);
      } else {
        if (matchingSpeciesBySpeciesCode && Object.keys(matchingSpeciesBySpeciesCode).length > 0) {
          // cocher les espèces cibles qui sont les mêmes que les espèces sources (même refespece)
          // dans le cas ou l'intervention porte sur une culture principale
          // cela signifie que l'on peut copier le stade de culture d'origine sur l'espèce corresppondant,
          speciesStadeBySpeciesCode = $scope._getSpeciesStadesBySpeciesCodes(speciesStadesDtos, matchingSpeciesBySpeciesCode);
          angular.forEach(targetedSpecies, function (species) {
            var originalSpeciesStade = speciesStadeBySpeciesCode[species.code];
            if (originalSpeciesStade) {
              delete originalSpeciesStade.topiaId;
              speciesStadeMigrationStatus.speciesStades.push(originalSpeciesStade);
            }
          });
          if (speciesStadesDtos.length === speciesStadeMigrationStatus.speciesStades.length) {
            speciesStadeMigrationStatus.status = "ALL_MIGRATED";
          } else if (speciesStadeMigrationStatus.speciesStades.length > 0 && speciesStadeBySpeciesCode.length < speciesStadeMigrationStatus.speciesStades.length) {
            speciesStadeMigrationStatus.status = "PARTIAL";
          }
        } else {
          //cocher toutes les espèces de la CP.
          speciesStadeMigrationStatus.speciesStades = $scope._createSpeciesStadesFromSpecies(targetedSpecies, false);
        }
      }
      $scope._addOrReplaceSpeciesStadeToSpeciesStadeBySpeciesCode(speciesStadeMigrationStatus.speciesStades);

      return speciesStadeMigrationStatus;
    };

    $scope._getSelectedPhaseConnectionOrNodeSpecies = function (fromIntermediate) {
      var result = [];
      if (fromIntermediate) {
        result = $scope.connectionIntermediateSpecies;
      } else {
        result = $scope.phaseConnectionOrNodeSpecies;
      }
      return result;
    };

    $scope._getInterventionInterventionTypes = function (interventions) {
      var interventionTypes = [];
      angular.forEach(interventions, function (intervention) {
        interventionTypes.push(intervention.type);
      });
      return interventionTypes;
    };

    $scope._loadCopyPasteGlobalInfo = function (interventions) {
      var itIdentifierQuery = $scope.getItkIdentifierQuery();
      var interventionTypes = $scope._getInterventionInterventionTypes(interventions);

      return $http.post(
        ENDPOINTS.loadCopyPasteGlobalInfoJson,
        itIdentifierQuery +
        "&agrosystInterventionTypes=" + angular.toJson(interventionTypes),
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
      ).then(function (response) {
        $scope.agrosystInterventionTypesToolsCouplingDtos = response.data.agrosystInterventionTypesToolsCouplingDtos;
        $scope.allCodeEspeceBotaniqueCodeQualifantBySpeciesCode = response.data.allCodeEspeceBotaniqueCodeQualifantBySpeciesCode;
        $scope.allSectorsByCodeEspeceBotaniqueCodeQualifiant = response.data.allSectorByCodeEspeceBotaniqueCodeQualifiant;
        $scope.practicedDomainInputStockUnitByCodes = response.data.practicedDomainInputStockUnitByCodes;
      }).catch(function (response) {
        var message = $scope.messages.copyPasteDataLoadingError;
        console.error(message, response);
        addPermanentError(message, response.status);
      });
    };

    // return new collection of migrated usages;
    $scope._migrateUsagesActionAndTargets = function (actionByIds, usages, speciesStadeMigrationResult, forceNewUsage, toCrop) {
      var speciesStadeMigrationStatus = speciesStadeMigrationResult.status;
      var speciesStades = speciesStadeMigrationResult.speciesStades;
      var migratedUsages = [];
      angular.forEach(usages, function (usage) {
        var valid = $scope._replaceUsageAction(actionByIds, usage, speciesStadeMigrationStatus);
        if (valid) {
          usage.inputUsageTopiaId = forceNewUsage ? "NEW-USAGE-" + guid() : usage.inputUsageTopiaId;
          if (usage.targets) {
            angular.forEach(usage.targets, function (target) {
              target.topiaId = null;
            });
          }
          migratedUsages.push(usage);
        }
      });
      return migratedUsages;
    };

    $scope._getInterventionSpeciesStadesMigrationResult = function (interventionToCopy, fromCropCode, toCropCode, fromSpecies, toSpecies) {
      var fromSpeciesStadesDtos = interventionToCopy.speciesStadesDtos;
      var matchingSpeciesBySpeciesCode = _findMatchingSpecies(fromSpecies, toSpecies);
      var toIntermediateCrop = null;
      var speciesStadeMigrationStatus = $scope._tryToMigrateSpeciesStades(
        fromSpeciesStadesDtos, toSpecies, matchingSpeciesBySpeciesCode, toIntermediateCrop);

      return speciesStadeMigrationStatus;
    };

    $scope.isSameCrop = function (zone) {
      var res = zone.croppingPlanEntryId === $scope.selectedPhaseConnectionOrNode.croppingPlanEntryId;
      return res;
    };

    $scope.validateCycle = function (cycle) {
      var result = true;
      if ($("#tabs-itk-li")[0].className === "selected") {
        if (cycle && (!$scope.selectedPhaseConnectionOrNode || cycle !== $scope.selectedPhaseConnectionOrNode)) {
          var interventions = cycle.interventions;
          for (var i = 0; i < interventions.length && result; i++) {
            var intervention = interventions[i];
            result = $scope.validateIntervention(intervention);
          }
        }
      }
      return result;
    };

    // valide une intervention avec ses actions et ses intrants
    $scope.validateIntervention = function (intervention, deepValidation) {
      var result = true;

      // au moins une action est obligatoire
      if (intervention && intervention !== $scope.editedIntervention &&
         !$scope.editedAction && !$scope.editedUsage) {

        // valid intervention name
        result &= (!!intervention.name);

        // valid intervention dates
        var start = intervention.startingPeriodDate || intervention.startInterventionDate;
        var end = intervention.endingPeriodDate || intervention.endInterventionDate;
        result &= (!!(start && end));
        if (result && intervention.startingPeriodDate) {
          result &= $scope.validPracticedPeriodFormat(intervention.startingPeriodDate);
          result &= $scope.validPracticedPeriodFormat(intervention.endingPeriodDate);
        }
        if (result && intervention.startInterventionDate) {
          result &= $scope.validEffectiveDate(intervention.startInterventionDate);
          result &= $scope.validEffectiveDate(intervention.endInterventionDate);
        }

        // valid intervention actions
        if (!$scope.editedAction) {
          var actions = intervention.actionDtos;
          if (!actions || actions.length === 0) {
            result = false;
          } else {
            for (var i = 0; i < actions.length && result; i++) {
              var action = actions[i];
              var errorMessages = $scope.validateAction(intervention, action);
              result &= errorMessages.length === 0;
            }
          }
        }

        // valid intervention usages
        if (!$scope.editedUsage) {
          var usages = intervention.usages;
          if (usages === undefined) {
            var seedingAction = intervention.actionDtos.filter(dto =>
              (dto.mainActionInterventionAgrosyst === 'SEMIS' || dto.mainActionInterventionAgrosyst === 'PLAN_COMPAGNE'))[0];
            if (seedingAction) {
              usages = seedingAction.seedLotInputUsageDtos;
            }
          }

          if (result && usages && usages.length > 0) {
            for (var j = 0; j < usages.length && result; j++) {
              var usage = usages[j];
              result &= $scope.validateUsage(usage, intervention);
            }
          }
        }

      }

      //return $scope.editedInterventionForm ? $scope.editedInterventionForm.$valid : true;
      return Boolean(result);

    };

    $scope.loadStadesForSpecies = function (species) {
      delete $scope.stadesList;
      var query = "vegetativeProfile=";
      if (angular.isDefined(species.profil_vegetatif_BBCH)) {
        query += species.profil_vegetatif_BBCH;
      } else if (angular.isDefined(species.species)) {
        query += species.species.profil_vegetatif_BBCH;
      }
      $http.post(
        ENDPOINTS.loadRefStadesEdiJson, query,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
      ).then(function (response) {

        // On reçoit une map composée d'une clef/valeur ou la clef est constituée
        //  de la concaténation des 4 derniers char du code AEE et de la 'colonne 2' du référentiel.
        if (response.data != "null") {
          $scope.stadesList = response.data;

          // mapping of the StadeEdi received and the one from the intervention SpeciesStade
          if ($scope.editedSpeciesStades && ($scope.editedSpeciesStades.stadeMin || $scope.editedSpeciesStades.stadeMax)) {
            var speciesStade = $scope.editedSpeciesStades;

            angular.forEach($scope.stadesList, function (stade) {
              if (speciesStade.stadeMin && speciesStade.stadeMin.topiaId === stade.topiaId) {
                speciesStade.stadeMin = stade;
              }
              if (speciesStade.stadeMax && speciesStade.stadeMax.topiaId === stade.topiaId) {
                speciesStade.stadeMax = stade;
              }
            });
          }
        }
      }).catch(function (response) {
        var message = $scope.messages.referentialEdiLoadingError;
        console.error(message, response);
        addPermanentError(message, response.status);
      });
    };

    $scope.startEditSpeciesStades = function (species) {
      var requiredValidation = true;
      var speciesStades = $scope.selectInterventionSpecies(species, requiredValidation);
      $scope.editedSpeciesStades = speciesStades;

      $scope.loadStadesForSpecies(species);
    };

    $scope.stopEditSpeciesStades = function () {
      delete $scope.editedSpeciesStades;
    };

    $scope.speciesStadeMinSelected = function () {
      if ($scope.editedSpeciesStades.stadeMin && !$scope.editedSpeciesStades.stadeMax) {
        $scope.editedSpeciesStades.stadeMax = $scope.editedSpeciesStades.stadeMin;
      }
    };

    $scope.speciesStadeMaxSelected = function () {
      if ($scope.editedSpeciesStades.stadeMax && !$scope.editedSpeciesStades.stadeMin) {
        $scope.editedSpeciesStades.stadeMin = $scope.editedSpeciesStades.stadeMax;
      }
    };

    $scope.deleteSelectedInterventions = function () {
      var interventionsToDelete = [];

      angular.forEach($scope.selectedInterventions, function (value, key) {
        if (value) {
          angular.forEach($scope.selectedPhaseConnectionOrNode.interventions, function (intervention) {
            if (key === intervention.topiaId) {
              interventionsToDelete.push(intervention);
            }
          });
        }
      });

      $scope._deleteIntervention(interventionsToDelete);
    };

    $scope.interventionsToDelete = [];
    $scope.deleteIntervention = function (intervention) {
      var interventionsToDelete = [];
      interventionsToDelete.push(intervention);
      $scope._deleteIntervention(interventionsToDelete);
    };


    $scope.removeInterventionsContext = null;

    $scope.removeInterventions = function () {
      if ($scope.removeInterventionsContext && $scope.removeInterventionsContext.interventions.length > 0) {
        angular.forEach($scope.removeInterventionsContext.interventions, function (intervention) {
          var indexOf = $scope.selectedPhaseConnectionOrNode.interventions.indexOf(intervention);
          $scope.selectedPhaseConnectionOrNode.interventions.splice(indexOf, 1);
        });
        $scope.selectedInterventions = {};
        $scope.allInterventionSelected = { selected: false };
      }
      delete $scope.removeInterventionsContex;
    };

    $scope._deleteIntervention = function (interventionsToDelete) {
      $scope.removeInterventionsContext = {};
      $scope.removeInterventionsContext.interventions = interventionsToDelete;
      if ($scope.removeInterventionsContext.interventions.length > 0) {

        var deleteInterventionsModel = {
          process: function () {
            $scope.removeInterventions();
            $scope._resetEditedIntervention();
          },
          cancelChanges: function () { }
        };

        _displayConfirmDialog($scope, $("#confirmDeleteIntervention"), 400, deleteInterventionsModel);
      }
    };

    $scope.getSpeciesStades = function (species) {
      var result;
      if ($scope.editedIntervention.speciesStadesDtos) {
        angular.forEach($scope.editedIntervention.speciesStadesDtos, function (speciesStades) {
          if (speciesStades.speciesCode === species.code) {
            result = speciesStades;
          }
        });
      }
      return result;
    };

    $scope._removeSpeciesFromHarvestingAction = function (result) {
      var speciesToRemoved = result.speciesToRemoved;
      var intervention = $scope.editedIntervention;
      var connectionIntermediateSpecies = result.connectionIntermediateSpecies;
      var phaseConnectionOrNodeSpecies = result.phaseConnectionOrNodeSpecies;
      var action = result.action;

      if (action && action.mainActionInterventionAgrosyst === "RECOLTE" && action.valorisationDtos.length > 0) {
        //$scope._clearEditedHarvestingActionContext(action);
        result.harvestingActionId = action.topiaId;
        var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, connectionIntermediateSpecies, phaseConnectionOrNodeSpecies);

        if (speciesStadeSpecies.length === 0) {
          // there is no more selected species then remove all valorisations
          action.valorisationDtos = [];
          result.affectedActionsNames.push("RECOLTE");
          return;
        }

        // remove valorisations related to the removed species
        // and dispatch yeald between other species

        // total yealdAverage for destination, sum of species yeald average for this destination
        var globalDestinationValorisationContext = [];// (table of {destinationId, yealdAverage})
        var removeSpeciesYealdAverageForDestination = [];// (table of {destinationId, removeSpeciesYealdAverage})
        var lastSpeciesCode = "";// must not be null
        var destinationIndex = 0;

        var speciesByCodes = $scope._getSpeciesStadeBySpeciesCodeAndByCodeEspeceBotanique(speciesStadeSpecies);

        var newValorisations = [];// contain all valid specises destisations valorisations

        angular.forEach(action.speciesValorisations, function (valorisation) {

          if (valorisation.destination) {

            var isDestinationValidForSpecies = $scope._getIsDestinationValidForSpeciesCode(speciesByCodes, valorisation);

            // while the species code does not change we are on the same valorisations's species
            destinationIndex = valorisation.speciesCode === lastSpeciesCode ? destinationIndex : 0;
            lastSpeciesCode = valorisation.speciesCode;

            var valorisationForDestination = globalDestinationValorisationContext[destinationIndex];
            if (!valorisationForDestination) {
              // if no global yealdAverage has been specified for this destination then create it
              valorisationForDestination = { destinationId: valorisation.destination.uiId, nbValidSpecies: 0 };
              globalDestinationValorisationContext[destinationIndex] = valorisationForDestination;
            }

            if (valorisation.speciesCode === speciesToRemoved.code) {
              // save the yeald average amount to remove for the destination
              var removeSpeciesDestinationValorisation = { destinationId: valorisation.destination.uiId, yealdAverage: parseFloat(valorisation.yealdAverage) };
              removeSpeciesYealdAverageForDestination[destinationIndex] = removeSpeciesDestinationValorisation;
            } else {
              // add species valorisation yeald average to the global destination amount
              if (valorisationForDestination.destinationId === valorisation.destination.uiId) {
                valorisationForDestination.nbValidSpecies += isDestinationValidForSpecies;
                newValorisations.push(valorisation);
              } else {
                console.error("Les destinations ne correspondent pas:" + valorisationForDestination.destinationId + "/" + valorisation.destination.uiId);
              }
            }
            destinationIndex++;
          }
        });

        destinationIndex = 0;
        lastSpeciesCode = "";

        var unValidvalorisations = [];
        angular.forEach(newValorisations, function (valorisationForDestination) {
          if (valorisationForDestination.destination) {
            // while the species code does not change we are on the same valorisations's speciesversive
            destinationIndex = valorisationForDestination.speciesCode === lastSpeciesCode ? destinationIndex : 0;
            lastSpeciesCode = valorisationForDestination.speciesCode;

            var globalYealdAverageForDestinationDetail = globalDestinationValorisationContext[destinationIndex];
            var isDestinationValidForSpecies = $scope._getIsDestinationValidForSpeciesCode(speciesByCodes, valorisationForDestination);
            if (isDestinationValidForSpecies) {
              var nbValidSpeciesForTheDestination = globalYealdAverageForDestinationDetail.nbValidSpecies;
              var removedYealdAverageDetail = removeSpeciesYealdAverageForDestination[destinationIndex];
              if (removedYealdAverageDetail.destinationId !== valorisationForDestination.destination.uiId) {
                console.error("Les destinations ne correspondent pas:" + removedYealdAverageDetail.destinationId + "/" + valorisationForDestination.destination.uiId);
              }
              var removedYealdAverage = removedYealdAverageDetail.yealdAverage;
              if (removedYealdAverage !== 0.0 && nbValidSpeciesForTheDestination !== 0) {
                var amountToDistribute = removedYealdAverage / nbValidSpeciesForTheDestination;
                valorisationForDestination.yealdAverage += amountToDistribute;
              }
            } else if (globalYealdAverageForDestinationDetail.nbValidSpecies === 0) {
              unValidvalorisations.push(valorisationForDestination);
            }
            destinationIndex++;
          }

        });

        if (unValidvalorisations.length > 0) {
          angular.forEach(unValidvalorisations, function (unValidvalorisation) {
            var index = newValorisations.indexOf(unValidvalorisation);
            if (index !== -1) {
              newValorisations.splice(index, 1);
            }
          });
        }
        action.valorisationDtos = newValorisations;
      }
    };

    $scope._removeSpeciesFromAction = function (intervention, originalSpeciesStades, speciesToRemoved, connectionIntermediateSpecies, phaseConnectionOrNodeSpecies) {
      var result = { affectedActionsNames: [], nbUsageRemoved: 0, harvestingActionId: null };
      for (var i = 0; i < intervention.actionDtos.length; i++) {
        var action = intervention.actionDtos[i];

        if (action.mainActionInterventionAgrosyst === "RECOLTE" && action.valorisationDtos.length > 0) {
          result.affectedActionsNames.push("RECOLTE");
          result.harvestingActionId = action.topiaId;
          result.speciesToRemoved = speciesToRemoved;
          result.intervention = intervention;
          result.connectionIntermediateSpecies = connectionIntermediateSpecies;
          result.phaseConnectionOrNodeSpecies = phaseConnectionOrNodeSpecies;
          result.originalSpeciesStades = originalSpeciesStades;
          if (angular.isUndefined(action.mainValorisations)) {
            $scope._prepareHarvestingActionModel(intervention, action, originalSpeciesStades);
          }
          result.action = action;
        } else if (action.mainActionInterventionAgrosyst === "SEMIS" && action.seedLotInputUsageDtos && action.seedLotInputUsageDtos.length > 0) {
          angular.forEach(action.seedLotInputUsageDtos, function(usage){
            angular.forEach(usage.seedingSpeciesDtos, function (seedingSpecies) {
              var inputSpeciesCode = seedingSpecies.domainSeedSpeciesInputDto.speciesSeedDto.code;
              if(speciesToRemoved.code === inputSpeciesCode) {
                seedingSpecies.qtAvg = 0;
              }
            })

          })

          result.affectedActionsNames.push(action.mainActionInterventionAgrosyst);
          result.seedingActionId = action.topiaId;
          result.speciesToRemoved = speciesToRemoved;
          result.intervention = intervention;
          result.connectionIntermediateSpecies = connectionIntermediateSpecies;
          result.phaseConnectionOrNodeSpecies = phaseConnectionOrNodeSpecies;
          result.originalSpeciesStades = originalSpeciesStades;
          result.action = action;
        }
      }
      return result;
    };

    $scope._removeNotValidUsages = function (intervention, changeOnActionImpact, species) {
      var usagesToRemoves = [];
      angular.forEach(intervention.usages, function (usage) {
        if (usage.seedingAction) {
          if (changeOnActionImpact.removeAllUsages) {
            usagesToRemoves.push(usage);
          }
        }
      });

      // remove usage from intervention
      angular.forEach(usagesToRemoves, function (usage) {
        var index = intervention.usages.indexOf(usage);
        if (index != -1) {
          intervention.usages.splice(index, 1);
        }
      });
    };

    $scope._getWarningMessageOnRemoveActionsAndUsages = function (actionsAndUsagesRemovedResult) {
      var warningMessage;
      var confirm = actionsAndUsagesRemovedResult.affectedActionsNames.length === 0;
      if (!confirm) {
        var pluralizeAction;
        var pluralizeUsage = "";
        var concernedActions = "";
        for (var i = 0; i < actionsAndUsagesRemovedResult.affectedActionsNames.length; i++) {
          concernedActions += actionsAndUsagesRemovedResult.affectedActionsNames[i];
          if (i !== actionsAndUsagesRemovedResult.affectedActionsNames.length - 1) {
            if (actionsAndUsagesRemovedResult.affectedActionsNames.length === 2) {
              concernedActions += " " + $scope.messages.and + " ";
            } else {
              concernedActions += ", ";
            }
          }
        }
        pluralizeAction = "- "
            + (actionsAndUsagesRemovedResult.affectedActionsNames === 1 ? $scope.messages.removeSpeciesUsagesWarning1 : $scope.messages.removeSpeciesUsagesWarningSeveral).replace('{}', concernedActions)
            + "\n\n";

        if (actionsAndUsagesRemovedResult.nbUsageRemoved > 0) {
          pluralizeUsage = actionsAndUsagesRemovedResult.nbUsageRemoved > 1 ?
              "- " + actionsAndUsagesRemovedResult.nbUsageRemoved + " " + $scope.messages.removeSpeciesInputDeletedSeveral : "- 1 " + $scope.messages.removeSpeciesInputDeleted1;
        }
        warningMessage = pluralizeAction + pluralizeUsage;
      }
      return warningMessage;
    };

    $scope.getSelectedWineValorisations = function (action) {
      var wineValorisations = [];
      angular.forEach(action.selectedWineValorisations, function (result, wineValorisation) {
        if (result) {
          wineValorisations.push(wineValorisation);
        }
      }
      );
      return wineValorisations;
    };

    $scope.selectWineValorisation = function (action, key) {

      var newValue = action.selectedWineValorisations[key];

      var rollbackContext = $scope._createRollbackActionContext(action, $scope.loadDestinationsContext);
      rollbackContext.rollBackAction.selectedWineValorisations[key] = !newValue;

      action.wineValorisations = $scope.getSelectedWineValorisations(action);

      var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades($scope.editedIntervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);

      $scope._loadDestinationsContext(speciesStadeSpecies, $scope.editedIntervention, action, rollbackContext);
    };

    $scope._addSpeciesLabelToMainValorisations = function (action, label) {
      angular.forEach(action.mainValorisations, function (mainValorisation) {
        mainValorisation.displayName += "- " + label + "<br>";
      });
    };

    var _addMainValorisationSpeciesDisplayName = function (species, action) {
      var label = $scope._getSpeciesLabelForSpecies(species);
      $scope._addSpeciesLabelToMainValorisations(action, label);
    };

    $scope._addSpeciesToHarvestingAction = function (intervention, harvestingAction, species) {
      if (intervention && harvestingAction && species) {
        var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);

        var addSpeciesToHarvestingActionProcess = {
          process: function (intervention, action) {
            // only add the species if the action has been detailed
            if (action.speciesValorisations && action.speciesValorisations.length > 0) {
              _addMainValorisationSpeciesDisplayName(species, action);
              if (action.isMixVarietyOrSpecies) {
                $scope._createValorisationForSpecies(action, species);
                var infoMessage = $scope.messages.warningAddHarvestSpecies;
                _displayInfoDialog($scope, $("#infoDialog"), 400, infoMessage, { process: function () { } });
              }
              action.valorisationDtos = action.speciesValorisations;
            }

            $scope.speciesStadeBySpeciesCode = $scope._getSpeciesStadeBySpeciesCode(intervention);

            $scope._loadInterventionDestinationContext(intervention);
          }
        };

        $scope.addSpeciesToHarvestingAction = true;
        var rollbackContext = null;
        $scope._loadDestinationsContext(speciesStadeSpecies, intervention, harvestingAction, rollbackContext, addSpeciesToHarvestingActionProcess);

      }

    };

    $scope._getRemoveSpeciesToHactionProcessFunction = function (actionsAndUsagesRemovedResult, intervention) {
      var removeSpeciesStadeSpeciesProcessModel = {
        process: function () {
          delete $scope.warningMessage;
          $scope.speciesStadeBySpeciesCode = $scope._getSpeciesStadeBySpeciesCode($scope.editedIntervention);
        }
      };
      return removeSpeciesStadeSpeciesProcessModel;
    };

    $scope._displayConfirmRemoveSpeciesStadeSpeciesDialog = function (message, originalIntervention, actionsAndUsagesRemovedResult) {
      if (message) {
        $scope.warningMessage = message;

        var removeSpeciesStadeSpeciesModel = {
          process: function () {
            var nextProcess = $scope._getRemoveSpeciesToHactionProcessFunction(actionsAndUsagesRemovedResult);
            var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades($scope.editedIntervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
            var action = actionsAndUsagesRemovedResult.action;
            $scope._loadDestinationsContext(speciesStadeSpecies, $scope.editedIntervention, action, null, nextProcess);
          },
          cancelChanges: function () {
            delete $scope.warningMessage;
            if ($scope.selectedPhaseConnectionOrNode || $scope.selectedPhaseConnectionOrNode.interventions) {
              var index = $scope.selectedPhaseConnectionOrNode.interventions.indexOf($scope.editedIntervention);
              if (index !== -1) {

                $scope.selectedPhaseConnectionOrNode.interventions[index] = originalIntervention;

                $scope.editedIntervention = originalIntervention;

                $scope.speciesStadeBySpeciesCode = $scope._getSpeciesStadeBySpeciesCode($scope.editedIntervention);

                $scope._loadInterventionDestinationContext($scope.editedIntervention);
              }
            }
          }
        };

        _displayConfirmDialog($scope, $("#confirmDialog"), 400, removeSpeciesStadeSpeciesModel);
      } else {
        $scope._loadInterventionDestinationContext($scope.editedIntervention);
      }

    };

    // select or unselect species
    $scope.selectInterventionSpecies = function (species, force) {

      $scope.stopEditSpeciesStades();

      if (!$scope.editedIntervention.speciesStadesDtos) {
        $scope.editedIntervention.speciesStadesDtos = [];
      }

      // look for SpeciesStades referencing the selected species
      // if found the SpeciesStades is removed, if not it is added
      var speciesStades = $scope.getSpeciesStades(species);

      // add
      if (!speciesStades) {

        speciesStades = {};
        speciesStades.speciesCode = species.code;
        $scope.editedIntervention.speciesStadesDtos.push(speciesStades);
        $scope.speciesStadeBySpeciesCode[species.code] = speciesStades;

        var action = $scope._getEditedInterventionHarvestingAction();
        $scope._addSpeciesToHarvestingAction($scope.editedIntervention, action, species);

      } else if (!force) {// remove

        var originalIntervention = angular.copy($scope.editedIntervention);
        // remove species stades from intervention
        var index = $scope.editedIntervention.speciesStadesDtos.indexOf(speciesStades);
        $scope.editedIntervention.speciesStadesDtos.splice(index, 1);
        delete $scope.speciesStadeBySpeciesCode[species.code];

        var originalSpeciesStades = originalIntervention.speciesStadesDtos;

        // remove action content related to the removed species and compute impact in usages
        var actionsAndUsagesRemovedResult = $scope._removeSpeciesFromAction($scope.editedIntervention, originalSpeciesStades, species, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
        // remove impacted usages
        $scope._removeNotValidUsages($scope.editedIntervention, actionsAndUsagesRemovedResult, species);
        // valid removed
        var warningMassage = $scope._getWarningMessageOnRemoveActionsAndUsages(actionsAndUsagesRemovedResult);

        $scope._displayConfirmRemoveSpeciesStadeSpeciesDialog(warningMassage, originalIntervention, actionsAndUsagesRemovedResult);

      }

      return speciesStades;
    };

    $scope.isSpeciesCheckboxDisabled = function(speciesCode) {
      return $scope.editedIntervention &&
        ($scope.editedIntervention.intermediateCrop ||
          $scope.editedIntervention.actionDtos
            .filter(action => action.valorisationDtos)
            .flatMap(action => action.valorisationDtos.map(valorisation => valorisation.speciesCode))
            .includes(speciesCode)
        );
    };

    $scope.isIntermediateSpeciesCheckboxDisabled = function(speciesCode) {
      return $scope.editedIntervention &&
        ($scope.editedIntervention.actionDtos
            .filter(action => action.valorisationDtos)
            .flatMap(action => action.valorisationDtos.map(valorisation => valorisation.speciesCode))
            .includes(speciesCode)
        );
    };

    // compute work rate from tools coupling
    // the lowest work rate is retain
    $scope._computeToolsCouplingsWorkRate = function (intervention) {

      var toolsCouplingWorkRateData = { workRateUnit: "H_HA" };// default values

      if (intervention.toolsCouplingCodes.length === 1) {
        var localToolsCoupling = _toolsCouplingCodeToToolsCoupling[intervention.toolsCouplingCodes[0]];
        if (localToolsCoupling) {
          toolsCouplingWorkRateData.workforce = localToolsCoupling.workforce;
          toolsCouplingWorkRateData.workRate = localToolsCoupling.workRate;
          toolsCouplingWorkRateData.workRateUnit = localToolsCoupling.workRateUnit;
          toolsCouplingWorkRateData.transitVolume = localToolsCoupling.transitVolume;
          toolsCouplingWorkRateData.materielTransportUnit = localToolsCoupling.materielTransportUnit;
        }
      }

      return toolsCouplingWorkRateData;
    };

    $scope._setupFlag = function (intervention) {
      if (intervention) {
        if ((!intervention.toolsCouplingCodes) || (intervention.toolsCouplingCodes.length === 0)) {
          $scope.userSetValues = {
            workRate: angular.isDefined(intervention.workRate),
            transitVolume: angular.isDefined(intervention.transitVolume),
            involvedPeopleNumber: angular.isDefined(intervention.involvedPeopleNumber) ||
              angular.isDefined(intervention.involvedPeopleCount)
          };
        } else {
          var toolsCouplingWorkRateData = $scope._computeToolsCouplingsWorkRate(intervention);
          // if user value are the same as tools coupling one consider it as not manually defined
          $scope.userSetValues = {
            workRate: (intervention.workRate !== toolsCouplingWorkRateData.workRate ||
              intervention.workRateUnit !== toolsCouplingWorkRateData.workRateUnit),
            transitVolume: (intervention.transitVolume !== toolsCouplingWorkRateData.transitVolume ||
              toolsCouplingWorkRateData.transitVolumeUnit),
            involvedPeopleNumber: (intervention.involvedPeopleNumber !== toolsCouplingWorkRateData.workforce && //practiced
              intervention.involvedPeopleCount !== toolsCouplingWorkRateData.workforce) // effective
          };

        }
      }
    };

    $scope.getInterventionTypeTooltip = function (type) {
      switch (type) {
        case "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES":
          return $scope.messages.interventionTypeTooltipPhyto;
        case "LUTTE_BIOLOGIQUE":
          return $scope.messages.interventionTypeTooltipLutteBio;
      }
    };

    $scope._loadToolsCouplings = function (intervention, initialLoading) {
      $scope.editedInterventionSelectedToolCouplings = {};
      if (intervention.type) {
        _toolsCouplingCodeToToolsCoupling = {};
        var query = $scope.getItkIdentifierQuery();
        return $http.post(
          ENDPOINTS.loadToolsCouplingJson,
          query +
          "&agrosystInterventionTypes=" + angular.toJson([intervention.type]),
          { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
        ).then(function(response) {
          $scope.toolsCouplingsByType = response;
        }).catch(function (response) {
            $scope.toolsCouplings = [];
            var message = $scope.messages.toolscouplingLoadingFailed;
            console.error(message, response);
            addPermanentError(message, response.status);
          });

      }
      return $q.when();
    };

    $scope.loadToolsCouplings = function (intervention, initialLoading) {
      $scope._loadToolsCouplings(intervention).
        then($scope._addLoadedToolsCoupling.bind($scope, intervention, initialLoading)).
        then($scope._displayNoSelectedToolsCoupling.bind($scope, intervention));
        ;
    };

    $scope._bindToolsCouplingDataToIntervention = function (intervention) {

      var toolsCouplingWorkRateData = $scope._computeToolsCouplingsWorkRate(intervention);
      if (toolsCouplingWorkRateData) {
        if (!$scope.userSetValues.workRate) {
          intervention.workRate = toolsCouplingWorkRateData.workRate;
          intervention.workRateUnit = toolsCouplingWorkRateData.workRateUnit;
        }
        if (!$scope.userSetValues.transitVolume) {
          intervention.transitVolume = toolsCouplingWorkRateData.transitVolume;
          intervention.transitVolumeUnit = toolsCouplingWorkRateData.materielTransportUnit;
        }
        if (!$scope.userSetValues.involvedPeopleNumber) {
          intervention.involvedPeopleNumber = toolsCouplingWorkRateData.workforce;//practiced
          intervention.involvedPeopleCount = toolsCouplingWorkRateData.workforce;//effective
        }
      } else {
        // default value
        delete intervention.workRate;
        intervention.workRateUnit = "H_HA";
      }
    };

    $scope._getTableOfObjectIfNotTable = function (object) {
      var result = [];
      if (!(Array.isArray(object))) {
        result.push(object);
      } else {
        result = object;
      }
      return result;
    };

    $scope._displayRemoveToolsCouplingActionDialog = function (toolsCouplingsChangesImpacts) {
      var confirmMessage = null;

      if (toolsCouplingsChangesImpacts) {

        if (toolsCouplingsChangesImpacts.actionsToRemove.length) {
          var message = "<br>" + $scope.messages.warning + "! ";
          if (toolsCouplingsChangesImpacts.isSelectedToolsCouplings) {
            message += $scope.messages.removeToolsCouplingSelectedToolsCouplings + " ";
          }
          if (toolsCouplingsChangesImpacts.actionsToRemove.length === 1) {
            message += $scope.messages.removeToolsCouplingActionsToRemoveOne + "\n";
            message += "<br>" + $scope.messages.removeToolsCouplingActionsConcernedOne + ":<br> - " + toolsCouplingsChangesImpacts.actionsToRemove[0].mainActionReference_label + "\n";

          } else {
            message += $scope.messages.removeToolsCouplingActionsToRemoveSeveral + "\n";
            message += "<br>" + $scope.messages.removeToolsCouplingActionsConcernedSeveral + ":<br>";
            angular.forEach(toolsCouplingsChangesImpacts.actionsToRemove, function (action) {
              message += "<li>" + action.mainActionReference_label + "</li>";
            });
            message += "<br>";
          }
        }

        if (toolsCouplingsChangesImpacts.usagesToRemove.length === 1) {
          message += "1 intrant porte sur une action à supprimer. Celui-ci sera également supprimé.\n";

        } else if (toolsCouplingsChangesImpacts.usagesToRemove.length > 1) {
          message += toolsCouplingsChangesImpacts.usagesToRemove.length + " intrants portent sur une action à supprimer. Ceux-ci seront également supprimés.\n";
        }

        if (toolsCouplingsChangesImpacts.actionsToMigrate.length) {
          message += "<br>" + $scope.messages.removeToolsCouplingSelectedToolsCouplings + " ";
          if (toolsCouplingsChangesImpacts.actionsToMigrate.length === 1) {
            message += $scope.messages.removeToolsCouplingActionsToMigrateOne + "\n";
            message += "<br>" + $scope.messages.removeToolsCouplingActionsConcernedOne + ":\n";
            message += "<li>" + toolsCouplingsChangesImpacts.actionsToMigrate[0].mainActionReference_label + "</li>";

          } else {
            message += $scope.messages.removeToolsCouplingActionsToMigrateSeveral + "\n";
            message += "<br>" + $scope.messages.removeToolsCouplingActionsConcernedSeveral + ":\n";
            angular.forEach(toolsCouplingsChangesImpacts.actionsToMigrate, function (action) {
              message += "<li>" + action.mainActionReference_label + "</li>";
            });
          }
        }

        if (toolsCouplingsChangesImpacts.actionsToRemove.length > 0 || toolsCouplingsChangesImpacts.actionsToMigrate.length > 0) {
          confirmMessage = message;
        }
      }
      return confirmMessage;
    };

    var displayInterventionToolsCouplingMainActionsAdded = function (mainActionInterventionAgrosyst, valid) {
      if (valid) {
        addSuccessMessage($scope.messages.mainActionAdded + ": " + $scope.i18n.AgrosystInterventionType[mainActionInterventionAgrosyst]);
      } else {
        addPermanentWarning($scope.messages.mainActionAdded + ": " + $scope.i18n.AgrosystInterventionType[mainActionInterventionAgrosyst] + " (" + $scope.messages.mainActionAddedMissingInfo + ") !");
      }
    };

    //    $scope._moveActionFromPreviousToolsCouplingToNewlySelectedOne = function(concernedActions, toolsCoupling) {
    //        angular.forEach(concernedActions.actionsToMove, function(action){
    //          action.toolsCouplingCode = toolsCoupling.code;
    //        });
    //    };

    // type = main or secondary
    $scope._getInterventionActionsByType = function (intervention) {
      var result = { MAIN: [], SECONDARY: [] };
      if (intervention.actionDtos) {
        angular.forEach(intervention.actionDtos, function (action) {
          if (action.toolsCouplingCode) {
            var indexOf = intervention.toolsCouplingCodes.indexOf(action.toolsCouplingCode);
            if (indexOf !== -1) {
              result.MAIN.push(action);
            } else {
              delete action.toolsCouplingCode;
              result.SECONDARY.push(action);
            }
          } else {
            result.SECONDARY.push(action);
          }
        });
      }
      return result;
    };

    $scope._addSecondariesActions = function (secondariesActions, availableActionTypes, toolsCouplingsChangesImpacts) {
      angular.forEach(secondariesActions, function (secondaryAction) {
        if (availableActionTypes[secondaryAction.mainActionInterventionAgrosyst]) {
          toolsCouplingsChangesImpacts.newInterventionActions.push(secondaryAction);
          // action is added and action type is no more available except for action type: ENTRETIEN_TAILLE_VIGNE_ET_VERGER, in that case many actions with this type can be added.
          if (secondaryAction.mainActionInterventionAgrosyst !== "ENTRETIEN_TAILLE_VIGNE_ET_VERGER" || secondaryAction.mainActionInterventionAgrosyst !== "TRAVAIL_DU_SOL") {
            delete availableActionTypes[secondaryAction.mainActionInterventionAgrosyst];
          }
        }
      });
    };

    $scope._getOriginalMainActionByAgrosystInterventionType = function (originalMainActions) {
      var originalMainActionByAgrosystInterventionType = {};
      if (originalMainActions) {
        angular.forEach(originalMainActions, function (originalMainAction) {
          // it can have many actions with type "ENTRETIEN_TAILLE_VIGNE_ET_VERGER" so for this type there is a collection of actions
          if (originalMainAction.mainActionInterventionAgrosyst === "ENTRETIEN_TAILLE_VIGNE_ET_VERGER" || originalMainAction.mainActionInterventionAgrosyst === "TRAVAIL_DU_SOL") {
            var mainActions = originalMainActionByAgrosystInterventionType[originalMainAction.mainActionInterventionAgrosyst];
            if (!mainActions) {
              mainActions = [];
            }
            mainActions.push(originalMainAction);
            originalMainActionByAgrosystInterventionType[originalMainAction.mainActionInterventionAgrosyst] = mainActions;
          } else {
            originalMainActionByAgrosystInterventionType[originalMainAction.mainActionInterventionAgrosyst] = originalMainAction;
          }
        });
      }
      return originalMainActionByAgrosystInterventionType;
    };

    $scope._getSecondaryTillageActionByReferenceCode = function (toolsCouplingsChangesImpacts) {
      var secondaryTillageActionByReferenceCode;
      if (toolsCouplingsChangesImpacts && toolsCouplingsChangesImpacts.newInterventionActions) {
        angular.forEach(toolsCouplingsChangesImpacts.newInterventionActions, function (action) {
          // it can have many actions with type "ENTRETIEN_TAILLE_VIGNE_ET_VERGER" so for this type there is a collection of actions
          if (TYPES_ALLOWING_3_ACTIONS.includes(action.mainActionInterventionAgrosyst)) {
            if (!secondaryTillageActionByReferenceCode) {
              secondaryTillageActionByReferenceCode = {};
            }
            secondaryTillageActionByReferenceCode[action.mainActionReference_code] = action;
          }
        });
      }
      return secondaryTillageActionByReferenceCode;
    };

    $scope._createNewAction = function (intervention, actionType, mainAction, toolsCouplingCode) {
      var newAction = {
        topiaId: "NEW-ACTION-" + guid(),
        mainActionInterventionAgrosyst: actionType,
        mainActionId: mainAction.topiaId,
        mainActionReference_label: mainAction.reference_label_Translated,
        mainActionReference_code: mainAction.reference_code,
        toolsCouplingCode: toolsCouplingCode
      };
      $scope._initAction(intervention, newAction);
      return newAction;
    };

    $scope._migrateOrAddTillageAction = function (intervention, mainAction, originalMainActions, originalMainActionByAgrosystInterventionType, toolsCouplingsChangesImpacts, toolsCoupling) {
      // migration is done if same reference_code otherwise action is added
      var originalTillageActions = originalMainActionByAgrosystInterventionType[mainAction.intervention_agrosyst];
      var secondaryTillageActionsByRefCode = $scope._getSecondaryTillageActionByReferenceCode(toolsCouplingsChangesImpacts);
      var newAction;
      if (originalTillageActions || secondaryTillageActionsByRefCode) {

        // if a same secondary tillage action exists as secondary then don't try do migrate are add a new one
        var doContinue = true;
        if (secondaryTillageActionsByRefCode) {
          var secondaryAction = secondaryTillageActionsByRefCode[mainAction.reference_code];
          doContinue = !angular.isDefined(secondaryAction);
        }

        // no secondary tillage action has been found, try to find main action to migrate
        // if none add a new main action
        if (doContinue) {
          var migratedTillageAction;
          if (originalTillageActions) {
            angular.forEach(originalTillageActions, function (originalTillageAction) {
              if (originalTillageAction.mainActionReference_code === mainAction.reference_code) {
                migratedTillageAction = originalTillageAction;
              }
            });
          }

          // migration is possible then add it to migrations otherwise add it to new main actions
          if (migratedTillageAction) {
            toolsCouplingsChangesImpacts.actionsToMigrate.push(migratedTillageAction);
            var index = originalMainActions.indexOf(migratedTillageAction);
            if (index !== -1) {
              originalMainActions.splice(index, 1);
            }
          } else {
            newAction = $scope._createNewAction(intervention, mainAction.intervention_agrosyst, mainAction, toolsCoupling.code);
            toolsCouplingsChangesImpacts.actionsToAdd.push(newAction);
          }
        }

      } else {
        // there were no original mains actions so they all have to be added
        // add action if possible (possible if Agrosyst intervention type is allowed)
        newAction = $scope._createNewAction(intervention, mainAction.intervention_agrosyst, mainAction, toolsCoupling.code);
        toolsCouplingsChangesImpacts.actionsToAdd.push(newAction);
      }
    };

    $scope._migrateOrAddAction = function (
      intervention,
      mainAction,
      originalMainActions,
      originalMainActionByAgrosystInterventionType,
      availableActionTypes,
      toolsCouplingsChangesImpacts,
      toolsCoupling) {

      // migrate actions if possible (possible if Agrosyst intervention type is allowed)
      var actionToMigrate = originalMainActionByAgrosystInterventionType[mainAction.intervention_agrosyst];
      var newAction;

      if (actionToMigrate) {
        if (availableActionTypes[actionToMigrate.mainActionInterventionAgrosyst]) {
          var isAlreadyMigrate = toolsCouplingsChangesImpacts.actionsToMigrate.indexOf(actionToMigrate);
          if (isAlreadyMigrate === -1) {
            toolsCouplingsChangesImpacts.actionsToMigrate.push(actionToMigrate);
            var index = originalMainActions.indexOf(actionToMigrate);
            if (index !== -1) {
              originalMainActions.splice(index, 1);
            }
            delete availableActionTypes[actionToMigrate.mainActionInterventionAgrosyst];
          } else {
            // already migrated action, so add new one
            newAction = $scope._createNewAction(intervention, mainAction.intervention_agrosyst, mainAction, toolsCoupling.code);
            toolsCouplingsChangesImpacts.actionsToAdd.push(newAction);
            delete availableActionTypes[mainAction.intervention_agrosyst];
          }
        }
      }
      // add action if possible (possible if Agrosyst intervention type is allowed)
      else {
        if (availableActionTypes[mainAction.intervention_agrosyst]) {
          newAction = $scope._createNewAction(intervention, mainAction.intervention_agrosyst, mainAction, toolsCoupling.code);
          toolsCouplingsChangesImpacts.actionsToAdd.push(newAction);
          // action is added and action type is no more available except for action type: ENTRETIEN_TAILLE_VIGNE_ET_VERGER, in that case many actions with this type can be added.
          delete availableActionTypes[mainAction.intervention_agrosyst];
        }
      }
    };

    $scope._addMainActions = function (intervention, toolsCoupling, originalMainActions, availableActionTypes, toolsCouplingsChangesImpacts) {
      // if no toolsCoupling then all mains actions have to be removed
      // if originalMainAction exists in the newly added tools coupling then there are migrated (actions are associated to the new tools coupling)
      // if originalMainAction doesn't exist in the newly added tools coupling then it has to be removed
      // if main action from tools coupling already exists for the intervention (as secondary) it is not added and the original action is not move from secondary to main.
      if (!toolsCoupling) {
        toolsCouplingsChangesImpacts.actionsToRemove = originalMainActions;
      } else {
        const originalMainActionByAgrosystInterventionType = $scope._getOriginalMainActionByAgrosystInterventionType(originalMainActions);
        const allToolsCouplings = $scope._getTableOfObjectIfNotTable(toolsCoupling);
        allToolsCouplings.forEach(toolsCoupling => {
          if (toolsCoupling.mainsActions) {
            toolsCoupling.mainsActions.forEach(mainAction => {
              const interventionType = mainAction.intervention_agrosyst;
              if (TYPES_ALLOWING_3_ACTIONS.includes(interventionType)) {
                $scope._migrateOrAddTillageAction(intervention, mainAction, originalMainActions, originalMainActionByAgrosystInterventionType, toolsCouplingsChangesImpacts, toolsCoupling);
              } else {
                $scope._migrateOrAddAction(intervention, mainAction, originalMainActions, originalMainActionByAgrosystInterventionType, availableActionTypes, toolsCouplingsChangesImpacts, toolsCoupling);
              }
            });
          }
        });

        // actions that could be migrated have been removed from originalMainActions.
        toolsCouplingsChangesImpacts.actionsToRemove = originalMainActions;
      }
    };

    $scope._computeUsagesToRemove = function (intervention, actionsToRemove) {
      var result = [];
      if (intervention && actionsToRemove && actionsToRemove.length > 0) {
        angular.forEach(actionsToRemove, function (actionsToRemove) {
          angular.forEach(intervention.usages, function (usage) {
            if (actionsToRemove.mainActionInterventionAgrosyst === usage.inputType) {
              result.push(usage);
            }
          });
        });
      }
      return result;
    };

    $scope._setInterventionName = function (intervention, toolsCoupling) {
      if ((!intervention.name || !intervention.name.trim()) && toolsCoupling) {
        intervention.name = toolsCoupling.toolsCouplingName;
      }
    };

    $scope._getActionsByIds = function (actions) {
      var newActionsByIds = {};
      angular.forEach(actions, function (action) {
        newActionsByIds[action.topiaId] = action;
      });
      return newActionsByIds;
    };

    // set actions change to intervention
    // propagate chang on usages
    // set tools coupling to the intervention
    // set intervention name if required
    $scope._processToolsCouplingsChanges = function (intervention, toolsCouplingsChangesImpacts, selectedToolsCouplings) {

      // reset auto set toolsCoupling values, keep user ones
      intervention.workRate = $scope.userSetValues.workRate ? intervention.workRate : null;
      intervention.workRateUnit = $scope.userSetValues.workRate ? intervention.workRateUnit : null;
      intervention.transitVolume = $scope.userSetValues.transitVolume ? intervention.transitVolume : null;
      intervention.transitVolumeUnit = $scope.userSetValues.transitVolume ? intervention.transitVolumeUnit : null;
      intervention.involvedPeopleNumber = $scope.userSetValues.involvedPeopleNumber ? intervention.involvedPeopleNumber : null;//practiced
      intervention.involvedPeopleCount = $scope.userSetValues.involvedPeopleNumber ? intervention.involvedPeopleCount : null;//effective

      // actions migration
      angular.forEach(toolsCouplingsChangesImpacts.actionsToMigrate, function (actionToMigrate) {
        // if there are actionsToMigrate then there is a selectedToolsCouplings
        actionToMigrate.toolsCouplingCode = selectedToolsCouplings.code;
        toolsCouplingsChangesImpacts.newInterventionActions.push(actionToMigrate);
      });

      // add new mains actions
      angular.forEach(toolsCouplingsChangesImpacts.actionsToAdd, function (actionToAdd) {
        toolsCouplingsChangesImpacts.newInterventionActions.push(actionToAdd);
        var errorMessages = $scope.validateAction(intervention, actionToAdd);
        var valid = errorMessages.length === 0;
        displayInterventionToolsCouplingMainActionsAdded(actionToAdd.mainActionInterventionAgrosyst, valid);
      });

      // replace intervention's actions.
      intervention.actionDtos = toolsCouplingsChangesImpacts.newInterventionActions;

      // remove usages
      angular.forEach(toolsCouplingsChangesImpacts.usagesToRemove, function (usageToRemove) {
        var index = intervention.usages.indexOf(usageToRemove);
        if (index !== -1) {
          intervention.usages.splice(index, 1);
        }
      });

      var newActionsByIds = $scope._getActionsByIds(intervention.actionDtos);
      var usages = [];
      angular.forEach(intervention.usages, function (usage) {
        var isValid = $scope._replaceUsageAction(newActionsByIds, usage, null);
        if (isValid) {
          usages.push(usage);
        }
      });
      intervention.usages = usages;

      // since #5568 only one tools coupling can be selected
      // set tools coupling to intervention
      $scope.editedInterventionSelectedToolCouplings = {};
      intervention.toolsCouplingCodes = [];
      if (selectedToolsCouplings) {
        intervention.toolsCouplingCodes.push(selectedToolsCouplings.code);
        $scope.editedInterventionSelectedToolCouplings[selectedToolsCouplings.code] = true;
        $scope._setInterventionName(intervention, selectedToolsCouplings);
        $scope._bindToolsCouplingDataToIntervention(intervention);
      }
      $scope._displayNoSelectedToolsCoupling();

    };

    $scope.displayConfirmToolsCouplingSelectionDialog = function (message, intervention, toolsCouplingsChangesImpacts, selectedToolsCouplings) {
      if (message) {
        $scope.warningMessage = message;

        var toolsCouplingChangeModel = {
          process: function () {
            delete $scope.warningMessage;

            //newInterventionActions
            $scope._processToolsCouplingsChanges(intervention, toolsCouplingsChangesImpacts, selectedToolsCouplings);

            $scope._checkAvailableActionAndInputTypes(intervention);
          },
          cancelChanges: function () {
            delete $scope.warningMessage;

            // rollback checkbox
            $scope.editedInterventionSelectedToolCouplings = {};

            angular.forEach(intervention.toolsCouplingCodes, function (toolsCouplingCode) {

              $scope.editedInterventionSelectedToolCouplings[toolsCouplingCode] = true;

            });

            $scope._displayNoSelectedToolsCoupling();

            $scope._checkAvailableActionAndInputTypes(intervention);
          }
        };

        _displayConfirmDialog($scope, $("#confirmDialog"), 400, toolsCouplingChangeModel);
      }

    };


    $scope._selectToolsCoupling = function (intervention, toolsCoupling) {
      var toolsCouplingsChangesImpacts = { isSelectedToolsCouplings: false, newInterventionActions: [], actionsToRemove: [], actionsToMigrate: [], actionsToAdd: [] };

      var selectedToolsCouplings;

      var indexOf = intervention.toolsCouplingCodes.indexOf(toolsCoupling.code);
      if (indexOf === -1) {
        selectedToolsCouplings = toolsCoupling;
        toolsCouplingsChangesImpacts.isSelectedToolsCouplings = true;
      }

      // get intervention mains actions and secondary actions
      var actionsByType = $scope._getInterventionActionsByType(intervention);

      var availableActionTypes = angular.copy($scope.agrosystInterventionTypes);

      // add secondaries actions
      $scope._addSecondariesActions(actionsByType.SECONDARY, availableActionTypes, toolsCouplingsChangesImpacts);

      // add mains actions
      $scope._addMainActions(intervention, selectedToolsCouplings, actionsByType.MAIN, availableActionTypes, toolsCouplingsChangesImpacts);

      // look for usages to remove
      toolsCouplingsChangesImpacts.usagesToRemove = $scope._computeUsagesToRemove(intervention, toolsCouplingsChangesImpacts.actionsToRemove);

      var confirmMessage = $scope._displayRemoveToolsCouplingActionDialog(toolsCouplingsChangesImpacts);

      if (confirmMessage) {
        $scope.displayConfirmToolsCouplingSelectionDialog(confirmMessage, intervention, toolsCouplingsChangesImpacts, selectedToolsCouplings);
      } else {
        //newInterventionActions
        $scope._processToolsCouplingsChanges(intervention, toolsCouplingsChangesImpacts, selectedToolsCouplings);

        $scope._checkAvailableActionAndInputTypes(intervention);
      }
    };

    $scope._setFormDirty = function () {
      if ($scope.effectiveCropCyclesEditForm) {
        $scope.effectiveCropCyclesEditForm.$setDirty();
      }
      if ($scope.practicedSystemForm) {
        $scope.practicedSystemForm.$setDirty();
      }
    };

    // select or deselect tools coupling
    $scope.toggleSelectedInterventionToolsCoupling = function (intervention, toolsCoupling) {
      // add the new tools coupling if it was not already added
      $scope._selectToolsCoupling(intervention, toolsCoupling);

      // add work rate, transitVolume and involvedPeopleNumber
      $scope._bindToolsCouplingDataToIntervention(intervention);

      // compute spendingTime
      $scope._computeSpendingTime(intervention);

      $scope._setFormDirty();

    };

    $scope.getSeedingSpecies = function (species) {
      // whe load the seedingSpeciesAction concerned by the species given in parameter
      // the seedingSpeciesAction if find according the species code.

      var seedingSpeciesAction = false;
      angular.forEach($scope.editedAction.seedingSpecies, function (actionSeedingSpeciesAction) {
        if (actionSeedingSpeciesAction.speciesCode === species.code) {
          seedingSpeciesAction = actionSeedingSpeciesAction;
        }
      });

      return seedingSpeciesAction;
    };

    $scope.addSeedingProductActions = function () {
      $scope.editedProductAction = {};
      if (angular.isUndefined($scope.editedAction.seedingProductActionDtos)) {
        $scope.editedAction.seedingProductActionDtos = [];
      }
      $scope.editedAction.seedingProductActionDtos.push($scope.editedProductAction);
    };

    $scope.editSeedingSpecies = function (species) {
      delete $scope.editedSeedingSpeciesAction;
      if (!$scope.editedAction.seedingSpecies) {
        $scope.editedAction.seedingSpecies = [];
      }

      angular.forEach($scope.editedAction.seedingSpecies, function (seedingSpeciesAction) {
        if (seedingSpeciesAction.speciesCode === species.code) {
          $scope.editedSeedingSpeciesAction = seedingSpeciesAction;
        }
      });

      // in case the species was not selected for this intervention it added.
      if (angular.isUndefined($scope.editedSeedingSpeciesAction)) {
        $scope.editedSeedingSpeciesAction = {};
        $scope.editedSeedingSpeciesAction.speciesCode = species.code;
        $scope.editedAction.seedingSpecies.push($scope.editedSeedingSpeciesAction);
      }
    };

    $scope.deleteSeedingProductActions = function (editedProductAction) {
      var indexOf = $scope.editedAction.seedingProductActionDtos.indexOf(editedProductAction);
      if (indexOf !== -1) {
        if (editedProductAction.refPcActaProductNameTmp) {
          $scope.productName = editedProductAction.refPcActaProductNameTmp;
        } else {
          $scope.productName = "";
        }
        var deleteSeedingProductModel = {
          process: function () {
            $scope.editedAction.seedingProductActionDtos.splice(indexOf, 1);
            delete $scope.editedProductAction;
          },
          cancelChanges: function () { }
        };
        _displayConfirmDialog($scope, $("#confirmRemovedProduct"), 400, deleteSeedingProductModel);
      }
    };

    $scope.stopEditSelectedSeedingObject = function () {
      $scope.actaTreatmentCode = "";
      if ($scope.editedProductAction) {
        delete $scope.editedProductAction;
      }
      if ($scope.editedSeedingSpeciesAction) {
        delete $scope.editedSeedingSpeciesAction;
      }
    };

    /*********************************************/
    /*                   Actions                 */
    /*********************************************/
    // _initAction is made on Intervention part

    // to be able to revert change
    //$scope.copieOfEditedValorisation;
    // action concernée pour l'affichage des espèces concernées de l'intrant.
    //$scope.seedingAction;

    // valide l'action suivant sont type et retourne vrai si l'action est valide
    $scope.validateAction = function (intervention, action, isCurrentAction) {
      var errorMessages = [];

      // Proportion de surface traitée
      // Volume de bouillie par ha
      if (action.mainActionInterventionAgrosyst === "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES") {
        if (!action.proportionOfTreatedSurface || action.proportionOfTreatedSurface <= 0) {
          errorMessages.push($scope.messages.missingProportionOfTreatedSurface);
        }
        if (typeof (action.boiledQuantity) === "undefined" || action.boiledQuantity === null || action.boiledQuantity < 0) {
          errorMessages.push($scope.messages.missingBoiledQuantity);
        }
      }

      // Quantité d'eau apportée
      else if (action.mainActionInterventionAgrosyst === "IRRIGATION") {
        if (!action.waterQuantityAverage) {
          errorMessages.push($scope.messages.missingWaterQuantityAverage);
        }
      }

      // Proportion de surface traitée
      // Volume de bouillie par ha (necessaire)
      else if (action.mainActionInterventionAgrosyst === "LUTTE_BIOLOGIQUE") {
        if (!action.proportionOfTreatedSurface || action.proportionOfTreatedSurface <= 0) {
          errorMessages.push($scope.messages.missingProportionOfTreatedSurface);
        }
      }

      // Rendements
      // Catégorie de rendement
      // Rendement
      // Unité
      else if (action.mainActionInterventionAgrosyst === "RECOLTE") {

        if (action.isWineSpecies && (!action.wineValorisations || (action.wineValorisations && (!Object.keys(action.wineValorisations) || Object.keys(action.wineValorisations).length === 0)))) {
          errorMessages.push($scope.messages.missingWineValorisations);
          return errorMessages;
        }

        if ($scope.editedValorisation && isCurrentAction) {
          errorMessages.push($scope.messages.saveValorisation);
          return errorMessages;
        }

        // when validate action
        if (!action.valorisationDtos || action.valorisationDtos.length === 0) {
          errorMessages.push($scope.messages.missingValorisations);
        }

        // when validate intervention
        if (!$scope.editedValorisation && action.valorisationDtos) {
          var wineDestinationFound = false;
          var globalYealdAverages = [];
          var lastSpecesCode = "";
          var destinationIndex = 0;
          var valorisations = [];

          angular.forEach(action.valorisationDtos, function (valorisation) {
            if (valorisation) {
              if (lastSpecesCode === valorisation.speciesCode) {
                destinationIndex += 1;
              } else {
                destinationIndex = 0;
                lastSpecesCode = valorisation.speciesCode;
              }
              if (globalYealdAverages.length === destinationIndex) {
                globalYealdAverages.push(parseFloat(valorisation.yealdAverage.toFixed(3)));
              } else {
                globalYealdAverages[destinationIndex] = globalYealdAverages[destinationIndex] + parseFloat(valorisation.yealdAverage.toFixed(3));
              }

              valorisations.push(valorisation);
              var sum = valorisation.salesPercent + valorisation.selfConsumedPersent + valorisation.noValorisationPercent;
              if (!((99.0 <= sum) && (sum <= 101.0))) {
                errorMessages.push($scope.messages.valorisationPartsSumError);
              }
              if (!valorisation.destinationId) {
                errorMessages.push($scope.messages.missingDestination);
              }
              if (!valorisation.yealdUnit) {
                errorMessages.push($scope.messages.missingYieldUnit);
              }

              if (valorisation.destination && valorisation.destination !== null) {

                wineDestinationFound = valorisation.destination.code_espece_botanique === "ZMO";
              }
            }
          });

          angular.forEach(globalYealdAverages, function (globalYealdAverage) {
            if (globalYealdAverage === 0.0) {
              errorMessages.push($scope.messages.missingYieldUnit);
            }
          });

          action.valorisationDtos = valorisations;

          if (wineDestinationFound) {
            if (!action.wineValorisations || action.wineValorisations.length === 0) {
              errorMessages.push($scope.messages.declareValorisation);
            }
          }
        }

        if (action.mainValorisations) {
          angular.forEach(action.mainValorisations, function (mainValorisation) {
            var totalYealAverage = $scope._computeMainValorisationYealdAverage(action, mainValorisation);
            if (parseFloat(totalYealAverage.toFixed(3)) === 0.0) {
              errorMessages.push($scope.messages.fixYields);
            }
          });
        }

      }

      // Quantité semée
       else if (!$scope.editedAction && action.mainActionInterventionAgrosyst === "SEMIS") {
         if (action.seedLotInputUsageDtos.length === 0) {
           errorMessages.push($scope.messages.seedingActionMissingSeedLot);
           $scope.seedingActionMissingLotError=$scope.messages.seedingActionMissingSeedLot;
         } else if (intervention && intervention.actionDtos.indexOf(action) !== -1){
           $scope.seedingActionMissingLotError=undefined;
         }
       }

      action.isValid = errorMessages.length === 0;
      return errorMessages;
    };

    $scope._addActionLightBoxAttributes = function (action) {
      if (!action.toolsCouplingCode && !action.topiaId) {
        action.availableActionTypes = $scope.editedIntervention.availableActionTypes;
        action.availableActions = $scope.editedIntervention.availableActions;
      }
    };

    /* Method called to start a new action creation use-case */
    $scope.startAddNewAction = function () {
      $scope._resetEditedAction();

      $scope.editedAction = {};
      // because of problems with light box all necessaries action dependence are push to the edited action
      $scope._addActionLightBoxAttributes($scope.editedAction);

      $scope._showActionLightBox($scope.editedIntervention, $scope.editedAction);
    };

    $scope._clearEditedHarvestingActionContext = function (action) {
      action.mainValorisations = {};
      action.filteredMainValorisations = [];
      action.speciesValorisations = [];
      action.speciesValorisationsBySpeciesCode = {};
      action.nbSameValorisationSectorFromMainValorisation = {};
      action.valorisationsByMainValorisationIds = {};
      action.nbDestinationForSpecies = 0;
      action.selectedWineValorisations = {};
      action.hideDetailedHarvestingActionValorisation = true;
    };

    $scope._setDestinationsUiIds = function (action) {
      for (var destinationLabel in action.destinationsByDestinationLabels) {
        for (var i = 0; i < action.destinationsByDestinationLabels[destinationLabel].length; i++) {
          action.destinationsByDestinationLabels[destinationLabel][i].uiId = action.destinationsByDestinationLabels[destinationLabel][i].topiaId;
        }
      }
    };

    $scope._setupHarvestingActionAttributes = function (action, speciesStadeSpecies, intervention) {
      $scope._clearEditedHarvestingActionContext(action);

      action.nbSpeciesStadeSpeciesLength = speciesStadeSpecies.length;
      action.speciesStadeSpecies = speciesStadeSpecies;
      action.speciesTotalArea = speciesStadeSpecies.reduce((sum, species) => sum + species.speciesArea, 0) || 100;

      action.valorisationsByMainValorisationIds = {};
      action.destinationsByDestinationLabels = $scope.loadDestinationsContext ? $scope.loadDestinationsContext.destinationsByDestinationLabels : {};
      $scope._setDestinationsUiIds(action);

      action.destinationsLength = Object.keys(action.destinationsByDestinationLabels).length;
      action.qualityCriteria = $scope.loadDestinationsContext ? $scope.loadDestinationsContext.qualityCriteria : {};
      action.qualityCriteriaUnits = $scope.criteriaUnits;
      action.hideDetailedHarvestingActionValorisation = true;
      action.selectedWineValorisations = {};
      action.sector = $scope.sector;
      action.organicGsDefined = angular.isDefined($scope.isOrganic) && $scope.isOrganic !== null;
      action.nbDestinationForSpecies = 0;
      action.yealdUnits = $scope.yealdUnits;
      if (!action.valorisationDtos) {
        action.valorisationDtos = [];
      }

      /*
       * Dans le cas des cultures intermédiaires, il faut récupérer l'objet correspondant à
       * cette culture dans $scope.connectionIntermediateCrop. Si on passe par
       * $scope.getCroppingPlanEntry($scope._getSelectedCroppingPlanEntryIdentifier()),
       * on récupère la culture principale ou dérobée.
       */
      let croppingPlanEntry = null;
      if (intervention.targetedCroppingPlanEntriesCode) {
        // cas du copier/coller
        croppingPlanEntry = $scope.getCroppingPlanEntry(intervention.targetedCroppingPlanEntriesCode);
      } else {
        croppingPlanEntry = intervention.intermediateCrop ?
          $scope.connectionIntermediateCrop
          : $scope.getCroppingPlanEntry($scope._getSelectedCroppingPlanEntryIdentifier());
      }


      action.cropType = croppingPlanEntry && croppingPlanEntry.type;
      action.isMixSpecies = croppingPlanEntry && croppingPlanEntry.mixSpecies;
      action.isMixVariety = croppingPlanEntry && croppingPlanEntry.mixVariety;
      action.isMixCompanion = croppingPlanEntry && croppingPlanEntry.mixCompanion;
      action.isMixVarietyOrSpecies = croppingPlanEntry && (croppingPlanEntry.mixSpecies || croppingPlanEntry.mixVariety);
      action.displayName = $scope._getSpeciesStadesSpeciesLabels(speciesStadeSpecies);
      action.filteredMainValorisations = [];
    };

    $scope._setWineValorisationCheckBox = function (action) {
      if (action.wineValorisations) {
        angular.forEach(action.wineValorisations, function (wineValorisation) {
          action.selectedWineValorisations[wineValorisation] = true;
        });
      } else {
        action.wineValorisations = [];
      }
    };

    $scope._checkValorisationIsSameSector = function (speciesByCodes, speciesValorisation, destinationsContext) {
      var result = false;
      var speciesForCode = speciesByCodes.speciesByCode[speciesValorisation.speciesCode];
      if (speciesForCode && speciesValorisation.destination) {
        var sectorForSpecies = $scope._getSectorForSpecies(destinationsContext, speciesForCode);
        result = $scope._computeIsSameSector(sectorForSpecies, speciesValorisation);
      }
      return result;
    };

    $scope._checkIfSpeciesValorisationTargetChange = function (allSpeciesValues, valorisation) {
      var amountChange = false;

      var keys = Object.keys(allSpeciesValues);
      for (var i = 0; i < keys.length; i++) {

        var amountKeys = keys[i];
        var amounts = allSpeciesValues[amountKeys];
        var firstAmount = parseInt(amounts[0]);
        for (var j = 0; j < amounts.length; j++) {
          amountChange = firstAmount !== parseInt(amounts[j]);
          if (amountChange) {
            break;
          }
        }
        if (amountChange) {
          break;
        }
      }

      valorisation.changeMainValorisationTargetAvailable = !amountChange;
    };

    $scope._setMainDestinationValorisationAmounts = function (action, mainValorisation) {
      var valorisationsForDestination = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
      var mainYealdAverage = parseFloat(mainValorisation.yealdAverage);
      var mainSalesPercent = 0;
      var mainSelfConsumedPersent = 0;
      var mainNoValorisationPercent = 0;
      var totalYealdAverage = 0;
      var totalDeclaredPart = 0;

      var allSpeciesValues = {};
      allSpeciesValues.salesPercent = [];
      allSpeciesValues.selfConsumedPersent = [];
      allSpeciesValues.noValorisationPercent = [];

      if (valorisationsForDestination) {
        angular.forEach(valorisationsForDestination, function (valorisationForDestination) {
          var yealdAverage = parseFloat(valorisationForDestination.yealdAverage);
          var part;
          if (!mainYealdAverage || mainYealdAverage === 0) {
            part = 0;
          } else if (action.isMixVarietyOrSpecies) {
            part = yealdAverage / mainYealdAverage;
          } else {
            part = valorisationForDestination.relativeArea;
          }
          totalDeclaredPart += part;

          mainSalesPercent += valorisationForDestination.salesPercent * part;
          mainSelfConsumedPersent += valorisationForDestination.selfConsumedPersent * part;
          mainNoValorisationPercent += valorisationForDestination.noValorisationPercent * part;

          totalYealdAverage += yealdAverage * part / 100;

          allSpeciesValues.salesPercent.push(valorisationForDestination.salesPercent);
          allSpeciesValues.selfConsumedPersent.push(valorisationForDestination.selfConsumedPersent);
          allSpeciesValues.noValorisationPercent.push(valorisationForDestination.noValorisationPercent);
        });
      }

      mainSalesPercent = mainSalesPercent / totalDeclaredPart;
      mainSelfConsumedPersent = mainSelfConsumedPersent / totalDeclaredPart;
      mainNoValorisationPercent = mainNoValorisationPercent / totalDeclaredPart;

      this._checkIfSpeciesValorisationTargetChange(allSpeciesValues, mainValorisation);

      var total = mainSalesPercent + mainSelfConsumedPersent + mainNoValorisationPercent;
      if (total >= 99 && total <= 101) {
        mainValorisation.salesPercent = Math.round(mainSalesPercent);
        mainValorisation.selfConsumedPersent = Math.round(mainSelfConsumedPersent);
        mainValorisation.noValorisationPercent = Math.round(mainNoValorisationPercent);
      } else {
        mainValorisation.salesPercent = 100;
        mainValorisation.selfConsumedPersent = 0;
        mainValorisation.noValorisationPercent = 0;
      }

      if (!action.isMixVarietyOrSpecies) {
        mainValorisation.yealdAverage = totalYealdAverage;
      }

    };

    $scope._addEditValorisationAttributes = function (action, valorisation) {
      valorisation._showYealdMinMediumMax = false;
      valorisation._isValidValorisationPercent = true;
      valorisation._isMainYealdAverageValid = true;
      valorisation._editPart = false;
    };

    $scope._getSpeciesValorisationsBySpeciesCodeToAction = function (valorisation, action) {
      var speciesValorisations = action.speciesValorisationsBySpeciesCode[valorisation.speciesCode];

      if (!speciesValorisations) {
        speciesValorisations = [];
        action.speciesValorisationsBySpeciesCode[valorisation.speciesCode] = speciesValorisations;
      }

      return speciesValorisations;
    };

    $scope._addValorisationToSpeciesValorisations = function (speciesValorisations, species, valorisation, force) {
      if (species || force) {
        speciesValorisations.push(valorisation);
      }
    };

    // main Valorisations are not saved in DB and have to be created when editing the harvesting action
    $scope._getOrCreateMainValorisationFromValorisation = function (intervention, action, valorisation) {
      var destinationId = valorisation.destination.uiId;
      var mainValorisation = action.mainValorisations[destinationId];

      if (!mainValorisation || mainValorisation._speciesValorisationsToCreate[valorisation.speciesCode]) {
        // if there are several valorisations with the same destination and species
        // (could happen before new yeal declaration or in a edaplos import)
        var duplicated = !!mainValorisation;
        var userCreatedValorisation = false;
        mainValorisation = $scope._createMainValorisation(intervention, action, userCreatedValorisation, valorisation.speciesCode);
        mainValorisation.yealdUnit = valorisation.yealdUnit;

        if (duplicated && valorisation.destination) {
          // new label and uiId for the valorisation
          valorisation.destination.uiId = generateUUID();

          var destinationLabel = valorisation.destination.destination_Translated;
          var i = 1;
          while (action.destinationsByDestinationLabels[destinationLabel + " (" + i + ")"]) {
            i++;
          }
          valorisation.destination.destination_Translated = destinationLabel + " (" + i + ")";
          action.destinationsByDestinationLabels[valorisation.destination.destination_Translated] = [valorisation.destination];
        }
        mainValorisation.destination = valorisation.destination;
        mainValorisation.destinationId = valorisation.destination.uiId;
        mainValorisation.destinationName = valorisation.destination ? valorisation.destination.destination_Translated : "";
        mainValorisation.yealdMin = valorisation.yealdMin ? parseFloat(valorisation.yealdMin) : null;
        mainValorisation.yealdMax = valorisation.yealdMax ? parseFloat(valorisation.yealdMax) : null;
        mainValorisation.yealdMedian = valorisation.yealdMedian ? parseFloat(valorisation.yealdMedian) : null;

        mainValorisation.salesPercent = valorisation.salesPercent ? parseInt(valorisation.salesPercent) : 0;
        mainValorisation.selfConsumedPersent = valorisation.selfConsumedPersent ? parseInt(valorisation.selfConsumedPersent) : 0;
        mainValorisation.noValorisationPercent = valorisation.noValorisationPercent ? parseInt(valorisation.noValorisationPercent) : 0;

        action.nbDestinationForSpecies = action.nbDestinationForSpecies + 1;
        action.mainValorisations[mainValorisation.destination.uiId] = mainValorisation;
      }
      return mainValorisation;
    };

    $scope._setValorisationValidStatus = function (action, valorisation) {
      var targetedSpecies = action.speciesByCodes.speciesByCode[valorisation.speciesCode];
      valorisation.isDestinationValidForSpecies = $scope._getIsDestinationValidForSpecies(targetedSpecies, valorisation);
      valorisation.locked = !valorisation.isDestinationValidForSpecies;
    };

    $scope._addValorisationToSpeciesValorisationsByMainValorisationId = function (action, valorisation, mainValorisationId) {
      var mainValorisationSpeciesValorisations = action.valorisationsByMainValorisationIds[mainValorisationId];
      if (!mainValorisationSpeciesValorisations) {
        mainValorisationSpeciesValorisations = [];
        action.valorisationsByMainValorisationIds[mainValorisationId] = mainValorisationSpeciesValorisations;
      }
      mainValorisationSpeciesValorisations.push(valorisation);
    };

    $scope._removeValorisationDestinationIfMainAsNone = function (mainValorisation, valorisation) {
      // possible in case off species changed, destination has to be redefined.
      if (!mainValorisation.destination) {
        valorisation.destination = null;
        delete valorisation.destinationId;
        delete valorisation.destinationName;
      }
    };

    $scope._computeValorisationsYealdAveragePart = function (action, mainValorisationById) {
      angular.forEach(action.valorisationsByMainValorisationIds, function (valorisationsForDestination, mainValorisationId) {
        var mainValorisation = mainValorisationById[mainValorisationId];
        var mainValorisationYeadAverage = parseFloat(mainValorisation.yealdAverage);
        angular.forEach(valorisationsForDestination, function (valorisation) {
          valorisation.yealdAveragePart = parseFloat((parseFloat(valorisation.yealdAverage) * 100.0) / mainValorisationYeadAverage);
        });
      });
    };

    $scope._addValorisationsToActionSpeciesValorisations = function (action) {
      angular.forEach(action.speciesValorisationsBySpeciesCode, function (valorisations) {
        angular.forEach(valorisations, function (valorisation) {
          action.speciesValorisations.push(valorisation);
        });
      });
    };

    $scope._prepareHarvestingActionModel = function (intervention, action, interventionSpecies, force) {
      $scope._setupHarvestingActionAttributes(action, interventionSpecies, intervention);
      $scope._setWineValorisationCheckBox(action);

      var speciesByCodes = $scope._getSpeciesStadeBySpeciesCodeAndByCodeEspeceBotanique(interventionSpecies);
      var speciesByCodeEspeceBotanique = speciesByCodes.speciesByCodeEspeceBotanique;
      action.speciesByCodes = speciesByCodes;

      action.isWineSpecies = $scope._doesWineSpeciesPresentFromSpeciesStades(speciesByCodeEspeceBotanique);

      // set organic according if the organic status is defined for the sector then use it otherwise loook for the user choice on first valorisation (not save on action, and same for all species)
      action.isOrganicCrop = $scope.sector && ($scope.isOrganic
        || typeof $scope.isOrganic != "boolean" && action.valorisationDtos.length > 0 && action.valorisationDtos[0].isOrganicCrop);
      var mainValorisationById = {};

      //
      // the following code is an hack for valorisation that have been paste on incorrect species code
      // and that should be ignored but they could be valid if the destination does not target any species
      var noSpeciesMatchingValorisationsByDestination = {};
      var valorisationForAction = [];

      // look for valorisation that doesn't match any species but where destination are valid as they are valid for all species
      for (var iv = 0; iv < action.valorisationDtos.length; iv++) {

        var valorisation = action.valorisationDtos[iv];
        // set the ui id, which by default is the topia id
        // we need this id when there are several valorisations with the same destination and species
        // (eg "A completer" category in edaplos import)
        var destinations = action.destinationsByDestinationLabels[valorisation.destinationName];
        if (destinations === undefined) {
          continue;
        }

          // try find destination with same Id first
        var currentDestination;
        var destinationWithSameLabels = action.destinationsByDestinationLabels[valorisation.destinationName];
        if (valorisation.destinationId) {
          currentDestination = destinationWithSameLabels.find(dest => dest.topiaId == valorisation.destinationId);
        }
        if (!currentDestination) {
          currentDestination = action.destinationsByDestinationLabels[valorisation.destinationName][0];
        }

        valorisation.destination = currentDestination;
        valorisation.destination.uiId = valorisation.destinationId;
        var destination = valorisation.destination;

        var species = $scope.allSpeciesByCode[valorisation.speciesCode];

        if (!species) {
          // no matchind species found for this valorisation
          // check if destination is valid
          var destination = valorisation.destination;
          if (destination &&
            destination.code_destination_A &&
            !destination.code_espece_botanique &&
            !destination.code_qualifiant_aee &&
            action.destinationsByDestinationLabels[valorisation.destination.destination_Translated] &&
            action.destinationsByDestinationLabels[valorisation.destination.destination_Translated].length > 0) {
            var destinations = action.destinationsByDestinationLabels[valorisation.destination.destination_Translated];
            var validDestination = null;
            for (var dest = 0; dest < destinations.length; dest++) {
              var detination0 = destinations[dest];
              if (detination0.code_destination_A === destination.code_destination_A &&
                !detination0.code_espece_botanique &&
                !detination0.code_qualifiant_aee
              ) {
                validDestination = detination0;
                break;
              }
            }
            // if valid destination we compute the global yeald average for it
            // to be able to dispatch it on every species
            if (validDestination) {
              valorisation.speciesCode = null;
              valorisation.destination = validDestination;
              var globalValorisationForDestination = noSpeciesMatchingValorisationsByDestination[validDestination.uiId];
              if (globalValorisationForDestination) {
                globalValorisationForDestination.yealdAverage += parseFloat(valorisation.yealdAverage);
              } else {
                valorisation.yealdAverage = parseFloat(valorisation.yealdAverage);
                noSpeciesMatchingValorisationsByDestination[validDestination.uiId] = valorisation;
              }
            }
          }

        } else {
          // the valorisation as matching species
          valorisationForAction.push(valorisation);
        }

        angular.forEach(valorisation.qualityCriteriaDtos, function (qualityCriteria) {
          qualityCriteria.refQualityCriteria = action.qualityCriteria[qualityCriteria.refQualityCriteriaId];
          var qualityCriteriaClasses = qualityCriteria.refQualityCriteria ? qualityCriteria.refQualityCriteria.qualityCriteriaClasses : [];
          qualityCriteria.refQualityCriteriaClass = qualityCriteria.refQualityCriteriaClassId && qualityCriteriaClasses ?
            qualityCriteriaClasses.filter(qcc => qcc.topiaId === qualityCriteria.refQualityCriteriaClassId)[0] : undefined;

        });
      }

      // we dispatch valid destinations that targeted unvalid species to the curent action species
      var matchingValorisationDestinations = Object.values(noSpeciesMatchingValorisationsByDestination);
      for (var mvd = 0; mvd < matchingValorisationDestinations.length; mvd++) {
        var mvdVal = matchingValorisationDestinations[mvd];
        // add destination to species so create new valorisations for each species
        var species = action.speciesStadeSpecies;
        var yealdAverageBySpecies = parseFloat((mvdVal.yealdAverage / species.length).toFixed(3));
        for (var sp0 = 0; sp0 < species.length; sp0++) {
          var valSpecies = species[sp0];
          var newVal = {};
          Object.assign(newVal, mvdVal);
          newVal.topiaId = $scope._getNewValorisationId();
          newVal.speciesCode = valSpecies.code;
          valorisationForAction.push(newVal);
        }

      }

      action.valorisationDtos = valorisationForAction;
      // end of hack for wrong past
      for (var av = 0; av < action.valorisationDtos.length; av++) {

        var valorisation = action.valorisationDtos[av];
        var species = $scope.allSpeciesByCode[valorisation.speciesCode];
        if (!species) {
          continue;
        }

        $scope._addEditValorisationAttributes(action, valorisation);

        var speciesValorisations = $scope._getSpeciesValorisationsBySpeciesCodeToAction(valorisation, action);

        valorisation.relativeArea = angular.isDefined(species.speciesArea) ?
          species.speciesArea * 100 / action.speciesTotalArea :
          action.speciesTotalArea / action.speciesStadeSpecies.length;

        // main Valorisations are not saved in DB and have to be created when editing the harvesting action
        var mainValorisation = $scope._getOrCreateMainValorisationFromValorisation(intervention, action, valorisation);

        // this is for copy/past otherwise it will be recreated later on
        mainValorisation.qualityCriteria0 = valorisation.qualityCriteriaDtos;

        mainValorisationById[mainValorisation.topiaId] = mainValorisation;

        // possible in case off species changed, destination has to be redefined.
        $scope._removeValorisationDestinationIfMainAsNone(mainValorisation, valorisation);

        $scope._setValorisationValidStatus(action, valorisation);

        $scope._addValorisationToSpeciesValorisationsByMainValorisationId(action, valorisation, mainValorisation.topiaId);

        mainValorisation.yealdAverage += parseFloat(valorisation.yealdAverage);

        $scope._addValorisationToSpeciesValorisations(speciesValorisations, species, valorisation, force);

        valorisation.displayName = $scope._getSpeciesLabelForSpecies(species);

      };

      if (action.isMixVarietyOrSpecies
        && Object.keys(speciesByCodes.speciesByCode).length > Object.keys(action.speciesValorisationsBySpeciesCode).length
        && !$scope.addSpeciesToHarvestingAction) {
        // toutes les valorisations ne sont pas présente, probablement un bug de migration
        // on recrée les valorisations par espèces à partir des valorisations principales
        action.nbDestinationForSpecies = 0;
        action.speciesValorisationsBySpeciesCode = {};
        action.valorisationDtos = [];
        action.speciesValorisations = [];
        angular.forEach(action.mainValorisations, function (mainValorisation) {
          mainValorisation.qualityCriteriaDtos = mainValorisation.qualityCriteria0;
          action.valorisationsByMainValorisationIds[mainValorisation.topiaId] = [];
          $scope._createSpeciesValorisationsForMainValorisation(action, mainValorisation);
        });
      } else {
        $scope._computeValorisationsYealdAveragePart(action, mainValorisationById);

        $scope._addValorisationsToActionSpeciesValorisations(action);

        angular.forEach(action.mainValorisations, function (mainValorisation) {
          $scope._computeMainValorisationQualityCriteria(action, mainValorisation);
          $scope._setMainDestinationValorisationAmounts(action, mainValorisation);

        });
      }

      delete $scope.addSpeciesToHarvestingAction;

      if (!action.isMixVarietyOrSpecies) {
        $scope.showDetailedHarvestingActionValorisation(action);
      }

    };
    // end of _prepareHarvestingActionModel

    $scope._createRollbackActionContext = function (action, loadDestinationsContext) {
      var rollbackActionContext = {};
      rollbackActionContext.rollBackAction = angular.copy(action);
      rollbackActionContext.rollBackLoadDestinationContext = angular.copy(loadDestinationsContext);
      return rollbackActionContext;
    };

    $scope.filterSpeciesValorisationsOfMainValorisation = function (action, mainValorisation) {
      $scope.showDetailedHarvestingActionValorisation(action);
      var filteredMainValorisations = action.filteredMainValorisations;
      if (!filteredMainValorisations.includes(mainValorisation)) {
        filteredMainValorisations.push(mainValorisation);
      } else {
        filteredMainValorisations.splice(filteredMainValorisations.indexOf(mainValorisation), 1);
      }
    };

    $scope.getFilteredMainValorisations = function (action) {
      if (action.filteredMainValorisations.length) {
        return action.filteredMainValorisations;
      }
      return action.mainValorisations;
    };

    /* Method called to start an action edition use-case */
    $scope.startEditAction = function (action) {

      $scope._resetEditedAction();
      // keep the original action unchanged until edited action has been validated and save
      $scope.rollbackActionContext = $scope._createRollbackActionContext(action, $scope.loadDestinationsContext);
      // use to be able to compute action change
      $scope.originalAction = action;

      var actionToEdit = angular.copy(action);

      if (actionToEdit.mainAction) {
        actionToEdit.mainActionInterventionAgrosyst = actionToEdit.mainAction.intervention_agrosyst;
      }

      // Make sure the edited action type is present in the available action types
      $scope.editedIntervention.availableActionTypes[actionToEdit.mainActionInterventionAgrosyst] =
        $scope.agrosystInterventionTypes[actionToEdit.mainActionInterventionAgrosyst];

      if (!actionToEdit.toolsCouplingCode && actionToEdit.mainAction) { // Si ce n'est pas une action 'principale'
        actionToEdit.mainAction = $scope.agrosystActionsFullIndex[actionToEdit.mainAction.topiaId];
      }

      if (actionToEdit.mainActionInterventionAgrosyst === "SEMIS") {
        $scope.seedingActionUsagesToRemove = [];
        actionToEdit.speciesForSeeding = $scope._computeSpeciesFromSpeciesStades($scope.editedIntervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
        actionToEdit.seedTypes = $scope.seedTypes;
        actionToEdit.yealdUnits = $scope.yealdUnits;
        actionToEdit.seedPlantUnits = $scope.seedPlantUnits;
      }

      if (actionToEdit.mainActionInterventionAgrosyst === "RECOLTE" && $scope.loadDestinationsContext) {
        var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades($scope.editedIntervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
        $scope._prepareHarvestingActionModel($scope.editedIntervention, actionToEdit, speciesStadeSpecies);
        actionToEdit.yealdUnits = $scope.yealdUnits;
        actionToEdit.seedPlantUnits = $scope.seedPlantUnits;
        actionToEdit.isOrganicCrop = action.isOrganicCrop;
      }

      if (actionToEdit.mainActionInterventionAgrosyst === "TRANSPORT") {
        actionToEdit.capacityUnits = $scope.capacityUnits;
      }

      // "APPLICATION_DE_PRODUITS_PHYTOSANITAURES - SEMIS"
      if (actionToEdit && actionToEdit.seedingProductActionDtos) {
        // map of (inputTmpId, phytoUsage)
        var allSeedingActionPhytoProductMap = [];
        angular.forEach(actionToEdit.seedingProductActionDtos, function (seedingProductActionDto) {
          if (seedingProductActionDto.inputTmpId) {
            allSeedingActionPhytoProductMap[seedingProductActionDto.inputTmpId] = seedingProductActionDto;
          }
        });
        var keys = Object.keys(allSeedingActionPhytoProductMap);
        angular.forEach(keys, function (key) {
          var seedingProductActionDto = allSeedingActionPhytoProductMap[key];
          angular.forEach($scope.editedIntervention.usages, function (usage) {
            if (seedingProductActionDto.inputTmpId === key) {
              seedingProductActionDto.phytoUsage = usage;
            }
          });
        });
      }

      // because of problems with light box all necessaries action dependence are push to the edited action
      $scope._addActionLightBoxAttributes(actionToEdit);

      $scope._showActionLightBox($scope.editedIntervention, actionToEdit);
    };

    $scope.nonValidAlert = function (type) {
      if (type) {

        var target;
        if (type === "ACTION") {
          target = $("#nonValidAction");
        } else {
          target = $("#nonValidUsage");
        }

        target.dialog({
          resizable: false,
          width: 600,
          modal: true,
          buttons: {
            action: {
              click: function () {
                $(this).dialog("destroy");
              },
              text: "Corriger",
              'class': 'btn-primary'
            }
          }
        });
      }
    };

    $scope._revertActionChange = function () {
      if ($scope.rollbackActionContext) {
        $scope.loadDestinationsContext = $scope.rollbackActionContext.rollBackLoadDestinationContext;
      }
    };

    $scope._getActionEditModel = function () {
      var actionEditModel = {
        process: function () {
          var validationMessage = $scope._recordAction($scope.editedAction);
          if (validationMessage) {
            $scope.editedAction.errorMessage = validationMessage;
            $scope.nonValidAlert("ACTION");
            var doNotClose = true;
            return doNotClose;
          } else {
            $scope._resetEditedAction();
          }
        },
        cancelChanges: function () {
          $scope._revertActionChange();
          $scope._resetEditedAction();
        }
      };
      return actionEditModel;
    };

    $scope._showActionLightBox = function (intervention, action) {

      $scope._checkAvailableActions(intervention, action);

      var actionEditModel = $scope._getActionEditModel();

      if (action.mainActionInterventionAgrosyst === "RECOLTE" && !$scope.loadDestinationsContext) {

        var displayActionDialogModel = {
          name: 'DISPLAY_ACTION',
          actionEditModel: actionEditModel,
          action: action,
          process: function () {
            _displayConfirmDialog($scope, $("#actions-edit-lightbox"), '95%', actionEditModel);
            $scope.editedAction = angular.copy(action);
            if ($scope.editedAction.cattleCode) {
              $scope.editedAction.cattle = $scope.cattles.find(cattle => cattle.code === $scope.editedAction.cattleCode);
            }
          }
        };

        var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
        var rollbackContext = null;
        $scope._loadDestinationsContext(speciesStadeSpecies, intervention, action, rollbackContext, displayActionDialogModel);

      } else {
        _displayConfirmDialog($scope, $("#actions-edit-lightbox"), '95%', actionEditModel);
        $scope.editedAction = angular.copy(action);
        $scope._addLoadDestinationsContextResultsToAction($scope.loadDestinationsContext, intervention, $scope.editedAction);
      }

    };

    $scope._removeNoneValidSeedingUsage = function () {
      if ($scope.seedingActionUsagesToRemove) {
        angular.forEach($scope.seedingActionUsagesToRemove, function (usageToRemove) {
          var indexOfUsageToRemove = $scope.editedIntervention.usages.indexOf(usageToRemove);
          if (indexOfUsageToRemove !== -1) {
            $scope.editedIntervention.usages.splice(indexOfUsageToRemove, 1);
          }
        });
        $scope.seedingActionUsagesToRemove = [];
      }
    };

    // si l'action est une nouvelle action, celle-ci est ajouté à l'intervention
    // si l'action est en édition, elle remplace l'original
    $scope._recordAction = function (action, force) {

      if (!action.mainActionInterventionAgrosyst || !action.mainActionId) {
        return $scope.messages.missingMainAction;
      }

      if (action.mainActionInterventionAgrosyst === "RECOLTE") {
        // do not save main Valorisations
        action.valorisationDtos = action.speciesValorisations;
        action.cattleCode = action.cattle ? action.cattle.code : null;
      }


      if (!force) {
        var errorMessages = $scope.validateAction($scope.editedIntervention, action, true);
        if (errorMessages.length > 0) {
          return errorMessages.join('<br>');
        }
      }

      if (!$scope.editedIntervention.actionDtos) {
        $scope.editedIntervention.actionDtos = [];
      }

      if (!$scope.editedIntervention.usages) {
        $scope.editedIntervention.usages = [];
      }

      $scope._removeActionLightBoxAttributes(action);

      if ($scope.originalAction) {
        // edit existing action
        $scope._removeNoneValidSeedingUsage();
        angular.copy(action, $scope.originalAction);
        action = $scope.originalAction;
      } else {
        // add new action
        action.topiaId = "NEW-ACTION-" + guid();
        $scope.editedIntervention.actionDtos.push(action);
      }

      $scope._checkAvailableActionAndInputTypes($scope.editedIntervention);

      // compute spending time for harvesting action
      if (action.mainActionInterventionAgrosyst === "RECOLTE") {
        $scope._computeSpendingTime($scope.editedIntervention);
      }

      $scope._addInterventionProportionOfTreatedSurface($scope.editedIntervention);
    };

    $scope.deleteAction = function (action) {
      var index = $scope.editedIntervention.actionDtos.indexOf(action);
      if (index !== -1) {
        $scope.deletedActionType = action.mainActionInterventionAgrosyst;

        var deleteActionModel = {
          process: function () {
            $scope.editedIntervention.actionDtos.splice(index, 1);
            delete $scope.editedAction;
            $scope._checkAvailableActionAndInputTypes($scope.editedIntervention);
            $scope._removeUsages(action);
          },
          cancelChanges: function () { }
        };

        _displayConfirmDialog($scope, $("#confirmRemovedAction"), 400, deleteActionModel);
      }
    };

    // TODO AThimel 31/01/2014 Il serait bon d'utiliser cette méthode (et la suivante) également pour l'affichage des cultures en haut d'écran d'ITK
    $scope._getSelectedCroppingPlanEntryIdentifier = function () {

      var result = $scope.selectedPhaseConnectionOrNode.croppingPlanEntryCode;

      if (!result) {
        result = $scope.selectedPhaseConnectionOrNode.croppingPlanEntryId;
      }

      if (!result && $scope.selectedPhaseConnectionOrNode.targetId) {
        result = $scope.connectionNodeIdToNodeObjectMap[$scope.selectedPhaseConnectionOrNode.targetId].croppingPlanEntryCode;
      }

      return result;
    };

    $scope.getCroppingPlanEntry = function (cpEntryIdentifier) {
      var result = $scope.croppingPlanIndex[cpEntryIdentifier];

      if (!result) {
        $scope.updateCroppingPlanIndex();
        result = $scope.croppingPlanIndex[cpEntryIdentifier];
      }

      if (result) {
        result.cpEntryIdentifier = cpEntryIdentifier;
      }

      return result;
    };

    $scope._getNewHarvestingActionCreatedPostDestinationContextLoadedModel = function (mainAction) {
      var removeHarvestingActionSpeciesModel = {
        name: "NEW_HARVESTING_ACTION",
        mainAction: mainAction,
        process: function () {
          $scope.editedAction.mainAction = mainAction;
          $scope.editedAction.mainActionInterventionAgrosyst = mainAction.intervention_agrosyst;
        }
      };
      return removeHarvestingActionSpeciesModel;
    };

    $scope.actionSelected = function (action, mainActionId) {
      var mainAction = action.availableActions.filter((action) => action.topiaId === mainActionId)[0]
      if (action && mainAction) {
        action.mainActionInterventionAgrosyst = mainAction.intervention_agrosyst;
        action.mainAction = mainAction;
        action.mainActionId = mainAction.topiaId;
        action.mainActionReference_label = mainAction.reference_label_Translated;
        action.mainActionReference_code = mainAction.reference_code;
        // force it here too, because user cas select action before action type
        $scope._initAction($scope.editedIntervention, action);

        if (mainAction && mainAction.intervention_agrosyst === "RECOLTE") {
          var nextFunction = $scope._getNewHarvestingActionCreatedPostDestinationContextLoadedModel(mainAction);
          var rollbackContext = null;
          var speciesStadeSpecies = $scope._computeSpeciesFromSpeciesStades($scope.editedIntervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);

          $scope._loadDestinationsContext(speciesStadeSpecies, $scope.editedIntervention, action, rollbackContext, nextFunction);
        } else if (mainAction && mainAction.intervention_agrosyst === "IRRIGATION") {
          $scope._loadInputs($scope.domainId, "IRRIGATION").then(function (response) {
            if (response.data && response.data.length > 0) {
              action.irrigationInputUsageDto = { inputType: "IRRIGATION", domainIrrigationInputDto: response.data[0] };
            }
          });
        }
      }
    };

    $scope.actionTypeSelected = function (action, actionType) {

      action.mainActionInterventionAgrosyst = actionType;

      // On rafraîchi la liste
      $scope._checkAvailableActions($scope.editedIntervention, action);

      // dans le cas où il n'y un qu'un seul choix celui-ci est affecté
      if (action.availableActions && action.availableActions.length === 1) {
        var mainAction = action.availableActions[0];
        $scope.actionSelected(action, mainAction.topiaId);
      } else {
        $scope._initAction($scope.editedIntervention, action);
      }
    };

    /**
    * the intervention
    * the phase connection or node species
    * the phase or node intermediate species
    **/
    $scope._computeSpeciesFromSpeciesStades = function (intervention, phaseOrNodeIntermediateSpecies, phaseConnectionOrNodeSpecies) {
      var speciesStadeSpecies = [];
      // cas ou la case « affectation à la culture intermédiaire Cochée
      if (intervention.intermediateCrop) {
        angular.forEach(phaseOrNodeIntermediateSpecies, function (species) {
          if (speciesStadeSpecies.indexOf(species) === -1) {
            speciesStadeSpecies.push(species);
          }
        });
      } else {
        angular.forEach(intervention.speciesStadesDtos, function (speciesStade) {
          var speciesStadeSpeciesCode = speciesStade.speciesCode;
          angular.forEach(phaseConnectionOrNodeSpecies, function (species) {
            if (species.code === speciesStadeSpeciesCode && speciesStadeSpecies.indexOf(species) === -1) {
              speciesStadeSpecies.push(species);
            }
          });
        });
      }
      return speciesStadeSpecies;
    };

    $scope._doesWineSpeciesPresentFromSpeciesStades = function (speciesByCode) {
      var result = false;
      if (speciesByCode) {
        result = Object.keys(speciesByCode).indexOf("ZMO_null") != -1;
      }
      return result;
    };

    $scope._getSpeciesStadeBySpeciesCodeAndByCodeEspeceBotanique = function (speciesStadeSpecies) {
      var result = {};
      var speciesByCode = {};
      var speciesByCodeEspeceBotanique = {};

      if (speciesStadeSpecies) {
        angular.forEach(speciesStadeSpecies, function (species) {

          // 1: practicedSystem, 2 effective
          var codeEspeceBotanique_code_qualifiant_AEE;
          if (species.code_espece_botanique) {
            codeEspeceBotanique_code_qualifiant_AEE = species.code_espece_botanique + "_" + (species.code_qualifiant_AEE || 'null');
          } else if (species.species && species.species.code_espece_botanique) {
            codeEspeceBotanique_code_qualifiant_AEE = species.species.code_espece_botanique + "_" + (species.species.code_qualifiant_AEE || 'null');
          }

          if (codeEspeceBotanique_code_qualifiant_AEE) {
            // avoid duplicate entry
            var value = speciesByCodeEspeceBotanique[codeEspeceBotanique_code_qualifiant_AEE];
            if (!value) {
              speciesByCodeEspeceBotanique[codeEspeceBotanique_code_qualifiant_AEE] = species;
            }
          }

          if (species.code) {
            // avoid duplicate entry
            var speciesForCode = speciesByCode[species.code];
            if (!speciesForCode) {
              speciesByCode[species.code] = species;
            }
          }

        });
      }

      result.speciesByCode = speciesByCode;
      result.speciesByCodeEspeceBotanique = speciesByCodeEspeceBotanique;
      return result;
    };

    $scope._checkAvailableActions = function (intervention, action) {
      var result;
      intervention.availableActions = $filter('filter')($scope.agrosystActionsFullList, function (elem) {
        var actionType = elem.intervention_agrosyst;
        if (action.mainActionInterventionAgrosyst) {
          // Seul le type de l'action est recherché
          result = actionType === action.mainActionInterventionAgrosyst;
        } else {
          // Est-ce que le type fait partie des types encore disponibles ?
          result = angular.isDefined(intervention.availableActionTypes[actionType]);
        }
        return result;
      });
      if (!action.toolsCouplingCode && !action.topiaId) {
        action.availableActions = intervention.availableActions;
      }
    };

    // $scope._checkUsagePricesToRemove = function (input) {
    //   // Il faut retirer un prix que si celui-ci n'a plus de raison pour la culture (et non pas uniquement l'intervention)
    //   //var priceInfo = _getUsagePricesInfos(input);
    //   // TODO dcosse 07/09/17
    // };

    $scope._removeUsages = function (action) {
      var usagesToRemove = [];

      if (action) {
        angular.forEach($scope.editedIntervention.usages, function (usage) {
          if ((usage.mineralFertilizersSpreadingAction && action.topiaId === usage.mineralFertilizersSpreadingAction.topiaId) ||
            (usage.pesticidesSpreadingAction && action.topiaId === usage.pesticidesSpreadingAction.topiaId) ||
            (usage.organicFertilizersSpreadingAction && action.topiaId === usage.organicFertilizersSpreadingAction.topiaId) ||
            (usage.irrigationAction && action.topiaId === usage.irrigationAction.topiaId) ||
            (usage.biologicalControlAction && action.topiaId === usage.biologicalControlAction.topiaId) ||
            (usage.harvestingAction && action.topiaId === usage.harvestingAction.topiaId) ||
            (usage.seedingAction && action.topiaId === usage.seedingAction.topiaId) ||
            (usage.maintenancePruningVinesAction && action.topiaId === usage.maintenancePruningVinesAction.topiaId) ||
            (usage.otherAction && action.topiaId === usage.otherAction.topiaId)
          ) {
            usages.push(usage);
          }
        });
      }

      angular.forEach(usagesToRemove, function (usageToRemove) {
        var indexOfUsageToRemove = $scope.editedIntervention.usages.indexOf(usageToRemove);
        if (indexOfUsageToRemove !== -1) {
          $scope.editedIntervention.usages.splice(indexOfUsageToRemove, 1);
        }
      });

    };

    $scope._removeValorisationQualityCriteriaWithGivenId = function (valorisation, qualityCriteriaId) {
      var speciesQualityCriteria = [];
      angular.forEach(valorisation.qualityCriteriaDtos, function (qualityCrit) {
        if (qualityCrit.refQualityCriteriaId != qualityCriteriaId) {
          speciesQualityCriteria.push(qualityCrit);
        }
      });
      valorisation.qualityCriteriaDtos = speciesQualityCriteria;
    };

    $scope.removeQualityCriteria = function (action, valorisation, qualityCriteria) {

      var index = valorisation.qualityCriteriaDtos.indexOf(qualityCriteria);
      if (index != -1) {
        valorisation.qualityCriteriaDtos.splice(index, 1);
        valorisation.removedQualityCriteriaIds[qualityCriteria.refQualityCriteriaId] = qualityCriteria;
        valorisation.allQualityCriteriaForValorisation[qualityCriteria.refQualityCriteriaId] = qualityCriteria.refQualityCriteria;
        valorisation.nbQualityCriteriaForValorisation = Object.keys(valorisation.allQualityCriteriaForValorisation).length;
      }
    };

    $scope._removeQualityCriteriaTempValues = function (valorisation) {
      delete valorisation.selectedQualityCriteria;
      delete valorisation.quantitativeValue;
      delete valorisation.binaryValue;
    };

    $scope.addQualityCriteria = function (valorisation) {
      var selectedQualityCriteria = valorisation.selectedQualityCriteria;
      if (selectedQualityCriteria) {
        var allQualityCriteriaForValorisation = {};
        angular.forEach(valorisation.allQualityCriteriaForValorisation, function (qualityCriteria, id) {
          if (id != selectedQualityCriteria.topiaId) {
            allQualityCriteriaForValorisation[id] = qualityCriteria;
          }
        });
        valorisation.allQualityCriteriaForValorisation = allQualityCriteriaForValorisation;
        valorisation.nbQualityCriteriaForValorisation = Object.keys(valorisation.allQualityCriteriaForValorisation).length;

        var refQualityCriteria = selectedQualityCriteria;
        var isBinaryType = refQualityCriteria.qualityAttributeType === "BINAIRE";
        var quantitativeValue = angular.isDefined(valorisation.quantitativeValue) ? valorisation.quantitativeValue : null;
        var binaryValue = isBinaryType ? valorisation.binaryValue ? valorisation.binaryValue : false : null;
        var refQualityCriteriaClass = refQualityCriteria.refQualityCriteriaClass || null;

        var qualityCriteria = {
          refQualityCriteriaId: refQualityCriteria.topiaId,
          refQualityCriteriaClassId: refQualityCriteriaClass ? refQualityCriteriaClass.topiaId : null,
          refQualityCriteriaClasslabel: refQualityCriteriaClass ? refQualityCriteriaClass.classe : '',
          refQualityCriteria: refQualityCriteria,
          quantitativeValue: parseFloat(quantitativeValue),
          binaryValue: binaryValue,
          main: valorisation.main
        };

        if (valorisation.qualityCriteriaDtos === undefined) {
          valorisation.qualityCriteriaDtos = [];
        }
        valorisation.qualityCriteriaDtos.push(qualityCriteria);

      }
      $scope._removeQualityCriteriaTempValues(valorisation);
    };

    $scope.getQualityCriteriaValue = function (action, qualityCriteria) {
      var value;
      if (qualityCriteria.quantitativeValue) {
        value = $filter('number')(qualityCriteria.quantitativeValue, 1) + " "
          + action.qualityCriteriaUnits[qualityCriteria.refQualityCriteria.criteriaUnit];

      } else if (qualityCriteria.binaryValue === true || qualityCriteria.binaryValue === false) {
        value = qualityCriteria.binaryValue && $scope.i18n.messages["common-yes"] || $scope.i18n.messages["common-no"];
      } else if (qualityCriteria.refQualityCriteriaClassId && qualityCriteria.refQualityCriteriaClasslabel) {
        value = qualityCriteria.refQualityCriteriaClasslabel;// TODO fixit duplicate with next condition
      } else if (qualityCriteria.refQualityCriteriaClassId && qualityCriteria.refQualityCriteriaClass) {
        value = qualityCriteria.refQualityCriteriaClass.classe;
      }
      return value;
    };

    $scope.haveValorisationsSameDestination = function (action, valorisation, valorisation2) {
      return valorisation && valorisation.destination && valorisation2 && valorisation2.destination
        && valorisation.destination.uiId == valorisation2.destination.uiId;
    };

    $scope.changeValorisationSpecies = function (action, valorisation) {
      if (valorisation.species) {
        $scope._updateValorisationSpecies(action, valorisation, valorisation.species.code, valorisation.speciesCode);
        $scope._setValorisationSpecies(action, valorisation, valorisation.species);
        $scope._setValorisationValidStatus(action, valorisation);
        $scope._updateActionSpeciesValorisations(action);
      }
    };

    $scope.addValorisation = function (action, mainValorisation) {
      var species = null;
      if ($scope.getAvailableSpeciesNbForValorisation(action, mainValorisation) == 1) {
        species = $scope.getLastAvailableSpeciesForValorisation(action, mainValorisation);
      }
      var harvestingActionValorisation = $scope._createSpeciesValorisationForMainValorisation(action, mainValorisation, species);
      $scope._addSpeciesValorisationToAction(action, harvestingActionValorisation, species, mainValorisation);
      $scope.editValorisation(harvestingActionValorisation, action);
    };

    $scope.getAvailableSpeciesNbForValorisation = function (action, mainValorisation) {
      return action.speciesStadeSpecies.length - action.valorisationsByMainValorisationIds[mainValorisation.topiaId].length;
    };

    $scope.getLastAvailableSpeciesForValorisation = function (action, mainValorisation) {
      var speciesCodeUsed = action.valorisationsByMainValorisationIds[mainValorisation.topiaId].map(v => v.speciesCode);
      return action.speciesStadeSpecies.find(species => !speciesCodeUsed.includes(species.code));
    };

    $scope.getAvailableSpeciesForValorisation = function (action, mainValorisation, valorisation) {
      var speciesCodeUsed = action.valorisationsByMainValorisationIds[mainValorisation.topiaId].map(v => v.speciesCode);
      return action.speciesStadeSpecies.filter(species => species.code == valorisation.speciesCode || !speciesCodeUsed.includes(species.code));
    };

    $scope.isMainValorisationValid = function (action, valorisation) {
      return valorisation.destination && valorisation.yealdAverageFixedFloat
        && (action.isMixVarietyOrSpecies || !valorisation.userCreatedValorisation || Object.values(valorisation._speciesValorisationsToCreate).includes(true));
    };

    $scope.isEditedValorisationValid = function (action, valorisation) {
      return !valorisation
        || (valorisation.main && $scope.isMainValorisationValid(action, valorisation))
        || (!valorisation.main && valorisation.speciesCode && valorisation._isValidValorisationPercent);
    };

    $scope.editValorisation = function (valorisation, action) {
      $scope.editedValorisation = valorisation;
      $scope.copieOfEditedValorisation = angular.copy(valorisation);
      $scope.copieOfEditedValorisation.yealdAverageFixedFloat = parseFloat(parseFloat($scope.copieOfEditedValorisation.yealdAverage).toFixed(1));

      $scope.editedValorisation.yealdAveragePartInt = parseInt(parseFloat($scope.editedValorisation.yealdAveragePart).toFixed(0));
      $scope.editedValorisation.yealdAverageFixedFloat = parseFloat(parseFloat($scope.editedValorisation.yealdAverage).toFixed(1));

      $scope.editedValorisation.removedQualityCriteriaIds = [];

      $scope.editedValorisation.allQualityCriteriaForValorisation = $scope.filterQualityCriteriaOnSpecies(action, $scope.editedValorisation);
      $scope.editedValorisation.nbQualityCriteriaForValorisation = Object.keys(valorisation.allQualityCriteriaForValorisation).length;

      $scope.setValorisationDestination($scope.editedValorisation, action);

      $scope.editedValorisation.locked = false;
      $scope.editedValorisation._isMainYealdAverageValid = true;
    };

    $scope.computeYealAverageChange = function (action, mainValorisation, valorisation) {
      var originalValorisation = $scope.copieOfEditedValorisation;
      $scope.copieOfEditedValorisation = $scope._computeYealAverageChange(action, mainValorisation, valorisation, originalValorisation);
    };

    $scope._revertToValidYealdAverage = function (valorisation, originalValorisation) {
      valorisation.yealdAveragePart = parseFloat(originalValorisation.yealdAveragePart);
      valorisation.yealdAverage = parseFloat(originalValorisation.yealdAverage);

      valorisation.yealdAveragePartInt = parseInt(parseFloat(originalValorisation.yealdAveragePart).toFixed(0));
      valorisation.yealdAverageFixedFloat = parseFloat(parseFloat(originalValorisation.yealdAverage).toFixed(1));
    };

    $scope._computeYealAverageChange = function (action, mainValorisation, currentValorisation, originalValorisation) {
      if (angular.isDefined(currentValorisation.yealdAverageFixedFloat) && !isNaN(parseFloat(currentValorisation.yealdAverageFixedFloat))) {
        currentValorisation.yealdAverage = currentValorisation.yealdAverageFixedFloat;

        if (action.isMixVarietyOrSpecies) {
          var mainYealdAverage = parseFloat(mainValorisation.yealdAverage);

          currentValorisation.locked = true;
          currentValorisation.yealdAveragePart = parseFloat((currentValorisation.yealdAverageFixedFloat * 100.0) / mainYealdAverage);
          currentValorisation.yealdAveragePartInt = parseInt(parseFloat(currentValorisation.yealdAveragePart).toFixed(0));

          var lockedValorisationAmount = 0;
          var nbUnable = 0;
          var valorisations = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
          angular.forEach(valorisations, function (valorisation) {
            if (valorisation.locked || !valorisation.isDestinationValidForSpecies) {
              lockedValorisationAmount = lockedValorisationAmount + parseFloat(valorisation.yealdAverage);
              nbUnable++;
            }
          });

          var diff = mainYealdAverage - lockedValorisationAmount;
          if (diff < 0 || (lockedValorisationAmount + diff) < mainYealdAverage) {
            currentValorisation._isMainYealdAverageValid = false;
            $scope._revertToValidYealdAverage(currentValorisation, originalValorisation);

          } else {
            currentValorisation._isMainYealdAverageValid = true;
            var amountDivider = valorisations.length - nbUnable;

            if (amountDivider !== 0) {
              var unlockedSpeciesAmount = diff / amountDivider;

              angular.forEach(valorisations, function (valorisation) {
                if (!valorisation.locked && valorisation.isDestinationValidForSpecies) {
                  valorisation.yealdAverage = unlockedSpeciesAmount;
                  valorisation.yealdAveragePart = parseFloat(valorisation.yealdAverage * 100.0 / mainYealdAverage);
                }
              });
              originalValorisation = angular.copy(currentValorisation);
            }
          }

        } else {
          var mainYealdAverage = 0;
          var valorisations = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
          angular.forEach(valorisations, function (valorisation) {
            mainYealdAverage += valorisation.yealdAverage * valorisation.relativeArea / 100;
          });
          mainValorisation.yealdAverage = mainYealdAverage;
          originalValorisation = angular.copy(currentValorisation);
        }

      } else {
        $scope._revertToValidYealdAverage(currentValorisation, originalValorisation);
      }

      return originalValorisation;
    };

    $scope.computeYealdAveragePartChange = function (action, mainValorisation, currentValorisation) {
      // to keep precision, while the final part is an Integer we use float
      if (!isNaN(currentValorisation.yealdAveragePartInt)) {
        var actualYealdAveragePart = parseFloat(currentValorisation.yealdAveragePartInt);
        var previousYealdAveragePart = parseFloat(currentValorisation.yealdAveragePart);
        currentValorisation.yealdAveragePart = actualYealdAveragePart;

        var mainYealdAverage = parseFloat(mainValorisation.yealdAverage);

        if (actualYealdAveragePart > 100.0) {
          currentValorisation.yealdAveragePart = previousYealdAveragePart;
        } else {
          currentValorisation.yealdAverage = ((actualYealdAveragePart * mainYealdAverage) / 100.0);
          currentValorisation.yealdAverageFixedFloat = currentValorisation.yealdAverage;
          $scope.computeYealAverageChange(action, mainValorisation, currentValorisation);
        }
      } else {
        $scope._revertToValidYealdAverage(currentValorisation, $scope.copieOfEditedValorisation);
      }

    };

    $scope.getMainValorisationNb = function (action) {
      return Object.keys(action.mainValorisations).length;
    }

    $scope._getMainQualityCriteria = function (mainQualityCriteriaByIds, qualityCriteriaLabel, qualityCriteria, allMainQualityCriteria) {
      var mainQualityCriteria = mainQualityCriteriaByIds[qualityCriteriaLabel];
      if (!mainQualityCriteria) {
        mainQualityCriteria = angular.copy(qualityCriteria);
        mainQualityCriteria.quantitativeValue = qualityCriteria.quantitativeValue ? 0 : null;
        delete mainQualityCriteria.binaryValue;
        mainQualityCriteriaByIds[qualityCriteriaLabel] = mainQualityCriteria;
        allMainQualityCriteria.push(mainQualityCriteria);
      }
      return mainQualityCriteria;
    };

    $scope._computeMainValorisationQualityCriteria = function (action, mainValorisation) {
      var valorisationsForDestination = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
      var globalQualityCriteriaPartByQualityCriteriaIds = {};
      var globalQualityCriteriaBinaryValueYesByQualityCriteriaIds = {};
      var globalTrueQualityCriteriaBinaryValueYesByQualityCriteriaIds = {};

      angular.forEach(valorisationsForDestination, function (valorisationForDestination) {
        angular.forEach(valorisationForDestination.qualityCriteriaDtos, function (qualityCriteria) {
          if (qualityCriteria.refQualityCriteria != undefined) {
            var qualityCriteriaLabel = qualityCriteria.refQualityCriteria.qualityCriteriaLabel;
            var value = globalQualityCriteriaPartByQualityCriteriaIds[qualityCriteriaLabel];
            globalQualityCriteriaPartByQualityCriteriaIds[qualityCriteriaLabel] = value ? value + valorisationForDestination.yealdAverage : valorisationForDestination.yealdAverage;

            if (angular.isDefined(qualityCriteria.binaryValue)) {
              var globalValue = globalQualityCriteriaBinaryValueYesByQualityCriteriaIds[qualityCriteriaLabel];
              globalQualityCriteriaBinaryValueYesByQualityCriteriaIds[qualityCriteriaLabel] = globalValue ? globalValue + valorisationForDestination.yealdAverage : valorisationForDestination.yealdAverage;

              var globalTrueValue = globalTrueQualityCriteriaBinaryValueYesByQualityCriteriaIds[qualityCriteriaLabel];
              if (qualityCriteria.binaryValue) {
                globalTrueQualityCriteriaBinaryValueYesByQualityCriteriaIds[qualityCriteriaLabel] = globalTrueValue ? globalTrueValue + valorisationForDestination.yealdAverage : valorisationForDestination.yealdAverage;
              } else {
                globalTrueQualityCriteriaBinaryValueYesByQualityCriteriaIds[qualityCriteriaLabel] = globalTrueValue || 0;
              }

            }
          }
        });
      });

      var mainQualityCriteriaByIds = {};
      var allMainQualityCriteria = [];

      angular.forEach(valorisationsForDestination, function (valorisationForDestination) {
        angular.forEach(valorisationForDestination.qualityCriteriaDtos, function (qualityCriteria) {
          if (qualityCriteria.refQualityCriteria != undefined) {
            var qualityCriteriaLabel = qualityCriteria.refQualityCriteria.qualityCriteriaLabel;
            var globalQualityCriteriaPartByQualityCriteriaId = globalQualityCriteriaPartByQualityCriteriaIds[qualityCriteriaLabel];
            var qualityCriteriaPart = valorisationForDestination.yealdAverage / globalQualityCriteriaPartByQualityCriteriaId;

            var mainQualityCriteria = $scope._getMainQualityCriteria(mainQualityCriteriaByIds, qualityCriteriaLabel, qualityCriteria, allMainQualityCriteria);

            if (qualityCriteria.quantitativeValue) {
              mainQualityCriteria.quantitativeValue += qualityCriteriaPart * qualityCriteria.quantitativeValue;
            } else if (angular.isDefined(qualityCriteria.binaryValue)) {
              var total = globalQualityCriteriaBinaryValueYesByQualityCriteriaIds[qualityCriteriaLabel];
              var part = globalTrueQualityCriteriaBinaryValueYesByQualityCriteriaIds[qualityCriteriaLabel];
              mainQualityCriteria.binaryValue = part / total > 0.5;
            }
          }
        });
      });

      mainValorisation.qualityCriteriaDtos = allMainQualityCriteria;
    };

    $scope._computeMainValorisationYealdAverage = function (action, mainValorisation) {
      var totalYealAverage = 0.0;
      var valorisationsForDestination = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
      angular.forEach(valorisationsForDestination, function (speciesValorisation) {
        totalYealAverage += parseFloat(speciesValorisation.yealdAverage);
      });
      return totalYealAverage;
    };

    $scope.stopEditSpeciesValorisation = function (action, valorisation) {
      $scope._addPendingQualityCriteria(valorisation);

      var destinationId = valorisation.destination.uiId;
      var mainValorisation = action.mainValorisations[destinationId];

      valorisation._isMainYealdAverageValid = true;
      delete valorisation.userCreatedValorisation;

      if (action.isMixVarietyOrSpecies) {
        var totalYealAverage = $scope._computeMainValorisationYealdAverage(action, mainValorisation);
        if (parseFloat((totalYealAverage).toFixed(1)) !== parseFloat((mainValorisation.yealdAverage).toFixed(1))) {
          valorisation._isMainYealdAverageValid = false;
          $scope._revertToValidYealdAverage(valorisation, $scope.copieOfEditedValorisation);
        }
      }

      if (valorisation._isMainYealdAverageValid) {
        $scope._computeMainValorisationQualityCriteria(action, mainValorisation);
        $scope._setMainDestinationValorisationAmounts(action, mainValorisation);

        delete $scope.editedValorisation;
        $scope._removeQualityCriteriaTempValues(valorisation);
      }
    };

    $scope._saveDestinationValorisationTargetChange = function (copyOfEditedValorisation, mainValorisation, action) {
      var valorisationsForDestination = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
      if (valorisationsForDestination) {
        angular.forEach(valorisationsForDestination, function (valorisationForDestination) {
          if (valorisationForDestination.salesPercent === copyOfEditedValorisation.salesPercent) {
            valorisationForDestination.salesPercent = parseInt(mainValorisation.salesPercent);
            valorisationForDestination.selfConsumedPersent = parseInt(mainValorisation.selfConsumedPersent);
            valorisationForDestination.noValorisationPercent = parseInt(mainValorisation.noValorisationPercent);
          }
        });
      }
    };

    $scope._saveMainValorisationForSpeciesCrop = function (action, mainValorisation) {
      var speciesValorisation = action.speciesValorisations.find(v => v.destination.uiId == mainValorisation.destination.uiId);
      if (!speciesValorisation) {
        // may not exist yet if it's a newly added destination
        $scope._addDetailedHarvestingActionValorisation(action, mainValorisation);
      } else {
        speciesValorisation.yealdAverage = mainValorisation.yealdAverage;
        speciesValorisation.yealdMin = mainValorisation.yealdMin;
        speciesValorisation.yealdMax = mainValorisation.yealdMax;
        speciesValorisation.yealdMedian = mainValorisation.yealdMedian;
        speciesValorisation.yealdUnit = mainValorisation.yealdUnit;
        speciesValorisation.destination = mainValorisation.destination;
        speciesValorisation.destinationId = mainValorisation.destination ? mainValorisation.destination.topiaId : "";
        speciesValorisation.destinationName = mainValorisation.destination ? mainValorisation.destination.destination_Translated : "";
        speciesValorisation.qualityCriteriaDtos = mainValorisation.qualityCriteriaDtos;
        speciesValorisation.salesPercent = parseInt(mainValorisation.salesPercent);
        speciesValorisation.selfConsumedPersent = parseInt(mainValorisation.selfConsumedPersent);
        speciesValorisation.noValorisationPercent = parseInt(mainValorisation.noValorisationPercent);
      }
    };

    $scope.stopEditMainValorisation = function (action, mainValorisation) {
      if ($scope.copieOfEditedValorisation.yealdAverageFixedFloat !== mainValorisation.yealdAverageFixedFloat) {
        if (!action.isMixVarietyOrSpecies && !mainValorisation.userCreatedValorisation && $scope.hasSeveralSpecies(action)) {
          var confirmModel = {
            process: function () {
              mainValorisation._forceSpeciesYealdAveragesToMainYealdAverage = true;
              mainValorisation.yealdAverage = parseFloat(mainValorisation.yealdAverageFixedFloat);
              $scope._stopEditMainValorisation(action, mainValorisation);
            }
          };
          _displayConfirmDialog($scope, $("#confirmEditYealdAverage"), 800, confirmModel);

        } else {
          mainValorisation.yealdAverage = parseFloat(mainValorisation.yealdAverageFixedFloat);
          $scope._stopEditMainValorisation(action, mainValorisation);
        }

      } else {
        $scope._stopEditMainValorisation(action, mainValorisation);
      }
    }

    $scope._stopEditMainValorisation = function (action, mainValorisation) {
      $scope._addPendingQualityCriteria(mainValorisation);

      if (action.speciesStadeSpecies.length === 1) {
        $scope._saveMainValorisationForSpeciesCrop(action, mainValorisation);
      } else {
        $scope._saveMainValorisationChange(mainValorisation, action);
        $scope._saveDestinationValorisationTargetChange($scope.copieOfEditedValorisation, mainValorisation, action);
      }

      delete mainValorisation.destinationsYealdUnits;
      delete $scope.editedValorisation;
      $scope._removeQualityCriteriaTempValues(mainValorisation);
    };

    $scope._addPendingQualityCriteria = function (valorisation) {
      if (valorisation.selectedQualityCriteria) {
        let hasValue = ((typeof valorisation.quantitativeValue == 'number') && valorisation.quantitativeValue !== 0);
        let canSaveEditedCriteria = (hasValue ||
          valorisation.selectedQualityCriteria.qualityAttributeType == 'BINAIRE' ||
          valorisation.selectedQualityCriteria.qualityAttributeType == 'QUALITATIVE');
        if (canSaveEditedCriteria === true) {
          $scope.addQualityCriteria(valorisation);
        }
      }
    };

    $scope._removeSpecisesValorisation = function (action, destinationId) {
      var speciesValorisationRemoved = 0;
      angular.forEach(action.speciesValorisationsBySpeciesCode, function (valorisations, speciesCode) {
        var valorisationsForSpecies = action.speciesValorisationsBySpeciesCode[speciesCode];
        var valorisationIndex = valorisationsForSpecies.findIndex(v => v.destination.uiId == destinationId);
        if (valorisationIndex >= 0) {
          speciesValorisationRemoved = 1;
          valorisationsForSpecies.splice(valorisationIndex, 1);

          var index = action.speciesValorisations.findIndex(v => v.destination.uiId == destinationId);
          if (index >= 0) {
            action.speciesValorisations.splice(index, 1);
          }
        }
      });
      action.nbDestinationForSpecies = action.nbDestinationForSpecies - speciesValorisationRemoved;
    };

    $scope.deleteDestination = function (valorisationToRemove, action) {
      var destinationIdToRemove = valorisationToRemove.destination ? valorisationToRemove.destination.uiId : null;
      if (action.mainValorisations[destinationIdToRemove]) {
        var deleteDestinationModel = {
          process: function () {
            // remove main one
            delete action.mainValorisations[destinationIdToRemove];

            // remove valorisation for same destination in detailed valorisations
            $scope._removeSpecisesValorisation(action, destinationIdToRemove);

            delete $scope.editedValorisation;
          },
          cancelChanges: function () { }
        };
        _displayConfirmDialog($scope, $("#confirmRemovedValorisation"), 400, deleteDestinationModel);
      }
    };

    $scope.deleteValorisation = function (action, mainValorisation, valorisationToRemove) {
      var deleteDestinationModel = {
        process: function () {
          var speciesCode = valorisationToRemove.speciesCode;
          var speciesValorisationForSpeciesCode = action.speciesValorisationsBySpeciesCode[speciesCode];
          speciesValorisationForSpeciesCode.splice(speciesValorisationForSpeciesCode.indexOf(valorisationToRemove), 1);

          action.speciesValorisations.splice(action.speciesValorisations.indexOf(valorisationToRemove), 1);

          var valorisationsForDestination = action.valorisationsByMainValorisationIds[mainValorisation.topiaId];
          valorisationsForDestination.splice(valorisationsForDestination.indexOf(valorisationToRemove), 1);

          if (valorisationToRemove.isDestinationValidForSpecies) {
            action.nbSameValorisationSectorFromMainValorisation[mainValorisation.topiaId]--;
          }

          delete $scope.editedValorisation;

          $scope._setSpeciesValorisationYealdAverage(action, mainValorisation);
        },
        cancelChanges: function () { }
      };
      _displayConfirmDialog($scope, $("#confirmRemovedValorisation"), 400, deleteDestinationModel);
    };

    $scope._checkAvailableActionAndInputTypes = function (intervention) {
      intervention.availableActionTypes = angular.copy($scope.agrosystInterventionTypes);

      $scope.actionsByAgrosystInterventionType = {
        APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX: [],
        APPLICATION_DE_PRODUITS_PHYTOSANITAIRES: [],
        AUTRE: [],
        ENTRETIEN_TAILLE_VIGNE_ET_VERGER: [],
        EPANDAGES_ORGANIQUES: [],
        IRRIGATION: [],
        LUTTE_BIOLOGIQUE: [],
        RECOLTE: [],
        SEMIS: [],
        TRANSPORT: [],
        TRAVAIL_DU_SOL: []
      };

      if (intervention.actionDtos.length > 0) {
        let nbActionTransportOrTravailDuSol = 0;

        // Iterate over each existing action to remove the in-use types
        angular.forEach(intervention.actionDtos, function (action) {
          var actionType = action.mainActionInterventionAgrosyst;
          $scope.actionsByAgrosystInterventionType[actionType].push(action);

          // no need to remove if actionType is neither ENTRETIEN_TAILLE_VIGNE_ET_VERGER nor TRAVAIL DU SOL (several actions of this type possible)
          if (actionType !== "ENTRETIEN_TAILLE_VIGNE_ET_VERGER" && actionType !== 'TRAVAIL_DU_SOL') {
            delete intervention.availableActionTypes[actionType];
          }

          // on compte le nombre d'actions de type TRANSPORT ou TRAVAIL_DU_SOL pour savoir s'il y a des actions supplémentaires autorisant l'ajout d'intrant de type AUTRE
          if (actionType === "TRANSPORT" || actionType === "TRAVAIL_DU_SOL") {
            nbActionTransportOrTravailDuSol = nbActionTransportOrTravailDuSol + 1;
          }
        });

        intervention.availableInputTypes = [];

        // filter available input types for the intervention actions
        var nbActionOtherThanTransportOrTravailDuSol = intervention.actionDtos.length - nbActionTransportOrTravailDuSol;

        if (intervention.actionDtos.find(action =>
            (action.mainActionInterventionAgrosyst === 'AUTRE' || action.mainActionInterventionAgrosyst === 'SEMIS')
            && substrateActionCodes.includes(action.mainActionReference_code))) {
          intervention.availableInputTypes.push("SUBSTRAT");
        }

        if (intervention.actionDtos.find(action => action.mainActionInterventionAgrosyst === 'AUTRE' && potActionCodes.includes(action.mainActionReference_code))) {
          intervention.availableInputTypes.push("POT");
        }

        angular.forEach($scope.inputTypesFullList, function (inputType) {

          // pour toutes les actions excepté Travail du sol et Transport il est possible de choisir un intrant de type autre
          if (inputType === "AUTRE" && nbActionOtherThanTransportOrTravailDuSol > 0) {
            intervention.availableInputTypes.push(inputType);
          } else {
            // Iterate over each action to add the available input types for the intervention
            angular.forEach(intervention.actionDtos, function (action) {
              // Do the rest only if the expected type is not already found
              if (intervention.availableInputTypes.indexOf(inputType) === -1 && inputType !== "IRRIGATION") {

                var actionType = action.mainActionInterventionAgrosyst;
                // an action exists for this input type
                if (inputType === actionType) {
                  intervention.availableInputTypes.push(actionType);
                }
              }
            });
          }
        });
      }
    };

    $scope.getNotRemovedUsage = function () {
      var remainingUsages = [];
      angular.forEach($scope.editedIntervention.usages, function (usage) {
        if ($scope.seedingActionUsagesToRemove.indexOf(usage) === -1) {
          remainingUsages.push(usage);
        }
      });
      return remainingUsages;
    };

    /*********************************************/
    /*                  Intrants                 */
    /*********************************************/

    // EffectiveCropCycleSpecies or PracticedCropCycleSpecies by codes

    $scope.getSpecies = function (speciesCode) {
      var result;
      if (speciesCode) {
        var interventionSpecies = $scope.editedIntervention.intermediateCrop ? $scope.connectionIntermediateSpecies : $scope.phaseConnectionOrNodeSpecies;
        // look for species from regular crop
        for (var index = 0; index < interventionSpecies.length; index++) {
          var species = interventionSpecies[index];
          if (species.code === speciesCode) {
            result = species;
            break;
          }
        }
      }
      return result;
    };

    $scope._addUsageLightBoxAttributes = function () {
      $scope.editedUsage.organicProductUnits = $scope.organicProductUnits;
      $scope.editedUsage.treatmentTargetCategories = $scope.treatmentTargetCategories;
      $scope.editedUsage.treatmentTargetCategoriesByParent = $scope.treatmentTargetCategoriesByParent;
      $scope.editedUsage.groupesCibles = $scope.groupesCibles;
      $scope.editedUsage.groupesCiblesByCode = $scope.groupesCiblesByCode;
      $scope.editedUsage.treatmentTargets = [];
      $scope.editedUsage.actaTreatmentProductTypes = $scope.actaTreatmentProductTypes;
      $scope.editedUsage.actaProducts = [];
      $scope.editedUsage.phytoProductUnits = $scope.phytoProductUnits;
      $scope.editedUsage.potInputUnits = $scope.potInputUnits;
      $scope.editedUsage.seedPlantUnits = $scope.seedPlantUnits;
      $scope.editedUsage.substrateInputUnits = $scope.substrateInputUnits;
      $scope.editedUsage.mineralProductUnits = $scope.mineralProductUnits;
      $scope.editedUsage.otherProductInputUnits = $scope.otherProductInputUnits;
      $scope.editedUsage.showOtherMineralProductElement = false;
    };

    $scope._removeUsageLightBoxAttributes = function (usage) {
      delete usage.organicProductUnits;
      delete usage.treatmentTargetCategories;
      delete usage.treatmentTargetCategoriesByParent;
      delete usage.groupesCibles;
      delete usage.treatmentTargets;
      delete usage.actaTreatmentProductTypes;
      delete usage.actaProducts;
      delete usage.phytoProductUnits;
      delete usage.potInputUnits;
      delete usage.seedPlantUnits;
      delete usage.substrateInputUnits;
      delete usage.mineralProductUnits;
      delete usage.otherProductInputUnits;
      delete usage.actions;
      delete usage.errorMessage;
    };

    $scope.startAddNewUsage = function (inputType) {

      $scope._resetEditedUsage();

      $scope._checkAvailableActionAndInputTypes($scope.editedIntervention);

      $scope.editedUsage = {};

      $scope._addUsageLightBoxAttributes();

      // l'usage ne peut être PLAN_COMPAGNE -> Plante compagne ça caractérise juste une ou plusieurs variété au sein d'une culture
      $scope.editedUsage.inputType = inputType === "PLAN_COMPAGNE" ? "SEMIS" : inputType;

      $scope.editedUsage.inputUsageTopiaId = "NEW-USAGE-" + guid();

      if (inputType === "SEMIS" || inputType === "PLAN_COMPAGNE" || inputType === "LUTTE_BIOLOGIQUE" || inputType === "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES") {
        $scope.editedUsage.targets = [];
        $scope.treatmentTarget = {};
      }

      $scope._loadInputs($scope.domainId, inputType).then(function (response) {
        $scope.treatInputs(response, inputType);
      });

      if (inputType === "AUTRE") {
        $scope.editedUsage.actions = $scope.editedIntervention.actionDtos;
        if ($scope.editedIntervention.actionDtos.length === 1) {
          const action = $scope.editedIntervention.actionDtos[0].mainAction;
          $scope.editedUsage.mainActionInterventionAgrosyst = action.intervention_agrosyst;
          $scope.editedUsage.mainActionReferenceLabel = action.reference_label;
        }
      } else if (inputType === "SEMIS" || inputType === "PLAN_COMPAGNE") {
        const action = $scope.actionsByAgrosystInterventionType[inputType][0].mainAction;
        $scope.editedUsage.mainActionInterventionAgrosyst = action.intervention_agrosyst;
        $scope.editedUsage.mainActionReferenceLabel = action.reference_label;

      } else if (inputType === "SUBSTRAT") {
        $scope.editedUsage.actions = $scope.editedIntervention.actionDtos.filter(action => substrateActionCodes.includes(action.mainActionReference_code));

        // dans le cas ou 1 action on sélectionne celle-ci
        let optionalAction;
        if($scope.editedUsage.actions.length === 1) {
           optionalAction = $scope.editedUsage.actions[0];
        }
        // dans le cas de + d'une action on priorise l'action de rempotage
        if (!optionalAction) {
          optionalAction = $scope.actionsByAgrosystInterventionType["AUTRE"].find(action => substrateActionCodes.includes(action.mainActionReference_code));
        }

        // sinon on prend l'action de semi 'plantation'
        if (!optionalAction &&
            $scope.actionsByAgrosystInterventionType.SEMIS &&
            $scope.actionsByAgrosystInterventionType.SEMIS[0]
             && $scope.actionsByAgrosystInterventionType.SEMIS[0].mainActionReference_code === 'SEW') {
             optionalAction = $scope.actionsByAgrosystInterventionType.SEMIS[0];
        }

        if (optionalAction) {
            const action = optionalAction.mainAction;
            $scope.editedUsage.mainActionInterventionAgrosyst = action.intervention_agrosyst;
            $scope.editedUsage.mainActionReferenceLabel = action.reference_label;
            $scope.editedUsage.mainActionId = action.topiaId;
        }

      } else if (inputType === "POT") {
        let optionalAction = $scope.actionsByAgrosystInterventionType["AUTRE"].find(action => potActionCodes.includes(action.mainActionReference_code));
        if (optionalAction) {
          const action = optionalAction.mainAction;
          $scope.editedUsage.mainActionInterventionAgrosyst = action.intervention_agrosyst;
          $scope.editedUsage.mainActionReferenceLabel = action.reference_label;
        }

      } else {
        const action = $scope.actionsByAgrosystInterventionType[inputType][0].mainAction;
        $scope.editedUsage.mainActionInterventionAgrosyst = action.intervention_agrosyst;
        $scope.editedUsage.mainActionReferenceLabel = action.reference_label;
      }

      $scope._showUsageLightBox();
    };

    // validation de l'usage
    $scope.validateUsage = function (usage, intervention, validPersistedQtAvg, validNewQtAvg) {
      var result = true;

      var inputType = usage.inputType;
      if (inputType === "APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX") {
        result &= angular.isDefined(usage.domainMineralProductInputDto);

      } else if (inputType === "EPANDAGES_ORGANIQUES") {
        result &= angular.isDefined(usage.domainOrganicProductInputDto);

      } else if (inputType === "AUTRE") {
        result &= angular.isDefined(usage.domainOtherProductInputDto);

      } else if (inputType === "SUBSTRAT") {
        result &= angular.isDefined(usage.domainSubstrateInputDto) && angular.isDefined(usage.qtAvg);

      } else if (inputType === "POT") {
        result &= angular.isDefined(usage.domainPotInputDto) && angular.isDefined(usage.qtAvg);

      } else if (inputType === "LUTTE_BIOLOGIQUE" || inputType === "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES") {
        result &= angular.isDefined(usage.domainPhytoProductInputDto);

      } else if (inputType === "SEMIS" || inputType === "PLAN_COMPAGNE") {
        result &= angular.isDefined(usage.domainSeedLotInputDto);
        if (result && intervention && usage.domainSeedLotInputDto) {
          if (validPersistedQtAvg) {
            result &= angular.isDefined(usage.seedingSpeciesDtos);
            if (result) {
              result &= usage.seedingSpeciesDtos.length > 0;
              angular.forEach(usage.seedingSpeciesDtos, function(speciesInputUsage) {
                result &= speciesInputUsage.domainSeedSpeciesInputDto !== undefined;
                if (result){
                  var inputSpeciesCode = speciesInputUsage.domainSeedSpeciesInputDto.speciesSeedDto.code;
                  result &= intervention.intermediateCrop ? true : intervention.speciesStadesDtos.find(ss => ss.speciesCode === inputSpeciesCode) !== undefined;
                  result &= speciesInputUsage.qtAvg !== undefined;
                }

              })
            }
          }
          if (validNewQtAvg) {
            angular.forEach(usage.domainSeedLotInputDto.speciesInputs, function(speciesInput) {
              var inputSpeciesCode = speciesInput.speciesSeedDto ? speciesInput.speciesSeedDto.code : '';
              result &= intervention.intermediateCrop ? true : intervention.speciesStadesDtos.find(ss => ss.speciesCode === inputSpeciesCode) !== undefined;
              result &= speciesInput.qtAvg !== undefined;
            })
          }

        }
      }

      result = Boolean(result);

      return result;
    };

    $scope._showUsageLightBox = function () {

      var editUsageModel = {
        process: function () {
          var validationMessage = $scope._recordUsage();
          if (validationMessage) {
            $scope.editedUsage.errorMessage = validationMessage;
            $scope.nonValidAlert("INPUT");
            var doNotClose = true;
            return doNotClose;
          } else {
            $scope._resetEditedUsage();
          }
        },
        cancelChanges: function () { $scope._resetEditedUsage(); }
      };

      _displayConfirmDialog($scope, $("#usage-edit-lightbox"), '80%', editUsageModel);
    };

    $scope._startEditPhytoProductUsage = function (usage, intervention) {
      var inputType = usage.inputType;
      $scope.treatmentTarget = {};
      $scope.computeRefDose();

      if (!usage.targets) {
        usage.targets = [];
      }

      if (inputType === "SEMIS" || inputType === "PLAN_COMPAGNE" ) {
        // cherche si l'action semis à des traitements de semences.
        $scope._checkAvailableActionAndInputTypes(intervention);
      }

      $scope._showUsageLightBox();
    };

    $scope.startEditUsage = function (editedUsage) {

      $scope._resetEditedUsage();

      $scope.editedUsage = angular.copy(editedUsage);

      if (editedUsage.domainMineralProductInputDto) {
        $scope.editedUsage.inputId = editedUsage.domainMineralProductInputDto.topiaId;
      } else if (editedUsage.domainPhytoProductInputDto) {
        $scope.editedUsage.inputId = editedUsage.domainPhytoProductInputDto.topiaId;
        if ($scope.editedUsage.domainPhytoProductInputDto.productType) {
          $scope.editedUsage.productType = $scope.editedUsage.domainPhytoProductInputDto.productType;
        }
      } else if (editedUsage.domainOrganicProductInputDto) {
        $scope.editedUsage.inputId = editedUsage.domainOrganicProductInputDto.topiaId;
      } else if (editedUsage.domainOtherProductInputDto) {
        $scope.editedUsage.inputId = editedUsage.domainOtherProductInputDto.topiaId;
      } else if (editedUsage.domainSubstrateInputDto) {
        $scope.editedUsage.inputId = editedUsage.domainSubstrateInputDto.topiaId;
      } else if (editedUsage.domainPotInputDto) {
        $scope.editedUsage.inputId = editedUsage.domainPotInputDto.topiaId;
      } else if (editedUsage.domainSeedLotInputDto) {
        $scope.editedUsage.inputId = editedUsage.domainSeedLotInputDto.topiaId;
        if ($scope.editedUsage.domainSeedLotInputDto.speciesInputs && $scope.editedUsage.domainSeedLotInputDto.speciesInputs.length > 0) {
          $scope.filterDomainSeedSpeciesFromSpeciesStades();
          $scope.editedUsage.domainSeedLotInputDto.speciesInputs.forEach((species) => {
            const seedingSpeciesDtos = $scope.editedUsage.seedingSpeciesDtos.filter((dto) => dto.domainSeedSpeciesInputDto.topiaId === species.topiaId);
            if (seedingSpeciesDtos && seedingSpeciesDtos.length > 0) {
              species.qtAvg = seedingSpeciesDtos[0].qtAvg;
              species.deepness = seedingSpeciesDtos[0].deepness;
            }
            if (species.speciesPhytoInputDtos && species.speciesPhytoInputDtos.length > 0) {
              species.speciesPhytoInputDtos.forEach((phyto) => {
                const hasProduct = seedingSpeciesDtos && seedingSpeciesDtos.length > 0 && seedingSpeciesDtos[0].seedProductInputDtos && seedingSpeciesDtos[0].seedProductInputDtos.length > 0;
                const seedProductInputDtos = hasProduct ? seedingSpeciesDtos[0].seedProductInputDtos.filter((dto) => dto.domainPhytoProductInputDto.topiaId === phyto.topiaId) : [];
                if (seedProductInputDtos && seedProductInputDtos.length > 0) {
                  phyto.targets = seedProductInputDtos[0].targets;
                  phyto.qtAvg = seedProductInputDtos[0].qtAvg;
                  phyto.qtMin = seedProductInputDtos[0].qtMin;
                  phyto.qtMed = seedProductInputDtos[0].qtMed;
                  phyto.qtMax = seedProductInputDtos[0].qtMax;
                  phyto.comment = seedProductInputDtos[0].comment;
                  phyto.inputUsageTopiaId = seedProductInputDtos[0].inputUsageTopiaId;
                } else {
                  phyto.targets = phyto.targets ? phyto.targets : [];
                }
                phyto.treatmentTarget = {}
              });
            }
          });
        }
      }

      $scope._addUsageLightBoxAttributes();

      // l'usage ne peut être PLAN_COMPAGNE -> Plante compagne ça caractérise juste une ou plusieurs variété au sein d'une culture
      var inputType = $scope.editedUsage.inputType === "PLAN_COMPAGNE" ? "SEMIS" : $scope.editedUsage.inputType;

      $scope._loadInputs($scope.domainId, inputType).then(function (response) {
        $scope.treatInputs(response, inputType);
      });

      if (inputType === "SEMIS" || inputType === "PLAN_COMPAGNE" || inputType === "LUTTE_BIOLOGIQUE" || inputType === "APPLICATION_DE_PRODUITS_PHYTOSANITAIRES") {
        $scope._startEditPhytoProductUsage($scope.editedUsage, $scope.editedIntervention);

      } else if (inputType === "AUTRE") {
        $scope.editedUsage.actions = $scope.editedIntervention.actionDtos;
        $scope._setActionToUsage($scope.editedUsage, $scope.editedIntervention.actionDtos);
      } else if (inputType === "SUBSTRAT") {
        $scope.editedUsage.actions = $scope.editedIntervention.actionDtos.filter(action => substrateActionCodes.includes(action.mainActionReference_code));
        $scope.editedUsage.mainActionId = $scope.editedUsage.actions.filter(action => action.topiaId === $scope.editedUsage.actionId)[0].mainActionId;
      }

      $scope._showUsageLightBox();

    };

    $scope.treatInputs = function (response, inputType) {
      delete $scope.editedUsage.inputs;
      if (inputType === "SEMIS" || inputType === "PLAN_COMPAGNE") {
        var speciesCodes = Object.keys($scope.speciesStadeBySpeciesCode);
        var cropCode;
        if ($scope.editedIntervention.intermediateCrop && $scope.connectionIntermediateCrop) {
          cropCode = $scope.connectionIntermediateCrop.code || $scope.connectionIntermediateCrop.croppingPlanEntryCode;
        } else {
          cropCode = $scope.phaseOrNodeCrop ? $scope.phaseOrNodeCrop.croppingPlanEntryCode || $scope.phaseOrNodeCrop.code : undefined;
        }
        $scope.editedUsage.inputs = response.data.filter((input) => {
          return input.cropSeedDto.code === cropCode;
        });
      } else {
        $scope.editedUsage.inputs = response.data;
        //$scope.autoSelectInput(inputType); Désactivé pour l'instant "en sélectionnant l'intrant on se rend pas compte s'il en manque ou pas dans le local"
      }
      $scope.sortInputs();
      $scope.getProductTypes();
    };

    $scope._recordUsage = function () {

      var inputType = $scope.editedUsage.inputType;

      // The mineralProduct is created with it's type and shape from the jsp and then push from the referential
      //   as soon as a composition is selected.
      //   if no composition is selected then mineralProduct object is not valid and so return an error
      if (!$scope.validateUsage($scope.editedUsage, $scope.editedIntervention, false, true)) {
        return $scope.messages.invalidUsage;
      }

      // compute working time
      if (inputType === "EPANDAGES_ORGANIQUES") {
        $scope._computeSpendingTime($scope.editedIntervention);
      }

      if (inputType === "SEMIS" || inputType === "PLAN_COMPAGNE") {
        $scope._retreatSpeciesAndPhytos($scope.editedUsage);
      }

      if (inputType === "SUBSTRAT") {
        let action = $scope.editedIntervention.actionDtos.filter((action) => action.mainActionId === $scope.editedUsage.mainActionId)[0];
        $scope.editedUsage.actionId = action.topiaId;
      }

      $scope._removeUsageLightBoxAttributes($scope.editedUsage);

      $scope._computeSpendingTime($scope.editedIntervention);

      $scope._addOrReplaceInIntervention($scope.editedUsage);

      delete $scope.seedingAction;
    };

    $scope._retreatSpeciesAndPhytos = function (usage) {
      // l'usage ne peut être PLAN_COMPAGNE -> Plante compagne ça caractérise juste une ou plusieurs variété au sein d'une culture
      if (usage.domainSeedLotInputDto.inputType === "SEMIS" || usage.domainSeedLotInputDto.inputType === "PLAN_COMPAGNE") {
        usage.inputType = "SEMIS";
      } else {
        usage.inputType = usage.domainSeedLotInputDto.inputType;
      }
      if (usage.domainSeedLotInputDto && usage.domainSeedLotInputDto.speciesInputs && usage.domainSeedLotInputDto.speciesInputs.length > 0) {
        if (usage.domainSeedLotInputDto.speciesInputs.length === 1) {
          usage.qtAvg = usage.domainSeedLotInputDto.speciesInputs[0].qtAvg;
          usage.deepness = usage.domainSeedLotInputDto.speciesInputs[0].deepness;
        }
        usage.seedingSpeciesDtos = usage.domainSeedLotInputDto.speciesInputs.map((species) => {
          var products = [];
          if (species.speciesPhytoInputDtos && species.speciesPhytoInputDtos.length > 0) {
            products = species.speciesPhytoInputDtos.map((phyto) => ({
              ...(species.seedProductInputDtos && species.seedProductInputDtos.some((dto) => dto.domainPhytoProductInputDto.topiaId === phyto.topiaId) ? species.seedProductInputDtos.filter((dto) => dto.domainPhytoProductInputDto.topiaId === phyto.topiaId)[0] : {}),
              domainPhytoProductInputDto: phyto,
              inputType: phyto.inputType,
              targets: phyto.targets,
              qtAvg: phyto.qtAvg,
              qtMin: phyto.qtMin,
              qtMed: phyto.qtMed,
              qtMax: phyto.qtMax,
              comment: phyto.comment,
              inputUsageTopiaId: phyto.inputUsageTopiaId
            }));
          }

          var result = {
            ...(usage.seedingSpeciesDtos && usage.seedingSpeciesDtos.some((dto) =>
              dto.domainSeedSpeciesInputDto.topiaId === species.topiaId) ? usage.seedingSpeciesDtos.filter((dto) => dto.domainSeedSpeciesInputDto.topiaId === species.topiaId)[0] : {}),
            productName: species.productName ? species.productName : species.inputName,
            qtAvg: species.qtAvg,
            deepness: species.deepness,
            domainSeedSpeciesInputDto: species,
            inputType: species.inputType,
            usageUnit: species.usageUnit,
            speciesIdentifier: $scope.practicedSystem ? species.speciesSeedDto.code : species.speciesSeedDto.topiaId,
            seedProductInputDtos: products
          }
          return result;
        });
      }
    };

    $scope._addOrReplaceInIntervention = function (usage) {
      const usages = $scope.getUsagesFromEditedIntervention(usage.inputType, usage.mainActionId);
      $scope._addOrReplaceInArray(usages, usage);
    };

    $scope._addOrReplaceInArray = function (usages, usage) {
      const index = usages.findIndex((elt) => elt.inputUsageTopiaId === usage.inputUsageTopiaId);
      if (index === -1) {
        usages.push(usage);
      } else {
        usages[index] = usage;
      }
    };

    $scope._setActionToUsage = function (usage, actions) {
      var inputAction;
      if (usage.mineralFertilizersSpreadingAction) {
        inputAction = usage.mineralFertilizersSpreadingAction;
      }
      else if (usage.pesticidesSpreadingAction) {
        inputAction = usage.pesticidesSpreadingAction;
      }
      else if (usage.organicFertilizersSpreadingAction) {
        inputAction = usage.organicFertilizersSpreadingAction;
      }
      else if (usage.irrigationAction) {
        inputAction = usage.irrigationAction;
      }
      else if (usage.biologicalControlAction) {
        inputAction = usage.biologicalControlAction;
      }
      else if (usage.harvestingAction) {
        inputAction = usage.harvestingAction;
      }
      else if (usage.seedingAction) {
        inputAction = usage.seedingAction;
      }
      else if (usage.maintenancePruningVinesAction) {
        inputAction = usage.maintenancePruningVinesAction;
      }
      else if (usage.otherAction) {
        inputAction = usage.otherAction;
      }

      // peut-être null dans le cas d'une erreur de validation de l'intrant
      if (inputAction) {
        angular.forEach(actions, function (action) {
          if (inputAction.topiaId === action.topiaId) {
            usage.action = action;
          }
        });
      }
    };

    $scope.deleteUsage = function (usage) {
      let usages = $scope.getUsagesFromEditedIntervention(usage.inputType, usage.mainActionId);
      var index = usages.findIndex((elt) => elt.inputUsageTopiaId === usage.inputUsageTopiaId);
      if (index !== -1) {
        var deleteUsagesModel = {
          process: function () { usages.splice(index, 1); },
          cancelChanges: function () { }
        };
        _displayConfirmDialog($scope, $("#confirmRemovedUsage"), 400, deleteUsagesModel);
      }
    };

    $scope.getUsagesFromEditedIntervention = function (inputType, mainActionId) {
      var action;

      if (inputType === 'POT') {
        action = $scope.editedIntervention.actionDtos.filter((action) => action.mainActionInterventionAgrosyst === "AUTRE")[0];
      } else if (inputType === 'AUTRE' || inputType === 'SUBSTRAT') {
        action = $scope.editedIntervention.actionDtos.filter((action) => action.mainActionId === mainActionId)[0];
      } else if (inputType === 'SEMIS' || inputType === 'PLAN_COMPAGNE'){
        action = $scope.editedIntervention.actionDtos.filter((action) => action.mainActionInterventionAgrosyst === 'SEMIS')[0];
      } else {
        action = $scope.editedIntervention.actionDtos.filter((action) => action.mainActionInterventionAgrosyst === inputType)[0];
      }
      if (inputType === 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX') {
        action.mineralProductInputUsageDtos = action.mineralProductInputUsageDtos ? action.mineralProductInputUsageDtos : [];
        return action.mineralProductInputUsageDtos;
      } else if ((inputType === 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES' || inputType === 'LUTTE_BIOLOGIQUE')) {
        action.phytoProductInputUsageDtos = action.phytoProductInputUsageDtos ? action.phytoProductInputUsageDtos : [];
        return action.phytoProductInputUsageDtos;
      } else if (inputType === 'EPANDAGES_ORGANIQUES') {
        action.organicProductInputUsageDtos = action.organicProductInputUsageDtos ? action.organicProductInputUsageDtos : [];
        return action.organicProductInputUsageDtos;
      } else if (inputType === 'AUTRE') {
        action.otherProductInputUsageDtos = action.otherProductInputUsageDtos ? action.otherProductInputUsageDtos : [];
        return action.otherProductInputUsageDtos;
      } else if (inputType === 'SUBSTRAT') {
        action.substrateInputUsageDtos = action.substrateInputUsageDtos ? action.substrateInputUsageDtos : [];
        return action.substrateInputUsageDtos;
      } else if (inputType === 'POT') {
        action.potInputUsageDtos = action.potInputUsageDtos ? action.potInputUsageDtos : [];
        return action.potInputUsageDtos;
      } else if ((inputType === 'SEMIS' || inputType === 'PLAN_COMPAGNE')) {
        action.seedLotInputUsageDtos = action.seedLotInputUsageDtos ? action.seedLotInputUsageDtos : [];
        return action.seedLotInputUsageDtos;
      }
      return undefined;
    };

    $scope._getAllSeedingSpeciesByCodes = function (intervention) {
      var allSpeciesForSeedingByCodes = {};
      var allSpeciesForSeeding = $scope._computeSpeciesFromSpeciesStades(intervention, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
      angular.forEach(allSpeciesForSeeding, function (speciesForSeeding) {
        allSpeciesForSeedingByCodes[speciesForSeeding.code] = speciesForSeeding;
      });
      return allSpeciesForSeedingByCodes;
    };

    $scope._addRefDoseIfExists = function (refDose) {
      if (!refDose) {
        var refDose = {};
      }
      if (angular.isUndefined(refDose.value)) {
        refDose.error = $scope.messages.unavailableReferenceDose;
      }
      return refDose;
    };

    $scope._loadActaReferenceDose = function (usage, refInputId, treatedSpecies) {
      if (angular.isDefined(usage) && angular.isDefined(refInputId) && angular.isDefined(treatedSpecies) && treatedSpecies.length > 0) {
        let campaign = $scope.domainCampaign;

        var targets = usage.targets;// ift cible
        var targetIds = null;
        var groupesCibles = null;
        if (angular.isDefined(targets) && targets.length > 0) {
          targetIds = [];
          groupesCibles = [];
          angular.forEach(targets, function (target) {
            if (target.target) {
              targetIds.push(target.target.reference_id ? target.target.reference_id : target.target.identifiant);
            } else if (target.targetId) {
              targetIds.push(target.targetId);
            }
            if (target.codeGroupeCibleMaa) {
              groupesCibles.push(target.codeGroupeCibleMaa);
            }
          });
          targetIds = encodeURIComponent(angular.toJson(targetIds))
          groupesCibles = encodeURIComponent(angular.toJson(groupesCibles))
        }
        return $http.post(
          ENDPOINTS.loadReferenceDoseJson, "refInputId=" + encodeURIComponent(refInputId) +
          "&speciesIds=" + encodeURIComponent(angular.toJson(treatedSpecies)) + "&targetIds=" + targetIds + "&groupesCibles=" + groupesCibles + "&campaign=" + campaign,
          { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
        ).then(function (response) {
          if (response.data && "null" !== response.data) {
            usage.referenceDose = response.data;
            if (usage.referenceDose.IFT_Ancienne) {
              usage.phytoProductUnit = usage.referenceDose.IFT_Ancienne.unit;
            } else if (usage.referenceDose.IFT_Cible_NonMillesime) {
              usage.phytoProductUnit = usage.referenceDose.IFT_Cible_NonMillesime.unit;
            }
            usage.referenceDose.IFT_Ancienne = $scope._addRefDoseIfExists(usage.referenceDose.IFT_Ancienne);
            usage.referenceDose.IFT_Cible_NonMillesime = $scope._addRefDoseIfExists(usage.referenceDose.IFT_Cible_NonMillesime);
            usage.referenceDose.IFT_Cible_Millesime = $scope._addRefDoseIfExists(usage.referenceDose.IFT_Cible_Millesime);
          }
        }).
          catch(function (response) {
            var message = $scope.messages.referenceDosesLoadingFailed;
            console.error(message, response);
            addPermanentError(message, response.status);
          });
      } else {
        return $q.when();
      }
    };

    $scope._getTreatedSpecies = function (intervention, usage, intermediateSpecies, phaseConnectionOrNodeSpecies) {
      var treatedSpecies = [];

      if (usage.domainPhytoProductInputDto) {
        // Dans le cas d'un intrant sur une action de type semi
        // il faut prendre en compte uniquement les espèces concernées par l'intrant

        // Pour les autre types d'action, si l'intervention ne porte pas sur une culture intermédiaire alors les espèces
        // peuvent être manuellement sélectionnées. Dans le cas contraire toutes les espèces sont sélectionnées
        if (intervention.intermediateCrop) {
          angular.forEach(intermediateSpecies, function (species) {
            var speciesId = $scope._getSpeciesId(species);
            treatedSpecies.push(speciesId);
          });
        } else {
          angular.forEach(intervention.speciesStadesDtos, function (speciesStade) {
            var speciesStadeSpeciesCode = speciesStade.speciesCode;
            angular.forEach(phaseConnectionOrNodeSpecies, function (species) {
              if (species.code === speciesStadeSpeciesCode) {
                var speciesId = $scope._getSpeciesId(species);
                treatedSpecies.push(speciesId);
              }
            });
          });
        }
      }

      return treatedSpecies;
    };

    $scope.computeRefDose = function () {

      delete $scope.editedUsage.referenceDose;

      var treatedSpecies = $scope._getTreatedSpecies($scope.editedIntervention, $scope.editedUsage, $scope.connectionIntermediateSpecies, $scope.phaseConnectionOrNodeSpecies);
      if (treatedSpecies.length > 0) {
        var refInputId = $scope.editedUsage.domainPhytoProductInputDto.refInputId;
        $scope._loadActaReferenceDose($scope.editedUsage, refInputId, treatedSpecies);
      } else {
        $scope.editedUsage.referenceDose = { error: $scope.messages.referenceDoseMissingSpecies };
      }

    };

    $scope.computeRefDosePhyto = function (phyto, treatedSpecies) {
      delete phyto.referenceDose;
      if (treatedSpecies.length > 0) {
        $scope._loadActaReferenceDose(phyto, phyto.refInputId, treatedSpecies);
      } else {
        phyto.referenceDose = { error: $scope.messages.referenceDoseMissingSpecies };
      }
    };

    $scope._computeCompositionKey = function (element) {
      // convert to string
      element = element + "";
      // split to integer and decimal part
      var part = element.split('.');
      var result = part[0] + ".";
      result += part.length > 1 ? part[1] + ("000000".substr(part[1].length)) : "000000";
      return result;
    };

    $scope.getGroupeCibleLabel = function (codeGroupeCibleMaa) {
      return $scope.editedUsage.groupesCiblesByCode[codeGroupeCibleMaa].groupeCibleMaa;
    };

    $scope.loadTreatmentTargets = function (category) {
      delete $scope.editedUsage.treatmentTargets;
      return $http.post(
        ENDPOINTS.loadTreatmentTargetsJson, "category=" + category,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
      ).then(function (response) {
        $scope.editedUsage.treatmentTargets = response.data;
        if ($scope.editedUsage.treatmentTargets && $scope.editedUsage.treatmentTargets.length === 1) {
          $scope.treatmentTarget.target = $scope.editedUsage.treatmentTargets[0];
          $scope.treatmentTargetSelected();
        }
      }).
        catch(function (response) {
          var message = $scope.messages.treatmentTargetsLoadingFailed;
          console.error(message, response);
          addPermanentError(message, response.status);
        });
    };

    $scope.loadTreatmentTargetsPhyto = function (category, phyto, species) {
      delete phyto.treatmentTargets;
      return $http.post(
        ENDPOINTS.loadTreatmentTargetsJson, "category=" + category,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
      ).then(function (response) {
        phyto.treatmentTargets = response.data;
        if (phyto.treatmentTargets && phyto.treatmentTargets.length === 1) {
          phyto.treatmentTarget.target = phyto.treatmentTargets[0];
          $scope.treatmentTargetSelectedPhyto(phyto, species);
        }
      }).
        catch(function (response) {
          var message = $scope.messages.treatmentTargetsLoadingFailed;
          console.error(message, response);
          addPermanentError(message, response.status);
        });
    };

    $scope.treatmentTargetCategorySelected = function () {
      var category = $scope.treatmentTarget.category;
      $scope.loadTreatmentTargets(category);
    };

    $scope.treatmentTargetCategorySelectedPhyto = function (phyto, species) {
      var category = phyto.treatmentTarget.category;
      $scope.loadTreatmentTargetsPhyto(category, phyto, species);
    };

    $scope.isGroupeCibleInCategory = function (groupeCible) {
      return !$scope.treatmentTarget.category
        || $scope.editedUsage && $scope.editedUsage.treatmentTargetCategoriesByParent[$scope.treatmentTarget.category].bioAgressorTypes.includes(groupeCible.referenceParam);
    };

    $scope.isTargetInGroupeCible = function (target) {
      return !$scope.treatmentTarget.groupeCibleMaa
        || $scope.treatmentTarget.groupeCibleMaa.cibleEdiRefCodes.includes(target.reference_code || target.identifiant);
    };

    $scope.isTreatmentTargetValid = function () {
      return $scope.treatmentTarget.category && ($scope.treatmentTarget.groupeCibleMaa || $scope.treatmentTarget.target);
    };

    $scope.isTreatmentTargetValidPhyto = function (phyto) {
      return phyto.treatmentTarget.category && (phyto.treatmentTarget.groupeCibleMaa || phyto.treatmentTarget.target);
    };

    $scope.treatmentTargetSelected = function () {
      var target = angular.copy($scope.treatmentTarget);
      if (target.groupeCibleMaa) {
        target.codeGroupeCibleMaa = target.groupeCibleMaa.codeGroupeCibleMaa;
      }
      if (target.target) {
        target.refBioAgressorTargetId = target.target.topiaId;
      }
      $scope.editedUsage.targets.push(target);
      delete $scope.treatmentTarget.category;
      delete $scope.treatmentTarget.groupeCibleMaa
      delete $scope.treatmentTarget.target;
      delete $scope.treatmentTargets;

      if ($scope.editedUsage.domainPhytoProductInputDto) {
        $scope.computeRefDose();
      }
    };

    $scope.doShowProducts = function (phytos, species) {
      if (phytos && phytos.length) {
        angular.forEach(phytos, function (phyto) {
          $scope.computeRefDosePhyto(phyto, [species.speciesSeedDto.speciesId], campaign);
        });
      }
    };

    $scope.treatmentTargetSelectedPhyto = function (phyto, species) {
      var target = angular.copy(phyto.treatmentTarget);
      if (target.groupeCibleMaa) {
        target.codeGroupeCibleMaa = target.groupeCibleMaa.codeGroupeCibleMaa;
      }
      if (target.target) {
        target.refBioAgressorTargetId = target.target.topiaId;
      }
      phyto.targets.push(target);
      delete phyto.treatmentTarget.category;
      delete phyto.treatmentTarget.groupeCibleMaa
      delete phyto.treatmentTarget.target;
      delete phyto.treatmentTargets;

      $scope.computeRefDosePhyto(phyto, [species.speciesSeedDto.speciesId]);
    };

    $scope.deleteTreatmentTarget = function (target) {
      var index = $scope.editedUsage.targets.indexOf(target);
      $scope.editedUsage.targets.splice(index, 1);

      if ($scope.editedUsage.domainPhytoProductInputDto) {
        $scope.computeRefDose();
      }
    };

    $scope.deleteTreatmentTargetPhyto = function (target, phyto, species) {
      var index = phyto.targets.indexOf(target);
      phyto.targets.splice(index, 1);
      $scope.computeRefDosePhyto(phyto, [species.speciesSeedDto.speciesId]);
    };

    $scope.mineralProductSelected = function () {
      $scope.editedUsage.domainMineralProductInputDto = $scope.editedUsage.inputs.filter((input) => input.topiaId === $scope.editedUsage.inputId)[0];
    };

    $scope.productTypeSelected = function () {
      $scope.editedUsage.inputs = $scope.editedUsage.allInputs.filter((input) => input.productType === $scope.editedUsage.productType);
      if ($scope.editedUsage.inputs && $scope.editedUsage.inputs.length === 1) {
        $scope.editedUsage.inputId = $scope.editedUsage.inputs[0].topiaId;
        $scope.phytoProductSelected();
      } else {
        $scope.editedUsage.inputId = undefined;
        $scope.editedUsage.domainPhytoProductInputDto = undefined;
        $scope.editedUsage.referenceDose = undefined;
      }
    };

    $scope.phytoProductSelected = function () {
      $scope.editedUsage.domainPhytoProductInputDto = $scope.editedUsage.inputs.filter((input) => input.topiaId === $scope.editedUsage.inputId)[0];
      if ($scope.editedUsage.domainPhytoProductInputDto.productType) {
        $scope.editedUsage.productType = $scope.editedUsage.domainPhytoProductInputDto.productType;
      }

      $scope.editedUsage.productName = $scope.editedUsage.domainPhytoProductInputDto.inputName;
      $scope.computeRefDose();
    };

    $scope.organicProductSelected = function () {
      $scope.editedUsage.domainOrganicProductInputDto = $scope.editedUsage.inputs.filter((input) => input.topiaId === $scope.editedUsage.inputId)[0];
    };

    $scope.substrateSelected = function () {
      $scope.editedUsage.domainSubstrateInputDto = $scope.editedUsage.inputs.filter((input) => input.topiaId === $scope.editedUsage.inputId)[0];
    }

    $scope.potSelected = function () {
      $scope.editedUsage.domainPotInputDto = $scope.editedUsage.inputs.filter((input) => input.topiaId === $scope.editedUsage.inputId)[0];
    }

    $scope.filterDomainSeedSpeciesFromSpeciesStades = function() {
      if ($scope.editedUsage.domainSeedLotInputDto.speciesInputs) {
        var interventionSeedingSpecies = [];
        angular.forEach($scope.editedUsage.domainSeedLotInputDto.speciesInputs, function(species){
          if ($scope.isSpeciesQtyAvailable(species)) {
            interventionSeedingSpecies.push(species);
          }
        });
        $scope.editedUsage.domainSeedLotInputDto.speciesInputs = interventionSeedingSpecies;
      }
    }

    $scope.semisSelected = function () {
      $scope.editedUsage.domainSeedLotInputDto = $scope.editedUsage.inputs.filter((input) => input.topiaId === $scope.editedUsage.inputId)[0];
      if ($scope.practicedSystem) {
        $scope.editedUsage.croppingPlanEntryIdentifier = $scope.editedUsage.domainSeedLotInputDto.cropSeedDto.code;
      } else {
        $scope.editedUsage.croppingPlanEntryIdentifier = $scope.editedUsage.domainSeedLotInputDto.cropSeedDto.topiaId;
      }

      $scope.filterDomainSeedSpeciesFromSpeciesStades();

      if ($scope.editedUsage.domainSeedLotInputDto.speciesInputs && $scope.editedUsage.domainSeedLotInputDto.speciesInputs.length > 0) {
        $scope.editedUsage.domainSeedLotInputDto.speciesInputs.forEach((species) => {
          if (species.speciesPhytoInputDtos && species.speciesPhytoInputDtos.length > 0) {
            species.speciesPhytoInputDtos.forEach((phyto) => {
              phyto.targets = [];
              phyto.treatmentTarget = {}
            });
          }
        });
      }
    }

    $scope.otherSelected = function () {
      $scope.editedUsage.domainOtherProductInputDto = $scope.editedUsage.inputs.filter((input) => input.topiaId === $scope.editedUsage.inputId)[0];
    }

    $scope._isValidKey = function (key) {
      var value = $scope.mineralProductElementNamesAndLibelles[key];
      var result = angular.isDefined(value);
      return result;
    };

    $scope.getMineralProductCompositionKeys = function () {
      var keys = Object.keys($scope.mineralProductElementNamesAndLibelles);
      return keys;
    };

    $scope._computeTotalWeightPercent = function (mineralProduct) {
      var totalWeightPercent = 0;
      if (mineralProduct) {
        var keys = Object.keys(mineralProduct);
        var mineralProductCompositionKeys = keys.filter($scope._isValidKey);
        angular.forEach(mineralProductCompositionKeys, function (mineralProductCompositionKey) {
          var elementWeightPercent = mineralProduct[mineralProductCompositionKey];
          var value = angular.isDefined(elementWeightPercent) && elementWeightPercent !== '' ? elementWeightPercent : null;
          if (value == null) {
            delete mineralProduct[mineralProductCompositionKey];
          } else {
            totalWeightPercent += parseFloat(value);
          }
        });
      }
      return totalWeightPercent;
    };

    $scope.orderedMineralProductElements = function (element) {
      // this is done to keep element ordered
      var allElements = $scope.getMineralProductCompositionKeys();
      return allElements.indexOf(element);
    };

    $scope._getElementsUsedForMineralProduct = function (mineralProduct) {
      var elements = [];
      var keys = Object.keys(mineralProduct);
      var mineralProductCompositionKeys = keys.filter($scope._isValidKey);
      angular.forEach(mineralProductCompositionKeys, function (mineralProductCompositionKey) {
        var found = angular.isDefined(mineralProduct[mineralProductCompositionKey]);
        if (found) {
          elements.push(mineralProductCompositionKey);
        }
      });
      return elements;
    };

    $scope._updateCommonlyUsedMineralElement = function (commonlyUsedElements, mineralProduct) {
      var elements = $scope._getElementsUsedForMineralProduct(mineralProduct);
      angular.forEach(elements, function (element) {
        var index = commonlyUsedElements.indexOf(element);
        if (index === -1) {
          commonlyUsedElements.push(element);
        }
      });
    };

    $scope.changeRefFertiMinUnifaElementsVisibility = function (input) {
      input.showOtherMineralProductElement = !input.showOtherMineralProductElement;
      $scope._updateCommonlyUsedMineralElement(input.commonlyUsedElements, input.mineralProduct);
    };

    // load all ferti min protucts that match the category and the shape divined into the scope
    //   and also the ferti min protucts related to it's id given as parameter (so an deactivated product is loaded as well)
    $scope._loadInputs = function (domainId, inputType) {
      // domainId is topiaId from effective domain code for practiced
      var query = "&domainId=" + domainId +
        "&inputType=" + inputType;
      // for practiced: growingSystemId
      var growingSystemId = null;
      if ($scope.practicedSystem) {
        growingSystemId = $scope.practicedSystem.growingSystemTopiaId;
      }
      query += "&growingSystemId=" + growingSystemId;
      return $http.post(
        ENDPOINTS.loadInputStockListJson, query,
        { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
      ).catch(function (response) {
          var message = "Echec de récupération des intrants du domaine de type " + inputType;
          console.error(message, response);
          addPermanentError(message, response.status);
        });
    };

    function _getDisplayNameFromComposition(composition) {
      var results = [];
      if (composition) {
        var elements = Object.keys($scope.mineralProductElementNamesAndLibelles);
        for (var i = 0; i < elements.length; i++) {
          var element = elements[i];
          if (angular.isDefined(composition[element])) {
            results.push($scope.mineralProductElementNamesAndLibelles[element] + "(" + composition[element] + "%)");
          }
        }
      }
      var result = results.join(", ");
      return result;
    }

    // Tri : actions principales puis secondaires
    $scope.mainActionSort = function (item) {
      return item.toolsCouplingCode ? -1 : 1;
    };

    $scope.isValidActionForUsage = function (action) {
      var result = action.mainActionInterventionAgrosyst !== 'TRAVAIL_DU_SOL' && action.mainActionInterventionAgrosyst !== 'TRANSPORT';
      return result;
    };

    // Titres
    $scope.getTitleForEditedUsage = function() {
      let title = "";
      const type = $scope.editedUsage.domainSeedLotInputDto.cropSeedDto.type;
      if (type === 'MAIN') {
        title += $scope.messages.commonCulturePrincipale;
      } else if (type === 'INTERMEDIATE') {
        title += $scope.messages.commonCultureIntermediaire;
      } else if (type === 'CATCH') {
        title += $scope.messages.commonCultureDerobee;
      }

      if ($scope.editedUsage.domainSeedLotInputDto.cropSeedDto.mixSpecies) {
        title += " - " + $scope.messages.commonCroppingPlanMixSpecies;
      }
      if ($scope.editedUsage.domainSeedLotInputDto.cropSeedDto.mixVariety) {
        title += " - " + $scope.messages.commonCroppingPlanMixVarieties;
      }

      return title;
    };

    $scope.getTitleForEditedAction = function() {
      let title = "";

      const type = $scope.editedAction.cropType;
      if (type === 'MAIN') {
        title += $scope.messages.commonCulturePrincipale;
      } else if (type === 'INTERMEDIATE') {
        title += $scope.messages.commonCultureIntermediaire;
      } else if (type === 'CATCH') {
        title += $scope.messages.commonCultureDerobee;
      }

      if ($scope.editedAction.isMixSpecies) {
        title += " - " + $scope.messages.commonCroppingPlanMixSpecies;
      }
      if ($scope.editedAction.isMixVariety) {
        title += " - " + $scope.messages.commonCroppingPlanMixVarieties;
      }

      return title;
    }

    // Tri des intrants par ordre alphabétique pour faciliter la sélection dans les listes déroulantes
    $scope.sortInputs = function() {
      $scope.editedUsage.inputs.sort((input1, input2) => input1.inputName.localeCompare(input2.inputName));
      $scope.editedUsage.allInputs = angular.copy($scope.editedUsage.inputs);
    };

    $scope.getProductTypes = function() {
      let productTypes = new Set();
      let newProductTypes = [];
      $scope.editedUsage.allInputs
        .filter(input => input.productType)
        .map(input => productTypes.add(input.productType));
      productTypes.forEach(pt => newProductTypes.push({trad:$scope.productTypes[pt], key:pt}));
      newProductTypes.sort((pt1, pt2) => pt1.trad.localeCompare(pt2.trad));
      $scope.editedUsage.productTypes = newProductTypes;
    };
  }
]);

AgrosystModule.filter('isGroupeCibleInCategoryPhyto', function () {
  return function (groupeCibles, phyto, scope) {
    return groupeCibles.filter((groupeCible) => !phyto.treatmentTarget.category
      || scope.editedUsage && scope.editedUsage.treatmentTargetCategoriesByParent[phyto.treatmentTarget.category].bioAgressorTypes.includes(groupeCible.referenceParam));
  };
});

AgrosystModule.filter('isTargetInGroupeCiblePhyto', function () {
  return function (targets, phyto) {
    return targets.filter((target) => !phyto.treatmentTarget.groupeCibleMaa
      || phyto.treatmentTarget.groupeCibleMaa.cibleEdiRefCodes.includes(target.reference_code || target.identifiant));
  };
});
