<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"  session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property="meta-requested" content='<%=request.getRequestURL().toString() + (request.getQueryString() != null ? ("?"+request.getQueryString()) : "")%>' />
    <meta property="meta-referer" content='<%=request.getHeader("referer")%>' />
    <title>Agrosyst : <decorator:title default="Agrosyst"/></title>
    <link rel="shortcut icon" href="<s:url value='/favicon.ico' />" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst.css' /><s:property value='getVersionSuffix()'/>" />
    <link rel="stylesheet" type="text/css" href="<s:url value='/webjars/font-awesome/4.7.0/css/font-awesome.min.css' />" />
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/layout.css' /><s:property value='getVersionSuffix()'/>" />
    <script type="text/javascript" src="<s:url namespace='/commons' action='endpoints' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript" src="<s:url namespace='/commons' action='i18n' /><s:property value='getVersionSuffix()'/>"></script>
    <!--
      <script type="text/javascript" src="<s:url value='/nuiton-js/agrosyst-ie.js' /><s:property value='getVersionSuffix()'/>"></script>
    -->
    <script type="text/javascript" src="<s:url value='/nuiton-js/agrosyst.js' /><s:property value='getVersionSuffix()'/>"></script>
    <!--<script src="https://code.jquery.com/jquery-migrate-1.4.1.js"></script>--><!-- used to help on migration -->
    <script type="text/javascript">

      function showBroadcastMessagesDialog() {
        $('#broadcastMessagesDialog').dialog({
          width : $(window).width() * 0.5,
          modal: true,
          open: function() {},
          buttons: {
            send: {
              text: "Fermer",
              'class': 'btn-primary',
              click: function() {
                var publishedMessagesURI = "<s:url namespace='/publishedmessage' action='published-messages' />";
                $.post(publishedMessagesURI);
                $(this).dialog("destroy");
              }
            }
          }
        });
      }

      function showNotSaveWarningDialog() {
        $('#notSaveWarningDialog').dialog({
          width : $(window).width() * 0.5,
          modal: true,
          open: function() {},
          buttons: {
            send: {
              text: "Fermer",
              'class': 'btn-primary',
              click: function() {
                $(this).dialog("destroy");
              }
            }
          }
        });
      }

      function goOnDephygraph() {

        var loadDephyGrahURI = "<s:url namespace='/auth' action='load-dephy-graph-token-json' />";
        return $.get(loadDephyGrahURI, function(result, status) {
            if (result && result !== "null" && status == "success" ) {
              window.open(JSON.parse(result));
            } else {
              addPermanentError("Échec de connexion à Dephygraph", status);
          }
        }).fail( function() {
          addPermanentError("Échec de connexion à Dephygraph", status);
        });
      }

      $(document).ready(function()  {
        var isBroadcastMessagesEmpty = <s:property value='broadcastMessagesEmpty' escapeHtml='false' />;
        if (!isBroadcastMessagesEmpty) {
          showBroadcastMessagesDialog();
        }
      });

      $(document).ready(function(){
        hidePageLoading();
        var isValidationError = <s:property value='errors' escapeHtml='false' />;
        if (isValidationError) {
          showNotSaveWarningDialog();
        }
      });
    </script>

    <script type="text/javascript">
      angular.element(document).ready(
        function() {
          addPermanentActionErrors();
        }
      );
    </script>

    <s:if test="!errorNotificationsEmpty">
      <script type="text/javascript">
        angular.element(document).ready(
            function() {
            <s:iterator value="errorNotifications">
              addPermanentError("<s:property value="text" />");
            </s:iterator>
          }
        );
      </script>
    </s:if>

    <s:if test="!warningNotificationsEmpty">
      <script type="text/javascript">
        angular.element(document).ready(
            function() {
            <s:iterator value="warningNotifications">
              addPermanentWarning("<s:property value="text" />");
            </s:iterator>
          }
        );
      </script>
    </s:if>

    <s:if test="!infoNotificationsEmpty">
      <script type="text/javascript">
        angular.element(document).ready(
            function() {
            <s:iterator value="infoNotifications">
              addSuccessMessage("<s:property value="text" />");
            </s:iterator>
          }
        );
      </script>
    </s:if>

    <decorator:head/>
  </head>
  <body>

    <s:if test="config.instanceBannerText != null">
      <div class="banner<s:if test="config.instanceBannerStyle != null"> banner${config.instanceBannerStyle}</s:if> ">
        <a href="https://forge.codelutin.com/projects/agrosyst">${config.instanceBannerText}</a>
      </div>
    </s:if>

    <div id="wrap-global">
      <div id="wrap-main">
        <div class="agrosyst-header noprint" style="background-image: url('<s:url value='/' />${requestScope.layoutData.currentUserBannerPath}');">
          <p class="photo-copyright">${requestScope.layoutData.currentUserBannerMeta}</p>
          <a href="<s:url namespace='/' action='index' />"><img id="logo-ecophyto-dephy" src="<s:url value='/img/logo-Ecophyto-Dephy-Header.png' />" alt="logo-ecophyto-dephy"/></a>

          <ul class="header-menu <decorator:getProperty property='page.current-category'/>">
            <li class="contextual"><a href="<s:url namespace='/contextual' action='domain-presentation' />"><span>Contextuel et Organisationnel</span></a>
              <ul>
                <li><a href="<s:url namespace='/domains' action='domains-list' />">Exploitation ou Domaine expérimental</a></li>
                <li><a href="<s:url namespace='/growingplans' action='growing-plans-list' />">Dispositif</a></li>
                <li><a href="<s:url namespace='/networks' action='networks-list' />">Réseau</a></li>
              </ul>

            </li>
            <li class="management"><a href="<s:url namespace='/managementmodes' action='index' />"><span>Système de culture / décisionnel</span></a>
              <ul>
                <li><a href="<s:url namespace='/growingsystems' action='growing-systems-list' />">Système de culture</a></li>
                <li><a href="<s:url namespace='/managementmodes' action='management-modes-list' />">Modèle décisionnel</a></li>
                <li><a href="<s:url namespace='/managementmodes' action='decision-rules-list' />">Règles de décision</a></li>
              </ul>
            </li>
            <li class="reports"><a href="<s:url namespace='/reports' action='index' />"><span>Bilan de campagne</span></a>
              <ul>
                <li><a href="<s:url namespace='/reports' action='report-regionals-list' />">Bilan de campagne / échelle régionale</a></li>
                <li><a href="<s:url namespace='/reports' action='report-growing-systems-list' />">Bilan de campagne / échelle système de culture</a></li>
              </ul>
            </li>
            <li class="effective"><a href="<s:url namespace='/effective' action='index' />"><span>Actes réalisés</span></a>
              <ul>
                <li><a href="<s:url namespace='/effective' action='effective-crop-cycles-list' />">Interventions culturales</a></li>
                <li><a href="<s:url namespace='/effective' action='effective-crop-cycles-list' />?measurement=true">Mesures et observations</a></li>
              </ul>
            </li>
            <li class="practiced"><a href="<s:url namespace='/practiced' action='index' />"><span>Actes synthétisés</span></a>
              <ul>
                <li><a href="<s:url namespace='/practiced' action='practiced-systems-list' />">Système synthétisé</a></li>
                <li><a href="<s:url namespace='/practiced' action='practiced-plots-list' />">Parcelle type</a></li>
              </ul>
            </li>
            <li class="performances"><a href="<s:url namespace='/performances' action='index' />"><span>Performances</span></a>
              <ul>
                <li><a href="<s:url namespace='/performances' action='performances-list' />">Réalisé</a></li>
                <li><a href="<s:url namespace='/performances' action='performances-list' />?practiced=true">Synthétisé</a></li>
                <s:if test="config.isDephygraphConnexionEnabled"><li><a><span onclick="goOnDephygraph()">Aller sur DEPHYgraph</span></a></li></s:if>
              </ul>
            </li>
          </ul>

          <ul class="header-userinfo">
            <li>
              <a class="userinfo-username" href="<s:url namespace='/user' action='user-account-input' />" title="Votre profil utilisateur">
                <span>${requestScope.layoutData.currentUserFirstName}&nbsp;${requestScope.layoutData.currentUserLastName}</span>
              </a>
            </li>
            <li>
              <a class="userinfo-message" title="Messages"><span onclick="displayAllPublishedMessages()">Messages</span></a>
            </li>
            <li class="with-submenu"><span class="userinfo-dephy"><span>DEPHY</span></span>
              <ul>
                <li><a href="https://www.youtube.com/watch?v=oV6sYI9D8bo&list=PLExrLoicKyXERYrHfApZL9M4gPI5hmT7Z&index=2" title="Tutos Infos' de DEPHY" target="_blank" rel="noopener noreferrer">Tutos Infos de DEPHY (Youtube)</a></li>
                <li><a href="<s:url namespace='/commons' action='help-raw' />?file=<s:url value='/help/consignes_de_saisie.pdf' />" title="Consignes de saisie" target="_blank" rel="noopener noreferrer">Consignes de saisie</a></li>
                <li><a href="https://www.opera-collaboratif.chambres-agriculture.fr/share/page/site/N75NDephycan/documentlibrary#filter=path%7C%2F08_AGROSYST%2FLettre_d%2527info%7C&page=1" title="Lettres d'info \"Saisir et valoriser les données DEPHY\"" target="_blank" rel="noopener noreferrer">Lettres d'info "Saisir et valoriser les données DEPHY"</a></li>
              </ul>
            </li>
            <li class="with-submenu"><span class="userinfo-help"><span>Aide</span></span>
             <ul>
              <li><a href="<s:url namespace='/commons' action='help-raw' />?file=<s:url value='/help/guide_utilisateur.pdf' />" title="Guide utilisateur" target="_blank" rel="noopener noreferrer">Guide utilisateur</a></li>
              <li><a href="<s:url namespace='/commons' action='help-raw' />?file=<s:url value='/help/guide_indicateurs.pdf' />" title="Guide des indicateurs" target="_blank" rel="noopener noreferrer">Guide des indicateurs</a></li>
              <li><a href="<s:url namespace='/commons' action='help-raw' />?file=<s:url value='/help/guide_local_intrants.pdf' />" title="Guide Local à intrants" target="_blank" rel="noopener noreferrer">Guide Local à intrants</a></li>
              <li><a href="<s:url value='/help/convertisseur_densite_semis.xlsx' />" title="Convertisseur densité semis" target="_blank" rel="noopener noreferrer">Convertisseur densité semis</a></li>
             </ul>
            </li>
            <s:if test="layoutData && (layoutData.currentUserAnAdmin || layoutData.currentUserAnIsDataProcessor)">
              <li>
                <a class="userinfo-admin" href="<s:url namespace='/admin' action='home' />" title="Administration"><span>Administration</span></a>
              </li>
            </s:if>
            <li>
              <a class="userinfo-connect" href="<s:url namespace='/auth' action='logout' />" title="Déconnexion"><span>Déconnexion</span></a>
            </li>
          </ul>
        </div>

        <div class="agrosyst-body">
          <div id="agrosyst-content">
            <decorator:body/>
          </div>
        </div>
      </div>
    </div>

    <div class="noprint">
      <div class="agrosyst-footer">
        <a href="http://agriculture.gouv.fr/ecophyto" class="external-link" target="_blank" rel="noopener noreferrer"><img class="logo" src="<s:url value='/img/logo-Ecophyto.png' />" alt="Ecophyto"/></a>
        <a href="http://www.inrae.fr" class="external-link" target="_blank" rel="noopener noreferrer"><img class="logo" src="<s:url value='/img/logo-Inrae.png' />" alt="Inra" title="Voir le site de l'INRAE"/></a>
        <a href="https://www.ofb.gouv.fr" class="external-link" target="_blank" rel="noopener noreferrer"><img class="logo" src="<s:url value='/img/logo-OFB.png' />" alt="Office Français de la Biodiversité" title="Voir le site de l'Office Français de la Biodiversité"/></a>
        <a href="http://agriculture.gouv.fr" class="external-link" target="_blank" rel="noopener noreferrer"><img class="logo" src="<s:url value='/img/Logo_Ministere_Agri_Alim.png' />" alt="Maaf" title="Voir le site de du ministère"/></a>
        <a href="https://www.ecologique-solidaire.gouv.fr/" class="external-link" target="_blank" rel="noopener noreferrer"><img class="logo" src="<s:url value='/img/Logo_Ministere_Ecologique.png' />" alt="Maaf" title="Voir le site de du ministère de la transition écologique et solidaire"/></a>
        <ul class="infos">
          <li><a href="http://forge.codelutin.com/projects/agrosyst">Agrosyst</a></li>
          <li><a href="http://forge.codelutin.com/projects/agrosyst/roadmap?completed=1">
              ${config.applicationVersion}</a></li>
          <li><a href="mailto:${config.contactEmail}">Contact</a></li>
          <li><a href="<s:url namespace='/auth' action='legal' />">Mentions légales</a></li>
          <li>Copyright 2013 - 2021 <a href="http://www.inrae.fr">INRAE</a></li>
          <li>Réalisé par <a href="http://www.codelutin.com">Code Lutin</a></li>
        </ul>
      </div>
    </div>

    <div class="noprint">
      <div id="context-panel">
        <ul>
          <li class="icon-campaign">
            <span class="label">Campagne(s)</span> <s:if test="layoutData.campaignsCount > 0"><span class="count">${layoutData.campaignsCount}</span></s:if>
            <s:if test="layoutData.campaignsCount > 0">
              <ul>
                <li class="title"></li>
                <s:iterator value="layoutData.campaigns">
                  <li><!-- <s:property value="key"/> --><s:property value="value"/></li>
                </s:iterator>
              </ul>
            </s:if>
          </li>
          <li class="icon-networkContext">
            <span class="label">Réseau(x)</span> <s:if test="layoutData.networksCount > 0"><span class="count">${layoutData.networksCount}</span></s:if>
            <s:if test="layoutData.networksCount > 0">
              <ul>
                <li class="title"></li>
                <s:iterator value="layoutData.networks">
                  <li><!-- <s:property value="key"/> --><s:property value="value"/></li>
                </s:iterator>
              </ul>
            </s:if>
          </li>
          <li class="icon-domain">
            <span class="label">Exploitation(s) ou domaine(s) expérimental(ux)</span> <s:if test="layoutData.domainsCount > 0"><span class="count">${layoutData.domainsCount}</span></s:if>
            <s:if test="layoutData.domainsCount > 0">
              <ul>
                <li class="title"></li>
                <s:iterator value="layoutData.domains">
                  <li><!-- <s:property value="key"/> --><s:property value="value"/></li>
                </s:iterator>
              </ul>
            </s:if>
          </li>
          <li class="icon-growingPlan">
            <span class="label">Dispositif(s)</span> <s:if test="layoutData.growingPlansCount > 0"><span class="count">${layoutData.growingPlansCount}</span></s:if>
            <s:if test="layoutData.growingPlansCount > 0">
              <ul>
                <li class="title"></li>
                <s:iterator value="layoutData.growingPlans">
                  <li><!-- <s:property value="key"/> --><s:property value="value"/></li>
                </s:iterator>
              </ul>
            </s:if>
          </li>
          <li class="icon-growingSystem">
            <span class="label">Système(s) de culture</span> <s:if test="layoutData.growingSystemsCount > 0"><span class="count">${layoutData.growingSystemsCount}</span></s:if>
             <s:if test="layoutData.growingSystemsCount > 0">
              <ul>
                <li class="title"></li>
                <s:iterator value="layoutData.growingSystems">
                  <li><!-- <s:property value="key"/> --><s:property value="value"/></li>
                </s:iterator>
              </ul>
            </s:if>
          </li>
        </ul>

        <a onclick="showFeedBackDialog({ send: '<s:text name="feedback-send" />', cancel: '<s:text name="common-cancel" />', noSector: '<s:text name="feedback-errors-noSector" />', noCategory: '<s:text name="feedback-errors-noCategory" />' })"
           class="btn-primary btn-feedback"
           title="Si vous voyez un bug ou si vous avez simplement une remarque à faire, remplissez ce formulaire pour nous faire parvenir votre avis. N'oubliez pas de nous indiquer sur quelles données vous travaillez (campagne, nom du domaine, etc.).">
          <span class="feedback-icon">!?</span>
          Un bug ? Une remarque ?
        </a>
      </div>
    </div>

    <div id="broadcastMessagesDialog" title="Messages" class="auto-hide">
      <jsp:include page="../content/publishedmessage/published-messages-list-raw.jsp" />
    </div>

    <div id="notSaveWarningDialog" title="ÉCHEC D'ENREGISTREMENT" class="auto-hide">
      <div>Vos données n'ont pu être enregistrées car certaines ne sont pas valides :</div>
      <div id="displayAllErrorMessagesDialog">
          <s:actionerror cssClass="errorMessageTable" escape="false"/>
          <s:fielderror cssClass="errorMessageTable" escape="false"/>
      </div>
    </div>

    <div id="context-selector" title="Contexte de navigation" class="auto-hide">
      Chargement...
    </div>

    <div class="fixed-toasts toasts">
      <div class="toast-group">
      </div>
    </div>

    <div id="feedBackDialog" title="Un bug ? Une remarque ?" class="auto-hide">
      <form id="feedBackForm" class="nomargin">
        <input type="hidden" name="appVersion" value="${config.applicationVersion}"/>

        <div class="help-explanation-panel nomargin">
          <strong>Si vous voyez un bug ou si vous avez simplement une remarque à faire, remplissez ce formulaire pour nous faire parvenir votre avis.
                  N'oubliez pas de nous indiquer sur quelles données vous travaillez (campagne, nom du domaine, etc.).</strong>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="dephyType">
              <span class="required">*</span>&nbsp;Dispositifs&nbsp;:
            </label>
          </span>
          <span class="wwctrl feedbackCategory">

            <s:iterator value="layoutData.dephyTypes">
              <input type="radio" onchange="showForItOnly()" name="dephyType" value="<s:property value='key' />" id="<s:property value='key' />"/><label for="<s:property value='key' />"><s:property value="value" /></label>
            </s:iterator>

          </span>
        </div>

        <div id="isNotNetworkIrBlock">
          <div class="wwgrp">
            <span class="wwlbl">
              <label for="isNotNetworkIr" >
                Je ne suis pas un Ingénieur Réseau&nbsp;:
              </label>
            </span>
            <span class="wwctrl feedbackCategory">
              <input type="checkbox" name="isNotNetworkIr" id="isNotNetworkIr" onchange="checkRequiredEmail()"/>
            </span>
          </div>
        </div>


        <div id="itEmailBlock" style="">
          <div id="wwgrp_layoutData_itEmail" class="wwgrp">
            <span id="wwlbl_layoutData_itEmail" class="wwlbl">
            <label name="label_for_itEmail" for="layoutData_itEmail" class="label"><it-email><span id='email_required' class='required''>*</span> Adresse mail de votre IT&nbsp:</it-email></label></span>
            <span id="wwctrl_layoutData_itEmail" class="wwctrl">
              <input type="email" name="layoutData.itEmail" value="${requestScope.layoutData.currentUserItEmail}" id="layoutData_itEmail" title="Vous pouvez configurer cette adresse sur la page de votre profil utilisateur." onchange="valideItEmail()" placeholder="ex. : prenom.nom@inrae.fr" required="true">
            </span>
          </div>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="sectorType" >
              <span class="required">*</span>&nbsp;Filière(s)&nbsp;:
            </label>
          </span>
          <span class="wwctrl feedbackCategory">

            <span>
              <s:subset start="0" count = "3" source = "layoutData.agrosystSectorTypes">
                 <s:iterator>
                   <input type="checkbox" name="sectorTypes_${key}" value="<s:property value='key' />" onchange="filterCategoriesIfNeeded()" id="<s:property value='key' />"/>
                   <label class="feedbackCategory-checkbox-label" for="<s:property value='key' />"><s:property value="value" /></label>
                 </s:iterator>
              </s:subset>
            </span>
            <div></div>
            <span>
              <s:subset start="3" count = "3" source = "layoutData.agrosystSectorTypes">
                 <s:iterator>
                   <input type="checkbox" name="sectorTypes_${key}" value="<s:property value='key' />" onchange="filterCategoriesIfNeeded()" id="<s:property value='key' />"/>
                   <label class="feedbackCategory-checkbox-label" for="<s:property value='key' />"><s:property value="value" /></label>
                 </s:iterator>
              </s:subset>
            </span>
            <div></div>
            <span>
              <s:subset start="6" count = "1" source = "layoutData.agrosystSectorTypes">
                 <s:iterator>
                   <input type="checkbox" name="sectorTypes_${key}" value="<s:property value='key' />" onchange="filterCategoriesIfNeeded()" id="<s:property value='key' />"/>
                   <label class="feedbackCategory-checkbox-label" for="<s:property value='key' />"><s:property value="value" /></label>
                 </s:iterator>
              </s:subset>
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="feedbackCategory" >
              <span class="required">*</span>&nbsp;Catégorie&nbsp;:
            </label>
          </span>
          <span class="wwctrl feedbackCategory" id="feedbackCategory">

            <select id="feedbackCategory" name="feedbackCategory">
              <option value="" />
              <s:iterator value="layoutData.agrosystFeedbackCategories">
                <option id="${feedbackCategory}" value='${feedbackCategory}' title="${help}" >${trad}</option>
              </s:iterator>
            </select>

          </span>
        </div>

        <label for="feedback" class="alignCenter">
          Merci de détailler ci-dessous votre rapport de bug ou votre remarque :
        </label>
        <textarea id="feedback" name="feedback" cols="72" rows="8" class="full-width"
                  placeholder="Bonjour,&#10;Je voudrais vous signaler un bug / une remarque / un manque dans un référentiel.&#10;Merci">
        </textarea>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="feedbackScreenshot">
              Copie d'écran :
            </label>
          </span>
          <span class="wwctrl feedbackCategory">
            <input id="feedbackScreenshot" type="checkbox" name="feedbackScreenshot" value="1" />
            <label for="feedbackScreenshot" class="checkboxLabel">Inclure une copie d'écran de la page actuellement affichée</label>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="feedbackAttachment">
              Pièce jointe :
            </label>
          </span>
          <span class="wwctrl">
            <input id="feedbackAttachment" type="file" name="feedbackAttachment" />
          </span>
        </div>

      </form>
    </div>

    <div id="feedBackDialogReturn" title="Récépissé de feedback" class="auto-hide">
      <div id='feedbackResultFailed' style="display: none;">
        <strong>Impossible d'enregistrer votre retour utilisateur, merci de contacter un administrateur d'Agrosyst.</strong>
      </div>
      <div id='feedbackResultSuccess' style="display: none;">
        <strong>Votre retour utilisateur a bien été enregistré et envoyé aux adresses suivantes :</strong>
        <ul id="recipientList"></ul>
        <strong>Merci !</strong>
      </div>
      <div id="feedbackResultNotSend" style="display: none;">
        <strong>Votre retour utilisateur n'a pas été envoyé faute de destinataire trouvé correspondant aux choix sélectionnés.</strong>
      </div>
      <div id='feedbackResultError' style="display: none;">
        <strong>Impossible d'enregistrer votre retour utilisateur, merci de contacter un administrateur d'Agrosyst.</strong>
      </div>
    </div>

    <div id="wrongEmail" title="Email invalide" class="auto-hide">
      <div>Désolé, l'adresse email de votre IT n'est pas une adresse mail reconnue d'Agrosyst.<br>L'adresse email doit être valide et correspondre à l'adresse email d'un utilisateur Agrosyst.</div>
    </div>

    <div id="pageLoadingParent">
      <div id="pageLoading"></div>
    </div>

  </body>
</html>
