<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <title>Agrosyst : <decorator:title default="Agrosyst"/></title>
     <link rel="shortcut icon" href="<s:url value='/favicon.ico' />" type="image/x-icon" />
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/layout.css' /><s:property value='getVersionSuffix()'/>" />
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-login.css' /><s:property value='getVersionSuffix()'/>" />
     <script type="text/javascript" src="<s:url namespace='/commons' action='endpoints' /><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript" src="<s:url value='/nuiton-js/agrosyst.js' /><s:property value='getVersionSuffix()'/>"></script>
     <decorator:head/>
  </head>
  <body class="login-page">

    <s:if test="config.instanceBannerText != null">
      <div class="banner<s:if test="config.instanceBannerStyle != null"> banner${config.instanceBannerStyle}</s:if>">
        <a href="https://forge.codelutin.com/projects/agrosyst">${config.instanceBannerText}</a>
      </div>
    </s:if>

    <decorator:body/>

    <div class="agrosyst-footer">
      <ul class="infos">
        <li><a href="http://forge.codelutin.com/projects/agrosyst">Agrosyst</a></li>
        <li><a href="http://forge.codelutin.com/projects/agrosyst/roadmap?completed=1">
          ${config.applicationVersion}</a></li>
        <li><a href="mailto:${config.contactEmail}">Contact</a></li>
        <li><a href="<s:url namespace='/auth' action='legal' />">Mentions légales</a></li>
        <li>Copyright 2013 - 2021 <a href="http://www.inrae.fr">INRAE</a></li>
        <li>Réalisé par <a href="http://www.codelutin.com">Code Lutin</a></li>
      </ul>
    </div>

    <div class="messages-panel">
      <s:if test="!infoNotificationsEmpty">
        <ul class="successMessage">
          <s:iterator value="infoNotifications">
            <li>
              <s:property value="text" />
              <span class="close-button" title="Fermer" onclick="$(this).parent().stop().fadeOut();">
                Fermer
              </span>
            </li>
          </s:iterator>
        </ul>
      </s:if>

      <s:if test="!warningNotificationsEmpty">
        <ul class="warningMessage">
          <s:iterator value="warningNotifications">
            <li>
              <s:property value="text" />
              <span class="close-button" title="Fermer" onclick="$(this).parent().stop().fadeOut();">
                Fermer
              </span>
            </li>
          </s:iterator>
        </ul>
      </s:if>

      <s:if test="!errorNotificationsEmpty">
        <ul class="errorMessage">
          <s:iterator value="errorNotifications">
            <li>
              <s:property value="text" />
              <span class="close-button" title="Fermer" onclick="$(this).parent().stop().fadeOut();">
                Fermer
              </span>
            </li>
          </s:iterator>
        </ul>
      </s:if>
    </div>

  </body>
</html>
