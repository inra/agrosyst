<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2023 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true"  session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property="meta-requested"
      content='<%=request.getRequestURL().toString() + (request.getQueryString() != null ? ("?"+request.getQueryString()) : "")%>' />
    <meta property="meta-referer" content='<%=request.getHeader("referer")%>' />
    <title>IPMworks : <decorator:title default="IPMworks" /></title>
    <link rel="shortcut icon" href="<s:url value='/ipmworks/favicon.ico' />" type="image/x-icon" />
    <link rel="stylesheet" type="text/css"
      href="<s:url value='/nuiton-js/ipmworks.css' /><s:property value='getVersionSuffix()'/>" />
    <link rel="stylesheet" type="text/css"
      href="<s:url value='/webjars/font-awesome/4.7.0/css/font-awesome.min.css' />" />
    <link rel="stylesheet" type="text/css"
      href="<s:url value='/nuiton-js/ipmworks-layout.css' /><s:property value='getVersionSuffix()'/>" />
    <script type="text/javascript"
      src="<s:url namespace='/commons' action='endpoints' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript"
      src="<s:url namespace='/commons' action='i18n' /><s:property value='getVersionSuffix()'/>&l=<s:property value='getLocale()'/>"></script>
    <script type="text/javascript"
      src="<s:url value='/nuiton-js/ipmworks.js' /><s:property value='getVersionSuffix()'/>"></script>
    <!--<script src="https://code.jquery.com/jquery-migrate-1.4.1.js"></script>-->
    <!-- used to help on migration -->
    <script type="text/javascript">

      function showBroadcastMessagesDialog() {
        $('#broadcastMessagesDialog').dialog({
          width: $(window).width() * 0.5,
          modal: true,
          open: function () { },
          buttons: {
            send: {
              text: "Fermer",
              'class': 'btn-primary',
              click: function () {
                var publishedMessagesURI = "<s:url namespace='/publishedmessage' action='published-messages' />";
                $.post(publishedMessagesURI);
                $(this).dialog("destroy");
              }
            }
          }
        });
      }

      function showNotSaveWarningDialog() {
        $('#notSaveWarningDialog').dialog({
          width: $(window).width() * 0.5,
          modal: true,
          open: function () { },
          buttons: {
            send: {
              text: "Fermer",
              'class': 'btn-primary',
              click: function () {
                $(this).dialog("destroy");
              }
            }
          }
        });
      }

      $(function () {
        var isBroadcastMessagesEmpty = <s:property value='broadcastMessagesEmpty' escapeHtml='false' />;
        if (!isBroadcastMessagesEmpty) {
          showBroadcastMessagesDialog();
        }
      });

      $(document).ready(function () {
        hidePageLoading();
        var isValidationError = <s:property value='errors' escapeHtml='false' />;
        if (isValidationError) {
          showNotSaveWarningDialog();
        }
      });
    </script>

    <script type="text/javascript">
      angular.element(document).ready(
        function () {
          addPermanentActionErrors();
        }
      );
    </script>

    <s:if test="!errorNotificationsEmpty">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            <s:iterator value="errorNotifications">
              addPermanentError("<s:property value='text' />");
            </s:iterator>
          }
        );
      </script>
    </s:if>

    <s:if test="!warningNotificationsEmpty">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            <s:iterator value="warningNotifications">
              addPermanentWarning("<s:property value='text' />");
            </s:iterator>
          }
        );
      </script>
    </s:if>

    <s:if test="!infoNotificationsEmpty">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            <s:iterator value="infoNotifications">
              addSuccessMessage("<s:property value='text' />");
            </s:iterator>
          }
        );
      </script>
    </s:if>

    <decorator:head />
  </head>

  <body>

    <s:if test="config.instanceBannerText != null">
      <div class="banner<s:if test='config.instanceBannerStyle !=null'> banner${config.instanceBannerStyle}</s:if>">
        <a href="https://forge.codelutin.com/projects/agrosyst">${config.instanceBannerText}</a>
      </div>
    </s:if>

    <div id="wrap-global">
      <div id="wrap-main">
        <div class="ipmworks-header noprint">
          <div id="title">
            <a href="<s:url namespace='/ipmworks' action='index' />">
              <img id="logo-ipmworks" src="<s:url value='/img/logo-ipmworks.png' />" alt="logo-ipmworks" />
            </a>
            <span class="bigger">
              <s:text name="layout-header-title" />
              <img id="logo-agrosyst" src="<s:url value='/img/Logo_AGROSYST_small.png' />" alt="logo-agrosyst" style="margin: 0 0 -6px 10px" />
            </span>,
            <s:text name="layout-header-subtitle" />
          </div>
          <ul class="header-userinfo">
            <li>
              <a class="userinfo-username"
                 href="<s:url namespace='/ipmworks/user' action='user-account-input' />"
                 title="<s:text name='layout-header-useraccount' />">
                <span>${requestScope.layoutData.currentUserFirstName}&nbsp;${requestScope.layoutData.currentUserLastName}</span>
              </a>
            </li>
            <li>
              <a class="userinfo-message" title="<s:text name='layout-header-messages' />">
                <span onclick="displayAllPublishedMessages()"><s:text name='layout-header-messages' /></span>
              </a>
            </li>
            <li class="with-submenu">
              <span class="userinfo-help"><span><s:text name='layout-header-help' /></span></span>
              <ul>
                <li><a
                    href="<s:url namespace='/commons' action='help-raw' />?file=<s:url value='/help/ipmworks_guideline_eng.pdf' />"
                    title="<s:text name='layout-header-userguide' />" target="_blank" rel="noopener noreferrer"><s:text name='layout-header-userguide' /></a></li>
                <li><a
                    href="<s:url namespace='/commons' action='help-raw' />?file=<s:url value='/help/ipmworks_guide_indicateurs_eng.pdf' />"
                    title="<s:text name='layout-header-indicatorguide' />" target="_blank" rel="noopener noreferrer"><s:text name='layout-header-indicatorguide' /></a></li>
                <li><a
                    href="<s:url namespace='/commons' action='help-raw' />?file=<s:url value='/help/ipmworks_survey2_interview guide_eng.pdf' />"
                    title="<s:text name='layout-header-interviewguide' />" target="_blank" rel="noopener noreferrer"><s:text name='layout-header-interviewguide' /></a></li>
              </ul>
            </li>
            <li>
              <a class="userinfo-connect" href="<s:url namespace='/ipmworks/auth' action='logout' />"
                title="<s:text name='layout-header-logout' />"><span><s:text name='layout-header-logout' /></span></a>
            </li>
          </ul>
        </div>

        <div class="ipmworks-body">
          <div id="ipmworks-content">
            <decorator:body />
          </div>
        </div>
      </div>
    </div>

    <div class="noprint">
      <footer class="ipmworks-footer">
        <div class="politics_logos">
          <div id="eu-approved">
            <img alt="eu flag" class="flag" src="<s:url value='/img/eu-flag.png' />" />
            <span><s:text name='layout-footer-europe' /></span>
          </div>
          <a href="http://agriculture.gouv.fr" class="logo" target="_blank" rel="noopener noreferrer">
            <img src="<s:url value='/img/Logo_Ministere_Agri_Alim_h.png' />" alt="Maa" title="Ministère de l'agriculture et de l'alimentation" />
          </a>
          <a href="https://www.ecologique-solidaire.gouv.fr/" class="logo" target="_blank" rel="noopener noreferrer">
            <img src="<s:url value='/img/Logo_Ministere_Ecologique_h.png' />" alt="Mtes" title="Ministère de la transition écologique et solidaire" />
          </a>
        </div>

        <div class="menu">
          <div class="menu-item"><a href="https://ipmworks.net" target="_blank">IPMworks</a></div>
          <div class="menu-item">${config.applicationVersion}</div>
          <div class="menu-item"><a href="mailto:${config.contactEmail}"><s:text name='layout-footer-contact' /></a></div>
          <div class="menu-item"><a href="<s:url namespace='/auth' action='legal' />"><s:text name='layout-footer-legal' /></a></div>
          <div class="menu-item">Copyright 2022 <a href="https://inrae.fr" target="_blank">INRAE</a></div>
          <div class="menu-item"><s:text name='layout-footer-madeby' />&nbsp;<a href="https://codelutin.com">Code Lutin</a></div>
        </div>

        <div id="feedback">
          <img src="<s:url value='/img/Logo_AGROSYST_small.png' />" alt="logo-agrosyst" style="height: 30px; margin-right: 60px;" />
          <a onclick="showFeedBackDialog({ send: '<s:text name="feedback-send" />', cancel: '<s:text name="common-cancel" />', noSector: '<s:text name="feedback-errors-noSector" />', noCategory: '<s:text name="feedback-errors-noCategory" />' })"
             class="btn-primary btn-feedback"
             title="<s:text name='feedback-help' />">
            <span class="feedback-icon">!?</span>
            <s:text name='feedback-title' />
          </a>
        </div>

      </footer>
    </div>

    <div id="broadcastMessagesDialog" title="Messages" class="auto-hide">
      <jsp:include page="../content/publishedmessage/published-messages-list-raw.jsp" />
    </div>

    <div id="notSaveWarningDialog" title="<s:text name='layout-notSaveWarningDialog-title' />" class="auto-hide">
      <div><s:text name='layout-notSaveWarningDialog-message' /></div>
      <div id="displayAllErrorMessagesDialog">
        <s:actionerror cssClass="errorMessageTable" escape="false" />
      </div>
    </div>

    <div class="fixed-toasts toasts">
      <div class="toast-group">
      </div>
    </div>

    <div id="feedBackDialog" title="<s:text name='feedback-title' />" class="auto-hide">
      <form id="feedBackForm" class="nomargin">
        <input type="hidden" name="appVersion" value="${config.applicationVersion}" />
        <input type="hidden" name="dephyType" value="IPMWORKS" />

        <div class="help-explanation-panel nomargin">
          <strong>
            <s:text name='feedback-help' />
          </strong>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="sectorType">
              <span class="required">*</span>&nbsp;<s:text name='feedback-sector' />:
            </label>
          </span>
          <span class="wwctrl feedbackCategory">
            <span>
              <s:subset start="0" count="3" source="layoutData.ipmWorksSectorTypes">
                <s:iterator>
                  <input type="checkbox" name="sectorTypes_${key}" value="<s:property value='key' />"
                    id="<s:property value='key' />" /><label for="<s:property value='key' />">
                    <s:property value="value" />
                  </label>
                </s:iterator>
              </s:subset>
            </span>
            <div></div>
            <span>
              <s:subset start="3" count="layoutData.ipmWorksSectorTypes.size()-3" source="layoutData.ipmWorksSectorTypes">
                <s:iterator>
                  <input type="checkbox" name="sectorTypes_${key}" value="<s:property value='key' />"
                    id="<s:property value='key' />" /><label for="<s:property value='key' />">
                    <s:property value="value" />
                  </label>
                </s:iterator>
              </s:subset>
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="feedbackCategory">
              <span class="required">*</span>&nbsp;<s:text name='feedback-category' />:
            </label>
          </span>
          <span class="wwctrl feedbackCategory" id="feedbackCategory">

            <select id="feedbackCategory" name="feedbackCategory">
              <option value="" />
              <s:iterator value="layoutData.ipmWorksFeedbackCategories">
                <option id="${feedbackCategory}" value='${feedbackCategory}' title="${help}">${trad}</option>
              </s:iterator>
            </select>

          </span>
        </div>

        <label for="feedback" class="alignCenter">
          <s:text name='feedback-detail' />:
        </label>
        <textarea id="feedback" name="feedback" cols="72" rows="8" class="full-width"
          placeholder="<s:text name='feedback-detail-placeholder' />">
        </textarea>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="feedbackScreenshot">
              <s:text name='feedback-screenshot' />:
            </label>
          </span>
          <span class="wwctrl feedbackCategory">
            <input id="feedbackScreenshot" type="checkbox" name="feedbackScreenshot" value="1" />
            <label for="feedbackScreenshot" class="checkboxLabel"><s:text name='feedback-screenshot-help' /></label>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="feedbackAttachment">
              <s:text name='feedback-attachment' />:
            </label>
          </span>
          <span class="wwctrl">
            <input id="feedbackAttachment" type="file" name="feedbackAttachment" />
          </span>
        </div>

      </form>
    </div>

    <div id="feedBackDialogReturn" title="<s:text name='feedback-receipt' />" class="auto-hide">
      <div id='feedbackResultFailed' style="display: none;">
        <strong><s:text name='feedback-result-failed' /></strong>
      </div>
      <div id='feedbackResultSuccess' style="display: none;">
        <strong><s:text name='feedback-result-success-message' /></strong>
        <ul id="recipientList"></ul>
        <strong><s:text name='feedback-result-success-thanks' /></strong>
      </div>
      <div id="feedbackResultNotSend" style="display: none;">
        <strong><s:text name='feedback-result-notSend' /></strong>
      </div>
      <div id='feedbackResultError' style="display: none;">
        <strong><s:text name='feedback-result-error' /></strong>
      </div>
    </div>

    <div id="pageLoadingParent">
      <div id="pageLoading"></div>
    </div>

  </body>

</html>
