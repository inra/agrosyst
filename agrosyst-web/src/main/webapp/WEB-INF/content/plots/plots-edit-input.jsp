<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <%-- Out of wro because wro can't handle it --%>
    <link rel="stylesheet" href="<s:url value='/webjars/leaflet/1.7.1/dist/leaflet.css' />" />

    <script src="<s:url value='/webjars/leaflet/1.7.1/dist/leaflet.js' />"></script>
    <script src="<s:url value='/js/map/leaflet-providers.js' />"></script>
    <script type="text/javascript" src="<s:url value='/nuiton-js/plot.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">

      angular.module('PlotEditModule', ['Agrosyst'])
      .value('plotInitData', {
        Plot: <s:property value="toJson(plot)" escapeHtml="false"/>,
        Grounds: <s:property value="toJson(grounds)" escapeHtml="false"/>,
        ParcelleZonages: <s:property value="toJson(parcelleZonages)" escapeHtml="false"/>,
        SelectedPlotZoningIds: <s:property value="toJson(selectedPlotZoningIds)" escapeHtml="false"/>,
        SelectedSolId: <s:property value="toJson(SelectedSolId)" escapeHtml="false"/>,
        SelectedSurfaceTextureId: <s:property value="toJson(selectedSurfaceTextureId)" escapeHtml="false"/>,
        SelectedSubSoilTextureId: <s:property value="toJson(selectedSubSoilTextureId)" escapeHtml="false"/>,
        SelectedSolProfondeurId: <s:property value="toJson(selectedSolProfondeurId)" escapeHtml="false"/>,
        SolTextures: <s:property value="toJson(solTextures)" escapeHtml="false"/>,
        SolProfondeurs: <s:property value="toJson(solProfondeurs)" escapeHtml="false"/>,
        SolCaracteristiques: <s:property value="toJson(solCaracteristiques)" escapeHtml="false"/>,
        SolHorizons: <s:property value="toJson(solHorizons)" escapeHtml="false"/>,
        Zones: <s:property value="toJson(zones)" escapeHtml="false"/>,
        zonesUsageList: <s:property value="toJson(zonesUsageList)" escapeHtml="false"/>,
        ZoneTypes: <s:property value="toJson(zoneTypes)" escapeHtml="false"/>,
        AdjacentElements: <s:property value="toJson(adjacentElements)" escapeHtml="false"/>,
        AdjacentElementIds: <s:property value="toJson(adjacentElementIds)" escapeHtml="false"/>,
        campaignsBounds: <s:property value="toJson(campaignsBounds)" escapeHtml="false" />,
        growingSystemTopiaId: <s:property value="toJson(growingSystemTopiaId)" escapeHtml="false" />
      });
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("La parcelle sur laquelle vous travaillez est inactive et/ou liée à un domaine inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

    <s:if test="plot.topiaId == null">
      <title>Nouvelle parcelle</title>
    </s:if>
    <s:else>
      <title>Parcelle '<s:property value="plot.name" />'</title>
    </s:else>
    <content tag="current-category">contextual</content>
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url action='domains-list' namespace='/domains' />">Domaines</a></li>
        <li>&gt; <a href="<s:url action='domains-edit-input' namespace='/domains'>
        <s:param name="domainTopiaId"><s:property value="domain.topiaId" /></s:param>
      </s:url>#tab_7"><s:property value="domain.name" /></a></li>
        <s:if test="plot.topiaId != null">
          <li>&gt; <s:property value="plot.name" /></li>
        </s:if>
        <s:else>
          <li>&gt; Nouvelle parcelle</li>
        </s:else>
      </ul>
    </div>

    <ul class="actions">
      <li><a class="action-retour" href="<s:url action='domains-edit-input' namespace='/domains'>
        <s:param name="domainTopiaId"><s:property value="domain.topiaId" /></s:param>
      </s:url>#tab_7">Retour à la liste des parcelles du domaine <s:property value="domain.name" /></a></li>
    </ul>

    <ul class="float-right informations">
      <s:if test="%{plot.topiaId != null}">
        <li>
          <span class="label">Parcelle<s:if test="!plot.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/plots' action='plots-edit-input'/>?plotTopiaId=<s:property value='plot.topiaId'/>"><s:property value="plot.name" /></a>
        </li>
        <li>
          <span class="label">Domaine<s:if test="!domain.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='domain.topiaId'/>"><s:property value="domain.name" /></a>
        </li>
        <li><span class="label">Campagne</span><s:property value="domain.campaign" />
        (<s:property value="(domain.campaign)-1" /> - <s:property value="domain.campaign" />)</li>
        <li><span class="label">Pièces jointes</span><a href="#" id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='plot.code' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(plot.code)" /></a></li>
      </s:if>
    </ul>

    <s:if test="relatedPlots != null">
      <ul class="timeline">
        <s:iterator value="relatedPlots" var="relatedPlot">
            <li<s:if test="#relatedPlot.value.equals(plot.topiaId)"> class="selected"</s:if>>
              <a href="<s:url namespace='/plots' action='plots-edit-input' />?plotTopiaId=<s:property value="value"/>"><s:property value="key" /></a>
            </li>
        </s:iterator>
      </ul>
    </s:if>

    <div ng-app="PlotEditModule" class="page-content">
      <form name="plotEditForm" action="<s:url action='plots-edit' namespace='/plots' />" method="post" class="tabs clear" ng-controller="PlotEditController" ag-confirm-on-exit>
        <s:actionerror cssClass="send-toast-to-js"/>
        <s:hidden name="plotTopiaId" value="%{plot.topiaId}"/>
        <s:hidden name="domainTopiaId" value="%{domain.topiaId}"/>

        <ul id="tabs-plots-menu"  class="tabs-menu clearfix">
          <li class="selected"><span>Généralités</span></li>
          <li><span>Zonage</span></li>
          <li><span>Equipements</span></li>
          <li><span>Sols</span></li>
          <li><span>Zones</span></li>
          <li><span>Éléments de voisinage</span></li>
          <li><span>Observations et mesures réalisées</span></li>

        </ul>

        <div id="tabs-plots-content" class="tabs-content">
          <div id="tab_0">
            <fieldset>

              <div class="wwgrp">
                <span class="wwlbl"><label>Domaine&nbsp;:</label></span>
                <span class="wwctrl generated-content">
                  <s:property value="domain.name" />
                </span>
              </div>

              <s:textfield label="Nom" id="plot.name" name="plot.name" ng-model="plot.name" placeholder="ex. : Parcelle tf620"
                labelPosition="left" labelSeparator=" :" requiredLabel="true" required="true"/>

              <div class="wwgrp">
                <s:fielderror fieldName="locationTopiaId" />
                <span class="wwlbl">
                    <label for="commune"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Commune&nbsp;:</label>
                </span>
                <span class="wwctrl">
                  <s:if test="%{plot.topiaId != null}">
                    <s:if test="%{plot.location != null}">
                      <input type="text" id="commune" placeholder="ex. : 37260, Artannes-sur-Indre" value="${plot.location.codePostal}, ${plot.location.commune}" />
                      <input type="hidden" id="locationTopiaId" name="locationTopiaId"  value="${plot.location.topiaId}" />
                    </s:if>
                    <s:else>
                      <input type="text" id="commune" placeholder="ex. : 37260, Artannes-sur-Indre" value="" />
                      <input type="hidden" id="locationTopiaId" name="locationTopiaId" />
                    </s:else>
                  </s:if>
                  <s:else>
                    <input type="text" id="commune" placeholder="ex. : 37260, Artannes-sur-Indre" value="${domain.location.codePostal}, ${domain.location.commune}"/>
                    <input type="hidden" id="locationTopiaId" name="locationTopiaId"  value="${domain.location.topiaId}" />
                  </s:else>
                </span>
              </div>

              <s:select label="Système de culture" name="growingSystemTopiaId"
                        list="growingSystems" labelPosition="left" labelSeparator=" :" listKey="topiaId"
                        listValue="name + ' (' + growingPlan.domain.campaign + ')'" emptyOption="true"
                        disabled="!changeableGrowingSystem"/>

              <div class="two-columns">
                <div class="wwgrp">
                  <s:fielderror fieldName="plot.area" />
                  <span class="wwlbl"><label for="plotAreaField"><span class="required">*</span> Surface totale&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="plotAreaField" type="text" name="plot.area" placeholder="ex. : 60.5" ng-model="plot.area" ag-float pattern="^[\-\+]?\d*[\.,]?\d*$" />
                      <span class="add-on">ha</span>
                    </span>
                  </span>
                </div>
              </div>

              <div class="two-columns">
                <div class="wwgrp">
                  <s:fielderror fieldName="plot.distanceToHQ" />
                  <span class="wwlbl"><label for="distanceToHQ">Distance au siège d’exploitation&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="distanceToHQ" type="text" name="plot.distanceToHQ" placeholder="ex. : 60.5" ng-model="plot.distanceToHQ" ag-float pattern="^\d*[\.,]?\d*$" />
                      <span class="add-on">km</span>
                    </span>
                  </span>
                </div>
              </div>

              <div class="wwgrp">
                <s:fielderror fieldName="plot.pacIlotNumber" />
                <span class="wwlbl"><label for="pacIlotNumber"><span class="required">*</span> Numéro d'ilôt PAC&nbsp;:</label></span>
                <span class="wwctrl">
                  <input id="pacIlotNumber" type="text" name="plot.pacIlotNumber" placeholder="ex. : 3" pattern="\d*" ng-model="plot.pacIlotNumber"  ag-integer pattern="\d+"/>
                </span>
              </div>

              <div class="wwgrp">
                <s:fielderror fieldName="plot.maxSlope" />
                <span class="wwlbl"><label for="plotMaxSlopeField">Pente maxi&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="plotMaxSlopeField" name="plot.maxSlope" ng-model="plot.maxSlope">
                    <option value=""></option>
                    <s:iterator value="maxSlopes">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>

              <div class="wwgrp">
                <s:fielderror fieldName="plot.waterFlowDistance" />
                <span class="wwlbl"><label for="plotWaterFlowDistanceField">Distance à un cours d'eau&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="plotWaterFlowDistanceField" name="plot.waterFlowDistance" ng-model="plot.waterFlowDistance">
                    <option value=""></option>
                    <s:iterator value="waterFlowDistances">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>
              <div class="wwgrp">
                <s:fielderror fieldName="plot.bufferStrip" />
                <span class="wwlbl"><label for="plotBufferStripField">Bande enherbée&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="plotBufferStripField" name="plot.bufferStrip" ng-model="plot.bufferStrip">
                    <option value=""></option>
                    <s:iterator value="bufferStrips">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>

              <div class="wwgrp">
                <s:fielderror fieldName="plot.latitude" />
                <span class="wwlbl"><label for="plotLatitude"> Latitude GPS du centre de la parcelle&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class='contextual-help'>
                    <span class='help-hover'>
                      <s:text name="help.plot.latitudeLongitude"/>
                    </span>
                  </span>
                  <input id="plotLatitude" type="text" name="plot.latitude" ng-model="plot.latitude" placeholder="ex. : 47.21050" />
                </span>
              </div>

              <s:textfield label="Longitude GPS du centre de la parcelle" name="plot.longitude" ng-model="plot.longitude" placeholder="ex. : -1.48646" labelPosition="left" labelSeparator=" :" />

              <div class="wwgrp">
                <span class="wwctrl">
                  <a class="btn icon-locate" title="Afficher sur une carte" ng-click="displayLocation()" >Voir la position sur la carte</a>
                  </span>
              </div>

              <s:textarea label="Commentaire sur la parcelle" name="plot.comment" ng-model="plot.comment" labelPosition="top" labelSeparator=" :" />
              <s:textarea label="Motif de fin d'utilisation" name="plot.activityEndComment" ng-model="plot.activityEndComment" labelPosition="top" labelSeparator=" :" />
            </fieldset>
          </div>

          <div id="tab_1" ng-controller="PlotZoningController">
           <fieldset>
             <div class="wwgrp radio-list">
                <span class="wwlbl"><label>Déclarer la parcelle hors de tout zonage&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="plot.outOfZoning" ng-value="true" ng-model="plot.outOfZoning" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="plot.outOfZoning" ng-value="false" ng-model="plot.outOfZoning" />Non
                  </label>
                </span>
             </div>

             <div ng-show="!plot.outOfZoning" class="slide-animation">
               <div class="wwgrp checkbox-list">
                 <div class="wwlbl">
                   <label class="label">Zonage&nbsp;:</label>
                 </div>
                 <div class="wwctrl">
                   <span ng-repeat="parcelleZonage in parcelleZonages">
                     <input type="checkbox" id="selectedPlotZoningIds-{{$index}}" name="selectedPlotZoningIds"
                     ng-model="selectedPlotZoningIds[parcelleZonage.topiaId]" value="{{parcelleZonage.topiaId}}" />
                     <label for="selectedPlotZoningIds-{{$index}}" class="checkboxLabelAgrosyst">{{parcelleZonage.libelle_engagement_parcelle}}</label>
                   </span>
                   <input type="hidden" id="__multiselect_selectedPlotZoningIds" name="__multiselect_selectedPlotZoningIds" value="" />
                 </div>
               </div>
             </div>

             <s:textarea label="Commentaire" name="plot.zoningComment" ng-model="plot.zoningComment" labelSeparator=" :" />
           </fieldset>
          </div>

          <div id="tab_2">
            <fieldset>
              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="irrigationSystem">Système d'irrigation&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="plot.irrigationSystem" ng-value="true" ng-model="plot.irrigationSystem" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="plot.irrigationSystem" ng-value="false" ng-model="plot.irrigationSystem" />Non
                  </label>
                </span>
              </div>

              <div ng-show="plot.irrigationSystem" class="slide-animation">
                <s:select label="Type de système d'irrigation" name="plot.irrigationSystemType" ng-model="plot.irrigationSystemType" list="irrigationSystemTypes"
                   labelPosition="left" labelSeparator=" :" emptyOption="true"/>
                <s:select label="Type de moteur de pompe" name="plot.pompEngineType" ng-model="plot.pompEngineType" list="pompEngineTypes"
                   labelPosition="left" labelSeparator=" :" emptyOption="true"/>
                <s:select label="Positionnement des tuyaux d'arrosage" name="plot.hosesPositionning" ng-model="plot.hosesPositionning" list="hosesPositionnings"
                  labelPosition="left" labelSeparator=" :" emptyOption="true"/>

                <div class="wwgrp radio-list">
                  <span class="wwlbl"><label for="fertigationSystem">Système de fertirrigation&nbsp;:</label></span>
                  <span class="wwctrl">
                    <label class="radioLabel">
                      <input type="radio" name="plot.fertigationSystem" ng-value="true" ng-model="plot.fertigationSystem" />Oui
                    </label>
                    <label class="radioLabel">
                      <input type="radio" name="plot.fertigationSystem" ng-value="false" ng-model="plot.fertigationSystem" />Non
                    </label>
                  </span>
                </div>
                <s:textfield label="Origine de l'eau" name="plot.waterOrigin" ng-model="plot.waterOrigin"
                   placeholder="ex. : Source" labelPosition="left" labelSeparator=" :" />
              </div>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="drainage"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Drainage&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="plot.drainage" ng-value="true" ng-model="plot.drainage" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="plot.drainage" ng-value="false" ng-model="plot.drainage" />Non
                  </label>
                </span>
              </div>
              <div ng-show="plot.drainage" class="slide-animation">
                <s:textfield label="Année de réalisation du drainage" name="plot.drainageYear" ng-model="plot.drainageYear" ag-campaign="wtfwstruts" pattern="(?:19|20)[0-9]{2}"
                   placeholder="ex. : 2013" labelPosition="left" labelSeparator=" :" />
              </div>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="frostProtection">Protection anti-gel&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="plot.frostProtection" ng-value="true" ng-model="plot.frostProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="plot.frostProtection" ng-value="false" ng-model="plot.frostProtection" />Non
                  </label>
                </span>
              </div>
              <div ng-show="plot.frostProtection" class="slide-animation">
               <s:select label="Type de protection anti-gel" name="plot.frostProtectionType" list="frostProtectionTypes"
                labelPosition="left" labelSeparator=" :" emptyOption="true"/>
              </div>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="hailProtection">Protection anti-grêle&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="plot.hailProtection" ng-value="true" ng-model="plot.hailProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="plot.hailProtection" ng-value="false" ng-model="plot.hailProtection" />Non
                  </label>
                </span>
              </div>
              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="rainproofProtection">Protection anti-pluie&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="plot.rainproofProtection" ng-value="true" ng-model="plot.rainproofProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="plot.rainproofProtection" ng-value="false" ng-model="plot.rainproofProtection" />Non
                  </label>
                </span>
              </div>
              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="pestProtection">Protection anti-insectes&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="plot.pestProtection" ng-value="true" ng-model="plot.pestProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="plot.pestProtection" ng-value="false" ng-model="plot.pestProtection" />Non
                  </label>
                </span>
              </div>

              <s:textarea label="Autre équipement" ng-model="plot.otherEquipment" name="plot.otherEquipment" labelPosition="top" labelSeparator=" :" />
              <s:textarea label="Commentaire sur l’équipement de la parcelle" ng-model="plot.equipmentComment" name="plot.equipmentComment" labelPosition="top" labelSeparator=" :" />
            </fieldset>
          </div>

          <div id="tab_3" ng-controller="PlotSolController">
            <fieldset>
              <input type="hidden" name="solHorizons" value="{{solHorizons}}" />

              <div class="wwgrp">
                <span class="wwlbl"><label for="selectedSolField"><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Sol du domaine&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="selectedSolField" name="selectedSolId" ng-model="selectedSolId" ng-change="updateSelectedSol(selectedSolId)">
                    <option value=""></option>
                    <s:iterator value="grounds">
                      <option value="<s:property value="topiaId" />"><s:property value="name" /></option>
                    </s:iterator>
                  </select>
                  <span ng-show="selectedSolId" class="indicative-content font-bold" title="Les données indicatives correspondent au sol sélectionné">Données indicatives du sol {{selectedSol.name}}</span>
                </span>
              </div>

              <div class="wwgrp" ng-show="selectedSolId">
                <span class="wwlbl"><label>&nbsp;</label></span>
              </div>

              <div ng-show="selectedSolId">
                <div class="wwgrp">
                  <span class="wwlbl"><label for="surfaceTextureField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Texture moyenne de la surface&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="surfaceTextureField" name="selectedSurfaceTextureId" ng-model="selectedSurfaceTextureId">
                      <option value=""></option>
                      <s:iterator value="solTextures" var="solTexture">
                        <option value="<s:property value="topiaId" />"><s:property value="classes_texturales_GEPAA" /></option>
                      </s:iterator>
                    </select>
                    <span class="indicative-content">{{selectedSol.refSolArvalis.sol_texture}}</span>
                  </span>
                </div>

                <div class="wwgrp">
                  <span class="wwlbl"><label for="subSoilTextureField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Texture moyenne du sous-sol&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="subSoilTextureField" name="selectedSubSoilTextureId" ng-model="selectedSubSoilTextureId">
                      <option value=""></option>
                      <s:iterator value="solTextures" var="solTexture">
                        <option value="<s:property value="topiaId" />"><s:property value="classes_texturales_GEPAA" /></option>
                      </s:iterator>
                    </select>
                    <span class="indicative-content">{{selectedSol.refSolArvalis.sol_texture}}</span>
                  </span>
                </div>

                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="solStoninessField">Pierrosité moyenne&nbsp;:</label>
                 </span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="solStoninessField" type="text" placeholder="ex. : 5" name="plot.solStoniness" ng-model="plot.solStoniness" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" /><span class="add-on">%</span>
                      <span class="indicative-content">{{selectedSol.refSolArvalis.sol_pierrosite}}</span>
                    </span>
                  </span>
                </div>

                <div class="wwgrp">
                  <span class="wwlbl"><label for="solDepthField"><span class="requiredIndicator"  title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Classe profondeur maxi enracinement&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="solDepthField" name="selectedSolProfondeurId" ng-model="selectedSolProfondeurId">
                      <option value=""></option>
                      <s:iterator value="solProfondeurs" var="solProfondeur">
                        <option value="<s:property value="topiaId" />"<s:if test="#solProfondeur.topiaId == selectedSolProfondeurId"> selected</s:if>><s:property value="libelle_classe" /></option>
                      </s:iterator>
                    </select>
                    <span class="indicative-content">{{selectedSol.refSolArvalis.sol_profondeur}}</span>
                  </span>
                </div>

                <div class="wwgrp">
                  <span class="wwlbl"><label for="solMaxDepthField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Profondeur maxi enracinement&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="solMaxDepthField" type="text" placeholder="ex. : 60" name="plot.solMaxDepth" ng-model="plot.solMaxDepth" ag-integer pattern="\d+" /><span class="add-on">cm</span>
                      <span class="indicative-content">{{selectedSol.refSolArvalis.sol_profondeur}}</span>
                    </span>
                  </span>
                </div>

                <div class="wwgrp">
                  <span class="wwlbl"><label>Réserve utile&nbsp;:</label></span>
                  <span class="wwctrl generated-content">
                    {{computeUsefullReserve()}} mm
                  </span>
                </div>

                <div class="wwgrp">
                  <span class="wwlbl"><label for="solOrganicMaterialPercentField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Pourcentage Matière Organique&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="solOrganicMaterialPercentField" type="text" placeholder="ex. : 5" name="plot.solOrganicMaterialPercent" ng-model="plot.solOrganicMaterialPercent" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" />
                      <span class="add-on">%</span>
                    </span>
                  </span>
                </div>

                <div class="wwgrp radio-list">
                  <span class="wwlbl"><label for="solBattanceField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Battance&nbsp;:</label></span>
                  <span class="wwctrl">
                    <label class="radioLabel">
                      <input type="radio" name="plot.solBattance" ng-value="true" ng-model="plot.solBattance" />Oui
                    </label>
                    <label class="radioLabel">
                      <input type="radio" name="plot.solBattance" ng-value="false" ng-model="plot.solBattance" />Non
                    </label>
                  </span>
                </div>

                <s:select label="PH eau" name="plot.solWaterPh" list="solWaterPhs"
                   labelPosition="left" labelSeparator=" :" emptyOption="true"/>

                <div class="wwgrp radio-list">
                  <span class="wwlbl"><label for="solHydromorphismsField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Hydromorphie&nbsp;:</label></span>
                  <span class="wwctrl">
                    <label class="radioLabel">
                      <input type="radio" name="plot.solHydromorphisms" ng-value="true" ng-model="plot.solHydromorphisms" />Oui
                    </label>
                    <label class="radioLabel">
                      <input type="radio" name="plot.solHydromorphisms" ng-value="false" ng-model="plot.solHydromorphisms" />Non
                    </label>
                    <span class="indicative-content">{{selectedSol.refSolArvalis.sol_hydromorphie}}</span>
                  </span>
                </div>

                <div class="wwgrp radio-list">
                  <span class="wwlbl"><label for="solLimestoneField">Calcaire&nbsp;:</label></span>
                  <span class="wwctrl">
                    <label class="radioLabel">
                      <input type="radio" name="plot.solLimestone" ng-value="true" ng-model="plot.solLimestone" />Oui
                    </label>
                    <label class="radioLabel">
                      <input type="radio" name="plot.solLimestone" ng-value="false" ng-model="plot.solLimestone" />Non
                    </label>
                    <span class="indicative-content">{{selectedSol.refSolArvalis.sol_calcaire}}</span>
                  </span>
                </div>

                <div ng-show="plot.solLimestone" class="slide-animation">
                  <div class="wwgrp">
                    <span class="wwlbl"><label for="solActiveLimestoneField">Proportion calcaire actif&nbsp;:</label></span>
                    <span class="wwctrl">
                      <span class="input-append">
                        <input id="solActiveLimestoneField" type="text" name="plot.solActiveLimestone" ng-model="plot.solActiveLimestone"
                               ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" placeholder="ex. : 5"/>
                        <span class="add-on">%</span>
                      </span>
                    </span>
                  </div>

                  <div class="wwgrp">
                    <span class="wwlbl"><label for="solTotalLimestoneField">Proportion calcaire total&nbsp;:</label></span>
                    <span class="wwctrl">
                      <span class="input-append">
                        <input id="solTotalLimestoneField" type="text" name="plot.solTotalLimestone" ng-model="plot.solTotalLimestone"
                               ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" placeholder="ex. : 5" />
                        <span class="add-on">%</span>
                      </span>
                    </span>
                  </div>
                </div>

                <s:textarea label="Commentaires" name="plot.solComment" ng-model="plot.solComment" labelPosition="top" labelSeparator=" :" />

                <div class="table-enclosure">
                  <label>Description des horizons du sol&nbsp;:</label>

                  <table class="data-table">
                    <thead>
                      <tr>
                        <th scope="col">Numéro de l'horizon</th>
                        <th scope="col">Cote basse (cm)</th>
                        <th scope="col">Épaisseur de l'horizon (cm)</th>
                        <th scope="col">Pierrosité horizon (%)</th>
                        <th scope="col">Texture horizon</th>
                        <th scope="col" class="column-xsmall">Suppr.</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-show="solHorizons.length == 0" >
                        <td colspan="6" class="empty-table">
                          Il n'y a pas encore d'horizons pour le sol de cette parcelle. Pour en ajouter, cliquez sur le bouton "Ajouter" ci-dessous
                        </td></tr>
                      <tr ng-repeat="solHorizon in solHorizons | orderBy:'lowRating'" ng-click="editSolHorizon(solHorizon, $index)"
                        ng-class="{'selected-line' : selectedSolHorizon == solHorizon}" class="selectCursor">
                        <td>
                          {{$index + 1}}
                        </td>
                        <td>
                          {{solHorizon.lowRating}}
                        </td>
                        <td>
                          {{computeSolHorizonThickness(solHorizon, $index)}}
                        </td>
                        <td>
                          {{solHorizon.stoniness}}
                        </td>
                        <td>
                          {{solTextureLabel(solHorizon)}}
                        </td>
                        <td>
                          <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteSolHorizon($index)"/>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="6">
                          <div class="table-end-button">
                            <input type="button" value="Ajouter" ng-click="addSolHorizon()"/>
                          </div>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>

                <div ng-show="selectedSolHorizon" class="slide-animation sub-form" ng-form="editSolHorizonForm">
                  <div class="sub-form-content">
                    <div class="wwgrp">
                      <span class="wwlbl"><label class="historicLabelStyle">Numéro de l'horizon&nbsp;:</label></span>
                      <span class="wwctrl generated-content">
                        {{selectedSolHorizonIndex + 1}}
                      </span>
                    </div>
                    <div class="wwgrp">
                      <div class="messages-panel" ng-show="editSolHorizonForm.selectedSolHorizonLowRating.$dirty && editSolHorizonForm.selectedSolHorizonLowRating.$error.validator">
                        <ul class="errorMessage">
                          <li>La cote basse doit être comprise entre celle de l'horizon précédent et celle de l'horizon suivant !</li>
                        </ul>
                      </div>
                      <span class="wwlbl"><label class="historicLabelStyle" for="lowRatingField">Cote basse&nbsp;:</label></span>
                      <span class="wwctrl">
                        <span class="input-append">
                          <input type="text" id="lowRatingField" ng-model="selectedSolHorizon.lowRating" name="selectedSolHorizonLowRating" ag-float pattern="^[\-\+]?\d*[\.,]?\d*$" placeholder="ex. : 25,1"
                        ui-validate="'validateHorizonLowRating($value)'"/>
                          <span class="add-on">cm</span>
                        </span>
                      </span>
                    </div>
                    <div class="wwgrp">
                      <span class="wwlbl"><label class="historicLabelStyle" for="thicknessField">Épaisseur de l'horizon&nbsp;:</label></span>
                      <span class="wwctrl generated-content">
                        {{computeSolHorizonThickness(selectedSolHorizon, selectedSolHorizonIndex)}} cm
                      </span>
                    </div>
                    <div class="wwgrp">
                      <span class="wwlbl"><label class="historicLabelStyle" for="stoninessField">Pierrosité horizon&nbsp;:</label></span>
                      <span class="wwctrl">
                        <span class="input-append">
                          <input type="text" id="stoninessField" ng-model="selectedSolHorizon.stoniness" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" placeholder="ex. : 5"/>
                          <span class="add-on">%</span>
                        </span>
                      </span>
                    </div>
                    <div class="wwgrp">
                      <span class="wwlbl"><label class="historicLabelStyle" for="solHorizonTextureField">Texture horizon&nbsp;:</label></span>
                      <span class="wwctrl"><select id="solHorizonTextureField" ng-model="selectedSolHorizon.solTextureId">
                        <option value=""></option>
                        <s:iterator value="solTextures" var="solTexture">
                          <option value="<s:property value="topiaId" />"><s:property value="classes_texturales_GEPAA" /></option>
                        </s:iterator>
                      </select></span>
                    </div>
                    <div class="wwgrp">
                      <span class="wwlbl"><label class="historicLabelStyle" for="horizonCommentField">Commentaire&nbsp;:</label></span>
                      <span class="wwctrl">
                        <textarea id="horizonCommentField" ng-model="selectedSolHorizon.comment"></textarea>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>

          <div id="tab_4" ng-controller="PlotZoneController">
            <fieldset>
              <input type="hidden" name="zones" value="{{zones}}" />

              <s:if test="plot.deletedZones">
                <div class="messages-panel">
                  <ul class="infoMessage">
                    <li>Certaines zones de cette parcelle ont été supprimées !</li>
                  </ul>
                </div>
              </s:if>

              <div class="table-enclosure">
                <label>Liste des zones de la parcelle&nbsp;:</label>

                <table class="data-table">
                  <thead>
                    <tr>
                      <th scope="col">Nom</th>
                      <th scope="col">Type</th>
                      <th scope="col">Surface</th>
                      <th scope="col" class="column-xsmall">État</th>
                      <th scope="col">Liens</th>
                      <th scope="col" class="column-xsmall">Suppr.</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-show="zones.length == 0" >
                      <td colspan="6" class="empty-table">
                        Il n'y a pas encore de zone sur cette parcelle. Pour en ajouter, cliquez sur le bouton "Ajouter" ci-dessous.
                      </td></tr>
                    <tr ng-repeat="zone in zones" ng-click="editZone(zone)"
                      ng-class="{'selected-line' : selectedZone == zone}" class="selectCursor">
                      <td>
                        {{zone.name}}
                      </td>
                      <td>
                        {{zoneTypes[zone.type]|orDash}}
                      </td>
                      <td>
                        {{zone.area|orDash}} ha
                      </td>
                      <td>
                        {{zone.active ? 'Active' : 'Inactive'}}
                      </td>
                      <td>
                        <a ng-if="zone.topiaId" href="<s:url action='effective-crop-cycles-edit-input' namespace='/effective' />?zoneTopiaId={{zone.topiaId}}">Interventions culturales</a>
                      </td>
                      <td>
                        <input class="btn-icon icon-delete" type="button" ng-click="deleteZone($index)"
                          ng-if="zones.length > 1 && !zonesUsageList[zone.topiaId]"  title="Supprimer" />
                        <input class="btn-icon icon-delete-disabled" type="button"
                          ng-if="zones.length == 1" title="Une parcelle comporte nécessairement une zone au minimum" />
                        <input class="btn-icon icon-delete-disabled" type="button"
                               ng-if="zones.length > 1 && zonesUsageList[zone.topiaId]" title="Cette zone est utilisée dans des cycles de cultures et ne peut donc être supprimée" />
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr class="noborder">
                      <td colspan="6">
                        <div class="table-end-button" ng-show="editZoneForm.$valid && availableAreaForNewZone() &gt; 0">
                          <input type="button" value="Ajouter" ng-click="addZone()"/>
                        </div>
                        <div class="table-end-button" ng-show="availableAreaForNewZone() &lt;= 0">
                          Pour créer une nouvelle zone, vous devez diminuer la surface d'une des autres zones.
                        </div>
                      </td>
                    </tr>
                  </tfoot>
                </table>

                <div class="help-explanation-panel" ng-show="!selectedZone">
                  Une zone principale de toute la surface de la parcelle est automatiquement créée en même temps que la parcelle. D’autres zones peuvent être créées en diminuant la surface de la parcelle.
                </div>
              </div>

              <div ng-show="selectedZone" class="slide-animation sub-form" ng-form="editZoneForm">
                <div class="sub-form-content">
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="zoneNameField"><span class="required">*</span> Nom de la zone&nbsp;:</label></span>
                    <span class="wwctrl">
                      <input id="zoneNameField" type="text" ng-model="selectedZone.name" ng-required="selectedZone" />
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="zoneTypeField">Type&nbsp;:</label></span>
                    <span class="wwctrl"><select id="zoneTypeField" ng-model="selectedZone.type">
                      <option value=""></option>
                      <s:iterator value="zoneTypes">
                        <option value="<s:property value='key' />"><s:property value="value" /></option>
                      </s:iterator>
                    </select></span>
                  </div>
                  <div class="wwgrp">
                    <div class="wwerr clearfix">
                      <div class="messages-panel" ng-show="editZoneForm.selectedZoneArea.$dirty && editZoneForm.selectedZoneArea.$error.validator">
                        <ul class="errorMessage">
                          <li>La surface totale des zones est supérieure à la surface totale de la parcelle !</li>
                        </ul>
                      </div>
                    </div>
                    <span class="wwlbl"><label class="historicLabelStyle" for="zoneAreaField"><span class="required">*</span> Surface&nbsp;:</label></span>
                    <span class="wwctrl">
                      <span class="input-append">
                        <input id="zoneAreaField" type="text" ng-model="selectedZone.area" name="selectedZoneArea"
                        ag-float pattern="^[\-\+]?\d*[\.,]?\d*$" ui-validate="'validateZoneSurface($value)'" ng-required="selectedZone"/>
                        <span class="add-on">ha</span>
                      </span>
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="zoneLatitudeField">Latitude du centre de la zone&nbsp;:</label></span>
                    <span class="wwctrl">
                      <input id="zoneLatitudeField" type="text" ng-model="selectedZone.latitude" ag-location pattern="^[+-]?[0-9]+([\.,][0-9]+)?" />
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="zoneLongitudeField">Longitude du centre de la zone&nbsp;:</label></span>
                    <span class="wwctrl">
                      <input id="zoneLongitudeField" type="text" ng-model="selectedZone.longitude" ag-location pattern="^[+-]?[0-9]+([\.,][0-9]+)?" />
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="zoneCommentField">Commentaire&nbsp;:</label></span>
                    <span class="wwctrl">
                      <textarea id="zoneCommentField" ng-model="selectedZone.comment"></textarea>
                    </span>
                  </div>
                  <div class="wwgrp">
                    <label class="historicLabelStyle" for="zoneActiveField">Actif&nbsp;:</label>
                    <input id="zoneActiveField" type="checkbox" ng-model="selectedZone.active" />
                  </div>
                </div>
              </div>
             </fieldset>
          </div>

          <div id="tab_5" ng-controller="PlotAdjacentElementsController">
            <fieldset>
              <div class="wwgrp checkbox-list">
                <div class="wwlbl">
                  <label class="label">Éléments de voisinage&nbsp;:</label>
                </div>
                <div class="wwctrl">
                  <span ng-repeat="adjacentElement in adjacentElements">
                    <input type="checkbox" id="adjacentElementIds-{{$index}}" name="adjacentElementIds"
                           ng-model="adjacentElementIds[adjacentElement.topiaId]" value="{{adjacentElement.topiaId}}" />
                    <label for="adjacentElementIds-{{$index}}" class="checkboxLabelAgrosyst">{{adjacentElement.iae_nom_Translated}}</label>
                  </span>
                  <input type="hidden" id="__multiselect_adjacentElementIds" name="__multiselect_adjacentElementIds" value="" />
                </div>
              </div>
              <div class="wwgrp">
                <div class="wwlbl">
                  <label class="label" for="zoneAdjacentCommentField">Commentaire&nbsp;:</label>
                </div>
                <div class="wwctrl">
                  <textarea id="zoneAdjacentCommentField" ng-model="plot.adjacentComment" name="plot.adjacentComment"></textarea>
                </div>
              </div>
            </fieldset>
          </div>

          <!-- Observations et mesures réalisées -->
          <%@include file="plots-edit-input-measurements.jsp" %>

          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url action='domains-edit-input' namespace='/domains'>
              <s:param name="domainTopiaId"><s:property value="domain.topiaId" /></s:param>
              </s:url>#tab_7">Annuler
            </a>
            <input type="submit" class="btn-primary" value="Enregistrer"
              <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
              <s:if test="!activated">disabled="disabled" title="La parcelle sur laquelle vous travaillez est inactive et/ou liée à un domaine inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
            />
          </span>
        </div>

        <div id="confirmDeleteZone" title="Suppression d'une zone" class="auto-hide">
          Êtes-vous sûr(e) de vouloir supprimer cette zone&nbsp;?
        </div>
      </form>
    
      <div id="show-location" title="Affichage d'une position GPS" class="auto-hide">
        Chargement...
      </div>

    </div>
  </body>
</html>
