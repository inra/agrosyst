<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- plots-edit-input-measurements.jsp -->
<div id="tab_6">
  <table class="data-table">
    <thead>
      <tr>
        <th scope="col">Zone</th>
        <th scope="col">Date</th>
        <th scope="col">Type</th>
        <th scope="col">Numéro de répétition</th>
        <th scope="col">Variable</th>
        <th scope="col">Valeur</th>
      </tr>
    </thead>
    <tbody>
      <s:if test="measurementSessions == null || measurementSessions.empty">
      <tr>
          <td class="empty-table" colspan="6">
            Il n'y a pas encore de mesures ou observations définies sur ce domaine.
          </td>
        </tr>
      </s:if>
      <s:iterator value="measurementSessions" var="measurementSession">
        <s:iterator value="#measurementSession.measurements" var="measurement">
            <tr>
              <td>
                ${measurementSession.zone.name}
              </td>
              <td>
                <a href="<s:url action='effective-measurements-edit-input' namespace='/effective'>
                  <s:param name="zoneTopiaId" value="#measurementSession.zone.topiaId" />
                </s:url>">
                  <s:if test="#measurementSession.startDate.equals(#measurementSession.endDate)">
                    Le <javatime:format value="${measurementSession.startDate}" pattern="dd/MM/yyyy" />
                  </s:if>
                  <s:else>
                    Du <javatime:format value="${measurementSession.startDate}" pattern="dd/MM/yyyy" /> au
                       <javatime:format value="${measurementSession.endDate}" pattern="dd/MM/yyyy" />
                  </s:else>
                </a>
              </td>

              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@PLANTE">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.PLANTE" /></td>
              </s:if>
              <s:elseif test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@SOL">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.SOL" /></td>
              </s:elseif>
              <s:elseif test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@TRANSFERT_DE_SOLUTES">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.TRANSFERT_DE_SOLUTES" /></td>
              </s:elseif>
              <s:elseif test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@GES">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.GES" /></td>
              </s:elseif>
              <s:elseif test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@METEO">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.METEO" /></td>
              </s:elseif>
              <s:elseif test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@STADE_CULTURE">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.STADE_CULTURE" /></td>
              </s:elseif>
              <s:elseif test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES" /></td>
              </s:elseif>
              <s:elseif test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@ADVENTICES">
                <td><s:text name="fr.inra.agrosyst.api.entities.measure.MeasurementType.ADVENTICES" /></td>
              </s:elseif>
              <s:else>
                <td>-</td>
              </s:else>

              <td>
                <s:if test="#measurement.repetitionNumber != null">
                  <s:property value="#measurement.repetitionNumber" />
                </s:if>
                <s:else>-</s:else>
              </td>

              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@PLANTE">
                <td>
                  <s:if test="#measurement.refMesure != null">
                    <s:property value="#measurement.refMesure.variable_mesuree" />
                  </s:if>
                  <s:else>-</s:else>
                </td>
                <td><s:property value="#measurement.measureValue" />&nbsp;<s:property value="#measurement.measureUnit" /></td>
              </s:if>
              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@SOL">
                <td>
                  <s:if test="#measurement.refMesure != null">
                    <s:property value="#measurement.refMesure.variable_mesuree" />
                  </s:if>
                  <s:else>-</s:else>
                </td>
                <td><s:property value="#measurement.measureValue" />&nbsp;<s:property value="#measurement.measureUnit" /></td>
              </s:if>
              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@TRANSFERT_DE_SOLUTES">
                <td>
                  <s:if test="#measurement.refMesure != null">
                    <s:property value="#measurement.refMesure.variable_mesuree" />
                  </s:if>
                  <s:else>-</s:else>
                </td>
                <td><s:property value="#measurement.measureValue" />&nbsp;<s:property value="#measurement.measureUnit" /></td>
              </s:if>
              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@GES">
                <td>
                  <s:if test="#measurement.refMesure != null">
                    <s:property value="#measurement.refMesure.variable_mesuree" />
                  </s:if>
                  <s:else>-</s:else>
                </td>
                <td><s:property value="#measurement.measureValue" />&nbsp;<s:property value="#measurement.measureUnit" /></td>
              </s:if>
              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@METEO">
                <td>
                  <s:if test="#measurement.refMesure != null">
                    <s:property value="#measurement.refMesure.variable_mesuree" />
                  </s:if>
                  <s:else>-</s:else>
                </td>
                <td><s:property value="#measurement.measureValue" />&nbsp;<s:property value="#measurement.measureUnit" /></td>
              </s:if>
              
              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@STADE_CULTURE">
                <td>-</td>
                <td><s:property value="#measurement.cropNumberObserved" /></td>
              </s:if>
              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES">
                  <s:if test="#measurement.protocol != null">
                    <td><s:property value="#measurement.protocol.protocole_libelle"/></td>
                    <td><s:property value="#measurement.quantitativeValue" />&nbsp;
                        <s:if test="#measurement.protocol.releve_qualifiant_unite_mesure != null && !#measurement.protocol.releve_qualifiant_unite_mesure.empty">
                          <s:property value="#measurement.protocol.releve_qualifiant_unite_mesure" />
                        </s:if>
                        <s:elseif test="#measurement.protocol.releve_unite != null && !#measurement.protocol.releve_unite.empty">
                          <s:property value="#measurement.protocol.releve_unite" />
                        </s:elseif>
                        <s:else>-</s:else>
                    </td>
                  </s:if>
                  <s:else>
                    <td>
                      <s:if test="#measurement.pest != null">
                        <s:property value="#measurement.pest.reference_label" />
                      </s:if>
                      <s:else>-</s:else>
                    </td>
                    <td><s:property value="#measurement.quantitativeValue" />&nbsp;
                        <s:if test="#measurement.cropUnitQualifier != null">
                          <s:property value="#measurement.cropUnitQualifier.reference_label" />
                        </s:if>
                        <s:elseif test="#measurement.unitEDI != null && !#measurement.unitEDI.empty">
                          <s:property value="#measurement.unitEDI" />
                        </s:elseif>
                        <s:else>-</s:else>
                    </td>
                  </s:else>
              </s:if>
              <s:if test="#measurement.measurementType == @fr.inra.agrosyst.api.entities.measure.MeasurementType@ADVENTICES">
                <td>
                  <s:if test="#measurement.measuredAdventice">
                    <s:property value="#measurement.measuredAdventice.adventice" />
                  </s:if>
                  <s:else>-</s:else>
                </td>
                <td>
                  <s:if test="#measurement.adventiceStage">
                    <s:property value="#measurement.adventiceStage.colonne2" />
                  </s:if>
                  <s:else>-</s:else>
                </td>
              </s:if>
            </tr>
         </s:iterator>
      </s:iterator>
    </tbody>
  </table>
</div>
