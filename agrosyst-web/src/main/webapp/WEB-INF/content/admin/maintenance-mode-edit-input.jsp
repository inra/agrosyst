<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Page d'activation du mode maintenance</title>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
    <content tag="current-category">contextual</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/admin.js'/><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">
       angular.module('MaintenanceModeEdit', ['Agrosyst', 'ui.switch'])
           .value('active', <s:property value="active" escapeHtml="false"/>)
           .value('disconnectAllUsers', <s:property value="disconnectAllUsers" escapeHtml="false"/>)
           .value('connectedUsersNb', <s:property value="connectedUsersNb" escapeHtml="false"/>);
   </script>
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url namespace='/' action='index' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Administration</a></li>
        <li>&gt; Mode maintenance</li>
      </ul>
    </div>

    <s:if test="currentUserAnAdmin">
      <jsp:include page="admin-menu.jsp" />

      <form ng-app='MaintenanceModeEdit'
            ng-controller="MaintenanceModeEditController"
            name="maintenance-mode-form"
            action="<s:url namespace='/admin' action='maintenance-mode-edit' />"
            method="post"
            class="tabs clear">

        <s:actionerror cssClass="send-toast-to-js"/>
        <ul id="tabs-message-menu" class="tabs-menu clearfix">
          <li class="selected"><span>Mode maintenance</span></li>
        </ul>

        <div id="tabs-message-content" class="tabs-content">

          <!-- Généralités -->
          <div>
            <fieldset>
              <legend class="invisibleLabel">Bloc de configuration du mode maintenance</legend>

              <switch name="active"
                      ng-model="active"
                      struts="true"
                      label="Actif&nbsp;:">
              </switch>
              <s:textarea id="message" label="Message" name="message" labelPosition="left" labelSeparator=" :" placeholder="ex. : Un message de miantenance"/>
              <switch name="disconnectAllUsers"
                      ng-model="disconnectAllUsers"
                      struts="true"
                      label="Déconnecter tous les utilisateurs ({{connectedUsersNb}})&nbsp;:">
              </switch>

            </fieldset>
          </div>

          <span class="form-buttons">
            <input type="submit" class="btn-primary" value="Enregistrer"/>
          </span>
        </div>
      </form>
    </s:if>
  </body>
</html>
