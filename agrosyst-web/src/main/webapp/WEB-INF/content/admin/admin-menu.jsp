<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<!-- admin/admin-menu.jsp -->

<div>
  <ul class="admin-menu">
    <li>Gestion des utilisateurs
      <ul>
        <li><a href="<s:url namespace='/security' action='users-list' />">Liste des utilisateurs</a></li>
        <li><a href="<s:url namespace='/security' action='users-edit-input' />">Ajouter un utilisateur</a></li>
      </ul>
    </li>
    <li>Référentiels
      <ul>
        <s:iterator value="referentialClasses" var="klass" status="stat">
        <%--<s:if test="(#stat.count % 6) == 1">
        <ul class="">
        </s:if>--%>
          <li>
            <a href="<s:url namespace='/generic' action='generic-entities-list' />?genericClassName=<s:property value="key" />"><s:text name="%{key}"/></a>
            - <s:property value="value" /> entité(s)
          </li>
        <%--s:if test="(#stat.count % 6) == 0">
        </ul>
        <s:if--%>
        </s:iterator>
      </ul>
    </li>
    <li>Listes d'autorité
      <ul>
        <s:iterator value="authorityClasses" var="klass">
          <li>
            <a href="<s:url namespace='/generic' action='generic-entities-list' />?genericClassName=<s:property value="key" />"><s:text name="%{key}"/></a>
            - <s:property value="value" /> valeur(s)
          </li>
        </s:iterator>
      </ul>
    </li>
    <li>Outils
      <ul>
        <li>
          <a href="<s:url namespace='/admin' action='match-french-amm-for-european-products-input' />">Correspondances AMM France / UE</a>
        </li>
      </ul>
    </li>
    <li>Système
      <ul>
        <li><a href="<s:url namespace='/admin' action='tracked-events-list' />">Journaux de traçabilité</a></li>
        <li><a href="<s:url namespace='/admin' action='maintenance-mode-edit-input' />">Mode maintenance</a></li>
        <li><a href="<s:url namespace='/publishedmessage' action='info-message-edit-input' />">Diffuser un message</a></li>
        <li><a href="<s:url namespace='/publishedmessage' action='info-message-list' />">Liste des messages diffusés</a></li>
        <li><a href="<s:url namespace='/generic' action='generic-entities-list' />?genericClassName=fr.inra.agrosyst.api.entities.security.HashedValue">
          <s:text name="fr.inra.agrosyst.api.entities.security.HashedValue"/>
        </a></li>
        <li><a href="<s:url namespace='/admin' action='show-configuration' />">Paramètres de configuration</a></li>
        <li><a href="<s:url namespace='/admin' action='admin-task' />">Gestion des tâches</a></li>
        <li><a href="<s:url namespace='/admin' action='admin-performances' />">Gestion des performances</a></li>
      </ul>
    </li>
  </ul>
</div>
