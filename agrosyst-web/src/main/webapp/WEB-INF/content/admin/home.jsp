<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Administration</title>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Administration</li>
      </ul>
    </div>

    <s:if test="admin">
      <jsp:include page="admin-menu.jsp" />
    </s:if>

    <s:actionmessage/>
    <s:actionerror cssClass="send-toast-to-js"/>

    <ul class="admin-home-menu">
      <li class="nopadding">
        <div class="stats-panel">
          <div class="data-table-title">
            <h3>Indicateurs de fonctionnement du SI</h3>
          </div>

          <ul class="stats-list">
            <li class="noppading">
              <div class="title">Domaines</div>
              <span class="indicator"><s:property value="activeDomainsCount" /></span> actifs
              / <span class="indicator"><s:property value="domainsCount" /></span>
            </li>
            <li>
              <div class="title">Dispositifs</div>
              <span class="indicator"><s:property value="activeGrowingPlansCount" /></span> actifs
              / <span class="indicator"><s:property value="growingPlansCount" /></span>
            </li>
            <li>
              <div class="title">Systèmes de culture</div>
              <span class="indicator"><s:property value="activeGrowingSystemsCount" /></span> actifs
              / <span class="indicator"><s:property value="growingSystemsCount" /></span>
            </li>
            <li>
              <div class="title">Réseaux</div>
              <span class="indicator"><s:property value="activeNetworksCount" /></span> actifs
              / <span class="indicator"><s:property value="networksCount" /></span>
            </li>
            <li>
              <div class="title">Utilisateurs</div>
              <span class="indicator"><s:property value="activeUsersCount" /></span> actifs
              / <span class="indicator"><s:property value="usersCount" /></span><br/>
              <span class="indicator"><s:property value="connectedUsersCount" /></span> connectés (<span class="indicator"><s:property value="activeSessionsCount" /></span> sessions)
            </li>
          </ul>

          <div class="help-explanation-panel clear">
            <strong>Les chiffres ci-dessous (domaines, dispositifs et systèmes de culture) tiennent compte des campagnes. Une entité qui existe sur 3 campagnes, sera compatibilisé 3 fois.</strong>
          </div>
        </div>  
      </li>
      <s:if test="admin">
        <li>Gestion des utilisateurs
          <ul>
            <li><a href="<s:url namespace='/security' action='users-list' />">Liste des utilisateurs</a></li>
            <li><a href="<s:url namespace='/security' action='users-edit-input' />">Ajouter un utilisateur</a></li>
          </ul>
        </li>
        <li>Référentiels
          <ul>
            <s:iterator value="referentialClasses" var="klass" status="stat">
              <%--<s:if test="(#stat.count % 6) == 1">
                <ul class="">
              </s:if>--%>
              <li>
                <a href="<s:url namespace='/generic' action='generic-entities-list' />?genericClassName=<s:property value="key" />"><s:text name="%{key}"/></a>
                 - <s:property value="value" /> entité(s)
              </li>
              <%--s:if test="(#stat.count % 6) == 0">
                </ul>
              <s:if--%>
            </s:iterator>
          </ul>
        </li>
        <li>Listes d'autorité
          <ul>
            <s:iterator value="authorityClasses" var="klass">
              <li>
                <a href="<s:url namespace='/generic' action='generic-entities-list' />?genericClassName=<s:property value="key" />"><s:text name="%{key}"/></a>
                 - <s:property value="value" /> valeur(s)
              </li>
            </s:iterator>
          </ul>
        </li>
        <li>Outils
          <ul>
            <li>
              <a href="<s:url namespace='/admin' action='match-french-amm-for-european-products-input' />">Correspondances AMM France / UE</a>
            </li>
          </ul>
        </li>
        <li>Système
          <ul>
            <li><a href="<s:url namespace='/admin' action='tracked-events-list' />">Journaux de traçabilité</a></li>
            <li><a href="<s:url namespace='/admin' action='maintenance-mode-edit-input' />">Mode maintenance</a></li>
            <li><a href="<s:url namespace='/publishedmessage' action='info-message-edit-input' />">Diffusion d'un message</a></li>
            <li><a href="<s:url namespace='/publishedmessage' action='info-message-list' />">Liste des messages diffusés</a></li>
            <li><a href="<s:url namespace='/generic' action='generic-entities-list' />?genericClassName=fr.inra.agrosyst.api.entities.security.HashedValue"><s:text name="fr.inra.agrosyst.api.entities.security.HashedValue"/></a></li>
            <li><a href="<s:url namespace='/admin' action='show-configuration' />">Paramètres de configuration</a></li>
            <li><a href="<s:url namespace='/admin' action='admin-task' />">Gestion des tâches</a></li>
            <li><a href="<s:url namespace='/admin' action='admin-performances' />">Gestion des performances</a></li>
          </ul>
        </li>
      </s:if>
    </ul>

  </body>
</html>
