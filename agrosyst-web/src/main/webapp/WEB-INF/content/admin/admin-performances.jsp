<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2020 INRAE
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Statistiques des exports vers base de données des performances</title>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
     <content tag="current-category">contextual</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/admin.js'/><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript">
         angular.module('PerformanceStatisticsList', ['Agrosyst']).
             value('performanceStatistics', <s:property value="toJson(performanceStatistics)" escapeHtml="false"/>);

         function launchPerformance(performanceAdminForm) {
           displayPageLoading();
           performanceAdminForm.attr("action", ENDPOINTS.launchPerformance);
           performanceAdminForm.submit();
         };

         function stopAllDbTasks(performanceAdminForm, taskId) {
            performanceAdminForm.attr("action", ENDPOINTS.stopAllDbTasks);
            performanceAdminForm.submit();
         };
     </script>
  </head>
  <body>
    <div ng-app='PerformanceStatisticsList' ng-controller="PerformanceStatisticsListController">

      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Administration</li>
        </ul>
      </div>

      <s:if test="admin">
        <jsp:include page="admin-menu.jsp" />

        <form method="post" id="performanceAdminForm" name="domainListForm">
          <div>
           <input type="button" onclick="launchPerformance($('#performanceAdminForm'));return false;" value="Lancer une performance globale" class="btn btn-darker"/>
           <input type="button" onclick="stopAllDbTasks($('#performanceAdminForm'));return false;" value="Stopper la performance globale" class="btn btn-darker"/>
          </div>
        </form>

        <table class="data-table" id="task-list" summary="Ensemble des performance en masse">
          <thead>
            <tr>
                <th scope="col">Performance Id</th>
                <th scope="col">Début</th>
                <th scope="col">Dernier</th>
                <th scope="col">nb practiced</th>
                <th scope="col">nd effective</th>
                <th scope="col">%</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            <tr ng-show="performanceStatistics.length == 0"><td colspan="9" class="empty-table">Aucun export de performance</td></tr>
            <tr ng-repeat="stat in performanceStatistics | orderBy: 'stat.endAtTime'">
              <td>{{stat.performanceId}}</td>
              <td>{{stat.startAt | orDash}}</td>
              <td>{{stat.endAt | orDash}}</td>
              <td>{{stat.nbPracticedPracticed}}</td>
              <td>{{stat.nbEffectiveSdc}}</td>
              <td>{{stat.nbTotalTask !== 0 ? (100*stat.nbTaskPerformed/stat.nbTotalTask) : 0 | number:2}}</td>
              <td>{{stat.performanceState}}</td>
              <td><input type="button" ng-click="refresh(stat.performanceId)" value="rafraîchir" class="btn btn-darker"/></td>
              <td>
                <input title="Supprimer une performance"
                       ng-if="stat.performanceState !== 'GENERATING'"
                       type="button"
                       class="btn-icon icon-delete"
                       ng-click="delete(stat)" />
                <input ng-if="stat.performanceState === 'GENERATING'"
                       title="Il n'est pas possible de supprimer une performance en cours d'exécution"
                       type="button"
                       class="btn-icon icon-delete-disabled"
                       diseable/>
              </td>
            </tr>
          </tbody>
        </table>
      </s:if>
      <div id="confirmDeletePerformances" title="Suppression d'une performance" class="auto-hide">
        Êtes-vous sûr(e) de vouloir supprimer cette performance ?
      </div>
    </div>
  </body>
</html>
