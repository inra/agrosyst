<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>CONFIGURATION</title>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url namespace='/' action='index' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Administration</a></li>
        <li>&gt; Configuration</li>
      </ul>
    </div>

    <s:if test="currentUserAnAdmin">
      <jsp:include page="admin-menu.jsp" />

      <ul class="actions-button">
        <li title="Recharger le fichier de configuration" class="flex col ">
          <div class="button-icon">
            <a class="action-reload" href="<s:url namespace='/admin' action='reload-config' />"><i class="fa fa-refresh" aria-hidden="true"></i></a>
          </div>
          <div class="button-text">
            <a class="action-reload" href="<s:url namespace='/admin' action='reload-config' />">Recharger</a>
          </div>
        </li>
      </ul>

      <div>
        <table class="data-table fixe_layout" summary="Ensemble des paramètres de configuration">
          <thead>
            <tr>
                <th scope="col" class="column-xsmall">List #</th>
                <th scope="col" class="column-xlarge-fixed">Clef</th>
                <th scope="col" class="column-xxxlarge-fixed">Valeur</th>
            </tr>
          </thead>
          <tbody>
            <s:iterator value="configDisplay" status="stat">
              <s:iterator>
                <s:if test="#stat.index % 2 == 0">
                <tr>
                  <td class="column-xsmall withBackground" title="<s:property value='#stat.index'/>"><s:property value="#stat.index"/></td>
                  <td class="column-xlarge-fixed withBackground alignLeft" title="<s:property value='key'/>"><s:property value="key"/></td>
                  <td class="column-xxxlarge-fixed withBackground alignLeft" title="<s:property value='value'/>"><s:property value="value"/></td>
                </tr>
                </s:if>
                <s:else>
                <tr>
                  <td class="column-xsmall" title="<s:property value='#stat.index'/>"><s:property value="#stat.index"/></td>
                  <td class="column-xlarge-fixed alignLeft" title="<s:property value='key'/>"><s:property value="key"/></td>
                  <td class="column-xxxlarge-fixed alignLeft" title="<s:property value='value'/>"><s:property value="value"/></td>
                </tr>
                </s:else>
              </s:iterator>
            </s:iterator>
          </tbody>
        </table>
      </div>
    </s:if>
  </body>
</html>
