<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2023 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Correspondances AMM France / UE</title>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' /><s:property value='getVersionSuffix()'/>" />
  </head>
  <body>
    <div>
      <div>
        <div id="filAriane">
          <ul class="clearfix">
            <li><a href="<s:url namespace='/' action='index' />" class="icone-home">Accueil</a></li>
            <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Administration</a></li>

            <li>&gt; Correspondances AMM France / UE</li>
          </ul>
        </div>

        <jsp:include page="../admin/admin-menu.jsp" />

        <s:actionmessage/>
        <div class="admin-page">
          <h1>Correspondances AMM France / UE</h1>

          <form action="<s:url namespace='/admin' action='match-french-amm-for-european-products' />" method="post" enctype="multipart/form-data" onsubmit="setTimeout(() => { hidePageLoading() }, 10000);">
            <s:file name="inputFile" label="Fichier d'AMM européens" labelSeparator="&nbsp;:" required="true" requiredLabel="true"/>
            <input type="submit"/>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
