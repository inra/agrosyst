<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2020 INRAE
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Administration des tâches</title>
     <content tag="current-category">contextual</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/admin.js'/><s:property value='getVersionSuffix()'/>"></script>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
     <script type="text/javascript">
         angular.module('TasksList', ['Agrosyst']).
             value('ContextFilterInit', {}).
             value('ListTasksInitData', <s:property value="toJson(runningAndPendingTasks)" escapeHtml="false"/>).
             value('taskFilter', <s:property value="toJson(filter)" escapeHtml="false"/>);

         function stopTask(performanceAdminForm, taskId) {
            performanceAdminForm.attr("action", ENDPOINTS.stopTask + "?taskId=" + encodeURIComponent(taskId));
            performanceAdminForm.submit();
         };
     </script>
  </head>
  <body>
    <div ng-app='TasksList' ng-controller="TasksListController">

      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Administration</li>
        </ul>
      </div>

      <s:if test="admin">
        <jsp:include page="admin-menu.jsp" />

        <form method="post" id="taskStopForm"></form>

        <table class="data-table" id="task-list" summary="Ensemble des paramètres de configuration">
          <thead>
            <tr>
                <th scope="col">tâche Id</th>
                <th scope="col">Performance Id</th>
                <th scope="col">Email utilisateur</th>
                <th scope="col">Name</th>
                <th scope="col">Début</th>
                <th scope="col">Forcer l'arrêt</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-show="tasks.length == 0"><td colspan="8" class="empty-table">Aucune tâche</td></tr>
            <tr ng-repeat="scheduledTask in tasks">
              <td>{{scheduledTask.taskId}}</td>
              <td>{{scheduledTask.performanceId}}</td>
              <td>{{scheduledTask.userEmail}}</td>
              <td>{{scheduledTask.description}}</td>
              <td>{{scheduledTask.startedAt}}</td>
              <td><input type="button" ng-click="stopTask(scheduledTask.taskId)" value="Stopper" class="btn btn-darker"/></td>
            </tr>

          </tbody>
          <tfoot>
            <tr>
              <td class="entity-list-info" colspan="2">
                <span class="counter">{{pager.count}} tâches</span>
              </td>
              <td class="pagination" colspan="4">
                <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
              </td>
            </tr>
          </tfoot>
        </table>
      </s:if>
    </div>
  </body>
</html>
