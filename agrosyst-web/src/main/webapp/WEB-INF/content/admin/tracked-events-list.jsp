<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Journaux de traçabilité</title>
    <script type="text/javascript" src="<s:url value='/nuiton-js/tracker.js' /><s:property value='getVersionSuffix()'/>"></script>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' /><s:property value='getVersionSuffix()'/>" />
    <script type="text/javascript">
      angular.module('TrackerList', ['Agrosyst', 'ngSanitize'])
      .value('events', <s:property value="toJson(events)" escapeHtml="false"/>)
      .value('eventTypes', <s:property value="toJson(eventTypes)" escapeHtml="false"/>)
      .value('roleTypes', <s:property value="toJson(roleTypes)" escapeHtml="false"/>);
    </script>
  </head>
  <body>
    <div ng-app="TrackerList" ng-controller="TrackerListController">
      <div id="filAriane">
        <ul class="clearfix">
          <li>
            <a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a>
          </li>
          <li>&gt;
            <a href="<s:url action='home' namespace='/admin' />">Administration</a>
          </li>
          <li>&gt; Journaux de traçabilité</li>
        </ul>
      </div>

      <jsp:include page="admin-menu.jsp" />

      <!--<ul class="actions">-->
      <!--</ul>-->

      <ul class="float-right actions">
        <li><a class="action-export-csv button-disabled">Export CSV</a></li>
      </ul>

      <s:actionmessage/>
      <s:actionerror cssClass="send-toast-to-js"/>

      <form method="post" id="trackedEventListForm">
        <table class="entity-list clear" id="tracked-event-list-table" summary="Ensemble des évènements enregistrés">
          <thead>
            <tr>
              <th scope="col" class="column-large">Date</th>
              <th scope="col" class="column-large">Type</th>
              <th scope="col" class="column-large">Auteur</th>
              <th scope="col" class="column-large">Rôle(s)</th>
              <th scope="col">Évènement</th>
            </tr>
          </thead>

          <tbody>
            <tr ng-show="events.length == 0"><td colspan="5" class="empty-table">Aucun évènement enregistré</td></tr>
            <tr ng-repeat="event in events" >
              <td class="column-large">
                {{event.date|date:'d MMM yyyy'}}&nbsp;à&nbsp;{{event.date|date:'HH:mm:ss'}}
              </td>
              <td class="column-large">
                {{eventTypes[event.type]}}
              </td>
              <td class="column-large">
                {{event.author.firstName}} {{event.author.lastName}}
              </td>
              <td class="column-large">
                {{event.author.roles|displayableList:roleTypes}}
              </td>
              <td ng-bind-html="event.log"></td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <td class="entity-list-info" colspan="3">
                <span class="counter">{{pager.count}} évènements</span>
              </td>
              <td class="pagination" colspan="2">
                <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
              </td>
            </tr>
          </tfoot>
        </table>
      </form>

    </div>
  </body>
</html>
