<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<span class="wwlbl"><label for="targeted_zone_campaign">Campagnes cibles&nbsp;:</label></span>
<span class="wwctrl">
  <span ng-if="!copyPasteInterventions || !copyPasteInterventions.campaigns || copyPasteInterventions.campaigns.length == 0">Aucune campagne cible.</span>
  <span ng-if="copyPasteInterventions && copyPasteInterventions.campaigns && copyPasteInterventions.campaigns.length > 0">
    <select class="full-width" id="targeted_zone_campaign" ng-options="value for value in copyPasteInterventions.campaigns | orderBy:'-toString()'"
            ng-model="copyPasteInterventions.targetedZoneCampaigns"
            ng-change="computeAvailableZonesForCampaign()"
            multiple>
    </select>
  </span>
</span>

<div class="context-element-title paddingBottom30">
  <label>Cochez les cultures vers lesquelles vous souhaitez copier les interventions.</label>
</div>

<div class="copyPastZoneList">
  <table class="entity-list clear full-width" id="navigation-context-domains-list">
    <thead>
      <tr>
        <th scope="col"></th>
        <th scope="col">Cultures (Interventions)</th>
        <th scope="col">Zone</th>
        <th scope="col">Parcelle</th>
        <th scope="col">Surf.</th>
        <th scope="col">Domaine</th>
        <th scope="col">Dispositif</th>
        <th scope="col">Système de Culture</th>
        <th scope="col">Campagne</th>
      </tr>
    </thead>

    <tbody>
      <tr ng-show="copyPasteInterventions.availableZonesForCampaign.length == 0"><td colspan="9" class="empty-table">Aucun résultat ne correspond à votre recherche.</td></tr>
      <tr ng-repeat="zone in copyPasteInterventions.availableZonesForCampaign | orderBy: 'campaign':true" ng-click="copyPasteInterventions.selectedPhasesOrNodes[zone.phaseId||zone.nodeId] = !copyPasteInterventions.selectedPhasesOrNodes[zone.phaseId||zone.nodeId]"
          ng-class="{'line-selected':copyPasteInterventions.selectedPhasesOrNodes[zone.phaseId||zone.nodeId]}">
        <td>
          <input type='checkbox' ng-checked="copyPasteInterventions.selectedPhasesOrNodes[zone.phaseId||zone.nodeId]" />
        </td>
        <td>{{zone.nbIntermediateCropInterventions > 0 ?
          "CI (" + zone.nbIntermediateCropInterventions + ")": ""}}
          {{zone.croppingPlanEntryName}} ({{zone.nbInterventions}}) -
          {{zone.phaseType ? perennialPhaseTypes[zone.phaseType] : ('rang ' + (zone.rank + 1))}}
        </td>
        <td>{{zone.zoneName}}</td>
        <td>{{zone.plotName}}</td>
        <td>{{zone.plotArea}}</td>
        <td>{{zone.domainName}}</td>
        <td>{{zone.growingPlanName|orDash}}</td>
        <td>{{zone.growingSystemName|orDash}}</td>
        <td>{{zone.campaign}}</td>
      </tr>
    </tbody>

    <tfoot>
      <tr>
        <td class="entity-list-info" colspan="9">
          <span class="selection"><ng-pluralize count="(copyPasteInterventions.selectedPhasesOrNodes|toSelectedLength)" when="{'0': 'Aucune culture sélectionnée', '1': '{} sélectionnée', 'other': '{} sélectionnées'}" /></span>
          <span class="unSelectAllDomains" ng-if="(copyPasteInterventions.selectedPhasesOrNodes|toSelectedLength)>0"><a ng-click="copyPasteInterventions.selectedPhasesOrNodes={}"> - Désélectionner
            <ng-pluralize count="(copyPasteInterventions.selectedPhasesOrNodes|toSelectedLength)" when="{'0': '', '1': 'la culture', 'other': 'toutes les cultures'}" /></a></span>
        </td>
      </tr>
    </tfoot>
  </table>
</div>
