<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- itk/species-actions-and-inputs.jsp -->

<!-- Espèces de l'intervention -->
<div class="table-enclosure marginTop30 paddingTop0 clear">
  <label>
    <span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> <s:text name="itk-species-table-title" />
    <span class='contextual-help'>
      <span class='help-hover'>
        <s:text name="help.species.list" />
      </span>
    </span>
  </label>

  <table class="data-table inline-edition full-width" >
    <thead>
      <tr>
        <th scope="col" class="column-tiny"></th>
        <th scope="col"><s:text name="itk-species-table-species" /></th>
        <th scope="col" class="column-xlarge"><s:text name="itk-species-table-minStade" /></th>
        <th scope="col" class="column-xlarge"><s:text name="itk-species-table-maxStade" /></th>
        <th scope="col"><s:text name="itk-species-table-edition" /></th>
      </tr>
    </thead>
    <tbody id="StadesEdiBody">
      <tr ng-if="phaseConnectionOrNodeSpecies.length === 0 && ((editedIntervention.intermediateCrop && connectionIntermediateSpecies.length === 0) || !editedIntervention.intermediateCrop)" >
        <td colspan="5" class="empty-table"><s:text name="itk-species-table-empty" /></td>
      </tr>
      <tr ng-repeat="species in phaseConnectionOrNodeSpecies">
        <td>
          <input type="checkbox" ng-checked="speciesStadeBySpeciesCode[species.code]"
                 ng-click="selectInterventionSpecies(species, false)" ng-disabled="isSpeciesCheckboxDisabled(species.code)"/>
        </td>

        <td>
          <div class="flex row">
            <span ng-if="species.compagne" class="textIcones SPECIES_COMPAGNE"><s:text name='common-cropping-plan-mix-companion-abbr' /></span>
            <span class="grow">
              <span>{{ species.speciesEspece }}</span>
              <span ng-if="species.speciesQualifiant">{{ species.speciesQualifiant }} </span>
              <span ng-if="species.varietyLibelle">({{ species.varietyLibelle }}) </span>
              <span ng-if="!species.varietyLibelle && species.edaplosUnknownVariety" class="warning-label">
                (<i class="fa fa-warning" aria-hidden="true"></i> <s:text name="itk-species-table-unknownVariety" /> "{{ species.edaplosUnknownVariety }}")
              </span>
            </span>
          </div>
        </td>

        <td ng-if= "!editedSpeciesStades || editedSpeciesStades.speciesCode != species.code">
          {{speciesStadeBySpeciesCode[species.code].stadeMin.label|orDash}}
        </td>
        <td ng-if="editedSpeciesStades && editedSpeciesStades.speciesCode === species.code" ng-disabled="editedIntervention.intermediateCrop">
          <select ng-model="editedSpeciesStades.stadeMin"
                  ng-options="stade.label for stade in stadesList"
                  ng-change="speciesStadeMinSelected()">
            <option value=""></option>
          </select>
        </td>

        <td ng-if= "!editedSpeciesStades || editedSpeciesStades.speciesCode != species.code">
          {{speciesStadeBySpeciesCode[species.code].stadeMax.label|orDash}}
        </td>
        <td ng-if="editedSpeciesStades && editedSpeciesStades.speciesCode === species.code" ng-disabled="editedIntervention.intermediateCrop">
          <select ng-model="editedSpeciesStades.stadeMax"
                  ng-options="stade.label for stade in stadesList"
                  ng-change="speciesStadeMaxSelected()">
            <option value=""></option>
          </select>
        </td>

        <td ng-if= "editedIntervention.intermediateCrop">
          <input type="button" class="btn-icon icon-edit-disabled" value="Modification non autorisé" title="Modification non autorisé"
                 ng-disabled="true"/>
        </td>
        <td ng-if= "!editedIntervention.intermediateCrop && (!editedSpeciesStades || editedSpeciesStades.speciesCode != species.code)">
          <input type="button" class="btn-icon icon-edit" value="<s:text name='itk-input-table-modify' />" title="<s:text name='itk-input-table-modify' />"
                 ng-click="startEditSpeciesStades(species)"/>
        </td>
        <td ng-if="!editedIntervention.intermediateCrop && (editedSpeciesStades && editedSpeciesStades.speciesCode === species.code)">
          <input type="button" class="btn-icon icon-save" value="Valider" title="Valider"
                 ng-click="stopEditSpeciesStades(species)"/>
        </td>
      </tr>
      <tr ng-if="editedIntervention.intermediateCrop" ng-repeat="species in connectionIntermediateSpecies">
        <td>
          <input type="checkbox" ng-checked="speciesStadeBySpeciesCode[species.code]"
                           ng-click="selectInterventionSpecies(species, false)" ng-disabled="isIntermediateSpeciesCheckboxDisabled(species.code)"/>
        </td>
        <td>
          {{species.speciesEspece}} {{species.varietyLibelle ? '(' + species.varietyLibelle + ')': ''}}
        </td>
        <td class="empty-table" colspan="3">
          <s:text name="itk-species-table-unavailable" />
        </td>
      </tr>
    </tbody>
  </table>
</div>
<!-- Actions -->
<div class="table-enclosure marginTop30 paddingTop0 clear">
  <label><span class="required">*</span><s:text name="itk-actions-table-actions" /></label>
  <table id="actions" class="data-table inline-edition full-width" title="<s:text name='help.species.actions' />">
    <thead>
      <tr>
        <th scope="col" class="column-xlarge"><s:text name="itk-actions-table-action" /></th>
        <th scope="col" class="column-large"><s:text name="itk-actions-table-mainOrSecondary" /></th>
        <th scope="col" class="column-small"><s:text name="itk-actions-table-editDelete" /></th>
      </tr>
    </thead>
    <tbody id="ActionsBody">
      <tr ng-if="editedIntervention.actionDtos.length === 0" class='line-error'>
        <td colspan="3" class="empty-table">
          <s:text name="itk-actions-table-empty" />
          <input id="no_action_error" type="text" ng-model="noAction" required style="opacity:0;width: 0;"/>
        </td>
      </tr>
      <tr ng-repeat="interventionAction in editedIntervention.actionDtos | orderBy:mainActionSort"
          ng-class="{'selected-line' : editedAction == interventionAction, 'line-error': (editedAction == interventionAction ? false : validateAction(editedIntervention, interventionAction, true).length > 0)}">
        <td>
          <input ng-if="!interventionAction.isValid" id="interventionActionError_{{$index}}" type="text" ng-model="noAction" required=true style="opacity:0;width: 0;"/>
          {{agrosystInterventionTypes[interventionAction.mainAction.intervention_agrosyst]}} / {{interventionAction.mainActionReference_label}} <span ng-if="seedingActionMissingLotError && interventionAction.mainActionInterventionAgrosyst === 'SEMIS'"><b>{{seedingActionMissingLotError}} /!\</b></span>
        </td>
        <td ng-show="interventionAction.toolsCouplingCode">
          <span title="<s:text name="itk-actions-table-toolscoupling" />"><s:text name="itk-actions-table-main" /></span>
        </td>
        <td ng-show="!interventionAction.toolsCouplingCode" >
          <span title="<s:text name="itk-actions-table-notToolscoupling" />"><s:text name="itk-actions-table-catch" /></span>
        </td>
        <td>
          <input type="button" class="btn-icon icon-edit" value="<s:text name="itk-actions-table-modify" />" title="<s:text name="itk-actions-table-modify" />" ng-click="startEditAction(interventionAction)"/>
          <input type="button" class="btn-icon icon-delete" value="<s:text name="itk-actions-table-delete" />" title="<s:text name="itk-actions-table-delete" />" ng-click="deleteAction(interventionAction)"/>
        </td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="3">
          <div class="table-end-button">
            <input type="button" value="<s:text name="itk-actions-table-addAction" />" ng-click="startAddNewAction()" ng-disabled="(editedIntervention.availableActionTypes|toSelectedLength) === 0"/>
          </div>
        </td>
      </tr>
    </tfoot>
  </table>

  <!-- lightbox actions -->
  <div id="actions-edit-lightbox" title="<s:text name="itk-action-popup-title" />" class="slide-animation dialog-form auto-hide" style="overflow:auto;">
    <span ng-show="editedAction">
      <div class="wwgrp" ng-show="!editedAction.toolsCouplingCode && !editedAction.topiaId" >
        <span class="wwlbl"><label><span class="required">*</span>&nbsp;<s:text name="itk-action-popup-type" />&nbsp;:</label></span>
        <span class="wwctrl">
          <select
              id="edited-action-mains-actions-type"
              ng-model="editedAction.mainActionInterventionAgrosyst"
              ng-options="key as value for (key, value) in editedAction.availableActionTypes"
              ng-change="actionTypeSelected(editedAction, editedAction.mainActionInterventionAgrosyst)"
              ng-required="editedAction">
              <option value=""></option>
          </select>
        </span>
      </div>
      <div class="wwgrp">
        <span class="wwlbl"><label for="mainAction"><span class="required">*</span>&nbsp;<s:text name="itk-action-popup-action" />&nbsp;:</label></span>
        <span class="wwctrl" ng-show="!editedAction.toolsCouplingCode && !editedAction.topiaId">
          <select id="mainAction"
            ng-model="editedAction.mainActionId"
            ng-options="interventionAgrosystTravailEDI.topiaId as interventionAgrosystTravailEDI.reference_label_Translated for interventionAgrosystTravailEDI in editedAction.availableActions"
            ng-required="editedAction"
            ng-change="actionSelected(editedAction, editedAction.mainActionId)">
            <option value=""></option>
          </select>
        </span>
        <span ng-show="(editedAction.mainAction && editedAction.toolsCouplingCode) || (editedAction.mainAction && editedAction.topiaId)"
              class="wwctrl generated-content">
         {{editedAction.mainActionReference_label}}
        </span>
      </div>

      <!-- Even if ng-if are false, the JSP will be included. This is the expected behavior -->
      <s:iterator value="agrosystInterventionTypes">
        <div ng-if="editedAction.mainActionInterventionAgrosyst === '${key}'" class="horizontal-separator">
          <!-- Include JSP for type '${value}' : ../itk/actions/${key}.jsp -->
          <jsp:include page="../itk/actions/${key}.jsp" />
        </div>
      </s:iterator>
    </span>
  </div>

</div>

<!-- Intrants -->
<div class="table-enclosure marginTop30 paddingTop0 clear">
  <label><s:text name="itk-input-table-title" /></label>
  <table class="data-table inline-edition full-width">
    <thead>
      <tr>
        <th scope="col"><s:text name="itk-input-table-type" /></th>
        <th scope="col"><s:text name="itk-input-table-action" /></th>
        <th scope="col" class="column-xlarge"><s:text name="itk-input-table-product" /></th>
        <th scope="col"><s:text name="itk-input-table-quantity" /></th>
        <th scope="col" class="column-small"><s:text name="itk-input-table-unit" /></th>
        <th scope="col" class="column-small"><s:text name="itk-input-table-editDelete" /></th>
      </tr>
    </thead>
    <tbody>
      <tr ng-show="editedIntervention.actionDtos.length === 0" >
        <td colspan="6" class="empty-table"><s:text name="itk-input-table-noAction" /></td>
      </tr>
      <tr ng-show="editedIntervention.actionDtos.length > 0 && getUsages(editedIntervention).length === 0" >
        <td colspan="6" class="empty-table"><s:text name="itk-input-table-noInput" /></td>
      </tr>

      <!-- Usages - NewGeneration -->
      <tr ng-repeat="usage in getUsages(editedIntervention)"
          ng-class="{'selected-line' : editedUsage == usage, 'line-error': (editedUsage == usage ? false : !validateUsage(usage, editedIntervention, true))}">
        <td>
          <span>{{inputTypesLabels[usage.inputType]}}</span>
        </td>
        <td>
          <span>{{agrosystInterventionTypes[usage.mainActionInterventionAgrosyst]}} / {{usage.mainActionReferenceLabel}}</span>
        </td>
        <td class="column-xlarge">
          <input ng-if="!validateUsage(usage, editedIntervention, true, false)" id="interventionActionError_{{$index}}" type="text" ng-model="noUsage" required=true style="opacity:0;width: 0;"/>
          <span ng-if="usage.domainOtherProductInputDto">
            {{usage.domainOtherProductInputDto.inputName|orDash}}
          </span>
          <span ng-if="usage.domainMineralProductInputDto">
            {{usage.domainMineralProductInputDto.inputName|orDash}}
          </span>
          <span ng-if="usage.domainOrganicProductInputDto">
            {{usage.domainOrganicProductInputDto.inputName|orDash}}
          </span>
          <span ng-if="usage.domainPhytoProductInputDto">
            {{usage.domainPhytoProductInputDto.inputName|orDash}}
          </span>
          <span ng-if="usage.domainSubstrateInputDto">
            {{usage.domainSubstrateInputDto.inputName|orDash}}
          </span>
          <span ng-if="usage.domainPotInputDto">
            {{usage.domainPotInputDto.inputName|orDash}}
          </span>
          <span ng-if="usage.domainSeedLotInputDto">
            {{usage.domainSeedLotInputDto.inputName|orDash}}
          </span>
        </td>
        <td>
          <div ng-if="usage.seedingSpeciesDtos && usage.seedingSpeciesDtos.length > 0">
            <ul ng-if="!usage.showCompleteQuantityList" class="products-list">
              <li ng-repeat="species in usage.seedingSpeciesDtos | limitTo:3">
                {{species.productName}}&nbsp;:&nbsp;{{species.qtAvg|orDash}}
                <span ng-if="species.qtMin">(<s:text name="itk-input-table-min" />&nbsp;:&nbsp;{{species.qtMin}})</span>
                <span ng-if="species.qtMax">(<s:text name="itk-input-table-max" />&nbsp;:&nbsp;{{species.qtMax}})</span>
                <span ng-if="species.qtMed">(<s:text name="itk-input-table-median" />&nbsp;:&nbsp;{{species.qtMed}})</span>
              </li>
            </ul>
            <ul ng-if="usage.showCompleteQuantityList" class="products-list">
              {{species.productName}}&nbsp;:&nbsp;{{species.qtAvg|orDash}}
              <span ng-if="species.qtMin">(<s:text name="itk-input-table-min" />&nbsp;:&nbsp;{{species.qtMin}})</span>
              <span ng-if="species.qtMax">(<s:text name="itk-input-table-max" />&nbsp;:&nbsp;{{species.qtMax}})</span>
              <span ng-if="species.qtMed">(<s:text name="itk-input-table-median" />&nbsp;:&nbsp;{{species.qtMed}})</span>
            </ul>
            <span ng-if="!usage.showCompleteQuantityList && usage.seedingSpeciesDtos.length > 3">{{usage.seedingSpeciesDtos.length - 3}} espèces(s) supplémentaire(s)</span>
          </div>
          <div ng-if="!usage.seedingSpeciesDtos">
            {{usage.qtAvg|orDash}}
            <span ng-if="usage.qtMin">(<s:text name="itk-input-table-min" />&nbsp;:&nbsp;{{usage.qtMin}})</span>
            <span ng-if="usage.qtMax">(<s:text name="itk-input-table-max" />&nbsp;:&nbsp;{{usage.qtMax}})</span>
            <span ng-if="usage.qtMed">(<s:text name="itk-input-table-median" />&nbsp;:&nbsp;{{usage.qtMed}})</span>
          </div>
        </td>
        <td class="column-small">
          <span ng-if="usage.domainOtherProductInputDto">
            {{i18n.OtherProductInputUnit[usage.domainOtherProductInputDto.usageUnit]}}
          </span>
          <span ng-if="usage.domainMineralProductInputDto">
            {{i18n.MineralProductUnit[usage.domainMineralProductInputDto.usageUnit]}}
          </span>
          <span ng-if="usage.domainOrganicProductInputDto">
            {{i18n.OrganicProductUnit[usage.domainOrganicProductInputDto.usageUnit]}}
          </span>
          <span ng-if="usage.domainPhytoProductInputDto">
            {{i18n.PhytoProductUnit[usage.domainPhytoProductInputDto.usageUnit]}}
          </span>
          <span ng-if="usage.domainSubstrateInputDto">
            {{i18n.SubstrateInputUnit[usage.domainSubstrateInputDto.usageUnit]}}
          </span>
          <span ng-if="usage.domainPotInputDto">
            {{i18n.PotInputUnit[usage.domainPotInputDto.usageUnit]}}
          </span>
          <span ng-if="usage.domainSeedLotInputDto">
            {{i18n.SeedPlantUnit[usage.domainSeedLotInputDto.usageUnit]}}
          </span>
          <span ng-if="!usage.domainOtherProductInputDto && !usage.domainMineralProductInputDto && !usage.domainOrganicProductInputDto && !usage.domainPhytoProductInputDto && !usage.domainSubstrateInputDto && !usage.domainPotInputDto && !usage.domainSeedLotInputDto">
            -
          </span>
        </td>
        <td>
          <input type="button" class="btn-icon icon-edit" value="<s:text name="itk-input-table-modify" />" title="<s:text name="itk-input-table-modify" />" ng-click="startEditUsage(usage)"/>
          <input type="button" class="btn-icon icon-delete" value="<s:text name="itk-input-table-delete" />" title="<s:text name="itk-input-table-delete" />" ng-click="deleteUsage(usage)"/>
        </td>
      </tr>

    </tbody>
    <tfoot>
      <tr>
        <td colspan="6">
          <div class="table-end-button" ng-repeat="inputType in editedIntervention.availableInputTypes">
            <input type="button" ng-class="{'button-warning':(seedingActionMissingLotError && inputType === 'SEMIS')}" value="{{inputTypesLabels[inputType]}}" ng-click="startAddNewUsage(inputType)"/>
          </div>
        </td>
      </tr>
    </tfoot>
  </table>


  <!-- lightbox intrants -->
  <div id="usage-edit-lightbox" title="<s:text name="itk-input-popup-title" />" class="slide-animation dialog-form auto-hide" style="overflow:auto;">
    <span ng-if="inputTypesLabels && editedUsage">
      <div class="wwgrp">
        <span class="wwlbl"><label><s:text name="itk-input-popup-type" />&nbsp;:</label></span>
        <span class="wwctrl generated-content">{{inputTypesLabels[editedUsage.inputType]}}</span>
      </div>

      <div ng-if="editedUsage.inputType === 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'" class="horizontal-separator"><jsp:include page="../itk/inputs/mineral-product-input.jsp" /></div>
      <div ng-if="editedUsage.inputType === 'EPANDAGES_ORGANIQUES'" class="horizontal-separator"><jsp:include page="../itk/inputs/organic-product-input.jsp" /></div>
      <div ng-if="editedUsage.inputType === 'AUTRE'" class="horizontal-separator"><jsp:include page="../itk/inputs/other-product-input.jsp" /></div>
      <div ng-if="editedUsage.inputType === 'LUTTE_BIOLOGIQUE' || editedUsage.inputType === 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'" class="horizontal-separator"><jsp:include page="../itk/inputs/phyto-product-input.jsp" /></div>
      <div ng-if="editedUsage.inputType === 'SUBSTRAT'" class="horizontal-separator"><jsp:include page="../itk/inputs/substrate-input.jsp" /></div>
      <div ng-if="editedUsage.inputType === 'POT'" class="horizontal-separator"><jsp:include page="../itk/inputs/pot-input.jsp" /></div>
      <div ng-if="editedUsage.inputType === 'SEMIS' || editedUsage.inputType === 'PLAN_COMPAGNE'" class="horizontal-separator"><jsp:include page="../itk/inputs/semis-input.jsp" /></div>
    </span>
  </div>
</div>

<div id="confirmRemovedUsage" title="<s:text name="itk-input-delete-confirm-title" />" class="auto-hide">
  <s:text name="itk-input-delete-confirm-message" />
</div>

<div id="confirmRemovedAction" title="<s:text name="itk-action-delete-confirm-title" />" class="auto-hide">
  <span ng-if="agrosystInterventionTypes && deletedActionType && inputTypesLabels">
    <s:text name="itk-action-delete-confirm-message" />&nbsp;:&nbsp;<span class="font-bold">{{agrosystInterventionTypes[deletedActionType]}}</span> ?
    <span ng-if="inputTypesLabels[deletedActionType]"><br/><br/><s:text name="itk-action-delete-confirm-help" /> : "<span class="font-bold">{{inputTypesLabels[deletedActionType]}}</span>"</span>
  </span>
</div>

<div id="nonValidAction" title="<s:text name="itk-action-invalid-title" />" class="auto-hide">
  <span ng-if="editedAction" ng-bind-html="editedAction.errorMessage"></span>
</div>

<div id="nonValidUsage" title="<s:text name="itk-input-invalid-title" />" class="auto-hide">
  <span ng-if="editedUsage">
    {{editedUsage.errorMessage}}
  </span>
</div>
