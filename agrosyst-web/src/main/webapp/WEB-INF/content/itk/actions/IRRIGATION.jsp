<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- itk/actions/IRRIGATION.jsp -->

<div class="wwgrp">
  <span class="wwlbl">
    <label class="fields-group"><s:text name="itk-action-irrigation-waterQuantity" />&nbsp;</label>
  </span>
  <div class="wwgrp">
      <span class="wwlbl">
          <label for="editedActionWaterwaterQuantityMin"><span class="required">*</span> <s:text name="itk-action-irrigation-waterQuantity-average" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedActionWaterwaterQuantityMin" type="text" ng-model="editedAction.waterQuantityAverage" ag-float-positive pattern="^\d*[\.,]?\d*$" required />
          <span class="add-on">mm</span>
        </span>
      </span>
  </div>
  <div class="wwgrp">
    <div class="wwctrl">
      <a class="btn" ng-click="showQtyMinMediumMax = !showQtyMinMediumMax"
                     ng-show="!showQtyMinMediumMax && !editedAction.waterwaterQuantityMin && !editedAction.waterQuantityMedian && !editedAction.waterQuantityMax">
        <s:text name="itk-action-irrigation-waterQuantity-show" />
      </a>
    </div>
  </div>
  <div ng-show="showQtyMinMediumMax || editedAction.waterQuantityMin || editedAction.waterQuantityMedian || editedAction.waterQuantityMax"
       class="slide-animation">
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedActionWaterQuantityMin"><s:text name="itk-action-irrigation-waterQuantity-minimum" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedActionWaterQuantityMin" type="text" ng-model="editedAction.waterQuantityMin" ag-float-positive pattern="^\d*[\.,]?\d*$" />
          <span class="add-on">mm</span>
        </span>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedActionWaterQuantityMax"><s:text name="itk-action-irrigation-waterQuantity-maximum" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedActionWaterQuantityMax" type="text" ng-model="editedAction.waterQuantityMax" ag-float-positive pattern="^\d*[\.,]?\d*$" />
          <span class="add-on">mm</span>
        </span>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedActionWaterQuantityMedian"><s:text name="itk-action-irrigation-waterQuantity-median" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedActionWaterQuantityMedian" type="text" ng-model="editedAction.waterQuantityMedian" ag-float-positive pattern="^\d*[\.,]?\d*$" />
          <span class="add-on">mm</span>
        </span>
      </span>
    </div>
  </div>
</div>

<div class="wwgrp">
  <span class="wwlbl"><label for="irrigation-azoteQuantity"><s:text name="itk-action-irrigation-azoteQuantity" />&nbsp;:</label></span>
  <span class="wwctrl">
    <span class='contextual-help'>
      <span class='help-hover'>
        <s:text name="help.irrigation.azoteQuantity" />
      </span>
    </span>
    <span class="input-append">
      <input type="text" id="irrigation-azoteQuantity"
             ng-model="editedAction.azoteQuantity"
             placeholder="<s:text name="itk-action-irrigation-azoteQuantity-placeholder" />"
             ag-integer pattern="\d+" />
      <span class="add-on">kg/ha</span>
    </span>
  </span>
</div>

<!-- Removed price editedAction.irrigationAction.cost -->

<div class="wwgrp horizontal-separator">
  <span class="wwlbl"><label for="irrigation-comment"><s:text name="common-comment" />&nbsp;:</label></span>
  <span class="wwctrl"><textarea id="irrigation-comment" ng-model="editedAction.comment"></textarea></span>
</div>
