<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<!-- itk/actions/APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.jsp -->

<div class="wwgrp">
  <span class="wwlbl"><label for="ferti-burial"><s:text name="itk-action-ferti-burial" />&nbsp;:</label></span>
  <span class="wwctrl"><input type="checkbox" id="ferti-burial" ng-model="editedAction.burial" /></span>
</div>

<div class="wwgrp">
  <span class="wwlbl"><label for="ferti-localizedSpreading"><s:text name="itk-action-ferti-localizedSpreading" />&nbsp;:</label></span>
  <span class="wwctrl"><input type="checkbox" id="ferti-localizedSpreading" ng-model="editedAction.localizedSpreading"/>
  </span>
</div>

<div class="wwgrp horizontal-separator">
  <span class="wwlbl"><label for="ferti-comment"><s:text name="common-comment"/>&nbsp;:</label></span>
  <span class="wwctrl"><textarea id="ferti-comment" ng-model="editedAction.comment"></textarea></span>
</div>
