<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<!-- itk/actions/TRANSPORT.jsp -->

<div class="wwgrp">
  <span class="wwlbl"><label for="carriage-action-loadCapacity"><s:text name="itk-action-carriage-action-loadCapacity" />&nbsp;:</label></span>
  <div class="wwctrl">
    <span class="two-columns">
      <input type="text" id="carriage-action-loadCapacity" ng-model="editedAction.loadCapacity"
             placeholder="<s:text name="itk-action-carriage-action-loadCapacity-placeholder" />" />
    </span>
    <span class="two-columns">
      <span class="wwlbl"><label for="carriage-action-loadCapacityUnit"><s:text name="itk-action-carriage-action-loadCapacityUnit" />&nbsp;:</label></span>
      <select id="carriage-action-loadCapacityUnit" ng-model="editedAction.capacityUnit"
              ng-options="key as value for (key, value) in editedAction.capacityUnits" class="width50percent">
      </select>
    </span>
  </div>
</div>

<div class="wwgrp">
  <span class="wwlbl"><label for="carriage-action-tripFrequency"><s:text name="itk-action-carriage-action-tripFrequency" />&nbsp;:</label></span>
  <span class="wwctrl"><input type="text" id="carriage-action-tripFrequency" ng-model="editedAction.tripFrequency"
                              placeholder="<s:text name="itk-action-carriage-action-tripFrequency-placeholder" />" ag-float-positive pattern="^\d*[\.,]?\d*$"/></span>
</div>


<div class="wwgrp horizontal-separator">
  <span class="wwlbl"><label for="carriage-action-comment"><s:text name="common-comment" />&nbsp;:</label></span>
  <span class="wwctrl"><textarea id="carriage-action-comment" ng-model="editedAction.comment"></textarea></span>
</div>
