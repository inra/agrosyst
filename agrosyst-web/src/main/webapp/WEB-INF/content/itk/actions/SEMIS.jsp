<%--
    #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
    --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!-- itk/actions/SEMIS.jsp -->

<div class="wwgrp">
  <span class="wwlbl">
    <label for="seeding-yealdTarget"><span class="requiredAdvised" title='<s:text name="help.dephy.advised"/>'>&pi;</span> <s:text name="itk-action-seeding-yealdTarget" />&nbsp;:</label>
  </span>
  <div class="wwctrl clearfix">
    <span class="two-columns">
       <input type="text" id="seeding-yealdTarget" ng-model="editedAction.yealdTarget" ag-float-positive pattern="^\d*[\.,]?\d*$" required />
    </span>
    <span class="two-columns">
      <span class="wwlbl"><label><s:text name="itk-action-seeding-yealdUnit" />&nbsp;:</label></span>
      <select id="seeding-seedingActionDto-yealdUnit" ng-model="editedAction.yealdUnit"
              ng-options="key as value for (key, value) in editedAction.yealdUnits"
              class="width50percent"></select>
    </span>
  </div>
</div>

<div class="wwgrp horizontal-separator">
  <span class="wwlbl"><label for="seeding-action-comment"><s:text name="common-comment" />&nbsp;:</label></span>
  <span class="wwctrl"><textarea id="seeding-action-comment" ng-model="editedAction.comment"></textarea></span>
</div>

<div id="confirmRemovedProduct" title="<s:text name="itk-action-seeding-deleteProduct-title" />" class="auto-hide">
  <s:text name="itk-action-seeding-deleteProduct-message" />&nbsp;:{{productName}} ?
</div>
