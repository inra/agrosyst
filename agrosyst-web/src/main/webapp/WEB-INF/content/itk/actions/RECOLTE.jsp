<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2024 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<!-- itk/actions/RECOLTE.jsp -->
<div ng-if="editedAction.mainAction">

  <switch name="isOrganicCrop"
          ng-model="editedAction.isOrganicCrop"
          struts="true"
          label="<s:text name='itk-action-harvest-isOrganicCrop' />&nbsp;:"
          disabled="editedAction.sector && editedAction.organicGsDefined"
          ng-change="setOrganicValorisation(editedAction)"
          help='{{editedAction.sector && editedAction.organicGsDefined ? "<s:text name='itk-action-harvest-isOrganicCrop-help-organicGsDefined' />" : "<s:text name='itk-action-harvest-isOrganicCrop-help' />"}}'
          >
  </switch>

  <div ng-if="editedAction.isWineSpecies">
    <div class="wwgrp">
      <span class="wwlbl">
        <label><span class="required">*</span> <s:text name="itk-action-harvest-wineValorisation" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <table>
          <tbody>
            <tr ng-repeat='(key, value) in wineValorisations'>
              <td><input type='checkbox'
                ng-model="editedAction.selectedWineValorisations[key]"
                ng-checked="editedAction.selectedWineValorisations[key] === true"
                ng-change="selectWineValorisation(editedAction, key)"
                ng-disabled="editedValorisation"
                />
                {{value}}
              </td>
            </tr>
          </tbody>
        </table>
      </span>
    </div>
  </div>

  <div ng-if="editedAction.mainActionReference_code === 'W25'">
    <div class="wwgrp">
      <span class="wwlbl">
        <label><s:text name="itk-action-harvest-cattle" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <select id="cattle" name="cattle"
                ng-model="editedAction.cattle"
                ng-options="cattle as cattle.label for cattle in cattles track by cattle.code">
          <option value=""></option>
        </select>
      </span>
    </div>

    <div class="wwgrp">
      <span class="wwlbl">
        <label><s:text name="itk-action-harvest-pastureLoad" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <input type="text" id="pastureLoad" name="pastureLoad"
               placeholder="<s:text name='itk-action-harvest-pastureLoad-placeholder' />"
               ng-model="editedAction.pastureLoad"
               ag-integer pattern="\d+"
        />
      </span>
    </div>

    <div class="wwgrp">
      <span class="wwlbl">
        <label><s:text name="itk-action-harvest-pastureType" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <select id="pastureType" name="pastureType"
          ng-model="editedAction.pastureType"
          ng-options="key as value for (key , value) in pastureTypes">
          <option value=""></option>
        </select>
      </span>
    </div>

    <switch name="pasturingAtNight"
            ng-model="editedAction.pasturingAtNight"
            struts="true"
            label="<s:text name="itk-action-harvest-pasturingAtNight" />&nbsp;:"
            >
    </switch>
  </div>

  <div class="flex row">
    <div class="flex"
         ng-class="{ 'row grow': !displayValorisationsBySpecies(editedAction),
                     'col half': displayValorisationsBySpecies(editedAction) }">
      <div ng-if="!editedAction.nbSpeciesStadeSpeciesLength"
           class="flex row half">
        <div class="warning">
          <i class="fa fa-warning" aria-hidden="true"></i>
          <s:text name="itk-action-harvest-noSpecies-warning" />
        </div>
      </div>
      <div ng-if="editedAction.nbSpeciesStadeSpeciesLength && !areDestinationsForSpecies(editedAction)"
           class="flex row half">
        <div class="warning">
          <i class="fa fa-warning" aria-hidden="true"></i>
          <s:text name="itk-action-harvest-noDestination-warning" />
          <span ng-if="editedAction.isWineSpecies">
            <s:text name="itk-action-harvest-noDestination-wine-warning" />
          </span>
        </div>
      </div>
      <div ng-if="areDestinationsForSpecies(editedAction) && editedAction.nbSpeciesStadeSpeciesLength"
           class="half">
        <div class="flex row">
          <div class="column-header grow"><span class="required">*</span> <s:text name="itk-action-harvest-yields" /></div>
        </div>
        <div class="rendements">

          <div class="culture-header flex row">
            <div class="culture textIcones {{editedAction.cropType}}"
                 title="{{ getTitleForEditedAction() }}">
              <span ng-if="editedAction.cropType == 'MAIN'"> <s:text name="common-cropping-plan-main-abbr" /> </span>
              <span ng-if="editedAction.cropType == 'CATCH'"> <s:text name="common-cropping-plan-catch-abbr" /> </span>
              <span ng-if="editedAction.cropType == 'INTERMEDIATE'"> <s:text name="common-cropping-plan-intermediate-abbr" /> </span>

              <span ng-if="editedAction.isMixSpecies" class="textIcones MIX_S"><s:text name='common-cropping-plan-mix-species-abbr' /></span>
              <span ng-if="editedAction.isMixVariety" class="textIcones MIX_V"><s:text name='common-cropping-plan-mix-varieties-abbr' /></span>
              <span ng-if="editedAction.isMixCompanion" class="textIcones MIX_C"><s:text name='common-cropping-plan-mix-companion-abbr'/></span>
            </div>
            <div class="culture"><s:text name='itk-action-harvest-crop' />&nbsp;: <span ng-bind-html="editedAction.displayName" class="font-bold"></span></div>
            <div class="grow alignRight">
              <button ng-click="addDestination(editedAction)"
                      ng-disabled="!hasInterventionValidDates(editedIntervention) || !isEditedValorisationValid(editedAction, editedValorisation)"
                      class="btn-fa dark-blue negative"
                      title="<s:text name='itk-action-harvest-addDestination' />">
                <i class="fa fa-plus" aria-hidden="true"></i>
              </button>
            </div>
          </div>
          <div class="rendement warning"
               ng-if="!hasInterventionValidDates(editedIntervention)">
            <i class="fa fa-warning" aria-hidden="true"></i>
            <s:text name='itk-action-harvest-missingDates' />
          </div>
          <div class="rendement empty"
               ng-if="hasInterventionValidDates(editedIntervention) && !hasMainValorisations(editedAction)">
            <s:text name='itk-action-harvest-emptyYields' /><span ng-if="editedAction.destinationsLength && editedAction.nbSpeciesStadeSpeciesLength > 0"><s:text name='itk-action-harvest-emptyYields-add' /></span>
          </div>
          <div class="rendement" ng-repeat="valorisation in editedAction.mainValorisations">
            <div class="flex row">
              <div>
                <span class="label"><s:text name="itk-action-harvest-destinationChoice" /></span>
                <span ng-if="editedValorisation != valorisation">
                  {{ valorisation.destinationName }}
                </span>
                <select ng-if="editedValorisation === valorisation"
                        ng-model="editedValorisation.selectedDestinations"
                        ng-options="key disable when isDisabledDestination(key, editedAction, editedValorisation) for (key, value) in editedAction.destinationsByDestinationLabels"
                        ng-change="setValorisationDestination(editedValorisation, editedAction)">
                </select>
              </div>
              <div class="grow alignRight">
                <button ng-if="editedValorisation != valorisation"
                        ng-disabled="!isEditedValorisationValid(editedAction, editedValorisation)"
                        ng-click="editValorisation(valorisation, editedAction)"
                        class="btn-fa blue"
                        title="<s:text name='common-modify' />">
                  <i class="fa fa-pencil" aria-hidden="true"></i>
                </button>
                <button ng-if="editedValorisation == valorisation"
                        ng-click="stopEditMainValorisation(editedAction, editedValorisation)"
                        ng-disabled="!isMainValorisationValid(editedAction, valorisation)"
                        class="btn-fa red"
                        title="<s:text name='common-save' />">
                  <i class="fa fa-floppy-o" aria-hidden="true"></i>
                </button>
                <button ng-click="deleteDestination(valorisation, editedAction)"
                        ng-disabled="getMainValorisationNb(editedAction) == 1 || (editedValorisation != valorisation && !isEditedValorisationValid(editedAction, editedValorisation))"
                        class="btn-fa blue"
                        title="<s:text name='common-delete' />">
                  <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
              </div>
            </div>

            <div class="flex row" ng-if="valorisation.selectedDestinations">
              <div class="flex col two-thirds">
                <div class="label"><s:text name='itk-action-harvest-yield' />&nbsp;:</div>
                <div class="list-item">
                  <span><s:text name='itk-action-harvest-total' /></span>&nbsp;:&nbsp;
                  <span ng-if="editedValorisation != valorisation">
                    {{ valorisation.yealdAverage | number:1 | orDash }}&nbsp;{{editedAction.yealdUnits[valorisation.yealdUnit]}}
                  </span>
                  <span ng-if="editedValorisation == valorisation">
                    <input type="text"
                           ng-model="editedValorisation.yealdAverageFixedFloat"
                           size="3"
                           ag-float-strictly-positive />
                    &nbsp;
                    <select ng-model="editedValorisation.yealdUnit"
                            ng-options="key as editedAction.yealdUnits[key] for (key, value) in valorisation.destinationsYealdUnits"
                            ng-change="setDestination(editedValorisation, editedAction)">
                    </select>
                  </span>
                </div>
                <div class="required" ng-if="editedValorisation == valorisation && !valorisation.yealdAverageFixedFloat">
                  <s:text name='itk-action-harvest-negativeYield' />
                </div>
                <div>
                  <a ng-click="valorisation._showYealdMinMediumMax = true"
                     ng-if="!valorisation._showYealdMinMediumMax && (editedValorisation == valorisation || valorisation.yealdMin || valorisation.yealdMax || valorisation.yealdMedian)">
                      {{editedValorisation == valorisation ? "<s:text name='itk-action-harvest-editAllYields' />" : "<s:text name='itk-action-harvest-showAllYields' />" }}
                  </a>
                </div>
                <div ng-if="editedValorisation != valorisation && valorisation._showYealdMinMediumMax"
                     class="slide-animation nowrap">
                  <div class="list-item">
                    <span class="rendement-label"><s:text name='itk-action-harvest-yield-min' /></span>&nbsp;:&nbsp;
                    <span>{{valorisation.yealdMin| number:1 | orDash}}</span>&nbsp;
                    <span>{{editedAction.yealdUnits[valorisation.yealdUnit]}}</span>
                  </div>
                  <div class="list-item">
                    <span class="rendement-label"><s:text name='itk-action-harvest-yield-median' /></span>&nbsp;:&nbsp;
                    <span>{{valorisation.yealdMedian | number:1 | orDash}}</span>&nbsp;
                    <span>{{editedAction.yealdUnits[valorisation.yealdUnit]}}</span>
                  </div>
                  <div class="list-item">
                    <span class="rendement-label"><s:text name='itk-action-harvest-yield-max' /></span>&nbsp;:&nbsp;
                    <span>{{valorisation.yealdMax| number:1 | orDash}}</span>&nbsp;
                    <span>{{editedAction.yealdUnits[valorisation.yealdUnit]}}</span>
                  </div>
                </div>
                <div ng-if="editedValorisation == valorisation && (valorisation._showYealdMinMediumMax || valorisation.yealdMin || valorisation.yealdMax || valorisation.yealdMedian)"
                     class="slide-animation nowrap">
                  <div class="list-item">
                    <span class="rendement-label"><s:text name='itk-action-harvest-yield-min' /></span>&nbsp;:&nbsp;
                    <input type="text" ng-model="valorisation.yealdMin" size="3" ag-float-strictly-positive />&nbsp;
                    <span>{{editedAction.yealdUnits[valorisation.yealdUnit]}}</span>
                  </div>
                  <div class="list-item">
                    <span class="rendement-label"><s:text name='itk-action-harvest-yield-median' /></span>&nbsp;:&nbsp;
                    <input type="text" ng-model="valorisation.yealdMedian" size="3" ag-float-strictly-positive />&nbsp;
                    <span>{{editedAction.yealdUnits[valorisation.yealdUnit]}}</span>
                  </div>
                  <div class="list-item">
                    <span class="rendement-label"><s:text name='itk-action-harvest-yield-max' /></span>&nbsp;:&nbsp;
                    <input type="text" ng-model="valorisation.yealdMax" size="3" ag-float-strictly-positive />&nbsp;
                    <span>{{editedAction.yealdUnits[valorisation.yealdUnit]}}</span>
                  </div>
                </div>
              </div>

              <div class="flex col one-third">
                <div class="label"><s:text name='itk-action-harvest-valorisation' /></div>
                <div ng-if="editedValorisation != valorisation || !editedValorisation.changeMainValorisationTargetAvailable">
                  <div ng-if="valorisation.salesPercent" class="list-item">
                    {{valorisation.salesPercent}} % <s:text name='itk-action-harvest-salesPercent' />
                  </div>
                  <div ng-if="valorisation.selfConsumedPersent" class="list-item">
                    {{valorisation.selfConsumedPersent}} % <s:text name='itk-action-harvest-selfConsumedPersent' />
                  </div>
                  <div ng-if="valorisation.noValorisationPercent" class="list-item">
                    {{valorisation.noValorisationPercent}} % <s:text name='itk-action-harvest-noValorisationPercent' />
                  </div>
                </div>
                <div ng-if="editedValorisation == valorisation && editedValorisation.changeMainValorisationTargetAvailable">
                  <div class="list-item">
                    <input type="text"
                           ng-model="valorisation.salesPercent"
                           size="3"
                           ag-sales />
                    % <s:text name='itk-action-harvest-salesPercent' />
                  </div>
                  <div class="list-item">
                    <input type="text"
                           ng-model="valorisation.selfConsumedPersent"
                           size="3"
                           ag-self-consumed />
                     % <s:text name='itk-action-harvest-selfConsumedPersent' />
                  </div>
                  <div class="list-item">
                    <input type="text"
                           ng-model="valorisation.noValorisationPercent"
                           size="3"
                           ag-no-valorisation />
                     % <s:text name='itk-action-harvest-noValorisationPercent' />
                  </div>
                  <div class="required" ng-if="!valorisation._isValidValorisationPercent">
                    <s:text name='itk-action-harvest-valorisation-sum-error' />
                  </div>
                </div>
              </div>
            </div>

            <div class="col" ng-if="valorisation.destination &&
                                   (valorisation.qualityCriteriaDtos.length && (!hasSeveralSpecies(editedAction) || editedAction.isMixVariety) ||
                                    editedValorisation == valorisation && (!hasSeveralSpecies(editedAction) || editedAction.isMixVariety))">
              <div class="label"><s:text name='itk-action-harvest-qualityCriteria' /></div>
              <div ng-repeat="qualityCriteria in valorisation.qualityCriteriaDtos"
                   ng-if="valorisation.qualityCriteriaDtos.length && (editedValorisation != valorisation || hasSeveralSpecies(editedAction))"
                   class="list-item">
                  <!-- nom du critère de qualité-->

                  {{qualityCriteria.refQualityCriteria.qualityCriteriaLabel_Translated}}&nbsp;:&nbsp;

                  <!-- valeur plus unité -->

                  <span ng-bind-html="getQualityCriteriaValue(editedAction, qualityCriteria)"></span>
              </div>

              <!-- édition d un critère de qualité associé à une espèce -->
              <div class="col" ng-if="editedValorisation == valorisation && (!hasSeveralSpecies(editedAction) || editedAction.isMixVariety)">
                <!-- Aucun critère de qualité disponible -->
                <div ng-if="valorisation.destination &&
                             valorisation.nbQualityCriteriaForValorisation === 0 &&
                             valorisation.qualityCriteriaDtos.length === 0">
                  <s:text name='itk-action-harvest-noCriteria' />
                </div>

                <!-- Affichage des critères de qualité déjà sélectionnés-->
                <div ng-repeat="qualityCriteria in valorisation.qualityCriteriaDtos" class="list-item">
                  {{qualityCriteria.refQualityCriteria.qualityCriteriaLabel_Translated}}&nbsp;:&nbsp;

                  <!-- valeur plus unité -->
                  <span ng-bind-html="getQualityCriteriaValue(editedAction, qualityCriteria)"></span>

                  <button ng-click="removeQualityCriteria(editedAction, editedValorisation, qualityCriteria)"
                          class="btn-fa btn-small blue"
                          title="<s:text name='common-delete' />">
                    <i class="fa fa-minus" aria-hidden="true"></i>
                  </button>
                </div>

                <div class="flex row criteria-edition">
                  <!-- choix du critère de qualité-->
                  <select ng-if="valorisation.destination && valorisation.nbQualityCriteriaForValorisation > 0"
                          style="width: 70%;"
                          ng-model="valorisation.selectedQualityCriteria"
                          ng-options="qualityCrit.qualityCriteriaLabel_Translated for (id, qualityCrit) in valorisation.allQualityCriteriaForValorisation">
                  </select>

                  <div ng-if="valorisation.destination && valorisation.nbQualityCriteriaForValorisation > 0">
                    <!-- critère binaire -->
                    <switch name="non"
                            ng-if="valorisation.selectedQualityCriteria &&
                                   valorisation.selectedQualityCriteria.qualityAttributeType === 'BINAIRE'"
                            ng-model="valorisation.binaryValue">
                    </switch>

                    <!-- critère quantitatif -->
                    <div ng-if="valorisation.selectedQualityCriteria &&
                                valorisation.selectedQualityCriteria.qualityAttributeType === 'QUANTITATIVE'">
                      <input type="text"
                             size="5"
                             ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"
                             ng-model="valorisation.quantitativeValue"/>
                      &nbsp;
                      <span class="add-on-transparent">
                        {{
                         (valorisation.selectedQualityCriteria && valorisation.selectedQualityCriteria.criteriaUnit) ?
                             editedAction.qualityCriteriaUnits[valorisation.selectedQualityCriteria.criteriaUnit] : ''
                        }}
                      </span>
                    </div>

                    <!-- critère qualitatif -->
                    <select ng-if="valorisation.selectedQualityCriteria && valorisation.selectedQualityCriteria.qualityAttributeType === 'QUALITATIVE'"
                            ng-model="valorisation.selectedQualityCriteria.refQualityCriteriaClass"
                            ng-options="qualityCritClasse.classe for qualityCritClasse in valorisation.selectedQualityCriteria.qualityCriteriaClasses">
                    </select>
                  </div>

                  <div ng-if="(valorisation.selectedQualityCriteria &&
                                valorisation.selectedQualityCriteria.qualityAttributeType === 'BINAIRE')">
                        <span ng-if="!valorisation.binaryValue"><s:text name='common-no'/></span>
                        <span ng-if="valorisation.binaryValue"><s:text name='common-yes' /></span>
                  </div>

                  <button ng-click="addQualityCriteria(valorisation)"
                          ng-disabled="!valorisation.selectedQualityCriteria ||
                                       !valorisation.quantitativeValue &&
                                       valorisation.selectedQualityCriteria.qualityAttributeType != 'BINAIRE' &&
                                       valorisation.selectedQualityCriteria.qualityAttributeType != 'QUALITATIVE'"
                          class="btn-fa btn-small blue"
                          title="<s:text name='common-add' />">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                  </button>
                </div>
              </div>
            </div>

            <div ng-if="editedValorisation == valorisation && !editedAction.isMixVarietyOrSpecies && valorisation.userCreatedValorisation
                        && (!editedAction.speciesStadeSpecies || editedAction.speciesStadeSpecies.length > 1)">
              <div class="label"><s:text name='itk-action-harvest-speciesVarietiesConcerned' /></div>
              <label ng-repeat="speciesStade in editedAction.speciesStadeSpecies">
                <input type='checkbox'
                       ng-model="editedValorisation._speciesValorisationsToCreate[speciesStade.code]" />
                <span ng-bind-html="getSpeciesLabelForSpecies(editedAction, speciesStade.code)"></span>
              </label>
            </div>
          </div>
        </div>
      </div>

      <div class="flex col half">
        <div class="column-header"><s:text name='common-comment' />&nbsp;:</div>
        <textarea id="harvesting-action-comment"
                  class="comment grow"
                  ng-model="editedAction.comment">
        </textarea>
      </div>
    </div>

    <div class="flex col half"
         ng-if="displayValorisationsBySpecies(editedAction)">
      <div class="flex row">
        <div class="column-header grow"><s:text name="itk-action-harvest-yealdAverageBySpecies"/></div><!-- Rendements par espèce / variété< -->
      </div>
      <div class="rendements" ng-repeat="mainValorisation in getFilteredMainValorisations(editedAction)">
        <div class="destination-header flex row">
          <div class="destination"><s:text name="itk-action-harvest-destinationChoice" /><span class="font-bold">{{ mainValorisation.destinationName }}</span></div>
          <div class="grow alignRight">
            <button ng-click="addValorisation(editedAction, mainValorisation)"
                    ng-if="!editedAction.isMixVarietyOrSpecies && getAvailableSpeciesNbForValorisation(editedAction, mainValorisation) > 0"
                    ng-disabled="!isEditedValorisationValid(editedAction, editedValorisation)"
                    class="btn-fa dark-blue negative"
                    title="Ajouter une espèce / variété">
              <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
          </div>
        </div>

        <div class="rendement" ng-repeat="valorisation in editedAction.valorisationsByMainValorisationIds[mainValorisation.topiaId]">
          <div class="flex row">
            <div>
              <span class="label"><s:text name="itk-action-harvest-speciesVariety" /></span>
              <span ng-if="editedValorisation != valorisation || !valorisation.userCreatedValorisation"
                    ng-bind-html="getSpeciesLabelForSpecies(editedAction, valorisation.speciesCode)"></span>
              <select ng-if="editedValorisation == valorisation && valorisation.userCreatedValorisation"
                      ng-model="editedValorisation.species"
                      ng-options="species as getSpeciesLabelForSpecies(editedAction, species.code) for species in getAvailableSpeciesForValorisation(editedAction, mainValorisation, editedValorisation)"
                      ng-change="changeValorisationSpecies(editedAction, editedValorisation)">
              </select>
              <span class="tooltip-ng warning positionRelative"
                    ng-if="valorisation.speciesCode && !valorisation.isDestinationValidForSpecies">
                &nbsp;<i class="icon fa fa-warning font-size95" aria-hidden="true"></i>
                <span class="tooltip-hover opposite" style="width:375px">
                  <s:text name="itk-action-harvest-invalidDestination" />
                </span>
              </span>
            </div>
            <div class="grow alignRight">
              <button ng-if="editedAction.isMixVarietyOrSpecies && editedValorisation && (editedValorisation == valorisation || haveValorisationsSameDestination(editedAction, editedValorisation, valorisation))"
                      ng-click="valorisation.locked = !valorisation.locked"
                      ng-disabled="!valorisation.isDestinationValidForSpecies"
                      class="btn-fa blue"
                      ng-class="{ 'negative': valorisation.locked }"
                      title="{{ valorisation.locked ? '<s:text name="common-unlock" />' : '<s:text name="common-lock" />' }}">
                <i class="fa fa-{{ valorisation.locked ? 'lock': 'unlock-alt' }}" aria-hidden="true"></i>
              </button>
              <button ng-if="editedValorisation != valorisation"
                      ng-disabled="!isEditedValorisationValid(editedAction, editedValorisation)"
                      ng-click="editValorisation(valorisation, editedAction)"
                      class="btn-fa blue"
                      title="<s:text name='common-modify' />">
                <i class="fa fa-pencil" aria-hidden="true"></i>
              </button>
              <button ng-if="editedValorisation == valorisation"
                      ng-click="stopEditSpeciesValorisation(editedAction, editedValorisation)"
                      ng-disabled="!isEditedValorisationValid(editedAction, editedValorisation)"
                      class="btn-fa red"
                      title="<s:text name='common-save' />">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>
              </button>
              <button ng-click="deleteValorisation(editedAction, mainValorisation, valorisation)"
                      ng-disabled="editedAction.valorisationsByMainValorisationIds[mainValorisation.topiaId].length == 1
                                    || (editedValorisation != valorisation && !isEditedValorisationValid(editedAction, editedValorisation))"
                      class="btn-fa blue"
                      title="<s:text name='common-delete' />">
                <i class="fa fa-trash" aria-hidden="true"></i>
              </button>
            </div>
          </div>

          <div class="flex row">
            <div class="flex col quarter" ng-if="!editedAction.isMixVarietyOrSpecies">
              <div class="label"><s:text name="itk-action-harvest-relativeArea" />&nbsp;:</div>
              <div>
                {{ valorisation.relativeArea | number:0 || "0" }}&nbsp;%
              </div>
            </div>
            <div class="flex col quarter" ng-if="editedAction.isMixVarietyOrSpecies">
              <div class="label"><s:text name="itk-action-harvest-yealdAveragePart" />&nbsp;:</div>
              <div ng-if="editedValorisation != valorisation">
                {{valorisation.yealdAveragePart | number:0 || "0"}}&nbsp;%
              </div>
              <div ng-if="editedValorisation == valorisation">
                <input type="text"
                       ng-model="valorisation.yealdAveragePartInt"
                       size="3"
                       ag-integer
                       pattern="\d+"
                       ng-blur="computeYealdAveragePartChange(editedAction, mainValorisation, editedValorisation)" />&nbsp;%
              </div>
            </div>
            <div class="flex col half">
              <div class="label"><s:text name="itk-action-harvest-yealdAverage" />&nbsp;:&nbsp;</div>
              <div>
                <span ng-if="editedValorisation != valorisation">
                  {{ valorisation.yealdAverage | number:1 | orDash }}
                </span>
                <input ng-if="editedValorisation == valorisation"
                       type="text"
                       ng-model="valorisation.yealdAverageFixedFloat"
                       size="3"
                       ag-float-positive pattern="^\d*[\.,]?\d*$"
                       ng-blur="computeYealAverageChange(editedAction, mainValorisation, editedValorisation)"
                       ng-disabled="!valorisation.isDestinationValidForSpecies" />

                {{ editedAction.yealdUnits[valorisation.yealdUnit] }}
              </div>
            </div>
            <div class="flex col quarter">
              <div class="label"><s:text name="itk-action-harvest-valorisation" /></div>
              <div ng-if="editedValorisation != valorisation">
                <div class="list-item" ng-if="valorisation.salesPercent">
                  {{valorisation.salesPercent}} % <s:text name="itk-action-harvest-salesPercent" />
                </div>
                <div class="list-item" ng-if="valorisation.selfConsumedPersent">
                  {{valorisation.selfConsumedPersent}} % <s:text name="itk-action-harvest-selfConsumedPersent" />
                </div>
                <div class="list-item" ng-if="valorisation.noValorisationPercent">
                  {{valorisation.noValorisationPercent}} % <s:text name="itk-action-harvest-noValorisationPercent" />
                </div>
              </div>
              <div ng-if="editedValorisation == valorisation">
                <div class="list-item">
                  <input type="text"
                         ng-model="valorisation.salesPercent"
                         size="3"
                         ag-sales />
                  % <s:text name="itk-action-harvest-salesPercent" />
                </div>
                <div class="list-item">
                  <input type="text"
                         ng-model="valorisation.selfConsumedPersent"
                         size="3"
                         ag-self-consumed />
                   % <s:text name="itk-action-harvest-selfConsumedPersent" />
                </div>
                <div class="list-item">
                  <input type="text"
                         ng-model="valorisation.noValorisationPercent"
                         size="3"
                         ag-no-valorisation />
                   % <s:text name="itk-action-harvest-noValorisationPercent" />
                </div>
                <div class="required" ng-if="!valorisation._isValidValorisationPercent">
                  <s:text name="itk-action-harvest-valorisation-sum-error" />
                </div>
              </div>
            </div>
          </div>

          <div class="col" ng-if="valorisation.isDestinationValidForSpecies && (valorisation.qualityCriteriaDtos.length || editedValorisation == valorisation)">
            <div class="label"><s:text name="itk-action-harvest-qualityCriteria" /></div>
            <div ng-repeat="qualityCriteria in valorisation.qualityCriteriaDtos"
                 ng-if="editedValorisation != valorisation"
                 class="list-item">
                <!-- nom du critère de qualité-->

                {{qualityCriteria.refQualityCriteria.qualityCriteriaLabel_Translated}}&nbsp;:&nbsp;

                <!-- valeur plus unité -->

                <span ng-bind-html="getQualityCriteriaValue(editedAction, qualityCriteria)"></span>
            </div>

            <!-- édition d un critère de qualité associé à une espèce -->
            <div class="col" ng-if="editedValorisation == valorisation">
              <!-- Aucun critère de qualité disponible -->
              <div ng-if="valorisation.destination &&
                           valorisation.nbQualityCriteriaForValorisation === 0 &&
                           valorisation.qualityCriteriaDtos.length === 0">
                <s:text name="itk-action-harvest-noCriteria" />
              </div>

              <!-- Affichage des critères de qualité déjà sélectionnés-->
              <div ng-repeat="qualityCriteria in valorisation.qualityCriteriaDtos" class="list-item">
                {{qualityCriteria.refQualityCriteria.qualityCriteriaLabel_Translated}}&nbsp;:&nbsp;

                <!-- valeur plus unité -->
                <span ng-bind-html="getQualityCriteriaValue(editedAction, qualityCriteria)"></span>

                <button ng-click="removeQualityCriteria(editedAction, editedValorisation, qualityCriteria)"
                        class="btn-fa btn-small blue"
                        title="<s:text name='common-delete' />">
                  <i class="fa fa-minus" aria-hidden="true"></i>
                </button>
              </div>

              <div class="flex row criteria-edition">
                <!-- choix du critère de qualité-->
                <select ng-if="valorisation.destination && valorisation.nbQualityCriteriaForValorisation > 0"
                        ng-model="valorisation.selectedQualityCriteria"
                        style="width: 70%;"
                        ng-options="qualityCrit.qualityCriteriaLabel_Translated for (id, qualityCrit) in valorisation.allQualityCriteriaForValorisation">
                </select>

                <div ng-if="valorisation.destination && valorisation.nbQualityCriteriaForValorisation > 0">
                  <!-- critère binaire -->
                  <switch name="non"
                          ng-if="(valorisation.selectedQualityCriteria &&
                                 valorisation.selectedQualityCriteria.qualityAttributeType === 'BINAIRE')"
                          ng-model="valorisation.binaryValue">
                  </switch>

                  <!-- critère quantitatif -->
                  <div ng-if="valorisation.selectedQualityCriteria &&
                              valorisation.selectedQualityCriteria.qualityAttributeType === 'QUANTITATIVE'">
                    <input type="text"
                           size="5"
                           ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"
                           ng-model="valorisation.quantitativeValue"/>
                    &nbsp;
                    <span class="add-on-transparent">
                      {{
                       (valorisation.selectedQualityCriteria && valorisation.selectedQualityCriteria.criteriaUnit) ?
                           editedAction.qualityCriteriaUnits[valorisation.selectedQualityCriteria.criteriaUnit] : ''
                      }}
                    </span>
                  </div>

                  <!-- critère qualitatif -->
                  <select ng-if="valorisation.selectedQualityCriteria && valorisation.selectedQualityCriteria.qualityAttributeType === 'QUALITATIVE'"
                          style="width: 80%;"
                          ng-model="valorisation.selectedQualityCriteria.refQualityCriteriaClass"
                          ng-options="qualityCritClasse.classe for (code, qualityCritClasse) in valorisation.selectedQualityCriteria.qualityCriteriaClasses">
                  </select>
                </div>

                <div ng-if="(valorisation.selectedQualityCriteria &&
                                 valorisation.selectedQualityCriteria.qualityAttributeType === 'BINAIRE')">
                         <span ng-if="!valorisation.binaryValue"><s:text name='common-no'/></span>
                         <span ng-if="valorisation.binaryValue"><s:text name='common-yes' /></span>
                  </div>

                <button ng-click="addQualityCriteria(valorisation)"
                        ng-disabled="!valorisation.selectedQualityCriteria ||
                                     !valorisation.quantitativeValue &&
                                     valorisation.selectedQualityCriteria.qualityAttributeType != 'BINAIRE' &&
                                     valorisation.selectedQualityCriteria.qualityAttributeType != 'QUALITATIVE'"
                        class="btn-fa btn-small blue"
                        title="<s:text name="common-add" />">
                  <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="confirmRemovedValorisation" title="<s:text name='itk-action-harvest-deleteValorisation-title' />" class="auto-hide">
    <s:text name='itk-action-harvest-deleteValorisation-message' />
  </div>

  <div id="confirmRemovedDestinations" title="<s:text name='itk-action-harvest-deleteValorisation-title' />" class="auto-hide">
    <ng-pluralize count="nbDestinationsToRemove"
                  when="{'1': '{} <s:text name='itk-action-harvest-invalidDestination-message-one' />', 'other': '{} <s:text name='itk-action-harvest-invalidDestination-message-several' />'}" />
  </div>

  <div id="confirmEditYealdAverage" title="<s:text name='itk-action-harvest-yielModification-title' />" class="auto-hide">
    <s:text name='itk-action-harvest-yielModification-message' />
  </div>

  <div id="destinationUnitChange" title="<s:text name='itk-action-harvest-destinationUnitChange-title' />" class="auto-hide">
    <div ng-bind-html="destinationUnitChangeMessage"></div>
    <div>&nbsp;</div>
    <table class="diagnoseTable">
      <tr>
        <td><s:text name="itk-action-harvest-destinationUnitChange-currentYield" />&nbsp;:</td>
        <td class="column-small">{{editedValorisation.yealdAverage| number:1 || "-"}}</td>
        <td>{{editedAction.yealdUnits[editedValorisation.previousYealdUnit]}}</td>
        <td><s:text name="itk-action-harvest-destinationUnitChange-newYield" />&nbsp;:</td>
        <td class="column-small"><input type="text" ng-model="editedValorisation.yealdAverageFixedFloat" size="3" ag-float-positive pattern="^\d*[\.,]?\d*$"/></td>
        <td>{{editedAction.yealdUnits[editedValorisation.destination.yealdUnit]}}</td>
      </tr>
    </table>
  </div>

</div>
