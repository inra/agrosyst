<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- itk/actions/APPLICATION_DE_PRODUITS_PHYTOSANITAIRES.jsp -->

<div class="wwgrp">
  <span class="wwlbl">
    <label for="pesticides-spreading-proportionOfTreatedSurface"><span class="required">*</span> <s:text name='itk-action-pesticides-spreading-proportionOfTreatedSurface' />&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <span class='contextual-help'>
      <span class='help-hover'>
        <s:text name="help.pesticidesSpreading.proportionOfTreatedSurface" />
      </span>
    </span>
    <span class="input-append">
      <input type="text" id="pesticides-spreading-proportionOfTreatedSurface"
             ng-model="editedAction.proportionOfTreatedSurface"
             placeholder="<s:text name='itk-action-pesticides-spreading-proportionOfTreatedSurface-placeholder' />" ag-required-percent-without_zero required />
      <span class="add-on">%</span>
    </span>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label for="pesticides-spreading-boiledQuantity"><span class="required">*</span> <s:text name='itk-action-pesticides-spreading-boiledQuantity' />&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <span class="input-append">
      <input type="text" id="pesticides-spreading-boiledQuantity"
             ng-model="editedAction.boiledQuantity"
             title="<s:text name="help.pesticidesSpreading.boiledQuantityInHL" />"
             placeholder="<s:text name='itk-action-pesticides-spreading-boiledQuantity-placeholder' />"
             ag-float-positive
             pattern="^\d*[\.,]?\d*$" required />
      <span class="add-on">hL</span>
    </span>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label for="pesticides-spreading-boiledQuantityPerTrip"><s:text name='itk-action-pesticides-spreading-boiledQuantityPerTrip' />&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <span class="input-append">
      <input type="text" id="pesticides-spreading-boiledQuantityPerTrip"
             ng-model="editedAction.boiledQuantityPerTrip"  title="<s:text name="help.pesticidesSpreading.boiledQuantityInHL" />"
             placeholder="<s:text name='itk-action-pesticides-spreading-boiledQuantityPerTrip-placeholder' />" ag-float-positive pattern="^\d*[\.,]?\d*$" />
      <span class="add-on">hL</span>
    </span>
  </span>
</div>

<div class="wwgrp radio-list">
  <span class="wwlbl">
    <label><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> <s:text name='itk-action-pesticides-spreading-antiDriftNozzle' />&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <label class="radioLabel">
      <input type="radio" ng-value="true" ng-model="editedAction.antiDriftNozzle" /><s:text name='common-yes' />
    </label>
    <label class="radioLabel">
      <input type="radio" ng-value="false" ng-model="editedAction.antiDriftNozzle" /><s:text name='common-no' />
    </label>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label for="pesticides-spreading-tripFrequency"><s:text name='itk-action-pesticides-spreading-tripFrequency' />&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <input type="text" id="pesticides-spreading-tripFrequency" ng-model="editedAction.tripFrequency"
           placeholder="<s:text name='itk-action-pesticides-spreading-tripFrequency-placeholder' />" ag-float-positive pattern="^\d*[\.,]?\d*$" />
  </span>
</div>

<div class="wwgrp horizontal-separator">
  <span class="wwlbl"><label for="pesticides-spreading-action-comment"><s:text name='common-comment' />&nbsp;:</label></span>
  <span class="wwctrl"><textarea id="pesticides-spreading-action-comment" ng-model="editedAction.comment"></textarea></span>
</div>
