<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<!-- itk/actions/TRAVAIL_DU_SOL.jsp -->

<div class="wwgrp">
  <span class="wwlbl"><label for="tillageDepth"><s:text name="itk-action-ground-tillageDepth" />&nbsp;:</label></span>
  <span class="wwctrl">
    <span class="input-append">
      <input  type="text" id="tillageDepth" ng-model="editedAction.tillageDepth"
              placeholder="<s:text name='itk-action-ground-tillageDepth-placeholder' />"
              ag-float-positive pattern="^\d*[\.,]?\d*$" >
      <span class="add-on">cm</span>
    </span>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl"><label for="otherSettingTool"><s:text name="itk-action-ground-otherSettingTool" />&nbsp;:</label></span>
  <span class="wwctrl"><input type="text" id="otherSettingTool"
      ng-model="editedAction.otherSettingTool"
      placeholder="<s:text name="itk-action-ground-otherSettingTool-placeholder" />" /></span>
</div>

<div class="wwgrp horizontal-separator">
  <span class="wwlbl"><label for="tillage-action-comment"><s:text name="common-comment" />&nbsp;:</label></span>
  <span class="wwctrl"><textarea id="tillage-action-comment" ng-model="editedAction.comment"></textarea></span>
</div>
