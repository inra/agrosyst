<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<!-- itk/actions/EPANDAGES_ORGANIQUES.jsp -->

<div class="wwgrp">
  <span class="wwlbl"><label for="organic-fertilizers-spreading-landfilledWaste"><s:text name='itk-action-organic-fertilizers-spreading-landfilledWaste' />&nbsp;:</label></span>
  <span class="wwctrl"><input type="checkbox" id="organic-fertilizers-spreading-landfilledWaste" ng-model="editedAction.landfilledWaste" /></span>
</div>

<div class="wwgrp horizontal-separator">
  <span class="wwlbl"><label for="organic-fertilizers-spreading-comment"><s:text name='common-comment' />&nbsp;:</label></span>
  <span class="wwctrl"><textarea id="organic-fertilizers-spreading-comment" ng-model="editedAction.comment"></textarea></span>
</div>
