<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- itk/inputs/substrate-input.jsp -->
<div class="wwgrp">
  <span class="wwlbl">
    <label for="editedUsage-substrate-action"><span class="required">*</span>Action&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <select id="editedUsage-substrate-action" ng-model="editedUsage.mainActionId"
            ng-options="action.mainActionId as (agrosystInterventionTypes[action.mainAction.intervention_agrosyst] + ' / ' + action.mainActionReference_label) for action in editedUsage.actions | orderBy:mainActionSort"></select>
  </span>
</div>
<div class="wwgrp">
  <span class="wwlbl">
    <label for="substrate-input"><span class="required">*</span>{{inputTypesLabels[editedUsage.inputType]}}&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <select id="substrate-input" ng-model="editedUsage.inputId"
            ng-options="input.topiaId as input.inputName for input in editedUsage.inputs" 
            ng-change="substrateSelected()">
    </select>
  </span>
  <div class="wwgrp">
    <span class="wwlbl">
      <label for="editedUsage-substrateInput-qtAvg"><span class="required">*</span><s:text name="common-quantity" />&nbsp;:</label>
    </span>
    <span class="wwctrl">
      <span class="two-columns">
        <input id="editedUsage-substrateInput-qtAvg" type="text" ng-model="editedUsage.qtAvg" ag-float pattern="^\d*[\.,]?\d*$" />
      </span>
      <span class="two-columns">
        <span class="add-on">{{editedUsage.substrateInputUnits[editedUsage.domainSubstrateInputDto.usageUnit]}}</span>
      </span>
    </span>
  </div>
</div>
