<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- itk/inputs/other-product-input.jsp -->
<div class="wwgrp">
  <span class="wwlbl">
    <label for="editedUsage-otherProductInput-action"><span class="required">*</span>Action&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <select id="editedUsage-otherProductInput-action" ng-model="editedUsage.mainActionId"
            ng-options="action.mainActionId as (agrosystInterventionTypes[action.mainAction.intervention_agrosyst] + ' / ' + action.mainActionReference_label) for action in editedUsage.actions | filter:isValidActionForInput |orderBy:mainActionSort"></select>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label for="other-input"><span class="required">*</span>{{inputTypesLabels[editedUsage.inputType]}}&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <select id="other-input" ng-model="editedUsage.inputId"
      ng-options="input.topiaId as input.inputName for input in editedUsage.inputs" ng-change="otherSelected()">
    </select>
  </span>
</div>

<div class="wwgrp clear horizontal-separator" >
  <span class="wwlbl">
    <label class="fields-group"><s:text name="common-dose-produit" />&nbsp;</label>
  </span>
  <div class="wwgrp">
    <span class="wwlbl">
      <label for="editedUsageQtAvg"><span class="requiredIndicator" title="<s:text name="help.performance.required" /> : <s:text name="common-i-phy-ift" />">&pi;</span> <s:text name="common-dose-moyenne" />&nbsp;:</label>
    </span>
    <span class="wwctrl">
      <span class="two-columns">
        <input id="editedUsageQtAvg" type="text" ng-model="editedUsage.qtAvg" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
      </span>
      <span class="two-columns">
        <span class="add-on">{{editedUsage.otherProductInputUnits[editedUsage.domainOtherProductInputDto.usageUnit]}}</span>
      </span>
    </span>
  </div>
  <div class="wwgrp">

  </div>
  <div class="wwgrp">
    <div class="wwctrl">
      <a class="btn" ng-click="showQtyMinMediumMax = !showQtyMinMediumMax"
     ng-show="!showQtyMinMediumMax && !editedUsage.qtMin && !editedUsage.qtMed && !editedUsage.qtMax"><s:text name="common-afficher-doses" /></a>
    </div>
  </div>
  <div ng-show="showQtyMinMediumMax || editedUsage.qtMin || editedUsage.qtMed || editedUsage.qtMax"
       class="slide-animation">
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedUsageQtMin"><s:text name="common-dose-minimale" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedUsageQtMin" type="text" ng-model="editedUsage.qtMin" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
          <span class="add-on">{{editedUsage.otherProductInputUnits[editedUsage.domainOtherProductInputDto.usageUnit]}}</span>
        </span>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedUsageQtMax"><s:text name="common-dose-maximale" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedUsageQtMax" type="text" ng-model="editedUsage.qtMax" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
          <span class="add-on">{{editedUsage.otherProductInputUnits[editedUsage.domainOtherProductInputDto.usageUnit]}}</span>
        </span>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedInputQtMed"><s:text name="common-dose-mediane" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedUsageQtMed" type="text" ng-model="editedUsage.qtMed" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
          <span class="add-on">{{editedUsage.otherProductInputUnits[editedUsage.domainOtherProductInputDto.usageUnit]}}</span>
        </span>
      </span>
    </div>
  </div>
</div>
