<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- itk/inputs/semis-input.jsp -->



<div class="wwgrp">
  <span class="wwlbl">
    <!-- Type d’intrant -->
    <label for="semis-input"><span class="required">*</span>{{inputTypesLabels[editedUsage.inputType]}}&nbsp;:</label>
  </span>
  <span class="wwctrl">
    <select ng-if="editedUsage.inputs && editedUsage.inputs.length > 0"
            id="semis-input" ng-model="editedUsage.inputId"
            ng-options="input.topiaId as input.inputName for input in editedUsage.inputs"
            ng-change="semisSelected()">
    </select>
    <div ng-if="!editedUsage.inputs || editedUsage.inputs.length === 0">
      <span><s:text name="itk-messages-js-seedingActionNoSeedLot" /></span>
    </div>
  </span>
</div>
<div class="wwgrp">
  <span class="wwlbl">
    <a href="<s:url value='/help/convertisseur_densite_semis.xlsx' />" title="Convertisseur densité semis" target="_blank" rel="noopener noreferrer" class="black-link">
      <em class="fa fa-download" aria-hidden="true"></em>&nbsp;<s:text name="common-density-converter" />
    </a>
  </span>
  <span class="wwctrl"></span>
</div>
<div class="wwgrp clear horizontal-separator" ng-if="editedUsage.domainSeedLotInputDto.speciesInputs.length === 0">
  <span style="margin-left: 30%;"><s:text name="itk-messages-js-seedLotUsageNoSpeciesMatching" /></span>
</div>
<div class="wwgrp clear horizontal-separator" ng-repeat="species in editedUsage.domainSeedLotInputDto.speciesInputs track by $index">
  <div class="rendements">
    <div class="culture-header flex row">
      <div class="culture textIcones {{editedUsage.domainSeedLotInputDto.cropSeedDto.type}}"
        title="{{ getTitleForEditedUsage() }}">
        <span ng-if="editedUsage.domainSeedLotInputDto.cropSeedDto.type === 'MAIN'">P</span>
        <span ng-if="editedUsage.domainSeedLotInputDto.cropSeedDto.type === 'INTERMEDIATE'">I</span>
        <span ng-if="editedUsage.domainSeedLotInputDto.cropSeedDto.type === 'CATCH'">D</span>
        <span ng-if="editedUsage.domainSeedLotInputDto.cropSeedDto.mixSpecies" class="textIcones MIX_S">ME</span>
        <span ng-if="editedUsage.domainSeedLotInputDto.cropSeedDto.mixVariety" class="textIcones MIX_V">MV</span>
        <span ng-if="editedUsage.domainSeedLotInputDto.cropSeedDto.mixCompanion" class="textIcones MIX_C"><s:text name='common-cropping-plan-mix-companion-abbr'/></span>
      </div>
      <div class="culture"><s:text name="common-espece" />&nbsp;: <span ng-bind-html="species.inputName" class="font-bold"></span><span ng-if="!isSpeciesQtyAvailable(editedUsage.domainSeedLotInputDto.speciesInputs[$index])"> (Cette culture n'ést pas sélectionnée parmis les espèces concernées)</span></div>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedUsage-semisInput-qtAvg"><span class="required">*</span><s:text name="common-quantity" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="two-columns">
          <input id="editedUsage-semisInput-qtAvg" type="text" ng-model="editedUsage.domainSeedLotInputDto.speciesInputs[$index].qtAvg" ng-disabled="!isSpeciesQtyAvailable(editedUsage.domainSeedLotInputDto.speciesInputs[$index])" ag-float-positive pattern="^\d*[\.,]?\d*$" />
        </span>
        <span class="two-columns">
          <span class="add-on">{{editedUsage.seedPlantUnits[editedUsage.domainSeedLotInputDto.usageUnit]}}</span>
        </span>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedUsage-semisInput-deepness"><s:text name="common-deepness" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedUsage-semisInput-deepness" type="text" ng-model="editedUsage.domainSeedLotInputDto.speciesInputs[$index].deepness" ag-float pattern="^[\-\+]?\d*[\.,]?\d*$" />
          <span class="add-on">cm</span>
        </span>
      </span>
    </div>
    <div class="wwgrp" ng-show="species.speciesPhytoInputDtos && species.speciesPhytoInputDtos.length > 0">
      <div class="wwctrl">
        <a class="btn" ng-click="showProducts[$index] = !showProducts[$index];doShowProducts(species.speciesPhytoInputDtos, species);">
          <span ng-show="!showProducts[$index]">
            <s:text name="common-afficher-produits" />&nbsp;({{species.speciesPhytoInputDtos.length}})
          </span>
          <span ng-show="showProducts[$index]">
            <s:text name="common-cacher-produits" />&nbsp;({{species.speciesPhytoInputDtos.length}})
          </span>
        </a>
      </div>
    </div>
    <div ng-show="showProducts[$index]">
      <div class="wwgrp clear" ng-repeat="phyto in species.speciesPhytoInputDtos">
        <div class="horizontal-separator" style="margin-left: 50px;margin-right: 50px;"></div>
        <span class="wwlbl">
          <label class="fields-group">
            {{phyto.inputName}}&nbsp;
          </label>
        </span>

        <div class="wwgrp">
          <span class="wwlbl">
            <label><span class="requiredAdvised" title="<s:text name=" help.dephy.advised" />">&pi;</span><s:text name="common-cibles-du-traitement" />&nbsp;:</label>
          </span>
        
          <span class="wwctrl">
            <table class="data-table">
              <thead>
                <tr>
                  <th scope="col" class="column-large">
                    <s:text name="common-categorie" />
                  </th>
                  <th scope="col" class="column-large">
                    <s:text name="common-groupe-cible" />
                  </th>
                  <th scope="col">
                    <s:text name="common-cible-traitement" />
                  </th>
                  <th scope="col" class="column-xsmall">
                    <s:text name="common-action" />
                  </th>
                </tr>
              </thead>
        
              <tbody>
                <tr ng-repeat="target in phyto.targets">
                  <td class="column-large">
                    {{target.category ? editedUsage.treatmentTargetCategories[target.category] : 'Adventice'}}
                  </td>
                  <td class="column-large">
                    <span ng-if="target.codeGroupeCibleMaa">{{getGroupeCibleLabel(target.codeGroupeCibleMaa)}}</span>
                  </td>
                  <td>
                    <span ng-if="target.target">{{target.target.reference_label ? target.target.reference_label : target.target.adventice}}</span>
                    <span ng-if="target.refBioAggressorLabel">{{target.refBioAggressorLabel}}</span>
                  </td>
                  <td>
                    <input type="button" class="btn-icon icon-delete" value="<s:text name=" common-remove" />"title="<s:text name="common-remove" />" ng-click="deleteTreatmentTargetPhyto(target, phyto, species)"/>
                  </td>
                </tr>
                <tr>
                  <td class="column-large">
                    <select ng-model="phyto.treatmentTarget.category"
                      ng-options="key as value for (key, value) in editedUsage.treatmentTargetCategories"
                      ng-change="treatmentTargetCategorySelectedPhyto(phyto, species)">
                    </select>
                  </td>
                  <td class="column-large">
                    <select ng-model="phyto.treatmentTarget.groupeCibleMaa"
                      ng-options="groupeCible.groupeCibleMaa for groupeCible in groupesCibles | isGroupeCibleInCategoryPhyto :phyto :this"
                      ng-disabled="!phyto.treatmentTarget.category">
                      <option label="" value="" />
                    </select>
                  </td>
                  <td ng-if="!phyto.treatmentTargets && !phyto.treatmentTarget.category" class="empty-table">
                    <s:text name="common-choisir-categorie-cible" />
                  </td>
                  <td ng-if="!phyto.treatmentTargets && phyto.treatmentTarget.category" class="empty-table">
                    <s:text name="common-chargement" />
                  </td>
                  <td ng-if="phyto.treatmentTargets">
                    <select ng-model="phyto.treatmentTarget.target"
                      ng-options="target.reference_label || target.adventice for target in phyto.treatmentTargets | isTargetInGroupeCiblePhyto :phyto"
                      ng-disabled="!phyto.treatmentTarget.category" ng-change="treatmentTargetSelectedPhyto(phyto, species)">
                    </select>
                  </td>
                  <td>
                    <button ng-click="treatmentTargetSelectedPhyto(phyto, species)" ng-disabled="!isTreatmentTargetValidPhyto(phyto)" class="btn-fa blue"
                      title="<s:text name=" common-ajouter" />">
                    <i class="fa fa-plus font-size95" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label>
              <s:text name="common-dose-reference-titre" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl" style="width:10%">
            <span class='contextual-help indicative-text-content'>?
              <span class='help-hover standard'>
                <s:text name="common-dose-reference-text" />
              </span>
            </span>
            <span ng-if="!editedUsage.inputId" class="empty-table">
              <s:text name="common-choisir-produit" />
            </span>
            <span ng-if="editedUsage.inputId && !phyto.referenceDose" class="empty-table">
              <s:text name="common-chargement" />
            </span>
            <span
              ng-if="editedUsage.inputId && phyto.referenceDose && phyto.referenceDose.IFT_Ancienne && phyto.referenceDose.IFT_Ancienne.error"
              class="empty-table">{{phyto.referenceDose.IFT_Ancienne.error}}</span>
            <span
              ng-if="editedUsage.inputId && phyto.referenceDose && phyto.referenceDose.IFT_Ancienne && !phyto.referenceDose.IFT_Ancienne.error"
              class="generated-content">{{phyto.referenceDose.IFT_Ancienne.value}}
              {{editedUsage.phytoProductUnits[phyto.referenceDose.IFT_Ancienne.unit]}}</span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label>
              <s:text name="common-dose-reference-non-millesime-titre" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl" style="width:10%">
            <span class='contextual-help indicative-text-content'>?
              <span class='help-hover standard'>
                <s:text name="common-dose-reference-non-millesime-text" />
              </span>
            </span>
            <span ng-if="!editedUsage.inputId" class="empty-table">
              <s:text name="common-choisir-produit" />
            </span>
            <span ng-if="editedUsage.inputId && !phyto.referenceDose" class="empty-table">
              <s:text name="common-chargement" />
            </span>
            <span
              ng-if="editedUsage.inputId && phyto.referenceDose && phyto.referenceDose.IFT_Cible_NonMillesime && phyto.referenceDose.IFT_Cible_NonMillesime.error"
              class="empty-table">{{phyto.referenceDose.IFT_Cible_NonMillesime.error}}</span>
            <span
              ng-if="editedUsage.inputId && phyto.referenceDose && phyto.referenceDose.IFT_Cible_NonMillesime && !phyto.referenceDose.IFT_Cible_NonMillesime.error"
              class="generated-content">{{phyto.referenceDose.IFT_Cible_NonMillesime.value}}
              {{editedUsage.phytoProductUnits[phyto.referenceDose.IFT_Cible_NonMillesime.unit]}}</span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label>
              <s:text name="common-dose-reference-millesime-titre" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl" style="width:10%">
            <span class='contextual-help indicative-text-content'>?
              <span class='help-hover standard reverse'>
                <s:text name="common-dose-reference-millesime-text" />
              </span>
            </span>
            <span class="empty-table"><i class="fa fa-exclamation-triangle"></i>
              <s:text name="common-indicateur-non-disponible" />
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label class="fields-group">
              <s:text name="common-dose-produit-applique" />&nbsp;
            </label>
          </span>
          <div class="wwgrp">
            <span class="wwlbl">
              <label for="phytoQtAvg"><span class="requiredIndicator" title="<s:text name=" help.performance.required" /> : <s:text name="common-i-phy-ift" />">&pi;</span>
            <s:text name="common-dose-moyenne" />&nbsp;:</label>
            </span>
            <span class="wwctrl">
              <span class="two-columns">
                <input id="phytoQtAvg" type="text" ng-model="phyto.qtAvg" ag-float-positive
                  pattern="^[+]?\d*[\.,]?\d*$" />
              </span>
              <span class="two-columns">
                <span class="add-on">{{editedUsage.phytoProductUnits[phyto.usageUnit]}}</span>
              </span>
            </span>
          </div>
          <div class="wwgrp">
            <div class="wwctrl">
              <a class="btn" ng-click="showQtyMinMediumMax[phyto.topiaId] = !showQtyMinMediumMax[phyto.topiaId]"
                ng-show="!showQtyMinMediumMax[phyto.topiaId] && !phyto.qtMin && !phyto.qtMed && !phyto.qtMax">
                <s:text name="common-afficher-doses" />
              </a>
            </div>
          </div>
          <div ng-show="showQtyMinMediumMax[phyto.topiaId] || phyto.qtMin || phyto.qtMed || phyto.qtMax"
            class="slide-animation">
            <div class="wwgrp">
              <span class="wwlbl">
                <label for="phytoQtMin">
                  <s:text name="common-dose-minimale" />&nbsp;:
                </label>
              </span>
              <span class="wwctrl">
                <span class="input-append">
                  <input id="phytoQtMin" type="text" ng-model="phyto.qtMin" ag-float-positive
                    pattern="^[+]?\d*[\.,]?\d*$" />
                  <span
                    class="add-on">{{editedUsage.phytoProductUnits[phyto.usageUnit]}}</span>
                </span>
              </span>
            </div>
            <div class="wwgrp">
              <span class="wwlbl">
                <label for="editedUsageQtMax">
                  <s:text name="common-dose-maximale" />&nbsp;:
                </label>
              </span>
              <span class="wwctrl">
                <span class="input-append">
                  <input id="phytoQtMax" type="text" ng-model="phyto.qtMax" ag-float-positive
                    pattern="^[+]?\d*[\.,]?\d*$" />
                  <span
                    class="add-on">{{editedUsage.phytoProductUnits[phyto.usageUnit]}}</span>
                </span>
              </span>
            </div>
            <div class="wwgrp">
              <span class="wwlbl">
                <label for="phytoQtMed">
                  <s:text name="common-dose-mediane" />&nbsp;:
                </label>
              </span>
              <span class="wwctrl">
                <span class="input-append">
                  <input id="phytoQtMed" type="text" ng-model="phyto.qtMed" ag-float-positive
                    pattern="^[+]?\d*[\.,]?\d*$" />
                  <span
                    class="add-on">{{editedUsage.phytoProductUnits[phyto.usageUnit]}}</span>
                </span>
              </span>
            </div>
          </div>
          <div class="wwgrp">
            <span class="wwlbl">
              <label for="phytoInputComment">
                <s:text name="common-commentaire" />&nbsp;:
              </label>
            </span>
            <span class="wwctrl">
              <input id="phytoInputComment" type="text" ng-model="phyto.comment" placeholder="Indiquez le nom du produit si vous avez sélectionné 'Autre insecticide' ou 'Autre fongicide'" />
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="wwgrp clear horizontal-separator">
  <div class="wwgrp">
    <span class="wwlbl">
      <label for="editedUsage-semisInput-comment"><s:text name="common-comment" />&nbsp;:</label>
    </span>
    <span class="wwctrl">
      <textarea id="editedUsage-semisInput-comment" type="text" ng-model="editedUsage.comment" ></textarea>
    </span>
  </div>
</div>
