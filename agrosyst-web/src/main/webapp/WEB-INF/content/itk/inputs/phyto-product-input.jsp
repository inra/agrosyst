<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- itk/inputs/phyto-product-input.jsp -->

<div class="wwgrp">
  <span class="wwlbl">
    <label><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> <s:text name="common-cibles-du-traitement" />&nbsp;:</label>
  </span>

  <span class="wwctrl">
    <table class="data-table">
      <thead>
        <tr>
          <th scope="col" class="column-large"><s:text name="common-categorie" /></th>
          <th scope="col" class="column-large"><s:text name="common-groupe-cible" /></th>
          <th scope="col"><s:text name="common-cible-traitement" /></th>
          <th scope="col" class="column-xsmall"><s:text name="common-action" /></th>
        </tr>
      </thead>

      <tbody>
        <tr ng-repeat="target in editedUsage.targets">
          <td class="column-large">
            {{target.category ? editedUsage.treatmentTargetCategories[target.category] : 'Adventice'}}
          </td>
          <td class="column-large">
            <span ng-if="target.codeGroupeCibleMaa">{{getGroupeCibleLabel(target.codeGroupeCibleMaa)}}</span>
          </td>
          <td>
            <span ng-if="target.target">{{target.target.reference_label ? target.target.reference_label : target.target.adventice}}</span>
            <span ng-if="target.refBioAggressorLabel">{{target.refBioAggressorLabel}}</span>
          </td>
          <td>
            <input type="button" class="btn-icon icon-delete" value="<s:text name="common-remove" />" title="<s:text name="common-remove" />" ng-click="deleteTreatmentTarget(target)"/>
          </td>
        </tr>
        <tr>
          <td class="column-large">
            <select ng-model="treatmentTarget.category"
                    ng-options="key as value for (key, value) in editedUsage.treatmentTargetCategories"
                    ng-change="treatmentTargetCategorySelected()">
            </select>
          </td>
          <td class="column-large">
            <select ng-model="treatmentTarget.groupeCibleMaa"
                    ng-options="groupeCible.groupeCibleMaa for groupeCible in groupesCibles | filter: isGroupeCibleInCategory"
                    ng-disabled="!treatmentTarget.category">
              <option label="" value="" />
            </select>
          </td>
          <td ng-if="!editedUsage.treatmentTargets && !treatmentTarget.category" class="empty-table">
            <s:text name="common-choisir-categorie-cible" />
          </td>
          <td ng-if="!editedUsage.treatmentTargets && treatmentTarget.category" class="empty-table">
            <s:text name="common-chargement" />
          </td>
          <td ng-if="editedUsage.treatmentTargets">
            <select ng-model="treatmentTarget.target"
                    ng-options="target.reference_label || target.adventice for target in editedUsage.treatmentTargets | filter: isTargetInGroupeCible"
                    ng-disabled="!treatmentTarget.category"
                    ng-change="treatmentTargetSelected()">
            </select>
          </td>
          <td>
            <button ng-click="treatmentTargetSelected()"
                    ng-disabled="!isTreatmentTargetValid()"
                    class="btn-fa blue"
                    title="<s:text name="common-ajouter" />">
              <i class="fa fa-plus font-size95" aria-hidden="true"></i>
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  </span>
</div>
<div class="wwgrp">
  <span class="wwlbl">
    <label for="product-type">
      <s:text name="common-type" />&nbsp;:
    </label>
  </span>
  <span class="wwctrl">
    <select id="product-type" ng-model="editedUsage.productType"
      ng-options="productType.key as productType.trad for productType in editedUsage.productTypes" ng-change="productTypeSelected()">
    </select>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label for="phyto-product-input">
      <span class="required">*</span>&nbsp;
      <span ng-if="editedUsage.inputType === 'LUTTE_BIOLOGIQUE'">
        <s:text name="fr.inra.agrosyst.api.entities.InputType.LUTTE_BIOLOGIQUE" />
      </span>
      <span ng-if="editedUsage.inputType === 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'">
        <s:text name="fr.inra.agrosyst.api.entities.InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES" />
      </span>
      &nbsp;:
    </label>
  </span>
  <span class="wwctrl">
    <select id="phyto-product-input" ng-model="editedUsage.inputId"
      ng-options="input.topiaId as input.inputName for input in editedUsage.inputs" ng-change="phytoProductSelected()">
    </select>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label><s:text name="common-dose-reference-non-millesime-titre" />&nbsp;:</label>
  </span>
  <span class="wwctrl" style="width:10%">
    <span class='contextual-help indicative-text-content'>?
      <span class='help-hover standard center'>
        <s:text name="common-dose-reference-non-millesime-text" />
      </span>
    </span>
    <span ng-if="!editedUsage.inputId" class="empty-table"><s:text name="common-choisir-produit" /></span>
    <span ng-if="editedUsage.inputId && !editedUsage.referenceDose" class="empty-table"><s:text name="common-chargement" /></span>
    <span ng-if="editedUsage.inputId && editedUsage.referenceDose && editedUsage.referenceDose.IFT_Cible_NonMillesime && editedUsage.referenceDose.IFT_Cible_NonMillesime.error" class="empty-table">{{editedUsage.referenceDose.IFT_Cible_NonMillesime.error}}</span>
    <span ng-if="editedUsage.inputId && editedUsage.referenceDose && editedUsage.referenceDose.IFT_Cible_NonMillesime && !editedUsage.referenceDose.IFT_Cible_NonMillesime.error" class="generated-content" >{{editedUsage.referenceDose.IFT_Cible_NonMillesime.value}} {{editedUsage.phytoProductUnits[editedUsage.referenceDose.IFT_Cible_NonMillesime.unit]}}</span>
  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label><s:text name="common-dose-reference-millesime-titre" />&nbsp;:</label>
  </span>
  <span class="wwctrl" style="width:10%">
    <span class='contextual-help indicative-text-content'>?
      <span class='help-hover standard reverse'>
        <s:text name="common-dose-reference-millesime-text" />
      </span>
    </span>

    <span ng-if="!editedUsage.inputId" class="empty-table"><s:text name="common-choisir-produit" /></span>
    <span ng-if="editedUsage.inputId && !editedUsage.referenceDose" class="empty-table"><s:text name="common-chargement" /></span>
    <span ng-if="editedUsage.inputId && editedUsage.referenceDose && editedUsage.referenceDose.IFT_Cible_Millesime && editedUsage.referenceDose.IFT_Cible_Millesime.error" class="empty-table">{{editedUsage.referenceDose.IFT_Cible_Millesime.error}}</span>
    <span ng-if="editedUsage.inputId && editedUsage.referenceDose && editedUsage.referenceDose.IFT_Cible_Millesime && !editedUsage.referenceDose.IFT_Cible_Millesime.error" class="generated-content" >{{editedUsage.referenceDose.IFT_Cible_Millesime.value}} {{editedUsage.phytoProductUnits[editedUsage.referenceDose.IFT_Cible_Millesime.unit]}}</span>

  </span>
</div>

<div class="wwgrp">
  <span class="wwlbl">
    <label><s:text name="common-dose-reference-titre" />&nbsp;:</label>
  </span>
  <span class="wwctrl" style="width:10%">
    <span class='contextual-help indicative-text-content'>?
      <span class='help-hover standard reverse'>
        <s:text name="common-dose-reference-text" />
      </span>
    </span>
    <span ng-if="!editedUsage.inputId" class="empty-table"><s:text name="common-choisir-produit" /></span>
    <span ng-if="editedUsage.inputId && !editedUsage.referenceDose" class="empty-table"><s:text name="common-chargement" /></span>
    <span ng-if="editedUsage.inputId && editedUsage.referenceDose && editedUsage.referenceDose.IFT_Ancienne && editedUsage.referenceDose.IFT_Ancienne.error" class="empty-table">{{editedUsage.referenceDose.IFT_Ancienne.error}}</span>
    <span ng-if="editedUsage.inputId && editedUsage.referenceDose && editedUsage.referenceDose.IFT_Ancienne && !editedUsage.referenceDose.IFT_Ancienne.error" class="generated-content" >{{editedUsage.referenceDose.IFT_Ancienne.value}} {{editedUsage.phytoProductUnits[editedUsage.referenceDose.IFT_Ancienne.unit]}}</span>
  </span>
</div>

<div class="wwgrp clear horizontal-separator">
  <span class="wwlbl">
    <label class="fields-group"><s:text name="common-dose-produit-applique" />&nbsp;</label>
  </span>
  <div class="wwgrp">
    <span class="wwlbl">
      <label for="editedUsageQtAvg"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : <s:text name="common-i-phy-ift" />">&pi;</span> <s:text name="common-dose-moyenne" />&nbsp;:</label>
    </span>
    <span class="wwctrl">
      <span class="two-columns">
        <input id="editedUsageQtAvg" type="text" ng-model="editedUsage.qtAvg" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
      </span>
      <span class="two-columns">
        <span class="add-on">{{editedUsage.phytoProductUnits[editedUsage.domainPhytoProductInputDto.usageUnit]}}</span>
      </span>
    </span>
  </div>
  <div class="wwgrp">
    <div class="wwctrl">
      <a class="btn" ng-click="showQtyMinMediumMax = !showQtyMinMediumMax"
    ng-show="!showQtyMinMediumMax && !editedUsage.qtMin && !editedUsage.qtMed && !editedUsage.qtMax"><s:text name="common-afficher-doses" /></a>
    </div>
  </div>
  <div ng-show="showQtyMinMediumMax || editedUsage.qtMin || editedUsage.qtMed || editedUsage.qtMax"
    class="slide-animation">
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedUsageQtMin"><s:text name="common-dose-minimale" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedUsageQtMin" type="text" ng-model="editedUsage.qtMin" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
          <span class="add-on">{{editedUsage.phytoProductUnits[editedUsage.domainPhytoProductInputDto.usageUnit]}}</span>
        </span>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedUsageQtMax"><s:text name="common-dose-maximale" />&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedUsageQtMax" type="text" ng-model="editedUsage.qtMax" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
          <span class="add-on">{{editedUsage.phytoProductUnits[editedUsage.domainPhytoProductInputDto.usageUnit]}}</span>
        </span>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label for="editedUsageQtMed"><s:text name="common-dose-mediane"/>&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class="input-append">
          <input id="editedUsageQtMed" type="text" ng-model="editedUsage.qtMed" ag-float-positive pattern="^[+]?\d*[\.,]?\d*$" />
          <span class="add-on">{{editedUsage.phytoProductUnits[editedUsage.domainPhytoProductInputDto.usageUnit]}}</span>
        </span>
      </span>
    </div>
  </div>
  <div class="wwgrp">
    <span class="wwlbl">
      <label for="editedUsagePhytoInputComment"><s:text name="common-commentaire" />&nbsp;:</label>
    </span>
    <span class="wwctrl">
      <input id="editedUsagePhytoInputComment" type="text" ng-model="editedUsage.comment" placeholder="Indiquez le nom du produit si vous avez sélectionné 'Autre insecticide' ou 'Autre fongicide'" />
    </span>
  </div>
</div>
