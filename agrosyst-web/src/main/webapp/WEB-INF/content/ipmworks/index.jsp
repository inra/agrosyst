<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2023 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
  <!DOCTYPE html>
  <%@taglib uri="/struts-tags" prefix="s" %>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

    <head>
      <title>Accueil</title>
      <script type="text/javascript"
        src="<s:url value='/nuiton-js/ipmworks-index.js'/><s:property value='getVersionSuffix()'/>"></script>
      <script>
        window.addEventListener("DOMContentLoaded", () => {
          AgrosystVueComponents.Ipmworks.getIpmworksIndexController({
            domainEditURL: "<s:url namespace='/ipmworks/domains' action='domains-edit-input' />",
            practicedSystemEditURL: "<s:url namespace='/ipmworks/practiced' action='practiced-systems-edit-input' />",
            performanceDownloadURL: "<s:url namespace='/ipmworks/performances' action='performances-download'/>",
            locale: "<s:property value='userLocale'/>",
            baseUrl: "<s:property value='baseUrl'/>",
            frontApp: "ipmworks",
            imgFolderUrl: "<s:url value='/img'/>",
            fetchEndpoint: ENDPOINTS.performancesListJson
          }).mount('#vuejs-ipmworks-index-controller');
        });
      </script>
      <link rel="stylesheet" type="text/css"
        href="<s:url value='/nuiton-js/ipmworks-index.css' /><s:property value='getVersionSuffix()'/>" />
      <link rel="stylesheet" type="text/css"
        href="<s:url value='/webjars/font-awesome/4.7.0/css/font-awesome.min.css' />" />

    </head>

    <body>
      <div id="vuejs-ipmworks-index-controller">
        <ipmworks-main-board></ipmworks-main-board>
        <ipmworks-map></ipmworks-map>
      </div>

      <s:include value="/components/ipmworks/index/ipmworks-map.html"></s:include>
      <s:include value="/components/ipmworks/index/ipmworks-main-board.html"></s:include>
      <s:include value="/components/ipmworks/index/ipmworks-domains-board-row.html"></s:include>
      <s:include value="/components/ipmworks/index/ipmworks-performance-table.html"></s:include>

    </body>

    </html>
