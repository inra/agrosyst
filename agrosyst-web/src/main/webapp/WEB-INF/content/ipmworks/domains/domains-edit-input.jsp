<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2022 - 2024 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <%-- Out of wro because wro can't handle it --%>
    <link rel="stylesheet" href="<s:url value='/webjars/leaflet/1.7.1/dist/leaflet.css' />" />
    <script src="<s:url value='/webjars/leaflet/1.7.1/dist/leaflet-src.js' />"></script>
    <!-- Select2 theme -->
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />
    <link rel="stylesheet" type="text/css"
      href="<s:url value='/nuiton-js/ipmworks-domains.css' /><s:property value='getVersionSuffix()'/>" />

    <script src="<s:url value='/js/map/leaflet-providers.js' />"></script>
    <script type="text/javascript" src="<s:url value='/nuiton-js/ipmworks-domains.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">

      function displaySelectedDomain(selectDomains) {
        var topiaId = $(selectDomains).children("option:selected").val();
        location.href="<s:url namespace='/ipmworks/domains' action='domains-edit-input' />?domainTopiaId=" + topiaId;
      }

      function exportDomain(domainId) {
        var domainIds = [];
        domainIds[0]=domainId;
        location.href="<s:url namespace='/ipmworks/domains' action='domains-export' />?domainIds=" + domainIds;
      }

      <s:if test='speciesToArea == null || "".equals(speciesToArea) || "{{speciesDistribution}}".equals(speciesToArea)'>
        var speciesDistribution = {};
      </s:if>
      <s:else>
        var speciesDistribution = <s:property value='speciesToArea' escapeHtml="false" />;
      </s:else>

      angular.module('DomainEditModule', ['Agrosyst', 'ui.date', 'ngSanitize', 'ui.select', 'ngAnimate', 'ui.switch'])
             .value('Domain', <s:property value='toJson(domain)' escapeHtml="false" />)
             .value('frontApp', "ipmworks")
             .value('campaignsBounds', <s:property value="toJson(campaignsBounds)" escapeHtml="false"/>)
             .value('PricesInitData', {
               readOnly: true,
               helpMessage: "<br><s:text name='domain-edit-input-price-are' /> <s:if test='domain.campaign==0'><s:text name='domain-edit-input-actually-edited' /></s:if><s:else><s:property value='domain.campaign' /> (<s:property value='(domain.campaign)-1' /> - <s:property value='domain.campaign' />)</s:else>.",
               pricesLoadExclusion: "excludeDomainId=<s:property value='domain.topiaId' escapeHtml='false' />",
             });

      window.addEventListener("DOMContentLoaded", () => {
        const domain = <s:property value="toJson(domain)" escapeHtml="false"/>;

        AgrosystVueComponents.Domains.frontApp="ipmworks";

        AgrosystVueComponents.Domains.getDomainContextController({
            domain,
            forFrance: false,
            frontApp: "ipmworks",
            types: <s:property value="toJson(types)" escapeHtml="false"/>,
            countries: <s:property value="toJson(countries)" escapeHtml="false"/>,
            countryTopiaId: "<s:property value='countryTopiaId' escapeHtml='false'/>",
            franceTopiaId: "<s:property value='franceTopiaId' escapeHtml='false'/>",
            domainGpsData: <s:property value='toJson(geoPoints)' escapeHtml="false" />,
            zoningValues: <s:property value='toJson(zoningValues)' escapeHtml="false" />,
            formatedDepartement: "<s:property value='formatedDepartement' escapeHtml='false' />",
            formatedPetiteRegionAgricoleName: "<s:property value='formatedPetiteRegionAgricoleName' escapeHtml='false' />",
            weatherStationSitesIdsAndNames: <s:property value='toJson(allRefStationMeteoIdsAndNames)' escapeHtml="false" />,
            weatherStations: <s:property value='toJson(weatherStations)' escapeHtml="false" />,
            countryChangeCallback: function(countryId) {
              fieldPropertiesController.countryTopiaId = countryId;
              inputsController.context.countryTopiaId = countryId;
            }
        }).mount('#tab_0');

        AgrosystVueComponents.Domains.getDomainCharacteristicsController({
            domain,
            forFrance: false,
            frontApp: "ipmworks",
            allRefLegalStatus: <s:property value="toJson(allRefLegalStatus)" escapeHtml="false"/>,
            domainSiret: "<s:property value='domainSiret' escapeHtml='false'/>",
            otex18s: <s:property value="toJson(otex18s)" escapeHtml="false"/>,
            otex70s: <s:property value="toJson(otex70s)" escapeHtml="false"/>,
            meadowAreaCheckHelpMessage: "<s:text name='help.domain.meadowArea.check' />",
            // TODO remplacer ce hack quand tout sera sous vue
            otex18callback: function(newValue) {
              angular.element("#domainEditForm").scope().updateShowLivestockUnits(newValue);
              angular.element("#domainEditForm").scope().$apply();
            }
        }).mount('#tab_1');

        const fieldPropertiesController = AgrosystVueComponents.Domains.getDomainFieldPropertiesListController({
            domain,
            maxSlopes: <s:property value='toJson(maxSlopes)' escapeHtml="false" />,
            waterFlowDistances: <s:property value='toJson(waterFlowDistances)' escapeHtml="false" />,
            irrigationSystemTypes: <s:property value='toJson(irrigationSystemTypes)' escapeHtml="false" />,
            pompEngineTypes: <s:property value='toJson(pompEngineTypes)' escapeHtml="false" />,
            hosesPositionnings: <s:property value='toJson(hosesPositionnings)' escapeHtml="false" />,
            frostProtectionTypes: <s:property value='toJson(frostProtectionTypes)' escapeHtml="false" />,
            solTextures: <s:property value='toJson(solTextures)' escapeHtml="false" />,
            solProfondeurs: <s:property value='toJson(solProfondeurs)' escapeHtml="false" />,
            solStoninesses: <s:property value='toJson(solStoninesses)' escapeHtml="false" />,
            solWaterPhs: <s:property value='toJson(solWaterPhs)' escapeHtml="false" />,
            solCaracteristiques: <s:property value='toJson(solCaracteristiques)' escapeHtml="false" />,
            adjacentElements: <s:property value='toJson(adjacentElements)' escapeHtml="false" />,
            countryTopiaId: "<s:property value='countryTopiaId' escapeHtml='false'/>",
            franceTopiaId: "<s:property value='franceTopiaId' escapeHtml='false'/>",
            forFrance: false,
            frontApp: "ipmworks"
        }).mount('#tab_3');

        AgrosystVueComponents.Domains.getDomainLivestockUnitsController({
          domain,
          forFrance: false,
          frontApp:"ipmworks"
        }).mount('#tab_2');

        const toolsController = AgrosystVueComponents.Domains.getDomainToolsController({
          domain,
          downloadToolsUrl: "<s:url namespace='/referential' action='materiel-referential-download'/>",
          domainTypes: <s:property value="toJson(types)" escapeHtml="false" />,
          forFrance: false,
          frontApp: "ipmworks"
        }).mount('#tab_4');

        if ($("#tabs-tools-li")[0].className === "selected") {
          toolsController.initMaterialTab();
        }

        $('#tabs-tools-li').click(function () {
          toolsController.initMaterialTab();
        });

        const croppingPlanController = AgrosystVueComponents.Domains.getDomainCroppingPlansController({
          domain,
          forFrance: false,
          frontApp: "ipmworks",
          domainTypes: <s:property value="toJson(types)" escapeHtml="false" />,
          downloadSpeciesUrl: "<s:url namespace='/referential' action='species-referential-download'/>"
        }).mount('#tab_5');

        // force diagram redraw on tab switch
        if ($("#tabs-crops-li")[0].className === "selected") {
          croppingPlanController.initCropTab();
        }

        $('#tabs-crops-li').click(function () {
          croppingPlanController.initCropTab();
        });

        const inputsController = AgrosystVueComponents.Domains.getDomainsEditInputInputsController({
            domainId: "<s:property value='domain.topiaId' escapeHtml='false' />",
            campaign: "<s:property value='domain.campaign' escapeHtml='false' />",
            countryTopiaId: "<s:property value='countryTopiaId' escapeHtml='false'/>",
            relatedDomains: <s:property value='toJson(relatedDomains)' escapeHtml='false' />,
            inputFetchEndpoint: ENDPOINT_DOMAINS_EDIT_INPUT_STOCK_CONTENT_JSON,
            loadFertiMineJson: ENDPOINTS.loadFertiMineJson,
            loadMineralProductElementJson: ENDPOINTS.loadMineralProductElementJson,
            loadFertiOrgaJson: ENDPOINTS.loadFertiOrgaJson,
            loadPhytoProductsJson: ENDPOINT_LOAD_PHYTO_PRODUCTS_JSON,
            loadBiologicalControlInputs: ENDPOINT_LOAD_BIOLOGICAL_CONTROL_INPUTS,
            loadInputRefPrices: ENDPOINT_LOAD_INPUT_REF_PRICES_JSON,
            downloadPhytoRef: ENDPOINT_DOWNLOAD_PHYTO_REF,
            loadRefPotsJson: ENDPOINT_LOAD_REF_POTS_JSON,
            loadRefSubstratesJson: ENDPOINT_LOAD_REF_SUBSTRATES_JSON,
            loadRefOtherInputsJson: ENDPOINT_LOAD_REF_OTHER_INPUTS_JSON,
            loadCropsContextJson: ENDPOINT_DOMAINS_EDIT_CROPS_CONTEXT_JSON,
            loadSeedingSpeciesAndProductsUsages: ENDPOINT_LOAD_SEEDING_SPECIES_AND_PRODUCTS_USAGES_JSON,
            loadSpeciesUnits: ENDPOINT_LOAD_SPECIES_UNIT_JSON,
            forFrance: false,
            frontApp: "ipmworks",
            i18n: I18N
        }).mount('#vuejs-inputs-app');

        const plotsController = AgrosystVueComponents.Domains.getDomainPlotsController({
          domain,
          forFrance: false,
          frontApp:"ipmworks",
        }).mount('#tab_7');

        // load plots
        $('#tabs-plots-li').click(function () {
          plotsController.intiPlotsTab();
        });
        if ($("#tabs-plots-li")[0].className === "selected") {
          plotsController.intiPlotsTab();
        };

        // onglet prix
        $('#tabs-prices-li').click(function(){
          angular.element('#tab_8').scope().initPricesTab();
        });
        if ($("#tabs-prices-li")[0].className==="selected") {
          angular.element('#tab_8').scope().initPricesTab();
        };
      });
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function notifyInactiveDomaine() {
            addPermanentWarning("Le domaine sur lequel vous travaillez est inactif. Réactivez le pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

    <s:if test="domain.topiaId == null">
      <title><s:text name="domain-new-title" /></title>
    </s:if>
    <s:else>
      <title><s:text name="domain-edit-title" /> '<s:property value="domain.name" />'</title>
    </s:else>
    <content tag="current-category">contextual</content>
  </head>

  <body>
    <div ng-app="DomainEditModule" class="page-content">
      <div id="filAriane">
        <ul class="clearfix">
          <li>
            <a href="<s:url action='index' namespace='/ipmworks' />">
              <span class="icone-home">
                <s:text name="breadcrumb-home" />
              </span>
            </li>
            <li>
              &gt; <s:text name="breadcrumb-farms" />
            </a>
          </li>
          <s:if test="domain.topiaId == null">
            <li>&gt; <s:text name="breadcrumb-new-farm" /></li>
          </s:if>
          <s:else>
            <li>&gt; <s:property value="domain.name" /></li>
          </s:else>
        </ul>
      </div>

      <s:if test="%{domain.topiaId != null}">
        <ul class="float-right informations">
          <li>
            <span class="label">
              <s:text name="domain-information-farm" />
              <s:if test="!activated">&nbsp;<span class="unactivated">(<s:text name="domain-information-farm-inactive" />)</span></s:if>
            </span>
            <a href="<s:url namespace='/ipmworks/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='domain.topiaId'/>"
                title="<s:text name='domain-information-farm-title' />">
              <s:property value="domain.name" />
            </a>
          </li>
          <s:fielderror fieldName="domain.campaign" />
          <li>
            <span class="label"><s:text name="domain-information-campaign" /></span>
            <s:property value="domain.campaign" /> (
            <s:property value="(domain.campaign)-1" /> -
            <s:property value="domain.campaign" />)
          </li>
          <li><span class="label"><s:text name="domain-information-farm-type" /></span>
            <s:text name="fr.inra.agrosyst.api.entities.DomainType.%{domain.type}" />
          </li>

          <li class="highlight">
            <span class="label"><s:text name="domain-information-responsibles" /></span>
            <a id="domainResponsiblesLink" class="action-admin-roles"
               onclick="editEntityRoles('DOMAIN_RESPONSIBLE', '<s:property value='domain.code' />', '<s:text name='entity-role-popup-title-domainResponsibles' />')"
               title="<s:text name='domain-information-responsibles-title' />">
              <s:text name="domain-information-responsibles-list" />
            </a>
          </li>
          <li class="highlight">
            <span class="label"><s:text name="domain-information-attachments" /></span>
            <a id="attachmentLink"
               class="action-attachements"
               onclick="displayEntityAttachments('<s:property value='domain.code' />', '<s:text name='attachments.attachmentFile' />')"
               title="<s:text name='domain-information-attachments-title' />">
              <s:property value="getAttachmentCount(domain.code)" />
            </a>
          </li>
        </ul>
      </s:if>

      <s:if test="relatedDomains != null">
        <ul class="timeline">
          <s:iterator value="relatedDomains" var="relatedDomain">
            <li<s:if test="#relatedDomain.value.equals(domain.topiaId)"> class="selected"</s:if>>
              <a href="<s:url namespace='/ipmworks/domains' action='domains-edit-input' />?domainTopiaId=<s:property value='value'/>">
                <s:property value="key" />
              </a>
            </li>
          </s:iterator>
        </ul>
      </s:if>

      <form name="domainEditForm"
            action="<s:url action='domains-edit' namespace='/ipmworks/domains' />"
            method="post" class="tabs clear"
            ng-controller="DomainMainController"
            id="domainEditForm"
            ag-confirm-on-exit>

        <s:actionerror escape="false" cssClass="send-toast-to-js"/>
        <s:hidden id="domainTopiaId" name="domainTopiaId" value="%{domain.topiaId}"/>
        <div ng-if="fieldValidateErrors">Vous avez des erreurs de validation</div>
        <ul id="tabs-domains-menu" class="tabs-menu clearfix">
          <li class="selected"><span><s:text name="domain-edit-context"/></span></li>
          <li><span><s:text name="domain-edit-characteristics"/></span></li>
          <li id="livestockUnits-li" ng-show="showLivestockUnits"><span><s:text name="domain-edit-livestockunits"/></span></li>
          <li><span><s:text name="domain-edit-field-properties"/></span></li>
          <li id="tabs-tools-li"><span><s:text name="domain-edit-tools"/></span></li>
          <li id="tabs-crops-li"><span><s:text name="domain-edit-crops"/></span></li>
          <li id="tabs-intrants-li"><span><s:text name="domain-edit-inputs"/></span></li>
          <li id="tabs-plots-li"><span><s:text name="domain-edit-plots"/></span></li>
          <li id="tabs-prices-li"><span><s:text name="domain-edit-prices"/></span></li>
        </ul>

        <div id="tabs-domains-content" class="tabs-content">
          <!-- Contexte -->
          <div ng-non-bindable id="tab_0">
            <s:include value="/components/domains/domain-context.html"></s:include>
          </div>

          <!-- Caractéristiques -->
          <div ng-non-bindable id="tab_1">
            <s:include value="/components/domains/domain-characteristics.html"></s:include>
          </div>

          <!-- Ateliers d'élevage -->
          <div ng-non-bindable id="tab_2" ng-show="showLivestockUnits">
            <s:include value="/components/domains/domain-livestockunits.html"></s:include>
          </div>

          <!-- Sols -->
          <div ng-non-bindable id="tab_3">
            <domain-field-properties></domain-field-properties>
          </div>

          <!-- Matériels / Combinaisons d'outils -->
          <div ng-non-bindable id="tab_4">
            <s:include value="/components/domains/domain-tools.html"></s:include>
          </div>

          <!-- Assolement -->
          <div ng-non-bindable id="tab_5">
            <s:include value="/components/domains/domain-cropping-plans.html"></s:include>
          </div>

          <!-- Intrants et prix -->
          <div ng-non-bindable id="tab_6">
            <%@include file="../../domains/domains-edit-input-inputs.jsp" %>
          </div>

          <!-- Parcelles -->
          <div ng-non-bindable id="tab_7">
            <s:include value="/components/domains/domain-plots.html"></s:include>
          </div>

          <%-- Prix --%>
          <!-- id is used to parse tab index from it s name -->
          <div id="tab_8" ng-controller="PricesController" class="page-content">
            <%@include file="../../domains/prices.jsp" %>
          </div>

          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url namespace='/ipmworks' action='index' />"><s:text name='common-cancel'/></a>
            <input type="submit" class="btn-primary" value="<s:text name='common-save'/>" id="submitDomainInput"
              <s:if test="readOnly">
                disabled="disabled"
                title="Vous n'avez pas les droits nécessaires"
              </s:if>
              <s:if test="!activated">
                disabled="disabled"
                title="Le domaine sur lequel vous travaillez est inactif. Réactivez le pour pouvoir apporter des modifications"
              </s:if>
            />
          </span>
        </div>
      </form>

      <div id="show-location" title="Affichage d'une position GPS" class="auto-hide">
        <s:text name='common-loading'/>...
      </div>

      <div id="validationFieldsError" title="ATTENTION: certains champs obligatoires ne sont pas renseignés, l'enregistrement ne peut se faire !" class="auto-hide"></div>
    </div>

    <s:include value="/components/domains/domain-coordinates.html"></s:include>
    <s:include value="/components/domains/domain-weatherstations.html"></s:include>
    <s:include value="/components/domains/domain-field-properties.html"></s:include>
    <s:include value="/components/domains/domain-tools-coupling-edition.html"></s:include>
    <s:include value="/components/domains/domain-tools-tool-edition.html"></s:include>
    <s:include value="/components/domains/domains-to-paste.html"></s:include>
    <s:include value="/components/domains/domain-tools-copy-modal.html"></s:include>
    <s:include value="/components/common/generic-step-progress-bar.html"></s:include>
    <s:include value="/components/common/generic-modal.html"></s:include>
  </body>
</html>
