<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2023 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Authentification</title>
    <script type="text/javascript">

      function requestForgottenPassword(next) {
        var email = $('input[name=email]', '#loginForm').val();
        $(location).attr('href', ENDPOINTS.forgottenPassword + "?email=" + email + "&next=" + next);
      }

      function changeLanguage(language) {
        const searchParams = (new URL(location)).searchParams;
        searchParams.set('lang', language);
        location.replace(location.origin + location.pathname + '?' + searchParams.toString());
      }

    </script>
  </head>
  <body>
      <div class="login-panel">
        <div class="pannel-top">
          <div><img id="logo-ecophyto" src="<s:url value='/img/Logo_AGROSYST_small.png' />" alt="logo-agrosyst"/></div>
          <div><img id="logo-ecophyto" src="<s:url value='/img/logo-ipmworks.png' />" alt="logo-ipmworks"/></div>
          <p><s:text name="login-message" /></p>
        </div>
        <form id="loginForm" action="<s:url action='login' namespace='/ipmworks/auth' />" method="post">
          <s:actionmessage/>
          <s:actionerror escape="false"/>
          <div class="fields">
            <s:select key="login-language"
                      name="lang" value="%{lang}" labelPosition="left"
                      list="languages" listKey="trigram" listValue="localNameWithEnglishName"
                      cssClass="login-language"
                      onchange="changeLanguage(event.target.value)" />
            <s:hidden name="next" value="%{next}" />
            <s:textfield type="email" key="login-email" labelPosition="left" id="email" name="email" value="%{email}" placeholder="%{getText('login-email')}" title="%{getText('login-email')}" required="true" cssClass="login-username"/>
            <s:password key="login-password" labelPosition="left" name="password" required="true" cssClass="login-password" placeholder="%{getText('login-password')}" title="%{getText('login-password')}"/>
            <a href="" onclick="requestForgottenPassword('<s:property value='next'/>');return false;"><s:text name="login-forgotten-password" /></a>
          </div>
          <input type="submit" class="btn-primary" value="<s:text name='login-connection' />"/>
        </form>
      </div>
  </body>
</html>
