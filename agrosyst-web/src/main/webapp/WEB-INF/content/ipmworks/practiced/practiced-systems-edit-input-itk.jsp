<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!--practiced-systems-edit-input-itk.jsp-->

<div ng-controller="ItkController" id="tab_3" class="page-content">

  <div ng-show="phasesAndConnectionsArray.length === 0" class="empty-table">
    <s:text name="practicedSystem.itks.missingPreviousData" />
  </div>
  <div class="table-enclosure paddingTop0" ng-show="phasesAndConnectionsArray.length > 0">
    <label><s:text name="practicedSystem.itks.table.title" />&nbsp;:
      <span class='contextual-help'>
        <span class='help-hover'>
          <s:text name="help.practiced-systems.cropCoupleNotUsedForThisCampaign"/>
        </span>
      </span>
    </label>
    <table class="data-table">
      <thead>
        <tr>
          <th scope="col" class="column-xsmall"><s:text name="practicedSystem.itks.table.cropType.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.table.crop.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.table.previousCrop.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.table.interventionNb.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.table.missingCrop.title" /></th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="phaseOrConn in phasesAndConnectionsArray"
            ng-click="editPhaseOrConnection(phaseOrConn)"
            ng-class="{'selected-line' : selectedPhaseConnectionOrNode === phaseOrConn, 'line-error': !validateCycle(phaseOrConn)}"
            class="selectCursor">
          <td>
            <div ng-if="!phaseOrConn.type" class="cell-on-one-row center">
              <div ng-if="phaseOrConn.intermediateCroppingPlanEntryCode"
                   class="textIcones INTERMEDIATE"
                   title="<s:text name='practicedSystem.itks.table.cropType.intermediate.label' /> ({{ croppingPlanIndex[phaseOrConn.intermediateCroppingPlanEntryCode].label }})"/>
                <s:text name='practicedSystem.itks.table.cropType.intermediate.abbr' />
              </div>
              <div ng-if="phaseOrConn.intermediateCroppingPlanEntryCode" style="margin-left: 10px" />
                /
              </div>
              <div ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).type === 'MAIN'"
                   class="textIcones MAIN"
                   title="<s:text name='practicedSystem.itks.table.cropType.main.label' />{{getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.species.label' />' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.variety.label' />' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.main.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr' />
                </span>
              </div>
              <div ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).type === 'CATCH'"
                   class="textIcones CATCH"
                   title="<s:text name='practicedSystem.itks.table.cropType.catch.label' />{{getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.species.label' />' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.variety.label' />' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.catch.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr' />
                </span>
              </div>
            </div>
            <div ng-if="phaseOrConn.type" class="cell-on-one-row center">
              <div ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).type === 'MAIN'"
                   class="textIcones MAIN"
                   title="<s:text name='practicedSystem.itks.table.cropType.main.label' />{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.species.label' />' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.variety.label' />' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.main.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr' />
                </span>
              </div>
              <div ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).type === 'CATCH'"
                   class="textIcones CATCH"
                   title="<s:text name='practicedSystem.itks.table.cropType.catch.label' />{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies ? ' - Mélange d\'espèces' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - Mélange de variétés' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.catch.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr' />
                </span>
              </div>
            </div>
          </td>
          <td ng-if="phaseOrConn.type">{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).label}} ({{perennialPhaseTypes[phaseOrConn.type]}})</td>
          <td ng-if="!phaseOrConn.type">
            {{phaseOrConn.intermediateCroppingPlanEntryCode ?
                "(" + croppingPlanIndex[phaseOrConn.intermediateCroppingPlanEntryCode].label + ") " :
                ""}}
            {{connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].label}} (<s:text name='common-rank' /> {{connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].x + 1}})
          </td>
          <td>{{phaseOrConn.type ? "-" : connectionNodeIdToNodeObjectMap[phaseOrConn.sourceId].label}}</td>
          <td>{{phaseOrConn.interventions.length}}</td>
          <td>
            <span ng-if ="!phaseOrConn.type">
              <input type="checkbox"
                     ng-model="phaseOrConn.notUsedForThisCampaign"
                     ng-disabled="phaseOrConn.interventions.length > 0" />
            </span>
            <span ng-if="phaseOrConn.type"> - </span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="table-enclosure paddingTop0" ng-show="selectedPhaseConnectionOrNode">

    <label>
      <s:text name='practicedSystem.itks.itk.label' />
      <span ng-show="!selectedPhaseConnectionOrNode.type">
        <s:text name='practicedSystem.itks.itk.crop.label' />
        {{connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].label + ' (<s:text name="common-rank" /> ' + (connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].x + 1) + ')'}}
      </span>
      <span ng-show="selectedPhaseConnectionOrNode.type">
        <s:text name='practicedSystem.itks.itk.phase.label' />
        {{perennialPhaseTypes[selectedPhaseConnectionOrNode.type]}}
      </span>
      &nbsp;:
      <span class='contextual-help'>
        <span class='help-hover'>
          <s:text name="help.practicedSystemItk.interventions" />
        </span>
      </span>
    </label>

    <table class="data-table">
      <thead>
        <tr>
          <th scope="col"
              title="{{allInterventionSelected.selected && '<s:text name="common-table-unselect-all" />' || '<s:text name="common-table-select-all" />'}}">
            <input type='checkbox'
                   ng-model="allInterventionSelected.selected"
                   ng-change="toggleSelectedInterventions()" />
          </th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.name.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.action.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.dates.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.intermediate.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.temporalFreq.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.spatialFreq.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.treatedAreaProportion.title" /></th>
          <th scope="col" title="<s:text name="help.PSCi" />"><s:text name="practicedSystem.itks.itk.table.psci.title" /></th>
          <th scope="col"><s:text name="practicedSystem.itks.itk.table.data.title" /></th>
          <th scope="col"><s:text name="common-delete-abbr" /></th>
        </tr>
      </thead>

      <tbody ui-sortable ng-model="selectedPhaseConnectionOrNode.interventions">
        <tr ng-if="!selectedPhaseConnectionOrNode.notUsedForThisCampaign && selectedPhaseConnectionOrNode.interventions.length === 0" >
          <td colspan="11" class="empty-table">
            <s:text name="practicedSystem.itks.itk.table.empty" />
          </td>
        </tr>
        <tr ng-if="selectedPhaseConnectionOrNode.notUsedForThisCampaign">
          <td colspan="11" class="empty-table">
            <s:text name="practicedSystem.itks.itk.table.crop.absent1"/> {{connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].label + ' (<s:text name="common-rank" /> ' + (connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].x + 1) + ')'}} <s:text name="practicedSystem.itks.itk.table.crop.absent2"/>.
          </td>
        </tr>
        <tr ng-if="!selectedPhaseConnectionOrNode.notUsedForThisCampaign && selectedPhaseConnectionOrNode.interventions.length > 0"
            ng-repeat="intervention in selectedPhaseConnectionOrNode.interventions"
            class="selectCursor"
            ng-class="{'selected-line' : editedIntervention == intervention, 'line-selected':selectedInterventions[intervention.topiaId], 'line-error': !validateIntervention(intervention)}"
            ng-click="editIntervention(intervention);selectedInterventions[intervention.topiaId] = !selectedInterventions[intervention.topiaId]"
            >
          <td>
            <input type='checkbox'
                   ng-checked="selectedInterventions[intervention.topiaId]"
                   ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid"/>
            <input ng-if="!validateIntervention(intervention)"
                   id="interventionError_{{$index}}"
                   type="text"
                   ng-model="noIntervention"
                   required
                   style="opacity:0;width: 0;padding: 0px;border-width: 0px;"/>
          </td>
          <td>{{ intervention.name|orDash }}</td>
          <td ng-click="intervention.showCompleteActionList = !intervention.showCompleteActionList">
            <ul ng-if="!intervention.showCompleteActionList" class="actions-list">
              <li ng-repeat="action in intervention.actions | limitTo:3">
                <span>
                  {{ agrosystInterventionTypes[action.mainAction.intervention_agrosyst] }} / {{ action.mainAction.reference_label_Translated | orDash }}
                </span>
              </li>
            </ul>
            <ul ng-if="intervention.showCompleteActionList" class="actions-list">
              <li ng-repeat="action in intervention.actions">
                <span>
                  {{ agrosystInterventionTypes[action.mainAction.intervention_agrosyst] }} / {{ action.mainAction.reference_label_Translated | orDash }}
                </span>
              </li>
            </ul>
            <span ng-if="!intervention.showCompleteActionList && intervention.actions.length > 3">{{  intervention.actions.length-3 }} <s:text name="practicedSystem.itks.itk.table.additionalActions" /></span>
            <span ng-if="!intervention.actions || intervention.actions.length === 0">-</span>
          </td>
          <td ng-if="!intervention.startingPeriodDate && !intervention.endingPeriodDate">-</td>
          <td ng-if="!intervention.startingPeriodDate && intervention.endingPeriodDate">
            <s:text name="practicedSystem.itks.itk.table.date.before" /> {{intervention.endingPeriodDate}}
          </td>
          <td ng-if="intervention.startingPeriodDate && !intervention.endingPeriodDate">
            <s:text name="practicedSystem.itks.itk.table.date.after" /> {{intervention.startingPeriodDate}}
          </td>
          <td ng-if="intervention.startingPeriodDate && intervention.startingPeriodDate == intervention.endingPeriodDate">
            <s:text name="practicedSystem.itks.itk.table.date.day" /> {{intervention.startingPeriodDate}}
          </td>
          <td ng-if="intervention.startingPeriodDate && intervention.endingPeriodDate && intervention.startingPeriodDate != intervention.endingPeriodDate">
            <s:text name="practicedSystem.itks.itk.table.date.from" /> {{intervention.startingPeriodDate}} <s:text name="practicedSystem.itks.itk.table.date.to" /> {{intervention.endingPeriodDate}}
          </td>
          <td>{{ intervention.intermediateCrop && '<s:text name="common-yes" />' || '-' }}</td>
          <td>{{ (intervention.temporalFrequency | number:2) | orDash }}</td>
          <td>{{ (intervention.spatialFrequency | number:2) | orDash }}</td>
          <td>
            <div ng-if="intervention.proportionOfBioTreatedSurface">
              <s:text name="practicedSystem.itks.itk.table.biocontrol" />&nbsp;: {{ (intervention.proportionOfBioTreatedSurface | number:0) }}&nbsp%
            </div>
            <div ng-if="intervention.proportionOfPhytoTreatedSurface">
              <s:text name="practicedSystem.itks.itk.table.phyto" />&nbsp;: {{ (intervention.proportionOfPhytoTreatedSurface | number:0) }}&nbsp%
            </div>
            <div ng-if="!intervention.proportionOfBioTreatedSurface && !intervention.proportionOfPhytoTreatedSurface">-</div>
          </td>
          <td title="<s:text name="help.PSCi" />" >{{ ((intervention.spatialFrequency * intervention.temporalFrequency) | number:2) | orDash }}</td>
          <td ng-click="intervention.showCompleteProductList = !intervention.showCompleteProductList"
              title="<s:text name='practicedSystem.itks.itk.table.intervention.data.more.title' />">
            <ul ng-if="!intervention.showCompleteProductList" class="products-list">
              <li ng-repeat="item in intervention.inputData | limitTo:3">
                <span ng-if="item.label">
                  {{ (item.label | orDash) | truncate:20 }}
                </span>
                <span ng-if="item.label && item.quantity">&nbsp;(</span>
                <span ng-if="item.quantity">{{ item.quantity | number:2 }}&nbsp;{{ item.unit | orDash }}</span>
                <span ng-if="item.label && item.quantity">)</span>
              </li>
            </ul>
            <ul ng-if="intervention.showCompleteProductList" class="products-list">
              <li ng-repeat="item in intervention.inputData">
                <span ng-if="item.label">
                  {{ item.label | orDash }}
                </span>
                <span ng-if="item.label && item.quantity">&nbsp;(</span>
                <span ng-if="item.quantity">{{ item.quantity | number:2 }}&nbsp;{{ item.unit | orDash }}</span>
                <span ng-if="item.label && item.quantity">)</span>
              </li>
            </ul>
            <span ng-if="!intervention.showCompleteProductList && intervention.inputData.length > 3">
              {{intervention.inputData.length - 3}} <s:text name="practicedSystem.itks.itk.table.intervention.data.additionalProducts" />
            </span>
            <span ng-if="!intervention.inputData || intervention.inputData.length === 0">-</span>
          </td>
          <td>
            <input type="button"
                   class="btn-icon icon-delete"
                   value="<s:text name='common-delete' />"
                   title="<s:text name='common-delete' />"
                   ng-click="deleteIntervention(intervention)"/>
          </td>
        </tr>
      </tbody>

      <tfoot>
        <tr ng-disabled="editedIntervention && !editedInterventionForm.$valid">
          <td colspan="4" class="table-global-info">
            <span ng-if="selectedPhaseConnectionOrNode.interventions.length > 1">
              <s:text name='practicedSystem.itks.itk.table.dragndrop' />
            </span>
          </td>
          <td colspan="7" ng-controller="ItkPracticedCopyPasteController">
            <div class="table-end-button">
              <input type="button"
                     value="<s:text name='practicedSystem.itks.itk.table.buttons.addIntervention' />"
                     ng-click="createIntervention()"
                     ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid" />
            </div>
            <div class="table-end-button">
              <input type="button"
                     value="<s:text name='practicedSystem.itks.itk.table.buttons.copyInterventions' />"
                     ng-click="copyPracticedInterventions()"
                     ng-show="(selectedInterventions|toSelectedLength) > 0"
                     class="button-copy"
                     ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid" />
            </div>
            <div class="table-end-button">
              <input type="button"
                     value="<s:text name='practicedSystem.itks.itk.table.buttons.pasteInterventions' />"
                     ng-click="pastePracticedInterventions()"
                     ng-show="pastedInterventions"
                     class="button-copy"
                     ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid" />
            </div>
            <div class="table-end-button">
              <input type="button"
                     value="<s:text name='practicedSystem.itks.itk.table.buttons.deleteInterventions' />"
                     ng-click="deleteSelectedInterventions()"
                     ng-show="(selectedInterventions|toSelectedLength) > 1"
                     class="button-delete"
                     ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid" />
            </div>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>

  <!-- Intervention sélectionnée -->
  <fieldset class="sub-form marginTop30" ng-form="editedInterventionForm" >
    <div id="new-intervention" class="entity-content sub-form-content full-width">

      <!-- Détails de l'intervention -->
      <div class="two-columns noclear">

        <!-- Type d'intervention-->
        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_type">
              <span class="required">*</span>&nbsp;<s:text name="practicedSystem.itks.itk.intervention.type" />
            </label>
          </span>
          <span class="wwctrl">
            <select id="intervention_type"
                    ng-model="editedIntervention.type"
                    ng-change="loadToolsCouplings(editedIntervention, false)"
                    ng-required="editedIntervention"
                    ng-disabled="editedIntervention.type && editedIntervention.toolsCouplingCodes.length > 0"
                    ng-attr-title="{{editedIntervention.toolsCouplingCodes.length > 0 ? '<s:text name="practicedSystem.itks.itk.intervention.type.help" />' : '' }}">
              <!-- the following line is required to enable HTML5 validation-->
              <option value=""></option>
              <option ng-repeat="(type, label) in agrosystInterventionTypes"
                      value="{{type}}"
                      title="{{ getInterventionTypeTooltip(type) }}"
                      ng-selected="editedIntervention.type === type">
                {{ label }}
              </option>
            </select>
          </span>
        </div>

        <div id="warning-no-selected-tools-coupling" title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.informationToBeTicked"/>">
          <div class="bubble-text">
            <p>
              <span class="warning_char"><i class="fa fa-warning" aria-hidden="true"></i></span>
              <span class="highlight-text-content">
                <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.informationToBeTicked"/>
                  <a class="highlight-text-content-redirect"
                     ng-click="redirectToToolsCooplingsTab()"
                     target="_blank"
                     href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='practicedSystem.growingSystem.growingPlan.domain.topiaId'/>"
                     title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>">
                     <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.viewFarm"/>
                  </a>
              </span>
            </p>
          </div>
        </div>
        <div class="wwgrp checkbox-list">
          <span class="wwlbl">
            <label>
              <span class="requiredAdvised lowProfile"
                    title="<s:text name="help.performance.required"/> : <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.performance"/>">
                &pi;
              </span>
              <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings"/>&nbsp;:
              <input class="button-refresh"
                     type="button"
                     ng-click="loadToolsCouplings(editedIntervention, false)"
                     title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.reloadDomainToolsCouplings"/>"
                     value="&#8635;"/>
            </label>
          </span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.practicedSystemItk.toolsCouplings" />
              </span>
            </span>

            <span ng-show="toolsCouplings && toolsCouplings.length > 0"
                  ng-repeat="toolsCoupling in toolsCouplings | filter: { manualIntervention: false } | orderBy: 'toolsCouplingName'">
              <input type="checkbox"
                     ng-model="editedInterventionSelectedToolCouplings[toolsCoupling.code]"
                     ng-change="toggleSelectedInterventionToolsCoupling(editedIntervention, toolsCoupling)"
                     id="toolsCoupling-{{ $index }}"/>
              <label class="checkboxLabel" for="toolsCoupling-{{$index}}">
                {{ toolsCoupling.manualIntervention ? "<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.manual" /> - " : "" }}{{ toolsCoupling.toolsCouplingName }}
              </label>
            </span>
            <span ng-show="toolsCouplings && toolsCouplings.length > 0"
                  ng-repeat="toolsCoupling in toolsCouplings |  filter: { manualIntervention: true } | orderBy: 'toolsCouplingName'">
              <input type="checkbox"
                     ng-model="editedInterventionSelectedToolCouplings[toolsCoupling.code]"
                     ng-change="toggleSelectedInterventionToolsCoupling(editedIntervention, toolsCoupling)"
                     id="intervention-{{$index}}"/>
              <label class="checkboxLabel" for="toolsCoupling-{{$index}}">
                {{ toolsCoupling.manualIntervention ? "<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.manual" /> - " : "" }}{{ toolsCoupling.toolsCouplingName }}
              </label>
            </span>
            <span id="warning-no-tools-coupling-available">
              <div class="info-text">
                  <span class="warning_char"><i class="fa fa-warning" aria-hidden="true"></i></span>
                  <div class="highlight-text-content">
                    <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.missingDomainToolsCouplings"/>
                    <a class="highlight-text-content-redirect"
                       ng-click="redirectToToolsCooplingsTab()"
                       target="_blank"
                       href="<s:url namespace='/ipmworks/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='practicedSystem.growingSystem.growingPlan.domain.topiaId'/>"
                       title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.viewFarm"/>">
                       <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.viewFarm"/>
                    </a>
                  </div>
              </div>
            </span>
            <span class="empty-list" ng-if="!editedIntervention.type">
              <s:text name="practicedSystem.itks.itk.intervention.type.empty" />
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_name">
              <span class="required">*</span>&nbsp;<s:text name="practicedSystem.itks.itk.intervention.name" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <input type="text"
                   id="intervention_name"
                   ng-model="editedIntervention.name"
                   ng-required="editedIntervention" />
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_starting_periode_date">
              <span class="required">*</span>&nbsp;<s:text name="practicedSystem.itks.itk.intervention.startDate" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <input type="text"
                   id="intervention_starting_periode_date"
                   ng-model="editedIntervention.startingPeriodDate"
                   placeholder="<s:text name="practicedSystem.itks.itk.intervention.startDate.placeholder" />"
                   ng-blur="interventionStartingDateSelected()"
                   ng-pattern="/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$/"
                   pattern="^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$"
                   ng-required="editedIntervention" />
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_ending_periode_date">
              <span class="required">*</span>&nbsp;<s:text name="practicedSystem.itks.itk.intervention.endDate" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <input type="text"
                   id="intervention_ending_periode_date"
                   ng-model="editedIntervention.endingPeriodDate"
                   placeholder="<s:text name="practicedSystem.itks.itk.intervention.endDate.placeholder" />"
                   ng-pattern="/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$/"
                   pattern="^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$"
                   ng-required="editedIntervention"
                   ng-blur="pushPeriodChangeToValorisations(editedIntervention, practicedSystem)" />
          </span>
        </div>

        <div class="wwgrp">
          <div class="wwlbl">
            <label for="intervention_comment" class="label">
              <s:text name="practicedSystem.itks.itk.intervention.comment" />&nbsp;:
            </label>
          </div>
          <span class="wwctrl">
            <textarea id="intervention_comment" ng-model="editedIntervention.comment"></textarea>
          </span>
        </div>
        <div class="wwgrp" ng-if="!selectedPhaseConnectionOrNode.type">
          <span class="wwlbl">
            <label for="intervention_intermediateCrop">
              <s:text name="practicedSystem.itks.itk.intervention.intermediate" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl" ng-if="connectionIntermediateCrop">
            <input type="checkbox"
                   id="intervention_intermediateCrop"
                   ng-model="editedIntervention.intermediateCrop"
                   ng-change="toggleIntermediateSpeciesSelection()"/></span>
          <span class="wwctrl" ng-if="!connectionIntermediateCrop">
            <input type="checkbox"
                   id="intervention_intermediateCrop"
                   ng-model="editedIntervention.intermediateCrop"
                   ng-change="toggleIntermediateSpeciesSelection()"
                   ng-disabled="true"
                   title="<s:text name="practicedSystem.itks.itk.intervention.intermediate.empty" />"/></span>
        </div>
      </div>
      <div class="two-columns two-columns-right noclear">
        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_spatialFrequency">
              <span class="required">*</span>&nbsp;
              <s:text name="practicedSystem.itks.itk.intervention.spatialFrequency" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.practicedSystemItk.spatialFrequency" />
              </span>
            </span>
            <input type="text"
                   id="intervention_spatialFrequency"
                   ng-model="editedIntervention.spatialFrequency"
                   placeholder="<s:text name="practicedSystem.itks.itk.intervention.spatialFrequency.placeholder" />"
                   ng-pattern="/^((([0])\.([0-9]*)|0)|1)$/"
                   ng-required="editedIntervention"
                   ng-change="computeSpendingTime()"
                   title="<s:text name="practicedSystem.itks.itk.intervention.spatialFrequency.title" />" />
          </span>
        </div>
        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_temporalFrequency">
              <span class="required">*</span>&nbsp;
              <s:text name="practicedSystem.itks.itk.intervention.temporalFrequency" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.practicedSystemItk.temporalFrequency" />
              </span>
            </span>
            <span class='contextual-warning clear' ng-if="editedIntervention.temporalFrequency > 1">
              <span class='help-hover'>
                <s:text name="warning.practicedSystemItk.temporalFrequency" />
              </span>
            </span>
            <input type="text"
                   id="intervention_temporalFrequency"
                   ng-required="editedIntervention"
                   ng-model="editedIntervention.temporalFrequency"
                   ng-change="computeSpendingTime()"
                   placeholder="<s:text name="practicedSystem.itks.itk.intervention.temporalFrequency.placeholder" />"
                   ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"/>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_workRate">
              <span class="requiredAdvised" title="<s:text name="help.performance.required"/> : <s:text name="help.performance.workTime" />">&pi;</span>&nbsp;
              <s:text name="practicedSystem.itks.itk.intervention.workRate" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal">  
                <input type="text" id="intervention_workRate"
                       ng-model="editedIntervention.workRate"
                       ng-change="processWorkRateChange()"
                       placeholder="<s:text name="practicedSystem.itks.itk.intervention.workRate.placeholder" />"
                       ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"/>
              </span>
              <span class="column-normal">  
                <select id="workRateUnit"
                        ng-model="editedIntervention.workRateUnit"
                        ng-options="materielWorkRateUnits[workRateUnit] for workRateUnit in workRateUnits"
                        ng-change="processWorkRateChange()">
                </select>
              </span>
              <span class='contextual-help'>
                <span class='help-hover'>
                  <s:text name="help.performance.workrate" />
                </span>
              </span>
            </div>
          </span>

          <span class="wwgrp" ng-if="editedIntervention.workRateUnit == 'H_HA' || editedIntervention.workRateUnit == 'HA_H'">
            <span class="wwlbl"><label for="revertWorkRate"></label></span>
            <span class="wwctrl">
              <div class="clearfix">
                <span class="column-normal float-left paddingRight10">  
                  <input ng-if="editedIntervention.workRate && editedIntervention.workRate != 0"
                         id="revertWorkRate"
                         disabled
                         value="{{ 1 / editedIntervention.workRate| number:3}}"
                         type="text" />
                  <input ng-if="!editedIntervention.workRate" id="revertWorkRate" disabled type="text" />
                </span>
                <span class="input-append column-normal">
                  <span class="add-on" ng-if="editedIntervention.workRateUnit == 'H_HA'">
                    {{materielWorkRateUnits['HA_H']}}
                  </span>
                  <span class="add-on" ng-if="editedIntervention.workRateUnit == 'HA_H'">
                    {{materielWorkRateUnits['H_HA']}}
                  </span>
                </span>
              </div>
            </span>
          </span>
        </div>

        <div class="wwgrp" ng-if="editedIntervention.workRateUnit === 'VOY_H'">
          <span class="wwlbl">
            <label for="transitVolume">
              <span class="requiredAdvised">&pi;</span> <s:text name="practicedSystem.itks.itk.intervention.volumePerTrip" />
            </label>
          </span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal">  
                <input type="text"
                       id="transitVolume"
                       ng-model="editedIntervention.transitVolume"
                       ng-blur="processTransitVolumeChange()"
                       ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"/>
              </span>
              <span class="column-normal">  
                <select id="transitVolumeUnit"
                        ng-model="editedIntervention.transitVolumeUnit"
                        ng-options="key as value for (key , value) in materielTransportUnits"
                        ng-change="processTransitVolumeChange()">
                </select>
              </span>
            </div>
          </span>
        </div>

        <div class="wwgrp" ng-if="editedIntervention.workRateUnit === 'BAL_H'">
          <span class="wwlbl">
            <label for="nbBalls">
              <span class="requiredIndicator" title="<s:text name='help.performance.required' /> : <s:text name='help.performance.workTime' />">*</span>&nbsp;
              <s:text name='practicedSystem.itks.itk.intervention.ballNb' />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal float-left paddingRight10">  
                <input type="text"
                       id="nbBalls"
                       ng-model="editedIntervention.nbBalls"
                       ng-blur="computeSpendingTime()"
                       ag-integer pattern="\d+"/>
              </span>
              <span class="input-append column-normal">
                <span class="add-on"><s:text name='practicedSystem.itks.itk.intervention.ballNb.unit' /></span>
              </span>
            </div>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label>
              <s:text name="practicedSystem.itks.itk.intervention.workTime" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <span class="input-append">
              <span ng-if="spendingTimeErrorMessage">
                {{spendingTimeErrorMessage}}
              </span>
              <span ng-if="!spendingTimeErrorMessage">
                {{ (editedIntervention.spendingTime | number:3) }} <s:text name="practicedSystem.itks.itk.intervention.workTime.unit" />
              </span>
            </span>
          </span>
        </div>

        <span>
          <span>
            <span class="wwlbl"><label></label></span>
            <span class="wwctrl">
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'H_HA'">
                <s:text name="practicedSystem.itks.itk.intervention.psci.HHa" />
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'HA_H'">
                <s:text name="practicedSystem.itks.itk.intervention.psci.HaH" />
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'VOY_H'">
                <s:text name="practicedSystem.itks.itk.intervention.psci.VoyH" />
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'BAL_H'">
                <s:text name="practicedSystem.itks.itk.intervention.psci.BalH" />
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'T_H'">
                <s:text name="practicedSystem.itks.itk.intervention.psci.TH" />
              </span>
            </span>
          </span>
        </span>


        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_progressionSpeed">
              <s:text name="practicedSystem.itks.itk.intervention.progressionSpeed" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <span class="input-append">
              <input type="text"
                     id="intervention_progressionSpeed"
                     ng-model="editedIntervention.progressionSpeed"
                     placeholder="<s:text name="practicedSystem.itks.itk.intervention.progressionSpeed.placeholder" />"
                     ag-integer pattern="\d+" />
              <span class="add-on"><s:text name="practicedSystem.itks.itk.intervention.progressionSpeed.unit" /></span>
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_involvedPeopleNumber">
              <s:text name="practicedSystem.itks.itk.intervention.involvedPeopleNumber" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <input type="text"
                   id="intervention_involvedPeopleNumber"
                   ng-model="editedIntervention.involvedPeopleNumber"
                   ng-change="processInvolvedPeopleNumberChange()"
                   placeholder="<s:text name="practicedSystem.itks.itk.intervention.involvedPeopleNumber.placeholder" />"
                   ag-float-positive
                   pattern="^\d+([\.,]\d+)?$" />
          </span>
        </div>
      </div>

      <!-- Actions et intrants -->
      <jsp:include page="../../itk/species-actions-and-inputs.jsp" />

    </div>
  </fieldset>

  <div id="confirmDeleteIntervention"
       title="<s:text name="practicedSystem.itks.itk.intervention.delete.confirm.title" />"
       class="auto-hide">
    <ng-pluralize count="removeInterventionsContext.interventions.length"
                  when="{'0': '', '1': '<s:text name="practicedSystem.itks.itk.intervention.delete.confirm.text.one" />', 'other': '<s:text name="practicedSystem.itks.itk.intervention.delete.confirm.text.several" />'}">
    </ng-pluralize>
    <div ng-repeat="intervention in removeInterventionsContext.interventions">
      - {{ intervention.name | orDash }}
    </div>
  </div>

  <div id="confirmDialog" title="<s:text name="common-warning" />" class="auto-hide">
    <span ng-if="warningMessage" ng-bind-html="warningMessage"></span>
  </div>

  <div id="infoDialog" title="<s:text name="common-info" />" class="auto-hide">
    <span ng-if="infoMessage" ng-bind-html="infoMessage"></span>
  </div>

</div>
