<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2023 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>

<!--practiced-systems-edit-input-seasonal.jsp-->

<div ng-controller="PracticedSeasonalCropCycleNodeController" id="tab_1">

  <div>

    <s:hidden name="practicedSeasonalCropCycleDtosJson" value="{{seasonalCropCycles}}"/>

    <div ng-if="!croppingPlanModel.length">
      <s:text name="practicedSystem.croppingPlan.empty"/>
    </div>

    <fieldset>

      <s:if test="practicedSystem.growingSystem != null">
        <div class="help-explanation">
          <s:text name="practicedSystem.seasonal.growingSystem.crops.help">
            <s:param><s:property value="practicedSystem.growingSystem.growingPlan.domain.name" /></s:param>
          </s:text>
          <s:if test="practicedSystem.campaigns != null">
            <s:text name="practicedSystem.seasonal.growingSystem.campaigns.help">
              <s:param><s:property value="practicedSystem.campaigns" /></s:param>
            </s:text>
          </s:if> :
        </div>
      </s:if>
      <div id="diagram">
        <div id="cropCycleDiagramDiv"></div>
      </div>

      <div ng-show="seasonalCropCycles && seasonalCropCycles.length>0 && seasonalCropCycles[0].cropCycleNodeDtos.length > 0" class="clearfix">
        <a ng-click="toggleGraphInfo(true)" class="btn" style="display: none;" id="graphInfosShow"><s:text name="practicedSystem.seasonal.connections.show" /></a>
        <a ng-click="toggleGraphInfo(false)" class="btn" id="graphInfosHide"><s:text name="practicedSystem.seasonal.connections.hide" /></a>
        <a ng-click="deletePracticedSeasonalCropCycleDto()" class="btn btn-darker float-right"><s:text name="practicedSystem.seasonal.cycle.delete" /></a>
      </div>

      <!-- Noeud sélectionné -->
      <div ng-if="selectedCropCycleNode" id="croppingPlanEntry" class="sub-form marginTop30">
        <div id="croppingPlanEntryDetails" class="sub-form-content">
          <a ng-click="deleteCropCycleNode(selectedCropCycleNode, true)" class="btn btn-darker float-right"><s:text name="practicedSystem.seasonal.crop.delete" /></a>

          <div class="noborder">
            <div class="croppingDetailsField">
              <label><s:text name="practicedSystem.seasonal.crop.label" />&nbsp;:</label>
              <span class="generated-content">
                {{selectedCropCycleNode.label}}
                <s:text name="practicedSystem.seasonal.growingSystem.campaigns.help">
                  <s:param>{{selectedCropCycleNode.x + 1}}</s:param>
                </s:text>
              </span>
            </div>
            <div class="croppingDetailsField">
              <label for="selectedCropCycleNodeCropEndCycle"><s:text name="practicedSystem.seasonal.cycle.end.label" />&nbsp;:</label>
              <input type="checkbox" id="selectedCropCycleNodeCropEndCycle"
                     ng-model="selectedCropCycleNode.endCycle"
                     ng-change='updateNode()' />
            </div>
            <div class="croppingDetailsField">
              <label for="selectedCropCycleNodeSameCampaign"><s:text name="practicedSystem.seasonal.crop.sameCampaign.label" />&nbsp;:</label>
              <input type="checkbox" id="selectedCropCycleNodeSameCampaign" ng-model="selectedCropCycleNode.sameCampaignAsPreviousNode"
                ng-change='updateNode()' />
            </div>
            <div class="croppingDetailsField" ng-if="selectedCropCycleNode.x == 0">
              <label for="selectedCropCycleNodeInitNodeFrequency"><s:text name="practicedSystem.seasonal.crop.frequency.label" />&nbsp;:</label>
              <span class="wwctrl">
                <span class="input-append">
                  <input type="text" id="selectedCropCycleNodeInitNodeFrequency" ng-model="selectedCropCycleNode.initNodeFrequency"
                  ng-change='updateNode()' ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" />
                  <span class="add-on">%</span>
                </span>
              </span>
            </div>
          </div>

          <!-- Liste des espèces du noeud sélectionné -->
          <div id="croppingPlanEntrySpecies" ng-if="selectedCroppingPlanEntryCode" class="paddingTop0">
            <table class="data-table full-width">
              <thead>
                <tr>
                  <th scope="col"><s:text name="practicedSystem.seasonal.crop.species.label" /></th>
                  <th scope="col"><s:text name="practicedSystem.seasonal.crop.qualifier.label" /></th>
                  <th scope="col"><s:text name="practicedSystem.seasonal.crop.seasonalType.label" /></th>
                  <th scope="col"><s:text name="practicedSystem.seasonal.crop.destination.label" /></th>
                  <th scope="col"><s:text name="practicedSystem.seasonal.crop.variety.label" /></th>
                </tr>
              </thead>
              <tbody>
                <tr ng-show="!croppingPlanEntrySpeciesIndex || !croppingPlanEntrySpeciesIndex[selectedCroppingPlanEntryCode] || croppingPlanEntrySpeciesIndex[selectedCroppingPlanEntryCode].length == 0">
                  <td colspan="6" class="empty-table"><s:text name="practicedSystem.seasonal.crop.nopsecies.message" /></td>
                </tr>
                <tr ng-repeat="cpEntrySpecies in croppingPlanEntrySpeciesIndex[selectedCroppingPlanEntryCode]">
                  <td>{{cpEntrySpecies.speciesEspece}}</td>
                  <td>{{cpEntrySpecies.speciesQualifiant|orDash}}</td>
                  <td>{{cpEntrySpecies.speciesTypeSaisonnier|orDash}}</td>
                  <td>{{cpEntrySpecies.speciesDestination|orDash}}</td>
                  <td ng-if="cpEntrySpecies.varietyLibelle">{{ cpEntrySpecies.varietyLibelle }}</td>
                  <td ng-if="!cpEntrySpecies.varietyLibelle && cpEntrySpecies.edaplosUnknownVariety"
                      class="warning-label">
                    <i class="fa fa-warning" aria-hidden="true"></i>
                    <s:text name="practicedSystem.seasonal.crop.unknownVariety.message">
                      <s:param>{{cpEntrySpecies.edaplosUnknownVariety}}</s:param>
                    </s:text>
                  </td>
                  <td ng-if="!cpEntrySpecies.varietyLibelle && !cpEntrySpecies.edaplosUnknownVariety">-</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <!-- Connection sélectionnée -->
      <div ng-if="selectedCropCycleConnection" id="intermediateCroppingPlanEntry" class="sub-form marginTop30">
        <div id="intermediateCroppingPlanEntryDetails" class="sub-form-content">
          <a ng-click="deleteCropCycleConnection(selectedCropCycleConnection, true)"
             class="btn btn-darker float-right"><s:text name="practicedSystem.seasonal.connection.delete" /></a>

          <div class="croppingDetailsField noborder">
            <label class="historicLabelStyle" for="selectedCropCycleConnectionFrequency"><s:text name="practicedSystem.seasonal.connection.frequency.label" />&nbsp;:</label>
            <span class="wwctrl">
              <input  type="text"
                      id="selectedCropCycleConnectionFrequency"
                      style="width: 5%;"
                      ng-model="selectedCropCycleConnection.croppingPlanEntryFrequency" 
                      ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$"
                      ng-change="updateConnection(selectedCropCycleConnection, selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry)" />
              <span><b>&nbsp;%&nbsp;</b></span>
              <a id="autoFreqAdjust"
                 class="fakeButton" 
                 ng-click="updateConnection(selectedCropCycleConnection, selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry, true)" 
                 title="<s:text name='practicedSystem.seasonal.connection.frequency.auto_ajustement' />">
                <i class="fa fa-wrench font-size95" aria-hidden="true"></i>
              </a>
            </span>
          </div>

          <div class="croppingDetailsField positionRelative">
            <label class="historicLabelStyle" for="selectedCropCycleConnectionNotUsedForThisCampaign">
              <s:text name="practicedSystem.seasonal.connection.missingCrop.message"/>
              <span class='contextual-help-in-label'>
                <span class='help-hover'>
                  <s:text name="help.practiced-systems.cropCoupleNotUsedForThisCampaign"/>
                </span>
              </span>
              :
            </label>
            <input type="checkbox" id="selectedCropCycleConnectionNotUsedForThisCampaign"
                   ng-model="selectedCropCycleConnection.notUsedForThisCampaign"
                   ng-attr-title="{{selectedCropCycleConnection.interventions.length > 0 ? '<s:text name="practicedSystem.seasonal.connection.withInterventions.title"/>' : ''}}"
                   ng-disabled="selectedCropCycleConnection.interventions.length > 0"
                   ng-change="updateConnection(selectedCropCycleConnection, selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry)" />

          </div>

          <div class="croppingDetailsField">
            <label class="historicLabelStyle" for="selectedCropCycleConnectionIntermediateCroppingPlanEntry">
              <s:text name="practicedSystem.seasonal.connection.intermediateCrop.label"/>&nbsp;:
            </label>
            <select id="selectedCropCycleConnectionIntermediateCroppingPlanEntry"
                ng-model="selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry"
                ng-options="item as item.label for item in intermediateCroppingPlanModel"
                ng-change="setSelectedIntermediateCroppingPlanEntryChange()">
              <option value=""></option>
            </select>
          </div>
        </div>
      </div>

      <div class="help-explanation-panel">
       <s:text name="help.practicedSystem.seasonal.drawing" />
      </div>

      <div id="confirmSeasonalCropChange"
           title="<s:text name='practicedSystem.seasonal.confirm.cropChange.title'/>"
           class="auto-hide">
        <span>
          <p ng-bind-html="seasonalCropChangeContext.warningMessages"></p>
        </span>
      </div>

      <div id="confirmRemovedSeasonalCycle" title="<s:text name='practicedSystem.seasonal.confirm.cycleDeletion.title'/>" class="auto-hide">
        <s:text name='practicedSystem.seasonal.confirm.cycleDeletion.message'/>
      </div>

    </fieldset>
  </div>
</div>
