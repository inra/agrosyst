<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2023 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <script type="text/javascript">
         function displayConfirmPassword() {
           var res = $('input[name=password]', '#userAccountForm').val();
           if (res && res.length > 0) {
             $('#confirmPassword').slideDown();
           } else {
             $('#wwgrp_confirmPassword').slideUp();
           }
         }
         displayConfirmPassword();

         function showWrongItEmailCallback(data, textStatus, jqXHR) {
           if (textStatus !== "success" || !data) {
             displayWrongEmail();
           }
         }

         function validItEmail() {
             var itEmail = $("input[name='user.itEmail']", '#userAccountForm').val();
             if (itEmail.length > 0) {
               var emailValidationObject = {email: itEmail};
               $.post(ENDPOINTS.validEmailJson, emailValidationObject, showWrongItEmailCallback, "json");
             }
         }

     </script>
     <title><s:text name="user-account-title"/></title>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li>
          <a href="<s:url action='index' namespace='/ipmworks' />" class="icone-home">
            <s:text name="breadcrumb-home"/>
          </a>
        </li>
        <li>&gt; <s:text name="user-account-title"/></li>
      </ul>
    </div>

    <ul class="actions">

    </ul>

    <s:actionmessage/>
    <s:actionerror cssClass="send-toast-to-js"/>

    <form id="userAccountForm" action="<s:url action='user-account' namespace='/ipmworks/user' />" method="post" class="tabs clear" ag-confirm-on-exit>
      
      <ul id="tabs-profil-menu" class="tabs-menu clearfix">
        <li class="selected"><span><s:text name="user-account-profile"/></span></li>
        <li><span><s:text name="user-account-roles"/></span></li>
      </ul>

      <div id="tabs-profil-content" class="tabs-content">
        <div>
          <fieldset>
            <s:textfield type="email"
                         label="%{getText('user-account-email')}"
                         name="user.email"
                         labelPosition="left"
                         labelSeparator=" :"
                         placeholder="%{getText('user-account-email-placeholder')}"
                         required="true"
                         requiredLabel="true" />
            <s:password label="%{getText('user-account-password')}"
                        onchange="displayConfirmPassword()"
                        name="password"
                        labelPosition="left"
                        labelSeparator=" :"
                        autocomplete="off" />
            <s:password id="confirmPassword"
                        label="%{getText('user-account-password-repeat')}"
                        name="confirmPassword"
                        labelPosition="left"
                        labelSeparator=" :"
                        autocomplete="off" />
            <s:textfield label="%{getText('user-account-firstName')}"
                         name="user.firstName"
                         labelPosition="left"
                         labelSeparator=" :"
                         placeholder="%{getText('user-account-firstName-placeholder')}"
                         required="true"
                         requiredLabel="true"
                         autocomplete="off" />
            <s:textfield label="%{getText('user-account-lastName')}"
                         name="user.lastName"
                         labelPosition="left"
                         labelSeparator=" :"
                         placeholder="%{getText('user-account-lastName-placeholder')}"
                         required="true"
                         requiredLabel="true"
                         autocomplete="off" />
            <s:textfield label="%{getText('user-account-organisation')}"
                         name="user.organisation"
                         labelPosition="left"
                         labelSeparator=" :"
                         placeholder="%{getText('user-account-email-placeholder')}"
                         autocomplete="off" />
            <s:textfield label="%{getText('user-account-phoneNumber')}"
                         name="user.phoneNumber"
                         labelPosition="left"
                         labelSeparator=" :"
                         placeholder="%{getText('user-account-phoneNumber-placeholder')}"
                         autocomplete="off" />
            <s:select label="%{getText('user-account-userLang')}"
                      name="user.userLang"
                      value="%{user.userLang}"
                      labelPosition="left"
                      labelSeparator=" :"
                      list="languages"
                      listKey="trigram"
                      listValue="names" />
          </fieldset>

          <span class="form-buttons">
            <input type="submit" class="btn-primary" value="<s:text name='common-save'/>" />
          </span>
        </div>

        <!-- Rôles -->

        <div class="table-enclosure clear">
          <table class="data-table inline-edition full-width" id="users-list-table">
            <thead>
              <tr>
                <th scope="col" class="column-xlarge-fixed"><s:text name="user-account-role"/></th>
                <th scope="col"><s:text name="user-account-entity"/></th>
              </tr>
            </thead>

            <tbody>
              <s:iterator value="userRoles" var="role">
                <tr>
                  <td class="column-xlarge-fixed">${roleTypes[role.type]}</td>
                  <td>
                    <s:if test="%{ #role.entity.label != null }">${role.entity.label}</s:if>
                    <s:else>-</s:else>
                    <s:if test="%{ isToutesCampagnes(#role) }"> (Toutes campagnes)</s:if>
                    <s:if test="%{ isRoleCampagne(#role) }"> (${role.entity.campaign})</s:if>
                  </td>
                </tr>
              </s:iterator>
            </tbody>
          </table>
        </div>
      </div>
    </form>
  </body>
</html>
