<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2022 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <s:if test="growingPlan.topiaId == null">
      <title>Nouveau dispositif</title>
    </s:if>
    <s:else>
      <title>Dispositif '<s:property value="growingPlan.name" />'</title>
    </s:else>
    <content tag="current-category">contextual</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/growingPlans.js' /><s:property value='getVersionSuffix()'/>"></script>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/growingPlans.css' /><s:property value='getVersionSuffix()'/>" />
    <!-- Select2 theme -->
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />
    <script type="text/javascript">
      angular.module('GrowingPlanEditModule', ['Agrosyst', 'ngSanitize', 'ui.select'])
      .value('growingPlan', <s:property value='toJson(growingPlan)' escapeHtml='false'/>);
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("Le dispositif sur lequel vous travaillez est inactif et/ou est lié à un domaine inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url action='growing-plans-list' namespace='/growingplans' />">Dispositifs</a></li>
        <s:if test="growingPlan.topiaId == null">
          <li>&gt; Nouveau dispositif</li>
        </s:if>
        <s:else>
          <li>&gt; <s:property value="growingPlan.name" /></li>
        </s:else>
      </ul>
    </div>

    <ul class="actions">
      <li><a class="action-retour" href="<s:url action='growing-plans-list' namespace='/growingplans' />">Retour à la liste des dispositifs</a></li>
    </ul>

    <ul class="float-right informations">
      <s:if test="%{growingPlan.name != null}">
        <li>
          <span class="label">Dispositif<s:if test="!growingPlan.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value='growingPlan.topiaId'/>" title="Voir le dispositif"><s:property value="growingPlan.name" /></a>
        </li>
      </s:if>
      <s:if test="%{growingPlan.domain != null}">
        <li>
          <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='growingPlan.domain.topiaId'/>" title="Voir le domaine">
            <span class="label">Domaine<s:if test="!growingPlan.domain.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <s:property value="growingPlan.domain.name" />
          </a>
        </li>
        <li><span class="label">Campagne</span>
          <s:property value="growingPlan.domain.campaign" /> (<s:property value="(growingPlan.domain.campaign)-1" /> - <s:property value="growingPlan.domain.campaign" />)
        </li>
      </s:if>
      <s:if test="%{growingPlan.code != null}">
        <li class="highlight">
          <span class="label">Responsables</span>
          <a id="growingPlanResponsiblesLink" class="action-admin-roles" onclick="editGrowingPlanResponsibles('<s:property value='growingPlan.code' />')" title="Afficher les responsables de ce dispositif">Voir la liste</a>
        </li>
        <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='growingPlan.code' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(growingPlan.code)" /></a></li>
      </s:if>
    </ul>

    <s:if test="relatedGrowingPlans != null">
      <ul class="timeline">
        <s:iterator value="relatedGrowingPlans" var="relatedGrowingPlan">
          <li<s:if test="#relatedGrowingPlan.value.equals(growingPlan.topiaId)"> class="selected"</s:if>>
            <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value="value"/>"><s:property value="key" /></a>
          </li>
        </s:iterator>
      </ul>
    </s:if>
    <div ng-app="GrowingPlanEditModule" class="page-content">
      <form name="growingPlansEditForm" action="<s:url action='growing-plans-edit' namespace='/growingplans' />" method="post" class="tabs clear" ng-controller="GrowingPlanController" ag-confirm-on-exit>
         <s:actionerror cssClass="send-toast-to-js"/>
         <s:hidden name="growingPlanTopiaId" value="%{growingPlan.topiaId}"/>

         <ul id="tabs-growing-plans-menu" class="tabs-menu clearfix">
           <li class="selected"><span>Généralités</span></li>
           <li><span>Systèmes de Culture liés</span></li>
         </ul>
         <div id="tabs-growing-plans-content" class="tabs-content">
           <!-- id is used to parse tab index from it s name -->
           <div id="tab_0">
             <fieldset>
              <legend class="invisibleLabel">Données générales d'un dispositif</legend>
              <s:if test="%{growingPlan.topiaId != null}">
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label><span class="required">*</span> Domaine</label>
                  </span>
                  <span id="domain" class="wwctrl generated-content">
                    {{growingPlan.domain.name}}
                  </span>
                  <s:hidden name="domainId" value="%{growingPlan.domain.topiaId}"/>
                </div>
              </s:if>
              <s:else>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label>
                      <span class="required">*</span> Domaine&nbsp;:
                      <span ng-if="!domainId">
                        <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
                      </span>
                    </label>
                  </span>
                  <span id="domain" class="wwctrl">
                    <ui-select ng-model="selectedDomain"
                               theme="select2"
                               on-select="selectDomain($item, false)"
                               ng-required="true"
                               spinner-enabled="true">
                      <ui-select-match placeholder="Sélectionner un domaine">
                          {{$select.selected.name}} ({{$select.selected.campaign}})
                      </ui-select-match>
                      <ui-select-choices repeat="domain in domains track by $index"
                                         refresh="refreshDomains($select.search)"
                                         refresh-delay="200">
                          <span ng-bind-html="domain.name + ' (' + domain.campaign + ')' | highlight: $select.search"></span>
                      </ui-select-choices>
                    </ui-select>
                  </span>
                  <input type="hidden" name="domainId" value="{{domainId}}"/>
                </div>
              </s:else>
               <s:textfield name="growingPlan.name" label="Nom du dispositif" ng-model="growingPlan.name"
                  labelPosition="left" labelSeparator=" :" placeholder="ex. : Dispositif 1" required="true" requiredLabel="true" />
               <s:select name="growingPlan.type" label="Type" ng-model="growingPlan.type" list="typesDephy"
                  labelPosition="left" labelSeparator=" :" required="true" requiredLabel="true" emptyOption="true" />
               <s:textarea name="growingPlan.description" ng-model="growingPlan.description" label="Description succincte du dispositif" labelPosition="top" labelSeparator=" :" placeholder="%{getText('help.domain.growingPlan.description')}" rows="2" cols="80"/>
               <s:textarea name="growingPlan.goals" ng-model="growingPlan.goals" label="Objectifs du dispositif" labelPosition="top" labelSeparator=" :" rows="2" cols="80"/>
               <s:textfield name="growingPlan.protocolReference" ng-model="growingPlan.protocolReference" label="Reference du protocole qui gère le dispositif" value="%{growingPlan.protocolReference}" labelPosition="top" labelSeparator=" :" placeholder="ex. : XX"/>
               <s:textfield name="growingPlan.institutionalStructure" ng-model="growingPlan.institutionalStructure" label="Partenaires institutionnels" value="%{growingPlan.institutionalStructure}" labelPosition="top" labelSeparator=" :" />
             </fieldset>
           </div>

           <!-- Systèmes de Culture -->
           <%@include file="growing-plans-input-growing-systems.jsp" %>

           <span class="form-buttons">
             <a class="btn-secondary" href="<s:url action='growing-plans-list' namespace='/growingplans' />">Annuler</a>
             <input type="submit" class="btn-primary" value="Enregistrer"
               <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
               <s:if test="!activated">disabled="disabled" title="Le dispositif sur lequel vous travaillez est inactif et/ou est lié à un domaine inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
             />
           </span>

         </div>
      </form>

    </div>
  </body>
</html>
