<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- growing-plans-input-growing-systems.jsp -->
<div id="tab_1">
  <table class="data-table">
    <thead>
      <tr>
        <th scope="col">Système de culture</th>
        <th scope="col">Réseaux</th>
      </tr>
    </thead>
    <tbody>
      <s:if test="growingSystems == null || growingSystems.empty">
      <tr>
          <td class="empty-table" colspan="2">
            Il n'y a pas encore de systèmes de cultures définis sur ce domaine.
          </td>
        </tr>
      </s:if>
      <s:iterator value="growingSystems">
        <tr>
          <td>
            <a href="<s:url action='growing-systems-edit-input' namespace='/growingsystems'>
              <s:param name="growingSystemTopiaId" value="topiaId" />
            </s:url>">${name}</a>
          </td>
          <td>
            <s:iterator value="networks" status="networkStatus">
              <a href="<s:url action='networks-edit-input' namespace='/networks'>
                <s:param name="networkTopiaId" value="topiaId" />
              </s:url>">${name}</a><s:if test="!#networkStatus.last">,</s:if>
            </s:iterator>
          </td>
        </tr>
      </s:iterator>
    </tbody>
  </table>
</div>
