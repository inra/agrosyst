<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Dispositifs</title>
     <content tag="current-category">contextual</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/growingPlans.js' /><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript">
        angular.module('GrowingPlansList', ['Agrosyst']).
            value('ContextFilterInit', {}).
            value('ListGrowingPlansInitData', <s:property value="toJson(growingPlans)" escapeHtml="false"/>).
            value('growingPlanFilter', <s:property value="toJson(growingPlanFilter)" escapeHtml="false"/>).
            value('asyncThreshold', <s:property value="exportAsyncThreshold"/>);
     </script>
  </head>
  <body>
    <div ng-app="GrowingPlansList" ng-controller="GrowingPlansListController" class="page-content">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Dispositifs</li>
        </ul>
      </div>
    
      <ul class="actions">
        <li>
          <a class="action-ajouter" href="<s:url namespace='/growingplans' action='growing-plans-edit-input' />">Créer un dispositif</a>
        </li>
      </ul>
    
      <ul class="float-right actions">
        <li><a class="action-desactiver" ng-class="{'button-disablNed':(selectedEntities|toSelectedLength)== 0}"
          onclick="growingPlansUnactivate($('#confirmUnactivateGrowingPlans'), $('#growingplansListForm'), false);return false;" ng-if="allSelectedGrowingPlanActive">Désactiver</a>
          <a class="action-activer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
          onclick="growingPlansUnactivate($('#confirmUnactivateGrowingPlans'), $('#growingplansListForm'), true);return false;" ng-if="!allSelectedGrowingPlanActive">Activer</a></li>
        <li><a class="action-dupliquer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) != 1}"
          ng-click="growingPlansDuplicate($('#confirmDuplicateGrowingPlans'))">Dupliquer</a></li>
        <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
               onclick="growingPlansExport($('#growingplansListForm'));return false;">Export XLS</a></li>
        <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
               ng-click="asyncGrowingPlansExport()">Export XLS</a></li>
      </ul>

      <form method="post" id="growingplansListForm">
        <input type="hidden" name="growingPlanIds" value="{{selectedEntities|toSelectedArray}}" />
        <%-- Specific fields for duplicate form submit --%>
        <input type="hidden" name="duplicateDomainId" value="{{duplicateDomainId}}" ng-if="duplicateDomainId" />
        <input type="hidden" name="duplicateGrowingSystems" value="{{duplicateGrowingSystems}}" ng-if="duplicateGrowingSystems" />

        <table class="entity-list clear fixe_layout" id="dispositifs-list-table">
          <thead>
            <tr>
              <th scope="col" class="column-tiny-fixe"
                  title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Désélectionner toute la page' || 'Sélectionner toute la page'}}">
                  <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
              </th>

              <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('GROWING_PLAN')">
                <sort>
                  <header_label>Dispositif</header_label>
                  <em ng-if="filter.sortedColumn == 'GROWING_PLAN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_PLAN, 'fa fa-sort-amount-desc ':sortColumn.GROWING_PLAN}" ></em>
                  <em ng-if="filter.sortedColumn != 'GROWING_PLAN'" class='fa fa-sort' ></em>
                </sort>
              </th>

              <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('DOMAIN')">
                <sort>
                  <header_label>Exploitation ou Domaine expérimental</header_label>
                  <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                  <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
                </sort>
              </th>

              <th scope="col" class="column-small" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne">
                <sort>
                  <header_label>Camp.</header_label>
                  <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                  <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
                </sort>
              </th>

              <th class="column-large" scope="col">Type</th>
              <th scope="col" class="column-xllarge-fixed">Responsable de dispositif</th>
              <th scope="col" class="column-xsmall">État</th>
            </tr>
            <tr>
              <td class="column-tiny-fixe"></td>
              <td class="column-xxslarge-fixed"><input type="text" ng-model="filter.growingPlanName" /></td>
              <td class="column-xxslarge-fixed"><input type="text" ng-model="filter.domainName" /></td>
              <td class="column-small"><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" /></td>
              <td class="column-large">
                <select ng-model="filter.typeDephy">
                  <option value="" />
                  <s:iterator value="types">
                    <option value="<s:property value="key" />"><s:property value="value" /></option>
                  </s:iterator>
                </select>
                </td>
              <td class="column-xllarge-fixed"><input type="text" ng-model="filter.mainContact" /></td>
              <td>
                <select ng-model="filter.active">
                  <option value="" />
                  <option value="true">Actif</option>
                  <option value="false">Inactif</option>
                </select>
              </td>
            </tr>
          </thead>

          <tbody>
            <tr ng-show="growingPlans.length == 0"><td colspan="7" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>

            <tr ng-repeat="growingPlan in growingPlans"
                ng-class="{'line-selected':selectedEntities[growingPlan.topiaId]}">
              <td class="column-tiny-fixe">
                <input type='checkbox'
                       ng-model="selectedEntities[growingPlan.topiaId]"
                       ng-checked="selectedEntities[growingPlan.topiaId]"
                       ng-click="toggleSelectedEntity(growingPlan.topiaId)"/>
              </td>
              <td class="column-xxslarge-fixed"
                  title="{{growingPlan.name}}">
                  <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId={{growingPlan.topiaId|encodeURIComponent}}">
                    {{growingPlan.name}}<span ng-if="!growingPlan.active" class="unactivated">&nbsp;(inactif)</span>
                  </a>
              </td>
              <td class="column-xxslarge-fixed"
                  title="{{growingPlan.domain.name}}">
                  <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId={{growingPlan.domain.topiaId|encodeURIComponent}}">
                    {{growingPlan.domain.name}}<span ng-if="!growingPlan.active" class="unactivated">&nbsp;(inactif)</span>
                  </a>
              </td>
              <td class="column-small center"
                  title="{{growingPlan.domain.campaign}} ({{growingPlan.domain.campaign-1}} - {{growingPlan.domain.campaign}})">
                  {{growingPlan.domain.campaign}}
              </td>
              <td class="column-large">
                  {{dephyTypes[growingPlan.type]}}
              </td>
              <td class="column-xllarge-fixed" title="{{growingPlan.responsibles|displayArrayProperties:['firstName','lastName']|orDash}}">
                  {{growingPlan.responsibles|displayArrayProperties:['firstName','lastName']|orDash}}
              </td>
              <td>{{growingPlan.active ? 'Actif' : 'Inactif'}}</td>
            </tr>

          </tbody>

          <tfoot>
            <tr>
              <td colspan="7">
                <div class="table-footer">
                  <div class="entity-list-info">
                    <span class="counter">{{pager.count}} dispositifs</span>
                    <span>
                      - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les dispositifs</a>
                    </span>
                    <span>
                      - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                    </span>
                    <span class="selection">
                      - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                    </span>
                  </div>
                  <div class="pagination">
                    <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                  </div>
                </div>
              </td>
            </tr>
          </tfoot>
        </table>
      </form>

      <div id="confirmUnactivateGrowingPlans" title="Activer/Désactiver des dispositifs" class="auto-hide">
        Êtes-vous sûr(e) de vouloir {{allSelectedGrowingPlanActive?'désactiver':'activer'}}
        <ng-pluralize count="(selectedEntities|toSelectedLength)"
             when="{'one': 'le dispositif {{firstSelectedGrowingPlan.name}}',
                     'other': 'les {} dispositifs sélectionnés'}"></ng-pluralize> ?
      </div>
    
      <div id="confirmDuplicateGrowingPlans" title="Dupliquer un dispositif" class="auto-hide">
        <input type="hidden" name="domains" value="{{domains}}" ng-if="duplicateDomainId" />
        Dupliquer le dispositif : {{firstSelectedGrowingPlan.name}}
        <hr />
        Vers le domaine :
          <select id="weedType"
                  ng-model="duplicateDomainId"
                  ng-options="domain.topiaId as domain.name + ' (' + domain.campaign + ')' for domain in domains">
          </select>
          <br />
      </div>

    </div>
  </body>
</html>
