<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Accueil</title>
  </head>
  <body>
    <div class="introduction-paragraph">
      Bienvenue sur le site du système d’information <strong>Agrosyst</strong>, un outil au service du plan Ecophyto et du réseau DEPHY.<br/><br/>
      
      <strong>Ecophyto</strong>, qu’est-ce que c’est ?
      <ul>
        <li>Un plan qui vise à réduire progressivement l’utilisation des produits phytosanitaires (communément appelés pesticides) en France tout en maintenant une agriculture économiquement performante.</li>
        <li>Une initiative lancée en 2008 à la suite du Grenelle Environnement : le plan est piloté par le Ministère de l’agriculture, de l’agroalimentaire et de la forêt.</li>
        <li>Des acteurs mobilisés : depuis 2008, agriculteurs, chercheurs, techniciens des chambres d’agriculture ou des instituts techniques ont déjà engagé de nombreuses actions pour tenter d’atteindre cet objectif.</li>
      </ul>
      
      Le plan Ecophyto se décline en plusieurs actions dont la mise en place d’un réseau national de démonstration, expérimentation et production de références sur des systèmes de culture économes en produits phytosanitaires : le réseau <strong>DEPHY</strong>.<br/><br/>

      <strong>Agrosyst</strong> est le système d’information conçu pour accueillir et mettre à disposition les données issues du réseau DEPHY.
    </div>

  </body>
</html>
