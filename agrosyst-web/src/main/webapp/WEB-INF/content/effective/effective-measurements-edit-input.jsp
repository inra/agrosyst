<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Mesures et observations</title>
    <content tag="current-category">effective</content>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/measurements.css' /><s:property value='getVersionSuffix()'/>" />
    <script type="text/javascript" src="<s:url value='/nuiton-js/measurements.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">
      angular.module("MeasurementsEditModule", ["Agrosyst", "ui.date"])
        .value("MeasurementsEditInitData", {
          measurementSessions: <s:property value="toJson(measurementSessions)" escapeHtml="false"/>,
          measurementMeasureTypes: <s:property value="toJson(measurementMeasureTypes)" escapeHtml="false"/>,
          measurementObservationTypes: <s:property value="toJson(measurementObservationTypes)" escapeHtml="false"/>,
          croppingPlanEntries: <s:property value="toJson(croppingPlanEntries)" escapeHtml="false"/>,
          substanceActives: <s:property value="toJson(substanceActives)" escapeHtml="false"/>,
          adventices: <s:property value="toJson(adventices)" escapeHtml="false"/>
        });
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("La zone sur laquelle vous travaillez est inactive et/ou est liée à une parcelle et/ou un domaine inactif(s). Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

  </head>
  <body>

    <div ng-app="MeasurementsEditModule" ng-controller="MeasurementsEditController">

      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url action='effective-crop-cycles-list' namespace='/effective' />?measurement=true">Zones</a></li>
          <li>&gt; Mesures et observations</li>
        </ul>
      </div>

      <ul class="actions">
        <li><a class="action-retour" href="<s:url action='effective-crop-cycles-list' namespace='/effective' />?measurement=true">Retour à la liste des zones</a></li>
      </ul>

      <ul class="float-right informations">
        <li>
          <span class="label">Zone</span>
          <a href="<s:url namespace='/effective' action='effective-measurements-edit-input'/>?zoneTopiaId=<s:property value='zone.topiaId'/>" title="Voir la zone"><s:property value="zone.name" /></a>
        </li>
        <li>
          <span class="label">Parcelle<s:if test="!zone.plot.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/plots' action='plots-edit-input'/>?plotTopiaId=<s:property value='zone.plot.topiaId'/>" title="Voir la parcelle"><s:property value="zone.plot.name" /></a>
        </li>
        <s:if test="zone.plot.growingSystem != null">
          <li>
            <span class="label">Système de culture<s:if test="!zone.plot.growingSystem.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId=<s:property value='zone.plot.growingSystem.topiaId'/>" title="Voir le système de culture"><s:property value="zone.plot.growingSystem.name" /></a>
          </li>
          <li>
            <span class="label">Dispositif<s:if test="!zone.plot.growingSystem.growingPlan.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value='zone.plot.growingSystem.growingPlan.topiaId'/>" title="Voir le dispositif"><s:property value="zone.plot.growingSystem.growingPlan.name" /></a>
          </li>
        </s:if>
        <li>
          <span class="label">Domaine<s:if test="!zone.plot.domain.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='zone.plot.domain.topiaId'/>" title="Voir le domaine"><s:property value="zone.plot.domain.name" /> (<s:property value="zone.plot.domain.campaign" />)</a>
        </li>
        <s:if test="%{zone.topiaId != null}">
          <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='zone.topiaId' />-measurement')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(zone.topiaId + '-measurement')" /></a></li>
        </s:if>
      </ul>

      <s:if test="relatedZones != null">
        <ul class="timeline">
          <s:iterator value="relatedZones" var="relatedZone">
              <li<s:if test="#relatedZone.value.equals(zone.topiaId)"> class="selected"</s:if>>
                <a href="<s:url namespace='/effective' action='effective-measurements-edit-input' />?zoneTopiaId=<s:property value='value'/>"><s:property value="key" /></a>
              </li>
          </s:iterator>
        </ul>
      </s:if>

      <form action="<s:url action='effective-measurements-edit' namespace='/effective' />"
            method="post" class="tabs clear"
            id="measurementForm" name="measurementForm" ag-confirm-on-exit>
        <s:actionerror cssClass="send-toast-to-js"/>
        <s:hidden name="zoneTopiaId" />

        <input type="hidden" name="measurementSessions" value="{{measurementSessions}}" />

        <ul id="tabs-menu" class="tabs-menu clearfix">
          <li class="selected"><span>Mesures et observations</span></li>
        </ul>

        <div id="tabs-content" class="tabs-content">

          <fieldset ng-form="editedMeasurementSessionForm" >
            <legend class="invisibleLabel">Mesures et observations</legend>
            <div class="measurements-sub-form clear" >
              <div class="clear button-open paddingTop20" ng-repeat="measurementSession in measurementSessions">
                <a ng-click="editMeasurementSession(measurementSession)" class="btn"
                   ng-class="{'btn-selected': editedMeasurementSession === measurementSession}" style="color:#000000;"
                   ng-disabled="editedMeasurementSessionForm.$invalid && editedMeasurementSession != measurementSession">
                  Série de mesures / observations N&deg;{{$index + 1}}
                  <br/>
                  <span ng-if="measurementSession.startDate && measurementSession.startDate.getTime() == measurementSession.endDate.getTime()">Le {{measurementSession.startDate|date: 'dd/MM/yyyy'}}</span>
                  <span ng-if="measurementSession.startDate && measurementSession.startDate.getTime() != measurementSession.endDate.getTime()">Du {{measurementSession.startDate|date: 'dd/MM/yyyy'}} au {{measurementSession.endDate|date: 'dd/MM/yyyy'}}</span>
                  <br />
                  <span class="detailled-infos" ng-show="measurementSession.measurements == 0">(Pas de mesures)</span>
                  <span class="detailled-infos" ng-show="measurementSession.measurements.length == 1">(1 mesure/observation)</span>
                  <span class="detailled-infos" ng-show="measurementSession.measurements.length > 1">({{measurementSession.measurements.length}} mesures/observations)</span>
                  <input type="button" class="btn-icon icon-delete float-right" value="Supprimer" title="Supprimer" ng-click="deleteMeasurementSession(measurementSession)"/>
                </a>
              </div>
              <div class=" clear button-open">
                <a type="button"
                   ng-click="addMeasurementSession()"
                   class="btn btn-darker marginTop30"
                   ng-disabled="editedMeasurementSessionForm.$invalid && editedMeasurementSession != measurementSession">Ajouter une série de mesures / observations</a>
              </div>

              <%-- Single selected session --%>
              <div class="measurements-sub-form-side-content" ng-if="editedMeasurementSession" class="slide-animation">
        
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="measurementSessionStartDateField"><span class="required">*</span> Date de début&nbsp;:</label>
                  </span>
                  <span class="wwctrl">
                    <input type="text" id="measurementSessionStartDateField" name="sessionStartDate" ui-date="dateOptions"
                        ng-model="editedMeasurementSession.startDate" ng-change="measurementSessionStartDateUpdated(editedMeasurementSession.startDate)"
                        ng-required="editedMeasurementSession" />
                  </span>
                </div>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="measurementSessionEndDateField">Date de fin&nbsp;:</label>
                  </span>
                  <span class="wwctrl">
                    <input type="text" id="measurementSessionEndDateField" name="sessionEndDate" ui-date="dateOptions" ui-date-format="dd/mm/yy"
                        ng-model="editedMeasurementSession.endDate" />
                  </span>
                </div>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="measurementSessionCroppingField">Culture&nbsp;:</label>
                  </span>
                  <span class="wwctrl">
                    <select id="measurementSessionCroppingField"
                              ng-model="editedMeasurementSession.croppingPlanEntry"
                              ng-options="croppingPlanEntry.name for croppingPlanEntry in croppingPlanEntries">
                            <option value=""></option>
                          </select>
                  </span>
                </div>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="measurementSessionMinStadeField">Stade min&nbsp;:</label>
                  </span>
                  <span class="wwctrl">
                    <input type="text" id="measurementSessionMinStadeField" ng-model="editedMeasurementSession.minStage" />
                  </span>
                </div>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="measurementSessionMaxStadeField">Stade max&nbsp;:</label>
                  </span>
                  <span class="wwctrl">
                    <input type="text" id="measurementSessionMaxStadeField" ng-model="editedMeasurementSession.maxStage" />
                  </span>
                </div>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="measurementSessionCommentField">Commentaire&nbsp;:</label>
                  </span>
                  <span class="wwctrl">
                    <textarea id="measurementSessionCommentField" ng-model="editedMeasurementSession.comment"></textarea>
                  </span>
                </div>
                
                <div class="noclear paddingTop0">
                  <table class="data-table full-width">
                    <thead>
                      <tr>
                        <th scope="col" class="column-xsmall" />
                        <th scope="col">Type</th>
                        <th scope="col">Espèce</th>
                        <th scope="col">Numéro de répétition</th>
                        <th scope="col">Variable</th>
                        <th scope="col">Valeur</th>
                        <th scope="col" class="column-xsmall">Suppr.</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr ng-show="editedMeasurementSession.measurements.length == 0">
                        <td colspan="7" class="empty-table">Aucune mesure ou observation pour cette session.</td>
                      </tr>
                      <tr ng-repeat="measurement in editedMeasurementSession.measurements"
                          ng-class="{'selected-line' : editedMeasurement == measurement}" class="selectCursor"
                          ng-click="editMeasurement(measurement);selectedMeasurements[measurement.$$hashKey] = !selectedMeasurements[measurement.$$hashKey]">
                        <td>
                          <input type='checkbox' ng-checked="selectedMeasurements[measurement.$$hashKey]" />
                        </td>
                        <td>{{measurement._measure ? measurementMeasureTypes[measurement.measurementType]: measurementObservationTypes[measurement.measurementType]}}</td>
                        <td>{{(measurement.measurementType == 'PLANTE' || measurement.measurementType == 'STADE_CULTURE') ? getSpeciesName(measurement) : '-'}}</td>
                        <td>{{measurement.repetitionNumber|orDash}}</td>
                        <td>
                          <span ng-if="measurement._measure">{{measurement.refMesure.variable_mesuree}}</span>
                          <span ng-if="measurement.measurementType == 'ADVENTICES'">{{measurement.measuredAdventice.adventice}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES'">{{measurement.pest.reference_label || measurement.protocol.protocole_libelle}}</span>
                          <span ng-if="measurement.measurementType == 'STADE_CULTURE'">-</span>
                        </td>
                        <td>
                          <span ng-if="measurement._measure">{{measurement.measureValue|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'ADVENTICES'">{{measurement.adventiceStage.colonne2|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES'">{{measurement.quantitativeValue|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'STADE_CULTURE'">{{measurement.cropNumberObserved|orDash}}</span>

                          <span ng-if="measurement._measure">{{measurement.measureUnit}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES' && measurement.protocolVgObs">{{measurement.protocol.releve_qualifiant_unite_mesure || measurement.protocol.releve_unite}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES' && !measurement.protocolVgObs">{{measurement.cropUnitQualifier.reference_label || measurement.unitEDI}}</span>
                        </td>
                        <td><input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteMeasurement(editedMeasurementSession, measurement)"/></td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="7">
                          <div class="table-end-button">
                            <input type="button" value="Ajouter une observation" ng-click="addMeasurement(false)"/>
                          </div>
                          <div class="table-end-button">
                            <input type="button" value="Ajouter une mesure" ng-click="addMeasurement(true)"/>
                          </div>
                          <div class="table-end-button">
                    <input type="button" value="Copier" ng-click="copyMeasurements()"
                      ng-show="(selectedMeasurements|toSelectedLength) > 0" class="button-copy" />
                  </div>
                  <div class="table-end-button">
                    <input type="button" value="Coller" ng-click="pasteMeasurements()"
                      ng-show="pastedMeasurements" class="button-copy" />
                  </div>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
      
                <fieldset class="measurements-sub-form" ng-show="editedMeasurement && editedMeasurement._measure" ng-controller="MeasurementsMeasureController">
                  <legend class="invisibleLabel">Mesures et observations</legend>
                  <s:select id="measurementSessionMeasuresTypesField" list="measurementMeasureTypes" ng-model="editedMeasurement.measurementType" emptyOption="true" label="Catégorie de mesure"
                    labelSeparator=" :" ng-required="editedMeasurement && editedMeasurement._measure" requiredLabel="true" ng-change="measureTypeChanged(editedMeasurement.measurementType)"
                    labelPosition="left" />
                    
                  <div ng-switch="editedMeasurement.measurementType">
                    <div ng-switch-when="PLANTE">
                      <div class="wwgrp checkbox-list">
                        <span class="wwlbl">
                          <label>Espèce&nbsp;:</label>
                        </span>
                        <span class="wwctrl clearfix" style="margin-left:0px;">
                          <span ng-if="!editedMeasurementSession.croppingPlanEntry">Aucune culture sélectionnée</span>
                          <span ng-if="editedMeasurementSession.croppingPlanEntry"
                                ng-repeat="species in editedMeasurementSession.croppingPlanEntry.croppingPlanSpecies">
                            <input id="croppingPlanSpeciesField{{$index}}" type="checkbox"
                              ng-checked="editedMeasurement.croppingPlanSpecies.topiaId == species.topiaId"
                              ng-click="editedMeasurement.croppingPlanSpecies = species" />
                            <label class="checkboxLabel" for="croppingPlanSpeciesField{{$index}}">
                               {{species.species.libelle_espece_botanique}}
                               {{species.variety ? '(' + (species.variety.variete || species.variety.denomination) + ')' : ''}}
                            </label>
                          </span>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionMeasuringProtocoleField" ng-model="editedMeasurement.measuringProtocol"
                        label="Protocole de mesure" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber"
                        ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionEffectiveAreaField" ng-model="editedMeasurement.effectiveOrAreaTaken"
                        label="Effectif ou surface prélevée" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionTypeVariableField">Type de variable mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionTypeVariableField"
                              ng-model="editedMeasurement.refMesureVariableTypeItem"
                              ng-options="variableTypes[refMesureVariableType] for refMesureVariableType in refMesureVariableTypes"
                              ng-change="measureVariableTypeChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp" ng-show="editedMeasurement.refMesureVariableTypeItem">
                        <span class="wwlbl">
                          <label for="measurementSessionVariableField">Variable mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionVariableField"
                            ng-options="refMesure as refMesure.variable_mesuree for refMesure in refMesures"
                            ng-model="editedMeasurement.refMesure" ng-change="selectRefMesure()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionMeasureTypeField">Type de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionMeasureTypeField" ng-model="editedMeasurement.measureType">
                            <option value="">
                            <s:iterator value="measureTypes">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionMeasureValueField" ng-model="editedMeasurement.measureValue"
                        label="Valeur de mesure" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp" ng-show="refMesureUnits && refMesureUnits.length" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionUnitField">Unité de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionUnitField" ng-model="editedMeasurement.measureUnit"
                            ng-options="refMesureUnit for refMesureUnit in refMesureUnits">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp" ng-show="refMesureVariableTypeItem == 'ANALYSES' || refMesureVariableTypeItem == 'MESURES_DE_BIOMASSE'" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionOrganField">Organe concerné&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionOrganField" ng-model="editedMeasurement.organSupport">
                            <option value=""></option>
                            <s:iterator value="supportOrganeEdis">
                              <option value="${topiaId}">${reference_label}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp" ng-show="editedMeasurement.refMesure.variable_mesuree == 'Teneur en élément'
                          || editedMeasurement.refMesure.variable_mesuree == 'Quantité d\'élément'" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionElementField">Élement&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionElementField" ng-model="editedMeasurement.chemicalElement">
                            <option value="">
                            <s:iterator value="chemicalElements">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp" ng-if="editedMeasurement.refMesure.variable_mesuree == 'Teneur en substance active'" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionSAField">Substance active&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionSAField" ng-model="editedMeasurement.activeSubstance"
                            ng-options="substanceActive as substanceActive.nom_produit + ' : ' + substanceActive.nom_commun_sa for substanceActive in substanceActives">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp" ng-show="editedMeasurement.refMesure.variable_mesuree == 'Rendement aux normes'" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionProductivityField">Catégorie de rendement&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionProductivityField" ng-model="editedMeasurement.productivityType">
                            <option value="">
                            <s:iterator value="productivityTypes">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                    </div>
                    
                    <div ng-switch-when="SOL">
                      <s:textfield id="measurementSessionMeasuringProtocolField" ng-model="editedMeasurement.measuringProtocol" label="Protocole de mesure" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionHorizonTypeField">Horizon mesuré&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionHorizonTypeField" ng-model="editedMeasurement.horizonType">
                            <option value="">
                            <s:iterator value="horizonTypes">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber"
                        ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionSamplingField" ng-model="editedMeasurement.sampling"
                        label="Echantillonnage" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionTypeVariableField">Type de variable mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionTypeVariableField"
                              ng-model="editedMeasurement.refMesureVariableTypeItem"
                              ng-options="variableTypes[refMesureVariableType] for refMesureVariableType in refMesureVariableTypes"
                              ng-change="measureVariableTypeChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp" ng-show="editedMeasurement.refMesureVariableTypeItem">
                        <span class="wwlbl">
                          <label for="measurementSessionVariableField">Variable mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionVariableField"
                            ng-options="refMesure as refMesure.variable_mesuree for refMesure in refMesures"
                            ng-model="editedMeasurement.refMesure" ng-change="selectRefMesure()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionMeasureTypeField">Type de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionMeasureTypeField" ng-model="editedMeasurement.measureType">
                            <option value="">
                            <s:iterator value="measureTypes">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionMeasureValueField" ng-model="editedMeasurement.measureValue"
                        label="Valeur de mesure" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp" ng-show="refMesureUnits && refMesureUnits.length" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionUnitField">Unité de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionUnitField" ng-model="editedMeasurement.measureUnit"
                            ng-options="refMesureUnit for refMesureUnit in refMesureUnits">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      
                      <div class="wwgrp" ng-show="editedMeasurement.refMesure.variable_mesuree == 'Teneur en élément'
                          || editedMeasurement.refMesure.variable_mesuree == 'Quantité d\'élément'" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionElementField">Élement&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionElementField" ng-model="editedMeasurement.chemicalElement">
                            <option value="">
                            <s:iterator value="chemicalElements">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                    </div>
                    
                    <div ng-switch-when="TRANSFERT_DE_SOLUTES">
                      <s:textfield id="measurementSessionMeasuringProtocoleField" ng-model="editedMeasurement.measuringProtocol" label="Protocole de mesure" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber" ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionSamplingField" ng-model="editedMeasurement.sampling" label="Echantillonnage" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionVariableField">Variable mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionVariableField"
                            ng-options="refMesure as refMesure.variable_mesuree for refMesure in refMesures"
                            ng-model="editedMeasurement.refMesure" ng-change="selectRefMesure()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionSAField">Molécule phytosanitaire mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionSAField" ng-model="editedMeasurement.activeSubstance"
                            ng-options="substanceActive as substanceActive.nom_commun_sa for substanceActive in substanceActives">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionNitrogenMoleculeField">Molécule azotée mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionNitrogenMoleculeField" ng-model="editedMeasurement.nitrogenMolecule">
                            <option value="">
                            <s:iterator value="nitrogenMolecules">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionMeasureTypeField">Type de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionMeasureTypeField" ng-model="editedMeasurement.measureType">
                            <option value="">
                            <s:iterator value="measureTypes">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionMeasureValueField" ng-model="editedMeasurement.measureValue"
                        label="Valeur de mesure" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp" ng-show="refMesureUnits && refMesureUnits.length" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionUnitField">Unité de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionUnitField" ng-model="editedMeasurement.measureUnit"
                            ng-options="refMesureUnit for refMesureUnit in refMesureUnits">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                    </div>
                    
                    <div ng-switch-when="GES">
                      <s:textfield id="measurementSessionMeasuringProtocoleField" ng-model="editedMeasurement.measuringProtocol" label="Protocole de mesure" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber" ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionSamplingField" ng-model="editedMeasurement.sampling" label="Echantillonnage" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionVariableField">Variable mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionVariableField"
                            ng-options="refMesure as refMesure.variable_mesuree for refMesure in refMesures"
                            ng-model="editedMeasurement.refMesure" ng-change="selectRefMesure()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionMeasureTypeField">Type de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionMeasureTypeField" ng-model="editedMeasurement.measureType">
                            <option value="">
                            <s:iterator value="measureTypes">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionMeasureValueField" ng-model="editedMeasurement.measureValue" label="Valeur de mesure" labelSeparator=" :"
                        labelPosition="left" />
                      <div class="wwgrp" ng-show="refMesureUnits && refMesureUnits.length" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionUnitField">Unité de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionUnitField" ng-model="editedMeasurement.measureUnit"
                            ng-options="refMesureUnit for refMesureUnit in refMesureUnits">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                    </div>
                    
                    <div ng-switch-when="METEO">
                      <s:textfield id="measurementSessionMeasuringProtocolField" ng-model="editedMeasurement.measuringProtocol" label="Protocole de mesure" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber" ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionSamplingField" ng-model="editedMeasurement.sampling" label="Echantillonnage" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionVariableField">Variable mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionVariableField"
                            ng-options="refMesure as refMesure.variable_mesuree for refMesure in refMesures"
                            ng-model="editedMeasurement.refMesure" ng-change="selectRefMesure()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionMeasureTypeField">Type de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionMeasureTypeField" ng-model="editedMeasurement.measureType">
                            <option value="">
                            <s:iterator value="measureTypes">
                              <option value="${key}">${value}</option>
                            </s:iterator>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionMeasureValueField" ng-model="editedMeasurement.measureValue" label="Valeur de mesure" labelSeparator=" :"
                        labelPosition="left" />
                      <div class="wwgrp" ng-show="refMesureUnits && refMesureUnits.length" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionUnitField">Unité de mesure&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionUnitField" ng-model="editedMeasurement.measureUnit"
                            ng-options="refMesureUnit for refMesureUnit in refMesureUnits">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                    </div>
                    
                    <%-- for all types --%>
                    <s:textarea id="measurementSessionCommentField" ng-model="editedMeasurement.comment" label="Commentaire" labelSeparator=" :"
                        labelPosition="left" />
                  </div>
                </fieldset>
                
                <fieldset class="measurements-sub-form" ng-show="editedMeasurement && !editedMeasurement._measure">
                  <legend class="invisibleLabel">Mesures et observations</legend>
                  <s:select id="measurementSessionObservationTypesField" list="measurementObservationTypes" ng-model="editedMeasurement.measurementType" emptyOption="true" label="Type d’observation"
                    labelSeparator=" :" ng-required="editedMeasurement && !editedMeasurement._measure" requiredLabel="true" ng-change="loadRefMesures()" labelPosition="left" />
  
                  <div ng-switch="editedMeasurement.measurementType">
                    <div ng-switch-when="STADE_CULTURE" ng-controller="MeasurementsObservationController">
                      <div class="wwgrp checkbox-list" style="margin-left: 0%;">
                        <span class="wwlbl">
                          <label>Espèce&nbsp;:</label>
                        </span>
                        <span class="wwctrl clearfix" style="margin-left:0px;">
                          <span ng-if="!editedMeasurementSession.croppingPlanEntry">Aucune culture sélectionnée</span>
                          <span ng-if="editedMeasurementSession.croppingPlanEntry"
                                ng-repeat="species in editedMeasurementSession.croppingPlanEntry.croppingPlanSpecies">
                            <input id="croppingPlanSpeciesField{{$index}}" type="checkbox"
                              ng-checked="editedMeasurement.croppingPlanSpecies.topiaId == species.topiaId"
                              ng-click="editedMeasurement.croppingPlanSpecies = species;measurementSpeciesChanged()" />
                            <label class="checkboxLabel" for="croppingPlanSpeciesField{{$index}}">
                               {{species.species.libelle_espece_botanique}}
                               {{species.variety ? '(' + (species.variety.variete || species.variety.denomination) + ')' : ''}}
                            </label>
                          </span>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionMeasuringProtocolField" ng-model="editedMeasurement.measuringProtocol" label="Protocole de mesure" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber" ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionCropNumberObservedField" ng-model="editedMeasurement.cropNumberObserved" ng-pattern="/^[0-9]*$/" label="Nombre de plantes observées" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionStadeAvgEdiField">Stade moyen&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionStadeAvgEdiField" ng-model="editedMeasurement.cropStageAverage"
                            ng-options="refStadeEdi as refStadeEdi.colonne2 for refStadeEdi in refStadeEdis">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="clearfix">
                          <a class="float-right btn btn-darker" ng-click="showStadeMinMediumMax = !showMinMediumMax"
                            ng-show="!showStadeMinMediumMax && !editedMeasurement.cropStageMin && !editedMeasurement.cropStageMedium && !editedMeasurement.cropStageMax">Afficher les stades minimum/median/maximum</a>  
                      </div>
                      <div ng-show="showStadeMinMediumMax || editedMeasurement.cropStageMin || editedMeasurement.cropStageMedium || editedMeasurement.cropStageMax"
                        class="slide-animation">
                        <div class="wwgrp">
                          <span class="wwlbl">
                            <label for="measurementSessionStadeMinEdiField">Stade mini&nbsp;:</label>
                          </span>
                          <span class="wwctrl">
                            <select id="measurementSessionStadeMinEdiField" ng-model="editedMeasurement.cropStageMin"
                              ng-options="refStadeEdi as refStadeEdi.colonne2 for refStadeEdi in refStadeEdis">
                              <option value=""></option>
                            </select>
                          </span>
                        </div>
                        <div class="wwgrp">
                          <span class="wwlbl">
                            <label for="measurementSessionStadeMedianEdiField">Stade median&nbsp;:</label>
                          </span>
                          <span class="wwctrl">
                            <select id="measurementSessionStadeMedianEdiField" ng-model="editedMeasurement.cropStageMedium"
                              ng-options="refStadeEdi as refStadeEdi.colonne2 for refStadeEdi in refStadeEdis">
                              <option value=""></option>
                            </select>
                          </span>
                        </div>
                        <div class="wwgrp">
                          <span class="wwlbl">
                            <label for="measurementSessionStadeMaxEdiField">Stade maxi&nbsp;:</label>
                          </span>
                          <span class="wwctrl">
                            <select id="measurementSessionStadeMaxEdiField" ng-model="editedMeasurement.cropStageMax"
                              ng-options="refStadeEdi as refStadeEdi.colonne2 for refStadeEdi in refStadeEdis">
                              <option value=""></option>
                            </select>
                          </span>
                        </div>
                      </div>
                    </div>
  
                    <div ng-switch-when="NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES" ng-controller="MeasurementsObservationVgObsController">
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionProtocolVgobsField">Protocole VgObs&nbsp;:</label>
                        </span>
                        <span class="wwctrl checbox-list">
                          <input id="measurementSessionProtocolVgobsField" type="checkbox" ng-model="editedMeasurement.protocolVgObs" class="checkboxLabel"/>
                        </span>
                      </div>

                      <div class="wwgrp" ng-if="editedMeasurement.protocolVgObs" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionProtocolField">Protocole VgObs&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionProtocolField" ng-model="vgObsFilter.label"
                            ng-options="protocoleVgObsLabel for protocoleVgObsLabel in protocoleVgObsLabels"
                            ng-change="protocoleVgObsLabelChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div ng-if="!editedMeasurement.protocolVgObs" class="slide-animation">
                        <s:textfield ng-model="editedMeasurement.measuringProtocol" label="Autre protocole" labelSeparator=" :" labelPosition="left" />
                      </div>

                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber" ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionCropNumberObservedField" ng-model="editedMeasurement.cropNumberObserved" ng-pattern="/^[0-9]*$/" label="Nombre de plantes observées" labelSeparator=" :" labelPosition="left" />
                      
                      <div class="wwgrp" ng-if="!editedMeasurement.protocolVgObs" class="slide-animation">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiPestTypeField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Type d’organisme&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiPestTypeField" ng-model="ediFilter.pestType"
                            ng-options="ediPestType for ediPestType in ediPestTypes"
                            ng-change="ediPestTypeChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>

                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiPestField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Nuisible&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiPestField" ng-model="editedMeasurement.pest"
                            ng-options="ediPest as ediPest.reference_label for ediPest in ediPests" ng-if="!editedMeasurement.protocolVgObs"
                            ng-disabled="!ediPests"
                            ng-change="ediPestChanged()">
                            <option value=""></option>
                          </select>
                          <select id="measurementSessionEdiPestField" ng-model="vgObsFilter.pest"
                            ng-options="protocoleVgObsPest for protocoleVgObsPest in protocoleVgObsPests" ng-if="editedMeasurement.protocolVgObs"
                            ng-disabled="!protocoleVgObsPests"
                            ng-change="protocoleVgObsPestChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>

                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiPestStadeField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Stade organisme observé&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiPestStadeField" ng-model="editedMeasurement.cropOrganismStage"
                            ng-options="ediPestStade as ediPestStade.reference_label for ediPestStade in ediPestStades" ng-if="!editedMeasurement.protocolVgObs"
                            ng-disabled="!ediPestStades"
                            ng-change="ediPestStadeChanged()">
                            <option value=""></option>
                          </select>
                          <select id="measurementSessionEdiPestStadeField" ng-model="vgObsFilter.stade"
                            ng-options="protocoleVgObsStade as protocoleVgObsStade || '<<< vide >>>' for protocoleVgObsStade in protocoleVgObsStades" ng-if="editedMeasurement.protocolVgObs"
                            ng-disabled="!protocoleVgObsStades"
                            ng-change="protocoleVgObsStadeChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiPestSupportField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Support organe observé&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiPestSupportField" ng-model="editedMeasurement.cropOrganSupport"
                            ng-options="ediSupportOrgane as ediSupportOrgane.reference_label for ediSupportOrgane in ediSupportOrganes" ng-if="!editedMeasurement.protocolVgObs"
                            ng-disabled="!ediSupportOrganes"
                            ng-change="ediSupportChanged()">
                            <option value=""></option>
                          </select>
                          <select id="measurementSessionEdiPestSupportField" ng-model="vgObsFilter.support"
                            ng-options="protocoleVgObsSupport as protocoleVgObsSupport || '<<< vide >>>' for protocoleVgObsSupport in protocoleVgObsSupports" ng-if="editedMeasurement.protocolVgObs"
                            ng-disabled="!protocoleVgObsSupports"
                            ng-change="protocoleVgObsSupportChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiNotationField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Type de notation&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiNotationField" ng-model="editedMeasurement.cropNotationType"
                            ng-options="ediTypeNotation as ediTypeNotation.reference_label for ediTypeNotation in ediTypeNotations" ng-if="!editedMeasurement.protocolVgObs"
                            ng-disabled="!ediTypeNotations"
                            ng-change="ediNotationChanged()">
                            <option value=""></option>
                          </select>
                          <select id="measurementSessionEdiNotationField" ng-model="vgObsFilter.observation" 
                            ng-options="protocoleVgObsObservation as protocoleVgObsObservation || '<<< vide >>>' for protocoleVgObsObservation in protocoleVgObsObservations" ng-if="editedMeasurement.protocolVgObs"
                            ng-disabled="!protocoleVgObsObservations"
                            ng-change="protocoleVgObsObservationChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiQualitativeField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Valeur qualitative&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiQualitativeField" ng-model="editedMeasurement.cropQualititiveValue"
                            ng-options="ediQualitative as ediQualitative.reference_label for ediQualitative in ediQualitatives" ng-if="!editedMeasurement.protocolVgObs"
                            ng-disabled="!ediQualitatives"
                            ng-change="ediQualitativeChanged()">
                            <option value=""></option>
                          </select>
                          <select id="measurementSessionEdiQualitativeField" ng-model="vgObsFilter.qualitative"
                            ng-options="protocoleVgObsQualitative as protocoleVgObsQualitative || '<<< vide >>>' for protocoleVgObsQualitative in protocoleVgObsQualitatives" ng-if="editedMeasurement.protocolVgObs"
                            ng-disabled="!protocoleVgObsQualitatives"
                            ng-change="protocoleVgObsQualitativeChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionQuantitativeValueField" ng-model="editedMeasurement.quantitativeValue" ag-float="wtfwsruts" pattern="^[\-\+]?\d*[\.,]?\d*$" label="Valeur quantitative" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiUnitField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Unite EDI&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiUnitField" ng-model="editedMeasurement.unitEDI"
                            ng-options="vgObsUnit for vgObsUnit in vgObsUnits" ng-if="!editedMeasurement.protocolVgObs"
                            ng-disabled="!vgObsUnits"
                            ng-change="ediUnitChanged()">
                            <option value=""></option>
                          </select>
                          <select id="measurementSessionEdiUnitField" ng-model="vgObsFilter.unit"
                            ng-options="protocoleVgObsUnit as protocoleVgObsUnit || '<<< vide >>>' for protocoleVgObsUnit in protocoleVgObsUnits" ng-if="editedMeasurement.protocolVgObs"
                            ng-disabled="!protocoleVgObsUnits"
                            ng-required="editedMeasurement.protocolVgObs"
                            ng-change="protocoleVgObsUnitChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionEdiQualifierField"><span class="required" ng-if="editedMeasurement.protocolVgObs">*</span> Qualifiant EDI&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionEdiQualifierField" ng-model="editedMeasurement.cropUnitQualifier"
                            ng-options="ediQualifierUnit as ediQualifierUnit.reference_label for ediQualifierUnit in ediQualifierUnits" ng-if="!editedMeasurement.protocolVgObs"
                            ng-disabled="!ediQualifierUnits">
                            <option value=""></option>
                          </select>
                          <select id="measurementSessionEdiQualifierField" ng-model="editedMeasurement.protocol"
                            ng-options="protocoleVgObsQualifier as protocoleVgObsQualifier.releve_qualifiant_unite_mesure || '<<< vide >>>' for protocoleVgObsQualifier in protocoleVgObsQualifiers" ng-if="editedMeasurement.protocolVgObs"
                            ng-disabled="!protocoleVgObsQualifiers"
                            ng-required="editedMeasurement.protocolVgObs">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionUnitOtherField" ng-model="editedMeasurement.unitOther" label="Unite autre" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionOtherQualifierField" ng-model="editedMeasurement.otherQualifier" label="Qualifiant autre" labelSeparator=" :" labelPosition="left" />
                    </div>
  
                    <div ng-switch-when="ADVENTICES" ng-controller="MeasurementsObservationController">
                      <s:textfield id="measurementSessionMeasurignProtocolField" ng-model="editedMeasurement.measuringProtocol" label="Protocole de mesure" labelSeparator=" :" labelPosition="left" />
                      <s:textfield id="measurementSessionRepetitionNumberField" ng-model="editedMeasurement.repetitionNumber" ng-pattern="/^[0-9]*$/" label="Numéro de répétition" labelSeparator=" :" labelPosition="left" />
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionAdventiceField">Adventice mesurée&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionAdventiceField" ng-model="editedMeasurement.measuredAdventice"
                            ng-options="adventice as adventice.adventice for adventice in adventices"
                            ng-change="mesurementAdventiceChanged()">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="measurementSessionAdventiceStageField">Stade adventice&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <select id="measurementSessionAdventiceStageField" ng-model="editedMeasurement.adventiceStage"
                            ng-options="refStadeEdi as refStadeEdi.colonne2 for refStadeEdi in refStadeEdis">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <s:textfield id="measurementSessionAdventiceAverageField" ng-model="editedMeasurement.adventiceAverage" ag-float="wtfwsruts" pattern="^[\-\+]?\d*[\.,]?\d*$" label="Nombre moyen/m²"
                          labelSeparator=" :" labelPosition="left" />
                      <div class="clearfix">
                        <a ng-click="showAdventiceMinMediumMax = !showMinMediumMax"
                            ng-show="!showAdventiceMinMediumMax && !editedMeasurement.adventiceMin && !editedMeasurement.adventiceMedian && !editedMeasurement.adventiceMax" class="float-right btn btn-darker">Afficher les nombres minimum/median/maximum</a>
                      </div>
                      <div ng-show="showAdventiceMinMediumMax || editedMeasurement.adventiceMin || editedMeasurement.adventiceMedian || editedMeasurement.adventiceMax"
                        class="slide-animation">
                        <s:textfield id="measurementSessionAdventiceMinField" ng-model="editedMeasurement.adventiceMin" ag-float="wtfwsruts" pattern="^[\-\+]?\d*[\.,]?\d*$" label="Nombre mini/m²" labelSeparator=" :" labelPosition="left" />
                        <s:textfield id="measurementSessionAdventiceMedianField" ng-model="editedMeasurement.adventiceMedian" ag-float="wtfwsruts" pattern="^[\-\+]?\d*[\.,]?\d*$" label="Nombre median/m²" labelSeparator=" :" labelPosition="left" />
                        <s:textfield id="measurementSessionAdventiceMaxField" ng-model="editedMeasurement.adventiceMax" ag-float="wtfwsruts" pattern="^[\-\+]?\d*[\.,]?\d*$" label="Nombre maxi/m²" labelSeparator=" :" labelPosition="left" />
                      </div>
                    </div>
                    
                    <%-- for all types --%>
                    <s:textarea id="measurementSessionComment2Field" ng-model="editedMeasurement.comment" label="Commentaire" labelSeparator=" :"
                        labelPosition="left" />
                  </div>
                </fieldset>
              </div>
              
              <%-- No selected session : display all mesures --%>
              <div class="measurements-sub-form-side-content" ng-show="!editedMeasurementSession">
                <div class="noclear paddingTop0">
                  <table class="data-table full-width">
                    <thead>
                      <tr>
                        <th scope="col">Série</th>
                        <th scope="col">Date</th>
                        <th scope="col">Espèce</th>
                        <th scope="col">Type</th>
                        <th scope="col">Numéro de répétition</th>
                        <th scope="col">Variable</th>
                        <th scope="col">Valeur</th>
                      </tr>
                    </thead>
                    <tbody ng-if="measurementSessions.length == 0">
                      <tr>
                        <td class="empty-table" colspan="7">Aucune série de mesures et observation pour cette zone.</td>
                      </tr>
                    </tbody>
                    <tbody ng-repeat="measurementSession in measurementSessions">
                      <tr ng-if="!measurementSession.measurements || measurementSession.measurements.length === 0" ng-click="editMeasurementSessionMeasurement(measurementSession, measurement)" class="selectCursor">
                        <td class="column-xsmall">
                          {{$parent.$index + 1}}
                        </td>
                        <td>
                          <span ng-if="measurementSession.startDate.getTime() == measurementSession.endDate.getTime()">Le {{measurementSession.startDate|date: 'dd/MM/yyyy'}}</span>
                          <span ng-if="measurementSession.startDate.getTime() != measurementSession.endDate.getTime()">Du {{measurementSession.startDate|date: 'dd/MM/yyyy'}} au {{measurementSession.endDate|date: 'dd/MM/yyyy'}}</span>
                        </td>
                        <td colspan="5" class="empty-table">
                          <span>Aucune mesure ou observation pour cette série</span>
                        </td>
                      </tr>

                      <tr ng-if="measurementSession.measurements && measurementSession.measurements.length > 0" ng-repeat="measurement in measurementSession.measurements" ng-click="editMeasurementSessionMeasurement(measurementSession, measurement)" class="selectCursor">
                        <td class="column-xsmall">
                          {{$parent.$parent.$index + 1}}
                        </td>
                        <td>
                          <span ng-if="measurementSession.startDate.getTime() == measurementSession.endDate.getTime()">Le {{measurementSession.startDate|date: 'dd/MM/yyyy'}}</span>
                          <span ng-if="measurementSession.startDate.getTime() != measurementSession.endDate.getTime()">Du {{measurementSession.startDate|date: 'dd/MM/yyyy'}} au {{measurementSession.endDate|date: 'dd/MM/yyyy'}}</span>
                        </td>
                        <td>{{(measurement.measurementType == 'PLANTE' || measurement.measurementType == 'STADE_CULTURE') ? getSpeciesName(measurement) : '-'}}</td>
                        <td>{{measurement._measure ? measurementMeasureTypes[measurement.measurementType]: measurementObservationTypes[measurement.measurementType]}}</td>
                        <td>{{measurement.repetitionNumber|orDash}}</td>
                        <!--Variable-->
                        <td>
                          <span ng-if="measurement._measure">{{measurement.refMesure.variable_mesuree|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'ADVENTICES'">{{measurement.measuredAdventice.adventice|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES'">{{measurement.pest.reference_label || measurement.protocol.protocole_libelle|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'STADE_CULTURE'">-</span>
                        </td>



                        <!--Valeur-->
                        <td>
                          <span ng-if="measurement._measure">{{measurement.measureValue|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'ADVENTICES'">{{measurement.adventiceStage.colonne2|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES'">{{measurement.quantitativeValue|orDash}}</span>
                          <span ng-if="measurement.measurementType == 'STADE_CULTURE'">{{measurement.cropNumberObserved|orDash}}</span>

                          <span ng-if="measurement._measure">{{measurement.measureUnit}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES' && measurement.protocolVgObs">{{measurement.protocol.releve_qualifiant_unite_mesure || measurement.protocol.releve_unite}}</span>
                          <span ng-if="measurement.measurementType == 'NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES' && !measurement.protocolVgObs">{{measurement.cropUnitQualifier.reference_label || measurement.unitEDI}}</span>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

            </div>
          </fieldset>

          <span class="form-buttons clear">
            <a class="btn-secondary">Annuler</a>
            <input type="submit" id="saveButton" class="btn-primary" value="Enregistrer"
              <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
              <s:if test="!activated">disabled="disabled" title="La zone sur laquelle vous travaillez est inactive et/ou est liée à une parcelle et/ou un domaine inactif(s). Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
            />
          </span>
        </div>
      </form>
      <!-- </div> -->

    </div>

  </body>
</html>
