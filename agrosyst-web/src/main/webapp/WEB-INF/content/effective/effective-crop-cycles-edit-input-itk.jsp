<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!--effective-crop-cycles-edit-input-itk.jsp-->

<div id="tab_2" ng-controller="ItkController" class="page-content">
  <div ng-show="phasesAndNodesArray.length === 0" class="empty-table">Vous devez déclarer la/les culture(s) assolée(s) de la campagne avant de pouvoir renseigner les interventions culturales.</div>
  <div class="table-enclosure" ng-show="phasesAndNodesArray.length > 0" >
    <label>Culture(s) présente(s) sur la zone au cours de cette campagne agricole&nbsp;:</label>
    <table class="data-table">
      <thead>
        <tr>
          <th scope="col">Cycle</th>
          <th scope="col" class="column-xsmall">Type de culture</th>
          <th scope="col">Culture</th>
          <th scope="col">Nombre d'interventions</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="phaseOrNode in phasesAndNodesArray | orderBy: 'x'" ng-click="editPhaseOrNode(phaseOrNode)"
            ng-class="{'selected-line' : selectedPhaseConnectionOrNode === phaseOrNode, 'line-error': !validateCycle(phaseOrNode)}"
            class="selectCursor">
          <td>{{phaseOrNode.type ? "Pérenne" : "Assolé"}}
            <input ng-if="!validateCycle(phaseOrNode)" id="phaseOrNodeError_{{$index}}" type="text" ng-model="nophaseOrNode" required style="opacity:0;width: 0;padding: 0px;border-width: 0px;"/>
          </td>
          <td>
            <div ng-if="!phaseOrNode.type" class="cell-on-one-row center">
              <div ng-if="phaseOrNode.intermediateCropLabel"
                   class="textIcones INTERMEDIATE"
                   title="Culture intermédiaire ({{ phaseOrNode.intermediateCropLabel }})"/>
                I
              </div>
              <div ng-if="phaseOrNode.intermediateCropLabel" style="margin-left: 10px" />
                /
              </div>
              <div ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).type === 'MAIN'"
                   class="textIcones MAIN"
                   title="Culture principale{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies ? ' - Mélange d\'espèces' : ''}}{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety ? ' - Mélange de variétés' : ''}}">
                <span>P</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies" class="textIcones MIX_S">ME</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety" class="textIcones MIX_V">MV</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixCompanion" class="textIcones MIX_C"><s:text name='common-cropping-plan-mix-companion-abbr' /></span>
              </div>
              <div ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).type === 'CATCH'"
                   class="textIcones CATCH"
                   title="Culture dérobée{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies ? ' - Mélange d\'espèces' : ''}}{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety ? ' - Mélange de variétés' : ''}}">
                <span>D</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies" class="textIcones MIX_S">ME</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety" class="textIcones MIX_V">MV</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixCompanion" class="textIcones MIX_C"><s:text name='common-cropping-plan-mix-companion-abbr' /></span>
              </div>
            </div>
            <div ng-if="phaseOrNode.type" class="cell-on-one-row center">
              <div ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).type === 'MAIN'"
                   class="textIcones MAIN"
                   title="Culture principale{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies ? ' - Mélange d\'espèces' : ''}}{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety ? ' - Mélange de variétés' : ''}}">
                <span>P</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies" class="textIcones MIX_S">ME</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety" class="textIcones MIX_V">MV</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixCompanion" class="textIcones MIX_C"><s:text name='common-cropping-plan-mix-companion-abbr' /></span>
              </div>
              <div ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).type === 'CATCH'"
                   class="textIcones CATCH"
                   title="Culture dérobée{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies ? ' - Mélange d\'espèces' : ''}}{{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety ? ' - Mélange de variétés' : ''}}">
                <span>D</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixSpecies" class="textIcones MIX_S">ME</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixVariety" class="textIcones MIX_V">MV</span>
                <span ng-if="getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).mixCompanion" class="textIcones MIX_C"><s:text name='common-cropping-plan-mix-companion-abbr' /></span>
              </div>
            </div>
          </td>
          <td ng-if="phaseOrNode.type">
            {{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).label}} ({{perennialPhaseTypes[phaseOrNode.type]}})
          </td>
          <td ng-if="!phaseOrNode.type">
            {{phaseOrNode.intermediateCropLabel ? "(" + phaseOrNode.intermediateCropLabel + ")" : ""}}
            {{getCroppingPlanEntry(phaseOrNode.croppingPlanEntryId).label}} (Rang {{phaseOrNode.x + 1}})
          </td>
          <td>{{phaseOrNode.interventions.length}}</td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="table-enclosure" ng-if="selectedPhaseConnectionOrNode">
    <label>Interventions culturales
      <span ng-show="!selectedPhaseConnectionOrNode.type">de la culture {{getCroppingPlanEntry(selectedPhaseConnectionOrNode.croppingPlanEntryId).label}}&nbsp;:</span>
      <span ng-show="selectedPhaseConnectionOrNode.type">de la phase {{perennialPhaseTypes[selectedPhaseConnectionOrNode.type]}}&nbsp;:</span>
    </label>
    <table class="data-table">
      <thead>
        <tr>
          <th scope="col" title="{{allInterventionSelected.selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
            <input type='checkbox' ng-model="allInterventionSelected.selected" ng-change="toggleSelectedInterventions()" /></th>
          <th scope="col">Nom</th>
          <th scope="col">Action(s)</th>
          <th scope="col">Dates d'intervention</th>
          <th scope="col">Affectation CI</th>
          <th scope="col" title="<s:text name="help.effectiveCropCycles.transitCount" />">Nombre de passages</th>
          <th scope="col">Fréquence spatiale</th>
          <th scope="col">Proportion surf. traitée</th>
          <th scope="col" title="<s:text name="help.PSCi" />">PSCi</th>
          <th scope="col">Données saisies</th>
          <th scope="col">Suppr.</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-show="selectedPhaseConnectionOrNode.interventions.length === 0" ><td colspan="11" class="empty-table">Il n'y a pas encore d'intervention pour la phase ou la culture sélectionnée. Pour en ajouter, cliquez sur le bouton "Ajouter une intervention" ci-dessous</td></tr>
        <tr ng-repeat="intervention in selectedPhaseConnectionOrNode.interventions|orderBy: 'startInterventionDate'"
            ng-class="{'selected-line' : editedIntervention === intervention, 'line-selected':selectedInterventions[intervention.topiaId], 'line-error': !validateIntervention(intervention)}"
            ng-disabled="editedInterventionForm.$invalid || effectiveCropCyclesEditForm.$invalid"
            ng-click="editIntervention(intervention);selectedInterventions[intervention.topiaId] = !selectedInterventions[intervention.topiaId]"
            class="selectCursor">
          <td>
            <input type='checkbox' ng-checked="selectedInterventions[intervention.topiaId]" ng-disabled="!editedInterventionForm.$valid"/>
            <input ng-if="!validateIntervention(intervention)" id="interventionError_{{$index}}" type="text" ng-model="noIntervention" required style="opacity:0;width: 0;padding: 0px;border-width: 0px;"/>
          </td>
          <td>{{intervention.name | orDash}}</td>
          <td ng-click="intervention.showCompleteActionList = !intervention.showCompleteActionList">
            <ul ng-if="!intervention.showCompleteActionList" class="actions-list">
              <li ng-repeat="action in intervention.actionDtos | limitTo:3">
                <span>
                  {{agrosystInterventionTypes[action.mainActionInterventionAgrosyst]}} / {{action.mainActionReference_label |orDash}}
                </span>
              </li>
            </ul>
            <ul ng-if="intervention.showCompleteActionList" class="actions-list">
              <li ng-repeat="action in intervention.actionDtos">
                <span>
                  {{agrosystInterventionTypes[action.mainActionInterventionAgrosyst]}} / {{action.mainActionReference_label |orDash}}
                </span>
              </li>
            </ul>
            <span ng-if="!intervention.showCompleteActionList && intervention.actionDtos.length > 3">{{intervention.actionDtos.length-3}} actions supplémentaires</span>
            <span ng-if="!intervention.actionDtos || intervention.actionDtos.length === 0">-</span>
          </td>
          <td ng-if="!intervention.startInterventionDate && !intervention.endInterventionDate">-</td>
          <td ng-if="!intervention.startInterventionDate && intervention.endInterventionDate">Avant le {{intervention.endInterventionDate|date:'dd/MM/yyyy'}}</td>
          <td ng-if="intervention.startInterventionDate && !intervention.endInterventionDate">Après le {{intervention.startInterventionDate|date:'dd/MM/yyyy'}}</td>
          <td ng-if="intervention.startInterventionDate && intervention.startInterventionDate.getTime() == intervention.endInterventionDate.getTime()">Le {{intervention.startInterventionDate|date:'dd/MM/yyyy'}}</td>
          <td ng-if="intervention.startInterventionDate && intervention.endInterventionDate && intervention.startInterventionDate.getTime() != intervention.endInterventionDate.getTime()">Du {{intervention.startInterventionDate|date:'dd/MM/yyyy'}} au {{intervention.endInterventionDate|date:'dd/MM/yyyy'}}</td>
          <td>{{intervention.intermediateCrop && 'Oui' || '-'}}</td>
          <td title="<s:text name="help.effectiveCropCycles.transitCount" />">{{intervention.transitCount | orDash }}</td>
          <td>{{(intervention.spatialFrequency | number:2) | orDash}}</td>
          <td>
            <div ng-if="intervention.proportionOfBioTreatedSurface">Lutte bio.&nbsp;: {{(intervention.proportionOfBioTreatedSurface | number:0)}}&nbsp%</div>
            <div ng-if="intervention.proportionOfPhytoTreatedSurface">Phyto.&nbsp;: {{(intervention.proportionOfPhytoTreatedSurface | number:0)}}&nbsp%</div>
            <div ng-if="!intervention.proportionOfBioTreatedSurface && !intervention.proportionOfPhytoTreatedSurface">-</div>
          </td>
          <td title="<s:text name="help.PSCi" />">{{(intervention.transitCount * intervention.spatialFrequency | number:2) | orDash}}</td>
          <td ng-click="intervention.showCompleteProductList = !intervention.showCompleteProductList">
            <ul ng-if="!intervention.showCompleteProductList" class="products-list">
              <li ng-repeat="item in intervention.usageData | limitTo:3">
                <span ng-if="item.label">
                  {{(item.label|orDash)|truncate:20}}
                </span>
                <span ng-if="item.label && item.quantity">&nbsp;(</span><span ng-if="item.quantity">{{item.quantity|number:2}}&nbsp;{{item.unit|orDash}}</span><span ng-if="item.label && item.quantity">)</span>
              </li>
            </ul>
            <ul ng-if="intervention.showCompleteProductList" class="products-list">
              <li ng-repeat="item in intervention.usageData">
                <span ng-if="item.label">
                  {{item.label|orDash}}
                </span>
                <span ng-if="item.label && item.quantity">&nbsp;(</span><span ng-if="item.quantity">{{item.quantity|number:2}}&nbsp;{{item.unit|orDash}}</span><span ng-if="item.label && item.quantity">)</span>
              </li>
            </ul>
            <span ng-if="!intervention.showCompleteProductList && intervention.usageData.length > 3">{{intervention.usageData.length - 3}} produit(s) supplémentaire(s)</span>
            <span ng-if="!intervention.usageData || intervention.usageData.length === 0">-</span>
          </td>
          <td>
            <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteIntervention(intervention)"/>
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="11">
            <div class="table-end-button">
              <input type="button" value="Ajouter une intervention" ng-click="createIntervention()" ng-disabled="!editedInterventionForm.$valid"/>
            </div>
            <div class="table-end-button" ng-controller="ItkEffectiveCopyPasteController">
              <input type="button" value="Copier les interventions" ng-click="copyEffectiveInterventions()"
                ng-show="(selectedInterventions|toSelectedLength) > 0" class="button-copy" ng-disabled="!editedInterventionForm.$valid"/>

              <jqdialog dialog-name="CopyPasteItk" auto-open="false" width="'55%'" class="dialog-form" modal="true"
                     buttons="{'OK': onCopyPasteItkDialogOk, 'Annuler': onCopyPasteItkDialogCancel}"
                     button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
                     title="'Copier/Coller des interventions'">
                  <span ng-if="!copyPasteInterventions.ready">
                    Chargement...
                  </span>
                  <span ng-if="copyPasteInterventions.availableZonesForCampaign">
                    <jsp:include page="../itk/copy-paste-interventions.jsp" />
                  </span>
              </jqdialog>
            </div>
            <div class="table-end-button">
              <input type="button" value="Supprimer les interventions" ng-click="deleteSelectedInterventions()"
                     ng-show="(selectedInterventions|toSelectedLength) > 1" class="button-delete"  ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign"/>
            </div>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>

  <!-- Intervention sélectionnée -->
  <fieldset class="sub-form marginTop30" ng-form="editedInterventionForm" >
    <legend class="invisibleLabel">Déclaration d'une intervention</legend>
    <div id="new-intervention" class="sub-form-content full-width">

      <!-- Détails de l'intervention -->
      <div class="two-columns noclear">

        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_type_{{phasesAndNodesArray.indexOf(selectedPhaseConnectionOrNode)}}_{{selectedPhaseConnectionOrNode.interventions.indexOf(editedIntervention)}}"><span class="required">*</span> Type d'intervention&nbsp;</label></span>
          <span class="wwctrl">
            <select id="intervention_type_{{phasesAndNodesArray.indexOf(selectedPhaseConnectionOrNode)}}_{{selectedPhaseConnectionOrNode.interventions.indexOf(editedIntervention)}}"
                    ng-model="editedIntervention.type"
                    ng-change="loadToolsCouplings(editedIntervention, false)"
                    ng-required="editedIntervention"
                    ng-disabled="editedIntervention.toolsCouplingCodes.length > 0"
                    ng-attr-title="{{editedIntervention.toolsCouplingCodes.length > 0 ? 'Une combinaison d\'outils est sélectionnée, merci de la dé-sélectionner avant de changer le type d\'intervention.' : '' }}">
              <!-- the following line is required to enable HTML5 validation-->
              <option value=""></option>
              <option ng-repeat="(type, label) in agrosystInterventionTypes"
                      value="{{type}}"
                      title="{{ getInterventionTypeTooltip(type) }}"
                      ng-selected="editedIntervention.type === type">
                {{ label }}
              </option>
            </select>
          </span>
        </div>

        <div id="warning-no-selected-tools-coupling" title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.informationToBeTicked"/>">
          <div class="bubble-text">
            <p>
              <span class="warning_char"><i class="fa fa-warning" aria-hidden="true"></i></span>
              <span class="highlight-text-content">
                <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.informationToBeTicked"/>
                <a class="highlight-text-content-redirect"
                   ng-click="redirectToToolsCooplingsTab()"
                   target="_blank"
                   href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='zone.plot.domain.topiaId'/>"
                   title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>">
                   <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>
                </a>
              </span>
            </p>
          </div>
        </div>
        <div class="wwgrp checkbox-list">
          <span class="wwlbl">
            <label>
            <span class="requiredAdvised lowProfile" title="<s:text name="help.performance.required"/> : Temps de travail et consommation de carburant">
              &pi;
            </span>
            <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings"/>&nbsp;:
            <input class="button-refresh"
                   type="button"
                   ng-click="loadToolsCouplings(editedIntervention, false)"
                   title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.reloadDomainToolsCouplings"/>"
                   value="&#8635;"/>
            </label>
          </span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.effectiveCropCycles.toolsCouplings" />
              </span>
            </span>
            <span ng-show="toolsCouplings && toolsCouplings.length > 0"
                  ng-repeat="toolsCoupling in toolsCouplings |  filter: {manualIntervention: false} | orderBy: 'toolsCouplingName'">
              <input type="checkbox" ng-model="editedInterventionSelectedToolCouplings[toolsCoupling.code]"
                     ng-change="toggleSelectedInterventionToolsCoupling(editedIntervention, toolsCoupling)"
                     id="toolsCoupling-{{$index}}"/>
              <label class="checkboxLabel" for="toolsCoupling-{{$index}}">{{toolsCoupling.manualIntervention ? "interv. manuelle - " : ""}}{{toolsCoupling.toolsCouplingName}}</label>
            </span>
            <span ng-show="toolsCouplings && toolsCouplings.length > 0"
                  ng-repeat="toolsCoupling in toolsCouplings |  filter: {manualIntervention: true} | orderBy: 'toolsCouplingName'">
              <input type="checkbox" ng-model="editedInterventionSelectedToolCouplings[toolsCoupling.code]"
                     ng-change="toggleSelectedInterventionToolsCoupling(editedIntervention, toolsCoupling)"
                     id="intervention-{{$index}}"/>
              <label class="checkboxLabel" for="intervention-{{$index}}">{{toolsCoupling.manualIntervention ? "interv. manuelle - " : ""}}{{toolsCoupling.toolsCouplingName}}</label>
            </span>
            <span ng-show="newMainsActionsPushed" ng-cloak>
              <div>
                <span>Les actions principales suivantes sont ajoutées ci dessous&nbsp;:</span>
                <ul>
                  <li ng-repeat="action in newMainsActionsPushed">
                    {{agrosystInterventionTypes[action.intervention_agrosyst]}} / {{action.reference_label_Translated}}
                  </li>
                </ul>
              </div>
            </span>
            <span id="warning-no-tools-coupling-available">
              <div class="info-text">
                  <span class="warning_char"><i class="fa fa-warning" aria-hidden="true"></i></span>
                  <div class="highlight-text-content">
                    <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.missingDomainToolsCouplings"/>
                    <a class="highlight-text-content-redirect"
                       ng-click="redirectToToolsCooplingsTab()"
                       target="_blank"
                       href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='zone.plot.domain.topiaId'/>"
                       title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>">
                       <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>
                    </a>
                  </div>
              </div>
            </span>
            <span class="empty-list" ng-if="!editedIntervention.type">
              Aucun type d'intervention sélectionné
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_name"><span class="required">*</span> Nom&nbsp;:</label></span>
          <span class="wwctrl"><input type="text" id="intervention_name" ng-model="editedIntervention.name" ng-required="editedIntervention" /></span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label for="interventionStartDateField"><span class="required">*</span> Date de début d'intervention (jj/mm/aaaa)&nbsp;:</label></span>
          <span class="wwctrl">
            <input type="text"
                  ng-if="editedIntervention"
                  id="interventionStartDateField"
                  ng-required="editedIntervention"
                  ng-model="editedIntervention.startInterventionDate"
                  ui-date="dateOptions"
                  placeholder="ex. : 30/07/2013"
                  ng-change="checkInterventionEndDateValidity();interventionStartingDateSelected()" />
            <input type="text"
                  ng-if="!editedIntervention"
                  id="interventionStartDateField"
                  ng-required="editedIntervention"
                  ng-model="editedIntervention.startInterventionDate"
                  placeholder="ex. : 30/07/2013"
                  ng-change="checkInterventionEndDateValidity();interventionStartingDateSelected()" />
          </span>
        </div>
        <div class="wwgrp">
          <span class="wwlbl"><label for="interventionEndDateField"><span class="required">*</span> Date de fin d'intervention (jj/mm/aaaa)&nbsp;:</label></span>
          <span class="wwctrl">
            <input type="text"
                   ng-if="editedIntervention"
                   id="interventionEndDateField"
                   name="interventionEndDateField"
                   ng-required="editedIntervention && editedIntervention.startInterventionDate"
                   ng-model="editedIntervention.endInterventionDate"
                   ui-date="dateOptions"
                   ng-change="checkInterventionEndDateValidity();pushPeriodChangeToValorisations(editedIntervention)"
                   placeholder="ex. : 30/07/2013"/>
            <input type="text"
                   ng-if="!editedIntervention"
                   id="interventionEndDateField"
                   name="interventionEndDateField"
                   ng-required="editedIntervention"
                   ng-model="editedIntervention.endInterventionDate"
                   ng-change="checkInterventionEndDateValidity();pushPeriodChangeToValorisations(editedIntervention)"
                   placeholder="ex. : 30/07/2013"/>
          </span>
        </div>
        <div class="wwgrp">
          <div class="wwlbl"><label for="intervention_comment" class="label">Commentaire&nbsp;:</label></div>
          <span class="wwctrl"><textarea id="intervention_comment" ng-model="editedIntervention.comment"></textarea></span>
        </div>
        <div class="wwgrp" ng-if="!selectedPhaseConnectionOrNode.type">
          <span class="wwlbl"><label for="intervention_intermediateCrop">Affectation à la culture intermédiaire&nbsp;:</label></span>
          <span class="wwctrl" ng-if="connectionIntermediateCrop">
            <input type="checkbox"
                   id="intervention_intermediateCrop"
                   ng-model="editedIntervention.intermediateCrop"
                   ng-change="toggleIntermediateSpeciesSelection()"/>
          </span>
          <span class="wwctrl" ng-if="!connectionIntermediateCrop">
            <input type="checkbox"
                   id="intervention_intermediateCrop"
                   ng-model="editedIntervention.intermediateCrop"
                   title="Il n'y a pas de culture intermédiaire."
                   disabled/>
          </span>
        </div>
        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_spatialFrequency"><span class="required">*</span> Fréquence spatiale&nbsp;:</label></span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.effectiveCropCycles.spatialFrequency" />
              </span>
            </span>
            <input type="text" id="intervention_spatialFrequency" ng-model="editedIntervention.spatialFrequency"
                placeholder="ex. : 0.85" ng-pattern="/^((([0])\.([0-9]*)|0)|1)$/" pattern="(([0])\.([0-9]*)|0)|1" ng-required="editedIntervention"
                ng-change="computeSpendingTime()" title="Ce champ ne concerne que le passage d'outils/d'hommes"/>
          </span>
        </div>
      </div>
      <div class="two-columns noclear">
        <div class="wwgrp">
          <span class="wwlbl"><label for="interventionTransitCountField"><span class="required">*</span> Nombre de passage&nbsp;:</label></span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.effectiveCropCycles.transitCount" />
              </span>
            </span>
            <input type="text" id="interventionTransitCountField" ng-required="editedIntervention" ng-model="editedIntervention.transitCount" ng-change="computeSpendingTime()" placeholder="ex. : 1" ag-integer pattern="\d+"/>
          </span>
        </div>
        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_workRate">
            <span class="requiredIndicator" title="<s:text name="help.performance.required"/> : Temps de travail">&pi;</span> Débit de chantier&nbsp;:</label>
            </span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal">
                <input type="text" id="intervention_workRate"
                       ng-model="editedIntervention.workRate"
                       ng-blur="processWorkRateChange()"
                       placeholder="ex. : 15.5" ag-float-positive pattern="^\d*[\.,]?\d*$"/>
              </span>
              <span class="column-normal">
                <select id="workRateUnit"
                        ng-model="editedIntervention.workRateUnit"
                        ng-options="materielWorkRateUnits[workRateUnit] for workRateUnit in workRateUnits"
                        ng-change="processWorkRateChange()">
                </select>
              </span>
              <span class='contextual-help'>
                <span class='help-hover'>
                  <s:text name="help.performance.workrate" />
                </span>
              </span>
            </div>
          </span>

          <span class="wwgrp" ng-if="editedIntervention.workRateUnit == 'H_HA' || editedIntervention.workRateUnit == 'HA_H'">
            <span class="wwlbl"><label for="revertWorkRate"></label></span>
            <span class="wwctrl">
              <div class="clearfix">
                <span class="column-normal float-left paddingRight10">
                  <input ng-if="editedIntervention.workRate && editedIntervention.workRate != 0" id="revertWorkRate" disabled value="{{1/editedIntervention.workRate| number:3}}" type="text"/>
                  <input ng-if="!editedIntervention.workRate" id="revertWorkRate" disabled type="text"/>
                </span>
                <span class="input-append column-normal">
                  <span ng-if="editedIntervention.workRateUnit == 'H_HA'" class="add-on">
                    {{materielWorkRateUnits['HA_H']}}
                  </span>
                  <span ng-if="editedIntervention.workRateUnit == 'HA_H'" class="add-on">
                    {{materielWorkRateUnits['H_HA']}}
                  </span>
                </span>
              </div>
            </span>
          </span>
        </div>

        <div class="wwgrp" ng-if="editedIntervention.workRateUnit === 'VOY_H'">
          <span class="wwlbl"><label for="transitVolume"><span class="requiredAdvised">&pi;</span> Volume par voyage</label></span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal">
                <input type="text" id="transitVolume" ng-model="editedIntervention.transitVolume"
                       ng-blur="processWorkTransitVolume()" ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"/>
              </span>
              <span class="column-normal">
                <select id="transitVolumeUnit"
                        ng-model="editedIntervention.transitVolumeUnit"
                        ng-options="key as value for (key , value) in materielTransportUnits"
                        ng-change="processWorkTransitVolume()">
                </select>
              </span>
            </div>
          </span>
        </div>

        <div class="wwgrp" ng-if="editedIntervention.workRateUnit === 'BAL_H'">
          <span class="wwlbl"><label for="nbBalls"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : Temps de travail">&pi;</span> Nombre de balles&nbsp;:</label></span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal float-left paddingRight10">
                <input type="text" id="nbBalls" ng-model="editedIntervention.nbBalls" ng-blur="computeSpendingTime()" ag-integer pattern="\d+"/>
              </span>
              <span class="input-append column-normal">
                <span class="add-on">bal/ha</span>
              </span>
            </div>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label>Temps de travail&nbsp;:</label></span>
          <span class="wwctrl">
            <span class="input-append">
              <span ng-if="spendingTimeErrorMessage">
                {{spendingTimeErrorMessage}}
              </span>
              <span ng-if="!spendingTimeErrorMessage && editedIntervention.spendingTime">
                {{(editedIntervention.spendingTime| number:3)}} h/ha
              </span>
            </span>
          </span>
        </div>

        <span>
          <span>
            <span class="wwlbl"><label></label></span>
            <span class="wwctrl">
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'H_HA'">
                PSCi x débit de chantier
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'HA_H'">
                PSCi x 1/(débit de chantier)
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'VOY_H'">
                (PSCi x dose/ha) / (débit de chantier x vol/voy)
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'BAL_H'">
                (PSCi x bal/ha) / débit de chantier
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'T_H'">
                (PSCi x rendement) / débit de chantier
              </span>
            </span>
          </span>
        </span>

        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_progressionSpeed">Vitesse d'avancement&nbsp;:</label></span>
          <span class="wwctrl">
            <span class="input-append">
              <input type="text" id="intervention_progressionSpeed" ng-model="editedIntervention.progressionSpeed" placeholder="ex. : 25" ag-integer pattern="\d+"/>
              <span class="add-on">km/h</span>
            </span>
          </span>
        </div>
        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_involvedPeopleCount">Nombre de personnes mobilisées&nbsp;:</label></span>
          <span class="wwctrl"><input type="text"
                                      id="intervention_involvedPeopleCount"
                                      ng-model="editedIntervention.involvedPeopleCount"
                                      ng-change="processInvolvedPeopleNumberChange()"
                                      placeholder="ex. : 3.5" ag-float-positive pattern="^\d*[\.,]?\d*$"/>
          </span>
        </div>
      </div>

      <!-- Actions et intrants -->
      <jsp:include page="../itk/species-actions-and-inputs.jsp" />

  </fieldset>

  <div id="confirmDeleteIntervention" title="Suppression d'intervention(s)" class="auto-hide">
    Êtes-vous sûr(e) de vouloir supprimer
    <ng-pluralize count="removeInterventionsContext.interventions.length" when="{'0': '', '1': 'l\'intervention&nbsp;:', 'other': 'les interventions&nbsp;:'}"></ng-pluralize>
    <div ng-repeat="intervention in removeInterventionsContext.interventions">
      - {{intervention.name | orDash}}
    </div>
  </div>

  <div id="confirmDialog" title="Attention" class="auto-hide">
    <span ng-if="warningMessage" ng-bind-html="warningMessage"></span>
  </div>

  <div id="infoDialog" title="Information" class="auto-hide">
    <span ng-if="infoMessage" ng-bind-html="infoMessage"></span>
  </div>

</div>
