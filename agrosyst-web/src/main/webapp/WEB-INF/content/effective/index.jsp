<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Actes réalisés</title>
    <content tag="current-category">effective</content>
  </head>
  <body>
    
    <div class="presentation-page">
      <h1>Actes réalisés</h1>
      
      <div>
      
      Au sein de ce menu, par campagne agricole, pour une zone d’une parcelle, vous pouvez :
      <ul>
        <li>détailler les interventions culturales réalisées ;</li>
        <li>décrire les observations/mesures effectuées et leurs résultats.</li>
      </ul>
        
      Les <strong>actes réalisés</strong> d’un système de culture correspondent à la description détaillée de qui se déroule sur chaque zone de chaque parcelle du système de culture, pour chaque campagne agricole : interventions culturales, observations, mesures.<br/><br/>
        
      Pour faciliter votre navigation dans ce menu et voir apparaître les zones et parcelles d’un seul domaine ou d’une campagne en particulier, n’hésitez pas à utiliser le contexte de navigation en bas de l’écran. 
        
      </div>
    </div>

  </body>
</html>
