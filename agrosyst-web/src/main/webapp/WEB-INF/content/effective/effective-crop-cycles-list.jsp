<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2024 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title><s:if test="measurement">
                Mesures et observations
              </s:if>
              <s:else>
                Interventions culturales
              </s:else></title>
     <content tag="current-category">effective</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/effective-list.js' /><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript">
         angular.module('EffectiveCropCyclesList', ['Agrosyst'])
            .value('EffectiveCropCyclesInitData', {
              zoneListResult: <s:property value="toJson(zoneListResult)" escapeHtml="false"/>,
              measurement: <s:property value="measurement" escapeHtml="false"/>,
              plotFilter: <s:property value="toJson(plotFilter)" escapeHtml="false"/>
            })
            .value('asyncThreshold', <s:property value="exportAsyncThreshold"/>)
            .value('measurementAsyncThreshold', <s:property value="measurementExportAsyncThreshold"/>);
     </script>
  </head>
  <body>
    <div ng-app="EffectiveCropCyclesList" ng-controller="EffectiveCropCyclesListController">
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; 
          <s:if test="measurement">
                Mesures et observations
              </s:if>
              <s:else>
                Interventions culturales
              </s:else></li>
      </ul>
    </div>
    
    <ul class="float-right actions">
      <li>
        <a ng-if="(selectedEntities|toSelectedLength) != 1"
           class="action-dupliquer"
           ng-class="{'button-disabled': true}">Dupliquer</a>
        <a ng-if="(selectedEntities|toSelectedLength) == 1"
           class="action-dupliquer"
           ng-class="{'button-disabled': false}"
           ng-click="effectiveDuplicate()">Dupliquer</a>
      </li>
      <%-- <li><a class="action-export-pdf button-disabled">Export PDF</a></li> --%>
      <s:if test="measurement">
        <li ng-if="(selectedEntities|toSelectedLength) < measurementAsyncThreshold">
          <a class="action-export-xls"
            ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
            onclick="effectiveMeasurementsExport($('#effectiveListForm'));return false;">Export XLS
          </a>
        </li>
        <li ng-if="(selectedEntities|toSelectedLength) >= measurementAsyncThreshold">
          <a class="action-export-xls"
           ng-click="asyncEffectiveMeasurementsExport()">Export XLS
          </a>
        </li>
      </s:if>
      <s:else>
        <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
            onclick="effectiveCropCyclesExport($('#effectiveListForm'));return false;">Export XLS</a></li>
        <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
            ng-click="asyncEffectiveCropCyclesExport()">Export XLS</a></li>
      </s:else>
      <li>
        <a
           class="action-desactiver"
           ng-disabled="relatedDomainIds.length !== 1 || filter.zoneState == 'false'"
           ng-class="{'button-disabled': relatedDomainIds.length !== 1 || filter.zoneState == 'false'}"
           title="Attention, lors d'une sélection multiple, il est seulement possible d'activer/désactiver les zones et parcelles de même domaine et campagne."
           ng-click="unActivateZone()">Désactiver zone
        </a>
      </li>
      <li>
        <a
           class="action-desactiver"
           ng-disabled="relatedDomainIds.length !== 1 || filter.plotState == 'false'"
           ng-class="{'button-disabled': relatedDomainIds.length !== 1 || filter.plotState == 'false'}"
           title="Attention, lors d'une sélection multiple, il est seulement possible d'activer/désactiver les zones et parcelles de même domaine et campagne."
           ng-click="unActivatePlot()">Désactiver Parcelle
        </a>
      </li>
      <li>
        <a
          class="action-activer"
          ng-disabled="relatedDomainIds.length !== 1 || (filter.plotState !== 'false' && filter.zoneState !== 'false')"
          ng-class="{'button-disabled': (relatedDomainIds.length !== 1 || (filter.plotState !== 'false' && filter.zoneState !== 'false'))}"
          title="Attention, lors d'une sélection multiple, il est seulement possible d'activer/désactiver les zones et parcelles de même domaine et campagne."
          ng-click="activatePlot()">Activer
        </a>
      </li>
    </ul>

    <form method="post" id="effectiveListForm">
      <input type="hidden" name="selectedEffectiveCropCycleIds" value="{{selectedEntities|toSelectedArray}}" />
      <input type="hidden" name="fromZoneId" value="{{effectiveDuplicateObject.fromZoneId}}"/>
      <input type="hidden" name="toZoneId" value="{{effectiveDuplicateObject.toZoneId}}"/>

      <table class="entity-list clear fixe_layout" id="effective-crop-cycles-list-table">
        <thead>
          <tr class="doubleHeight">
            <th scope="col" class="column-tiny"
                title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
            </th>
            <th scope="col" class="column-large" ng-click="changeSort('ZONE')">
              <sort>
                <header_label_b>
                  <s:if test="measurement">
                    <span>Mesures et observations</span>
                  </s:if>
                  <s:else>
                    <span>Interventions culturales</span>
                  </s:else>
                  <span>de la zone</span>
                </header_label_b>
                <em ng-if="filter.sortedColumn == 'ZONE'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.ZONE, 'fa fa-sort-amount-desc ':sortColumn.ZONE}" ></em>
                <em ng-if="filter.sortedColumn != 'ZONE'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('PLOT')">
              <sort>
                <header_label>Parcelle</header_label>
                <em ng-if="filter.sortedColumn == 'PLOT'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.PLOT, 'fa fa-sort-amount-desc ':sortColumn.PLOT}" ></em>
                <em ng-if="filter.sortedColumn != 'PLOT'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('DOMAIN')">
              <sort>
                <header_label>Exploitation ou Domaine expérimental</header_label>
                <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_PLAN')">
              <sort>
                <header_label>Dispositif</header_label>
                <em ng-if="filter.sortedColumn == 'GROWING_PLAN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_PLAN, 'fa fa-sort-amount-desc ':sortColumn.GROWING_PLAN}" ></em>
                <em ng-if="filter.sortedColumn != 'GROWING_PLAN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_SYSTEM')">
              <sort>
                <header_label>Système de culture</header_label>
                <em ng-if="filter.sortedColumn == 'GROWING_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.GROWING_SYSTEM}" ></em>
                <em ng-if="filter.sortedColumn != 'GROWING_SYSTEM'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-large">
              Cultures (Interventions)
            </th>
            <th scope="col" class="column-xsmall" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne">
              <sort>
                <header_label>Camp.</header_label>
                <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
              </sort>
            </th>

            <th scope="col" class="column-small">État zone</th>
            <th scope="col" class="column-small">État parcelle</th>

          </tr>
          <tr>
            <td></td>
            <td><input type="text" ng-model="filter.zoneName" /></td>
            <td><input type="text" ng-model="filter.plotName" /></td>
            <td><input type="text" ng-model="filter.domainName" /></td>
            <td><input type="text" ng-model="filter.growingPlanName" /></td>
            <td><input type="text" ng-model="filter.growingSystemName" /></td>
            <td><input type="text" ng-model="filter.croppingPlanInfo" /></td>
            <td><input type="text" ng-model="filter.campaign" /></td>
            <td>
              <select ng-model="filter.zoneState">
                <option value="" />
                <option value="true">Active</option>
                <option value="false">Inactive</option>
              </select>
            </td>
            <td>
              <select ng-model="filter.plotState">
                <option value="" />
                <option value="true">Active</option>
                <option value="false">Inactive</option>
              </select>
            </td>
          </tr>
        </thead>

        <tbody>
          <tr ng-show="effectiveCropCycles.length == 0"><td colspan="10" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>

          <tr ng-repeat="effectiveCropCycle in effectiveCropCycles"
              ng-class="{'line-selected':selectedEntities[effectiveCropCycle.topiaId]}">
            <td>
              <input type='checkbox'
                     ng-model="selectedEntities[effectiveCropCycle.topiaId]"
                     ng-checked="selectedEntities[effectiveCropCycle.topiaId]"
                     ng-click="toggleSelectedEntity(effectiveCropCycle)"/>
            </td>
            <td class="column-large">
              <s:if test="measurement">
                <a href="<s:url namespace='/effective' action='effective-measurements-edit-input' />?zoneTopiaId={{effectiveCropCycle.topiaId|encodeURIComponent}}">{{effectiveCropCycle.name}}<span ng-if="!effectiveCropCycle.active" class="unactivated">&nbsp;(inactif)</span></a>
              </s:if>
              <s:else>
                <a href="<s:url namespace='/effective' action='effective-crop-cycles-edit-input' />?zoneTopiaId={{effectiveCropCycle.topiaId|encodeURIComponent}}">{{effectiveCropCycle.name}}<span ng-if="!effectiveCropCycle.active" class="unactivated">&nbsp;(inactif)</span></a>
              </s:else>
            </td>
            <td class="column-xlarge-fixed" title="{{effectiveCropCycle.plot.name}} ({{effectiveCropCycle.plot.area}} ha)">
              <a href="<s:url namespace='/plots' action='plots-edit-input' />?plotTopiaId={{effectiveCropCycle.plot.topiaId|encodeURIComponent}}">{{effectiveCropCycle.plot.name}} ({{effectiveCropCycle.plot.area}} ha)<span ng-if="!effectiveCropCycle.plot.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xxslarge-fixed" title="{{effectiveCropCycle.plot.domain.name}}">
              <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId={{effectiveCropCycle.plot.domain.topiaId|encodeURIComponent}}">{{effectiveCropCycle.plot.domain.name}}<span ng-if="!effectiveCropCycle.plot.domain.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xlarge-fixed" title="{{effectiveCropCycle.plot.growingSystem.growingPlan.name}}">
              <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId={{effectiveCropCycle.plot.growingSystem.growingPlan.topiaId|encodeURIComponent}}">{{effectiveCropCycle.plot.growingSystem.growingPlan.name}}<span ng-if="!effectiveCropCycle.plot.growingSystem.growingPlan.active &&effectiveCropCycle.plot.growingSystem.growingPlan.topiaId" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xlarge-fixed" title="{{effectiveCropCycle.plot.growingSystem.name}}">
              <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId={{effectiveCropCycle.plot.growingSystem.topiaId|encodeURIComponent}}">{{effectiveCropCycle.plot.growingSystem.name}}<span ng-if="!effectiveCropCycle.plot.growingSystem.active && effectiveCropCycle.plot.growingSystem.topiaId" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td>
              {{effectiveCropCycle.croppingPlanInfo}}
            </td>
            <td class="column-xsmall">
              {{effectiveCropCycle.plot.domain.campaign}}
            </td>
            <td class="column-xsmall">
              {{effectiveCropCycle.active ? 'Active' : 'Inactive'}}
            </td>
            <td class="column-xsmall">
              {{effectiveCropCycle.plot.active ? 'Active' : 'Inactive'}}
            </td>
          </tr>

        </tbody>

        <tfoot>
          <tr>
            <td colspan="10">
              <div class="table-footer">
                <div class="entity-list-info">
                  <span class="counter">{{pager.count}} zones</span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner toutes les zones</a>
                  </span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                  </span>
                  <span class="selection">
                    - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucune sélectionnée', '1': '{} sélectionnée', 'other': '{} sélectionnées'}" />
                  </span>
                </div>
                <div class="pagination">
                  <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                </div>
              </div>
            </td>
          </tr>
        </tfoot>
      </table>
    </form>

    <div id="confirmValidateDuplication" title="Validation " class="auto-hide">
      <div ng-if="availableZonesForDuplication.length === 0">Aucune zone de disponible pour la duplication</div>
      <div ng-if="availableZonesForDuplication.length > 0">
        <div class="wwgrp">
          <span class="wwlbl">
            <label for="zoneIdForDuplication"> Dupliquer le réalisé vers la zone :</label>
          </span>
          <span class="wwctrl">
            <select id="zoneIdForDuplication"
                    ng-model="effectiveDuplicateObject.toZoneId"
                    ng-options="zone.topiaId as zone.name + ' (' + zone.plot.name + ' - ' + zone.plot.domain.campaign + ')' for zone in availableZonesForDuplication">
            </select>
          </span>
        </div>
      </div>
    </div>

    <div id="confirmUnactivateEffectiveCropCycles" title="Activer/Désactiver des cycles" class="auto-hide">
      Êtes-vous sûr(e) de vouloir {{allSelectedEffectiveCropCycleActive?'désactiver':'activer'}}
      <ng-pluralize count="(selectedEntities|toSelectedLength)"
           when="{'one': 'le cycle {{firstSelectedEffectiveCropCycle.name}}',
                   'other': 'les {} cycles sélectionnés'}"></ng-pluralize> ?
    </div>

    <div id="confirmUnactivateDomains" title="Activation/Désactivation des domaines" class="auto-hide">
      Êtes-vous sûr(e) de vouloir {{allSelectedDomainActive?'désactiver':'activer'}}
      <ng-pluralize count="(selectedEntities|toSelectedLength)"
           when="{'one': 'le domaine {{firstSelectedDomain.name}}',
                   'other': 'les {} domaines sélectionnés'}"></ng-pluralize> ?
    </div>
    
    </div>
  </body>
</html>
