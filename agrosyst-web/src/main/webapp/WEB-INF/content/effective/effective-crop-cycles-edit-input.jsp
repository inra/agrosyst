<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Interventions culturales '<s:property value="zone.name" />'</title>
    <content tag="current-category">effective</content>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/effective.css' /><s:property value='getVersionSuffix()'/>" />
    <!-- Select2 theme -->
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />

    <script type="text/javascript" src="<s:url value='/nuiton-js/effective.js' /><s:property value='getVersionSuffix()'/>"></script>

    <script type="text/javascript">
      angular.module('EffectiveCropCyclesEditModule', ['Agrosyst', 'ui.date', 'ui.select', 'ui.switch', 'ngSanitize'])
      .value('EffectiveCropCyclesInitData', {
        zoneTopiaId: "<s:property value='zone.topiaId'/>",
        seasonalCropCycles: <s:property value='effectiveSeasonalCropCyclesJson' escapeHtml="false" />,
        perennialCropCycles: <s:property value='effectivePerennialCropCyclesJson' escapeHtml="false" />,
        croppingPlanModel: <s:property value='toJson(effectiveCropCyclesMainModels)' escapeHtml="false" />,
        intermediateCroppingPlanModel: <s:property value='toJson(effectiveCropCyclesIntermediateModels)' escapeHtml="false" />,
        zoneCroppingPlanEntries: <s:property value="croppingPlanEntriesJson" escapeHtml="false" />,
        domainId: <s:property value='toJson(domainId)' escapeHtml="false"/>,
        campaign:<s:property value='toJson(campaign)' escapeHtml="false"/>,
        campaignsBounds: <s:property value="toJson(campaignsBounds)" escapeHtml="false" />
      })
      .value('ItkInitData', {
        prefix: "effective",
        mineralProductTypes: <s:property value="toJson(mineralProductTypes)" escapeHtml="false" />,
        treatmentTargetCategories: <s:property value="toJson(treatmentTargetCategories)" escapeHtml="false" />,
        treatmentTargetCategoriesByParent: <s:property value="toJson(treatmentTargetCategoriesByParent)" escapeHtml="false" />,
        groupesCibles: <s:property value="toJson(groupesCibles)" escapeHtml="false" />,
        actaTreatmentProductTypes: <s:property value="toJson(actaTreatmentProductTypes)" escapeHtml="false" />,
        currentCampaign:"<s:property value="zone.plot.domain.campaign" />",
        sector: <s:property value='toJson(growingSystemSector)' escapeHtml="false" />,
        isOrganic: <s:property value='toJson(organic)' escapeHtml="false"/>,
        mineralProductElementNamesAndLibelles: <s:property value='toJson(mineralProductElementNamesAndLibelles)' escapeHtml="false"/>,
        cattles: <s:property value="toJson(cattles)" escapeHtml="false" />,
        defaultDestinationName: <s:property value="toJson(defaultDestinationName)" escapeHtml="false" />,
        substratesByCaracteristic1: <s:property value="toJson(substratesByCaracteristic1)" escapeHtml="false" />,
        refPots: <s:property value="toJson(refPots)" escapeHtml="false" />,
        productTypes: <s:property value="toJson(productTypes)" escapeHtml="false" />
      })
      .value('I18nMessages', {
        toolscouplingLoadingActionsFailed: '<s:text name="itk-messages-js-toolscouplingLoadingActionsFailed" />',
        removeSeedingUsages1: '<s:text name="itk-messages-js-removeSeedingUsages1" />',
        removeSeedingUsagesSeveral: '<s:text name="itk-messages-js-removeSeedingUsagesSeveral" />',
        unknownVariety: '<s:text name="itk-messages-js-unknownVariety" />',
        deleteNotMigrateValorisationsConfirm: '<s:text name="itk-messages-js-deleteNotMigrateValorisationsConfirm" />',
        interventionDeleted: '<s:text name="itk-messages-js-interventionDeleted" />',
        interventionsDeleted: '<s:text name="itk-messages-js-interventionsDeleted" />',
        commonCulturePrincipale: '<s:text name="common-culture-principale" />',
        commonCultureIntermediaire: '<s:text name="common-culture-intermediaire" />',
        commonCultureDerobee: '<s:text name="common-culture-derobee" />',
        commonCroppingPlanMixSpecies: '<s:text name="common-cropping-plan-mix-species" />',
        commonCroppingPlanMixVarieties: '<s:text name="common-cropping-plan-mix-varieties" />',
        connectionCropChangeImpactFromIntermediateOne: '<s:text name="itk-messages-js-connectionCropChangeImpactFromIntermediateOne" />',
        connectionCropChangeImpactFromIntermediateSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactFromIntermediateSeveral" />',
        connectionCropChangeImpactFromMainOne: '<s:text name="itk-messages-js-connectionCropChangeImpactFromMainOne" />',
        connectionCropChangeImpactFromMainSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactFromMainSeveral" />',
        connectionCropChangeImpactToIntermediateOne: '<s:text name="itk-messages-js-connectionCropChangeImpactToIntermediateOne" />',
        connectionCropChangeImpactToIntermediateSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactToIntermediateSeveral" />',
        connectionCropChangeImpactToMainOne: '<s:text name="itk-messages-js-connectionCropChangeImpactToMainOne" />',
        connectionCropChangeImpactToMainSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactToMainSeveral" />',
        loadDestinationsContextDataError: '<s:text name="itk-messages-js-loadDestinationsContextDataError" />',
        loadDestinationsContextError: '<s:text name="itk-messages-js-loadDestinationsContextError" />',
        destinationUnitChange: '<s:text name="itk-messages-js-destinationUnitChange" />',
        psciNotComputable: '<s:text name="itk-messages-js-psciNotComputable" />',
        noWorkRate: '<s:text name="itk-messages-js-noWorkRate" />',
        noWorkRateUnit: '<s:text name="itk-messages-js-noWorkRateUnit" />',
        noTransitVolume: '<s:text name="itk-messages-js-noTransitVolume" />',
        noTransitVolumeUnit: '<s:text name="itk-messages-js-noTransitVolumeUnit" />',
        inconsistentUnits: '<s:text name="itk-messages-js-inconsistentUnits" />',
        noInputDose: '<s:text name="itk-messages-js-noInputDose" />',
        inputDoseRequired: '<s:text name="itk-messages-js-inputDoseRequired" />',
        balHaRequired: '<s:text name="itk-messages-js-balHaRequired" />',
        noHarvestAction: '<s:text name="itk-messages-js-noHarvestAction" />',
        noYieldOnHarvestAction: '<s:text name="itk-messages-js-noYieldOnHarvestAction" />',
        wrongUnitsOnHarvestAction: '<s:text name="itk-messages-js-wrongUnitsOnHarvestAction" />',
        catchCropSameCampaignAsPreviousWarning: '<s:text name="itk-messages-js-wrongUnitsOnHarvestAction" />',
        inconsistentUnitsOnHarvestAction: '<s:text name="itk-messages-js-inconsistentUnitsOnHarvestAction" />',
        inconsistentWorkrateUnit: '<s:text name="itk-messages-js-inconsistentWorkrateUnit" />',
        invalidDates: '<s:text name="itk-messages-js-invalidDates" />',
        copyPasteDataLoadingError: '<s:text name="itk-messages-js-copyPasteDataLoadingError" />',
        copyPasteIncompleteMigration: '<s:text name="itk-messages-js-copyPasteIncompleteMigration" />',
        referentialEdiLoadingError: '<s:text name="itk-messages-js-referentialEdiLoadingError" />',
        and: '<s:text name="common-and" />',
        removeSpeciesUsagesWarning1: '<s:text name="itk-messages-js-removeSpeciesUsagesWarning-one" />',
        removeSpeciesUsagesWarningSeveral: '<s:text name="itk-messages-js-removeSpeciesUsagesWarning-several" />',
        removeSpeciesInputDeleted1: '<s:text name="itk-messages-js-removeSpeciesInputDeleted-one" />',
        removeSpeciesInputDeletedSeveral: '<s:text name="itk-messages-js-removeSpeciesInputDeleted-several" />',
        warningAddHarvestSpecies: '<s:text name="itk-messages-js-warningAddHarvestSpecies" />',
        interventionTypeTooltipPhyto: '<s:text name="itk-messages-js-interventionTypeTooltip-phyto" />',
        interventionTypeTooltipLutteBio: '<s:text name="itk-messages-js-interventionTypeTooltip-lutteBio" />',
        toolscouplingLoadingFailed: '<s:text name="itk-messages-js-toolscouplingLoadingFailed" />',
        warning: '<s:text name="common-warning" />',
        removeToolsCouplingSelectedToolsCouplings: '<s:text name="itk-messages-js-removeToolsCoupling-selectedToolsCouplings" />',
        removeToolsCouplingActionsToRemoveOne: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToRemove-one" />',
        removeToolsCouplingActionsToRemoveSeveral: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToRemove-several" />',
        removeToolsCouplingActionsConcernedOne: '<s:text name="itk-messages-js-removeToolsCoupling-actionsConcerned-one" />',
        removeToolsCouplingActionsConcernedSeveral: '<s:text name="itk-messages-js-removeToolsCoupling-actionsConcerned-several" />',
        removeToolsCouplingActionsToMigrateOne: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToMigrate-one" />',
        removeToolsCouplingActionsToMigrateSeveral: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToMigrate-several" />',
        mainActionAdded: '<s:text name="itk-messages-js-mainActionAdded" />',
        mainActionAddedMissingInfo: '<s:text name="itk-messages-js-mainActionAdded-missingInfo" />',
        missingProportionOfTreatedSurface: '<s:text name="itk-messages-js-missingProportionOfTreatedSurface" />',
        seedingActionMissingSeedLot: '<s:text name="itk-messages-js-seedingActionMissingSeedLot" />',
        missingBoiledQuantity: '<s:text name="itk-messages-js-missingBoiledQuantity" />',
        missingWaterQuantityAverage: '<s:text name="itk-messages-js-missingWaterQuantityAverage" />',
        missingWineValorisations: '<s:text name="itk-messages-js-missingWineValorisations" />',
        saveValorisation: '<s:text name="itk-messages-js-saveValorisation" />',
        missingValorisations: '<s:text name="itk-messages-js-missingValorisations" />',
        valorisationPartsSumError: '<s:text name="itk-messages-js-valorisationPartsSumError" />',
        missingDestination: '<s:text name="itk-messages-js-missingDestination" />',
        missingYieldUnit: '<s:text name="itk-messages-js-missingYieldUnit" />',
        declareValorisation: '<s:text name="itk-messages-js-declareValorisation" />',
        fixYields: '<s:text name="itk-messages-js-fixYields" />',
        missingMainAction: '<s:text name="itk-messages-js-missingMainAction" />',
        invalidUsage: '<s:text name="itk-messages-js-invalidUsage" />',
        unavailableReferenceDose: '<s:text name="itk-messages-js-unavailableReferenceDose" />',
        referenceDosesLoadingFailed: '<s:text name="itk-messages-js-referenceDosesLoadingFailed" />',
        referenceDoseMissingSpecies: '<s:text name="itk-messages-js-referenceDoseMissingSpecies" />',
        treatmentTargetsLoadingFailed: '<s:text name="itk-messages-js-treatmentTargetsLoadingFailed" />',
      });

      angular.element(document).ready(
        function draw_diagram() {
          var diagramElem = $("#cropCycleDiagramDiv");
          diagramElem.cropCycleDiagram({
            nodeClickCallback: function(event) {
              $('#tab_0').scope().setSelectedNode(event.data);
            },
            connectionClickCallback: function(diag, connData) {
              $('#tab_0').scope().setSelectedConnection(connData);
            },
            dataModificationCallback: function(methodName, data, nodeOrConn) {
              // some event are fired by angular himself
              // to not fired event back to angular to avoid $apply bug
              if (methodName != 'updateConnection' && methodName != 'removeConnection' && methodName != 'updateNode') {
                $('#tab_0').scope().setData(data);
              } else if (methodName == 'updateNode') {
                $('#tab_1').scope().$apply(); // force le refresh
              }
            }
          });

          // tell angular to init diagramElem with current angular data (nodes, connections and models)
          $('#tab_0').scope().initChartJs();

          // force diagram redraw on tab switch
          $('#tabs-seasonal-crop-cycle-li').click(function(){
              $('#cropCycleDiagramDiv').cropCycleDiagram('repaintEverything');
              $('#tab_0').scope().initSeasonalCropCycleTab();
          });

          $('#tabs-perennial-crop-cycle-li').click(function(){
            $('#tab_1').scope().initPerennialCropCycleTab();
          });

          $('#tabs-itk-li').click(function(){
            $('#tab_2').scope().initItkTab();
          });
        }
      );
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("La zone sur laquelle vous travaillez est inactive et/ou est liée à une parcelle/un domaine inactif(s). Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

    <s:if test="%{zone.plot.growingSystem == null}">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            for (var anchor of ["#tabs-effective-crop-cycles-toasts"]) {
              addInlineMessage(anchor, "warning",
                "La zone sur laquelle vous travaillez est sur une parcelle rattachée à aucun système de culture." +
                "Allez dans \"Système de culture\" puis \"Parcelles\" pour affecter des parcelles à un système de culture."
              );
            }
          }
        );
      </script>
    </s:if>

  </head>
  <body>
    <div ng-app="EffectiveCropCyclesEditModule" ng-controller="EffectiveCropCyclesEditController" id="effective-crop-cycles-edit-controller" class="page-content">

      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url action='effective-crop-cycles-list' namespace='/effective' />">Intervention culturales</a></li>
          <li>&gt; <s:property value="zone.name" /></li>
        </ul>
      </div>

      <ul class="actions">
        <li><a class="action-retour" href="<s:url action='effective-crop-cycles-list' namespace='/effective' />">Retour à la liste des zones des parcelles</a></li>
      </ul>

      <ul class="float-right informations">
        <li>
          <span class="label">Campagne</span>
          <s:property value="zone.plot.domain.campaign" /> (<s:property value="(zone.plot.domain.campaign)-1" /> - <s:property value="zone.plot.domain.campaign" />)
        </li>
        <li>
          <span class="label">Zone<s:if test="!zone.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/effective' action='effective-crop-cycles-edit-input'/>?zoneTopiaId=<s:property value='zone.topiaId'/>"><s:property value="zone.name" /></a>
        </li>
        <li>
          <span class="label">Parcelle<s:if test="!zone.plot.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/plots' action='plots-edit-input'/>?plotTopiaId=<s:property value='zone.plot.topiaId'/>"><s:property value="zone.plot.name" /></a>
        </li>
        <s:if test="zone.plot.growingSystem != null">
          <li>
            <span class="label">Système de culture<s:if test="!zone.plot.growingSystem.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId=<s:property value='zone.plot.growingSystem.topiaId'/>"><s:property value="zone.plot.growingSystem.name" /></a>
          </li>
          <li>
            <span class="label">Dispositif<s:if test="!zone.plot.growingSystem.growingPlan.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value='zone.plot.growingSystem.growingPlan.topiaId'/>"><s:property value="zone.plot.growingSystem.growingPlan.name" /></a>
          </li>
        </s:if>
        <li>
          <span class="label">Domaine<s:if test="!zone.plot.domain.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='zone.plot.domain.topiaId'/>"><s:property value="zone.plot.domain.name" /></a>
        </li>
        <s:if test="%{zone.topiaId != null}">
          <li class="highlight"><span class="label">Pièces jointes</span><a href="#" id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='zone.topiaId' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(zone.topiaId)" /></a></li>
        </s:if>
      </ul>

      <s:if test="relatedZones != null">
        <ul class="timeline">
          <s:iterator value="relatedZones" var="relatedZone">
              <li<s:if test="#relatedZone.value.equals(zone.topiaId)"> class="selected"</s:if>>
                <a href="<s:url namespace='/effective' action='effective-crop-cycles-edit-input' />?zoneTopiaId=<s:property value='value'/>"><s:property value="key" /></a>
              </li>
          </s:iterator>
        </ul>
      </s:if>

      <form name="effectiveCropCyclesEditForm" action="<s:url action='effective-crop-cycles-edit' namespace='/effective' />" method="post" style="overflow:inherit;" class="tabs clear" ag-confirm-on-exit>
        <input type="hidden" name="zoneTopiaId" value="<s:property value='zone.topiaId'/>"/>

        <s:actionerror escape="false" cssClass="send-toast-to-js"/>

        <ul id="tabs-menu" class="tabs-menu clearfix">
          <li id="tabs-seasonal-crop-cycle-li" class="selected"><span>Culture(s) assolée(s) de la campagne</span></li>
          <li id="tabs-perennial-crop-cycle-li"><span>Culture(s) pérenne(s)<span class="content-italic">Caractéristiques de la plantation</span></span></li>
          <li id="tabs-itk-li" ng-click="initItkTab()"><span>Interventions culturales</span></li>
        </ul>

        <div id="tabs-content" class="tabs-content">

          <!-- Culture(s) assolée(s) de la campagne -->
          <div id="tab_0" ng-controller="EffectiveCropCyclesSeasonnalController">
            <input type="hidden" name="effectiveSeasonalCropCyclesJson" value="{{seasonalCropCycles}}" />

            <div id="tabs-effective-crop-cycles-toasts"></div>

            <div ng-show="seasonalCropCycles.length == 0">
              <input type="button" class="btn" value="Déclarer la/les culture(s) assolée(s) de la campagne" ng-click="createEffectiveSeasonalCropCycle()"/>
            </div>
            <div ng-show="seasonalCropCycles.length == 1">

              <fieldset>
                <legend class="invisibleLabel">Description d'un cycle de cultures assolées</legend>
                <div class="help-explanation" ng-if="domainId">Cultures du domaine <s:property value="zone.plot.domain.name" /> pour la campagne <s:property value="zone.plot.domain.campaign" /> :</div>
                <div ng-if="zoneTopiaId && !domainId" class="help-explanation">
                  Vous n'êtes pas autorisé à voir les cultures du domaine !'
                </div>
                <div id="diagram" ng-if="domainId">
                  <div id="cropCycleDiagramDiv"></div>
                </div>

                <!-- Noeud sélectionné -->
                <div ng-show="selectedCropCycleNode" id="croppingPlanEntry" class="sub-form marginTop30">
                  <div id="croppingPlanEntryDetails" class="sub-form-content">
                    <a ng-click="deleteCropCycleNode(selectedCropCycleNode)" class="btn btn-darker float-right">Supprimer la culture</a>

                    <div class="noborder">
                      <div class="croppingDetailsField">
                        <label class="historicLabelStyle">Culture&nbsp;:</label>
                        <span class="generated-content">{{selectedCropCycleNode.label}}</span>
                        <span ng-show="selectedCropCycleNode.nodeId != 'NODE_BEFORE'" class="generated-content"> (rang&nbsp;{{selectedCropCycleNode.x + 1}})</span>
                      </div>
                    </div>

                    <!-- Liste des espèces du noeud sélectionné -->
                    <div id="croppingPlanEntrySpecies" class="paddingTop0">
                      <table class="data-table full-width">
                        <thead>
                          <tr>
                            <th scope="col">Espèce</th>
                            <th scope="col">Qualifiant</th>
                            <th scope="col">Type saisonnier</th>
                            <th scope="col">Destination</th>
                            <th scope="col">Cépage/Variété</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-show="!selectedCropCycleNodeSpecies || selectedCropCycleNodeSpecies.length == 0">
                            <td colspan="6" class="empty-table">Aucune espèce n'est renseignée pour cette culture</td>
                          </tr>
                          <tr ng-repeat="cpEntrySpecies in selectedCropCycleNodeSpecies">
                            <td>{{cpEntrySpecies.species.libelle_espece_botanique_Translated}}</td>
                            <td>{{cpEntrySpecies.species.libelle_qualifiant_AEE_Translated|orDash}}</td>
                            <td>{{cpEntrySpecies.species.libelle_type_saisonnier_AEE_Translated|orDash}}</td>
                            <td>{{cpEntrySpecies.species.libelle_destination_AEE_Translated|orDash}}</td>
                            <td ng-if="cpEntrySpecies.variety && (cpEntrySpecies.variety.variete || cpEntrySpecies.variety.denomination)">{{ cpEntrySpecies.variety.variete || cpEntrySpecies.variety.denomination }}</td>
                            <td ng-if="!cpEntrySpecies.variety && cpEntrySpecies.edaplosUnknownVariety"
                                class="warning-label">
                              <i class="fa fa-warning" aria-hidden="true"></i> Variété non reconnue "{{ cpEntrySpecies.edaplosUnknownVariety }}"
                            </td>
                            <td ng-if="(!cpEntrySpecies.variety || !cpEntrySpecies.variety.variete && !cpEntrySpecies.variety.denomination) && !cpEntrySpecies.edaplosUnknownVariety">-</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <!-- Connection sélectionnée -->
                <div ng-show="selectedCropCycleConnection" id="intermediateCroppingPlanEntry" class="sub-form marginTop30">
                  <div id="intermediateCroppingPlanEntryDetails" class="sub-form-content">
                    <a ng-click="deleteCropCycleConnection(selectedCropCycleConnection)" class="btn  btn-darker float-right">Supprimer la connexion</a>

                    <div class="croppingDetailsField noborder">
                      <label class="historicLabelStyle" for="selectedCropCycleConnectionIntermediateCroppingPlanEntry">Culture intermédiaire&nbsp;:</label>
                      <select id="selectedCropCycleConnectionIntermediateCroppingPlanEntry"
                          ng-model="selectedCropCycleConnection.selectedIntermediateCroppingPlanEntry"
                          ng-options="item as item.label for item in intermediateCroppingPlanModel"
                          ng-change="savePreviousSelectedIntermediateCroppingPlanEntry()">
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="help-explanation-panel">
                  <s:text name="help.effectiveCropCycles.drawing" />
                </div>

              </fieldset>

            </div>

            <div id="confirmSeasonalCropChange" title="Les changements seront répercutés dans l'onglet 'Interventions culturales'." class="auto-hide">
              <span>
                <p ng-bind-html="seasonalCropChangeContext.warningMessages"></p>
              </span>
            </div>

          </div>

          <!-- Phases de production de cultures pérennes -->
          <%@include file="effective-crop-cycles-edit-input-perennial.jsp" %>

          <!-- Itinéraire technique -->
          <%@include file="effective-crop-cycles-edit-input-itk.jsp" %>

          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url action='effective-crop-cycles-list' namespace='/effective' />">Annuler</a>
            <input type="submit" id="saveButton" class="btn-primary" value="Enregistrer"
                   <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
                   <s:if test="!activated">disabled="disabled" title="La zone sur laquelle vous travaillez est inactive et/ou est liée à une parcelle/un domaine inactif(s). Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
            />
          </span>

        </div>
      </form>

    </div>
  </body>
</html>
