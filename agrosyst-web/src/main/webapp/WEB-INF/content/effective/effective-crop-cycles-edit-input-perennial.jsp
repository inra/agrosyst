<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- effective-crop-cycles-edit-input-perennial.jsp -->

<div id="tab_1" ng-controller="EffectiveCropCyclesPerennialController">
  <input type="hidden" name="effectivePerennialCropCyclesJson" value="{{perennialCropCycles}}" />

  <div ng-if="zoneTopiaId && !domainId" class="help-explanation">
    Vous n'êtes pas autorisé à voir les cultures du domaine !'
  </div>
  <div ng-show="domainId && !croppingPlanModel.length" class="help-explanation">
    Aucune culture principale ou dérobée associée au domaine, n'est trouvée pour cette campagne.
  </div>
  <ul class="accordion-panel" ng-if="domainId">
    <li ng-repeat="effectivePerennialCropCycleDto in perennialCropCycles">
      <div class="clearfix accordion-element-title"
           ng-class="{selected: selectedPerennialCropCycle == effectivePerennialCropCycleDto}"
           ng-click="toogleCycle(effectivePerennialCropCycleDto)">
        <a ng-if="effectivePerennialCropCycleDto.croppingPlanEntryId">{{croppingPlanEntriesIndex[effectivePerennialCropCycleDto.croppingPlanEntryId].name}}</a>
        <a ng-if="!effectivePerennialCropCycleDto.croppingPlanEntryId">Sélectionner la culture présente sur la zone</a>
        <input type="button" class="btn float-right" value="Supprimer" ng-click="deleteEffectivePerennialCropCycle(effectivePerennialCropCycleDto)"/>
      </div>
      <div class="accordion-element-content" ng-show="selectedPerennialCropCycle == effectivePerennialCropCycleDto"
           ng-form="effectivePerennialCropCycleForm">
        <div id="tabs-content-effective-perennial-crop-cycle_{{$index}}">
          <fieldset class="entity-content">
            <legend class="invisibleLabel">Description d'un cycle de culture pérenne</legend>
            <!-- Colonne de gauche -->
            <div class="two-columns">
              <div class="wwgrp">
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_croppingPlanEntryField"><span class="required">*</span> Culture&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="PerennialCropEdition{{$index}}_croppingPlanEntryField"
                          ng-model="effectivePerennialCropCycleDto.selectedPerennialCropCycleCroppingPlanEntryId"
                          ng-change="croppingPlanEntrySelected()"
                          required>
                    <option value=""></option>
                    <s:iterator value="effectiveCropCyclesMainModels" var="croppingPlanEntry">
                      <option value="${croppingPlanEntry.croppingPlanEntryId}">${croppingPlanEntry.label}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>

              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_phaseTypeField"><span class="required">*</span> Phase&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="PerennialCropEdition{{$index}}_phaseTypeField"
                          ng-model="effectivePerennialCropCycleDto.phaseDtos[0].type"
                          ng-options="key as value for (key, value) in perennialPhaseTypes"
                          required>
                          <!-- the following line is required to enable HTML5 validation-->
                          <option value=""></option>
                  </select>
                </span>
              </div>

              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_phaseDurationField">Durée de la phase&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="PerennialCropEdition{{$index}}_phaseDurationField"
                           type="text"
                           ng-model="effectivePerennialCropCycleDto.phaseDtos[0].duration"
                           ag-integer pattern="\d+"
                           placeholder="ex. : 3"/>
                    <span class="add-on">an(s)</span>
                  </span>
                </span>
              </div>

              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingYearField">Année de plantation&nbsp;:</label></span>
                <span class="wwctrl">
                  <input id="PerennialCropEdition{{$index}}_plantingYearField" type="text" ng-model="effectivePerennialCropCycleDto.plantingYear" placeholder="ex. : 2013" ag-campaign pattern="(?:19|20)[0-9]{2}"/>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingInterFurrowField">Inter-rang de plantation&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="PerennialCropEdition{{$index}}_plantingInterFurrowField" type="text" ng-model="effectivePerennialCropCycleDto.plantingInterFurrow" placeholder="ex. : 300" ag-integer pattern="\d+"/>
                    <span class="add-on">cm</span>
                  </span>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingSpacing">Espacement de plantation sur le rang&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="PerennialCropEdition{{$index}}_plantingSpacing" type="text" ng-model="effectivePerennialCropCycleDto.plantingSpacing" placeholder="ex. : 300" ag-integer pattern="\d+"/>
                    <span class="add-on">cm</span>
                  </span>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingDensityField"><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Densité de plantation&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="PerennialCropEdition{{$index}}_plantingDensityField" type="text" ng-model="effectivePerennialCropCycleDto.plantingDensity" placeholder="ex. : 400" ag-float-positive pattern="^\d*[\.,]?\d*$" />
                    <span class="add-on">Plantes/ha</span>
                  </span>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="orchardFrutalFormField_{{$index}}">Forme fruitière vergers&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="orchardFrutalFormField_{{$index}}" ng-model="effectivePerennialCropCycleDto.orchardFrutalForm">
                    <option value=""></option>
                    <s:iterator value="orchardFrutalForms">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_foliageHeight">Hauteur de frondaison&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="PerennialCropEdition{{$index}}_foliageHeight" type="text" ng-model="effectivePerennialCropCycleDto.foliageHeight" placeholder="ex. : 330" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
                    <span class="add-on">cm</span>
                  </span>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_foliageThickness">Épaisseur de frondaison&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="PerennialCropEdition{{$index}}_foliageThickness" type="text" ng-model="effectivePerennialCropCycleDto.foliageThickness" placeholder="ex. : 170" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
                    <span class="add-on">cm</span>
                  </span>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="vineFrutalFormField_{{$index}}">Forme fruitière vigne&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="vineFrutalFormField_{{$index}}" ng-model="effectivePerennialCropCycleDto.vineFrutalForm">
                    <option value=""></option>
                    <s:iterator value="vineFrutalForms">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="orientationField_{{$index}}">Orientation des rangs&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="orientationField_{{$index}}" ng-model="effectivePerennialCropCycleDto.orientationId">
                    <option value=""></option>
                    <s:iterator value="refOrientationEDIs">
                      <option value="${topiaId}">${reference_label}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>
            </div>
            <!-- Colonne de droite -->
            <div class="two-columns">
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingDeathRateField">Taux de mortalité dans la plantation&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="PerennialCropEdition{{$index}}_plantingDeathRateField" type="text" ng-model="effectivePerennialCropCycleDto.plantingDeathRate" placeholder="ex. : 10.5" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$"/>
                    <span class="add-on">%</span>
                  </span>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingDeathRateMeasureYearField">Année de mesure de ce taux de mortalité&nbsp;:</label></span>
                <span class="wwctrl">
                  <input id="PerennialCropEdition{{$index}}_plantingDeathRateMeasureYearField" type="text" ng-model="effectivePerennialCropCycleDto.plantingDeathRateMeasureYear" placeholder="ex. : 2012" ag-campaign pattern="(?:19|20)[0-9]{2}"/>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_weedTypeField"><span class="required">*</span> Type d'enherbement&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="PerennialCropEdition{{$index}}_weedTypeField" ng-model="effectivePerennialCropCycleDto.weedType" required>
                    <s:iterator value="weedTypes">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="pollinatorField_{{$index}}">Pollinisateurs&nbsp;:</label></span>
                <span class="wwctrl">
                  <input type="checkbox" id="pollinatorField_{{$index}}" ng-model="effectivePerennialCropCycleDto.pollinator" />
                </span>
              </div>
              <div ng-show="effectivePerennialCropCycleDto.pollinator" class="slide-animation">
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_pollinatorPercentField">% de pollinisateur&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append_{{$index}}">
                      <input type="text" id="PerennialCropEdition{{$index}}_pollinatorPercentField" ng-model="effectivePerennialCropCycleDto.pollinatorPercent" placeholder="ex. : 15" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$"/>
                      <span class="add-on">%</span>
                    </span>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="pollinatorSpreadModeField_{{$index}}">Mode de répartition des pollinisateurs&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="pollinatorSpreadModeField_{{$index}}" ng-model="effectivePerennialCropCycleDto.pollinatorSpreadMode">
                      <option value=""></option>
                      <s:iterator value="pollinatorSpreadModes">
                        <option value="${key}">${value}</option>
                      </s:iterator>
                    </select>
                  </span>
                </div>
              </div>
              <div class="wwgrp" >
                <span class="wwlbl"><label for="otherCharacteristicsField_{{$index}}">Autres caractéristiques du couvert végétal&nbsp;:</label></span>
                <span class="wwctrl">
                  <textarea id="otherCharacteristicsField_{{$index}}" ng-model="effectivePerennialCropCycleDto.otherCharacteristics"></textarea>
                </span>
              </div>
            </div>

            <!-- Tableau des espèces -->
            <div class="table-enclosure clear">
              <label>Espèces&nbsp;:</label>
              <table class="data-table fixed-layout">
                <thead>
                  <tr>
                    <th scope="col">Espèce</th>
                    <th scope="col">Qualifiant</th>
                    <th scope="col">Type saisonnier</th>
                    <th scope="col">Destination</th>
                    <th scope="col">Cépage / Variété</th>
                    <th scope="col">Porte-greffe</th>
                    <th scope="col">Clone de la greffe</th>
                    <th scope="col">Certification des plants</th>
                    <th scope="col">Date de sur-greffage</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-show="effectivePerennialCropCycleDto.speciesDtos.length === 0" ><td colspan="9" class="empty-table">Il n'y a pas d'espèces pour la culture sélectionnée.</td></tr>
                  <tr ng-repeat="cropCyclePerennialSpeciesItem in effectivePerennialCropCycleDto.speciesDtos">
                    <td>{{cropCyclePerennialSpeciesItem.speciesEspece}}</td>
                    <td class="column-small">{{cropCyclePerennialSpeciesItem.speciesQualifiant|orDash}}</td>
                    <td class="column-small">{{cropCyclePerennialSpeciesItem.speciesTypeSaisonnier|orDash}}</td>
                    <td class="column-small">{{cropCyclePerennialSpeciesItem.speciesDestination|orDash}}</td>
                    <td ng-if="cropCyclePerennialSpeciesItem.varietyLibelle">{{ cropCyclePerennialSpeciesItem.varietyLibelle }}</td>
                    <td ng-if="!cropCyclePerennialSpeciesItem.varietyLibelle && cropCyclePerennialSpeciesItem.edaplosUnknownVariety"
                        class="warning-label">
                      <i class="fa fa-warning" aria-hidden="true"></i> Variété non reconnue "{{ cropCyclePerennialSpeciesItem.edaplosUnknownVariety }}"
                    </td>
                    <td ng-if="!cropCyclePerennialSpeciesItem.varietyLibelle && !cropCyclePerennialSpeciesItem.edaplosUnknownVariety">-</td>
                    <td class="column-auto" id="graftSupport-{{$index}}">
                      <ui-select ng-model="cropCyclePerennialSpeciesItem.graftSupport"
                                 title="{{cropCyclePerennialSpeciesItem.graftSupport.label}}"
                                 id="graftSupport-{{$index}}"
                                 ng-disabled="disabled"
                                 theme="select2"
                                 reset-search-input="false"
                                 ng-mouseover="enableAutocompleteGraftSupport($index)"
                                 on-select="bindSelectedCropCyclePerennialSpeciesGraftSupport($item, cropCyclePerennialSpeciesItem)"
                                 style="width:95%;">
                        <ui-select-match placeholder="ex. : Amandier x Pêcher GF 677">{{$select.selected.label}}</ui-select-match>
                        <ui-select-choices repeat="graftSupport in graftSupports track by $index"
                                           refresh="autocompleteGraftSupport($index, cropCyclePerennialSpeciesItem, $select.search)"
                                           refresh-delay="0">
                          <span ng-bind-html="graftSupport.label | highlight: $select.search"></span>
                        </ui-select-choices>
                      </ui-select>
                    </td>
                    <td class="column-auto">
                      <ui-select ng-model="cropCyclePerennialSpeciesItem.graftClone"
                                 id="graftSupportClone-{{$index}}"
                                 title="{{cropCyclePerennialSpeciesItem.graftClone.label}}"
                                 ng-disabled="disabled"
                                 theme="select2"
                                 reset-search-input="false"
                                 ng-mouseover="enableGetAutocompleteGraftClone($index)"
                                 on-select="bindSelectedCropCyclePerennialSpeciesGraftClone($item, cropCyclePerennialSpeciesItem)"
                                 style="width:95%;">
                        <ui-select-match placeholder="ex. : 569, 1978 (Savoie)">{{$select.selected.label}}</ui-select-match>
                        <ui-select-choices repeat="graftSupportClone in graftSupportClones track by $index"
                                           refresh="getAutocompleteGraftClone($index, cropCyclePerennialSpeciesItem, $select.search)"
                                           refresh-delay="0">
                          <span ng-bind-html="graftSupportClone.label | highlight: $select.search"></span>
                        </ui-select-choices>
                      </ui-select>
                    </td>
                    <td class="column-xsmall"><input type="checkbox" ng-model="cropCyclePerennialSpeciesItem.plantsCertified" /></td>
                    <td class="column-auto">
                      <input type="text"
                             ui-date="dateOptions"
                             ng-click="cropCyclePerennialSpeciesItem.overGraftDate = cropCyclePerennialSpeciesItem.overGraftDate ? cropCyclePerennialSpeciesItem.overGraftDate : effectiveCampaign + '-01-01'"
                             ng-model="cropCyclePerennialSpeciesItem.overGraftDate" />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </fieldset>
        </div>
      </div>
      <div id="confirmRemovedEffectivePerennialCropCycle" title="Suppression des caractéristiques de la plantation" class="auto-hide">
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
          Êtes-vous sûr(e) de vouloir supprimer les caractéristiques de la plantation&nbsp;
          <span ng-if="cycleCroppingPlanEntryId">:&nbsp;{{croppingPlanEntriesIndex[cycleCroppingPlanEntryId].name}}&nbsp;</span>?
        </p>
        <p ng-if="cycleCroppingPlanEntryId">
          <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
          {{effectivePerennialCropCycleNbInterventions}}&nbsp;interventions seront supprimées.
        </p>
      </div>

      <div id="confirmCropChange" title="Le changement de culture sera répercuté dans l'onglet 'Interventions culturales'." class="auto-hide">
        <span>
          <p ng-bind-html="cropChange.impactMessage"></p>
        </span>
      </div>
  </ul>
  <input ng-if="domainId" type="button" class="btn full-width marginTop30" value="Déclarer les caractéristiques de la plantation" ng-click="createEffectivePerennialCropCycle()"/>
</div>


