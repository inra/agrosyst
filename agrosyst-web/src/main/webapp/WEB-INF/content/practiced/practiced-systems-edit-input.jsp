<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2024 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  #L%domai
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/practicedSystems.css' /><s:property value='getVersionSuffix()'/>" />
    <script type="text/javascript" src="<s:url value='/nuiton-js/practicedSystems.js' /><s:property value='getVersionSuffix()'/>"></script>
    <!-- Select2 theme -->
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />

    <script type="text/javascript">

      angular.module('PracticedSystemModule', ['Agrosyst', 'ui.date', 'ui.sortable', 'ui.select', 'ui.switch', 'ngSanitize'])
        .value('PracticedSystemInitData', {
          // practised system wide data
          defaultGrowingSystemId : <s:property value='toJson(defaultGrowingSystemId)' escapeHtml="false" />,
          seasonalCropCycles: <s:property value='toJson(practicedSeasonalCropCycleDtos)' escapeHtml="false" />,
          perennialCropCycles: <s:property value="toJson(practicedPerennialCropCycleDtos)" escapeHtml="false" />,
          croppingPlanModel: <s:property value='toJson(practicedSystemMainCropCycleModels)' escapeHtml="false" />,
          intermediateCroppingPlanModel : <s:property value='toJson(practicedSystemIntermediateCropCycleModels)' escapeHtml="false" />,
          croppingPlanEntrySpeciesIndex: <s:property value='toJson(practicedSystemCroppingPlanEntryCodesToSpecies)' escapeHtml="false" />,
          orientationEDIs: <s:property value="toJson(refOrientationEDIs)" escapeHtml="false" />,
          // init data
          growingSystemTopiaId: <s:property value="toJson(growingSystemTopiaId)" escapeHtml="false" />,
          practicedSystemTopiaId: <s:property value="toJson(practicedSystem.topiaId)" escapeHtml="false"/>,
          practicedSystemName: <s:property value="toJson(practicedSystem.name)" escapeHtml="false"/>,
          practicedSystemSource: <s:property value="toJson(practicedSystem.source)" escapeHtml="false"/>,
          practicedSystemComment: <s:property value="toJson(practicedSystem.comment)" escapeHtml="false"/>,
          campaigns: <s:property value="campaigns" escapeHtml="false" />,
          domainId: <s:property value="toJson(domainCode)" escapeHtml="false" />,
          domainTopiaId: <s:property value="toJson(domainId)" escapeHtml="false" />,
          actaTreatmentProductTypes: <s:property value="toJson(actaTreatmentProductTypes)" escapeHtml="false" />,
          campaignsBounds: <s:property value="toJson(campaignsBounds)" escapeHtml="false" />,
          gsCampaigns: <s:property value="toJson(gsCampaigns)" escapeHtml="false" />,
          campaignsBounds: <s:property value="toJson(campaignsBounds)" escapeHtml="false" />,
          frontApp: 'agrosyst',
          domainCampaign: <s:property value="toJson(domainCampaign)" escapeHtml="false" />
        })
        .value('ItkInitData', {
          prefix: "practiced-system",
          mineralProductTypes: <s:property value="toJson(mineralProductTypes)" escapeHtml="false" />,
          treatmentTargetCategories: <s:property value="toJson(treatmentTargetCategories)" escapeHtml="false" />,
          treatmentTargetCategoriesByParent: <s:property value="toJson(treatmentTargetCategoriesByParent)" escapeHtml="false" />,
          groupesCibles: <s:property value="toJson(groupesCibles)" escapeHtml="false" />,
          actaTreatmentProductTypes: <s:property value="toJson(actaTreatmentProductTypes)" escapeHtml="false" />,
          sector: <s:property value='toJson(growingSystemSector)' escapeHtml="false"/>,
          isOrganic: <s:property value='toJson(organic)' escapeHtml="false"/>,
          mineralProductElementNamesAndLibelles: <s:property value='toJson(mineralProductElementNamesAndLibelles)' escapeHtml="false"/>,
          cattles: <s:property value="toJson(cattles)" escapeHtml="false" />,
          defaultDestinationName: <s:property value="toJson(defaultDestinationName)" escapeHtml="false" />,
          substratesByCaracteristic1: <s:property value="toJson(substratesByCaracteristic1)" escapeHtml="false" />,
          refPots: <s:property value="toJson(refPots)" escapeHtml="false" />,
          productTypes: <s:property value="toJson(productTypes)" escapeHtml="false" />
        })
        .value('I18nMessages', {
          toolscouplingLoadingActionsFailed: '<s:text name="itk-messages-js-toolscouplingLoadingActionsFailed" />',
          removeSeedingUsages1: '<s:text name="itk-messages-js-removeSeedingUsages1" />',
          removeSeedingUsagesSeveral: '<s:text name="itk-messages-js-removeSeedingUsagesSeveral" />',
          unknownVariety: '<s:text name="itk-messages-js-unknownVariety" />',
          deleteNotMigrateValorisationsConfirm: '<s:text name="itk-messages-js-deleteNotMigrateValorisationsConfirm" />',
          interventionDeleted: '<s:text name="itk-messages-js-interventionDeleted" />',
          interventionsDeleted: '<s:text name="itk-messages-js-interventionsDeleted" />',
          commonCulturePrincipale: '<s:text name="common-culture-principale" />',
          commonCultureIntermediaire: '<s:text name="common-culture-intermediaire" />',
          commonCultureDerobee: '<s:text name="common-culture-derobee" />',
          commonCroppingPlanMixSpecies: '<s:text name="common-cropping-plan-mix-species" />',
          commonCroppingPlanMixVarieties: '<s:text name="common-cropping-plan-mix-varieties" />',
          connectionCropChangeImpactFromIntermediateOne: '<s:text name="itk-messages-js-connectionCropChangeImpactFromIntermediateOne" />',
          connectionCropChangeImpactFromIntermediateSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactFromIntermediateSeveral" />',
          connectionCropChangeImpactFromMainOne: '<s:text name="itk-messages-js-connectionCropChangeImpactFromMainOne" />',
          connectionCropChangeImpactFromMainSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactFromMainSeveral" />',
          connectionCropChangeImpactToIntermediateOne: '<s:text name="itk-messages-js-connectionCropChangeImpactToIntermediateOne" />',
          connectionCropChangeImpactToIntermediateSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactToIntermediateSeveral" />',
          connectionCropChangeImpactToMainOne: '<s:text name="itk-messages-js-connectionCropChangeImpactToMainOne" />',
          connectionCropChangeImpactToMainSeveral: '<s:text name="itk-messages-js-connectionCropChangeImpactToMainSeveral" />',
          loadDestinationsContextDataError: '<s:text name="itk-messages-js-loadDestinationsContextDataError" />',
          loadDestinationsContextError: '<s:text name="itk-messages-js-loadDestinationsContextError" />',
          destinationUnitChange: '<s:text name="itk-messages-js-destinationUnitChange" />',
          psciNotComputable: '<s:text name="itk-messages-js-psciNotComputable" />',
          noWorkRate: '<s:text name="itk-messages-js-noWorkRate" />',
          noWorkRateUnit: '<s:text name="itk-messages-js-noWorkRateUnit" />',
          noTransitVolume: '<s:text name="itk-messages-js-noTransitVolume" />',
          noTransitVolumeUnit: '<s:text name="itk-messages-js-noTransitVolumeUnit" />',
          inconsistentUnits: '<s:text name="itk-messages-js-inconsistentUnits" />',
          noInputDose: '<s:text name="itk-messages-js-noInputDose" />',
          inputDoseRequired: '<s:text name="itk-messages-js-inputDoseRequired" />',
          balHaRequired: '<s:text name="itk-messages-js-balHaRequired" />',
          noHarvestAction: '<s:text name="itk-messages-js-noHarvestAction" />',
          noYieldOnHarvestAction: '<s:text name="itk-messages-js-noYieldOnHarvestAction" />',
          wrongUnitsOnHarvestAction: '<s:text name="itk-messages-js-wrongUnitsOnHarvestAction" />',
          catchCropSameCampaignAsPreviousWarning: '<s:text name="itk-messages-js-wrongUnitsOnHarvestAction" />',
          inconsistentUnitsOnHarvestAction: '<s:text name="itk-messages-js-inconsistentUnitsOnHarvestAction" />',
          inconsistentWorkrateUnit: '<s:text name="itk-messages-js-inconsistentWorkrateUnit" />',
          invalidDates: '<s:text name="itk-messages-js-invalidDates" />',
          copyPasteDataLoadingError: '<s:text name="itk-messages-js-copyPasteDataLoadingError" />',
          copyPasteIncompleteMigration: '<s:text name="itk-messages-js-copyPasteIncompleteMigration" />',
          referentialEdiLoadingError: '<s:text name="itk-messages-js-referentialEdiLoadingError" />',
          and: '<s:text name="common-and" />',
          removeSpeciesUsagesWarning1: '<s:text name="itk-messages-js-removeSpeciesUsagesWarning-one" />',
          removeSpeciesUsagesWarningSeveral: '<s:text name="itk-messages-js-removeSpeciesUsagesWarning-several" />',
          removeSpeciesInputDeleted1: '<s:text name="itk-messages-js-removeSpeciesInputDeleted-one" />',
          removeSpeciesInputDeletedSeveral: '<s:text name="itk-messages-js-removeSpeciesInputDeleted-several" />',
          warningAddHarvestSpecies: '<s:text name="itk-messages-js-warningAddHarvestSpecies" />',
          interventionTypeTooltipPhyto: '<s:text name="itk-messages-js-interventionTypeTooltip-phyto" />',
          interventionTypeTooltipLutteBio: '<s:text name="itk-messages-js-interventionTypeTooltip-lutteBio" />',
          toolscouplingLoadingFailed: '<s:text name="itk-messages-js-toolscouplingLoadingFailed" />',
          warning: '<s:text name="common-warning" />',
          removeToolsCouplingSelectedToolsCouplings: '<s:text name="itk-messages-js-removeToolsCoupling-selectedToolsCouplings" />',
          removeToolsCouplingActionsToRemoveOne: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToRemove-one" />',
          removeToolsCouplingActionsToRemoveSeveral: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToRemove-several" />',
          removeToolsCouplingActionsConcernedOne: '<s:text name="itk-messages-js-removeToolsCoupling-actionsConcerned-one" />',
          removeToolsCouplingActionsConcernedSeveral: '<s:text name="itk-messages-js-removeToolsCoupling-actionsConcerned-several" />',
          removeToolsCouplingActionsToMigrateOne: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToMigrate-one" />',
          removeToolsCouplingActionsToMigrateSeveral: '<s:text name="itk-messages-js-removeToolsCoupling-actionsToMigrate-several" />',
          mainActionAdded: '<s:text name="itk-messages-js-mainActionAdded" />',
          mainActionAddedMissingInfo: '<s:text name="itk-messages-js-mainActionAdded-missingInfo" />',
          missingProportionOfTreatedSurface: '<s:text name="itk-messages-js-missingProportionOfTreatedSurface" />',
          seedingActionMissingSeedLot: '<s:text name="itk-messages-js-seedingActionMissingSeedLot" />',
          seedingActionNoSeedLot: '<s:text name="itk-messages-js-seedingActionNoSeedLot" />',
          missingBoiledQuantity: '<s:text name="itk-messages-js-missingBoiledQuantity" />',
          missingWaterQuantityAverage: '<s:text name="itk-messages-js-missingWaterQuantityAverage" />',
          missingWineValorisations: '<s:text name="itk-messages-js-missingWineValorisations" />',
          saveValorisation: '<s:text name="itk-messages-js-saveValorisation" />',
          missingValorisations: '<s:text name="itk-messages-js-missingValorisations" />',
          valorisationPartsSumError: '<s:text name="itk-messages-js-valorisationPartsSumError" />',
          missingDestination: '<s:text name="itk-messages-js-missingDestination" />',
          missingYieldUnit: '<s:text name="itk-messages-js-missingYieldUnit" />',
          declareValorisation: '<s:text name="itk-messages-js-declareValorisation" />',
          fixYields: '<s:text name="itk-messages-js-fixYields" />',
          missingMainAction: '<s:text name="itk-messages-js-missingMainAction" />',
          invalidUsage: '<s:text name="itk-messages-js-invalidUsage" />',
          unavailableReferenceDose: '<s:text name="itk-messages-js-unavailableReferenceDose" />',
          referenceDosesLoadingFailed: '<s:text name="itk-messages-js-referenceDosesLoadingFailed" />',
          referenceDoseMissingSpecies: '<s:text name="itk-messages-js-referenceDoseMissingSpecies" />',
          treatmentTargetsLoadingFailed: '<s:text name="itk-messages-js-treatmentTargetsLoadingFailed" />',
          graftSupportsLoadingFailed: '<s:text name="itk-messages-js-graftSupportsLoadingFailed" />',
          deletePhaseConfirm: '<s:text name="itk-messages-js-deletePhaseConfirm" />',
          deletePhaseConfirmOne: '<s:text name="itk-messages-js-deletePhaseConfirm-one" />',
          deletePhaseConfirmSeveral: '<s:text name="itk-messages-js-deletePhaseConfirm-several" />',
          speciesLoadingFailed: '<s:text name="itk-messages-js-speciesLoadingFailed" />',
          noCropSelected: '<s:text name="itk-messages-js-noCropSelected" />',
          noCampaignForPracticed: '<s:text name="itk-messages-js-noCampaignForPracticed" />',
          cropChangeRefused: '<s:text name="itk-messages-js-cropChangeRefused" />',
          forPhase: '<s:text name="itk-messages-js-forPhase" />',
          forPhaseIntervention: '<s:text name="itk-messages-js-forPhase-intervention" />',
          forPhaseInterventions: '<s:text name="itk-messages-js-forPhase-interventions" />',
          forIntervention: '<s:text name="itk-messages-js-forIntervention" />',
          forInterventionDefaultValuesForAction: '<s:text name="itk-messages-js-forIntervention-defaultValuesForAction" />',
          forInterventionDefaultValuesForActions: '<s:text name="itk-messages-js-forIntervention-defaultValuesForActions" />',
          removeCampaignConfirmSeveral:  '<s:text name="itk-messages-js-removeCampaignConfirm-title-several" />',
          removeCampaignConfirmOne: '<s:text name="itk-messages-js-removeCampaignConfirm-title-one" />',
          removeCampaignConfirmContentCampaign: '<s:text name="itk-messages-js-removeCampaignConfirm-content-campaign" />',
          removeCampaignConfirmContentElements: '<s:text name="itk-messages-js-removeCampaignConfirm-content-elements" />'
        });

        angular.element(document).ready(
          function draw_diagram() {
            var diagramElem = $("#cropCycleDiagramDiv");
            diagramElem.cropCycleDiagram({
              nodeClickCallback: function(event) {
                $('#tab_1').scope().setSelectedNode(event.data);
              },
              connectionClickCallback: function(diag, connData) {
                $('#tab_1').scope().setSelectedConnection(connData);
              },
              dataModificationCallback: function(methodName, data, nodeOrConn) {
                // some event are fired by angular himself
                // to not fired event back to angular to avoid $apply bug
                if (methodName != 'updateConnection' && methodName != 'removeConnection' && methodName != 'updateNode') {
                  $('#tab_1').scope().setData(data);
                } else if (methodName == 'updateNode') {
                  $('#tab_1').scope().$apply(); // force le refresh
                }
              }
            }
          );

          // tell angular to init diagramElem with current angular data (nodes, connections and models)
          $('#tab_1').scope().initChartJs();

          // force diagram redraw on tab switch
          $('#tabs-seasonal-crop-cycle-li').click(function(){
            $('#cropCycleDiagramDiv').cropCycleDiagram('repaintEverything');
            $('#tab_1').scope().initSeasonalCropCycleTab();
          });

          $('#tabs-itk-li').click(function(){
            $('#tab_3').scope().initItkTab();
          });

        }
      );

  </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("Le système synthétisé sur lequel vous travaillez est inactif et/ou est lié à un/des système(s) de cultures et/ou un/des dispositif(s) et/ou un/des domaine(s) inactifs pour sa/ses campagne(s). Réactivez l(es)' entité(s) inactive(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

    <s:if test="practicedSystem.topiaId == null">
      <title>Nouveau système synthétisé</title>
    </s:if>
    <s:else>
      <title>Système synthétisé '<s:property value="practicedSystem.name" />'</title>
    </s:else>
    <content tag="current-category">practiced</content>
  </head>
  <body>
    <div ng-app="PracticedSystemModule" class="page-content">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url action='practiced-systems-list' namespace='/practiced' />">Systèmes synthétisés</a></li>
          <s:if test="practicedSystem.topiaId == null">
            <li>&gt; Nouveau système synthétisé</li>
          </s:if>
          <s:else>
            <li>&gt; <s:property value="practicedSystem.name" /></li>
          </s:else>
        </ul>
      </div>

      <ul class="actions">
        <li><a class="action-retour" href="<s:url action='practiced-systems-list' namespace='/practiced' />">Retour à la liste des systèmes synthétisés</a></li>
      </ul>

      <ul class="float-right informations">
        <s:if test="%{practicedSystem.topiaId != null}">
          <li>
            <span class="label">Système synthétisé<s:if test="!practicedSystem.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/practiced' action='practiced-systems-edit-input'/>?practicedSystemTopiaId=<s:property value='practicedSystem.topiaId'/>" title="Voir le système synthétisé"><s:property value="practicedSystem.name" /></a>
          </li>
          <li>
            <span class="label">Campagnes agricoles</span>
            <s:property value='practicedSystem.campaigns'/>
          </li>
          <li>
            <span class="label">Système de culture<s:if test="!practicedSystem.growingSystem.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId=<s:property value='practicedSystem.growingSystem.topiaId'/>" title="Voir le système de culture"><s:property value="practicedSystem.growingSystem.name" /></a>
          </li>
          <li>
            <span class="label">Dispositif<s:if test="!practicedSystem.growingSystem.growingPlan.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value='practicedSystem.growingSystem.growingPlan.topiaId'/>" title="Voir le dispositif"><s:property value="practicedSystem.growingSystem.growingPlan.name" /></a>
          </li>
          <li>
            <span class="label">Domaine<s:if test="!practicedSystem.growingSystem.growingPlan.domain.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='practicedSystem.growingSystem.growingPlan.domain.topiaId'/>" title="Voir le domaine"><s:property value="practicedSystem.growingSystem.growingPlan.domain.name" /></a>
          </li>
          <li>
            <span class="label">Validation</span>
            <s:if test="practicedSystem.validationDate == null">Jamais validé</s:if>
            <s:else>Le <javatime:format value="${practicedSystem.validationDate}" pattern="d MMM yyyy" /> à <javatime:format value="${practicedSystem.validationDate}" pattern="HH:mm" /></s:else>
          </li>
          <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='practicedSystem.topiaId' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(practicedSystem.topiaId)" /></a></li>
        </s:if>
      </ul>

      <form name="practicedSystemForm" id="practicedSystemForm" action="<s:url action='practiced-systems-edit' namespace='/practiced'/>"
            method="post" class="tabs clear" ng-controller="PracticedSystemController" ag-confirm-on-exit>
        <s:actionerror escape="false" cssClass="send-toast-to-js"/>
        <s:hidden name="practicedSystemTopiaId" value="%{practicedSystem.topiaId}"/>
        <s:hidden name="growingSystemTopiaId" value="{{growingSystemTopiaId}}"/>

        <ul id="tabs-menu" class="tabs-menu clearfix">
          <li class="selected" ng-click="validateSolOccupationPercent()"><span>Généralités</span></li>
          <li id="tabs-seasonal-crop-cycle-li" ng-click="initSeasonalCropTab()"><span>Cultures assolées<span class="content-italic">Rotation</span></span></li>
          <li ng-click="setPerennialCropCyclesTabOpened()"><span>Cultures pérennes<span class="content-italic">Caractéristiques de la plantation</span></span></li>
          <li id="tabs-itk-li" ng-click="initItkTab()"><span>Itinéraire technique</span></li>
        </ul>

        <div id="tabs-content" class="tabs-content">

          <%-- Généralités --%>
          <div id="tab_0">
            <fieldset>
              <s:if test="practicedSystem.topiaId != null">
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="growing-system"><span class="required">*</span> Système de culture&nbsp;:</label>
                  </span>
                  <span id="growing-system" class="wwctrl generated-content">
                    <s:property value="practicedSystem.growingSystem.name"/> (<s:property value="practicedSystem.growingSystem.growingPlan.domain.campaign"/>)
                  </span>
                </div>
              </s:if>
              <s:else>
                <div class="wwgrp">
                  <s:fielderror fieldName="growingSystemTopiaId" />
                  <span class="wwlbl">
                    <label for="selectedGrowingSystem"><span class="required">*</span> Système de culture&nbsp;:
                      <input ng-show="!growingSystemTopiaId" type="text" name="selectedGrowingSystem" ng-model="growingSystemTopiaId" required style="opacity:0;width: 0;"/>
                    </label>
                  </span>
                  <span id="growingSystemTopiaId" class="wwctrl">
                    <ui-select ng-model="selectedGrowingSystem"
                               id="selectedGrowingSystem"
                               theme="select2"
                               on-select="changeGrowingSystem($item, false)"
                               ng-required="true"
                               spinner-enabled="true">
                      <ui-select-match placeholder="Sélectionner un système de culture">
                          {{$select.selected.name}} ({{$select.selected.growingPlan.domain.campaign}})
                      </ui-select-match>
                      <ui-select-choices repeat="growingSystem in growingSystems track by $index"
                                         refresh="refreshGrowingSystems($select.search)"
                                         refresh-delay="200">
                          <span ng-bind-html="growingSystem.name + ' (' + growingSystem.growingPlan.domain.campaign + ')' | highlight: $select.search"></span>
                      </ui-select-choices>
                    </ui-select>
                  </span>
                </div>
              </s:else>

              <s:textfield name="practicedSystem.name" label="Nom du système synthétisé" ng-model="practicedSystem.name"
                labelPosition="left" labelSeparator=" :" placeholder="ex. : Système synthétisé 1" required="true" requiredLabel="true"
                title="Le nom du système synthétisé est obligatoire"/>
              <s:select name="practicedSystem.source" label="Source" ng-model="practicedSystem.source" list="sources"
                labelPosition="left" labelSeparator=" :" emptyOption="true"/>
              <div id="campaignsErrorMessage" ng-show="!areCampaignsPartOfGrowingSystemCampaigns && practicedSystem.campaigns" class="messages-panel">
                <ul class="warningMessage">
                  <li>
                    La campagne du système de culture auquel vous avez relié votre système synthétisé n'apparait pas dans la série de campagnes agricoles sur laquelle porte votre système synthétisé ({{gsCampaignsValues}}).
                    Si vous venez de retirer cette campagne, Agrosyst l'a automatiquement rajoutée.
                    </span>
                    <span class="close-button" ng-click="displayCampaignsErrorWarning()">Fermer</span>
                  </li>
                </ul>
              </div>

              <div id="wwgrp_practicedSystem_campaigns" class="wwgrp">
                <span id="wwlbl_practicedSystem_campaigns" class="wwlbl">
                  <label for="practicedSystem_campaigns" class="label">
                    <span class="required">*</span>
                    Série de campagnes agricoles&nbsp;:
                  </label>
                </span>
                <s:fielderror fieldName="practicedSystem.campaigns" />
                <span id="wwctrl_practicedSystem_campaigns" class="wwctrl">
                  <span class='contextual-help'>
                    <span class='help-hover'>
                      <s:text name="help.practicedSystem.campaigns" />
                    </span>
                  </span>
                  <input type="text" name="practicedSystem.campaigns" id="practicedSystem_campaigns"
                         placeholder="ex. : 2012, 2013, 2015" ng-model="practicedSystem.campaigns" ag-campaign-series pattern="^([0-9]{4}(?:[\.,;: -_])*[ ]*)+$" required>
                </span>
              </div>

              <s:textarea name="practicedSystem.comment" label="Commentaire" ng-model="practicedSystem.comment" labelPosition="top" labelSeparator=" :" cssClass="textarea-large"/>
            </fieldset>
          </div>

          <%-- Cycle pluriannuel synthétisé de culture(s) assolée(s) --%>
          <%@include file="practiced-systems-edit-input-seasonal.jsp" %>

          <%-- Cycle pluriannuel synthétisé de culture(s) pérenne(s) --%>
          <%@include file="practiced-systems-edit-input-perennial.jsp" %>

          <%-- Itinéraire technique --%>
          <%@include file="practiced-systems-edit-input-itk.jsp" %>

          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url action='practiced-systems-list' namespace='/practiced' />">Annuler</a>
            <input type="submit" id="saveButton" class="btn-primary" value="Enregistrer"
              <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
              <s:if test="!activated">disabled="disabled" title="Le système synthétisé sur lequel vous travaillez est inactif et/ou est lié à un/des système(s) de cultures et/ou un/des dispositif(s) et/ou un/des domaine(s) inactifs pour sa/ses campagne(s). Réactivez l(es)' entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
            />
          </span>
        </div>

        <div id="confirmRemovedElements" class="auto-hide">
          <span ng-bind-html="confirmContent"></span>
        </div>
      </form>

    </div>
  </body>
</html>
