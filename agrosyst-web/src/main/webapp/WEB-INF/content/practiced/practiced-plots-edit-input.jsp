<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    
    <%-- Out of wro because wro can't handle it --%>
    <link rel="stylesheet" href="<s:url value='/webjars/leaflet/1.7.1/dist/leaflet.css' />" />

    <script src="<s:url value='/webjars/leaflet/1.7.1/dist/leaflet.js' />"></script>
    <script src="<s:url value='/js/map/leaflet-providers.js' />"></script>
    <script type="text/javascript" src="<s:url value='/nuiton-js/practicedPlots.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">

      angular.module('PracticedPlotEditModule', ['Agrosyst'])
      .value('PracticedPlot', <s:property value="toJson(practicedPlot)" escapeHtml="false"/>)
      .value('ParcelleZonages', <s:property value="toJson(parcelleZonages)" escapeHtml="false"/>)
      .value('SelectedPlotZoningIds', <s:property value="toJson(selectedPlotZoningIds)" escapeHtml="false"/>)
      .value('SelectedSurfaceTextureId', <s:property value="toJson(selectedSurfaceTextureId)" escapeHtml="false"/>)
      .value('SelectedSubSoilTextureId', <s:property value="toJson(selectedSubSoilTextureId)" escapeHtml="false"/>)
      .value('SelectedSolProfondeurId', <s:property value="toJson(selectedSolProfondeurId)" escapeHtml="false"/>)
      .value('SolTextures', <s:property value="toJson(solTextures)" escapeHtml="false"/>)
      .value('SolProfondeurs', <s:property value="toJson(solProfondeurs)" escapeHtml="false"/>)
      .value('SolCaracteristiques', <s:property value="toJson(solCaracteristiques)" escapeHtml="false"/>)
      .value('SolHorizons', <s:property value="toJson(solHorizons)" escapeHtml="false"/>)
      .value('AdjacentElements', <s:property value="toJson(adjacentElements)" escapeHtml="false"/>)
      .value('AdjacentElementIds', <s:property value="toJson(adjacentElementIds)" escapeHtml="false"/>)
      .value('campaignsBounds', <s:property value="toJson(campaignsBounds)" escapeHtml="false" />);
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("La parcelle type sur laquelle vous travaillez est inactive et/ou est liée à un système synthétisé inactif. Réactivez les pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

    <s:if test="practicedPlot.topiaId == null">
      <title>Nouvelle parcelle type</title>
    </s:if>
    <s:else>
      <title>Parcelle type '<s:property value="practicedPlot.name" />'</title>
    </s:else>
    <content tag="current-category">practiced</content>
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url action='practiced-plots-list' namespace='/practiced' />">Parcelles types</a></li>
        <s:if test="practicedPlot.topiaId != null">
          <li>&gt; <s:property value="practicedPlot.name"/></li>
        </s:if>
        <s:else>
          <li>&gt; Nouvelle parcelle type</li>
        </s:else>
      </ul>
    </div>

    <ul class="actions">
      <li><a class="action-retour" href="<s:url action='practiced-plots-list' namespace='/practiced' />">Retour à la liste des parcelles types</a></li>
    </ul>

    <s:if test="%{practicedPlot.topiaId != null}">
      <ul class="float-right informations">
        <li>
          <span class="label">Parcelle type</span>
          <a href="<s:url namespace='/practiced' action='practiced-plots-edit-input'/>?practicedPlotTopiaId=<s:property value='practicedPlot.topiaId'/>">
            <s:property value="practicedPlot.name" />
          </a>
        </li>
        <li>
          <span class="label">Système synthétisé<s:if test="!activated">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/practiced' action='practiced-systems-edit-input'/>?practicedSystemTopiaId=<s:property value='practicedSystem.topiaId'/>"><s:property value="practicedSystem.name" /></a>
        </li>
        <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='practicedPlot.topiaId' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(practicedPlot.topiaId)" /></a></li>
      </ul>
    </s:if>

    <div ng-app="PracticedPlotEditModule" class="page-content">
      <form name="practicedPlotEditForm" action="<s:url action='practiced-plots-edit' namespace='/practiced' />" method="post" class="tabs clear" ng-app="PracticedPlotEditModule" ng-controller="PracticedPlotEditController" ag-confirm-on-exit>
        <s:actionerror cssClass="send-toast-to-js"/>
        <s:hidden name="practicedPlotTopiaId" value="%{practicedPlot.topiaId}" />

        <ul id="tabs-practiced-plots-menu"  class="tabs-menu clearfix">
           <li class="selected"><span>Généralités</span></li><!--
           --><li><span>Zonage</span></li><!--
           --><li><span>Équipements</span></li><!--
           --><li><span>Sols</span></li><!--
           --><li><span>Éléments de voisinage</span></li>
        </ul>

        <div id="tabs-practiced-plots-content" class="tabs-content">
          <div id="tab_0">
            <fieldset>

              <s:if test="%{practicedPlot.topiaId != null}">
                <div class="wwgrp">
                  <span class="wwlbl"><label>Système synthétisé&nbsp;:</label></span>
                  <span class="wwctrl generated-content">
                    <span class='contextual-help'>
                      <span class='help-hover'>
                        <s:text name="help.practicedPlot.practicedSystem" />
                      </span>
                    </span>
                    <s:property value="practicedSystem.name" />
                  </span>
                </div>
              </s:if>
              <s:else>
                <div id="wwgrp_practicedSystemTopiaId" class="wwgrp">
                  <span id="wwlbl_practicedSystemTopiaId" class="wwlbl">
                    <label for="practicedSystemTopiaId" class="label">
                      <span class="required">*</span>
                      Système synthétisé&nbsp;:
                    </label>
                  </span>
                  <span id="wwctrl_practicedSystemTopiaId" class="wwctrl">
                    <span class='contextual-help'>
                      <span class='help-hover'>
                        <s:text name="help.practicedPlot.practicedSystem" />
                      </span>
                    </span>
                    <select name="practicedSystemTopiaId" id="practicedSystemTopiaId" required>
                      <option value=""></option>
                      <s:iterator value="practicedSystems">
                        <option value="${topiaId}" <s:if test="practicedSystemTopiaId == topiaId" > selected="selected" </s:if>>${name}</option>
                      </s:iterator>
                    </select>
                  </span>
                </div>
              </s:else>

              <s:textfield label="Nom" name="practicedPlot.name" ng-model="practicedPlot.name" placeholder="ex. : Parcelle tf620"
                labelPosition="left" labelSeparator=" :" requiredLabel="true" required="true"/>

              <div class="wwgrp">
                <s:fielderror fieldName="locationTopiaId" />
                <span class="wwlbl">
                    <label for="commune"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Commune&nbsp;:</label>
                </span>
                <span class="wwctrl">
                  <s:if test="%{practicedPlot.location != null}">
                    <input type="text" id="commune" placeholder="ex. : 37260, Artannes-sur-Indre" value="${practicedPlot.location.codePostal}, ${practicedPlot.location.commune}" />
                    <input type="hidden" id="locationTopiaId" name="locationTopiaId"  value="${practicedPlot.location.topiaId}" />
                  </s:if>
                  <s:else>
                    <input type="text" id="commune" placeholder="ex. : 37260, Artannes-sur-Indre" />
                    <input type="hidden" id="locationTopiaId" name="locationTopiaId" />
                  </s:else>
                </span>
              </div>

              <div class="wwgrp">
                <s:fielderror fieldName="practicedPlot.area" />
                <span class="wwlbl"><label for="plotAreaField"><span class="required">*</span> Surface moyenne&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="plotAreaField" type="text" name="practicedPlot.area" ng-model="practicedPlot.area" ag-float-positive pattern="^\d+([\.,]\d+)?$" required/>
                    <span class="add-on">ha</span>
                  </span>
                </span>
              </div>
              <s:textfield label="Numéro d'ilôt PAC" name="practicedPlot.pacIlotNumber" ng-model="practicedPlot.pacIlotNumber" pattern="\d*" labelPosition="left" labelSeparator=" :" />
              <div class="wwgrp">
                <s:fielderror fieldName="practicedPlot.maxSlope" />
                <span class="wwlbl"><label for="plotMaxSlopeField">Pente maxi&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="plotMaxSlopeField" name="practicedPlot.maxSlope" ng-model="practicedPlot.maxSlope">
                    <option value=""></option>
                    <s:iterator value="maxSlopes">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>

              <div class="wwgrp">
                <s:fielderror fieldName="practicedPlot.waterFlowDistance" />
                <span class="wwlbl"><label for="plotWaterFlowDistanceField">Distance à un cours d'eau&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="plotWaterFlowDistanceField" name="practicedPlot.waterFlowDistance" ng-model="practicedPlot.waterFlowDistance">
                    <option value=""></option>
                    <s:iterator value="waterFlowDistances">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>
              <div class="wwgrp">
                <s:fielderror fieldName="practicedPlot.bufferStrip" />
                <span class="wwlbl"><label for="plotBufferStripField">Bande enherbée&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="plotBufferStripField" name="practicedPlot.bufferStrip" ng-model="practicedPlot.bufferStrip">
                    <option value=""></option>
                    <s:iterator value="bufferStrips">
                      <option value="${key}">${value}</option>
                    </s:iterator>
                  </select>
                </span>
              </div>
              <s:textfield label="Latitude GPS du centre de la parcelle" name="practicedPlot.latitude" ng-model="practicedPlot.latitude" placeholder="ex. : 47.21050" labelPosition="left" labelSeparator=" :" ag-location="wtfwstruts" pattern="^[+-]?[0-9]+([\.,][0-9]+)?" />
              <s:textfield label="Longitude GPS du centre de la parcelle" name="practicedPlot.longitude" ng-model="practicedPlot.longitude" placeholder="ex. : -1.48646" labelPosition="left" labelSeparator=" :" ag-location="wtfwstruts" pattern="^[+-]?[0-9]+([\.,][0-9]+)?" />

              <div class="wwgrp">
                <span class="wwctrl">
                  <a class="btn icon-locate" title="Afficher sur une carte" onclick="displayLocation('<s:property value="practicedPlot.name" />', $('#practicedPlot_latitude').val(), $('#practicedPlot_longitude').val(), ''); return false;" >Voir la position sur la carte</a>
                  </span>
              </div>

              <s:textarea label="Commentaire sur la parcelle" name="practicedPlot.comment" ng-model="practicedPlot.comment" labelPosition="top" labelSeparator=" :" />
              <s:textarea label="Motif de fin d'utilisation" name="practicedPlot.activityEndComment" ng-model="practicedPlot.activityEndComment" labelPosition="top" labelSeparator=" :" />
            </fieldset>
          </div>

          <!-- Zonage -->
          <div id="tab_1" ng-controller="PracticedPlotZoningController">
            <fieldset>
              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="outOfZoning">Déclarer la parcelle hors de tout zonage&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.outOfZoning" ng-value="true" ng-model="practicedPlot.outOfZoning" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.outOfZoning" ng-value="false" ng-model="practicedPlot.outOfZoning" />Non
                  </label>
                </span>
              </div>

              <div ng-show="!practicedPlot.outOfZoning" class="slide-animation">
                <div class="wwgrp">
                  <div class="wwlbl">
                    <label class="label">Zonage&nbsp;:</label>
                  </div>
                  <div class="wwctrl checkbox-list">
                    <span ng-repeat="parcelleZonage in parcelleZonages">
                      <input type="checkbox" id="selectedPlotZoningIds-{{$index}}" name="selectedPlotZoningIds"
                      ng-model="selectedPlotZoningIds[parcelleZonage.topiaId]" value="{{parcelleZonage.topiaId}}" class="checkbox-list" />
                      <label for="selectedPlotZoningIds-{{$index}}" class="checkboxLabelAgrosyst">{{parcelleZonage.libelle_engagement_parcelle}}</label>
                    </span>
                    <input type="hidden" id="__multiselect_selectedPlotZoningIds" name="__multiselect_selectedPlotZoningIds" value="" />
                  </div>
                </div>
              </div>

              <s:textarea label="Commentaire" name="practicedPlot.zoningComment" ng-model="practicedPlot.zoningComment" labelSeparator=" :" />
            </fieldset>
          </div>

          <!-- Equipements -->
          <div id="tab_2">
            <fieldset>
              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="irrigationSystem">Système d'irrigation&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.irrigationSystem" ng-value="true" ng-model="practicedPlot.irrigationSystem" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.irrigationSystem" ng-value="false" ng-model="practicedPlot.irrigationSystem" />Non
                  </label>
                </span>
              </div>

              <div ng-show="practicedPlot.irrigationSystem" class="slide-animation">
                <s:select label="Type de système d'irrigation" name="practicedPlot.irrigationSystemType" ng-model="practicedPlot.irrigationSystemType" list="irrigationSystemTypes"
                          labelPosition="left" labelSeparator=" :" emptyOption="true"/>
                <s:select label="Type de moteur de pompe" name="practicedPlot.pompEngineType" ng-model="practicedPlot.pompEngineType" list="pompEngineTypes"
                          labelPosition="left" labelSeparator=" :" emptyOption="true"/>
                <s:select label="Positionnement des tuyaux d'arrosage" name="practicedPlot.hosesPositionning" ng-model="practicedPlot.hosesPositionning" list="hosesPositionnings"
                          labelPosition="left" labelSeparator=" :" emptyOption="true"/>

                <div class="wwgrp radio-list">
                  <span class="wwlbl"><label for="fertigationSystem">Système de fertirrigation&nbsp;:</label></span>
                  <span class="wwctrl">
                    <label class="radioLabel">
                      <input type="radio" name="practicedPlot.fertigationSystem" ng-value="true" ng-model="practicedPlot.fertigationSystem" />Oui
                    </label>
                    <label class="radioLabel">
                      <input type="radio" name="practicedPlot.fertigationSystem" ng-value="false" ng-model="practicedPlot.fertigationSystem" />Non
                    </label>
                  </span>
                </div>
                 <s:textfield label="Origine de l'eau" name="practicedPlot.waterOrigin" ng-model="practicedPlot.waterOrigin"
                   placeholder="ex. : Source" labelPosition="left" labelSeparator=" :" />
              </div>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="drainage"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Drainage&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.drainage" ng-value="true" ng-model="practicedPlot.drainage" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.drainage" ng-value="false" ng-model="practicedPlot.drainage" />Non
                  </label>
                </span>
              </div>
              <div ng-show="practicedPlot.drainage" class="slide-animation">
                <s:textfield label="Année de réalisation du drainage" name="practicedPlot.drainageYear" ng-model="practicedPlot.drainageYear" ag-campaign="wtfwstruts" pattern="(?:19|20)[0-9]{2}"
                             placeholder="ex. : 2013" labelPosition="left" labelSeparator=" :" />
              </div>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="frostProtection">Protection anti-gel&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.frostProtection" ng-value="true" ng-model="practicedPlot.frostProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.frostProtection" ng-value="false" ng-model="practicedPlot.frostProtection" />Non
                  </label>
                </span>
              </div>
              <div ng-show="practicedPlot.frostProtection" class="slide-animation">
                <s:select label="Type de protection anti-gel" name="practicedPlot.frostProtectionType" list="frostProtectionTypes"
                          labelPosition="left" labelSeparator=" :" emptyOption="true"/>
              </div>


              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="hailProtection">Protection anti-grêle&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.hailProtection" ng-value="true" ng-model="practicedPlot.hailProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.hailProtection" ng-value="false" ng-model="practicedPlot.hailProtection" />Non
                  </label>
                </span>
              </div>
              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="rainproofProtection">Protection anti-pluie&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.rainproofProtection" ng-value="true" ng-model="practicedPlot.rainproofProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.rainproofProtection" ng-value="false" ng-model="practicedPlot.rainproofProtection" />Non
                  </label>
                </span>
              </div>
              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="pestProtection">Protection anti-insectes&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.pestProtection" ng-value="true" ng-model="practicedPlot.pestProtection" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.pestProtection" ng-value="false" ng-model="practicedPlot.pestProtection" />Non
                  </label>
                </span>
              </div>

              <s:textarea label="Autre équipement" ng-model="practicedPlot.otherEquipment" name="practicedPlot.otherEquipment" labelPosition="top" labelSeparator=" :" />
              <s:textarea label="Commentaire sur l’équipement de la parcelle" ng-model="practicedPlot.equipmentComment" name="practicedPlot.equipmentComment" labelPosition="top" labelSeparator=" :" />
            </fieldset>
          </div>

          <!-- Sols -->
          <div id="tab_3" ng-controller="PracticedPlotSolController">
            <fieldset>
              <input type="hidden" name="solHorizons" value="{{solHorizons}}" />

              <div class="wwgrp">
                <span class="wwlbl"><label for="surfaceTextureField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Texture moyenne de la surface&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="surfaceTextureField" name="selectedSurfaceTextureId" ng-model="selectedSurfaceTextureId">
                    <option value=""></option>
                    <s:iterator value="solTextures" var="solTexture">
                      <option value="<s:property value="topiaId" />"><s:property value="classes_texturales_GEPAA" /></option>
                    </s:iterator>
                  </select>
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label for="subSoilTextureField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Texture moyenne du sous-sol&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="subSoilTextureField" name="selectedSubSoilTextureId" ng-model="selectedSubSoilTextureId">
                    <option value=""></option>
                    <s:iterator value="solTextures" var="solTexture">
                      <option value="<s:property value="topiaId" />"><s:property value="classes_texturales_GEPAA" /></option>
                    </s:iterator>
                  </select>
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label for="solStoninessField">Pierrosité moyenne&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="solStoninessField" type="text" placeholder="ex. : 5" name="practicedPlot.solStoniness" ng-model="practicedPlot.solStoniness" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" />
                    <span class="add-on">%</span>
                  </span>
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label for="solDepthField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Classe profondeur maxi enracinement&nbsp;:</label></span>
                <span class="wwctrl">
                  <select id="solDepthField" name="selectedSolProfondeurId" ng-model="selectedSolProfondeurId">
                    <option value=""></option>
                    <s:iterator value="solProfondeurs" var="solProfondeur">
                      <option value="<s:property value="topiaId" />"<s:if test="#solProfondeur.topiaId == selectedSolProfondeurId"> selected</s:if>><s:property value="libelle_classe" /></option>
                    </s:iterator>
                  </select>
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label for="solMaxDepthField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Profondeur maxi enracinement&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="solMaxDepthField" type="text" placeholder="ex. : 60" name="practicedPlot.solMaxDepth" ng-model="practicedPlot.solMaxDepth" ag-integer pattern="\d+" />
                    <span class="add-on">cm</span>
                  </span>
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label>Réserve utile&nbsp;:</label></span>
                <span class="wwctrl generated-content">
                  {{computeUsefullReserve()}} mm
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label for="solOrganicMaterialPercentField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Pourcentage Matière Organique&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="input-append">
                    <input id="solOrganicMaterialPercentField" type="text" placeholder="ex. : 5" name="practicedPlot.solOrganicMaterialPercent" ng-model="practicedPlot.solOrganicMaterialPercent" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" />
                    <span class="add-on">%</span>
                  </span>
                </span>
              </div>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="solBattanceField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Battance&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.solBattance" ng-value="true" ng-model="practicedPlot.solBattance" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.solBattance" ng-value="false" ng-model="practicedPlot.solBattance" />Non
                  </label>
                </span>
              </div>

              <s:select label="PH eau" name="practicedPlot.solWaterPh" list="solWaterPhs"
                        labelPosition="left" labelSeparator=" :" emptyOption="true"/>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="solHydromorphismsField"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : I-Phy">&pi;</span> Hydromorphie&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.solHydromorphisms" ng-value="true" ng-model="practicedPlot.solHydromorphisms" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.solHydromorphisms" ng-value="false" ng-model="practicedPlot.solHydromorphisms" />Non
                  </label>
                </span>
              </div>

              <div class="wwgrp radio-list">
                <span class="wwlbl"><label for="solLimestoneField">Calcaire&nbsp;:</label></span>
                <span class="wwctrl">
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.solLimestone" ng-value="true" ng-model="practicedPlot.solLimestone" />Oui
                  </label>
                  <label class="radioLabel">
                    <input type="radio" name="practicedPlot.solLimestone" ng-value="false" ng-model="practicedPlot.solLimestone" />Non
                  </label>
                </span>
              </div>

              <div ng-show="practicedPlot.solLimestone" class="slide-animation">
                <div class="wwgrp">
                  <span class="wwlbl"><label for="solActiveLimestoneField">Proportion calcaire actif&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="solActiveLimestoneField" type="text" name="practicedPlot.solActiveLimestone" ng-model="practicedPlot.solActiveLimestone"
                      ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" placeholder="ex. : 5"/>
                      <span class="add-on">%</span>
                    </span>
                  </span>
                </div>

                <div class="wwgrp">
                  <span class="wwlbl"><label for="solTotalLimestoneField">Proportion calcaire total&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="solTotalLimestoneField" type="text" name="practicedPlot.solTotalLimestone" ng-model="practicedPlot.solTotalLimestone"
                      ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" placeholder="ex. : 5" />
                      <span class="add-on">%</span>
                    </span>
                  </span>
                </div>
              </div>

              <s:textarea label="Commentaires" name="practicedPlot.solComment" ng-model="practicedPlot.solComment" labelPosition="top" labelSeparator=" :" />

              <div class="table-enclosure">
                <label>Description des horizons du sol&nbsp;:</label>

                <table class="data-table">
                  <thead>
                    <tr>
                      <th scope="col">Numéro de l'horizon</th>
                      <th scope="col">Cote basse (cm)</th>
                      <th scope="col">Épaisseur de l'horizon (cm)</th>
                      <th scope="col">Pierrosité horizon (%)</th>
                      <th scope="col">Texture horizon</th>
                      <th scope="col" class="column-xsmall">Suppr.</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-show="solHorizons.length == 0" >
                      <td colspan="6" class="empty-table">
                        Il n'y a pas encore d'horizons pour le sol de cette parcelle. Pour en ajouter, cliquez sur le bouton "Ajouter" ci-dessous
                      </td>
                    </tr>
                    <tr ng-repeat="solHorizon in solHorizons" ng-click="editSolHorizon(solHorizon, $index)"
                        ng-class="{'selected-line' : selectedSolHorizon == solHorizon}" class="selectCursor">
                      <td>
                        {{$index + 1}}
                      </td>
                      <td>
                        {{solHorizon.lowRating}}
                      </td>
                      <td>
                        {{computeSolHorizonThickness(solHorizon, $index)}}
                      </td>
                      <td>
                        {{solHorizon.stoniness}}
                      </td>
                      <td>
                        {{solTextureLabel(solHorizon)}}
                      </td>
                      <td>
                        <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteSolHorizon($index)"/>
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="6">
                        <div class="table-end-button">
                          <input type="button" value="Ajouter" ng-click="addSolHorizon()"/>
                        </div>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>

              <div ng-show="selectedSolHorizon" class="slide-animation sub-form" ng-form="editSolHorizonForm">
                <div class="sub-form-content">
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" >Numéro de l'horizon&nbsp;:</label></span>
                    <span class="wwctrl generated-content">
                      {{selectedSolHorizonIndex + 1}}
                    </span>
                  </div>
                  <div class="wwgrp">
                    <div class="messages-panel"
                         ng-show="editSolHorizonForm.selectedSolHorizonLowRating.$dirty && editSolHorizonForm.selectedSolHorizonLowRating.$error.validator">
                      <ul class="errorMessage">
                        <li>La cote basse doit être comprise entre celle de l'horizon précédent et celle de l'horizon suivant !</li>
                      </ul>
                    </div>
                    <span class="wwlbl"><label class="historicLabelStyle" for="lowRatingField">Cote basse&nbsp;:</label></span>
                    <span class="wwctrl">
                      <span class="input-append">
                        <input type="text" id="lowRatingField" ng-model="selectedSolHorizon.lowRating" name="selectedSolHorizonLowRating"
                               ag-float pattern="^[\-\+]?\d*[\.,]?\d*$" placeholder="ex. : 25"
                               ui-validate="'validateHorizonLowRating($value)'"/>
                        <span class="add-on">cm</span>
                      </span>
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="thicknessField">Épaisseur de l'horizon&nbsp;:</label></span>
                    <span class="wwctrl generated-content">
                      {{computeSolHorizonThickness(selectedSolHorizon, selectedSolHorizonIndex)}} cm
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="stoninessField">Pierrosité horizon&nbsp;:</label></span>
                    <span class="wwctrl">
                      <span class="input-append">
                        <input type="text" id="stoninessField" ng-model="selectedSolHorizon.stoniness"
                               ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" placeholder="ex. : 5"/>
                        <span class="add-on">%</span>
                      </span>
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="solHorizonTextureField">Texture horizon&nbsp;:</label></span>
                    <span class="wwctrl">
                      <select id="solHorizonTextureField" ng-model="selectedSolHorizon.solTextureId">
                        <option value=""></option>
                        <s:iterator value="solTextures" var="solTexture">
                          <option value="<s:property value="topiaId" />"><s:property value="classes_texturales_GEPAA" /></option>
                        </s:iterator>
                      </select>
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl"><label class="historicLabelStyle" for="horizonCommentField">Commentaire&nbsp;:</label></span>
                    <span class="wwctrl">
                      <textarea id="horizonCommentField" ng-model="selectedSolHorizon.comment"></textarea>
                    </span>
                  </div>
                </div>
              </div>
            </fieldset>
          </div>

          <!-- Éléments de voisinage -->
          <div id="tab_4" ng-controller="PracticedPlotAdjacentElementsController">
            <fieldset>
              <div class="wwgrp">
                <div class="wwlbl">
                  <label class="label">Éléments de voisinage&nbsp;:</label>
                </div>
                <div class="wwctrl checkbox-list">
                  <span ng-repeat="adjacentElement in adjacentElements">
                    <input type="checkbox" id="adjacentElementIds-{{$index}}" name="adjacentElementIds"
                           ng-model="adjacentElementIds[adjacentElement.topiaId]" value="{{adjacentElement.topiaId}}" class="checkbox-list" />
                    <label for="adjacentElementIds-{{$index}}" class="checkboxLabelAgrosyst">{{adjacentElement.iae_nom_Translated}}</label>
                  </span>
                  <input type="hidden" id="__multiselect_adjacentElementIds" name="__multiselect_adjacentElementIds" value="" />
                </div>
              </div>
              <div class="wwgrp">
                <div class="wwlbl">
                  <label class="label" for="zoneAdjacentCommentField">Commentaire&nbsp;:</label>
                </div>
                <div class="wwctrl">
                  <textarea id="zoneAdjacentCommentField" ng-model="practicedPlot.adjacentComment" name="practicedPlot.adjacentComment"></textarea>
                </div>
              </div>
            </fieldset>
          </div>

          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url action='practiced-plots-list' namespace='/practiced' />">Annuler</a>
            <input type="submit" class="btn-primary" value="Enregistrer"
              <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
              <s:if test="!activated">disabled="disabled" title="La parcelle type sur laquelle vous travaillez est inactive et/ou est liée à un système synthétisé inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
            />
          </span>
        </div>
      </form>
    
    <div id="show-location" title="Affichage d'une position GPS" class="auto-hide">
      Chargement...
    </div>
  </body>
</html>
