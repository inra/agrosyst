<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Actes synthétisés</title>
    <content tag="current-category">practiced</content>
  </head>
  <body>
    
    <div class="presentation-page">
      <h1>Actes synthétisés</h1>
      
      <div>
      
        Au sein de ce menu, vous pouvez :
        <ul>
          <li>déclarer des actes synthétisés ;</li>
          <li>décrire la parcelle type d’un système de culture.</li>
        </ul>
        
        Les <strong>actes synthétisés</strong> d’un système de culture forment également ce que l’on appelle « le pratiqué », ils correspondent à une synthèse des actes réalisés à deux échelles :
        <ul>
          <li>l’échelle spatiale de la sole du système de culture ; </li>
          <li>l’échelle temporelle d’une ou plusieurs campagnes agricoles.</li>
        </ul>
        
        La description d’actes synthétisés est particulièrement utile pour les domaines dans lesquels les cultures sont gérées de façon semblable sur différentes parcelles.<br/><br/>
        
        On peut distinguer différents <strong>systèmes synthétisés</strong> en fonction de la façon dont on synthétise les actes réalisés. Par exemple, on peut avoir un système synthétisé à l’échelle d’une campagne agricole, « synthétisé annuel », ou à l’échelle de plusieurs campagnes agricoles, « synthétisé pluriannuel ». <br/><br/>
        
        Pour permettre le calcul de performances d’un système synthétisé, il est nécessaire de définir une <strong>parcelle type</strong>, représentative de la sole du système de culture.
      
      </div>
    </div>

  </body>
</html>
