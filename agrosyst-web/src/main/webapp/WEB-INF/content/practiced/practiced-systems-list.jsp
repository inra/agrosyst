<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2024 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
  <title>Systèmes synthétisés</title>
  <content tag="current-category">practiced</content>
  <script type="text/javascript" src="<s:url value='/nuiton-js/practicedSystems-list.js' /><s:property value='getVersionSuffix()'/>"></script>
  <script type="text/javascript">
    angular.module('PracticedSystemsList', ['Agrosyst', 'ngSanitize']).
    value('PracticedSystemsInitData', <s:property value="toJson(practicedSystems)" escapeHtml="false"/>).
    value('practicedSystemFilter', <s:property value="toJson(practicedSystemFilter)" escapeHtml="false"/>).
    value('practicedSystemsExportAsyncThreshold', <s:property value="practicedSystemsExportAsyncThreshold"/>);
  </script>
</head>
<body>
  <div ng-app="PracticedSystemsList" ng-controller="PracticedSystemsListController">
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Systèmes synthétisés</li>
      </ul>
    </div>

    <ul class="actions">
      <li>
        <a class="action-ajouter"
           href="<s:url namespace='/practiced' action='practiced-systems-edit-input' />">Créer un système synthétisé</a>
      </li>
    </ul>

    <ul class="float-right actions">

      <li>
        <a class="action-desactiver"
           ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
           onclick="practicedSystemUnactivate($('#confirmUnactivatePracticedSystems'), $('#practicedSystemListForm'), false);return false;"
           ng-if="allSelectedPracticedSystemActive">Désactiver</a>
        <a class="action-activer"
           ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
           onclick="practicedSystemUnactivate($('#confirmUnactivatePracticedSystems'), $('#practicedSystemListForm'), true);return false;"
           ng-if="!allSelectedPracticedSystemActive">Activer</a></li>
      <li>
        <a id="validate-practiced-system-button"
           class="action-unValidate" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
           onclick="validatePracticedSystems($('#confirmValidatePracticedSystems'), $('#practicedSystemListForm'), false);return false;"
           ng-if="allSelectedPracticedSystemsValidable">Dévalider</a>
        <a id="validate-practiced-system-button"
           class="action-validate" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
           onclick="validatePracticedSystems($('#confirmValidatePracticedSystems'), $('#practicedSystemListForm'), true);return false;"
           ng-if="!allSelectedPracticedSystemsValidable">Valider</a>
      </li>
      <li>
        <a ng-if="(selectedEntities|toSelectedLength) != 1"
           class="action-dupliquer"
           ng-class="{'button-disabled': true}">Dupliquer</a>
        <a ng-if="(selectedEntities|toSelectedLength) == 1"
           class="action-dupliquer"
           ng-class="{'button-disabled': false}"
           ng-click="practicedSystemDuplicate()">Dupliquer</a>
      </li>
      <%-- <li><a class="action-import button-disabled">Import</a></li>--%>
      <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
             onclick="practicedSystemsExport($('#practicedSystemListForm'));return false;">Export XLS</a></li>
      <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
             ng-click="asyncPracticedSystemsExport()">Export XLS</a></li>
    </ul>
    <form method="post" id="practicedSystemListForm">
      <input type="hidden" name="practicedSystemIds" value="{{selectedEntities|toSelectedArray}}" />
      <input type="hidden" name="practicedSystemId" value="{{practicedSystemIdToDuplicate}}"/>
      <input type="hidden" name="growingSystemId" value="{{practicedSystemDuplicateObject.growingSystemIdForDuplication}}"/>

      <table class="entity-list clear fixe_layout">
        <thead>
          <tr class="doubleHeight">
            <th scope="col" class="column-tiny"
                title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
            </th>
            <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('PRACTICED_SYSTEM')">
              <sort>
                <header_label>Système synthétisé</header_label>
                <em ng-if="filter.sortedColumn == 'PRACTICED_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.PRACTICED_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.PRACTICED_SYSTEM}" ></em>
                <em ng-if="filter.sortedColumn != 'PRACTICED_SYSTEM'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('DOMAIN')">
              <sort>
                <header_label>Exploitation ou Domaine expérimental</header_label>
                <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_PLAN')">
              <sort>
                <header_label>Dispositif</header_label>
                <em ng-if="filter.sortedColumn == 'GROWING_PLAN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_PLAN, 'fa fa-sort-amount-desc ':sortColumn.GROWING_PLAN}" ></em>
                <em ng-if="filter.sortedColumn != 'GROWING_PLAN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_SYSTEM')">
              <sort>
                <header_label>Système de culture</header_label>
                <em ng-if="filter.sortedColumn == 'GROWING_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.GROWING_SYSTEM}" ></em>
                <em ng-if="filter.sortedColumn != 'GROWING_SYSTEM'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-small" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne(s)">
              <sort>
                <header_label>Camp.(s)</header_label>
                <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-small">Validation</th>
            <th scope="col" class="column-xsmall">État</th>
          </tr>
          <tr>
            <td></td>
            <td><input type="text" ng-model="filter.practicedSystemName" /></td>
            <td><input type="text" ng-model="filter.domainName" /></td>
            <td><input type="text" ng-model="filter.growingPlanName" /></td>
            <td><input type="text" ng-model="filter.growingSystemName" /></td>
            <td><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.practicedSystemCampaign" /></td>
            <td>
              <select ng-model="filter.validated">
                <option value="" />
                <option value="true">Validé</option>
                <option value="false">Non validé</option>
              </select>
            </td>
            <td>
              <select ng-model="filter.active">
                <option value="" />
                <option value="true">Actif</option>
                <option value="false">Inactif</option>
              </select>
            </td>
          </tr>
        </thead>
        <tbody>
          <tr ng-show="practicedSystems.length == 0"><td colspan="8" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>

          <tr ng-repeat="practicedSystem in practicedSystems"
              ng-class="{'line-selected':selectedEntities[practicedSystem.topiaId]}">
            <td>
              <input type='checkbox' ng-model="selectedEntities[practicedSystem.topiaId]" ng-checked="selectedEntities[practicedSystem.topiaId]" ng-click="toggleSelectedEntity(practicedSystem.topiaId)"/>
            </td>
            <td class="column-xxslarge-fixed" title="{{practicedSystem.name}}">
              <a href="<s:url namespace='/practiced' action='practiced-systems-edit-input'/>?practicedSystemTopiaId={{practicedSystem.topiaId|encodeURIComponent}}">{{practicedSystem.name}}<span ng-if="!practicedSystem.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xxslarge-fixed" title="{{practicedSystem.growingSystem.growingPlan.domain.name}}">
              <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId={{practicedSystem.growingSystem.growingPlan.domain.topiaId|encodeURIComponent}}">{{practicedSystem.growingSystem.growingPlan.domain.name}}<span ng-if="!practicedSystem.growingSystem.growingPlan.domain.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xlarge-fixed" title="{{practicedSystem.growingSystem.growingPlan.name}}">
              <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId={{practicedSystem.growingSystem.growingPlan.topiaId|encodeURIComponent}}">{{practicedSystem.growingSystem.growingPlan.name}}<span ng-if="!practicedSystem.growingSystem.growingPlan.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xlarge-fixed" title="{{practicedSystem.growingSystem.name}}">
              <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId={{practicedSystem.growingSystem.topiaId|encodeURIComponent}}">{{practicedSystem.growingSystem.name}}<span ng-if="!practicedSystem.growingSystem.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-small">{{practicedSystem.campaigns}}</td>
            <td>{{practicedSystem.validated ? 'Validé' : 'Non validé'}}</td>
            <td>{{practicedSystem.active ? 'Actif' : 'Inactif'}}</td>
          </tr>

        </tbody>
        <tfoot>
          <tr>
            <td colspan="8">
              <div class="table-footer">
                <div class="entity-list-info">
                  <span class="counter">{{pager.count}} systèmes synthétisés</span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les systèmes synthétisés</a>
                  </span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                  </span>
                  <span class="selection">
                    - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                  </span>
                </div>
                <div class="pagination">
                  <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                </div>
              </div>
            </td>
          </tr>
        </tfoot>
      </table>
    </form>

    <div id="failedDuplicatePracticedSystem" title="Échec de duplication du système synthétisé" class="auto-hide">
      <div>
        <span ng-bind-html="duplicationErrorMessageContainer.duplicationErrorMessage"/>
      </div>
    </div>

    <div id="confirmDuplicatePracticedSystem" title="Dupliquer le système synthétisé" class="auto-hide">
      <div ng-if="availableGrowingSystemsForDuplication.length === 0">Aucun sytème de culture disponible pour la duplication</div>
      <div ng-if="availableGrowingSystemsForDuplication.length > 0">
        <div class="wwgrp">
          <span class="wwlbl">
            <label for="growingSystemIdForDuplication"> Dupliquer le système synthétisé vers le système de culture :</label>
          </span>
          <span class="wwctrl">
            <select id="growingSystemIdForDuplication"
                    ng-model="practicedSystemDuplicateObject.growingSystemIdForDuplication"
                    ng-options="gs.topiaId as gs.name + ' (' + gs.growingPlan.domain.campaign + ')' for gs in availableGrowingSystemsForDuplication">
            </select>
          </span>
        </div>
      </div>
    </div>

    <div id="confirmUnactivatePracticedSystems" title="Activation/Désactivation des systèmes synthétisés" class="auto-hide">
      Êtes-vous sûr(e) de vouloir {{allSelectedPracticedSystemActive?'désactiver':'activer'}}
      <ng-pluralize count="(selectedEntities|toSelectedLength)"
                    when="{'one': 'le système synthétisé {{firstSelectedPracticedSystem.name}}',
                    'other': 'les {} systèmes synthétisés sélectionnés'}"></ng-pluralize> ?
    </div>

    <div id="confirmValidatePracticedSystems" title="Validation du système synthétisé" class="auto-hide">
      Êtes-vous sûr(e) de vouloir {{allSelectedPracticedSystemsValidable ?'dévalider':'valider'}}
      <ng-pluralize count="(selectedEntities|toSelectedLength)"
                    when="{'one': 'le système synthétisé {{firstSelectedPracticedSystem.name}}',
                    'other': 'les {} systèmes synthétisés sélectionnés'}"></ng-pluralize> ?
    </div>

  </div>
</body>
</html>
