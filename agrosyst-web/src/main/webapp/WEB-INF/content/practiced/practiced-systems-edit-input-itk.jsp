<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!--practiced-systems-edit-input-itk.jsp-->

<div ng-controller="ItkController" id="tab_3" class="page-content">

  <div ng-show="phasesAndConnectionsArray.length === 0" class="empty-table">Vous devez renseigner les onglets précédents pour pouvoir renseigner l'itinéraire technique synthétisé.</div>
  <div class="table-enclosure paddingTop0" ng-show="phasesAndConnectionsArray.length > 0" >
    <label>Liste de l'ensemble des phases et cultures du système synthétisé&nbsp;:
      <span class='contextual-help'>
        <span class='help-hover'>
          <s:text name="help.practiced-systems.cropCoupleNotUsedForThisCampaign"/>
        </span>
      </span>
    </label>
    <table class="data-table">
      <thead>
        <tr>
          <th scope="col">Cycle</th>
          <th scope="col" class="column-xsmall">Type de culture</th>
          <th scope="col">Culture</th>
          <th scope="col">Culture précédente</th>
          <th scope="col">Nombre d'interventions</th>
          <th scope="col">Culture absente</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-repeat="phaseOrConn in phasesAndConnectionsArray" ng-click="editPhaseOrConnection(phaseOrConn)"
            ng-class="{'selected-line' : selectedPhaseConnectionOrNode === phaseOrConn, 'line-error': !validateCycle(phaseOrConn)}"
            class="selectCursor">
          <td>{{phaseOrConn.type ? "Pérenne" : "Assolé"}}
            <input ng-if="!validateCycle(phaseOrConn)" id="phaseOrConnError_{{$index}}" type="text" ng-model="noPhaseOrConn" required style="opacity:0;width: 0;padding: 0px;border-width: 0px;"/>
          </td>
          <td>
            <div ng-if="!phaseOrConn.type" class="cell-on-one-row center">
              <div ng-if="phaseOrConn.intermediateCroppingPlanEntryCode"
                   class="textIcones INTERMEDIATE"
                   title="<s:text name='practicedSystem.itks.table.cropType.intermediate.label' /> ({{ croppingPlanIndex[phaseOrConn.intermediateCroppingPlanEntryCode].label }})"/>
                <s:text name='practicedSystem.itks.table.cropType.intermediate.abbr' />
              </div>
              <div ng-if="phaseOrConn.intermediateCroppingPlanEntryCode" style="margin-left: 10px" />
                /
              </div>
              <div ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).type === 'MAIN'"
                   class="textIcones MAIN"
                   title="<s:text name='practicedSystem.itks.table.cropType.main.label' />{{getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.species.label' />' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.variety.label' />' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.main.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr'/>
                </span>
              </div>
              <div ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).type === 'CATCH'"
                   class="textIcones CATCH"
                   title="<s:text name='practicedSystem.itks.table.cropType.catch.label' />{{getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.species.label' />' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.variety.label' />' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.catch.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr'/>
                </span>
              </div>
            </div>
            <div ng-if="phaseOrConn.type" class="cell-on-one-row center">
              <div ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).type === 'MAIN'"
                   class="textIcones MAIN"
                   title="<s:text name='practicedSystem.itks.table.cropType.main.label' />{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.species.label' />' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - <s:text name='practicedSystem.itks.table.cropType.mix.variety.label' />' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.main.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr'/>
                </span>
              </div>
              <div ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).type === 'CATCH'"
                   class="textIcones CATCH"
                   title="<s:text name='practicedSystem.itks.table.cropType.catch.label' />{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies ? ' - Mélange d\'espèces' : ''}}{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety ? ' - Mélange de variétés' : ''}}">
                <span><s:text name='practicedSystem.itks.table.cropType.catch.abbr' /></span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixSpecies"
                      class="textIcones MIX_S">
                  <s:text name='practicedSystem.itks.table.cropType.mix.species.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixVariety"
                      class="textIcones MIX_V">
                  <s:text name='practicedSystem.itks.table.cropType.mix.variety.abbr' />
                </span>
                <span ng-if="getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).mixCompanion"
                      class="textIcones MIX_C">
                  <s:text name='common-cropping-plan-mix-companion-abbr'/>
                </span>
              </div>
            </div>
          </td>
          <td ng-if="phaseOrConn.type">{{getCroppingPlanEntry(phaseOrConn.croppingPlanEntryCode).label}} ({{perennialPhaseTypes[phaseOrConn.type]}})</td>
          <td ng-if="!phaseOrConn.type">
            {{phaseOrConn.intermediateCroppingPlanEntryCode ?
                "(" + croppingPlanIndex[phaseOrConn.intermediateCroppingPlanEntryCode].label + ") " :
                ""}}
            {{connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].label}} (Rang {{connectionNodeIdToNodeObjectMap[phaseOrConn.targetId].x + 1}})
          </td>
          <td>{{phaseOrConn.type ? "-" : connectionNodeIdToNodeObjectMap[phaseOrConn.sourceId].label}}</td>
          <td>{{phaseOrConn.interventions.length}}</td>
          <td>
            <span ng-if ="!phaseOrConn.type"><input type="checkbox" ng-model="phaseOrConn.notUsedForThisCampaign"
                   ng-disabled="phaseOrConn.interventions.length > 0" /></span>
            <span ng-if="phaseOrConn.type"> - </span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="table-enclosure paddingTop0" ng-show="selectedPhaseConnectionOrNode">

    <label>Itinéraire technique synthétisé
      <span ng-show="!selectedPhaseConnectionOrNode.type">de la culture {{connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].label + ' (Rang ' + (connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].x + 1) + ')'}}</span>
      <span ng-show="selectedPhaseConnectionOrNode.type">de la phase {{perennialPhaseTypes[selectedPhaseConnectionOrNode.type]}}</span>
      &nbsp;:
      <span class='contextual-help'>
        <span class='help-hover'>
          <s:text name="help.practicedSystemItk.interventions" />
        </span>
      </span>
    </label>

    <table class="data-table">
      <thead>
        <tr>
          <th scope="col" title="{{allInterventionSelected.selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
            <input type='checkbox' ng-model="allInterventionSelected.selected" ng-change="toggleSelectedInterventions()" /></th>
          <th scope="col">Nom</th>
          <th scope="col">Action(s)</th>
          <th scope="col">Dates d'intervention (JJ/MM)</th>
          <th scope="col">Affectation CI</th>
          <th scope="col">Fréquence temporelle</th>
          <th scope="col">Fréquence spatiale</th>
          <th scope="col">Proportion surf. traitée</th>
          <th scope="col" title="<s:text name="help.PSCi" />">PSCi</th>
          <th scope="col">Données saisies</th>
          <th scope="col">Suppr.</th>
        </tr>
      </thead>

      <tbody ui-sortable ng-model="selectedPhaseConnectionOrNode.interventions">
        <tr ng-if="!selectedPhaseConnectionOrNode.notUsedForThisCampaign && selectedPhaseConnectionOrNode.interventions.length === 0" ><td colspan="11" class="empty-table">Il n'y a pas encore d'intervention pour la phase ou la culture sélectionnée. Pour en ajouter, cliquez sur le bouton "Ajouter une intervention" ci-dessous</td></tr>
        <tr ng-if="selectedPhaseConnectionOrNode.notUsedForThisCampaign" ><td colspan="11" class="empty-table">la culture {{connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].label + ' (Rang ' + (connectionNodeIdToNodeObjectMap[selectedPhaseConnectionOrNode.targetId].x + 1) + ')'}} est absente sur cette campagne</td></tr>
        <tr ng-if="!selectedPhaseConnectionOrNode.notUsedForThisCampaign && selectedPhaseConnectionOrNode.interventions.length > 0"
            ng-repeat="intervention in selectedPhaseConnectionOrNode.interventions" class="selectCursor"
            ng-class="{'selected-line' : editedIntervention == intervention, 'line-selected':selectedInterventions[intervention.topiaId], 'line-error': !validateIntervention(intervention)}"
            ng-click="editIntervention(intervention);selectedInterventions[intervention.topiaId] = !selectedInterventions[intervention.topiaId]"
            >
          <td>
            <input type='checkbox' ng-checked="selectedInterventions[intervention.topiaId]" ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid"/>
            <input ng-if="!validateIntervention(intervention)" id="interventionError_{{$index}}" type="text" ng-model="noIntervention" required style="opacity:0;width: 0;padding: 0px;border-width: 0px;"/>
          </td>
          <td>{{intervention.name|orDash}}</td>
          <td ng-click="intervention.showCompleteActionList = !intervention.showCompleteActionList">
            <ul ng-if="!intervention.showCompleteActionList" class="actions-list">
              <li ng-repeat="action in intervention.actionDtos | limitTo:3">
                <span>
                  {{agrosystInterventionTypes[action.mainActionInterventionAgrosyst]}} / {{action.mainActionReference_label |orDash}}
                </span>
              </li>
            </ul>
            <ul ng-if="intervention.showCompleteActionList" class="actions-list">
              <li ng-repeat="action in intervention.actionDtos">
                <span>
                  {{agrosystInterventionTypes[action.mainActionInterventionAgrosyst]}} / {{action.mainActionReference_label |orDash}}
                </span>
              </li>
            </ul>
            <span ng-if="!intervention.showCompleteActionList && intervention.actionDtos.length > 3">{{intervention.actionDtos.length-3}} actions supplémentaires</span>
            <span ng-if="!intervention.actionDtos || intervention.actionDtos.length === 0">-</span>
          </td>
          <td ng-if="!intervention.startingPeriodDate && !intervention.endingPeriodDate">-</td>
          <td ng-if="!intervention.startingPeriodDate && intervention.endingPeriodDate">Avant le {{intervention.endingPeriodDate}}</td>
          <td ng-if="intervention.startingPeriodDate && !intervention.endingPeriodDate">Après le {{intervention.startingPeriodDate}}</td>
          <td ng-if="intervention.startingPeriodDate && intervention.startingPeriodDate == intervention.endingPeriodDate">Le {{intervention.startingPeriodDate}}</td>
          <td ng-if="intervention.startingPeriodDate && intervention.endingPeriodDate && intervention.startingPeriodDate != intervention.endingPeriodDate">Du {{intervention.startingPeriodDate}} au {{intervention.endingPeriodDate}}</td>
          <td>{{intervention.intermediateCrop && 'Oui' || '-'}}</td>
          <td>{{(intervention.temporalFrequency | number:2) | orDash}}</td>
          <td>{{(intervention.spatialFrequency | number:2) | orDash}}</td>
          <td>
            <div ng-if="intervention.proportionOfBioTreatedSurface">Lutte bio.&nbsp;: {{(intervention.proportionOfBioTreatedSurface | number:0)}}&nbsp%</div>
            <div ng-if="intervention.proportionOfPhytoTreatedSurface">Phyto.&nbsp;: {{(intervention.proportionOfPhytoTreatedSurface | number:0)}}&nbsp%</div>
            <div ng-if="!intervention.proportionOfBioTreatedSurface && !intervention.proportionOfPhytoTreatedSurface">-</div>
          </td>
          <td title="<s:text name="help.PSCi" />" >{{((intervention.spatialFrequency * intervention.temporalFrequency) | number:2)
                |orDash}}</td>
          <td ng-click="intervention.showCompleteProductList = !intervention.showCompleteProductList" title="Cliquez pour voir la liste complète">
            <ul ng-if="!intervention.showCompleteProductList" class="products-list">
              <li ng-repeat="item in intervention.usageData | limitTo:3">
                <span ng-if="item.label">
                  {{(item.label|orDash)|truncate:20}}
                </span>
                <span ng-if="item.label && item.quantity">&nbsp;(</span><span ng-if="item.quantity">{{item.quantity|number:2}}&nbsp;{{item.unit|orDash}}</span><span ng-if="item.label && item.quantity">)</span>
              </li>
            </ul>
            <ul ng-if="intervention.showCompleteProductList" class="products-list">
              <li ng-repeat="item in intervention.usageData">
                <span ng-if="item.label">
                  {{item.label|orDash}}
                </span>
                <span ng-if="item.label && item.quantity">&nbsp;(</span><span ng-if="item.quantity">{{item.quantity|number:2}}&nbsp;{{item.unit|orDash}}</span><span ng-if="item.label && item.quantity">)</span>
              </li>
            </ul>
            <span ng-if="!intervention.showCompleteProductList && intervention.usageData.length > 3">{{intervention.usageData.length - 3}} produit(s) supplémentaire(s)</span>
            <span ng-if="!intervention.usageData || intervention.usageData.length === 0">-</span>
          </td>
          <td>
            <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteIntervention(intervention)"/>
          </td>
        </tr>
      </tbody>

      <tfoot>
        <tr ng-disabled="editedIntervention && !editedInterventionForm.$valid">
          <td colspan="4" class="table-global-info"><span ng-if="selectedPhaseConnectionOrNode.interventions.length > 1">Glissez / déposez une intervention pour changer l'ordre de celles-ci</span></td>
          <td colspan="7" ng-controller="ItkPracticedCopyPasteController">
            <div class="table-end-button">
              <input type="button" value="Ajouter une intervention" ng-click="createIntervention()" ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid"/>
            </div>
            <div class="table-end-button">
              <input type="button" value="Copier les interventions" ng-click="copyPracticedInterventions()"
                ng-show="(selectedInterventions|toSelectedLength) > 0" class="button-copy" ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid"/>
            </div>
            <div class="table-end-button">
              <input type="button" value="Coller les interventions" ng-click="pastePracticedInterventions()"
                ng-show="pastedInterventions" class="button-copy" ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid"/>
            </div>
            <div class="table-end-button">
              <input type="button" value="Supprimer les interventions" ng-click="deleteSelectedInterventions()"
                     ng-show="(selectedInterventions|toSelectedLength) > 1" class="button-delete" ng-disabled="selectedPhaseConnectionOrNode.notUsedForThisCampaign || !editedInterventionForm.$valid"/>
            </div>
          </td>
        </tr>
      </tfoot>
    </table>
  </div>

  <!-- Intervention sélectionnée -->
  <fieldset class="sub-form marginTop30" ng-form="editedInterventionForm" >
    <div id="new-intervention" class="entity-content sub-form-content full-width">

      <!-- Détails de l'intervention -->
      <div class="two-columns noclear">

        <!-- Type d'intervention-->
        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_type"><span class="required">*</span> Type d'intervention&nbsp;</label></span>
          <span class="wwctrl">
            <select id="intervention_type"
                    ng-model="editedIntervention.type"
                    ng-change="loadToolsCouplings(editedIntervention, false)"
                    ng-required="editedIntervention"
                    ng-disabled="editedIntervention.type && editedIntervention.toolsCouplingCodes.length > 0"
                    ng-attr-title="{{editedIntervention.toolsCouplingCodes.length > 0 ? 'Une combinaison d\'outils est sélectionnée, merci de la dé-sélectionner avant de changer le type d\'intervention.' : '' }}">
              <!-- the following line is required to enable HTML5 validation-->
              <option value=""></option>
              <option ng-repeat="(type, label) in agrosystInterventionTypes"
                      value="{{type}}"
                      title="{{ getInterventionTypeTooltip(type) }}"
                      ng-selected="editedIntervention.type === type">
                {{ label }}
              </option>
            </select>
          </span>
        </div>
        <div id="warning-no-selected-tools-coupling" title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.informationToBeTicked"/>">
          <div class="bubble-text">
            <p>
              <span class="warning_char"><i class="fa fa-warning" aria-hidden="true"></i></span>
              <span class="highlight-text-content"><s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.informationToBeTicked"/>
                <a class="highlight-text-content-redirect"
                   ng-click="redirectToToolsCooplingsTab()"
                   target="_blank"
                   href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='practicedSystem.growingSystem.growingPlan.domain.topiaId'/>"
                   title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>">
                   <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>
                </a>
              </span>
            </p>
          </div>
        </div>
        <div class="wwgrp checkbox-list">
          <span class="wwlbl">
            <label>
              <span class="requiredAdvised lowProfile"
                    title="<s:text name="help.performance.required"/> : <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.performance"/>">
                &pi;
              </span>
              <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings"/>&nbsp;:
              <input class="button-refresh"
                     type="button"
                     ng-click="loadToolsCouplings(editedIntervention, false)"
                     title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.reloadDomainToolsCouplings"/>"
                     value="&#8635;"/>
            </label>
          </span>
          <span class="wwctrl" ng-class="{'two-columns-ifo-rigth-text' : !Object.keys(_toolsCouplingCodeToToolsCoupling).length}">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.practicedSystemItk.toolsCouplings" />
              </span>
            </span>

            <span ng-show="toolsCouplings && toolsCouplings.length > 0"
                  ng-repeat="toolsCoupling in toolsCouplings |  filter: {manualIntervention: false} | orderBy: 'toolsCouplingName'">
              <input type="checkbox" ng-model="editedInterventionSelectedToolCouplings[toolsCoupling.code]"
                     ng-change="toggleSelectedInterventionToolsCoupling(editedIntervention, toolsCoupling)"
                     id="toolsCoupling-{{$index}}"/>
              <label class="checkboxLabel" for="toolsCoupling-{{$index}}">{{toolsCoupling.manualIntervention ? "interv. manuelle - " : ""}}{{toolsCoupling.toolsCouplingName}}</label>
            </span>
            <span ng-show="toolsCouplings && toolsCouplings.length > 0"
                  ng-repeat="toolsCoupling in toolsCouplings |  filter: {manualIntervention: true} | orderBy: 'toolsCouplingName'">
              <input type="checkbox" ng-model="editedInterventionSelectedToolCouplings[toolsCoupling.code]"
                     ng-change="toggleSelectedInterventionToolsCoupling(editedIntervention, toolsCoupling)"
                     id="intervention-{{$index}}"/>
              <label class="checkboxLabel" for="intervention-{{$index}}">
                {{ toolsCoupling.manualIntervention ? "<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.manual" /> - " : "" }}{{ toolsCoupling.toolsCouplingName }}
              </label>
            </span>
            <span id="warning-no-tools-coupling-available">
              <div class="info-text">
                  <span class="warning_char"><i class="fa fa-warning" aria-hidden="true"></i></span>
                  <div class="highlight-text-content">
                    <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.missingDomainToolsCouplings"/>
                    <a class="highlight-text-content-redirect"
                       ng-click="redirectToToolsCooplingsTab()"
                       target="_blank"
                       href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='practicedSystem.growingSystem.growingPlan.domain.topiaId'/>"
                       title="<s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>">
                       <s:text name="practicedSystem.itks.itk.intervention.toolsCouplings.goToDomain"/>
                    </a>
                  </div>
              </div>
            </span>
            <span class="empty-list" ng-if="!editedIntervention.type">
              Aucun type d'intervention sélectionné
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_name"><span class="required">*</span> Nom&nbsp;:</label></span>
          <span class="wwctrl"><input type="text" id="intervention_name" ng-model="editedIntervention.name" ng-required="editedIntervention" /></span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_starting_periode_date"><span class="required">*</span> Date de début d'intervention (JJ/MM)&nbsp;:</label></span>
          <span class="wwctrl"><input type="text" id="intervention_starting_periode_date"
          ng-model="editedIntervention.startingPeriodDate" placeholder="ex. : 15/06" ng-blur="interventionStartingDateSelected()"
          ng-pattern="/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$/"
          pattern="^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$"
          ng-required="editedIntervention"
          /></span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_ending_periode_date"><span class="required">*</span> Date de fin d'intervention (JJ/MM)&nbsp;:</label></span>
          <span class="wwctrl"><input type="text" id="intervention_ending_periode_date"
          ng-model="editedIntervention.endingPeriodDate" placeholder="ex. : 30/06"
          ng-pattern="/^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$/"
          pattern="^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[012])$"
          ng-required="editedIntervention"
          ng-blur="pushPeriodChangeToValorisations(editedIntervention, practicedSystem)"
          /></span>
        </div>

        <div class="wwgrp">
          <div class="wwlbl"><label for="intervention_comment" class="label">Commentaire&nbsp;:</label></div>
          <span class="wwctrl"><textarea id="intervention_comment" ng-model="editedIntervention.comment"></textarea></span>
        </div>
        <div class="wwgrp" ng-if="!selectedPhaseConnectionOrNode.type">
          <span class="wwlbl"><label for="intervention_intermediateCrop">Affectation à la culture intermédiaire&nbsp;:</label></span>
          <span class="wwctrl" ng-if="connectionIntermediateCrop">
            <input type="checkbox"
                   id="intervention_intermediateCrop"
                   ng-model="editedIntervention.intermediateCrop"
                   ng-change="toggleIntermediateSpeciesSelection()"/></span>
          <span class="wwctrl" ng-if="!connectionIntermediateCrop">
            <input type="checkbox"
                   id="intervention_intermediateCrop"
                   ng-model="editedIntervention.intermediateCrop"
                   ng-change="toggleIntermediateSpeciesSelection()"
                   ng-disabled="true"
                   title="Il n'y a pas de culture intermédiaire."/></span>
        </div>
      </div>
      <div class="two-columns two-columns-right noclear">
        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_spatialFrequency"><span class="required">*</span> Fréquence spatiale de l'intervention&nbsp;:</label></span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.practicedSystemItk.spatialFrequency" />
              </span>
            </span>
            <input type="text" id="intervention_spatialFrequency" ng-model="editedIntervention.spatialFrequency" placeholder="ex. : 0.5"
              ng-pattern="/^((([0])\.([0-9]*)|0)|1)$/" ng-required="editedIntervention" ng-change="computeSpendingTime()" title="Ce champ ne concerne que le passage d'outils/d'hommes" />
          </span>
        </div>
        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_temporalFrequency"><span class="required">*</span> Fréquence temporelle de l'intervention&nbsp;:</label></span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.practicedSystemItk.temporalFrequency" />
              </span>
            </span>
            <span class='contextual-warning clear' ng-if="editedIntervention.temporalFrequency > 1">
              <span class='help-hover'>
                <s:text name="warning.practicedSystemItk.temporalFrequency" />
              </span>
            </span>
            <input type="text" id="intervention_temporalFrequency" ng-required="editedIntervention" ng-model="editedIntervention.temporalFrequency" ng-change="computeSpendingTime()" placeholder="ex. : 1.5" ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"/>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl">
            <label for="intervention_workRate">
              <span class="requiredAdvised" title="<s:text name="help.performance.required"/> : <s:text name="help.performance.workTime" />">&pi;</span>&nbsp;
              <s:text name="practicedSystem.itks.itk.intervention.workRate" />&nbsp;:
            </label>
          </span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal">  
                <input type="text" id="intervention_workRate"
                       ng-model="editedIntervention.workRate"
                       ng-change="processWorkRateChange()"
                       placeholder="ex. : 1.5" ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"/>
              </span>
              <span class="column-normal">  
                <select id="workRateUnit"
                        ng-model="editedIntervention.workRateUnit"
                        ng-options="materielWorkRateUnits[workRateUnit] for workRateUnit in workRateUnits"
                        ng-change="processWorkRateChange()">
                </select>
              </span>
              <span class='contextual-help'>
                <span class='help-hover'>
                  <s:text name="help.performance.workrate" />
                </span>
              </span>
            </div>
          </span>

          <span class="wwgrp" ng-if="editedIntervention.workRateUnit == 'H_HA' || editedIntervention.workRateUnit == 'HA_H'">
            <span class="wwlbl"><label for="revertWorkRate"></label></span>
            <span class="wwctrl">
              <div class="clearfix">
                <span class="column-normal float-left paddingRight10">  
                  <input ng-if="editedIntervention.workRate && editedIntervention.workRate != 0" id="revertWorkRate" disabled value="{{1/editedIntervention.workRate| number:3}}" type="text"/>
                  <input ng-if="!editedIntervention.workRate" id="revertWorkRate" disabled type="text" />
                </span>
                <span class="input-append column-normal">
                  <span class="add-on" ng-if="editedIntervention.workRateUnit == 'H_HA'">
                    {{materielWorkRateUnits['HA_H']}}
                  </span>
                  <span class="add-on" ng-if="editedIntervention.workRateUnit == 'HA_H'">
                    {{materielWorkRateUnits['H_HA']}}
                  </span>
                </span>
              </div>
            </span>
          </span>
        </div>

        <div class="wwgrp" ng-if="editedIntervention.workRateUnit === 'VOY_H'">
          <span class="wwlbl"><label for="transitVolume"><span class="requiredAdvised">&pi;</span> Volume par voyage</label></span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal">  
                <input type="text" id="transitVolume" ng-model="editedIntervention.transitVolume"
                       ng-blur="processTransitVolumeChange()" ag-float pattern="^[\-\+]?\d*[\.,]?\d*$"/>
              </span>
              <span class="column-normal">  
                <select id="transitVolumeUnit"
                        ng-model="editedIntervention.transitVolumeUnit"
                        ng-options="key as value for (key , value) in materielTransportUnits"
                        ng-change="processTransitVolumeChange()">
                </select>
              </span>
            </div>
          </span>
        </div>

        <div class="wwgrp" ng-if="editedIntervention.workRateUnit === 'BAL_H'">
          <span class="wwlbl"><label for="nbBalls"><span class="requiredIndicator" title="<s:text name="help.performance.required"/> : Temps de travail">*</span> Nombre de balles&nbsp;:</label></span>
          <span class="wwctrl">
            <div class="clearfix">
              <span class="column-normal float-left paddingRight10">  
                <input type="text" id="nbBalls" ng-model="editedIntervention.nbBalls" ng-blur="computeSpendingTime()" ag-integer pattern="\d+"/>
              </span>
              <span class="input-append column-normal">
                <span class="add-on">bal/ha</span>
              </span>
            </div>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label>Temps de travail&nbsp;:</label></span>
          <span class="wwctrl">
            <span class="input-append">
              <span ng-if="spendingTimeErrorMessage">
                {{spendingTimeErrorMessage}}
              </span>
              <span ng-if="!spendingTimeErrorMessage">
                {{(editedIntervention.spendingTime| number:3)}} h/ha
              </span>
            </span>
          </span>
        </div>

        <span>
          <span>
            <span class="wwlbl"><label></label></span>
            <span class="wwctrl">
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'H_HA'">
                PSCi x débit de chantier
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'HA_H'">
                PSCi x 1/(débit de chantier)
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'VOY_H'">
                (PSCi x dose/ha) / (débit de chantier x vol/voy)
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'BAL_H'">
                (PSCi x bal/ha) / débit de chantier
              </span>
              <span class="input-append" ng-if="editedIntervention.workRateUnit === 'T_H'">
                (PSCi x rendement) / débit de chantier
              </span>
            </span>
          </span>
        </span>


        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_progressionSpeed">Vitesse d'avancement&nbsp;:</label></span>
          <span class="wwctrl">
            <span class="input-append">
              <input type="text" id="intervention_progressionSpeed"
                     ng-model="editedIntervention.progressionSpeed"
                     placeholder="ex. : 25" ag-integer pattern="\d+"/>
              <span class="add-on">km/h</span>
            </span>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl"><label for="intervention_involvedPeopleNumber">Nombre de personnes mobilisées&nbsp;:</label></span>
          <span class="wwctrl"><input type="text" id="intervention_involvedPeopleNumber"
                                      ng-model="editedIntervention.involvedPeopleNumber"
                                      ng-change="processInvolvedPeopleNumberChange()" placeholder="ex. : 3.5"
                                      ag-float-positive pattern="^\d+([\.,]\d+)?$"/></span>
        </div>
      </div>

      <!-- Actions et intrants -->
      <jsp:include page="../itk/species-actions-and-inputs.jsp" />

    </div>
  </fieldset>

  <div id="confirmDeleteIntervention" title="Suppression d'intervention(s)" class="auto-hide">
    Êtes-vous sûr(e) de vouloir supprimer
    <ng-pluralize count="removeInterventionsContext.interventions.length" when="{'0': '', '1': 'l\'intervention&nbsp;:', 'other': 'les interventions&nbsp;:'}"></ng-pluralize>
    <div ng-repeat="intervention in removeInterventionsContext.interventions">
      - {{intervention.name | orDash}}
    </div>
  </div>

  <div id="confirmDialog" title="Attention" class="auto-hide">
    <span ng-if="warningMessage" ng-bind-html="warningMessage"></span>
  </div>

  <div id="infoDialog" title="Information" class="auto-hide">
    <span ng-if="infoMessage" ng-bind-html="infoMessage"></span>
  </div>

</div>
