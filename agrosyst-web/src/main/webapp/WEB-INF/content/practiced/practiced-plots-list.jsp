<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Parcelles types</title>
    <content tag="current-category">practiced</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/practicedPlots.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">
      angular.module('PracticedPlotsList', ['Agrosyst'])
        .value('PracticedPlotsInitData', <s:property value="toJson(practicedPlots)" escapeHtml="false"/>)
        .value('practicedPlotFilter', <s:property value="toJson(practicedPlotFilter)" escapeHtml="false"/>);
    </script>
  </head>
  <body>
    <div ng-app="PracticedPlotsList" ng-controller="PracticedPlotsListController">
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Parcelles types</li>
      </ul>
    </div>
    
    <ul class="actions">
      <li>
        <a class="action-ajouter" href="<s:url namespace='/practiced' action='practiced-plots-edit-input' />">Créer une parcelle type</a>
      </li>
    </ul>
    
    <%-- <ul class="float-right actions">
      <li><a class="action-import button-disabled">Import</a></li>
      <li><a class="action-export-pdf button-disabled">Export PDF</a></li>
      <li><a class="action-export-xls button-disabled">Export XLS</a></li>
    </ul>--%>
    <form method="post">
      <input type="hidden" name="practicedPlotIds" value="{{selectedPracticedPlot}}" ng-repeat="selectedPracticedPlot in selectedPracticedPlots|toSelectedArray" />
      <div class="table_with_footer practiced_plots">
        <table class="entity-list clear scrollable_table fixe_layout">
          <thead>
            <tr class="doubleHeight">
              <th scope="col" class="column-xxlarge-fixed" ng-click="changeSort('PLOT')">
                <sort>
                  <header_label>Parcelle type</header_label>
                  <em ng-if="filter.sortedColumn == 'PLOT'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.PLOT, 'fa fa-sort-amount-desc ':sortColumn.PLOT}" ></em>
                  <em ng-if="filter.sortedColumn != 'PLOT'" class='fa fa-sort' ></em>              </sort>
              </th>
              <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('PRACTICED_SYSTEM')">
                <sort>
                  <header_label>Système synthétisé</header_label>
                  <em ng-if="filter.sortedColumn == 'PRACTICED_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.PRACTICED_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.PRACTICED_SYSTEM}" ></em>
                  <em ng-if="filter.sortedColumn != 'PRACTICED_SYSTEM'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('DOMAIN')">
                <sort>
                  <header_label>Exploitation ou Domaine expérimental</header_label>
                  <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                  <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('GROWING_PLAN')">
                <sort>
                  <header_label>Dispositif</header_label>
                  <em ng-if="filter.sortedColumn == 'GROWING_PLAN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_PLAN, 'fa fa-sort-amount-desc ':sortColumn.GROWING_PLAN}" ></em>
                  <em ng-if="filter.sortedColumn != 'GROWING_PLAN'" class='fa fa-sort' ></em>
                  </em>
                </sort>
              </th>
              <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_SYSTEM')">
                <sort>
                  <header_label>Système de culture</header_label>
                  <em ng-if="filter.sortedColumn == 'GROWING_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.GROWING_SYSTEM}" ></em>
                  <em ng-if="filter.sortedColumn != 'GROWING_SYSTEM'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-small" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne(s)">
                <sort>
                  <header_label>Camp.(s)</header_label>
                  <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                  <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
                </sort>
              </th>
            </tr>
            <tr>
              <td class="column-xxslarge-fixed"><input type="text" ng-model="filter.practicedPlotName" /></td>
              <td><input type="text" ng-model="filter.practicedSystemName" /></td>
              <td><input type="text" ng-model="filter.domainName" /></td>
              <td><input type="text" ng-model="filter.growingPlanName" /></td>
              <td><input type="text" ng-model="filter.growingSystemName" /></td>
              <td><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.practicedSystemCampaign" /></td>
            </tr>
          </thead>
          <tbody>
            <tr ng-show="practicedPlots.length == 0">
              <td colspan="6" class="empty-table">
                Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation
              </td>
            </tr>
            <tr ng-repeat="practicedPlot in practicedPlots"
                ng-click="selectedPracticedPlots[practicedPlot.topiaId] = !selectedPracticedPlots[practicedPlot.topiaId]"
                ng-class="{'line-selected':selectedPracticedPlots[practicedPlot.topiaId]}">
              <td class="column-xxlarge-fixed"
                  title="{{ practicedPlot.name }}">
                <a href="<s:url namespace='/practiced' action='practiced-plots-edit-input'/>?practicedPlotTopiaId={{ practicedPlot.topiaId | encodeURIComponent }}">
                  {{ practicedPlot.name }}
                </a>
              </td>
              <td class="column-xxslarge-fixed">
                <a ng-repeat="practicedSystem in practicedPlot.practicedSystems"
                   title="{{ practicedSystem.name }}"
                   href="<s:url namespace='/practiced' action='practiced-systems-edit-input'/>?practicedSystemTopiaId={{ practicedSystem.topiaId | encodeURIComponent }}">
                  {{ practicedSystem.name }}<span ng-if="!practicedSystem.active" class="unactivated">&nbsp;(inactif)</span>
                  <span ng-if="!$last">, </span>
                </a>
              </td>
              <td class="column-xxslarge-fixed">
                <a ng-repeat="practicedSystem in practicedPlot.practicedSystems"
                   title="{{ practicedSystem.growingSystem.growingPlan.domain.name }}"
                   href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId={{ practicedSystem.growingSystem.growingPlan.domain.topiaId | encodeURIComponent }}">
                  {{ practicedSystem.growingSystem.growingPlan.domain.name }}<span ng-if="!practicedSystem.growingSystem.growingPlan.domain.active" class="unactivated">&nbsp;(inactif)</span>
                  <span ng-if="!$last">, </span>
                </a>
              </td>
              <td class="column-xxslarge-fixed">
                <a ng-repeat="practicedSystem in practicedPlot.practicedSystems"
                   title="{{ practicedSystem.growingSystem.growingPlan.name }}"
                   href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId={{ practicedSystem.growingSystem.growingPlan.topiaId | encodeURIComponent }}">
                  {{ practicedSystem.growingSystem.growingPlan.name }}<span ng-if="!practicedSystem.growingSystem.growingPlan.active" class="unactivated">&nbsp;(inactif)</span>
                  <span ng-if="!$last">, </span>
                </a>
              </td>
              <td class="column-xlarge-fixed">
                <a ng-repeat="practicedSystem in practicedPlot.practicedSystems"
                   title="{{ practicedSystem.growingSystem.name }}"
                   href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId={{ practicedSystem.growingSystem.topiaId | encodeURIComponent }}">
                  {{ practicedSystem.growingSystem.name }}<span ng-if="!practicedSystem.growingSystem.active" class="unactivated">&nbsp;(inactif)</span>
                  <span ng-if="!$last">, </span>
                </a>
              </td>
              <td class="column-small">
                <span ng-repeat="practicedSystem in practicedPlot.practicedSystems">
                  {{ practicedSystem.campaigns }}<span ng-if="!$last">, </span>
                </span>
              </td>
          </tbody>
        </table>
        <div class="table-footer">
          <div class="entity-list-info">
            <span class="counter">{{pager.count}} parcelles types</span>
          </div>
          <div class="pagination">
            <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
          </div>
        </div>
      </div>
    </form>
    </div>
  </body>
</html>
