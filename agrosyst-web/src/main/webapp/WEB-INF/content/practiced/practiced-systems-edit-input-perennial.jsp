<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!--practiced-systems-edit-input-perennial.jsp-->

<%-- Liste des Cycles pluriannuels synthétisés de culture(s) pérenne(s) --%>

<div id="tab_2" ng-controller="PracticedPerennialCropCycleController">
  <div>

    <s:hidden name="practicedPerennialCropCycleDtosJson" value="{{perennialCropCycles}}"/>

    <ul class="accordion-panel">
      <li ng-show="!croppingPlanModel.length">
        Le système synthétisé ne contient aucune culture principale ou dérobée associée au domaine pour les campagnes sélectionnées.
        Vous devez modifier votre saisie pour pouvoir déclarer les caractéristiques de la plantation.
      </li>
      <li ng-repeat="practicedPerennialCropCycleDto in perennialCropCycles">
        <div class="clearfix accordion-element-title" ng-class="{selected: cycle == practicedPerennialCropCycleDto}" ng-click="toggleCycle(practicedPerennialCropCycleDto)">
          <a ng-if="practicedPerennialCropCycleDto.croppingPlanEntryName">{{practicedPerennialCropCycleDto.croppingPlanEntryName}}
            <span ng-if="practicedPerennialCropCycleDto.practicedPerennialCropCycle.solOccupationPercent">
              - {{practicedPerennialCropCycleDto.practicedPerennialCropCycle.solOccupationPercent}} % de la sole du système de culture
            </span>
          </a>
          <a ng-if="!practicedPerennialCropCycleDto.croppingPlanEntryName">Sélectionner une culture</a>
          <input type="button" class="btn float-right" value="Supprimer" ng-click="deletePracticedPerennialCropCycle(practicedPerennialCropCycleDto)"/>
        </div>
        <div class="accordion-element-content" ng-show="cycle == practicedPerennialCropCycleDto"
             ng-form="practicedPerennialCropCycleForm">

          <div id="tabs-content-practiced-perennial-crop-cycle_{{$index}}">
            <fieldset class="entity-content">
              <!-- Colonne de gauche -->
              <div class="two-columns">
                <div class="wwgrp">
                  <s:fielderror fieldName="croppingPlanEntry" />
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_croppingPlanEntryField"><span class="required">*</span> Culture&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="PerennialCropEdition{{$index}}_croppingPlanEntryField"
                            ng-model="practicedPerennialCropCycleDto.selectedCroppingPlanEntryDto"
                            ng-options="croppingPlanEntryDto.label for croppingPlanEntryDto in croppingPlanModel"
                            ng-change="propagateCropChangeOnPerennialCycle()"
                            required>
                            <option value=""></option>
                    </select>
                  </span>
                </div>

                <div class="wwgrp">
                  <div class="messages-panel" ng-show="displayWarningSolOccupationPercent">
                    <ul class="errorMessage">
                      <li>Le pourcentages des cultures dans la sole du SdC ne peut-être < 0 et > 100 !</li>
                    </ul>
                  </div>
                  <s:fielderror fieldName="solOccupationPercent" />
                  <input ng-if="displayWarningSolOccupationPercent" id="PerennialCropEdition{{$index}}_solOccupationPercent_error"
                         type="text" ng-model="solOccupationPercentError"
                         ng-required="displayWarningSolOccupationPercent" style="opacity:0;width: 0;"/>
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_solOccupationPercent"><span class="required">*</span> Pourcentage dans la sole du SdC&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="PerennialCropEdition{{$index}}_solOccupationPercent"
                              type="text"
                              ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.solOccupationPercent"
                              name="selectedPracticedPerennialCropCycleSolOccupationPercent"
                              ng-change="validSolOccupationPercent()"
                              required
                              ag-float pattern="^[\-\+]?\d*[\.,]?\d*$" />
                        <span class="add-on">%</span>
                    </span>
                  </span>
                </div>

                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingYear">Année de plantation&nbsp;:</label></span>
                  <span class="wwctrl">
                    <input id="PerennialCropEdition{{$index}}_plantingYear" type="text"
                           ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.plantingYear"
                           placeholder="ex. : 2013" ag-campaign pattern="(?:19|20)[0-9]{2}"/>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingInterFurrow">Inter-rang de plantation&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="PerennialCropEdition{{$index}}_plantingInterFurrow" type="text" ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.plantingInterFurrow"
                        placeholder="ex. : 300" ag-integer pattern="\d+" />
                      <span class="add-on">cm</span>
                    </span>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingSpacing">Espacement de plantation sur le rang&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="PerennialCropEdition{{$index}}_plantingSpacing" type="text" ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.plantingSpacing"
                        placeholder="ex. : 300" ag-integer pattern="\d+" />
                      <span class="add-on">cm</span>
                    </span>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingDensity"><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Densité de plantation&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="PerennialCropEdition{{$index}}_plantingDensity" type="text" ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.plantingDensity" placeholder="ex. : 400" ag-float-positive pattern="^\d*[\.,]?\d*$"/>
                      <span class="add-on">Plantes/ha</span>
                    </span>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_orchardFrutalForm">Forme fruitière vergers&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="PerennialCropEdition{{$index}}_orchardFrutalForm" ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.orchardFrutalForm" ng-options="key as value for (key, value) in orchardFrutalForms"></select>
                  </span>
                </div>

                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_foliageHeight">Hauteur de frondaison&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="PerennialCropEdition{{$index}}_foliageHeight" type="text"
                             ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.foliageHeight" placeholder="ex. : 330"
                             ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
                      <span class="add-on">cm</span>
                    </span>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_foliageThickness">Épaisseur de frondaison&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="PerennialCropEdition{{$index}}_foliageThickness" type="text"
                             ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.foliageThickness" placeholder="ex. : 170"
                             ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
                      <span class="add-on">cm</span>
                    </span>
                  </span>
                </div>

                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_vineFrutalForm">Forme fruitière vigne&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="PerennialCropEdition{{$index}}_vineFrutalForm" ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.vineFrutalForm"
                            ng-options="key as value for (key, value) in vineFrutalForms"></select>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_orientation">Orientation des rangs&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="PerennialCropEdition{{$index}}_orientation" ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.orientation"
                            ng-options="orientation.reference_label for orientation in orientationEDIs"></select>
                  </span>
                </div>
              </div>
              <!-- Colonne de droite -->
              <div class="two-columns">
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingDeathRate">Taux de mortalité dans la plantation&nbsp;:</label></span>
                  <span class="wwctrl">
                    <span class="input-append">
                      <input id="PerennialCropEdition{{$index}}_plantingDeathRate" type="text"
                             ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.plantingDeathRate" placeholder="ex. : 10.5"
                             ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$"/>
                      <span class="add-on">%</span>
                    </span>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_plantingDeathRateMeasureYear">Année de mesure de ce taux de mortalité&nbsp;:</label></span>
                  <span class="wwctrl">
                    <input id="PerennialCropEdition{{$index}}_plantingDeathRateMeasureYear" type="text"
                           ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.plantingDeathRateMeasureYear" placeholder="ex. : 2012"
                           ag-campaign pattern="(?:19|20)[0-9]{2}"/>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_weedType"><span class="required">*</span> Type d'enherbement&nbsp;:</label></span>
                  <span class="wwctrl">
                    <select id="PerennialCropEdition{{$index}}_weedType"
                            ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.weedType"
                            ng-options="key as value for (key, value) in weedTypes">
                    </select>
                  </span>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_pollinator">Pollinisateurs&nbsp;:</label></span>
                  <span class="wwctrl">
                    <input type="checkbox" id="PerennialCropEdition{{$index}}_pollinator"
                           ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.pollinator" />
                  </span>
                </div>
                <div ng-show="practicedPerennialCropCycleDto.practicedPerennialCropCycle.pollinator" class="slide-animation">
                  <div class="wwgrp" >
                    <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_pollinatorPercent">% de pollinisateur&nbsp;:</label></span>
                    <span class="wwctrl">
                      <span class="input-append">
                        <input type="text" id="PerennialCropEdition{{$index}}_pollinatorPercent"
                               ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.pollinatorPercent"
                               placeholder="ex. : 15" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$"/>
                        <span class="add-on">%</span>
                      </span>
                    </span>
                  </div>
                  <div class="wwgrp" >
                    <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_pollinatorSpreadMode">Mode de répartition des pollinisateurs&nbsp;:</label></span>
                    <span class="wwctrl">
                      <select id="PerennialCropEdition{{$index}}_pollinatorSpreadMode"
                              ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.pollinatorSpreadMode"
                              ng-options="key as value for (key, value) in pollinatorSpreadModes"></select>
                    </span>
                  </div>
                </div>
                <div class="wwgrp" >
                  <span class="wwlbl"><label for="PerennialCropEdition{{$index}}_other_characteristics">Autres caractéristiques du couvert végétal&nbsp;:</label></span>
                  <span class="wwctrl">
                    <textarea id="PerennialCropEdition{{$index}}_other_characteristics"
                              ng-model="practicedPerennialCropCycleDto.practicedPerennialCropCycle.otherCharacteristics"></textarea>
                  </span>
                </div>
              </div>

              <!-- Tableau des espèces -->
              <div class="table-enclosure clear">
                <label>Espèces&nbsp;:</label>
                <table class="data-table fixed-layout">
                  <thead>
                    <tr>
                      <th scope="col">Espèce</th>
                      <th scope="col">Qualifiant</th>
                      <th scope="col">Type saisonnier</th>
                      <th scope="col">Destination</th>
                      <th scope="col">Cépage / Variété</th>
                      <th scope="col">Porte-greffe</th>
                      <th scope="col">Clone de la greffe</th>
                      <th scope="col">Certification des plants</th>
                      <th scope="col">Date de sur-greffage</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr ng-show="practicedPerennialCropCycleDto.speciesDto.length === 0" ><td colspan="9" class="empty-table">Il n'y a pas d'espèces pour la culture sélectionnée.</td></tr>
                    <tr ng-repeat="cropCyclePerennialSpeciesItem in practicedPerennialCropCycleDto.speciesDto">
                      <td>{{cropCyclePerennialSpeciesItem.speciesEspece}}</td>
                      <td class="column-small">{{cropCyclePerennialSpeciesItem.speciesQualifiant|orDash}}</td>
                      <td class="column-small">{{cropCyclePerennialSpeciesItem.speciesTypeSaisonnier|orDash}}</td>
                      <td class="column-small">{{cropCyclePerennialSpeciesItem.speciesDestination|orDash}}</td>
                      <td ng-if="cropCyclePerennialSpeciesItem.varietyLibelle">{{ cropCyclePerennialSpeciesItem.varietyLibelle }}</td>
                      <td ng-if="!cropCyclePerennialSpeciesItem.varietyLibelle && cropCyclePerennialSpeciesItem.edaplosUnknownVariety"
                          class="warning-label">
                        <i class="fa fa-warning" aria-hidden="true"></i> Variété non reconnue "{{ cropCyclePerennialSpeciesItem.edaplosUnknownVariety }}"
                      </td>
                      <td ng-if="!cropCyclePerennialSpeciesItem.varietyLibelle && !cropCyclePerennialSpeciesItem.edaplosUnknownVariety">-</td>
                      <td class="column-auto" id="graftSupport-{{$index}}">
                        <ui-select ng-model="cropCyclePerennialSpeciesItem.graftSupport"
                                   id="graftSupport-{{$index}}"
                                   title="{{cropCyclePerennialSpeciesItem.graftSupport.label}}"
                                   ng-disabled="disabled"
                                   theme="select2"
                                   reset-search-input="false"
                                   ng-mouseover="enableAutocompleteGraftSupport($index)"
                                   on-select="bindSelectedCropCyclePerennialSpeciesGraftSupport($item, cropCyclePerennialSpeciesItem)"
                                   style="width:95%;">
                          <ui-select-match placeholder="ex. : Amandier x Pêcher GF 677">{{$select.selected.label}}</ui-select-match>
                          <ui-select-choices repeat="graftSupport in graftSupports track by $index"
                                             refresh="autocompleteGraftSupport($index, cropCyclePerennialSpeciesItem, $select.search)"
                                             refresh-delay="0">
                            <span ng-bind-html="graftSupport.label | highlight: $select.search"></span>
                          </ui-select-choices>
                        </ui-select>
                      </td>
                      <td class="column-auto">
                        <ui-select ng-model="cropCyclePerennialSpeciesItem.graftClone"
                                   id="graftSupportClone-{{$index}}"
                                   title="{{cropCyclePerennialSpeciesItem.graftClone.label}}"
                                   ng-disabled="disabled"
                                   theme="select2"
                                   reset-search-input="false"
                                   ng-mouseover="enableGetAutocompleteGraftClone($index)"
                                   on-select="bindSelectedCropCyclePerennialSpeciesGraftClone($item, cropCyclePerennialSpeciesItem)"
                                   style="width:100%;">
                          <ui-select-match placeholder="<s:text name='practicedSystem.perennial.species.graftClone.placeholder'/>">{{$select.selected.label}}</ui-select-match>
                          <ui-select-choices repeat="graftSupportClone in graftSupportClones track by $index"
                                             refresh="getAutocompleteGraftClone($index, cropCyclePerennialSpeciesItem, $select.search)"
                                             refresh-delay="0">
                            <span ng-bind-html="graftSupportClone.label | highlight: $select.search"></span>
                          </ui-select-choices>
                        </ui-select>
                      </td>

                      <td class="column-xsmall"><input type="checkbox" ng-model="cropCyclePerennialSpeciesItem.plantCertified" /></td>
                      <td class="column-auto">
                        <input ng-if="cycle" type="text" ui-date="dateOptions" ng-model="cropCyclePerennialSpeciesItem.overGraftDate" />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="table-enclosure">
                <label>Phases du cycle de culture&nbsp;:</label>
                <table class="data-table graph-table fixed-layout">
                  <tbody>
                    <tr>
                      <td ng-repeat="cropCyclePhase in cycle.cropCyclePhaseDtos|orderBy: orderByPhaseType">
                        <svg xmlns="http://www.w3.org/2000/svg">
                          <line x1="0" y1="95%" x2="100%" y2="95%" stroke-width="3"
                            stroke="#016680"  ng-if="cropCyclePhase.type == 'INSTALLATION'" />
                          <line x1="0" y1="95%" x2="100%" y2="10%" stroke-width="3"
                            stroke="#016680"  ng-if="cropCyclePhase.type == 'MONTEE_EN_PRODUCTION'" />
                          <line x1="0" y1="10%" x2="100%" y2="10%" stroke-width="3"
                            stroke="#016680"  ng-if="cropCyclePhase.type == 'PLEINE_PRODUCTION'" />
                          <line x1="0" y1="10%" x2="100%" y2="60%" stroke-width="3"
                            stroke="#016680"  ng-if="cropCyclePhase.type == 'DECLIN_DE_PRODUCTION'" />
                        </svg>
                        <div class="title">{{perennialPhaseTypes[cropCyclePhase.type]}}</div>
                      </td>
                    </tr>
                    <tr>
                      <td ng-repeat="cropCyclePhase in cycle.cropCyclePhaseDtos|orderBy: orderByPhaseType">
                        <div class="wwctrl">
                          <span class="input-append">
                            <input type="text" ng-model="cropCyclePhase.duration" ag-integer pattern="\d+" placeholder="ex. : 3" />
                            <span class="add-on">an(s)</span>
                          </span>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td ng-repeat="cropCyclePhase in cycle.cropCyclePhaseDtos|orderBy: orderByPhaseType">
                        <a type="button" title="Supprimer" ng-click="removeCropCyclePhase(cropCyclePhase)" >Supprimer</a>
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>
                        <div ng-repeat="cropCyclePhaseType in availablePhaseTypes()" class="table-end-button">
                          <input type="button" value="Déclarer une phase de '{{perennialPhaseTypes[cropCyclePhaseType]}}'" ng-click="addCropCyclePhase(cropCyclePhaseType)"/>
                        </div>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </fieldset>
          </div>
        </div>
      </li>
    </ul>
    <input type="button" ng-click="createPracticedPerennialCropCycle()" value="Déclarer les caractéristiques de la plantation" class="btn full-width marginTop30"/>
  </div>

  <div id="addPhaseMaxNumberRaised" title="Un cycle pluriannuel synthétisé de culture pérenne ne peut contenir que 4 phases !" class="auto-hide">
  </div>

  <div id="confirmCropChange" title="Le changement de culture sera répercuté dans l'onglet 'Itinéraire technique'." class="auto-hide">
    <span>
      <p ng-bind-html="cropChange.impactMessage"></p>
    </span>
  </div>

  <div id="confirmDeletePhase" title="La suppression de la phase sera répercutée dans l'onglet 'Itinéraire technique'." class="auto-hide">
    <span>
      <p ng-bind-html="deletePhase.impactMessage"></p>
    </span>
  </div>

  <div id="confirmRemovedPerennialCycle" title="Suppression des caractéristiques de la plantation" class="auto-hide">
    <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
      Êtes-vous sûr(e) de vouloir supprimer les caractéristiques de la plantation&nbsp;
      <span ng-if="practicedPerennialCropCycleName">:&nbsp;{{practicedPerennialCropCycleName}}&nbsp;</span>?
    </p>
    <p ng-if="practicedPerennialCropCycleNbInterventions">
      <span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
      {{practicedPerennialCropCycleNbInterventions}}&nbsp;interventions seront supprimées.
    </p>
  </div>

  <div id="confirmRemovedPhase" title="Suppression d'une phase" class="auto-hide">
    Êtes-vous sûr(e) de vouloir supprimer la phase de&nbsp;:{{perennialPhaseTypes[cropCyclePhaseToRemove.type]}}&nbsp;?
    <span ng-if="cropCyclePhaseToRemove.interventions.length > 0">
      Il y a&nbsp;{{cropCyclePhaseToRemove.interventions.length}} intervention<ng-pluralize count="cropCyclePhaseToRemove.interventions.length" when="{'0': '', '1': '', 'other': 's'}" /> dessus.
    </span>
  </div>

</div>
