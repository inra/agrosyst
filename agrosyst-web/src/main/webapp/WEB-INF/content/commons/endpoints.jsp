<%-- %%Ignore-License%% --%>
<%@ page language="java" contentType="text/javascript; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
// allow this page to be cached by browser
response.setDateHeader("Expires", System.currentTimeMillis() + 86400000L); // 24 hours in future.
%>
var ENDPOINTS = {<s:iterator status="status" value="endpoints">
    <s:property value="key"/>: '<s:property value="value"/>'<s:if test="!#status.last">,</s:if></s:iterator>
};

// Reprise des actions legacy (endpoints-js.jsp)

// Navigation context
var ENDPOINT_NAVIGATION_CONTEXT_CHOOSE_INPUT = '<s:url namespace="/context" action="context-choose-raw-input" />';

// Utilisateurs

// Domains
var ENDPOINT_DOMAINS_LOAD_PLOTS = "<s:url namespace='/domains' action='domain-edit-load-plots' />";
var ENDPOINT_DOMAINS_EDIT_CROPS_CONTEXT_JSON = "<s:url namespace='/domains' action='domain-edit-crops-context-json' />";
var ENDPOINT_LOAD_SEEDING_SPECIES_AND_PRODUCTS_USAGES_JSON = "<s:url namespace='/domains' action='load-seeding-species-and-products-usages-json' />";
var ENDPOINT_DOMAINS_EDIT_INPUT_STOCK_CONTENT_JSON = "<s:url namespace='/domains' action='domain-edit-input-stock-context-json' />";
var ENDPOINT_DOMAINS_EDIT_EQUIPMENTS_CONTEXT_JSON = "<s:url namespace='/domains' action='domain-edit-equipment-context-json' />";
var ENDPOINT_DOMAINS_EDIT_SOL_ARVALIS_LIST_JSON = "<s:url namespace='/domains' action='domain-edit-sol-arvalis-list-json' />";
var ENDPOINT_DOMAINS_EDIT_MATERIEL_TYPE2_JSON = "<s:url namespace='/domains' action='domain-edit-materiel-type2-json' />";
var ENDPOINT_DOMAINS_EDIT_MATERIEL_TYPE3_JSON = "<s:url namespace='/domains' action='domain-edit-materiel-type3-json' />";
var ENDPOINT_DOMAINS_EDIT_MATERIEL_TYPE4_JSON = "<s:url namespace='/domains' action='domain-edit-materiel-type4-json' />";
var ENDPOINT_DOMAINS_EDIT_MATERIEL_UNITE_JSON = "<s:url namespace='/domains' action='domain-edit-materiel-unite-json' />";
var ENDPOINT_DOMAINS_EDIT_MATERIEL_ALL_JSON = "<s:url namespace='/domains' action='domain-edit-materiel-all-json' />";
var ENDPOINT_DOMAINS_LIST_OTEX_JSON = "<s:url namespace='/domains' action='get-ref-otex-json' />";
var ENDPOINT_DOMAINS_CHECK_DOMAIN_NAME_JSON = "<s:url namespace='/domains' action='check-domain-name-json' />";
var ENDPOINT_DOMAINS_MATERIELS_FIND_MATERIEL = "<s:url namespace='/domains' action='domain-edit-materiel-find-materiel' />";
var ENDPOINT_LOAD_ALL_ACTIVE_AGROSYST_ACTIONS_JSON = "<s:url namespace='/domains' action='load-all-active-agrosyst-actions-json' />";
var ENDPOINT_LOAD_LIVESTOCKS_AND_CATTLES_JSON = "<s:url namespace='/domains' action='load-livestock-units-actions-json' />";
var ENDPOINT_LOAD_INPUT_REF_PRICES_JSON = "<s:url namespace='/domains' action='load-input-ref-prices-json' />";
var ENDPOINT_LOAD_PHYTO_PRODUCTS_JSON = "<s:url namespace='/domains' action='load-phyto-products-json' />";
var ENDPOINT_LOAD_BIOLOGICAL_CONTROL_INPUTS = "<s:url namespace='/domains' action='load-biological-control-inputs' />";
var ENDPOINT_DOWNLOAD_PHYTO_REF = "<s:url namespace='/referential' action='acta-traitement-produit-referential-download' />";
var ENDPOINT_DOWNLOAD_OTHER_INPUTS_REF = "<s:url namespace='/referential' action='other-inputs-referential-download' />";
var ENDPOINT_DOWNLOAD_SUBSTRATES_REF = "<s:url namespace='/referential' action='substrates-referential-download' />";
var ENDPOINT_LOAD_REF_POTS_JSON = "<s:url namespace='/domains' action='load-ref-pots-json' />";
var ENDPOINT_LOAD_REF_SUBSTRATES_JSON = "<s:url namespace='/domains' action='load-ref-substrates-json' />";
var ENDPOINT_LOAD_REF_OTHER_INPUTS_JSON = "<s:url namespace='/domains' action='load-ref-other-inputs-json' />";
var ENDPOINT_LOAD_SPECIES_UNIT_JSON = "<s:url namespace='/domains' action='load-seeding-species-units-json' />";

// Systèmes synthétisés
var ENDPOINT_PRACTICED_SYSTEMS_CROPS_TOOLS_JSON = "<s:url namespace='/practiced' action='practiced-systems-crops-tools-json' />";

var ENDPOINT_LOAD_FERTI_ENGRAIS_ORG_JSON = "<s:url namespace='/practiced' action='load-ferti-engrais-orgs-json' />";
//todo remove
var ENDPOINT_LOAD_ACTA_TREATEMENT_CODES_AND_NAMES_JSON = "<s:url namespace='/practiced' action='load-acta-treatment-codes-and-names-json' />";
//todo remove
var ENDPOINT_LOAD_ACTA_PRODUCT_UNITS_JSON = "<s:url namespace='/practiced' action='load-acta-product-units-json' />";

// Management modes
var ENDPOINT_MANAGEMENTMODES_GROWING_SYSTEM_DATA_JSON = "<s:url namespace='/managementmodes' action='management-modes-edit-growing-system-data-json' />";
var ENDPOINT_MANAGEMENTMODES_LOAD_REF_STRATEGY_LEVERS_JSON = "<s:url namespace='/managementmodes' action='management-modes-edit-load-ref-strategy-levers-json' />";
var ENDPOINT_MANAGEMENTMODES_LOAD_REF_STRATEGY_LEVERS_FOR_TERM_JSON = "<s:url namespace='/managementmodes' action='management-modes-edit-load-ref-strategy-levers-for-term-json' />";
var ENDPOINT_DECISIONRULES_CROPPING_PLAN_ENTRIES_JSON = "<s:url namespace='/managementmodes' action='decision-rules-edit-cropping-plan-entries-json' />";
var ENDPOINT_DECISIONRULES_BIO_AGRESSORS_JSON = "<s:url namespace='/managementmodes' action='decision-rules-edit-bio-agressors-json' />";
var ENDPOINT_CREATE_DECISION_RULE_JSON = "<s:url namespace='/managementmodes' action='create-decision-rules-edit-json' />";
var ENDPOINT_AVAILABLE_GROWING_SYSTEM_FOR_MM_DUPLICATION_JSON = "<s:url namespace='/managementmodes' action='available-growing-system-for-duplication-json' />";
var ENDPOINT_AVAILABLE_GROWING_SYSTEM_FOR_PS_DUPLICATION_JSON = "<s:url namespace='/growingsystems' action='available-growing-system-for-duplication-json' />";
var ENDPOINT_LOAD_REQUIRED_DECISION_RULES = "<s:url namespace='/managementmodes' action='load-required-decision-rules-json' />";
var ENDPOINT_LOAD_MARKETING_DESTINATION_OBJECTIVES_FOR_SECTOR = "<s:url namespace='/growingsystems' action='load-marketing-destination-objectives-for-sector-json' />";
var ENDPOINT_MANAGEMENT_MODES_LOAD_WRITABLE_MM_FOR_GS = "<s:url namespace='/managementmodes' action='management-modes-edit-load-writable-management-modes-For-growing-system-ids-json' />";

    // Measurement and Observations
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VARIABLE_TYPES_JSON = "<s:url namespace='/effective' action='effective-measurements-variable-types-json' />";

var ENDPOINT_EFFECTIVE_MEASUREMENTS_VARIABLES_JSON = "<s:url namespace='/effective' action='effective-measurements-variables-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_STADES_JSON = "<s:url namespace='/effective' action='effective-measurements-stades-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSLABELS_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-labels-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSPESTS_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-pests-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSSTADES_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-stades-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSSUPPORTS_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-supports-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSOBSERVATIONS_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-observations-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSQUALITATIVES_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-qualitatives-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSUNITS_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-units-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_VGOBSQUALIFIERS_JSON = "<s:url namespace='/effective' action='effective-measurements-vgobs-qualifiers-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIPESTTYPES_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-pesttypes-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIPESTS_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-pests-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIPESTSTADES_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-peststades-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDISUPPORTS_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-supports-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDINOTATIONS_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-notations-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIQUALITATIVES_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-qualitatives-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIQUALIFIERUNITS_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-qualifierunits-json' />";
var ENDPOINT_EFFECTIVE_MEASUREMENTS_EDIUNITS_JSON = "<s:url namespace='/effective' action='effective-measurements-edi-units-json' />";

// Espèces
var ENDPOINT_GRAFT_SUPPORT_JSON = "<s:url namespace='/species' action='list-graft-support-json' />";
var ENDPOINT_GRAFT_CLONE_JSON = "<s:url namespace='/species' action='list-graft-clone-json' />";

// PracticedSystem
var ENDPOINT_LOAD_GROWING_SYSTEM_COMPLETE_JSON = "<s:url namespace='/growingsystems' action='load-growing-system-complete-json' />";
