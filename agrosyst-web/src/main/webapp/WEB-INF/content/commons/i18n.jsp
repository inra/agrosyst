<%-- %%Ignore-License%% --%>
<%@ page language="java" contentType="text/javascript; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
// allow this page to be cached by browser
response.setDateHeader("Expires", System.currentTimeMillis() + 86400000L); // 24 hours in future.
%>
var I18N = <s:property value="toJson(i18n)" escapeHtml="false"/>;
var USER_LOCALE = "<s:property value='userLocale'/>";
var DEFAULT_LOCALE = "<s:property value='defaultLocale'/>";
