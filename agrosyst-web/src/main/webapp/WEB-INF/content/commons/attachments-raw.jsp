<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  Copyright (C) 2022 - 2024 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
  angular.element(document).ready(function() {
    angular.module('AttachmentsModule', ['Agrosyst', 'blueimp.fileupload'])
      .value('AttachmentsData', {
        attachmentMetadatas: <s:property value='toJson(attachmentMetadatas)' escapeHtml="false" />
      })
      .value('fileMaxSize', <s:property value='config.uploadAttachmentsMaxSize' escapeHtml='false' />)
      .value('allowedExtensions', "<s:property value='config.uploadAllowedExtensions' escapeHtml='false' />")
      .value('readOnly', "<s:property value='readOnly' escapeHtml='false' />")
      .value('i18n', I18N);
    angular.bootstrap(angular.element('#attachmentDialogDiv'), ['AttachmentsModule']);
  });
</script>

<div id="attachmentDialogDiv" ng-app="AttachmentsModule" ng-controller="AttachmentsController">

  <form action="<s:url action='attachments-upload-json' namespace='/commons' />" method="POST" enctype="multipart/form-data" data-file-upload="options">
    <s:if test="readOnly"><div>{{ messages["attachments.noRigth"] }}</div></s:if>
    <table class="data-table">
      <thead>
        <tr>
          <th scope="col">{{ messages["attachments.file"] }}</th>
          <th scope="col" class="column-small">{{ messages["attachments.fileSize"] }}</th>
          <th scope="col" class="column-large">{{ messages["attachments.fileAutor"] }}</th>
          <th scope="col">{{ messages["attachments.uploadDate"] }}</th>
          <th scope="col" class="column-xsmall">{{ messages["attachments.fileRemoveAbr"] }}</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-show="attachmentMetadatas.length == 0 && queue.length == 0">
          <td class="empty-table" colspan="5">Aucune pièce jointe n'a encore été ajoutée</td>
        </tr>
        <tr ng-repeat="attachment in attachmentMetadatas">
          <td>
            <a href="<s:url action='attachments-download' namespace='/commons'/>?attachmentTopiaId={{attachment.topiaId}}" title="{{attachment.contentType}}" target="_blank" rel="noopener noreferrer">{{attachment.name}}</a>
          </td>
          <td class="column-small">
            {{attachment.size|formatFileSize}}
          </td>
          <td class="column-large">
            {{attachment.author.firstName}} {{attachment.author.lastName}}
          </td>
          <td>
            Le {{attachment.topiaCreateDate|date:'dd/MM/yyyy'}} à&nbsp;{{attachment.topiaCreateDate|date:'HH:mm'}}
          </td>
          <td>
            <input type="button" class="btn-icon icon-delete" value="Supprimer" title={{ messages["attachments.fileRemove"] }} <s:if test="readOnly"> disabled="disabled" title={{ messages["attachments.noRigth"] }} </s:if><s:else> ng-click="deleteAttachment($index)" </s:else>/>
          </td>
        </tr>
        <tr ng-repeat="file in queue" data-file-upload-progress="file.$progress()" ng-if="file.$state() != 'resolved'">
          <td>{{file.name}}</td>
          <td class="column-small">{{file.size|formatFileSize}}</td>
          <td colspan="2">
            <span ng-if="file.error">{{file.error}}</span>
            <div class="progress progress-animated active fade" data-ng-class="{pending: 'in'}[file.$state()]" ng-if="!file.error">
              <div class="progress-bar progress-bar-success" data-ng-style="{width: num + '%'}">{{num}} %</div>
            </div>
          </td>
          <td>
            <input type="button" class="btn-icon icon-delete" value={{ messages["attachments.fileRemove"] }} title={{ messages["attachments.fileRemove"] }} ng-click="cancelFileUpload(file)" />
          </td>
        </tr>
      </tbody>
    </table>

    <input type="hidden" name="objectReferenceId" value="${objectReferenceId}" />

      <div class="fileupload-buttonbar">
        <span class="fileinput-button form-buttons">
          <span class="btn btn-darker" ng-class="{disabled: disabled || readOnly}">{{ messages["attachments.addFile"] }}</span>
          <input class="file" type="file" name="files" multiple ng-disabled="disabled || readOnly" />
        </span>
      </div>

  </form>
</div>
