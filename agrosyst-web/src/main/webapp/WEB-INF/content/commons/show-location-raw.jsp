<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<div id="map" style="height: 100%">
</div>
<script type="text/javascript">

  // create a map in the "map" div, set the view to a given place and zoom
  var map = L.map('map').setView([${centerLatitude}, ${centerLongitude}], 10);

  //add an OpenStreetMap tile layer
  <s:if test="config.ignKeyjs == null">
    L.tileLayer.provider('Esri.WorldImagery').addTo(map);
  </s:if>

  <s:else>
    // load API keys configuration, then load the interface
    // on charge la configuration de la clef API, puis on charge l'application
    Geoportal.GeoRMHandler.getConfig(['${config.ignKeyjs}'], null, null, {
      onContractsComplete: function() {
        var matrixIds3857 = new Array(22);
        for (var i= 0; i<22; i++) {
            matrixIds3857[i]= {
                identifier    : "" + i,
                topLeftCorner : new L.LatLng(20037508,-20037508)
            };
        }

        // on parcours plutot via la map de translation pour
        // que l'ordre soit déterminé et que la carte photo
        // soit la dernière et affichée par défaut
        var translate = {
            "GEOGRAPHICALGRIDSYSTEMS.MAPS": "Cartes",
            "GEOGRAPHICALGRIDSYSTEMS.PLANIGN": "Plan",
            "ORTHOIMAGERY.ORTHOPHOTOS": "Photos aériennes"
        };

        var baseLayers = {};
        for (var mapId in translate) {
          var layer = gGEOPORTALRIGHTSMANAGEMENT[gGEOPORTALRIGHTSMANAGEMENT.apiKey].resources[mapId + ":WMTS"];
          // mapKey can be not available for given apiKey
          if (layer) {
            var ignLayer = new L.TileLayer.WMTS(layer.url, {
              layer: layer.name,
              style: 'normal',
              tilematrixSet: "PM",
              matrixIds: matrixIds3857,
              format: 'image/jpeg',
              attribution: "&copy; <a href='http://www.ign.fr'>IGN</a>"
            });
            map.addLayer(ignLayer);

            baseLayers[translate[mapId]] = ignLayer;
          }
        }
        
        // all layers found
        L.control.layers(baseLayers, {}).addTo(map);
        // metric scale bar
        L.control.scale({'position':'bottomleft','metric':true,'imperial':false}).addTo(map);
      }
   });
  
  </s:else>

  <s:if test="departmentShape != null">

    var department = <s:property value="departmentShape.shape" escapeHtml="false"/>;
    var geoJsonFeature = {
      "type": "Feature",
      "geometry": department
    };
    var departmentShape = L.geoJson(geoJsonFeature).addTo(map);
    departmentShape.bindPopup("<s:property value="departmentName" />");
  </s:if>

  // add a marker in the given location, attach some popup content to it and open the popup
  <s:if test="location != null">
    /*var RedIcon = L.Icon.Default.extend({
      options: {
        iconUrl: '<s:url value='/img/marker-icon-red.png' />'
      }
    });
    var redIcon = new RedIcon();*/
    var redIcon = L.icon({
      iconUrl: '<s:url value='/img/marker-icon-red.png' />'
    });
    L.marker([${location.latitude}, ${location.longitude}], {icon: redIcon}).addTo(map).bindPopup('<s:property value="location.commune" escapeJavaScript='true'/>');
  </s:if>
  
  <s:if test="(latitude != 'null') && (longitude != 'null')">
    L.marker([<s:property value="latitude"/>, <s:property value="longitude"/>]).addTo(map)
    <s:if test="name != null">.bindPopup("<s:property value="name" escapeJavaScript="true" />")</s:if>
  .openPopup();
  </s:if>
  <s:else>
    L.marker([<s:property value="centerLatitude"/>, <s:property value="centerLongitude"/>], {opacity: 0}).addTo(map)
    <s:if test="name != null">.bindPopup("<s:property value="name" escapeJavaScript="true" />", {className: 'hiddenArrow'})</s:if>
  .openPopup();
  </s:else>

</script>
