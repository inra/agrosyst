<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Page de saisie d'un message à publier aux utilisateurs d'Agrosyst</title>
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url namespace='/' action='index' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Administration</a></li>
        <li>&gt; Diffusion d'un message</li>
      </ul>
    </div>

    <form name="info-message-form" action="<s:url namespace='/publishedmessage' action='info-message-edit' />" method="post" class="tabs clear">
      <s:actionerror cssClass="send-toast-to-js"/>
      <ul id="tabs-message-menu" class="tabs-menu clearfix">
        <li class="selected"><span>Généralités</span></li>
      </ul>

      <div id="tabs-message-content" class="tabs-content">

        <!-- Généralités -->
        <div>
          <fieldset>
            <legend class="invisibleLabel">Bloc de saisie d'un message à diffuser</legend>
            <s:textfield id="title" label="Titre" name="title" labelPosition="left" labelSeparator=" :" placeholder="ex. : Message d'Agrosyst" required="true" requiredLabel="true"/>
            <s:textarea id="content" label="Contenu" name="content" labelPosition="left" labelSeparator=" :" placeholder="ex. : <h1>Un message</h1>"/>
          </fieldset>
        </div>


        <span class="form-buttons">
          <input type="submit" class="btn-primary" value="Enregistrer"/>
        </span>
      </div>

    </form>
  </body>
</html>
