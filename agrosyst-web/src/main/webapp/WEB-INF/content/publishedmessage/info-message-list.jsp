<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Messages de Diffusion</title>
    <content tag="current-category">contextual</content>
    <script type="text/javascript">
      angular.module('InfoMessageList', ['Agrosyst']).
      value('ListInfoMessageInitData', <s:property value="toJson(messages)" escapeHtml="false"/>).
      value('messageFilter', <s:property value="toJson(messageFilter)" escapeHtml="false"/>);
    </script>
  </head>
  <body>
    <div ng-app='InfoMessageList' ng-controller="InfoMessageListController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Messages</li>
        </ul>
      </div>

      <ul class="actions">
        <li>
          <a class="action-ajouter" href="<s:url namespace='/publishedmessage' action='info-message-edit-input' />">Créer un message</a>
        </li>
      </ul>

      <form method="post" id="infoMessageListForm">

        <table class="entity-list clear" id="message-list-table">
          <thead>
            <tr>
              <th scope="col" class="column-normal">Date</th>
              <th scope="col">Titre</th>
            </tr>
          </thead>

          <tbody>
            <tr ng-show="messages.length === 0">
              <td colspan="2" class="empty-table">Aucun message</td>
            </tr>

            <tr ng-repeat="message in messages"
                ng-class="{'line-selected':selectedMessages[messages.topiaId]}">
              <td>
                {{message.messageDate | date:'dd/MM/yyyy'}}
              </td>
              <td>
                {{message.title}}
              </td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <td class="entity-list-info">
                <span class="counter">{{pager.count}} messages</span>
              </td>
              <td class="pagination">
                <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
              </td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </body>
</html>
