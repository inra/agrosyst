<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<div id="displayAllBroadcastedMessagesDialog">
    <s:iterator value="broadcastMessages">
      <table class="data-table" id="message-list-table">
        <thead>
          <tr>
            <th scope="col">
              Le <javatime:format value="${messageDate}" pattern="dd/MM/yyyy" />&nbsp;:<span><s:property value='title' /></span>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <s:text name="%{content}" />
            </td>
          </tr>
        </tbody>
      </table>
    </s:iterator>

    <div class="content-italic-center">
      <s:if test='isFirstTimeMessageRead'>
        <span>Retrouvez ces messages en cliquant sur l'enveloppe en haut à droite de l'écran.</span>
      </s:if>
    </div>
</div>
