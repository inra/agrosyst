<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <s:if test='referential'>
      <title>Référentiel '<s:text name="%{genericClassName}"/>'</title>
    </s:if>
    <s:else>
      <title>Liste d'autorité '<s:text name="%{genericClassName}"/>'</title>
    </s:else>
    <script type="text/javascript" src="<s:url value='/nuiton-js/generic.js' /><s:property value='getVersionSuffix()'/>"></script>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' /><s:property value='getVersionSuffix()'/>" />
    <script type="text/javascript">
      /**
       * Controller de la liste des domaines.
       * Double utilisation possible avec ou sans contexte.
       */
      AgrosystModule.controller('GenericListController', ['$scope', '$http', '$timeout', '$window', 'GenericListInitData', 'GenericClassName',
        function($scope, $http, $timeout, $window, initData, genericClassName) {
          $scope.genericClassName = genericClassName;
          $scope.entities = initData.elements;
          $scope.pager = initData;
          $scope.filter = {};
          $scope.filter.propertyNamesAndValues = {};
          $scope.selectedEntities = {}; // ids
          $scope.selectedEntitiesEntities = {};
          $scope.allSelectedEntitiesActive;

          $scope.import = {};
          $scope.import.editURlEnable=false;

          $scope.allSelectedEntities = [];
          $scope.allSelectedEntities[0] = {selected : false, nbSel : 0};

          $scope.typeReferentielCommune = "REFERENTIEL_COMMUNE_FRANCE";

          // list of referential that can be imported from API,
          //   key: name of referential
          //   value: if import use API interface, the name of "dataset" or if use redirected URL "false" ?
          $scope.import.apiImportReferential = {"fr.inra.agrosyst.api.entities.referential.RefSolArvalis" : "base-sol",
                                         "fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot" : false,
                                         "fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot" : false,
                                         "fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot" : false,
                                         "fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive" : false
                                         };
          $scope.import.apiImportMAAReferential = [
              "fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit",
              "fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible",
              "fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole"
          ];
          $scope.import.apiBaseURL = "https://api-agro.opendatasoft.com/api/records/1.0/download/?format=json";
          $scope.import.apiURL_dataset = "&dataset=";
          $scope.import.apiURL_apikey = "&apikey=";
          $scope.import.apiKey;
          $scope.import.apiURL = null;

          $scope.openLink = function() {
            var url = $scope.import.apiURL == null ?
                      ($scope.import.apiBaseURL +
                      $scope.import.apiURL_dataset + $scope.import.apiImportReferential[$scope.genericClassName] +
                      $scope.import.apiURL_apikey + $scope.import.apiKey)
                      : $scope.import.apiURL;

            $window.open(url, '_blank');
          };

          $scope.importMAAReferentials = function() {
            $scope.maaApiImportsCampaign = new Date().getFullYear();
            $("#maaAPI-import-form").dialog({
              resizable: false,
              width: 500,
              modal: true,
              buttons: {
                "Importer": {
                  click: function() {
                    $(this).dialog("close");
                    displayPageLoading();
                    let testApiOnProduct = $scope.maaApiImportsNumeroAmmIdMetier ? "&numeroAmmIdMetier=" + $scope.maaApiImportsNumeroAmmIdMetier : "";
                    let ajaxRequest = 'campagne=' + $scope.maaApiImportsCampaign + '&genericClassName=' + $scope.genericClassName + testApiOnProduct;
                    $http.post($scope.endpoints.runMaaImports, ajaxRequest, {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
                    .then(function(response) {
                        hidePageLoading();
                        addSuccessMessage("L'import des données de l'API MAA est en cours, vous recevrez un rapport par email quand il sera terminé.");
                    })
                    .catch(function(response) {
                      console.error("Can't import MAA API", response);
                      hidePageLoading();
                      addPermanentError("Erreur lors du lancement de l'import de l'API MAA", response.status);
                    });
                  },
                  text: 'Importer',
                  'class': 'btn-primary'
                },
                "Annuler": {
                  click: function() {
                    $(this).dialog("close");
                  },
                  text: 'Annuler',
                  'class': 'float-left btn-secondary'
                }
              }
            });
          }

          $scope.refresh = function(newValue, oldValue) {
              if (newValue === oldValue) { return; } // prevent init useless call

              // don't send empty active filter
              if ($scope.filter.active === '') {
                  delete $scope.filter.active;
              }

              var ajaxRequest = "filter=" + encodeURIComponent(angular.toJson($scope.filter));
              ajaxRequest += "&genericClassName=" + genericClassName;
              $http.post(ENDPOINTS.genericEntitiesListJson, ajaxRequest,
                      {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}).
              then(function(response) {
                  $scope.entities = response.data.elements;
                  $scope.pager = response.data;
                  var selectedEntitiesForPage = $scope.allSelectedEntities[$scope.pager.currentPage.pageNumber];
                  var nbSel = 0;
                  angular.forEach($scope.pager.elements, function(gs){
                    nbSel += $scope.selectedEntities[gs.topiaId] ? 1 : 0;
                  });
                  selectedEntitiesForPage.nbSel = nbSel;
                  selectedEntitiesForPage.selected = $scope.pager.count === selectedEntitiesForPage.nbSel;
              }).
              catch(function(response) {
                console.error("Can't get type 2 data", response);
              });
          };

          // watch with timer
          var timer = false;
          $scope.$watch('[<s:iterator value="properties" var="property" status="status">filter.propertyNamesAndValues.<s:property value="property"/><s:if test="!#status.last">, </s:if></s:iterator>]',
            function(newValue, oldValue) {
              if (timer) {
                  $timeout.cancel(timer);
              }
              timer = $timeout(function() {
                let page = $scope.filter.page;
                $scope.filter.page = 0;
                if (!page) {
                  $scope.refresh(newValue, oldValue);
                }
              }, 350);
             }, true);

          // watch without timer
          $scope.$watch('[filter.active]', function(newValue, oldValue) {
            let page = $scope.filter.page;
            $scope.filter.page = 0;
            if (!page) {
              $scope.refresh(newValue, oldValue);
            }
          }, true);

          // watch without timer
          $scope.$watch('[filter.page, filter.pageSize]', $scope.refresh, true);

          $scope.$watch('selectedEntities', function(newValue, oldValue) {
              // save entities entities
              angular.forEach($scope.entities, function(elt) {
                  if ($scope.selectedEntities[elt.topiaId]) {
                      $scope.selectedEntitiesEntities[elt.topiaId] = elt;
                  } else {
                      delete $scope.selectedEntitiesEntities[elt.topiaId];
                  }
              });
              // active property and first selected
              $scope.allSelectedEntitiesActive = true;
              angular.forEach($scope.selectedEntitiesEntities, function(entity) {
                  $scope.allSelectedEntitiesActive &= entity.active;
              });
          }, true);

          $scope.entitiesImportAPI = function () {
            delete $scope.import.apiKey;
            $scope.import.editURlEnable = false;
            $("#genericAPI-import-form").dialog({
                resizable: false,
                width: $(window).width() * 0.7,
                modal: true,
                buttons: {
                    "Importer": {
                      click: function() {
                        $("#genericAPI-import-form-submit").trigger('click');
                        $(this).dialog("close");
                      },
                      text: 'Importer',
                      'class': 'btn-primary'
                    },
                    "Annuler": {
                      click: function() {
                        $(this).dialog("close");
                      },
                      text: 'Annuler',
                      'class': 'float-left btn-secondary'
                    }
                }
            });
          };

          $scope.toggleSelectedEntities = function() {
            _toggleSelectedEntities($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, $scope.pager.elements);
          };

          $scope.toggleSelectedEntity = function(entityId) {
            _toggleSelectedEntity($scope.selectedEntities, $scope.allSelectedEntities, $scope.pager, entityId);
          };

          $scope.clearSelection = function() {
                $scope.selectedEntities = {};
                $scope.allSelectedEntities = [];
          };

      }]);

      angular.module('GenericList', ['Agrosyst']).
        value('GenericListInitData', <s:property value="entitiesJson" escapeHtml="false"/>).
        value('GenericClassName', "<s:property value="genericClassName"/>");

    </script>
  </head>
  <body>
    <div ng-app='GenericList' ng-controller="GenericListController">
      <div>
        <div id="filAriane">
          <ul class="clearfix">
            <li><a href="<s:url namespace='/' action='index' />" class="icone-home">Accueil</a></li>
            <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Administration</a></li>
            <s:if test='referential'>
              <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Référentiels</a></li>
            </s:if>
            <s:else>
              <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Listes d'autorité</a></li>
            </s:else>
            <li>&gt; <s:text name="%{genericClassName}"/></li>
          </ul>
        </div>

        <jsp:include page="../admin/admin-menu.jsp" />

        <s:actionmessage/>
        <div class="admin-page">
          <h1><s:text name="%{genericClassName}"/></h1>
          <s:if test='referential'>
            <ul class="actions">
              <li><a class="action-import" onclick="entitiesImport();return false;">Import CSV</a></li>
              <li ng-if="import.apiImportReferential[genericClassName] != null">
                <a class="action-import" ng-class="{'button-disabled': false}"
                     ng-click="entitiesImportAPI()">Import API</a></li>
              <li ng-if="import.apiImportMAAReferential.includes(genericClassName)">
                <a class="action-import" ng-class="{'button-disabled': false}"
                     ng-click="importMAAReferentials()">Import API</a></li>
              <li ng-if="import.apiImportReferential[genericClassName] == null && !import.apiImportMAAReferential.includes(genericClassName)">
                <a class="action-import" ng-class="{'button-disabled': true}">Import API</a></li>
              <li><a class="action-export-csv" ng-class="{'button-disabled':false}"
                     onclick="entitiesExport($('#genericEntitiesListForm'), false);return false;">Modèle d'import</a></li>
            </ul>
            <ul class="float-right actions">
              <li><a class="action-desactiver" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
                onclick="entitiesUnactivate($('#confirmUnactivateEntities'), $('#genericEntitiesListForm'), false); return false;" ng-if="allSelectedEntitiesActive">Désactiver</a>
                <a class="action-activer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
                onclick="entitiesUnactivate($('#confirmUnactivateEntities'), $('#genericEntitiesListForm'), true);return false;" ng-if="!allSelectedEntitiesActive">Activer</a>
              </li>

              <%--
               export de RefLocation via fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto
              --%>
              <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolArvalis" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefOrientationEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielTraction" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielOutil" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefLegalStatus" ||
                  genericClassName == "fr.inra.agrosyst.services.referential.csv.RefEspeceDto" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefVarieteGeves" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefOrientationEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefOTEX" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStadeEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefAdventice" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefFertiOrga" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefUniteEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStationMeteo" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesCarburant" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesEngrais" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesPhyto" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesSemence" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjCarburant" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjEngrais" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjPhyto" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjSemence" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMesure" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEdi" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefElementVoisinage" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefLienCulturesEdiActa" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSaActaIphy" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefTraitSdC" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefDestination" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefQualityCriteria" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSPC" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefAnimalType" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMarketingDestination" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStrategyLever" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixCarbu" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixEspece" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixPhyto" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixIrrig" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixAutre" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCulturesAgrosystCulturesMAA" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSubstrate" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPot" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixPot" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCountry" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefOtherInput" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS" ||
                  genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSeedUnits" ||
                  genericClassName == "fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto"
                  ' >
                <li>
                  <a class="action-export-csv" ng-class="{'button-disabled':false}"
                     onclick="entitiesExport($('#genericEntitiesListForm'), true);return false;">Exporter tout</a>
                </li>
                <li>
                  <a class="action-export-csv" ng-class="{'button-disabled':false}"
                     onclick="entitiesExport($('#genericEntitiesListForm'), false);return false;">Exporter la sélection</a>
                </li>
              </s:if>
            </ul>
          </s:if>

          <form method="post" id="genericEntitiesListForm" class="clear" name="listForm">
            <input type="hidden" name="entityIds" value="{{selectedEntities|toSelectedArray}}" />
            <input type="hidden" name="genericClassName" value="<s:property value='genericClassName'/>"/>
            <input type="hidden" name="exportAllEntities" value="false"/>

            <table class="entity-list clear" id="generic-list-table">
              <thead>
                <tr>
                  <s:if test='referential && !translation'>
                    <th scope="col" class="column-tiny"
                        title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                        <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
                    </th>
                  </s:if>
                  <s:iterator value="properties" var="property">
                    <th scope="col"><s:property value="property"/></th>
                  </s:iterator>
                  <s:if test='referential && !translation'>
                    <th scope="col" class="column-xsmall">État</th>
                  </s:if>
                  <s:if test="refCountry">
                    <th scope="col" class="column-xsmall">Pays</th>
                  </s:if>
                </tr>
                <s:if test='referentialOrHashedValues'>
                  <tr>
                    <s:if test='referential && !translation'>
                      <td />
                    </s:if>
                    <s:iterator value="properties" var="property">
                      <td><input type="text" ng-model="filter.propertyNamesAndValues.<s:property value='property'/>" /></td>
                    </s:iterator>

                    <s:if test='referential && !translation'>
                      <td>
                        <select ng-model="filter.active">
                          <option value=""/>
                          <option value="true">Actif</option>
                          <option value="false">Inactif</option>
                        </select>
                      </td>
                    </s:if>
                    <s:if test="refCountry">
                      <td></td>
                    </s:if>
                  </tr>
                </s:if>
              </thead>

              <tbody>
                <tr ng-show="entities.length === 0">
                  <td colspan="<s:property value='propertiesSize'/>" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres</td>
                </tr>
                <s:if test='referential'>
                  <tr ng-repeat="entity in entities"
                        ng-class="{'line-selected':selectedEntities[entity.topiaId]}">

                    <s:if test='!translation'>
                      <td>
                        <input type='checkbox' ng-model="selectedEntities[entity.topiaId]" ng-checked="selectedEntities[entity.topiaId]" ng-click="toggleSelectedEntity(entity.topiaId)"/>
                      </td>
                    </s:if>

                    <s:iterator value="properties" var="property">
                      <td>{{entity.<s:property value="property"/>}}</td>
                    </s:iterator>
                    <s:if test="!translation">
                      <td>{{entity.active ? 'Actif' : 'Inactif'}}</td>
                    </s:if>
                    <s:if test="refCountry">
                      <td>{{entity.refCountry.trigram}}</td>
                    </s:if>
                  </tr>
                </s:if>
                <s:else>
                  <tr ng-repeat="entity in entities">
                    <s:iterator value="properties" var="property">
                      <td>{{entity.<s:property value="property"/>}}</td>
                    </s:iterator>
                  </tr>
                </s:else>
              </tbody>
            </table>
          </form>
          <div class="pagination pagination-out-of-table">
            <span class="counter">{{pager.count}} entitées</span>
            <s:if test="!translation">
              <span class="selection">
                <a ng-click="clearSelection()" href="">Tous désélectionner</a>
              </span>
              <span class="selection">
                <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
              </span>
            </s:if>
          </div>
          <div class="pagination pagination-out-of-table">
            <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
          </div>
          <div id="confirmUnactivateEntities" title="Activation/Désactivation des entrées du référentiel" class="auto-hide">
            Êtes-vous sûr(e) de vouloir {{allSelectedEntitiesActive?'désactiver':'activer'}} cette(ces) entrée(s) du référentiel ?
          </div>
        </div>
      </div>


      <s:if test='referential'>

        <form action="<s:url namespace='/admin' action='run-import' />" method="post" enctype="multipart/form-data" id="generic-import-form" class="auto-hide" title="Import">
          <div class="introduction-paragraph">Pour rajouter ou modifier des données, vous devez importer un fichier au format <strong>CSV</strong> encodé en <strong>UTF8</strong> comportant les informations
             à ajouter ou mettre à jour. Pour cela, vous pouvez créer un fichier ou utiliser la fonctionnalité d'export pour
             travailler à partir d'un modèle.
           </div>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit"'>
            <s:file name="source1" label="Fichier ActaTraitementsProduits" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement"'>
            <s:file name="source1" label="Fichier traitement produit eDaplos" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg"'>
            <s:file name="source1" label="Fichier ActaTraitementsProduitsCateg" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolArvalis"'>
            <s:file name="source1" label="Fichier Sol Arvalis" labelSeparator=" :" required="true" requiredLabel="true"/>
            <s:file name="source2" label="Fichier Regions" labelSeparator=" :" required="true" requiredLabel="true" />
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.services.referential.csv.RefLocationExtendedDto"'>
            <div class="wwgrp">
              <div class="wwlbl">
                <label><span class="required">*</span> Type de référentiel</label>
              </div>
              <div class="wwctrl">
                <input type="radio" name="typeReferentielCommune" value="REFERENTIEL_COMMUNE_FRANCE" id="refCommuneFrance" ng-model="typeReferentielCommune" />Référentiel français
                <input type="radio" name="typeReferentielCommune" value="REFERENTIEL_COMMUNE_EUROPE" id="refCommuneEurope" ng-model="typeReferentielCommune" />Référentiel européen
              </div>
            </div>

            <div ng-if="typeReferentielCommune == 'REFERENTIEL_COMMUNE_FRANCE'">
              <s:file name="source1" label="Fichier des communes françaises" labelSeparator=" :" required="true" requiredLabel="true"/>
            </div>
            <div ng-if="typeReferentielCommune == 'REFERENTIEL_COMMUNE_EUROPE'">
              <s:file name="source1" label="Fichier communes européennes" labelSeparator=" :" required="true" requiredLabel="true"/>
            </div>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCountry"'>
            <s:file name="source1" label="Fichier des pays" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielTraction"'>
            <s:file name="source1" label="Fichier de tracteurs BCMA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur"'>
            <s:file name="source1" label="Fichier d'automoteurs BCMA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation"'>
            <s:file name="source1" label="Fichier de matériels d'irrigation BCMA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMaterielOutil"'>
            <s:file name="source1" label="Fichier d'outils BCMA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefLegalStatus"'>
            <s:file name="source1" label="Fichier des statuts juridiques" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.services.referential.csv.RefEspeceDto"'>
            <s:file name="source1" label="Fichier des espèces" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefVarieteGeves"'>
            <s:file name="source1" label="Fichier des variétés GEVES" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape"'>
            <s:file name="source1" label="Fichier des variétés PlantGrape" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape"'>
            <s:file name="source1" label="Fichier des Clones PlantGrape" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete"'>
            <s:file name="source1" label="Fichier d'association Espèces <> Variétés" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefOrientationEDI"'>
            <s:file name="source1" label="Fichier des orientations EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefOTEX"'>
              <s:file name="source1" label="Fichier des orientations technico-économiques des exploitations agricoles OTEX" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI"'>
            <s:file name="source1" label="Fichier des types d'action Agrosyst de travails EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStadeEDI"'>
            <s:file name="source1" label="Fichier des stades de culture EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI"'>
            <s:file name="source1" label="Fichier des zonages de parcelle EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo"'>
            <s:file name="source1" label="Fichier des profondeurs de sol INDIGO" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo"'>
            <s:file name="source1" label="Fichier des caracteristiques de sol INDIGO" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa"'>
            <s:file name="source1" label="Fichier des textures de sol GEPPA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA"'>
            <s:file name="source1" label="Fichier des produits fertilisants minéraux" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefAdventice"'>
            <s:file name="source1" label="Fichier référentiel adventices" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement"'>
            <s:file name="source1" label="Fichier référentiel 'Amortissement du matériel'" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI"'>
            <s:file name="source1" label="Fichier des cibles d'un traitement" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefFertiOrga"'>
            <s:file name="source1" label="Fichier des types de produit fertilisant" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefUniteEDI"'>
            <s:file name="source1" label="Fichier des unités EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStationMeteo"'>
            <s:file name="source1" label="Fichier des stations meteo" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesCarburant"'>
            <s:file name="source1" label="Fichier des GES Carburants" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesEngrais"'>
            <s:file name="source1" label="Fichier des GES Engrais" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesPhyto"'>
            <s:file name="source1" label="Fichier des GES Phyto" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGesSemence"'>
            <s:file name="source1" label="Fichier des GES Semences" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjCarburant"'>
            <s:file name="source1" label="Fichier des NRJ Carburants" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjEngrais"'>
            <s:file name="source1" label="Fichier des NRJ Engrais" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjPhyto"'>
            <s:file name="source1" label="Fichier des NRJ Phyto" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjSemence"'>
            <s:file name="source1" label="Fichier des NJR Semences" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil"'>
            <s:file name="source1" label="Fichier des NRJ/GES Outils" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMesure"'>
            <s:file name="source1" label="Fichier des mesures" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI"'>
            <s:file name="source1" label="Fichier des supports organe EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI"'>
            <s:file name="source1" label="Fichier des stades nuisibles EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI"'>
            <s:file name="source1" label="Fichier des types notations EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI"'>
            <s:file name="source1" label="Fichier des valeurs qualitatives EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI"'>
            <s:file name="source1" label="Fichier des unites qualifiants EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive"'>
            <s:file name="source1" label="Fichier des substances active" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs"'>
            <s:file name="source1" label="Fichier des protocoles VgObs" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefElementVoisinage"'>
            <s:file name="source1" label="Fichier des éléments de voisinage" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy"'>
            <s:file name="source1" label="Fichier des phyto substance active I-Phy" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture"'>
            <s:file name="source1" label="Fichier des types de conduite" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC"'>
            <s:file name="source1" label="Fichier ACTA Dosage SPC Complet" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot"'>
            <s:file name="source1" label="Fichier API ACTA Dosage SPC" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot"'>
            <s:file name="source1" label="Fichier API ACTA Dosage SA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot"'>
            <s:file name="source1" label="Fichier API ACTA Produit" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures"'>
            <s:file name="source1" label="Fichier ACTA Groupe cultures" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefLienCulturesEdiActa"'>
            <s:file name="source1" label="Fichier lien cultures EDI-ACTA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSaActaIphy"'>
            <s:file name="source1" label="Fichier SA ACTA/IPHY" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefTraitSdC"'>
            <s:file name="source1" label="Fichier des traits de système de culture" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol"'>
            <s:file name="source1" label="Fichier couverture au sol des cultures" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle"'>
            <s:file name="source1" label="Fichier couverture au sol des cultures annuelles" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne"'>
            <s:file name="source1" label="Fichier couverture au sol des cultures pérennes" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy"'>
            <s:file name="source1" label="Fichier zone climatiques I-Phy" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefDestination"'>
            <s:file name="source1" label="Fichier destination des cultures après récolte" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefQualityCriteria"'>
            <s:file name="source1" label="Fichier des critères de qualité des récoltes" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice"'>
            <s:file name="source1" label="Fichier des prix des récoltes" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter"'>
            <s:file name="source1" label="Fichier des correspondance entre unité de rendement et prix de récolte" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector"'>
            <s:file name="source1" label="Fichier des relations entre espèce et filières" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefStrategyLever"'>
            <s:file name="source1" label="Fichier des leviers par filières" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefAnimalType"'>
            <s:file name="source1" label="Fichier des type d’animaux" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMarketingDestination"'>
            <s:file name="source1" label="Fichier des modes de commercialisation" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI"'>
            <s:file name="source1" label="Fichier de correspondance Travail EDI - Intrant EDI" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass"'>
            <s:file name="source1" label="Fichier des classes de critères de qualité" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixCarbu"'>
            <s:file name="source1" label="Fichier des prix de référence pour le carburant" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixEspece"'>
            <s:file name="source1" label="Fichier des prix de référence pour les semences" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin"'>
            <s:file name="source1" label="Fichier des prix de référence pour les intrants minéraux" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga"'>
            <s:file name="source1" label="Fichier des prix de référence pour les intrant organiques" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>


          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixPhyto"'>
            <s:file name="source1" label="Fichier des prix de référence pour les intrants phytosanitaires" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixIrrig"'>
            <s:file name="source1" label="Fichier des prix de référence pour les intrants en irrigation" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixAutre"'>
            <s:file name="source1" label="Fichier des prix de références pour d'autres types d'intrants" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter"'>
            <s:file name="source1" label="Fichier de convertion d'unité d'intrant vers unité de prix" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools"'>
            <s:file name="source1" label="Fichier de correspondance code espèce autre outils" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType"'>
            <s:file name="source1" label="Fichier des type d’animaux des troupeaux" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment"'>
            <s:file name="source1" label="Fichier des aliments de ration des troupeaux" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole"'>
            <s:file name="source1" label="Fichier des références MAA Biocontrole" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible"'>
            <s:file name="source1" label="Fichier des doses de référence MAA par groupe cible" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA"'>
            <s:file name="source1" label="Fichier des correspondances cibles Agrosyst <-> groupes cibles MAA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCulturesAgrosystCulturesMAA"'>
            <s:file name="source1" label="Fichier des correspondances cultures Agrosyst <-> cultures MAA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter"'>
            <s:file name="source1" label="Des destinataires des messages utilisateurs" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSubstrate"'>
            <s:file name="source1" label="Fichier des substrats" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate"'>
            <s:file name="source1" label="Fichier des prix de référence des substrats" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPot"'>
            <s:file name="source1" label="Fichier des pots" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPrixPot"'>
            <s:file name="source1" label="Fichier des prix de référence des pots" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement"'>
            <s:file name="source1" label="Fichier de correspondance Types produits <-> Autres" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.TradRefMateriel"'>
            <s:file name="source1" label="Fichier des traductions des matériels" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.TradRefSol"'>
            <s:file name="source1" label="Fichier des traductions des sols" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.TradRefVivant"'>
            <s:file name="source1" label="Fichier des traductions du vivant" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.TradRefDivers"'>
            <s:file name="source1" label="Fichier des traductions diverses" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.TradRefIntervention"'>
            <s:file name="source1" label="Fichier des traductions des interventions" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.TradRefDestination"'>
            <s:file name="source1" label="Fichier des traductions des destinations" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.TradRefIntrant"'>
            <s:file name="source1" label="Fichier des traductions des intrants" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefOtherInput"'>
            <s:file name="source1" label="Fichier de données du référentiel des intrants 'Autres'" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne"'>
            <s:file name="source1" label="Fichier de données du référentiel des Substances Actives Commission Européenne" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM"'>
            <s:file name="source1" label="Fichier de données du référentiel Composition de susbtances actives par numero amm" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM"'>
            <s:file name="source1" label="Fichier de données du référentiel Phrases de risque classes et mentions de danger par AMM" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA"'>
            <s:file name="source1" label="Fichier de données du référentiel de conversion des unités pour les QSA" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS"'>
            <s:file name="source1" label="Fichier de données du référentiel de correspondance Matériel outil <-> TS" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>
          <s:if test='genericClassName == "fr.inra.agrosyst.api.entities.referential.RefSeedUnits"'>
            <s:file name="source1" label="Fichier de données du référentiel de correspondance des unités de semis <-> semences" labelSeparator=" :" required="true" requiredLabel="true"/>
          </s:if>

          <s:hidden id="run_import_genericClassName" name="genericClassName" value="%{genericClassName}"/>
          <input type="submit" id="generic-import-form-submit" class="auto-hide"/>
        </form>

        <form action="<s:url namespace='/admin' action='run-api-import' />" method="post" enctype="multipart/form-data" id="genericAPI-import-form" class="auto-hide" title="Import API">
          <div ng-if="import.apiImportReferential[genericClassName] === false">
            <div class="wwgrp">
              <span class="wwlbl">
                <label for="url">URL&nbsp;:</label>
              </span>
              <span class="wwctrl">
                <input id="apiURL" type="text" ng-model="import.apiURL"/>
              </span>
            </div>
            <div class="wwgrp">
              <span class="wwlbl">
                <label for="user">Nom d'utilisateur&nbsp;:</label>
              </span>
              <span class="wwctrl">
                <input id="user" type="text" ng-model="import.user" />
              </span>
            </div>
            <div class="wwgrp">
              <span class="wwlbl">
                <label for="passwd">Mot de passe&nbsp;:</label>
              </span>
              <span class="wwctrl">
                <input id="passwd" type="password" ng-model="import.password" />
              </span>
            </div>
          </div>


          <div ng-if="import.apiImportReferential[genericClassName] != false">

            <div class="wwgrp" ng-if="!import.editURlEnable">
              <span class="wwlbl">
                <label for="apiKeyInput">Clef&nbsp;:</label>
              </span>
              <span class="wwctrl">
                <input id="apiKeyInput" type="text" ng-model="import.apiKey" placeholder="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"/>
              </span>
            </div>

            <div class="wwgrp">
              <span class="wwlbl">
                <label for="apiURLInput">URL&nbsp;:</label>
              </span>
              <span class="wwctrl">
                <span class="input-append">
                  <span id="defaultURL" ng-if="!import.editURlEnable">
                    {{import.apiBaseURL +
                         import.apiURL_dataset + import.apiImportReferential[genericClassName] +
                         import.apiURL_apikey + import.apiKey}}
                  </span>
                  <span ng-show="import.editURlEnable">
                    <input id="apiURLInput" type="text" ng-model="import.apiURL"/>
                  </span>
                </span>
                <span class="add-on">
                  <span ng-show="!import.editURlEnable">
                    <input type="button" class="btn-icon icon-edit" value="<s:text name='itk-input-table-modify' />" title="<s:text name='itk-input-table-modify' />" ng-click="import.editURlEnable=true"/>
                  </span>
                  <span id="openUrl">
                    <input type="button" class="btn-icon icon-locate" value="Modifier" title="Tester" ng-click="openLink()"/>
                  </span>
                </span>
              </span>
            </div>
          </div>

          <s:hidden id="api_import_form_genericClassName" name="genericClassName" value="%{genericClassName}"/>
          <s:hidden id="api_import_form_apiURL" name="url" value="{{import.apiURL}}"/>
          <s:hidden id="api_import_form_apiKey" name="apiKey" value="{{import.apiKey}}"/>
          <s:hidden id="api_import_form_user" name="apiUser" value="{{import.user}}"/>
          <s:hidden id="api_import_form_password" name="apiPasswd" value="{{import.password}}"/>

          <input type="submit" id="genericAPI-import-form-submit" class="auto-hide"/>
        </form>

        <form id="maaAPI-import-form"
             class="auto-hide"
             title="Imports API MAA">
          <div class="wwgrp" style="text-align: center; margin: 10px;">
            L'import sera lancé en arrière-plan, vous recevrez un email récapitulatif quand il sera terminé.
          </div>
          <div class="wwgrp">
            <span class="wwlbl" style="width: 50%;">
              <label for="campagne">Campagne&nbsp;:</label>
            </span>
            <span class="wwctrl" style="width: 50%;">
              <input id="campagne" name="campagne" type="text" ag-integer ng-model="maaApiImportsCampaign" placeholder="2024"/>
            </span>
          </div>
          <div class="wwgrp">
            <span class="wwlbl"  style="width: 50%;">
              <label for="numeroAmmIdMetier">NumeroAmmIdMetier (pour test)&nbsp;:</label>
            </span>
            <span class="wwctrl" style="width: 50%;">
              <input id="numeroAmmIdMetier" name="numeroAmmIdMetier" type="text" ag-integer ng-model="maaApiImportsNumeroAmmIdMetier" placeholder="2060131"/>
            </span>
          </div>
        </form>

      </s:if>
    </div>
  </body>
</html>
