<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Charte d'utilisation</title>
  </head>
  <body>
    <div class="legal">
      <h1>Charte d'utilisation du Système d'Information Agrosyst</h1>

      <h2>Droits et devoirs des utilisateurs</h2>

      <p>Agrosyst est un système d’information dédié à la description et l’évaluation des systèmes de culture. Il a été développé par l’INRA, dans le cadre du plan ECOPHYTO financé par l’ONEMA, pour le compte du réseau DEPHY, en partenariat avec le ministère en charge de l’agriculture, la Cellule d’Animation Nationale du réseau DEPHY  et de nombreux acteurs (techniciens et ingénieurs des chambres d’agriculture ou des instituts techniques, experts) représentés au sein du Comité des Partenaires.</p>
      
      <h3>Propriété et utilisation des données saisies par les utilisateurs</h3>
      <p>Les données enregistrées dans Agrosyst sont la propriété de l’institution de rattachement de la personne qui les a enregistrées dans le système d’information. L’institution propriétaire des données peut à tout moment avoir accès aux données dont elle est propriétaire pour son usage propre. L’accès aux données sera possible soit directement via l’interface du système d’information soit en faisant à l’équipe Agrosyst de l’INRA (<em>agrosyst-users [at] listes.inra.fr</em>) la demande d’une extraction des données concernées.</p>
      <p>Les données enregistrées dans le système d’information Agrosyst dans le cadre du réseau DEPHY peuvent être librement utilisées par la Cellule d’Animation Nationale pour la valorisation et l’animation du réseau, la production de références sur les systèmes économes en pesticides, et la production de supports de communication à l’échelle nationale. Les ingénieurs-réseau DEPHY ont accès aux données de leurs groupes de fermes. Ils sont responsables de la qualité des données correspondant aux systèmes de culture qu’ils suivent, quelles que soient les modalités d’enregistrement des données (saisie direct ou migration depuis un autre système d’information), et sont encouragés à les valoriser pour l’animation de groupe et la production de supports de communication locale. Les ingénieurs territoriaux ont accès aux données des territoires qu’ils animent dans leurs filières.</p> 
      <p>Les données enregistrées dans le cadre du réseau DEPHY sont également accessibles, après anonymisation, pour les partenaires des projets de recherche financés dans le cadre de l’appel à projets Pour et Sur le Plan ECOPHYTO (PSPE). Les partenaires des projets PSPE s’engagent à n’utiliser ces données que dans le cadre des recherches délimitées par les conventions des projets de recherche PSPE. La levée de l’anonymisation pourra être demandée aux propriétaires de données si les projets de recherche le requièrent.</p>
      <p>Toute autre utilisation des données enregistrées dans le cadre du réseau DEPHY ne sera possible qu’après sollicitation du Comité des Partenaires. En cas d’avis favorable, le Comité des Partenaires encouragera les demandeurs à prendre contact avec les propriétaires des données pour vérifier l’adéquation entre les données et le projet de valorisation.</p>
      <p>Les données enregistrées dans le système d’information Agrosyst hors du cadre du réseau DEPHY pourront être valorisées par leurs propriétaires. Sauf accord préalable avec les propriétaires de ces données saisies hors du cadre DEPHY, ces données pourront éventuellement contribuer à des valorisations par des tiers, uniquement si le Comité des Partenaires donne un avis favorable à ces projets de valorisation après sollicitation par le(s) candidat(s) à la valorisation. Les propriétaires des données enregistrées hors du cadre DEPHY pourront être représentés au Comité des Partenaires pour contribuer à l’élaboration de ses avis sur la valorisation des données. Le Comité des Partenaires veillera notamment à préserver les intérêts des propriétaires des données contre d’éventuels projets de valorisation abusive.</p>
      <p>Les utilisateurs sont responsables de l'interprétation et de l'utilisation qu'ils font des résultats. Il leur appartient d'en faire un usage conforme aux réglementations en vigueur et aux recommandations de la Commission nationale de l'informatique et des libertés (Cnil) pour les données à caractère personnel. En particulier, lors de la diffusion de résultats d’analyses, il appartient aux diffuseurs de ne pas divulguer des informations permettant d’identifier les fournisseurs de données (exploitations agricoles et sites expérimentaux) sauf autorisation expresse de ces fournisseurs.</p>
      
      <h3>Données nominatives sur les exploitants et exploitations agricoles</h3>
      <p>Le système d’information Agrosyst comporte des informations confidentielles nominatives sur les agriculteurs qui conduisent les systèmes de culture décrits. Les propriétaires des données ont la responsabilité d’assurer le respect des droits des personnes concernées définis par la loi française ‘Informatique et liberté’ : (i) les agriculteurs doivent être informés du fait que le système d’information Agrosyst contient des informations nominatives les concernant ; (ii) ils ont la possibilité d’accéder à tout moment aux données qui les concernent, soit sur demande auprès du propriétaire des données, soit directement sur demande auprès de l’équipe Agrosyst de l’INRA (<em>agrosyst-users [at] listes.inra.fr</em>) ; (iii) ils ont le droit de rectifier des données erronées les concernant  par les mêmes moyens; enfin (iv) ils ont le droit de s'opposer au fait d'apparaître dans le système d’information Agrosyst, ou de refuser que les données soient utilisées ou communiquées à des tiers.</p>
      <p>Rappel : la participation au réseau DEPHY est associée à un engagement moral de l’agriculteur à participer à la vie du réseau et à contribuer à la production de références sur les systèmes économes en pesticides.</p>
      <p>Les propriétaires des données ont la responsabilité de faire signer le formulaire ‘<em>Droits des agriculteurs relatifs au système d’information Agrosyst</em>’ (accessible depuis le lien « Mentions légales » en bas de l'application) autorisant l’enregistrement de données les concernant. Les Ingénieurs Réseau DEPHY conserveront les originaux de ces formulaires signés par les agriculteurs de leurs groupes de fermes, et devront pouvoir les présenter à la demande à tout moment. Ils en transmettent une copie scannée à l‘équipe Agrosyst de l’INRA (<em>agrosyst-users [at] listes.inra.fr</em>).</p>
      
      <h3>Propriété des bases de données de références</h3>
      <p>Les listes déroulantes proposées dans les différents écrans d’Agrosyst sont fournies par les partenaires du projet et dans le seul but de répondre aux besoins du réseau DEPHY. Ces données restent la propriété de leurs fournisseurs. Toute copie totale ou partielle ne peut se faire sans leur accord.</p>
      
      <h3>Propriété du logiciel Agrosyst</h3>
      <p>Le site Agrosyst est la propriété de l’Inra. Toute copie totale ou partielle ne peut se faire sans l'accord de l'Institut.</p>
      
      <h3>Déclaration CNIL</h3>
      <p>Conformément aux dispositions contenues dans la loi n° 78-17 du 6 Janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés, vous disposez d’un droit d’accès, de rectification, de modification et de suppression concernant les données qui vous concernent. Vous pouvez faire exercer ce droit en écrivant à <em>agrosyst-users [at] listes.inra.fr</em></p>
      <p>Les droits et devoirs des utilisateurs peuvent être retrouvés à tout moment grâce au lien « Mentions légales » de l’application.</p>

      <form action="<s:url action='charter' namespace='/auth' />" method="post">
        <s:hidden name="next" />
        <button name="accepted" type="submit" value="false" class="btn btn-darker">Refuser</button>
        <button name="accepted" type="submit" value="true" class="btn float-right">Accepter la charte</button>
      </form>
    </div>
  </body>
</html>
