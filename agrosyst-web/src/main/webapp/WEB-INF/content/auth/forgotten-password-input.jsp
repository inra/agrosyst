<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Mot de passe perdu</title>
  </head>
  <body>
      <div class="login-panel">
        <div class="pannel-top">
          <img id="logo-ecophyto" src="<s:url value='/img/logo-Ecophyto-Dephy.png' />" alt="logo-ecophyto"/>
          <h1>Récupération du mot de passe</h1>
          <s:if test="reminderSent == false">
            <p>Veuillez indiquer votre email pour récupérer votre mot de passe</p>
          </s:if>
        </div>
        <s:actionmessage/>
        <s:if test="reminderSent == false">
          <form action="<s:url action='forgotten-password' namespace='/auth' />" method="post">
            <s:hidden name="next" value="%{next}" />
            <div class="fields">
              <s:textfield type="email" label="Email" name="email" value="%{email}" labelSeparator=" :" labelPosition="left" placeholder="Adresse e-mail" required="true" cssClass="login-username"/>
              <a href="<s:url namespace='/auth' action='login-input' />">Retour à la page d'authentification</a>
            </div>
            <s:actionerror/>
            <input type="submit" class="btn-primary" value="Réinitialiser"/>
          </form>
        </s:if>
        <s:else>
          <div class="fields">
            <a href="<s:url namespace='/auth' action='login-input' />">Retour à la page d'authentification</a>
          </div>
        </s:else>
      </div>
  </body>
</html>
