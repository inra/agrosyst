<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Authentification</title>
    <script type="text/javascript">

      function requestForgottenPassword(next) {
        var email = $('input[name=email]', '#loginForm').val();
        $(location).attr('href', ENDPOINTS.forgottenPassword + "?email=" + email + "&next=" + next);
      }

    </script>
  </head>
  <body>
      <div class="login-panel">
        <div class="pannel-top">
          <img id="logo-ecophyto" src="<s:url value='/img/logo-Ecophyto-Dephy.png' />" alt="logo-ecophyto"/>
          <h1>Authentification</h1>
          <p>Vous devez vous authentifier pour accéder au système d’information Agrosyst. Veuillez saisir votre adresse e-mail et votre mot de passe</p>
        </div>
        <form id="loginForm" action="<s:url action='login' namespace='/auth' />" method="post">
          <s:actionmessage/>
          <s:actionerror escape="false"/>
          <div class="fields">
            <s:hidden name="next" value="%{next}" />
            <s:textfield type="email" label="Email" id="email" name="email" value="%{email}" labelSeparator=" :" labelPosition="left" placeholder="Adresse e-mail" title="Adresse e-mail" required="true" cssClass="login-username"/>
            <s:password label="Mot de passe" name="password" labelSeparator=" :" labelPosition="left" required="true" cssClass="login-password" placeholder="Mot de passe" title="Mot de passe"/>
            <a href="" onclick="requestForgottenPassword('<s:property value='next'/>');return false;">Mot de passe oublié ?</a>
          </div>
          <input type="submit" class="btn-primary" value="Connexion"/>
        </form>
      </div>
  </body>
</html>
