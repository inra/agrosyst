<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Performances réalisées</title>
     <content tag="current-category">performances</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/performances/performances-list.js' /><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript">
         angular.module('PerformancesList', ['Agrosyst'])
         .value("PerformancesListData", {
           performanceFilter: <s:property value="toJson(performanceFilter)" escapeHtml="false"/>,
           performances: <s:property value="toJson(performances)" escapeHtml="false"/>,
           practiced: <s:property value="practiced" escapeHtml="false"/>
         });
     </script>
  </head>
  <body>
    <div ng-app="PerformancesList" ng-controller="PerformancesListController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Performances <s:if test="practiced">synthétisées</s:if><s:else>réalisées</s:else></li>
        </ul>
      </div>

      <ul class="actions">
        <li>
          <a class="action-ajouter" href="<s:property value="baseUrl" />/nova/#/<s:property value="currentUserLanguage" />/performances/new?practiced=<s:property value="practiced" />">Créer une nouvelle performance</a>
        </li>
      </ul>

      <ul class="float-right actions">
        <li><a class="action-desactiver" ng-class="{'button-disabled': (selectedPerformances|toSelectedLength) == 0}"
            onclick="performanceDelete($('#confirmDeletePerformances'), $('#performanceListForm'))">Supprimer</a></li>
      </ul>
  
      <form method="post" id="performanceListForm">
        <input type="hidden" name="performanceIds" value="{{selectedPerformances|toSelectedArray}}" />
        <s:hidden name="practiced" />
        <table class="entity-list clear fixe_layout">
          <thead>
            <tr class="doubleHeight">
              <th scope="col" class="column-tiny"></th>
              <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('PERFORMANCE')">
                <sort>
                  <header_label>Performance</header_label>
                  <em ng-if="filter.sortedColumn == 'PERFORMANCE'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.PERFORMANCE, 'fa fa-sort-amount-desc ':sortColumn.PERFORMANCE}" ></em>
                  <em ng-if="filter.sortedColumn != 'PERFORMANCE'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-xxslarge-fixed">Domaines</th>
              <th scope="col" class="column-xlarge-fixed">Systèmes de culture</th>
              <s:if test="!practiced">
                <th scope="col" class="column-xlarge-fixed">Parcelles</th>
                <th scope="col" class="column-xlarge-fixed">Zones</th>
              </s:if>
              <th scope="col" class="column-small" ng-click="changeSort('DATE')">
                <sort>
                  <header_label>Date de calcul</header_label>
                  <em ng-if="filter.sortedColumn == 'DATE'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DATE, 'fa fa-sort-amount-desc ':sortColumn.DATE}" ></em>
                  <em ng-if="filter.sortedColumn != 'DATE'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-small">Action</th>
            </tr>
            <tr>
              <td class="column-tiny"></td>
              <td><input type="text" ng-model="filter.performanceName" /></td>
              <td><input type="text" ng-model="filter.domainName" /></td>
              <td><input type="text" ng-model="filter.growingSystemName" /></td>
              <s:if test="!practiced">
                <td><input type="text" ng-model="filter.plotName" /></td>
                <td><input type="text" ng-model="filter.zoneName" /></td>
              </s:if>
              <td class="column-normal"></td>
              <td class="column-small"></td>
            </tr>
          </thead>

          <tbody>
            <tr ng-show="performances.length == 0"><td colspan="<s:if test='practiced'>6</s:if><s:else>8</s:else>" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>
            <tr ng-repeat="performance in performances" ng-click="selectedPerformances[performance.topiaId] = !selectedPerformances[performance.topiaId]"
                  ng-class="{'line-selected':selectedPerformances[performance.topiaId]}">
              <td class="column-tiny">
                <input type='checkbox' ng-checked="selectedPerformances[performance.topiaId]" />
              </td>
              <td class="column-xlarge-fixed">
                <a href="<s:property value="baseUrl" />/nova/#/<s:property value="currentUserLanguage" />/performances/{{performance.topiaId|encodeURIComponent}}?practiced=<s:property value="practiced" />">{{performance.name}}</a>
              </td>
              <td class="column-xxslarge-fixed">
                <span ng-repeat="domain in performance.domains.elements">
                  <a  href="<s:url namespace='/domains'
                      action='domains-edit-input'/>?domainTopiaId={{domain.topiaId|encodeURIComponent}}">{{domain.name}} ({{domain.campaign}})<span ng-if='!domain.active' class='unactivated'>&nbsp;(inactif)</span></a>{{$last?'':', '}}
                </span>
                <span ng-if="performance.domains && performance.domains.count > performance.domains.elements.length">... ({{performance.domains.count}} domaines au total)</span>
              </td>
              <td class="column-xlarge-fixed" >
                <span ng-repeat="growingSystem in performance.growingSystems.elements">
                  <a href="<s:url namespace='/growingsystems'
                    action='growing-systems-edit-input'/>?growingSystemTopiaId={{growingSystem.topiaId|encodeURIComponent}}">{{growingSystem.name}}<span ng-if='!growingSystem.active' class='unactivated'>&nbsp;(inactif)</span></a>{{$last?'':', '}}
                </span>
                <span ng-if="performance.growingSystems && performance.growingSystems.count > performance.growingSystems.elements.length">... ({{performance.growingSystems.count}} systèmes de culture au total)</span>
              </td>
              <s:if test="!practiced">
                <td class="column-xlarge-fixed">
                  <span ng-repeat="plot in performance.plots.elements">
                    <a href="<s:url namespace='/plots'
                       action='plots-edit-input'/>?plotTopiaId={{plot.topiaId|encodeURIComponent}}">{{plot.name}}<span ng-if='!plot.active' class='unactivated'>&nbsp;(inactif)</span></a>{{$last?'':', '}}
                  </span>
                  <span ng-if="performance.plots && performance.plots.count > performance.plots.elements.length">... ({{performance.plots.count}} parcelles au total)</span>
                </td>
                <td class="column-xlarge-fixed">
                  <span ng-repeat="zone in performance.zones.elements">
                    <a href="<s:url namespace='/plots'
                       action='plots-edit-input'/>?plotTopiaId={{zone.plot.topiaId|encodeURIComponent}}">{{zone.name}}<span ng-if='!zone.active' class='unactivated'>&nbsp;(inactif)</span></a>{{$last?'':', '}}
                </span>
                <span ng-if="performance.zones && performance.zones.count > performance.zones.elements.length">... ({{performance.zones.count}} zones au total)</span>
                </td>
              </s:if>
              <td class="column-small">
                {{performance.updateDate|date:'dd/MM/yyyy à HH:mm'}}
              </td>
              <td class="column-small center" ng-switch="performance.computeStatus">
                <span ng-switch-when="NONE" title="Sauvegarder à nouveau la performance pour regénérer le fichier">Non généré</span>
                <span ng-switch-when="GENERATING">En cours de génération</span>
                <span ng-switch-when="FAILED">Échec de génération</span>
                <a href="performances-download.action?performanceTopiaId={{performance.topiaId}}" ng-switch-when="SUCCESS"><em class='fa fa-download' title="Télécharger"></em></a>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="<s:if test='practiced'>6</s:if><s:else>8</s:else>">
                <div class="table-footer">
                  <div class="entity-list-info">
                    <span class="counter">{{pager.count}} performances</span>
                    <span class="selection">
                      - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucune sélectionnée', '1': '{} sélectionnée', 'other': '{} sélectionnées'}" />
                    </span>
                  </div>
                  <div class="pagination">
                    <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                  </div>
                </div>
              </td>
            </tr>
          </tfoot>
        </table>
      </form>
      
      <div id="confirmDeletePerformances" title="Suppression de performances" class="auto-hide">
        Êtes-vous sûr(e) de vouloir supprimer
        <ng-pluralize count="(selectedPerformances|toSelectedLength)"
             when="{'one': 'la performance {{firstSelectedPerformance.name}}',
                     'other': 'les {} performances sélectionnées'}"></ng-pluralize> ?
      </div>

    </div>
  </body>
</html>
