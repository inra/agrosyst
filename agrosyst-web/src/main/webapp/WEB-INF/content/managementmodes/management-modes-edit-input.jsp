<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2024 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <link rel="stylesheet" media="print and (orientation:landscape) and (color)" href="<s:url value='/nuiton-js/layout.css' /><s:property value='getVersionSuffix()'/>" type="text/css"/>
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />
    <s:if test="managementMode.topiaId == null">
      <title>Nouveau modèle décisionnel</title>
    </s:if>
    <s:else>
      <title>Modèle décisionnel</title>
    </s:else>
    <content tag="current-category">management</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/managementModes.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script>
    angular.module('ManagementModesEdit', ['Agrosyst', 'ngSanitize', 'ui.select', 'ui.switch'])
     .value('ManagementModesInitData', {
       managementModeCategory:<s:property value="toJson(managementModeCategory)" escapeHtml="false"/>,
       managementModeTopiaId:<s:property value="toJson(managementMode.topiaId)" escapeHtml="false"/>,
       domainTopiaId: <s:property value="toJson(domainTopiaId)" escapeHtml="false"/>,
       growingSystemTopiaId: <s:property value="toJson(growingSystemTopiaId)" escapeHtml="false"/>,
       growingSystemSector: <s:property value="toJson(growingSystemsSector)" escapeHtml="false"/>,
       sections: <s:property value="toJson(sections)" escapeHtml="false"/>,
       croppingPlanEntries: <s:property value="toJson(croppingPlanEntries)" escapeHtml="false"/>,
       decisionRules: <s:property value="toJson(decisionRules)" escapeHtml="false"/>,
       availableManagementModeCategories: <s:property value="toJson(availableManagementModeCategories)" escapeHtml="false"/>,
       filteredBioAgressorTypes: <s:property value="toJson(filteredBioAgressorTypes)" escapeHtml="false"/>,
       groupesCibles: <s:property value="toJson(groupesCibles)" escapeHtml="false"/>,
       typeDEPHY: <s:property value="toJson(typeDEPHY)" escapeHtml="false"/>
     });
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("Le modèle décisionnel sur lequel vous travaillez est inactif et/ou est lié à un système de culture et/ou dispositif et/ou domaine inactif(s). Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

  </head>
  <body>
    <div ng-app="ManagementModesEdit"  ng-controller="ManagementModesEditController" class="page-content">
      <div id="filAriane">
        <ul class="clearfix noprint">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url namespace='/managementmodes' action='management-modes-list' />">Modèles décisionnels</a></li>
          <s:if test="managementMode.topiaId == null">
            <li>&gt; Nouveau modèle décisionnel</li>
          </s:if>
          <s:else>
            <li>&gt; Modèle décisionnel</li>
          </s:else>
        </ul>
      </div>

      <ul class="actions noprint">
        <li><a class="action-retour" href="<s:url action='management-modes-list' namespace='/managementmodes' />">Retour à la liste des modèles décisionnels</a></li>
      </ul>

      <ul class="float-right informations noprint">
        <s:if test="%{managementMode.topiaId != null}">
          <li>
            <span class="label">Système de culture<s:if test="!managementMode.growingSystem.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId=<s:property value='managementMode.growingSystem.topiaId'/>" title="Voir le système de culture"><s:property value="managementMode.growingSystem.name" /></a>
          </li>
          <li><span class="label">Campagne</span>
            <s:property value="managementMode.growingSystem.growingPlan.domain.campaign" /> (<s:property value="(managementMode.growingSystem.growingPlan.domain.campaign)-1" /> - <s:property value="managementMode.growingSystem.growingPlan.domain.campaign" />)
          </li>

          <s:if test="%{managementMode.growingSystem.sector != null}">
            <li><span class="label">Filière</span><s:text name="fr.inra.agrosyst.api.entities.Sector.%{managementMode.growingSystem.sector}"/></li>
          </s:if>

          <s:if test="%{managementMode.growingSystem.growingPlan != null}">
            <li>
              <span class="label">Dispositif<s:if test="!managementMode.growingSystem.growingPlan.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
              <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value='managementMode.growingSystem.growingPlan.topiaId'/>" title="Voir le dispositif"><s:property value="managementMode.growingSystem.growingPlan.name" /></a>
            </li>
            <s:if test="%{managementMode.growingSystem.growingPlan.domain != null}">
              <li>
                <span class="label">Domaine<s:if test="!managementMode.growingSystem.growingPlan.domain.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
                <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='managementMode.growingSystem.growingPlan.domain.topiaId'/>" title="Voir le domaine"><s:property value="managementMode.growingSystem.growingPlan.domain.name" /></a>
              </li>
            </s:if>
          </s:if>
          <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='managementMode.topiaId' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(managementMode.topiaId)" /></a></li>
        </s:if>
      </ul>

      <s:if test="%{managementMode.topiaId != null}">
        <div class="horizontalBanner noprint">
          <div>
            <span class="timeline-title">Modèles décisionnels du système de culture&nbsp;:&nbsp;{{managementMode.growingSystem.name}}</span>

            <ul class="timeline">
              <s:iterator value="relatedManagementModes" var="relatedManagementMode">
                <li<s:if test="#relatedManagementMode.topiaId.equals(managementMode.topiaId)"> class="selected"</s:if>>
                  <a href="<s:url namespace='/managementmodes' action='management-modes-edit-input' />?managementModeTopiaId=<s:property value='topiaId'/>">
                    {{managementModeCategories['<s:property value="category" />']}}
                  </a>
                  <s:if test="managementMode.topiaId != null && availableManagementModeCategories.size > 0 && !readOnly && managementMode.category.toString() == 'PLANNED' && activated">
                    <input type="button" class="btn" value="Créer le modèle décisionnel constaté" ng-click="showNewManagementModeForm = true" />
                  </s:if>
                </li>
              </s:iterator>
            </ul>
            <form ng-if="showNewManagementModeForm" method="POST" action="<s:url namespace='/managementmodes' action='create-new-related-management-mode' />">
              <fieldset>
                <input type="hidden" name="growingSystemTopiaId" value="<s:property value='managementMode.growingSystem.topiaId'/>"/>
                <input type="hidden" name="category" value="<s:property value='managementMode.category'/>"/>
                <div ng-if="managementMode.category == 'PLANNED'" class="sub-form clearfix">
                  <div class="sub-form-content slide-animation">
                    <div>Merci d'indiquer les raisons principales du passage au constaté</div>
                    <textarea id="managementMode-changeReasonFromPlanned" name="changeReasonFromPlanned"></textarea>
                    <div>Merci d'indiquer les principales différences par rapport au décisionnel prévu pour cette campagne</div>
                    <textarea id="managementMode-mainChangesFromPlanned" name="mainChangesFromPlanned"></textarea>
                  </div>
                </div>
                <input type="submit" class="btn" value="Valider la création"/>
              </fieldset>
            </form>
          </div>
        </div>
      </s:if>

      <form name="managementModesEditForm" action="<s:url action='management-modes-edit' namespace='/managementmodes' />"
            method="post" class="tabs clear" ag-confirm-on-exit>
        <s:actionerror cssClass="send-toast-to-js"/>
        <s:hidden name="managementModeTopiaId" />

        <ul id="tabs-management-mode-menu" class="tabs-menu clearfix noprint">
          <li class="selected"><span>Généralités</span></li>
          <li><span>Tableau décisionnel</span></li>
        </ul>

        <div id="tabs-management-mode-content" class="tabs-content">
          <!-- Généralité -->
          <div id="tab_0" class="noprint">
            <fieldset>
  
              <s:if test="managementMode.persisted">
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label>Système de culture&nbsp;:</label>
                  </span>
                  <span class="wwctrl generated-content">
                    <span class='contextual-help'>
                      <span class='help-hover'>
                        <s:text name="help.managementModes.growingSystems" />
                      </span>
                    </span>
                    <s:property value="managementMode.growingSystem.name" /> (<s:property value="managementMode.growingSystem.growingPlan.domain.campaign" />)
                  </span>
                </div>
              </s:if>
              <s:else>
                <div id="wwgrp_growingSystemTopiaId" class="wwgrp">
                  <s:fielderror fieldName="growingSystemTopiaId" />
                  <span id="wwlbl_growingSystemTopiaId" class="wwlbl">
                    <label for="growingSystemTopiaId" class="label">
                      <span class="required">*</span>
                      Système de culture&nbsp;:
                    </label>
                  </span>
                  <span id="wwctrl_growingSystemTopiaId" class="wwctrl">
                    <span class='contextual-help'>
                      <span class='help-hover'>
                        <s:text name="help.managementModes.growingSystems" />
                      </span>
                    </span>
                    <s:hidden name="growingSystemTopiaId"  value="{{growingSystemTopiaId}}"/>
                    <select name="growingSystemTopiaIdToDephy" id="growingSystemTopiaId" ng-model="growingSystemTopiaIdToDephy" required>
                      <option value=""></option>
                      <s:iterator value="growingSystems">
                        <option value="${topiaId};${growingPlan.type};${sector}"
                          <s:if test="growingSystemTopiaId == topiaId"> selected="selected" </s:if> >
                          ${name} (${growingPlan.domain.campaign})
                        </option>
                      </s:iterator>
                    </select>
                  </span>
                </div>
              </s:else>

              <div class="wwgrp">
                <s:fielderror fieldName="managementMode.category" />
                <span class="wwlbl"><label for="category"><span class="required" ng-if="!managementMode.topiaId">*</span> Catégorie&nbsp;</label></span>
                <span class="wwctrl" ng-if="!managementMode.topiaId">
                  <select id="category" name="managementMode.category" ng-model="managementMode.category" required>
                    <option value=""></option>
                    <option ng-repeat="availableManagementModeCategory in availableManagementModeCategories"
                            value="{{availableManagementModeCategory}}"
                            ng-selected="managementMode.category == availableManagementModeCategory">
                      {{managementModeCategories[availableManagementModeCategory]}}
                    </option>
                  </select>
                </span>
                <span class="wwctrl generated-content" ng-if="managementMode.topiaId">
                  {{managementModeCategories[managementMode.category]}}
                </span>
              </div>

              <s:textarea name="managementMode.mainChanges" label="Principaux changements depuis les campagnes précédentes" labelSeparator=" :" />
              <s:textarea name="managementMode.changeReason" label="Motifs de changement" labelSeparator=" :" />
              <div ng-if="managementMode.category == 'OBSERVED'">
                <s:textarea name="managementMode.mainChangesFromPlanned" label="Principales différences par rapport au décisionnel prévu pour cette campagne" labelSeparator=" :" />
                <s:textarea name="managementMode.changeReasonFromPlanned" label="Motifs de ces différences" labelSeparator=" :" />
              </div>

              <div class="wwgrp">
                <span class="wwlbl">
                  <label class="historicLabelStyle" for="sectionTypeField"> Historique des changements&nbsp;:</label>
                </span>
                <span class="wwctrl generated-content management-mode-history">
                  <div class="scrollable-table">
                    <table class="data-table no-thead" >
                      <s:iterator value="histories" var="history">
                        <tr><td><s:property value="history" escapeHtml="false"/></td></tr>
                      </s:iterator>
                      <s:if test="historySizeLimit == histories.size()">
                        <tr class="empty-table"><td>La liste est limitée aux <s:property value="historySizeLimit" /> derniers changements...</td></tr>
                      </s:if>
                    </table>
                  </div>
                </span>
              </div>
            </fieldset>
          </div>

          <!-- Tableau décisionnel -->
          <div id="tab_1" class="clearfix" ng-controller="ManagementModesSectionController">
            <input type="hidden" name="sections" value="{{sections}}" />
            <div class= "infoLine block"
                 style="margin-top: 20px;"
                 ng-if="growingSystemSector === 'MARAICHAGE' || growingSystemSector === 'HORTICULTURE' || growingSystemSector === 'CULTURES_TROPICALES'">
              <div class="endLineEvidence">
                <div style="cursor:pointer"
                    class="textIcones blue x2"
                    title="Information importante">!</div>
              </div>
              <a href="<s:url value='/help/bioagg_cultures_spe.pdf' />"
                 target="_blank" rel="noopener noreferrer" class="black-link"
                 style="color: #013f4e;"
                 title="Cliquez sur ce bouton pour télécharger la liste des bio-agresseurs à saisir si présents dans le SDC">
                 Liste des bioagresseurs à saisir si présents dans le SDC&nbsp;:&nbsp;
                 <em class="fa fa-download" aria-hidden="true"></em>
              </a>
            </div>
            <div ng-repeat="(key, value) in sectionTypes">
              <div id="sectionTypes_{{key}}" ng-if="(sections|filter:{sectionType:key}).length > 0" class="section">
                <legend class="font-bold">{{sectionTypes[key]}}</legend>
                <span>
                  <%@include file="management-modes-edit-section-table-input.jsp" %>
                </span>
              </div>
            </div>

            <div class="sub-form slide-animation noprint">
              <div ng-if="growingSystemTopiaId" class="clear button-open paddingTop20 ng-scope">
                <a ng-click="addSection()" class="btn btn-darker">Ajouter une rubrique</a>
              </div>
              <div ng-if="!growingSystemTopiaId" class="clear button-open paddingTop20 ng-scope">
                Merci de renseigner un système de culture
              </div>
              <div class="sub-form-side-content" ng-show="editedSection">
                <a ng-click="removeSection()" class="btn btn-darker float-right">Supprimer cette rubrique</a>
                <div class="noborder">

                  <div class="wwgrp">
                    <!-- <s:fielderror fieldName="editedSection.sectionType" /> -->
                    <span class="wwlbl">
                      <label for="sectionTypeField" class="historicLabelStyle"><span class="required">*</span> Type de rubrique&nbsp;:</label>
                    </span>
                    <span class="wwctrl">
                      <select id="sectionTypeField" ng-model="sectionType"
                              ng-change="sectionTypeChange(editedSection, sectionType)"
                              ng-required="editedSection">
                        <option value=""></option>
                        <option ng-repeat="sectionType in sectionTypesOrdered" value="{{sectionType}}">{{sectionTypes[sectionType]}}</option>
                      </select>
                    </span>
                  </div>

                  <div ng-if="editedSection.sectionType">

                    <div ng-show="editedSection.sectionType === 'ADVENTICES' || editedSection.sectionType === 'MALADIES' || editedSection.sectionType === 'RAVAGEURS'">
                      <div class="wwgrp" ng-show="editedSection.sectionType != 'ADVENTICES'">
                        <span class="wwlbl">
                          <label for="groupeCibleField" class="historicLabelStyle">Groupe cible&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <span class='contextual-help' style='margin-right: 5%;'>
                            <span class='help-hover'>
                              <s:text name="help.report.groupeCibleMaa" />
                            </span>
                          </span>
                          <ui-select ng-model="editedSection.codeGroupeCibleMaa"
                                     id="groupeCibleField"
                                     reset-search-input="true"
                                     theme="select2"
                                     style="width: 300px;"
                                     on-select="groupeCibleSelected($item)">
                            <ui-select-match placeholder="ex. : Champignons">{{$select.selected.groupeCibleMaa}}</ui-select-match>
                            <ui-select-choices repeat="groupeCible.codeGroupeCibleMaa as groupeCible in groupesCibles | filter : isGroupeCibleInCategory | filter: {groupeCibleMaa: $select.search}">
                              <span ng-bind-html="groupeCible.groupeCibleMaa | highlight: $select.search"></span>
                            </ui-select-choices>
                          </ui-select>

                        </span>
                      </div>

                      <div class="wwgrp">
                          <span class="wwlbl">
                            <label for="bioAggressorField"  class="historicLabelStyle">
                              <a href="<s:url namespace='/referential' action='management-mode-bio-aggressors-download'/>"
                                 style="color: black; font-size:20px; padding: 0 5px"
                                 title="Cliquez sur ce bouton pour télécharger la liste des bio-agresseurs">
                                <em class="fa fa-download" aria-hidden="true"></em>
                              </a>
                              <span class="required" ng-if="editedSection && notDephyExpe() && (editedSection.sectionType === 'ADVENTICES' || editedSection.sectionType === 'MALADIES' || editedSection.sectionType === 'RAVAGEURS')">*</span> Bio-agresseur considéré&nbsp;:
                              <span ng-if="!editedSection.bioAgressorTopiaId && notDephyExpe() && (editedSection.sectionType === 'ADVENTICES' || editedSection.sectionType === 'MALADIES' || editedSection.sectionType === 'RAVAGEURS')">
                                <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
                              </span>
                              </label>
                          </span>
                          <span class="wwctrl">

                            <ui-select ng-model="editedSection.bioAgressorTopiaId"
                                       id="bioAggressorField"
                                       reset-search-input="true"
                                       theme="select2"
                                       style="width: 300px;"
                                       ng-required="editedSection && notDephyExpe() && (editedSection.sectionType === 'ADVENTICES' || editedSection.sectionType === 'MALADIES' || editedSection.sectionType === 'RAVAGEURS')"
                                       on-select="bioAggressorSelected($item)">
                              <ui-select-match placeholder="ex. : Brome">{{$select.selected.adventice || $select.selected.reference_label}}</ui-select-match>
                              <ui-select-choices repeat="bioAggressor.topiaId as bioAggressor in bioAgressors | filter: inGroupeCibleBioAgressor | filter: $select.search">
                                <span ng-bind-html="bioAggressor.adventice || bioAggressor.reference_label | highlight: $select.search"></span>
                              </ui-select-choices>
                            </ui-select>
                        </span>
                      </div>


                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="switchSdcConcerne" class="historicLabelStyle"> SdC concerné par le bioagresseur&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <switch name="switchSdcConcerne" ng-model="editedSection.sdcConcerneBioAgresseur" class="smaller" on="Oui" off="Non"/>
                        </span>
                      </div>
                    </div>

                    <div ng-if="editedSection.sectionType === 'MAITRISE_DES_DOMMAGES_PHYSIQUES'">
                      <div class="wwgrp">
                        <span class="wwlbl"><label for="damageType" class="label historicLabelStyle"> Dommage considéré&nbsp;:</label></span>

                        <span class="wwctrl">
                          <select id="damageType" ng-model="editedSection.damageType"
                                  ng-options="key as value for (key, value) in damageTypes">
                            <option label="" value="" />
                          </select>
                        </span>
                      </div>
                    </div>

                    <div class="wwgrp" ng-if="growingSystemSector !== 'VITICULTURE'">
                      <span class="wwlbl"><label for="agronomicObjectiveField"  class="historicLabelStyle">Objectifs agronomiques&nbsp;:</label></span>
                      <span class="wwctrl">
                        <textarea id="agronomicObjectiveField" ng-model="editedSection.agronomicObjective" rows="5" cols="50"
                        placeholder="ex. : Tolérance de dégats et dommages de récolte dans la limite de l'obtention du stock fourager d'herbe et de maïs ensilage"></textarea>
                      </span>
                    </div>

                    <div class="wwgrp">
                      <span class="wwlbl">
                        <label for="categoryObjectiveField"  class="historicLabelStyle">
                        <span class="required" ng-if="editedSection && notDephyExpe()">*</span>
                        Caractérisation des objectifs agronomiques (niveau de tolérance)&nbsp;:</label>
                      </span>
                      <span class="wwctrl">
                        <select
                            id="categoryObjectiveField"
                            ng-model="editedSection.categoryObjective"
                            ng-options="key as value for (key, value) in i18n['CategoryObjective#' + sectionType]"
                            ng-required="editedSection && notDephyExpe()">
                            <option value=""></option>
                        </select>
                      </span>
                    </div>
                    <div class="wwgrp">
                      <span class="wwlbl"><label for="expectedResultField" class="historicLabelStyle">Résultats attendus&nbsp;:</label></span>
                      <span class="wwctrl">
                        <textarea id="expectedResultField" ng-model="editedSection.expectedResult" rows="5" cols="50"
                        placeholder="ex. : Maïs ensilage : 12t de MS/ha"></textarea>
                      </span>
                    </div>

                    <div>
                      <label class="historicLabelStyle">
                        <span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Liste des leviers&nbsp;:
                      </label>

                      <div class="input-fields">
                        <table class="data-table full-width">
                          <thead>
                            <tr>
                              <th scope="col"/>
                              <th scope="col">Leviers</th>
                              <th scope="col">Explication</th>
                              <th scope="col">Culture</th>
                              <th scope="col" class="column-xsmall">Suppr.</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-show="editedSection.strategiesDto.length == 0"><td colspan="6" class="empty-table">Aucune stratégie. Vous pouvez créer de nouvelles stratégies en cliquant sur "Ajouter&nbsp;une&nbsp;stratégie".</td></tr>
                            <tr ng-repeat="strategy in editedSection.strategiesDto" class="selectCursor"
                                ng-click="editStrategy(editedSection, strategy);selectedStrategies[strategy.$$hashKey] = !selectedStrategies[strategy.$$hashKey]"
                                ng-class="{'selected-line' : strategy == editedStrategy, 'line-selected':selectedStrategies[strategy.$$hashKey], 'line-error': !validateStrategy(editedSection, strategy)}">
                              <td>
                                <input type='checkbox' ng-disabled="!validateStrategy(editSection, strategy)" ng-checked="selectedStrategies[strategy.$$hashKey]" />
                              </td>
                              <td>
                                {{strategy.refStrategyLever ? getLeverLabel(strategy.refStrategyLever) : '' | orDash | truncate:50}}
                              </td>
                              <td>
                                {{(strategy.explanation|orDash|truncate:50)}}
                              </td>
                              <td>
                                <span ng-if="strategy.cropIds && strategy.cropIds.length > 0">
                                <span ng-repeat="cropId in strategy.cropIds">
                                  <span ng-if="$index < 4">
                                    {{getCroppingPlanEntryName(cropId)}}{{($index + 1) < strategy.cropIds.length ? ',&nbsp;' : ''}}
                                  </span>
                                  <span ng-if="$index === 4">
                                    ...
                                  </span>
                                  <span ng-if="$index > 4"></span>
                                </span>
                                </span>
                                <span ng-if="!strategy.cropIds || strategy.cropIds.length === 0"> - </span>
                              </td>
                              <td>
                                <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="removeStrategy(editedSection, $index)" />
                              </td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td colspan="5">
                                <div class="table-end-button">
                                  <input type="button" value="Ajouter un levier" ng-click="createStrategy(editedSection)" />
                                </div>
                                <div class="table-end-button">
                                  <input type="button" value="Copier les stratégies" ng-click="copyStrategies()"
                                    ng-show="(selectedStrategies|toSelectedLength) > 0" class="button-copy" />
                                  <input type="button" value="Coller les stratégies" ng-click="pasteStrategies()"
                                    ng-show="pastedStrategies" class="button-copy" />
                                </div>
                              </td>
                            </tr>
                          </tfoot>
                        </table>
                        <div id="addStrategy" class="sub-form slide-animation clearfix" ng-if="editedStrategy">

                          <div class="wwgrp">
                            <span class="wwlbl">
                              <label for="strategyLever" class="historicLabelStyle">
                                <a href="<s:url namespace='/referential' action='management-mode-levers-download'/>"
                                   style="color: black; font-size:20px; padding: 0 5px"
                                   title="Cliquez sur ce bouton pour télécharger la liste des leviers">
                                  <em class="fa fa-download" aria-hidden="true"></em>
                                </a>
                                <span class="required">*</span>
                                Levier&nbsp;:
                                <span ng-if="!selectedStrategyLever.lever">
                                  <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
                                </span>
                              </label>
                            </span>
                            <span id="strategyLever" class="wwctrl">
                              <ui-select ng-model="selectedStrategyLever"
                                         input-id="selectedStrategyLever"
                                         theme="select2"
                                         on-select="bindStrategyLeverData($item)"
                                         style="display:inline-block; width: 61%;"
                                         spinner-enabled="true">

                                  <ui-select-match placeholder="ex. : Réduction de dose">
                                      {{displayLeverUiSelect($select.selected)}}
                                  </ui-select-match>
                                <ui-select-choices repeat="strategyLever in strategyLevers track by $index"
                                                   refresh="refreshStrategyLevers($select.search)"
                                                   refresh-delay="200">
                                    <span ng-bind-html="getLeverLabel(strategyLever) | highlight: $select.search"></span>
                                </ui-select-choices>
                              </ui-select>
                            </span>
                            <s:fielderror fieldName="editedStrategy.refStrategyLever" />
                          </div>

                          <div class="wwgrp">
                            <!-- <s:fielderror fieldName="editedStrategy.explanation" /> -->
                            <span class="wwlbl"><label class="historicLabelStyle">Précision sur le levier&nbsp;:</label></span>
                            <span class="wwctrl">
                              <textarea ng-model="editedStrategy.explanation" rows="5" cols="50"
                                placeholder="ex. : Atténuation : semis retardé de 15 jours, densité adaptée pour eviter le surpeuplement"></textarea>
                            </span>
                          </div>

                          <div class="wwgrp">
                            <span class="wwlbl"><label for="strategyCrops" class="label historicLabelStyle"><span class="required" ng-if="editedSection && notDephyExpe()">*</span> Culture(s) concernée(s) par la stratégie&nbsp;:</label></span>
                            <span class="wwctrl list">
                              <div ng-if="editedStrategy.cropIds && editedStrategy.cropIds.length > 0">
                                <span ng-repeat="cropId in editedStrategy.cropIds" class="tag">
                                  <span>{{getCroppingPlanEntryName(cropId)}}</span>
                                  <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="removeStrategyCrop(editedStrategy, $index)"/>
                                </span>
                              </div>
                              <div>
                                <select id="strategyCrops"
                                    ng-model="croppingPlanEntry"
                                    ng-required="notDephyExpe() && (editedStrategy.cropIds && editedStrategy.cropIds.length === 0)"
                                    ng-if="strategyCroppingPlanEntries && strategyCroppingPlanEntries.length > 0"
                                    title="Ajouter une culture"
                                    ng-options="croppingPlanEntry as croppingPlanEntry.name for croppingPlanEntry in strategyCroppingPlanEntries"
                                    ng-change="addStrategyCrop(editedSection, editedStrategy, croppingPlanEntry)">
                                    <option value=""></option>
                                </select>
                              </div>
                              <div ng-if="strategyCroppingPlanEntries && strategyCroppingPlanEntries.length === 0 && editedStrategy.cropIds.length === 0">
                                Aucune culture disponible
                              </div>
                            </span>
                          </div>

                          <!-- ref #8788 Dans la fenêtre gris foncé des stratégies, masquer le champ "Nom de la conduite de culture" (ATTENTION : pas supprimer, mais uniquement masquer en attendant de voir si ce champ manque à des utilisateurs ou non).-->
                          <!--<div class="wwgrp">
                            <span class="wwlbl"><label class="historicLabelStyle">Nom de la conduite de culture&nbsp;:</label></span>
                            <span class="wwctrl">
                              <input type="text" ng-model="editedStrategy.croppingPlanManagmentName" />
                            </span>
                          </div>-->

                          <div class="wwgrp checkbox-list">
                            <span class="wwlbl"><label class="historicLabelStyle">Règles&nbsp;:</label></span>
                            <span class="wwctrl">
                              <span ng-if="editedSection.sectionType && !editedSection.bioAgressorType && strategyAvailableDecisionRules.length == 0">Aucune règle n'est applicable pour le type de rubrique
                                <strong>{{sectionTypes[editedSection.sectionType]}}</strong>.</span>
                              <span ng-if="editedSection.bioAgressorType && !editedStrategy.croppingPlanEntryId && strategyAvailableDecisionRules.length == 0">Aucune règle n'est applicable pour le type de bio agresseur
                                <strong>{{bioAgressorTypes[editedSection.bioAgressorType]}}</strong>.</span>
                              <span ng-if="editedSection.bioAgressorType && editedStrategy.croppingPlanEntryId && strategyAvailableDecisionRules.length == 0">Aucune règle n'est applicable pour le type de bio agresseur
                                <strong>{{bioAgressorTypes[editedSection.bioAgressorType]}}</strong> et la culture <strong>{{getCroppingPlanEntryName(editedStrategy.croppingPlanEntryId)}}</strong>.</span>
                              <span ng-repeat="value in strategyAvailableDecisionRulesMap">
                                <input type="checkbox" ng-checked="strategyIsSelectedRule(editedStrategy, value)"
                                  ng-click="strategySelectRule(editedStrategy, value)"
                                  id="selectRule-{{$index}}"/>
                                <label for="selectRule-{{$index}}" class="checkboxLabel" ng-mouseover="showMoreDecisionRulesInfo[$index] = true" ng-mouseleave="showMoreDecisionRulesInfo[$index] = false">{{value.name}} (version {{value.versionNumber}})</label>
                                <div class="tooltip-info" ng-if="showMoreDecisionRulesInfo[$index]">
                                  <ul>
                                    <li><span class="font-bold">{{value.name}}</span>&nbsp;<span>(version {{value.versionNumber}})</span></li>
                                    <li><span class="font-bold">Type d'intervention&nbsp;:&nbsp;</span><span>{{agrosystInterventionTypes[value.interventionType]}}</span></li>
                                    <li><span class="font-bold">Culture(s)&nbsp;:&nbsp;</span>
                                        <span ng-if="value.decisionRuleCrop && value.decisionRuleCrop.length > 0" ng-repeat="decisionRuleCrop in value.decisionRuleCrop">
                                          {{croppingPlanEntriesByCode[decisionRuleCrop.croppingPlanEntryCode].name}}
                                        </span>
                                        <span ng-if="!value.decisionRuleCrop || value.decisionRuleCrop.length === 0">-</span>
                                    </li>
                                    <li><span class="font-bold">Type de bio-agresseur&nbsp;:&nbsp;</span>{{value.bioAgressorType ? bioAgressorTypes[value.bioAgressorType] : '-'}}</li>
                                    <li><span class="font-bold">Bio-agresseur&nbsp;:&nbsp;</span>{{value.bioAgressor && value.bioAgressor.reference_label ? value.bioAgressor.reference_label : '-'}}</li>
                                  </ul>
                                </div>
                              </span>
                              <input ng-if="managementMode.growingSystem || growingSystemTopiaId" type="button" value="Créer une nouvelle règle de décision" ng-click="showLightoxCreateNewDecisionRules()"/>
                            </span>
                          </div>

                          <div id="decision-rules-edit-lightbox" class="slide-animation dialog-form auto-hide">
                            <div class="wwgrp">
                              <span class="wwlbl"><label for="new-decision-rule-name"  class="historicLabelStyle">Nom de la règle de décision&nbsp;:</label></span>
                              <span class="wwctrl">
                                <input id="new-decision-rule-name" type="text" ng-model="decisionRule.name" />
                              </span>
                              <div class="wwgrp">
                                <span class="wwlbl"><label for="intervention_type"  class="historicLabelStyle"><span class="required">*</span> Type d'intervention&nbsp;</label></span>
                                <span class="wwctrl">
                                  <select id="intervention_type" ng-model="decisionRule.interventionType"
                                          ng-options="key as value for (key, value) in agrosystInterventionTypes"
                                          ng-required="editedIntervention">
                                  </select>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="confirm-lightBox" title="Attention" class="slide-animation dialog-form auto-hide">
              {{message}}
            </div>
          </div>

          <span class="noprint">
            <span class="form-buttons">
              <a class="btn-secondary" href="<s:url namespace='/managementmodes' action='management-modes-list' />">Annuler</a>
              <input type="submit" class="btn-primary" value="Enregistrer"
                <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
                <s:if test="!activated">disabled="disabled" title="Le modèle décisionnel sur lequel vous travaillez est inactif et/ou est lié à un système de culture et/ou dispositif et/ou domaine inactif(s). Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
              />
            </span>
          </span>
        </div>

      </form>

    </div>
  </body>
</html>
