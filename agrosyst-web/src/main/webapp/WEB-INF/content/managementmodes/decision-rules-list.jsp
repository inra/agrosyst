<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Règles de décision</title>
     <content tag="current-category">management</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/managementModes.js' /><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript">
         angular.module('DecisionRulesList', ['Agrosyst'])
            .value('DecisionRulesInitData', <s:property value="toJson(decisionRules)" escapeHtml="false"/>)
            .value('decisionRuleFilter', <s:property value="toJson(decisionRuleFilter)" escapeHtml="false"/>)
            .value('decisionRulesExportAsyncThreshold', <s:property value="decisionRulesExportAsyncThreshold"/>);
     </script>
  </head>
  <body>
    <div ng-app="DecisionRulesList" ng-controller="DecisionRulesListController">
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Règles de décision</li>
      </ul>
    </div>

    <ul class="actions">
      <li>
        <a class="action-ajouter" href="<s:url namespace='/managementmodes' action='decision-rules-edit-input' />">Créer une règle de décision</a>
      </li>
    </ul>
    
    <ul class="float-right actions">
      <li><a class="action-desactiver" ng-class="{'button-disabled':(selectedEntities|toSelectedLength)== 0}"
        onclick="decisionRulesUnactivate($('#confirmUnactivateDecisionRules'), $('#decisionRulesListForm'), false);return false;" ng-if="allSelectedDecisionRuleActive">Désactiver</a>
        <a class="action-activer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
        onclick="decisionRulesUnactivate($('#confirmUnactivateDecisionRules'), $('#decisionRulesListForm'), true);return false;" ng-if="!allSelectedDecisionRuleActive">Activer</a></li>
      <li><a class="action-dupliquer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) != 1}"
        onclick="decisionRulesDuplicate($('#confirmDuplicateDecisionRules'), $('#decisionRulesListForm'));return false;">Dupliquer</a></li>
      <%-- <li><a class="action-import button-disabled">Import</a></li>
      <li><a class="action-export-pdf button-disabled">Export PDF</a></li>--%>
      <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
             onclick="decisionRulesExport($('#decisionRulesListForm'));return false;">Export XLS</a></li>
      <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
             ng-click="asyncDecisionRulesExport()">Export XLS</a></li>
    </ul>

    <form method="post" id="decisionRulesListForm">
      <input type="hidden" name="decisionRuleIds" value="{{selectedEntities|toSelectedArray}}" />

      <table class="entity-list clear">
        <thead>
          <tr>
            <th scope="col" class="column-tiny"
                title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
            </th>
            <th scope="col" class="column-xxlarge-fixed" ng-click="changeSort('DECISION_RULE')">
              <sort>
                <header_label>Règle de décision</header_label>
                <em ng-if="filter.sortedColumn == 'DECISION_RULE'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DECISION_RULE, 'fa fa-sort-amount-desc ':sortColumn.DECISION_RULE}" ></em>
                <em ng-if="filter.sortedColumn != 'DECISION_RULE'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xxlarge-fixed" ng-click="changeSort('DOMAIN')">
              <sort>
                <header_label>Exploitation ou Domaine expérimental</header_label>
                <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col">Intervention concernée</th>
            <th scope="col" class="column-xsmall">État</th>
          </tr>
          <tr>
            <td></td>
            <td><input type="text" ng-model="filter.decisionRuleName" /></td>
            <td><input type="text" ng-model="filter.domainName" /></td>
            <td>
              <select ng-model="filter.interventionType">
                <option value=""></option>
                <s:iterator value="agrosystInterventionTypes">
                  <option value='<s:property value="key.name()" />'><s:property value="value" /></option>
                </s:iterator>
              </select>
            </td>
            <td>
              <select ng-model="filter.active">
                <option value="" />
                <option value="true">Actif</option>
                <option value="false">Inactif</option>
              </select>
            </td>
          </tr>
        </thead>
  
        <tbody>
          <tr ng-show="decisionRules.length == 0"><td colspan="5" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>
          <tr ng-repeat="decisionRule in decisionRules"
              ng-class="{'line-selected':selectedEntities[decisionRule.topiaId]}">
            <td>
              <input type='checkbox' ng-model="selectedEntities[decisionRule.topiaId]" ng-checked="selectedEntities[decisionRule.topiaId]" ng-click="toggleSelectedEntity(decisionRule.topiaId)"/>
            </td>
            <td>
              <a href="<s:url namespace='/managementmodes' action='decision-rules-edit-input'/>?decisionRuleCode={{decisionRule.code|encodeURIComponent}}">{{decisionRule.name}}<span ng-if="!decisionRule.active" class="unactivated">&nbsp;(inactif)</span></a>
              (version {{decisionRule.versionNumber}})
            </td>
            <td>{{decisionRule.domain.name}}<span ng-if="!decisionRule.domain.active" class="unactivated">&nbsp;(inactif)</span></td>
            <td>{{agrosystInterventionTypes[decisionRule.interventionType]}}</td>
            <td class="column-small">{{decisionRule.active ? 'Active' : 'Inactive'}}</td>
          </tr>
        </tbody>
        
        <tfoot>
          <tr>
            <td colspan="5">
              <div class="table-footer">
                <div class="entity-list-info">
                  <span class="counter">{{pager.count}} règles de décision</span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner toutes les règles de décision</a>
                  </span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                  </span>
                  <span class="selection">
                    - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucune sélectionnée', '1': '{} sélectionnée', 'other': '{} sélectionnées'}" />
                  </span>
                </div>
                <div class="pagination">
                  <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                </div>
              </div>
            </td>
          </tr>
        </tfoot>
      </table>
    </form>

    <div id="confirmUnactivateDecisionRules" title="Activer/Désactiver des règles de décision" class="auto-hide">
      Êtes-vous sûr(e) de vouloir {{allSelectedDecisionRuleActive?'désactiver':'activer'}}
      <ng-pluralize count="(_selectedEntities|toSelectedLength)"
           when="{'one': 'la règle {{firstSelectedDecisionRule.name}}',
                   'other': 'les {} règles sélectionnées'}"></ng-pluralize> ?
    </div>
    
    <div id="confirmDuplicateDecisionRules" title="Dupliquer une règle de décision" class="auto-hide">
      Êtes-vous sûr(e) de vouloir dupliquer la règle de décision : {{firstSelectedDecisionRule.name}} ?
    </div>

    </div>
  </body>
</html>
