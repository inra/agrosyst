<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Modèles décisionnels</title>
     <content tag="current-category">management</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/managementModes.js' /><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript">
         angular.module('ManagementModesList', ['Agrosyst'])
            .value('ManagementModesInitData', <s:property value="toJson(managementModeDtos)" escapeHtml="false"/>)
            .value('managementModeFilter', <s:property value="toJson(managementModeFilter)" escapeHtml="false"/>)
            .value('managementModesExportAsyncThreshold', <s:property value="managementModesExportAsyncThreshold"/>);
     </script>
  </head>
  <body>
    <div ng-app="ManagementModesList" ng-controller="ManagementModesListController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Modèles décisionnels</li>
        </ul>
      </div>

      <ul class="actions">
        <li>
          <a class="action-ajouter" href="<s:url namespace='/managementmodes' action='management-modes-edit-input' />">Créer un modèle décisionnel</a>
        </li>
      </ul>
    
      <ul class="float-right actions">
        <li><a class="action-desactiver" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
               ng-click="managementModesDelete()">Supprimer</a></li>
        <li>
          <a ng-if="(toManagementModeSelectedLength() != 1)"
             class="action-dupliquer"
             ng-class="{'button-disabled':true}">Dupliquer</a>
          <a ng-if="(toManagementModeSelectedLength() == 1)"
             class="action-dupliquer"
             ng-class="{'button-disabled':false}"
             ng-click="showLightBoxDuplicateManagementMode()">Dupliquer</a>
        </li>
        <%-- <li><a class="action-import button-disabled">Import</a></li>
        <li><a class="action-export-pdf button-disabled">Export PDF</a></li>--%>
        <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
               onclick="managementModesExport($('#managementModesListForm'));return false;">Export XLS</a></li>
        <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
               ng-click="asyncManagementModesExport()">Export XLS</a></li>
      </ul>

      <form method="post" id="managementModesListForm">
        <s:actionerror cssClass="send-toast-to-js"/>
        <input type="hidden" ng-if="forManagementModeDuplication.plannedManagementModeSelected" name="plannedManagementModeId" value="{{forManagementModeDuplication.plannedManagementModeId}}"/>
        <input type="hidden" ng-if="forManagementModeDuplication.observedManagementModeSelected " name="observedManagementModeId" value="{{forManagementModeDuplication.observedManagementModeId}}"/>
        <input type="hidden" name="growingSystemId" value="{{forManagementModeDuplication.growingSystemIdForDuplication}}"/>
        <input id="managementModeDtoMap" type="hidden" name="managementModeDtoMap" value="{{selectedEntities}}"/>
        <input id="selectedManagementModesToDelete" type="hidden" name="selectedManagementModesToDelete" value="{{selectedManagementModesToDelete}}"/>

        <table class="entity-list clear fixe_layout" id="management-modes-list-table">
          <thead>
            <tr>
              <th scope="col" class="column-tiny"
                  title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                  <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
              </th>
              <th scope="col" colspan="2" class="column-small">Modèle décisionnel</th>
              <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_SYSTEM')">
                <sort>
                  <header_label>Système de culture</header_label>
                  <em ng-if="filter.sortedColumn == 'GROWING_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.GROWING_SYSTEM}" ></em>
                  <em ng-if="filter.sortedColumn != 'GROWING_SYSTEM'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_PLAN')">
                <sort>
                  <header_label>Dispositif</header_label>
                    <em ng-if="filter.sortedColumn == 'GROWING_PLAN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_PLAN, 'fa fa-sort-amount-desc ':sortColumn.GROWING_PLAN}" ></em>
                    <em ng-if="filter.sortedColumn != 'GROWING_PLAN'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('DOMAIN')">
                <sort>
                  <header_label>Exploitation ou Domaine expérimental</header_label>
                  <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                  <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-small center" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne">
                <sort>
                  <header_label>Camp.</header_label>
                  <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                  <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
                </sort>
              </th>
            </tr>
            <tr>
              <td></td>
              <td colspan="2"></td>
              <td class="column-xlarge-fixed"><input type="text" ng-model="filter.growingSystemName" /></td>
              <td class="column-xlarge-fixed"><input type="text" ng-model="filter.growingPlanName" /></td>
              <td class="column-xxslarge-fixed"><input type="text" ng-model="filter.domainName" /></td>
              <td class="column-small"><input type="text" ng-model="filter.campaign" /></td>
            </tr>
          </thead>

          <tbody>
            <tr ng-show="managementModeDtos.length == 0"><td colspan="7" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>
            <tr ng-repeat="managementModeDto in managementModeDtos"
                ng-class="{'line-selected':selectedEntities[managementModeDto.growingSystem.topiaId]}">
              <td>
                <input type='checkbox' ng-model="selectedEntities[managementModeDto.growingSystem.topiaId]" ng-checked="selectedEntities[managementModeDto.growingSystem.topiaId]" ng-click="toggleSelectedEntity(managementModeDto)"/>
              </td>
              <td class="column-small">
                <a ng-if="managementModeDto.plannedManagementModeId" href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{managementModeDto.plannedManagementModeId|encodeURIComponent}}">Prévu</a>
                <span ng-if="!managementModeDto.plannedManagementModeId">-</span>
              </td>
              <td class="column-small">
                <a ng-if="managementModeDto.observedManagementModeId" href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{managementModeDto.observedManagementModeId|encodeURIComponent}}">Constaté</a>
                <span ng-if="!managementModeDto.observedManagementModeId">-</span>
              </td>
              <td class="column-xlarge-fixed" title="{{managementModeDto.growingSystem.name}}">
                <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId={{managementModeDto.growingSystem.topiaId|encodeURIComponent}}">{{managementModeDto.growingSystem.name}}<span ng-if="!managementModeDto.growingSystem.active" class="unactivated">&nbsp;(inactif)</span></a>
              </td>
              <td class="column-xlarge-fixed" title="{{managementModeDto.growingSystem.growingPlan.name}}">
                <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId={{managementModeDto.growingSystem.growingPlan.topiaId|encodeURIComponent}}">{{managementModeDto.growingSystem.growingPlan.name}}<span ng-if="!managementModeDto.growingSystem.growingPlan.active" class="unactivated">&nbsp;(inactif)</span></a>
              </td>
              <td class="column-xxslarge-fixed" title="{{managementModeDto.growingSystem.growingPlan.domain.name}}">
                <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId={{managementModeDto.growingSystem.growingPlan.domain.topiaId|encodeURIComponent}}">{{managementModeDto.growingSystem.growingPlan.domain.name}}<span ng-if="!managementModeDto.growingSystem.growingPlan.domain.active" class="unactivated">&nbsp;(inactif)</span></a>
              </td>
              <td class="column-small center"
                  title="{{managementModeDto.growingSystem.growingPlan.domain.campaign}} ({{managementModeDto.growingSystem.growingPlan.domain.campaign-1}} - {{managementModeDto.growingSystem.growingPlan.domain.campaign}}">
                {{managementModeDto.growingSystem.growingPlan.domain.campaign}}
              </td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <td colspan="7">
                <div class="table-footer">
                  <div class="entity-list-info">
                    <span class="counter">{{pager.count}} systèmes de culture avec des modèles décisionnels</span>
                    <span>
                      - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les modèles décisionnels</a>
                    </span>
                    <span>
                      - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                    </span>
                    <span class="selection">
                      - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                    </span>
                  </div>
                  <div class="pagination">
                    <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                  </div>
                </div>
              </td>
          </tfoot>
        </table>
      </form>

      <div id="confirmUnactivateManagementModes" title="Activer/Désactiver des modèle décisionnel" class="auto-hide">
        Êtes-vous sûr(e) de vouloir {{allSelectedManagementModeActive?'désactiver':'activer'}}
        <ng-pluralize count="toManagementModeSelectedLength()"
             when="{'one': 'le modèle décisionnel {{firstSelectedGrowingSystem.name}}',
                     'other': 'les {} modèles décisionnels sélectionnés'}"></ng-pluralize> ?
      </div>

      <div id="confirmDuplicateManagementMode" title="Dupliquer un modèle décisionnel" class="auto-hide">
        <div ng-if="availableGrowingSystemsForDuplication.length === 0">Aucun sytème de culture disponible pour la duplication</div>
        <div ng-if="availableGrowingSystemsForDuplication.length > 0">
          <div ng-if="forManagementModeDuplication.plannedManagementModeId &&
                      forManagementModeDuplication.observedManagementModeId">
            <div class="wwgrp">
              <span class="wwctrl">
                <input type="checkbox" id="plannedManagementMode" ng-model="forManagementModeDuplication.plannedManagementModeSelected" />
              </span>
              <span class="wwlbl"><label for="plannedManagementMode"> Dupliquer le prévu du modèle décisionnel</label></span>
            </div>
            <div class="wwgrp">
              <span class="wwctrl">
                <input type="checkbox" id="observedManagementMode" ng-model="forManagementModeDuplication.observedManagementModeSelected" />
              </span>
              <span class="wwlbl"><label for="observedManagementMode"> Dupliquer le constaté du modèle décisionnel</label></span>
            </div>
          </div><br />
          <div class="wwgrp">
            <span class="wwlbl">
              <label for="growingSystemIdForDuplication"> Dupliquer le modèle décisionnel vers le système de culture :</label>
            </span>
            <span class="wwctrl">
              <select id="growingSystemIdForDuplication"
                      ng-model="forManagementModeDuplication.growingSystemIdForDuplication"
                      ng-options="gs.topiaId as gs.name + ' (' + gs.growingPlan.domain.campaign + ')' for gs in availableGrowingSystemsForDuplication">
              </select>
            </span>
          </div>
        </div>
      </div>

      <div id="select-managements-modes-light-box" ng-show="showDeleteManagementModeLB" title="Veuillez sélectionner les modèles décisionnels à supprimer" class="slide-animation dialog-form auto-hide" style="overflow:auto;">
        <table class="entity-list clear">
          <thead>
            <tr>
              <th scope="col" colspan=2>Modèle décisionnel</th>
              <th scope="col" rowspan=2>Système de culture</th>
              <th scope="col" rowspan=2>Dispositif</th>
              <th scope="col" rowspan=2>Exploitation ou Domaine expérimental</th>
              <th scope="col" rowspan=2>Campagne</th>
            </tr>
            <tr>
              <th scope="col">prévu</th>
              <th scope="col">constaté</th>
            </tr>
          </thead>

          <tbody>
            <tr ng-if="availableManagementModesToDeletes.length !== selectedGrowingSystems.length">
              <td colspan = "6">
                <div class="wwgrp">
                  <div class="clearfix">
                    <div class="messages-panel">
                      <ul class="warningMessage">
                        <li>N'apparaissent que les modèles décisionnels pour lesquels vous possédez les droits de modification</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
            <tr ng-repeat="row in availableManagementModesToDeletes"
                ng-class="{'line-selected':selectedManagementModesToDelete[row.growingSystem.topiaId]}">
              <td class="column-small">
                <input ng-disabled="!row.plannedManagementModeId" type='checkbox'
                       title="{{row.plannedManagementModeId ? 'Sélectionner pour suppression' : 'non présent' }}"
                       ng-click="selectManagementModesToDelete(row.plannedManagementModeId)"/>
              </td>
              <td class="column-small">
                <input ng-disabled="!row.observedManagementModeId" type='checkbox'
                       title="{{row.observedManagementModeId ? 'Sélectionner pour suppression' : 'non présent' }}"
                       ng-click="selectManagementModesToDelete(row.observedManagementModeId)"/>
              </td>
              <td>
                {{row.growingSystem.name}}
              </td>
              <td>
                {{row.growingSystem.growingPlan.name}}
              </td>
              <td>
                {{row.growingSystem.growingPlan.domain.name}}
              </td>
              <td>
                {{row.growingSystem.growingPlan.domain.campaign}} ({{row.growingSystem.growingPlan.domain.campaign-1}} - {{row.growingSystem.growingPlan.domain.campaign}})
              </td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <td class="entity-list-info" colspan="6">
                <span class="counter">{{deleteSelectionPager.count}} systèmes de culture avec des modèles décisionnels</span> -
                <span class="selection"><ng-pluralize count="selectedManagementModesToDelete.length" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" /></span>
              </td>
          </tfoot>
        </table>
      </div>

    </div>
  </body>
</html>
