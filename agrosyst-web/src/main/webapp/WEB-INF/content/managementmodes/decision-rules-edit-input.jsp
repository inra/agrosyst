<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <s:if test="decisionRule.topiaId == null">
      <title>Nouvelle règle de décision</title>
    </s:if>
    <s:else>
      <title>Règle de décision '<s:property value="decisionRule.name" />'</title>
    </s:else>
    <content tag="current-category">management</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/managementModes.js' /><s:property value='getVersionSuffix()'/>"></script>
    <%-- Out of wro because wro can't handle it --%>
    <script type="text/javascript" src="<s:url value='/webjars/ckeditor/4.14.0/standard/ckeditor.js' />"></script>
    
    <script>

    angular.module('DecisionRulesEdit', ['Agrosyst']).value('DecisionRulesInitData', {
      decisionRule: <s:property value="toJson(decisionRule)" escapeHtml="false"/>,
      domainCode: '<s:property value="domainCode"/>',
      domains: <s:property value="toJson(domains)" escapeHtml="false"/>,
      croppingPlanEntries: <s:property value="toJson(croppingPlanEntries)" escapeHtml="false"/>,
      bioAgressorTopiaId: '<s:property value="bioAgressorTopiaId"/>',
      bioAgressors: <s:property value="toJson(bioAgressors)" escapeHtml="false"/>,
      bioAgressorTypes: <s:property value="toJson(bioAgressorTypes)" escapeHtml="false"/>,
      groupesCibles: <s:property value="toJson(groupesCibles)" escapeHtml="false"/>
    });
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("La règle de décision sur laquelle vous travaillez est inactive et/ou est liée à un domaine inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

  </head>
  <body>
    <div ng-app="DecisionRulesEdit" ng-controller="DecisionRulesEditController" class="page-content">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url namespace='/managementmodes' action='decision-rules-list' />">Règles de décision</a></li>
          <s:if test="decisionRule.topiaId == null">
            <li>&gt; Nouvelle règle de décision</li>
          </s:if>
          <s:else>
            <li>&gt; <s:property value="decisionRule.name" /></li>
          </s:else>
        </ul>
      </div>
      
      <ul class="actions">
        <li><a class="action-retour" href="<s:url namespace='/managementmodes' action='decision-rules-list' />">Retour à la liste des règles de décision</a></li>
      </ul>
      
      <ul class="float-right informations">
        <s:if test="%{decisionRule.topiaId != null}">
          <li>
            <span class="label">Règle de décision<s:if test="!decisionRule.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/managementmodes' action='decision-rules-edit-input'/>?decisionRuleTopiaId=<s:property value='decisionRule.topiaId'/>"
               title="Voir la règle de décision"><s:property value="decisionRule.name" />
            </a>
          </li>
          <li><span class="label">Version</span><s:property value='decisionRule.versionNumber' /></li>
          <li>
            <span class="label">Domaine<s:if test="!activated && decisionRule.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <s:property value="domainName"/>
          </li>
        </s:if>
        <s:if test="%{decisionRule.topiaId != null}">
          <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='decisionRule.code' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(decisionRule.code)" /></a></li>
        </s:if>
      </ul>
      
      <s:if test="decisionRule.topiaId != null">
        <div class="horizontalBanner">
          <div>
            <span class="timeline-title history">Historique des versions</span>          
            <s:if test="decisionRule.topiaId != null && !readOnly && activated"><a class="btn" ng-click="showNewDecisionRuleVersionForm = true">Créer une nouvelle version</a></s:if>

            <ul class="timeline">
              <s:iterator value="relatedDecisionRules" var="relatedDecisionRule">
                <li<s:if test="#relatedDecisionRule.topiaId.equals(decisionRule.topiaId)"> class="selected"</s:if>>
                  <a href="<s:url namespace='/managementmodes' action='decision-rules-edit-input' />?decisionRuleTopiaId=<s:property value='topiaId'/>">
                    Version&nbsp;<s:property value="versionNumber" />
                    &nbsp;&nbsp; (<s:date name="topiaCreateDate" format="d MMM yyyy" /> à <s:date name="topiaCreateDate" format="HH:mm" />)
                  </a>
                </li>
              </s:iterator>
            </ul>
          </div>

          <form ng-if="showNewDecisionRuleVersionForm" method="POST" action="<s:url namespace='/managementmodes' action='create-new-decision-rule-version' />" class="slide-animation">
            <fieldset>
              <input type="hidden" name="decisionRuleCode" value="<s:property value='decisionRule.code'/>"/>
              <textarea id="decisionRule-versionReason" name="versionReason" placeholder="Merci d'indiquer les raisons qui vous poussent à créer une nouvelle version" class="full-width"></textarea>
              <input type="submit" class="btn float-right" value="Nouvelle version" <s:if test="!activated">disabled="disabled" title="La règle de décision sur laquelle vous travaillez est inactive et/ou est liée à un domaine inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>/>
            </fieldset>
          </form>

        </div>
      </s:if>

      <form name="decisionRulesEditForm" action="<s:url action='decision-rules-edit' namespace='/managementmodes' />" method="post" class="tabs clear marginTop150" ag-confirm-on-exit>
        <s:actionerror cssClass="send-toast-to-js"/>
        <s:hidden name="decisionRuleTopiaId" />

        <ul id="tabs-decision-rule-menu" class="tabs-menu clearfix">
          <li class="selected">
            <s:if test="decisionRule.persisted">
              <span><s:property value="decisionRule.name" />&nbsp;(Version&nbsp;<s:property value="decisionRule.versionNumber" />)</span>
            </s:if>
            <s:else>
              <span>Généralités</span>
            </s:else>
          </li>
        </ul>
        
        <div id="tabs-decision-rule-content" class="tabs-content">
          <div id="tab_0">
            <fieldset>

              <s:if test="decisionRule.persisted">
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label>Domaine</label>
                  </span>
                  <span id="domain" class="wwctrl generated-content">
                    <span class='contextual-help'>
                      <span class='help-hover'>
                        <s:text name="help.decisionRules.domain" />
                      </span>
                    </span>
                    <s:property value="domainName"/>
                  </span>
                </div>
              </s:if>
              <s:else>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="domainCode"><span class="required">*</span>&nbsp;Domaine&nbsp;:</label>
                  </span>
                  <span class="wwctrl">
                    <span class='contextual-help'>
                      <span class='help-hover'>
                        <s:text name="help.decisionRules.domain" />
                      </span>
                    </span>
                    <select id="domainCode" ng-model="domainCode"
                            ng-options="domain.code as domain.name for domain in domains"
                            ng-change="domainSelected(domainCode)" required>
                      <option value=""></option>
                    </select>
                    <input type="hidden" name="domainCode" value="{{domainCode}}" />
                  </span>
                </div>
              </s:else>

              <s:textfield name="decisionRule.name" label="Nom" labelSeparator=" :" required="required" requiredLabel="true" labelPosition="left" ng-model="decisionRule.name"/>
              <s:select list="agrosystInterventionTypes" name="decisionRule.interventionType" label="Type d'intervention" labelSeparator=" :"
                emptyOption="true" required="required" requiredLabel="true" labelPosition="left" ng-model="decisionRule.interventionType"/>

              <input type="hidden" name="decisionRuleCropsJson" value="{{getJson(decisionRule.decisionRuleCrop)}}" />

              <div class="wwgrp" ng-if="decisionRule.decisionRuleCrop && decisionRule.decisionRuleCrop.length > 0">
                <s:fielderror fieldName="parent" />
                <span class="wwlbl"><label for="croppingPlanEntryField">Culture concernée&nbsp;:</label></span>
                <span class="wwctrl" >
                  <div ng-repeat="decisionRuleCrop in decisionRule.decisionRuleCrop" class="tag">
                    <span>{{croppingPlanEntriesByCode[decisionRuleCrop.croppingPlanEntryCode].name}}</span>
                    <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="removeDecisionRuleCrop(decisionRule, $index)"/>
                  </div>
                  <span class='contextual-help'>
                    <span class='help-hover'>
                      <s:text name="help.network.parents" />
                    </span>
                  </span>
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl">
                  <label for="croppingPlanEntryField">Ajouter un culture&nbsp;:</label>
                </span>
                <span class="wwctrl">
                  <select id="croppingPlanEntryField" ng-model="croppingPlanEntry"
                      ng-options="croppingPlanEntry as croppingPlanEntry.name for croppingPlanEntry in decisionRuleCroppingPlanEntries"
                      ng-change="addDecisionRuleCrop(decisionRule, croppingPlanEntry)">
                  </select>
                </span>
              </div>

              <s:select list="translatedBioAgressorType" name="decisionRule.bioAgressorType" label="Type de bio-agresseur" labelSeparator=" :"
                 emptyOption="true" labelPosition="left" ng-model="decisionRule.bioAgressorType" ng-change="updateBioAgressors(decisionRule.bioAgressorType)"/>

              <div class="wwgrp">
                <span class="wwlbl">
                  <label for="groupeCibleField">Groupe cible&nbsp;:</label>
                </span>
                <span class="wwctrl">
                  <span class='contextual-help'>
                    <span class='help-hover'>
                      <s:text name="help.report.groupeCibleMaa" />
                    </span>
                  </span>
                  <input type="hidden" name="decisionRule.codeGroupeCibleMaa" value="{{decisionRule.codeGroupeCibleMaa}}" />

                  <select id="groupeCibleField" ng-model="decisionRule.codeGroupeCibleMaa"
                          ng-options="groupeCible.codeGroupeCibleMaa as groupeCible.groupeCibleMaa for groupeCible in groupesCibles | filter: isGroupeCibleInCategory">
                    <option label="" value="" />
                  </select>

                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl">
                  <label for="bioAgressorField">Bio-agresseur concerné&nbsp;:</label>
                </span>
                <span class="wwctrl">
                  <select id="bioAgressorField" ng-model="bioAgressorTopiaId"
                          ng-options="bioAgressor.topiaId as bioAgressor.adventice ? bioAgressor.adventice : bioAgressor.reference_label for bioAgressor in bioAgressors | filter: inGroupeCibleBioAgressor">
                    <option value=""></option>
                  </select>
                  {{bioAgressor}}
                  <input type="hidden" name="bioAgressorTopiaId" value="{{bioAgressorTopiaId}}" />
                </span>
              </div>

              <div class="wwgrp">
                <div class="wwlbl">
                  <label for="decisionObjectField"><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Objet de la décision&nbsp;:</label>
                </div>
                <div class="wwctrl">
                  <textarea id="decisionObjectField" name="decisionRule.decisionObject" ng-model="decisionRule.decisionObject"
                    placeholder="ex. : Opportunité d'intervention fongicide"></textarea>
                </div>
              </div>

              <s:textarea name="decisionRule.domainValidity" ng-model="decisionRule.domainValidity" label="Domaine de validité" labelSeparator=" :" placeholder="ex. : Contexte pédoclimatique caractéristiques du système de culture, bornes temporelles..." />
              <s:textfield name="decisionRule.source" ng-model="decisionRule.source" label="Source de la règle de décision" labelSeparator=" :" placeholder="ex. : Guide technique viticulture durable" labelPosition="left" />
              <s:textarea name="decisionRule.usageComment" ng-model="decisionRule.usageComment" label="Commentaire sur l'utilisation de la règle de décision" labelSeparator=" :" />
              
              <div class="wwgrp">
                <div class="wwlbl">
                  <label for="objectiveField"><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Objectif&nbsp;:</label>
                </div>
                <div class="wwctrl">
                  <textarea id="objectiveField" name="decisionRule.objective" ng-model="decisionRule.objective"
                    placeholder="ex. : Maitriser l'excoriose : absence de dommage de récolte pour le campagne en cours"></textarea>
                </div>
              </div>
              <div class="wwgrp">
                <div class="wwlbl">
                  <label for="expectedResultField"><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Résultat attendu&nbsp;:</label>
                </div>
                <div class="wwctrl">
                  <textarea id="expectedResultField" name="decisionRule.expectedResult" ng-model="decisionRule.expectedResult"
                    placeholder="ex. : Dommages de récolte nuls (dégâts tolérés)"></textarea>
                </div>
              </div>
              <div class="wwgrp">
                <div class="wwlbl">
                  <label for="ckeditorSolution"><span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Solution&nbsp;:</label>
                </div>
                <div class="wwctrl">
                  <textarea id="ckeditorSolution" name="decisionRule.solution" ng-model="decisionRule.solution"></textarea>
                </div>
              </div>
              <s:textarea name="decisionRule.decisionCriteria" ng-model="decisionRule.decisionCriteria" label="Critère de décision / seuil" labelSeparator=" :" placeholder="ex. : Nombre de ceps contaminés lors de la taille" />
              <s:textarea name="decisionRule.resultCriteria" ng-model="decisionRule.resultCriteria" label="Critère d'évaluation du résultat attendu" labelSeparator=" :" placeholder="ex. : Développement de la maladie (% de ceps touchés l'année suivante <= % de ceps touchés cette année)"/>
              <s:textarea name="decisionRule.observation" ng-model="decisionRule.observation" label="Observation / outil" labelSeparator=" :" />
              <s:textarea name="decisionRule.solutionComment" ng-model="decisionRule.solutionComment" label="Commentaire sur la solution" labelSeparator=" :" />
              <s:textarea name="decisionRule.versionReason" ng-model="decisionRule.versionReason" label="Motif de changement de version" labelSeparator=" :"/>
            </fieldset>
          </div>
  
          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url namespace='/managementmodes' action='decision-rules-list' />">Annuler</a>
            <input type="submit" class="btn-primary" value="Enregistrer"
              <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
              <s:if test="!activated">disabled="disabled" title="La règle de décision sur laquelle vous travaillez est inactive et/ou est liée à un domaine inactif. Réactivez l(es)'entité(s) inactive(s) pour pouvoir apporter des modifications."</s:if>
            />
          </span>
        </div>
      </form>

    </div>
  </body>
</html>
