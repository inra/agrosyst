<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<div class="print">
  <table class="data-table sections">
  <thead>
    <tr>
      <th scope="col" class="thead" ng-if="key === 'MALADIES' || key === 'RAVAGEURS'" rowspan="2">
        Groupe cible
      </th>
      <th scope="col" class="thead" ng-if="key === 'ADVENTICES' || key === 'MALADIES' || key === 'RAVAGEURS'" rowspan="2">
        Bio-agresseur considéré
      </th>
      <th scope="col" class="thead" rowspan="2" ng-if="growingSystemSector !== 'VITICULTURE'">Objectifs agronomiques</th>
      <th scope="col" class="thead" rowspan="2">Résultats attendus par l'agriculteur</th>
      <th scope="col" class="thead" rowspan="2">Caractérisation des objectifs agronomiques (niveau de tolérance)</th>
      <th scope="col" class="thead column-xlarge" colspan="3">Leviers mobilisés</th>
    </tr>
    <tr>
      <th scope="col" class="thead">Levier</th>
      <th scope="col" class="thead">Explication</th>
      <th scope="col" class="thead column-small">Culture(s)</th>
    </tr>
  </thead>
  <tbody ng-repeat="section in sections | filter:{sectionType:key}" ng-click="editSection(section)"
         class="selectCursor" ng-class="{'selected-section' : section == originalSection, 'line-error': !validateSection(section)}" >
    <!-- 1ère ligne -->
    <tr>
      <td class="thead" ng-show="key === 'MALADIES' || key === 'RAVAGEURS'" rowspan="{{computeMaxSize(section)}}">
        {{groupesCiblesByCode[section.codeGroupeCibleMaa].groupeCibleMaa | orDash}}
      </td>
      <td class="thead" ng-show="key === 'ADVENTICES' || key === 'MALADIES' || key === 'RAVAGEURS'" rowspan="{{computeMaxSize(section)}}">
        {{section.bioAgressorLabel|orDash}}
      </td>
      <td class="preserveLineBreak" rowspan="{{section.maxSize}}" ng-if="growingSystemSector !== 'VITICULTURE'">
        {{section.agronomicObjective|orDash}}
        <!-- TODO ne pas afficher quand filière viticulture -->
      </td>
      <td class="preserveLineBreak" rowspan="{{section.maxSize}}">
        {{section.expectedResult|orDash}}
      </td>
      <td class="preserveLineBreak" rowspan="{{section.maxSize}}">
        {{i18n['CategoryObjective#' + key][section.categoryObjective]|orDash}}
      </td>

      <!-- Annuel ou parcellaire -->
      <td ng-if="section.annualStrategies.length === 0" colspan="3" class="empty-table column-xlarge">Aucun levier mobilisé</td>
      <td ng-if="section.annualStrategies.length > 0">
        <span title="{{getLeverLabel(section.annualStrategies[0].refStrategyLever) | orDash}}">
          {{getLeverLabel(section.annualStrategies[0].refStrategyLever) | orDash | truncate:50}}
        </span>
      </td>
      <td ng-if="section.annualStrategies.length > 0" class="column-large">
        <span title="{{section.annualStrategies[0].explanation|orDash}}">
          {{section.annualStrategies[0].explanation|orDash|truncate:50}}
        </span>
      </td>
      <td ng-if="section.annualStrategies.length > 0" class="column-small">
        <span ng-if="section.annualStrategies[0].cropIds">
          <span title="{{getCroppingPlansEntryNames(section.annualStrategies[0].cropIds, true)}}">
            {{getCroppingPlansEntryNames(section.annualStrategies[0].cropIds)}}
          </span>
        </span>
        <span ng-if="!section.annualStrategies[0].cropIds"> - </span>
      </td>
    </tr>

    <!-- Lignes suivantes -->
    <tr ng-if="section.maxSize > 1" ng-repeat="i in section.extraRange track by $index">
      <!-- Annuel ou parcellaire -->
      <td ng-if="section.annualStrategies[$index+1]">
        <span title="{{getLeverLabel(section.annualStrategies[$index+1].refStrategyLever) | orDash}}">
          {{getLeverLabel(section.annualStrategies[$index+1].refStrategyLever) | orDash | truncate:50}}
        </span>
      </td>
      <td ng-if="section.annualStrategies[$index+1]" class="column-large">
        {{section.annualStrategies[$index+1].explanation|orDash|truncate:50}}
      </td>
      <td ng-if="section.annualStrategies[$index+1]" class="column-small">
        <span title="{{getCroppingPlansEntryNames(section.annualStrategies[$index+1].cropIds, true)}}">
          {{getCroppingPlansEntryNames(section.annualStrategies[$index+1].cropIds)}}
        </span>
      </td>
    </tr>

  </tbody>
</table>
</div>
