<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <script type="text/javascript">
         function displayConfirmPassword() {
           var res = $('input[name=password]', '#userEditForm').val();
           if (res && res.length) {
             $('#wwgrp_confirmPassword').slideDown();
           } else {
             $('#wwgrp_confirmPassword').slideUp();
           }
         }
         displayConfirmPassword();

        function showWrongItEmailCallback(data, textStatus, jqXHR) {
          if (textStatus !== "success" || !data) {
            displayWrongEmail();
          }
        }

       function validItEmail() {
           var itEmail = $("input[name='user.itEmail']", '#userEditForm').val();
           if (itEmail.length > 0) {
             var emailValidationObject = {email: itEmail};
             $.post(ENDPOINTS.validEmailJson, emailValidationObject, showWrongItEmailCallback, "json");
           }
       }

     </script>
    <script type="text/javascript" src="<s:url value='/nuiton-js/security.js' /><s:property value='getVersionSuffix()'/>"></script>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' /><s:property value='getVersionSuffix()'/>" />
    <script type="text/javascript">
      angular.module('UserRolesEdit', ['Agrosyst', 'ui.autocomplete']).
        value('userRoles', <s:property value="toJson(userRoles)" escapeHtml="false"/>);
    </script>
     <s:if test="user.topiaId == null">
       <title>Nouvel utilisateur</title>
     </s:if>
     <s:else>
       <title>Utilisateur '<s:property value="user.firstName" />&nbsp;<s:property value="user.lastName" />'</title>
     </s:else>
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url namespace='/' action='index' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Administration</a></li>
        <li>&gt; <a href="<s:url namespace='/security' action='users-list' />">Utilisateurs</a></li>
        <s:if test="user.topiaId == null">
          <li>&gt; Nouvel utilisateur</li>
        </s:if>
        <s:else>
          <li>&gt; <s:property value="user.firstName" />&nbsp;<s:property value="user.lastName" /></li>
        </s:else>
      </ul>
    </div>

    <jsp:include page="../admin/admin-menu.jsp" />

    <ul class="actions">
      <li><a class="action-retour" href="<s:url namespace='/security' action='users-list' />">Retour à la liste des utilisateurs</a></li>
    </ul>

    <form id="userEditForm" name="userEditForm" action="<s:url namespace='/security' action='users-edit'  />" method="post" class="tabs clear" ng-app='UserRolesEdit' ag-confirm-on-exit >
      <s:actionerror cssClass="send-toast-to-js"/>
      <ul id="tabs-users-menu" class="tabs-menu clearfix">
         <li class="selected"><span>Généralités</span></li><!--
         --><li><span>Profil(s)</span></li>
      </ul>

      <div id="tabs-users-content" class="tabs-content">

        <!-- Généralités -->
        <div>
          <fieldset>
            <s:hidden name="userTopiaId" value="%{user.topiaId}" />
            <s:textfield type="email" label="Email" name="user.email" value="%{user.email}" labelPosition="left" labelSeparator=" :" placeholder="ex. : gerard.manvussa@inrae.fr" required="true" requiredLabel="true"/>
            <s:if test="newUser">
              <s:password label="Mot de passe" name="password" labelPosition="left" labelSeparator=" :" required="true" requiredLabel="true" autocomplete="off"/>
              <s:password label="Mot de passe (confirmer)" name="confirmPassword" labelPosition="left" labelSeparator=" :" required="true" requiredLabel="true" autocomplete="off"/>
            </s:if>
            <s:else>
              <s:password label="Mot de passe (ne remplir que si vous souhaitez modifier le mot de passe)" onchange="displayConfirmPassword()" name="password" labelPosition="left" labelSeparator=" :" autocomplete="off"/>
              <s:password id="confirmPassword" label="Mot de passe (confirmer)" name="confirmPassword" labelPosition="left" labelSeparator=" :" autocomplete="off"/>
            </s:else>
            <s:textfield label="Prénom" name="user.firstName" value="%{user.firstName}" labelPosition="left" labelSeparator=" :" placeholder="ex. : Gérard" required="true" requiredLabel="true" autocomplete="off" />
            <s:textfield label="Nom" name="user.lastName" value="%{user.lastName}" labelPosition="left" labelSeparator=" :" placeholder="ex. : Manvussa" required="true" requiredLabel="true" autocomplete="off" />
            <s:textfield label="Organisation" name="user.organisation" value="%{user.organisation}" labelPosition="left" labelSeparator=" :" placeholder="ex. : INRA" autocomplete="off" />
            <s:textfield type="email" label="Adresse mail de votre IT" onChange="validItEmail()" id="user_itEmail" name="user.itEmail" labelPosition="left" labelSeparator=" :" placeholder="ex. : prenom.nom@inrae.fr"/>
            <s:textfield label="Numéro de téléphone" name="user.phoneNumber" value="%{user.phoneNumber}" labelPosition="left" labelSeparator=" :" placeholder="ex. : 01.02.03.04.05" autocomplete="off" />
            <s:checkbox label="Actif" name="user.active" value="%{user.active}" labelPosition="left" labelSeparator=" :" />
            <s:select label="Langue" name="user.userLang" value="%{user.userLang}" labelPosition="left" labelSeparator=" :"
              list="languages" listKey="trigram" listValue="names" />
          </fieldset>
        </div>

        <!-- Rôles -->
        <div ng-controller="UserRolesController">

          <input type="hidden" name="userRolesJson" value="{{userRoles}}"/>

          <div  class="table-enclosure clear">

            <table class="data-table inline-edition full-width" id="users-list-table">
              <thead>
                <tr>
                  <th scope="col" class="column-tiny"></th>
                  <th scope="col" class="column-xlarge-fixed">Rôle</th>
                  <th scope="col">Entité</th>
                  <th scope="col" class="column-small">Actions</th>
                </tr>
              </thead>

              <tbody>
                <tr ng-if="userRoles.length == 0"><td colspan="3" class="empty-table">L'utilisateur n'a aucun rôle pour l'instant</td></tr>
                <tr ng-repeat="role in userRoles" ng-class="{'line-selected':isSelectedUserRole(role)}">
                  <td>
                    <input type='checkbox' ng-model="selectedEntitiesForDeletionMap[role.topiaId].selected" ng-ckecked="selectedEntitiesForDeletionMap[role.topiaId].selected" ng-click="toggleSelectedEntityForDeletion(role.topiaId)" />
                  </td>
                  <!-- Lecture -->
                  <td ng-if="!isSelectedUserRole(role)" class="column-xlarge-fixed">{{roleTypes[role.type]}}</td>
                  <td ng-if="!isSelectedUserRole(role)">
                    {{role.entity.label|orDash}}
                    <span ng-if="role.entity.identifier && (role.type == 'DOMAIN_RESPONSIBLE' || role.type == 'GROWING_PLAN_RESPONSIBLE' || (role.type == 'GS_DATA_PROCESSOR' && !role.entity.campaign))">(Toutes campagnes)</span>
                    <span ng-if="role.entity.identifier && role.type == 'GS_DATA_PROCESSOR' && role.entity.campaign">({{role.entity.campaign}})</span>
                  </td>

                  <!-- Édition -->
                  <td ng-if="isSelectedUserRole(role)" class="column-xlarge-fixed">
                    <select ng-model="selectedUserRole.type"
                            ng-options="key as value for (key , value) in roleTypes"
                            ng-change="roleTypeSelected()">
                    </select>
                  </td>
                  <td ng-if="isSelectedUserRole(role) && !selectedUserRole.type">
                    <span class="empty-table">Aucun type de rôle sélectionné</span>
                  </td>
                  <td ng-if="isSelectedUserRole(role) && selectedUserRole.type">
                    <span ng-if="!availableEntities" class="empty-table">Chargement...</span>
                    <span ng-if="availableEntities && availableEntities.length === 0" class="empty-table">Aucune entité sélectionnable</span>
                    <select ng-if="availableEntities && availableEntities.length > 0"
                            ng-model="selectedUserRole.entity"
                            ng-options="entity as entity.label for entity in availableEntities"
                            ng-disabled="isSelectedUserRoleLabelDisabled()"
                            ng-change="roleEntitySelected()">
                    </select>
                    <select ng-if="selectedUserRole.type == 'GS_DATA_PROCESSOR'"
                            ng-model="selectedUserRole.entity.campaign"
                            ng-options="c for c in availableEntityCampaigns"
                            ng-disabled="!selectedUserRole.entity.identifier">
                      <option value="">Toutes campagnes</option>
                    </select>
                  </td>

                  <td>
                    <input ng-if="!isSelectedUserRole(role)" type="button" class="btn-icon icon-edit" title="Éditer" ng-click="selectUserRole(role)" />
                    <input ng-if="isSelectedUserRole(role)" type="button" class="btn-icon icon-save" title="Valider" ng-click="unSelectUserRole()" />
                    <input type="button" class="btn-icon icon-delete" title="Supprimer" ng-click="deleteUserRole('#confirmDeleteUserRoles', role)" />
                  </td>
                </tr>
              </tbody>

              <tfoot>
                <tr>
                  <td colspan="3">
                    <div class="table-end-button">
                      <input type="button" value="Ajouter un rôle" ng-click="addUserRole()"/>
                    </div>
                  </td>
                  <td>
                    <div class="table-end-button">
                      <input type="button" value="Supprimer les rôles" class="button-delete" ng-click="deleteSelectedUserRoles('#confirmDeleteUserRoles')" ng-disabled="selectedEntitiesForDeletionCount == 0" />
                    </div>
                  </td>
                </tr>
              </tfoot>
            </table>

            <div id="confirmDeleteUserRoles" title="Suppression de rôles" class="auto-hide">
              {{confirmDeleteUserRolesMsg}}
            </div>

          </div>
        </div>

        <span class="form-buttons">
          <a class="btn-secondary" href="<s:url namespace='/security' action='users-list' />">Annuler</a>
          <input type="submit" class="btn-primary" value="Enregistrer"/>
        </span>
      </div>
    </form>
  </body>
</html>
