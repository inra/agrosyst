<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Utilisateurs</title>
     <script type="text/javascript" src="<s:url value='/nuiton-js/security.js' />"></script>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
     <script type="text/javascript">
       angular.module('UsersList', ['Agrosyst']).
         value('usersInitData', {
           users: <s:property value="toJson(users)" escapeHtml="false"/>,
           usersFilter: <s:property value="toJson(usersFilter)" escapeHtml="false"/>
         }
       );
     </script>
  </head>
  <body>
    <div ng-app='UsersList' ng-controller="UsersListController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url namespace='/' action='index' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url namespace='/admin' action='home' />">Administration</a></li>
          <li>&gt; Utilisateurs</li>
        </ul>
      </div>

      <jsp:include page="../admin/admin-menu.jsp" />

      <ul class="actions">
        <li><a class="action-ajouter" href="<s:url namespace='/security' action='users-edit-input' />">Ajouter un utilisateur</a></li>
        <li><a class="action-import" onclick="importUsers($('#confirmImportUsers'));return false;">Import</a></li>
      </ul>

      <ul class="float-right actions">
        <li><a class="action-desactiver" ng-class="{'button-disabled':(selectedUsers|toSelectedLength) == 0}"
          onclick="unactivateUsers($('#confirmUnactivateUsers'), $('#usersListForm'), false);return false;" ng-if="allSelectedUserActive">Désactiver</a>
          <a class="action-activer" ng-class="{'button-disabled':(selectedUsers|toSelectedLength) == 0}"
          onclick="unactivateUsers($('#confirmUnactivateUsers'), $('#usersListForm'), true);return false;" ng-if="!allSelectedUserActive">Activer</a></li>
        <li><a class="action-dupliquer" ng-class="{'button-disabled':(selectedUsers|toSelectedLength) != 1}"
         ng-click="choseUserToCopyRoles()">Copier les rôles</a></li>
      </ul>

      <form method="post" id="usersListForm">
        <input type="hidden" name="userIds" value="{{selectedUsers|toSelectedArray}}" />
        <table class="entity-list clear" id="users-list-table">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">Nom</th>
              <th scope="col">Prénom</th>
              <th scope="col">Email</th>
              <th scope="col">Organisation</th>
              <th scope="col">Profils</th>
              <th scope="col" class="column-xsmall">État</th>
            </tr>
            <tr>
              <td>
              </td>
              <td><input type="text" ng-model="filter.lastName" /></td>
              <td><input type="text" ng-model="filter.firstName" /></td>
              <td><input type="text" ng-model="filter.email" /></td>
              <td><input type="text" ng-model="filter.organisation" /></td>
              <td>
                <select ng-model="filter.roleType">
                  <option value="" />
                  <s:iterator value="roleTypes">
                    <option value="<s:property value='key' />"><s:property value="value" /></option>
                  </s:iterator>
                </select>
              </td>
              <td>
                <select ng-model="filter.active">
                  <option value="" />
                  <option value="true">Actif</option>
                  <option value="false">Inactif</option>
                </select>
              </td>
            </tr>
          </thead>

          <tbody>
            <tr ng-show="users.length == 0"><td colspan="7" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>
            <tr ng-repeat="user in users" ng-click="selectedUsers[user.topiaId] = !selectedUsers[user.topiaId]"
                  ng-class="{'line-selected':selectedUsers[user.topiaId]}">
              <td>
                <input type='checkbox' ng-checked="selectedUsers[user.topiaId]" />
              </td>
              <td>
                <a href="<s:url namespace='/security' action='users-edit-input' />?userTopiaId={{user.topiaId|encodeURIComponent}}">{{user.lastName}}</a>
              </td>
              <td>
                <a href="<s:url namespace='/security' action='users-edit-input' />?userTopiaId={{user.topiaId|encodeURIComponent}}">{{user.firstName}}</a>
              </td>
              <td>
                <a href="<s:url namespace='/security' action='users-edit-input' />?userTopiaId={{user.topiaId|encodeURIComponent}}">{{user.email}}</a>
              </td>
              <td>{{user.organisation}}</td>
              <td>{{user.roles|displayableList:roleTypes}}</td>
              <td>{{user.active ? 'Actif' : 'Inactif'}}</td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <td class="entity-list-info" colspan="5">
                <span class="counter">{{pager.count}} utilisateurs</span> -
                <span class="selection"><ng-pluralize count="(selectedUsers|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" /></span>
              </td>
              <td class="pagination" colspan="2">
                <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
              </td>
            </tr>
          </tfoot>
        </table>
      </form>

      <div id="confirmUnactivateUsers" title="Activation/Désactivation des utilisateurs" class="auto-hide">
        Êtes-vous sûr(e) de vouloir {{allSelectedUserActive?'désactiver':'activer'}}
        <ng-pluralize count="(selectedUsers|toSelectedLength)"
             when="{'one': 'l\'utilisateur {{firstSelectedUser.firstName}} {{firstSelectedUser.lastName}}',
                     'other': 'les {} utilisateurs sélectionnés'}"></ng-pluralize> ?
      </div>

      <div id="confirmImportUsers" title="Import des utilisateurs et/ou rôles" class="auto-hide">
        <form action="<s:url namespace='/security' action='users-and-roles-import' />" method="post" enctype="multipart/form-data" class="nomargin">
          <h4>Utilisateurs</h4>
          <p> Sélectionner le fichier CSV des utilisateurs :
            <input type="file" name="userFile" /></p>
          <p>Les utilisateurs déjà existants seront ignorés, et les nouveaux
            utilisateurs recevront une invitation à saisir leur mot de passe par email.</p>
          <p>Format CSV attendu : email;prenom;nom;organisation;tele</p>
          <h4 class="marginTop30">Rôles</h4>
          <p> Sélectionner le fichier CSV des rôles :
            <input type="file" name="rolesFile" /></p>
          <p>Format CSV attendu : email;role;entity;campaign</p>
        </form>
      </div>

      <!-- selection des destinations vers lesquelles coller les roles utilisateur -->
      <%@include file="users-list-copy-paste-roles.jsp" %>
    </div>
  </body>
</html>
