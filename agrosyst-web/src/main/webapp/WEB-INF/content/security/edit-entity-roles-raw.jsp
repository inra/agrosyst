<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
    angular.element(document).ready(function() {
        angular.module('EditEntityRoles', ['Agrosyst'])
            .value('editEntityRolesInitData', {
                roleType: "<s:property value='roleType'/>",
                entityCode: "<s:property value='entityCode'/>",
                roles: <s:property value='toJson(roles)' escapeHtml="false"/>,
                roleTypes: <s:property value='toJson(roleTypes)' escapeHtml="false"/>,
                availableCampaigns: <s:property value='toJson(availableCampaigns)' escapeHtml="false"/>,
                readOnly: <s:property value='readOnly'/>
            });
        angular.bootstrap(angular.element('#editEntityRolesDialogDiv'), ['EditEntityRoles']);
    });
</script>

<div id="editEntityRolesDialogDiv" ng-app="EditEntityRoles" ng-controller="EditEntityRolesController">

  <form name="edit-entity-roles" action="<s:url namespace='/security' action='save-entity-roles'/>" method="POST" >

    <input type="hidden" name="roleType" value="<s:property value='roleType' />"/>
    <input type="hidden" name="entityCode" value="<s:property value='entityCode' />"/>
    <input type="hidden" name="rolesJson" value="{{roles}}"/>

    <table class="data-table">
      <thead>
        <tr>
          <th scope="col">Utilisateur</th>
          <th scope="col">Rôle</th>
          <th scope="col"  ng-if="!readOnly" class="column-small">Actions</th>
        </tr>
      </thead>
      <tbody>
        <tr ng-if="!roles">
          <td class="empty-table" colspan="{{readOnly ? '2' : '3'}}">Vous n'avez pas les droits nécessaires pour consulter cette liste</td>
        </tr>
        <tr ng-if="roles.length == 0">
          <td class="empty-table" colspan="{{readOnly ? '2' : '3'}}">Aucun rôle</td>
        </tr>
        <tr ng-repeat="role in roles">
          <!-- Lecture -->
          <td ng-if="!isSelectedUserRole(role)">{{role.user.firstName}} {{role.user.lastName}} <span ng-if="role.user.email" >({{role.user.email}})</span> <span ng-if="role.user && role.user.active === false" class="empty-table">- Inactif</span></td>
          <td ng-if="!isSelectedUserRole(role)">{{roleTypes[role.type]}} (<span ng-if="!role.entity || !role.entity.campaign">Toutes campagnes</span><span ng-if="role.entity && role.entity.campaign">{{role.entity.campaign}}</span>)</td>

          <!-- Édition -->
          <td ng-if="isSelectedUserRole(role)">
            <span ng-if="!availableUsers" class="empty-table">Chargement ...</span>
            <select ng-if="availableUsers"
                    ng-model="role.user"
                    ng-options="aUser as (aUser.firstName + ' ' + aUser.lastName) group by aUser.groupActive for aUser in availableUsers|orderBy:['-active','+firstName','+lastName']">
            </select>
          </td>
          <td ng-if="isSelectedUserRole(role)">
            {{roleTypes[role.type]}}
            <select ng-model="selectedUserRole.entity.campaign"
                    ng-options="c for c in availableCampaigns"
                    ng-disabled="selectedUserRole.type != 'GS_DATA_PROCESSOR'">
            <option value="">Toutes campagnes</option>
          </select>
          </td>

          <td ng-if="!readOnly">
            <input ng-if="!isSelectedUserRole(role)" type="button" class="btn-icon icon-edit" title="Éditer" ng-click="selectUserRole(role)" />
            <input ng-if="isSelectedUserRole(role)" type="button" class="btn-icon icon-save" title="Valider" ng-click="unSelectUserRole()" />
            <input type="button" class="btn-icon icon-delete" title="Supprimer" ng-click="deleteUserRole(role)" />
          </td>
        </tr>
      </tbody>
      <tfoot ng-if="!readOnly">
        <tr>
          <td colspan="3">
            <div class="table-end-button">
              <input type="button" value="{{addLabel[roleType]}}" ng-click="addUserRole()"/>
            </div>
          </td>
        </tr>
      </tfoot>
    </table>

  </form>

</div>
