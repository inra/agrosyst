<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!-- Users -->
<div id="selectUsersToCopy" title="Copier-coller les rôles de l'utilisateur"  class="slide-animation dialog-form auto-hide">
  <a class="context-element-opener"></a>

  <span class="context-element-title">
    <label>Copier-coller vers les utilisateurs&nbsp;:
      <span class="count" ng-show="copyDialogContext.selectedDomains|toSelectedLength">{{copyRolesDialogObjects.selectedUsers|toSelectedLength}}</span>
    </label>
  </span>
  <div class="context-element-content">
    <table class="entity-list clear full-width" id="navigation-context-users-list">
      <thead>
        <tr>
          <th scope="col"></th>
          <th scope="col">Nom</th>
          <th scope="col">Prénom</th>
          <th scope="col">Email</th>
          <th scope="col">Organisation</th>
          <th scope="col">Profils</th>
          <th scope="col">État</th>
        </tr>
        <tr>
          <td>
          </td>
          <td><input type="text" ng-model="copyRolesDialogObjects.filter.lastName" /></td>
          <td><input type="text" ng-model="copyRolesDialogObjects.filter.firstName" /></td>
          <td><input type="text" ng-model="copyRolesDialogObjects.filter.email" /></td>
          <td><input type="text" ng-model="copyRolesDialogObjects.filter.organisation" /></td>
          <td>
            <select ng-model="copyRolesDialogObjects.filter.roleType">
              <option value="" />
              <s:iterator value="roleTypes">
                <option value="<s:property value='key' />"><s:property value="value" /></option>
              </s:iterator>
            </select>
          </td>
          <td>
            <select ng-model="copyRolesDialogObjects.filter.active">
              <option value=""></option>
              <option ng-value="true">Actif</option>
              <option ng-value="false">Inactif</option>
            </select>
          </td>
        </tr>
      </thead>

      <tbody>
        <tr ng-show="copyRolesDialogObjects.users.length == 0"><td colspan="6" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que les éléments précédemment sélectionnés</td></tr>
        <tr ng-repeat="user in copyRolesDialogObjects.users" ng-click="copyRolesDialogObjects.selectedUsers[user.topiaId] = !copyRolesDialogObjects.selectedUsers[user.topiaId]"
            ng-class="{'line-selected':copyRolesDialogObjects.selectedUsers[role.topiaId]}">
          <td>
            <input type='checkbox' ng-checked="copyRolesDialogObjects.selectedUsers[user.topiaId]" />
          </td>
          <td>{{user.lastName}}</td>
          <td>{{user.firstName}}</td>
          <td>{{user.email}}</td>
          <td>{{user.organisation}}</td>
          <td>{{user.roles|displayableList:roleTypes}}</td>
          <td>{{user.active ? 'Actif' : 'Inactif'}}</td>
        </tr>
      </tbody>

      <tfoot>
        <tr>
          <td class="entity-list-info" colspan="3">
            <span class="counter">{{copyRolesDialogObjects.pager.count}} utilisateurs</span> -
            <span class="selection"><ng-pluralize count="(copyRolesDialogObjects.selectedUsers|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" /></span>
            <span class="unSelectAllUsers" ng-if="(copyRolesDialogObjects.selectedUsers|toSelectedLength) > 0"><a ng-click="copyRolesDialogObjects.selectedUsers={}"> - Désélectionner
              <ng-pluralize count="(copyRolesDialogObjects.selectedUsers|toSelectedLength)" when="{'0': '', '1': 'l\'utilisateur', 'other': 'tous les utilisateurs'}" /></a></span>
          </td>
          <td class="pagination" colspan="3">
            <ag-pagination pager="copyRolesDialogObjects.pager" pagination-update="copyRolesDialogObjects.filter.page=page" />
          </td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
