<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- growing-systems-input-practiced-systems.jsp -->
<div id="tab_3">

  <s:if test="managementModes == null || managementModes.empty">
    Il n'y a pas encore de modèles décisionnels définis sur ce domaine.
  </s:if>
  <s:else>
    Modèles décisionnels liés au système de culture:
    <ul>
      <s:iterator value="managementModes" var="managementMode">
        <li>
          <a href="<s:url action='management-modes-edit-input' namespace='/managementmodes'>
              <s:param name="managementModeTopiaId" value="topiaId" />
            </s:url>">Modèle décisionnel
              <s:if test="#managementMode.category == @fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory@PLANNED">
                <s:text name="fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory.PLANNED"/>
              </s:if>
              <s:if test="#managementMode.category == @fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory@OBSERVED">
                <s:text name="fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory.OBSERVED"/>
              </s:if>
            </a>
        </li>
      </s:iterator>
    </ul>
  </s:else>
</div>
