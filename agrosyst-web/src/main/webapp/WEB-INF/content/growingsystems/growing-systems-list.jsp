<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Systèmes de culture</title>
     <content tag="current-category">management</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/growingSystems.js' /><s:property value='getVersionSuffix()'/>"></script>
     <script type="text/javascript">
        angular.module('GrowingSystemsList', ['Agrosyst']).
            value('ContextFilterInit', {}).
            value('ListGrowingSystemsInitData', <s:property value="growingSystemsJson" escapeHtml="false"/>).
            value('growingSystemFilter', <s:property value="toJson(growingSystemFilter)" escapeHtml="false"/>).
            value('asyncThreshold', <s:property value="exportAsyncThreshold"/>);
     </script>
  </head>
  <body>
    <div ng-app="GrowingSystemsList" ng-controller="GrowingSystemsListController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Systèmes de culture</li>
        </ul>
      </div>

      <ul class="actions">
        <li>
          <a class="action-ajouter" href="<s:url namespace='/growingsystems' action='growing-systems-edit-input' />">Créer un système de culture</a>
        </li>
      </ul>

      <ul class="float-right actions">
        <li>
          <a class="action-desactiver"
             id="unactivate-growing-system-button"
             ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
             onclick="unactivateGrowingSystems($('#confirmUnactivateGrowingSystems'), $('#growingsystemsListForm'), false);return false;"
             ng-if="allSelectedGrowingSystemActive">Désactiver</a>
          <a class="action-activer"
             ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
             onclick="unactivateGrowingSystems($('#confirmUnactivateGrowingSystems'), $('#growingsystemsListForm'), true);return false;"
             ng-if="!allSelectedGrowingSystemActive">Activer</a></li>
        <li>

        <%-- le désactivé devient activé, l'activé par défaut --%>
        <li>
          <a id="validate-growing-system-button"
             class="action-unValidate" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
             onclick="validateGrowingSystems($('#confirmValidateGrowingSystems'), $('#growingsystemsListForm'), false);return false;"
             ng-if="allSelectedGrowingSystemsValidate">Dévalider</a>
          <a id="validate-growing-system-button"
             class="action-validate" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
             onclick="validateGrowingSystems($('#confirmValidateGrowingSystems'), $('#growingsystemsListForm'), true);return false;"
             ng-if="!allSelectedGrowingSystemsValidate">Valider</a>
        </li>

        <%-- <li><a class="action-import button-disabled">Import</a></li>
        <li><a class="action-export-pdf button-disabled">Export PDF</a></li>--%>
        <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
               onclick="growingSystemsExport($('#growingsystemsListForm'));return false;">Export XLS</a></li>
        <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
               ng-click="asyncGrowingSystemsExport()">Export XLS</a></li>
      </ul>

      <form method="post" id="growingsystemsListForm">
        <input type="hidden" name="growingSystemIds" value="{{selectedEntities|toSelectedArray}}" />
        <div class="table_with_footer growing_systems">
          <table class="entity-list clear scrollable_table fixe_layout" id="growing-systems-list-table">
            <thead>
              <tr class="doubleHeight">
                <th scope="col" class="column-tiny-fixe"
                    title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                    <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
                </th>
                <th scope="col" class="column-large" ng-click="changeSort('GROWING_SYSTEM')">
                  <sort>
                    <header_label>Système de culture</header_label>
                    <em ng-if="filter.sortedColumn == 'GROWING_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.GROWING_SYSTEM}" ></em>
                    <em ng-if="filter.sortedColumn != 'GROWING_SYSTEM'" class='fa fa-sort' ></em>
                  </sort>
                </th>
                <th scope="col" class="column-xlarge" ng-click="changeSort('DOMAIN')">
                  <sort>
                    <header_label>Exploitation ou Domaine expérimental</header_label>
                    <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                    <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
                  </sort>
                </th>
                <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_PLAN')">
                  <sort>
                    <header_label>Dispositif</header_label>
                    <em ng-if="filter.sortedColumn == 'GROWING_PLAN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_PLAN, 'fa fa-sort-amount-desc ':sortColumn.GROWING_PLAN}" ></em>
                    <em ng-if="filter.sortedColumn != 'GROWING_PLAN'" class='fa fa-sort' ></em>
                  </sort>
                </th>
                <th scope="col" class="column-xlarge-fixed">Réseaux</th>
                <th scope="col" class="column-small"
                    id="header_campaign"
                    ng-click="changeSort('CAMPAIGN')"
                    title="Campagne">
                  <sort>
                    <header_label>Camp.</header_label>
                    <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                    <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
                  </sort>
                </th>
                <th scope="col" class="column-large">
                  Filière
                </th>
                <th scope="col" class="column-small" ng-click="changeSort('DEPHY_NUMBER')">
                  <sort>
                    <header_label>Numéro DEPHY</header_label>
                    <em ng-if="filter.sortedColumn == 'DEPHY_NUMBER'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DEPHY_NUMBER, 'fa fa-sort-amount-desc ':sortColumn.DEPHY_NUMBER}" ></em>
                    <em ng-if="filter.sortedColumn != 'DEPHY_NUMBER'" class='fa fa-sort' ></em>
                  </sort>
                </th>
                <th scope="col" class="column-small" title="Validation">Validation</th>
                <th scope="col" class="column-tiny" title="État">État</th>
              </tr>
              <tr>
                <td class="column-tiny-fixe"></td>
                <td class="column-large"><input type="text" ng-model="filter.growingSystemName" /></td>
                <td class="column-xlarge"><input type="text" ng-model="filter.domainName" /></td>
                <td class="column-xlarge-fixed"><input type="text" ng-model="filter.growingPlanName" /></td>
                <td class="column-xlarge-fixed"><input type="text" ng-model="filter.network" /></td>
                <td class="column-small"><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" /></td>
                <td class="column-large">
                  <select ng-model="filter.sector">
                    <option value="" />
                    <s:iterator value="sectors">
                      <option value="<s:property value='key' />"><s:property value="value" /></option>
                    </s:iterator>
                  </select>
                </td>
                <td><input type="text" ng-model="filter.dephyNumber" /></td>
                <td class="column-small">
                  <select ng-model="filter.validated">
                    <option value="" />
                    <option value="true">Validé</option>
                    <option value="false">Non validé</option>
                  </select>
                </td>
                <td class="column-xsmall">
                  <select ng-model="filter.active">
                    <option value="" />
                    <option value="true">Actif</option>
                    <option value="false">Inactif</option>
                  </select>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr ng-show="growingSystems.length == 0"><td colspan="10" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>
              <tr ng-repeat="growingSystem in growingSystems"
                    ng-class="{'line-selected':selectedEntities[growingSystem.topiaId]}">
                <td class="column-tiny-fixe">
                  <input type='checkbox' ng-model="selectedEntities[growingSystem.topiaId]" ng-checked="selectedEntities[growingSystem.topiaId]" ng-click="toggleSelectedEntity(growingSystem.topiaId)"/>
                </td>
                <td class="column-large"
                    title="{{growingSystem.name}}">
                  <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId={{growingSystem.topiaId|encodeURIComponent}}">{{growingSystem.name}}<span ng-if="!growingSystem.active" class="unactivated">&nbsp;(inactif)</span></a>
                </td>
                <td class="column-xlarge"
                    title="{{growingSystem.growingPlan.domain.name}}">
                  <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId={{growingSystem.growingPlan.domain.topiaId|encodeURIComponent}}">{{growingSystem.growingPlan.domain.name}}<span ng-if="!growingSystem.growingPlan.domain.active" class="unactivated">&nbsp;(inactif)</span></a>
                </td>
                <td class="column-xlarge-fixed"
                    title="{{growingSystem.growingPlan.name}}">
                  <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId={{growingSystem.growingPlan.topiaId|encodeURIComponent}}">{{growingSystem.growingPlan.name}}<span ng-if="!growingSystem.growingPlan.active" class="unactivated">&nbsp;(inactif)</span></a>
                </td>
                <td class="column-xlarge-fixed"
                    title="{{growingSystem.networks|displayMapValues}}">
                  {{mapSize(growingSystem.networks) > 3 ? (mapSize(growingSystem.networks) + " réseaux") : (growingSystem.networks|displayMapValues:3|orDash) }}
                </td>
                <td class="column-small center"
                    title="{{growingSystem.growingPlan.domain.campaign}} ({{growingSystem.growingPlan.domain.campaign-1}} - {{growingSystem.growingPlan.domain.campaign}})">
                  {{growingSystem.growingPlan.domain.campaign}}
                </td>
                <td class="column-large">
                  {{sectors[growingSystem.sector]}}
                </td>
                <td class="column-small">{{growingSystem.dephyNumber| orDash}}</td>
                <td class="column-small center">
                  {{growingSystem.validated ? 'Validé' : 'Non validé'}}
                </td>
                <td class="column-xsmall center">
                  {{growingSystem.active ? 'Actif' : 'Inactif'}}
                </td>
              </tr>
            </tbody>
          </table>
          <div class="table-footer">
            <div class="entity-list-info">
              <span class="counter">{{pager.count}} systèmes de culture</span>
              <span>
                - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les systèmes de culture</a>
              </span>
              <span>
                - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
              </span>
              <span class="selection">
                - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
              </span>
            </div>
            <div class="pagination">
              <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
            </div>
          </div>
        </div>

      </form>

      <div id="confirmUnactivateGrowingSystems" title="Activer/Désactiver des systèmes de culture" class="auto-hide">
        Êtes-vous sûr(e) de vouloir {{allSelectedGrowingSystemActive?'désactiver':'activer'}}
        <ng-pluralize count="(selectedEntities|toSelectedLength)"
             when="{'one': 'le système de culture {{firstSelectedGrowingSystem.name}}',
                     'other': 'les {} systèmes de culture sélectionnés'}"></ng-pluralize> ?
      </div>

      <div id="confirmValidateGrowingSystems" title="Validation des systèmes de culture" class="auto-hide">
        Êtes-vous sûr(e) de vouloir {{allSelectedGrowingSystemsValidate?'dévalider':'valider'}}
        <ng-pluralize count="(selectedEntities|toSelectedLength)"
             when="{'one': 'le système de culture {{firstSelectedGrowingSystem.name}}',
                     'other': 'les {} systèmes de culture sélectionnés'}"></ng-pluralize> ?
      </div>

    </div>
  </body>
</html>
