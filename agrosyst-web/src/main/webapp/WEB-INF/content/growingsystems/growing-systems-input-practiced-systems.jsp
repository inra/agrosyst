<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2014 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- growing-systems-input-practiced-systems.jsp -->
<div id="tab_4">
  <table class="data-table">
    <thead>
      <tr>
        <th scope="col">Système synthétisé</th>
        <th scope="col">Série de campagnes</th>
        <th scope="col">État</th>
      </tr>
    </thead>
    <tbody>
      <s:if test="practicedSystems == null || practicedSystems.empty">
      <tr>
          <td class="empty-table" colspan="3">
            Il n'y a pas encore de systèmes synthétisés définis sur ce domaine.
          </td>
        </tr>
      </s:if>
      <s:iterator value="practicedSystems">
        <tr>
          <td>
            <a href="<s:url action='practiced-systems-edit-input' namespace='/practiced'>
              <s:param name="practicedSystemTopiaId" value="topiaId" />
            </s:url>">${name}</a>
          </td>
          <td>
            ${campaigns}
          </td>
          <td>
            ${active ? 'Actif' : 'Inactif'}
          </td>
        </tr>
      </s:iterator>
    </tbody>
  </table>
</div>
