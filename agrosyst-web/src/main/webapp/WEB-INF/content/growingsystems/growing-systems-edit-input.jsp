<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://sargue.net/jsptags/time" prefix="javatime" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <s:if test="growingSystem.topiaId == null">
      <title>Nouveau système de culture</title>
    </s:if>
    <s:else>
      <title>Système de culture '<s:property value="growingSystem.name" />'</title>
    </s:else>
    <content tag="current-category">management</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/growingSystems.js' /><s:property value='getVersionSuffix()'/>"></script>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/growingSystems.css' /><s:property value='getVersionSuffix()'/>" />
    <!-- Select2 theme -->
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />
    <script type="text/javascript">
      angular.module('GrowingSystemEditModule', ['Agrosyst', 'ngSanitize', 'ui.date', 'ui.select'])
      .value('availablePlots', <s:property value='toJson(availablePlots)' escapeHtml="false"/>)
      .value('growingSystem', <s:property value='toJson(growingSystem)' escapeHtml='false'/>)
      .value('growingSystemNetworks', <s:property value='toJson(growingSystem.networks)' escapeHtml='false'/>)
      .value('domainSAUArea', <s:property value='toJson(domainSAUArea)' escapeHtml='false'/>)
      .value('selectedPlotsIds', <s:property value='toJson(selectedPlotsIds)' escapeHtml='false'/>)
      .value('selectedGrowingPlan', <s:property value='toJson(selectedGrowingPlan)' escapeHtml='false'/>)
      .value('typeAgricultureTopiaId', "<s:property value='typeAgricultureTopiaId' escapeHtml='false'/>")
      .value('growingSystemCharacteristics', <s:property value="toJson(growingSystemCharacteristics)" escapeHtml="false"/>)
      .value('growingSystemCharacteristicIds', <s:property value="toJson(growingSystemCharacteristicIds)" escapeHtml="false"/>)
      .value('marketingDestinationsBySector', <s:property value="toJson(marketingDestinationsBySector)" escapeHtml="false"/>)
      .value('readOnly', <s:property value="readOnly" escapeHtml="false"/>)
      .directive('typeAgricultureObligatoire', function() {
        return {
          require: 'ngModel',
          link: function(scope, element, attributes, control) {
            control.$validators.typeAgricultureObligatoire = function(modelValue, viewValue) {
              return modelValue != "<s:property value='missingTypeAgricultureTopiaId' escapeHtml='false'/>";
            };
          }
        };
      });

      angular.element(document).ready(
        function () {
          $("#typeAgricultureField > option").each(function() {
            if ($(this).val() == "<s:property value='missingTypeAgricultureTopiaId' escapeHtml='false'/>") {
                this.disabled = true;
            }
          });

          var updateTypeAgricultureFeedback = function() {
            if ($("#typeAgricultureField option:selected").val() == "<s:property value='missingTypeAgricultureTopiaId' escapeHtml='false'/>") {
              $("#typeAgricultureField").css("color", "red");
            } else {
              $("#typeAgricultureField").css("color", "inherit");
            }
          };
          $("#typeAgricultureField").change(updateTypeAgricultureFeedback);
          updateTypeAgricultureFeedback();
        }
      );
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("Le système de culture sur lequel vous travaillez est inactif et/ou est lié à un dispositif et/ou un domaine inactif. Réactivez le(s) pour pouvoir apporter des modifications.");
          }
        );
      </script>
    </s:if>

    <s:if test="%{selectedPlotsIds.isEmpty()}">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            for (var anchor of ["#tabs-growing-system-content-toasts", "#tabs-growing-system-plots-toasts"]) {
              addInlineMessage(anchor, "warning",
                "Le système de culture sur lequel vous travaillez n'a aucune parcelle affectée." +
                "Si vous saisissez en réalisé, cochez les parcelles dans \"Parcelles\" pour les affecter à ce système de culture."
              );
            }
          }
        );
      </script>
    </s:if>

    <s:if test='%{growingSystem.growingPlan  != null && growingSystem.growingPlan.type.toString() == "DEPHY_FERME" && growingSystem.modality.toString() == "ALLEGE"}'>
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            for (var anchor of ["#tabs-growing-system-content-modality-warning"]) {
              addInlineMessage(anchor, "warning",
                "Attention, ce système de culture a été enregistré en suivi allégé et ne sera pas pris en compte dans les analyses de la Cellule de Référence."
              );
            }
          }
        );
      </script>
    </s:if>

  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; <a href="<s:url action='growing-systems-list' namespace='/growingsystems' />">Systèmes de culture</a></li>
        <s:if test="growingSystem.topiaId == null">
          <li>&gt; Nouveau système de culture</li>
        </s:if>
        <s:else>
          <li>&gt; <s:property value="growingSystem.name" /></li>
        </s:else>
      </ul>
    </div>
    
    <ul class="actions">
      <li><a class="action-retour" href="<s:url action='growing-systems-list' namespace='/growingsystems' />">Retour à la liste des systèmes de culture</a></li>
    </ul>
    
    <ul class="float-right informations">
      <s:if test="%{growingSystem.name != null}">
        <li>
          <span class="label">Système de culture<s:if test="!growingSystem.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId=<s:property value='growingSystem.topiaId'/>" title="Voir le système de culture"><s:property value="growingSystem.name" /></a>
        </li>
      </s:if>
      <s:if test="%{growingSystem.sector != null}">
        <li><span class="label">Filière</span><s:text name="fr.inra.agrosyst.api.entities.Sector.%{growingSystem.sector}"/></li>
      </s:if>
      <s:if test="%{growingSystem.growingPlan != null}">
        <li><span class="label">Campagne</span>
          <s:property value="growingSystem.growingPlan.domain.campaign" /> (<s:property value="(growingSystem.growingPlan.domain.campaign)-1" /> - <s:property value="growingSystem.growingPlan.domain.campaign" />)
        </li>
        <li>
          <span class="label">Dispositif<s:if test="!growingSystem.growingPlan.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
          <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value='growingSystem.growingPlan.topiaId'/>" title="Voir le dispositif"><s:property value="growingSystem.growingPlan.name" /></a>
        </li>
        <s:if test="%{growingSystem.growingPlan.domain != null}">
          <li>
            <span class="label">Domaine<s:if test="!growingSystem.growingPlan.domain.active">&nbsp;<span class="unactivated">(inactif)</span></s:if></span>
            <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='growingSystem.growingPlan.domain.topiaId'/>" title="Voir le domaine"><s:property value="growingSystem.growingPlan.domain.name" /></a>
          </li>
        </s:if>
      </s:if>
      <s:if test="%{growingSystem.code != null}">
        <li>
          <span class="label">Validation</span>
          <s:if test="growingSystem.validationDate == null">Jamais validé</s:if>
          <s:else>Le <javatime:format value="${growingSystem.validationDate}" pattern="d MMM yyyy" /> à <javatime:format value="${growingSystem.validationDate}" pattern="HH:mm" /></s:else>
        </li>
        <li class="highlight">
          <span class="label">Exploitants</span>
          <a id="growingSystemDataProcessorsLink" class="action-admin-roles" onclick="editGrowingSystemDataProcessors('<s:property value='growingSystem.code' />')" title="Afficher les exploitatns de ce système de culture">Voir la liste</a>
        </li>
        <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='growingSystem.code' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(growingSystem.code)" /></a></li>
      </s:if>
    </ul>

    <s:if test="relatedGrowingSystems != null">
      <ul class="timeline">
        <s:iterator value="relatedGrowingSystems" var="relatedGrowingSystem">
          <li<s:if test="#relatedGrowingSystem.value.equals(growingSystem.topiaId)"> class="selected"</s:if>>
            <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId=<s:property value="value"/>"><s:property value="key" /></a>
          </li>
        </s:iterator>
      </ul>
    </s:if>

    <div ng-app="GrowingSystemEditModule" class="page-content">
      <form name="growingSystemEditForm" action="<s:url action='growing-systems-edit' namespace='/growingsystems' />" method="post" class="tabs clear" ng-controller="GrowingSystemPlotsController" ag-confirm-on-exit>

        <s:actionerror cssClass="send-toast-to-js"/>
        <s:hidden id="growingSystemTopiaId" name="growingSystemTopiaId" value="%{growingSystem.topiaId}"/>
        <input type="hidden" name="growingSystemNetworkIds" value="{{gsNetwork.topiaId}}" ng-repeat="gsNetwork in gsNetworks" />
        <input type="hidden" name="selectedPlotsIds" value="{{selectedPlotId}}" ng-repeat="selectedPlotId in selectedPlots|toSelectedArray" />
        <input type="hidden" name="marketingDestinationsBySector" value="{{marketingDestinationsBySector}}" />

        <ul id="tabs-growing-system-menu" class="tabs-menu clearfix">
          <li class="selected"><span>Généralités</span></li><!--
          --><li><span>Principaux traits</span></li><!--
          --><li><span>Parcelles</span></li><!--
          --><li><span>Modèles décisionnels liés</span></li><!--
          --><li><span>Systèmes synthétisés liés</span></li>
        </ul>

        <div id="tabs-growing-system-content" class="tabs-content">
          <div id="tab_0">

            <div id="tabs-growing-system-content-toasts"></div>
            <div id="tabs-growing-system-content-modality-warning"></div>

            <fieldset>
              <legend class="invisibleLabel">Données générales d'un dispositif</legend>
              <s:if test="%{growingSystem.topiaId != null}">
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label for="growing-plan"><span class="required">*</span> Dispositif&nbsp;:</label>
                  </span>
                  <span id="growing-plan" class="wwctrl generated-content">
                    <s:property value="%{growingSystem.growingPlan.name}" />
                    &nbsp;({{i18n.TypeDEPHY[growingSystem.growingPlan.type]}})
                  </span>
                </div>
              </s:if>
              <s:else>
                <input type="hidden" name="growingPlanTopiaId" value="{{growingPlanTopiaId}}" />
                <div class="wwgrp">
                  <s:fielderror fieldName="growingPlanTopiaId" />
                  <span class="wwlbl">
                    <label for="growingPlanTopiaId"><span class="required">*</span> Dispositif&nbsp;:
                      <input ng-show="!growingPlanTopiaId" type="text" name="selectedGrowingPlan" ng-model="growingPlanTopiaId" required style="opacity:0;width: 0;"/>
                    </label>
                  </span>
                  <span id="growingPlanTopiaId" class="wwctrl">
                    <ui-select ng-model="selectedGrowingPlan"
                               theme="select2"
                               on-select="selectGrowingPlan($item, false)"
                               ng-required="true"
                               spinner-enabled="true">
                      <ui-select-match placeholder="Sélectionner un dispositif">
                          {{$select.selected.name}} ({{$select.selected.domain.campaign}})
                      </ui-select-match>
                      <ui-select-choices repeat="growingPlan in growingPlans track by $index"
                                         refresh="refreshGrowingPlans($select.search)"
                                         refresh-delay="200">
                          <span ng-bind-html="growingPlan.name + ' (' + growingPlan.domain.campaign + ')' | highlight: $select.search"></span>
                      </ui-select-choices>
                    </ui-select>
                  </span>
                </div>
              </s:else>

              <s:textfield name="growingSystem.name" ng-model="growingSystem.name" label="Nom du système de culture"
                labelPosition="left" labelSeparator=" :" placeholder="ex.: Système de culture 1" required="true" requiredLabel="true" />

              <s:fielderror fieldName="growingSystem.modality" />
              <div ng-if="growingSystem.growingPlan.type == 'DEPHY_FERME'" class="wwgrp">
                <span class="wwlbl">
                  <label for="modalityType"><span class="required">*</span> Modalité de suivi dans DEPHY&nbsp;:</label>
                </span>
                <input type="hidden" name="growingSystem.modality" value="{{growingSystem.modality}}" />
                <span class="wwctrl">
                  <select ng-options="key as value for (key, value) in i18n.ModalityDephy"
                          label="Modalité de suivi dans DEPHY"
                          ng-model="growingSystem.modality"
                          id="modalityType"
                          ng-required="growingSystem.growingPlan.type == 'DEPHY_FERME'">
                    <option value=""></option>
                  </select>
                </span>
              </div>

              <div ng-if="growingSystem.growingPlan.type == 'DEPHY_FERME' && growingSystem.modality == 'DETAILLE'">
                <s:textfield name="growingSystem.dephyNumber" label="Numéro DEPHY" ng-model="growingSystem.dephyNumber" placeholder="ex. : ARF10438"
                             labelPosition="left" labelSeparator=" :" maxlength="15"
                             required="true" requiredLabel="true"/>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label for="networks" class="label">Réseau(x) de rattachement&nbsp;:</label></span>
                <span class="wwctrl">
                  <input type="button"
                         class="btn-icon icon-locate float-right"
                         value="Afficher la hiérarchie des réseaux"
                         title="Afficher la hiérarchie des réseaux"
                         ng-click="displayNetworks(growingSystem.name, gsNetworks)"/>
                  <div ng-if="!gsNetworks.length" style="font-style: italic; color: #BBB; line-height: 25px;">Aucun réseau de rattachement sélectionné</div>
                  <div ng-repeat="network in gsNetworks" class="tag" ng-class="{noclear: $first}">
                    <span>{{network.name}}</span>
                    <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="removeNetwork(network)"/>
                  </div>
                  <input id="networks" type="text" placeholder="Tapez le nom d'un réseau pour l'ajouter" class="clear float-left"/>
                </span>
              </div>

              <div class="wwgrp">
                <span class="wwlbl"><label for="growingSystem.sector" class="label"><span class="required">*</span> Filière&nbsp;:</label></span>
                <span class="wwctrl">
                  <span class="contextual-help">
                    <span class='help-hover'>
                      Indiquez la filière majoritaire
                    </span>
                  </span>
                  <input type="hidden" name="growingSystem.sector" value="{{growingSystem.sector}}" />
                  <select ng-model="growingSystem.sector"
                          id="growingSystem.sector"
                          ng-options="key as value for (key, value) in i18n.Sector"
                          ng-change="propagateSectorChange()"
                          name="growingSystem.sector"
                          required="true">
                  </select>
                </span>
              </div>

              <div class="wwgrp" ng-if="'MARAICHAGE' === growingSystem.sector || 'HORTICULTURE' === growingSystem.sector">
                <s:fielderror fieldName="growingSystem.production" />
                <span class="wwlbl">
                  <label for="productionType">
                    <span class="required">*</span> Type de production&nbsp;:
                  </label>
                </span>
                <span id="productionType" class="wwctrl">
                  <input type="hidden" name="growingSystem.production" value="{{growingSystem.production}}" />
                  <select ng-options="i18n.ProductionType[productionType] for productionType in getProductionTypes(growingSystem.sector)"
                          ng-model="growingSystem.production"
                          required="true">
                  </select>
                </span>
              </div>

              <div class="infoLine block" style="margin: 20px 0;">
                <div class="endLineEvidence">
                  <div style="cursor:pointer"
                       class="textIcones blue x2"
                       title="Information importante">!</div>
                </div>
                <div>
                  Veuillez noter que le type de conduite choisi impactera vos actions de récoltes déjà saisies et celles à venir. <br/>
                  Par exemple, si vous sélectionnez le type de conduite &#xAB;Agriculture biologique&#xBB;, toutes vos actions de récolte auront la case &#xAB;
                  Culture biologique&#xBB; cochée.
                </div>
              </div>

              <!-- 'Information obligatoire' non sélectionnable -->
              <div id="wwgrp_typeAgricultureField" class="wwgrp">
                <span id="wwlbl_typeAgricultureField" class="wwlbl">
                  <label for="typeAgricultureField" class="label">
                    <span class="required">*</span>
                    Type de conduite&nbsp;:
                  </label>
                </span>
                <span id="wwctrl_typeAgricultureField" class="wwctrl" style="">
                  <span class="contextual-help">
                    <span class="help-hover">
                      <p><strong>Agriculture conventionnelle :</strong> mode de production n’ayant pas la certification « agriculture biologique ».</p>
                      <p><strong>Agriculture biologique :</strong> mode de production ayant la certification « agriculture biologique ».</p>
                      <p><strong>Conversion à l’AB :</strong> période de transition entre le mode de production conventionnel et l’obtention de la certification « agriculture biologique ».</p>
                    </span>
                  </span>
                  <select name="typeAgricultureTopiaId" id="typeAgricultureField" style=""
                        type-agriculture-obligatoire="true"
                        ng-model="typeAgricultureTopiaId"
                        required="true">
                    <s:iterator value="typeAgricultures" status="typeAgriculture">
                      <option ng-if=
                        "'<s:property value='topiaId' />' == '<s:property value='missingTypeAgricultureTopiaId' escapeHtml='false'/>'"
                        value="<s:property value='topiaId' />"
                        ng-selected="'<s:property value='topiaId' />' === '<s:property value='typeAgricultureTopiaId' />'">
                          <s:property value='reference_label_Translated' />
                      </option>
                      <option ng-if=
                        "'<s:property value='topiaId' />' != '<s:property value='missingTypeAgricultureTopiaId' escapeHtml='false'/>'"
                        value="<s:property value='topiaId' />"
                        ng-selected="'<s:property value='topiaId' />' === '<s:property value='typeAgricultureTopiaId' />'"
                        style="color: black;">
                          <s:property value='reference_label_Translated' />
                      </option>
                    </s:iterator>
                  </select>
                </span>
              </div>


              <div class="marginTop30">

              <div class="subRubrique"><h2>Description du SdC</h2></div>
                <table class="diagnoseTable">
                  <tr>
                    <td class="labelTd colLeft grey">
                      <label for="growingSystem_startingDate" class="labelgrey"><span class="required">*</span> SdC pratiqué depuis&nbsp;:</label>
                    </td>
                    <td class="contentTd">
                      <s:textfield cssClass="starting_date-field" id="growingSystem_startingDate" name="growingSystemStartingDate"
                      ng-model="growingSystem.startingDate" ui-date="dateOptions" placeholder="ex. : 12/07/2013" required="true" />
                    </td>
                    <td class="labelTd colLeft noPadding" rowspan="3">
                      <label for="growingSystem_overallObjectives">Objectifs généraux assignés au système de culture&nbsp;:</label>
                    </td>
                    <td rowspan="3">
                      <s:textarea id="growingSystem_overallObjectives" name="growingSystem.overallObjectives" rows="2" cols="40"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="labelTd colLeft grey">
                      <label for="growingSystem_description" class="labelgrey">Description&nbsp;:</label>
                    </td>
                    <td class="contentTd">
                      <s:textarea id="growingSystem_description" name="growingSystem.description" ng-model="growingSystem.description" rows="2" cols="40"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="labelTd colLeft grey">
                      <label for="growingSystem_endingDate" class="labelgrey">Date de fin de pratique du SdC&nbsp;:</label>
                    </td>
                    <td class="contentTd">
                      <s:textfield cssClass="ending_date-field" id="growingSystem_endingDate" name="growingSystemEndingDate"
                        ng-model="growingSystem.endingDate" ui-date="dateOptions" placeholder="ex. : 12/07/2013"/>
                    </td>
                  </tr>
                </table>
                <div class="wwgrp">
                  <span class="wwlbl">
                    <label class="tags comment">
                      <span>
                        <a ng-click="showEndActivityComment = !showEndActivityComment" href=""><span class="visible">Motif(s) de fin de pratique</span><span class="blanc">&nbsp;&nbsp;&nbsp;</span></a>
                      </span>
                    </label>
                  </span>
                  <span class="wwctrl">
                    <textarea ng-if="showEndActivityComment" name="growingSystem.endActivityComment" ng-model="growingSystem.endActivityComment" class="ng-pristine ng-valid"></textarea>
                  </span>
                </div>
              </div>

              <div class="marginTop30">

              <div class="subRubrique"><h2>Caractéristiques du SdC</h2></div>

                <table class="diagnoseTable">
                  <tr>
                    <td class="labelTd colLeft grey">
                      <label for="growingSystemAffectedAreaRate" class="labelgrey"><span class="requiredAdvised" title="Recommandé pour le fonctionnement du réseau DEPHY">π</span> Pourcentage de surface du domaine affectée&nbsp;:</label>
                    </td>
                    <td class="smallContentTd">
                      <s:fielderror fieldName="growingSystem.affectedAreaRate" />
                      <span class="input-append smallInput">
                        <input id="growingSystemAffectedAreaRate" type="text" class="labelgrey column-small" name="growingSystem.affectedAreaRate" placeholder="ex&nbsp;: 80" ng-model="growingSystem.affectedAreaRate" maxlength="5" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$"="" class="ng-pristine ng-valid">
                        <span class="add-on">%</span>
                      </span>
                    </td>
                    <td class="labelTd colLeft noPadding" rowspan="4">
                      <label for="growingSystemMarketingDestinations"> Mode de commercialisation&nbsp;:</label>
                    </td>
                    <td rowspan="4">
                      <table class="diagnoseTable">
                        <tr ng-if="flatMarketingDestinationsBySector.length === 0"><td colspan=3>Aucun mode de commercialisation pour la filière sélectionnée</td><tr>
                        <tr ng-repeat="marketingDestination in flatMarketingDestinationsBySector">
                          <td>
                            <input type="checkbox" name="marketingDestination.selectedForGS" value="true" ng-model="marketingDestination.selectedForGS" id="selectedForGS_{{$index}}">
                            <input type="hidden" id="__checkbox_marketingDestination_selectedForGS_{{$index}}" name="__checkbox_marketingDestination_selectedForGS_{{$index}}" value="true">
                          </td>
                          <td>
                            <label class="whiteBG textCenter" for="selectedForGS_{{$index}}">{{marketingDestination.refMarketingDestination.marketingDestination}}</label>
                          </td>
                          <td>
                            <input type="text" class="labelgrey column-small" name="marketingDestination.part" placeholder="ex&nbsp;: 80" ng-model="marketingDestination.part" maxlength="5" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" ng-change="autoSelect(marketingDestination)" class="ng-pristine ng-valid">
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr ng-if="domainSAUArea">
                    <td class="labelTd colLeft grey">
                      <label class="labelgrey underline">Surface du système de culture&nbsp;:</label>
                    </td>
                    <td class="smallContentTd">
                      <span class="wwctrl generated-content" ng-if="growingSystem.affectedAreaRate">
                        {{growingSystem.affectedAreaRate / 100 * domainSAUArea|number:2}} ha
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="labelTd colLeft grey">
                      <label for="growingSystemAffectedWorkForceRate" class="labelgrey">
                        <span class="requiredAdvised" title="<s:text name="help.dephy.advised"/>">&pi;</span> Pourcentage de la main d’oeuvre affectée&nbsp;:
                      </label>
                    </td>
                    <td class="smallContentTd">
                      <s:fielderror fieldName="growingSystem.affectedWorkForceRate" />
                      <span class="input-append smallInput">
                        <input id="growingSystemAffectedWorkForceRate" type="text" name="growingSystem.affectedWorkForceRate" placeholder="ex&nbsp;: 75" class="column-small"
                          ng-model="growingSystem.affectedWorkForceRate" maxlength="5" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" />
                        <span class="add-on">%</span>
                      </span>
                    </td>
                  </tr>

                  <tr>
                    <td class="labelTd colLeft grey">
                      <label for="growingSystemDomainsToolsUsageRate" class="labelgrey">Taux d'utilisation des outils et installations du domaine&nbsp;:</label>
                    </td>
                    <td class="smallContentTd">
                      <s:fielderror fieldName="growingSystem.domainsToolsUsageRate" />
                      <span class="input-append smallInput">
                        <input id="growingSystemDomainsToolsUsageRate" type="text" name="growingSystem.domainsToolsUsageRate" placeholder="ex&nbsp;: 55" class="column-small"
                          ng-model="growingSystem.domainsToolsUsageRate" maxlength="5" ag-percent pattern="^100$|^[0-9]{1,2}$|^[0-9]{1,2}[\.,][0-9]{1,3}$" />
                        <span class="add-on">%</span>
                      </span>
                    </td>
                  </tr>
                </table>
              </div>
            </fieldset>
          </div>

          <div id="tab_1" ng-controller="GrowingSystemCharacteristicController">
            <fieldset>
              <s:select name="growingSystem.categoryStrategy" label="Catégorie de stratégie globale"
                ng-model="growingSystem.categoryStrategy" list="categoryStrategies" labelPosition="left" labelSeparator=" :" emptyOption="true" />
            </fieldset>

            <fieldset class="diagnoseEntries">
              <s:textfield name="growingSystem.averageIFT" ng-model="growingSystem.averageIFT" label="IFT moyen (hors biocontrôle)" labelPosition="left" labelSeparator=" :" ag-float="wtfwsruts" pattern="^[\-\+]?\d*[\.,]?\d*$" />

              <s:textfield name="growingSystem.biocontrolIFT" ng-model="growingSystem.biocontrolIFT" label="IFT biocontrôle" labelPosition="left" labelSeparator=" :" ag-float="wtfwsruts" pattern="^[\-\+]?\d*[\.,]?\d*$" />

              <div class="wwgrp" ng-show="growingSystem.averageIFT || growingSystem.biocontrolIFT">
                <span class="generated-content wwctrl">
                  <a ng-click="showIFTDetails = !showIFTDetails" href="">
                    <strong>
                      {{!(growingSystem.estimatingIftRules || growingSystem.iftSeedsType || growingSystem.doseType) ? (showIFTDetails ? 'Cacher' : 'Déclarer') + " les modalités d'IFT" : ""}}
                      {{growingSystem.estimatingIftRules ? (estimatingIftRules[growingSystem.estimatingIftRules]) : ""}}
                      {{growingSystem.iftSeedsType ? " - " + iftSeedsTypes[growingSystem.iftSeedsType] : ""}}
                      {{growingSystem.doseType ? " - " + doseTypes[growingSystem.doseType] : ""}}
                    </strong>
                  </a>
                </span>
              </div>
            </fieldset>

            <div ng-if="showIFTDetails" class="diagnoseEntries animate-tags-if">
              <s:select id="estimatingIftRules" ng-model="growingSystem.estimatingIftRules" name="growingSystem.estimatingIftRules" label="Modalités d’estimation de l’IFT" labelSeparator=" :"
                        value="%{growingSystem.estimatingIftRules}" list="estimatingIftRules"
                        labelPosition="left" emptyOption="true" />

              <s:select id="IftSeedsType" ng-model="growingSystem.iftSeedsType" name="growingSystem.iftSeedsType" label="Type d’IFT" labelSeparator=" :"
                        value="%{growingSystem.iftSeedsType}" list="iftSeedsTypes"
                        labelPosition="left" emptyOption="true" />

              <s:select id="DoseType" ng-model="growingSystem.doseType" name="growingSystem.doseType" label="Type de doses de référence" labelSeparator=" :"
                        value="%{growingSystem.doseType}" list="doseTypes"
                        labelPosition="left" emptyOption="true" />
            </div>

            <fieldset>
              <div class="wwgrp checkbox-list" ng-repeat="(typeTrait,labelTrait) in growingSystemCharacteristicTypes">
                <span class="wwlbl">
                  <label class="label">{{labelTrait}}&nbsp;:</label>

                  <div class="label-button">
                    <input type="button" ng-click="characteristics.showFullList = !characteristics.showFullList "
                           class="btn float-right font-size95"
                           value="Cliquer ici pour {{characteristics.showFullList ? 'réduire' :'modifier'}} les caractéristiques" />
                  </div>
                </span>

                <span class="wwctrl">
                  <span ng-if="typeTrait=='TRAIT_CONDUITE_CULTURES'" class='contextual-help'>
                    <span class='help-hover'>
                      <s:text name="help.domain.growingSystems.cultureManagement" />
                    </span>
                  </span>

                  <ul ng-if="!characteristics.showFullList">
                    <li ng-repeat="characteristic in filteredCharacteristics = (growingSystemCharacteristics | filter:selectedCharacteristic | filter:{type_trait_sdc:typeTrait})">
                     {{characteristic.nom_trait}}
                    </li>
                  </ul>

                  <span ng-show="characteristics.showFullList" ng-repeat="growingSystemCharacteristic in growingSystemCharacteristics | filter:{type_trait_sdc:typeTrait}">
                    <input type="checkbox" id="growingSystemCharacteristicIds-{{$index}}{{typeTrait}}"
                           style="margin-bottom:0;"
                           name="growingSystemCharacteristicIds" ng-model="growingSystemCharacteristicIds[growingSystemCharacteristic.topiaId]"
                           value="{{growingSystemCharacteristic.topiaId}}" />
                    <label for="growingSystemCharacteristicIds-{{$index}}{{typeTrait}}" class="checkboxLabel">{{growingSystemCharacteristic.nom_trait}}</label>
                  </span>

                  <input type="hidden" id="__multiselect_growingSystemCharacteristicIds" name="__multiselect_growingSystemCharacteristicIds" value="" />
                </span>

                <span ng-if="typeTrait=='TRAIT_GESTION_CYCLE' && (characteristics.showFullList || growingSystem.cycleManagementComment.length>0)" >
                  <s:textarea name="growingSystem.cycleManagementComment" ng-model="growingSystem.cycleManagementComment"
                              rows="2" cols="80" placeholder="Autres traits de gestion du cycle pluriannuel"/>
                </span>

                <span ng-if="typeTrait=='TRAIT_CONDUITE_CULTURES' && (characteristics.showFullList || growingSystem.cultureManagementComment.length>0)">
                  <s:textarea  name="growingSystem.cultureManagementComment" ng-model="growingSystem.cultureManagementComment"
                                rows="2" cols="80" placeholder="Autres traits de conduite des cultures"/>
                </span>

                <span ng-if="typeTrait=='TRAIT_TRAVAIL_SOL' && (characteristics.showFullList || growingSystem.groundWorkComment.length>0)" >
                  <s:textarea name="growingSystem.groundWorkComment" ng-model="growingSystem.groundWorkComment"
                              rows="2" cols="80" placeholder="Autres traits de travail du sol"/>
                </span>

                <span ng-if="typeTrait=='TRAIT_GESTION_EXTRA_PARCELLAIRE' && (characteristics.showFullList || growingSystem.parcelsManagementComment.length>0)" >
                  <s:textarea name="growingSystem.parcelsManagementComment" ng-model="growingSystem.parcelsManagementComment"
                              rows="2" cols="80" placeholder="Autres traits de gestion du paysage ou extra-parcellaire"/>
                </span>
              </div>
            </fieldset>
          </div>

          <!-- Parcelles -->
          <div id="tab_2">
             <div id="tabs-growing-system-plots-toasts"></div>

            <div class="help-explanation-panel">
              Ce tableau vous permet de sélectionner les parcelles du domaine à affecter au système de culture actuel. <br/>
              Les parcelles du domaine affectées à un autre système de culture ne sont pas proposées.
            </div>
            <div class="table-enclosure">
              <table id="plotsTable" class="data-table">
                <thead>
                  <tr>
                    <th scope="col">
                      <input type="checkbox"
                        ng-checked="areAllPlotsSelected()"
                        ng-click="toggleSelectedPlots()"
                        class="ng-pristine ng-untouched ng-valid ng-empty"
                        ng-show="!readOnly"
                        ng-class = "{'disabled':!availablePlots || availablePlots.length == 0}"
                        ng-disabled="!availablePlots || availablePlots.length == 0" />
                      Affectation
                    </th>
                    <th scope="col">Nom de la parcelle</th>
                    <th scope="col">Numéro d'ilôt PAC</th>
                    <th scope="col">Culture(s)</th>
                  </tr>
                </thead>
                <tbody id="parcelles">
                  <tr ng-show="!readOnly && (!availablePlots || availablePlots.length === 0)"><td colspan="4" class="empty-table">Aucune parcelle n'est disponible pour ce système de culture.</td></tr>
                  <tr ng-show="readOnly"><td colspan="4" class="empty-table">Vous n'êtes pas autorisé à voir les parcelles.</td></tr>
                  <tr ng-repeat="plot in availablePlots">
                    <td>
                      <div class="wwgrp">
                        <input type='checkbox'
                          ng-model="selectedPlots[plot.topiaId]"
                          ng-disabled="!plot.active || !growingSystem.growingPlan.domain.active"
                          title="{{(!plot.active || !growingSystem.growingPlan.domain.active) ? 'La parcelle et/ou son domaine sont inactifs. Réactivez le(s) pour pouvoir apporter des modifications.' : ''}}"/>
                      </div>
                    </td>
                    <td>
                      <div class="wwgrp">
                        {{plot.name}}
                      </div>
                    </td>
                    <td>
                      <div class="wwgrp">
                        {{plot.pacIlotNumber}}
                      </div>
                    </td>
                    <td>
                      <div class="wwgrp">
                        {{plot.croppingPlanInfo}}
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class="wwgrp">
              <span class="wwlbl">
                <label for="growingSystemPlotOutputReason">Motifs de sortie des parcelles&nbsp;:</label>
              </span>
              <span class="wwctrl">
                <span class="input-append">
                  <textarea id="growingSystemPlotOutputReason" name="growingSystem.plotOutputReason" placeholder=""
                       ng-model="growingSystem.plotOutputReason"></textarea>
                </span>
              </span>
            </div>
          </div>

          <!-- Modèles décisionnels liés -->
          <%@include file="growing-systems-input-management-modes.jsp" %>

          <!-- Systèmes synthétisés liés -->
          <%@include file="growing-systems-input-practiced-systems.jsp" %>

          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url action='growing-systems-list' namespace='/growingsystems' />">Annuler</a>
            <input type="submit" class="btn-primary" value="Enregistrer"
              <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
              <s:if test="!activated">disabled="disabled" title="Le système de culture sur lequel vous travaillez est inactif et/ou est lié à un dispositif et/ou un domaine inactif. Réactivez le(s) pour pouvoir apporter des modifications."</s:if>
            />
          </span>
        </div>
      </form>

    </div>

    <div id="show-networks" title="Hiérarchie des réseaux" class="auto-hide">
      Chargement...
    </div>

  </body>
</html>
