<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Système de culture / Décisionnel</title>
    <content tag="current-category">management</content>
  </head>
  <body>
    
    <div class="presentation-page">
      <h1>Système de culture / Décisionnel</h1>
      
      <div>
        Au sein de ce menu, vous pouvez :
        <ul>
          <li>créer des systèmes de culture ;</li>
          <li>détailler leur modèle décisionnel et les règles de décisions suivies.</li>
        </ul>
        
        Chaque <strong>système de culture</strong> est défini par un nom et ses principaux traits de stratégie contre les bioagresseurs. Il est relié à un seul dispositif et peut être rattaché à différents réseaux.<br/><br/>
        
        Pour chaque système de culture, on peut définir un modèle décisionnel, c’est-à-dire les stratégies adoptées pour différentes thématiques (maîtrise des adventices, gestion du travail du sol, etc.). Puis on peut attacher à ces modèles décisionnelsdes règles de décision détaillant comment sont adaptés les éléments d’une intervention culturale selon le contexte connu ou futur.
      </div>
    </div>

  </body>
</html>
