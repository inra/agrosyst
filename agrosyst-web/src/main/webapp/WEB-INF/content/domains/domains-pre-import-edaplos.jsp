<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Import Edaplos</title>
    <content tag="current-category">contextual</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/domains.js'/><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">
        angular.module('ImportEdaplos', ['Agrosyst']).
            value('EDaplosResult', <s:property value="toJson(edaplosParsingResults)" escapeHtml="false"/>);
     </script>
  </head>
  <body>
    <div ng-app="ImportEdaplos" ng-controller="ImportEdaplosController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url action='domains-list' namespace='/domains' />">Domaines</a></li>
          <li>&gt; Import eDaplos</li>
        </ul>
      </div>
      <ul class="float-right actions">
        <li><a class="action-import-edaplos" onclick="displayDialogAndSubmitForm($('#confirmImportEdaplosDialog'));return false;">Import eDaplos</a></li>
      </ul>
      <div class="agrosyst-body">

        <form action="<s:url action='domains-import-edaplos' namespace='/domains' />" method="post" class="form tabs">

          <s:hidden name="edaplosFileId" value="%{edaplosFileId}" />

          <ul id="tabs-menu" class="tabs-menu clearfix">
            <li id="edaplos" class="selected">eDaplos</li>
          </ul>

          <div id="tabs-content" class="tabs-content">

            <s:actionmessage/>
            <s:actionerror cssClass="send-toast-to-js"/>
            <s:fielderror />

            <div id="edaplos-results" ng-if="eDaplosResult">
                <h1>Analyse du fichier d'import</h1>

                <div class="import-global-messages">
                  <ul ng-repeat="message in eDaplosResult.errorMessages track by $index" class="errorMessage">
                      <li style="padding-left: 105px;line-height: 23px">
                        <div class="positionRelative" ng-if="message.path">
                          <span class="contextual-help">
                            <span class='help-hover' style="white-space: pre-wrap;">{{message.path}}</span>
                          </span>
                        </div>

                        {{message.label}}
                      </li>
                  </ul>
                  <ul ng-repeat="message in eDaplosResult.warningMessages track by $index" class="warningMessage">
                      <li style="padding-left: 105px;line-height: 23px">
                        <div class="positionRelative" ng-if="message.path">
                          <span class="contextual-help">
                            <span class='help-hover' style="white-space: pre-wrap;">{{message.path}}</span>
                          </span>
                        </div>

                        {{message.label}}
                      </li>
                  </ul>
                  <ul ng-repeat="message in eDaplosResult.infoMessages track by $index" class="infoMessage">
                    <li style="padding-left: 105px;line-height: 23px">
                      <div class="positionRelative" ng-if="message.path">
                        <span class="contextual-help">
                          <span class='help-hover' style="white-space: pre-wrap;">{{message.path}}</span>
                        </span>
                      </div>

                      {{message.label}}
                    </li>
                  </ul>
                </div>

                <a ng-if="eDaplosResult.edaplosParsingStatus != 'EXCEPTION'" href="<s:url action='domains-export-edaplos' namespace='/domains'><s:param name='edaplosFileId' value='edaplosFileId' /></s:url>">Télécharger le rapport au format CSV</a>

                <p ng-if="eDaplosResult.edaplosParsingStatus == 'SUCCESS'">Ce rapport sera envoyé par e-mail suite à la validation de l'import.</p>
            </div>
            <span class="form-buttons" ng-if="eDaplosResult">
              <input type='submit' id="saveButton" class='btn-primary' value="Valider l'import"
                title="Valider et confirmer l'import eDaplos"
                ng-if="eDaplosResult.edaplosParsingStatus == 'SUCCESS'"/>
              <input type='submit' class='btn-primary' value="Valider l'import"
                disabled="disabled" title="Vous devez corriger les erreurs avant de poursuivre l'import"
                ng-if="eDaplosResult.edaplosParsingStatus != 'SUCCESS'"/>
            </span>
          </div>
        </form>
      </div>

      <div id="confirmImportEdaplosDialog" title="Import eDaplos" class="auto-hide">
        <form action="<s:url action='domains-pre-import-edaplos' />" method="post" enctype="multipart/form-data">
          <p>Fichier eDaplos (zip ou fichier xml) : <input type="file" name="file" /></p>
        </form>
      </div>
    </div>
  </body>
</html>
