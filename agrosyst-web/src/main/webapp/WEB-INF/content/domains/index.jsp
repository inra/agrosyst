<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Contextuel et Organisationnel</title>
    <content tag="current-category">contextual</content>
  </head>
  <body>
    
    <div class="presentation-page">
      <h1>Contextuel et Organisationnel</h1>
      
      <div>
        Au sein de ce menu, vous pouvez :
        <ul>
          <li>créer des domaines ;</li>
          <li>créer les dispositifs présents au sein des domaines ;</li>
          <li>créer des réseaux auxquels vous relierez ensuite des systèmes de culture.</li>
        </ul>
        
        Un <strong>domaine</strong> regroupe les parcelles sur lesquelles sont étudiés les systèmes de culture. Il peut s’agir d’une exploitation agricole, d’un domaine expérimental ou encore d’une ferme de lycée agricole. La description du domaine est importante pour préciser les éléments contextuels aux systèmes de culture étudiés, le contexte ayant une influence sur le pilotage des systèmes de culture et sur leurs résultats.<br/><br/>
        
        Un <strong>dispositif</strong> est un élément organisationnel qui permet de regrouper des systèmes de culture étudiés de façon coordonnée sur un domaine. Un dispositif est de type DEPHY-EXPE ou DEPHY-FERME. Il peut y en avoir plusieurs pour un même domaine mais un système de culture n’appartient qu’à un seul dispositif.<br/><br/>
        
        Un <strong>réseau</strong> est un groupe de systèmes de culture ou un groupe de réseaux qui suivent un protocole ou une animation commune.
      </div>
    </div>

  </body>
</html>
