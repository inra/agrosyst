<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!--prices.jsp-->
<!-- TODO mal nommé: signifie l'ensemble des prix hors prix de semences et récoltes -->
<input type="hidden" name="allRegularPricesJson" value="{{allRegularPrices}}"/>

<div class="help-explanation-panel">
  <strong ng-if="helpMessage" ng-bind-html="helpMessage"></strong>
  <strong ng-if="!helpMessage"><s:text name="prices-help" /></strong>
</div>

<fieldset class="sub-form marginTop30"></fieldset>

<table ng-if="loadingPrices" class="data-table data-table-loading width-table">
  <thead>
    <tr>
      <th><s:text name="common-loading"/> <i class="fa fa-spinner fa-pulse"></i></th>
    </tr>
  </thead>
</table>

<div>
   <div ng-if="harvestingPrices">
      <table class="data-table width-table">
        <thead>
          <tr>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-effectivePracticed" /></th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-croppingSystem" /></th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-plotPracticed" /></th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-campaigns" /></th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-crop" /></th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-destination" /></th>
            <th scope="col"><s:text name="prices-harvest-table-yeald" /></th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-price" /></th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-unit" /></th>
            <th scope="col" rowspan="2" colspan="2">
              <s:text name="prices-harvest-table-userPrice" />
              <span class="tooltip-ng help positionRelative">
                &nbsp;<i class="icon fa fa-question-circle font-size130 " style="color:white" aria-hidden="true"></i>
                <span class="tooltip-hover opposite" style="width:300px">
                  <s:text name="common-average-formula"/>
                </span>
              </span>
            </th>
            <th scope="col" rowspan="2"><s:text name="prices-harvest-table-refPrice" /></th>
          </tr>
        </thead>
        <tbody>
          <tr ng-if="harvestingPrices.length === 0">
            <td colspan="8">
              <s:text name="prices-harvest-table-empty" />
            </td>
          </tr>
          <tr ng-repeat-start="price in harvestingPrices"
            ng-if="price.headerCropLine || (price.headerSummaryLine && (price.headerSummaryPrice && !price.mainMetaInf.showSpeciesValorisationPrices)) || (!price.headerSummaryPrice && price.mainMetaInf.showSpeciesValorisationPrices) || (price.headerSummaryPrice && !price.mainMetaInf.showSpeciesValorisationPrices)"
            class="{{price.headerCropLine || price.headerSummaryLine || price.headerSummaryPrice ? (price.actionPricesNumber%2 === 0 ? 'main-line-1' : 'main-line') : ''}}">

            <!-- Réalisé / Synthétisé -->
            <td ng-if="price.summaryInfNewLine && price.headerSummaryPrice" rowspan="{{price.summaryInfNewLineDestinationCount}}">
              <div ng-if="price.practicedSystemId"><s:text name='prices-harvest-table-practiced' /></div>
              <div ng-if="price.zoneId"><s:text name='prices-harvest-table-effective' /></div>
            </td>
            <td ng-show="price.newSerie && !price.summaryInf" rowspan="{{price.speciesMainMetaInf ? price.speciesMainMetaInf.rowSpan : 1}}">
              <div ng-if="price.practicedSystemId"><s:text name='prices-harvest-table-practiced' /></div>
              <div ng-if="price.zoneId"><s:text name='prices-harvest-table-effective' /></div>
            </td>

            <!-- Système de culture -->
            <td ng-if="price.summaryInfNewLine && price.headerSummaryPrice" rowspan="{{price.summaryInfNewLineDestinationCount}}">
              <div ng-if="price.growingSystemName">{{price.growingSystemName}}</div>
            </td>
            <td ng-show="price.newSerie && !price.summaryInf" rowspan="{{price.speciesMainMetaInf ? price.speciesMainMetaInf.rowSpan : 1}}">
              <div ng-if="price.growingSystemName">{{price.growingSystemName}}</div>
            </td>

            <!-- Nom parcelle / Nom Synthétisé -->
            <td ng-if="price.summaryInfNewLine && price.headerSummaryPrice" rowspan="{{price.summaryInfNewLineDestinationCount}}">
              <div ng-if="price.practicedSystemId">
                <a href="<s:url namespace='/practiced' action='practiced-systems-edit-input'/>?practicedSystemTopiaId={{price.practicedSystemId|encodeURIComponent}}">{{price.practicedSystemName}}</a>
              </div>
              <div ng-if="price.zoneId">
                <a href="<s:url namespace='/effective' action='effective-crop-cycles-edit-input'/>?zoneTopiaId={{price.zoneId|encodeURIComponent}}">{{price.zoneName}}</a>
              </div>
            </td>
            <td ng-show="price.newSerie && !price.summaryInf" rowspan="{{price.speciesMainMetaInf ? price.speciesMainMetaInf.rowSpan : 1}}">
              <div ng-if="price.practicedSystemId">
                <a href="<s:url namespace='/practiced' action='practiced-systems-edit-input'/>?practicedSystemTopiaId={{price.practicedSystemId|encodeURIComponent}}">{{price.practicedSystemName}}</a>
              </div>
              <div ng-if="price.zoneId">
                <a href="<s:url namespace='/effective' action='effective-crop-cycles-edit-input'/>?zoneTopiaId={{price.zoneId|encodeURIComponent}}">{{price.zoneName}}</a>
              </div>
            </td>

            <!-- Campagne(s) -->
            <td ng-if="price.summaryInfNewLine && price.headerSummaryPrice" rowspan="{{price.summaryInfNewLineDestinationCount}}">
              <div ng-if="price.campaign">{{price.campaign}}</div>
            </td>
            <td ng-show="price.newSerie && !price.summaryInf" rowspan="{{price.speciesMainMetaInf ? price.speciesMainMetaInf.rowSpan : 1}}">
              <div ng-if="price.campaign">{{price.campaign}}</div>
            </td>

            <!-- nom de la culture ou de l'espèce -->
            <td ng-show="price.newSerie && !price.summaryInf" rowspan="{{price.speciesMainMetaInf ? price.speciesMainMetaInf.rowSpan : 1}}">
              <strong><div ng-bind-html="price.displayName"></div></strong>
              <div ng-if="price.headerSummaryLine">
                <input type="button"
                       title="<s:text name='prices-harvest-table-showPriceBySpecies'/>"
                       ng-click="price.mainMetaInf.showSpeciesValorisationPrices = !price.mainMetaInf.showSpeciesValorisationPrices"
                       value="<s:text name='prices-harvest-table-showLess'/>"
                />
              </div>
            </td>

            <!-- Destination-->
            <!-- Dans le cas d'une culture on n'affiche plus rien sur la ligne -->
            <td ng-if="price.headerSummaryLine" colspan="7">
            </td>

            <!-- display destination label in all case except if it's a mixed species or in case summary-->
            <td ng-if="price.displayDetails">
              <strong>{{price.destination ? price.destination : "-"}}
            </td>

            <!-- déclaration rendement sur 2 colonnes dans le cas d'une culture principale-->
            <td ng-if="price.displayDetails">
              <s:text name="prices-harvest-table-average" /> : {{price.yealdAverage| number:1|orDash}} {{yealdUnits[price.yealdUnit]}}<br/>
              <a ng-click="price.harvestingActionValorisation._showYealdMinMediumMax = true"
                 ng-show="!price.harvestingActionValorisation._showYealdMinMediumMax && (price.harvestingActionValorisation.yealdMin || price.harvestingActionValorisation.yealdMax || price.harvestingActionValorisation.yealdMedian)">
                <s:text name="prices-harvest-table-showYealds" />
              </a>
                <span ng-show="price.harvestingActionValorisation._showYealdMinMediumMax" class="slide-animation nowrap">
                  <s:text name="prices-harvest-table-min" /> : {{price.harvestingActionValorisation.yealdMin| number:1}} {{yealdUnits[price.yealdUnit]}} <br>
                  <s:text name="prices-harvest-table-median" /> : {{price.harvestingActionValorisation.yealdMedian| number:1}} {{yealdUnits[price.yealdUnit]}} <br>
                  <s:text name="prices-harvest-table-max" /> : {{price.harvestingActionValorisation.yealdMax| number:1}} {{yealdUnits[price.yealdUnit]}} <br>
                </span>
            </td>

            <!-- estimate prices-->
            <td ng-if="price.summaryInfNewLine && !price.headerSummaryPrice"
                colspan="2" rowspan="{{price.summaryInfNewLineDestinationCount}}">
              <strong><s:text name="prices-harvest-table-mixPrice" /></strong>
            </td>
            <td ng-if="price.summaryInfNewLine && price.headerSummaryPrice"
                rowspan="{{price.summaryInfNewLineDestinationCount}}">
              <div ng-bind-html="price.displayName"></div></strong>
              <div>
                <input type="button"
                       title="<s:text name="prices-harvest-table-showPriceBySpecies" />"
                       ng-click="price.mainMetaInf.showSpeciesValorisationPrices = !price.mainMetaInf.showSpeciesValorisationPrices"
                       value="<s:text name="prices-harvest-table-showMore" />"
                    />
              </div>
            </td>
            <td ng-if="price.summaryInf">
              <strong>{{price.destination}}</strong>
            </td>
            <td ng-if="price.summaryInf"></td><!-- Rendement (vide)-->

            <!-- Prix réel-->
            <td ng-if="price.displayDetails">
              <input type="number" step="any" min="0"
                     id="price_{{price.type}}{{price.objectId}}"
                     ng-model="price.price"
                     ng-change="setUserPrice(price)"
                     ag-float-positive
                     pattern="^\d*[\.,]?\d*$"
                     ng-disabled="(price.destination === '<s:text name="prices-harvest-table-toComplete" />' || !price.yealdAverage)"
                     title="{{price.destination === '<s:text name="prices-harvest-table-toComplete" />' ? '<s:text name="prices-harvest-table-fixDestination" />' : (!price.yealdAverage ? '<s:text name="prices-harvest-table-noYeald" />' : '<s:text name="prices-harvest-table-fillHarvestPrice" />' )}}"/>
            </td>
            <td ng-if="price.summaryInf">
              <span ng-if="price.sharedPriceInfos.price">{{price.sharedPriceInfos.price | number:2}} {{priceUnits[price.sharedPriceInfos.priceUnit]}}</span>
            </td>

            <td ng-if="price.headerSummaryPrice" colspan="4"></td>

            <!-- Unité de prix-->
            <td ng-if="price.displayDetails">
              <span ng-if="price.hasPriceUnits">
                <select id="price_{{price.type}}{{price.objectId}}_unit"
                        ng-model="price.priceUnit"
                        ng-options="key as value for (key, value) in price.priceUnits"
                        ng-disabled="price.destination === '<s:text name="prices-harvest-table-toComplete" />'"
                        title="{{price.destination === '<s:text name="prices-harvest-table-toComplete" />' ? '<s:text name="prices-harvest-table-fixDestination" />' : ''}}">
                  <!--{{priceUnits[price.priceUnit ? price.priceUnit : price.harvestingActionValorisation.medianUnitRefPrice]}}-->
                </select>
              </span>
              <span ng-if="!price.hasPriceUnits">
                -
              </span>
            </td>

            <!-- Prix saisis par les utilisateurs(/dest)-->
            <!-- Ne pas afficher s'il s'agit du prix estimé du mélange -->
            <td ng-if="price.displayDetails">
              <span ng-if="price.harvestingActionValorisation.userPricesMedian">
                {{price.harvestingActionValorisation.userPricesMedian | number:2}}
                [{{price.harvestingActionValorisation.userLowerPrice | number:2}} - {{price.harvestingActionValorisation.userHigherPrice | number:2}}]
                {{priceUnits[price.harvestingActionValorisation.userPriceUnit]}}
              </span>
              <span ng-if="!price.harvestingActionValorisation.userPricesMedian">
                -
              </span>
            </td>
            <td ng-if="price.displayDetails">
              <span ng-if="price.harvestingActionValorisation.userNbPricesByCampaigns">
                <span ng-repeat="(key, value) in price.harvestingActionValorisation.userNbPricesByCampaigns">
                  ({{key}}):{{value}} <br>
                </span>
              </span>
              <span ng-if="price.harvestingActionValorisation.countedPrices === 0">
                -
              </span>
            </td>

            <!-- Prix de référence (/dest)-->
            <td ng-if="price.displayDetails">
              <span ng-if="price.harvestingActionValorisation.hasAverageRefPrice">
                <span ng-if="price.harvestingActionValorisation.averageRefPrice && price.harvestingActionValorisation.averageRefPrice !== 0">
                  {{price.harvestingActionValorisation.averageRefPrice | number:2}}&nbsp;
                </span>
                <span ng-if="!price.harvestingActionValorisation.averageRefPrice || price.harvestingActionValorisation.averageRefPrice === 0">
                  <s:text name="prices-harvest-table-neRefPrice" />
                </span>
                {{priceUnits[price.harvestingActionValorisation.medianUnitRefPrice]}}&nbsp;
                {{price.harvestingActionValorisation.campaignsPricesSummary}}
              </span>
              <span ng-if="!price.harvestingActionValorisation.hasAverageRefPrice">
                -
              </span>
            </td>
          </tr>
          <tr ng-repeat-end ng-if="((price.headerSummaryPrice && price.summaryInfNewLineDestinationCount === 1) || (price.headerSummaryPrice && !price.newSerie)) && price.actionPricesNumber != undefined && !price.mainMetaInf.showSpeciesValorisationPrices"
            class="{{price.headerCropLine || price.headerSummaryLine || price.headerSummaryPrice ? (price.actionPricesNumber%2 === 0 ? 'main-line-1' : 'main-line') : ''}}">
            <td colspan="12">
              <input type="text"
                     id="price_{{price.type}}{{price.objectId}}_{{price.actionId}}"
                     ng-model="price.globalPrice"
                     ag-float
                     pattern="^[\-\+]?\d*[\.,]?\d*$"/>
              <select id="price_{{price.type}}{{price.objectId}}_price_unit__{{price.actionId}}"
                ng-model="price.globalPriceUnit"
                ng-options="key as value for (key, value) in price.priceUnits">
              </select>
              <input type="button" title="<s:text name='prices-harvest-table-affectPriceToHarvest' />" ng-click="setGlobalPrice(price)"
                value="<s:text name='prices-harvest-table-affectPriceToHarvest' />" />
            </td>
          </tr>
        </tbody>
      </table>
   </div>
</div>

