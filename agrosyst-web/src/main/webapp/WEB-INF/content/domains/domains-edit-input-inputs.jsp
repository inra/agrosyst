<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2022 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<!-- domains-edit-input-inputs.jsp -->
<div id="vuejs-inputs-app">
  <input type="hidden" name="inputStockUnitsJson" :value="JSON.stringify(domainInputs)" />
  <fieldset>
    <legend class="invisibleLabel">{{ $t("messages.domain-edit-input-input-declaration") }}</legend>
    <div class="sub-form clearfix">
      <div class="help-explanation-panel">
        <o-icon size="small" icon="question-circle"></o-icon>
        {{ $t("messages.domain-edit-input-price-are") }} <s:if test='domain.campaign==0'>{{
          $t("messages.domain-edit-input-actually-edited") }}.</s:if>
        <s:else>
          <s:property value='domain.campaign' />
          (<s:property value='(domain.campaign)-1' /> - <s:property value='domain.campaign' />).
        </s:else><br />{{ $t("messages.domain-edit-input-input-will-be-used") }}
      </div>
    </div>

    <div v-for="(inputType, index) in inputTypes" :key="inputType" class="input-list"
      :class="index % 2 === 0 ? 'clear' : 'list-right'">
      <label style="text-align: left;" @click="newInput(inputType)">{{ $t("InputType." + inputType) }}&nbsp;:</label>
      <ul>
        <template v-for="domainInput in sortedInputs()" :key="domainInput.key">
          <li v-if="domainInput.inputType === inputType || (inputType === 'SEMIS' && domainInput.inputType === 'PLAN_COMPAGNE')">
            <span class="float-right">
              <!--<input @click="showMoreInfos($event.target)" type='button' class='btn-icon icon-infos'
                :value="$t('messages.common-more-infos')" :title="$t('messages.common-more-infos')" />-->
              <input @click="editInput(inputType, domainInput)" type='button' class='btn-icon icon-edit'
                :value="$t('messages.common-modify')" :title="$t('messages.common-modify')" />
              <input v-if="isUsed(inputType, domainInput)" type="button" class="btn-icon icon-delete-disabled"
                :value="$t('messages.common-remove')"
                :title="$t('messages.domain-edit-input-cant-remove-because-used')" />
              <input v-else @click="deleteInput(domainInput)" type="button" class="btn-icon icon-delete"
                :value="$t('messages.common-remove')" :title="$t('messages.common-remove')" />
            </span>
            <span class="input-name">
              {{ domainInput.inputName }} <span v-if="domainInput.deprecated">&nbsp;<em class="fa fa-exclamation-triangle" aria-hidden="true" :title="$t('messages.domain-edit-default-other-inputs-warning')"></em></span>
            </span>
            <div class="more-infos">
              <div v-if="domainInput.price">Prix&nbsp;: {{ domainInput.price.price }}{{ $t("PriceUnit." +
                domainInput.price.priceUnit) }}</div>
            </div>
          </li>
        </template>
        <li v-if="showAddButton(inputType)" @click="newInput(inputType)" style="padding: 3px;">
          <a class="add-input-button">
            {{ $t("messages.domain-edit-input-add-an-input") }}
          </a>
        </li>
      </ul>
    </div>
    <div class="input-list">
      <label style="text-align: left;" @click="openWorkforceOperationModal()">{{ $t("messages.domain-edit-input-workforce-operation-inputs") }}&nbsp;:</label>
      <ul>
        <template v-for="domainInput in sortedInputs()" :key="domainInput.key">
          <li v-if="domainInput.inputType === 'CARBURANT' || domainInput.inputType === 'IRRIGATION' || domainInput.inputType === 'MAIN_OEUVRE_MANUELLE' || domainInput.inputType === 'MAIN_OEUVRE_TRACTORISTE'">
            <span class="input-name">
              {{ $t("InputType." + domainInput.inputType) }}
            </span>
            <div class="more-infos">
              <div v-if="domainInput.price">Prix&nbsp;: {{ domainInput.price.price }}{{ $t("PriceUnit." + domainInput.price.priceUnit) }}</div>
            </div>
          </li>
        </template>
        <li @click="openWorkforceOperationModal()" style="padding: 3px;">
          <a class="add-input-button">
            {{ $t("messages.domain-edit-input-edit-workforce-operation-inputs") }}
          </a>
        </li>
      </ul>
    </div>
  </fieldset>

  <div class="sub-form clear clearfix field-separator">
    <div class="fields-in-one-centered-row">
      <label class="middle">{{ $t("messages.domain-edit-input-copy-inputs") }}&nbsp;:&nbsp;</label>
      <a class="btn clearer ng-binding" @click="openCopyModal()">
        <input class="btn-icon noHover icon-square-duplicate" :title="$t('messages.domain-edit-input-copy-inputs')">
      </a>
    </div>
  </div>

  <div class="o-container">
    <domain-inputs-fertilisants-mineraux-modal v-if="selectedInputType === 'APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)"
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-fertilisants-mineraux-modal>
    <domain-inputs-phytosanitaires-modal v-if="selectedInputType === 'APPLICATION_DE_PRODUITS_PHYTOSANITAIRES'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)" :ipmworks=!context.forFrance
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-phytosanitaires-modal>
    <domain-inputs-other-modal v-if="selectedInputType === 'AUTRE'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)"
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-other-modal>
    <domain-inputs-workforce-operation-modal v-if="isWorkforceOperationModalActive"
      :is-open="isWorkforceOperationModalActive" :irrigation-input="editedIrrigationInput" :fuel-input="editedFuelInput"
      :manual-workforce-input="editedManualWorkforceInput" :mechanized-workforce-input="editedMechanizedWorkforceInput"
      @save="saveInputs($event)" @close="closeWorkforceOperationModal()">
    </domain-inputs-workforce-operation-modal>
    <domain-inputs-epandages-organiques-modal v-if="selectedInputType === 'EPANDAGES_ORGANIQUES'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)"
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-epandages-organiques-modal>
    <domain-inputs-lutte-biologique-modal v-if="selectedInputType === 'LUTTE_BIOLOGIQUE'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)"
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-lutte-biologique-modal>
    <domain-inputs-pot-modal v-if="selectedInputType === 'POT'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)"
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-pot-modal>
    <domain-inputs-semis-modal v-if="selectedInputType === 'SEMIS'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)"
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-semis-modal>
    <domain-inputs-substrate-modal v-if="selectedInputType === 'SUBSTRAT'"
      :selected-input-type="selectedInputType" :selected-input="editedInput" :is-used="isUsed(selectedInputType, editedInput)"
      @save="saveInput($event)" @close="closeModal()">
    </domain-inputs-substrate-modal>
  </div>

  <div class="o-container">
    <o-modal :active="isCopyModalActive" @close="closeCopyModal()" :width="904" :can-cancel="['escape']">
      <domains-edit-input-copy-inputs-modal v-if="isCopyModalActive" :domain-inputs="domainInputs"
        @copy="copyInputs($event)" @close="closeCopyModal()"></domains-edit-input-inputs-modal>
    </o-modal>
  </div>
</div>

<!-- Include template of sub components that are not inlined -->
<s:include value="/components/domains/domains-edit-input-copy-inputs-modal.html"></s:include>
<s:include value="/components/domains/form/domains-edit-input-inputs-modal-price-section.html"></s:include>
<s:include value="/components/domains/form/domains-edit-input-inputs-modal-light-price-section.html"></s:include>
<s:include value="/components/domains/domain-inputs-fertilisants-mineraux-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-phytosanitaires-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-other-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-workforce-operation-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-epandages-organiques-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-lutte-biologique-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-pot-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-semis-modal.html"></s:include>
<s:include value="/components/domains/domain-inputs-substrate-modal.html"></s:include>
