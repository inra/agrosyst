<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>Domaines</title>
    <content tag="current-category">contextual</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/agrosyst-domain-list.js'/><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">
        angular.module('DomainsList', ['Agrosyst']).
            value('ContextFilterInit', {}).
            value('ListDomainsInitData', <s:property value="toJson(domains)" escapeHtml="false"/>).
            value('domainFilter', <s:property value="toJson(domainFilter)" escapeHtml="false"/>).
            value('asyncThreshold', <s:property value="exportAsyncThreshold"/>);
    </script>
  </head>
  <body>
    <div ng-app='DomainsList' ng-controller="DomainsListController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; Domaines</li>
        </ul>
      </div>

      <ul class="actions">
        <li>
          <a class="action-ajouter" href="<s:url namespace='/domains' action='domains-edit-input' />">Créer un domaine</a>
        </li>
      </ul>

      <ul class="float-right actions">
        <li><a class="action-desactiver" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
          onclick="domainChangeActivateStatus($('#confirmUnactivateDomains'), $('#domainsListForm'), false);return false;" ng-if="allSelectedDomainActive">Désactiver</a>
          <a class="action-activer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
          onclick="domainChangeActivateStatus($('#confirmUnactivateDomains'), $('#domainsListForm'), true);return false;" ng-if="!allSelectedDomainActive">Activer</a></li>
        <li><a class="action-prolonger" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) != 1}"
          onclick="domainExtend($('#confirmExtendDomains'), $('#domainsListForm'));return false;">Prolonger</a></li>
        <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls"
               ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
               onclick="domainExport($('#domainsListForm'));return false;">Export XLS</a></li>
        <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
                ng-click="asyncDomainExport()">Export XLS</a></li>
         <li><a class="action-import-edaplos" onclick="displayDialogAndSubmitForm($('#confirmImportEdaplosDialog'));return false;">Import eDaplos</a></li>
        <s:if test="currentUserAnAdmin">
          <li><a class="action-supprimer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) != 1}"
            onclick="domainDelete($('#confirmDeleteDomain'), $('#domainsListForm'));return false;">Supprimer</a></li>
        </s:if>
      </ul>

      <form method="post" id="domainsListForm" name="domainListForm">
        <input type="hidden" name="domainIds" value="{{selectedEntities|toSelectedArray}}" />
        <input type="hidden" name="extendCampaign" value="{{extendCampaign}}" ng-if="extendCampaign" />

        <table class="entity-list clear fixe_layout" id="domains-list-table">
          <thead>
            <tr class="doubleHeight">
              <th scope="col" class="column-tiny-fixe"
                  title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Désélectionner toute la page' || 'Sélectionner toute la page'}}">
                  <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
              </th>
              <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('DOMAIN')">
                <sort>
                  <header_label>Exploitation ou Domaine expérimental</header_label>
                  <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                  <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-small" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne">
                <sort>
                  <header_label>Camp.</header_label>
                  <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                  <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-normal" ng-click="changeSort('MAIN_CONTACT')">
                <sort>
                  <header_label>Interlocuteur principal</header_label>
                  <em ng-if="filter.sortedColumn == 'MAIN_CONTACT'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.MAIN_CONTACT, 'fa fa-sort-amount-desc ':sortColumn.MAIN_CONTACT}" ></em>
                  <em ng-if="filter.sortedColumn != 'MAIN_CONTACT'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-large">
                Type
              </th>
              <th scope="col" class="column-xllarge-fixed">
                Responsable
              </th>
              <th scope="col" class="column-xsmall" ng-click="changeSort('DEPARTEMENT')" title="Département">
                <sort>
                  <header_label>Dép.</header_label>
                  <em ng-if="filter.sortedColumn == 'DEPARTEMENT'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DEPARTEMENT, 'fa fa-sort-amount-desc ':sortColumn.DEPARTEMENT}" ></em>
                  <em ng-if="filter.sortedColumn != 'DEPARTEMENT'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-normal" ng-click="changeSort('SIRET')">
                <sort>
                  <header_label>Siret</header_label>
                  <em ng-if="filter.sortedColumn == 'SIRET'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.SIRET, 'fa fa-sort-amount-desc ':sortColumn.SIRET}" ></em>
                  <em ng-if="filter.sortedColumn != 'SIRET'" class='fa fa-sort' ></em>
                </sort>
              </th>
              <th scope="col" class="column-xsmall">
                État
              </th>
            </tr>
            <tr>
              <td class="column-tiny-fixe"></td>
              <td class="column-xxslarge-fixed"><input type="text" ng-model="filter.domainName" /></td>
              <td class="column-small"><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" /></td>
              <td class="column-normal"><input type="text" ng-model="filter.mainContact" /></td>
              <td class="column-large">
                <select ng-model="filter.type">
                  <option value="" />
                  <s:iterator value="types">
                    <option value="<s:property value="key" />"><s:property value="value" /></option>
                  </s:iterator>
                </select></td>
              <td class="column-xllarge-fixed"><input ng-model="filter.responsable" type="text" /></td>
              <td class="column-xsmall"><input ng-model="filter.departement" type="text" /></td>
              <td class="column-normal"><input ng-model="filter.siret" type="text" /></td>
              <td>
                <select ng-model="filter.active">
                  <option value="" />
                  <option value="true">Actif</option>
                  <option value="false">Inactif</option>
                </select>
              </td>
            </tr>
          </thead>

          <tbody>
            <tr ng-show="domains.length == 0"><td colspan="8" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>

            <tr ng-repeat="domain in domains"
                ng-class="{'line-selected':selectedEntities[domain.topiaId]}">
              <td class="column-tiny-fixe">
                <input type='checkbox' ng-model="selectedEntities[domain.topiaId]" ng-checked="selectedEntities[domain.topiaId]" ng-click="toggleSelectedEntity(domain.topiaId)"/>
              </td>
              <td class="column-xxslarge-fixed" title="{{domain.name}}">
                <a href="<s:url namespace='/domains' action='domains-edit-input' />?domainTopiaId={{domain.topiaId|encodeURIComponent}}">{{domain.name}}</a>
              </td>
              <td class="column-small center" title="{{domain.campaign}} ({{domain.campaign-1}} - {{domain.campaign}})">{{domain.campaign}}</td>
              <td class="column-normal">{{domain.mainContact}}</td>
              <td class="column-large">{{domainTypes[domain.type]}}</td>
              <td class="column-xllarge-fixed">{{domain.responsibles|displayArrayProperties:['firstName','lastName']|orDash}}</td>
              <td class="column-xsmall">{{domain.location.departement}}</td>
              <td class="column-normal">{{domain.siret}}</td>
              <td>{{domain.active ? 'Actif' : 'Inactif'}}</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="9">
                <div class="table-footer">
                  <div class="entity-list-info">
                    <span class="counter">{{pager.count}} domaines</span>
                    <span>
                      - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les domaines</a>
                    </span>
                    <span>
                      - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                    </span>
                    <span class="selection">
                      - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                    </span>
                  </div>
                  <div class="pagination">
                    <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                  </div>
                </div>
              </td>
            </tr>
          </tfoot>
        </table>

      </form>

      <div id="confirmUnactivateDomains" title="Activation/Désactivation des domaines" class="auto-hide">
        Êtes-vous sûr(e) de vouloir {{allSelectedDomainActive?'désactiver':'activer'}}
        <ng-pluralize count="(selectedEntities|toSelectedLength)"
             when="{'one': 'le domaine {{firstSelectedDomain.name}}',
                     'other': 'les {} domaines sélectionnés'}"></ng-pluralize> ?
      </div>

      <div id="confirmDeleteDomain" title="Suppression du domaine" class="auto-hide">
        Êtes-vous sûr(e) de vouloir supprimer le domaine {{firstSelectedDomain.name}} ({{firstSelectedDomain.campaign}}) ?
      </div>

      <div id="confirmExtendDomains" title="Prolongation de domaine" class="auto-hide">
        <span class="ui-icon ui-icon-wrench" style="float: left; margin: 0 7px 80px 0;"></span>
        Prolonger le domaine : {{firstSelectedDomain.name}}<br />
        <hr />
        Nouvelle campagne du domaine : <input id="extendCampaignField" ng-model="extendCampaign" size="4" ng-pattern="/^[0-9]+$/" />
      </div>

      <div id="confirmImportEdaplosDialog" title="Import eDaplos" class="auto-hide">
        <form action="<s:url action='domains-pre-import-edaplos' />" method="post" enctype="multipart/form-data">
          <p>Fichier eDaplos (zip ou fichier xml) : <input type="file" name="file" /></p>
        </form>
      </div>

    </div>

  </body>
</html>
