<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <script type="text/javascript" src="<s:url value='/nuiton-js/networks.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript" src="<s:url value='/nuiton-js/domains.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">
      angular.module('NetworksEditModule', ['Agrosyst','ui.date'])
          .value('managers', <s:property value="toJson(managers)" escapeHtml="false"/>)
          .value('network', <s:property value='toJson(network)' escapeHtml='false'/>)
          .value('growingSystemIndicators', <s:property value="toJson(indicators.growingSystems)" escapeHtml="false"/>);
    </script>

    <s:if test="!activated">
      <script type="text/javascript">
        angular.element(document).ready(
          function () {
            addPermanentWarning("Le réseau sur lequel vous travaillez est inactif. Réactivez le pour pouvoir y apporter des modifications.");
          }
        );
      </script>
    </s:if>

    <s:if test="network.topiaId == null">
      <title>Nouveau réseau</title>
    </s:if>
    <s:else>
      <title>Réseau '<s:property value="network.name" />'</title>
    </s:else>
    <content tag="current-category">contextual</content>
  </head>
  <body>
    <div ng-app="NetworksEditModule" ng-controller="NetworkMainController" class="page-content">

      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url action='networks-list' namespace='/networks' />">Réseaux</a></li>
          <s:if test="network.topiaId == null">
            <li>&gt; Nouveau réseau</li>
          </s:if>
          <s:else>
            <li>&gt; <s:property value="network.name" /></li>
          </s:else>
        </ul>
      </div>

      <ul class="actions">
        <li><a class="action-retour" href="<s:url action='networks-list' namespace='/networks' />">Retour à la liste des réseaux</a></li>
      </ul>

      <ul class="float-right informations">
        <s:if test="%{network.topiaId != null}">
          <li class="highlight"><span class="label">Pièces jointes</span><a id="attachmentLink" class="action-attachements" onclick="displayEntityAttachments('<s:property value='network.topiaId' />')" title="Voir les pièces jointes"><s:property value="getAttachmentCount(network.topiaId)" /></a></li>
        </s:if>
      </ul>

      
      <form name="networksEditForm" action="<s:url action='networks-edit' namespace='/networks' />" method="post" class="tabs clear" ag-confirm-on-exit>
        <s:actionerror cssClass="send-toast-to-js"/>
        <s:hidden id="networkTopiaId" name="networkTopiaId" value="%{network.topiaId}"/>
        <s:hidden id="newNetworkManagers" name="networkManagerDtoJson" value="{{currentNetworkManagers}}"/>
        <s:hidden id="parents" name="parentIdsJson" value="{{getParentIds()}}"/>

        <ul id="tabs-networks-menu" class="tabs-menu clearfix">
          <li class="selected"><span>Généralités</span></li><!--
        --><li><span>Indicateurs</span></li>
        </ul>

        <div id="tabs-networks-content" class="tabs-content">

          <!-- Génératlités -->
          <div id="tab_0">
            <fieldset>
              <s:fielderror fieldName="name"/>
              <div class="wwarn clearfix" ng-show="alreadyExistingNetwork">
                <div class="wwlbl"></div>
                <div class="messages-panel">
                  <ul class="warningMessage">
                    <li>Un réseau existe déjà avec ce nom</li>
                  </ul>
                </div>
              </div>
              <s:textfield id="name"
                    ng-model="network.name" name="network.name" value="%{network.name}"
                    labelSeparator=" :"
                    labelPosition="left" label="Nom du réseau" placeholder="ex. : R1"
                    required="true" requiredLabel="true"/>

              <s:textfield id="codeConventionDephy"
                    ng-model="network.codeConventionDephy"
                    name="network.codeConventionDephy"
                    value="%{network.codeConventionDephy}"
                    labelPosition="left"
                    labelSeparator=" :"
                    label="Code convention DEPHY"
                    placeholder="ex. : A0B1C2D3"
                    pattern="\w{8}"/>

              <s:textarea name="network.description" label="Description" labelSeparator=" :" ng-model="network.description"/>

              <div class="wwgrp">
                <s:fielderror fieldName="parent" />
                <span class="wwlbl"><label for="network_parents" class="label">Réseau(x) parent(s)&nbsp;:</label></span>
                <span class="wwctrl" >
                  <div ng-repeat="parent in parents" class="tag">
                    <span>{{parent.name}}</span>
                    <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="removeParent(parent)"/> 
                  </div>
                  <span class='contextual-help'>
                    <span class='help-hover'>
                      <s:text name="help.network.parents" />
                    </span>
                  </span>
                  <input id="network_parents" class="clear float-left" type="text" placeholder="Tapez le nom d'un réseau pour l'ajouter comme parent"/>
                </span>
              </div>
              
              <div class="table-enclosure marginTop30">
                <label>Responsable(s) du réseau</label>
                <table id="NetworkManagers" class="data-table fixed-layout" >
                  <thead>
                    <tr>
                      <th scope="col">Prénom</th>
                      <th scope="col">Nom</th>
                      <th scope="col">De</th>
                      <th scope="col">Jusqu'au</th>
                      <th scope="col" class="column-xsmall">État</th>
                    </tr>
                  </thead>
                  <tbody id="NetworkManagersBody">
                    <tr ng-repeat="networkManager in currentNetworkManagers |filter: {active: 'true'} " ng-click="editNetworkManager(networkManager)" class="selectCursor">
                      <td>
                        {{networkManager.user.firstName}}
                      </td>
                      <td>
                        {{networkManager.user.lastName}}
                      </td>
                      <td>
                        {{networkManager.fromDate | date:'dd/MM/yyyy' | orDash}}
                      </td>
                      <td>
                        {{networkManager.toDate | date:'dd/MM/yyyy' | orDash}}
                      </td>
                      <td ng-switch="networkManager.active">
                        <span ng-switch-when="true">Actif</span>
                        <span ng-switch-when="false">Inactif</span>
                      </td>
                    </tr>

                    <tr ng-repeat="networkManager in currentNetworkManagers |filter: {active: 'false'}" ng-click="editNetworkManager(networkManager)" ng-show="showInactivesManagers || nbActiveManagers === 0" >
                      <td>
                        {{networkManager.user.firstName}}
                      </td>
                      <td>
                        {{networkManager.user.lastName}}
                        <!-- for HTML5 validation -->
                        <input ng-show='!networkManager.user'
                               id="user_error_{{$index}}"
                               type="text"
                               ng-model="noValidUser"
                               ng-required="!networkManager.user"
                               style="opacity:0;width: 0;"/>
                      </td>
                      <td>
                        {{networkManager.fromDate | date:'dd/MM/yyyy' | orDash}}
                      </td>
                      <td>
                        {{networkManager.toDate | date:'dd/MM/yyyy'}}
                      </td>
                      <td ng-switch="networkManager.active">
                        <span ng-switch-when="true">Actif</span>
                        <span ng-switch-when="false">Inactif</span>
                      </td>
                    </tr>

                  </tbody>
                  <tfoot>
                    <tr>
                      <td>
                        <div class="table-end-button float-left" ng-show="showAddManagerButton()">
                          <input type="button" value="{{ (nbActiveManagers === 0) && 'Il n\'y a pas de responsable actif pour ce réseau !' || showInactivesManagers && 'Cacher les responsables inactifs' || 'Afficher les responsables inactifs'}}" ng-click="showInactives()" class="btn-see"/>
                        </div>
                      </td>
                      <td colspan="4">
                        <div class="table-end-button">
                          <s:if test="!readOnly">
                            <input type="button" value="Ajouter" ng-click="addManager()"/>
                          </s:if>
                        </div>
                      </td>
                    </tr>
                  </tfoot>
                </table>
              </div>

              <s:fielderror fieldName="user"/>
              <div ng-form="addNetworkManagerForm">
                <div class="slide-animation sub-form" ng-show="currentEditedManager" >
                  <label class="sub-form-title">Ajout d'un utilisateur au réseau</label>
                  <div class="sub-form-content">
                    <div class="wwgrp" >
                      <span class="wwlbl">
                        <label class="historicLabelStyle" for="network_user"><span class="required">*</span> Utilisateur</label>
                      </span>
                      <span class="wwctrl">
                        <input id="network_user" type="text"
                               ng-model="currentEditedManagerUserName"
                               placeholder="ex. : Annie Verssaire"
                               ng-required="currentEditedManager"/>
                      </span>
                    </div>

                    <div class="wwgrp" ng-if="currentEditedManager">
                      <span class="wwlbl">
                        <label class="historicLabelStyle" for="userFromDate">Période de</label>
                      </span>
                      <span class="wwctrl">
                        <input id="userFromDate" type="text" ui-date="dateOptions" ng-model="currentEditedManager.fromDate" />
                      </span>
                    </div>

                    <div class="wwgrp" ng-if="currentEditedManager">
                      <span class="wwlbl">
                        <label class="historicLabelStyle" for="userToDate">jusqu'à</label>
                      </span>
                      <span class="wwctrl">
                        <input id="userToDate" type="text" ui-date="dateOptions" ng-model="currentEditedManager.toDate" />
                      </span>
                    </div>

                    <div class="wwgrp" ng-if="currentEditedManager">
                      <label class="historicLabelStyle" for="active">Actif</label>
                      <input id="active" type="checkbox" ng-model="currentEditedManager.active"/>
                    </div>
                    <span class="button-actions clearfix">
                      <input type="button" value="Valider" class="btn btn-darker float-right"
                        <s:if test="readOnly">ng-disabled="true" title="Vous n'avez pas les droits nécessaires"</s:if>
                        <s:else>ng-disabled="!isFormValid()" ng-click="saveAddedNetworkManarger()" </s:else>
                      />
                      <input type="button" value="Annuler" ng-click="cancelAddedNetworkManarger()" class="btn float-left"/>
                    </span>
                  </div>
                </div>
              </div>

            </fieldset>
          </div>

          <!-- Indicateurs -->
          <div id="tab_1" class="stats-panel">
            <div class="data-table-title">
              <h3>Indicateurs de fonctionnement du réseau</h3>
            </div>
                
            <ul class="stats-list">
              <li>
                <div class="title">Domaines</div>
                <span class="indicator"><s:property value="indicators.activeDomainsCount" /></span> actifs
                / <span class="indicator"><s:property value="indicators.domainsCount" /></span>
              </li>
              <li>
                <div class="title">Dispositifs</div>
                <span class="indicator"><s:property value="indicators.activeGrowingPlansCount" /></span> actifs
                / <span class="indicator"><s:property value="indicators.growingPlansCount" /></span>
              </li>
              <li>
                <div class="title">Sous-réseaux</div>
                <span class="indicator"><s:property value="indicators.activeSubNetworksCount" /></span> actifs
                / <span class="indicator"><s:property value="indicators.subNetworksCount" /></span>
              </li>
            </ul>
            <table class="data-table">
              <thead>
                <tr>
                  <th scope="col">Systèmes de culture</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Actifs</th>
                  <th scope="col">Validés</th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="gsIndicator in growingSystemIndicators" >
                  <td ng-if="gsIndicator.typeDephy">{{gsIndicator.typeDephy}}</td>
                  <td ng-if="gsIndicator.sector">{{sectors[gsIndicator.sector]}}</td>
                  <td ng-if="!gsIndicator.sector && !gsIndicator.typeDephy">Total</td>
                  <td class="indicator">{{gsIndicator.count}}</td>
                  <td class="indicator">{{gsIndicator.active}}</td>
                  <td class="indicator">{{gsIndicator.validated}}</td>
                </tr>
              </tbody>
            </table>
        
            <div class="help-explanation-panel clear">
              <strong>Les chiffres ci-dessus (domaines, dispositifs et systèmes de culture) tiennent compte des campagnes. Une entité qui existe sur 3 campagnes, sera compatibilisé 3 fois.</strong>
            </div>
          </div>

          <span class="form-buttons">
            <a class="btn-secondary" href="<s:url action='networks-list' namespace='/networks' />">Annuler</a>
            <input type="submit" class="btn-primary" value="Enregistrer"
              <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>
              <s:if test="!activated">disabled="disabled" title="Le réseau sur lequel vous travaillez est inactif. Réactivez le pour pouvoir y apporter des modifications."</s:if>
            />
          </span>

        </div>

      </form>

    </div>
  </body>
</html>
