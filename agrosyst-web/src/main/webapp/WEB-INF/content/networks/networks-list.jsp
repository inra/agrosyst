<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <title>Réseaux</title>
     <content tag="current-category">contextual</content>
     <script type="text/javascript" src="<s:url value='/nuiton-js/networks.js' /><s:property value='getVersionSuffix()'/>"></script>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/networks.css' /><s:property value='getVersionSuffix()'/>" />
     <script type="text/javascript">
         angular.module('NetworksList', ['Agrosyst'])
            .value('ContextFilterInit', {})
            .value('ListNetworksInitData', <s:property value="toJson(networks)" escapeHtml="false"/>)
            .value('networkFilter', <s:property value="toJson(networkFilter)" escapeHtml="false"/>);

         <s:if test="graphNetworks != null">
           var data = {
             "networks": <s:property value="toJson(graphNetworks)" escapeHtml="false" />,
             "connections":  <s:property value="toJson(graphConnections)" escapeHtml="false" />
           }

           $(function() {
              $("#networksDiagram").networkDiagram({"data": data,
                networkClickCallback: function(event) {
                  var topiaId = event.data.id.replace(/-(?=.+_[^_]+$)/g, ".");
                  location.href="<s:url namespace='/networks' action='networks-edit-input' />?networkTopiaId=" + topiaId;
                }
              });
           });
         </s:if>
     </script>
  </head>
  <body>
    <div ng-app="NetworksList" ng-controller="NetworksListController">
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Réseaux</li>
      </ul>
    </div>

    <ul class="actions">
      <li>
        <a class="action-ajouter" href="<s:url namespace='/networks' action='networks-edit-input' />">Créer un réseau</a>
      </li>
    </ul>
    
    <ul class="float-right actions">
      <li><a class="action-desactiver" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
        onclick="networkUnactivate($('#confirmUnactivateNetworks'), $('#networkListForm'), false);return false;" ng-if="allSelectedNetworkActive">Désactiver</a>
        <a class="action-activer" ng-class="{'button-disabled':(selectedEntities|toSelectedLength) == 0}"
        onclick="networkUnactivate($('#confirmUnactivateNetworks'), $('#networkListForm'), true);return false;" ng-if="!allSelectedNetworkActive">Activer</a></li>
      <%--<li><a class="action-export-pdf button-disabled">Export PDF</a></li>
      <li><a class="action-export-xls button-disabled">Export XLS</a></li> --%>
    </ul>

    <form method="post" id="networkListForm">
      <input type="hidden" name="networkIds" value="{{selectedEntities|toSelectedArray}}" />
      <table class="entity-list clear" id="networks-list-table">
        <thead>
          <tr>
            <th scope="col" class="column-tiny"
                title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
            </th>
            <th scope="col" class="column-xxlarge-fixed" ng-click="changeSort('NETWORK')">
              <sort>
                <header_label>Réseau</header_label>
                <em ng-if="filter.sortedColumn == 'NETWORK'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.NETWORK, 'fa fa-sort-amount-desc ':sortColumn.NETWORK}" ></em>
                <em ng-if="filter.sortedColumn != 'NETWORK'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col">Responsables</th>
            <th scope="col" class="column-xsmall">État</th>
          </tr>
          <tr>
            <td />
            <td><input type="text" ng-model="filter.networkName" /></td>
            <td><input type="text" ng-model="filter.networkManager" /></td>
            <td>
              <select ng-model="filter.active">
                <option value="" />
                <option value="true">Actif</option>
                <option value="false">Inactif</option>
              </select>
            </td>
          </tr>
        </thead>
  
        <tbody>
          <tr ng-show="networks.length == 0"><td colspan="4" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>
          <tr ng-repeat="network in networks"
              ng-class="{'line-selected':selectedEntities[network.topiaId]}">
            <td>
              <input type='checkbox' ng-model="selectedEntities[network.topiaId]" ng-checked="selectedEntities[network.topiaId]" ng-click="toggleSelectedEntity(network.topiaId)"/>
            </td>
            <td>
              <a href="<s:url namespace='/networks' action='networks-edit-input'/>?networkTopiaId={{network.topiaId|encodeURIComponent}}">{{network.name}}<span ng-if="!network.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td>
              <a href="<s:url namespace='/networks' action='networks-edit-input'/>?networkTopiaId={{network.topiaId|encodeURIComponent}}">{{getManagers(network)}}</a>
            </td>
            <td>{{network.active ? 'Actif' : 'Inactif'}}</td>
          </tr>
        </tbody>
        
        <tfoot>
          <tr>
            <td colspan="4">
              <div class="table-footer">
                <div class="entity-list-info">
                  <span class="counter">{{pager.count}} réseaux</span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les réseaux</a>
                  </span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                  </span>
                  <span class="selection">
                    - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                  </span>
                </div>
                <div class="pagination">
                  <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                </div>
              </div>
            </td>
          </tr>
        </tfoot>
      </table>

      <s:if test="graphNetworks == null">
        <div class="disabledGraph">Le graphique des réseaux est désactivé au delà de ${networkDisplayLimit} réseaux.</div>
      </s:if>
    </form>

    <div id="networksDiagram" class="positionRelative"></div>

    <div id="confirmUnactivateNetworks" title="Activer/Désactiver des réseaux" class="auto-hide">
      Êtes-vous sûr(e) de vouloir {{allSelectedNetworkActive?'désactiver':'activer'}}
      <ng-pluralize count="(selectedEntities|toSelectedLength)"
           when="{'one': 'le réseau {{firstSelectedNetwork.name}}',
                   'other': 'les {} réseaux sélectionnés'}"></ng-pluralize> ?
    </div>

    </div>
  </body>
</html>
