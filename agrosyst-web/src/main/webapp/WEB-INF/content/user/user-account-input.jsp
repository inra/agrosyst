<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
     <script type="text/javascript">
         function displayConfirmPassword() {
           var res = $('input[name=password]', '#userAccountForm').val();
           if (res && res.length > 0) {
             $('#confirmPassword').slideDown();
           } else {
             $('#wwgrp_confirmPassword').slideUp();
           }
         }
         displayConfirmPassword();

         function showWrongItEmailCallback(data, textStatus, jqXHR) {
           if (textStatus !== "success" || !data) {
             displayWrongEmail();
           }
         }

         function validItEmail() {
             var itEmail = $("input[name='user.itEmail']", '#userAccountForm').val();
             if (itEmail.length > 0) {
               var emailValidationObject = {email: itEmail};
               $.post(ENDPOINTS.validEmailJson, emailValidationObject, showWrongItEmailCallback, "json");
             }
         }

     </script>
     <title>Compte personnel</title>
     <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/agrosyst-admin.css' />" />
  </head>
  <body>
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Compte personnel</li>
      </ul>
    </div>

    <ul class="actions">

    </ul>

    <s:actionmessage/>
    <s:actionerror cssClass="send-toast-to-js"/>

    <form id="userAccountForm" action="<s:url action='user-account' namespace='/user' />" method="post" class="tabs clear" ag-confirm-on-exit>
      
      <ul id="tabs-profil-menu" class="tabs-menu clearfix">
        <li class="selected"><span>Profil</span></li>
        <li><span>Rôle(s)</span></li>
      </ul>

      <div id="tabs-profil-content" class="tabs-content">
        <div>
          <fieldset>
            <s:textfield type="email" label="Email" name="user.email" labelPosition="left" labelSeparator=" :" placeholder="ex. : prenom.nom@inrae.fr" required="true" requiredLabel="true"/>
            <s:password label="Mot de passe (ne remplir que si vous souhaitez modifier le mot de passe)" onchange="displayConfirmPassword()" name="password" labelPosition="left" labelSeparator=" :" autocomplete="off"/>
            <s:password id="confirmPassword" label="Mot de passe (confirmer)" name="confirmPassword" labelPosition="left" labelSeparator=" :" autocomplete="off"/>
            <s:textfield label="Prénom" name="user.firstName" labelPosition="left" labelSeparator=" :" placeholder="ex. : Prénom" required="true" requiredLabel="true" autocomplete="off" />
            <s:textfield label="Nom" name="user.lastName" labelPosition="left" labelSeparator=" :" placeholder="ex. : Nom" required="true" requiredLabel="true" autocomplete="off" />
            <s:textfield label="Organisation" name="user.organisation" labelPosition="left" labelSeparator=" :" placeholder="ex. : INRAE" autocomplete="off" />
            <s:textfield type="email" id="user_itEmail" onChange="validItEmail()" label="Adresse mail de votre IT" name="user.itEmail" labelPosition="left" labelSeparator=" :" placeholder="ex. : prenom.nom@inrae.fr"/>
            <s:textfield label="Numéro de téléphone" name="user.phoneNumber" labelPosition="left" labelSeparator=" :" placeholder="ex. : 01.02.03.04.05" autocomplete="off" />
            <s:select label="Langue" name="user.userLang" value="%{user.userLang}" labelPosition="left" labelSeparator=" :"
              list="languages" listKey="trigram" listValue="names" />
          </fieldset>

          <fieldset>
            <div class="wwgrp">
              <s:fielderror fieldName="user.banner" />
              <span class="wwlbl">
                <label for="userBannerField">Bandeau :</label>
              </span>
              <span class="wwctrl">
                <s:iterator value="bannerMap" var="bannerItem">
                  <input id="banner${key}Field" type="radio" name="user.banner" value="${key}"
                    <s:if test="#bannerItem.key.equals(user.banner)">checked</s:if> />
                  <label for="banner${key}Field">
                    <img src="<s:url value='/' />${value[0]}" class="full-width"/>
                  </label>
                </s:iterator>
              </span>
            </div>
          </fieldset>

          <span class="form-buttons">
            <input type="submit" class="btn-primary" value="Enregistrer" />
          </span>
        </div>

        <!-- Rôles -->

        <div class="table-enclosure clear">
          <table class="data-table inline-edition full-width" id="users-list-table">
            <thead>
              <tr>
                <th scope="col" class="column-xlarge-fixed">Rôle</th>
                <th scope="col">Entité</th>
              </tr>
            </thead>

            <tbody>
              <s:iterator value="userRoles" var="role">
                <tr>
                  <td class="column-xlarge-fixed">${roleTypes[role.type]}</td>
                  <td>
                    <s:if test="%{ #role.entity.label != null }">${role.entity.label}</s:if>
                    <s:else>-</s:else>
                    <s:if test="%{ isToutesCampagnes(#role) }"> (Toutes campagnes)</s:if>
                    <s:if test="%{ isRoleCampagne(#role) }"> (${role.entity.campaign})</s:if>
                  </td>
                </tr>
              </s:iterator>
            </tbody>
          </table>
        </div>
      </div>
    </form>
  </body>
</html>
