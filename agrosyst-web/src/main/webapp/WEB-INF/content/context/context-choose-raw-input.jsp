<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2013 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Agrosyst : Choix du contexte de navigation</title>

    <!--script type="text/javascript" src="<s:url value='/js/endpoints-js.jsp' />"></script>
    <script type="text/javascript" src="<s:url value='/nuiton-js/agrosyst.js' /><s:property value='getVersionSuffix()'/>"></script-->

    <script type="text/javascript">
      angular.element(document).ready(function() {
          angular.module('NavigationContext', ['Agrosyst', 'ui.switch']).
            value('ContextFilterInit', {
                selectedCampaigns: <s:property value="toJson(selectedCampaigns)" escapeHtml="false" />,
                selectedNetworks: <s:property value="toJson(selectedNetworks)" escapeHtml="false" />,
                selectedDomains: <s:property value="toJson(selectedDomains)" escapeHtml="false" />,
                selectedGrowingPlans: <s:property value="toJson(selectedGrowingPlans)" escapeHtml="false" />,
                selectedGrowingSystems: <s:property value="toJson(selectedGrowingSystems)" escapeHtml="false" />,
                fromNavigationContextChoose: true
            }).
            value('ListCampaignsInitData', <s:property value="toJson(campaigns)" escapeHtml="false"/>).
            value('ListNetworksInitData', <s:property value="toJson(networks)" escapeHtml="false"/>).
            value('ListDomainsInitData', <s:property value="toJson(domains)" escapeHtml="false"/>).
            value('ListGrowingPlansInitData', <s:property value="toJson(growingPlans)" escapeHtml="false"/>).
            value('ListGrowingSystemsInitData', <s:property value="toJson(growingSystems)" escapeHtml="false"/>).
            value('domainTypes', <s:property value="toJson(domainTypes)" escapeHtml="false"/>).
            value('sectors', <s:property value="toJson(sectors)" escapeHtml="false"/>).
            value('dephyTypes', <s:property value="toJson(dephyTypes)" escapeHtml="false"/>).
            value('networkFilter', { active: true }).
            value('growingSystemFilter', { active: true }).
            value('growingPlanFilter', { active: true }).
            value('domainFilter', { active: true }).
            value('asyncThreshold', 0);
          angular.bootstrap($('#navigation-context-form'), ['NavigationContext']);
      });

      $(function() {
        $('.context-element .context-element-title').click(function() {
          toggleContextElement($(this));
        });
        $('.context-element .context-element-opener').click(function() {
          toggleContextElement($(this));
        });
      });
    </script>
  </head>
  <body class="agrosyst-body">
    <form id="navigation-context-form"
          ng-app="NavigationContext"
          ng-controller="NavigationContextController">

      <fieldset>
        <div class="header-paragraph">
          <p>Les éléments que vous sélectionnez ci-dessous conditionnent votre navigation au sein du système
            d’information Agrosyst. Par exemple, si vous sélectionnez un domaine et un système de culture, l'ensemble
            des informations qui s'afficheront seront contextuelles de ce domaine et ce système de culture.</p>
          <p>Lorsque vous modifiez le contexte de navigation, chaque élément que vous sélectionnez applique un filtre
            sur les éléments suivants.</p>
          <p>Pour supprimer tous les filtres, cliquez sur 'Tout désélectionner' puis 'Appliquer' en bas de cette page.</p>
        </div>

        <!-- Campagnes -->
        <div class="context-element campaign">
          <a class="context-element-opener"></a>
          <span class="context-element-title">
            <label for="navigation-context-campaign">Campagne(s) (année de récolte) 
              <span class="count" ng-show="contextFilter.selectedCampaigns|toSelectedLength">
                {{contextFilter.selectedCampaigns|toSelectedLength}}
              </span>
            </label>
          </span>
          <div class="context-element-content">
            <table class="entity-list clear full-width" id="navigation-context-campaign-list">
              <thead>
                <tr>
                  <th scope="col"
                      class="column-tiny"
                      title="{{(contextFilter.selectedCampaigns|toSelectedLength) == campaigns.length && 'Tout désélectionner' || 'Tous sélectionner'}}">
                    <input type='checkbox'
                           ng-click="toggleCampaigns()"
                           ng-checked="(contextFilter.selectedCampaigns|toSelectedLength) == campaigns.length"/>
                  </th>
                  <th scope="col">Campagne</th>
                </tr>
              </thead>

              <tbody>
                <tr ng-repeat="campaign in campaigns"
                    ng-class="{'line-selected': contextFilter.selectedCampaigns[campaign]}">
                  <td class="column-xsmall">
                    <input type='checkbox'
                           ng-model="contextFilter.selectedCampaigns[campaign]"
                           ng-checked="contextFilter.selectedCampaigns[campaign]"
                           ng-click="toggleCampaign(campaign)" />
                  </td>
                  <td>{{campaign}}</td>
                </tr>
              </tbody>
              
              <tfoot>
                <tr>
                  <td class="entity-list-info" colspan="5">
                    <span class="counter">{{campaigns.length}} campagnes</span>
                    <span>
                      - <a ng-class="{'active':(contextFilter.selectedCampaigns|toSelectedLength) < campaigns.length}"
                           ng-click="selectAllCampaigns()" href="">
                           Sélectionner toutes les campagnes
                        </a>
                    </span>
                    <span>
                      - <a ng-class="{'active':(contextFilter.selectedCampaigns|toSelectedLength) > 0}"
                           ng-click="clearSelectedCampaigns()" href="">
                           Tout désélectionner</a>
                    </span>
                    <span class="selection">
                      - <ng-pluralize
                         count="(contextFilter.selectedCampaigns|toSelectedLength)"
                         when="{'0': 'Aucune sélectionnée', '1': '{} sélectionnée', 'other': '{} sélectionnées'}" />
                    </span>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>

        <!-- Networks -->
        <div class="context-element networkContext" ng-controller="NetworksListController">
          <a class="context-element-opener"></a>
          <span class="context-element-title">
            <label>Réseau(x)
              <span class="count"
                    ng-show="selectedEntities|toSelectedLength">
                {{selectedEntities|toSelectedLength}}
              </span>
            </label>
          </span>
          <div class="context-element-content">
            <table class="entity-list clear full-width" id="navigation-context-networks-list">
              <thead>
                <tr>
                  <th scope="col"
                      class="column-tiny"
                      title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                    <input type='checkbox'
                           ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected"
                           ng-change="toggleSelectedEntities()" />
                  </th>
                  <th scope="col">Réseau</th>
                  <th scope="col">Responsables</th>
                  <th scope="col">État</th>
                </tr>
                <tr>
                  <td>
                  </td>
                  <td><input type="text" ng-model="filter.networkName" /></td>
                  <td><input type="text" ng-model="filter.networkManager" /></td>
                  <td class="column-small">
                    <select ng-model="filter.active">
                      <option value="" />
                      <option value="true">Actif</option>
                      <option value="false">Inactif</option>
                    </select>
                  </td>
                </tr>
              </thead>

              <tbody>
                <tr ng-show="networks.length == 0">
                  <td colspan="4" class="empty-table">
                    Aucun résultat ne correspond à votre recherche
                  </td>
                </tr>
                <tr ng-repeat="network in networks"
                    ng-class="{'line-selected': selectedEntities[network.topiaId]}">
                  <td>
                    <input type='checkbox'
                           ng-model="selectedEntities[network.topiaId]"
                           ng-checked="selectedEntities[network.topiaId]"
                           ng-click="toggleSelectedEntity(network.topiaId)"/>
                  </td>
                  <td>
                    {{network.name}}
                  </td>
                  <td>{{getManagers(network)}}</td>
                  <td ng-switch="network.active">
                    <span ng-switch-when="true">Actif</span>
                    <span ng-switch-when="false">Inactif</span>
                  </td>
                </tr>
              </tbody>

              <tfoot>
                <tr>
                  <td colspan="4">
                    <div class="table-footer">
                      <div class="entity-list-info">
                        <div>
                          <span class="counter">{{pager.count}} réseaux</span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}"
                               ng-click="selectAllEntities()" href="">
                               Sélectionner tous les réseaux
                              </a>
                          </span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}"
                                 ng-click="clearSelection()" href="">
                                 Tout désélectionner
                              </a>
                          </span>
                          <span class="selection">
                            - <ng-pluralize
                               count="(selectedEntities|toSelectedLength)"
                               when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                          </span>
                        </div>
                        <div class="filter-selected-switch">
                          <switch ng-model="filter.selected"
                                  disabled="(selectedEntities|toSelectedLength) == 0"
                                  class="smaller">
                          </switch>
                          <div>
                            Filtrer les réseaux sélectionnés
                          </div>
                        </div>
                      </div>
                      <div class="pagination">
                        <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                      </div>
                    </div>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>

        <!-- Domaines -->
        <div class="context-element domain" ng-controller="DomainsListController">
          <a class="context-element-opener"></a>
          <span class="context-element-title">
            <label>Domaine(s) 
              <span class="count"
                    ng-show="selectedEntities|toSelectedLength">
                {{selectedEntities|toSelectedLength}}
              </span>
            </label>
          </span>
          <div class="context-element-content">
            <table class="entity-list clear full-width" id="navigation-context-domains-list">
              <thead>
                <tr>
                  <th scope="col"
                      class="column-tiny"
                      title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                    <input type='checkbox'
                           ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected"
                           ng-change="toggleSelectedEntities()" />
                  </th>
                  <th scope="col">Exploitation ou Domaine expérimental</th>
                  <th scope="col">Campagne</th>
                  <th scope="col">Type</th>
                  <th scope="col">Responsable(s)</th>
                  <th scope="col">Département</th>
                  <th scope="col" class="column-normal">Siret</th>
                  <th scope="col">État</th>
                </tr>
                <tr>
                  <td>
                  </td>
                  <td><input type="text" ng-model="filter.domainName" /></td>
                  <td class="column-small"><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" /></td>
                  <td>
                    <select ng-model="filter.type">
                      <option value="" />
                      <s:iterator value="domainTypes">
                        <option value="<s:property value='key' />"><s:property value="value" /></option>
                      </s:iterator>
                    </select></td>
                  <td><input ng-model="filter.responsable" type="text" /></td>
                  <td class="column-small"><input ng-model="filter.departement" type="text" /></td>
                  <td><input ng-model="filter.siret" type="text" /></td>
                  <td class="column-xsmall">
                    <select ng-model="filter.active">
                      <option value="" />
                      <option value="true">Actif</option>
                      <option value="false">Inactif</option>
                    </select>
                  </td>
                </tr>
              </thead>

              <tbody>
                <tr ng-show="domains.length == 0">
                  <td colspan="7" class="empty-table">
                    Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que les éléments précédemment sélectionnés
                  </td>
                </tr>
                <tr ng-repeat="domain in domains"
                    ng-class="{'line-selected':contextFilter.selectedDomains[domain.topiaId]}">
                  <td>
                    <input type='checkbox'
                           ng-model="selectedEntities[domain.topiaId]"
                           ng-checked="selectedEntities[domain.topiaId]"
                           ng-click="toggleSelectedEntity(domain.topiaId)" />
                  </td>
                  <td>{{domain.name}}</td>
                  <td>{{domain.campaign}}</td>
                  <td>{{domainTypes[domain.type]}}</td>
                  <td>{{domain.responsibles|displayArrayProperties:['firstName','lastName']|orDash}}</td>
                  <td>{{domain.location.departement}}</td>
                  <td>{{domain.siret}}</td>
                  <td ng-switch="domain.active">
                    <span ng-switch-when="true">Actif</span>
                    <span ng-switch-when="false">Inactif</span>
                  </td>
                </tr>
              </tbody>
              
              <tfoot>
                <tr>
                  <td colspan="8">
                    <div class="table-footer">
                      <div class="entity-list-info">
                        <div>
                          <span class="counter">{{pager.count}} domaines</span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}"
                               ng-click="selectAllEntities()" href="">
                               Sélectionner tous les domaines
                            </a>
                          </span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}"
                               ng-click="clearSelection()" href="">
                               Tout désélectionner
                              </a>
                          </span>
                          <span class="selection">
                            - <ng-pluralize
                               count="(selectedEntities|toSelectedLength)"
                               when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                          </span>
                        </div>
                        <div class="filter-selected-switch">
                          <switch ng-model="filter.selected"
                                  disabled="(selectedEntities|toSelectedLength) == 0"
                                  class="smaller">
                          </switch>
                          <div>
                            Filtrer les domaines sélectionnés
                          </div>
                        </div>
                      </div>
                      <div class="pagination">
                        <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                      </div>
                    </div>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>

        <!-- Dispositifs -->
        <div class="context-element growingPlan" ng-controller="GrowingPlansListController">
          <a class="context-element-opener"></a>
          <span class="context-element-title">
            <label>Dispositif(s) 
              <span class="count"
                    ng-show="selectedEntities|toSelectedLength">
                {{selectedEntities|toSelectedLength}}
              </span>
            </label>
          </span>
          <div class="context-element-content">
            <table class="entity-list clear full-width" id="navigation-context-dispositifs-list">
              <thead>
                <tr>
                  <th scope="col"
                      class="column-tiny"
                      title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                    <input type='checkbox'
                           ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected"
                           ng-change="toggleSelectedEntities()" />
                  </th>
                  <th scope="col">Dispositif</th>
                  <th scope="col">Exploitation ou Domaine expérimental</th>
                  <th scope="col">Campagne</th>
                  <th scope="col">Type</th>
                  <th scope="col">Responsable(s)</th>
                  <th scope="col">État</th>
                </tr>
                <tr>
                  <td></td>
                  <td><input type="text" ng-model="filter.growingPlanName" /></td>
                  <td><input type="text" ng-model="filter.domainName" /></td>
                  <td class="column-small"><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" /></td>
                  <td>
                    <select ng-model="filter.typeDephy">
                      <option value="" />
                      <option value="DEPHY_EXPE">DEPHY-EXPE</option>
                      <option value="DEPHY_FERME">DEPHY-FERME</option>
                      <option value="NOT_DEPHY">Hors DEPHY</option>
                    </select></td>
                  <td><input type="text" ng-model="filter.mainContact" /></td>
                  <td class="column-xsmall">
                    <select ng-model="filter.active">
                      <option value="" />
                      <option value="true">Actif</option>
                      <option value="false">Inactif</option>
                    </select>
                  </td>
                </tr>
              </thead>
        
              <tbody>
                <tr ng-show="growingPlans.length == 0">
                  <td colspan="7" class="empty-table">
                    Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que les éléments précédemment sélectionnés
                  </td>
                </tr>
                <tr ng-repeat="growingPlan in growingPlans"
                    ng-class="{'line-selected':contextFilter.selectedGrowingPlans[growingPlan.topiaId]}">
                  <td>
                    <input type='checkbox'
                           ng-model="selectedEntities[growingPlan.topiaId]"
                           ng-checked="selectedEntities[growingPlan.topiaId]"
                           ng-click="toggleSelectedEntity(growingPlan.topiaId)" />
                  </td>
                  <td>{{growingPlan.name}}</td>
                  <td>{{growingPlan.domain.name}}</td>
                  <td>{{growingPlan.domain.campaign}}</td>
                  <td>{{dephyTypes[growingPlan.type]}}</td>
                  <td>{{growingPlan.responsibles|displayArrayProperties:['firstName','lastName']|orDash}}</td>
                  <td ng-switch="growingPlan.active">
                    <span ng-switch-when="true">Actif</span>
                    <span ng-switch-when="false">Inactif</span>
                  </td>
                </tr>
              </tbody>
              
              <tfoot>
                <tr>
                  <td colspan="7">
                    <div class="table-footer">
                      <div class="entity-list-info">
                        <div>
                          <span class="counter">{{pager.count}} dispositifs</span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}"
                                 ng-click="selectAllEntities()" href="">
                                 Sélectionner tous les dispositifs
                              </a>
                          </span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}"
                                 ng-click="clearSelection()" href="">
                                 Tout désélectionner
                              </a>
                          </span>
                          <span class="selection">
                            - <ng-pluralize
                               count="(selectedEntities|toSelectedLength)"
                               when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                          </span>
                        </div>
                        <div class="filter-selected-switch">
                          <switch ng-model="filter.selected"
                                  disabled="(selectedEntities|toSelectedLength) == 0"
                                  class="smaller">
                          </switch>
                          <div>
                            Filtrer les dispositifs sélectionnés
                          </div>
                        </div>
                      </div>
                      <div class="pagination">
                        <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                      </div>
                    </div>
                  </td>
                </tr>
              </tfoot>
            </table>

          </div>
        </div>

        <!-- Systèmes de culture -->
        <div class="context-element growingSystem" ng-controller="GrowingSystemsListController">
          <a class="context-element-opener"></a>
          <span class="context-element-title">
            <label>Système(s) de culture 
              <span class="count"
                    ng-show="contextFilter.selectedGrowingSystems|toSelectedLength">
                {{selectedEntities|toSelectedLength}}
              </span>
            </label>
          </span>
          <div class="context-element-content">
            <table class="entity-list clear full-width" id="growing-systems-list-table">
              <thead>
                <tr>
                  <th scope="col"
                      class="column-tiny"
                      title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                    <input type='checkbox'
                           ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected"
                           ng-change="toggleSelectedEntities()" />
                  </th>
                  <th scope="col">Système de culture</th>
                  <th scope="col">Exploitation ou Domaine expérimental</th>
                  <th scope="col">Dispositif / Filière</th>
                  <th scope="col">Campagne</th>
                  <th scope="col">Filière</th>
                  <th scope="col">Numéro DEPHY</th>
                  <th scope="col">État</th>
                </tr>
                <tr>
                  <td></td>
                  <td><input type="text" ng-model="filter.growingSystemName" /></td>
                  <td><input type="text" ng-model="filter.domainName" /></td>
                  <td><input type="text" ng-model="filter.growingPlanName" /></td>
                  <td class="column-small">
                    <input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" />
                  </td>
                  <td>
                    <select ng-model="filter.sector">
                      <option value="" />
                      <s:iterator value="sectors">
                        <option value="<s:property value='key' />"><s:property value="value" /></option>
                      </s:iterator>
                  </select></td>
                  <td><input class="column-small" type="text" ng-model="filter.dephyNumber" /></td>
                  <td class="column-xsmall">
                    <select ng-model="filter.active">
                      <option value="" />
                      <option value="true">Actif</option>
                      <option value="false">Inactif</option>
                    </select>
                  </td>
                </tr>
              </thead>

              <tbody>
                <tr ng-show="growingSystems.length == 0">
                  <td colspan="8" class="empty-table">
                    Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que les éléments précédemment sélectionnés
                  </td>
                </tr>
                <tr ng-repeat="growingSystem in growingSystems"
                    ng-class="{'line-selected':contextFilter.selectedGrowingSystems[growingSystem.topiaId]}">
                  <td>
                    <input type='checkbox'
                           ng-model="selectedEntities[growingSystem.topiaId]"
                           ng-checked="selectedEntities[growingSystem.topiaId]"
                           ng-click="toggleSelectedEntity(growingSystem.topiaId)" />
                  </td>
                  <td>{{growingSystem.name}}</td>
                  <td>{{growingSystem.growingPlan.domain.name}}</td>
                  <td>{{growingSystem.growingPlan.name}}</td>
                  <td>{{growingSystem.growingPlan.domain.campaign}}</td>
                  <td>{{sectors[growingSystem.sector]}}</td>
                  <td>{{growingSystem.dephyNumber}}</td>

                  <td ng-switch="growingSystem.active">
                    <span ng-switch-when="true">Actif</span>
                    <span ng-switch-when="false">Inactif</span>
                  </td>
                </tr>
              </tbody>

              <tfoot>
                <tr>
                  <td colspan="8">
                    <div class="table-footer">
                      <div class="entity-list-info">
                        <div>
                          <span class="counter">{{pager.count}} systèmes de culture</span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}"
                                 ng-click="selectAllEntities()" href="">
                                 Sélectionner tous les systèmes de culture
                              </a>
                          </span>
                          <span>
                            - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}"
                                 ng-click="clearSelection()" href="">
                                 Tout désélectionner
                              </a>
                          </span>
                          <span class="selection">
                            - <ng-pluralize
                               count="(selectedEntities|toSelectedLength)"
                               when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                          </span>
                        </div>
                        <div class="filter-selected-switch">
                          <switch ng-model="filter.selected"
                                  disabled="(selectedEntities|toSelectedLength) == 0"
                                  class="smaller">
                          </switch>
                          <div>
                            Filtrer les systèmes de culture sélectionnés
                          </div>
                        </div>
                      </div>
                      <div class="pagination">
                        <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                      </div>
                    </div>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>

      </fieldset>

      <div class="buttons">
        <input type="button" class="btn-secondary" value="Tout désélectionner" ng-click="clearNavigationContext()"/>
        <input type="button" class="btn-primary" value="Appliquer" ng-click="saveNavigationContext()"/>
      </div>

      <jqdialog dialog-name="displayTooManyElements" auto-open="false" width="400" class="dialog-form" modal="true"
           buttons="{'Ok': onDisplayTooManyElementsOk}"
           button-classes="{'Ok': 'btn-primary'}">
        {{message}}
      </jqdialog>
    </form>

  </body>
</html>
