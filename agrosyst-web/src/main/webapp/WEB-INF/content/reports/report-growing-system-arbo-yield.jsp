<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="CropPestMasterEditController">
<div ng-controller="CropAllMastersController">
<div ng-controller="CropArboYieldMastersController">
  <input type="hidden" name="arboYieldLossesJson" value="{{cropMasters}}" />
  <input type="hidden" name="yieldInfosJson" value="{{yieldInfo}}" />

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadCropMaster()" href=""><span class="required" ng-if="isDephyFerme">*</span>&nbsp;Rendement et qualité</a>
          <div ng-if="isDephyFerme && ((viewPortArray && viewPortArray.length === 0) || (cropMasters && cropMasters.length === 0))">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>

  <div ng-if="showCropMasters" class="loadContentEnclosure">
    <div class="grid-enclosure "
      ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;">
      <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
      <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
      <div ng-attr-style="grid-column: 1;grid-row: 2;" class="gridColumnHeader"><span class="required">*</span>&nbsp;L’objectif de rendement est-il atteint ?</div>
      <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnMainHeader oddCell">Si les objectifs de rendement ne sont pas atteints, causes (par ordre d’importance)&nbsp;:</div>
      <div ng-attr-style="grid-column: 1;grid-row: 4;" class="gridColumnHeader">- Cause 1</div>
      <div ng-attr-style="grid-column: 1;grid-row: 5;" class="gridColumnHeader oddCell">- Cause 2</div>
      <div ng-attr-style="grid-column: 1;grid-row: 6;" class="gridColumnHeader">- Cause 3</div>
      <div ng-attr-style="grid-column: 1;grid-row: 7;" class="gridColumnHeader oddCell">Commentaires sur la qualité</div>
      <div ng-attr-style="grid-column: 1;grid-row: 8;" class="gridColumnHeader"></div>

      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 1;" class="gridMainHeader" ng-repeat-start="cropMaster in viewPortArray">
        <span ng-repeat="crop in cropMaster.crops">
          {{crop.name}}<span ng-if="!$last">,</span>
       </span>
      </div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridCell" ng-class="{gridCellLast: $last}">{{cropMaster.yieldObjective|translate:'YieldObjective'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell oddCell" ng-class="{gridCellLast: $last}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" class="gridCell" ng-class="{gridCellLast: $last}">{{cropMaster.cause1|translate:'YieldLossCause#ArboViti'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{cropMaster.cause2|translate:'YieldLossCause#ArboViti'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" class="gridCell" ng-class="{gridCellLast: $last}">{{cropMaster.cause3|translate:'YieldLossCause#ArboViti'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell oddCell" ng-class="{gridCellLast: $last}" ag-read-more="{{cropMaster.comment}}"></div>

      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" class="gridActions" ng-class="{gridCellLast: $last}" ng-repeat-end>
        <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editCropMaster(cropMaster)" />
        <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteCropMaster(cropMaster)" />
      </div>
      <div ng-attr-style="grid-column: 2;grid-row: 1 / span 8;" class="gridEmpty" ng-if="viewPortArray.length == 0">
        Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
      </div>
      <div ng-attr-style="grid-column: 2 / span 4;grid-row: 9;" class="gridFooter">
        <input type="button" value="Ajouter une culture" ng-click="addCropMaster()"/>
      </div>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
      <span class="wwlbl">
         <label class="label">Commentaires – Résultats obtenus et faits marquants&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <textarea ng-model="yieldInfo.comment"></textarea>
      </span>
    </div>
  </div>
  
  <jqdialog dialog-name="addArboYield" auto-open="false" width="'55%'" class="dialog-form" modal="true"
     buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
     button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
     title="'Rendement et qualité'">

    <%@include file="inc-crop-template.jsp" %>

    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboYieldYieldObjective"><span class="required">*</span>&nbsp;L’objectif de rendement est-il atteint ?&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboYieldYieldObjective" ng-options="key as value for (key, value) in i18n.YieldObjective"
            ng-model="editedCropObject.yieldObjective" class="column-xlarge" ng-required="editedCropObject">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label">
        <label for="arboYieldCause1">
          <span class="required" ng-if="isDephyFerme && editedCropObject.yieldObjective !== 'MORE_95'">*</span>&nbsp;
          Cause 1&nbsp;:
        </label>
      </span>
      <span class="wwctrl">
        <select id="arboYieldCause1" ng-options="key as value for (key, value) in i18n['YieldLossCause#Arbo' + (isDephyExpe ? 'Expe' : 'Viti')]"
            ng-model="editedCropObject.cause1" class="column-xlarge"
            ng-required="isDephyFerme && editedCropObject.yieldObjective !== 'MORE_95'">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboYieldCause1">Cause 2&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboYieldCause2" ng-options="key as value for (key, value) in i18n['YieldLossCause#Arbo' + (isDephyExpe ? 'Expe' : 'Viti')]"
            ng-model="editedCropObject.cause2" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboYieldCause3">Cause 3&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboYieldCause3" ng-options="key as value for (key, value) in i18n['YieldLossCause#Arbo' + (isDephyExpe ? 'Expe' : 'Viti')]"
            ng-model="editedCropObject.cause3" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboYieldComment">Commentaires sur la qualité&nbsp;:</label></span>
      <span class="wwctrl">
        <textarea id="arboYieldComment" ng-model="editedCropObject.comment" class="column-xlarge" column="20" rows="5"></textarea>
      </span>
    </div>
  </jqdialog>
</div>
</div>
</div>
