<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div class="subRubrique" ng-if="reportGrowingSystem.sectors.indexOf('MARAICHAGE') != -1" ng-controller="MaraichageScopeController">
  <h2>Bilan des pratiques et État sanitaire des cultures (Maraichage)</h2>
  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-maraichage-adventice.jsp" %>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-maraichage-disease.jsp" %>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-maraichage-pest.jsp" %>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllMastersController">
      <%@include file="report-growing-system-maraichage-foods.jsp" %>
      <div ng-controller="CropFoodMastersDialogController">
        <jqdialog dialog-name="addFoodMaraichage" auto-open="false" width="'55%'" class="dialog-form" modal="true"
           buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
           button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
           title="'Maîtrise de l’alimentation hydrique et minérale'">
          <%@include file="inc-add-food.jsp" %>
        </jqdialog>
      </div>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllMastersController">
      <%@include file="report-growing-system-maraichage-yield.jsp" %>
    </div>
  </div>
</div>
