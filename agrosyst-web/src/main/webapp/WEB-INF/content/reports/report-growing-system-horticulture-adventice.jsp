<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<div>
  <div ng-controller="CropAdventiceMastersController">

    <input type="hidden" name="cropAdventiceMastersJson" value="{{getCropPestMasters()}}"/>

    <%-- section --%>
    <div class="wwgrp">
      <span class="wwlbl">
        <label class="tags">
          <div class="tags">
            <a ng-click="loadCropPestMaster()" href=""><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Maîtrise des adventices</a>
            <div ng-if="!isDephyExpe && ((viewPortArray && viewPortArray.length === 0) || (cropPestMasters && cropPestMasters.length === 0))">
              <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
            </div>
          </div>
        </label>
      </span>
    </div>

    <div ng-show="showCropPestMasters" class="loadContentEnclosure">
      <div class="grid-enclosure" ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;"
            ng-style="{'-ms-grid-columns': '20% (1fr)[' + (viewPortArray.length||1) + '] min-content;'}">
        <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
        <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
        <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnMainHeader">Pression avant désherbage&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 4;" class="gridColumnHeader oddCell">
          - <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de pression
          <span class="tooltip-ng help positionRelative">
            &nbsp;<i class="icon fa fa-question-circle font-size130" aria-hidden="true"></i>
            <span class="tooltip-hover opposite" style="width:300px">
              <s:text name="help.report.pressureScaleInt" />
            </span>
          </span>
        </div>
        <div ng-attr-style="grid-column: 1;grid-row: 5;" ng-if="forceDisplayAll" class="gridColumnHeader">- Expression de l’agriculteur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 6;" ng-class="{oddCell: forceDisplayAll}" class="gridColumnMainHeader">Résultats obtenus, niveau de maîtrise finale&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 7;" ng-class="{oddCell: !forceDisplayAll}" class="gridColumnHeader">
          - <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise
          <span class="tooltip-ng help positionRelative">
            &nbsp;<i class="icon fa fa-question-circle font-size130" aria-hidden="true"></i>
            <span class="tooltip-hover opposite" style="width:250px">
              <s:text name="help.report.masterScaleInt" />
            </span>
          </span>
        </div>
        <div ng-attr-style="grid-column: 1;grid-row: 8;" ng-if="forceDisplayAll" class="gridColumnHeader oddCell">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>{{ !isDephyExpe ? "Qualification du niveau de maîtrise" : "Satisfaction" }}</div>
        <div ng-attr-style="grid-column: 1;grid-row: 9;" ng-if="forceDisplayAll" class="gridColumnHeader">- Expression de l’agriculteur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 10;" ng-class="{oddCell: forceDisplayAll}" class="gridColumnMainHeader">IFT calculé à la cible&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 11;" ng-class="{oddCell: !forceDisplayAll}" class="gridColumnHeader">- IFT-Herbi.</div>
        <div ng-attr-style="grid-column: 1;grid-row: 12;" ng-class="{oddCell: forceDisplayAll}" class="gridColumnHeader">- Commentaires de l'expérimentateur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 13;" ng-class="{oddCell: !forceDisplayAll}" class="gridColumnHeader"></div>

        <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 1;" class="gridMainHeader" ng-repeat-start="pestMaster in viewPortArray" ng-if="pestMaster.$$cropspan > 0">
          <span ng-repeat="crop in pestMaster.$$crop.crops">
            {{crop.name}}<span ng-if="!$last">,</span>
          </span>

        </div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridSubHeader">
          {{pestMaster.agressor.adventice || pestMaster.agressor.reference_label}}
        </div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{pestMaster.pressureScaleInt}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" ng-if="forceDisplayAll" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.pressureFarmerComment}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" class="gridCell" ng-class="{gridCellLast: $last, oddCell: forceDisplayAll}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell" ng-class="{gridCellLast: $last, oddCell: !forceDisplayAll}">{{pestMaster.masterScaleInt}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" ng-if="forceDisplayAll" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{pestMaster.qualifier|translate:'PestMasterLevelQualifier'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" ng-if="forceDisplayAll" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.resultFarmerComment}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 10;" class="gridCell" ng-class="{gridCellLast: $last, oddCell: forceDisplayAll}"></div>
        <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 11;" class="gridCell cellForCrop" ng-class="{gridCellLast: $last, oddCell: !forceDisplayAll}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.iftMain}}</div>
        <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 12;" class="gridCell cellForCrop" ng-class="{gridCellLast: $last, oddCell: forceDisplayAll}" ng-if="pestMaster.$$cropspan > 0" ag-read-more="{{pestMaster.$$crop.adviserComments}}"></div>

        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 13;" class="gridActions" ng-class="{gridCellLast: $last}" ng-repeat-end>
          <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editPestMasterFromCropPestMaster(pestMaster.$$crop, pestMaster)" />
          <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteCropPestMaster(pestMaster.$$crop)" />
        </div>
        <div ng-attr-style="grid-column: 2;grid-row: 1 / span 13;" class="gridEmpty" ng-if="viewPortArray.length == 0">
          Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
        </div>
        <div ng-attr-style="grid-column: 2 / span 4;grid-row: 14;" class="gridFooter">
            <input type="button" value="Ajouter une culture" ng-click="addCropPestMaster()"/>
        </div>
      </div>

      <div ng-if="forceDisplayAll" class="wwgrp">
        <span class="wwlbl"><label>Modèle décisionnel&nbsp;:</label></span>
        <span class="wwctrl">
          <div>
            <a class="asLabelUnderlined" ng-if="observeManagementModeId" target="_blank" rel="noopener noreferrer"
               href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{observeManagementModeId}}#sectionTypes_ADVENTICES">
               Ouvrir le modèle décisionnel
            </a>
          </div>
          <div ng-if="!observeManagementModeId">Aucun</div>
        </span>
      </div>
    </div>
  </div>

  <div ng-controller="CropAdventiceMastersDialogController">
    <jqdialog dialog-name="addAdventiceCropsHorticulture" auto-open="false" width="'55%'" class="dialog-form" modal="true"
        buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
        button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
        title="'Maîtrise des adventices'">
      <div class="asTable">

        <%@include file="inc-crop-template.jsp" %>

        <div class="section"><span class="required">*</span>&nbsp;Adventice(s) pour ces cultures</div>

        <div>
          <ul class="asTable">
            <li class="asContentTable0 fromContent">

              <span ng-repeat="pestMaster in editedCropObject.pestMasters">
                <span style="cursor:pointer" class="link"
                   ng-click="editPestMaster(editedCropObject, pestMaster)">{{pestMaster.agressor.adventice || pestMaster.agressor.reference_label}}</span>
                <input type="button"
                       class="btn-icon icon-delete"
                       ng-click="deletePestMaster(editedCropObject, pestMaster)"
                       value="x"
                       title="Supprimer"/>
                <span ng-if="!$last">,</span>
              </span>

              <span class="info" ng-if="editedCropObject.pestMasters.length === 0">
                Aucune adventice déclarée
              </span>

              <span class="endLine">
                <div style="cursor:pointer"
                     class="textIcones blue x2"
                     ng-click="editPestMaster(editedCropObject)"
                     title="Déclarer une adventice">
                     +
                </div>
              </span>

            </li>
          </ul>
        </div>

        <div class="section">Quel niveau d’utilisation de pesticides ?</div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="iftMain-all-adventice">IFT-Herbicide&nbsp;:</label></span>
          <span class="wwctrl">
            <input id="iftMain-all-adventice"
                   class="column-xlarge"
                   type="text"
                   ng-model="editedCropObject.iftMain"
                   placeholder="ex: 1.5"
                   ag-float2dec
                   pattern="^\d+(?:[\.,]\d{1,2})?$"/>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="adviserComments">Commentaire de l'expérimentateur&nbsp;:</label></span>
          <span class="wwctrl">
            <textarea id="adviserComments"
                      type="text"
                      ng-model="editedCropObject.adviserComments"
                      class="column-xlarge"
                      column="20"
                      rows="5">
            </textarea>
          </span>
        </div>
      </div>
    </jqdialog>

    <jqdialog dialog-name="addAdventiceHorticulture" auto-open="false" width="'55%'" class="dialog-form" modal="true"
         buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
         button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
         title="'Adventice ciblée'">
      <div id="addAdventice" class="asTable">

        <div class="horizontal-separator">Pression avant désherbage</div>
        <div class="wwgrp">
          <span class="wwlbl wide-label">
            <label for="editedPestBioAgressor"><span class="required">*</span>&nbsp;Adventice&nbsp;:</label>
          </span>
          <span class="wwctrl">
            <span ng-if="!bioAgressors || bioAgressors.length === 0">Chargement en cours...</span>
            <select
              id="editedPestBioAgressor"
              ng-model="editedPestMaster.agressor"
              ng-if="bioAgressors && bioAgressors.length > 0"
              ng-options="bioAgressor as (bioAgressor.adventice ? bioAgressor.adventice : bioAgressor.reference_label) for bioAgressor in bioAgressors"
              class="column-xlarge">
            </select>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="diseasePressureScale"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de pression&nbsp;:</label></span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.report.pressureScaleInt" />
              </span>
            </span>
            <input type="number"
                   class="column-xlarge"
                   id="diseasePressureScale"
                   ng-model="editedPestMaster.pressureScaleInt"
                   placeholder="entre 1 et 10"
                   ag-integer
                   min="1" max="10"/>
          </span>
        </div>

        <div ng-if="forceDisplayAll" class="wwgrp">
          <span class="wwlbl wide-label"><label for="pressureFarmerComment">Expression de l'agriculteur&nbsp;:</label></span>
          <span class="wwctrl">
            <textarea id="pressureFarmerComment"
                      type="text"
                      ng-model="editedPestMaster.pressureFarmerComment"
                      class="column-xlarge"
                      column="20"
                      rows="5">
            </textarea>
          </span>
        </div>

        <div class="horizontal-separator">Résultats obtenus, niveau de maîtrise finale</div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="pestMasterScale"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise&nbsp;:</label></span>
          <span class="wwctrl">
            <span class='contextual-help'>
              <span class='help-hover'>
                <s:text name="help.report.masterScaleInt" />
              </span>
            </span>
            <input type="number"
                   class="column-xlarge"
                   id="pestMasterScale"
                   ng-model="editedPestMaster.masterScaleInt"
                   placeholder="entre 1 et 10"
                   ag-integer
                   min="1" max="10"/>
          </span>
        </div>

        <div ng-if="forceDisplayAll" class="wwgrp">
          <span class="wwlbl wide-label"><label for="resultSatisfaction"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>{{ !isDephyExpe ? "Qualification du niveau de maîtrise" : "Satisfaction" }}&nbsp;:</label></span>
          <span class="wwctrl">
            <select id="resultSatisfaction" ng-options="key as value for (key, value) in i18n.PestMasterLevelQualifier"
                ng-model="editedPestMaster.qualifier" class="column-xlarge">
              <option value=""></option>
            </select>
          </span>
        </div>

        <div ng-if="forceDisplayAll" class="wwgrp">
          <span class="wwlbl wide-label"><label for="resultFarmerComment">Expression de l'agriculteur&nbsp;:</label></span>
          <span class="wwctrl">
            <textarea id="resultFarmerComment"
                      type="text"
                      ng-model="editedPestMaster.resultFarmerComment"
                      class="column-xlarge"
                      column="20"
                      rows="5">
            </textarea>
          </span>
        </div>
      </div>
    </jqdialog>
  </div>
</div>
