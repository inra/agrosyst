<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2017 INRA, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>

<div class="section"><span class="required">*</span>&nbsp;Quelles cultures concernées ?</div>
  <div>
    <ul class="asTable">
      <li class="asContentTable0 fromContent">
        <span ng-repeat="crop in crops"
              class="tag large"
              style="background: {{crop.backgroundColor}}"
              ng-class="{'none': editedCropObject.crops.indexOf(crop) !== -1}"
              ng-click="addOrRemoveCrop(editedCropObject, crop)">
          <input type="checkbox" id="species_{{$index}}"
                 ng-checked="editedCropObject.crops.indexOf(crop) !== -1">
            {{crop.name}}
          </input>
        </span>
      </li>
    </ul>
  </div>

  <div class="section">Affiner la déclaration par espèces/variétés ?
    <span class="info light" ng-if="editedCropObject.species.length === 0">
      (Aucune espèce à afficher)
    </span>
    <span ng-show="editedCropObject.species.length > 0"
          ng-click="boxShowSpecies = !boxShowSpecies"
          style="cursor:pointer" class="info light">
        (Cliquer pour {{boxShowSpecies ? 'cacher' : 'afficher'}})
    </span>
  </div>

  <div ng-if="boxShowSpecies">
    <ul class="asTable">
      <li class="asContentTable0 fromContent">
        <span ng-repeat="crop in editedCropObject.crops">
          <span ng-repeat="cps in crop.croppingPlanSpecies"
                class="tag large"
                style="background: {{crop.backgroundColor}}"
                ng-class="{'none':(editedCropObject.species.indexOf(cps) === -1)}"
                ng-click="addOrRemoveSpecies(editedCropObject, crop, cps)"
                title="{{(editedCropObject.species.indexOf(cps) === -1) ? 'Cliquer pour sélectionner l\'espèce' : 'Cliquer pour retirer l\'espèce'}}">
            <input type="checkbox" id="species_{{$index}}"
              ng-checked="(editedCropObject.species.indexOf(cps) !== -1)">
              {{cps.species.libelle_espece_botanique}}{{(cps.variety && cps.variety.denomination) ? ' (' + cps.variety.denomination + ')' : ''}}
            </input>
          </span>
        </span>
      </li>
    </ul>
  </div>
