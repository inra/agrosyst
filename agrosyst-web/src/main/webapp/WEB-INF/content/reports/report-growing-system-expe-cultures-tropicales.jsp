<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div class="subRubrique" ng-if="reportGrowingSystem.sectors.indexOf('CULTURES_TROPICALES') != -1" ng-controller="CulturesTropicalesScopeController">
  <h2>Bilan des pratiques et État sanitaire des cultures (Cultures tropicales)</h2>
  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-all-adventice.jsp" %>
      <div ng-controller="CropAdventiceMastersDialogController">
        <jqdialog dialog-name="addAdventiceCropsCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
            buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
            button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
            title="'Maîtrise des adventices'">
          <%@include file="inc-add-adventice-crops.jsp" %>
        </jqdialog>
        <jqdialog dialog-name="addAdventiceCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
             buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
             button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
             title="'Adventice ciblée'">
          <%@include file="inc-add-adventice.jsp" %>
        </jqdialog>
      </div>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-all-disease.jsp" %>
      <div ng-controller="CropDiseaseMastersDialogController">
        <!-- edition -->
        <jqdialog dialog-name="addDiseaseCropsCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
           buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
           button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
           title="'Maîtrise des maladies'">
            <%@include file="inc-add-disease-crops.jsp" %>
        </jqdialog>

        <jqdialog dialog-name="addDiseaseCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
           buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
           button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
           title="'Maladie ciblée'">
          <%@include file="inc-add-disease.jsp" %>
        </jqdialog>
      </div>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-all-pest.jsp" %>
      <div ng-controller="CropPestMastersDialogController">
        <!-- edition -->
        <jqdialog dialog-name="addPestCropsCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
              buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
              button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
              title="'Maîtrise des ravageurs'">
            <%@include file="inc-add-pest-crops.jsp" %>
        </jqdialog>

        <jqdialog dialog-name="addPestCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
           buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
           button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
           title="'Ravageur ciblé'">
          <%@include file="inc-add-pest.jsp" %>
        </jqdialog>
      </div>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllMastersController">
      <%@include file="report-growing-system-all-foods.jsp" %>
      <div ng-controller="CropFoodMastersDialogController">
        <jqdialog dialog-name="addFoodCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
           buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
           button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
           title="'Maîtrise de l’alimentation hydrique et minérale'">
          <%@include file="inc-add-food.jsp" %>
        </jqdialog>
      </div>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllMastersController">
      <%@include file="report-growing-system-all-yield.jsp" %>
      <div ng-controller="CropYieldMastersDialogController">
        <jqdialog dialog-name="addYieldCulturesTropicales" auto-open="false" width="'55%'" class="dialog-form" modal="true"
           buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
           button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
           title="'Rendement et qualité'">
          <%@include file="inc-add-yield.jsp" %>
        </jqdialog>
      </div>
    </div>
  </div>
</div>
