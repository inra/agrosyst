<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div>
  <%@include file="inc-crop-template.jsp" %>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allFoodsFoodIrrigation">Irrigation&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allFoodsFoodIrrigation" ng-options="key as value for (key, value) in i18n.FoodIrrigation"
          ng-model="editedCropObject.foodIrrigation" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allFoodsHydriqueStress">Stress hydrique&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allFoodsHydriqueStress" ng-options="key as value for (key, value) in i18n.StressLevel"
          ng-model="editedCropObject.hydriqueStress" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allFoodsHydriqueStress">Azote&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allFoodsHydriqueStress" ng-options="key as value for (key, value) in i18n.StressLevel"
          ng-model="editedCropObject.azoteStress" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allFoodsRiskFarmerComment">Alimentation minérale (hors azote)&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="allFoodsRiskFarmerComment" ng-model="editedCropObject.mineralFood" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allFoodsTemp">Température et rayonnement&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allFoodsTemp" ng-options="key as value for (key, value) in i18n.StressLevel"
          ng-model="editedCropObject.tempStress" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allFoodsComment">Commentaires – Résultats obtenus et faits marquants&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="allFoodsComment" ng-model="editedCropObject.comment" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>
</div>
