<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="VitiYieldMastersController">

  <input type="hidden" name="yieldInfosJson" value="{{yieldInfo}}" />
  <input type="hidden" name="vitiYieldObjective" value="{{reportGrowingSystem.vitiYieldObjective}}" />
  <input type="hidden" name="vitiLossCause1" value="{{reportGrowingSystem.vitiLossCause1}}" />

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadVitiYields()" href=""><span class="required" ng-if="isDephyFerme">*</span>&nbsp;Rendement et qualité</a>
          <div ng-if="(isDephyFerme && !reportGrowingSystem.vitiYieldObjective) || ((isDephyFerme && reportGrowingSystem.vitiYieldObjective !== 'MORE_95') && !reportGrowingSystem.vitiLossCause1)">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>

  <div ng-show="showVitiYields" class="loadContentEnclosure">

      <div class="wwgrp">
        <span class="wwlbl">
          <label for="vitiYieldObjective"><span class="required" ng-if="isDephyFerme">*</span>&nbsp;L’objectif de rendement est-il atteint ?&nbsp;:</label>
        </span>
        <span class="wwctrl column-xlarge">
          <select id="vitiYieldObjective"
                  ng-model="reportGrowingSystem.vitiYieldObjective"
                  ng-options="key as value for (key , value) in yieldObjectives"
                  ng-required="isDephyFerme">
          </select>
        </span>
      </div>

      <div class="wwgrp">
        <span class="wwlbl">
          <label for="vitiYieldObjective"><span class="required" ng-if="isDephyFerme && reportGrowingSystem.vitiYieldObjective !== 'MORE_95'">*</span>&nbsp;Si les objectifs de rendement ne sont pas atteints, causes (par ordre d’importance)&nbsp;:</label>
        </span>
        <span class="wwctrl column-xlarge">
          <select id="vitiYieldObjective"
                  ng-model="reportGrowingSystem.vitiLossCause1"
                  ng-options="key as value for (key , value) in vitiYieldLossCauses"
                  ng-required="isDephyFerme && reportGrowingSystem.vitiYieldObjective !== 'MORE_95'">
            <option value="" ng-if="!(isDephyFerme && reportGrowingSystem.vitiYieldObjective !== 'MORE_95')"></option>
          </select>

        </span>
      </div>

      <s:select label="Cause 2"
                name="reportGrowingSystem.vitiLossCause2" list="vitiYieldLossCauses"
                ng-model="reportGrowingSystem.vitiLossCause2"
                labelPosition="left" labelSeparator=" :" emptyOption="true" cssClass="column-xlarge"/>
      <s:select label="Cause 3"
                name="reportGrowingSystem.vitiLossCause3" list="vitiYieldLossCauses"
                ng-model="reportGrowingSystem.vitiLossCause3"
                labelPosition="left" labelSeparator=" :" emptyOption="true" cssClass="column-xlarge"/>

      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <s:fielderror fieldName="reportGrowingSystem.vitiYieldQuality" />
        <span class="wwlbl">
           <label class="label">Commentaires sur la qualité&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <textarea name="reportGrowingSystem.vitiYieldQuality">${reportGrowingSystem.vitiYieldQuality}</textarea>
        </span>
      </div>

      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl">
           <label class="label">Commentaires – Résultats obtenus et faits marquants&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <textarea ng-model="yieldInfo.comment"></textarea>
        </span>
      </div>
  </div>
</div>
