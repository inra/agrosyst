<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="CropPestMasterEditController">
<div ng-controller="CropAllPestMastersController">
<div ng-controller="CropArboPestMastersController">
  <input type="hidden" name="arboCropPestMastersJson" value="{{cropPestMasters}}"/>

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadCropPestMaster('arboPest')" href=""><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Maîtrise des ravageurs</a>
          <div ng-if="!isDephyExpe && ((viewPortArray && viewPortArray.length === 0) || (cropPestMasters && cropPestMasters.length === 0))">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>
  <div ng-show="showArboPestCropPestMasters" class="loadContentEnclosure">
    <div class="grid-enclosure" ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;">
        <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
        <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
        <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnMainHeader">Pression sur le système de culture&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 4;" class="gridColumnHeader oddCell">- Inoculum année(s) précédente(s)</div>
        <div ng-attr-style="grid-column: 1;grid-row: 5;" class="gridColumnHeader">- Echelle de pression</div>
        <div ng-attr-style="grid-column: 1;grid-row: 6;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader oddCell">- Evolution par rapport à l’année précédente</div>
        <div ng-attr-style="grid-column: 1;grid-row: 7;" class="gridColumnMainHeader" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}">Résultats obtenus, niveau de maîtrise finale&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 8;" class="gridColumnHeader" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise</div>
        <div ng-attr-style="grid-column: 1;grid-row: 9;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- % parcelles touchées</div>
        <div ng-attr-style="grid-column: 1;grid-row: 10;" class="gridColumnHeader oddCell">- % arbres touchés</div>
        <div ng-attr-style="grid-column: 1;grid-row: 11;" class="gridColumnHeader">- % dommages sur fruits</div>
        <div ng-attr-style="grid-column: 1;grid-row: 12;" class="gridColumnHeader oddCell">- % dommages sur pousses ou feuilles</div>
        <div ng-attr-style="grid-column: 1;grid-row: 13;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Inoculum pour les années suivantes</div>
        <div ng-attr-style="grid-column: 1;grid-row: 14;" class="gridColumnHeader" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Qualification du niveau de maîtrise</div>
        <div ng-attr-style="grid-column: 1;grid-row: 15;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Expression de l’agriculteur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 16;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader oddCell">- Commentaires du conseiller</div>
        <div ng-attr-style="grid-column: 1;grid-row: 17;" class="gridColumnMainHeader" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}">IFT&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 18;" class="gridColumnHeader" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}">- Nombre de traitements</div>
        <div ng-attr-style="grid-column: 1;grid-row: 19;" class="gridColumnHeader" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}">- IFT-ravageurs chimique (insecticides, anti-limaces…)</div>
        <div ng-attr-style="grid-column: 1;grid-row: 20;" class="gridColumnHeader" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}">- IFT-ravageurs biocontrôle</div>
        <div ng-attr-style="grid-column: 1;grid-row: 21;" class="gridColumnHeader" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}"></div>

        <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 1;" class="gridMainHeader" ng-repeat-start="pestMaster in viewPortArray" ng-if="pestMaster.$$cropspan > 0">
          <span ng-repeat="crop in pestMaster.$$crop.crops">
            {{crop.name}}<span ng-if="!$last">,</span>
          </span>
        </div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridSubHeader">{{getAgressorLabel(pestMaster)}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{pestMaster.previousYearInoculum|translate:'InoculumLevel'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" class="gridCell" ng-class="{gridCellLast: $last}">{{pestMaster.pressureScale|translate:'PressureScale'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{pestMaster.pressureEvolution|translate:'PressureEvolution'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{pestMaster.masterScale|translate:'MasterScale'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}">{{pestMaster.percentAffectedPlots|translate:'DamageLevel'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 10;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{isDephyExpe ? pestMaster.precisePercentAffectedTrees : pestMaster.percentAffectedTrees}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 11;" class="gridCell" ng-class="{gridCellLast: $last}">{{isDephyExpe ? pestMaster.precisePercentDamageFruits : pestMaster.percentDamageFruits}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 12;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{isDephyExpe ? pestMaster.precisePercentDamageLeafs : pestMaster.percentDamageLeafs}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 13;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}">{{pestMaster.nextYearInoculum|translate:'InoculumLevel'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 14;" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{pestMaster.qualifier|translate:'GlobalMasterLevelQualifier'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 15;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.resultFarmerComment}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 16;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell oddCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.adviserComments}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 17;" class="gridCell oddCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}"></div>
        <div id="ArboPestCropPestMasters_treatmentCount_{{$index}}" ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 18;" class="gridCell cellForCrop" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.treatmentCount}}<input type="text" ng-model="pestMaster.$$crop.treatmentCount" style="opacity:0;width: 0;"/></div>
        <div id="ArboPestCropPestMasters_chemicalPestIFT_{{$index}}" ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 19;" class="gridCell cellForCrop" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.chemicalPestIFT}}<input type="text" ng-model="pestMaster.$$crop.chemicalPestIFT" style="opacity:0;width: 0;"/></div>
        <div id="ArboPestCropPestMasters_bioControlPestIFT_{{$index}}" ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 20;" class="gridCell cellForCrop" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.bioControlPestIFT}}<input type="text" ng-model="pestMaster.$$crop.bioControlPestIFT" style="opacity:0;width: 0;"/></div>

        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 21;" class="gridActions" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}" ng-repeat-end>
          <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editPestMasterFromCropPestMaster(pestMaster.$$crop, pestMaster)" />
          <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteCropPestMaster(pestMaster.$$crop)" />
        </div>
        <div ng-attr-style="grid-column: 2;grid-row: 1 / span 22;" class="gridEmpty" ng-if="viewPortArray.length == 0">
          Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
        </div>
        <div ng-attr-style="grid-column: 1 / span 5;grid-row: 23;" class="gridFooter">
          <input type="button" value="Ajouter une culture" ng-click="addCropPestMaster()"/>
        </div>
    </div>

      <div class="wwgrp">
          <s:fielderror fieldName="reportGrowingSystem.arboChemicalPestIFT" />
          <span class="wwlbl">
             <label class="label">IFT-ravageurs du système de culture (chimique)&nbsp;:</label>
          </span>
          <span class="wwctrl">
            <input id="ArboPestCropPestMasters_arboChemicalPestIFT"
                 class="column-xlarge" type="text" name="reportGrowingSystem.arboChemicalPestIFT" value="${reportGrowingSystem.arboChemicalPestIFT}" placeholder="ex: 1.5"/>
          </span>
      </div>
      <div class="wwgrp">
            <span class="wwlbl">
               <label class="label">IFT-ravageurs biocontrôle du système de culture&nbsp;:</label>
            </span>
            <span class="wwctrl">
              <input id="ArboPestCropPestMasters_arboBioControlPestIFT"
                 class="column-xlarge" type="text" name="reportGrowingSystem.arboBioControlPestIFT" value="${reportGrowingSystem.arboBioControlPestIFT}" placeholder="ex: 1.5" />
            </span>
      </div>
      <s:select label="Niveau global de maîtrise des ravageurs"
                id="ArboPestCropPestMasters_arboPestQualifier"
                requiredLabel="true" ng-required="viewPortArray.length > 0" emptyOption="true"
                name="reportGrowingSystem.arboPestQualifier" list="globalMasterLevelQualifiers"
                labelPosition="left" labelSeparator=" :" cssClass="column-xlarge"/>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
          <span class="wwlbl"><label>Modèle décisionnel&nbsp;:</label></span>
          <span class="wwctrl">
            <div>
              <a class="asLabelUnderlined" ng-if="observeManagementModeId" target="_blank" rel="noopener noreferrer"
                 href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{observeManagementModeId}}#sectionTypes_RAVAGEURS">
                 Ouvrir le modèle décisionnel
              </a>
            </div>
            <div ng-if="!observeManagementModeId">Aucun</div>
          </span>
      </div>
  </div>

  <!-- edition -->
  <jqdialog dialog-name="addArboPestCrops" auto-open="false" width="'55%'" class="dialog-form" modal="true"
         buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
         button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
         title="'Maîtrise des ravageurs'">
      <div class="asTable">

        <%@include file="inc-crop-template.jsp" %>

        <div class="section"><span class="required">*</span>&nbsp;Ravageur(s) pour ces cultures</div>

        <div>
          <ul class="asTable">
            <li class="asContentTable0 fromContent">

              <span ng-repeat="pestMaster in editedCropObject.pestMasters">
                <span style="cursor:pointer" class="link"
                   ng-click="editPestMaster(editedCropObject, pestMaster)">{{getAgressorLabel(pestMaster)}}</span>
                <input type="button" class="btn-icon icon-delete" ng-click="deletePestMaster(editedCropObject, pestMaster)" value="x" title="Supprimer"/>
                <span ng-if="!$last">,</span>
              </span>

              <span class="info" ng-if="editedCropObject.pestMasters.length === 0">
                Aucun ravageur déclaré
              </span>

              <span class="endLine">
                <div style="cursor:pointer"
                     class="textIcones blue x2"
                     ng-click="editPestMaster(editedCropObject)"
                     title="Déclarer une adventice">
                     +
                </div>
              </span>

            </li>
          </ul>
        </div>

        <div class="section">Quel niveau d’utilisation de pesticides ?</div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="arboPestTreatmentCount">Nombre de traitements&nbsp;:</label></span>
          <span class="wwctrl">
            <input id="arboPestTreatmentCount" type="text"
                 class="column-xlarge" ng-model="editedCropObject.treatmentCount" placeholder="ex: 1" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="arboPestChemicalPestIFT">IFT-ravageurs chimique (insecticides, anti-limaces…)&nbsp;:</label></span>
          <span class="wwctrl">
            <input id="arboPestChemicalPestIFT" type="text"
                 class="column-xlarge" ng-model="editedCropObject.chemicalPestIFT" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="arboPastBioControlPestIFT">IFT-ravageurs biocontrôle&nbsp;:</label></span>
          <span class="wwctrl">
            <input id="arboPastBioControlPestIFT" type="text"
                 class="column-xlarge" ng-model="editedCropObject.bioControlPestIFT" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
          </span>
        </div>
      </div>
  </jqdialog>

  <jqdialog dialog-name="addArboPest" auto-open="false" width="'55%'" class="dialog-form" modal="true"
        buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
        button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
        title="'Ravageur ciblé'">

    <div class="horizontal-separator">Pression avant intervention</div>

    <div class="oneFieldOutOfTwoRequired">
      <span class="required">*</span>&nbsp;<em>au moins un des deux</em>
    </div>

    <div class="wwgrp">
      <span class="wwlbl wide-label">
        <label for="codeGroupeCibleMaa" class="label">Groupe cible&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class='contextual-help'>
          <span class='help-hover'>
            <s:text name="help.report.groupeCibleMaa" />
          </span>
        </span>
        <select ng-model="editedPestMaster.codeGroupeCibleMaa"
                class="column-xlarge"
                ng-options="groupeCible.codeGroupeCibleMaa as groupeCible.groupeCibleMaa for groupeCible in groupesCibles | filter: isGroupeCibleInCategory('RAVAGEUR')">
          <option label="" value="" />
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label">
        <label for="arboPestAgressor" class="label">Ravageur&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <select id="arboAdventiceAgressor"
                ng-model="editedPestMaster.agressor"
                ng-if="bioAgressors && bioAgressors.length > 0"
                ng-options="(bioAgressor.adventice ? bioAgressor.adventice : bioAgressor.reference_label) disable when bioAgressor.main for bioAgressor in bioAgressors | filter: inGroupeCibleBioAgressor(editedPestMaster)"
                class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestPreviousYearInoculum">Inoculum année(s) précédente(s)&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboPestPreviousYearInoculum" ng-options="key as value for (key, value) in i18n.InoculumLevel"
            ng-model="editedPestMaster.previousYearInoculum" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestPressureScale">Echelle de pression&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboPestPressureScale" ng-options="key as value for (key, value) in i18n.PressureScale"
            ng-model="editedPestMaster.pressureScale" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestPressureEvolution">Evolution par rapport à l’année précédente&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboPestPressureEvolution" ng-options="key as value for (key, value) in i18n.PressureEvolution"
            ng-model="editedPestMaster.pressureEvolution" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>

    <div class="horizontal-separator">Résultats obtenus, niveau de maîtrise finale</div>

    <div class="wwgrp">
        <span class="wwlbl wide-label"><label for="arboPestPressureEvolution"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="arboPestPressureEvolution" ng-options="key as value for (key, value) in i18n.MasterScale"
              ng-model="editedPestMaster.masterScale" class="column-xlarge" required="{{!isDephyExpe}}">
            <option value=""></option>
          </select>
        </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestPercentAffectedPlots">% parcelles touchées&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboPestPercentAffectedPlots" ng-options="key as value for (key, value) in i18n.DamageLevel"
            ng-model="editedPestMaster.percentAffectedPlots" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestPercentAffectedTrees">% arbres touchés&nbsp;:</label></span>
      <span class="wwctrl">
        <select ng-if="!isDephyExpe" id="arboPestPercentAffectedTrees" ng-options="key as value for (key, value) in i18n.DamageLevel"
            ng-model="editedPestMaster.percentAffectedTrees" class="column-xlarge">
          <option value=""></option>
        </select>
        <input ng-if="isDephyExpe" id="arboPestPercentAffectedTrees"
                 class="column-xlarge" type="text" ng-model="editedPestMaster.precisePercentAffectedTrees" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestPercentDamageFruits">% dommages sur fruits&nbsp;:</label></span>
      <span class="wwctrl">
        <select ng-if="!isDephyExpe" id="arboPestPercentDamageFruits" ng-options="key as value for (key, value) in i18n.DamageLevel"
            ng-model="editedPestMaster.percentDamageFruits" class="column-xlarge">
          <option value=""></option>
        </select>
        <input ng-if="isDephyExpe" id="arboPestPercentDamageFruits"
                 class="column-xlarge" type="text" ng-model="editedPestMaster.precisePercentDamageFruits" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestPercentDamageLeafs">% dommages sur pousses ou feuilles&nbsp;:</label></span>
      <span class="wwctrl">
        <select ng-if="!isDephyExpe" id="arboPestPercentDamageLeafs" ng-options="key as value for (key, value) in i18n.DamageLevel"
            ng-model="editedPestMaster.percentDamageLeafs" class="column-xlarge">
          <option value=""></option>
        </select>
        <input ng-if="isDephyExpe" id="arboPestPercentDamageLeafs"
                 class="column-xlarge" type="text" ng-model="editedPestMaster.precisePercentDamageLeafs" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="arboPestNextYearInoculum">Inoculum pour les années suivantes&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="arboPestNextYearInoculum" ng-options="key as value for (key, value) in i18n.InoculumLevel"
              ng-model="editedPestMaster.nextYearInoculum" class="column-xlarge">
            <option value=""></option>
          </select>
        </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestQualifier"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Qualification du niveau de maîtrise&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboPestQualifier" ng-options="key as value for (key, value) in (isDephyExpe ? i18n.GlobalMasterLevelQualifier : i18n['GlobalMasterLevelQualifier#ArboNotExpe'])"
            ng-model="editedPestMaster.qualifier" class="column-xlarge" required="{{!isDephyExpe}}">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboPestResultFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
      <span class="wwctrl">
        <textarea id="arboPestResultFarmerComment" ng-model="editedPestMaster.resultFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="arboPestAdviserComments">Commentaires du conseiller&nbsp;:</label></span>
        <span class="wwctrl">
          <textarea id="arboPestAdviserComments" ng-model="editedPestMaster.adviserComments" class="column-xlarge" column="20" rows="5"></textarea>
        </span>
    </div>
  </jqdialog>
</div>
</div>
</div>
