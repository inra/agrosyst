<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="VitiPestMastersController" ng-init="init(false)">
  <input type="hidden" name="vitiPestMastersJson" value="{{vitiPestMasters}}"/>

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadVitiPestMasters()" href=""><span class="required">*</span>&nbsp;Maîtrise des ravageurs</a>
          <div ng-if="!isDephyExpe && ((viewPortArray && viewPortArray.length === 0) || (vitiPestMasters && vitiPestMasters.length === 0))">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>

  <div ng-show="showVitiPestMasters" class="loadContentEnclosure">
      <div class="grid-enclosure" ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;">
        <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
        <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
        <div ng-attr-style="grid-column: 1;grid-row: 2;" class="gridColumnMainHeader">Pression sur le système de culture&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnHeader">- Échelle de pression</div>
        <div ng-attr-style="grid-column: 1;grid-row: 4;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Evolution par rapport à l’année précédente</div>
        <div ng-attr-style="grid-column: 1;grid-row: 5;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Expression de l’agriculteur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 6;" class="gridColumnMainHeader">Résultats obtenus, niveau de maîtrise finale&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 7;" class="gridColumnHeader">- Échelle de maîtrise</div>
        <div ng-attr-style="grid-column: 1;grid-row: 8;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Note globale d’attaque sur feuilles</div>
        <div ng-attr-style="grid-column: 1;grid-row: 9;" ng-if="!isDephyExpe" class="gridColumnHeader">- Note globale d’attaque sur grappes</div>
        <div ng-attr-style="grid-column: 1;grid-row: 9;" ng-if="isDephyExpe" class="gridColumnHeader">- Nombre de perforations pour 100 grappes</div>
        <div ng-attr-style="grid-column: 1;grid-row: 10;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Qualification du niveau de maîtrise</div>
        <div ng-attr-style="grid-column: 1;grid-row: 11;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Expression de l’agriculteur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 12;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Commentaires du conseiller</div>
        <div ng-attr-style="grid-column: 1;grid-row: 13;" class="gridColumnMainHeader">IFT&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 14;" class="gridColumnHeader">- <span class="required">*</span>&nbsp;Nombre de traitements</div>
        <div ng-attr-style="grid-column: 1;grid-row: 15;" class="gridColumnHeader">- dont nombre de traitements obligatoires</div>
        <div ng-attr-style="grid-column: 1;grid-row: 16;" class="gridColumnHeader">- <span class="required">*</span>&nbsp;IFT-ravageurs (chimique)</div>
        <div ng-attr-style="grid-column: 1;grid-row: 17;" class="gridColumnHeader">- <span class="required">*</span>IFT-ravageurs biocontrôle</div>
        <div ng-attr-style="grid-column: 1;grid-row: 18;" class="gridColumnHeader"></div>

        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 1;" class="gridSubHeader" ng-repeat-start="vitiPestMaster in viewPortArray">{{getAgressorLabel(vitiPestMaster)}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.pressureScale|translate:'PressureScale'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.pressureEvolution|translate:'PressureEvolution'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell oddCell" ng-class="{gridCellLast: $last}" ag-read-more="{{vitiPestMaster.pressureFarmerComment}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell oddCell" ng-class="{gridCellLast: $last, notToFill: isCicadelleFlavescenceDoree(vitiPestMaster)}">{{vitiPestMaster.masterScale|translate:'MasterScale#VitiPest'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last, notToFill: isCicadelleFlavescenceDoree(vitiPestMaster)}">{{vitiPestMaster.leafPestAttackRate|translate:'PestAttackRate'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" ng-if="!isDephyExpe" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last, notToFill: isCicadelleFlavescenceDoree(vitiPestMaster)}">{{vitiPestMaster.grapePestAttackRate|translate:'PestAttackRate'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" ng-if="isDephyExpe" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last, notToFill: !isTordeuseGrappe(vitiPestMaster)}">{{vitiPestMaster.nbPestGrapePerforation}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 10;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last, notToFill: isCicadelleFlavescenceDoree(vitiPestMaster)}">{{vitiPestMaster.qualifier|translate:'PestMasterLevelQualifier'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 11;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last, notToFill: isCicadelleFlavescenceDoree(vitiPestMaster)}" ag-read-more="{{vitiPestMaster.resultFarmerComment}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 12;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last, notToFill: isCicadelleFlavescenceDoree(vitiPestMaster)}" ag-read-more="{{vitiPestMaster.adviserComments}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 13;" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 14;" id="VitiPestMasters_treatmentCount_{{$index}}" class="gridCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{vitiPestMaster.treatmentCount}}<input type="text" ng-model="vitiPestMaster.treatmentCount" ng-required="vitiPestMaster" style="opacity:0;width: 0;"/></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 15;" id="VitiPestMasters_nbPestTraitementRequired_{{$index}}" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last, notToFill: !isCicadelleFlavescenceDoree(vitiPestMaster)}">{{vitiPestMaster.nbPestTraitementRequired}}<input type="text" ng-model="vitiPestMaster.nbPestTraitementRequired" style="opacity:0;width: 0;"/></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 16;" id="VitiPestMasters_chemicalFungicideIFT_{{$index}}" class="gridCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{vitiPestMaster.chemicalFungicideIFT}}<input type="text" ng-model="vitiPestMaster.chemicalFungicideIFT" ng-required="vitiPestMaster" style="opacity:0;width: 0;"/></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 17;" id="VitiPestMasters_bioControlFungicideIFT_{{$index}}" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{vitiPestMaster.bioControlFungicideIFT}}<input type="text" ng-model="vitiPestMaster.bioControlFungicideIFT" ng-required="vitiPestMaster" style="opacity:0;width: 0;"/></div>

        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 18;" class="gridActions" ng-class="{gridCellLast: $last}" ng-repeat-end>
          <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editVitiPest(vitiPestMaster)" />
          <input type="button" class="btn-icon icon-delete" ng-class="{'icon-delete-disabled':(vitiPestMaster.agressor && requiredPests.indexOf(vitiPestMaster.agressor.reference_id) !== -1)}" value="Supprimer" title="Supprimer" ng-click="deleteVitiPest(vitiPestMaster)" ng-disabled="vitiPestMaster.agressor && requiredPests.indexOf(vitiPestMaster.agressor.reference_id) !== -1"/>
        </div>
        <div ng-attr-style="grid-column: 2;grid-row: 1 / span 18;" class="gridEmpty" ng-if="vitiPestMasters.length == 0">
          Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
        </div>
        <div ng-attr-style="grid-column: 1 / span 5;grid-row: 19;" class="gridLegendAndFooter gridFooter">
          <div class="legend">
            <span>Légende :</span>
            <div class="notToFill">Champ facultatif</div>
          </div>
          <input type="button" value="Ajouter un ravageur" ng-click="addVitiPest()"/>
        </div>
      </div>

      <div class="wwgrp">
          <s:fielderror fieldName="reportGrowingSystem.vitiPestChemicalPestIFT" />
          <span class="wwlbl">
             <label class="label"><span class="required">*</span>&nbsp;IFT-ravageurs du système de culture (chimique)&nbsp;:</label>
          </span>
          <span class="wwctrl">
            <input id="VitiPestMasters_vitiPestChemicalPestIFT"
                   class="column-xlarge"
                   type="text"
                   ng-required="viewPortArray.length > 0"
                   pattern="^\d+(?:[\.,]\d+)?$"
                   name="reportGrowingSystem.vitiPestChemicalPestIFT"
                   value="${reportGrowingSystem.vitiPestChemicalPestIFT}"
                   placeholder="ex: 1.5"/>
          </span>
      </div>
      <div class="wwgrp">
            <s:fielderror fieldName="reportGrowingSystem.vitiPestBioControlPestIFT" />
            <span class="wwlbl">
               <label class="label"><span class="required">*</span>&nbsp;IFT-ravageurs biocontrôle du système de culture&nbsp;:</label>
            </span>
            <span class="wwctrl">
              <input id="VitiPestMasters_vitiPestBioControlPestIFT"
                     class="column-xlarge"
                     type="text"
                     ng-required="viewPortArray.length > 0"
                     pattern="^\d+(?:[\.,]\d+)?$"
                     name="reportGrowingSystem.vitiPestBioControlPestIFT"
                     value="${reportGrowingSystem.vitiPestBioControlPestIFT}"
                     placeholder="ex: 1.5"/>
            </span>
      </div>
      <s:select label="Niveau global de maîtrise des ravageurs"
                id="VitiPestMasters_vitiPestQualifier"
                requiredLabel="true" ng-required="viewPortArray.length > 0" emptyOption="true"
                name="reportGrowingSystem.vitiPestQualifier" list="globalMasterLevelQualifiers"
                labelPosition="left" labelSeparator=" :" cssClass="column-xlarge"/>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
          <span class="wwlbl"><label>Modèle décisionnel&nbsp;:</label></span>
          <span class="wwctrl">
            <div>
              <a class="asLabelUnderlined" ng-if="observeManagementModeId" target="_blank" rel="noopener noreferrer"
                 href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{observeManagementModeId}}#sectionTypes_RAVAGEURS">
                 Ouvrir le modèle décisionnel
              </a>
            </div>
            <div ng-if="!observeManagementModeId">Aucun</div>
          </span>
      </div>
  </div>

  <jqdialog dialog-name="addVitiPest" auto-open="false" width="'75%'" class="dialog-form" modal="true"
      buttons="{'OK': onDialogOk, 'Annuler': onDialogCancel}"
      button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
      title="'Maîtrise des ravageurs'">

    <div>Pression avant intervention</div>

    <div class="oneFieldOutOfTwoRequired">
      <span class="required">*</span>&nbsp;<em>au moins un des deux</em>
    </div>

    <div class="wwgrp">
      <span class="wwlbl wide-label">
        <label for="codeGroupeCibleMaa" class="label">Groupe cible&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span class='contextual-help'>
          <span class='help-hover'>
            <s:text name="help.report.groupeCibleMaa" />
          </span>
        </span>
      <ui-select ng-model="editedPest.codeGroupeCibleMaa"
                 id="vitiPestGroupeCibleField"
                 reset-search-input="true"
                 theme="select2"
                 style="width: 300px;"
                 uis-open-close="onOpenVitiPestGroupeCible(isOpen)">
        <ui-select-match placeholder="ex. : Insectes">{{$select.selected.groupeCibleMaa}}</ui-select-match>
        <ui-select-choices repeat="groupeCible.codeGroupeCibleMaa as groupeCible in groupesCibles | filter : isGroupeCibleInCategory('RAVAGEUR') | filter: {groupeCibleMaa: $select.search}">
          <span ng-bind-html="groupeCible.groupeCibleMaa | highlight: $select.search"></span>
        </ui-select-choices>
      </ui-select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label">
        <label for="vitiPestAgressor" class="label">Ravageur&nbsp;:</label>
      </span>
      <span class="wwctrl">
        <span ng-if="!bioAgressors || bioAgressors.length === 0">Chargement en cours...</span>
          <ui-select id="vitiPestAgressor"
                     ng-model="editedPest.agressor"
                     reset-search-input="true"
                     theme="select2"
                     style="width: 300px;"
                     uis-open-close="onOpenVitiPestAgressor(isOpen)"
                     ng-if="bioAgressors && bioAgressors.length > 0"
                     ng-disabled="editedPest.changeableDisease">
            <ui-select-match placeholder="ex. : Black rot">{{$select.selected.adventice || $select.selected.reference_label}}</ui-select-match>
            <ui-select-choices repeat="bioAggressor in bioAgressors | filter: inGroupeCibleBioAgressor(editedPest) | filter: $select.search">
              <span ng-bind-html="bioAggressor.adventice || bioAggressor.reference_label | highlight: $select.search"></span>
            </ui-select-choices>
          </ui-select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="vitiPestPressureScale"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de pression&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="vitiPestPressureScale" ng-options="key as value for (key, value) in i18n.PressureScale"
            ng-model="editedPest.pressureScale" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
      <span class="wwlbl wide-label"><label for="vitiPestPressureEvolution">Evolution par rapport à l’année précédente&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="vitiPestPressureEvolution" ng-options="key as value for (key, value) in i18n.PressureEvolution"
            ng-model="editedPest.pressureEvolution" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiPestPressureFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
        <span class="wwctrl">
          <textarea id="vitiPestPressureFarmerComment" ng-model="editedPest.pressureFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
        </span>
    </div>

    <div class="horizontal-separator">Résultats obtenus, niveau de maîtrise finale</div>

    <div id="cicadelleFlavescenceDoreeGroup" class="notToFillGroup" ng-class="{notToFill: isCicadelleFlavescenceDoree(editedPest)}">

      <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="vitiPestPressureEvolution"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise&nbsp;:</label></span>
          <span class="wwctrl">
            <select id="vitiPestPressureEvolution" ng-options="key as value for (key, value) in i18n['MasterScale#VitiPest']"
                ng-model="editedPest.masterScale" class="column-xlarge" ng-class="{notToFill: isCicadelleFlavescenceDoree(editedPest)}">
              <option value=""></option>
            </select>
          </span>
      </div>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiPestLeafAttackRate">Note globale d’attaque sur feuilles&nbsp;:</label></span>
        <span class="wwctrl">
          <span class='contextual-help'>
            <span class='help-hover'>
              <table>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Tordeuses</th>
                  <th scope="col">Cicadelle verte</th>
                  <th scope="col">Acariens</th>
                </tr>
                <tr>
                  <th scope="col">1 – Pas de dégâts</th>
                  <td rowspan="4">Pour les tordeuses de la grappe, nombre de perforations pour 100 grappes (enre 0 et 100)</td>
                  <td>0</td>
                  <td>0</td>
                </tr>
                <tr>
                  <th scope="col">2 – Attaque faible</th>
                  <td>&lt; 50 larves pour 100 feuilles</td>
                  <td>&lt; 10% feuilles avec 1 forme mobile</td>
                </tr>
                <tr>
                  <th scope="col">3 – Attaque moyenne</th>
                  <td>50 à 100 larves pour 100 feuilles</td>
                  <td>10 à 30% feuilles avec 1 forme mobile</td>
                </tr>
                <tr>
                  <th scope="col">4 (seuil IFV) – Attaque forte</th>
                  <td>&gt; 100 larves pour 100 feuilles</td>
                  <td>&gt; 30% feuilles avec 1 forme mobile</td>
                </tr>
              </table>
            </span>
          </span>
          <select id="vitiPestLeafAttackRate" ng-options="key as value for (key, value) in i18n.PestAttackRate"
              ng-model="editedPest.leafPestAttackRate" class="column-xlarge" ng-class="{notToFill: isCicadelleFlavescenceDoree(editedPest)}">
            <option value=""></option>
          </select>
        </span>
      </div>
      <div ng-if="!isDephyExpe" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiPestGrapeAttackRate">Note globale d’attaque sur grappes&nbsp;:</label></span>
        <span class="wwctrl">
          <span class='contextual-help'>
            <span class='help-hover'>
              <table>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Tordeuses</th>
                  <th scope="col">Cicadelle verte</th>
                  <th scope="col">Acariens</th>
                </tr>
                 <tr>
                  <th scope="col">1 – Pas de dégâts</th>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                </tr>
                <tr>
                  <th scope="col">2 – Attaque faible</th>
                  <td>&lt; 5% de grappes perforées</td>
                  <td>&lt; 50 larves pour 100 feuilles</td>
                  <td>&lt; 10% feuilles avec 1 forme mobile</td>
                </tr>
                <tr>
                  <th scope="col">3 – Attaque moyenne</th>
                  <td>5 à 10% de grappes perforées</td>
                  <td>50 à 100 larves pour 100 feuilles</td>
                  <td>10 à 30% feuilles avec 1 forme mobile</td>
                </tr>
                <tr>
                  <th scope="col">4 (seuil IFV) – Attaque forte</th>
                  <td>&gt; 10% de grappes perforées</td>
                  <td>&gt; 100 larves pour 100 feuilles</td>
                  <td>&gt; 30% feuilles avec 1 forme mobile</td>
                </tr>
              </table>
            </span>
          </span>
          <select id="vitiPestGrapeAttackRate" ng-options="key as value for (key, value) in i18n.PestAttackRate"
              ng-model="editedPest.grapePestAttackRate" class="column-xlarge" ng-class="{notToFill: isCicadelleFlavescenceDoree(editedPest)}">
            <option value=""></option>
          </select>
        </span>
      </div>

      <div id="vitiNbPestGrapePerforationGroup" class="notToFillGroup" ng-class="{notToFill: (isDephyExpe && !isTordeuseGrappe(editedPest) && !isCicadelleFlavescenceDoree(editedPest))}">

      <div ng-if="isDephyExpe" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiNbPestGrapePerforation">Nombre de perforations pour 100 grappes&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text"
                 id="vitiNbPestGrapePerforation"
                 ng-model="editedPest.nbPestGrapePerforation"
                 ag-float pattern="^\d+(?:[\.,]\d{1})?$"
                 class="column-xlarge"
                 ng-class="{notToFill: !isTordeuseGrappe(editedPest)}"
                 placeholder="Note entre 0 et 100" />
        </span>
      </div>
        <div class="notToFillGroupLegend">Champ facultatif</div>
      </div> <!-- vitiNbPestGrapePerforationGroup -->

      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiPestQualifier"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Qualification du niveau de maîtrise&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="vitiPestQualifier" ng-options="key as value for (key, value) in i18n.PestMasterLevelQualifier"
              ng-model="editedPest.qualifier" class="column-xlarge" ng-class="{notToFill: isCicadelleFlavescenceDoree(editedPest)}">
            <option value=""></option>
          </select>
        </span>
      </div>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiPestResultFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
        <span class="wwctrl">
          <textarea id="vitiPestResultFarmerComment" ng-model="editedPest.resultFarmerComment"
                    class="column-xlarge" ng-class="{notToFill: isCicadelleFlavescenceDoree(editedPest)}"
                    column="20" rows="5">
          </textarea>
        </span>
      </div>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
          <span class="wwlbl wide-label"><label for="vitiPestAdviserComments">Commentaires du conseiller&nbsp;:</label></span>
          <span class="wwctrl">
            <textarea id="vitiPestAdviserComments" ng-model="editedPest.adviserComments"
                      class="column-xlarge" ng-class="{notToFill: isCicadelleFlavescenceDoree(editedPest)}"
                      column="20" rows="5">
            </textarea>
          </span>
      </div>
      <div class="notToFillGroupLegend">Champs facultatifs</div>
    </div> <!-- cicadelleFlavescenceDoreeGroup -->

    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="vitiPestTreatmentCount"><span class="required">*</span>&nbsp;Nombre de traitements&nbsp;:</label></span>
      <span class="wwctrl">
        <input type="text" id="vitiPestTreatmentCount" ng-required="viewPortArray.length > 0" ng-model="editedPest.treatmentCount" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$" class="column-xlarge" placeholder="ex: 1" />
      </span>
    </div>

    <div id="notCicadelleFlavescenceDoreeGroup" class="notToFillGroup" ng-class="{notToFill: !isCicadelleFlavescenceDoree(editedPest)}">

    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="vitiNbPestTraitementRequired">Dont nombre de traitements obligatoires&nbsp;:</label></span>
      <span class="wwctrl">
        <input type="text" id="vitiNbPestTraitementRequired" ng-model="editedPest.nbPestTraitementRequired" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$" class="column-xlarge" ng-class="{notToFill: !isCicadelleFlavescenceDoree(editedPest)}" placeholder="ex: 1" />
      </span>
    </div>

      <div class="notToFillGroupLegend">Champ facultatif</div>
    </div> <!-- notCicadelleFlavescenceDoreeGroup -->

    <div class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiPestChemicalPestIFT"><span class="required">*</span>&nbsp;IFT-ravageurs (chimique)&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text" id="vitiPestChemicalPestIFT" ng-required="viewPortArray.length > 0" ng-model="editedPest.chemicalFungicideIFT" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$" class="column-xlarge" placeholder="ex: 1.5" />
        </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="vitiPestBioControlFungicideIFT"><span class="required">*</span>&nbsp;IFT-ravageurs biocontrôle&nbsp;:</label></span>
      <span class="wwctrl">
        <input type="text" id="vitiPestBioControlPestIFT" ng-required="viewPortArray.length > 0" ng-model="editedPest.bioControlFungicideIFT" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$" class="column-xlarge" placeholder="ex: 1.5" />
      </span>
    </div>
  </jqdialog>
</div>
