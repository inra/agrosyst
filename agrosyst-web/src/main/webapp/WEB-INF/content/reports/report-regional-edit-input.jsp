<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <s:if test="reportRegional.topiaId == null">
      <title>Nouveau bilan de campagne / échelle régionale</title>
    </s:if>
    <s:else>
      <title>Bilan de campagne / échelle régionale '<s:property value="reportRegional.name" />'</title>
    </s:else>
    <content tag="current-category">reports</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/report-regional.js' /><s:property value='getVersionSuffix()'/>"></script>
    <link rel="stylesheet" type="text/css" href="<s:url value='/nuiton-js/report-regional.css' /><s:property value='getVersionSuffix()'/>" />
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />
    <script type="text/javascript">
      angular.module('ReportRegionalModule', ['Agrosyst', 'ngSanitize', 'ui.select'])
        .value('ReportRegionalInitData', {
          reportRegional: <s:property value="toJson(reportRegional)" escapeHtml="false"/>,
          reportRegionalNetworks: <s:property value="toJson(reportRegional.networks)" escapeHtml="false"/>,
          campaignsBounds: <s:property value="toJson(campaignsBounds)" escapeHtml="false" />,
          networkIdsUsed:<s:property value="toJson(networkIdsUsed)" escapeHtml="false" />,
          groupesCibles:<s:property value="toJson(groupesCibles)" escapeHtml="false" />,
          diseaseBioAgressorParentType:<s:property value="toJson(diseaseBioAgressorParentType)" escapeHtml="false" />,
          pestBioAgressorParentType:<s:property value="toJson(pestBioAgressorParentType)" escapeHtml="false" />
        });
    </script>
  </head>
  <body>
    <div ng-app="ReportRegionalModule" ng-controller="ReportRegionalController">
        <div id="filAriane">
          <ul class="clearfix">
            <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
            <li>&gt; <a href="<s:url action='report-regionals-list' namespace='/reports' />">Bilans de campagne</a></li>
            <s:if test="reportRegional.topiaId == null">
              <li>&gt; Nouveau bilan de campagne / échelle régionale</li>
            </s:if>
            <s:else>
              <li>&gt; <s:property value="reportRegional.name" /></li>
            </s:else>
          </ul>
        </div>

        <ul class="actions">
          <li><a class="action-retour" href="<s:url action='report-regionals-list' namespace='/reports' />">Retour à la liste des bilans de campagne</a></li>
        </ul>

        <ul class="float-right informations">
          <s:if test="reportRegional.name != null">
            <li>
              <span class="label">Bilan de campagne / échelle régionale</span>
              <a href="<s:url namespace='/reports' action='report-regional-edit-input'/>?reportRegionalTopiaId=<s:property value="reportRegional.topiaId" />" title="Voir le bilan de campagne">
                <s:property value="reportRegional.name" />
              </a>
            </li>
          </s:if>

          <s:if test="reportRegional.campaign != 0">
            <li><span class="label">Campagne</span>
              <s:property value="reportRegional.campaign" /> (<s:property value="(reportRegional.campaign)-1" /> - <s:property value="reportRegional.campaign" />)
            </li>
          </s:if>
          <s:if test="reportRegional.networks != null && !reportRegional.networks.empty()">
            <li>
              <span class="label">Réseau</span>
              <a href="<s:url namespace='/networks' action='networks-edit-input'/>?networkTopiaId=<s:property value="reportRegional.networks[0].topiaId" />" title="Voir le réseau">
                <s:property value="reportRegional.networks[0].name" />
              </a>
            </li>
          </s:if>
          <s:if test="reportRegional.sectors != null">
            <li>
              <span class="label">Filière</span>
              <s:text name="fr.inra.agrosyst.api.entities.Sector.%{reportRegional.sectors[0]}"/>
            </li>
          </s:if>
        </ul>

        <div class="page-content">
          <s:if test="relatedReportRegionals != null">
            <ul class="timeline">
              <s:iterator value="relatedReportRegionals" var="relatedReportRegional">
                  <li<s:if test="#relatedReportRegional.value.equals(reportRegional.topiaId)"> class="selected"</s:if>>
                    <a href="<s:url namespace='/reports' action='report-regional-edit-input' />?reportRegionalTopiaId=<s:property value='value'/>"><s:property value="key" /></a>
                  </li>
              </s:iterator>
            </ul>
          </s:if>

          <form name="reportEditForm" action="<s:url action='report-regional-edit' namespace='/reports' />" method="post" class="tabs clear" ag-confirm-on-exit>
            <s:actionerror cssClass="send-toast-to-js"/>
            <s:hidden name="reportRegionalTopiaId" value="%{reportRegional.topiaId}"/>

            <ul id="tabs-report-menu" class="tabs-menu clearfix">
              <li class="selected"><span>Généralités</span></li><!--
              --><li><span>Faits marquants</span></li>
            </ul>

            <div id="tabs-report-content" class="tabs-content">
              <div id="tab_0" ng-controller="ReportRegionalMainController">
                <fieldset>
                    <s:if test="reportRegional.topiaId == null">
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label for="selectCampaign" class="label"><span class="required">*</span>&nbsp;Campagne&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <input id="selectCampaign" type="text"
                                 name="reportRegional.campaign" ng-model="reportRegional.campaign"
                                 ng-model-options="configuration.debounce"
                                 placeholder="ex. : 2018"
                                 ag-campaign pattern="(?:19|20)[0-9]{2}"
                                 required/>
                        </span>
                      </div>
                    </s:if>
                    <s:else>
                      <div class="wwgrp">
                        <span class="wwlbl">
                          <label class="label"><span class="required">*</span>&nbsp;Campagne:</label>
                        </span>
                        <div class="wwctrl">
                           <s:property value="reportRegional.campaign" />
                        </div>
                      </div>
                    </s:else>
                    <div class="wwgrp checkbox-list">
                      <s:fielderror fieldName="reportRegional.sectors" />
                      <div class="wwlbl">
                        <label for="reportRegional_sectors" class="label"><span class="required">*</span>&nbsp;Filière:</label>
                      </div>
                      <div class="wwctrl">
                        <input id="reportRegional-sectors-{{$index}}" type="checkbox" name="reportRegional.sectors"
                               ng-repeat-start="(key, value) in i18n.Sector"
                               value="{{key}}"
                               ng-click="toggleSelectedSector(key)"
                               ng-checked="reportRegional.sectors.indexOf(key) != -1"
                               ng-required="reportRegional.sectors.length === 0"/>
                        <label for="reportRegional-sectors-{{$index}}" class="checkboxLabelAgrosyst" ng-repeat-end>{{value}}</label>
                      </div>
                    </div>
                    <div class="wwgrp checkbox-list" ng-if="reportRegional.sectors.indexOf('ARBORICULTURE') != -1" class="slide-animation">
                      <s:fielderror fieldName="reportRegional.sectorSpecies" />
                      <div class="wwlbl">
                        <label for="reportRegional_sectors" class="label"><span class="required">*</span>&nbsp;Espèces pour la filière Arboriculture:</label>
                      </div>
                      <div class="wwctrl">
                        <input id="reportRegional-sectorspecies-{{$index}}"
                               type="checkbox"
                               name="reportRegional.sectorSpecies"
                               ng-repeat-start="(key, value) in i18n.SectorSpecies"
                               value="{{key}}" ng-click="toggleSelectedSectorSpecies(key)"
                               ng-checked="reportRegional.sectorSpecies.indexOf(key) != -1"
                               ng-required="reportRegional.sectorSpecies.length === 0" />
                        <label for="reportRegional-sectorspecies-{{$index}}" class="checkboxLabelAgrosyst" ng-repeat-end>{{value}}</label>
                      </div>
                    </div>
                    <s:textfield label="Rédacteur" name="reportRegional.author" placeholder="ex. : Gérard Manvussa"
                        labelSeparator=" :" labelPosition="left" required="true" requiredLabel="true"/>
                    <s:textfield label="Nom du bilan de campagne / échelle regional" name="reportRegional.name"
                        labelSeparator=" :" labelPosition="left" required="true" requiredLabel="true"
                        ng-model="reportRegional.name"/>

                    <div class="wwgrp" ng-controller="ReportRegionalNetworkController">
                      <input type="hidden" name="reportRegionalNetworkIds" value="{{reportRegionalNetwork.topiaId}}" ng-repeat="reportRegionalNetwork in reportRegionalNetworks" />
                      <s:fielderror fieldName="reportRegionalNetworkIds" />
                      <span class="wwlbl"><label for="networks" class="label"><span class="required">*</span>&nbsp;Réseaux&nbsp;:</label></span>
                      <span class="wwctrl">
                        <input type="button" class="btn-icon icon-locate float-right" value="Afficher la hiérarchie des réseaux"
                          title="Afficher la hiérarchie des réseaux" ng-click="displayNetworks(reportRegional.name, reportRegionalNetworks)"/>
                          <div ng-repeat="network in reportRegionalNetworks" class="tag" ng-class="{noclear: $first}">
                            <span>{{network.name}}</span>

                              <input type="button"
                                     class="btn-icon icon-delete"
                                     ng-class="{'icon-delete-disabled': networkIdsUsed.indexOf(network.topiaId) !== -1}"
                                     ng-disabled="networkIdsUsed.indexOf(network.topiaId) !== -1"
                                     value="Supprimer"
                                     title="Supprimer"
                                     ng-click="removeNetwork(network)"/>

                          </div>
                          <input id="networks" type="text" placeholder="Tapez le nom d'un réseau pour l'ajouter" class="clear float-left" ng-required="reportRegionalNetworks.length === 0"/>
                      </span>
                    </div>
                </fieldset>
              </div>

              <!-- Faits marquants -->
              <div id="tab_1">
                <fieldset>
                  <div class= "infoLine block"
                       style="margin-top: 20px;">
                    <div class="endLineEvidence">
                      <div style="cursor:pointer"
                          class="textIcones blue x2"
                          title="Information importante">!</div>
                    </div>
                    <div>
                      <a href="<s:url namespace='/referential' action='management-mode-bio-aggressors-download'/>"
                         target="" rel="noopener noreferrer" class="black-link"
                         style="color: #013f4e;"
                         title="Cliquez sur ce bouton pour télécharger la liste des bioagresseurs disponibles sur Agrosyst">
                         Liste des bioagresseurs disponibles sur Agrosyst
                        <em class="fa fa-download" aria-hidden="true"></em>
                      </a>
                    </div>
                  </div>
                <fieldset>
                  <s:textarea label="Faits marquants de l’année (climat, bioagresseurs…)" name="reportRegional.highlights"
                    labelSeparator=" :" labelPosition="left" rows="10"/>

                  <div class="subRubrique">
                    <h2>Conditions climatiques - Pluviométrie</h2>

                    <div ng-if="reportRegional.sectors.indexOf('CULTURES_TROPICALES') == -1">
                      <div class="wwgrp">
                        <s:fielderror fieldName="reportRegional.rainfallMarchJune" />
                        <span class="wwlbl">
                          <label class="label">Pluviométrie : nombre de mm du 1er mars au 30 juin&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <span class="input-append">
                            <input type="text" placeholder="ex: 5" name="reportRegional.rainfallMarchJune" ng-model="reportRegional.rainfallMarchJune" ag-float ag-float-positive pattern="^[\-\+]?\d*[\.,]?\d*$" />
                            <span class="add-on">mm</span>
                          </span>
                        </span>
                      </div>

                      <div class="wwgrp">
                        <s:fielderror fieldName="reportRegional.rainyDaysMarchJune" />
                        <span class="wwlbl">
                          <label class="label">Nombre de jours de pluie du 1er mars au 30 juin&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <input type="text" placeholder="ex: 5" name="reportRegional.rainyDaysMarchJune" ng-model="reportRegional.rainyDaysMarchJune" ag-integer pattern="\d+" />
                        </span>
                      </div>

                      <div class="wwgrp">
                        <s:fielderror fieldName="reportRegional.rainfallJulyOctober" />
                        <span class="wwlbl">
                          <label class="label">Pluviométrie : nombre de mm du 1er juillet au 31 octobre&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <span class="input-append">
                            <input type="text" placeholder="ex: 5" name="reportRegional.rainfallJulyOctober" ng-model="reportRegional.rainfallJulyOctober" ag-float ag-float-positive pattern="^[\-\+]?\d*[\.,]?\d*$" />
                            <span class="add-on">mm</span>
                          </span>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div ng-if="reportRegional.sectors.indexOf('CULTURES_TROPICALES') != -1">
                    <table class="data-table">
                      <thead>
                        <tr>
                          <th scope="col">Trimestre</th>
                          <th scope="col">1er janvier au 31 mars</th>
                          <th scope="col">1er avril au 30 juin</th>
                          <th scope="col">1er juillet au 30 septembre</th>
                          <th scope="col">1er octobre au 31 décembre</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="col" class="thead">Nombre de mm sur la période</th>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainfallJanuaryMarch" ng-model="reportRegional.tropicalRainfallJanuaryMarch" placeholder="ex: 5" ag-float ag-float-positive pattern="^[\-\+]?\d*[\.,]?\d*$" />
                          </td>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainfallAprilJune" ng-model="reportRegional.tropicalRainfallAprilJune" ag-float ag-float-positive pattern="^[\-\+]?\d*[\.,]?\d*$" />
                          </td>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainfallJulySeptember" ng-model="reportRegional.tropicalRainfallJulySeptember" ag-float ag-float-positive pattern="^[\-\+]?\d*[\.,]?\d*$" />
                          </td>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainfallOctoberDecember" ng-model="reportRegional.tropicalRainfallOctoberDecember" ag-float ag-float-positive pattern="^[\-\+]?\d*[\.,]?\d*$" />
                          </td>
                        </tr>
                        <tr>
                          <th scope="col" class="thead">Nombre de jours de pluie sur la période</th>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainyDaysJanuaryMarch" ng-model="reportRegional.tropicalRainyDaysJanuaryMarch" placeholder="ex: 5" ag-integer pattern="\d+" />
                          </td>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainyDaysAprilJune" ng-model="reportRegional.tropicalRainyDaysAprilJune" ag-integer pattern="\d+" />
                          </td>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainyDaysJulySeptember" ng-model="reportRegional.tropicalRainyDaysJulySeptember" ag-integer pattern="\d+" />
                          </td>
                          <td>
                            <input type="text" name="reportRegional.tropicalRainyDaysOctoberDecember" ng-model="reportRegional.tropicalRainyDaysOctoberDecember" ag-integer pattern="\d+" />
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <div class="subRubrique" ng-controller="ReportRegionalPressureController">
                    <h2>Pression en bioagresseurs dans la région</h2>

                    <input type="hidden" name="diseasePressuresJson" value="{{reportRegional.diseasePressures}}" />
                    <div class="table-enclosure" style="padding:0;">
                      <label>Pression en maladies</label>
                      <table class="data-table">
                        <thead>
                          <tr>
                            <th scope="col">Groupe cible</th>
                            <th scope="col">Maladie(s)</th>
                            <th scope="col" class="column-small" rowspan="2">Cultures concernées</th>
                            <th scope="col">Pression de l’année (échelle régionale)</th>
                            <th scope="col">Evolution de la pression par rapport à la campagne précédente</th>
                            <th scope="col">Commentaires</th>
                            <th scope="col" class="column-small">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-if="reportRegional.diseasePressures.length == 0" class="empty-table">
                            <td colspan="6">Aucune maladie</td>
                          </tr>
                          <tr ng-repeat="diseasePressure in reportRegional.diseasePressures">
                            <td>{{getGroupeCibleLabel(diseasePressure.codeGroupeCibleMaa)}}</td>
                            <td>
                              <span ng-repeat="agressor in diseasePressure.agressors">
                                {{agressor.reference_label}}<span ng-if="!$last">,</span>
                              </span>
                            </td>
                            <td>{{diseasePressure.crops}}</td>
                            <td>{{diseasePressure.pressureScale|translate:'PressureScale#RegionPest'}}</td>
                            <td>{{diseasePressure.pressureEvolution|translate:'PressureEvolution'}}</td>
                            <td ag-read-more="{{diseasePressure.comment}}"></td>
                            <td>
                              <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editPestOrDisease(diseasePressure, true)"/>
                              <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deletePestOrDisease($index, true)"/>
                            </td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="7">
                              <div class="table-end-button">
                                <input type="button" value="Ajouter une maladie" ng-click="addPestOrDisease(true)"/>
                              </div>
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>

                    <input type="hidden" name="pestPressuresJson" value="{{reportRegional.pestPressures}}" />
                    <div class="table-enclosure">
                      <label>Pression en ravageurs</label>
                      <table class="data-table">
                        <thead>
                          <tr>
                            <th scope="col">Groupe cible</th>
                            <th scope="col">Ravageur(s)</th>
                            <th scope="col" class="column-small" rowspan="2">Cultures concernées</th>
                            <th scope="col">Pression de l’année (échelle régionale)</th>
                            <th scope="col">Evolution de la pression par rapport à la campagne précédente</th>
                            <th scope="col">Commentaires</th>
                            <th scope="col" class="column-small">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-if="reportRegional.pestPressures.length == 0" class="empty-table">
                            <td colspan="6">Aucun ravageur</td>
                          </tr>
                          <tr ng-repeat="pestPressure in reportRegional.pestPressures">
                            <td>{{getGroupeCibleLabel(pestPressure.codeGroupeCibleMaa)}}</td>
                            <td>
                              <span ng-repeat="agressor in pestPressure.agressors">
                                {{agressor.reference_label}}<span ng-if="!$last">,</span>
                              </span>
                            </td>
                            <td>{{pestPressure.crops}}</td>
                            <td>{{pestPressure.pressureScale|translate:'PressureScale#RegionPest'}}</td>
                            <td>{{pestPressure.pressureEvolution|translate:'PressureEvolution'}}</td>
                            <td ag-read-more="{{pestPressure.comment}}"></td>
                            <td>
                              <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editPestOrDisease(pestPressure)"/>
                              <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deletePestOrDisease($index)"/>
                            </td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="7">
                              <div class="table-end-button">
                                <input type="button" value="Ajouter un ravageur" ng-click="addPestOrDisease()"/>
                              </div>
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>

                    <jqdialog dialog-name="PestEdit" auto-open="false" width="'55%'" class="dialog-form"
                              buttons="{'OK': onEditDiseaseOk, 'Annuler': onEditDiseaseCancel}"
                              button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}">

                      <div class="oneFieldOutOfTwoRequired">
                        <span class="required">*</span>&nbsp;<em>au moins un des deux</em>
                      </div>

                      <div class="wwgrp">
                        <span class="wwlbl wide-label">
                          <label for="codeGroupeCibleMaa" class="label">Groupe cible&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <span class='contextual-help'>
                            <span class='help-hover'>
                              <s:text name="help.report.groupeCibleMaa" />
                            </span>
                          </span>
                          <ui-select ng-model="editedPest.codeGroupeCibleMaa"
                                     id="groupeCibleField"
                                     reset-search-input="true"
                                     theme="select2"
                                     uis-open-close="onOpenGroupeCible(isOpen)"
                                     style="width: 300px;">
                            <ui-select-match placeholder="ex. : Champignons">{{$select.selected.groupeCibleMaa}}</ui-select-match>
                            <ui-select-choices repeat="groupeCible.codeGroupeCibleMaa as groupeCible in groupesCibles | filter : isGroupeCibleInCategory(isDisease) | filter: {groupeCibleMaa: $select.search}">
                              <span ng-bind-html="groupeCible.groupeCibleMaa | highlight: $select.search"></span>
                            </ui-select-choices>
                          </ui-select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl wide-label">
                          <label for="diseaseAgressor" class="label">{{isDisease ? 'Maladie(s)' : 'Ravageur(s)'}}&nbsp;:</label>
                        </span>
                        <span class="wwctrl">
                          <ui-select ng-model="agressor.selected"
                                     id="diseaseAgressor"
                                     reset-search-input="true"
                                     theme="select2"
                                     style="width: 300px;"
                                     uis-open-close="onOpenAggressor(isOpen)"
                                     on-select="addAgressor($item)">
                            <ui-select-match placeholder="ex. : Amadouvier officinal">{{$select.selected.adventice || $select.selected.reference_label}}</ui-select-match>
                            <ui-select-choices repeat="bioAggressor in bioAgressors | filter: disableWhenMain | filter: inGroupeCibleNotUsedBioAgressor | filter: $select.search">
                              <span ng-bind-html="bioAggressor.adventice || bioAggressor.reference_label | highlight: $select.search"></span>
                            </ui-select-choices>
                          </ui-select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl wide-label">&nbsp;</span>
                        <span class="wwctrl">
                          <div ng-repeat="agressor in editedPest.agressors" class="tag" ng-class="{noclear: $first}">
                            <span>{{agressor.reference_label}}</span>
                            <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="removeAgressor(agressor)"/>
                          </div>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl wide-label"><label for="diseaseCrops"><span class="required">*</span>&nbsp;Cultures&nbsp;:</label></span>
                        <span class="wwctrl">
                          <input id="diseaseCrops" type="text" ng-model="editedPest.crops" class="column-xlarge"/>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl wide-label"><label for="diseasePressurceScale">Pression de l’année&nbsp;:</label></span>
                        <span class="wwctrl">
                          <select id="diseasePressurceScale" ng-options="key as value for (key, value) in i18n['PressureScale#RegionPest']"
                              ng-model="editedPest.pressureScale" class="column-xlarge">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl wide-label"><label for="diseasePressureEvolution">Evolution de la pression par rapport à la campagne précédente&nbsp;:</label></span>
                        <span class="wwctrl">
                          <select id="diseasePressureEvolution" ng-options="key as value for (key, value) in i18n.PressureEvolution"
                              ng-model="editedPest.pressureEvolution" class="column-xlarge">
                            <option value=""></option>
                          </select>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <span class="wwlbl wide-label"><label for="diseaseComment">Commentaires&nbsp;:</label></span>
                        <span class="wwctrl">
                          <textarea id="diseaseComment" type="text" ng-model="editedPest.comment" class="column-xlarge" column="20" rows="5"></textarea>
                        </span>
                      </div>
                    </jqdialog>
                  </div>

                  <div class="subRubrique" ng-if="reportRegional.sectors.indexOf('ARBORICULTURE') != -1 && reportRegional.sectorSpecies.indexOf('POMMIER_POIRIER') != -1">
                    <h2>Pression Tavelure dans la région</h2>

                    <div class="diagnoseEntries">
                      <div class="wwgrp">
                        <s:fielderror fieldName="reportRegional.primaryContaminations" />
                        <span class="wwlbl">
                           <label class="label">INOKI : nombre de contaminations primaires</label>
                        </span>
                        <span class="wwctrl">
                          <input type="text" name="reportRegional.primaryContaminations" value="${reportRegional.primaryContaminations}" placeholder="ex: 1.5"/>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <s:fielderror fieldName="reportRegional.numberOfDaysWithPrimaryContaminations" />
                        <span class="wwlbl">
                           <label class="label">INOKI : nombre de jours avec contaminations primaires</label>
                        </span>
                        <span class="wwctrl">
                          <input type="text" name="reportRegional.numberOfDaysWithPrimaryContaminations" value="${reportRegional.numberOfDaysWithPrimaryContaminations}" placeholder="ex: 1"/>
                        </span>
                      </div>
                      <div class="wwgrp">
                        <s:fielderror fieldName="reportRegional.secondaryContaminations" />
                        <span class="wwlbl">
                           <label class="label">RIM Pro : nombre de contaminations secondaires (fruit)</label>
                        </span>
                        <span class="wwctrl">
                          <input type="text" name="reportRegional.secondaryContaminations" value="${reportRegional.secondaryContaminations}" placeholder="ex: 1.5"/>
                        </span>
                      </div>

                      <div class="wwgrp">
                        <s:fielderror fieldName="reportRegional.rimSum" />
                        <span class="wwlbl">
                           <label class="label">RIM Pro : somme des RIM sur l’année</label>
                        </span>
                        <span class="wwctrl">
                          <input type="text" name="reportRegional.rimSum" value="${reportRegional.rimSum}" placeholder="ex: 1.5"/>
                        </span>
                      </div>
                    </div>
                  </div>

                </fieldset>
              </div>

              <span class="form-buttons">
                <a class="btn-secondary" href="<s:url action='report-regionals-list' namespace='/reports' />">Annuler</a>
                <input type="submit" class="btn-primary" value="Enregistrer" <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>/>
              </span>
            </div>
          </form>

        </div>
    </div>

    <div id="show-networks" title="Hiérarchie des réseaux" class="auto-hide">
      Chargement...
    </div>
  </body>
</html>
