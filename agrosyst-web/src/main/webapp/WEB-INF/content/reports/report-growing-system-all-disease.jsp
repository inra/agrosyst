<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="CropDiseaseMastersController">

  <input type="hidden" name="cropDiseaseMastersJson" value="{{getCropPestMasters()}}"/>

  <%-- section --%>
  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadCropPestMaster('disease')" href=""><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Maîtrise des maladies</a>
          <div ng-if="!isDephyExpe && ((viewPortArray && viewPortArray.length === 0) || (cropPestMasters && cropPestMasters.length === 0))">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>

  <div ng-show="showDiseaseCropPestMasters" class="loadContentEnclosure">
    <div class="grid-enclosure" ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;"
          ng-style="{'-ms-grid-columns': '20% (1fr)[' + (viewPortArray.length||1) + '] min-content;'}">
      <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
      <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
      <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnMainHeader">Pression avant intervention&nbsp;:</div>
      <div ng-attr-style="grid-column: 1;grid-row: 4;" class="gridColumnHeader oddCell">- <span ng-if="!isDephyExpe" class="required"class="required">*&nbsp;</span>Échelle de pression</div>
      <div ng-attr-style="grid-column: 1;grid-row: 5;" class="gridColumnHeader">- Expression de l’agriculteur</div>
      <div ng-attr-style="grid-column: 1;grid-row: 6;" class="gridColumnMainHeader oddCell">Résultats obtenus, niveau de maîtrise finale&nbsp;:</div>
      <div ng-attr-style="grid-column: 1;grid-row: 7;" class="gridColumnHeader">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise</div>
      <div ng-attr-style="grid-column: 1;grid-row: 8;" class="gridColumnHeader oddCell">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>{{ !isDephyExpe ? "Qualification du niveau de maîtrise" : "Satisfaction" }}</div>
      <div ng-attr-style="grid-column: 1;grid-row: 9;" class="gridColumnHeader">- Expression de l’agriculteur</div>
      <div ng-attr-style="grid-column: 1;grid-row: 10;" class="gridColumnMainHeader oddCell">IFT&nbsp;:</div>
      <div ng-attr-style="grid-column: 1;grid-row: 11;" class="gridColumnHeader">- IFT-Fongicide</div>
      <div ng-attr-style="grid-column: 1;grid-row: 12;" class="gridColumnHeader oddCell">- Commentaires du conseiller</div>
      <div ng-attr-style="grid-column: 1;grid-row: 13;" class="gridColumnHeader"></div>

      <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 1;" class="gridMainHeader" ng-repeat-start="pestMaster in viewPortArray" ng-if="pestMaster.$$cropspan > 0">
        <span ng-repeat="crop in pestMaster.$$crop.crops">
          {{crop.name}}<span ng-if="!$last">,</span>
        </span>
      </div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridSubHeader">
        {{getAgressorLabel(pestMaster)}}
      </div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{pestMaster.pressureScale|translate:'PressureScale'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.pressureFarmerComment}}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" class="gridCell oddCell" ng-class="{gridCellLast: $last}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell" ng-class="{gridCellLast: $last}">{{pestMaster.masterScale|translate:'MasterScale#Pest'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{pestMaster.qualifier|translate:'PestMasterLevelQualifier'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.resultFarmerComment}}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 10;" class="gridCell oddCell" ng-class="{gridCellLast: $last}"></div>
      <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 11;" class="gridCell cellForCrop" ng-class="{gridCellLast: $last}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.iftMain}}</div>
      <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 12;" class="gridCell cellForCrop oddCell" ng-class="{gridCellLast: $last}" ng-if="pestMaster.$$cropspan > 0" ag-read-more="{{pestMaster.$$crop.adviserComments}}"></div>

      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 13;" class="gridActions" ng-class="{gridCellLast: $last}" ng-repeat-end>
        <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editPestMasterFromCropPestMaster(pestMaster.$$crop, pestMaster)" />
        <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteCropPestMaster(pestMaster.$$crop)" />
      </div>
      <div ng-attr-style="grid-column: 2;grid-row: 1 / span 13;" class="gridEmpty" ng-if="viewPortArray.length == 0">
        Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
      </div>
      <div ng-attr-style="grid-column: 1 / span 5;grid-row: 14;" class="gridFooter">
        <input type="button" value="Ajouter une culture" ng-click="addCropPestMaster()"/>
      </div>
    </div>

    <div class="wwgrp">
      <span class="wwlbl"><label>Modèle décisionnel&nbsp;:</label></span>
      <span class="wwctrl">
        <div>
          <a class="asLabelUnderlined" ng-if="observeManagementModeId" target="_blank" rel="noopener noreferrer"
             href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{observeManagementModeId}}#sectionTypes_MALADIES">
             Ouvrir le modèle décisionnel
          </a>
        </div>
        <div ng-if="!observeManagementModeId">Aucun</div>
      </span>
    </div>
  </div>
</div>
