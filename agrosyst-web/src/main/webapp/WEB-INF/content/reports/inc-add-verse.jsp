<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div>
  <%@include file="inc-crop-template.jsp" %>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allVerseRiskScale">Échelle de risque&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allVerseRiskScale" ng-options="key as value for (key, value) in i18n.RiskScale"
          ng-model="editedCropObject.riskScale" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allVerseRiskFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="allVerseRiskFarmerComment" ng-model="editedCropObject.riskFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allVerseVerseMasterScale">Échelle de maîtrise&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allVerseVerseMasterScale" ng-options="key as value for (key, value) in i18n['MasterScale#Verse']"
          ng-model="editedCropObject.masterScale" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
 <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="allVerseQualifier">{{ !isDephyExpe ? "Qualification du niveau de maîtrise" : "Satisfaction" }}&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="allVerseQualifier" ng-options="key as value for (key, value) in i18n.PestMasterLevelQualifier"
            ng-model="editedCropObject.qualifier" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
  </div>
  <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="allVerseResultFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
      <span class="wwctrl">
        <textarea id="allVerseResultFarmerComment" ng-model="editedCropObject.resultFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
      </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="iftMain-all-verse">IFT-Régulateur&nbsp;:</label></span>
    <span class="wwctrl">
      <input id="iftMain-all-verse" type="text" ng-model="editedCropObject.iftMain" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allVerseAdviserComments">Commentaires du conseiller&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="allVerseAdviserComments" ng-model="editedCropObject.adviserComments" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>
</div>
