<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2022 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div id="addAdventice" class="asTable">

  <div class="horizontal-separator">Pression avant désherbage</div>
  <div class="wwgrp">
    <span class="wwlbl wide-label">
      <label for="editedPestBioAgressor"><span class="required">*</span>&nbsp;Adventice&nbsp;:
        <span ng-if="!editedPestMaster.agressor">
          <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
        </span>
      </label>
    </span>
    <span class="wwctrl">
      <span ng-if="!bioAgressors || bioAgressors.length === 0">Chargement en cours...</span>
      <ui-select id="editedPestBioAgressor"
                 ng-model="editedPestMaster.agressor"
                 reset-search-input="true"
                 theme="select2"
                 style="width: 300px;"
                 ng-if="bioAgressors && bioAgressors.length > 0"
                 uis-open-close="onOpenEditedPestBioAgressor(isOpen)">
        <ui-select-match placeholder="ex. : Black rot">{{$select.selected.adventice || $select.selected.reference_label}}</ui-select-match>
        <ui-select-choices repeat="bioAggressor in bioAgressors | filter: $select.search">
          <span ng-bind-html="bioAggressor.adventice || bioAggressor.reference_label | highlight: $select.search"></span>
        </ui-select-choices>
      </ui-select>
    </span>
  </div>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="diseasePressureScale"><span class="required">*</span>&nbsp;Échelle de pression&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="diseasePressureScale" ng-options="key as value for (key, value) in i18n['PressureScale#Adventice']"
          ng-model="editedPestMaster.pressureScale" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="pressureFarmerComment">Expression de l'agriculteur&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="pressureFarmerComment" type="text" ng-model="editedPestMaster.pressureFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>

  <div class="horizontal-separator">Résultats obtenus, niveau de maîtrise finale</div>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="pestMasterScale"><span class="required">*</span>&nbsp;Échelle de maîtrise&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="pestMasterScale" ng-options="key as value for (key, value) in i18n['MasterScale#Adventice']"
          ng-model="editedPestMaster.masterScale" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="resultSatisfaction"><span class="required">*</span>&nbsp;{{ !isDephyExpe ? "Qualification du niveau de maîtrise" : "Satisfaction" }}&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="resultSatisfaction" ng-options="key as value for (key, value) in i18n.PestMasterLevelQualifier"
          ng-model="editedPestMaster.qualifier" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="resultFarmerComment">Expression de l'agriculteur&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="resultFarmerComment" type="text" ng-model="editedPestMaster.resultFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>

</div>
