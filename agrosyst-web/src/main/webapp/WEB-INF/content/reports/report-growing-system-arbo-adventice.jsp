<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="CropPestMasterEditController">
<div ng-controller="CropAllPestMastersController">
<div ng-controller="CropArboAdventiceMastersController">
  <input type="hidden" name="arboCropAdventiceMastersJson" value="{{cropPestMasters}}"/>

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadCropPestMaster('arboAdventice')" href=""><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Maîtrise des adventices</a>
          <div ng-if="!isDephyExpe && ((viewPortArray && viewPortArray.length === 0) || (cropPestMasters && cropPestMasters.length === 0))">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>

  <div ng-show="showArboAdventiceCropPestMasters" class="loadContentEnclosure">
    <div class="grid-enclosure"
      ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;">
      <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
      <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
      <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnMainHeader">Pression sur le système de culture&nbsp;:</div>
      <div ng-attr-style="grid-column: 1;grid-row: 4;" class="gridColumnHeader oddCell">- {{isDephyExpe ? "Échelle de pression" : "Niveau d’enherbement"}}</div>
      <div ng-attr-style="grid-column: 1;grid-row: 5;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Evolution par rapport à l’année précédente</div>
      <div ng-attr-style="grid-column: 1;grid-row: 6;" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}" class="gridColumnMainHeader">Résultats obtenus, niveau de maîtrise finale&nbsp;:</div>
      <div ng-attr-style="grid-column: 1;grid-row: 7;" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}" class="gridColumnHeader">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise{{isDephyExpe ? " visée" : ""}} </div>
      <div ng-attr-style="grid-column: 1;grid-row: 8;" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}" class="gridColumnHeader">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Qualification du niveau de maîtrise</div>
      <div ng-attr-style="grid-column: 1;grid-row: 9;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Expression de l’agriculteur</div>
      <div ng-attr-style="grid-column: 1;grid-row: 10;" ng-if="!isDephyExpeAndNotDisplayForced()"class="gridColumnHeader oddCell">- Commentaires du conseiller</div>
      <div ng-attr-style="grid-column: 1;grid-row: 11;" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}" class="gridColumnMainHeader">IFT&nbsp;:</div>
      <div ng-attr-style="grid-column: 1;grid-row: 12;" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}" class="gridColumnHeader">- Nombre de traitements</div>
      <div ng-attr-style="grid-column: 1;grid-row: 13;" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}" class="gridColumnHeader">- IFT-Herbicide (chimique) (insecticides, anti-limaces…)</div>
      <div ng-attr-style="grid-column: 1;grid-row: 14;" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced()}" class="gridColumnHeader">- IFT Herbicide biocontrôle</div>
      <div ng-attr-style="grid-column: 1;grid-row: 15;" ng-class="{oddCell: isDephyExpeAndNotDisplayForced()}" class="gridColumnHeader"></div>

      <div ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 1;" class="gridMainHeader" ng-repeat-start="pestMaster in viewPortArray" ng-if="pestMaster.$$cropspan > 0">
        <span ng-repeat="crop in pestMaster.$$crop.crops">
          {{crop.name}}<span ng-if="!$last">,</span>
        </span>
      </div>
      <div ng-if="!isDephyExpe" ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridSubHeader">{{pestMaster.agressor.adventice || pestMaster.agressor.reference_label}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{pestMaster.grassingLevel|translate:'GrassingLevel'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}">{{pestMaster.grassingEvolution|translate:'GrassingEvolution'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" class="gridCell" ng-class="{gridCellLast: $last, oddCell: !isDephyExpeAndNotDisplayForced()}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell" ng-class="{gridCellLast: $last, oddCell: isDephyExpeAndNotDisplayForced()}">{{pestMaster.masterScale|translate:'MasterScale#ArboAdventice' + (isDephyExpe ? 'Expe' : '')}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" class="gridCell" ng-class="{gridCellLast: $last, oddCell: !isDephyExpeAndNotDisplayForced()}">{{pestMaster.qualifier|translate:'PestMasterLevelQualifier'}}</div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.resultFarmerComment}}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 10;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell oddCell" ng-class="{gridCellLast: $last}" ag-read-more="{{pestMaster.adviserComments}}"></div>
      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 11;" class="gridCell" ng-class="{gridCellLast: $last, oddCell: isDephyExpeAndNotDisplayForced()}"></div>
      <div id="ArboPestCropPestMasters_treatmentCount_{{$index}}" ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 12;" class="gridCell cellForCrop" ng-class="{gridCellLast: $last, oddCell: !isDephyExpeAndNotDisplayForced()}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.treatmentCount}}<input type="text" ng-model="pestMaster.$$crop.treatmentCount" style="opacity:0;width: 0;"/></div>
      <div id="ArboPestCropPestMasters_chemicalPestIFT_{{$index}}" ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 13;" class="gridCell cellForCrop" ng-class="{gridCellLast: $last, oddCell: isDephyExpeAndNotDisplayForced()}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.chemicalPestIFT}}<input type="text" ng-model="pestMaster.$$crop.chemicalPestIFT" style="opacity:0;width: 0;"/></div>
      <div id="ArboPestCropPestMasters_bioControlPestIFT_{{$index}}" ng-attr-style="grid-column: {{$index+2}} / span {{pestMaster.$$cropspan}};grid-row: 14;" class="gridCell cellForCrop" ng-class="{gridCellLast: $last, oddCell: !isDephyExpeAndNotDisplayForced()}" ng-if="pestMaster.$$cropspan > 0">{{pestMaster.$$crop.bioControlPestIFT}}<input type="text" ng-model="pestMaster.$$crop.bioControlPestIFT" style="opacity:0;width: 0;"/></div>

      <div ng-attr-style="grid-column: {{$index+2}};grid-row: 15;" class="gridActions" ng-class="{gridCellLast: $last}" ng-repeat-end>
        <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editPestMasterFromCropPestMaster(pestMaster.$$crop, pestMaster)" />
        <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteCropPestMaster(pestMaster.$$crop)" />
      </div>
      <div ng-attr-style="grid-column: 2;grid-row: 1 / span 15;" class="gridEmpty" ng-if="viewPortArray.length == 0">
        Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
      </div>
      <div ng-attr-style="grid-column: 1 / span 5;grid-row: 16;" class="gridFooter">
        <input type="button" value="Ajouter une culture" ng-click="addCropPestMaster()"/>
      </div>
    </div>

    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl"><label>Modèle décisionnel&nbsp;:</label></span>
        <span class="wwctrl">
          <div>
            <a class="asLabelUnderlined" ng-if="observeManagementModeId" target="_blank" rel="noopener noreferrer"
               href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{observeManagementModeId}}#sectionTypes_ADVENTICES">
               Ouvrir le modèle décisionnel
            </a>
          </div>
          <div ng-if="!observeManagementModeId">Aucun</div>
        </span>
    </div>
  </div>

  <!-- edition -->
  <jqdialog dialog-name="addArboAdventiceCrops" auto-open="false" width="'55%'" class="dialog-form" modal="true"
      buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
      button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
      title="'Maîtrise des adventices'">
      <div class="asTable">

        <%@include file="inc-crop-template.jsp" %>

        <div class="section"><span class="required">*</span>&nbsp;Adventice(s) pour ces cultures</div>

        <div>
          <ul class="asTable">
            <li class="asContentTable0 fromContent">

              <span ng-repeat="pestMaster in editedCropObject.pestMasters">
                <span style="cursor:pointer" class="link"
                   ng-click="editPestMaster(editedCropObject, pestMaster)">{{pestMaster.agressor.adventice || pestMaster.agressor.reference_label}}</span>
                <input type="button" class="btn-icon icon-delete" ng-click="deletePestMaster(editedCropObject, pestMaster)" value="x" title="Supprimer"/>
                <span ng-if="!$last">,</span>
              </span>

              <span class="info" ng-if="editedCropObject.pestMasters.length === 0">
                Aucun adventice déclaré
              </span>

              <span class="endLine" ng-if="editedCropObject.pestMasters.length === 0">
                <div style="cursor:pointer"
                     class="textIcones blue x2"
                     ng-click="editPestMaster(editedCropObject)"
                     title="Déclarer une adventice">
                     +
                </div>
              </span>

            </li>
          </ul>
        </div>

        <div class="section">Quel niveau d’utilisation de pesticides ?</div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="arboAdventicetreatmentCount">Nombre de traitements&nbsp;:</label></span>
          <span class="wwctrl">
            <input id="arboAdventicetreatmentCount" type="text"
                 class="column-xlarge" ng-model="editedCropObject.treatmentCount" placeholder="ex: 1" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="arboAdventicechemicalPestIFT">IFT-Herbicide (chimique) (insecticides, anti-limaces…)&nbsp;:</label></span>
          <span class="wwctrl">
            <input id="arboAdventicechemicalPestIFT" type="text"
                 class="column-xlarge" ng-model="editedCropObject.chemicalPestIFT" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
          </span>
        </div>

        <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="arboAdventicebioControlPestIFT">IFT Herbicide biocontrôle&nbsp;:</label></span>
          <span class="wwctrl">
            <input id="arboAdventicebioControlPestIFT" type="text"
                 class="column-xlarge" ng-model="editedCropObject.bioControlPestIFT" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
          </span>
        </div>
      </div>
  </jqdialog>

  <jqdialog dialog-name="addArboAdventice" auto-open="false" width="'55%'" class="dialog-form" modal="true"
       buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
       button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
       title="'Adventice ciblée'">

    <div class="horizontal-separator">Pression avant désherbage</div>

    <div class="wwgrp" ng-if="!isDephyExpe">
      <span class="wwlbl wide-label"><label for="arboAdventiceAgressor" class="label"><span class="required">*</span>&nbsp;Adventice&nbsp;:</label></span>
      <span class="wwctrl">
        <select
            id="arboAdventiceAgressor"
            ng-model="editedPestMaster.agressor"
            ng-if="bioAgressors && bioAgressors.length > 0"
            ng-options="bioAgressor as (bioAgressor.adventice ? bioAgressor.adventice : bioAgressor.reference_label) for bioAgressor in bioAgressors"
            class="column-xlarge">
        </select>
      </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboAdventiceGrassingLevel">{{isDephyExpe ? "Échelle de pression" : "Niveau d’enherbement"}}&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboAdventiceGrassingLevel" ng-options="key as value for (key, value) in i18n.GrassingLevel"
            ng-model="editedPestMaster.grassingLevel" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboAdventiceGrassingEvolution">Evolution par rapport à l’année précédente&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboAdventiceGrassingEvolution" ng-options="key as value for (key, value) in i18n.GrassingEvolution"
            ng-model="editedPestMaster.grassingEvolution" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>

    <div class="horizontal-separator">Résultats obtenus, niveau de maîtrise finale</div>

    <div class="wwgrp">
        <span class="wwlbl wide-label"><label for="arboAdventiceAdventiceMasterScale"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise{{isDephyExpe ? " visée" : ""}}&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="arboAdventiceAdventiceMasterScale" ng-options="key as value for (key, value) in i18n['MasterScale#ArboAdventice' + (isDephyExpe ? 'Expe' : '')]"
              ng-model="editedPestMaster.masterScale" class="column-xlarge">
            <option value=""></option>
          </select>
        </span>
    </div>
    <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboAdventiceQualifier"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Qualification du niveau de maîtrise&nbsp;:</label></span>
      <span class="wwctrl">
        <select id="arboAdventiceQualifier" ng-options="key as value for (key, value) in (isDephyExpe ? i18n.GlobalMasterLevelQualifier : i18n['GlobalMasterLevelQualifier#ArboNotExpe'])"
            ng-model="editedPestMaster.qualifier" class="column-xlarge">
          <option value=""></option>
        </select>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
      <span class="wwlbl wide-label"><label for="arboAdventiceResultFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
      <span class="wwctrl">
        <textarea id="arboAdventiceResultFarmerComment" ng-model="editedPestMaster.resultFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
      </span>
    </div>
    <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="arboAdventiceAdviserComments">Commentaires du conseiller&nbsp;:</label></span>
        <span class="wwctrl">
          <textarea id="arboAdventiceAdviserComments" ng-model="editedPestMaster.adviserComments" class="column-xlarge" column="20" rows="5"></textarea>
        </span>
    </div>
  </jqdialog>
</div>
</div>
</div>
