<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
  <title>Bilans de campagne</title>
  <content tag="current-category">reports</content>
  <script type="text/javascript" src="<s:url value='/nuiton-js/report-regional-list.js' /><s:property value='getVersionSuffix()'/>"></script>
  <script type="text/javascript">
    angular.module('ReportRegionalListModule', ['Agrosyst', 'ngSanitize'])
        .value('ReportRegionalListInitData', {reportRegionals: <s:property value="toJson(reportRegionals)" escapeHtml="false"/>, filter: <s:property value="toJson(filter)" escapeHtml="false"/>})
        .value('relatedReportGrowingSystemNames',  <s:property value="toJson(lightBoxNotifications)" escapeHtml="false"/>)
        .value('reportRegionalsExportAsyncThreshold',  <s:property value="reportRegionalsExportAsyncThreshold"/>);
  </script>
</head>
<body>
  <div ng-app="ReportRegionalListModule" ng-controller="ReportRegionalListController">
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Bilans de campagne</li>
      </ul>
    </div>

    <ul class="actions">
      <li>
        <a class="action-ajouter"
           href="<s:url namespace='/reports' action='report-regional-edit-input' />">Créer un bilan de campagne / échelle régionale</a>
      </li>
    </ul>

    <ul class="float-right actions">
      <li>
        <a class="action-desactiver"
           ng-class="{'button-disabled':(selectedEntities|toSelectedLength) === 0}"
           ng-click="reportRegionalDelete('#reportsListForm')">Supprimer</a>
      <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls"
           ng-class="{'button-disabled':(selectedEntities|toSelectedLength) === 0}"
           ng-click="reportRegionalExportXls('#reportsListForm')">Export XLS</a></li>
      <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
           ng-click="asyncReportRegionalExportXls()">Export XLS</a></li>
    </ul>

    <form method="post" id="reportsListForm">
      <input type="hidden" name="reportRegionalIds" value="{{selectedEntities|toSelectedArray}}" />

      <table class="entity-list clear fixe_layout">
        <thead>
          <tr>
            <th scope="col" class="column-tiny"
                title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
            </th>
            <th scope="col" class="column-xxxlarge-fixed" ng-click="changeSort('REPORT')">
              <sort>
                <header_label>Bilan de campagne / échelle Système de culture</header_label>
                <em ng-if="filter.sortedColumn == 'REPORT'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.REPORT, 'fa fa-sort-amount-desc ':sortColumn.REPORT}" ></em>
                <em ng-if="filter.sortedColumn != 'REPORT'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-small" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne">
              <sort>
                <header_label>Camp.</header_label>
                <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col">Réseau</th>
            <th scope="col" class="column-large">Filière</th>
          </tr>
          <tr>
            <td></td>
            <td class="column-xxlarge-fixed"><input type="text" ng-model="filter.name" ng-model-options="configuration.debounce" /></td>
            <td class="column-small"><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" ng-model-options="configuration.debounce" /></td>
            <td><input type="text" ng-model="filter.network" ng-model-options="configuration.debounce" /></td>
            <td>
              <select ng-model="filter.sector">
                <option value="" />
                <s:iterator value="sectors">
                  <option value="<s:property value='key' />"><s:property value="value" /></option>
                </s:iterator>
              </select>
            </td>
          </tr>
        </thead>
        <tbody>
          <tr ng-show="reportRegionals.length == 0">
            <td colspan="5" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td>
          </tr>
          <tr ng-repeat="reportRegional in reportRegionals"
              ng-class="{'line-selected':selectedEntities[reportRegional.topiaId]}">
            <td>
              <input type='checkbox' ng-model="selectedEntities[reportRegional.topiaId]" ng-checked="selectedEntities[reportRegional.topiaId]" ng-click="toggleSelectedEntity(reportRegional.topiaId)"/>
            </td>
            <td class="column-xxxlarge-fixed" title="{{reportRegional.name}}">
              <a href="<s:url namespace='/reports' action='report-regional-edit-input'/>?reportRegionalTopiaId={{reportRegional.topiaId}}">{{reportRegional.name}}</a>
            </td>
            <td class="column-small">
              {{reportRegional.campaign}}
            </td>
            <td>
              <span ng-repeat="network in reportRegional.networks">
                <a href="<s:url namespace='/networks' action='networks-edit-input'/>?networkTopiaId={{network.topiaId}}" ng-if="$index < 3">{{network.name}}<span ng-if="!network.active" class="unactivated">&nbsp;(inactif)</span></a><span ng-if="$index < 2 && !$last">,</span>
                <span ng-if="$index == 3" title="{{reportRegional.networks.length - 3}} réseaux de plus">&hellip;</span>
              </span>
            </td>
            <td>
              <span ng-repeat="sector in reportRegional.sectors">
                {{sector|translate: 'Sector'}}<span ng-if="!$last">, </span>
              </span>
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="5">
              <div class="table-footer">
                <div class="entity-list-info">
                  <span class="counter">{{pager.count}} bilans de campagne / échelle régional</span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les bilans de campagne</a>
                  </span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                  </span>
                  <span class="selection">
                    - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                  </span>
                </div>
                <div class="pagination">
                  <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                </div>
              </div>
            </td>
          </tr>
        </tfoot>
      </table>
    </form>

    <jqdialog dialog-name="confirmDeleteReportRegionals"
         auto-open="false" width="'400'" class="dialog-form" modal="true"
         buttons="{'Supprimer': reportRegionalDeleteClose, 'Annuler': reportRegionalDeleteCancel}"
         button-classes="{'Supprimer': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
         title="'Suppression de bilans de campagne / échelle régionale'">
      Êtes-vous sûr(e) de vouloir supprimer
      <ng-pluralize count="(selectedEntities|toSelectedLength)"
           when="{'one': 'le bilan de campagne / échelle régionale {{firstSelectedReport.name}}',
                   'other': 'les {} bilans de campagne / échelle régionale sélectionnés'}"></ng-pluralize> ?
    </jqdialog>


    <jqdialog dialog-name="failedDeleteReportGrowingSystems" auto-open="false" width="'800'" class="dialog-form" modal="true"
         buttons="{'Ok': failedDeleteReportGrowingSystemsClose}"
         button-classes="{'Ok': 'btn-primary'}"
         title="'Échec de suppression des bilans de campagne'">
      <div ng-bind-html = "deleteReportMessage"></div>
    </jqdialog>

  </div>
</body>
</html>
