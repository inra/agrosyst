<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <s:if test="reportGrowingSystem.topiaId == null">
      <title>Nouveau bilan de campagne / échelle Système de culture</title>
    </s:if>
    <s:else>
      <title>Bilan de campagne / échelle Système de culture '<s:property value="reportGrowingSystem.name" />'</title>
    </s:else>
    <content tag="current-category">reports</content>
    <script type="text/javascript" src="<s:url value='/nuiton-js/report-growing-system.js' /><s:property value='getVersionSuffix()'/>"></script>
    <script type="text/javascript">
      angular.module('ReportGrowingSystemModule', ['Agrosyst', 'ngSanitize', 'ui.select'])
        .value('ReportGrowingSystemInitData', {
        reportGrowingSystem: <s:property value="toJson(reportGrowingSystem)" escapeHtml="false" />,
        campaignsBounds: <s:property value="toJson(campaignsBounds)" escapeHtml="false" />,
        growingSystemId: <s:property value="toJson(growingSystemId)" escapeHtml="false" />,
        reportGrowingSystemId: <s:property value="toJson(reportGrowingSystemId)" escapeHtml="false" />,
        reportFilter: <s:property value="toJson(reportFilter)" escapeHtml="false" />,
        reportRegionalId: <s:property value="toJson(reportRegionalId)" escapeHtml="false" />,
        reportGrowingSystemId: <s:property value="toJson(reportGrowingSystemId)" escapeHtml="false" />,
        reportRegionalList: <s:property value="toJson(reportRegionalList)" escapeHtml="false" />,
        observeManagementModeId: <s:property value="toJson(observeManagementModeId)" escapeHtml="false"/>,
        highlights: <s:property value="toJson(highlights)" escapeHtml="false"/>,
        campaignsBounds: <s:property value="toJson(campaignsBounds)" escapeHtml="false" />,
        dephyType: '<s:property value="reportGrowingSystem.growingSystem.growingPlan.type" escapeHtml="false"/>',
        groupesCibles: <s:property value="toJson(groupesCibles)" escapeHtml="false" />,
        treatmentTargetCategoriesByParent: <s:property value="toJson(treatmentTargetCategoriesByParent)" escapeHtml="false" />,
        defaultArboAdventiceId: '<s:property value="defaultArboAdventiceId" escapeHtml="false" />',
        allIftEstimationMethods: <s:property value="toJson(allIftEstimationMethods)" escapeHtml="false" />,
        iftEstimationMethods: <s:property value="toJson(iftEstimationMethods)" escapeHtml="false" />,
        yieldObjectives: <s:property value="toJson(yieldObjectives)" escapeHtml="false" />,
        vitiYieldLossCauses: <s:property value="toJson(vitiYieldLossCauses)" escapeHtml="false" />
        });
    </script>
    <link rel="stylesheet" href="<s:url value='/webjars/select2/3.5.4/select2.css' />" />
  </head>
  <body>
    <div ng-app="ReportGrowingSystemModule" ng-controller="ReportGrowingSystemController">
      <div id="filAriane">
        <ul class="clearfix">
          <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
          <li>&gt; <a href="<s:url action='report-growing-systems-list' namespace='/reports' />">Bilans de campagne</a></li>
          <s:if test="reportGrowingSystem.topiaId == null">
            <li>&gt; Nouveau bilan de campagne / échelle Système de culture</li>
          </s:if>
          <s:else>
            <li>&gt; <s:property value="reportGrowingSystem.name" /></li>
          </s:else>
        </ul>
      </div>

      <ul class="actions">
        <li><a class="action-retour" href="<s:url action='report-growing-systems-list' namespace='/reports' />">Retour à la liste des bilans de campagne</a></li>
      </ul>

      <ul class="float-right informations">
        <s:if test="%{reportGrowingSystem.name != null}">
          <li>
            <span class="label">Bilan de campagne / échelle Système de culture</span>
            <s:property value="reportGrowingSystem.name" />
          </li>
        </s:if>

        <s:if test="%{reportGrowingSystem.growingSystem != null}">
          <li><span class="label">Campagne</span>
            <s:property value="reportGrowingSystem.growingSystem.growingPlan.domain.campaign" />
          </li>
          <li>
            <span class="label">Système de culture</span>
            <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId=<s:property value='reportGrowingSystem.growingSystem.topiaId'/>" title="Voir le système de culture"><s:property value="reportGrowingSystem.growingSystem.name" /></a>
          </li>
          <li>
            <span class="label">Exploitation ou domaine expérimental</span>
            <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId=<s:property value='reportGrowingSystem.growingSystem.growingPlan.domain.topiaId'/>" title="Voir le domaine"><s:property value="reportGrowingSystem.growingSystem.growingPlan.domain.name" /></a>
          </li>
          <li>
            <span class="label">Dispositif</span>
            <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId=<s:property value='reportGrowingSystem.growingSystem.growingPlan.topiaId'/>" title="Voir le dispositif"><s:property value="reportGrowingSystem.growingSystem.growingPlan.name" /></a>
          </li>
        </s:if>
      </ul>

      <div class="page-content">

        <s:if test="relatedReportGrowingSystems != null">
          <ul class="timeline">
            <s:iterator value="relatedReportGrowingSystems" var="relatedReportGrowingSystem">
                <li<s:if test="#relatedReportGrowingSystem.value.equals(reportGrowingSystem.topiaId)"> class="selected"</s:if>>
                  <a href="<s:url namespace='/reports' action='report-growing-system-edit-input' />?reportGrowingSystemId=<s:property value='value'/>"><s:property value="key" /></a>
                </li>
            </s:iterator>
          </ul>
        </s:if>

        <form name="reportGrowingSystemEditForm" action="<s:url action='report-growing-system-edit' namespace='/reports' />" method="post" class="tabs clear" ag-confirm-on-exit>
          <s:actionerror cssClass="send-toast-to-js"/>

          <input type="hidden" name="extendFromReportGrowingSystem" value="${extendFromReportGrowingSystem}" />
          <input type="hidden" name="reportSectors" value="{{reportGrowingSystem.sectors}}"/>

          <ul id="tabs-report-menu" class="tabs-menu clearfix">
            <li class="selected"><span>Généralités</span></li><!--
            --><li><span>Faits marquants</span></li>
          </ul>
          <div id="tabs-report-content" class="tabs-content">
            <!-- Généralités -->
            <div id="tab_0">
              <fieldset>
                <input type="hidden" name="growingSystemId" value="{{growingSystemId}}"/>
                <input type="hidden" name="reportGrowingSystemSectors" value="{{reportGrowingSystemSectors}}"/>
                <input type="hidden" name="reportFilter" value="{{reportFilter}}"/>
                <input type="hidden" name="reportRegionalId" value="{{reportRegionalId}}"/>
                <input type="hidden" name="reportGrowingSystemId" value="{{reportGrowingSystemId}}" />

                <s:if test="%{reportGrowingSystem.growingSystem == null}">

                  <s:fielderror fieldName="campaign" />
                  <div class="wwgrp">
                    <span class="wwlbl">
                      <label for="selectCampaign" class="label"><span class="required">*</span>&nbsp;Campagne&nbsp;:</label>
                    </span>
                    <span class="wwctrl">
                      <input id="selectCampaign" type="text" ng-model="reportFilter.campaign"
                             ng-model-options="configuration.debounce"
                             ng-change="loadCreationContextFromCampaign()"
                             placeholder="ex. : 2018"
                             ag-campaign pattern="(?:19|20)[0-9]{2}"
                             required/>
                    </span>
                  </div>

                  <s:fielderror fieldName="sectorsError" />
                  <div class="wwgrp checkbox-list">
                    <span class="wwlbl">
                      <label class="label" for="sectorsList"><span class="required">*</span>&nbsp;Filières&nbsp;:</label>
                    </span>
                    <span id="sectorsList" class="wwctrl">
                      <input type="checkbox" ng-repeat-start="(key, value) in i18n.Sector"
                         ng-click="toggleSelectedSector(key)"
                         value="{{key}}"
                         id="reportRegional-sectors-{{$index}}"
                         ng-model="selectedSectors[key]"
                         ng-checked="selectedSectors[key]"
                         ng-required="reportGrowingSystem.sectors.length === 0"/>
                        <label for="reportRegional-sectors-{{$index}}" class="checkboxLabelAgrosyst" ng-repeat-end>{{value}}</label>
                    </span>
                  </div>

                  <div class="wwgrp">
                    <span class="wwlbl">
                      <label for="selectDomain" class="label">Domaine&nbsp;:</label>
                    </span>
                    <span class="wwctrl">
                      <select id="selectDomain"
                              ng-model="reportFilter.domainId"
                              ng-options="domainId as domainName for (domainId, domainName) in domains"
                              ng-change="loadCreationContextFromDomain()">
                        <option value=""></option>
                      </select>
                    </span>
                  </div>

                  <s:fielderror fieldName="selectGrowingSystemError" />
                  <div class="wwgrp">
                    <span class="wwlbl">
                      <label for="selectGrowingSystem" class="label"><span class="required">*</span>&nbsp;Système de culture&nbsp;:</label>
                    </span>
                    <span class="wwctrl">
                      <select id="selectGrowingSystem"
                              ng-model="reportFilter.growingSystemId"
                              ng-change="loadCreationContextFromSDC()"
                              required>
                        <!-- the following line is required to enable HTML5 validation-->
                        <option value=""></option>
                        <option ng-repeat="growingSystem in growingSystems"
                                value="{{growingSystem.topiaId}}">
                          {{growingSystem.label}}
                        </option>
                      </select>
                    </span>
                  </div>

                  <s:fielderror fieldName="authorError" />
                    <s:textfield name="reportGrowingSystem.author"
                      label="Rédacteur"
                      labelPosition="left"
                      labelSeparator=" :"
                      placeholder="ex. : Gérard Manvussa"
                      required="true"
                      requiredLabel="true" />

                </s:if>

                <s:if test="%{reportGrowingSystem.topiaId != null}">

                  <div class="wwgrp">
                    <span class="wwlbl">
                      <label class="label"><span class="required">*</span>&nbsp;Campagne&nbsp;:</label>
                    </span>
                    <span class="wwctrl">
                      <s:property value="reportGrowingSystem.growingSystem.growingPlan.domain.campaign" />
                    </span>
                  </div>

                  <s:fielderror fieldName="sectorsError" />
                  <div class="wwgrp checkbox-list">
                    <span class="wwlbl">
                      <label for="sectorsList" class="label" for="sectorsList"><span class="required">*</span>&nbsp;Filière&nbsp;:</label>
                    </span>
                    <span id="sectorsList" class="wwctrl">
                      <input type="checkbox" ng-repeat-start="(key, value) in i18n.Sector"
                         ng-click="toggleSelectedSector(key)"
                         value="{{key}}"
                         id="reportRegional-sectors-{{$index}}"
                         ng-model="selectedSectors[key]"
                         ng-checked="selectedSectors[key]"
                         ng-required="reportGrowingSystem.sectors.length === 0"/>
                      <label for="reportRegional-sectors-{{$index}}" class="checkboxLabelAgrosyst" ng-repeat-end>{{value}}</label>
                    </span>
                  </div>
                  <div class="wwgrp">
                    <span class="wwlbl">
                      <label class="label"><span class="required">*</span>&nbsp;Système de culture&nbsp;:</label>
                    </span>
                    <span class="wwctrl">
                      <s:property value="reportGrowingSystem.growingSystem.name" /> ({{tDephyType}})
                    </span>
                  </div>

                  <s:fielderror fieldName="authorError" />
                    <s:textfield name="reportGrowingSystem.author"
                      label="Rédacteur"
                      labelPosition="left"
                      labelSeparator=" :"
                      placeholder="ex. : Gérard Manvussa"
                      required="true"
                      requiredLabel="true" />
                </s:if>
                <s:fielderror fieldName="selectReportRegionalError" />

                <div class="fade-animation" ng-class="{'ng-hide': !growingSystemId}">
                  <div class="wwgrp">
                    <span class="wwlbl">
                      <label class="label"><span ng-if="!isDephyExpe"><span class="required">*</span>&nbsp;</span>Bilan de campagne / echelle régionale&nbsp;:</label>
                    </span>
                    <span class="wwctrl">
                      <select id="reportRegionals"
                              ng-model="reportRegionalId"
                              ng-options="reportRegionalId as reportRegionalName for (reportRegionalId, reportRegionalName) in reportRegionals"
                              ng-required="!isDephyExpe">
                              <!-- the following line is required to enable HTML5 validation-->
                              <option value=""></option>
                      </select>
                    </span>
                  </div>
                </div>

              </fieldset>
            </div>

            <%@include file="report-growing-system-striking-facts.jsp" %>

            <span class="form-buttons">
              <a class="btn-secondary" href="<s:url action='report-growing-systems-list' namespace='/reports' />">Annuler</a>
              <input type="submit" class="btn-primary" value="Enregistrer" <s:if test="readOnly">disabled="disabled" title="Vous n'avez pas les droits nécessaires"</s:if>/>
            </span>
          </div>
        </form>

        <div id="confirmStrikingFactImpact" title="Attention, cette action entraine la réinitialisation de certaines données de l'onglet 'Faits marquants', voulez-vous continuer ?" class="auto-hide"></div>

      </div>
    </div>
  </body>
</html>
