<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="VitiPestMastersController" ng-init="init(true)">
  <input type="hidden" name="vitiDiseaseMastersJson" value="{{vitiPestMasters}}"/>

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadVitiPestMasters()" href=""><span class="required">*</span>&nbsp;Maîtrise des maladies</a>
          <div ng-if="!isDephyExpe && ((viewPortArray && viewPortArray.length === 0) || (vitiPestMasters && vitiPestMasters.length === 0))">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>

  <div ng-show="showVitiDiseasePestMasters" class="loadContentEnclosure">
      <div class="grid-enclosure" ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;">
        <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
        <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
        <div ng-attr-style="grid-column: 1;grid-row: 2;" class="gridColumnMainHeader">Pression sur le système de culture&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnHeader oddCell">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de pression</div>
        <div ng-attr-style="grid-column: 1;grid-row: 4;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Evolution par rapport à l’année précédente</div>
        <div ng-attr-style="grid-column: 1;grid-row: 5;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader oddCell">- Expression de l’agriculteur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 6;" class="gridColumnMainHeader">Résultats obtenus, niveau de maîtrise finale&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 7;" class="gridColumnHeader oddCell">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise</div>
        <div ng-attr-style="grid-column: 1;grid-row: 8;" ng-if="isDephyExpe" class="gridColumnHeader">- Note de fréquence d’attaque sur feuilles</div>
        <div ng-attr-style="grid-column: 1;grid-row: 9;" ng-if="isDephyExpe" class="gridColumnHeader">- Note d’intensité d’attaque sur feuilles</div>
        <div ng-attr-style="grid-column: 1;grid-row: 10;" ng-if="!isDephyExpe" class="gridColumnHeader">- Note globale d’attaque sur feuilles</div>
        <div ng-attr-style="grid-column: 1;grid-row: 11;" ng-if="isDephyExpe" class="gridColumnHeader oddCell">- Note de fréquence d’attaque sur grappes</div>
        <div ng-attr-style="grid-column: 1;grid-row: 12;" ng-if="isDephyExpe" class="gridColumnHeader oddCell">- Note d’intensité d’attaque sur grappes</div>
        <div ng-attr-style="grid-column: 1;grid-row: 13;" ng-if="!isDephyExpe" class="gridColumnHeader oddCell">- Note globale d’attaque sur grappes</div>
        <div ng-attr-style="grid-column: 1;grid-row: 14;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- <span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Qualification du niveau de maîtrise</div>
        <div ng-attr-style="grid-column: 1;grid-row: 15;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader oddCell">- Expression de l’agriculteur</div>
        <div ng-attr-style="grid-column: 1;grid-row: 16;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridColumnHeader">- Commentaires du conseiller</div>
        <div ng-attr-style="grid-column: 1;grid-row: 17;" class="gridColumnMainHeader oddCell">IFT&nbsp;:</div>
        <div ng-attr-style="grid-column: 1;grid-row: 18;" class="gridColumnHeader">- <span class="required">*</span>&nbsp;Nombre de traitements</div>
        <div ng-attr-style="grid-column: 1;grid-row: 19;" class="gridColumnHeader oddCell">- <span class="required">*</span>&nbsp;IFT-Fongicide (chimique)</div>
        <div ng-attr-style="grid-column: 1;grid-row: 20;" class="gridColumnHeader">- <span class="required">*</span>&nbsp;IFT-Fongicide biocontrôle</div>
        <div ng-attr-style="grid-column: 1;grid-row: 21;" class="gridColumnHeader"></div>

        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 1;" class="gridSubHeader" ng-repeat-start="vitiPestMaster in viewPortArray">{{getAgressorLabel(vitiPestMaster)}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.pressureScale|translate:'PressureScale'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.pressureEvolution|translate:'PressureEvolution'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell oddCell" ng-class="{gridCellLast: $last}" ag-read-more="{{vitiPestMaster.pressureFarmerComment}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.masterScale|translate:'MasterScale#VitiDisease'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" ng-if="isDephyExpe" class="gridCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.leafDiseaseAttackRatePreciseValue}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" ng-if="isDephyExpe" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.leafDiseaseAttackIntensityPreciseValue}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 10;" ng-if="!isDephyExpe" class="gridCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.leafDiseaseAttackRate|translate:'DiseaseAttackRate'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 11;" ng-if="isDephyExpe" class="gridCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.grapeDiseaseAttackRatePreciseValue}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 12;" ng-if="isDephyExpe" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.grapeDiseaseAttackIntensityPreciseValue}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 13;" ng-if="!isDephyExpe" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.grapeDiseaseAttackRate|translate:'DiseaseAttackRate'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 14;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}">{{vitiPestMaster.qualifier|translate:'PestMasterLevelQualifier'}}</div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 15;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell oddCell" ng-class="{gridCellLast: $last}" ag-read-more="{{vitiPestMaster.resultFarmerComment}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 16;" ng-if="!isDephyExpeAndNotDisplayForced()" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{vitiPestMaster.adviserComments}}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 17;" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}"></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 18;" id="VitiDiseasePestMasters_treatmentCount_{{$index}}" class="gridCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{vitiPestMaster.treatmentCount}}<input type="text" ng-model="vitiPestMaster.treatmentCount" ng-required="vitiPestMaster" style="opacity:0;width: 0;"/></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 19;" id="VitiDiseasePestMasters_chemicalFungicideIFT_{{$index}}" class="gridCell" ng-class="{oddCell: !isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{vitiPestMaster.chemicalFungicideIFT}}<input type="text" ng-model="vitiPestMaster.chemicalFungicideIFT" ng-required="vitiPestMaster" style="opacity:0;width: 0;"/></div>
        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 20;" id="VitiDiseasePestMasters_bioControlFungicideIFT_{{$index}}" class="gridCell" ng-class="{oddCell: isDephyExpeAndNotDisplayForced(), gridCellLast: $last}">{{vitiPestMaster.bioControlFungicideIFT}}<input type="text" ng-model="vitiPestMaster.chemicalFungicideIFT" ng-required="vitiPestMaster" style="opacity:0;width: 0;"/></div>

        <div ng-attr-style="grid-column: {{$index+2}};grid-row: 21;" class="gridActions" ng-class="{gridCellLast: $last}" ng-repeat-end>
          <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editVitiPest(vitiPestMaster)" />
          <input type="button" class="btn-icon icon-delete" ng-class="{'icon-delete-disabled':(vitiPestMaster.agressor && requiredDiseases.indexOf(vitiPestMaster.agressor.reference_id) !== -1)}" value="Supprimer" title="Supprimer" ng-click="deleteVitiPest(vitiPestMaster)" ng-disabled="vitiPestMaster.agressor && requiredDiseases.indexOf(vitiPestMaster.agressor.reference_id) !== -1" />
        </div>
        <div ng-attr-style="grid-column: 2;grid-row: 1 / span 21;" class="gridEmpty" ng-if="vitiPestMasters.length == 0">
          Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
        </div>
        <div ng-attr-style="grid-column: 1 / span 5;grid-row: 22;" class="gridFooter">
          <input type="button" value="Ajouter une maladie" ng-click="addVitiPest()"/>
        </div>
      </div>

      <div class="wwgrp">
        <s:fielderror fieldName="reportGrowingSystem.vitiDiseaseChemicalFungicideIFT" />
        <span class="wwlbl">
           <label class="label"><span class="required">*</span>&nbsp;IFT-fongicide du système de culture (chimique)&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <input type="text"
                 id="VitiDiseasePestMasters_vitiDiseaseChemicalFungicideIFT"
                 class="column-xlarge"
                 ng-required="viewPortArray.length > 0"
                 pattern="^\d+(?:[\.,]\d+)?$"
                 name="reportGrowingSystem.vitiDiseaseChemicalFungicideIFT"
                 value="${reportGrowingSystem.vitiDiseaseChemicalFungicideIFT}"
                 placeholder="ex: 1.5"/>
        </span>
      </div>
      <div class="wwgrp">
        <s:fielderror fieldName="reportGrowingSystem.vitiDiseaseBioControlFungicideIFT" />
        <span class="wwlbl">
           <label class="label"><span class="required">*</span>&nbsp;IFT-fongicide biocontrôle du système de culture&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <input type="text"
                 id="VitiDiseasePestMasters_vitiDiseaseBioControlFungicideIFT"
                 class="column-xlarge"
                 ng-required="viewPortArray.length > 0"
                 pattern="^\d+(?:[\.,]\d+)?$"
                 name="reportGrowingSystem.vitiDiseaseBioControlFungicideIFT"
                 value="${reportGrowingSystem.vitiDiseaseBioControlFungicideIFT}"
                 placeholder="ex: 1.5"/>
        </span>
      </div>
      <div class="wwgrp">
        <s:fielderror fieldName="reportGrowingSystem.vitiDiseaseCopperQuantity" />
        <span class="wwlbl">
           <label class="label"><span class="required">*</span>&nbsp;Quantité de cuivre appliquée (Kg Cu/ha)&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <input type="text"
                 id="VitiDiseasePestMasters_vitiDiseaseCopperQuantity"
                 class="column-xlarge"
                 ng-required="viewPortArray.length > 0"
                 pattern="^\d+(?:[\.,]\d+)?$"
                 name="reportGrowingSystem.vitiDiseaseCopperQuantity"
                 value="${reportGrowingSystem.vitiDiseaseCopperQuantity}"
                 ng-model="reportGrowingSystem.vitiDiseaseCopperQuantity"
                 placeholder="ex: 1.5"/>
        </span>
      </div>
      <s:select label="Niveau global de maîtrise des maladies"
                id="VitiDiseasePestMasters_vitiDiseaseQualifier"
                requiredLabel="true" ng-required="viewPortArray.length > 0" emptyOption="true"
                name="reportGrowingSystem.vitiDiseaseQualifier" list="globalMasterLevelQualifiers"
                labelPosition="left" labelSeparator=" :" cssClass="column-xlarge"/>
      <div class="wwgrp" ng-if="!isDephyExpeAndNotDisplayForced()">
          <span class="wwlbl"><label>Modèle décisionnel&nbsp;:</label></span>
          <span class="wwctrl">
            <div>
              <a class="asLabelUnderlined" ng-if="observeManagementModeId" target="_blank" rel="noopener noreferrer"
                 href="<s:url namespace='/managementmodes' action='management-modes-edit-input'/>?managementModeTopiaId={{observeManagementModeId}}#sectionTypes_MALADIES">
                 Ouvrir le modèle décisionnel
              </a>
            </div>
            <div ng-if="!observeManagementModeId">Aucun</div>
          </span>
      </div>

      <div ng-if="reportGrowingSystem.vitiDiseaseCopperQuantity > 8" class="infoLine block" style="margin-top: 20px;background: orange;">
        <div class="endLineEvidence">
          <div style="cursor:pointer"
            class="textIcones blue x2"
            title="Information importante">!</div>
        </div>
        <div>Attention, la quantité de cuivre saisie paraît trop importante, merci de vérifier cette valeur.</strong></div>
      </div>
  </div>

  <jqdialog dialog-name="addVitiDisease" auto-open="false" width="'55%'" class="dialog-form" modal="true"
     buttons="{'OK': onDialogOk, 'Annuler': onDialogCancel}"
     button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
     title="'Maîtrise des maladies'">
    <div class="asTable">

      <div class="horizontal-separator">Pression avant intervention</div>

      <div class="oneFieldOutOfTwoRequired">
        <span class="required">*</span>&nbsp;<em>au moins un des deux</em>
      </div>

      <div class="wwgrp">
        <span class="wwlbl wide-label">
          <label for="codeGroupeCibleMaa" class="label">Groupe cible&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <span class='contextual-help'>
            <span class='help-hover'>
              <s:text name="help.report.groupeCibleMaa" />
            </span>
          </span>
          <ui-select ng-model="editedPest.codeGroupeCibleMaa"
                     id="vitiDiseaseGroupeCibleField"
                     reset-search-input="true"
                     theme="select2"
                     style="width: 300px;"
                     uis-open-close="onOpenVitiDiseaseGroupeCible(isOpen)"
                     ng-disabled="editedPest.changeableDisease">
            <ui-select-match placeholder="ex. : Bactéries">{{$select.selected.groupeCibleMaa}}</ui-select-match>
            <ui-select-choices repeat="groupeCible.codeGroupeCibleMaa as groupeCible in groupesCibles | filter : isGroupeCibleInCategory('MALADIE') | filter: {groupeCibleMaa: $select.search}">
              <span ng-bind-html="groupeCible.groupeCibleMaa | highlight: $select.search"></span>
            </ui-select-choices>
          </ui-select>
        </span>
      </div>
      <div class="wwgrp">
        <span class="wwlbl wide-label">
          <label for="vitiDiseaseAgressor" class="label">Maladie&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <span ng-if="!bioAgressors || bioAgressors.length === 0">Chargement en cours...</span>
          <ui-select ng-model="editedPest.agressor"
                     id="vitiDiseaseAgressor"
                     reset-search-input="true"
                     theme="select2"
                     style="width: 300px;"
                     uis-open-close="onOpenVitiDiseaseAgressor(isOpen)"
                     ng-if="bioAgressors && bioAgressors.length > 0"
                     ng-disabled="editedPest.changeableDisease"
                     ng-required="editedPest">
            <ui-select-match placeholder="ex. : Black rot">{{$select.selected.adventice || $select.selected.reference_label}}</ui-select-match>
            <ui-select-choices repeat="bioAggressor in bioAgressors | filter: inGroupeCibleBioAgressor(editedPest) | filter: $select.search">
              <span ng-bind-html="bioAggressor.adventice || bioAggressor.reference_label | highlight: $select.search"></span>
            </ui-select-choices>
          </ui-select>
        </span>
      </div>
      <div class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiDiseasePressureScale"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de pression&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="vitiDiseasePressureScale" ng-options="key as value for (key, value) in i18n.PressureScale"
              ng-model="editedPest.pressureScale" class="column-xlarge">
            <option value=""></option>
          </select>
        </span>
      </div>
      <div class="wwgrp" ng-if="!isDephyExpeAndNotDisplayForced()">
        <span class="wwlbl wide-label"><label for="vitiDiseasePressureEvolution">Evolution par rapport à l’année précédente&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="vitiDiseasePressureEvolution" ng-options="key as value for (key, value) in i18n.PressureEvolution"
              ng-model="editedPest.pressureEvolution" class="column-xlarge">
            <option value=""></option>
          </select>
        </span>
      </div>
      <div class="wwgrp" ng-if="!isDephyExpeAndNotDisplayForced()">
          <span class="wwlbl wide-label"><label for="vitiDiseasePressureFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
          <span class="wwctrl">
            <textarea id="vitiDiseasePressureFarmerComment" ng-model="editedPest.pressureFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
          </span>
      </div>

      <div class="horizontal-separator">Résultats obtenus, niveau de maîtrise finale</div>

      <div class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiDiseasePestMasterScale"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de maîtrise&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="vitiDiseasePestMasterScale" ng-options="key as value for (key, value) in i18n['MasterScale#VitiDisease']"
              ng-model="editedPest.masterScale" class="column-xlarge">
            <option value=""></option>
          </select>
        </span>
      </div>
      <div class="wwgrp" ng-if="!isDephyExpe">
        <span class="wwlbl wide-label"><label for="vitiDiseaseLeafAttackRate">Note globale d’attaque sur feuilles&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="vitiDiseaseLeafAttackRate" ng-options="key as value for (key, value) in i18n.DiseaseAttackRate"
              ng-model="editedPest.leafDiseaseAttackRate" class="column-xlarge">
            <option value=""></option>
          </select>
        </span>
      </div>
      <div class="wwgrp" ng-if="isDephyExpe">
        <span class="wwlbl wide-label"><label for="vitiDiseaseLeafAttackRatePreciseValue">Note de fréquence d’attaque sur feuilles&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text"
                 id="vitiDiseaseLeafAttackRatePreciseValue"
                 ng-model="editedPest.leafDiseaseAttackRatePreciseValue"
                 ag-float pattern="^\d+(?:[\.,]\d{1})?$"
                 class="column-xlarge"
                 placeholder="Note entre 0 et 100" />
        </span>
      </div>
      <div class="wwgrp" ng-if="isDephyExpe">
        <span class="wwlbl wide-label"><label for="vitiDiseaseLeafAttackRatePreciseValue">Note d’intensité d’attaque sur feuilles&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text"
                 id="vitiDiseaseLeafAttackRatePreciseValue"
                 ng-model="editedPest.leafDiseaseAttackIntensityPreciseValue"
                 ag-float pattern="^\d+(?:[\.,]\d{1})?$"
                 class="column-xlarge"
                 placeholder="Note entre 0 et 100" />
        </span>
      </div>
      <div class="wwgrp" ng-if="!isDephyExpe">
          <span class="wwlbl wide-label"><label for="vitiDiseaseGrapeAttackRate">Note globale d’attaque sur grappes&nbsp;:</label></span>
          <span class="wwctrl">
            <select id="vitiDiseaseGrapeAttackRate" ng-options="key as value for (key, value) in i18n.DiseaseAttackRate"
                ng-model="editedPest.grapeDiseaseAttackRate" class="column-xlarge">
              <option value=""></option>
            </select>
          </span>
      </div>
      <div class="wwgrp" ng-if="isDephyExpe">
        <span class="wwlbl wide-label"><label for="vitiDiseaseGrapeAttackRatePreciseValue">Note de fréquence d’attaque sur grappes&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text"
                 id="vitiDiseaseGrapeAttackRatePreciseValue"
                 ng-model="editedPest.grapeDiseaseAttackRatePreciseValue"
                 ag-float pattern="^\d+(?:[\.,]\d{1})?$"
                 class="column-xlarge"
                 placeholder="Note entre 0 et 100" />
        </span>
      </div>
      <div class="wwgrp" ng-if="isDephyExpe">
        <span class="wwlbl wide-label"><label for="vitiDiseaseGrapeAttackIntensityPreciseValue">Note d’intensité d’attaque sur grappes&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text"
                 id="vitiDiseaseGrapeAttackIntensityPreciseValue"
                 ng-model="editedPest.grapeDiseaseAttackIntensityPreciseValue"
                 ag-float pattern="^\d+(?:[\.,]\d{1})?$"
                 class="column-xlarge"
                 placeholder="Note entre 0 et 100" />
        </span>
      </div>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiDiseaseQualifier"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Qualification du niveau de maîtrise&nbsp;:</label></span>
        <span class="wwctrl">
          <select id="vitiDiseaseQualifier" ng-options="key as value for (key, value) in i18n.PestMasterLevelQualifier"
              ng-model="editedPest.qualifier" class="column-xlarge">
            <option value=""></option>
          </select>
        </span>
      </div>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiDiseaseResultFarmerComment">Expression de l’agriculteur&nbsp;:</label></span>
        <span class="wwctrl">
          <textarea id="vitiDiseaseResultFarmerComment" ng-model="editedPest.resultFarmerComment" class="column-xlarge" column="20" rows="5"></textarea>
        </span>
      </div>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
          <span class="wwlbl wide-label"><label for="vitiDiseaseAdviserComments">Commentaires du conseiller&nbsp;:</label></span>
          <span class="wwctrl">
            <textarea id="vitiDiseaseAdviserComments" ng-model="editedPest.adviserComments" class="column-xlarge" column="20" rows="5"></textarea>
          </span>
      </div>
      <div class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiDiseaseTreatmentCount"><span class="required">*</span>&nbsp;Nombre de traitements&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text" id="vitiDiseaseTreatmentCount" ng-model="editedPest.treatmentCount"
                 ng-required="editedPest" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$" class="column-xlarge" placeholder="ex: 1" />
        </span>
      </div>
      <div class="wwgrp">
          <span class="wwlbl wide-label"><label for="vitiDiseaseChemicalFungicideIFT"><span class="required">*</span>&nbsp;IFT-Fongicide (chimique)&nbsp;:</label></span>
          <span class="wwctrl">
            <input type="text" ng-required="editedPest" id="vitiDiseaseChemicalFungicideIFT" ng-model="editedPest.chemicalFungicideIFT" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$" class="column-xlarge" placeholder="ex: 1.5" />
          </span>
      </div>
      <div class="wwgrp">
        <span class="wwlbl wide-label"><label for="vitiDiseaseBioControlFungicideIFT"><span class="required">*</span>&nbsp;IFT-Fongicide biocontrôle&nbsp;:</label></span>
        <span class="wwctrl">
          <input type="text" ng-required="editedPest" id="vitiDiseaseBioControlFungicideIFT" ng-model="editedPest.bioControlFungicideIFT" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$" class="column-xlarge" placeholder="ex: 1.5" />
        </span>
      </div>
    </div>
  </jqdialog>
</div>
