<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div class="subRubrique" ng-if="reportGrowingSystem.sectors.indexOf('HORTICULTURE') != -1" ng-controller="HorticultureScopeController">
  <h2>Bilan des pratiques et État sanitaire des cultures (Horticulture)</h2>
  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-horticulture-adventice.jsp" %>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-horticulture-disease.jsp" %>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllPestMastersController">
      <%@include file="report-growing-system-horticulture-pest.jsp" %>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllMastersController">
      <%@include file="report-growing-system-horticulture-foods.jsp" %>
    </div>
  </div>

  <div ng-controller="CropPestMasterEditController">
    <div ng-controller="CropAllMastersController">
      <%@include file="report-growing-system-horticulture-yield.jsp" %>
    </div>
  </div>
</div>
