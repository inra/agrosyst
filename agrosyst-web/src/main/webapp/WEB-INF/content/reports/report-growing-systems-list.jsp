<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 - 2019 INRA, CodeLutin
  Copyright (C) 2020 - 2021 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<!DOCTYPE html>
<%@taglib uri="/struts-tags" prefix="s" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
  <title>Bilans de campagne</title>
  <content tag="current-category">reports</content>
  <script type="text/javascript" src="<s:url value='/nuiton-js/report-growing-system-list.js' /><s:property value='getVersionSuffix()'/>"></script>
  <script type="text/javascript">
    angular.module('ReportGrowingSystemListModule', ['Agrosyst'])
      .value('ReportGrowingSystemListInitData', <s:property value="toJson(reportGrowingSystems)" escapeHtml="false"/>)
      .value('reportGrowingSystemFilter', <s:property value="toJson(reportGrowingSystemFilter)" escapeHtml="false"/>)
      .value('reportGrowingSystemsExportAsyncThreshold',  <s:property value="reportGrowingSystemsExportAsyncThreshold"/>);
  </script>
</head>
<body>
  <div ng-app="ReportGrowingSystemListModule" ng-controller="ReportGrowingSystemListController">
    <div id="filAriane">
      <ul class="clearfix">
        <li><a href="<s:url action='index' namespace='/' />" class="icone-home">Accueil</a></li>
        <li>&gt; Bilans de campagne / échelle Système de culture</li>
      </ul>
    </div>

    <ul class="actions">
      <li>
        <a class="action-ajouter"
           href="<s:url namespace='/reports' action='report-growing-system-edit-input' />">Créer un bilan de campagne / échelle Système de culture</a>
      </li>
    </ul>

    <ul class="float-right actions">
      <li>
        <a class="action-desactiver"
           ng-class="{'button-disabled':(selectedEntities|toSelectedLength) === 0}"
           ng-click="reportGrowingSystemDelete('#reportsListForm')">Supprimer</a>
      <li ng-if="(selectedEntities|toSelectedLength) < asyncThreshold"><a class="action-export-xls"
           ng-class="{'button-disabled':(selectedEntities|toSelectedLength) === 0}"
           ng-click="reportGrowingSystemExportXls('#reportsListForm')">Export XLS</a></li>
      <li ng-if="(selectedEntities|toSelectedLength) >= asyncThreshold"><a class="action-export-xls"
           ng-click="asyncReportGrowingSystemExportXls()">Export XLS</a></li>
      <li><a class="action-export-xls"
           ng-class="{'button-disabled':(selectedEntities|toSelectedLength) === 0}"
           ng-click="reportGrowingSystemExportPdf('#reportsListForm')">Export PDF</a></li>
    </ul>
    <form method="post" id="reportsListForm">
      <input type="hidden" name="reportGrowingSystemIds" value="{{selectedEntities|toSelectedArray}}" />
      <input type="hidden" name="reportGrowingSystemSectionsJson" value="{{exportReportSections}}" />
      <input type="hidden" name="exportOptions.managementSections" value="{{section}}" ng-repeat="section in exportManagementSections" />
      <input type="hidden" name="exportOptions.includeReportRegional" value="{{includeReportRegional}}" ng-if="includeReportRegional" />
      <input type="hidden" name="extendCampaign" value="{{extendCampaign}}" ng-if="extendCampaign" />

      <table class="entity-list clear fixe_layout">
        <thead>
          <tr>
            <th scope="col" class="column-tiny"
                title="{{allSelectedEntities[pager.currentPage.pageNumber].selected && 'Tout désélectionner' || 'Tous sélectionner'}}">
                <input type='checkbox' ng-model="allSelectedEntities[pager.currentPage.pageNumber].selected" ng-change="toggleSelectedEntities()" />
            </th>
            <th scope="col" class="column-xxlarge-fixed" ng-click="changeSort('REPORT')">
              <sort>
                <header_label>Bilan de campagne / échelle Système de culture</header_label>
                <em ng-if="filter.sortedColumn == 'REPORT'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.REPORT, 'fa fa-sort-amount-desc ':sortColumn.REPORT}" ></em>
                <em ng-if="filter.sortedColumn != 'REPORT'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xxslarge-fixed" ng-click="changeSort('DOMAIN')">
              <sort>
                <header_label>Exploitation ou Domaine expérimental</header_label>
                <em ng-if="filter.sortedColumn == 'DOMAIN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DOMAIN, 'fa fa-sort-amount-desc ':sortColumn.DOMAIN}" ></em>
                <em ng-if="filter.sortedColumn != 'DOMAIN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_PLAN')">
              <sort>
                <header_label>Dispositif</header_label>
                <em ng-if="filter.sortedColumn == 'GROWING_PLAN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_PLAN, 'fa fa-sort-amount-desc ':sortColumn.GROWING_PLAN}" ></em>
                <em ng-if="filter.sortedColumn != 'GROWING_PLAN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xlarge-fixed" ng-click="changeSort('GROWING_SYSTEM')">
              <sort>
                <header_label>Système de culture</header_label>
                <em ng-if="filter.sortedColumn == 'GROWING_SYSTEM'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.GROWING_SYSTEM, 'fa fa-sort-amount-desc ':sortColumn.GROWING_SYSTEM}" ></em>
                <em ng-if="filter.sortedColumn != 'GROWING_SYSTEM'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-xsmall" id="header_campaign" ng-click="changeSort('CAMPAIGN')" title="Campagne">
              <sort>
                <header_label>Camp.</header_label>
                <em ng-if="filter.sortedColumn == 'CAMPAIGN'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.CAMPAIGN, 'fa fa-sort-amount-desc ':sortColumn.CAMPAIGN}" ></em>
                <em ng-if="filter.sortedColumn != 'CAMPAIGN'" class='fa fa-sort' ></em>
              </sort>
            </th>
            <th scope="col" class="column-small" ng-click="changeSort('DEPHY_NUMBER')">
              <sort>
                <header_label>Numéro DEPHY</header_label>
                <em ng-if="filter.sortedColumn == 'DEPHY_NUMBER'" ng-class="{'fa fa-sort-amount-asc':!sortColumn.DEPHY_NUMBER, 'fa fa-sort-amount-desc ':sortColumn.DEPHY_NUMBER}" ></em>
                <em ng-if="filter.sortedColumn != 'DEPHY_NUMBER'" class='fa fa-sort' ></em>
              </sort>
            </th>
          </tr>
          <tr>
            <td class="column-tiny"></td>
            <td class="column-xxlarge-fixed"><input type="text" ng-model="filter.name" ng-model-options="configuration.debounce" /></td>
            <td class="column-xxslarge-fixed"><input type="text" ng-model="filter.domainName" ng-model-options="configuration.debounce" /></td>
            <td class="column-xlarge-fixed"><input type="text" ng-model="filter.growingPlanName" ng-model-options="configuration.debounce" /></td>
            <td class="column-xlarge-fixed"><input type="text" ng-model="filter.growingSystemName" ng-model-options="configuration.debounce" /></td>
            <td><input type="text" ng-pattern="/^[0-9]{4}$/" ng-model="filter.campaign" ng-model-options="configuration.debounce" /></td>
            <td class="column-small"><input type="text" ng-model="filter.dephyNumber" ng-model-options="configuration.debounce" /></td>
          </tr>
        </thead>
        <tbody>
          <tr ng-show="reportGrowingSystems.length == 0"><td colspan="7" class="empty-table">Aucun résultat ne correspond à votre recherche. Vérifiez vos filtres ainsi que votre contexte de navigation</td></tr>
          <tr ng-repeat="reportGrowingSystem in reportGrowingSystems"
              ng-class="{'line-selected':selectedEntities[reportGrowingSystem.topiaId]}">
            <td class="column-tiny">
              <input type='checkbox' ng-model="selectedEntities[reportGrowingSystem.topiaId]" ng-checked="selectedEntities[reportGrowingSystem.topiaId]" ng-click="toggleSelectedEntity(reportGrowingSystem.topiaId)"/>
            </td>
            <td class="column-xxlarge-fixed" title="{{reportGrowingSystem.name}}"><!-- name -->
              <a href="<s:url namespace='/reports' action='report-growing-system-edit-input'/>?reportGrowingSystemId={{reportGrowingSystem.topiaId|encodeURIComponent}}">{{reportGrowingSystem.name}}</a>
            </td>
            <td class="column-xxslarge-fixed" title="{{reportGrowingSystem.growingSystem.growingPlan.domain.name}}"><!-- domain -->
              <a href="<s:url namespace='/domains' action='domains-edit-input'/>?domainTopiaId={{reportGrowingSystem.growingSystem.growingPlan.domain.topiaId|encodeURIComponent}}">{{reportGrowingSystem.growingSystem.growingPlan.domain.name}}<span ng-if="!reportGrowingSystem.growingSystem.growingPlan.domain.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xlarge-fixed" title="{{reportGrowingSystem.growingSystem.growingPlan.name}}"><!-- growingPlan -->
              <a href="<s:url namespace='/growingplans' action='growing-plans-edit-input'/>?growingPlanTopiaId={{reportGrowingSystem.growingSystem.growingPlan.topiaId|encodeURIComponent}}">{{reportGrowingSystem.growingSystem.growingPlan.name}}<span ng-if="!reportGrowingSystem.growingSystem.growingPlan.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xlarge-fixed" title="{{reportGrowingSystem.growingSystem.name}}"><!-- growingSystem -->
              <a href="<s:url namespace='/growingsystems' action='growing-systems-edit-input'/>?growingSystemTopiaId={{reportGrowingSystem.growingSystem.topiaId|encodeURIComponent}}">{{reportGrowingSystem.growingSystem.name}}<span ng-if="!reportGrowingSystem.growingSystem.active" class="unactivated">&nbsp;(inactif)</span></a>
            </td>
            <td class="column-xsmall center"><!-- campaign -->
              {{reportGrowingSystem.growingSystem.growingPlan.domain.campaign}}
            </td>
            <td class="column-small" title="{{reportGrowingSystem.growingSystem.dephyNumber| orDash}}"><!-- dephyNumber -->
              {{reportGrowingSystem.growingSystem.dephyNumber| orDash}}
            </td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="7">
              <div class="table-footer">
                <div class="entity-list-info">
                  <span class="counter">{{pager.count}} bilans de campagne</span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) < pager.count}" ng-click="selectAllEntities()" href=""> Sélectionner tous les bilans de campagne</a>
                  </span>
                  <span>
                    - <a ng-class="{'active':(selectedEntities|toSelectedLength) > 0}" ng-click="clearSelection()" href="">Tout désélectionner</a>
                  </span>
                  <span class="selection">
                    - <ng-pluralize count="(selectedEntities|toSelectedLength)" when="{'0': 'Aucun sélectionné', '1': '{} sélectionné', 'other': '{} sélectionnés'}" />
                  </span>
                </div>
                <div class="pagination">
                  <ag-pagination pager="pager" pagination-update="filter.page=page;filter.pageSize=pageSize" />
                </div>
              </div>
            </td>
          </tr>
        </tfoot>
      </table>
    </form>

    <jqdialog dialog-name="deleteReportGrowingSystems" auto-open="false" width="'400'" class="dialog-form" modal="true"
         buttons="{'Supprimer': deleteReportGrowingSystemsClose, 'Annuler': deleteReportGrowingSystemsCancel}"
         button-classes="{'Supprimer': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
         title="'Suppression de bilans de campagne / échelle système de culture'">
      Êtes-vous sûr(e) de vouloir supprimer
      <ng-pluralize count="(selectedEntities|toSelectedLength)"
           when="{'one': 'le bilan de campagne / échelle système de culture {{firstSelectedReport.name}}',
                   'other': 'les {} bilans de campagne / échelle système de culture sélectionnés'}"></ng-pluralize> ?
    </jqdialog>

    <jqdialog dialog-name="exportReportGrowingSystems" auto-open="false" width="'700'" class="dialog-form" modal="true"
         buttons="{'Exporter': exportReportGrowingSystemsClose, 'Annuler': exportReportGrowingSystemsCancel}"
         button-classes="{'Exporter': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
         title="'Export de bilans de campagne / échelle système de culture'">

       <form>
          <div style="margin-bottom: 10px;"><input type="checkbox" ng-model="includeReportRegional" />
            Données du bilan de campagne régional correspondant</div>

          <div class="materiel-list" style="width: 31%;" ng-repeat="(sector, sections) in availableReportGrowingSystemSections"
            ng-show="isNotEmpty(availableReportGrowingSystemSections[sector])">
            <label>{{sector == "null" ? 'Cultures assolées' : i18n.Sector[sector]}}&nbsp;:</label>
            <div ng-repeat="(key, value) in sections">
              <input type="checkbox"
                value="{{key}}" ng-click="toggleExportSection(sector, key)" ng-checked="exportReportSections[sector].indexOf(key) != -1"/>
              {{value}}
            </div>
          </div>
          <div class="materiel-list clear" style="width: 97%; margin-top: 10px;" ng-show="isNotEmpty(availableSectionType)">
            <label>Décisionnel&nbsp;:</label>
            <div ng-repeat="(key, value) in availableSectionType">
              <input type="checkbox"
                value="{{key}}" ng-click="toggleManagementSection(key)" ng-checked="exportManagementSections.indexOf(key) != -1"
                ng-disabled="exportManagementDisabled.indexOf(key) != -1" />
              {{value}}
            </div>
          </div>
       </form>
    </jqdialog>

    <jqdialog dialog-name="extendReportGrowingSystem" auto-open="false" title="'Prolongation de bilan de campagne'" modal="true" width="'600'"
      buttons="{'Prolonger': extendReportGrowingSystemClose, 'Annuler': extendReportGrowingSystemCancel}"
      button-classes="{'Prolonger': 'btn-primary', 'Annuler': 'float-left btn-secondary'}">
      <span class="ui-icon ui-icon-wrench" style="float: left; margin: 0 7px 80px 0;"></span>
      Prolonger le bilan de campagne / échelle régionale : {{firstSelectedReport.name}}<br />
      <hr />
      Nouvelle campagne du bilan de campagne : <input id="extendCampaignField" ng-model="extendCampaign" size="4" ng-pattern="/^[0-9]+$/" />
    </jqdialog>
  </div>
</body>
</html>
