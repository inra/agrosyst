<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div>

  <%@include file="inc-crop-template.jsp" %>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allYieldYieldObjective"><span class="required">*</span>&nbsp;L’objectif de rendement est-il atteint ?&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allYieldYieldObjective" ng-options="key as value for (key, value) in i18n.YieldObjective"
          ng-model="editedCropObject.yieldObjective" class="column-xlarge"
          ng-required="editedCropObject">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label">
      <label for="allYieldCause1">
        <span class="required" ng-if="isDephyFerme && editedCropObject.yieldObjective !== 'MORE_95'">*</span>&nbsp;
        Cause 1&nbsp;:
      </label>
    </span>
    <span class="wwctrl">
      <select id="allYieldCause1" ng-options="key as value for (key, value) in i18n['YieldLossCause#All']"
          ng-model="editedCropObject.cause1" class="column-xlarge"
          ng-required="isDephyFerme && editedCropObject.yieldObjective !== 'MORE_95'">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allYieldCause1">Cause 2&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allYieldCause2" ng-options="key as value for (key, value) in i18n['YieldLossCause#All']"
          ng-model="editedCropObject.cause2" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allYieldCause3">Cause 3&nbsp;:</label></span>
    <span class="wwctrl">
      <select id="allYieldCause3" ng-options="key as value for (key, value) in i18n['YieldLossCause#All']"
          ng-model="editedCropObject.cause3" class="column-xlarge">
        <option value=""></option>
      </select>
    </span>
  </div>
  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="allYieldComment">Commentaires sur la qualité&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="allYieldComment" ng-model="editedCropObject.comment" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>
</div>
