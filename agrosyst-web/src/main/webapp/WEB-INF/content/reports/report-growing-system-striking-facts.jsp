<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 - 2019 INRA, CodeLutin
  Copyright (C) 2020 INRAE, CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<!-- Faits marquants -->
<div id="tab_1" ng-controller="ReportGrowingSystemHighlightController">
  <fieldset>
    <div class= "infoLine block"
         style="margin-top: 20px;">
      <div class="endLineEvidence">
        <div style="cursor:pointer"
            class="textIcones blue x2"
            title="Information importante">!</div>
      </div>
      <div>
        <a href="<s:url value='/help/bioagg_cultures_spe.pdf' />"
           ng-if="selectedSectors.MARAICHAGE || selectedSectors.HORTICULTURE || selectedSectors.CULTURES_TROPICALES"
           target="_blank" rel="noopener noreferrer" class="black-link"
           style="color: #013f4e;"
           title="Cliquez sur ce bouton pour télécharger la liste des bio-agresseurs à saisir si présents dans le SDC">
           Liste des bioagresseurs à saisir si présents dans le SDC&nbsp;:&nbsp;
           <em class="fa fa-download" aria-hidden="true"></em>
        </a>
      </div>
      <div>
        <a href="<s:url namespace='/referential' action='management-mode-bio-aggressors-download'/>"
           target="" rel="noopener noreferrer" class="black-link"
           style="color: #013f4e;"
           title="Cliquez sur ce bouton pour télécharger la liste des bioagresseurs disponibles sur Agrosyst">
           Liste des bioagresseurs disponibles sur Agrosyst
          <em class="fa fa-download" aria-hidden="true"></em>
        </a>
      </div>
    </div>

    <div class="infoLine block" style="margin-top: 20px;" ng-show="isDephyExpe">
      <div class="endLineEvidence">
        <div style="cursor:pointer"
             class="textIcones blue x2"
             title="Information importante">!</div>
      </div>
      <div>
        <div>Certains champs ont été masqués par défaut, afin de faciliter la saisie d'un bilan de campagne pour la ou les filières choisies</div>
        <div><input type="checkbox" ng-value="false" ng-model="forceDisplayAll" />Afficher l'ensemble des champs du bilan de campagne</div>
      </div>
    </div>

    <div class="fade-animation" ng-class="{'ng-hide': isDephyExpeAndNotDisplayForced()}">

      <div class="subRubrique fade-animation">
        <h2>Faits marquants de l’année</h2>

        <div class="diagnoseEntries">
          <s:textarea name="reportGrowingSystem.highlightsEvolutions" label="Quelles sont les principales évolutions du système de culture depuis le début du suivi ?" labelPosition="top" labelSeparator="" cssClass="textarea-large"/>
          <s:textarea name="reportGrowingSystem.highlightsMeasures" label="Quelles mesures spécifiques aux conditions de l’année ont été mises en place sur le système de culture ?" labelPosition="top" labelSeparator="" cssClass="textarea-large"/>
          <s:textarea name="reportGrowingSystem.highlightsPerformances" label="Quels sont les faits marquants de l’année en terme de conduite des cultures et de performances techniques (rendement, qualité…) ?" labelPosition="top" labelSeparator="" cssClass="textarea-large"/>
          <s:textarea name="reportGrowingSystem.highlightsTeachings" label="Quels enseignements pour améliorer le système de culture ?" labelPosition="top" labelSeparator="" cssClass="textarea-large"/>
        </div>
      </div>
    </div>

    <div ng-if="isDephyExpe">
      <div class="infoLine block" style="margin-top: 20px;">
        <div class="endLineEvidence">
          <div style="cursor:pointer"
               class="textIcones blue x2"
               title="Information importante">!</div>
        </div>
        <div><strong>Les IFT à indiquer pour la maîtrise des maladies, ravageurs et adventices sont à calculer selon la méthode de l’IFT à la cible</strong></div>
      </div>
    </div>

    <div class="subRubrique" ng-if="reportGrowingSystem.sectors.includes('ARBORICULTURE')">
      <h2>Bilan des pratiques et État sanitaire des cultures (Arboriculture)</h2>

      <div ng-if="isDephyFerme" class="infoLine block" style="margin-top: 20px;">
        <div class="endLineEvidence">
          <div style="cursor:pointer"
               class="textIcones blue x2"
               title="Information importante">!</div>
        </div>
        <div>
          <div>&nbsp;-&nbsp; Rendement et qualité (<strong>tous les champs ‘*’)</strong> </div>
        </div>
      </div>

      <%@include file="report-growing-system-arbo-disease.jsp" %>
      <%@include file="report-growing-system-arbo-pest.jsp" %>
      <%@include file="report-growing-system-arbo-adventice.jsp" %>
      <%@include file="report-growing-system-arbo-foods.jsp" %>
      <%@include file="report-growing-system-arbo-yield.jsp" %>
    </div>

    <div ng-if="isDephyExpe">
      <jsp:include page="report-growing-system-expe-cultures-tropicales.jsp" />
      <jsp:include page="report-growing-system-expe-grandes-cultures.jsp" />
      <jsp:include page="report-growing-system-expe-horticulture.jsp" />
      <jsp:include page="report-growing-system-expe-maraichage.jsp" />
      <jsp:include page="report-growing-system-expe-polyculture-elevage.jsp" />
    </div>

    <div class="subRubrique" ng-if="!isDephyExpe && isOtherThanArboAndViti()" ng-controller="CulturesAssoleesScopeController">
      <h2>Bilan des pratiques et État sanitaire des cultures (Cultures assolées)</h2>

      <div ng-if="isDephyFerme" class="infoLine block" style="margin-top: 20px;">
        <div class="endLineEvidence">
          <div style="cursor:pointer"
               class="textIcones blue x2"
               title="Information importante">!</div>
        </div>
        <div>
          <div>&nbsp;-&nbsp; Rendement et qualité (<strong>tous les champs ‘*’)</strong> </div>
        </div>
      </div>

      <div ng-controller="CropPestMasterEditController">
        <div ng-controller="CropAllPestMastersController">
          <%@include file="report-growing-system-all-adventice.jsp" %>
          <div ng-controller="CropAdventiceMastersDialogController">
            <jqdialog dialog-name="addAdventiceCropsCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
                buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
                button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
                title="'Maîtrise des adventices'">
              <%@include file="inc-add-adventice-crops.jsp" %>
            </jqdialog>
            <jqdialog dialog-name="addAdventiceCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
                 buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
                 button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
                 title="'Adventice ciblée'">
              <%@include file="inc-add-adventice.jsp" %>
            </jqdialog>
          </div>
        </div>
      </div>

      <div ng-controller="CropPestMasterEditController">
        <div ng-controller="CropAllPestMastersController">
          <%@include file="report-growing-system-all-disease.jsp" %>
          <div ng-controller="CropDiseaseMastersDialogController">
            <!-- edition -->
            <jqdialog dialog-name="addDiseaseCropsCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
               buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
               button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
               title="'Maîtrise des maladies'">
                <%@include file="inc-add-disease-crops.jsp" %>
            </jqdialog>

            <jqdialog dialog-name="addDiseaseCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
               buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
               button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
               title="'Maladie ciblée'">
              <%@include file="inc-add-disease.jsp" %>
            </jqdialog>
          </div>
        </div>
      </div>

      <div ng-controller="CropPestMasterEditController">
        <div ng-controller="CropAllPestMastersController">
          <%@include file="report-growing-system-all-pest.jsp" %>
          <div ng-controller="CropPestMastersDialogController">
            <!-- edition -->
            <jqdialog dialog-name="addPestCropsCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
                  buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
                  button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
                  title="'Maîtrise des ravageurs'">
                <%@include file="inc-add-pest-crops.jsp" %>
            </jqdialog>

            <jqdialog dialog-name="addPestCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
               buttons="{'OK': onPestDialogOk, 'Annuler': onPestDialogCancel}"
               button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
               title="'Ravageur ciblé'">
              <%@include file="inc-add-pest.jsp" %>
            </jqdialog>
          </div>
        </div>
      </div>

      <div ng-controller="CropPestMasterEditController" ng-show="(reportGrowingSystem.sectors.indexOf('GRANDES_CULTURES') != -1 || reportGrowingSystem.sectors.indexOf('POLYCULTURE_ELEVAGE') != -1)">
        <div ng-controller="CropAllMastersController">
          <%@include file="report-growing-system-all-verse.jsp" %>
          <div ng-controller="CropVerseMastersDialogController">
            <jqdialog dialog-name="addVerseCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
                buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
                button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
                title="'Maîtrise de la verse'">
              <%@include file="inc-add-verse.jsp" %>
            </jqdialog>
          </div>
        </div>
      </div>

      <div ng-controller="CropPestMasterEditController">
        <div ng-controller="CropAllMastersController">
          <%@include file="report-growing-system-all-foods.jsp" %>
          <div ng-controller="CropFoodMastersDialogController">
            <jqdialog dialog-name="addFoodCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
               buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
               button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
               title="'Maîtrise de l’alimentation hydrique et minérale'">
              <%@include file="inc-add-food.jsp" %>
            </jqdialog>
          </div>
        </div>
      </div>

      <div ng-controller="CropPestMasterEditController">
        <div ng-controller="CropAllMastersController">
          <%@include file="report-growing-system-all-yield.jsp" %>
          <div ng-controller="CropYieldMastersDialogController">
            <jqdialog dialog-name="addYieldCulturesAssolees" auto-open="false" width="'55%'" class="dialog-form" modal="true"
               buttons="{'OK': onCropDialogOk, 'Annuler': onCropDialogCancel}"
               button-classes="{'OK': 'btn-primary', 'Annuler': 'float-left btn-secondary'}"
               title="'Rendement et qualité'">
              <%@include file="inc-add-yield.jsp" %>
            </jqdialog>
          </div>
        </div>
      </div>
    </div>

    <div class="subRubrique" ng-if="reportGrowingSystem.sectors.includes('VITICULTURE') && reportFilter.growingSystemId">
      <h2>Bilan des pratiques et État sanitaire des cultures (Viticulture)</h2>
      <div class="infoLine block" style="margin-top: 20px;">
        <div class="endLineEvidence">
          <div style="cursor:pointer"
               class="textIcones blue x2"
               title="Information importante">!</div>
        </div>
        <div>
          <div><strong>/!\ La saisie des données est obligatoire (choix de la filière) pour les tableaux </strong>:</div>
          <div>&nbsp;-&nbsp;Maitrise des maladies (<strong>4 maladies</strong> pré-renseignées dans le tableau + tous les champs <strong>‘*’</strong> sous le tableau)</div>
          <div>&nbsp;-&nbsp;Maitrise des ravageurs (<strong>3 ravageurs</strong> pré-renseignés dans le tableau + tous les champs <strong>‘*’</strong> sous le tableau)</div>
          <div ng-if="isDephyFerme">&nbsp;-&nbsp; Rendement et qualité (<strong>tous les champs ‘*’)</strong> </div>
          <div><strong>Vous devrez a minima compléter ces champs obligatoires avant de pouvoir enregistrer.</strong></div>
          <div><em>Les cases grisées représentent des informations non obligatoires pour les bilans de campagne EXPE.</em></div>
        </div>
      </div>

      <%@include file="report-growing-system-viti-disease.jsp" %>
      <%@include file="report-growing-system-viti-pest.jsp" %>
      <%@include file="report-growing-system-viti-adventice.jsp" %>
      <div ng-if="!isDephyExpeAndNotDisplayForced()">
        <%@include file="report-growing-system-viti-foods.jsp" %>
      </div>
      <%@include file="report-growing-system-viti-yield.jsp" %>

    </div>

    <div class="infoLine" style="margin-top: 20px;">
      <div class="endLineEvidence">
        <div style="cursor:pointer"
             class="textIcones blue x2"
             title="Information importante">!</div>
      </div>
      <div ng-if="observeManagementModeId">
        Le modèle décisionnel (constaté) existant pour ce SdC peut être complété (à l'enregistrement) à l'aide des
        données de votre bilan de campagne. Cochez la case suivante si vous souhaitez le compléter.
      </div>
      <div ng-if="!observeManagementModeId">
        Un modèle décisionnel (constaté) peut être créé (à l'enregistrement) à partir des données de votre bilan de
        campagne. Cochez la case suivante si vous souhaitez sa création.
      </div>
    </div>

    <div class="createManagementMode">
      <input id="createManagementMode" name="createManagementMode"
             type="checkbox" value="true"/>
      <label for="createManagementMode" class="createManagementMode" ng-if="observeManagementModeId">
        Compléter le modèle décisionnel existant à partir des données de ce bilan de campagne
      </label>
      <label for="createManagementMode" class="createManagementMode" ng-if="!observeManagementModeId">
        Créer un modèle décisionnel à partir des données de ce bilan de campagne
      </label>
    </div>

  </fieldset>

  <jqdialog dialog-name="displayQuestion" auto-open="false" width="400" class="dialog-form" modal="true"
       buttons="{'Oui': onDisplayQuestionYes, 'Non': onDisplayQuestionNo}"
       button-classes="{'Oui': 'btn-primary', 'Non': 'float-left btn-secondary'}">
    <div ng-bind-html="message"></div>
  </jqdialog>

  <jqdialog dialog-name="displayInfo" auto-open="false" width="400" class="dialog-form" modal="true"
       buttons="{'Ok': onDisplayInfoOk}"
       button-classes="{'Ok': 'btn-primary'}">
    <div ng-bind-html="message"></div>
  </jqdialog>

</div>
