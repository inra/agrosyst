<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="VitiAdventiceMastersController">

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadVitiAdventiceMasters()" href=""><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Maîtrise des adventices</a>
          <div ng-if="!isDephyExpe && !reportGrowingSystem.vitiAdventicePressureScale">
            <input type="text" ng-model="error" required="true" style="opacity:0;width: 0;"/>
          </div>
        </div>
      </label>
    </span>
  </div>
  <div ng-show="showVitiAdventiceMasters" class="loadContentEnclosure">
    <div ng-if="!isDephyExpeAndNotDisplayForced()">
      <div class="wwgrp">
        <span class="wwlbl">
          <label class="label"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Échelle de pression&nbsp;:</label>
        </span>
        <s:select name="reportGrowingSystem.vitiAdventicePressureScale" list="vitiAdventicePressureScales"
            emptyOption="true" cssClass="column-xlarge" ng-model="reportGrowingSystem.vitiAdventicePressureScale" required="{{!isDephyExpe}}"/>
      </div>
      <div class="wwgrp">
        <s:fielderror fieldName="reportGrowingSystem.vitiAdventicePressureFarmerComment" />
        <span class="wwlbl">
          <label class="label">Pression des adventices  - Expression de l’agriculteur&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <textarea name="reportGrowingSystem.vitiAdventicePressureFarmerComment">${reportGrowingSystem.vitiAdventicePressureFarmerComment}</textarea>
        </span>
      </div>
    </div>
    <div class="wwgrp">
      <span class="wwlbl">
        <label class="label"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span>Résultats obtenus, niveau de maîtrise finale&nbsp;:</label>
      </span>
      <s:select name="reportGrowingSystem.vitiAdventiceQualifier" list="globalMasterLevelQualifiers"
            emptyOption="true" cssClass="column-xlarge" required="{{!isDephyExpe}}"/>
    </div>
      <div ng-if="!isDephyExpeAndNotDisplayForced()" class="wwgrp">
        <s:fielderror fieldName="reportGrowingSystem.vitiAdventiceResultFarmerComment" />
        <span class="wwlbl">
          <label class="label">Niveau de maîtrise  - Expression de l’agriculteur&nbsp;:</label>
        </span>
        <span class="wwctrl">
          <textarea name="reportGrowingSystem.vitiAdventiceResultFarmerComment">${reportGrowingSystem.vitiAdventiceResultFarmerComment}</textarea>
        </span>
      </div>

      <table class="data-table">
        <thead>
          <tr>
            <th scope="col"><span ng-if="!isDephyExpe" class="required">*&nbsp;</span></th>
            <th scope="col" colspan="2">Nombre de traitements</th>
            <th scope="col" colspan="2">IFT</th>
          </tr>
          <tr>
              <th scope="col"/>
              <th scope="col">Chimique</th>
              <th scope="col">Biocontrôle</th>
              <th scope="col">Chimique</th>
              <th scope="col">Biocontrôle</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="col" class="thead">Traitements herbicides  (hors épamprage)</th>
            <td>
              <input type="text" name="reportGrowingSystem.vitiHerboTreatmentChemical" value="${reportGrowingSystem.vitiHerboTreatmentChemical}" required="{{!isDephyExpe}}" />
            </td>
            <td>
              <input type="text" name="reportGrowingSystem.vitiHerboTreatmentBioControl" value="${reportGrowingSystem.vitiHerboTreatmentBioControl}" required="{{!isDephyExpe}}" />
            </td>
            <td>
              <input type="text" name="reportGrowingSystem.vitiHerboTreatmentChemicalIFT" value="${reportGrowingSystem.vitiHerboTreatmentChemicalIFT}" required="{{!isDephyExpe}}" />
            </td>
            <td>
              <input type="text" name="reportGrowingSystem.vitiHerboTreatmentBioControlIFT" value="${reportGrowingSystem.vitiHerboTreatmentBioControlIFT}" required="{{!isDephyExpe}}" />
            </td>
          </tr>
          <tr>
              <th scope="col" class="thead">Epamprage</th>
              <td>
                <input type="text" name="reportGrowingSystem.vitiSuckeringChemical" value="${reportGrowingSystem.vitiSuckeringChemical}" required="{{!isDephyExpe}}" />
              </td>
              <td>
                <input type="text" name="reportGrowingSystem.vitiSuckeringBioControl" value="${reportGrowingSystem.vitiSuckeringBioControl}" required="{{!isDephyExpe}}" />
              </td>
              <td>
                <input type="text" name="reportGrowingSystem.vitiSuckeringChemicalIFT" value="${reportGrowingSystem.vitiSuckeringChemicalIFT}" required="{{!isDephyExpe}}" />
              </td>
              <td>
                <input type="text" name="reportGrowingSystem.vitiSuckeringBioControlIFT" value="${reportGrowingSystem.vitiSuckeringBioControlIFT}" required="{{!isDephyExpe}}"  />
              </td>
          </tr>
        </tbody>
      </table>
  </div>
</div>
