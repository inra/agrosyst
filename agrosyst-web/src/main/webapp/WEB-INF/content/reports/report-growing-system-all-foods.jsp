<%--
  #%L
  Agrosyst :: Web
  $Id$
  $HeadURL$
  %%
  Copyright (C) 2017 INRA
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<%@taglib uri="/struts-tags" prefix="s" %>

<div ng-controller="CropFoodMastersController">

  <input type="hidden" name="foodMastersJson" value="{{getCropMasters()}}"/>

  <div class="wwgrp">
    <span class="wwlbl">
      <label class="tags">
        <div class="tags">
          <a ng-click="loadCropMaster()" href="">Maîtrise de l’alimentation hydrique et minérale</a>
        </div>
      </label>
    </span>
  </div>

  <div class="grid-enclosure loadContentEnclosure" ng-if="showCropMasters"
    ng-attr-style="grid-template-columns: 20% repeat({{viewPortArray.length||1}}, 1fr) min-content;">
    <div ng-attr-style="grid-column: 1;grid-row: 1;" ng-click="moveViewPort(-1)" ng-if="viewPortIndex > 0" class="gridArrow">&Lang;</div>
    <div ng-attr-style="grid-column: {{viewPortArray.length + 3}};grid-row: 1;" ng-click="moveViewPort(+1)" ng-if="viewPortIndex < maxViewPort - 3" class="gridArrow">&Rang;</div>
    <div ng-attr-style="grid-column: 1;grid-row: 2;" class="gridColumnMainHeader">Alimentation hydrique&nbsp;:</div>
    <div ng-attr-style="grid-column: 1;grid-row: 3;" class="gridColumnHeader oddCell">- Irrigation</div>
    <div ng-attr-style="grid-column: 1;grid-row: 4;" class="gridColumnHeader">- Stress hydrique</div>
    <div ng-attr-style="grid-column: 1;grid-row: 5;" class="gridColumnHeader oddCell">Azote</div>
    <div ng-attr-style="grid-column: 1;grid-row: 6;" class="gridColumnHeader">Alimentation minérale (hors azote)</div>
    <div ng-attr-style="grid-column: 1;grid-row: 7;" class="gridColumnHeader oddCell">Température et rayonnement</div>
    <div ng-attr-style="grid-column: 1;grid-row: 8;" class="gridColumnHeader">Commentaires – Résultats obtenus et faits marquants</div>
    <div ng-attr-style="grid-column: 1;grid-row: 9;" class="gridColumnHeader"></div>

    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 1;" class="gridMainHeader" ng-repeat-start="cropMaster in viewPortArray">
      <span ng-repeat="crop in cropMaster.crops">
        {{crop.name}}<span ng-if="!$last">,</span>
      </span>
    </div>
    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 2;" class="gridCell" ng-class="{gridCellLast: $last}"></div>
    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 3;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{cropMaster.foodIrrigation|translate:'FoodIrrigation'}}</div>
    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 4;" class="gridCell" ng-class="{gridCellLast: $last}">{{cropMaster.hydriqueStress|translate:'StressLevel'}}</div>
    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 5;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{cropMaster.azoteStress|translate:'StressLevel'}}</div>
    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 6;" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{cropMaster.mineralFood}}"></div>
    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 7;" class="gridCell oddCell" ng-class="{gridCellLast: $last}">{{cropMaster.tempStress|translate:'StressLevel'}}</div>
    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 8;" class="gridCell" ng-class="{gridCellLast: $last}" ag-read-more="{{cropMaster.comment}}"></div>

    <div ng-attr-style="grid-column: {{$index+2}};grid-row: 9;" class="gridActions" ng-class="{gridCellLast: $last}" ng-repeat-end>
      <input type="button" class="btn-icon icon-edit" value="Modifier" title="Modifier" ng-click="editCropMaster(cropMaster)" />
      <input type="button" class="btn-icon icon-delete" value="Supprimer" title="Supprimer" ng-click="deleteCropMaster(cropMaster)" />
    </div>
    <div ng-attr-style="grid-column: 2;grid-row: 1 / span 9;" class="gridEmpty" ng-if="viewPortArray.length == 0">
      Aucune donnée présente. Vous pouvez en ajouter un en cliquant sur le bouton "Ajouter"
    </div>
    <div ng-attr-style="grid-column: 2 / span 4;grid-row: 10;" class="gridFooter">
      <input type="button" value="Ajouter une culture" ng-click="addCropMaster()"/>
    </div>
  </div>
</div>
