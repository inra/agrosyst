<%--
  #%L
  Agrosyst :: Web
  %%
  Copyright (C) 2013 - 2020 INRAE, Code Lutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/gpl-3.0.html>.
  #L%
  --%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" session="false" %>
<div class="asTable" id="addCropAndSpecies">

  <%@include file="inc-crop-template.jsp" %>

  <div class="section">
    <span class="required">*</span>&nbsp;Ravageur(s) pour ces cultures
  </div>

  <div>
    <ul class="asTable">
      <li class="asContentTable0 fromContent">

        <span ng-repeat="pestMaster in editedCropObject.pestMasters">
          <span style="cursor:pointer" class="link"
            ng-click="editPestMaster(editedCropObject, pestMaster)">{{getAgressorLabel(pestMaster)}}</span>
          <input type="button" class="btn-icon icon-delete" ng-click="deletePestMaster(editedCropObject, pestMaster)" value="x" title="Supprimer"/>
          <span ng-if="!$last">,</span>
        </span>

        <span class="info" ng-if="editedCropObject.pestMasters.length === 0">
          Aucun ravageur déclaré
        </span>

        <span class="endLine">
          <div style="cursor:pointer"
               class="textIcones blue x2"
               ng-click="editPestMaster(editedCropObject)"
               title="Déclarer un ravageur">
               +
          </div>
        </span>

      </li>
    </ul>
  </div>

  <div class="section">Quel niveau d’utilisation de pesticides ?</div>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="iftMain-all-pest">IFT-Insecticide&nbsp;:</label></span>
    <span class="wwctrl">
      <input id="iftMain-all-pest" type="text"
                 class="column-xlarge" ng-model="editedCropObject.iftMain" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
    </span>
  </div>

  <div class="wwgrp">
      <span class="wwlbl wide-label"><label for="iftOther">IFT-autre (ravageurs)&nbsp;:</label></span>
      <span class="wwctrl">
        <input id="iftOther" type="text"
                 class="column-xlarge" ng-model="editedCropObject.iftOther" placeholder="ex: 1.5" ag-float2dec pattern="^\d+(?:[\.,]\d{1,2})?$"/>
      </span>
  </div>

  <div class="wwgrp">
    <span class="wwlbl wide-label"><label for="adviserComments">Commentaire du conseiller&nbsp;:</label></span>
    <span class="wwctrl">
      <textarea id="adviserComments" type="text" ng-model="editedCropObject.adviserComments" class="column-xlarge" column="20" rows="5"></textarea>
    </span>
  </div>
</div>
