/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-typescript',
    '@vue/eslint-config-prettier/skip-formatting'
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  plugins: ['no-relative-import-paths'],
  rules: {
    'vue/multi-word-component-names': 'off',
    'no-relative-import-paths/no-relative-import-paths': [
      'error',
      { allowSameFolder: false, rootDir: 'src', prefix: '@' }
    ]
  },
  overrides: [
    {
      files: ['vitest.config.ts'],
      rules: {
        'no-relative-import-paths/no-relative-import-paths': 'off'
      }
    }
  ]
}
