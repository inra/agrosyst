/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { API_URL, INIT } from '@/services/constants'
import type { GrowingSystemDto } from '@/services/growing-systems/growing-system'
import type { GrowingSystemRequestFilter } from '@/services/growing-systems/growing-system-request-filter'
import type { GrowingSystemFilters } from '@/services/growing-systems/growing-system-filters'
import type { PageDto, PageParameters } from '@/services/page'
import type { Sector } from '@/model/growing-systems/sector'

const GROWING_SYSTEMS_PATH = '/growing-systems'

export function buildFilters(
  selectedDomains: string[] | null,
  name: string | null,
  domainName: string | null,
  gorwinPlanName: string | null,
  campaign: number | null,
  sector: Sector | null,
  dephyNumber: string | null,
  active: boolean | null,
  selectedIds: string[] | null
): GrowingSystemFilters {
  return { selectedDomains, name, domainName, gorwinPlanName, campaign, sector, dephyNumber, active, selectedIds }
}

function buildGrowingSystemRequestFilter(filters: GrowingSystemFilters, params?: PageParameters): GrowingSystemRequestFilter {
  return {
    selectedDomains: filters.selectedDomains ? Array.from(filters.selectedDomains) : undefined,
    name: filters.name ? filters.name : undefined,
    domainName: filters.domainName ? filters.domainName : undefined,
    gorwinPlanName: filters.gorwinPlanName ? filters.gorwinPlanName : undefined,
    campaign: filters.campaign ? filters.campaign : undefined,
    sector: filters.sector ? filters.sector : undefined,
    dephyNumber: filters.dephyNumber ? filters.dephyNumber : undefined,
    active: filters.active ? filters.active : undefined,
    selectedIds: filters.selectedIds ? Array.from(filters.selectedIds) : undefined,
    pageNumber: params?.pageNumber,
    pageSize: params?.pageSize,
  };
}

export async function getGrowingSystems(
  filters: GrowingSystemFilters,
  params: PageParameters
): Promise<PageDto<GrowingSystemDto>> {

  const queryParams = buildGrowingSystemRequestFilter(filters, params);

  return fetch(`${API_URL}${GROWING_SYSTEMS_PATH}`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des systèmes de cultures:", error));
}

export async function getGrowingSystemIds(
  filters: GrowingSystemFilters): Promise<string[]> {

    const queryParams = buildGrowingSystemRequestFilter(filters, undefined);

    return fetch(`${API_URL}${GROWING_SYSTEMS_PATH}/filtered-ids`, {
      ...INIT,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(queryParams),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP Status: ${response.status}`);
        }
        return response.json();
      })
      .then((json) => json)
      .catch((error) => console.error("Erreur de récuparation des Ids de systèmes de cultures:", error));
}
