/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { API_URL, INIT } from '@/services/constants'
import type { NetworkDto } from '@/services/networks/network'
import type { PageDto, PageParameters } from '@/services/page'
import type { NetworkFilters } from '@/services/networks/network-filters'
import type { NetworkRequestFilter } from '@/services/networks/network-request-filter'

const NETWORKS_PATH = '/networks'

export function buildFilters(
  name: string | null,
  responsable: string | null,
  active: boolean | null,
  selectedIds: string[] | null
): NetworkFilters {
  return { name, responsable, active, selectedIds }
}

function buildNetworkRequestFilter(filters: NetworkFilters, params?: PageParameters): NetworkRequestFilter {
  return {
    name: filters.name ? filters.name : undefined,
    responsable: filters.responsable ? filters.responsable : undefined,
    active: filters.active ? filters.active : undefined,
    selectedIds: filters.selectedIds ? Array.from(filters.selectedIds) : undefined,
    pageNumber: params?.pageNumber,
    pageSize: params?.pageSize,
  };
}

export async function getNetworks(filters: NetworkFilters, params: PageParameters): Promise<PageDto<NetworkDto>> {
  const queryParams = buildNetworkRequestFilter(filters, params)
  return fetch(`${API_URL}${NETWORKS_PATH}`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des réseaux:", error));
}

export async function getNetworkIds(filters: NetworkFilters): Promise<string[]> {
  const queryParams = buildNetworkRequestFilter(filters, undefined)
  return fetch(`${API_URL}${NETWORKS_PATH}/filtered-ids`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des Ids de réseaux:", error));
}
