/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
export interface PageParameters {
  pageNumber: number
  pageSize: number
}

export function buildParams(pageNumber: number, pageSize: number): PageParameters {
  return { pageNumber, pageSize }
}

export interface PageDto<T> {
  elements: T[]
  count: number
  currentPage: {
    pageNumber: number
    pageSize: number
  }
}

export const DEFAULT_PAGE = {
  elements: [],
  count: 0,
  currentPage: { pageNumber: 0, pageSize: 10 }
} as PageDto<any>
