/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
export function getSubPath() {
  var result = window.location.pathname.match(/^\/([^\/]+)\/nova\/$/)
  if (result) {
    return '/' + result[1]
  }
  return ''
}
export const API_URL: string =
  import.meta.env.VITE_APP_API_URL || `${window.location.protocol}//${window.location.host}${getSubPath()}/rest`
export const ACTION_URL: string =
  import.meta.env.VITE_APP_ACTION_URL || `${window.location.protocol}//${window.location.host}${getSubPath()}`
export const INIT: RequestInit = { credentials: 'same-origin' }
