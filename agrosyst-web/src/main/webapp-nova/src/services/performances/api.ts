/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { API_URL, INIT } from '@/services/constants'
import type { PerformanceDto } from '@/services/performances/performance'
import type { PerformanceCreationEditionDto } from '@/services/performances/performance-creation-edition'

export async function createPerformance(performance: PerformanceCreationEditionDto): Promise<PerformanceDto> {
  return fetch(`${API_URL}/performances`, {
    ...INIT,
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(performance)
  })
    .then((response) => response.json())
    .then((json) => json as PerformanceDto)
}

export async function readPerformance(topiaId: String): Promise<PerformanceDto> {
  return fetch(`${API_URL}/performances/${topiaId}`, INIT)
    .then((response) => response.json())
    .then((json) => json as PerformanceDto)
}

export async function updatePerformance(
  topiaId: String,
  performance: PerformanceCreationEditionDto
): Promise<PerformanceDto> {
  return fetch(`${API_URL}/performances/${topiaId}`, {
    ...INIT,
    method: 'put',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(performance)
  })
    .then((response) => response.json())
    .then((json) => json as PerformanceDto)
}
