/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { API_URL, INIT } from '@/services/constants'
import type { DomainDto } from '@/services/domains/domain'
import type { DomainFilters } from '@/services/domains/domain-filters'
import type { PageDto, PageParameters } from '@/services/page'
import type { DomainRequestFilter } from '@/services/domains/domain-request-filter'
import type { DomainType } from '@/model/domains/domain-type'

const DOMAINS_PATH = '/domains'

export function buildFilters(
  name: string | null,
  campaign: number | null,
  type: DomainType | null,
  responsable: string | null,
  departement: string | null,
  active: boolean | null,
  selectedIds: string[] | null,
  withoutContext: boolean | null
): DomainFilters {
  return { name, campaign, type, responsable, departement, active, selectedIds, withoutContext }
}

function buildDomainRequestFilter(filters: DomainFilters, params?: PageParameters): DomainRequestFilter {
  return {
    name: filters.name ? filters.name : undefined,
    campaign: filters.campaign ? filters.campaign : undefined,
    type: filters.type ? filters.type : undefined,
    responsable: filters.responsable ? filters.responsable : undefined,
    departement: filters.departement ? filters.departement : undefined,
    active: filters.active ? filters.active : undefined,
    selectedIds: filters.selectedIds ? Array.from(filters.selectedIds) : undefined,
    withoutContext: filters.withoutContext ? filters.withoutContext : undefined,
    pageNumber: params?.pageNumber,
    pageSize: params?.pageSize,
  };
}

export async function getDomains(filters: DomainFilters, params: PageParameters): Promise<PageDto<DomainDto>> {
  const queryParams = buildDomainRequestFilter(filters, params)
  return fetch(`${API_URL}${DOMAINS_PATH}`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des domaines:", error));
}

export async function getDomainIds(filters: DomainFilters): Promise<string[]> {
  const queryParams = buildDomainRequestFilter(filters, undefined)
  return fetch(`${API_URL}${DOMAINS_PATH}/filtered-ids`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des Ids de domaines:", error));
}
