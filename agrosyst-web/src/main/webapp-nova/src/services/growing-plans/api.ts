/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { TypeDEPHY } from '@/model/growing-plans/type-dephy'
import { API_URL, INIT } from '@/services/constants'
import type { GrowingPlanDto } from '@/services/growing-plans/growing-plan'
import type { GrowingPlanFilters } from '@/services/growing-plans/growing-plan-filters'
import type { PageDto, PageParameters } from '@/services/page'
import type { GrowingPlanRequestFilter } from '@/services/growing-plans/growing-plan-request-filter'

const GROWING_PLANS_PATH = '/growing-plans'

export function buildFilters(
  name: string | null,
  domainName: string | null,
  campaign: number | null,
  typeDEPHY: TypeDEPHY | null,
  responsable: string | null,
  active: boolean | null,
  selectedIds: string[] | null
): GrowingPlanFilters {
  return { name, domainName, campaign, typeDEPHY, responsable, active, selectedIds }
}

function buildGrowingPlanRequestFilter(filters: GrowingPlanFilters, params?: PageParameters): GrowingPlanRequestFilter {
  return {
    name: filters.name ? filters.name : undefined,
    domainName: filters.domainName ? filters.domainName : undefined,
    campaign: filters.campaign ? filters.campaign : undefined,
    typeDEPHY: filters.typeDEPHY ? filters.typeDEPHY : undefined,
    responsable: filters.responsable ? filters.responsable : undefined,
    active: filters.active ? filters.active : undefined,
    selectedIds: filters.selectedIds ? Array.from(filters.selectedIds) : undefined,
    pageNumber: params?.pageNumber,
    pageSize: params?.pageSize,
  };
}

export async function getGrowingPlans(
  filters: GrowingPlanFilters,
  params: PageParameters
): Promise<PageDto<GrowingPlanDto>> {
  const queryParams = buildGrowingPlanRequestFilter(filters, params)
  return fetch(`${API_URL}${GROWING_PLANS_PATH}`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des dispositifs:", error));
}

export async function getGrowingSystemIds(filters: GrowingPlanFilters): Promise<string[]> {
  const queryParams = buildGrowingPlanRequestFilter(filters, undefined)
  return fetch(`${API_URL}${GROWING_PLANS_PATH}/filtered-ids`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des Ids de dispositifs:", error));
}