/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { API_URL, INIT } from '@/services/constants'
import type { PageDto, PageParameters } from '@/services/page'
import type { PlotDto } from '@/services/plots/plot'
import type { PlotFilters } from '@/services/plots/plot-filters'
import type { PlotRequestFilter } from '@/services/plots/plot-request-filter'

const PLOTS_PATH = '/plots'

export function buildFilters(
  selectedDomains: string[] | null,
  selectedGrowingSystems: string[] | null,
  active: boolean | null,
  selectedIds: string[] | null
): PlotFilters {
  return {
    selectedDomains,
    selectedGrowingSystems,
    active,
    selectedIds
  }
}

function buildPlotRequestFilter(filters: PlotFilters, params?: PageParameters): PlotRequestFilter {
  return {
    selectedDomains: filters.selectedDomains ? filters.selectedDomains : undefined,
    selectedGrowingSystems: filters.selectedGrowingSystems ? filters.selectedGrowingSystems : undefined,
    active: filters.active ? filters.active : undefined,
    selectedIds: filters.selectedIds ? Array.from(filters.selectedIds) : undefined,
    pageNumber: params?.pageNumber,
    pageSize: params?.pageSize,
  };
}

export async function getPlots(filters: PlotFilters, params: PageParameters): Promise<PageDto<PlotDto>> {
  const queryParams = buildPlotRequestFilter(filters, params)
  return fetch(`${API_URL}${PLOTS_PATH}`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des parcelles:", error));
}

export async function getPlotIds(filters: PlotFilters): Promise<string[]> {
  const queryParams = buildPlotRequestFilter(filters, undefined)
  return fetch(`${API_URL}${PLOTS_PATH}/filtered-ids`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des Ids de parcelles:", error));
}
