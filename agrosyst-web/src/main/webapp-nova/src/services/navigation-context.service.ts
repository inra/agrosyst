/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { initCookie, getCookie, setCookie } from '@/services/cookies.service'

export interface NavigationContext {
  campaigns: number[] | null
  networks: string[] | null
  domains: string[] | null
  growingPlans: string[] | null
  growingSystems: string[] | null
}

export const EMPTY_CONTEXT = {
  campaigns: [],
  networks: [],
  domains: [],
  growingPlans: [],
  growingSystems: []
} as NavigationContext

export function buildNavigationContext(
  campaigns: number[] | null,
  networks: string[] | null,
  domains: string[] | null,
  growingPlans: string[] | null,
  growingSystems: string[] | null
): NavigationContext {
  return { campaigns, networks, domains, growingPlans, growingSystems }
}

export namespace NavigationContextService {
  const NAVIGATION_CONTEXT_COOKIE_NAME = 'nav.context'

  export function initContext() {
    initCookie()
  }

  export function getContext(): NavigationContext | undefined {
    return getCookie(NAVIGATION_CONTEXT_COOKIE_NAME) as NavigationContext
  }

  export function setContext(context: NavigationContext) {
    setCookie(NAVIGATION_CONTEXT_COOKIE_NAME, context)
  }
}
