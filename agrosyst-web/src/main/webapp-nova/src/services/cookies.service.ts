/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { inject } from 'vue'
import type { VueCookies } from 'vue-cookies'
import { getSubPath } from './constants'

let $cookies: VueCookies | undefined = undefined

export function initCookie() {
  $cookies = inject<VueCookies>('$cookies')
}
export function getCookie(keyName: string): any | undefined {
  return $cookies?.get(keyName)
}

export function setCookie(keyName: string, value: any) {
  const host = window.location.host
  const domain = host.includes(':') ? host.substring(0, host.indexOf(':')) : host
  let subPath = getSubPath()
  $cookies?.set(keyName, value, 0, subPath ? subPath : '/', domain, window.location.protocol === 'https', 'Lax')
}
