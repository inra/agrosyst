/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { API_URL, INIT } from '@/services/constants'
import type { PageDto, PageParameters } from '@/services/page'
import type { ZoneDto } from '@/services/zones/zone'
import type { ZoneFilters } from '@/services/zones/zone-filters'
import type { ZoneRequestFilter } from '@/services/zones/zone-request-filter'

const ZONES_PATH = '/zones'

export function buildFilters(
  selectedPlots: string[] | null,
  active: boolean | null,
  selectedIds: string[] | null
): ZoneFilters {
  return { selectedPlots, active, selectedIds }
}

function buildZoneRequestFilter(filters: ZoneFilters, params?: PageParameters): ZoneRequestFilter {
  return {
    selectedPlots: filters.selectedPlots ? filters.selectedPlots : undefined,
    active: filters.active ? filters.active : undefined,
    selectedIds: filters.selectedIds ? Array.from(filters.selectedIds) : undefined,
    pageNumber: params?.pageNumber,
    pageSize: params?.pageSize,
  };
}

export async function getZones(filters: ZoneFilters, params: PageParameters): Promise<PageDto<ZoneDto>> {
  const queryParams = buildZoneRequestFilter(filters, params)
  return fetch(`${API_URL}${ZONES_PATH}`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des zones:", error));
}

export async function getZoneIds(filters: ZoneFilters): Promise<string[]> {
  const queryParams = buildZoneRequestFilter(filters, undefined)
  return fetch(`${API_URL}${ZONES_PATH}/filtered-ids`, {
    ...INIT,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryParams),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP Status: ${response.status}`);
      }
      return response.json();
    })
    .then((json) => json)
    .catch((error) => console.error("Erreur de récuparation des Ids de zones:", error));
}
