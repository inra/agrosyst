/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { getLocale, loadLocaleMessages, setI18nLanguage, SUPPORT_LOCALES } from '@/i18n'
import type { I18n } from 'vue-i18n'
import type { Router, RouteRecordRaw } from 'vue-router'
import { createRouter, createWebHashHistory } from 'vue-router'

export function setupRouter(i18n: I18n): Router {
  const locale = getLocale(i18n)

  const routes: RouteRecordRaw[] = [
    {
      path: '/:locale/performances/new',
      name: 'PerformanceCreation',
      meta: { title: 'Nouvelle performance' },
      component: () => import('../views/performances/PerformanceView.vue')
    },
    {
      path: '/:locale/performances/:topiaId',
      name: 'PerformanceEdition',
      meta: { title: 'Performance' },
      component: () => import('../views/performances/PerformanceView.vue'),
      props: true
    },
    {
      path: '/:locale/performances',
      name: 'PerformancesList',
      meta: { title: 'Performances réalisées' },
      component: () => import('../views/performances/PerformancesView.vue')
    },
    {
      path: '/:locale/',
      name: 'Home',
      meta: { title: 'Accueil' },
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'CatchAllRedirect',
      redirect: () => {
        return `/${locale}`
      }
    }
  ]

  const router = createRouter({
    history: createWebHashHistory(import.meta.env.BASE_URL),
    routes
  })

  router.beforeEach(async (to, from, next) => {
    const appTitle =
      to.query.frontApp && String(to.query.frontApp).toLocaleLowerCase() === 'ipmworks' ? 'IPMworks' : 'Agrosyst'
    document.title = `${appTitle} : ${to.meta.title}`

    const paramsLocale = to.params.locale as string

    // use locale if paramsLocale is not in SUPPORT_LOCALES
    if (!SUPPORT_LOCALES.includes(paramsLocale)) {
      return `/${locale}`
    }

    // load locale messages
    if (!i18n.global.availableLocales.includes(paramsLocale)) {
      await loadLocaleMessages(i18n, paramsLocale)
    }

    // set i18n language
    setI18nLanguage(i18n, paramsLocale)

    next()
  })

  return router
}
