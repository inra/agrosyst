/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { IFT_AUTRES } from '@/model/performances/indicators/ift/ift-autres'
import { IFT_BIOCONTROLE } from '@/model/performances/indicators/ift/ift-biocontrole'
import { IFT_CHIMIQUE_TOTAL } from '@/model/performances/indicators/ift/ift-chimique-total'
import { IFT_CHIMIQUE_TOTAL_HORS_HERBICIDE } from '@/model/performances/indicators/ift/ift-chimique-total-hors-herbicide'
import { IFT_CHIMIQUE_TOTAL_HORS_HERBICIDE_HTS } from '@/model/performances/indicators/ift/ift-chimique-total-hors-herbicide-hts'
import { IFT_CHIMIQUE_TOTAL_HTS } from '@/model/performances/indicators/ift/ift-chimique-total-hts'
import { IFT_FONGICIDE } from '@/model/performances/indicators/ift/ift-fongicide'
import { IFT_HERBICIDE } from '@/model/performances/indicators/ift/ift-herbicide'
import { IFT_INSECTICIDE } from '@/model/performances/indicators/ift/ift-insecticide'
import { IFT_TOTAL_AVEC_BIOCONTROLE } from '@/model/performances/indicators/ift/ift-total-avec-biocontrole'
import { IFT_TRAITEMENT_DE_SEMENCES } from '@/model/performances/indicators/ift/ift-traitement-de-semences'
import { IFT_A_LA_CIBLE_NON_MILLESIME } from '@/model/performances/methods/ift-a-la-cible-non-millesime'
import { IFT_A_LA_CIBLE_MILLESIME } from '@/model/performances/methods/ift-a-la-cible-millesime'
import { IFT_A_LA_CULTURE_MILLESIME } from '@/model/performances/methods/ift-a-la-culture-millesime'
import { IFT_A_LA_CULTURE_NON_MILLESIME } from '@/model/performances/methods/ift-a-la-culture-non-millesime'
import { IFT_A_L_ANCIENNE_NON_MILLESIME } from '@/model/performances/methods/ift-a-l-ancienne-non-millesime'

export const IFT: CategoryKind = {
  key: 'IFT',
  methods: [
    IFT_A_LA_CIBLE_NON_MILLESIME,
    IFT_A_LA_CIBLE_MILLESIME,
    IFT_A_LA_CULTURE_NON_MILLESIME,
    IFT_A_LA_CULTURE_MILLESIME,
    IFT_A_L_ANCIENNE_NON_MILLESIME
  ],
  indicators: [
    IFT_CHIMIQUE_TOTAL,
    IFT_CHIMIQUE_TOTAL_HTS,
    IFT_CHIMIQUE_TOTAL_HORS_HERBICIDE,
    IFT_CHIMIQUE_TOTAL_HORS_HERBICIDE_HTS,
    IFT_HERBICIDE,
    IFT_FONGICIDE,
    IFT_INSECTICIDE,
    IFT_TRAITEMENT_DE_SEMENCES,
    IFT_AUTRES,
    IFT_TOTAL_AVEC_BIOCONTROLE,
    IFT_BIOCONTROLE
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
