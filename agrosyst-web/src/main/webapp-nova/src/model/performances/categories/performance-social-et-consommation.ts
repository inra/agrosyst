/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { CONSOMMATION_DE_CARBURANT } from '@/model/performances/indicators/performance-social-et-consommation/consommation-de-carburant'
import { CONSOMMATION_EAU } from '@/model/performances/indicators/performance-social-et-consommation/consommation-eau'
import { NOMBRE_UHT_NECESSAIRES } from '@/model/performances/indicators/performance-social-et-consommation/nombre-uht-necessaires'
import { SURFACE_PAR_UHT } from '@/model/performances/indicators/performance-social-et-consommation/surface-par-uht'

export const PERFORMANCE_SOCIALE_ET_CONSOMMATION: CategoryKind = {
  key: 'PERFORMANCE_SOCIALE_ET_CONSOMMATION',
  methods: [],
  indicators: [CONSOMMATION_DE_CARBURANT, CONSOMMATION_EAU, SURFACE_PAR_UHT, NOMBRE_UHT_NECESSAIRES],
  active: true,
  indicatorsBySubCategories: {}
} as const
