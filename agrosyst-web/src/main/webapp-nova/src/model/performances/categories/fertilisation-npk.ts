/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { K2O_MINERAL } from '@/model/performances/indicators/fertilisation-npk/k2o-mineral'
import { K2O_ORGANIQUE } from '@/model/performances/indicators/fertilisation-npk/k2o-organique'
import { K2O_TOTAL } from '@/model/performances/indicators/fertilisation-npk/k2o-total'
import { N_MINERAL } from '@/model/performances/indicators/fertilisation-npk/n-mineral'
import { N_ORGANIQUE } from '@/model/performances/indicators/fertilisation-npk/n-organique'
import { N_TOTAL } from '@/model/performances/indicators/fertilisation-npk/n-total'
import { P2O5_MINERAL } from '@/model/performances/indicators/fertilisation-npk/p2o5-mineral'
import { P2O5_ORGANIQUE } from '@/model/performances/indicators/fertilisation-npk/p2o5-organique'
import { P2O5_TOTAL } from '@/model/performances/indicators/fertilisation-npk/p2o5-total'

export const FERTILISATION_NPK: CategoryKind = {
  key: 'FERTILISATION_NPK',
  methods: [],
  indicators: [
    N_TOTAL,
    N_MINERAL,
    N_ORGANIQUE,
    P2O5_TOTAL,
    P2O5_MINERAL,
    P2O5_ORGANIQUE,
    K2O_TOTAL,
    K2O_MINERAL,
    K2O_ORGANIQUE
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
