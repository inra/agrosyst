/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { NOMBRE_DE_PASSAGES } from '@/model/performances/indicators/temps-de-travail/nombre-de-passages'
import { TEMPS_DE_TRAVAIL_MANUEL } from '@/model/performances/indicators/temps-de-travail/temps-de-travail-manuel'
import { TEMPS_DE_TRAVAIL_MECANISE } from '@/model/performances/indicators/temps-de-travail/temps-de-travail-mecanise'
import { TEMPS_DE_TRAVAIL_TOTAL as INDICATOR_TEMPS_DE_TRAVAIL_TOTAL } from '@/model/performances/indicators/temps-de-travail/temps-de-travail-total'
import { TEMPS_UTILISATION_DU_MATERIEL } from '@/model/performances/indicators/temps-de-travail/temps-utilisation-du-materiel'
import { TEMPS_DE_TRAVAIL_PAR_MOIS } from '@/model/performances/methods/temps-de-travail-par-mois'
import { TEMPS_DE_TRAVAIL_TOTAL as METHOD_TEMPS_DE_TRAVAIL_TOTAL } from '@/model/performances/methods/temps-de-travail-total'

export const TEMPS_DE_TRAVAIL: CategoryKind = {
  key: 'TEMPS_DE_TRAVAIL',
  methods: [METHOD_TEMPS_DE_TRAVAIL_TOTAL, TEMPS_DE_TRAVAIL_PAR_MOIS],
  indicators: [
    INDICATOR_TEMPS_DE_TRAVAIL_TOTAL,
    TEMPS_DE_TRAVAIL_MECANISE,
    TEMPS_UTILISATION_DU_MATERIEL,
    TEMPS_DE_TRAVAIL_MANUEL,
    NOMBRE_DE_PASSAGES
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
