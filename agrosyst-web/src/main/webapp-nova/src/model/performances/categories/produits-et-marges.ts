/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { MARGE_BRUTE } from '@/model/performances/indicators/produits-et-marges/marge-brute'
import { MARGE_DIRECTE } from '@/model/performances/indicators/produits-et-marges/marge-directe'
import { MARGE_SEMI_NETTE } from '@/model/performances/indicators/produits-et-marges/marge-semi-nette'
import { PRODUIT_BRUT } from '@/model/performances/indicators/produits-et-marges/produit-brut'

import { PRIX_REELS_PRODUITS_MARGES } from '@/model/performances/methods/prix-reels-produits-marges'
import { PRIX_STANDARDISES_MILLESIMES_PRODUITS_MARGES } from '@/model/performances/methods/prix-standardises-millesimes-produits-charges'
import { AVEC_AUTOCONSOMMATION_PRODUITS_MARGES } from '@/model/performances/methods/avec-autoconsommation-produits-marges'
import { SANS_AUTOCONSOMMATION_PRODUITS_MARGES } from '@/model/performances/methods/sans-autoconsommation-produits-marges'

export const PRODUITS_ET_MARGES: CategoryKind = {
  key: 'PRODUITS_ET_MARGES',
  methods: [
    PRIX_REELS_PRODUITS_MARGES,
    PRIX_STANDARDISES_MILLESIMES_PRODUITS_MARGES,
    AVEC_AUTOCONSOMMATION_PRODUITS_MARGES,
    SANS_AUTOCONSOMMATION_PRODUITS_MARGES
  ],
  indicators: [PRODUIT_BRUT, MARGE_BRUTE, MARGE_SEMI_NETTE, MARGE_DIRECTE],
  active: true,
  indicatorsBySubCategories: {}
} as const
