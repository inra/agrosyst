/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { NOMBRE_INTERVENTIONS_DE_DESHERBAGE_MECANIQUE } from '@/model/performances/indicators/strategie-agronomique/nombre-interventions-de-desherbage-mecanique'
import { NOMBRE_INTERVENTIONS_DE_LABOUR } from '@/model/performances/indicators/strategie-agronomique/nombre-interventions-de-labour'
import { NOMBRE_INTERVENTIONS_DE_TRAVAIL_DU_SOL_HORS_LABOUR } from '@/model/performances/indicators/strategie-agronomique/nombre-interventions-de-travail-du-sol-hors-labour'
import { RECOURS_AU_DESHERBAGE_MECANIQUE } from '@/model/performances/indicators/strategie-agronomique/recours-au-desherbage-mecanique'
import { TYPE_DE_TRAVAIL_DU_SOL } from '@/model/performances/indicators/strategie-agronomique/type-de-travail-du-sol'

export const STRATEGIE_AGRONOMIQUE: CategoryKind = {
  key: 'STRATEGIE_AGRONOMIQUE',
  methods: [],
  indicators: [
    TYPE_DE_TRAVAIL_DU_SOL,
    NOMBRE_INTERVENTIONS_DE_LABOUR,
    NOMBRE_INTERVENTIONS_DE_TRAVAIL_DU_SOL_HORS_LABOUR,
    RECOURS_AU_DESHERBAGE_MECANIQUE,
    NOMBRE_INTERVENTIONS_DE_DESHERBAGE_MECANIQUE
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
