/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { CHARGES } from '@/model/performances/categories/charges'
import { FERTILISATION_AUTRE } from '@/model/performances/categories/fertilisation-autre'
import { FERTILISATION_NPK } from '@/model/performances/categories/fertilisation-npk'
import { IFT } from '@/model/performances/categories/ift'
import { PERFORMANCE_SOCIALE_ET_CONSOMMATION } from '@/model/performances/categories/performance-social-et-consommation'
import { PRODUITS_ET_MARGES } from '@/model/performances/categories/produits-et-marges'
import { RECOURS_AUX_MOYENS_BIOLOGIQUES } from '@/model/performances/categories/recours-aux-moyens-biologiques'
import { STRATEGIE_AGRONOMIQUE } from '@/model/performances/categories/strategie-agronomique'
import { SUBSTANCES_ACTIVES } from '@/model/performances/categories/substances-actives'
import { SUBSTANCES_ACTIVES_SPECIFIQUES } from '@/model/performances/categories/substances-actives-specifiques'
import { TEMPS_DE_TRAVAIL } from '@/model/performances/categories/temps-de-travail'

export const Categories = {
  IFT,
  SUBSTANCES_ACTIVES,
  SUBSTANCES_ACTIVES_SPECIFIQUES,
  RECOURS_AUX_MOYENS_BIOLOGIQUES,
  PRODUITS_ET_MARGES,
  CHARGES,
  STRATEGIE_AGRONOMIQUE,
  FERTILISATION_NPK,
  FERTILISATION_AUTRE,
  TEMPS_DE_TRAVAIL,
  PERFORMANCE_SOCIALE_ET_CONSOMMATION
} as const
