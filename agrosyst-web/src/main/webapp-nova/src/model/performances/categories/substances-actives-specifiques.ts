/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'

import { QSA_GLYPHOSATE } from '@/model/performances/indicators/substances-actives/qsa-glyphosate'
import { QSA_S_METOLACHLORE } from '@/model/performances/indicators/substances-actives/qsa-s-metolachlore'
import { QSA_PROSULFOCARBE } from '@/model/performances/indicators/substances-actives/qsa-prosulfocarbe'
import { QSA_CHLORTOLURON } from '@/model/performances/indicators/substances-actives/qsa-chlortoluron'
import { QSA_DIFLUFENICAN } from '@/model/performances/indicators/substances-actives/qsa-diflufenican'
import { QSA_LAMBDA_CYHALOTHRINE } from '@/model/performances/indicators/substances-actives/qsa-lambda-cyhalothrine'
import { QSA_BOSCALID } from '@/model/performances/indicators/substances-actives/qsa-boscalid'
import { QSA_FLUOPYRAM } from '@/model/performances/indicators/substances-actives/qsa-fluopyram'
import { QSA_BIXAFEN } from '@/model/performances/indicators/substances-actives/qsa-bixafen'
import { QSA_DICAMBA } from '@/model/performances/indicators/substances-actives/qsa-dicamba'
import { QSA_MANCOZEB } from '@/model/performances/indicators/substances-actives/qsa-mancozeb'
import { QSA_PHOSMET } from '@/model/performances/indicators/substances-actives/qsa-phosmet'
import { QSA_TEBUCONAZOLE } from '@/model/performances/indicators/substances-actives/qsa-tebuconazole'
import { QSA_DIMETHENAMID_P } from '@/model/performances/indicators/substances-actives/qsa-dimethenamid-p'
import { QSA_PENDIMETHALIN } from '@/model/performances/indicators/substances-actives/qsa-pendimethalin'
import { QSA_FLUFENACET } from '@/model/performances/indicators/substances-actives/qsa-flufenacet'
import { QSA_ACLONIFEN } from '@/model/performances/indicators/substances-actives/qsa-aclonifen'
import { QSA_ISOXABEN } from '@/model/performances/indicators/substances-actives/qsa-isoxaben'
import { QSA_BEFLUTAMID } from '@/model/performances/indicators/substances-actives/qsa-beflutamid'
import { QSA_ISOPROTURON } from '@/model/performances/indicators/substances-actives/qsa-isoproturon'
import { QSA_CLOTHIANIDINE } from '@/model/performances/indicators/substances-actives/qsa-clothianidine'
import { QSA_IMIDACLOPRIDE } from '@/model/performances/indicators/substances-actives/qsa-imidaclopride'
import { QSA_THIAMETHOXAM } from '@/model/performances/indicators/substances-actives/qsa-thiamethoxam'
import { QSA_ACETAMIPRIDE } from '@/model/performances/indicators/substances-actives/qsa-acetamipride'
import { QSA_THIACLOPRIDE } from '@/model/performances/indicators/substances-actives/qsa-thiaclopride'
import { QSA_TRIALLATE } from '@/model/performances/indicators/substances-actives/qsa-triallate'
import { QSA_METSULFURON } from '@/model/performances/indicators/substances-actives/qsa-metsulfuron'
import { QSA_FLORASULAM } from '@/model/performances/indicators/substances-actives/qsa-florasulam'
import { QSA_PICOLINAFEN } from '@/model/performances/indicators/substances-actives/qsa-picolinafen'
import { QSA_PROPOXYCARBAZONE } from '@/model/performances/indicators/substances-actives/qsa-propoxycarbazone'
import { QSA_TRIBENURON } from '@/model/performances/indicators/substances-actives/qsa-tribenuron'
import { QSA_SULFOSULFURON } from '@/model/performances/indicators/substances-actives/qsa-sulfosulfuron'
import { QSA_NEONICOTINOIDES } from '@/model/performances/indicators/substances-actives/qsa-neonicotinoides'
import { QSA_HERBICIDES_RACINAIRES } from '@/model/performances/indicators/substances-actives/qsa-herbicides-racinaires'

import { SAS_AVEC_TRAITEMENTS_DE_SEMENCES } from '@/model/performances/methods/sas_avec-traitements-de-semences'
import { SAS_HORS_TRAITEMENTS_DE_SEMENCES } from '@/model/performances/methods/sas_hors-traitements-de-semences'

export const SUBSTANCES_ACTIVES_SPECIFIQUES: CategoryKind = {
  key: 'SUBSTANCES_ACTIVES_SPECIFIQUES',
  methods: [SAS_AVEC_TRAITEMENTS_DE_SEMENCES, SAS_HORS_TRAITEMENTS_DE_SEMENCES],
  indicators: [
    QSA_GLYPHOSATE,
    QSA_CHLORTOLURON,
    QSA_DIFLUFENICAN,
    QSA_PROSULFOCARBE,
    QSA_S_METOLACHLORE,
    QSA_BOSCALID,
    QSA_FLUOPYRAM,
    QSA_LAMBDA_CYHALOTHRINE,
    QSA_BIXAFEN,
    QSA_DICAMBA,
    QSA_MANCOZEB,
    QSA_PHOSMET,
    QSA_TEBUCONAZOLE,
    QSA_DIMETHENAMID_P,
    QSA_PENDIMETHALIN,
    QSA_FLUFENACET,
    QSA_ACLONIFEN,
    QSA_ISOXABEN,
    QSA_BEFLUTAMID,
    QSA_ISOPROTURON,
    QSA_CLOTHIANIDINE,
    QSA_IMIDACLOPRIDE,
    QSA_THIAMETHOXAM,
    QSA_ACETAMIPRIDE,
    QSA_THIACLOPRIDE,
    QSA_TRIALLATE,
    QSA_METSULFURON,
    QSA_FLORASULAM,
    QSA_PICOLINAFEN,
    QSA_PROPOXYCARBAZONE,
    QSA_TRIBENURON,
    QSA_SULFOSULFURON,
    QSA_NEONICOTINOIDES,
    QSA_HERBICIDES_RACINAIRES
  ],
  active: true,
  indicatorsBySubCategories: {
    "Herbicides": [
      QSA_GLYPHOSATE, QSA_DICAMBA, QSA_HERBICIDES_RACINAIRES, QSA_ACLONIFEN, QSA_BEFLUTAMID, QSA_CHLORTOLURON, QSA_DIFLUFENICAN, QSA_DIMETHENAMID_P, QSA_FLORASULAM, QSA_FLUFENACET,
      QSA_ISOPROTURON, QSA_ISOXABEN, QSA_METSULFURON, QSA_PENDIMETHALIN, QSA_PICOLINAFEN, QSA_PROPOXYCARBAZONE, QSA_PROSULFOCARBE, QSA_S_METOLACHLORE,
      QSA_SULFOSULFURON, QSA_TRIALLATE, QSA_TRIBENURON
    ],
    "Fongicides": [QSA_BOSCALID, QSA_BIXAFEN, QSA_FLUOPYRAM, QSA_MANCOZEB, QSA_TEBUCONAZOLE],
    "Insecticides": [
      QSA_LAMBDA_CYHALOTHRINE, QSA_PHOSMET, QSA_NEONICOTINOIDES, QSA_ACETAMIPRIDE, QSA_CLOTHIANIDINE, QSA_IMIDACLOPRIDE, QSA_THIACLOPRIDE, QSA_THIAMETHOXAM
    ]
  }
} as const
