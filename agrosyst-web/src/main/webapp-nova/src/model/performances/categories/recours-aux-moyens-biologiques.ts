/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { RECOURS_AUX_MACRO_ORGANISMES } from '@/model/performances/indicators/recours-aux-moyens-biologiques/recours-aux-macro-organismes'
import { RECOURS_AUX_MOYENS_BIOLOGIQUES as INDICATOR_RECOURS_AUX_MOYENS_BIOLOGIQUES } from '@/model/performances/indicators/recours-aux-moyens-biologiques/recours-aux-moyens-biologiques'
import { RECOURS_AUX_PRODUITS_ABIOTIQUES_SANS_AMM } from '@/model/performances/indicators/recours-aux-moyens-biologiques/recours-aux-produits-abiotiques-sans-amm'
import { RECOURS_AUX_PRODUITS_BIOTIQUES_SANS_AMM } from '@/model/performances/indicators/recours-aux-moyens-biologiques/recours-aux-produits-biotiques-sans-amm'

export const RECOURS_AUX_MOYENS_BIOLOGIQUES: CategoryKind = {
  key: 'RECOURS_AUX_MOYENS_BIOLOGIQUES',
  methods: [],
  indicators: [
    INDICATOR_RECOURS_AUX_MOYENS_BIOLOGIQUES,
    RECOURS_AUX_MACRO_ORGANISMES,
    RECOURS_AUX_PRODUITS_BIOTIQUES_SANS_AMM,
    RECOURS_AUX_PRODUITS_ABIOTIQUES_SANS_AMM
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
