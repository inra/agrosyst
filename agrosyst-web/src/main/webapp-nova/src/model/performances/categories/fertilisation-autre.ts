/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { B_MINERAL } from '@/model/performances/indicators/fertilisation-autre/b-mineral'
import { CA_MINERAL } from '@/model/performances/indicators/fertilisation-autre/ca-mineral'
import { CAO_ORGANIQUE } from '@/model/performances/indicators/fertilisation-autre/cao-organique'
import { CU_MINERAL } from '@/model/performances/indicators/fertilisation-autre/cu-mineral'
import { FE_MINERAL } from '@/model/performances/indicators/fertilisation-autre/fe-mineral'
import { MGO_MINERAL } from '@/model/performances/indicators/fertilisation-autre/mgo-mineral'
import { MGO_ORGANIQUE } from '@/model/performances/indicators/fertilisation-autre/mgo-organique'
import { MN_MINERAL } from '@/model/performances/indicators/fertilisation-autre/mn-mineral'
import { MO_MINERAL } from '@/model/performances/indicators/fertilisation-autre/mo-mineral'
import { NA2O_MINERAL } from '@/model/performances/indicators/fertilisation-autre/na2o-mineral'
import { S_ORGANIQUE } from '@/model/performances/indicators/fertilisation-autre/s-organique'
import { SO3_MINERAL } from '@/model/performances/indicators/fertilisation-autre/so3-mineral'
import { ZN_MINERAL } from '@/model/performances/indicators/fertilisation-autre/zn-mineral'

export const FERTILISATION_AUTRE: CategoryKind = {
  key: 'FERTILISATION_AUTRE',
  methods: [],
  indicators: [
    CA_MINERAL,
    CAO_ORGANIQUE,
    MGO_MINERAL,
    MGO_ORGANIQUE,
    SO3_MINERAL,
    S_ORGANIQUE,
    B_MINERAL,
    CU_MINERAL,
    FE_MINERAL,
    MN_MINERAL,
    MO_MINERAL,
    NA2O_MINERAL,
    ZN_MINERAL
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
