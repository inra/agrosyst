/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { HRI_1 } from '@/model/performances/indicators/substances-actives/hri-1'
import { HRI_1_G1 } from '@/model/performances/indicators/substances-actives/hri-1-g1'
import { HRI_1_G2 } from '@/model/performances/indicators/substances-actives/hri-1-g2'
import { HRI_1_G3 } from '@/model/performances/indicators/substances-actives/hri-1-g3'
import { HRI_1_G4 } from '@/model/performances/indicators/substances-actives/hri-1-g4'
import { ENVIRONMENTAL_RISK_USES } from '@/model/performances/indicators/substances-actives/environmental-risk-uses'
import { TOXIC_USER_USES } from '@/model/performances/indicators/substances-actives/toxic-user-uses'
import { CMR_USES } from '@/model/performances/indicators/substances-actives/cmr-uses'
import { QSA_CMR } from '@/model/performances/indicators/substances-actives/qsa-cmr'
import { QSA_CUIVRE_FERTILISATION } from '@/model/performances/indicators/substances-actives/qsa-cuivre-fertilisation'
import { QSA_CUIVRE_PHYTOSANITAIRE } from '@/model/performances/indicators/substances-actives/qsa-cuivre-phytosanitaire'
import { QSA_CUIVRE_TOTAL } from '@/model/performances/indicators/substances-actives/qsa-cuivre-total'
import { QSA_DANGER_ENVIRONNEMENT } from '@/model/performances/indicators/substances-actives/qsa-danger-environnement'
import { QSA_SOUFRE_FERTILISATION } from '@/model/performances/indicators/substances-actives/qsa-soufre-fertilisation'
import { QSA_SOUFRE_PHYTOSANITAIRE } from '@/model/performances/indicators/substances-actives/qsa-soufre-phytosanitaire'
import { QSA_SOUFRE_TOTAL } from '@/model/performances/indicators/substances-actives/qsa-soufre-total'
import { QSA_SUBSTANCES_CANDIDATES_SUBSTITUTION } from '@/model/performances/indicators/substances-actives/qsa-substances-candidates-substitution'
import { QSA_SUBSTANCES_FAIBLE_RISQUE } from '@/model/performances/indicators/substances-actives/qsa-substances-faible-risque'
import { QSA_TOTALE } from '@/model/performances/indicators/substances-actives/qsa-totale'
import { QSA_TOXIQUES_UTILISATEUR } from '@/model/performances/indicators/substances-actives/qsa-toxiques-utilisateur'

import { SA_AVEC_TRAITEMENTS_DE_SEMENCES } from '@/model/performances/methods/sa_avec-traitements-de-semences'
import { SA_HORS_TRAITEMENTS_DE_SEMENCES } from '@/model/performances/methods/sa_hors-traitements-de-semences'

export const SUBSTANCES_ACTIVES: CategoryKind = {
  key: 'SUBSTANCES_ACTIVES',
  methods: [SA_AVEC_TRAITEMENTS_DE_SEMENCES, SA_HORS_TRAITEMENTS_DE_SEMENCES],
  indicators: [
    QSA_TOTALE,
    QSA_DANGER_ENVIRONNEMENT,
    QSA_TOXIQUES_UTILISATEUR,
    QSA_CMR,
    QSA_SUBSTANCES_CANDIDATES_SUBSTITUTION,
    QSA_SUBSTANCES_FAIBLE_RISQUE,
    QSA_CUIVRE_TOTAL,
    QSA_CUIVRE_PHYTOSANITAIRE,
    QSA_CUIVRE_FERTILISATION,
    QSA_SOUFRE_TOTAL,
    QSA_SOUFRE_PHYTOSANITAIRE,
    QSA_SOUFRE_FERTILISATION,
    ENVIRONMENTAL_RISK_USES,
    TOXIC_USER_USES,
    CMR_USES,
    HRI_1,
    HRI_1_G1,
    HRI_1_G2,
    HRI_1_G3,
    HRI_1_G4
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
