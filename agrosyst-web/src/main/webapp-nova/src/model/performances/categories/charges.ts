/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import type { CategoryKind } from '@/model/performances/category-kind'
import { CHARGES_DE_MAIN_OEUVRE_MANUELLE } from '@/model/performances/indicators/charges/charges-de-main-oeuvre-manuelle'
import { CHARGES_DE_MAIN_OEUVRE_TOTALES } from '@/model/performances/indicators/charges/charges-de-main-oeuvre-totales'
import { CHARGES_DE_MAIN_OEUVRE_TRACTORISTE } from '@/model/performances/indicators/charges/charges-de-main-oeuvre-tractoriste'
import { CHARGES_DE_MECANISATION } from '@/model/performances/indicators/charges/charges-de-mecanisation'
import { CHARGES_OPERATIONELLES_AUTRES } from '@/model/performances/indicators/charges/charges-operationnelles-autres'
import { CHARGES_OPERATIONELLES_EPANDAGE_ORGANIQUE } from '@/model/performances/indicators/charges/charges-operationnelles-epandage-organique'
import { CHARGES_OPERATIONELLES_FERTILISATION_MINERALE } from '@/model/performances/indicators/charges/charges-operationnelles-fertilisation-minerale'
import { CHARGES_OPERATIONELLES_IRRIGATION } from '@/model/performances/indicators/charges/charges-operationnelles-irrigation'
import { CHARGES_OPERATIONELLES_PHYTO_AVEC_AMM } from '@/model/performances/indicators/charges/charges-operationnelles-phyto-avec-amm'
import { CHARGES_OPERATIONELLES_PHYTO_SANS_AMM } from '@/model/performances/indicators/charges/charges-operationnelles-phyto-sans-amm'
import { CHARGES_OPERATIONELLES_POTS } from '@/model/performances/indicators/charges/charges-operationnelles-pots'
import { CHARGES_OPERATIONELLES_SEMIS } from '@/model/performances/indicators/charges/charges-operationnelles-semis'
import { CHARGES_OPERATIONELLES_SUBSTRATS } from '@/model/performances/indicators/charges/charges-operationnelles-substrats'
import { CHARGES_OPERATIONELLES_TOTALES } from '@/model/performances/indicators/charges/charges-operationnelles-totales'
import { CHARGES_OPERATIONELLES_TRAITEMENTS_DE_SEMENCE } from '@/model/performances/indicators/charges/charges-operationnelles-traitements-de-semence'

import { PRIX_REELS_CHARGES } from '@/model/performances/methods/prix-reels-charges'
import { PRIX_STANDARDISES_MILLESIMES_CHARGES } from '@/model/performances/methods/prix-standardises-millesimes-charges'

export const CHARGES: CategoryKind = {
  key: 'CHARGES',
  methods: [PRIX_REELS_CHARGES, PRIX_STANDARDISES_MILLESIMES_CHARGES],
  indicators: [
    CHARGES_OPERATIONELLES_TOTALES,
    CHARGES_OPERATIONELLES_SEMIS,
    CHARGES_OPERATIONELLES_FERTILISATION_MINERALE,
    CHARGES_OPERATIONELLES_EPANDAGE_ORGANIQUE,
    CHARGES_OPERATIONELLES_PHYTO_AVEC_AMM,
    CHARGES_OPERATIONELLES_PHYTO_SANS_AMM,
    CHARGES_OPERATIONELLES_TRAITEMENTS_DE_SEMENCE,
    CHARGES_OPERATIONELLES_IRRIGATION,
    CHARGES_OPERATIONELLES_SUBSTRATS,
    CHARGES_OPERATIONELLES_POTS,
    CHARGES_OPERATIONELLES_AUTRES,
    CHARGES_DE_MECANISATION,
    CHARGES_DE_MAIN_OEUVRE_TOTALES,
    CHARGES_DE_MAIN_OEUVRE_TRACTORISTE,
    CHARGES_DE_MAIN_OEUVRE_MANUELLE
  ],
  active: true,
  indicatorsBySubCategories: {}
} as const
