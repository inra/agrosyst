/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import { reduceByKey } from '@/model/common/kind'
import { Categories } from '@/model/performances/categories'
import type { CategoryKind } from '@/model/performances/category-kind'

export const Indicators = {
  ...Object.values(Categories).reduce(
    (acc: any, curr: CategoryKind) => ({ ...acc, ...reduceByKey(curr.indicators) }),
    {}
  )
} as const
