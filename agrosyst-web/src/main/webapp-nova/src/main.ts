/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import '@/assets/main.css'

import App from '@/App.vue'
import { setupI18n } from '@/i18n'
import fr from '@/locales/fr.json'
import { setupRouter } from '@/router'
import { createApp } from 'vue'
import VueCookies from 'vue-cookies'

const i18n = setupI18n({
  legacy: false,
  locale: 'fr',
  fallbackLocale: 'fr',
  globalInjection: true,
  messages: {
    fr
  },
  datetimeFormats: {
    fr: {
      short: {
        hour: 'numeric',
        minute: 'numeric'
      }
    }
  }
})
const router = setupRouter(i18n)
const app = createApp(App)

app.use(i18n)
app.use(router)
app.use(VueCookies)

app.mount('#app')
