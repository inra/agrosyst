<#--
 #%L
 Agrosyst :: Web
 $Id$
 $HeadURL$
 %%
 Copyright (C) 2013 - 2014 INRA
 %%
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/gpl-3.0.html>.
 #L%
-->
<#if fieldErrors??><#t/>
    <#assign eKeys = fieldErrors.keySet()><#t/>
    <#assign eKeysSize = eKeys.size()><#t/>
    <#assign doneStartUlTag=false><#t/>
    <#assign doneEndUlTag=false><#t/>
    <#assign haveMatchedErrorField=false><#t/>
    <#if (fieldErrorFieldNames?size > 0) ><#t/>
        <#list fieldErrorFieldNames as fieldErrorFieldName><#t/>
            <#list eKeys as eKey><#t/>
                <#if (eKey = fieldErrorFieldName)><#t/>
                    <#assign haveMatchedErrorField=true><#t/>
                    <#assign eValue = fieldErrors[fieldErrorFieldName]><#t/>
                    <#if (haveMatchedErrorField && (!doneStartUlTag))><#t/>
                    <div class="wwerr clearfix">
                        <div class="messages-panel"><ul <#rt/>
                        <#if parameters.id?if_exists != "">
                                id="${parameters.id}"<#rt/>
                        </#if>
                        <#if parameters.cssClass??>
                                class="${parameters.cssClass}"<#rt/>
                            <#else>
                                class="errorMessage"<#rt/>
                        </#if>
                        <#if parameters.cssStyle??>
                                style="${parameters.cssStyle}"<#rt/>
                        </#if>
                            >
                        <#assign doneStartUlTag=true><#t/>
                    </#if><#t/>
                    <#list eValue as eEachValue><#t/>
                        <li><#if parameters.escape>${eEachValue!}<#else>${eEachValue!}</#if></li>
                    </#list><#t/>
                </#if><#t/>
            </#list><#t/>
        </#list><#t/>
        <#if (haveMatchedErrorField && (!doneEndUlTag))><#t/>
        </ul></div></div>
            <#assign doneEndUlTag=true><#t/>
        </#if><#t/>
        <#else><#t/>
        <#if (eKeysSize > 0)><#t/>
        <div class="messages-panel"><ul<#rt/>
            <#if parameters.cssClass??>
                    class="${parameters.cssClass}"<#rt/>
                <#else>
                    class="errorMessage"<#rt/>
            </#if>
            <#if parameters.cssStyle??>
                    style="${parameters.cssStyle}"<#rt/>
            </#if>
                >
            <#list eKeys as eKey><#t/>
                <#assign eValue = fieldErrors[eKey]><#t/>
                <#list eValue as eEachValue><#t/>
                    <li><span><#if parameters.escape>${eEachValue!}<#else>${eEachValue!}</#if></span></li>
                </#list><#t/>
            </#list><#t/>
        </ul></div>
        </#if><#t/>
    </#if><#t/>
</#if><#t/>
