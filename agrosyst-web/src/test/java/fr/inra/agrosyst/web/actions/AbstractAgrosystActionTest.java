package fr.inra.agrosyst.web.actions;

/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2019 INRA
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.context.NavigationContextServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.Serial;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Quelques test sur du code présent dans les action struts.
 *
 * @author chatellier
 */
public class AbstractAgrosystActionTest {

    protected AbstractAgrosystAction action;
    
    public void setupServices() {
        action = new AbstractAgrosystAction() {
            @Serial
            private static final long serialVersionUID = 8212371013941564113L;
        };

        action.setNavigationContextService(new NavigationContextServiceImpl());
    }

    /**
     * Test que les integers sont bien écrit sans quotes.
     */
    @Test
    public void testJsonIntegerTypes() {
        setupServices();
        Map<Integer, String> regions = new HashMap<>();
        regions.put(53, "Bretagne");
        String result = action.toJson(regions);
        Assertions.assertTrue(result.contains("53"));

        Map<String, Integer> regions2 = new HashMap<>();
        regions2.put("Bretagne", 53);
        String result2 = action.toJson(regions2);
        Assertions.assertTrue(result2.contains("53"));
    }

    @Test
    public void testFormatParseLocalDateTime() {
        setupServices();
        LocalDateTime now = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        String str = now.format(formatter);
        // On simule le passage par une URL (cas du broadcast au Login)
        String encoded = URLEncoder.encode(str, StandardCharsets.UTF_8);
        String decoded = URLDecoder.decode(encoded, StandardCharsets.UTF_8);
        LocalDateTime dateTime = LocalDateTime.parse(decoded, formatter);

        Assertions.assertEquals(now, dateTime);
    }

}
