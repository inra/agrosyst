package fr.inra.agrosyst.web.actions.security;

/*-
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.ImmutableAuthenticatedUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class JwtHelperTest {

    protected JwtHelper jwtHelper;

    @BeforeEach
    public void beforeEachTest() {
        jwtHelper = new JwtHelper("lksdjfskdjbnfkdsjhfsdlkrfj", 10);
    }

    @Test
    public void testWriteReadTokenMinimal() {
        AuthenticatedUser authenticatedUser = ImmutableAuthenticatedUser.builder()
                .topiaId("toto")
                .firstName("to")
                .lastName("tu")
                .email("azerty@lili.go")
                .isAcceptedCharter(false)
                .sid("whatever")
                .roles(ImmutableSet.of(RoleType.DOMAIN_RESPONSIBLE, RoleType.GS_DATA_PROCESSOR))
                .language(Language.FRENCH)
                .build();

        // D'abord on créé le token JWT à partir du AuthenticatedUser
        String token = jwtHelper.createJwtToken(authenticatedUser);

        // Puis on le vérifie
        AuthenticatedUser verified = jwtHelper.verifyJwtToken(token);

        // Et enfin on compare l'objet source et l'objet vérifié
        Assertions.assertEquals(authenticatedUser, verified);
    }

    @Test
    public void testWriteReadTokenFull() {
        AuthenticatedUser authenticatedUser = ImmutableAuthenticatedUser.builder()
                .topiaId("toto")
                .firstName("to")
                .lastName("tu")
                .email("azerty@lili.go")
                .isAcceptedCharter(true)
                .sid("whatever")
                .roles(ImmutableSet.of(RoleType.DOMAIN_RESPONSIBLE, RoleType.GS_DATA_PROCESSOR))
                .banner("plkmsdsdlkmf")
                .itEmail("un-ti-mail@oups.fr")
                .language(Language.FRENCH)
                .build();

        // D'abord on créé le token JWT à partir du AuthenticatedUser
        String token = jwtHelper.createJwtToken(authenticatedUser);

        // Puis on le vérifie
        AuthenticatedUser verified = jwtHelper.verifyJwtToken(token);

        // Et enfin on compare l'objet source et l'objet vérifié
        Assertions.assertEquals(authenticatedUser, verified);
    }

}
