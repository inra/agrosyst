/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Widget pour faire les diagram pluriannuel
 *
 * usage: <div id="toto"></div><script>$(function(){$("#toto").cropCycleDiagram(options)})</script>
 *
 * Les options possibles sont:
 * data: {nodes: [], connections: []},
 * models: [],
 * nodeTemplate: '<div id="%{nodeId}s" class="%{classes}s"><span class="name">%{label}s</span><div class="ep"></div></div>',
 * trash: true,
 * columnWidth: 170,
 * endpointsRadius: 6
 *
 * data peut-etre une chaine json, on un objet
 *
 * trash peut-être une chaine de caractère contenant du code HTML, ou un sélecteur
 * pour représenter Trash. false, n'affiche pas de Trash
 *
 * Pour mettre à jour les data vous pouvez faire:
 *   $("#toto").cropCycleDiagram("data", {...});
 *
 */
jQuery.widget("agrosyst.cropCycleDiagram", {
    options: {
        data: {nodes: [], connections: []},
        models: [],
        nodeTemplate: '<div id="%{nodeId}s" class="%{classes}s"><span class="name">%{label}s</span><div class="ep"></div></div>',
        trash: true,
        columnWidth: 170,
        endpointsRadius: 6
    },
    _create: function() {
        var diagram = this;
        this.element.addClass("diagram");

        this.trashElem = jQuery("<div class='trash'></div>").appendTo(this.element);
        this.modelsElem = jQuery("<div class='models'></div>").appendTo(this.element);
        this.columnsElem = jQuery("<div class='columns'></div>").css("width", "100%").appendTo(this.element);

        this.jsPlumb = jsPlumb.getInstance();
        this.jsPlumb.importDefaults({
            // default drag options
            DragOptions: {cursor: 'pointer', zIndex: 2000},
            Endpoints: [["Dot", {radius: this.options.endpointsRadius}], ["Dot", {radius: this.options.endpointsRadius}]],
            ConnectionsDetachable: false
        });

// detecte l'ajout dune connexion et change sa couleur
        this.jsPlumb.bind("connection", function(conn) {
            conn.connection.setPaintStyle(diagram._getConnectionStyle());
            diagram._addConnection(conn.sourceId, conn.targetId);
        });

// detecte la suppression d'une connexion qui peut provenir de la suppression
// de la connection ou d'un noeud
        this.jsPlumb.bind("connectionDetached", function(conn) {
            diagram._removeConnection(conn.sourceId, conn.targetId);
        });

// detecte le click sur une connexion et la supprime
        this.jsPlumb.bind("click", function(conn) {
            // suppression du lien clique
            jsPlumb.detach(conn);
        });

// permet d'interdire de mettre 2 fois la meme connexion entre les memes noeuds
        this.jsPlumb.bind("beforeDrop", function(info) {
            var result = !diagram._containsConnection(info.sourceId, info.targetId);
            return result;
        });


        this.curColourIndex = 1;
        this.maxColourIndex = 24;



        this._addColumn(this.columnsElem, 1);
        this._addTrash(this.trashElem, this.options.trash);
        this._addModel(this.modelsElem, this.options.models);

        this.data(this.options.data);
    },
    _setOption: function(key, value) {
        this._super(key, value);
        if (key === "trash") {
            this._addTrash(this.trashElem, this.options.trash);
        } else if (key === "models") {
            this._addModel(this.modelsElem, this.options.models);
        } else {
            this._redraw();
        }
    },
    _setOptions: function(options) {
        this._super(options);
        this.redraw();
    },
    _destroy: function() {
        this.element
                .removeClass("diagram")
                .empty();
    },
    data: function(value) {
        if (value) {
            if (typeof value === "string") {
                value = JSON.parse(value);
            }
            this.options.data = {};
            jQuery.extend(this.options.data, value);

            this.redraw();
        } else {
            return this.options.data;
        }
    },
    _templateNode: function(data) {
        return this._format(this.options.nodeTemplate, data);
    },
    _getNodeId: function(data) {
        var result = data && data.nodeId || "new-node-" + this._generateUUID();
        return result;
    },
    /**
     * Format la chaine avec les info de data. Si data est un objet il faut que dans
     * le template on retrouve des %{fieldname}s. Si data est un tableau
     * les %s sont convertie dans l'ordre ou on les trouves
     * @param {String} template
     * @param {Object or Array} data
     * @returns {String}
     */
    _format: function(template, data) {
        var index = 0;
        var result = template.replace(/%\{(.*?)\}s/g, function(match, field) {
            return data[field] || data[index++] || "";
        });
        return result;
    },
    /**
     * Genere un UUID
     * @returns {String}
     */
    _generateUUID: function() {
        var d = Date.now();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x7 | 0x8)).toString(16);
        });
        return uuid;
    },
    /**
     * Return node with nodeId or undefined if not found
     * @param {String} nodeId
     * @returns {NodeData or undefined}
     */
    _getNodeData: function(nodeId) {
        var result;
        for (var i in this.options.data.nodes) {
            if (this.options.data.nodes[i].nodeId === nodeId) {
                result = this.options.data.nodes[i];
                break;
            }
        }
        return result;
    },
    /**
     * Retourne tous les noeuds dans l'ordre
     * @returns {Array} all nodes ordered by position x,y
     */
    _getNodes: function() {
        var result = this.options.data.nodes;
        result.sort(function(a, b) {
            var v;
            if (a.x < b.x) {
                v = -1;
            } else if (a.x > b.x) {
                v = 1;
            } else {
                if (a.y < b.y) {
                    v = -1;
                } else if (a.y > b.y) {
                    v = 1;
                } else {
                    v = 0;
                }
            }
            return v;
        });
        return result;
    },
    /**
     * Ajout d'un nouveau noeud. data est directement utilise sans faire de copie
     * nodeId est ajoute au data. Si le noeud existait deja, les donnees actuelles
     * sont remplacer par les donnees en argument
     * @param {String} nodeId
     * @param {Object} data
     * @returns {undefined}
     */
    _addNode: function(nodeId, data) {
        this._getNodeData(nodeId) && this._removeNode(nodeId); // prevent already present node
        data.nodeId = nodeId;
        this.options.data.nodes.push(data);
    },
    /**
     * Met a jour les data d'un noeud avec les valeurs passees dans data, si le noeud
     * n'existe pas rien n'est fait
     * @param {String} nodeId
     * @param {Object} data
     * @returns {undefined}
     */
    _updateNode: function(nodeId, data) {
        var node = this._getNodeData(nodeId);
        node && jQuery.extend(node, data);
    },
    /**
     * Supprime un noeud
     * @param {String} nodeId
     * @returns {undefined}
     */
    _removeNode: function(nodeId) {
        this.options.data.nodes = this.options.data.nodes.filter(function(e, i, array) {
            var result = e.nodeId !== nodeId;
            return result;
        });

        this.options.data.connections = this.options.data.connections.filter(function(e, i, array) {
            // on retient l'element si result est vrai
            var result = !(e.sourceId === nodeId || e.targetId === nodeId);
            return result;
        });
    },
    _addConnection: function(sourceId, targetId) {
        // on ajoute que si elle n'existe pas deja
        this._containsConnection(sourceId, targetId) || this.options.data.connections.push({sourceId: sourceId, targetId: targetId});
    },
    _removeConnection: function(sourceId, targetId) {
        this.options.data.connections = this.options.data.connections.filter(function(e, i, array) {
            var result = !(e.sourceId === sourceId && e.targetId === targetId);
            return result;
        });
    },
    /**
     * retourne vrai si cette connexion existe deja
     */
    _containsConnection: function(sourceId, targetId) {
        var result = this.options.data.connections.some(function(element, index, array) {
            return element.sourceId === sourceId && element.targetId === targetId;
        });
        return result;
    },
// helper method to generate a color from a cycle of colors.
    _nextColour: function() {
        var R, G, B;
        R = parseInt(128 + Math.sin((this.curColourIndex * 3 + 0) * 1.3) * 128);
        G = parseInt(128 + Math.sin((this.curColourIndex * 3 + 1) * 1.3) * 128);
        B = parseInt(128 + Math.sin((this.curColourIndex * 3 + 2) * 1.3) * 128);
        this.curColourIndex = this.curColourIndex + 1;
        if (this.curColourIndex > this.maxColourIndex)
            this.curColourIndex = 1;
        return "rgb(" + R + "," + G + "," + B + ")";
    },
// this is the paint style for the connecting lines
    _getConnectionStyle: function() {
        var connectorPaintStyle = {
            lineWidth: 2,
            strokeStyle: "#deea18",
            joinstyle: "round",
            outlineColor: "#EAEDEF",
            outlineWidth: 1
        };
        connectorPaintStyle.strokeStyle = this._nextColour();
        return connectorPaintStyle;
    },
    /**
     * Genere un element HTML pret a etre ajouter au DOM
     * @param {Object} modelData toutes les infos pour creer le noeud
     *  (au minimum: nodeId, classes, label)
     * @returns {DOM Node}
     */
    _makeNode: function(modelData) {
        var node = $(this._templateNode(modelData));

        this.jsPlumb.makeSource(node, {
            filter: ".ep",
            anchor: "RightMiddle",
            connector: ["Straight"], // ["StateMachine", { curviness: 0 }], //["Flowchart", {stub: [40, 60], gap: 6, cornerRadius: 5, alwaysRespectStubs: false}],
            connectorStyle: this._getConnectionStyle()
        });

        this.jsPlumb.makeTarget(node, {
            dropOptions: {hoverClass: "dragHover"},
            anchor: "LeftMiddle"
        });
        return node;
    },
    /**
     * Remet a jour tous les liens qui sont dans la colonne source et destination
     * @param {Element} source
     * @param {Element} dest
     * @param {String} eventName
     * @returns {undefined}
     */
    _refresh: function(source, dest, eventName) {
        var all = $(source).add(dest);
        all = all.children(".node:not(.ui-sortable-placeholder)");
        var diagram = this;
        all.each(function(i, e) {
            var item = $(e);
            var nodeId = item.attr("id");
            if (nodeId) {
                diagram.jsPlumb.repaint(item);

                var x = item.parent().data("index");
                var y = item.index();
                diagram._updateNode(nodeId, {x: x, y: y});
            }
        });
    },
    /**
     * Ajoute autant de colonne que besoin pour satisfaire min. Si le nombre
     * de colonne convient deja, rien n'est fait
     * @param {JQuery Element} parent
     * @param {Number} minColumn le nombre mini de colonne que doit avoir parent
     * @returns {undefined}
     */
    _addColumn: function(parent, minColumn) {
        var diagram = this;
        if (typeof minColumn !== "number") {
            // minColumn is column element it self
            // we must add one column if minColumn is last, and index is 0 based
            minColumn = $(minColumn).data("index") + 2;
        }
        var columnNumber = parent.children(".column").length;
        while (columnNumber < minColumn) {
            columnNumber++;
            $(parent).css("min-width", (columnNumber * this.options.columnWidth + 20) + "px");
            var column = $('<div class="column"></div>')
                    .css({width: this.options.columnWidth, float: "left"})
                    .appendTo(parent);
            column.data("index", columnNumber - 1); // to have 0 based index
            column.sortable({
                connectWith: ".column",
                receive: function(event, ui) {
                    diagram._addColumn(parent, this);
                    diagram._refresh(this, ui.sender, "receive");
                },
                // TODO, faire le tri dans les evenements reellement util pour le refresh (a priori stop est necessaire)
                activate: function(event, ui) {
                    diagram._refresh(this, ui.sender, "activate");
                },
                beforeStop: function(event, ui) {
                    diagram._refresh(this, ui.sender, "beforeStop");
                },
                change: function(event, ui) {
                    diagram._refresh(this, ui.sender, "change");
                },
                deactivate: function(event, ui) {
                    diagram._refresh(this, ui.sender, "deactivate");
                },
                out: function(event, ui) {
                    diagram._refresh(this, ui.sender, "out");
                },
                over: function(event, ui) {
                    diagram._refresh(this, ui.sender, "over");
                },
                remove: function(event, ui) {
                    diagram._refresh(this, ui.sender, "remove");
                },
                sort: function(event, ui) {
                    diagram._refresh(this, ui.sender, "sort");
                },
                start: function(event, ui) {
                    diagram._refresh(this, ui.sender, "start");
                },
                stop: function(event, ui) {
                    diagram._refresh(this, ui.sender, "stop");
                },
                update: function(event, ui) {
                    diagram._refresh(this, ui.sender, "update");
                }
            });
            column.disableSelection();

            column.droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ":not(.ui-sortable-helper)",
                drop: function(event, ui) {
                    var x = $(this).data("index");
                    var y = $(this).children().length;
                    // si on a ajoute a la derniere colonne, on en cree une nouvelle
                    diagram._addColumn(parent, this);

                    var nodeId = diagram._getNodeId();
                    var model = $(ui.draggable);
                    var clone = diagram._makeNode(jQuery.extend({nodeId: nodeId, classes: "node"}, model.data("info")));
                    clone.appendTo(this);
                    var data = jQuery.extend({}, {x: x, y: y}, model.data("info"));
                    diagram._addNode(nodeId, data);
                }
            });
        }
    },
    _addTrash: function(trashElem, trashOption) {
        var diagram = this;
        trashElem.empty();
        var trash;
        if (trashOption === true) {
            trash = "<span>Trash</span>";
        } else {
            trash = trashOption;
        }
        if (trash) {
            trash = jQuery(trash).appendTo(trashElem);
            trash.droppable({
                accept: ".node",
                activeClass: "ui-state-highlight",
                drop: function(event, ui) {
                    var item = $(ui.draggable);
                    var parent = item.parent();
                    item.animate({width: 0, height: 0}, function() {
                        diagram.jsPlumb.remove(item);
                        diagram._refresh(parent);
                        diagram._removeNode(item.attr("id"));
                    });
                }
            });
        }
    },
    _addModel: function(parent, models) {
        parent.empty();
        for (var m in models) {
            var data = jQuery.extend({classes: "model"}, models[m]);
            var elem = $(this._templateNode(data));
            elem.data("info", models[m]);

            elem.draggable({
                appendTo: "body",
                helper: "clone"
            });

            elem.appendTo(parent);
        }
    },
    redraw: function() {
        var diagram = this;
        // clean all before redraw
        $(".columns .column .node").each(function(e) {
            diagram.remove(e);
        });

        var nodes = this._getNodes();

        // restauration des noeuds
        for (var n in nodes) {
            var node = nodes[n];
            var nodeElem = this._makeNode(jQuery.extend({classes: "node"}, node));
            var x = node.x || 0;
            this._addColumn(this.columnsElem, x + 2); // ensure capacity
            var column = $(".column", this.columnsElem).eq(x);
            column.append(nodeElem);
        }

        // restauration des connexions
        var connections = this.options.data.connections;
        for (var c in connections) {
            var conn = connections[c];
            this.jsPlumb.connect({source: conn.sourceId, target: conn.targetId});
        }
    }

});

