/*
 * #%L
 * Agrosyst :: Web
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Widget pour faire les diagram des reseaux
 *
 * usage: <div id="toto"></div><script>$(function(){$("#toto").networkDiagram(options)})</script>
 *
 * Les options possibles sont:
 * data: {networks: [], connections: []},
 * networkTemplate: '<span id="%{id}s" class="network">%{label}s</span>',
 * endpointsRadius: 4
 *
 * data peut-etre une chaine json, on un objet
 *
 * Pour mettre à jour les data vous pouvez faire:
 *   $("#toto").networkDiagram("data", {...});
 *
 */
jQuery.widget("agrosyst.networkDiagram", {
    options: {
        data: {networks: [], connections: []},
        networkTemplate: '<span id="%{id}s" class="network">%{label}s</span>',
        endpointsRadius: 4
    },
    _create: function() {
        this.element.addClass("networkDiagram");

        this.jsPlumb = jsPlumb.getInstance();
        this.jsPlumb.importDefaults({
            // default drag options
            Endpoints: [["Dot", {radius: this.options.endpointsRadius}], ["Dot", {radius: this.options.endpointsRadius}]],
            ConnectionsDetachable: false,
            Anchors: ["TopCenter", "BottomCenter"],
            Connector: ["Straight"],
            PaintStyle: {
                    lineWidth: 2,
                    joinstyle: "round",
                    outlineColor: "#EAEDEF",
                    outlineWidth: 1,
                    strokeStyle: "blue"
                }
        });
        this.jsPlumb.Defaults.Container = this.element;

        this.data(this.options.data);
    },
    _setOption: function(key, value) {
        this._super(key, value);
        this._redraw();
    },
    _setOptions: function(options) {
        this._super(options);
        this.redraw();
    },
    _destroy: function() {
        this.element
                .removeClass("networkDiagram")
                .empty();
    },
    data: function(value) {
        if (value) {
            if (typeof value === "string") {
                value = JSON.parse(value);
            }
            this.options.data = {};
            jQuery.extend(this.options.data, value);

            this.redraw();
        } else {
            return this.options.data;
        }
    },
    /**
     * Format la chaine avec les info de data. Si data est un objet il faut que dans
     * le template on retrouve des %{fieldname}s. Si data est un tableau
     * les %s sont convertie dans l'ordre ou on les trouves
     * @param {String} template
     * @param {Object or Array} data
     * @returns {String}
     */
    _format: function(template, data) {
        var index = 0;
        var result = template.replace(/%\{(.*?)\}s/g, function(match, field) {
            return data[field] || data[index++] || "";
        });
        return result;
    },
    /**
     * Genere un element HTML pret a etre ajouter au DOM
     * @param {Object} modelData toutes les infos pour creer le noeud
     *  (au minimum: nodeId, classes, label)
     * @returns {DOM Node}
     */
    _makeNetwork: function(modelData) {
        var network = $(this._format(this.options.networkTemplate, modelData));
        return network;
    },
    redraw: function() {
        // clean all before redraw
        $(this.element).empty();

        var networks = this.options.data.networks;

        // restauration des noeuds
        for (var r in networks) {
            var row = $("<div class='row r"+r+"'></div>").appendTo(this.element);
            for (var n in networks[r]) {
                var network = networks[r][n];
                var networkElem = this._makeNetwork(network);
                row.append(networkElem);
            }
        }

        // restauration des connexions
        var connections = this.options.data.connections;
        for (var c in connections) {
            var conn = connections[c];
            this.jsPlumb.connect({source: conn.sourceId, target: conn.targetId});
        }
    }

});

