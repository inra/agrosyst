Il s'agit de l'interface web de Agrosyst. Elle fait appel aux services.

Le module produit un WAR prêt à être déployé dans un tomcat ou équivalent.

Si l'application est déployée sur JETTY, il est nécessaire de modifier la valeur de l'attribut "maxFormContentSize" pour lui assigner une valeur supérieur à celle par défaut (200000)
La valeur 5000000 fonctionne.

Voir la documentation suivante:
http://www.eclipse.org/jetty/documentation/current/setting-form-size.html