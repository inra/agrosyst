Il s'agit du module qui encapsule les comportements métiers du projet. Il
permet aux applications finales (batch, interface Web) d'obtenir et de
modifier les données via des interfaces haut niveau "métier" .
