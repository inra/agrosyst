package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;

public class RefVarietePlantGrapeTopiaDaoTest extends AbstractAgrosystTest {

    private RefVarietePlantGrapeTopiaDao refVarietePlantGrapesDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        loginAsAdmin();
        refVarietePlantGrapesDao = getPersistenceContext().getRefVarietePlantGrapeDao();
        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.importVarietePlantGrape();
    }

    @Test
    public void testFindCodeGnisIgnoreSpace() {
        RefVarietePlantGrape testSpace = refVarietePlantGrapesDao.findAnyOrNullForCodeGnisEqualsIgnoreSpace("Testspace");
        Assertions.assertNotNull(testSpace);
        Assertions.assertEquals("Test space", testSpace.getCode_gnis());

        RefVarietePlantGrape testSpaces = refVarietePlantGrapesDao.findAnyOrNullForCodeGnisEqualsIgnoreSpace("Testseveralspaces");
        Assertions.assertNotNull(testSpaces);
        Assertions.assertEquals("Test several spaces", testSpaces.getCode_gnis());

        RefVarietePlantGrape testNoSpace = refVarietePlantGrapesDao.findAnyOrNullForCodeGnisEqualsIgnoreSpace("TestNoSpace");
        Assertions.assertNotNull(testNoSpace);
        Assertions.assertEquals("TestNoSpace", testNoSpace.getCode_gnis());
    }
}
