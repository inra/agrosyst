package fr.inra.agrosyst.api.entities;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;

/**
 * Cette classe effectue des tests sur les contraintes de generation du modèle.
 * 
 * @author Eric Chatellier
 */
public class AgrosystModelTest {
    private static final Log LOGGER = LogFactory.getLog(AgrosystModelTest.class);

    /**
     * Test que les contraintes natural id sont présentes dans le fichier agrosyst.properties.
     * @throws IOException 
     */
    @Test
    public void testReferentialNaturalId() throws IOException {

        for (AgrosystEntityEnum entityEnum : AgrosystEntityEnum.values()) {
            Class<? extends TopiaEntity> entityClass = entityEnum.getImplementation();

            if (ReferentialEntity.class.isAssignableFrom(entityClass)) {

                // Can't define natural id on inheritance
                if (Modifier.isAbstract(entityClass.getModifiers())) {
                    LOGGER.debug("Skip natural-id check, defined on abstract class");
                } else if (entityClass.getSuperclass().getInterfaces()[0].getInterfaces().length == 1) {
                    // cas XXX extend XXXAbstract
                    // XXXAsbtract implement RefMateriel
                    // RefMateriel implement TopiaEntity
                    LOGGER.debug("Skip natural-id check, defined on super class");
                } else {
                
                    String mapping = "/" + entityClass.getPackage().getName().replace('.', '/') + "/" + entityClass.getSimpleName() + ".hbm.xml";
                    InputStream entityMapping = AgrosystModelTest.class.getResourceAsStream(mapping);
                    String content = IOUtils.toString(entityMapping, Charset.defaultCharset());
                    if (!content.contains("natural-id")) {
                        LOGGER.warn("La classe " + entityClass.getName() + " n'a pas de contraintes de natural-id");
                    }
                    Assertions.assertTrue(content.contains("natural-id"), "La classe " + entityClass.getName() + " n'a pas de contraintes de natural id");
                }
            }
        }
    }
}
