package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.assertj.core.util.Sets;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public class CroppingPlanEntryTopiaDaoTest extends AbstractAgrosystTest {

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        loginAsAdmin();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        testDatas = serviceFactory.newInstance(TestDatas.class);
    }

    @Test
    protected void testGetCampaignsForCrop() throws IOException {
        testDatas.createTestDomains();

        CroppingPlanEntry crop = croppingPlanEntryDao.forTopiaIdEquals("ble-id").findUnique();
        List<Integer> campaignsForCrop = croppingPlanEntryDao.getCampaignsForCrop(crop.getCode());
        Assertions.assertFalse(campaignsForCrop.isEmpty());
        Assertions.assertEquals(1, campaignsForCrop.size());
        Assertions.assertEquals(2013, campaignsForCrop.iterator().next());
    }

    @Test
    protected void testFindACropFromCode() throws IOException {
        testDatas.createTestDomains();

        CroppingPlanEntry crop = croppingPlanEntryDao.forTopiaIdEquals("ble-id").findUnique();
        CroppingPlanEntry cropForCode = croppingPlanEntryDao.findACropFromCode(crop.getCode());
        Assertions.assertEquals(crop, cropForCode);
    }
    @Test
    protected void testCropNotExistsForCampaign() throws IOException {
        testDatas.createTestDomains();

        CroppingPlanEntry crop = croppingPlanEntryDao.forTopiaIdEquals("ble-id").findUnique();
        Set<Integer> campaigns = Sets.newTreeSet(2010, 2011);
        String cropName = croppingPlanEntryDao.findFirstEntryNameFromCode(crop.getCode(), campaigns);
        Assertions.assertEquals("Blé /!\\ Cette culture n'est pas présente pour les campagne [2010, 2011] mais existe sur la campagne 2013", cropName);
    }
}
