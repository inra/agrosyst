package fr.inra.agrosyst.api.entities;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;

public class LivestockUnitTopiaDaoTest  extends AbstractAgrosystTest {

    protected LivestockUnitTopiaDao livestockUnitDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        loginAsAdmin();
        livestockUnitDao = getPersistenceContext().getLivestockUnitDao();
        testDatas = serviceFactory.newInstance(TestDatas.class);
        alterSchema();
    }

    @Test
    public void testFindLivestockUnitForCattleCodeAndDomainCode() throws IOException {
        testDatas.createTestDomains();
        testDatas.importFrTradRefVivant();

        LivestockUnit result = livestockUnitDao.findLivestockUnitForCattleCodeAndDomainCode(
                "domaine_baulon_code_0_cattle_code",
                "domaine_baulon_code_0",
                Language.FRENCH
                );

        Assertions.assertThat(result).isNotNull();
    }

    @Test
    public void testFindLivestockUnitForCattleCodeAndDomainId() throws IOException {
        testDatas.createTestDomains();
        testDatas.importFrTradRefVivant();

        LivestockUnit result = livestockUnitDao.findLivestockUnitForCattleCodeAndDomainId(
                "domaine_baulon_code_0_cattle_code",
                TestDatas.BAULON_TOPIA_ID,
                Language.FRENCH
        );

        Assertions.assertThat(result).isNotNull();
    }

}
