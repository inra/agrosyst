package fr.inra.agrosyst.api.entities.referential;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.util.List;

public class RefVarieteGevesTopiaDaoTest extends AbstractAgrosystTest {

    private RefVarieteGevesTopiaDao refVarieteGevesDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        loginAsAdmin();
        refVarieteGevesDao = getPersistenceContext().getRefVarieteGevesDao();
        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.importVarieteGeves();
    }

    @Test
    public void testFindCodeGnisIgnoreSpace() {
        List<RefVarieteGeves> testSpace = refVarieteGevesDao.findAllForCodeGnisVarieteEqualsIgnoreSpace("Testspace");
        Assertions.assertNotNull(testSpace);
        Assertions.assertEquals(4, testSpace.size());
        Assertions.assertTrue(testSpace.stream().allMatch(rvg -> "Test space".equals(rvg.getCode_gnis_variete())));

        List<RefVarieteGeves> testSpaces = refVarieteGevesDao.findAllForCodeGnisVarieteEqualsIgnoreSpace("Testseveralspaces");
        Assertions.assertNotNull(testSpaces);
        Assertions.assertEquals(3, testSpaces.size());
        Assertions.assertTrue(testSpaces.stream().allMatch(rvg -> "Test several spaces".equals(rvg.getCode_gnis_variete())));

        List<RefVarieteGeves> testNoSpace = refVarieteGevesDao.findAllForCodeGnisVarieteEqualsIgnoreSpace("TestNoSpace");
        Assertions.assertNotNull(testNoSpace);
        Assertions.assertEquals(2, testNoSpace.size());
        Assertions.assertTrue(testNoSpace.stream().allMatch(rvg -> "TestNoSpace".equals(rvg.getCode_gnis_variete())));
    }
}
