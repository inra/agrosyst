package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterImpl;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.domain.DomainServiceImpl;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageTargetIFT;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AbstractQSATest extends AbstractAgrosystTest {

    protected DomainInputStockUnitService domainInputStockUnitService;
    protected DomainService domainService;
    protected PerformanceServiceImplForTest performanceService;
    protected EffectiveCropCycleService effectiveCropCycleService;

    protected DomainTopiaDao domainDao;
    protected ZoneTopiaDao zoneTopiaDao;

    protected EffectiveCropCyclePhaseTopiaDao cropCyclePhaseDAO;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDAO;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeTopiaDao;

    protected PracticedCropCyclePhaseTopiaDao practicedCropCyclePhasesDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeTopiaDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionTopiaDao;

    protected PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionTopiaDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDIDAO;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitsTopiaDao;

    protected PesticideProductInputUsageTopiaDao pesticideProductInputUsageDao;
    protected ImportService importService;
    protected ReferentialService referentialService;

    protected Domain baulon;
    protected RefCountry france;
    protected Collection<IndicatorFilter> indicatorFilters;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        testDatas = serviceFactory.newInstance(TestDatas.class);

        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);

        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        domainService = serviceFactory.newService(DomainService.class);

        // init dao
        domainDao = serviceContext.getPersistenceContext().getDomainDao();
        zoneTopiaDao = serviceContext.getPersistenceContext().getZoneDao();
        practicedCropCyclePhasesDao = serviceContext.getPersistenceContext().getPracticedCropCyclePhaseDao();
        cropCyclePhaseDAO = serviceContext.getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = serviceContext.getPersistenceContext().getEffectivePerennialCropCycleDao();
        practicedSeasonalCropCycleDao = serviceContext.getPersistenceContext().getPracticedSeasonalCropCycleDao();
        effectiveInterventionDAO = serviceContext.getPersistenceContext().getEffectiveInterventionDao();
        practicedInterventionDao = serviceContext.getPersistenceContext().getPracticedInterventionDao();
        effectiveSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getEffectiveSpeciesStadeDao();
        practicedSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getPracticedSpeciesStadeDao();
        pesticidesSpreadingActionTopiaDao = serviceContext.getPersistenceContext().getPesticidesSpreadingActionDao();
        refActionAgrosystTravailEDIDAO = serviceContext.getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        refActaTraitementsProduitsTopiaDao = serviceContext.getPersistenceContext().getRefActaTraitementsProduitDao();
        practicedCropCycleNodeDao = serviceContext.getPersistenceContext().getPracticedCropCycleNodeDao();
        practicedCropCycleConnectionTopiaDao = serviceContext.getPersistenceContext().getPracticedCropCycleConnectionDao();

        pesticideProductInputUsageDao = serviceContext.getPersistenceContext().getPesticideProductInputUsageDao();
        importService = serviceFactory.newService(ImportService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);

        testDatas.createTestDomains();

        // login
        loginAsAdmin();
        alterSchema();

        indicatorFilters = performanceService.getAllIndicatorFilters();

        // initialisation des paramètres de test
        this.baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        this.france = baulon.getLocation().getRefCountry();

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/sa/refMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        testDatas.createTestPlots();
        testDatas.importRefCompositionSubstancesActivesParNumeroAMM();
        testDatas.importRefSubstancesActivesCommissionEuropeenne();
        testDatas.importPhrasesRisqueEtClassesMentionDangerParAmm();
        testDatas.importRefConversionUnitesQSA();
        testDatas.importActaTraitementsProduits();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaGroupeCultures();
    }

    protected static @NotNull IndicatorFilter getIndicatorFilter(Class<? extends AbstractIndicatorQSA> clazz) {
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setWithSeedingTreatment(true);
        indicatorFilter.setWithoutSeedingTreatment(true);
        indicatorFilter.setClazz(clazz.getSimpleName());
        return indicatorFilter;
    }

    protected @NotNull StringWriter launchPerformanceTest(Class<? extends AbstractIndicatorQSA> indicatorClass, boolean practiced, Class<? extends AbstractIndicator>... requiredIndicatorClass) throws IOException {
        if (practiced) {
            testDatas.createPracticedDataSetForQSAIndicators();

        } else {
            testDatas.createEffectiveDataSetForQSAIndicators();
        }

        return lauchPerformanceTest(indicatorClass, practiced, requiredIndicatorClass);
    }

    private @NotNull StringWriter lauchPerformanceTest(Class<? extends AbstractIndicatorQSA> indicatorClass, boolean practiced, Class<? extends AbstractIndicator>[] requiredIndicatorClass) {
        IndicatorFilter indicatorFilter = getIndicatorFilter(indicatorClass);

        //
        // Calcul de l'indicateur
        //

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(practiced);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, List.of(indicatorFilter), null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        List<AbstractIndicator> requiredIndicators = new ArrayList<>();

        for (Class<? extends AbstractIndicator> aClass : requiredIndicatorClass) {
            AbstractIndicator indicator_ = serviceFactory.newInstance(aClass);
            requiredIndicators.add(indicator_);
        }

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun
        requiredIndicators.add(indicatorVintageTargetIFT);

        AbstractIndicatorQSA testedIndicator = serviceFactory.newInstance(indicatorClass);
        testedIndicator.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        requiredIndicators.add(testedIndicator);

        convertToWriter(performance, writer, indicatorFilter, requiredIndicators);
        return out;
    }

    private void convertToWriter(Performance performance, IndicatorMockWriter writer, IndicatorFilter indicatorFilter, List<? extends AbstractIndicator> indicators) {
        if (indicators.size() == 1) {
            performanceService.convertToWriter(performance, writer, List.of(indicatorFilter), indicators.get(0));
        } else if (indicators.size() == 2) {
            performanceService.convertToWriter(performance, writer, List.of(indicatorFilter), indicators.get(0), indicators.get(1));
        } else if (indicators.size() == 3) {
            performanceService.convertToWriter(performance, writer, List.of(indicatorFilter), indicators.get(0), indicators.get(1), indicators.get(2));
        } else if (indicators.size() == 4) {
            performanceService.convertToWriter(performance, writer, List.of(indicatorFilter), indicators.get(0), indicators.get(1), indicators.get(2), indicators.get(3));
        }
    }

    protected void testPour1ApplicationEnSynthetise(RefActaTraitementsProduit produit,
                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                    double quantite,
                                                    String expectedContent,
                                                    AbstractIndicatorQSA indicatorQSA) throws IOException {
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), 1.0, 1.0, indicatorQSA, 100.0, 25d, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorActiveSubstancesAmount(RefActaTraitementsProduit produit,
                                                                        DomainPhytoProductInputDto domainPhytoProduct,
                                                                        double quantite,
                                                                        String expectedContent) throws IOException {
        IndicatorActiveSubstanceAmount indicatorActiveSubstanceAmount = serviceFactory.newInstance(IndicatorActiveSubstanceAmount.class);
        indicatorActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), 1.0, 1.0, indicatorActiveSubstanceAmount, 100.0, 25d, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorTotalAmount(RefActaTraitementsProduit produit,
                                                                        DomainPhytoProductInputDto domainPhytoProduct,
                                                                        double quantite,
                                                                        String expectedContent) throws IOException {
        IndicatorTotalActiveSubstanceAmount indicatorTotalActiveSubstanceAmount = serviceFactory.newInstance(IndicatorTotalActiveSubstanceAmount.class);
        indicatorTotalActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), 1.0, 1.0, indicatorTotalActiveSubstanceAmount, 100.0, 25d, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorTotalAmount(RefActaTraitementsProduit produit,
                                                                        DomainPhytoProductInputDto domainPhytoProduct,
                                                                        double quantite,
                                                                        String expectedContent,
                                                                        double frequenceSpatiale,
                                                                        double frequenceTemporelle) throws IOException {
        IndicatorTotalActiveSubstanceAmount indicatorTotalActiveSubstanceAmount = serviceFactory.newInstance(IndicatorTotalActiveSubstanceAmount.class);
        indicatorTotalActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent),  frequenceSpatiale, frequenceTemporelle, indicatorTotalActiveSubstanceAmount, 100.0, 25d, false);
    }
    protected void testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(RefActaTraitementsProduit produit,
                                                                                 DomainPhytoProductInputDto domainPhytoProduct,
                                                                                 double quantite,
                                                                                 String expectedContent) throws IOException {
        IndicatorTotalActiveSubstanceAmount totalActiveSubstanceAmount = serviceFactory.newInstance(IndicatorTotalActiveSubstanceAmount.class);
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        totalActiveSubstanceAmount.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), 1.0, 1.0, totalActiveSubstanceAmount, 100.0, 25d, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(RefActaTraitementsProduit produit,
                                                                                 DomainPhytoProductInputDto domainPhytoProduct,
                                                                                 double quantite,
                                                                                 String expectedContent,
                                                                                 double frequenceSpatiale,
                                                                                 double frequenceTemporelle, double proportionOfTreatedSurface, double boiledQuantity) throws IOException {
        IndicatorTotalActiveSubstanceAmount totalActiveSubstanceAmount = serviceFactory.newInstance(IndicatorTotalActiveSubstanceAmount.class);
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        totalActiveSubstanceAmount.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), frequenceSpatiale, frequenceTemporelle, totalActiveSubstanceAmount, proportionOfTreatedSurface, boiledQuantity, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1(RefActaTraitementsProduit produit,
                                                                 DomainPhytoProductInputDto domainPhytoProduct,
                                                                 double quantite,
                                                                 String expectedContent, double proportionOfTreatedSurface, double boiledQuantity) throws IOException {
        IndicatorHRI1 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1.class);
        indicatorHRI1.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), 1.0, 1.0, indicatorHRI1, proportionOfTreatedSurface, boiledQuantity, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1(RefActaTraitementsProduit produit,
                                                                 DomainPhytoProductInputDto domainPhytoProduct,
                                                                 double quantite,
                                                                 String expectedContent,
                                                                 double frequenceSpatiale,
                                                                 double frequenceTemporelle,
                                                                 double proportionOfTreatedSurface,
                                                                 double boiledQuantity) throws IOException {
        IndicatorHRI1 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1.class);
        indicatorHRI1.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent),  frequenceSpatiale, frequenceTemporelle, indicatorHRI1, proportionOfTreatedSurface, boiledQuantity, false);
    }

        protected void testPour1ApplicationSemisPlusieursIntrantsAvecTS_EnSynthetiseIndicatorHRI1(RefActaTraitementsProduit produit,
                                                                 DomainPhytoProductInputDto domainPhytoProduct,
                                                                 double quantite,
                                                                 String expectedContent,
                                                                 double frequenceSpatiale,
                                                                 double frequenceTemporelle, double proportionOfTreatedSurface, double boiledQuantity) throws IOException {
        IndicatorHRI1 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1.class);
        indicatorHRI1.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent),  frequenceSpatiale, frequenceTemporelle, indicatorHRI1, proportionOfTreatedSurface, boiledQuantity, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1_G1(RefActaTraitementsProduit produit,
                                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                                    Double quantite,
                                                                    String expectedContent, double proportionOfTreatedSurface, double boiledQuantity) throws IOException {
        IndicatorHRI1_G1 indicator = serviceFactory.newInstance(IndicatorHRI1_G1.class);
        indicator.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), 1.0, 1.0, indicator, proportionOfTreatedSurface, boiledQuantity, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1_G1(RefActaTraitementsProduit produit,
                                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                                    Double quantite,
                                                                    List<String> expectedContent,
                                                                    double frequenceSpatiale,
                                                                    double frequenceTemporelle, double proportionOfTreatedSurface, double boiledQuantity) throws IOException {
        IndicatorHRI1_G1 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1_G1.class);
        indicatorHRI1.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, expectedContent,  frequenceSpatiale, frequenceTemporelle, indicatorHRI1, proportionOfTreatedSurface, boiledQuantity, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(RefActaTraitementsProduit produit,
                                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                                    Double quantite,
                                                                    String expectedContent, double proportionOfTreatedSurface, double boiledQuantity) throws IOException {
        IndicatorHRI1_G2 indicator = serviceFactory.newInstance(IndicatorHRI1_G2.class);
        indicator.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, List.of(expectedContent), 1.0, 1.0, indicator, proportionOfTreatedSurface, boiledQuantity, false);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(RefActaTraitementsProduit produit,
                                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                                    double quantite,
                                                                    List<String> expectedContent,
                                                                    double frequenceSpatiale,
                                                                    double frequenceTemporelle, double proportionOfTreatedSurface, double boiledQuantity, boolean useOriginaleWriterFormat) throws IOException {
        IndicatorHRI1_G2 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1_G2.class);
        indicatorHRI1.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, expectedContent,  frequenceSpatiale, frequenceTemporelle, indicatorHRI1, proportionOfTreatedSurface, boiledQuantity, useOriginaleWriterFormat);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1_G3(RefActaTraitementsProduit produit,
                                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                                    double quantite,
                                                                    List<String> expectedContent,
                                                                    double frequenceSpatiale,
                                                                    double frequenceTemporelle, double proportionOfTreatedSurface, double boiledQuantity,
                                                                    boolean useOriginaleWriterFormat) throws IOException {
        IndicatorHRI1_G3 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1_G3.class);
        indicatorHRI1.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, expectedContent,  frequenceSpatiale, frequenceTemporelle, indicatorHRI1, proportionOfTreatedSurface, boiledQuantity, useOriginaleWriterFormat);
    }

    protected void testPour1ApplicationEnSynthetiseIndicatorHRI1_G4(RefActaTraitementsProduit produit,
                                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                                    double quantite,
                                                                    List<String> expectedContent,
                                                                    double frequenceSpatiale,
                                                                    double frequenceTemporelle, double proportionOfTreatedSurface, double boiledQuantity) throws IOException {
        IndicatorHRI1_G4 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1_G4.class);
        indicatorHRI1.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        this.testPour1ApplicationEnSynthetise(produit, domainPhytoProduct, quantite, expectedContent,  frequenceSpatiale, frequenceTemporelle, indicatorHRI1, proportionOfTreatedSurface, boiledQuantity, false);
    }

    protected void testPour1ApplicationEnSynthetise(RefActaTraitementsProduit produit,
                                                    DomainPhytoProductInputDto domainPhytoProduct,
                                                    Double quantite,
                                                    List<String> expectedContents,
                                                    double frequenceSpatiale,
                                                    double frequenceTemporelle,
                                                    AbstractIndicatorQSA indicatorQSA,
                                                    double proportionOfTreatedSurface,
                                                    double boiledQuantity,
                                                    boolean useOriginaleWriterFormat) throws IOException {
        final ArrayList<DomainInputDto> domainInputs = Lists.newArrayList(domainPhytoProduct);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntries);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputs, pesristedCropResult);

        final Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(baulon);
        List<AbstractDomainInputStockUnit> domainPhytoInputs = domainInputStock.get(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        Map<RefActaTraitementsProduit, DomainPhytoProductInput> domainInputPhytoByRefInput =
                domainPhytoInputs.stream()
                        .map(dp -> ((DomainPhytoProductInput)dp))
                        .collect(Collectors.toMap(DomainPhytoProductInput::getRefInput, Function.identity()));

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null,"Systeme de culture Baulon 1");
        final Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );
        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);


        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_TOPIA_ID, "I_TEST_000",
                PracticedIntervention.PROPERTY_NAME, "Intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, frequenceSpatiale,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, frequenceTemporelle);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        final DomainPhytoProductInput domainPhytoProductInputLongrun = domainInputPhytoByRefInput.get(produit);
        final PesticideProductInputUsage longrunUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductInputLongrun,
                PesticideProductInputUsage.PROPERTY_TARGETS, null,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, quantite
        );

        RefInterventionAgrosystTravailEDI actionSEX = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEX").findAny();
        pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, proportionOfTreatedSurface,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, boiledQuantity,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(longrunUsage)
        );

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(useOriginaleWriterFormat);
        writer.setPerformance(performance);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorQSA);
        final String content = out.toString();
        for (String expectedContent : expectedContents) {
            assertThat(content).usingComparator(comparator).isEqualTo(expectedContent);
        }
    }


    protected void testPour1ApplicationPlusieursIntrantsSansTS_EnSynthetiseIndicatorHRI1(
            List<TestUsage> testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle,
            double proportionOfTreatedSurface,
            double boiledQuantity) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1.class);
        indicatorHRI1.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseSansTS(testUsages, frequenceSpatiale, frequenceTemporelle, proportionOfTreatedSurface, boiledQuantity, List.of(indicatorHRI1), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsSansTS_EnSynthetiseIndicatorHRI1_G1(
            List<TestUsage> testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle,
            double proportionOfTreatedSurface,
            double boiledQuantity) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G1 indicatorHRI1G1 = serviceFactory.newInstance(IndicatorHRI1_G1.class);
        indicatorHRI1G1.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseSansTS(testUsages, frequenceSpatiale, frequenceTemporelle, proportionOfTreatedSurface, boiledQuantity, List.of(indicatorHRI1G1), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsAvecTS_EnSynthetiseIndicatorHRI1(
            SeedSLotTestUsage testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1 indicatorHRI1 = serviceFactory.newInstance(IndicatorHRI1.class);
        indicatorHRI1.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseAvecTS(testUsages, frequenceSpatiale, frequenceTemporelle, List.of(indicatorHRI1), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsAvecTS_EnSynthetiseIndicatorHRI1_G1(
            SeedSLotTestUsage testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G1 indicatorHRI1G1 = serviceFactory.newInstance(IndicatorHRI1_G1.class);
        indicatorHRI1G1.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseAvecTS(testUsages, frequenceSpatiale, frequenceTemporelle, List.of(indicatorHRI1G1), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsAvecTS_EnSynthetiseIndicatorHRI1_G2(
            SeedSLotTestUsage testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G2 indicatorHRI1G2 = serviceFactory.newInstance(IndicatorHRI1_G2.class);
        indicatorHRI1G2.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseAvecTS(testUsages, frequenceSpatiale, frequenceTemporelle, List.of(indicatorHRI1G2), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsAvecTS_EnSynthetiseIndicatorHRI1_G3(
            SeedSLotTestUsage testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G3 indicatorHRI1G3 = serviceFactory.newInstance(IndicatorHRI1_G3.class);
        indicatorHRI1G3.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseAvecTS(testUsages, frequenceSpatiale, frequenceTemporelle, List.of(indicatorHRI1G3), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsAvecTS_EnSynthetiseIndicatorHRI1_G4(
            SeedSLotTestUsage testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G4 indicatorHRI1G4 = serviceFactory.newInstance(IndicatorHRI1_G4.class);
        indicatorHRI1G4.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseAvecTS(testUsages, frequenceSpatiale, frequenceTemporelle, List.of(indicatorHRI1G4), expectedContents);
    }
    protected void testPour1ApplicationPlusieursIntrantsSansTS_EnSynthetiseIndicatorHRI1_G2(
            List<TestUsage> testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle,
            double proportionOfTreatedSurface,
            double boiledQuantity) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G2 indicatorHRI1_G2 = serviceFactory.newInstance(IndicatorHRI1_G2.class);
        indicatorHRI1_G2.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseSansTS(testUsages, frequenceSpatiale, frequenceTemporelle, proportionOfTreatedSurface, boiledQuantity, List.of(indicatorHRI1_G2), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsSansTS_EnSynthetiseIndicatorHRI1_G3(
            List<TestUsage> testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle,
            double proportionOfTreatedSurface,
            double boiledQuantity) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G3 indicatorHRI1_G3 = serviceFactory.newInstance(IndicatorHRI1_G3.class);
        indicatorHRI1_G3.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseSansTS(testUsages, frequenceSpatiale, frequenceTemporelle, proportionOfTreatedSurface, boiledQuantity, List.of(indicatorHRI1_G3), expectedContents);
    }

    protected void testPour1ApplicationPlusieursIntrantsSansTS_EnSynthetiseIndicatorHRI1_G4(
            List<TestUsage> testUsages,
            List<String> expectedContents,
            double frequenceSpatiale,
            double frequenceTemporelle,
            double proportionOfTreatedSurface,
            double boiledQuantity) throws IOException {
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        IndicatorHRI1_G4 indicatorHRI1_G4 = serviceFactory.newInstance(IndicatorHRI1_G4.class);
        indicatorHRI1_G4.setConversionRatesByUnit(coeffsConversionVersKgHa);
        this.testPourNApplicationEnSynthetiseSansTS(testUsages, frequenceSpatiale, frequenceTemporelle, proportionOfTreatedSurface, boiledQuantity, List.of(indicatorHRI1_G4), expectedContents);
    }

    protected void testPourNApplicationEnSynthetiseSansTS(
            List<TestUsage> testUsages,
            double frequenceSpatiale,
            double frequenceTemporelle,
            double proportionOfTreatedSurface,
            double boiledQuantity,
            List<AbstractIndicatorQSA> indicators,
            List<String> expectedContents) throws IOException {
        Collection<DomainInputDto> domainInputs = new ArrayList<>();
        testUsages.stream().map(TestUsage::domainPhytoProduct).forEach(domainInputs::add);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntries);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputs, pesristedCropResult);

        final Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(baulon);
        List<AbstractDomainInputStockUnit> domainPhytoInputs = domainInputStock.get(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        Map<RefActaTraitementsProduit, DomainPhytoProductInput> domainInputPhytoByRefInput =
                domainPhytoInputs.stream()
                        .map(dp -> ((DomainPhytoProductInput)dp))
                        .collect(Collectors.toMap(DomainPhytoProductInput::getRefInput, Function.identity()));

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null,"Systeme de culture Baulon 1");
        final Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );
        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);


        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_TOPIA_ID, "I_TEST_000",
                PracticedIntervention.PROPERTY_NAME, "Intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, frequenceSpatiale,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, frequenceTemporelle);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        List<AbstractInputUsage> usages = new ArrayList<>();
        for (TestUsage testUsage : testUsages) {
            final DomainPhytoProductInput domainPhytoProductInputLongrun = domainInputPhytoByRefInput.get(testUsage.produit());
            final PesticideProductInputUsage longrunUsage = pesticideProductInputUsageDao.create(
                    PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductInputLongrun,
                    PesticideProductInputUsage.PROPERTY_TARGETS, null,
                    AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                    AbstractInputUsage.PROPERTY_QT_AVG, testUsage.quantite()
            );
            usages.add(longrunUsage);
        }

        RefInterventionAgrosystTravailEDI actionSEX = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEX").findAny();
        pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, proportionOfTreatedSurface,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, boiledQuantity,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, usages
        );

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        if (indicators.size() == 1) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst());
        } else if (indicators.size() == 2) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1));
        } else if (indicators.size() == 3) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1), indicators.get(2));
        } else if (indicators.size() == 4) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1), indicators.get(2), indicators.get(3));
        } else if (indicators.size() == 5) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1), indicators.get(2), indicators.get(3), indicators.get(4));
        }
        final String content = out.toString();
        for (String expectedContent : expectedContents) {
            assertThat(content).usingComparator(comparator).isEqualTo(expectedContent);
        }
    }

    protected void testPourNApplicationEnSynthetiseAvecTS(
            SeedSLotTestUsage testUsages,
            double frequenceSpatiale,
            double frequenceTemporelle,
            List<AbstractIndicatorQSA> indicators,
            List<String> expectedContents) throws IOException {

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null,"Systeme de culture Baulon 1");
        final Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = testUsages.crop();

        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);


        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_TOPIA_ID, "I_TEST_000",
                PracticedIntervention.PROPERTY_NAME, "Intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, frequenceSpatiale,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, frequenceTemporelle);

        List<SeedSpeciesTestUsage> seedSpeciesTestUsages = testUsages.speciesTestUsages();
        seedSpeciesTestUsages.stream().map(SeedSpeciesTestUsage::species).forEach(
                cps -> {
                    final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, cps.getCode());
                    intervention.addSpeciesStades(speciesStadeBle);
                }
        );

        Collection<SeedSpeciesInputUsage> seedSpeciesInputUsages = new ArrayList<>();
        DomainSeedLotInput domainSeedLotInput = testUsages.domainSeedLotInput();
        for (SeedSpeciesTestUsage seedSpeciesTestUsage : seedSpeciesTestUsages) {
            DomainSeedSpeciesInput domainSeedSpeciesInput = seedSpeciesTestUsage.domainSeedSpeciesInput();
            Collection<DomainPhytoProductInput> speciesPhytoInputs = domainSeedSpeciesInput.getSpeciesPhytoInputs();
            Map<RefActaTraitementsProduit, DomainPhytoProductInput> domainSeedProdByRefProdIds = new HashMap<>();
            for (DomainPhytoProductInput speciesPhytoInput : speciesPhytoInputs) {
                RefActaTraitementsProduit refInput = speciesPhytoInput.getRefInput();
                domainSeedProdByRefProdIds.put(refInput, speciesPhytoInput);
            }

            List<SeedProductInputUsage> speciesUsagePhytoProductUsages = new ArrayList<>();
            for (TestUsage speciesPhytoProductTestUsage : seedSpeciesTestUsage.speciesProductUsages()) {
                RefActaTraitementsProduit produit = speciesPhytoProductTestUsage.produit();
                DomainPhytoProductInput domainPhytoProductInput = domainSeedProdByRefProdIds.get(produit);
                SeedProductInputUsage seedProductInputUsage = getPersistenceContext().getSeedProductInputUsageDao().create(
                        SeedProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductInput,
                        AbstractInputUsage.PROPERTY_QT_AVG, speciesPhytoProductTestUsage.quantite(),
                        AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE
                );
                speciesUsagePhytoProductUsages.add(seedProductInputUsage);
            }

            SeedSpeciesInputUsage seedSpeciesInputUsage = getPersistenceContext().getSeedSpeciesInputUsageDao().create(
                    SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, speciesUsagePhytoProductUsages,
                    SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, domainSeedSpeciesInput,
                    AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                    AbstractInputUsage.PROPERTY_QT_AVG, seedSpeciesTestUsage.quantite()
            );
            seedSpeciesInputUsages.add(seedSpeciesInputUsage);
        }


        SeedLotInputUsage seedLotInputUsage = getPersistenceContext().getSeedLotInputUsageDao().create(
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, domainSeedLotInput,
                SeedLotInputUsage.PROPERTY_DEEPNESS, 56.8,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, seedSpeciesInputUsages,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );

        RefInterventionAgrosystTravailEDI mainAction = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao().forNaturalId("SEW").findUnique();
        getPersistenceContext().getSeedingActionUsageDao().create(
                SeedingActionUsage.PROPERTY_PRACTICED_INTERVENTION, intervention,
                SeedingActionUsage.PROPERTY_MAIN_ACTION, mainAction,
                SeedingActionUsage.PROPERTY_YEALD_TARGET, 100.0,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotInputUsage)
        );

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        if (indicators.size() == 1) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst());
        } else if (indicators.size() == 2) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1));
        } else if (indicators.size() == 3) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1), indicators.get(2));
        } else if (indicators.size() == 4) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1), indicators.get(2), indicators.get(3));
        } else if (indicators.size() == 5) {
            performanceService.convertToWriter(performance, writer, indicatorFilters, indicators.getFirst(), indicators.get(1), indicators.get(2), indicators.get(3), indicators.get(4));
        }
        final String content = out.toString();
        for (String expectedContent : expectedContents) {
            assertThat(content).usingComparator(comparator).isEqualTo(expectedContent);
        }
    }
}
