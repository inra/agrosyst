package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.StandardSystemProperty;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import fr.inra.agrosyst.api.H2SQL10CustomDialect;
import fr.inra.agrosyst.api.entities.AgrosystTopiaApplicationContext;
import fr.inra.agrosyst.api.entities.AgrosystTopiaDaoSupplier;
import fr.inra.agrosyst.api.entities.AgrosystTopiaPersistenceContext;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.ServiceFactory;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.internal.InstancesSynchroHelper;
import fr.inra.agrosyst.services.security.SecurityContext;
import jakarta.persistence.ValidationMode;
import lombok.Getter;
import org.apache.poi.util.TempFile;
import org.hibernate.tool.schema.Action;
import org.nuiton.topia.persistence.HibernateAvailableSettings;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaTransaction;

import java.io.File;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class TestServiceContext implements ServiceContext {

    protected final Method testMethod;

    protected AgrosystTopiaPersistenceContext transaction;
    protected AgrosystTopiaApplicationContext rootContext;
    
    @Getter
    protected ServiceFactory serviceFactory;
    protected AgrosystServiceConfig config;
    
    protected AuthenticatedUser authenticatedUser;
    protected SecurityContext securityContext;

    TestServiceContext(Method testMethod) {
        this.testMethod = testMethod;
        this.serviceFactory = new DefaultServiceFactory(this);
    }
    
    public TestServiceContext(Method testMethod,
                              AgrosystServiceConfig config,
                              AuthenticatedUser authenticatedUser) {
        this.testMethod = testMethod;
        this.serviceFactory = new DefaultServiceFactory(this);
        this.config = config;
        this.authenticatedUser = authenticatedUser;

        beforeEachTest();
    }
    
    @Override
    public ServiceContext newServiceContext() {
        TestServiceContext result = new TestServiceContext(this.testMethod, this.config, this.authenticatedUser);
        return result;
    }

    @Override
    public ServiceContext newServiceContext(AuthenticatedUser authenticatedUser) {
        TestServiceContext result = new TestServiceContext(this.testMethod, this.config, authenticatedUser);
        return result;
    }

    @Override
    public void close() {
        // do nothing for test, done by #afterEachTest()
    }

    @Override
    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    @Override
    public LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    @Override
    public OffsetDateTime getOffsetTime() {
        return OffsetDateTime.now();
    }

    @Override
    public TopiaTransaction getTransaction() {
        TopiaTransaction result = getTransaction(true);
        return result;
    }

    @Override
    public TopiaTransaction getTransaction(boolean create) {
        AgrosystTopiaPersistenceContext result = getTransaction0(create);
        return result;
    }

    @Override
    public AgrosystTopiaDaoSupplier getDaoSupplier() {
        AgrosystTopiaDaoSupplier result = getTransaction0(true);
        return result;
    }

    @Override
    public AgrosystTopiaPersistenceContext getPersistenceContext() {
        // must stay to false for test, test should not open new transactions
        AgrosystTopiaPersistenceContext result = getTransaction0(false);
        return result;
    }

    @Override
    public AgrosystTopiaPersistenceContext getPersistenceContext(boolean create) {
        AgrosystTopiaPersistenceContext result = getTransaction0(create);
        return result;
    }

    private AgrosystTopiaPersistenceContext getTransaction0(boolean create) {
        Preconditions.checkState(rootContext != null);

        if (transaction == null && create) {
            transaction = rootContext.newPersistenceContext();
        }
        return transaction;
    }

    @Override
    public <E extends AgrosystService> E newService(Class<E> clazz) {
        return serviceFactory.newService(clazz);
    }

    @Override
    public <I> I newInstance(Class<I> clazz) {
        return serviceFactory.newInstance(clazz);
    }
    
    @Override
    public <E extends AgrosystService, F extends AgrosystService> F newExpectedService(Class<E> clazz, Class<F> expectedClazz) {
        return serviceFactory.newExpectedService(clazz, expectedClazz);
    }
    
    @Override
    public AgrosystServiceConfig getConfig() {
        if (config == null) {
            config = new AgrosystServiceConfig("agrosyst-test.properties");

        }
        return config;
    }

    protected void setUpPoiTmpStrategy() {
        String path = Joiner.on(File.separator).join(StandardSystemProperty.JAVA_IO_TMPDIR.value(), String.valueOf(System.nanoTime()), testMethod.getDeclaringClass(), testMethod.getName(), "poifiles");
        TempFile.setTempFileCreationStrategy(new TestPoiTempFileCreationStrategy(path));
    }

    protected void beforeEachTest() {

        setUpPoiTmpStrategy();

        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        TopiaConfiguration topiaConfiguration = topiaConfigurationBuilder
                .forTestDatabase(testMethod.getDeclaringClass(), testMethod.getName())
                .useAlreadyExistingDatabaseAsIs()
                .doNotValidateSchemaOnStartup()
                .useHikariConnectionPool()
                .build();

        Map<String, String> p0 = Maps.fromProperties(newHibernateExtraConfigurationForTraductions());
        Map<String, String> r0 = topiaConfiguration.getHibernateExtraConfiguration();
        r0.putAll(p0);

        rootContext = AgrosystConfigurationHelper.newApplicationContext(getConfig(), topiaConfiguration);
    }

    static Properties newHibernateExtraConfigurationForTraductions() {
        Properties properties = new Properties();

        properties.setProperty(HibernateAvailableSettings.SHOW_SQL, "false");
        properties.setProperty(HibernateAvailableSettings.CONNECTION_PROVIDER, "org.hibernate.hikaricp.internal.HikariCPConnectionProvider");
        properties.setProperty("hibernate.hikari.minimumIdle", "1");
        properties.setProperty("hibernate.hikari.maximumPoolSize", "20");
        properties.setProperty("hibernate.hikari.idleTimeout", "600000");
        properties.setProperty(HibernateAvailableSettings.JAKARTA_HBM2DDL_DATABASE_ACTION, Action.UPDATE.name());
        properties.setProperty(HibernateAvailableSettings.JAKARTA_VALIDATION_MODE, ValidationMode.NONE.name());
        properties.setProperty(HibernateAvailableSettings.DIALECT, H2SQL10CustomDialect.class.getName());

        return properties;
    }

    void afterEachTest() {
        try {
            if (rootContext != null && transaction != null && !transaction.isClosed()) {
                try {
                    transaction.rollback();
                } catch (Exception e) {
                    //nothing to do
                }
                try {
                    transaction.getHibernateSupport().getHibernateSession().clear();
                } catch (Exception e) {
                    //nothing to do
                }
                try {
                    transaction.close();
                } catch (Exception e){
                    //nothing to do
                }
            }
        } finally {
            transaction = null;
            if (rootContext != null && !rootContext.isClosed()) {
                rootContext.close();
            }
            serviceFactory = null;
            securityContext = null;

            config = null;

            authenticatedUser = null;
        }
    }

    public void setAuthenticatedUser(AuthenticatedUser authUser) {
        this.authenticatedUser = authUser;
        securityContext = null;
    }

    @Override
    public SecurityContext getSecurityContext() {
        if (securityContext == null) {
            securityContext = new SecurityContext(getServiceFactory(), authenticatedUser);
        }
        return securityContext;
    }
    
    @Override
    public SecurityContext getSecurityContextAsUser(String userId) {
        SecurityContext securityContext = new SecurityContext(getServiceFactory(), authenticatedUser, userId);
        return securityContext;
    }
    
    @Override
    public Gson getGson() {
        return new AgrosystGsonSupplier().get();
    }

    @Override
    public InstancesSynchroHelper getSynchroHelper() {
        // Non pertinent dans les tests
        return new InstancesSynchroHelper(Optional.empty());
    }

    @Override
    public BusinessTasksManager getTaskManager() {
        return rootContext.getTaskManager();
    }

}
