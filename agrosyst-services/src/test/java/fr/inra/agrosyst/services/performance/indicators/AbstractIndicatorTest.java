package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionImpl;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AbstractIndicatorTest extends AbstractAgrosystTest {

    private AbstractIndicator abstractIndicator;

    @BeforeEach
    public void setup() {
        /* Mock qui permet de fournir une implémentation par défaut de la classe abstraite AbstractIndicator
         * Les méthodes non abstraites définies dans AbstractIndicator sont appelables telles quelles, sans
         * que leur comportement ne soit changé.
         */
        this.abstractIndicator = Mockito.mock(AbstractIndicator.class, Mockito.CALLS_REAL_METHODS);
    }

    private static Stream<Arguments> incorrectDataProvider() {
        return Stream.of(
                Arguments.of(getPracticedIntervention("coucou")),
                Arguments.of(getPracticedIntervention("123/09")),
                Arguments.of(getPracticedIntervention("123")),
                Arguments.of(getPracticedIntervention("1/01")),
                Arguments.of(getPracticedIntervention("01/1"))
        );
    }
    private static PracticedIntervention getPracticedIntervention(final String startingDate) {
        return getPracticedIntervention(startingDate, startingDate);
    }
    private static PracticedIntervention getPracticedIntervention(final String startingDate, final String endingDate) {
        final PracticedInterventionImpl practicedIntervention = new PracticedInterventionImpl();
        practicedIntervention.setStartingPeriodDate(startingDate);
        practicedIntervention.setEndingPeriodDate(endingDate);
        return practicedIntervention;
    }


    @ParameterizedTest
    @MethodSource("practicedInterventionProvider")
    public void testWithPracticedInterventions(final PracticedIntervention practicedIntervention, final double expectedDayOfYear) {
        final double dayOfYear = abstractIndicator.computeAverageDayOfYear(practicedIntervention);
        assertEquals(expectedDayOfYear, dayOfYear);
    }

    private static Stream<Arguments> practicedInterventionProvider() {
        return Stream.of(
                Arguments.of(
                        getPracticedIntervention("01/07"),
                        183.0
                ),
                Arguments.of(
                        getPracticedIntervention("03/02", "10/02"),
                        37.5
                ),
                Arguments.of(
                        getPracticedIntervention("01/01"),
                        1.0
                ),
                Arguments.of(
                        getPracticedIntervention("31/12"),
                        366.0
                )
        );
    }

}
