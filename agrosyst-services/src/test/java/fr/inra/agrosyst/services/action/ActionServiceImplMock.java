package fr.inra.agrosyst.services.action;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2023 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;

import java.util.List;

public class ActionServiceImplMock extends ActionServiceImpl {

    public ActionServiceImplMock(ActionServiceImpl actionService) {
        super.i18nService = actionService.i18nService;
        super.inputUsageService = actionService.inputUsageService;
        super.inputPriceService = actionService.inputPriceService;
        super.referentialService = actionService.referentialService;
        super.priceService = actionService.priceService;

        super.abstractActionDao = actionService.abstractActionDao;
        super.biologicalControlActionDao = actionService.biologicalControlActionDao;
        super.carriageActionDao = actionService.carriageActionDao;
        super.croppingPlanEntryDao = actionService.croppingPlanEntryDao;
        super.croppingPlanSpeciesDao = actionService.croppingPlanSpeciesDao;
        super.effectiveCropCycleConnectionDao = actionService.effectiveCropCycleConnectionDao;
        super.effectivePerennialCropCycleDao = actionService.effectivePerennialCropCycleDao;
        super.harvestingActionDao = actionService.harvestingActionDao;
        super.harvestingActionValorisationDao = actionService.harvestingActionValorisationDao;
        super.irrigationActionDao = actionService.irrigationActionDao;
        super.livestockUnitDao = actionService.livestockUnitDao;
        super.maintenancePruningVinesActionDao = actionService.maintenancePruningVinesActionDao;
        super.mineralFertilizersSpreadingActionDao = actionService.mineralFertilizersSpreadingActionDao;
        super.organicFertilizersSpreadingActionDao = actionService.organicFertilizersSpreadingActionDao;
        super.otherActionDao = actionService.otherActionDao;
        super.pesticidesSpreadingActionDao = actionService.pesticidesSpreadingActionDao;
        super.qualityCriteriaDao = actionService.qualityCriteriaDao;
        super.refDestinationDao = actionService.refDestinationDao;
        super.refInterventionAgrosystTravailEdiDao = actionService.refInterventionAgrosystTravailEdiDao;
        super.seedingActionUsageDao = actionService.seedingActionUsageDao;
        super.tillageActionDao = actionService.tillageActionDao;

        // from domain input
        super.domainPhytoProductInputDao = actionService.domainPhytoProductInputDao;
        super.domainIrrigationInputDao = actionService.domainIrrigationInputDao;
        super.domainMineralProductInputDao = actionService.domainMineralProductInputDao;
        super.domainOrganicProductInputDao = actionService.domainOrganicProductInputDao;
        super.domainOtherInputDao = actionService.domainOtherInputDao;
        super.domainPotInputDao = actionService.domainPotInputDao;
        super.domainSeedLotInputDao = actionService.domainSeedLotInputDao;
        super.domainSubstrateInputDao = actionService.domainSubstrateInputDao;

        super.context = actionService.getContext();
    }

    public List<AbstractActionDto> transformeEntitiesToDtos(List<AbstractAction> actions) {
        return super.transformeEntitiesToDtos(actions);
    }
}
