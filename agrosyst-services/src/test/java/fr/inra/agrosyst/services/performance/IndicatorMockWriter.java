package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPriceSummary;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.export.ExportUtils;
import fr.inra.agrosyst.services.performance.indicators.EffectiveItkCropCycleScaleKey;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.util.Precision;

import java.io.IOException;
import java.io.Writer;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;
import static org.nuiton.i18n.I18n.t;

/**
 * Mock writer to analyze main code output.
 * 
 * @author Eric Chatellier
 */
public class IndicatorMockWriter implements IndicatorWriter {
    
    @Getter
    protected final DecimalFormat df;
    
    {
        final DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        decimalFormatSymbols.setDecimalSeparator('.');
        df = new DecimalFormat(DECIMAL_FORMAT, decimalFormatSymbols);
        df.setRoundingMode(RoundingMode.HALF_UP);
    }
    
    public static final Function<Map<Pair<RefDestination, YealdUnit>, Double>, String> GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION = yealdAverages -> {
        List<String> allPrintablePlotYealdAverage = new ArrayList<>();
        if (yealdAverages != null) {
            for (Map.Entry<Pair<RefDestination, YealdUnit>, Double> pairDoubleEntry : yealdAverages.entrySet()) {
                
                final Pair<RefDestination, YealdUnit> destAndUnit = pairDoubleEntry.getKey();
                final String unit = AgrosystI18nService.getEnumTraductionWithDefaultLocale(destAndUnit.getRight());
                final String destination = destAndUnit.getLeft().getDestination();
                final Double yealdAverage = pairDoubleEntry.getValue();
                
                String printableYeald = String.format("[%s]|%s|%s", destination, yealdAverage, unit);
                allPrintablePlotYealdAverage.add(printableYeald);
            }
        }
        return String.join("#", allPrintablePlotYealdAverage);
    };
    
    protected Writer writer;
    
    @Setter
    protected Performance performance;

    @Setter
    protected boolean useOriginaleWriterFormat = true;
    
    public IndicatorMockWriter(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void init() {
        
    }
    
    @Override
    public void addComputedIndicators(GenericIndicator... indicators) {
    
    }
    
    @Override
    public void afterAllIndicatorsAreWritten() {
    
    }
    
    @Override
    public void finish() {
        
    }

    protected void writeCsv(String sheetName,
                            String years,
                            String indicatorCategory,
                            String indicatorName,
                            Object value,
                            String printableYealdAverages,
                            Integer reliabilityIndex,
                            String comment,
                            String referenceDosages,
                            Double connexionFrequency,
                            String... columns) {
        try {
            
            writer.write(sheetName);
            writer.write(";");
            
            for (String dynColumn : columns) {
                writer.write(dynColumn);
                writer.write(";");
            }
            
            if (connexionFrequency != null) {
                // only seasonnal
                writer.write(String.valueOf(connexionFrequency));
            } else {
                writer.write("");
            }
            writer.write(";");
            
            // "Année(s)"
            writer.write(years);
            writer.write(";");
            
            // "Approche de calcul"
            String practicedOrEffective = performance.isPracticed() ? "Synthétisé" : "Réalisé";
            writer.write(practicedOrEffective);
            writer.write(";");
            
            // "Catégorie d'indicateur"
            writer.write(l(getLocale(), indicatorCategory));
            writer.write(";");
            
            // "Indicateur"
            writer.write(l(getLocale(), indicatorName));
            writer.write(";");
            
            // "Valeur"
            if (value == null) {
                writer.write("");
            } else {
                if (value instanceof Double) {
                    writer.write(Double.valueOf(df.format(value)).toString());// to be as ISO as possible with PoiIndicatorWriter
                } else {
                    writer.write((String) value);// to be as ISO as possible with PoiIndicatorWriter
                }
            }
            writer.write(";");
            
            // "Rendement"
            if (printableYealdAverages != null) {
                writer.write(printableYealdAverages);
                writer.write(";");
            }
            
            // "Taux de complétion (en %)"
            writer.write(String.valueOf(reliabilityIndex));
            writer.write(";");
            
            // "Détails des champs non renseignés requis pour le calcul de l'indicateur"
            writer.write(comment);
            writer.write(";");
            
            // "Doses de référence utilisées pour le calcul IFT culture"
            if (referenceDosages != null) {
                writer.write(referenceDosages);
                writer.write(";");
            }
            
            writer.write("\n");
            
        } catch (IOException ex) {
            throw new AgrosystTechnicalException("Can't write in memory writer ? seriously ?", ex);
        }
    }

    protected void writeCsv(String... args) {
        try {
            for (String arg : args) {
                writer.write(arg);
                writer.write(";");
            }
            writer.write("\n");
        } catch (IOException ex) {
            throw new AgrosystTechnicalException("Can't write in memory writer ? seriously ?", ex);
        }
    }
    
    @Override
    public void writePracticedSeasonalIntervention(
            String its,
            String irs,
            String campaigns,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            GrowingSystem growingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            int rank,
            CroppingPlanEntry previousPlanEntry,
            CroppingPlanEntry intermediateCrop,
            PracticedIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
    
        CroppingPlanEntry interCrop = intervention.isIntermediateCrop() ? intermediateCrop : croppingPlanEntry;
        
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);

        if (useOriginaleWriterFormat) {
            writeCsv(
                    "interventionSheet",
                    domain.getName(),
                    growingSystem.getName(),
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                    previousPlanEntry.getName(),
                    intervention.getName(),
                    indicatorCategory,
                    indicatorName,
                    campaigns,
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
            double roundSafeValue = 0;
            if (value instanceof Double) {
                roundSafeValue = ((Double) value) + 1e-6;
            }
            writeCsv(
                    "interventionSheet",
                    campaigns, //                                                                    "Année(s)",
                    //                                                                               "Approche de calcul" :implicite
                    //                                                                               Ignore for intervention sheet
                    indicatorCategory, //                                                            "Catégorie d'indicateur"
                    indicatorName, //                                                                "Indicateur"
                    value instanceof Double ? roundSafeValue : value, //                                                                        "Valeur"
                    null,                                                         // Not used for intervention sheet
                    reliabilityIndex, //                                                             "Taux de complétion (en %)"
                    comment, //                                                           "Détails des champs non renseignés requis pour le calcul de l'indicateur",
                    referenceDosages == null ? "" : String.join(", ", referenceDosages), //  "Doses de référence utilisées pour le calcul IFT culture"
                    null,
        
                    // dynamic cells (first fields displayed)
                    getDomainName(domain), //                                                        "Domaine_Exploitation"
                    its, irs, //                                                                     "Nom du réseau de l'IT", "Nom du réseau de l'IR"
                    getGrowingSystemName(Optional.ofNullable(growingSystem)), //                                           "Systèmes de culture"
                    geDephyNumber(Optional.ofNullable(growingSystem)), //                                                 "Numéro DEPHY"
                    getTypeDeConduiteSDC(Optional.ofNullable(growingSystem)), //                                          "Type de conduite du SdC"
                    practicedSystem.getName(), //                                                    "Système synthétisé"
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry != null ? croppingPlanEntry.getName() : "N/A", rank + 1), //        "Culture"
                    intermediateCrop != null ? intermediateCrop.getName() : "", //                   "Affectation CI"
                    getCropSpeciesName(interCrop), //                                                "Espèces"
                    getCropVarietyName(interCrop), //                                                "Variétés"
                    getInterventionSpeciesName(interCrop, intervention), //                          "Espèces concernées par l'intervention",
                    getInterventionVarietyName(interCrop, intervention), //                          "Varietes concernées par l'intervention"
                    previousPlanEntry != null ? previousPlanEntry.getName() : "", //                 "Précédent/Phase"
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(intervention.getType()), //                "Type d'intervention"
                    intervention.getName(), //                                                       "Intervention"
                    intervention.getTopiaId(), //                                 "Identifiant de l'intervention",
                    intervention.getStartingPeriodDate(), //                                         "Date de début"
                    intervention.getEndingPeriodDate(), //                                           "Date de fin"
                    getActionsToString(actions), //                                            "Action(s)"
                    getInputUsagesToString(inputUsages, actions, interCrop),
                    getNbInputUsages(inputUsages),
                    getGroupesCiblesToString(inputUsages, groupesCiblesByCode),
                    getInputUsageTargetsToString(inputUsages),
                    isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle) ? "oui" : "non");
        }
    }
    
    @Override
    public void writeInputUsage(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorName,
            AbstractInputUsage usage,
            AbstractAction action,
            EffectiveIntervention effectiveIntervention,
            PracticedIntervention practicedIntervention,
            Collection<String> codeAmmBioControle,
            Domain domain,
            GrowingSystem growingSystem,
            Plot plot,
            Zone zone,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase practicedCropCyclePhase,
            Double solOccupationPercent,
            EffectiveCropCyclePhase effectiveCropCyclePhase,
            Integer rank,
            CroppingPlanEntry previousCrop,
            CroppingPlanEntry intermediateCrop0,
            Class<? extends GenericIndicator> indicatorClass,
            Object value,
            Integer reliabilityIndexForInputId,
            String reliabilityCommentForInputId,
            String iftRefDoseUserInfos,
            String iftRefDoseValue,
            String iftRefDoseUnit,
            Map<String, String> groupesCiblesByCode) {

        String biocontrole = null;
        final Optional<Boolean> maybeInputUsageBiocontrole = isInputUsageBiocontrole(usage, codeAmmBioControle);
        if (maybeInputUsageBiocontrole.isPresent()) {
             biocontrole = maybeInputUsageBiocontrole.get() ? "oui" : "non";
        }

        // ajout d'1e-6 pour contrer les potentielles erreurs d'arrondi
        final double roundSafeValue = value instanceof Double ? (Double) value + 1e-6 : 0.0d;
        final String valueToWrite = this.useOriginaleWriterFormat ? String.valueOf(value) : df.format(roundSafeValue);
        if (practicedSystem != null) {
        
            String precedentOrPhaseType;
            if (practicedCropCyclePhase != null) {
                precedentOrPhaseType = AgrosystI18nService.getEnumTraductionWithDefaultLocale(practicedCropCyclePhase.getType());
            } else if (previousCrop != null) {
                precedentOrPhaseType = previousCrop.getName();
            } else {
                precedentOrPhaseType = "";
            }

            CroppingPlanEntry intermediateCrop = null;
            if (practicedIntervention.isIntermediateCrop()) {
                intermediateCrop = intermediateCrop0;
            }
        
            String cultureName;
            if (rank != null) {
                cultureName = l(Locale.FRANCE,
                        CROP_NAME_RANK,
                        croppingPlanEntry != null ? croppingPlanEntry.getName() :
                                "N/A", rank + 1);
            } else {
                cultureName = croppingPlanEntry.getName();
            }

            writeCsv(
                    "inputSheet",
                    // dynamic cells (first fields displayed)
                    getDomainName(domain),                                             //"Domaine_Exploitation",
                    its,                                                                        //"Nom du réseau de l'IT",
                    irs,                                                                        //"Nom du réseau de l'IR",
                    getGrowingSystemName((Optional.ofNullable(growingSystem))),                         //"Systèmes de culture",
                    geDephyNumber((Optional.ofNullable(growingSystem))),                                //"Numéro DEPHY",
                    getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),                         //"Type de conduite du SdC",
                    practicedSystem.getName(),                                                  //"Système synthétisé",
                    cultureName,                                                //"Culture",
                    intermediateCrop != null ? intermediateCrop.getName() : "",                // "Affectation CI"
                    getCropSpeciesName(croppingPlanEntry),                      //"Espèces",
                    getCropVarietyName(croppingPlanEntry),                      //"Variétés",
                    getInterventionSpeciesName(croppingPlanEntry, practicedIntervention),//"Espèces concernées par l'intervention",
                    getInterventionVarietyName(croppingPlanEntry, practicedIntervention),//"Variétés concernées par l'intervention",
                    precedentOrPhaseType,                     //"Précédent/Phase",
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(practicedIntervention.getType()),              //"Type d'intervention",
                    practicedIntervention.getName(),                                                     //"Intervention",
                    practicedIntervention.getStartingPeriodDate(),                                       //"Date de début",
                    practicedIntervention.getEndingPeriodDate(),                                         //"Date de fin",
                    getActionToString(action),                                //"Action",
                    getInputUsageProductName(usage, action, intermediateCrop, croppingPlanEntry, true),//"Intrant
                    getTargetsToString(usage),                                 //"Cible(s) de traitement"
                    biocontrole, //"Biocontrôle"
                    practicedSystem.getCampaigns(),
                    indicatorCategory,
                    indicatorName,
                    valueToWrite,
                    String.valueOf(reliabilityIndexForInputId),
                    reliabilityCommentForInputId,
                    Strings.nullToEmpty(iftRefDoseUserInfos));
        } else {
        
            String precedentOrPhaseType;
            if (effectiveCropCyclePhase != null) {
                precedentOrPhaseType = AgrosystI18nService.getEnumTraductionWithDefaultLocale(effectiveCropCyclePhase.getType());
            } else if (previousCrop != null) {
                precedentOrPhaseType = previousCrop.getName();
            } else {
                precedentOrPhaseType = "";
            }

            CroppingPlanEntry intermediateCrop = null;
            if (effectiveIntervention.isIntermediateCrop()) {
                intermediateCrop = intermediateCrop0;
            }
        
            writeCsv(
                    "inputSheet",
                    // dynamic cells (first fields displayed)
                    getDomainName(domain),                                             //"Domaine_Exploitation",
                    its,                                                                        //"Nom du réseau de l'IT",
                    irs,                                                                        //"Nom du réseau de l'IR",
                    getGrowingSystemName((Optional.ofNullable(growingSystem))),                         //"Systèmes de culture",
                    geDephyNumber((Optional.ofNullable(growingSystem))),                                //"Numéro DEPHY",
                    getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),                         //"Type de conduite du SdC",
                    plot.getName(),                                                             //"Parcelle",
                    zone.getName(),                                                             //"Zone",
                    String.valueOf(zone.getArea()),                                             //"Surface de la zone (en ha)",
                    croppingPlanEntry.getName(),                                                //"Culture"
                    intermediateCrop != null ? intermediateCrop.getName() : "",                 //"Affectation CI"
                    getCropSpeciesName(croppingPlanEntry),                      //"Espèces",
                    getCropVarietyName(croppingPlanEntry),                      //"Variétés",
                    getInterventionSpeciesName(effectiveIntervention),          //"Espèces concernées par l'intervention",
                    getInterventionVarietyName(effectiveIntervention),          //"Variétés concernées par l'intervention",
                    precedentOrPhaseType,                     //"Précédent/Phase",
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(effectiveIntervention.getType()),     //"Type d'intervention",
                    effectiveIntervention.getName(),                                            //"Intervention",
                    INTERVENTION_DATE_FORMAT_DDMMYYYY.format(effectiveIntervention.getStartInterventionDate()),//"Date de début",
                    INTERVENTION_DATE_FORMAT_DDMMYYYY.format(effectiveIntervention.getEndInterventionDate()),//"Date de fin",
                    getActionToString(action),                                  //"Action",
                    getInputUsageProductName(usage, action, intermediateCrop, croppingPlanEntry, true),//"Intrant",
                    getTargetsToString(usage),                                  //"Cible(s) de traitement"
                    biocontrole, //"Biocontrôle"
                    String.valueOf(domain.getCampaign()),
                    indicatorCategory,
                    indicatorName,
                    valueToWrite,
                    String.valueOf(reliabilityIndexForInputId),
                    reliabilityCommentForInputId,
                    Strings.nullToEmpty(iftRefDoseUserInfos));
        }
    }
    
    @Override
    public void writePracticedPerennialIntervention(
            String its,
            String irs,
            String campaigns,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            GrowingSystem growingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase phase,
            Double perennialCropCyclePercent,
            PracticedIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
    
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
        if (useOriginaleWriterFormat) {
            writeCsv(
                    "interventionSheet",
                    domain.getName(),
                    growingSystem.getName(),
                    croppingPlanEntry.getName(),
                    phase.getType().name(),
                    intervention.getName(),
                    indicatorCategory,
                    indicatorName,
                    campaigns,
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
            final double roundSafeValue = value instanceof Double ? (Double) value + 1e-6 : 0.0d;
            writeCsv(
                    "interventionSheet",
                    campaigns,
                    indicatorCategory,
                    indicatorName,
                    roundSafeValue,
                    "", reliabilityIndex, comment,
                    referenceDosages == null ? "" : String.join(", ", referenceDosages),
        
                    // dynamic cells (first fields displayed)
                    null, getDomainName(domain),
                    its, irs,
                    getGrowingSystemName((Optional.ofNullable(growingSystem))),
                    geDephyNumber((Optional.ofNullable(growingSystem))),
                    getTypeDeConduiteSDC((Optional.ofNullable(growingSystem))),
                    practicedSystem.getName(),
                    croppingPlanEntry.getName(),
                    "", // "Affectation CI" non applicable for perennial
                    getCropSpeciesName(croppingPlanEntry),
                    getCropVarietyName(croppingPlanEntry),
                    getInterventionSpeciesName(croppingPlanEntry, intervention),
                    getInterventionVarietyName(croppingPlanEntry, intervention),
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(phase.getType()),
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(intervention.getType()),
                    intervention.getName(),
                    intervention.getTopiaId(),
                    intervention.getStartingPeriodDate(),
                    intervention.getEndingPeriodDate(),
                    getActionsToString(actions),
                    getInputUsagesToString(inputUsages, actions, croppingPlanEntry),
                    getNbInputUsages(inputUsages),
                    getGroupesCiblesToString(inputUsages, groupesCiblesByCode),
                    getInputUsageTargetsToString(inputUsages),
                    isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle) ? "oui" : "non");
        }
    }
    
    @Override
    public void writePracticedSeasonalCrop(String its,
                                           String irs,
                                           String campaigns,
                                           Double connexionFrequency,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Integer rank,
                                           Domain domain,
                                           GrowingSystem growingSystem,
                                           PracticedSystem practicedSystem,
                                           CroppingPlanEntry croppingPlanEntry,
                                           CroppingPlanEntry previousPlanEntry,
                                           String culturePrecedentRangId,
                                           Class<? extends GenericIndicator> indicatorClass, PracticedCropCycleConnection practicedCropCycleConnection) {
        
        
        if (useOriginaleWriterFormat) {
            writeCsv(
                    "croppingPlanSheet",
                    domain.getName(), growingSystem.getName(),
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1), previousPlanEntry.getName(),
                    connexionFrequency == null ? "" : connexionFrequency.toString(),
                    indicatorCategory, indicatorName, campaigns, domain.getName(),
                    String.valueOf(value), String.valueOf(reliabilityIndex), comment);
        } else {
            writeCsv(
                    "croppingPlanSheet",
                    campaigns,
                    indicatorCategory,
                    indicatorName,
                    value,
                    GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage), reliabilityIndex,
                    comment,
                    // dynamic cells
                    null,
                    connexionFrequency,
                    domain.getName(), irs,
                    its,
                    getGrowingSystemName(Optional.ofNullable(growingSystem)),
                    geDephyNumber(Optional.ofNullable(growingSystem)),
                    getTypeDeConduiteSDC(Optional.ofNullable(growingSystem)),
                    practicedSystem.getName(),
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                    getCropSpeciesName(croppingPlanEntry),
                    getCropVarietyName(croppingPlanEntry),
                    //  "Précédent/Phase"
                    previousPlanEntry != null ? previousPlanEntry.getName() : "",// "% des cultures pérennes" not applicable for seasonal
                    "");
        }
    }
    
    @Override
    public void writePracticedPerennialCop(String its,
                                           String irs,
                                           String campaigns,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Domain domain,
                                           GrowingSystem growingSystem,
                                           PracticedSystem practicedSystem,
                                           CroppingPlanEntry croppingPlanEntry,
                                           PracticedCropCyclePhase phase,
                                           Double perennialCropCyclePercent,
                                           Class<? extends GenericIndicator> indicatorClass) {
        writeCsv(domain.getName(), growingSystem.getName(),
                croppingPlanEntry.getName(), phase.getType().name(),
                indicatorCategory, indicatorName, campaigns, domain.getName(), String.valueOf(value), String.valueOf(reliabilityIndex), comment);
    }
    
    @Override
    public void writePracticedSystem(String its,
                                     String irs,
                                     String campaigns,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     GrowingSystem growingSystem,
                                     PracticedSystem practicedSystem,
                                     Class<? extends GenericIndicator> indicatorClass) {
        
        if (useOriginaleWriterFormat) {
            writeCsv("practicedSystemSheet", domain.getName(), growingSystem.getName(), practicedSystem.getName(),
                    indicatorCategory, indicatorName, campaigns, domain.getName(), String.valueOf(value), String.valueOf(reliabilityIndex), comment);
        } else {
            
            writeCsv("practicedSystemSheet", campaigns, indicatorCategory, indicatorName, value, null, reliabilityIndex, comment,
                    null,
        
                    // dynamic cells
                    null,
                    domain.getName(), irs,
                    its,
                    getGrowingSystemName(Optional.ofNullable(growingSystem)),
                    geDephyNumber(Optional.ofNullable(growingSystem)), //XXX order change since #10412
                    getTypeDeConduiteSDC(Optional.ofNullable(growingSystem)),
                    getApprovedGrowingSystem(Optional.ofNullable(growingSystem)),
                    practicedSystem.getName(), practicedSystem.isValidated() ? "OUI" : "NON");
        }
    }
    
    @Override
    public void writePracticedDomain(String campaigns,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     String growingSystemsTypeAgricultureLabels) {
        
        if (useOriginaleWriterFormat) {
            writeCsv("domainSheet", domain.getName(),
                    indicatorCategory, indicatorName, campaigns, domain.getName(), String.valueOf(value), String.valueOf(reliabilityIndex), comment);
        } else {
            
            writeCsv("domainSheet",
                    campaigns,
                    indicatorCategory,
                    indicatorName,
                    value,
                    null,
                    reliabilityIndex,
                    comment,
                    null,
                    null,
                    domain.getName(),
                    growingSystemsTypeAgricultureLabels);
        }
    }

    @Override
    public void writePrices(
            RefCampaignsInputPricesByDomainInput domainInputPriceRefPrices,
            List<HarvestingPrice> harvestingPrices,
            Map<String, HarvestingValorisationPriceSummary> refHarvestingPrices,
            List<Equipment> equipments,
            Domain domain,
            PracticedSystem practicedSystem, RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
            MultiKeyMap<String, List<RefHarvestingPrice>> refHarvestingPricesByCodeEspeceAndCodeDestination,
            List<CroppingPlanSpecies> croppingPlanSpecies) {

        Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> allPriceRefPrices = new HashMap<>();
        allPriceRefPrices.putAll(domainInputPriceRefPrices.mineralRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.speciesRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.fertiOrgaRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.phytoRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.irrigRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.fuelRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.otherRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.potRefPriceForInput());
        allPriceRefPrices.putAll(domainInputPriceRefPrices.substrateRefPriceForInput());

        for (Map.Entry<AbstractDomainInputStockUnit, Optional<InputRefPrice>> domainInputOptionalRefPrice : allPriceRefPrices.entrySet()) {
            writeDomainInputPriceRow(domainInputOptionalRefPrice, domain, practicedSystem);
        }

        if (CollectionUtils.isNotEmpty(harvestingPrices)) {
            for (HarvestingPrice harvestingPrice : harvestingPrices) {
                String valorisationId = harvestingPrice.getHarvestingActionValorisation().getTopiaId();
                HarvestingValorisationPriceSummary harvestingValorisationPriceSummary = refHarvestingPrices.get(valorisationId);
                writeHarvestingPriceRow(harvestingPrice, harvestingValorisationPriceSummary, domain, practicedSystem);
            }
        }

        MultiValuedMap<RefMateriel, Equipment> equipmentsByMateriel = new ArrayListValuedHashMap<>();
        for (Equipment equipment : equipments) {
            equipmentsByMateriel.put(equipment.getRefMateriel(), equipment);
        }
        equipmentsByMateriel.keySet().stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(materiel -> materiel.getClass().getName()
                        + equipmentsByMateriel.get(materiel).stream()
                        .map(Equipment::getName)
                        .filter(Objects::nonNull)
                        .min(Comparator.naturalOrder())
                        .orElse("")))
                .forEachOrdered(materiel -> writeEquipmentRow(materiel, equipmentsByMateriel.get(materiel), practicedSystem));
    }

    @Override
    public void writeEffectiveSeasonalIntervention(
            String its,
            String irs,
            int campaign,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String reliabilityComment,
            List<String> referenceDosages,
            Domain domain,
            Optional<GrowingSystem> optionalGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            CroppingPlanEntry intermediateCrop,
            int rank,
            CroppingPlanEntry previousPlanEntry,
            EffectiveIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverages,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
    
        CroppingPlanEntry interCrop = intervention.isIntermediateCrop() ? intermediateCrop : croppingPlanEntry;
    
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
        
        if (useOriginaleWriterFormat) {
            writeCsv(
                    "interventionSheet",
                    domain.getName(), getGrowingSystemName(optionalGrowingSystem), plot.getName(), zone.getName(),
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                    intermediateCrop != null ? intermediateCrop.getName() : "",
                    previousPlanEntry != null ? previousPlanEntry.getName() : t(NO_PREVIOUS_CROP),
                    intervention.getName(),
                    indicatorCategory, indicatorName,
                    String.valueOf(campaign), domain.getName(), String.valueOf(value), String.valueOf(reliabilityIndex), reliabilityComment);
        } else {
            writeCsv("interventionSheet",
                    String.valueOf(campaign),
                    indicatorCategory,
                    indicatorName,
                    value,
                    "", reliabilityIndex, reliabilityComment,
                    referenceDosages == null ? "" : String.join(", ", referenceDosages),
                    // dynamic cells
                    null, getDomainName(domain),
                    its, irs,
                    getGrowingSystemName(optionalGrowingSystem),
                    geDephyNumber(optionalGrowingSystem),
                    getTypeDeConduiteSDC(optionalGrowingSystem),
                    plot.getName(),
                    zone.getName(),
                    String.valueOf(zone.getArea()),
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                    intermediateCrop != null ? intermediateCrop.getName() : "",
                    getCropSpeciesName(interCrop),
                    getCropVarietyName(interCrop),
                    getInterventionSpeciesName(intervention),
                    getInterventionVarietyName(intervention),
                    previousPlanEntry != null ? previousPlanEntry.getName() : t(NO_PREVIOUS_CROP),
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(intervention.getType()),
                    intervention.getName(),
                    intervention.getTopiaId(),
                    INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getStartInterventionDate()),
                    INTERVENTION_DATE_FORMAT_DDMMYYYY.format(intervention.getEndInterventionDate()),
                    getActionsToString(actions),
                    getInputUsagesToString(inputUsages, actions, interCrop),
                    getNbInputUsages(inputUsages),
                    getGroupesCiblesToString(inputUsages, groupesCiblesByCode),
                    getInputUsageTargetsToString(inputUsages),
                    isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle) ? "oui" : "non");
        }
    }
    
    @Override
    public void writeEffectivePerennialIntervention(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorName,
            Object value,
            Integer reliabilityIndex,
            String comment,
            List<String> referenceDosages,
            Domain domain,
            Optional<GrowingSystem> optionalGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            EffectiveCropCyclePhase phase,
            EffectiveIntervention intervention,
            Collection<AbstractAction> actions,
            Collection<String> codeAmmBioControle,
            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
            Map<String, String> groupesCiblesByCode,
            Class<? extends GenericIndicator> indicatorClass) {
    
        Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(actions, domain);
        
        if (useOriginaleWriterFormat) {
            writeCsv(
                    "interventionSheet",
                    domain.getName(),
                    getGrowingSystemName(optionalGrowingSystem),
                    plot.getName(),
                    zone.getName(),
                    croppingPlanEntry.getName(),
                    phase.getType().name(),
                    intervention.getName(),
                    indicatorCategory,
                    indicatorName,
                    String.valueOf(domain.getCampaign()),
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
            writeCsv("interventionSheet",
                    String.valueOf(domain.getCampaign()),
                    indicatorCategory,
                    indicatorName,
                    value, // not display for intervention
                    "", reliabilityIndex,
                    comment,
                
                    // dynamic cells:
                    referenceDosages == null ? "" : String.join(", ", referenceDosages),
                    null,
                    getDomainName(domain), irs,
                    its,
                    getGrowingSystemName(optionalGrowingSystem),
                    geDephyNumber(optionalGrowingSystem),
                    getTypeDeConduiteSDC(optionalGrowingSystem),
                    plot.getName(),
                    zone.getName(),
                    croppingPlanEntry.getName(),// "Affectation CI"
                    "",
                    getCropSpeciesName(croppingPlanEntry),
                    getCropVarietyName(croppingPlanEntry),
                    getInterventionSpeciesName(intervention),
                    getInterventionVarietyName(intervention),
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(phase.getType()),
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(intervention.getType()),
                    intervention.getName(),
                    getActionsToString(actions),
                    getInputUsagesToString(inputUsages, actions, croppingPlanEntry),
                    getInputUsageTargetsToString(inputUsages),
                    isOneInputUsageBiocontrole(inputUsages, codeAmmBioControle) ? "0" : "N");
        }
    }
    
    @Override
    public void writeEffectiveSeasonalCrop(String its,
                                           String irs,
                                           int campaign,
                                           String indicatorCategory,
                                           String indicatorName,
                                           Object value,
                                           Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                           Integer reliabilityIndex,
                                           String comment,
                                           Domain domain,
                                           Optional<GrowingSystem> optionalGrowingSystem,
                                           Plot plot,
                                           CroppingPlanEntry croppingPlanEntry,
                                           Integer rank,
                                           CroppingPlanEntry previousPlanEntry,
                                           Class<? extends GenericIndicator> indicatorClass) {
        
        if (useOriginaleWriterFormat) {
            writeCsv(
                    "croppingPlanSheet",
                    domain.getName(),
                    getGrowingSystemName(optionalGrowingSystem),
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                    previousPlanEntry != null ? previousPlanEntry.getName() : t(NO_PREVIOUS_CROP),
                    indicatorCategory,
                    indicatorName,
                    String.valueOf(campaign),
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
            writeCsv(
                    "croppingPlanSheet",
                    String.valueOf(campaign),
                    indicatorCategory,
                    indicatorName,
                    value,
                    GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverage), reliabilityIndex,
                    comment,
                    // dynamic cells
                    null, null,
                    domain.getName(), irs,
                    its,
                    getGrowingSystemName(optionalGrowingSystem),
                    geDephyNumber(optionalGrowingSystem),
                    getTypeDeConduiteSDC(optionalGrowingSystem),
                    plot.getName(),
                    l(Locale.FRANCE, CROP_NAME_RANK, croppingPlanEntry.getName(), rank + 1),
                    getCropSpeciesName(croppingPlanEntry),
                    getCropVarietyName(croppingPlanEntry),
                    previousPlanEntry != null ? previousPlanEntry.getName() : t(NO_PREVIOUS_CROP));
        }
    }
    
    @Override
    public void writeEffectivePerennialCrop(String its,
                                            String irs,
                                            int campaign,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object value,
                                            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverages,
                                            Integer reliabilityIndex,
                                            String comment,
                                            Domain domain,
                                            Optional<GrowingSystem> optionalGrowingSystem,
                                            Plot plot,
                                            CroppingPlanEntry croppingPlanEntry,
                                            EffectiveCropCyclePhase phase,
                                            Class<? extends GenericIndicator> indicatorClass) {
        
        if (useOriginaleWriterFormat) {
            writeCsv("croppingPlanSheet",
                    domain.getName(),
                    getGrowingSystemName(optionalGrowingSystem),
                    croppingPlanEntry.getName(),
                    phase.getType().name(),
                    indicatorCategory,
                    indicatorName,
                    String.valueOf(campaign),
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
            writeCsv(
                    "croppingPlanSheet",
                    String.valueOf(campaign),
                    indicatorCategory,
                    indicatorName,
                    value,
                    GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(cropYealdAverages),
                    reliabilityIndex,
                    comment,
                    // dynamic cells
                    null, null,
                    domain.getName(), irs,
                    its,
                    getGrowingSystemName(optionalGrowingSystem),
                    geDephyNumber(optionalGrowingSystem),
                    getTypeDeConduiteSDC(optionalGrowingSystem),
                    croppingPlanEntry.getName(),
                    getCropSpeciesName(croppingPlanEntry),
                    getCropVarietyName(croppingPlanEntry),
                    AgrosystI18nService.getEnumTraductionWithDefaultLocale(phase.getType()));
        }
        
    }
    
    @Override
    public void writeEffectiveZone(String its,
                                   String irs,
                                   String indicatorCategory,
                                   String indicatorName,
                                   Object value,
                                   Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald,
                                   Integer reliabilityIndex,
                                   String comment,
                                   Domain anonymiseDomain,
                                   List<CroppingPlanSpecies> domainCroppingPlanSpecies,
                                   Optional<GrowingSystem> optionalGrowingSystem,
                                   Plot plot,
                                   Zone zone,
                                   String speciesNames,
                                   String varietyNames,
                                   Class<? extends GenericIndicator> indicatorClass) {
        
        if (useOriginaleWriterFormat) {
            
            writeCsv("zoneSheet",
                   anonymiseDomain.getName(),
                    getGrowingSystemName(optionalGrowingSystem),
                    plot.getName(),
                    zone.getName(),
                    indicatorCategory,
                    indicatorName,
                    String.valueOf(anonymiseDomain.getCampaign()),
                    anonymiseDomain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
    
            writeCsv("zoneSheet",
                    String.valueOf(anonymiseDomain.getCampaign()),
                    indicatorCategory,
                    indicatorName,
                    value,
                    GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(zoneAverageYeald),
                    reliabilityIndex,
                    comment,
                    null,
                    null,
                    anonymiseDomain.getName(),
                    irs,
                    its,
                    getGrowingSystemName(optionalGrowingSystem),
                    geDephyNumber(optionalGrowingSystem),
                    getTypeDeConduiteSDC(optionalGrowingSystem),
                    plot.getName(),
                    zone.getName(),
                    speciesNames,
                    varietyNames);
        }
    }
    
    @Override
    public void writeEffectivePlot(String its,
                                   String irs,
                                   String indicatorCategory,
                                   String indicatorName,
                                   Object value,
                                   Integer reliabilityIndex,
                                   String comment,
                                   Domain domain,
                                   Collection<CroppingPlanSpecies> domainCroppingPlanSpecies,
                                   Optional<GrowingSystem> optionalGrowingSystem,
                                   Plot plot,
                                   Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages,
                                   Class<? extends GenericIndicator> indicatorClass,
                                   String zoneIds) {
        
        if (useOriginaleWriterFormat) {
            
            writeCsv("plotSheet",
                    domain.getName(),
                    getGrowingSystemName(optionalGrowingSystem),
                    plot.getName(),
                    indicatorCategory,
                    indicatorName,
                    String.valueOf(domain.getCampaign()),
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
            
            writeCsv("plotSheet",
                    String.valueOf(domain.getCampaign()),
                    indicatorCategory,
                    indicatorName,
                    value,
                    GET_PRINTABLE_YEALD_AVERAGE_FOR_DESTINATION.apply(plotYealdAverages),
                    reliabilityIndex,
                    comment,
                    null,
                    null,
                    domain.getName(),
                    irs,
                    its,
                    getGrowingSystemName(optionalGrowingSystem),
                    geDephyNumber(optionalGrowingSystem),
                    getTypeDeConduiteSDC(optionalGrowingSystem),
                    plot.getName());
        }
    }
    
    @Override
    public void writeEffectiveGrowingSystem(String its,
                                            String irs,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object value,
                                            Integer reliabilityIndex,
                                            String comment,
                                            Domain domain,
                                            Optional<GrowingSystem> optionalGrowingSystem,
                                            Class<? extends GenericIndicator> indicatorClass,
                                            Collection<CroppingPlanSpecies> agrosystSpecies) {
    
        String campaign = String.valueOf(domain.getCampaign());
        
        if (useOriginaleWriterFormat) {
            writeCsv("growingSystemSheet",
                    domain.getName(),
                    getGrowingSystemName(optionalGrowingSystem),
                    indicatorCategory,
                    indicatorName,
                    campaign,
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
    
            writeCsv(
                    "growingSystemSheet",
                    campaign,
                    indicatorCategory,
                    indicatorName,
                    value,
                    "",
                    reliabilityIndex,
                    comment,
                    // dynamic cells
                    null, null,
                    domain.getName(), irs,
                    its,
                    getGrowingSystemName(optionalGrowingSystem),
                    geDephyNumber(optionalGrowingSystem),
                    getTypeDeConduiteSDC(optionalGrowingSystem), getApprovedGrowingSystem(optionalGrowingSystem));
        }
    }
    
    @Override
    public void writeEffectiveDomain(String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     String growingSystemsTypeAgricultureLabels) {
        
        if (useOriginaleWriterFormat) {
            writeCsv("domainSheet",
                    domain.getName(),
                    indicatorCategory,
                    indicatorName,
                    String.valueOf(domain.getCampaign()),
                    domain.getName(),
                    String.valueOf(value),
                    String.valueOf(reliabilityIndex),
                    comment);
        } else {
    
            writeCsv("domainSheet",
                    String.valueOf(domain.getCampaign()),
                    indicatorCategory,
                    indicatorName,
                    value,
                    "",
                    reliabilityIndex,
                    comment,
                    null,
                    null,
                    domain.getName(),
                    growingSystemsTypeAgricultureLabels);
        }
    }

    @Override
    public void writeEffectiveItk(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorLabel,
            Object itkValue,
            Integer effectiveItkCropPrevCropPhaseReliabilityIndex,
            String effectiveItkCropPrevCropPhaseComments,
            Domain anonymiseDomain,
            Optional<GrowingSystem> optionalAnoGrowingSystem,
            Plot plot,
            Zone anonymizeZone,
            Class<? extends GenericIndicator> aClass,
            EffectiveItkCropCycleScaleKey key) {
    
    }
    
    protected void writeDomainInputPriceRow(
            Map.Entry<AbstractDomainInputStockUnit, Optional<InputRefPrice>> domainInputOptionalRefPrice,
            Domain domain,
            PracticedSystem practicedSystem){

        AbstractDomainInputStockUnit domainInput = domainInputOptionalRefPrice.getKey();
        InputPrice inputPrice = domainInput.getInputPrice();
        try {
            if (inputPrice == null) {
                return;
            }

            if (!useOriginaleWriterFormat) {
                writer.write("priceSheet;");
            }

            // Domaine
            if (domain != null) {
                writer.write(domain.getName());
            }
            writer.write(";");
            // Campagne
            if (domain != null) {
                writer.write(String.valueOf(domain.getCampaign()));
            }
            if (practicedSystem == null && domain != null) {
                writer.write(String.valueOf(domain.getCampaign()));
            } else if (practicedSystem != null) {
                writer.write(practicedSystem.getCampaigns());
            }
            writer.write(";");
            // Type
            String type = AgrosystI18nService.getEnumTraductionWithDefaultLocale(InputPriceCategory.HARVESTING_ACTION);
            writer.write(type);
            writer.write(";");
            // Nom
            String libelle;
            final String startingFormattingPattern = "<span class=\"warning-label\"><i class=\"fa fa-warning\" aria-hidden=\"true\"></i>";
            final String endingFormattingPattern = "</span>";
            libelle = Strings.nullToEmpty(StringUtils.firstNonBlank(inputPrice.getDisplayName(), domainInput.getInputName()));
            libelle = StringUtils.replace(libelle, startingFormattingPattern, "/!\\ ");
            libelle = StringUtils.replace(libelle, endingFormattingPattern, "");
            writer.write(libelle);
            writer.write(";");
            // Complément information
            writer.write(";");
            // Durée de vie théorique des matériels (années)
            writer.write(";");
            // Investissement matériels (euro)
            writer.write(";");

            Double refInputPriceValue = null;
            PriceUnit refInputPriceUnit = null;
            String refInputPriceSource = null;

            Optional<InputRefPrice> optionalRefPrice = domainInputOptionalRefPrice.getValue();
            if (optionalRefPrice.isPresent()) {
                InputRefPrice refInputPrice = optionalRefPrice.get();
                refInputPriceValue = refInputPrice.averageRefPrice().value();
                refInputPriceUnit = refInputPrice.averageRefPrice().unit();
            }

            // Prix utilisés pour calculs en réel
            Double priceValue = inputPrice.getPrice();
            Double value;
            if (priceValue != null) {
                value = priceValue;
            } else {
                value = refInputPriceValue;
            }
            if (value != null) {
                writer.write(String.valueOf(Precision.round(value, 2, RoundingMode.HALF_UP.ordinal())));
            } else {
                writer.write("-");
            }
            writer.write(";");
            // Unité
            String priceUnit;
            if (inputPrice.getPrice() != null && inputPrice.getPriceUnit() != null) {
                priceUnit = AgrosystI18nService.getEnumTraductionWithDefaultLocale(inputPrice.getPriceUnit());
            } else if (refInputPriceUnit != null) {
                priceUnit = AgrosystI18nService.getEnumTraductionWithDefaultLocale(refInputPriceUnit);
            } else {
                priceUnit = "-";
            }
            writer.write(priceUnit);
            writer.write(";");
            // Prix pour mode de calcul Standardisé - Millésimé
            if (refInputPriceValue != null) {
                writer.write(String.valueOf(Precision.round(refInputPriceValue, 2, RoundingMode.HALF_UP.ordinal())));
            } else {
                writer.write("-");
            }
            writer.write(";");
            // Unité
            String refPriceUnit = refInputPriceUnit != null ? AgrosystI18nService.getEnumTraductionWithDefaultLocale(refInputPriceUnit) : "-";
            writer.write(refPriceUnit);
            writer.write(";");
            writer.write(Objects.toString(refInputPriceSource, ExportUtils.NULL_SOURCE_PLACEHOLDER));
            writer.write(";");
            writer.write("\n");
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't write in memory writer ? seriously ?", ex);
        }
    }

    protected void writeHarvestingPriceRow(
            HarvestingPrice harvestingPrice,
            HarvestingValorisationPriceSummary harvestingValorisationPriceSummary,
            Domain domain,
            PracticedSystem practicedSystem
    ) {
        try {
            if (!useOriginaleWriterFormat) {
                writer.write("priceSheet;");
            }
            // Domaine
            if (domain != null) {
                writer.write(domain.getName());
            }
            writer.write(";");
            // Campagne
            if (domain != null) {
                writer.write(String.valueOf(domain.getCampaign()));
            }
            if (practicedSystem == null && domain != null) {
                writer.write(String.valueOf(domain.getCampaign()));
            } else if (practicedSystem != null) {
                writer.write(practicedSystem.getCampaigns());
            }
            writer.write(";");
            // Type
            String type = AgrosystI18nService.getEnumTraductionWithDefaultLocale(InputPriceCategory.HARVESTING_ACTION);
            writer.write(type);
            writer.write(";");
            // Nom
            String libelle;
            final String startingFormattingPattern = "<span class=\"warning-label\"><i class=\"fa fa-warning\" aria-hidden=\"true\"></i>";
            final String endingFormattingPattern = "</span>";
            libelle = Strings.nullToEmpty(harvestingPrice.getDisplayName());
            libelle = StringUtils.replace(libelle, startingFormattingPattern, "/!\\ ");
            libelle = StringUtils.replace(libelle, endingFormattingPattern, "");
            writer.write(libelle);
            writer.write(";");
            // Complément information
            String information = Optional.of(harvestingPrice)
                    .map(HarvestingPrice::getHarvestingActionValorisation)
                    .map(HarvestingActionValorisation::getDestination)
                    .map(RefDestination::getDestination)
                    .orElse(null);

            if (information != null) {
                writer.write(information);
            }
            writer.write(";");
            // Durée de vie théorique des matériels (années)
            writer.write(";");
            // Investissement matériels (euro)
            writer.write(";");

            Double refInputPriceValue = null;
            PriceUnit refInputPriceUnit = null;
            String refInputPriceSource = null;

            final HarvestingActionValorisation harvestingActionValorisation = harvestingPrice.getHarvestingActionValorisation();
            if (harvestingActionValorisation != null) {
                if (harvestingValorisationPriceSummary != null) {
                    refInputPriceValue = harvestingValorisationPriceSummary.getAveragePrice();
                    if (refInputPriceValue != null) {
                        refInputPriceUnit = harvestingValorisationPriceSummary.getPriceUnit();
                        refInputPriceSource = harvestingValorisationPriceSummary.getSources().entrySet().stream()
                                .sorted(Map.Entry.comparingByKey())
                                .map(Map.Entry::getValue)
                                .collect(Collectors.joining(", "));
                    }
                }

            }

            // Prix utilisés pour calculs en réel
            Double priceValue = harvestingPrice.getPrice();
            Double value;
            if (priceValue != null) {
                value = priceValue;
            } else {
                value = refInputPriceValue;
            }
            if (value != null) {
                writer.write(String.valueOf(Precision.round(value, 2, RoundingMode.HALF_UP.ordinal())));
            } else {
                writer.write("-");
            }
            writer.write(";");
            // Unité
            String priceUnit;
            if (harvestingPrice.getPrice() != null && harvestingPrice.getPriceUnit() != null) {
                priceUnit = AgrosystI18nService.getEnumTraductionWithDefaultLocale(harvestingPrice.getPriceUnit());
            } else if (refInputPriceUnit != null) {
                priceUnit = AgrosystI18nService.getEnumTraductionWithDefaultLocale(refInputPriceUnit);
            } else {
                priceUnit = "-";
            }
            writer.write(priceUnit);
            writer.write(";");
            // Prix pour mode de calcul Standardisé - Millésimé
            if (refInputPriceValue != null) {
                writer.write(String.valueOf(Precision.round(refInputPriceValue, 2, RoundingMode.HALF_UP.ordinal())));
            } else {
                writer.write("-");
            }
            writer.write(";");
            // Unité
            String refPriceUnit = refInputPriceUnit != null ? AgrosystI18nService.getEnumTraductionWithDefaultLocale(refInputPriceUnit) : "-";
            writer.write(refPriceUnit);
            writer.write(";");
            writer.write(Objects.toString(refInputPriceSource, ExportUtils.NULL_SOURCE_PLACEHOLDER));
            writer.write(";");
            writer.write("\n");
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't write in memory writer ? seriously ?", ex);
        }
    }

    protected void writeEquipmentRow(RefMateriel refMateriel, Collection<Equipment> equipments, PracticedSystem practicedSystem) {
        try {
            long nbDomains = equipments.stream()
                    .map(Equipment::getDomain)
                    .map(Domain::getCode)
                    .distinct()
                    .count();
            Validate.isTrue(nbDomains == 1);

            // new line
            Domain domain = equipments.stream()
                    .map(Equipment::getDomain)
                    .findFirst()
                    .orElse(null);

            String campaigns = equipments.stream()
                    .map(Equipment::getDomain)
                    .map(Domain::getCampaign)
                    .distinct()
                    .map(Object::toString)
                    .collect(Collectors.joining(", "));

            String equipmentNames = equipments.stream()
                    .map(Equipment::getName)
                    .distinct()
                    .collect(Collectors.joining(", "));

            // Domaine
            if (domain != null) {
                writer.write(domain.getName());
            }
            writer.write(";");
            if (practicedSystem != null) {
                writer.write(practicedSystem.getName());
                writer.write(";");
            }
            // Campagne
            writer.write(campaigns);
            writer.write(";");
            // Type
            Class<?> genericClass;
            if (refMateriel instanceof RefMaterielAutomoteur) {
                genericClass = RefMaterielAutomoteur.class;
            } else if (refMateriel instanceof RefMaterielIrrigation) {
                genericClass = RefMaterielIrrigation.class;
            } else if (refMateriel instanceof RefMaterielOutil) {
                genericClass = RefMaterielOutil.class;
            } else if (refMateriel instanceof RefMaterielTraction) {
                genericClass = RefMaterielTraction.class;
            } else {
                genericClass = RefMateriel.class;
            }
            writer.write(t(genericClass.getName()));
            writer.write(";");
            // Nom
            writer.write(equipmentNames);
            writer.write(";");
            // Complément information
            writer.write(refMateriel.getTypeMateriel1());
            writer.write(";");
            // Durée de vie théorique des matériels (années)
            writer.write(String.valueOf(refMateriel.getDuree_vie_theorique()));
            writer.write(";");
            // Investissement matériels (euro)
            writer.write(String.valueOf(refMateriel.getPrixMoyenAchat()));
            writer.write(";");
            writer.write("\n");
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't write in memory writer ? seriously ?", ex);
        }
    }

}
