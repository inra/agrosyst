package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import org.junit.jupiter.api.BeforeEach;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.util.Collection;

public abstract class AbstractIndicatorDecomposedOperatingExpensesTest extends AbstractAgrosystTest {
    protected PerformanceServiceImplForTest performanceService;
    protected EffectiveCropCycleService effectiveCropCycleService;

    protected ZoneTopiaDao zoneDao;
    protected InputPriceTopiaDao priceDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    protected EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected PracticedCropCyclePhaseTopiaDao practicedCropCyclePhaseDao;
    protected PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeDao;
    protected RefEspeceTopiaDao refEspeceDao;


    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        PerformanceServiceImpl _performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        performanceService = new PerformanceServiceImplForTest(_performanceService);
        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);

        loginAsAdmin();
        alterSchema();

        zoneDao = getPersistenceContext().getZoneDao();
        priceDao = getPersistenceContext().getInputPriceDao();
        refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        effectiveCropCyclePhaseDao = getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = getPersistenceContext().getEffectivePerennialCropCycleDao();
        effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        effectiveSpeciesStadeDao = getPersistenceContext().getEffectiveSpeciesStadeDao();
        growingSystemDao = getPersistenceContext().getGrowingSystemDao();
        practicedSystemDao = getPersistenceContext().getPracticedSystemDao();
        practicedCropCyclePhaseDao = getPersistenceContext().getPracticedCropCyclePhaseDao();
        practicedPerennialCropCycleDao = getPersistenceContext().getPracticedPerennialCropCycleDao();
        practicedInterventionDao = getPersistenceContext().getPracticedInterventionDao();
        practicedSpeciesStadeDao = getPersistenceContext().getPracticedSpeciesStadeDao();
        refEspeceDao = getPersistenceContext().getRefEspeceDao();

        testDatas = serviceFactory.newInstance(TestDatas.class);
    }

    protected IndicatorFilter createIndicatorFilter(Collection<DecomposedOperatingExpenses> doeIndicators) {
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorDecomposedOperatingExpenses.class.getSimpleName());
        indicatorFilter.setComputeReal(true);
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setDoeIndicators(doeIndicators);
        return indicatorFilter;
    }
}
