package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMinTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import static fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX;
import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorDecomposedMineralOperatingExpensesTest extends AbstractIndicatorDecomposedOperatingExpensesTest {
    private DomainMineralProductInputTopiaDao mineralProductInputDao;
    private MineralProductInputUsageTopiaDao mineralProductInputUsageDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        super.setupServices();
        mineralProductInputDao = getPersistenceContext().getDomainMineralProductInputDao();
        mineralProductInputUsageDao = getPersistenceContext().getMineralProductInputUsageDao();
    }

    protected void createRefPrixFertiMin(int year) {
        RefPrixFertiMinTopiaDao refPrixFertiMinDao = getPersistenceContext().getRefPrixFertiMinDao();

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.K2_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.MG_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.S_O3
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.K2_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.MG_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.S_O3
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.K2_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.MG_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.S_O3
        );
    }

    @Test
    public void inraeEffectiveWithPriceTest()  throws IOException {
        inraeEffectiveTest(
                400.0d,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles fertilisation minérale (€/ha);2013;Baulon;800.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, fertilisation minérale (€/ha);2013;Baulon;0.0928;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Application de produits minéraux;intervention;03/04/2013;03/04/2014;Application de produits minéraux;AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. (2.0 t/ha);;null;2013;Indicateur économique;Charges opérationnelles réelles fertilisation minérale (€/ha);800.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Application de produits minéraux;intervention;03/04/2013;03/04/2014;Application de produits minéraux;AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. (2.0 t/ha);;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, fertilisation minérale (€/ha);0.0928;100;;;");
                });
    }

    @Test
    public void inraeEffectiveWithoutPriceTest()  throws IOException {
        inraeEffectiveTest(
                null,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles fertilisation minérale (€/ha);2013;Baulon;0.0928;75;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Application de produits minéraux;intervention;03/04/2013;03/04/2014;Application de produits minéraux;AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. (2.0 t/ha);;null;2013;Indicateur économique;Charges opérationnelles réelles fertilisation minérale (€/ha);0.0928;50;Intrant 'Engrais/amendement (organo)minéral (categ=442 forme=Compacte n=0.000000 p2O5=0.000000 k2O=40.000000 bore=0.000000 calcium=0.000000 fer=0.000000 manganese=0.000000 molybdene=0.000000 mgO=6.000000 oxyde_de_sodium=0.000000 sO3=12.000000 cuivre=0.000000 zinc=0.000000 Fertilisation minérale)' : Prix non renseigné;;");
                });
    }

    private void inraeEffectiveTest(Double price, Consumer<String> assertions) throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        testDatas.createRefInputUnitPriceUnitConverter();

        RefInterventionAgrosystTravailEDI actionMineral = refInterventionAgrosystTravailEDIDao.forNaturalId("SEM").findUnique();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixFertiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "intervention",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 4, 3),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign() + 1, 4, 3),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0d,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention.addSpeciesStades(intervention1Stades);

        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Compacté",
                RefFertiMinUNIFA.PROPERTY_CATEG, 442,
                RefFertiMinUNIFA.PROPERTY_K2_O, 40.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 6.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 12.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT."
        );

        String objectId = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(mineralProduct);
        var mineralPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. K2O(40%), MgO(6%), SO3(12%)",
                InputPrice.PROPERTY_PRICE, price
        );
        var mineralInput = mineralProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainMineralProductInput.PROPERTY_INPUT_NAME, mineralProduct.getType_produit(),
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.T_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, mineralPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var mineralUsage = mineralProductInputUsageDao.create(
                MineralProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, mineralInput,
                MineralProductInputUsage.PROPERTY_QT_AVG, 2d
        );

        MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao = getPersistenceContext().getMineralFertilizersSpreadingActionDao();
        mineralFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionMineral,
                MineralFertilizersSpreadingAction.PROPERTY_MINERAL_PRODUCT_INPUT_USAGES, List.of(mineralUsage),
                MineralFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertions.accept(content);
    }

    @Test
    public void inraePracticedTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixFertiMin(domain.getCampaign());

        RefInterventionAgrosystTravailEDI actionMineral = refInterventionAgrosystTravailEDIDao.forNaturalId("SEM").findUnique();

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100.0d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0d,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.0d,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(intervention1Stades);

        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Compacté",
                RefFertiMinUNIFA.PROPERTY_CATEG, 442,
                RefFertiMinUNIFA.PROPERTY_K2_O, 40.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 6.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 12.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT."
        );

        String objectId = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(mineralProduct);
        var mineralPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domain,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. K2O(40%), MgO(6%), SO3(12%)",
                InputPrice.PROPERTY_PRICE, 400.0d
        );
        var mineralInput = mineralProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                DomainMineralProductInput.PROPERTY_INPUT_NAME, mineralProduct.getType_produit(),
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.T_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, mineralPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var mineralUsage = mineralProductInputUsageDao.create(
                MineralProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, mineralInput,
                MineralProductInputUsage.PROPERTY_QT_AVG, 2d
        );

        MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao = getPersistenceContext().getMineralFertilizersSpreadingActionDao();
        mineralFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionMineral,
                MineralFertilizersSpreadingAction.PROPERTY_MINERAL_PRODUCT_INPUT_USAGES, List.of(mineralUsage),
                MineralFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles fertilisation minérale (€/ha);2013, 2017;Baulon;800.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, fertilisation minérale (€/ha);2013, 2017;Baulon;0.0928;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04;03/04;Application de produits minéraux;AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. (2.0 t/ha);;null;2013, 2017;Indicateur économique;Charges opérationnelles réelles fertilisation minérale (€/ha);800.0;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04;03/04;Application de produits minéraux;AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. (2.0 t/ha);;null;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, fertilisation minérale (€/ha);0.0928;100;;;");
    }
}
