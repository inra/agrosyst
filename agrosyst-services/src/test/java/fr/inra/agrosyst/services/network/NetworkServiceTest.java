package fr.inra.agrosyst.services.network;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.util.LinkedHashMap;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class NetworkServiceTest extends AbstractAgrosystTest {

    protected NetworkService networkService;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        networkService = serviceFactory.newService(NetworkService.class);
        serviceFactory.newInstance(TestDatas.class).createTestNetworks();
        loginAsAdmin();
        alterSchema();
    }

    @Test
    public void testSearchNetworks() {

        String r0TopiaId = networkService.findNetworksByName("R0", null).iterator().next();
        String r1TopiaId = networkService.findNetworksByName("R1", null).iterator().next();
        String r2TopiaId = networkService.findNetworksByName("R2", null).iterator().next();
        String r3TopiaId = networkService.findNetworksByName("R3", null).iterator().next();
        String r4TopiaId = networkService.findNetworksByName("R4", null).iterator().next();
        String r5TopiaId = networkService.findNetworksByName("R5", null).iterator().next();

        {
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("R", 10, null, null, true, false); // true means Network point of view
            Assertions.assertEquals(4, networks.size());
            Assertions.assertTrue(networks.containsKey(r2TopiaId));
            Assertions.assertTrue(networks.containsKey(r3TopiaId));
            Assertions.assertTrue(networks.containsKey(r4TopiaId));
        }

        {
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("R", 10, Sets.newHashSet(r2TopiaId), null, true, false);
            Assertions.assertEquals(3, networks.size());
            Assertions.assertTrue(networks.containsKey(r3TopiaId));
            Assertions.assertTrue(networks.containsKey(r4TopiaId));
        }

        {
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("R", 10, Sets.newHashSet(r2TopiaId), r3TopiaId, true, false);
            Assertions.assertEquals(2, networks.size());
            Assertions.assertTrue(networks.containsKey(r4TopiaId));
        }

        {
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("3", 10, null, null, true, false);
            Assertions.assertEquals(1, networks.size());
            Assertions.assertTrue(networks.containsKey(r3TopiaId));
        }

        Network r2 = networkService.getNetwork(r2TopiaId);

        {
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("R", 10, null, r2TopiaId, true, false);
            Assertions.assertEquals(3, networks.size());
            Assertions.assertTrue(networks.containsKey(r3TopiaId));
            Assertions.assertTrue(networks.containsKey(r4TopiaId));
        }

        // Set R3 as a parent of R2
        networkService.createOrUpdateNetwork(r2, Collections2.transform(r2.getManagers(), Networks.TO_MANAGER_DTO::apply), Lists.newArrayList(r3TopiaId));

        { // The search method does not exclude parents, unless specified
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("R", 10, null, r2TopiaId, true, false);
            Assertions.assertEquals(3, networks.size());
            Assertions.assertTrue(networks.containsKey(r3TopiaId));
            Assertions.assertTrue(networks.containsKey(r4TopiaId));
        }

        { // The search method does exclude children networks
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("R", 10, null, r3TopiaId, true, false);
            Assertions.assertEquals(2, networks.size());
            Assertions.assertTrue(networks.containsKey(r4TopiaId));
        }


        // Search from the SdC point of view. Only excluded network is the one having a child network (R3)
        {
            LinkedHashMap<String, String> networks = networkService.searchNameFilteredActiveNetworks("R", 10, null, null, false, false); // false means SdC point of view
            Assertions.assertEquals(5, networks.size());
            Assertions.assertTrue(networks.containsKey(r0TopiaId));
            Assertions.assertTrue(networks.containsKey(r1TopiaId));
            Assertions.assertTrue(networks.containsKey(r2TopiaId));
            Assertions.assertTrue(networks.containsKey(r4TopiaId));
            Assertions.assertTrue(networks.containsKey(r5TopiaId));
        }

    }

    @Test
    public void testSearchNetworksFails() {
        String r0TopiaId = networkService.findNetworksByName("R0", null).iterator().next();

        Assertions.assertThrows(IllegalArgumentException.class, () ->
                networkService.searchNameFilteredActiveNetworks("R", 10, null, r0TopiaId, false, false));
    }
}
