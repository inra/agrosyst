package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionImpl;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

/**
 * Test specific à l'indicateur temps de travail total.
 */
public class IndicatorTotalWorkTimeTest extends AbstractAgrosystTest {

    @Test
    public void testEffectiveIntervention() {
        IndicatorTotalWorkTime indicatorTotalWorkTime = serviceFactory.newInstance(IndicatorTotalWorkTime.class);

        EffectiveIntervention intervention = new EffectiveInterventionImpl();
        intervention.setTopiaId("DUMMY");
        intervention.setInvolvedPeopleCount(2.0d);
        intervention.setStartInterventionDate(LocalDate.of(2023, 8, 1));
        intervention.setEndInterventionDate(LocalDate.of(2023, 8, 31));

        PerformanceEffectiveInterventionExecutionContext interventionContext =
                new PerformanceEffectiveInterventionExecutionContext(
                        intervention,
                        null,
                        null,
                        Collections.emptyList(),
                        null,
                        null,
                        Collections.emptyList(),
                        new HashSetValuedHashMap<>());
        interventionContext.setManualWorkTime(5.0d);
        interventionContext.setMechanizedWorkTime(7.5d);

        PerformanceEffectiveDomainExecutionContext domainContext = new PerformanceEffectiveDomainExecutionContext(
                Pair.of(null, null),
                Collections.emptyList(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                0.0d,
                0.0d,
                0.0d,
                null,
                Optional.empty(),
                Collections.emptyList());

        WriterContext writerContext = WriterContext.createNotUsedWriterContext();
        Double[] totalWorkTimes = indicatorTotalWorkTime.manageIntervention(
                writerContext,
                null,
                domainContext,
                null,
                null,
                interventionContext);
        Assertions.assertEquals(13, totalWorkTimes.length);
        Assertions.assertEquals(12.5d, totalWorkTimes[12], 0.00001d); // Total
        Assertions.assertEquals(0.0d, totalWorkTimes[0], 0.00001d); // January
        Assertions.assertEquals(0.0d, totalWorkTimes[6], 0.00001d); // July
        Assertions.assertEquals(12.5d, totalWorkTimes[7], 0.00001d); // August
        Assertions.assertEquals(0.0d, totalWorkTimes[8], 0.00001d);
    }

    @Test
    public void testPracticedIntervention() {
        IndicatorTotalWorkTime indicatorTotalWorkTime = serviceFactory.newInstance(IndicatorTotalWorkTime.class);

        PracticedIntervention intervention = new PracticedInterventionImpl();
        intervention.setTopiaId("DUMMY");
        intervention.setInvolvedPeopleNumber(2.0d);
        intervention.setStartingPeriodDate("28/7");
        intervention.setEndingPeriodDate("12/8");

        PerformancePracticedInterventionExecutionContext interventionContext = PerformancePracticedInterventionExecutionContext.createPerformancePracticedInterventionExecutionContext(
                intervention,
                Collections.emptySet(),
                null,
                null,
                Collections.emptyList(),
                null,
                Collections.emptyList(),
                new HashSetValuedHashMap<>());
        interventionContext.setManualWorkTime(5.0d);
        interventionContext.setMechanizedWorkTime(7.5d);

        PerformancePracticedDomainExecutionContext domainContext = new PerformancePracticedDomainExecutionContext(
                null,
                null,
                null,
                null,
                0.0d,
                0.0d,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        WriterContext writerContext = WriterContext.createNotUsedWriterContext();
        Double[] totalWorkTimes = indicatorTotalWorkTime.manageIntervention(
                writerContext,
                null,
                domainContext,
                null,
                null,
                null,
                interventionContext,
                null
        );
        Assertions.assertEquals(13, totalWorkTimes.length);
        Assertions.assertEquals(12.5d, totalWorkTimes[12], 0.00001d); // Total
        Assertions.assertEquals(0.0d, totalWorkTimes[0], 0.00001d); // January
        Assertions.assertEquals(3.125d, totalWorkTimes[6], 0.00001d); // July
        Assertions.assertEquals(9.375d, totalWorkTimes[7], 0.00001d); // August
        Assertions.assertEquals(0.0d, totalWorkTimes[8], 0.00001d);
    }
}
