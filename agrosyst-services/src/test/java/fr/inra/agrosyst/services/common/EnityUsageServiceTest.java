package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Setter
public class EnityUsageServiceTest extends AbstractAgrosystTest {

    protected BiologicalProductInputUsageTopiaDao biologicalProductInputUsageDao;
    protected DomainTopiaDao domainDao;
    protected DomainPhytoProductInputTopiaDao domainPhytoProductInputDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao;

    protected EntityUsageService entityUsageService;


    @BeforeEach
    public void prepareTest() throws IOException {
        testDatas = serviceFactory.newInstance(TestDatas.class);
        entityUsageService = serviceFactory.newInstance(EntityUsageService.class);

        biologicalProductInputUsageDao = getPersistenceContext().getBiologicalProductInputUsageDao();
        domainDao = getPersistenceContext().getDomainDao();
        domainPhytoProductInputDao = getPersistenceContext().getDomainPhytoProductInputDao();
        refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();

        testDatas.createTestDomains();
    }

    @Test
    protected void testGetDomainInputUsageMap() throws IOException {

        Domain domainBaulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domainBaulon.getLocation().getRefCountry();

        RefActaTraitementsProduit produitAcanto = refActaTraitementsProduitDao.createByNotNull("3147", 140, "9900206", france);
        produitAcanto.setNom_produit("ACANTO");
        RefActaTraitementsProduit produitAback = refActaTraitementsProduitDao.createByNotNull("5232", 147, "2090041", france);
        produitAback.setNom_produit("ABAK");
        testDatas.importActaTraitementsProduits();

        DomainPhytoProductInput phytoInput = domainPhytoProductInputDao.newInstance();
        phytoInput.setCode(UUID.randomUUID().toString());
        phytoInput.setInputType(InputType.LUTTE_BIOLOGIQUE);
        phytoInput.setDomain(domainBaulon);
        phytoInput.setProductType(ProductType.HERBICIDAL);
        phytoInput.setRefInput(produitAcanto);
        phytoInput.setUsageUnit(PhytoProductUnit.L_HA);// from RefActaDose
        phytoInput.setInputName(produitAcanto.getNom_produit());
        phytoInput.setInputKey(produitAcanto.getId_produit() + "_" + produitAcanto.getId_traitement() + "_" + PhytoProductUnit.L_HA);

        DomainPhytoProductInput notUsedPhytoInput = domainPhytoProductInputDao.newInstance();
        notUsedPhytoInput.setCode(UUID.randomUUID().toString());
        notUsedPhytoInput.setInputType(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        notUsedPhytoInput.setDomain(domainBaulon);
        notUsedPhytoInput.setProductType(ProductType.HERBICIDAL);
        notUsedPhytoInput.setRefInput(produitAback);
        notUsedPhytoInput.setUsageUnit(PhytoProductUnit.AA_HA);// none if referential
        notUsedPhytoInput.setInputName(produitAback.getNom_produit());
        notUsedPhytoInput.setInputKey(produitAback.getId_produit() + "_" + produitAback.getId_traitement() + "_" + PhytoProductUnit.AA_HA);

        domainPhytoProductInputDao.create(phytoInput);
        domainPhytoProductInputDao.create(notUsedPhytoInput);

        BiologicalProductInputUsage biologicalProductInputUsage = biologicalProductInputUsageDao.newInstance();
        biologicalProductInputUsage.setInputType(InputType.LUTTE_BIOLOGIQUE);
        biologicalProductInputUsage.setDomainPhytoProductInput(phytoInput);
        biologicalProductInputUsage.setQtAvg(2.5);

        biologicalProductInputUsageDao.create(biologicalProductInputUsage);

        getPersistenceContext().commit();

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = new ArrayList<>();
        domainInputStockUnits.add(phytoInput);
        domainInputStockUnits.add(notUsedPhytoInput);

        Map<String, Boolean> usages = entityUsageService.getDomainInputUsageMap(domainInputStockUnits);
        Assertions.assertTrue(usages.get(phytoInput.getTopiaId()));
        Assertions.assertFalse(usages.get(notUsedPhytoInput.getTopiaId()));
    }

}
