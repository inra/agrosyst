package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionImpl;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @see IndicatorUTH
 */
public class IndicatorUTHTest extends AbstractAgrosystTest {

    private static Stream<Arguments> effectiveDataProvider() {
        return Stream.of(
                Arguments.of(4252.0, 2.6575),
                Arguments.of(8981.0, 5.613125),
                Arguments.of(7606.0, 4.75375),
                Arguments.of(3756.0, 2.3475),
                Arguments.of(7931.0, 4.956875),
                Arguments.of(2837.0, 1.773125),
                Arguments.of(675.0, 0.421875),
                Arguments.of(2838.0, 1.77375),
                Arguments.of(9389.0, 5.868125),
                Arguments.of(9692.0, 6.0575),
                Arguments.of(562.0, 0.35125),
                Arguments.of(3522.0, 2.20125),
                Arguments.of(2567.0, 1.604375),
                Arguments.of(3949.0, 2.468125),
                Arguments.of(4575.0, 2.859375),
                Arguments.of(4433.0, 2.770625),
                Arguments.of(2592.0, 1.62),
                Arguments.of(4903.0, 3.064375),
                Arguments.of(3317.0, 2.073125),
                Arguments.of(3863.0, 2.414375)
        );
    }

    @ParameterizedTest
    @MethodSource("effectiveDataProvider")
    public void testEffectiveIntervention(final double totalWorkTime, final double expectedUth) {
        EffectiveIntervention intervention = new EffectiveInterventionImpl();
        intervention.setTopiaId("DUMMY");
        intervention.setInvolvedPeopleCount(2.0d);
        intervention.setStartInterventionDate(LocalDate.of(2023, 8,1));
        intervention.setEndInterventionDate(LocalDate.of(2023, 8,31));

        PerformanceEffectiveInterventionExecutionContext interventionContext =
                new PerformanceEffectiveInterventionExecutionContext(
                        intervention,
                        null,
                        null,
                        Collections.emptyList(),
                        null,
                        null,
                        Collections.emptyList(), new HashSetValuedHashMap<>());
        interventionContext.setTotalWorkTime(totalWorkTime);

        PerformanceEffectiveDomainExecutionContext domainContext = new PerformanceEffectiveDomainExecutionContext(
                Pair.of(null, null),
                Collections.emptyList(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                0.0d,
                0.0d,
                0.0d,
                null,
                Optional.empty(),
                Collections.emptyList());


        IndicatorUTH indicatorUTH = serviceFactory.newInstance(IndicatorUTH.class);
        WriterContext writerContext = WriterContext.createNotUsedWriterContext();
        Double[] uthValue = indicatorUTH.manageIntervention(
                writerContext,
                null,
                domainContext,
                null,
                null,
                interventionContext);
        Assertions.assertEquals(1, uthValue.length);
        Assertions.assertEquals(expectedUth, uthValue[0]);
    }


    private static Stream<Arguments> practicedDataProvider() {
        return Stream.of(
                Arguments.of(1714.0, 1.07125),
                Arguments.of(3027.0, 1.891875),
                Arguments.of(1455.0, 0.909375),
                Arguments.of(8882.0, 5.55125),
                Arguments.of(9536.0, 5.96),
                Arguments.of(4230.0, 2.64375),
                Arguments.of(3108.0, 1.9425),
                Arguments.of(3699.0, 2.311875),
                Arguments.of(9519.0, 5.949375),
                Arguments.of(473.0, 0.295625),
                Arguments.of(759.0, 0.474375),
                Arguments.of(2705.0, 1.690625),
                Arguments.of(4149.0, 2.593125),
                Arguments.of(3578.0, 2.23625),
                Arguments.of(3940.0, 2.4625),
                Arguments.of(3369.0, 2.105625),
                Arguments.of(716.0, 0.4475),
                Arguments.of(1427.0, 0.891875),
                Arguments.of(4152.0, 2.595),
                Arguments.of(1708.0, 1.0675)
        );
    }

    @ParameterizedTest
    @MethodSource("practicedDataProvider")
    public void testPracticedIntervention(final double totalWorkTime, final double expectedUth) {
        PracticedIntervention intervention = new PracticedInterventionImpl();
        intervention.setTopiaId("DUMMY");
        intervention.setInvolvedPeopleNumber(2.0d);
        intervention.setStartingPeriodDate("28/7");
        intervention.setEndingPeriodDate("12/8");

        PerformancePracticedInterventionExecutionContext interventionContext = PerformancePracticedInterventionExecutionContext.createPerformancePracticedInterventionExecutionContext(
                intervention,
                Collections.emptySet(),
                null,
                null,
                Collections.emptyList(),
                null,
                Collections.emptyList(),
                new HashSetValuedHashMap<>());
        interventionContext.setTotalWorkTime(totalWorkTime);

        PerformancePracticedDomainExecutionContext domainContext = new PerformancePracticedDomainExecutionContext(
                null,
                null,
                null,
                null,
                0.0d,
                0.0d,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        IndicatorUTH indicatorUTH = serviceFactory.newInstance(IndicatorUTH.class);
        WriterContext writerContext = WriterContext.createNotUsedWriterContext();
        Double[] uthValue = indicatorUTH.manageIntervention(
                writerContext,
                null,
                domainContext,
                null,
                null,
                null,
                interventionContext,
                null
        );
        Assertions.assertEquals(1, uthValue.length);
        Assertions.assertEquals(expectedUth, uthValue[0]);
    }
}
