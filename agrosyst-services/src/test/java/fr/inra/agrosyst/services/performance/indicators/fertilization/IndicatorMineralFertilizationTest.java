package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorMineralFertilizationTest extends AbstractIndicatorFertilizationTest {

    @Test
    public void testQuantiteAzoteIrrigationRealise_12359() {
        // La quantité d'eau n'est de toutes manières pas prise en compte pour le calcul, donc on met
        // une quantité à 1 par défaut.
        String indicatorN = "Quantité de N minéral apportée (en kg/ha)";
        this.testIrrigationEnRealise(1, 25, 1.0, 1, indicatorN, "25.0");
        this.testIrrigationEnRealise(1, 15, 1.5, 1, indicatorN, "22.5");
        this.testIrrigationEnRealise(1, 12, 2.0, 1, indicatorN, "24.0");
        this.testIrrigationEnRealise(1, 35, 1.0, 1, indicatorN, "35.0");
        this.testIrrigationEnRealise(1, 5, 3.0, 1, indicatorN, "15.0");
    }

    private void testIrrigationEnRealise(double waterAverageQuantity, int azoteQuantity, double spatialFrequency, int transitCount, String expectedIndicatorName, String expectedIndicatorValue) {
        final String nomIntervention = this.createIrrigationEffectiveIntervention(
                waterAverageQuantity,
                azoteQuantity,
                spatialFrequency,
                transitCount
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorMineralFertilization);
        final String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo(
                "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                        "Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;;;Pleine production;Irrigation;" +
                        nomIntervention + ";12/03/2013;12/03/2013;Irrigation;" +
                        "Eau d’irrigation;;null;2013;Fertilisation minérale;" + expectedIndicatorName + ";" + expectedIndicatorValue
        );
    }


    @Test
    public void testQuantiteAzoteIrrigationSynthetise_12359() throws IOException {
        this.testIrrigationEnSynthetise(1, 25, 5.0, 1.0, "125.0");
        this.testIrrigationEnSynthetise(1, 6, 4.0, 1.5, "36.0");
        this.testIrrigationEnSynthetise(1, 7, 5.0, 2.0, "70.0");
        this.testIrrigationEnSynthetise(1, 20, 6.0, 1.0, "120.0");
        this.testIrrigationEnSynthetise(1, 50, 7.0, 3.0, "1050.0");
    }

    private void testIrrigationEnSynthetise(double waterAverageQuantity, int azoteQuantity, double spatialFrequency, double temporalFrequency, String expectedIndicatorValue) throws IOException {
        final String nomIntervention = createIrrigationPracticedIntervention(
                waterAverageQuantity,
                azoteQuantity,
                spatialFrequency,
                temporalFrequency
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorMineralFertilization indicatorIrrigation = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorIrrigation);
        final String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo(
                "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                        "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Irrigation;" +
                        nomIntervention + ";03/04;03/04;Irrigation;Eau d’irrigation;;null;2012, 2013;" +
                        "Fertilisation minérale;Quantité de N minéral apportée (en kg/ha);" + expectedIndicatorValue
        );
    }


    @Test
    public void testFertilisationRealise_12359() {
        final List<DonneesTestFertilisationMineRealise> donneesTestFertilisation = this.getDonneesTestFertilisationMineraleRealise();


        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        // Exécution des tests
        for (DonneesTestFertilisationMineRealise d : donneesTestFertilisation) {
            this.testFertilisationRealise(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.transitCount(),
                    d.expectedIndicatorNameAndValue(),
                    "Fertilisation minérale",
                    indicatorMineralFertilization);
        }
    }

    @Test
    public void testFertilisationSynthetise_12359() throws IOException {
        final List<DonneesTestFertilisationMineSynthetise> donneesTestFertilisation = this.getDonneesTestFertilisationMineraleSynthetise();

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        for (DonneesTestFertilisationMineSynthetise d : donneesTestFertilisation) {
            this.testFertilisationSynthetise(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.temporalFrequency(),
                    d.expectedIndicatorNameAndInfo(),
                    "Fertilisation minérale",
                    indicatorMineralFertilization
            );
        }
    }
}
