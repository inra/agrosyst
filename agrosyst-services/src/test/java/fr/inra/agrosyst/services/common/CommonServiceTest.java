package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;

import java.util.Locale;

public class CommonServiceTest {
    
    @Test
    public void testNormalise() {
        
        String tesxtWithAccent = "0 - Tracteur 180CV ";
    
        String result = CommonService.NORMALISE.apply(tesxtWithAccent);
    
        Assertions.assertEquals("0tracteur180cv", result);
    }

    @Test
    public void testEnumToStringUsingI18n() {

        // le ClassPathI18nInitializer ne peut pas être utilisé dans les tests car le class loader n'est pas un URLClassLoader
        I18n.init(new DefaultI18nInitializer("agrosyst-services", null, "i18n/"), Locale.FRANCE);

        Assertions.assertEquals(SeedPlantUnit.KG_PAR_HA.name(), SeedPlantUnit.KG_PAR_HA.toString());
        Assertions.assertEquals("Récolte", I18n.t("fr.inra.agrosyst.api.entities.AgrosystInterventionType.RECOLTE"));
        Assertions.assertEquals("kg/ha", AgrosystI18nService.getEnumTraductionWithDefaultLocale(SeedPlantUnit.KG_PAR_HA));
    }

}
