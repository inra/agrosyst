package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorSulfurTotalProductPhytoTest extends AbstractQSATest {

    @Test
    public void testQuantitesSubstancesActivesRealise_12394() throws IOException {
        testDatas.createEffectiveDataSetForQSATotalIndicators();

        /*
         * Calcul des indicateurs
         */

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(false);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        IndicatorSulfurPhytoProduct indicatorSulfurPhytoProduct = serviceFactory.newInstance(IndicatorSulfurPhytoProduct.class);
        indicatorSulfurPhytoProduct.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        IndicatorSulfurTotalProduct indicatorSulfurTotalProduct = serviceFactory.newInstance(IndicatorSulfurTotalProduct.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorSulfurPhytoProduct, indicatorSulfurTotalProduct);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);BOUILLIE BORDELAISE SP (48.0 kg/ha);;N;;2013;Réalisé;Substances actives;QSA Soufre total;0.0;;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 4;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);OIDIASE 80 (15.0 kg/ha);;N;;2013;Réalisé;Substances actives;QSA Soufre total;0.0;;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 2;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);QUINOGAM 75 (5.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Soufre total;0.25;;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 3;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);GERMINATE MG LIQUIDE ORANGE (10.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Soufre total;3.75;;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 1;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DITHANE C-90 (3.0 kg/ha);;N;;2013;Réalisé;Substances actives;QSA Soufre total;2.1;;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé tendre;;;2013;Réalisé;Substances actives;QSA Soufre total;6.1;;0;;");
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Blé;Blé tendre;;Pleine production;;2013;Réalisé;Substances actives;QSA Soufre phyto;6.1;;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("plotSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;;2013;Réalisé;Substances actives;QSA Soufre phyto;6.1;;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("growingSystemSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;NON;;2013;Réalisé;Substances actives;QSA Soufre phyto;6.1;;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("domainSheet;Baulon;Agriculture conventionnelle;;2013;Réalisé;Substances actives;QSA Soufre phyto;6.1;;100;;");
    }

    @Test
    public void testQuantitesSubstancesActivesSynthetise_12394() throws IOException {
        testDatas.createPracticedDataSetForQSATotalIndicators();

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        IndicatorSulfurPhytoProduct indicatorSulfurPhytoProduct = serviceFactory.newInstance(IndicatorSulfurPhytoProduct.class);
        indicatorSulfurPhytoProduct.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        IndicatorSulfurTotalProduct indicatorSulfurTotalProduct = serviceFactory.newInstance(IndicatorSulfurTotalProduct.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorSulfurPhytoProduct, indicatorSulfurTotalProduct);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 4;Intervention_4_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);OIDIASE 80 (7.5 kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Soufre total;0.0;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;Intervention_5_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);BOUILLIE BORDELAISE SP (6.6 kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Soufre total;0.0;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 1;Intervention_1_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DITHANE C-90 (56.0 kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Soufre total;137.2;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 2;Intervention_2_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);QUINOGAM 75 (5.0 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Soufre total;1.165;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 3;Intervention_3_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);GERMINATE MG LIQUIDE ORANGE (2.0 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Soufre total;0.747;0;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);Blé tendre;;Blé;;0.0;2012, 2013;Synthétisé;Substances actives;QSA Soufre total;139.112;;0;;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;NON;Efficience;NON;;2012, 2013;Synthétisé;Substances actives;QSA Soufre total;139.112;0;;");
        assertThat(content).usingComparator(comparator).isEqualTo("domainSheet;Baulon;Agriculture conventionnelle;;2012, 2013;Synthétisé;Substances actives;QSA Soufre phyto;139.112;100;;");
    }
}
