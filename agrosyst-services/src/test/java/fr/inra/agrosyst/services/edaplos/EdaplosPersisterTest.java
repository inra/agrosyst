/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.edaplos;

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.api.services.edaplos.EdaplosResultLog;
import fr.inra.agrosyst.services.edaplos.model.PlotAgriculturalProcess;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.inra.agrosyst.api.entities.PhytoProductUnit.*;
import static fr.inra.agrosyst.services.edaplos.EdaplosConstants.UnitCode.*;
import static org.assertj.core.api.Assertions.assertThat;

public class EdaplosPersisterTest {

    protected final EdaplosPersister edaplosPersister = new EdaplosPersister();

    /**
     * Unité quantité par unité de surface EDI	Unité Agrosyst	Conversion nécessaire (o/n)	Facteur de conversion
     * l/ha	                                    L/ha	        n
     * l/ha	                                    ml/ha	        o	1000
     * l/ha	                                    ml/m2	        o	0,1
     * l/ha	                                    L/m²	        o	0,0001
     * kg/ha	                                t/ha	        o	0,001
     * kg/ha	                                kg/ha	        n
     * kg/ha	                                g/ha	        o	1000
     * kg/ha	                                g/m2	        o	0,1
     * kg/ha	                                kg/m2	        o	0,0001
     * kg/ha	                                kg/ha	        n
     * q/ha	                                    kg/ha	        o	100
     * t/ha	                                    kg/ha	        o	1000
     * kg/ha	                                q/ha	        o	0,01
     * q/ha	                                    q/ha	        n
     * t/ha	                                    q/ha	        o	10
     * kg/ha	                                t/ha	        o	0,001
     * q/ha	                                    t/ha	        o	0,1
     * t/ha	                                    t/ha	        n
     */
    @Test
    public void testEdiToAgrosystUnitConversion() {
        // phyto
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(LTR, L_HA, 1.0)).isEqualTo(1);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(LTR, ML_HA, 1.0)).isEqualTo(1000);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(LTR, ML_M2, 1.0)).isEqualTo(0.1);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(LTR, L_M2, 1.0)).isEqualTo(0.0001);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, T_HA, 1.0)).isEqualTo(0.001);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, KG_HA, 1.0)).isEqualTo(1);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, G_HA, 1.0)).isEqualTo(1000);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, G_M2, 1.0)).isEqualTo(0.1);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, KG_M2, 1.0)).isEqualTo(0.0001);
        // recolte
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, KG_HA, 1.0)).isEqualTo(1);
        //assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(QUL, KG_HA, 1.0)).isEqualTo(100);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(TNE, KG_HA, 1.0)).isEqualTo(1000);
        //assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, Q_HA, 1.0)).isEqualTo(0.01);
        //assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(QUL, Q_HA, 1.0)).isEqualTo(1);
        //assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(TNE, Q_HA, 1.0)).isEqualTo(10);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, T_HA, 1.0)).isEqualTo(0.001);
        //assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(QUL, T_HA, 1.0)).isEqualTo(0.1);
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(TNE, T_HA, 1.0)).isEqualTo(1);

        // all other unit must return null
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(LTR, L_M3, 1.0)).isNull();
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(KGM, G_10M2, 1.0)).isNull();
        assertThat(edaplosPersister.convertEdiToAgrosystPhytoUnit(TNE, ADULTES_M2, 1.0)).isNull();
    }
    
    @Test
    public void testGetTransitCountFromTripNumber() {
        EdaplosPersister edaplosPersister = new EdaplosPersister();
        
        int result = edaplosPersister.getTransitCountFromTripNumber(null);
        Assertions.assertEquals(0, result);
    
        result = edaplosPersister.getTransitCountFromTripNumber("0");
        Assertions.assertEquals(0, result);
    
        result = edaplosPersister.getTransitCountFromTripNumber("0.0");
        Assertions.assertEquals(0, result);
    
        result = edaplosPersister.getTransitCountFromTripNumber("1");
        Assertions.assertEquals(1, result);
        
        result = edaplosPersister.getTransitCountFromTripNumber("1.0");
        Assertions.assertEquals(1, result);
    
        result = edaplosPersister.getTransitCountFromTripNumber("1.2");
        Assertions.assertEquals(0, result);
    
        result = edaplosPersister.getTransitCountFromTripNumber("2");
        Assertions.assertEquals(2, result);
    
        result = edaplosPersister.getTransitCountFromTripNumber("2.0");
        Assertions.assertEquals(2, result);
    
        result = edaplosPersister.getTransitCountFromTripNumber("2.5");
        Assertions.assertEquals(0, result);
        
    }
    
    @Test
    public void testGetTransitCountFromPlotAgriculturalProcess() {
        EdaplosPersister edaplosPersister = new EdaplosPersister();
    
        PlotAgriculturalProcess plotAgriculturalProcess = new PlotAgriculturalProcess();
        
        plotAgriculturalProcess.setTripNumber(null);
        Integer result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertNull(result);
    
        Integer expectedValue = 0;
        plotAgriculturalProcess.setTripNumber("0");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
    
        plotAgriculturalProcess.setTripNumber("0.0");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
    
        plotAgriculturalProcess.setTripNumber("1.2");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
    
        plotAgriculturalProcess.setTripNumber("2.5");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
    
        plotAgriculturalProcess.setTripNumber("2,2");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
    
        expectedValue = 1;
        plotAgriculturalProcess.setTripNumber("1");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
    
        plotAgriculturalProcess.setTripNumber("1.0");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);

        expectedValue = 2;
        plotAgriculturalProcess.setTripNumber("2");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
    
        plotAgriculturalProcess.setTripNumber("2.0");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
        
        plotAgriculturalProcess.setTripNumber("2,0");
        result = edaplosPersister.getTransitCountFromPlotAgriculturalProcess(plotAgriculturalProcess);
        Assertions.assertEquals(expectedValue, result);
        
    }
    
    @Test
    public void testStringFormat() {

        Map<SeedType, String> seedTypeTranslations = new HashMap<>();

        Pair<String, String> param0 = Pair.of("OUTI999999999", "5 Houe rotative 6m (50%)");
        Pair<String, String> param1 = Pair.of("", "4 Semoir monograine 7 rangs, écartement 60 (50%)");
    
        List<Pair<String, String>> params = Lists.newArrayList(param0, param1);
        
        List<String> expectedResults = new ArrayList<>();
        String model = "Certains matériels ne sont pas reconnus dans Agrosyst. Code du matériel : '%s'. Descriptif du matériel : '%s'. Veuillez contacter l'équipe Agrosyst pour les ajouter.";
        for (Pair<String, String> param : params) {
            final String expectedResult = "Certains matériels ne sont pas reconnus dans Agrosyst. Code du matériel : '" + param.getLeft() + "'. Descriptif du matériel : '" + param.getRight() + "'. Veuillez contacter l'équipe Agrosyst pour les ajouter.";
            expectedResults.add(expectedResult);
            Assertions.assertEquals(expectedResult, String.format(model, param.getLeft(), param.getRight()));
        }
    
        EdaplosParsingResult edaplosParsingResult = new EdaplosParsingResult();
        EdaplosContext context = new EdaplosContext(seedTypeTranslations, edaplosParsingResult);
    
        for (Pair<String, String> param : params) {
            final String formatedMessage = String.format("Certains matériels ne sont pas reconnus dans Agrosyst. Code du matériel : '%s'. Descriptif du matériel : '%s'. Veuillez contacter l'équipe Agrosyst pour les ajouter."
                    , param.getLeft()
                    , param.getRight()
            );
            context.formatWarning(formatedMessage);
        }
        
        List<EdaplosResultLog> logs = edaplosParsingResult.getWarningMessages();
        for (EdaplosResultLog log : logs) {
            Assertions.assertTrue(expectedResults.contains(log.getLabel()));
        }
    }
}
