package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorTotalFertilizationTest extends AbstractIndicatorFertilizationTest {

    private final String indicatorN = "Quantité totale de N apportée (en kg/ha)";
    private final String indicatorP2O5 = "Quantité totale de P2O5 apportée (en kg/ha)";
    private final String indicatorK2O = "Quantité totale de K2O apportée (en kg/ha)";

    @Test
    public void testRealiseInterventionOrganique() {
        final List<DonneesTestFertilisationOrganiqueRealise> donneesTestFertilisation = new LinkedList<>();
        final List<TestDataRealise> testDatas = List.of(
                new TestDataRealise("SDY", OrganicProductUnit.T_HA, 5, 3.5, 1, "35.0", "12.25", "7.0", "0.0", "0.0", "0.0"),
                new TestDataRealise("SDR", OrganicProductUnit.T_HA, 5, 1.0, 1, "147.5", "228.5", "25.0", "75.0", "0.0", "0.0"),
                new TestDataRealise("SDV", OrganicProductUnit.M_CUB_HA, 15, 2.0, 1, "1.5", "6.0", "12.0", "0.0", "60.0", "0.0"),
                new TestDataRealise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 1, "91.5", "75.0", "105.0", "0.0", "0.0", "75.0"),
                new TestDataRealise("SCM", OrganicProductUnit.M_CUB_HA, 15, 1.0, 1, "102.0", "142.5", "82.5", "0.0", "0.0", "0.0")
        );
        for (TestDataRealise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id);
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S()

            );

            donneesTestFertilisation.add(new DonneesTestFertilisationOrganiqueRealise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg,
                    testData.spatialFrequency,
                    testData.transitCount,
                    Map.of(
                            indicatorN, testData.expectedN,
                            indicatorP2O5, testData.expectedP,
                            indicatorK2O, testData.expectedK
                    )
            ));
        }

        for (DonneesTestFertilisationOrganiqueRealise donneesTestFertilisationRealise : donneesTestFertilisation) {
            this.testEpandageOrganiqueRealise(donneesTestFertilisationRealise);
        }
    }

    private void testEpandageOrganiqueRealise(DonneesTestFertilisationOrganiqueRealise donneesTestFertilisation) {
        final OrganicInterventionInfo interventionInfo = this.createOrganicFertilizerSpreadingEffectiveIntervention(
                donneesTestFertilisation.refFertiOrga,
                donneesTestFertilisation.transitCount,
                donneesTestFertilisation.spatialFrequency,
                donneesTestFertilisation.qtAvg,
                donneesTestFertilisation.domainOrganicProductInput
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTotalFertilization indicatorTotalFertilization = serviceFactory.newInstance(IndicatorTotalFertilization.class);
        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOrganicFertilization, indicatorMineralFertilization, indicatorTotalFertilization);
        final String content = out.toString();

        donneesTestFertilisation.expectedIndicatorNameAndValue.forEach((indicatorName, indicatorValue) -> {
            assertThat(content).usingComparator(comparator).isEqualTo(
                    "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;" +
                            "Application de produits minéraux;" + interventionInfo.interventionName() + ";12/03/2013;12/03/2013;Épandage organique;" +
                            writer.getInputUsageProductName(interventionInfo.organicProductInputUsage(), interventionInfo.organicFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                            ";;null;2013;Fertilisation totale;"  + indicatorName+ ";" + indicatorValue
            );

            assertThat(content).usingComparator(comparator).isEqualTo(
                    "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;" +
                            "Application de produits minéraux;" + interventionInfo.interventionName() + ";Épandage organique;" +
                            writer.getInputUsageProductName(interventionInfo.organicProductInputUsage(), interventionInfo.organicFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                            ";;N;;2013;Réalisé;Fertilisation totale;" + indicatorName + ";" + indicatorValue
            );
        });
    }

    @Test
    public void testSynthetiseInterventionOrganique() throws IOException {
        final List<DonneesTestFertilisationOrganiqueSynthetise> allDonneesTestFertilisation = new LinkedList<>();
        final List<TestDataSynthetise> testDatas = List.of(
                new TestDataSynthetise("SDY", OrganicProductUnit.T_HA, 5, 4.0, 1.0, "40.0", "14.0", "8.0", "0.0", "0.0", "0.0"),
                new TestDataSynthetise("SDY", OrganicProductUnit.T_HA, 5, 3.0, 6.0, "180.0", "63.0", "36.0", "0.0", "0.0", "0.0"),
                new TestDataSynthetise("SDR", OrganicProductUnit.T_HA, 5, 2.0, 1.0, "295.0", "457.0", "50.0", "150.0", "0.0", "0.0"),
                new TestDataSynthetise("SDV", OrganicProductUnit.M_CUB_HA, 15, 1.0, 6.0, "4.5", "18.0", "36.0", "0.0", "180.0", "0.0"),
                new TestDataSynthetise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 2.0, "183.0", "150.0", "210.0", "0.0", "0.0", "150.0"),
                new TestDataSynthetise("SCM", OrganicProductUnit.M_CUB_HA, 15, 1.5, 1.0, "153.0", "213.75", "123.75", "0.0", "0.0", "0.0")
        );
        for (TestDataSynthetise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id);
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S()
            );

            final String inputName = domainOrganicProductInput.getInputName();
            allDonneesTestFertilisation.add(new DonneesTestFertilisationOrganiqueSynthetise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg,
                    testData.spatialFrequency,
                    testData.temporalFrequency,
                    inputName,
                    Map.of(
                            indicatorN, testData.expectedN,
                            indicatorP2O5, testData.expectedP,
                            indicatorK2O, testData.expectedK
                    )
            ));
        }

        for (DonneesTestFertilisationOrganiqueSynthetise donneesTestFertilisationSynthetise : allDonneesTestFertilisation) {
            this.testEpandageOrganiqueSynthetise(donneesTestFertilisationSynthetise);
        }
    }

    public void testEpandageOrganiqueSynthetise(DonneesTestFertilisationOrganiqueSynthetise donneesTestFertilisation) throws IOException {

        final OrganicInterventionInfo interventionInfo = this.createOrganicFertilizerSpreadingPracticedIntervention(
                donneesTestFertilisation.temporalFrequency,
                donneesTestFertilisation.spatialFrequency,
                donneesTestFertilisation.qtAvg,
                donneesTestFertilisation.domainOrganicProductInput
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTotalFertilization indicatorTotalFertilization = serviceFactory.newInstance(IndicatorTotalFertilization.class);
        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOrganicFertilization, indicatorMineralFertilization, indicatorTotalFertilization);
        final String content = out.toString();

        donneesTestFertilisation.expectedIndicatorNameAndValue.forEach((indicatorName, indicatorValue) -> {
            assertThat(content).usingComparator(comparator).isEqualTo(
                    "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Application de produits minéraux;" +
                            interventionInfo.interventionName() + ";" + "03/04;03/04;Épandage organique;" +
                            writer.getInputUsageProductName(interventionInfo.organicProductInputUsage(), interventionInfo.organicFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                            ";;null;2012, 2013;Fertilisation totale;" + indicatorName + ";" + indicatorValue
            );

            assertThat(content).usingComparator(comparator).isEqualTo(
                    "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Application de produits minéraux;" +
                            interventionInfo.interventionName() + ";" + interventionInfo.interventionTopiaId() + ";03/04;03/04;Épandage organique;" +
                            writer.getInputUsageProductName(interventionInfo.organicProductInputUsage(), interventionInfo.organicFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                            ";1;;;non;;2012, 2013;Synthétisé;Fertilisation totale;" + indicatorName + ";" + indicatorValue
            );
        });
    }

    @Test
    public void testRealiseInterventionMinerale() {
        final List<DonneesTestFertilisationMineRealise> donneesTestFertilisationMineraleRealise = this.getDonneesTestFertilisationMineraleRealise();
        for (DonneesTestFertilisationMineRealise d : donneesTestFertilisationMineraleRealise) {
            this.testFertilisationMineraleRealise(d.mineralProduct(), d.domainMineralProductInput(), d.qtAvg(), d.spatialFrequency(), d.transitCount(), d.expectedIndicatorNameAndValue());
        }
    }

    private void testFertilisationMineraleRealise(RefFertiMinUNIFA mineralProduct,
                                                  DomainMineralProductInput domainMineralProductInput,
                                                  double qtAvg,
                                                  double spatialFrequency,
                                                  int transitCount,
                                                  Map<String, String> expectedIndicatorNameAndValue) {
        final MineralInterventionInfo interventionInfo = this.createMineralSpreadingEffectiveIntervention(
                mineralProduct,
                domainMineralProductInput,
                qtAvg,
                spatialFrequency,
                transitCount
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTotalFertilization indicatorTotalFertilization = serviceFactory.newInstance(IndicatorTotalFertilization.class);
        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOrganicFertilization, indicatorMineralFertilization, indicatorTotalFertilization);
        final String content = out.toString();

        expectedIndicatorNameAndValue.forEach((mineralIndicatorName, indicatorValue) -> {
            final String indicatorName = getIndicatorNameFromMineralIndicator(mineralIndicatorName);
            if (indicatorName != null) {
                assertThat(content).usingComparator(comparator).isEqualTo(
                        "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                                "Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;" +
                                "Application de produits minéraux;" + interventionInfo.interventionName() + ";12/03/2013;12/03/2013;" +
                                "Application de produits minéraux;" + writer.getInputUsageProductName(interventionInfo.mineralProductInputUsage(), interventionInfo.mineralFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                                ";;null;2013;Fertilisation totale;" + indicatorName + ";" + indicatorValue
                );

                assertThat(content).usingComparator(comparator).isEqualTo(
                        "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                                "Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Application de produits minéraux;" +
                                interventionInfo.interventionName() + ";Application de produits minéraux;" +
                                writer.getInputUsageProductName(interventionInfo.mineralProductInputUsage(), interventionInfo.mineralFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                                ";;N;;2013;Réalisé;Fertilisation totale;" + indicatorName + ";" + indicatorValue
                );
            }
        });
    }

    @Test
    public void testSynthetiseInterventionMinerale() throws IOException {
        final List<DonneesTestFertilisationMineSynthetise> donneesTestFertilisationMineraleSynthetise = getDonneesTestFertilisationMineraleSynthetise();
        for (DonneesTestFertilisationMineSynthetise d : donneesTestFertilisationMineraleSynthetise) {
            this.testFertilisationSynthetise(d.mineralProduct(), d.domainMineralProductInput(), d.qtAvg(), d.spatialFrequency(), d.temporalFrequency(), d.expectedIndicatorNameAndInfo());
        }
    }

    private void testFertilisationSynthetise(RefFertiMinUNIFA mineralProduct,
                                             DomainMineralProductInput domainMineralProductInput,
                                             double qtAvg,
                                             double spatialFrequency,
                                             double temporalFrequency,
                                             Map<String, IndicatorInfo> expectedIndicatorNameAndInfo) throws IOException {
        final MineralInterventionInfo interventionInfo = this.createMineralSpreadingPracticedIntervention(
                mineralProduct,
                domainMineralProductInput,
                qtAvg,
                spatialFrequency,
                temporalFrequency
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTotalFertilization indicatorTotalFertilization = serviceFactory.newInstance(IndicatorTotalFertilization.class);
        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOrganicFertilization, indicatorMineralFertilization, indicatorTotalFertilization);
        final String content = out.toString();

        expectedIndicatorNameAndInfo.forEach((mineralIndicatorName, indicatorInfo) -> {
            final String indicatorName = getIndicatorNameFromMineralIndicator(mineralIndicatorName);
            if (indicatorName != null) {
                assertThat(content).usingComparator(comparator).isEqualTo(
                        "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                                "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Application de produits minéraux;" +
                                interventionInfo.interventionName() + ";03/04;03/04;Application de produits minéraux;" +
                                writer.getInputUsageProductName(interventionInfo.mineralProductInputUsage(), interventionInfo.mineralFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                                ";;null;2012, 2013;Fertilisation totale;" + indicatorName + ";" + indicatorInfo.value()
                );

                assertThat(content).usingComparator(comparator).isEqualTo(
                        "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                                "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Application de produits minéraux;" +
                                interventionInfo.interventionName() + ";" + interventionInfo.interventionTopiaId() +
                                ";03/04;03/04;Application de produits minéraux;" +
                                writer.getInputUsageProductName(interventionInfo.mineralProductInputUsage(), interventionInfo.mineralFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                                ";1;;;non;;2012, 2013;Synthétisé;Fertilisation totale;" + indicatorName + ";" + indicatorInfo.value()
                );
            }
        });
    }

    @Test
    public void testRealiseInterventionOrganique2Intrants() {
        final List<DonneesTestFertilisationOrganiqueRealise> donneesTestFertilisation = new LinkedList<>();
        final List<TestDataRealise> testDatas = List.of(
                new TestDataRealise("SDY", OrganicProductUnit.T_HA, 5, 1.0, 1, "10.0", "3.5", "2.0", "0.0", "0.0", "0.0"),
                new TestDataRealise("SDR", OrganicProductUnit.T_HA, 5, 1.0, 1, "147.5", "228.5", "25.0", "75.0", "0.0", "0.0")
        );
        final Map<String, String> totalsExpectedByIndicators = Map.of(
                indicatorN, "157.5",
                indicatorP2O5, "232.0",
                indicatorK2O, "27.0"
        );
        for (TestDataRealise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id);
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S()

            );

            donneesTestFertilisation.add(new DonneesTestFertilisationOrganiqueRealise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg,
                    testData.spatialFrequency,
                    testData.transitCount,
                    Map.of(
                            indicatorN, testData.expectedN,
                            indicatorP2O5, testData.expectedP,
                            indicatorK2O, testData.expectedK
                    )
            ));
        }

        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        List<Zone> zones = zoneTopiaDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        final String nomIntervention = "Intervention %s".formatted(UUID.randomUUID());
        EffectiveIntervention intervention = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, nomIntervention,
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1);
        EffectiveSpeciesStade interventionStades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention.addSpeciesStades(interventionStades);

        Map<DonneesTestFertilisationOrganiqueRealise, OrganicProductInputUsage> organicProductInputUsagesByDonneesTest = new HashMap<>();

        for (DonneesTestFertilisationOrganiqueRealise donneesTest : donneesTestFertilisation) {
            organicProductInputUsagesByDonneesTest.put(donneesTest, organicProductInputUsageDao.create(
                    OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, donneesTest.domainOrganicProductInput,
                    OrganicProductInputUsage.PROPERTY_QT_AVG, donneesTest.qtAvg
            ));
        }

        RefInterventionAgrosystTravailEDI actionSEK = this.refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEK").findAny();
        final OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction = organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEK,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, organicProductInputUsagesByDonneesTest.values(),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTotalFertilization indicatorTotalFertilization = serviceFactory.newInstance(IndicatorTotalFertilization.class);
        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOrganicFertilization, indicatorMineralFertilization, indicatorTotalFertilization);
        final String content = out.toString();

        final List<AbstractInputUsage> organicProductInputUsages = new ArrayList<>();
        donneesTestFertilisation.forEach(donneesTest -> {
            donneesTest.expectedIndicatorNameAndValue().forEach((indicatorName, indicatorValue) -> {
                final OrganicProductInputUsage inputUsage = organicProductInputUsagesByDonneesTest.get(donneesTest);
                organicProductInputUsages.add(inputUsage);

                assertThat(content).usingComparator(comparator).isEqualTo(
                        "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                                "Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;" +
                                "Application de produits minéraux;" + nomIntervention + ";12/03/2013;12/03/2013;Épandage organique;" +
                                writer.getInputUsageProductName(inputUsage, organicFertilizersSpreadingAction, null, cultureBle, true) +
                                ";;null;2013;Fertilisation totale;"  + indicatorName+ ";" + indicatorValue
                );
            });
        });

        totalsExpectedByIndicators.forEach((indicatorName, indicatorValue) -> {
            assertThat(content).usingComparator(comparator).isEqualTo(
                    "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Application de produits minéraux;" +
                            nomIntervention + ";Épandage organique;" +
//                            writer.getInputUsageProductName(organicProductInputUsages.get(0), organicFertilizersSpreadingAction, null, cultureBle, true) + ", " +
//                            writer.getInputUsageProductName(organicProductInputUsages.get(1), organicFertilizersSpreadingAction, null, cultureBle, true) +
                            writer.getInputUsagesToString(organicProductInputUsages, List.of(organicFertilizersSpreadingAction), cultureBle) +
                            ";;N;;2013;Réalisé;Fertilisation totale;" + indicatorName + ";" + indicatorValue
            );
        });
    }

    private String getIndicatorNameFromMineralIndicator(String mineralIndicatorName) {
        final String indicatorN = "Quantité de N minéral apportée (en kg/ha)";
        final String indicatorP2O5 = "Quantité de P2O5 minéral apportée (en kg/ha)";
        final String indicatorK2O = "Quantité de K2O minéral apportée (en kg/ha)";
        return switch (mineralIndicatorName) {
            case indicatorN -> "Quantité totale de N apportée (en kg/ha)";
            case indicatorP2O5 -> "Quantité totale de P2O5 apportée (en kg/ha)";
            case indicatorK2O -> "Quantité totale de K2O apportée (en kg/ha)";
            default -> null;
        };
    }

    private record TestDataRealise(String id, OrganicProductUnit organicProductUnit, double qtAvg, double spatialFrequency, int transitCount, String expectedN, String expectedP, String expectedK, String expectedCaO, String expectedMgO, String expectedS) {}
    private record TestDataSynthetise(String id, OrganicProductUnit organicProductUnit, double qtAvg, double spatialFrequency, double temporalFrequency, String expectedN, String expectedP, String expectedK, String expectedCaO, String expectedMgO, String expectedS) {}

    private record DonneesTestFertilisationOrganiqueRealise(RefFertiOrga refFertiOrga,
                                                            DomainOrganicProductInput domainOrganicProductInput,
                                                            double qtAvg,
                                                            double spatialFrequency,
                                                            int transitCount,
                                                            Map<String, String> expectedIndicatorNameAndValue){}

    private record DonneesTestFertilisationOrganiqueSynthetise(RefFertiOrga refFertiOrga,
                                                               DomainOrganicProductInput domainOrganicProductInput,
                                                               double qtAvg,
                                                               double spatialFrequency,
                                                               double temporalFrequency,
                                                               String inputName, Map<String, String> expectedIndicatorNameAndValue){}

}
