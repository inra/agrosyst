package fr.inra.agrosyst.services.performance.indicators.ift;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Ift;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class IndicatorVintageCropIftTest extends AbstractIftTest{

    public static final String SHORT_PRACTICED_EXPECTED_CONTENT = "%d;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;100;;Dose pour '(%s-%s):%s %s';";
    public static final String SHORT_EFFECTIVE_EXPECTED_CONTENT = "%d;Réalisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;;100;;Dose pour '(%s-%s):%s %s';";

    public static final String PRACTICED_BIOCONTROLE_EXPECTED_CONTENT = "I-%s;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;oui;;%d;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;100;;Dose pour '(%s-%s):%s %s';";
    public static final String EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT = "I-%s-id;04/03/%d;04/03/%d;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;oui;;%d;Réalisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;;100;;Dose pour '(%s-%s):%s %s';";

    public static final String PRACTICED_BIOCONTROLE_EXPECTED_CONTENT_WITHOUT_DOSE = "I-%s;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;oui;;%d;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;88;";
    public static final String EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT_WITHOUT_DOSE = "I-%s-id;04/03/%d;04/03/%d;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;oui;;%d;Réalisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;;88;";

    public static final String PRACTICED_EXPECTED_CONTENT = "I-%s;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;non;;%d;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;100;;Dose pour '(%s-%s):%s %s';";
    public static final String EFFECTIVE_EXPECTED_CONTENT = "I-%s-id;04/03/%d;04/03/%d;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;non;;%d;Réalisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;;100;;Dose pour '(%s-%s):%s %s';";

    public static final String PRACTICED_EXPECTED_CONTENT_WITHOUT_DOSE = "I-%s;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;non;;%d;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;88;";
    public static final String EFFECTIVE_EXPECTED_CONTENT_WITHOUT_DOSE = "I-%s-id;04/03/%d;04/03/%d;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);%s (%s %s);1;%s;;non;;%d;Réalisé;Indicateurs de pression d’utilisation des intrants : IFT à la culture millésimé;%s;%s;;88;";

    private @NotNull IndicatorVintageCropIFT createPerformanceIndicator() {
        IndicatorVintageCropIFT indicator = serviceFactory.newInstance(IndicatorVintageCropIFT.class);
        Collection<Ift> iftsToDisplay = Arrays.stream(Ift.values()).toList();
        indicator.init(iftsToDisplay);

        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(
                performance,
                "", Collections.singletonList(TestDatas.BAULON_TOPIA_ID),
                null, null, null, indicatorFilters, null, true, true, false);

        IndicatorMockWriter writer = new IndicatorMockWriter(new StringWriter());
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);
        return indicator;
    }

    // usage_id                                         6
    // intervention_id
    // Type de saisie                          Synthétisé
    // Fréquence spatiale                               1
    // Fréquence temporelle                             1
    // Nombre de passages
    // Proportion de surface traitée                  100
    // PSCI                                             1
    // PSCI Phyto                                       1
    // code_amm                                         1
    // Intrant                                  Herbicide
    // Quantité d'intrant                               1
    // Unité                                         L_HA
    // code espèce botanique                     10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  100
    // Cibles
    // Campagne                                      2015
    // Commentaire
    // usage_id                                         1
    // intervention_id                                  1
    // Dose de référence                                4
    // IFT Chimique total                            0,25
    // IFT Chimique total HTS                        0,25
    // IFT Chimique total HH                            0
    // IFT Herbicide                                 0,25
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_6() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.25);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.25);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.25);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "6_usage_id",
                1,
                1,
                100.0,
                herbi1,
                1.0d,
                "10-11-12",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages                                  1
        testPour1ApplicationEnRealise(
                "1_usage_id",
                1,
                1,
                100.0,
                herbi1,
                1.0d,
                "10-11-12",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                         7
    // intervention_id
    // Type de saisie               Synthétisé
    // Fréquence spatiale                             0,5
    // Fréquence temporelle                             1
    // Nombre de passages
    // Proportion de surface traitée                  100
    // PSCI                                           0,5
    // PSCI Phyto                                     0,5
    // code_amm                                         1
    // Intrant                      Herbicide
    // Quantité d'intrant                               1
    // Unité                        L_HA
    // code espèce botanique        10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  100
    // Cibles
    // Campagne                                      2020
    // Commentaire
    // usage_id                                         2
    // intervention_id                                  2
    // Dose de référence                                7
    // IFT Chimique total              0,0714285714285714
    // IFT Chimique total HTS          0,0714285714285714
    // IFT Chimique total HH                            0
    // IFT Herbicide                   0,0714285714285714
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_7() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.071);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.071);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.071);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "7_usage_id",
                0.5,
                1,
                100.0,
                herbi1,
                1.0d,
                "10-11-12",
                "100",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages                                  1
        testPour1ApplicationEnRealise(
                "2_usage_id",
                0.5,
                1,
                100.0,
                herbi1,
                1.0d,
                "10-11-12",
                "100",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                         8
    // intervention_id
    // Type de saisie                          Synthétisé
    // Fréquence spatiale                               1
    // Fréquence temporelle                             2
    // Nombre de passages
    // Proportion de surface traitée                  100
    // PSCI                                             2
    // PSCI Phyto                                       2
    // code_amm                                         1
    // Intrant                                  Herbicide
    // Quantité d'intrant                               1
    // Unité                                         L_HA
    // code espèce botanique                     10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  100
    // Cibles
    // Campagne                                      2017
    // Commentaire
    // usage_id                                         3
    // intervention_id                                  3
    // Dose de référence                                1
    // IFT Chimique total                               2
    // IFT Chimique total HTS                           2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                    2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_8() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 2.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 2.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 2.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "8_usage_id",
                1.0,
                2,
                100.0,
                herbi1,
                1.0d,
                "10-11-12",
                "100",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "3_usage_id",
                1.0,
                2,
                100.0,
                herbi1,
                1.0d,
                "10-11-12",
                "100",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                         9
    // intervention_id
    // Type de saisie                          Synthétisé
    // Fréquence spatiale                               1
    // Fréquence temporelle                             1
    // Nombre de passages
    // Proportion de surface traitée                   50
    // PSCI                                             1
    // PSCI Phyto                                     0,5
    // code_amm                                         1
    // Intrant                                  Herbicide
    // Quantité d'intrant                               1
    // Unité                                         L_HA
    // code espèce botanique                     20;21;22
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  100
    // Cibles
    // Campagne                                      2015
    // Commentaire
    // usage_id                                         4
    // intervention_id                                  4
    // Dose de référence                               22
    // IFT Chimique total              0,0227272727272727
    // IFT Chimique total HTS          0,0227272727272727
    // IFT Chimique total HH                            0
    // IFT Herbicide                   0,0227272727272727
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_9() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.023);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.023);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.023);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "9_usage_id",
                1.0,
                1,
                50.0,
                herbi1,
                1.0d,
                "20-21-22",
                "100",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "4_usage_id",
                1.0,
                1,
                50.0,
                herbi1,
                1.0d,
                "20-21-22",
                "100",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                        10
    // intervention_id
    // Type de saisie                          Synthétisé
    // Fréquence spatiale                               1
    // Fréquence temporelle                             1
    // Nombre de passages
    // Proportion de surface traitée                  100
    // PSCI                                             1
    // PSCI Phyto                                       1
    // code_amm                                         1
    // Intrant                                  Herbicide
    // Quantité d'intrant                               2
    // Unité                                         L_HA
    // code espèce botanique                     20;21;22
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  200
    // Cibles
    // Campagne                                      2012
    // Commentaire
    // usage_id                                         5
    // intervention_id                                  5
    // Dose de référence                               22
    // IFT Chimique total              0,0909090909090909
    // IFT Chimique total HTS          0,0909090909090909
    // IFT Chimique total HH                            0
    // IFT Herbicide                   0,0909090909090909
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_10() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.091);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.091);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.091);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "10_usage_id",
                1.0,
                1,
                100.0,
                herbi1,
                2.0d,
                "20-21-22",
                "200",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "5_usage_id",
                1.0,
                1,
                100.0,
                herbi1,
                2.0d,
                "20-21-22",
                "200",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                                                         12
    // intervention_id
    // Type de saisie                                                           Synthétisé
    // Fréquence spatiale                                                             0,25
    // Fréquence temporelle                                                              2
    // Nombre de passages
    // Proportion de surface traitée                                                    80
    // PSCI                                                                            0,5
    // PSCI Phyto                                                                      0,4
    // code_amm                                                                          1
    // Intrant                                                                   Herbicide
    // Quantité d'intrant                                                               10
    // Unité                                                                          L_HA
    // code espèce botanique
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                                                   100
    // Cibles
    // Campagne                                                                       2015
    // Commentaire                  Pas de culture -> pas de dose de référence -> IFT = PS
    // usage_id                                        11
    // intervention_id
    // Dose de référence
    // IFT Chimique total                             0,4
    // IFT Chimique total HTS                         0,4
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,4
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_12_sans_espece() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.4);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.4);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.4);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "12_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "11_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                      14
    // intervention_id
    // Type de saisie                        Synthétisé
    // Fréquence spatiale                          0,25
    // Fréquence temporelle                           2
    // Nombre de passages
    // Proportion de surface traitée                 80
    // PSCI                                         0,5
    // PSCI Phyto                                   0,4
    // code_amm                                       1
    // Intrant                                Herbicide
    // Quantité d'intrant                             2
    // Unité                                       L_HA
    // code espèce botanique                   10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                    2015
    // Commentaire
    // usage_id                                        13
    // intervention_id                                 13
    // Dose de référence                                4
    // IFT Chimique total                             0,2
    // IFT Chimique total HTS                         0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_14_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.2);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "14_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "13_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                             16
    // intervention_id
    // Type de saisie                               Synthétisé
    // Fréquence spatiale                                 0,25
    // Fréquence temporelle                                  2
    // Nombre de passages
    // Proportion de surface traitée                        80
    // PSCI                                                0,5
    // PSCI Phyto                                          0,4
    // code_amm                                              1
    // Intrant                                       Herbicide
    // Quantité d'intrant                                    2
    // Unité                                              L_HA
    // code espèce botanique                             10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                       100
    // Cibles
    // Campagne                                           2015
    // Commentaire
    // usage_id                                        15
    // intervention_id                                 15
    // Dose de référence                                4
    // IFT Chimique total                             0,2
    // IFT Chimique total HTS                         0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_16() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.2);               // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "16_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "15_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           18
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            1
    // Intrant                                     Herbicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                        10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        17
    // intervention_id                                 17
    // Dose de référence                                4
    // IFT Chimique total                             0,2
    // IFT Chimique total HTS                         0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_18_avec_2_groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.2);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "18_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "17_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           20
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            1
    // Intrant                                     Herbicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        19
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total                             0,2
    // IFT Chimique total HTS                         0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_20_avec_2_cod_maa_2groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.2);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "20_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "19_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           22
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            1
    // Intrant                                     Herbicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        21
    // intervention_id              
    // Dose de référence                                4
    // IFT Chimique total                             0,2
    // IFT Chimique total HTS                         0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_22_avec_2_cod_maa_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.2);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "22_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "21_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           24
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            1
    // Intrant                                     Herbicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                        10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                     100
    // Cibles
    // Campagne                                         2018
    // Commentaire
    // usage_id                                        23
    // intervention_id                                 23
    // Dose de référence                                1
    // IFT Chimique total                             0,8
    // IFT Chimique total HTS                         0,8
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,8
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_24_sur_campagne_plus_grande_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.8);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.8);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.8);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "24_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                "100",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "23_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                "100",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           26
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            1
    // Intrant                                     Herbicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                        10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                     100
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                        25
    // intervention_id                                 25
    // Dose de référence                                4
    // IFT Chimique total                             0,2
    // IFT Chimique total HTS                         0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_26_sur_campagne_plus_petite_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.2);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "26_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                "100",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "25_usage_id",
                0.25,
                2,
                80.0,
                herbi1,
                2.0d,
                "10-11-12",
                "100",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // les tests qui suivent sont identiques aux précédents, mais avec un produit Fongicide


    // usage_id                                           32
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                                1
    // PSCI Phyto                                          1
    // code_amm                                            2
    // Intrant                                     Fongicide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                        10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                     100
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        27
    // intervention_id                                  1
    // Dose de référence                                4
    // IFT Chimique total                            0,25
    // IFT Chimique total HTS                        0,25
    // IFT Chimique total HH                         0,25
    // IFT Herbicide                                    0
    // IFT Fongicide                                 0,25
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_32() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.25);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.25);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.25); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.25);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "32_usage_id",
                1,
                1,
                100.0,
                fongi,
                1.0d,
                "10-11-12",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "27_usage_id",
                1,
                1,
                100.0,
                fongi,
                1.0d,
                "10-11-12",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                        33
    // intervention_id
    // Type de saisie                           Synthétisé
    // Fréquence spatiale                             0,5
    // Fréquence temporelle                             1
    // Nombre de passages
    // Proportion de surface traitée                  100
    // PSCI                                           0,5
    // PSCI Phyto                                     0,5
    // code_amm                                         2
    // Intrant                                  Fongicide
    // Quantité d'intrant                               1
    // Unité                                         L_HA
    // code espèce botanique                     10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  100
    // Cibles
    // Campagne                                      2020
    // Commentaire
    // usage_id                                        28
    // intervention_id
    // Dose de référence                                7
    // IFT Chimique total              0,0714285714285714
    // IFT Chimique total HTS          0,0714285714285714
    // IFT Chimique total HH           0,0714285714285714
    // IFT Herbicide                                    0
    // IFT Fongicide                   0,0714285714285714
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_33() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.071);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.071);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.071); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.071);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "33_usage_id",
                0.5,
                1,
                100.0,
                fongi,
                1.0d,
                "10-11-12",
                "100",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "28_usage_id",
                0.5,
                1,
                100.0,
                fongi,
                1.0d,
                "10-11-12",
                "100",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                        34
    // intervention_id
    // Type de saisie                          Synthétisé
    // Fréquence spatiale                               1
    // Fréquence temporelle                             2
    // Nombre de passages
    // Proportion de surface traitée                  100
    // PSCI                                             2
    // PSCI Phyto                                       2
    // code_amm                                         2
    // Intrant                                  Fongicide
    // Quantité d'intrant                               1
    // Unité                                         L_HA
    // code espèce botanique                     10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  100
    // Cibles
    // Campagne                                      2017
    // Commentaire
    // usage_id                                        29
    // intervention_id
    // Dose de référence                                1
    // IFT Chimique total                               2
    // IFT Chimique total HTS                           2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                    2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_34() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 2.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 2.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 2.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 2.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "34_usage_id",
                1.0,
                2,
                100.0,
                fongi,
                1.0d,
                "10-11-12",
                "100",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "29_usage_id",
                1.0,
                2,
                100.0,
                fongi,
                1.0d,
                "10-11-12",
                "100",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                        35
    // intervention_id
    // Type de saisie                          Synthétisé
    // Fréquence spatiale                               1
    // Fréquence temporelle                             1
    // Nombre de passages
    // Proportion de surface traitée                   50
    // PSCI                                             1
    // PSCI Phyto                                     0,5
    // code_amm                                         2
    // Intrant                                  Fongicide
    // Quantité d'intrant                               1
    // Unité                                         L_HA
    // code espèce botanique                     20;21;22
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  100
    // Cibles
    // Campagne                                      2015
    // Commentaire
    // usage_id                                        30
    // intervention_id
    // Dose de référence                               22
    // IFT Chimique total                            0,0227272727272727
    // IFT Chimique total HTS                        0,0227272727272727
    // IFT Chimique total HH                            0
    // IFT Herbicide                                 0,0227272727272727
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_35() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.023);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.023);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.023); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.023);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "35_usage_id",
                1.0,
                1,
                50.0,
                fongi,
                1.0d,
                "20-21-22",
                "100",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "30_usage_id",
                1.0,
                1,
                50.0,
                fongi,
                1.0d,
                "20-21-22",
                "100",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                        36
    // intervention_id
    // Type de saisie                          Synthétisé
    // Fréquence spatiale                               1
    // Fréquence temporelle                             1
    // Nombre de passages
    // Proportion de surface traitée                  100
    // PSCI                                             1
    // PSCI Phyto                                       1
    // code_amm                                         2
    // Intrant                                  Fongicide
    // Quantité d'intrant                               2
    // Unité                                         L_HA
    // code espèce botanique                     20;21;22
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                  200
    // Cibles
    // Campagne                                      2012
    // Commentaire
    // usage_id                                        31
    // intervention_id                                   
    // Dose de référence                               22
    // IFT Chimique total                            0,0909090909090909
    // IFT Chimique total HTS                        0,0909090909090909
    // IFT Chimique total HH                            0
    // IFT Herbicide                                 0,0909090909090909
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_36() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.091);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.091);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.091); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.091);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "36_usage_id",
                1.0,
                1,
                100.0,
                fongi,
                2.0d,
                "20-21-22",
                "200",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "31_usage_id",
                1.0,
                1,
                100.0,
                fongi,
                2.0d,
                "20-21-22",
                "200",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                                                         38
    // intervention_id
    // Type de saisie                                                           Synthétisé
    // Fréquence spatiale                                                             0,25
    // Fréquence temporelle                                                              2
    // Nombre de passages
    // Proportion de surface traitée                                                    80
    // PSCI                                                                            0,5
    // PSCI Phyto                                                                      0,4
    // code_amm                                                                          2
    // Intrant                                                                   Fongicide
    // Quantité d'intrant                                                               10
    // Unité                                                                          L_HA
    // code espèce botanique
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                                                   100
    // Cibles
    // Campagne                                                                       2015
    // Commentaire                  Pas de culture -> pas de dose de référence -> IFT = PS
    // usage_id                                        37
    // intervention_id
    // Dose de référence
    // IFT Chimique total                             0,4
    // IFT Chimique total HTS                         0,4
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,4
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_38_sans_espece() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.4);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.4);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.4);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.4);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "38_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "37_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                      40
    // intervention_id
    // Type de saisie                        Synthétisé
    // Fréquence spatiale                          0,25
    // Fréquence temporelle                           2
    // Nombre de passages
    // Proportion de surface traitée                 80
    // PSCI                                         0,5
    // PSCI Phyto                                   0,4
    // code_amm                                       2
    // Intrant                                Fongicide
    // Quantité d'intrant                             2
    // Unité                                       L_HA
    // code espèce botanique                   10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                    2015
    // Commentaire
    // usage_id                                        39
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,114285714285714
    // IFT Chimique total HTS           0,114285714285714
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,114285714285714
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_40_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                  // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.2);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "40_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "39_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                   42
    // intervention_id
    // Type de saisie                     Synthétisé
    // Fréquence spatiale                       0,25
    // Fréquence temporelle                        2
    // Nombre de passages
    // Proportion de surface traitée              80
    // PSCI                                      0,5
    // PSCI Phyto                                0,4
    // code_amm                                    2
    // Intrant                             Fongicide
    // Quantité d'intrant                          2
    // Unité                                    L_HA
    // code espèce botanique                   10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100
    // Cibles
    // Campagne                                 2015
    // Commentaire
    // usage_id                                        41
    // intervention_id                                   
    // Dose de référence                                4
    // IFT Chimique total               0.2
    // IFT Chimique total HTS           0.2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0.2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_42() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);               // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.2);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "42_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "41_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           44
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            2
    // Intrant                                     Fongicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                        10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        43
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0.2
    // IFT Chimique total HTS           0.2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0.2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_44_avec_2_groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.2);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "44_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "43_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           46
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            2
    // Intrant                                     Fongicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        45
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0.2
    // IFT Chimique total HTS           0.2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0.2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_46_avec_2_cod_maa_2groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.2);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "46_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "45_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           48
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            2
    // Intrant                                     Fongicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        47
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0.2
    // IFT Chimique total HTS           0.2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0.2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_48_avec_2_cod_maa_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.2);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "48_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "47_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           50
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            2
    // Intrant                                     Fongicide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                        10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                     100
    // Cibles
    // Campagne                                         2018
    // Commentaire
    // usage_id                                        24
    // intervention_id
    // Dose de référence                                1
    // IFT Chimique total               0,8
    // IFT Chimique total HTS           0,8
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,8
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_50_sur_campagne_plus_grande_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.8);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.8);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.8);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.8);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "50_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                "100",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "49_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                "100",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           52
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            2
    // Intrant                                     Fongicide
    // Quantité d'intrant                                  2
    // Unité                                             L_HA
    // code espèce botanique                         10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                     100
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                        51
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,2
    // IFT Chimique total HTS           0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_52_sur_campagne_plus_petite_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                  // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.2);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                  // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);                 // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                  // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);        // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "52_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                "100",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "51_usage_id",
                0.25,
                2,
                80.0,
                fongi,
                2.0d,
                "10-11-12",
                "100",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // les tests qui suivent sont identiques aux précédents, mais avec un produit Insecticide


    // usage_id                                           58
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                                1
    // PSCI Phyto                                          1
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                           53
    // intervention_id
    // Dose de référence                                   4
    // IFT Chimique total                  0,25
    // IFT Chimique total HTS              0,25
    // IFT Chimique total HH               0,25
    // IFT Herbicide                                       0
    // IFT Fongicide                       0,25
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                                     0
    @Test
    public void testInterventionSynthetise_usage_id_58() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.25);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.25);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.25); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.25);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "58_usage_id",
                1,
                1,
                100.0,
                insecticide,
                1.0d,
                "10",
                "100;101;102",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        testPour1ApplicationEnRealise(
                "53_usage_id",
                1,
                1,
                100.0,
                insecticide,
                1.0d,
                "10",
                "100;101;102",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           59
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                0,5
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                              0,5
    // PSCI Phyto                                        0,5
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2020
    // Commentaire
    // usage_id                                        54
    // intervention_id
    // Dose de référence                                7
    // IFT Chimique total              0,0714285714285714
    // IFT Chimique total HTS          0,0714285714285714
    // IFT Chimique total HH           0,0714285714285714
    // IFT Herbicide                                    0
    // IFT Fongicide                   0,0714285714285714
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_59() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.071);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.071);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.071); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.071);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "59_usage_id",
                0.5,
                1,
                100.0,
                insecticide,
                1.0d,
                "10",
                "100;101;102",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        testPour1ApplicationEnRealise(
                "54_usage_id",
                0.5,
                1,
                100.0,
                insecticide,
                1.0d,
                "10",
                "100;101;102",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           60
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                                2
    // PSCI Phyto                                          2
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                           60
    // intervention_id
    // Dose de référence                                1
    // IFT Chimique total               2
    // IFT Chimique total HTS           2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_60() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 2.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 2.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 2.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 2.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "60_usage_id",
                1.0,
                2,
                100.0,
                insecticide,
                1.0d,
                "10",
                "100;101;102",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        testPour1ApplicationEnRealise(
                "55_usage_id",
                1.0,
                2,
                100.0,
                insecticide,
                1.0d,
                "10",
                "100;101;102",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           61
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                      50
    // PSCI                                                1
    // PSCI Phyto                                        0,5
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        56
    // intervention_id
    // Dose de référence                               22
    // IFT Chimique total                            0,0227272727272727
    // IFT Chimique total HTS                        0,0227272727272727
    // IFT Chimique total HH                            0
    // IFT Herbicide                                 0,0227272727272727
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_61() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.023);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.023);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.023); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.023);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "61_usage_id",
                1.0,
                1,
                50.0,
                insecticide,
                1.0d,
                "20",
                "100;101;102",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        testPour1ApplicationEnRealise(
                "56_usage_id",
                1.0,
                1,
                50.0,
                insecticide,
                1.0d,
                "20",
                "100;101;102",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           62
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                                1
    // PSCI Phyto                                          1
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             200;201;202
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                        57
    // intervention_id
    // Dose de référence                               22
    // IFT Chimique total                            0,0909090909090909
    // IFT Chimique total HTS                        0,0909090909090909
    // IFT Chimique total HH                            0
    // IFT Herbicide                                 0,0909090909090909
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_62() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.091);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.091);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.091); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.091);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "62_usage_id",
                1.0,
                1,
                100.0,
                insecticide,
                2.0d,
                "20",
                "200;201;202",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        testPour1ApplicationEnRealise(
                "57_usage_id",
                1.0,
                1,
                100.0,
                insecticide,
                2.0d,
                "20",
                "200;201;202",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                                                         64
    // intervention_id
    // Type de saisie                                                           Synthétisé
    // Fréquence spatiale                                                             0,25
    // Fréquence temporelle                                                              2
    // Nombre de passages
    // Proportion de surface traitée                                                    80
    // PSCI                                                                            0,5
    // PSCI Phyto                                                                      0,4
    // code_amm                                                                          3
    // Intrant                                                                 Insecticide
    // Quantité d'intrant                                                               10
    // Unité                                                                          L_HA
    // code espèce botanique
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                                                   100
    // Cibles
    // Campagne                                                                       2015
    // Commentaire                  Pas de culture -> pas de dose de référence -> IFT = PS
    // usage_id                                        63
    // intervention_id
    // Dose de référence
    // IFT Chimique total                             0,4
    // IFT Chimique total HTS                         0,4
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,4
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_64_sans_espece() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.4);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.4);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.4);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.4);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "64_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "36.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "63_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "36.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                      66
    // intervention_id
    // Type de saisie                        Synthétisé
    // Fréquence spatiale                          0,25
    // Fréquence temporelle                           2
    // Nombre de passages
    // Proportion de surface traitée                 80
    // PSCI                                         0,5
    // PSCI Phyto                                   0,4
    // code_amm                                       3
    // Intrant                                Fongicide
    // Quantité d'intrant                             2
    // Unité                                       L_HA
    // code espèce botanique                   10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                    2015
    // Commentaire
    // usage_id                                        65
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,2
    // IFT Chimique total HTS           0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_66_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                  // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.2);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "66_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "65_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                   68
    // intervention_id
    // Type de saisie                     Synthétisé
    // Fréquence spatiale                       0,25
    // Fréquence temporelle                        2
    // Nombre de passages
    // Proportion de surface traitée              80
    // PSCI                                      0,5
    // PSCI Phyto                                0,4
    // code_amm                                    3
    // Intrant                             Fongicide
    // Quantité d'intrant                          2
    // Unité                                    L_HA
    // code espèce botanique                   10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100
    // Cibles
    // Campagne                                 2015
    // Commentaire
    // usage_id                                        67
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,2
    // IFT Chimique total HTS           0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_68() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);               // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.2);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "68_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages 2
        testPour1ApplicationEnRealise(
                "67_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           70
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        69
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,2
    // IFT Chimique total HTS           0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_70_avec_2_groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.2);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "70_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages 2
        testPour1ApplicationEnRealise(
                "69_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           72
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        71
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,2
    // IFT Chimique total HTS           0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_72_avec_2_cod_maa_2groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.2);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "72_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "71_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           74
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                        73
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,2
    // IFT Chimique total HTS           0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_74_avec_2_cod_maa_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.2);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "74_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "73_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           76
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2018
    // Commentaire
    // usage_id                                        75
    // intervention_id
    // Dose de référence                                1
    // IFT Chimique total               0,8
    // IFT Chimique total HTS           0,8
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,8
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_76_sur_campagne_plus_grande_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.8);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.8);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.8);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.8);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "76_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-11-12",
                "100;101;102",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "75_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10-11-12",
                "100;101;102",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           78
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            3
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                           78
    // intervention_id
    // Dose de référence                                4
    // IFT Chimique total               0,2
    // IFT Chimique total HTS           0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                    0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    // IFT Autres                                       0
    // IFT biocontrôle                                  0
    @Test
    public void testInterventionSynthetise_usage_id_78_sur_campagne_plus_petite_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.2);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                  // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.2);                  // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);                 // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                  // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);        // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "78_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10",
                "100;101;102",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "77_usage_id",
                0.25,
                2,
                80.0,
                insecticide,
                2.0d,
                "10",
                "100;101;102",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // les tests qui suivent sont identiques aux précédents, mais avec un produit biocontrol

    // usage_id                                           84
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                                1
    // PSCI Phyto                                          1
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                           79
    // intervention_id
    // Dose de référence                                   4
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                     0,25
    @Test
    public void testInterventionSynthetise_usage_id_84() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.25);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "84_usage_id",
                1,
                1,
                100.0,
                biocontrol,
                1.0d,
                "10",
                "100;101;102",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             1
        testPour1ApplicationEnRealise(
                "79_usage_id",
                1,
                1,
                100.0,
                biocontrol,
                1.0d,
                "10",
                "100;101;102",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           85
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                0,5
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                              0,5
    // PSCI Phyto                                        0,5
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2020
    // Commentaire
    // usage_id                                           80
    // intervention_id
    // Dose de référence                                   7
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                    0,0714285714285714
    @Test
    public void testInterventionSynthetise_usage_id_85() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.071);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "85_usage_id",
                0.5,
                1,
                100.0,
                biocontrol,
                1.0d,
                "10",
                "100;101;102",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             1
        testPour1ApplicationEnRealise(
                "80_usage_id",
                0.5,
                1,
                100.0,
                biocontrol,
                1.0d,
                "10",
                "100;101;102",
                2020,
                "7.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           86
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                                2
    // PSCI Phyto                                          2
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                           81
    // intervention_id
    // Dose de référence                                   1
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                     2.0
    @Test
    public void testInterventionSynthetise_usage_id_86() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 2.0);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "86_usage_id",
                1.0,
                2,
                100.0,
                biocontrol,
                1.0d,
                "10",
                "100;101;102",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             1
        testPour1ApplicationEnRealise(
                "81_usage_id",
                1.0,
                2,
                100.0,
                biocontrol,
                1.0d,
                "10",
                "100;101;102",
                2017,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           87
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                      50
    // PSCI                                                1
    // PSCI Phyto                                        0,5
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  1
    // Unité                                            L_HA
    // code espèce botanique                              20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                           82
    // intervention_id
    // Dose de référence                                  22
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                                  0.023
    @Test
    public void testInterventionSynthetise_usage_id_87() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.023);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "87_usage_id",
                1.0,
                1,
                50.0,
                biocontrol,
                1.0d,
                "20",
                "100;101;102",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             1
        testPour1ApplicationEnRealise(
                "82_usage_id",
                1.0,
                1,
                50.0,
                biocontrol,
                1.0d,
                "20",
                "100;101;102",
                2015,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                           88
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                                  1
    // Fréquence temporelle                                1
    // Nombre de passages
    // Proportion de surface traitée                     100
    // PSCI                                                1
    // PSCI Phyto                                          1
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             200;201;202
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                           83
    // intervention_id
    // Dose de référence                                  22
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                                  0,0909090909090909
    @Test
    public void testInterventionSynthetise_usage_id_88() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0); // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);            // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);              // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);              // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);             // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);              // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.091);    // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "88_usage_id",
                1.0,
                1,
                100.0,
                biocontrol,
                2.0d,
                "20",
                "200;201;202",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             1
        testPour1ApplicationEnRealise(
                "83_usage_id",
                1.0,
                1,
                100.0,
                biocontrol,
                2.0d,
                "20",
                "200;201;202",
                2012,
                "22.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                                                         90
    // intervention_id
    // Type de saisie                                                           Synthétisé
    // Fréquence spatiale                                                             0,25
    // Fréquence temporelle                                                              2
    // Nombre de passages
    // Proportion de surface traitée                                                    80
    // PSCI                                                                            0,5
    // PSCI Phyto                                                                      0,4
    // code_amm                                                                          5
    // Intrant                                                                 Insecticide
    // Quantité d'intrant                                                               10
    // Unité                                                                          L_HA
    // code espèce botanique
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                                                   100
    // Cibles
    // Campagne                                                                       2015
    // Commentaire                  Pas de culture -> pas de dose de référence -> IFT = PS
    // usage_id                                                                         89
    // intervention_id
    // Dose de référence                                                                  
    // IFT Chimique total                                                                0
    // IFT Chimique total HTS                                                            0
    // IFT Chimique total HH                                                             0
    // IFT Herbicide                                                                     0
    // IFT Fongicide                                                                     0
    // IFT insecticide                                                                   0
    // IFT TS                                                                            0
    // IFT Autres                                                                        0
    // IFT biocontrôle                                                                0.04
    @Test
    public void testInterventionSynthetise_usage_id_90_sans_espece() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.4);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "90_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_BIOCONTROLE_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);

        testPour1ApplicationEnRealise(
                "89_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                10.0d,
                "NO_SPECIES",
                "200",
                2015,
                "",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT_WITHOUT_DOSE,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                      92
    // intervention_id
    // Type de saisie                        Synthétisé
    // Fréquence spatiale                          0,25
    // Fréquence temporelle                           2
    // Nombre de passages
    // Proportion de surface traitée                 80
    // PSCI                                         0,5
    // PSCI Phyto                                   0,4
    // code_amm                                       5
    // Intrant                                Fongicide
    // Quantité d'intrant                             2
    // Unité                                       L_HA
    // code espèce botanique                   10;11;12
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                    2015
    // Commentaire
    // usage_id                                      91
    // intervention_id
    // Dose de référence                              4
    // IFT Chimique total                             0
    // IFT Chimique total HTS                         0
    // IFT Chimique total HH                          0
    // IFT Herbicide                                  0
    // IFT Fongicide                                  0
    // IFT insecticide
    // IFT TS                                         0
    // IFT Autres                                     0
    // IFT biocontrôle                              0.2
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_92_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                  // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.2);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "92_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_BIOCONTROLE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        //  Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "91_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-11-12",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                   94
    // intervention_id
    // Type de saisie                     Synthétisé
    // Fréquence spatiale                       0,25
    // Fréquence temporelle                        2
    // Nombre de passages
    // Proportion de surface traitée              80
    // PSCI                                      0,5
    // PSCI Phyto                                0,4
    // code_amm                                    5
    // Intrant                             Fongicide
    // Quantité d'intrant                          2
    // Unité                                    L_HA
    // code espèce botanique                   10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100
    // Cibles
    // Campagne                                 2015
    // Commentaire
    // usage_id                                   93
    // intervention_id
    // Dose de référence                           4
    // IFT Chimique total                          0
    // IFT Chimique total HTS                      0
    // IFT Chimique total HH                       0
    // IFT Herbicide                               0
    // IFT Fongicide                               0
    // IFT insecticide                             0
    // IFT TS                                      0
    // IFT Autres                                  0
    // IFT biocontrôle              0,2
    @Test
    public void testInterventionSynthetise_usage_id_94() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);               // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.2);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "94_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_BIOCONTROLE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "93_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-20",
                "100",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           96
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                           95
    // intervention_id
    // Dose de référence                                   4
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                                   0.2
    @Test
    public void testInterventionSynthetise_usage_id_96_avec_2_groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.2);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "96_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_BIOCONTROLE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "95_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-11-12",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                           98
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                 100;200
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                           20
    // intervention_id
    // Dose de référence                                   4
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                                   0,2
    @Test
    public void testInterventionSynthetise_usage_id_98_avec_2_cod_maa_2groupe_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.2);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "98_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_BIOCONTROLE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        //  Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "97_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-20",
                "100;200",
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                          100
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                           10;20
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’
    // Cibles
    // Campagne                                         2015
    // Commentaire
    // usage_id                                           99
    // intervention_id
    // Dose de référence                                   4
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                                   0,2
    // => Dans le cas où il n'y a pas de cible, on prend la plus petite dose existante
    @Test
    public void testInterventionSynthetise_usage_id_100_avec_2_cod_maa_sans_cible() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.2);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "100_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                PRACTICED_BIOCONTROLE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "99_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-20",
                null,
                2015,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                EFFECTIVE_BIOCONTROL_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                false);
    }

    // usage_id                                          102
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2018
    // Commentaire
    // usage_id                                          101
    // intervention_id
    // Dose de référence                                   1
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                    0,8
    @Test
    public void testInterventionSynthetise_usage_id_102_sur_campagne_plus_grande_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);              // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);               // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.8);      // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "102_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-11-12",
                "100;101;102",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "101_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10-11-12",
                "100;101;102",
                2018,
                "1.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // usage_id                                          104
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                            5
    // Intrant                                   Insecticide
    // Quantité d'intrant                                  2
    // Unité                                            L_HA
    // code espèce botanique                              10
    // code qualifiant AEE
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’             100;101;102
    // Cibles
    // Campagne                                         2012
    // Commentaire
    // usage_id                                          103
    // intervention_id
    // Dose de référence                                   4
    // IFT Chimique total                                  0
    // IFT Chimique total HTS                              0
    // IFT Chimique total HH                               0
    // IFT Herbicide                                       0
    // IFT Fongicide                                       0
    // IFT insecticide                                     0
    // IFT TS                                              0
    // IFT Autres                                          0
    // IFT biocontrôle                                 0.2
    @Test
    public void testInterventionSynthetise_usage_id_104_sur_campagne_plus_petite_que_domaine() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit biocontrol = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.0);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé",0.0);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.0);                  // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                  // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);                 // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                  // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.2);        // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "104_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10",
                "100;101;102",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "103_usage_id",
                0.25,
                2,
                80.0,
                biocontrol,
                2.0d,
                "10",
                "100;101;102",
                2012,
                "4.0",
                PhytoProductUnit.L_HA,
                PhytoProductUnit.L_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }

    // cas de la betterave

    // usage_id                                          108
    // intervention_id
    // Type de saisie                             Synthétisé
    // Fréquence spatiale                               0,25
    // Fréquence temporelle                                2
    // Nombre de passages
    // Proportion de surface traitée                      80
    // PSCI                                              0,5
    // PSCI Phyto                                        0,4
    // code_amm                                      6400401
    // Intrant                                     Herbicide
    // Quantité d'intrant                                 10
    // Unité                                           KG_HA
    // code espèce botanique                             ZAP
    // code qualifiant AEE                               ZAO
    // code type saisonnier AEE
    // code destination AEE
    // code_groupe_cible_maa dans l’                       5
    // Cibles                                      Adventice
    // Campagne                                         2016
    // Commentaire
    // usage_id                                       108
    // intervention_id
    // Dose de référence                               20
    // IFT Chimique total                             0,2
    // IFT Chimique total HTS                         0,2
    // IFT Chimique total HH                            0
    // IFT Herbicide                                  0,2
    // IFT Fongicide                                    0
    // IFT insecticide                                  0
    // IFT TS                                           0
    @Test
    public void testInterventionSynthetise_usage_id_108() throws IOException {

        IndicatorVintageCropIFT indicator = createPerformanceIndicator();

        RefActaTraitementsProduit herbicide = refActaTraitementsProduitsTopiaDao.forNaturalId("3574", 147, france).findUnique();

        Map<String, Double> indicatorExpectedValue = new HashMap<>();
        indicatorExpectedValue.put("IFT chimique total _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT chimique tot hts _ à la culture millésimé", 0.2);
        indicatorExpectedValue.put("IFT hh (ts inclus) _ à la culture millésimé", 0.0);   // IFT Chimique total HH
        indicatorExpectedValue.put("IFT h _ à la culture millésimé", 0.2);                  // IFT Herbicide
        indicatorExpectedValue.put("IFT f _ à la culture millésimé", 0.0);                // IFT Fongicide
        indicatorExpectedValue.put("IFT i _ à la culture millésimé", 0.0);                  // IFT insecticide
        indicatorExpectedValue.put("IFT ts _ à la culture millésimé", 0.0);                 // IFT TS
        indicatorExpectedValue.put("IFT a _ à la culture millésimé", 0.0);                  // IFT Autres
        indicatorExpectedValue.put("IFT biocontrole _ à la culture millésimé", 0.0);        // IFT biocontrôle

        testPour1ApplicationEnSynthetise(
                "108_usage_id",
                0.25,
                2,
                80.0,
                herbicide,
                10.0d,
                "zap-zao",
                "5",
                2016,
                "20.0",
                PhytoProductUnit.KG_HA,
                PhytoProductUnit.KG_HA,
                SHORT_PRACTICED_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);

        // Nombre de passages                             2
        testPour1ApplicationEnRealise(
                "107_usage_id",
                0.25,
                2,
                80.0,
                herbicide,
                10.0d,
                "zap-zao",
                "5",
                2016,
                "20.0",
                PhytoProductUnit.KG_HA,
                PhytoProductUnit.KG_HA,
                SHORT_EFFECTIVE_EXPECTED_CONTENT,
                indicator,
                indicatorExpectedValue,
                true);
    }
}
