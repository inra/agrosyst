package fr.inra.agrosyst.services.measurement;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA
 * Copyright (C) 2020 - 2044 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.VariableType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.measure.HorizonType;
import fr.inra.agrosyst.api.entities.measure.Measure;
import fr.inra.agrosyst.api.entities.measure.MeasureImpl;
import fr.inra.agrosyst.api.entities.measure.MeasureType;
import fr.inra.agrosyst.api.entities.measure.MeasurementSession;
import fr.inra.agrosyst.api.entities.measure.MeasurementSessionImpl;
import fr.inra.agrosyst.api.entities.measure.MeasurementType;
import fr.inra.agrosyst.api.entities.measure.Observation;
import fr.inra.agrosyst.api.entities.measure.ObservationImpl;
import fr.inra.agrosyst.api.entities.referential.RefMesureTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObsTopiaDao;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveZoneFilter;
import fr.inra.agrosyst.api.services.measurement.MeasurementService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaDaoSupplier;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class MeasurementServiceTest extends AbstractAgrosystTest {

    protected MeasurementService measurementService;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected DomainService domainService;
    protected PlotService plotService;
    protected RefMesureTopiaDao refMesureTopiaDao;
    protected RefProtocoleVgObsTopiaDao refProtocoleVgObsTopiaDao;

    @BeforeEach
    public void setUp() {
        measurementService = serviceFactory.newService(MeasurementService.class);
        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);
        domainService = serviceFactory.newService(DomainService.class);
        plotService = serviceFactory.newService(PlotService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        //testDatas.importAllEspeces();
        refMesureTopiaDao = serviceContext.getDaoSupplier().getRefMesureDao();
        refProtocoleVgObsTopiaDao = serviceContext.getDaoSupplier().getRefProtocoleVgObsDao();
        loginAsAdmin();
        alterSchema();
    }
 
    /**
     * Crée quelques mesures de tests.
     */
    protected void createTestMesures() throws IOException {
        // required imports
        testDatas.createTestPlots();
        testDatas.importMesure();
        testDatas.importProtocoleVgObs();

        // get first zone of first plot of Baulon domain
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        Domain domainBaulon = domainService.getFilteredDomains(domainFilter).getElements().getFirst();
        List<Plot> plots = plotService.findAllForDomain(domainBaulon);
        Plot plot1 = plots.getFirst();
        Zone zone1 = plotService.getZonesAndUsages(plot1.getTopiaId()).getElements().getFirst();
        Assertions.assertNotNull(zone1);

        // create some mesure for that zone
        MeasurementSession session = new MeasurementSessionImpl();
        session.setStartDate(serviceContext.getCurrentDate());
        session.setEndDate(serviceContext.getCurrentDate());

        // Mesure sol
        Measure mesureSol = new MeasureImpl();
        mesureSol.setMeasurementType(MeasurementType.SOL);
        mesureSol.setMeasuringProtocol("test");
        mesureSol.setHorizonType(HorizonType.HT_0_15);
        mesureSol.setRepetitionNumber(1);
        mesureSol.setSampling("ech-1");
        mesureSol.setRefMesure(refMesureTopiaDao.forNaturalId("Teneur en carbone organique", MeasurementType.SOL, VariableType.ANALYSE_DE_SOL).findUnique());
        mesureSol.setMeasureType(MeasureType.INDIVIDUAL_VALUE_COMPUTED);
        mesureSol.setMeasureUnit("g/kg");

        // Empty one
        Measure measureMeteo = new MeasureImpl();
        measureMeteo.setMeasurementType(MeasurementType.METEO);

        // observation vgobs
        Observation obsVgObs = new ObservationImpl();
        obsVgObs.setMeasurementType(MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES);
        obsVgObs.setProtocolVgObs(true);
        obsVgObs.setProtocol(refProtocoleVgObsTopiaDao.forNaturalId("PR036",
                "PR036-AEE166ANARLI-1", "1", "AAK36", "").findUnique());
        Assertions.assertNotNull(obsVgObs.getProtocol()); // Tour eiffel : check !
        obsVgObs.setComment("taiste");

        // empty one
        Observation obsStade = new ObservationImpl();
        obsStade.setMeasurementType(MeasurementType.STADE_CULTURE);

        // save those mesures with session
        /*mesureSol.setMeasurementSession(session);
        measureMeteo.setMeasurementSession(session);
        obsStade.setMeasurementSession(session);
        obsVgObs.setMeasurementSession(session);*/
        session.addAllMeasurements(Arrays.asList(mesureSol, measureMeteo, obsStade, obsVgObs));
        measurementService.updateMeasurementSessions(zone1, Collections.singletonList(session));
    }

    /**
     * Assure que les enum ont une concordance avec les référentiels.
     */
    @Test
    public void testEnumGetVariables() throws IOException {
        testDatas.importMesure();

        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.PLANTE).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.SOL).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.TRANSFERT_DE_SOLUTES).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.GES).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.METEO).size() >= 1);
        // mais pas les observations
        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.ADVENTICES).isEmpty());
        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.NUISIBLE_MALADIES_PHYSIOLOGIQUES_AUXILIAIRES).isEmpty());
        Assertions.assertTrue(measurementService.findAllVariableTypes(MeasurementType.STADE_CULTURE).isEmpty());
    }
    
    /**
     * Assure que les enum et les types de variables ont une concordance avec les référentiels.
     */
    @Test
    public void testEnumGetMesures() throws IOException {
        testDatas.importMesure();

        Assertions.assertTrue(measurementService.findAllVariables(MeasurementType.PLANTE, VariableType.ANALYSES).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariables(MeasurementType.SOL, VariableType.ANALYSE_DE_SOL).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariables(MeasurementType.TRANSFERT_DE_SOLUTES, null).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariables(MeasurementType.GES, null).size() >= 1);
        Assertions.assertTrue(measurementService.findAllVariables(MeasurementType.METEO, null).size() >= 1);
    }

    /**
     * Test de recherche des cultures utilisé dans les cycles réalisés pour une zone.
     */
    @Test
    public void getCroppingPlanEntriesForZone() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiMinUnifa();
        testDatas.createTestEffectiveInterventions();

        // get test zone
        EffectiveZoneFilter plotFilter = new EffectiveZoneFilter();
        plotFilter.setPageSize(1);
        Zone zoneBaulon = effectiveCropCycleService.getFilteredZones(plotFilter).getElements().getFirst();
        TopiaDaoSupplier supplier = getDaoSupplier();
        Assertions.assertEquals("TEST_PLOT_2", zoneBaulon.getPlot().getName());

        // test get cropping plan entry
        Set<CroppingPlanEntry> croppingPlanEntries = measurementService.getZoneCroppingPlanEntries(zoneBaulon);
        Assertions.assertEquals(0, croppingPlanEntries.size());
    }

    /**
     * Test measurement xls export.
     * @throws IOException 
     */
    @Test
    public void testExportXls() throws IOException {
        createTestMesures();
        // empty filter
        EffectiveZoneFilter zoneFilter = new EffectiveZoneFilter();
        List<Zone> zones = effectiveCropCycleService.getFilteredZones(zoneFilter).getElements();
        List<String> zoneIds = Lists.transform(zones, Entities.GET_TOPIA_ID::apply);

        try (InputStream is = measurementService.exportEffectiveMeasurementsAsXls(zoneIds).content().toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        }
    }
    
    /**
     * Test measurement xls model export.
     * @throws IOException 
     */
    @Test
    public void testExportXlsEmpty() throws IOException {

        try (InputStream is = measurementService.exportEffectiveMeasurementsAsXls(null).content().toInputStream()){
            Assertions.assertTrue(is.available() > 1);
        }
    }

}
