package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.common.CacheService;

/**
 * Created by davidcosse on 04/12/15.
 */
public class ImportServiceImplForTest extends ImportServiceImpl {

    public ImportServiceImplForTest(RefLocationTopiaDao refLocationDao,
                                    RefSolArvalisTopiaDao refSolArvalisDao,
                                    RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao,
                                    HarvestingPriceTopiaDao priceTopiaDao,
                                    CacheService cacheService,
                                    ServiceContext context) {
        this.refLocationDao = refLocationDao;
        this.refSolArvalisDao = refSolArvalisDao;
        this.refFertiMinUNIFADao = refFertiMinUNIFATopiaDao;
        this.harvestingPriceDao = priceTopiaDao;
        this.cacheService = cacheService;
        this.context = context;
    }

}
