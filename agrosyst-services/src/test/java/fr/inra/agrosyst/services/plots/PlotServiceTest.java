package fr.inra.agrosyst.services.plots;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA
 * Copyright (C) 2020 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.SolHorizon;
import fr.inra.agrosyst.api.entities.SolHorizonImpl;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.entities.performance.PerformanceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.InputStream;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class PlotServiceTest extends AbstractAgrosystTest {

    protected DomainService domainService;
    protected ReferentialService referentialService;
    protected PlotService plotService;

    protected AgrosystI18nService i18nService;
    protected PerformanceTopiaDao performanceTopiaDao;
    protected ZoneTopiaDao zoneTopiaDao;
    protected PlotTopiaDao plotTopiaDao;
    protected RefSolArvalisTopiaDao refSolArvalisDao;
    protected AgrosystUserTopiaDao userTopiaDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        loginAsAdmin();
        alterSchema();

        performanceTopiaDao = getPersistenceContext().getPerformanceDao();
        zoneTopiaDao = getPersistenceContext().getZoneDao();
        plotTopiaDao = getPersistenceContext().getPlotDao();
        refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        userTopiaDao = getPersistenceContext().getAgrosystUserDao();

        domainService = serviceFactory.newService(DomainService.class);
        plotService = serviceFactory.newService(PlotService.class);
        i18nService = serviceFactory.newService(AgrosystI18nService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        plotService = serviceFactory.newService(PlotService.class);

        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.createTestPlots();
    }

    /**
     * Anomalie #3387: org.hibernate.HibernateException:
     * Don't change the reference to a collection with cascade="all-delete-orphan": fr.inra.agrosyst.api.entities.PlotImpl.solHorizon
     * @throws DomainExtendException 
     */
    @Test
    public void testPlotSolHorizonExtends() throws DomainExtendException, IOException {
        // import required by test:
        testDatas.importSolArvalis();

        // get ref sol arvalis:
        List<RefSolArvalis> solArvalis = referentialService.getSolArvalis(72); // aquitaine

        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        Domain domainBaulon = domainService.getFilteredDomains(domainFilter).getElements().getFirst();

        // create domain sol
        Ground sol = domainService.newSol();
        sol.setName("Test sol baulon");
        sol.setRefSolArvalis(solArvalis.get(0));
        List<Ground> sols = Lists.newArrayList(sol);
        domainService.createOrUpdateDomain(domainBaulon, domainBaulon.getLocation().getTopiaId(), null, null, null,
                null, null, sols, null, null, null, null, null);

        List<Ground> grounds = domainService.getGrounds(domainBaulon.getTopiaId());
        Assertions.assertEquals(1, grounds.size());
        Assertions.assertEquals(sol.getRefSolArvalis(), grounds.getFirst().getRefSolArvalis());
        Assertions.assertEquals(sol.getName(), grounds.getFirst().getName());

        List<Plot> plots = getPersistenceContext().getPlotDao().findAllOrderByNameForDomain(domainBaulon);
        Assertions.assertEquals(4, plots.size());
        Plot plot = plots.getFirst();
        List<Zone> plotZones = zoneTopiaDao.forPlotEquals(plot).findAll();
        Assertions.assertEquals("Plot Baulon 1", plot.getName());

        // Create plot sol
        plot.setGround(grounds.get(0));
        SolHorizon solHorizon = new SolHorizonImpl();
        solHorizon.setLowRating(42.0);
        plot.addSolHorizon(solHorizon);
        plotService.createOrUpdatePlot(plot, plot.getDomain().getTopiaId(), plot.getDomain().getLocation().getTopiaId(),
                plot.getGrowingSystem().getTopiaId(), null, null, null, null, null, null, plotZones, null);
        
        // test extends
        Domain baulon2018 = domainService.extendDomain(domainBaulon.getTopiaId(), 2018);
        plots = plotService.findAllForDomain(baulon2018);
        Plot plot2018 = plots.getFirst();
        Assertions.assertEquals("Plot Baulon 1", plot2018.getName());
        Assertions.assertNotEquals(plot, plot2018); // test plot duplicate
        Assertions.assertEquals(1, plot2018.getSolHorizon().size()); // test plot duplicate
        Assertions.assertEquals(plot.getDomain().getLocation().getTopiaId(), plot2018.getLocation().getTopiaId());
    
        plotZones = zoneTopiaDao.forPlotEquals(plot2018).findAll();
        Plot res = plotService.createOrUpdatePlot(plot2018, plot2018.getDomain().getTopiaId(), null,
                plot2018.getGrowingSystem().getTopiaId(), null, null, null, null, null, null, plotZones, null);
    
        Assertions.assertEquals("Plot Baulon 1", res.getName());
        Assertions.assertEquals(plot2018, res); // test plot duplicate
        Assertions.assertNull(plot2018.getLocation());
        Assertions.assertEquals(1, plot2018.getSolHorizon().size()); // test plot duplicate
    }

    /**
     * Test plot duplicate.
     */
    @Test
    public void testPlotDuplicate() {
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        Domain domainBaulon = domainService.getFilteredDomains(domainFilter).getElements().getFirst();

        List<Plot> plots = plotService.findAllForDomain(domainBaulon);
        Assertions.assertEquals(4, plots.size());
        Plot plot = plots.getFirst();
        Assertions.assertNotNull(plot.geteDaplosIssuerId());

        Plot plot2 = plotService.duplicatePlot(plot.getTopiaId());
        Assertions.assertNotNull(plot2);
        Assertions.assertNull(plot2.geteDaplosIssuerId());

        // assert plots
        plots = plotService.findAllForDomain(domainBaulon);
        Assertions.assertEquals(5, plots.size());
        Assertions.assertNotEquals(plot.getCode(), plot2.getCode());
        // Check eDaplosIssuerId has not been deleted
        Optional<Plot> plotOptional = plots.stream().filter(filterPlot -> plot.getTopiaId().equals(filterPlot.getTopiaId())).findFirst();
        Assertions.assertEquals(plot.geteDaplosIssuerId(), plotOptional.get().geteDaplosIssuerId());

    }

    /**
     * Test to duplicate and then merge plots.
     */
    @Test
    public void testPlotMerge() {
        // duplicate a plot
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        Domain domainBaulon = domainService.getFilteredDomains(domainFilter).getElements().getFirst();
        List<Plot> plots = plotService.findAllForDomain(domainBaulon);
        Plot plot1 = plots.getFirst();
        plot1.setArea(11.0);
        Plot plot2 = plotService.duplicatePlot(plot1.getTopiaId());
        plot2.setArea(31.0);

        // re merge duplicated plot
        Plot plot = plotService.mergePlots(Arrays.asList(plot1.getTopiaId(), plot2.getTopiaId()));
        Assertions.assertEquals(plot2, plot);
        Assertions.assertEquals(42.0, plot.getArea());
    }

    /**
     * Test DAO getDomainPlotTotalArea.
     */
    @Test
    public void testGetDomainTotalArea() {
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        Domain domainBaulon = domainService.getFilteredDomains(domainFilter).getElements().getFirst();
        String domainBaulonTopiaId = domainBaulon.getTopiaId();
        double totalArea = domainService.getDomainSAUArea(domainBaulonTopiaId);
        Assertions.assertEquals(100.0, totalArea, 0.001);

        double plotTotalArea = plotTopiaDao.getDomainPlotTotalArea(domainBaulonTopiaId);
        Assertions.assertEquals(149.0, plotTotalArea, 0.001);
    }

    @Test
    public void testPlotAffected() {
//        Boolean forZonesUsedForPerformances = true;
//        Boolean forZonesUsedForMesurementSessions = true;
//        Boolean forZonesUsedForEffectivePerennialCropCycles = true;
//        Boolean forZonesUsedForEffectiveSeasonalCropCycles = true;

//        testDatas.createTestPlots();

        AgrosystUser user;
        if (userTopiaDao.count() == 0L) {
            testDatas.createTestUsers();
            user = userTopiaDao.forEmailEquals("jenvoie@atoit.fr").findAny();
        } else {
            user = userTopiaDao.findAll().getFirst();
        }

        Plot plot = plotTopiaDao.findAll().getFirst();

        List<Zone> zones = zoneTopiaDao.forPlotEquals(plot).findAll();

        Zone zone;
        if (zones.isEmpty()) {
            zone = zoneTopiaDao.create(
                    Zone.PROPERTY_PLOT, plot,
                    Zone.PROPERTY_CODE, UUID.randomUUID().toString()
            );
        } else {
            zone = zones.getFirst();
        }

        zones = zoneTopiaDao.forPlotEquals(plot).findAll();


        performanceTopiaDao.create(
                Performance.PROPERTY_EXPORT_TYPE, ExportType.FILE,
                Performance.PROPERTY_NAME, "performance_test",
                Performance.PROPERTY_UPDATE_DATE, OffsetDateTime.now(),
                Performance.PROPERTY_AUTHOR, user,
                Performance.PROPERTY_COMPUTE_STATUS, PerformanceState.SUCCESS,
                Performance.PROPERTY_ZONES, Collections.singleton(zone)
        );

        getCurrentTransaction().commit();

        UsageList<Zone> usages = plotService.getZonesAndUsages(plot.getTopiaId());
        Assertions.assertEquals(zones.size(), usages.getElements().size());
        Assertions.assertEquals(true, usages.getUsageMap().get(zone.getTopiaId()));

        //TODO david 19/03/2014 finir le test
    }
    
    /**
     * Test plots xls export.
     * @throws IOException 
     */
    @Test
    public void testExportXls() throws IOException {
        // empty filter
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        Domain domainBaulon = domainService.getFilteredDomains(domainFilter).getElements().getFirst();
        List<Plot> plots = plotService.findAllForDomain(domainBaulon);
        List<String> plotIds = plots.stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());

        try (InputStream is = plotService.exportPlotsAsXls(plotIds).content().toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        }
    }

    /**
     * Test plots xls modele export.
     * @throws IOException 
     */
    @Test
    public void testExportXlsEmpty() throws IOException {

        try (InputStream is = plotService.exportPlotsAsXls(null).content().toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        }
    }

    @Test
    public void testTranslationSolArvalis() throws IOException {
        testDatas.importSolArvalis();
        testDatas.insertReferentialTranslations(getPersistenceContext());

        List<RefSolArvalis> solArvalis = referentialService.getSolArvalis(72); // aquitaine
        Assertions.assertTrue(!solArvalis.isEmpty());
        RefSolArvalis refSolArvalis = solArvalis.getFirst();
        Assertions.assertFalse(refSolArvalis.getSol_nom().endsWith("i18n"));
        Assertions.assertFalse(refSolArvalis.getSol_texture().endsWith("i18n"));
        Assertions.assertTrue(refSolArvalis.getSol_nom_Translated().endsWith("i18n"));
        Assertions.assertTrue(refSolArvalis.getSol_texture_Translated().endsWith("i18n"));

        getPersistenceContext().commit();

        RefSolArvalis refSolArvalis1 = refSolArvalisDao.forTopiaIdEquals(refSolArvalis.getTopiaId()).findUnique();
        Assertions.assertFalse(refSolArvalis1.getSol_nom().endsWith("i18n"));
        Assertions.assertFalse(refSolArvalis1.getSol_texture().endsWith("i18n"));

        refSolArvalisDao.clear();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);
        i18nService.fillRefSolArvalisTranslations(List.of(refSolArvalis.getTopiaId()), translationMap);

        ReferentialEntityTranslation entityTranslation = translationMap.getEntityTranslation(refSolArvalis.getTopiaId());
        String solNom = entityTranslation.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_NOM, "");
        String solTexture = entityTranslation.getPropertyTranslation(RefSolArvalis.PROPERTY_SOL_TEXTURE, "");
        Assertions.assertTrue(solNom.endsWith("i18n"));
        Assertions.assertTrue(solTexture.endsWith("i18n"));
    }
}
