package fr.inra.agrosyst.services.performance.indicators.ift;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPCTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.domain.DomainServiceImpl;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorLegacyIFTTest extends AbstractAgrosystTest {

    protected AgrosystI18nService i18nService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected DomainService domainService;
    protected PerformanceServiceImplForTest performanceService;
    protected GrowingSystemService growingSystemService;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected ImportService importService;

    protected DomainTopiaDao domainDao;
    protected ZoneTopiaDao zoneTopiaDao;
    protected EffectiveCropCyclePhaseTopiaDao cropCyclePhaseDAO;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDAO;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeTopiaDao;
    protected PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionTopiaDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDIDAO;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitsTopiaDao;
    
    protected SeedingActionUsageTopiaDao seedingActionUsageTopiaDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleTopiaDao;
    protected EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeTopiaDao;
    protected EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionTopiaDao;

    protected PesticideProductInputUsageTopiaDao pesticideProductInputUsageDao;
    
    @BeforeEach
    public void setupServices() throws TopiaException {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        i18nService = serviceFactory.newService(AgrosystI18nService.class);
        growingSystemService = serviceFactory.newService(GrowingSystemService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);
        importService = serviceFactory.newService(ImportService.class);
        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        domainService = serviceFactory.newService(DomainService.class);

        // init dao
        domainDao = serviceContext.getPersistenceContext().getDomainDao();
        zoneTopiaDao = serviceContext.getPersistenceContext().getZoneDao();
        cropCyclePhaseDAO = serviceContext.getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = serviceContext.getPersistenceContext().getEffectivePerennialCropCycleDao();
        effectiveInterventionDAO = serviceContext.getPersistenceContext().getEffectiveInterventionDao();
        effectiveSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getEffectiveSpeciesStadeDao();
        pesticidesSpreadingActionTopiaDao = serviceContext.getPersistenceContext().getPesticidesSpreadingActionDao();
        refActionAgrosystTravailEDIDAO = serviceContext.getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        refActaTraitementsProduitsTopiaDao = serviceContext.getPersistenceContext().getRefActaTraitementsProduitDao();
        seedingActionUsageTopiaDao = serviceContext.getPersistenceContext().getSeedingActionUsageDao();
        effectiveSeasonalCropCycleTopiaDao  = serviceContext.getPersistenceContext().getEffectiveSeasonalCropCycleDao();
        effectiveCropCycleNodeTopiaDao  = serviceContext.getPersistenceContext().getEffectiveCropCycleNodeDao();
        effectiveCropCycleConnectionTopiaDao  = serviceContext.getPersistenceContext().getEffectiveCropCycleConnectionDao();
    
        pesticideProductInputUsageDao = serviceContext.getPersistenceContext().getPesticideProductInputUsageDao();
        
        // login
        loginAsAdmin();
        alterSchema();
    }

    public static final Function<CroppingPlanEntry, String> GET_CPE_NAME = input -> input == null ? null : input.getName();

    protected static final Function<CroppingPlanSpecies, String> GET_CPS_VARIETY_DENOMINATION = input -> {
        RefVarieteGeves variety = input == null ? null : (RefVarieteGeves) input.getVariety();
        String result = variety == null ? null : variety.getDenomination();
        return result;
    };
    
    /**
     * refs #4058 Implementation d'un test issue des données fournies par l'INRA (Exemple-calcul-IFT.xlsx).
     *
     * Numéro Intervention  Type produit              Nom produit        PSC Surface parcelle    PSCi    Dose de référence   Unité   Dose appliquée  Unité   IFT intervention
     * 1                    Herbicide                 Archipel           100 1                   100     240                 g/ha    150             g/ha    0,6
     * 1                    Adjuvant                  Actirob            100 1                   100     1                   L/ha    1               L/ha
     * 2                    Substance de croissance   Cycocel CL         100 1                   100     2,5                 L/ha    2               L/ha    0,8
     * 3                    Herbicide                 Allié max SX       75  1                   75      35                  g/ha    20              g/ha    0,43
     * 4                    Fongicide                 Pyros EW           100 1                   100     1                   L/ha    0,7             L/ha    0,7
     * 5                    Fongicide                 Opus               100 1                   100     1                   L/ha    0,6             L/ha    0,6
     * 6                    Fongicide                 Fandango S         100 1                   100     2                   L/ha    1,2             L/ha    0,6
     *
     * Ce test produit un warning:
     * Can't find reference dose for Actirob B/Divers - Divers - Huiles adjuvantes
     * Mais c'est normal.
     */
    @Test
    public void testInraDataIFT() throws IOException {
        // required imports
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaDosageSpc();
        testDatas.importActaGroupeCultures();
    
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = baulon.getLocation().getRefCountry();
        
        RefActaTraitementsProduit produitArchipel = refActaTraitementsProduitsTopiaDao.forNaturalId("5166", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductArchipelDto = testDatas.createDomainPhytoProductDto(produitArchipel, ProductType.HERBICIDAL, PhytoProductUnit.G_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
    
        RefActaTraitementsProduit produitActirobB = refActaTraitementsProduitsTopiaDao.forNaturalId("2838", 124, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductActirobBDto = testDatas.createDomainPhytoProductDto(produitActirobB, ProductType.ADJUVANTS, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
    
        RefActaTraitementsProduit produitCyclocelCL = refActaTraitementsProduitsTopiaDao.forNaturalId("4569", 123, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductCyclocelCLDto = testDatas.createDomainPhytoProductDto(produitCyclocelCL, ProductType.GROWTH_SUBSTANCES, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
    
        RefActaTraitementsProduit produitAllie = refActaTraitementsProduitsTopiaDao.forNaturalId("5190", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductAllieDto = testDatas.createDomainPhytoProductDto(produitAllie, ProductType.HERBICIDAL, PhytoProductUnit.G_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
    
        RefActaTraitementsProduit produitPyrosEW = refActaTraitementsProduitsTopiaDao.forNaturalId("3154", 140, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductPyrosEWDto = testDatas.createDomainPhytoProductDto(produitPyrosEW, ProductType.FUNGICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
    
        RefActaTraitementsProduit produitOpus = refActaTraitementsProduitsTopiaDao.forNaturalId("3027", 140, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductOpusDto = testDatas.createDomainPhytoProductDto(produitOpus, ProductType.FUNGICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
    
        RefActaTraitementsProduit produitFandango = refActaTraitementsProduitsTopiaDao.forNaturalId("4840", 140, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductFandangoDto = testDatas.createDomainPhytoProductDto(produitFandango, ProductType.FUNGICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        
        final ArrayList<DomainInputDto> domainInputs = Lists.newArrayList(
                domainPhytoProductArchipelDto,
                domainPhytoProductActirobBDto,
                domainPhytoProductCyclocelCLDto,
                domainPhytoProductAllieDto,
                domainPhytoProductPyrosEWDto,
                domainPhytoProductOpusDto,
                domainPhytoProductFandangoDto);
    
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputs, pesristedCropResult);
    
        final Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(baulon);
        List<AbstractDomainInputStockUnit> domainPhytoInputs = domainInputStock.get(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        Map<RefActaTraitementsProduit, DomainPhytoProductInput> domainInputPhytoByRefInput =
                domainPhytoInputs.stream()
                        .map(dp -> ((DomainPhytoProductInput)dp))
                        .collect(Collectors.toMap(DomainPhytoProductInput::getRefInput, Function.identity()));
    
    
        // "Plot Baulon 1" (blé tendre d'hiver)
        List<Zone> zones = zoneTopiaDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();
        
        EffectiveCropCyclePhase cropCyclePhase = cropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        // ajout d'une intervention
        EffectiveIntervention intervention1 = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention 1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.LUTTE_BIOLOGIQUE,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1);

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention1.addSpeciesStades(intervention1Stades);
        
        // une action et son intrant (Herbicide Archipel)
        final DomainPhytoProductInput domainPhytoProductArchipel = domainInputPhytoByRefInput.get(produitArchipel);
        final PesticideProductInputUsage archipelUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductArchipel,
                PesticideProductInputUsage.PROPERTY_TARGETS, null,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, 150.0
        );
        // un autre intrant (Actirob B)
        // Non pris en compte dans calcul IFT
        final DomainPhytoProductInput domainPhytoProductActirobB = domainInputPhytoByRefInput.get(produitActirobB);
        final PesticideProductInputUsage actirobBUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductActirobB,
                PesticideProductInputUsage.PROPERTY_TARGETS, null,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, 1.0
        );
        
        // une action et son intrant (Archipel) + un autre intrant (Actirob B) non pris en compte dans calcul IFT
        RefInterventionAgrosystTravailEDI actionSEX = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEX").findAny();
        PesticidesSpreadingAction pestAction1 = pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention1,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(archipelUsage, actirobBUsage));
        
        // ajout d'une intervention
        EffectiveIntervention intervention2 = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention 2",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.LUTTE_BIOLOGIQUE,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1);

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention2Stades = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention2.addSpeciesStades(intervention2Stades);
        
        final DomainPhytoProductInput domainPhytoProductCyclocelCL = domainInputPhytoByRefInput.get(produitCyclocelCL);
        final PesticideProductInputUsage cyclocelCLUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductCyclocelCL,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, 2.0
        );
        // une action et son intrant (Cycocel CL)
        PesticidesSpreadingAction pestAction2 = pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention2,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(cyclocelCLUsage)
        );
        
        // ajout d'une intervention
        EffectiveIntervention intervention3 = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention 3",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.LUTTE_BIOLOGIQUE,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.75);

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention3Stades = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention3.addSpeciesStades(intervention3Stades);
    
        final DomainPhytoProductInput domainPhytoProductAllie = domainInputPhytoByRefInput.get(produitAllie);
        final PesticideProductInputUsage allieUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductAllie,
                PesticideProductInputUsage.PROPERTY_TARGETS, null,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, 20.0
        );
        // une action et son intrant (Allié Max SX)
        PesticidesSpreadingAction pestAction3 = pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention3,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(allieUsage)
        );
        
        // ajout d'une intervention
        EffectiveIntervention intervention4 = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention 4",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.LUTTE_BIOLOGIQUE,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1);

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention4Stades = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention4.addSpeciesStades(intervention4Stades);
    
        final DomainPhytoProductInput domainPhytoProductPyrosEW= domainInputPhytoByRefInput.get(produitPyrosEW);
        final PesticideProductInputUsage pyrosEWUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductPyrosEW,
                PesticideProductInputUsage.PROPERTY_TARGETS, null,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, 0.7
        );
        // une action et son intrant (Allié Max SX)
        PesticidesSpreadingAction pestAction4 = pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention4,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(pyrosEWUsage));
        
        // ajout d'une intervention
        EffectiveIntervention intervention5 = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention 5",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.LUTTE_BIOLOGIQUE,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1);

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention5Stades = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention5.addSpeciesStades(intervention5Stades);

        // Fongicide - Opus
        final DomainPhytoProductInput domainPhytoProducOpus = domainInputPhytoByRefInput.get(produitOpus);
        final PesticideProductInputUsage opusUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProducOpus,
                PesticideProductInputUsage.PROPERTY_TARGETS, null,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, 0.6
        );
        
        // une action et son intrant (Opus)
        pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention5,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(opusUsage));

        // ajout d'une intervention
        EffectiveIntervention intervention6 = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention 6",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.LUTTE_BIOLOGIQUE,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1);

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention6Stades = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention6.addSpeciesStades(intervention6Stades);
    
        final DomainPhytoProductInput domainPhytoProductFandango = domainInputPhytoByRefInput.get(produitFandango);
        final PesticideProductInputUsage fandangoUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductFandango,
                PesticideProductInputUsage.PROPERTY_TARGETS, null,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, 1.2
        );
        
        // une action et son intrant (Fandango)
        pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention6,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(fandangoUsage)
        );
    
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        
        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        final IndicatorLegacyIFT indicatorLegacyIFT = serviceFactory.newInstance(IndicatorLegacyIFT.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorLegacyIFT);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator).isEqualTo("IFT chimique total _ à l'ancienne;2013;Baulon;3.75");
        assertThat(content).usingComparator(comparator).isEqualTo("IFT chimique tot hts _ à l'ancienne;2013;Baulon;3.75");
        assertThat(content).usingComparator(comparator).isEqualTo("IFT h _ à l'ancienne;2013;Baulon;1.05");
        assertThat(content).usingComparator(comparator).isEqualTo("IFT f _ à l'ancienne;2013;Baulon;1.9;");
        assertThat(content).usingComparator(comparator).isEqualTo("IFT i _ à l'ancienne;2013;Baulon;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("IFT a _ à l'ancienne;2013;Baulon;0.8;");
        assertThat(content).usingComparator(comparator).isEqualTo("IFT hh (ts inclus) _ à l'ancienne;2013;Baulon;2.7;");
        assertThat(content).usingComparator(comparator).isEqualTo("IFT biocontrole _ à l'ancienne;2013;Baulon;0.0;");
    }
    
    @Test
    public void testNotUsedCropOnPracticedSeasonalCropCycle() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importCountries();
        testDatas.importRefPrixPhyto();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaGroupeCultures();
        testDatas.importActaDosageSpc();

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefEspece.csv")) {
            importService.importEspeces(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefActaDosageSPC.csv")) {
            importService.importActaDosageSpc(stream);
        }
        // depuis la correction sur ImportServiceImpl pour ne pas mettre à true toute les nouvelles lignes importées mais tenir compte de l'attribut Actif
        final RefActaDosageSPCTopiaDao refActaDosageSPCDao = getPersistenceContext().getRefActaDosageSPCDao();
        RefActaDosageSPC refActaDosageSPC = refActaDosageSPCDao.forProperties(RefActaDosageSPC.PROPERTY_ID_CULTURE, "5091", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, 147, RefActaDosageSPC.PROPERTY_ID_CULTURE, 29, RefActaDosageSPC.PROPERTY_ACTIVE, false).findUniqueOrNull();
        if (refActaDosageSPC != null) {
            refActaDosageSPC.setActive(true);
            refActaDosageSPCDao.update(refActaDosageSPC);
            getPersistenceContext().commit();
        }
        
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefActaTraitementsProduit.csv")) {
            importService.importActaTraitementsProduits(stream);
        }
        
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefActaGroupeCultures.csv")) {
            importService.importActaGroupeCultures(stream);
        }
        
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        final Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);
        
        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();
        
        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        //     *     • Colza     : 1 traitement phytosanitaire avec Colzor Trio, 2 L/ha
        //     *     • Féverole  : 1 traitement phytosanitaire avec Kerb Flo, 1 L/ha
        //     *     • Blé       : 1 traitement phytosanitaire avec Atlantis WG, 500 g/ha
        //     *     • Triticale : 1 traitement phytosanitaire avec Atlantis WG, 250 g/ha
        //     *     • Orge      : 1 traitement phytosanitaire avec Bofix, 3 L/ha
        //     *
        //     *     id_produit	nom_produit	id_traitement	code_traitement	nom_traitement	                                                            NODU 2013	Source
        //     *     5194	        COLZOR TRIO	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     3690	        KERB FLO	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     5167	        ATLANTIS WG	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     5091	        BOFIX	    147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.addColzaPoisFeveroleTriticaleCropsToDomain(domain, testDatas.findCroppingPlanEntries(domain.getTopiaId()));
        
        testDatas.createPracticedSeasonalCropCycleWithNotUseCropInCycle(croppingPlanEntries, ps);
        
        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);
        
        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        
       IndicatorLegacyIFT indicatorLegacyIFT = serviceFactory.newInstance(IndicatorLegacyIFT.class);
        
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorLegacyIFT);
        
        String content = out.toString();
        
        //Calcul de l’indicateur IFT à l’échelle Culture
        //    • Colza : IFT=0.5
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Colza (rang 1);Colza;;Orge;;25.0;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;0.5;;100;;");
        //    • Pois (absent) : IFT=0
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Pois (rang 2);;;Colza;;6.25;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;0.0;;0;;");
        //    • Féverole : IFT=0.5333
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Féverole (rang 2);Fèverole;;Colza;;18.75;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;0.533;;100;;");
        //    • Blé : IFT=1
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);Blé tendre;;Pois;;3.75;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;1.0;;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);Blé tendre;;Féverole;;11.25;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;1.0;;100;;");
        //    • Triticale : IFT=0.5
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Triticale (rang 3);Triticale;;Pois;;2.5;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;0.5;;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Triticale (rang 3);Triticale;;Féverole;;7.5;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;0.5;;100;;");
        //    • Orge: IFT=1
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);Blé tendre, Orge;;Triticale;;10.0;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;1.0;;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);Blé tendre, Orge;;Blé;;15.0;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;1.0;;100;;");
        
        // agrégation à l'échelle du synthétisé donc avec prise en compte des poids
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;NON;Efficience;NON;;2012, 2013;Synthétisé;Indicateurs de pression d’utilisation des intrants : IFT à l’ancienne;IFT chimique total _ à l'ancienne;0.72;");
        
    }
}
