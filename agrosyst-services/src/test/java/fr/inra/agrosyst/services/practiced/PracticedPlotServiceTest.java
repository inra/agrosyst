package fr.inra.agrosyst.services.practiced;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

public class PracticedPlotServiceTest  extends AbstractAgrosystTest {

    protected PracticedPlotService practicedPlotService;

    @BeforeEach
    public void prepareTest() throws IOException {
        testDatas = serviceFactory.newInstance(TestDatas.class);
        loginAsAdmin();
        alterSchema();

        practicedPlotService = serviceFactory.newService(PracticedPlotService.class);

        testDatas.importStadesEDI();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importRefEspeces();
    }

    @Test
    public void testGetPracticedSystemsWithoutPracticedPlot() throws IOException {

        PracticedSystem ps = testDatas.createTestPraticedSystems();

        PracticedPlotTopiaDao practicedPlotDao = getPersistenceContext().getPracticedPlotDao();
        List<PracticedPlot> practicedPlots = practicedPlotDao.forPracticedSystemContains(ps).findAll();
        practicedPlotDao.deleteAll(practicedPlots);

        List<PracticedSystem> practicedSystemList = practicedPlotService.getPracticedSystemsWithoutPracticedPlot(null);

        Assertions.assertEquals(1, practicedSystemList.size());
    }

    @Test
    public void testGetPracticedSystemsWithoutPracticedPlotFailedWithInnnactivePS() throws IOException {

        PracticedSystem ps = testDatas.createTestPraticedSystems();

        PracticedPlotTopiaDao practicedPlotDao = getPersistenceContext().getPracticedPlotDao();
        List<PracticedPlot> practicedPlots = practicedPlotDao.forPracticedSystemContains(ps).findAll();
        practicedPlotDao.deleteAll(practicedPlots);

        ps.setActive(false);
        PracticedSystemTopiaDao practicedSystemDao = getPersistenceContext().getPracticedSystemDao();
        practicedSystemDao.update(ps);

        List<PracticedSystem> practicedSystemList = practicedPlotService.getPracticedSystemsWithoutPracticedPlot(null);

        Assertions.assertEquals(0, practicedSystemList.size());
    }

    @Test
    public void testGetPracticedSystemsWithoutPracticedPlotFailedWithInnnactiveGS() throws IOException {

        PracticedSystem ps = testDatas.createTestPraticedSystems();

        PracticedPlotTopiaDao practicedPlotDao = getPersistenceContext().getPracticedPlotDao();
        List<PracticedPlot> practicedPlots = practicedPlotDao.forPracticedSystemContains(ps).findAll();
        practicedPlotDao.deleteAll(practicedPlots);

        ps.getGrowingSystem().setActive(false);
        GrowingSystemTopiaDao growingSystemTopiaDao = getPersistenceContext().getGrowingSystemDao();
        growingSystemTopiaDao.update(ps.getGrowingSystem());

        List<PracticedSystem> practicedSystemList = practicedPlotService.getPracticedSystemsWithoutPracticedPlot(null);

        Assertions.assertEquals(0, practicedSystemList.size());
    }

    @Test
    public void testGetPracticedSystemsWithoutPracticedPlotFailedWithInnnactiveGP() throws IOException {

        PracticedSystem ps = testDatas.createTestPraticedSystems();

        PracticedPlotTopiaDao practicedPlotDao = getPersistenceContext().getPracticedPlotDao();
        List<PracticedPlot> practicedPlots = practicedPlotDao.forPracticedSystemContains(ps).findAll();
        practicedPlotDao.deleteAll(practicedPlots);

        ps.getGrowingSystem().getGrowingPlan().setActive(false);
        GrowingPlanTopiaDao growingPlanDao = getPersistenceContext().getGrowingPlanDao();
        growingPlanDao.update(ps.getGrowingSystem().getGrowingPlan());

        List<PracticedSystem> practicedSystemList = practicedPlotService.getPracticedSystemsWithoutPracticedPlot(null);

        Assertions.assertEquals(0, practicedSystemList.size());
    }

    @Test
    public void testGetPracticedSystemsWithoutPracticedPlotFailedWithInnnactiveDomain() throws IOException {

        PracticedSystem ps = testDatas.createTestPraticedSystems();

        PracticedPlotTopiaDao practicedPlotDao = getPersistenceContext().getPracticedPlotDao();
        List<PracticedPlot> practicedPlots = practicedPlotDao.forPracticedSystemContains(ps).findAll();
        practicedPlotDao.deleteAll(practicedPlots);

        ps.getGrowingSystem().getGrowingPlan().getDomain().setActive(false);
        DomainTopiaDao domainDao = getPersistenceContext().getDomainDao();
        domainDao.update(ps.getGrowingSystem().getGrowingPlan().getDomain());

        List<PracticedSystem> practicedSystemList = practicedPlotService.getPracticedSystemsWithoutPracticedPlot(null);

        Assertions.assertEquals(0, practicedSystemList.size());
    }
}
