package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPCTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceImpl;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRootTopiaDao;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRootTopiaDao;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.referential.csv.CommuneInseeModel;
import fr.inra.agrosyst.services.referential.csv.CommunePostCodeModel;
import fr.inra.agrosyst.services.referential.csv.RefLocationCommunesFranceImportModel;
import fr.inra.agrosyst.services.referential.csv.RefLocationDto;
import fr.inra.agrosyst.services.referential.json.Dosages;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.topia.persistence.TopiaException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Test du service d'import.
 *
 * @author Eric Chatellier
 */
public class ImportServiceTest extends AbstractAgrosystTest {

    private static final Log LOGGER = LogFactory.getLog(ImportServiceTest.class);

    protected static final Comparator<InseeAndOsmCommunesComparison> COMMUNES_COMPARISON_COMPARATOR = (o1, o2) -> {
        String code1 = MoreObjects.firstNonNull(o1.getInseeCode(), "null");
        String code2 = MoreObjects.firstNonNull(o2.getInseeCode(), "null");
        return code1.compareTo(code2);
    };

    protected ImportService importService;
    protected ReferentialService referentialService;

    @BeforeEach
    public void setupServices() {
        importService = serviceFactory.newService(ImportService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        alterSchema();
    }

    @Test
    public void testImportCommunesFrance() throws TopiaException, IOException {
        ImportResult result = testDatas.importCommunesFrance();
        testImportCommunes(result, 55);
    }

    protected void testImportCommunes(ImportResult result, int nb) throws TopiaException {

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(nb, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());

        RefLocationTopiaDao refLocationTopiaDao = getPersistenceContext().getRefLocationDao();
        Assertions.assertEquals(nb, refLocationTopiaDao.count());
    }

    @Test
    public void compareInseeAndOsmCommunes() throws IOException {

        Map<String, InseeAndOsmCommunesComparison> comparisons = new HashMap<>();

        ImportModel<RefLocationDto> inseeCsvModel = new CommuneInseeModel();
        ImportModel<RefLocationDto> postCodeCsvModel = new CommunePostCodeModel();
        ImportModel<RefLocationDto> osmCsvModel = new RefLocationCommunesFranceImportModel();

        try (
                InputStream communesFranceStream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/communes_france.csv");
                InputStream inseeStream = ImportServiceTest.class.getResourceAsStream("/referentiels/communes_insee.csv")
        ) {

            Map<String, RefLocationDto> postCodes = new HashMap<>();
            // lecture du CSV et création de l'index
            try (Import<RefLocationDto> importer = Import.newImport(postCodeCsvModel,
                    new InputStreamReader(ImportServiceTest.class.getResourceAsStream("/referentiels/communes_codePosteaux.csv"),
                            serviceContext.getConfig().getFileEncoding()))) {
                postCodes.putAll(Maps.uniqueIndex(importer, ImportServiceImpl.GET_REF_LOCATION_DTO_CODE_INSEE::apply));
            } catch (Exception e) {
                Assertions.fail();
            }

            // import data
            try (Import<RefLocationDto> importer0 = Import.newImport(inseeCsvModel, new InputStreamReader(inseeStream, serviceContext.getConfig().getFileEncoding()))) {
                for (RefLocationDto dto : importer0) {

                    // compute the codeInsee
                    String codeInsee = ImportServiceImpl.GET_REF_LOCATION_DTO_CODE_INSEE.apply(dto);
                    InseeAndOsmCommunesComparison comparison = new InseeAndOsmCommunesComparison();
                    comparison.setInseeCode(codeInsee);
                    InseeAndOsmCommunesComparison previous = comparisons.put(codeInsee, comparison);
                    if (previous != null) {
                        LOGGER.warn("2 communes pour le code insee " + codeInsee);
                    }

                    comparison.setInInseeReferential(true);
                    String commune = ImportServiceImpl.GET_REF_LOCATION_DTO_PRETTY_COMMUNE.apply(dto);
                    comparison.setInseeName(commune);

                    String codeInseeForPostCodes = codeInsee.replace("2A", "20").replace("2B", "20");
                    if (postCodes.containsKey(codeInseeForPostCodes)) {
                        RefLocationDto codePostalDto = postCodes.get(codeInseeForPostCodes);
                        comparison.setInseePostCode(Strings.padStart(codePostalDto.getCodePostal(), 5, '0'));
                    }

                }
            } catch (Exception e) {
                Assertions.fail();
            }

            // import data
            try (Import<RefLocationDto> importer1 = Import.newImport(osmCsvModel, new InputStreamReader(communesFranceStream, serviceContext.getConfig().getFileEncoding()))) {
                for (RefLocationDto dto : importer1) {

                    String codeInsee = ImportServiceImpl.GET_REF_LOCATION_DTO_CODE_INSEE.apply(dto);
                    InseeAndOsmCommunesComparison comparison = comparisons.get(codeInsee);
                    if (comparison == null) {
                        comparison = new InseeAndOsmCommunesComparison();
                        comparison.setInseeCode(codeInsee);
                        comparisons.put(codeInsee, comparison);
                    }

                    comparison.setInOsmReferential(true);
                    String codePostal = dto.getCodePostal().split(";")[0];
                    comparison.setOsmPostCode(Strings.padStart(codePostal, 5, '0'));
                    String commune = ImportServiceImpl.GET_REF_LOCATION_DTO_PRETTY_COMMUNE.apply(dto);
                    comparison.setOsmName(commune);

                }
            } catch (Exception e) {
                Assertions.fail();
            }

        } catch (Exception e) {
            Assertions.fail();
            LOGGER.warn("Erreur à l'import ", e);

        }
        // Trying to look for the temporary folder to store data for the test
        File file = File.createTempFile("compareInseeAndOsmCommunes", "log");
        file.deleteOnExit();
        LOGGER.warn("Comparaison des codes postaux OSM/INSEE disponible dans le fichier : " + file.getAbsolutePath());
        try (FileWriter writer = new FileWriter(file)) {

            Collection<InseeAndOsmCommunesComparison> communesComparisons = comparisons.values();

            {
                Collection<InseeAndOsmCommunesComparison> osmOnly =
                        Collections2.filter(communesComparisons, iaocc -> {
                            assert iaocc != null;
                            return iaocc.isInOsmReferential() && !iaocc.isInInseeReferential();
                        });

                List<InseeAndOsmCommunesComparison> sorted = Lists.newArrayList(osmOnly);
                sorted.sort(COMMUNES_COMPARISON_COMPARATOR);
                writer.write(String.format("Codes INSEE seulement dans le référentiel OSM : %d%n", osmOnly.size()));
                for (InseeAndOsmCommunesComparison iaocc : sorted) {
                    writer.write(String.format(" - %s %s%n", iaocc.getInseeCode(), iaocc.getOsmName()));
                }
                writer.write(String.format("%n"));
            }

            {
                Collection<InseeAndOsmCommunesComparison> inseeOnly =
                        Collections2.filter(communesComparisons, iaocc -> {
                            assert iaocc != null;
                            return !iaocc.isInOsmReferential() && iaocc.isInInseeReferential();
                        });
                List<InseeAndOsmCommunesComparison> sorted = Lists.newArrayList(inseeOnly);
                sorted.sort(COMMUNES_COMPARISON_COMPARATOR);
                writer.write(String.format("Codes INSEE seulement dans le référentiel de l'INSEE : %d%n", inseeOnly.size()));
                for (InseeAndOsmCommunesComparison iaocc : sorted) {
                    writer.write(String.format(" - %s %s%n", iaocc.getInseeCode(), iaocc.getInseeName()));
                }
                writer.write(String.format("%n"));
            }

            {
                Collection<InseeAndOsmCommunesComparison> differentPostCodes =
                        Collections2.filter(communesComparisons, iaocc -> {
                            assert iaocc != null;
                            return iaocc.isInOsmReferential() && iaocc.isInInseeReferential()
                                    && ObjectUtils.notEqual(iaocc.getInseePostCode(), iaocc.getOsmPostCode());
                        });
                List<InseeAndOsmCommunesComparison> sorted = Lists.newArrayList(differentPostCodes);
                sorted.sort(COMMUNES_COMPARISON_COMPARATOR);
                writer.write(String.format("Codes postaux différents '<Code INSEE> : <Code postal INSEE>/<Code postal OSM> (<Commune>)' : %d%n", differentPostCodes.size()));
                for (InseeAndOsmCommunesComparison iaocc : sorted) {
                    writer.write(String.format(" - %s : %s/%s (%s)%n", iaocc.getInseeCode(), iaocc.getInseePostCode(), iaocc.getOsmPostCode(), iaocc.getInseeName()));
                }
                writer.write(String.format("%n"));
            }

            {
                Collection<InseeAndOsmCommunesComparison> differentNames =
                        Collections2.filter(communesComparisons, iaocc -> {
                            assert iaocc != null;
                            return iaocc.isInOsmReferential() && iaocc.isInInseeReferential()
                                    && ObjectUtils.notEqual(iaocc.getInseeName(), iaocc.getOsmName());
                        });
                List<InseeAndOsmCommunesComparison> sorted = Lists.newArrayList(differentNames);
                sorted.sort(COMMUNES_COMPARISON_COMPARATOR);
                writer.write(String.format("Noms différents '<Code INSEE> : <Nom INSEE>/<Nom OSM>' : %d%n", differentNames.size()));
                for (InseeAndOsmCommunesComparison iaocc : sorted) {
                    writer.write(String.format(" - %s : %s/%s%n", iaocc.getInseeCode(), iaocc.getInseeName(), iaocc.getOsmName()));
                }
                writer.write(String.format("%n"));
            }
        } catch (IOException e) {
            Assertions.fail();
        }

    }

    /**
     * Test de recuperer la liste des regions de l'ensemble des types de sols arvalis.
     */
    @Test
    public void testImportSolArvalis() throws IOException {

        ImportResult result = testDatas.importSolArvalis();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(51, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());


        Map<Integer, String> solArvalisRegions = referentialService.getSolArvalisRegions();
        Assertions.assertEquals(3, solArvalisRegions.size());
        Assertions.assertEquals("Alsace", solArvalisRegions.get(42));
        Assertions.assertEquals("Aquitaine", solArvalisRegions.get(72));
        Assertions.assertEquals("Midi-Pyrénées", solArvalisRegions.get(73));

        List<RefSolArvalis> refSolArvalis = referentialService.getSolArvalis(72);
        Assertions.assertEquals(28, refSolArvalis.size());
    }

    @Test
    public void testImportLegalStatus() throws IOException {

        ImportResult result = testDatas.importLegalStatus();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(9, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportEspeces() throws IOException {


        try (InputStream stream = ImportServiceTest.class.getResourceAsStream("/referentiels/refEspece.csv")) {
            ImportResult result = importService.importEspeces(stream);
            Assertions.assertFalse(result.hasErrors());
            Assertions.assertEquals(812, result.getCreated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(0, result.getUpdated());
            Assertions.assertEquals(0, result.getDeleted());
        }
    }

    @Test
    public void testImportVarieteGeves() throws IOException {

        try (InputStream stream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/varietes_GEVES.csv")) {
            ImportResult result = importService.importVarietesGeves(stream);
            Assertions.assertFalse(result.hasErrors());
            Assertions.assertEquals(52, result.getCreated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(0, result.getUpdated());
            Assertions.assertEquals(0, result.getDeleted());
        }
    }

    @Test
    public void testImportVarietePlantGrape() throws IOException {
        ImportResult result = testDatas.importVarietePlantGrape();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(52, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportClonesPlantGrape() throws IOException {
        ImportResult result = testDatas.importClonesPlantGrape();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportEspecesToVarietes() throws IOException {
        ImportResult result = testDatas.importEspecesToVarietes();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportOTEX() throws IOException {
        ImportResult result = testDatas.importOTEX();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    /**
     * Test le nombre d'elements du referentiel orientation edi importés.
     */
    @Test
    public void testImportOrientationEdi() throws IOException {
        ImportResult result = testDatas.importOrientationEdi();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(8, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    /**
     * Test du résultat de l'import du référentiel parcelle zonage edi.
     */
    @Test
    public void testImportParcelleZonageEdi() throws IOException {
        ImportResult result = testDatas.importParcelleZonageEdi();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(6, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    /**
     * Test du résultat de l'import du référentiel sol profondeur indigo.
     */
    @Test
    public void testImportSolProfondeurIndigo() throws IOException {
        ImportResult result = testDatas.importSolProfondeurIndigo();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(4, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    /**
     * Test du résultat de l'import du référentiel sol texture geppa.
     */
    @Test
    public void testImportSolTextureGeppa() throws IOException {
        ImportResult result = testDatas.importSolTextureGeppa();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(17, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    /**
     * Test du résultat de l'import du référentiel sol caracteristique indigo.
     */
    @Test
    public void testImportSolCaracteristiquesIndigo() throws IOException {
        ImportResult result = testDatas.importSolCaracteristiquesIndigo();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(35, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportUnitesEdi() throws IOException {
        ImportResult result = testDatas.importUnitesEdi();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(14, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportFertiMinUnifa() throws IOException {
        try (InputStream stream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/ferti_min_UNIFA.csv")) {
            ImportResult result = importService.importFertiMinUNIFA(stream);
            Assertions.assertFalse(result.hasErrors());
            Assertions.assertEquals(35, result.getCreated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(15, result.getUpdated());
            Assertions.assertEquals(0, result.getDeleted());
        }
    }

    @Test
    public void testImportFertiMinUnifaWithNull() throws IOException {
        // CATEG;Type_produit;CODEPROD     ;FORME;  N ;P2O5 ;K2O ;Bore ;Calcium ; Fer ;Manganèse ;Molybdène ;MgO ;Oxyde de Sodium ;SO3 ;Cuivre ;Zinc ;source
        // 108  ;SULFONITRATE;0001080012100;Granulé;21;     ;    ;     ;11      ;     ;          ;          ;0   ;                ;16  ;       ;     ;UNIFA

        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA fertiMin = refFertiMinUNIFATopiaDao.create(
                RefFertiMinUNIFA.PROPERTY_CATEG, 108,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "SULFONITRATE",
                RefFertiMinUNIFA.PROPERTY_CODEPROD, "0001080012100",
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_N, 21d,
                RefFertiMinUNIFA.PROPERTY_CALCIUM, 11d,
                RefFertiMinUNIFA.PROPERTY_MG_O, 0d,
                RefFertiMinUNIFA.PROPERTY_S_O3, 16d,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );
//        HarvestingPriceTopiaDao priceDao = getPersistenceContext().getHarvestingPriceDao();
//
//        Price p = priceDao.create(
//                Price.PROPERTY_DISPLAY_NAME, "test",
//                Price.PROPERTY_CATEGORY, PriceCategory.MINERAL_INPUT_CATEGORIE,
//                Price.PROPERTY_TYPE, PriceType.INPUT,
//                Price.PROPERTY_OBJECT_ID, StringUtils.stripAccents(Referentials.GET_FERTI_MIN_UNIFA_NATURAL_ID_WITH_NULL.apply(fertiMin)),
//                Price.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
//                Price.PROPERTY_SOURCE_UNIT,"test"
//        );
        getCurrentTransaction().commit();


        try (InputStream stream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/ferti_min_UNIFA.csv")) {
            ImportResult result = importService.importFertiMinUNIFA(stream);
            Assertions.assertFalse(result.hasErrors());
            Assertions.assertEquals(34, result.getCreated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(16, result.getUpdated());
            Assertions.assertEquals(0, result.getDeleted());

//            p = priceDao.forTopiaIdEquals(p.getTopiaId()).findUnique();
//            Assertions.assertEquals(StringUtils.stripAccents(Referentials.GET_FERTI_MIN_UNIFA_NATURAL_ID_WITH_NULL.apply(fertiMin)), p.getObjectId());
        }
    }

    @Test
    public void testImportFertiMinUnifaWithoutNull() throws IOException {
        // CATEG;Type_produit;CODEPROD     ;FORME;  N ;P2O5 ;K2O ;Bore ;Calcium ; Fer ;Manganèse ;Molybdène ;MgO ;Oxyde de Sodium ;SO3 ;Cuivre ;Zinc ;source
        // 108  ;SULFONITRATE;0001080012100;Granulé;21;     ;    ;     ;11      ;     ;          ;          ;0   ;                ;16  ;       ;     ;UNIFA

        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA fertiMin = refFertiMinUNIFATopiaDao.create(
                RefFertiMinUNIFA.PROPERTY_CATEG, 108,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "SULFONITRATE",
                RefFertiMinUNIFA.PROPERTY_CODEPROD, "0001080012100",
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_N, 21d,
                RefFertiMinUNIFA.PROPERTY_P2_O5, 0d,
                RefFertiMinUNIFA.PROPERTY_K2_O, 0d,
                RefFertiMinUNIFA.PROPERTY_BORE, 0d,
                RefFertiMinUNIFA.PROPERTY_CALCIUM, 11d,
                RefFertiMinUNIFA.PROPERTY_FER, 0d,
                RefFertiMinUNIFA.PROPERTY_MANGANESE, 0d,
                RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0d,
                RefFertiMinUNIFA.PROPERTY_MG_O, 0d,
                RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0d,
                RefFertiMinUNIFA.PROPERTY_S_O3, 16d,
                RefFertiMinUNIFA.PROPERTY_CUIVRE, 0d,
                RefFertiMinUNIFA.PROPERTY_ZINC, 0d,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );
//        HarvestingPriceTopiaDao priceDao = getPersistenceContext().getHarvestingPriceDao();
//
//        Price p = priceDao.create(
//                Price.PROPERTY_DISPLAY_NAME, "test",
//                Price.PROPERTY_CATEGORY, PriceCategory.MINERAL_INPUT_CATEGORIE,
//                Price.PROPERTY_TYPE, PriceType.INPUT,
//                Price.PROPERTY_OBJECT_ID, InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(fertiMin),
//                Price.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
//                Price.PROPERTY_SOURCE_UNIT,"test"
//        );
        getCurrentTransaction().commit();


        try (InputStream stream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/ferti_min_UNIFA.csv")) {
            ImportResult result = importService.importFertiMinUNIFA(stream);
            Assertions.assertFalse(result.hasErrors());
            Assertions.assertEquals(34, result.getCreated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(16, result.getUpdated());
            Assertions.assertEquals(0, result.getDeleted());

//            p = priceDao.forTopiaIdEquals(p.getTopiaId()).findUnique();
//            Assertions.assertEquals(InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(fertiMin), p.getObjectId());
        }
    }

    /**
     * Test du résultat de l'import du référentiel Adventices.
     */
    @Test
    public void testImportAdventices() throws IOException {
        ImportResult result = testDatas.importAdventices();
        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(49, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    /**
     * Test du résultat de l'import du référentiel nuisibles edi.
     */
    @Test
    public void testImportNuisiblesEDI() throws IOException {

        try (InputStream stream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/nuisibles_edi.csv")) {
            ImportResult result = importService.importNuisiblesEDI(stream);

            Assertions.assertFalse(result.hasErrors());
            Assertions.assertEquals(50, result.getCreated());
            Assertions.assertEquals(0, result.getUpdated());
            Assertions.assertEquals(0, result.getDeleted());
        }
    }

    /**
     * Test du résultat de l'import du référentiel Adventices.
     */
    @Test
    public void testImportFertiTypesEffluents() throws IOException {
        ImportResult result = testDatas.importFertiOrga();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(46, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    /**
     * Test du résultat de l'import du référentiel StadeEDI.
     */
    @Test
    public void testImportStadesEDI() throws IOException {

        try (InputStream stream = Test.class.getResourceAsStream("/referentiels/test/stades_EDI.csv")) {
            ImportResult result = importService.importStadesEdiCSV(stream);
            Assertions.assertFalse(result.hasErrors());
            Assertions.assertEquals(57, result.getCreated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(0, result.getUpdated());
            Assertions.assertEquals(0, result.getDeleted());
        }
    }

    /**
     * Test du résultat de l'import du référentiel ActionAgrosystTravailEdi
     */
    @Test
    public void testImportActionAgrosystTravailEdi() throws IOException {
        ImportResult result = testDatas.importInterventionAgrosystTravailEdi();
        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(78, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportStationMeteo() throws IOException {
        ImportResult result = testDatas.importStationMeteo();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(47, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportGesCarburants() throws IOException {
        ImportResult result = testDatas.importGesCarburants();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(1, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportGesEngrais() throws IOException {
        ImportResult result = testDatas.importGesEngrais();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(14, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportGesPhyto() throws IOException {
        ImportResult result = testDatas.importGesPhyto();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(5, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportGesSemences() throws IOException {
        ImportResult result = testDatas.importGesSemences();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(11, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportNrjCarburants() throws IOException {
        ImportResult result = testDatas.importNrjCarburants();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(1, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportNrjEngrais() throws IOException {
        ImportResult result = testDatas.importNrjEngrais();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(14, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportNrjPhyto() throws IOException {
        ImportResult result = testDatas.importNrjPhyto();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(5, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportNrjSemences() throws IOException {
        ImportResult result = testDatas.importNrjSemences();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(11, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportNjrGesOutils() throws IOException {
        ImportResult result = testDatas.importNrjGesOutils();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(5, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportMesure() throws IOException {
        ImportResult result = testDatas.importMesure();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(49, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportStadeNuisibleEDI() throws IOException {
        ImportResult result = testDatas.importStadeNuisibleEDI();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportTypeNotationEDI() throws IOException {
        ImportResult result = testDatas.importTypeNotationEDI();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(8, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportValeurQualitativeEDI() throws IOException {
        ImportResult result = testDatas.importValeurQualitativeEDI();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportUnitesQualifiantEDI() throws IOException {
        ImportResult result = testDatas.importUnitesQualifiantEDI();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportSupportOrganeEdi() throws IOException {
        ImportResult result = testDatas.importSupportOrganeEdi();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportActaTraitementsProduits() throws IOException {
        testDatas.getDefaultFranceCountry();
        ImportResult result = testDatas.importActaTraitementsProduits();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(128, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportActaSubstanceActive() throws IOException {
        ImportResult result = testDatas.importActaSubstanceActive();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(51, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportProtocoleVgObs() throws IOException {
        ImportResult result = testDatas.importProtocoleVgObs();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportElementVoisinage() throws IOException {
        ImportResult result = testDatas.importElementVoisinage();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(19, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportPhytoSubstanceActiveIphy() throws IOException {
        ImportResult result = testDatas.importPhytoSubstanceActiveIphy();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(63, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportTypeAgriculture() throws IOException {
        ImportResult result = testDatas.importTypeAgriculture();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(7, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportActaDosageSpc() throws IOException {
        testDatas.importActaGroupeCultures();
        ImportResult result = testDatas.importActaDosageSpc();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(220, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportActaGroupeCultures() throws IOException {
        ImportResult result = testDatas.importActaGroupeCultures();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportSaActaIphy() throws IOException {
        ImportResult result = testDatas.importSaActaIphy();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(54, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportCultureEdiGroupeCouvSol() throws IOException {
        ImportResult result = testDatas.importCultureEdiGroupeCouvSol();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(64, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportCouvSolAnnuelle() throws IOException {
        ImportResult result = testDatas.importCouvSolAnnuelle();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(1404, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportCouvSolPerenne() throws IOException {
        ImportResult result = testDatas.importCouvSolPerenne();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(432, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportTraitSdC() throws IOException {
        ImportResult result = testDatas.importTraitSdC();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(13, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportZoneClimatiqueIphy() throws IOException {
        ImportResult result = testDatas.importZoneClimatiqueIphy();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(96, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Disabled("Ignoré pour ne pas saisir le mot de passe dans le test")
    @Test
    public void testImportFromAPIWithAuth() throws IOException {
        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        ImportServiceImplForTest importService = new ImportServiceImplForTest(
                refLocationDao,
                refSolArvalisDao,
                refFertiMinUNIFATopiaDao,
                priceTopiaDao,
                cacheService,
                serviceContext);

        CloseableHttpClient httpclient = HttpClientBuilder.create().build();
        HttpHost targetHost = new HttpHost("api-agro.opendatasoft.com", 443, "https");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(targetHost.getHostName(), targetHost.getPort()),
                new UsernamePasswordCredentials("estelle.ancelet", "XXXX"));


        //Create AuthCache instance
        AuthCache authCache = new BasicAuthCache();
        // Generate BASIC scheme object and add it to the local auth cache
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);

        // Add AuthCache to the execution context
        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credsProvider);
        context.setAuthCache(authCache);

        HttpGet httpget = new HttpGet("/service/test-connection/dosagesSA");
        CloseableHttpResponse response = httpclient.execute(
                targetHost, httpget, context);

        String jsonResult = importService.transformHttpResponseToJson(response);

        Dosages results = importService.mapDosageResult(jsonResult);

        Assertions.assertTrue(results != null && CollectionUtils.isNotEmpty(results.getDosagesSA()));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(results.getDosagesSA().size() + " DosageSA\n");
        }
    }

    @Disabled("Ignoré en cas de changement d'url")
    @Test
    public void testLoadSolDataFromAPI() throws IOException, URISyntaxException {
        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        CloseableHttpClient httpclient = HttpClientBuilder.create().build();

        // exemples d'urls
        // https://api-agro.opendatasoft.com/service/test-connection/dosagesSA
        // https://api-agro.opendatasoft.com/api/records/1.0/search?dataset=base-sol
        URI uri = new URIBuilder()
                .setScheme("https")
                .setHost("api-agro.opendatasoft.com")
                .setPort(443)
                .setPath("/api/records/1.0/search")
                .addParameter("dataset", "base-sol")
                .addParameter("apikey", "")
                .addParameter("rows", "-1")
                .build();

        // test de la validité de l'url
        URL url = uri.toURL();
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();
        Assertions.assertEquals(HttpURLConnection.HTTP_OK, connection.getResponseCode());

        HttpGet httpget = new HttpGet(uri);

        CloseableHttpResponse response = httpclient.execute(httpget);

        String jsonResult = importService.transformHttpResponseToJson(response);

        ImportServiceImpl.BaseSol results = mapSolResult(jsonResult);

        Assertions.assertTrue(results != null && CollectionUtils.isNotEmpty(results.getRecords()));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(results.getRecords().size() + " Sol\n");
        }
    }

    @Disabled("Ignoré l'API n'est plus accéssible")
    @Test
    public void testImportSolFromAPIWithDuplicatedKey() throws IOException {
        testDatas.importSolArvalis();

        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        String jsonResult = "[" +
                "  {" +
                "    \"datasetid\": \"base-sol\", " +
                "    \"recordid\": \"473ab1af66e57646a0c43d1689e53ff65af7c05f\", " +
                "    \"fields\": {" +
                "                  \"id_type_sol\": \"AL5036000\", " +
                "                  \"region\": \"Alsace\", " +
                "                  \"calcaire\": \"Calcaire\", " +
                "                  \"nom_arvalis\": \"Sol limono-argileux à argilo-limoneux calcaire profond sur alluvions\", " +
                "                  \"pierrosite\": \"Non caillouteux\", " +
                "                  \"texture\": \"Limon argileux\", " +
                "                  \"profondeur\": \"Profond\", " +
                "                  \"nom_referenciel_pedologique_francais\": \"Calcosol fluvique\", " +
                "                  \"hydromorphie\": \"Non hydromorphe\", " +
                "                  \"nom_sol\": \"Sol profond de Hardt (ou Ried brun profond)\"" +
                "                }," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", " +
                "    \"recordid\": \"76e5fabb9c516dfe59b46aee11171391219b5c22\", " +
                "    \"fields\": {" +
                "                  \"id_type_sol\": \"AL0016502\", \"region\": \"Alsace\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol argileux humifère hydromorphe profond sur argiles alluviales\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Argile\", \"profondeur\": \"Profond\", \"nom_referenciel_pedologique_francais\": \"Réductisol-histosol saprique\", \"hydromorphie\": \"Hydromorphe\", \"nom_sol\": \"Ried noir\"" +
                "               }," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", " +
                "    \"recordid\": \"2e9008ec2933e5795c9483c3eeee9ce3953e7534\", " +
                "    \"fields\": {" +
                "                  \"id_type_sol\": \"AL0113001\", " +
                "                  \"region\": \"Alsace\", " +
                "                  \"calcaire\": \"Non calcaire\", " +
                "                  \"nom_arvalis\": \"Sol argileux calcaire peu caillouteux sur marne\", " +
                "                  \"pierrosite\": \"Peu caillouteux\", " +
                "                  \"texture\": \"Argile\", \"profondeur\": \"Moyen\", " +
                "                  \"nom_referenciel_pedologique_francais\": \"Calcosol\", " +
                "                  \"hydromorphie\": \"Non hydromorphe\", " +
                "                  \"nom_sol\": \"Argilo-calcaire moyen\"}," +
                "    \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {\"datasetid\": \"base-sol\", \"recordid\": \"03670de512fd87b712ddeff84b30b0637900cf22\", \"fields\": {\"id_type_sol\": \"AL0066000\", \"region\": \"Alsace\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limoneux profond sur loess\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Limon\", \"profondeur\": \"Profond\", \"nom_referenciel_pedologique_francais\": \"Luvisol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Limons lehm\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", \"recordid\": \"a2e58f4986c2e7e660b9dab6a679255c40c1ee6f\", \"fields\": {\"id_type_sol\": \"LO5531003\", \"region\": \"Lorraine\", \"calcaire\": \"Calcaire\", \"nom_arvalis\": \"Sol limono-argileux à argilo-limoneux calcaire caillouteux superficiel sur calcaire dur non fissuré\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Superficiel\", \"nom_referenciel_pedologique_francais\": \"Rendosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Argilo-calcaire très superficiel\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", \"recordid\": \"00f48a08d19cb218914467031ed538aaf6edd0c4\", \"fields\": {\"id_type_sol\": \"HN0016501\", \"region\": \"Haute-Normandie\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol argileux profond hydromorphe sur argiles alluviales\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Argile\", \"profondeur\": \"Profond\", \"nom_referenciel_pedologique_francais\": \"Redoxisol fluvique\", \"hydromorphie\": \"Hydromorphe\", \"nom_sol\": \"Alluvions argileuses hydromorphes\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", \"recordid\": \"8f41d41b3105cba14338a0c37ab1a144d130a5a8\", \"fields\": {\"id_type_sol\": \"IF0533001\", \"region\": \"Ile-de-France\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limono-argileux caillouteux sur argile à silex\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Brunisol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Limon argileux caillouteux sur argile à silex\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", \"recordid\": \"8b828982f14535b35c102dee068d0580e0e374ce\", \"fields\": {\"id_type_sol\": \"HN0563500\", \"region\": \"Haute-Normandie\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limoneux caillouteux hydromorphe sur argile à silex\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Brunisol\", \"hydromorphie\": \"Hydromorphe\", \"nom_sol\": \"Limon caillouteux superficiel hydromorphe/Argile à silex\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", \"recordid\": \"c9186278807031b17f99ad8cbba15f6b04740c3a\", \"fields\": {\"id_type_sol\": \"IF0563001\", \"region\": \"Ile-de-France\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limoneux caillouteux sur argile à silex\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Planosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Limon caillouteux superficiel/Argile à silex\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", \"recordid\": \"97fba6cfcdcd03de03d27b17412dc72b1d3bf4b7\", \"fields\": {\"id_type_sol\": \"IF5033004\", \"region\": \"Ile-de-France\", \"calcaire\": \"Calcaire\", \"nom_arvalis\": \"Sol limono-argileux à argilo-limoneux calcaire sur calcaire sableux\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Calcosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Argilo-calcaire moyen\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                "  {" +
                "    \"datasetid\": \"base-sol\", \"recordid\": \"97fba6cfcdcd03de03d27b17412dc72b1d3bf4b7\", \"fields\": {\"id_type_sol\": \"IF5033004\", \"region\": \"Ile-de-France\", \"calcaire\": \"Calcaire\", \"nom_arvalis\": \"Sol limono-argileux à argilo-limoneux calcaire sur calcaire sableux\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Calcosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Argilo-calcaire moyen\"}," +
                "               \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}]";


        try (InputStream is = importService.produceCsvImportResult(jsonResult, RefSolArvalis.class);
             InputStream regionsStream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/sols_regions.csv")) {

            ImportResult result = importService.importSolArvalisCSV(is, regionsStream);

            Assertions.assertEquals(7, result.getCreated());
            Assertions.assertEquals(3, result.getUpdated());
            Assertions.assertEquals(1, result.getIgnored());
        }
    }

    @Disabled("Ignoré l'API n'est plus accéssible")
    @Test
    public void testImportSolFromAPIKey() throws IOException {
        testDatas.importSolArvalis();

        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        String jsonResult = "[" +
                "{\"datasetid\": \"base-sol\", \"recordid\": \"473ab1af66e57646a0c43d1689e53ff65af7c05f\", \"fields\": " +
                "{\"id_type_sol\": \"AL5036000\", \"region\": \"Alsace\", \"calcaire\": \"Calcaire\", \"nom_arvalis\": \"Sol limono-argileux à argilo-limoneux calcaire profond sur alluvions\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Profond\", \"nom_referenciel_pedologique_francais\": \"Calcosol fluvique\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Sol profond de Hardt (ou Ried brun profond)\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"76e5fabb9c516dfe59b46aee11171391219b5c22\", \"fields\": {\"id_type_sol\": \"AL0016502\", \"region\": \"Alsace\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol argileux humifère hydromorphe profond sur argiles alluviales\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Argile\", \"profondeur\": \"Profond\", \"nom_referenciel_pedologique_francais\": \"Réductisol-histosol saprique\", \"hydromorphie\": \"Hydromorphe\", \"nom_sol\": \"Ried noir\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}, " +
                "{\"datasetid\": \"base-sol\", \"recordid\": \"2e9008ec2933e5795c9483c3eeee9ce3953e7534\", \"fields\": {\"id_type_sol\": \"AL0113001\", \"region\": \"Alsace\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol argileux calcaire peu caillouteux sur marne\", \"pierrosite\": \"Peu caillouteux\", \"texture\": \"Argile\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Calcosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Argilo-calcaire moyen\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"03670de512fd87b712ddeff84b30b0637900cf22\", \"fields\": {\"id_type_sol\": \"AL0066000\", \"region\": \"Alsace\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limoneux profond sur loess\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Limon\", \"profondeur\": \"Profond\", \"nom_referenciel_pedologique_francais\": \"Luvisol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Limons lehm\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"a2e58f4986c2e7e660b9dab6a679255c40c1ee6f\", \"fields\": {\"id_type_sol\": \"LO5531003\", \"region\": \"Lorraine\", \"calcaire\": \"Calcaire\", \"nom_arvalis\": \"Sol limono-argileux à argilo-limoneux calcaire caillouteux superficiel sur calcaire dur non fissuré\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Superficiel\", \"nom_referenciel_pedologique_francais\": \"Rendosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Argilo-calcaire très superficiel\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"00f48a08d19cb218914467031ed538aaf6edd0c4\", \"fields\": {\"id_type_sol\": \"HN0016501\", \"region\": \"Haute-Normandie\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol argileux profond hydromorphe sur argiles alluviales\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Argile\", \"profondeur\": \"Profond\", \"nom_referenciel_pedologique_francais\": \"Redoxisol fluvique\", \"hydromorphie\": \"Hydromorphe\", \"nom_sol\": \"Alluvions argileuses hydromorphes\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"8f41d41b3105cba14338a0c37ab1a144d130a5a8\", \"fields\": {\"id_type_sol\": \"IF0533001\", \"region\": \"Ile-de-France\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limono-argileux caillouteux sur argile à silex\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Brunisol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Limon argileux caillouteux sur argile à silex\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"8b828982f14535b35c102dee068d0580e0e374ce\", \"fields\": {\"id_type_sol\": \"HN0563500\", \"region\": \"Haute-Normandie\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limoneux caillouteux hydromorphe sur argile à silex\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Brunisol\", \"hydromorphie\": \"Hydromorphe\", \"nom_sol\": \"Limon caillouteux superficiel hydromorphe/Argile à silex\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"c9186278807031b17f99ad8cbba15f6b04740c3a\", \"fields\": {\"id_type_sol\": \"IF0563001\", \"region\": \"Ile-de-France\", \"calcaire\": \"Non calcaire\", \"nom_arvalis\": \"Sol limoneux caillouteux sur argile à silex\", \"pierrosite\": \"Caillouteux\", \"texture\": \"Limon\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Planosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Limon caillouteux superficiel/Argile à silex\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}," +
                " {\"datasetid\": \"base-sol\", \"recordid\": \"97fba6cfcdcd03de03d27b17412dc72b1d3bf4b7\", \"fields\": {\"id_type_sol\": \"IF5033004\", \"region\": \"Ile-de-France\", \"calcaire\": \"Calcaire\", \"nom_arvalis\": \"Sol limono-argileux à argilo-limoneux calcaire sur calcaire sableux\", \"pierrosite\": \"Non caillouteux\", \"texture\": \"Limon argileux\", \"profondeur\": \"Moyen\", \"nom_referenciel_pedologique_francais\": \"Calcosol\", \"hydromorphie\": \"Non hydromorphe\", \"nom_sol\": \"Argilo-calcaire moyen\"}," +
                " \"record_timestamp\": \"2015-07-15T14:09:40+00:00\"}]";


        try (InputStream refSolArvalisStream = importService.produceCsvImportResult(jsonResult, RefSolArvalis.class);
             InputStream regionsStream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/sols_regions.csv")) {
            ImportResult result = importService.importSolArvalisCSV(refSolArvalisStream, regionsStream);

            Assertions.assertEquals(7, result.getCreated());
            Assertions.assertEquals(3, result.getUpdated());
        }
    }

    protected ImportServiceImpl.BaseSol mapSolResult(String jsonResult) {
        Gson gson = new Gson();
        return gson.fromJson(jsonResult, ImportServiceImpl.BaseSol.class);
    }

    @Disabled("Ignoré l'API n'est plus accéssible")
    @Test
    public void testImportDosageSa() throws IOException {
        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        RefActaDosageSaRootTopiaDao refActaDosageSaRootDao = getPersistenceContext().getRefActaDosageSaRootDao();

        // Entity to removed
        refActaDosageSaRootDao.create(
                RefActaDosageSaRoot.PROPERTY_ID_PRODUIT, "0",
                RefActaDosageSaRoot.PROPERTY_NOM_PRODUIT, "Entity to remove",
                RefActaDosageSaRoot.PROPERTY_ID_TRAITEMENT, 117,
                RefActaDosageSaRoot.PROPERTY_CODE_TRAITEMENT, "D1",
                RefActaDosageSaRoot.PROPERTY_ID_CULTURE, 215,
                RefActaDosageSaRoot.PROPERTY_NOM_CULTURE, "carotte",
                RefActaDosageSaRoot.PROPERTY_REMARQUE_CULTURE, "aucune",
                RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_VALEUR, "4000",
                RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_UNITE, "g/ha",
                RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_COMMENTAIRE, "",
                RefActaDosageSaRoot.PROPERTY_ACTIVE, true
        );

        // Existing Entity
        refActaDosageSaRootDao.create(
                RefActaDosageSaRoot.PROPERTY_ID_PRODUIT, "2674",
                RefActaDosageSaRoot.PROPERTY_NOM_PRODUIT, "FLOCTER",
                RefActaDosageSaRoot.PROPERTY_ID_TRAITEMENT, 117,
                RefActaDosageSaRoot.PROPERTY_CODE_TRAITEMENT, "D1",
                RefActaDosageSaRoot.PROPERTY_ID_CULTURE, 215,
                RefActaDosageSaRoot.PROPERTY_NOM_CULTURE, "carotte",
                RefActaDosageSaRoot.PROPERTY_REMARQUE_CULTURE, "",
                RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_VALEUR, "4000",
                RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_UNITE, "g/ha",
                RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_COMMENTAIRE, "de s.a.",
                RefActaDosageSaRoot.PROPERTY_ACTIVE, true
        );

        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        String json = """
                {
                  "dosagesSA": [
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 215,
                      "nom_culture": "carotte",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 223,
                      "nom_culture": "céleri-rave",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 231,
                      "nom_culture": "scorsonère",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 235,
                      "nom_culture": "panais",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 236,
                      "nom_culture": "topinambour",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 246,
                      "nom_culture": "aubergine",
                      "remarque_culture": null,
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 247,
                      "nom_culture": "tomate",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 250,
                      "nom_culture": "poivron",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 253,
                      "nom_culture": "courgette",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 255,
                      "nom_culture": "concombre",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 260,
                      "nom_culture": "melon",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    },
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "idTraitement": 117,
                      "codeTraitement": "D1",
                      "id_culture": 262,
                      "nom_culture": "pastèque",
                      "remarque_culture": "",
                      "dosage_sa_valeur": "4000",
                      "dosage_sa_unite": "g/ha",
                      "dosage_sa_commentaire": "de s.a."
                    }
                  ]
                }""";

        // procus csv for analyse

        try (InputStream is = importService.produceCsvImportResult(json, RefActaDosageSaRoot.class)) {
            long nbActiveEntitiesBeforeImport = refActaDosageSaRootDao.forProperties(RefActaDosageSaRoot.PROPERTY_ACTIVE, true).count();
            // re-inject it to save in DB
            ImportResult result = importService.importRefActaDosageSaRoot(is);

            Assertions.assertEquals(11, result.getCreated());
            Assertions.assertEquals(11 + nbActiveEntitiesBeforeImport, result.getCreated() + nbActiveEntitiesBeforeImport);
            Assertions.assertEquals(2, result.getUpdated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(0, result.getUnactivated());
            Assertions.assertFalse(result.hasErrors());
        }
    }

    @Disabled("Ignoré l'API n'est plus accéssible")
    @Test
    public void testImportActaProduitRoot() throws IOException {
        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        RefActaProduitRootTopiaDao refActaProduitRootDao = getPersistenceContext().getRefActaProduitRootDao();

        // Entity to removed
        refActaProduitRootDao.create(
                RefActaProduitRoot.PROPERTY_ID_PRODUIT, "0",
                RefActaProduitRoot.PROPERTY_NOM_PRODUIT, "Entity to remove",
                RefActaProduitRoot.PROPERTY_AMM, "AMM",
                RefActaProduitRoot.PROPERTY_ACTIVE, true
        );

        // Existing Entity
        refActaProduitRootDao.create(
                RefActaProduitRoot.PROPERTY_ID_PRODUIT, "2674",
                RefActaProduitRoot.PROPERTY_NOM_PRODUIT, "FLOCTER_2",
                RefActaProduitRoot.PROPERTY_AMM, "2120069",
                RefActaProduitRoot.PROPERTY_ACTIVE, true
        );

        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        String json = """
                {
                  "produits": [
                    {
                      "idProduit": 2674,
                      "nomProduit": "FLOCTER",
                      "amm": "2120069"
                    },
                    {
                      "idProduit": 2675,
                      "nomProduit": "BASAMID GRANULE",
                      "amm": "6800403"
                    },
                    {
                      "idProduit": 2676,
                      "nomProduit": "NEMAZATE",
                      "amm": "2110185"
                    },
                    {
                      "idProduit": 2677,
                      "nomProduit": "NEMATHORIN",
                      "amm": "2010426"
                    },
                    {
                      "idProduit": 2678,
                      "nomProduit": "NEMATHORIN 10 G",
                      "amm": "9600199"
                    },
                    {
                      "idProduit": 2679,
                      "nomProduit": "FUMICAL",
                      "amm": "8400242"
                    },
                    {
                      "idProduit": 2681,
                      "nomProduit": "ESACO",
                      "amm": "9300306"
                    }\
                ]}""";

        // procus csv for analyse
        try (InputStream is = importService.produceCsvImportResult(json, RefActaProduitRoot.class)) {
            long nbActiveEntitiesBeforeImport = refActaProduitRootDao.forProperties(RefActaProduitRoot.PROPERTY_ACTIVE, true).count();
            // re-inject it to save in DB
            ImportResult result = importService.importRefActaProduitRoot(is);

            Assertions.assertEquals(6, result.getCreated());
            Assertions.assertEquals(6 + nbActiveEntitiesBeforeImport, result.getCreated() + nbActiveEntitiesBeforeImport);
            Assertions.assertEquals(2, result.getUpdated());
            Assertions.assertEquals(0, result.getIgnored());
            Assertions.assertEquals(0, result.getUnactivated());
        }
    }

    @Disabled("Ignoré l'API n'est plus accéssible")
    @Test
    public void testImportActaSubstanceActives() throws IOException {
        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        RefActaSubstanceActiveTopiaDao refActaSubstanceActiveDao = getPersistenceContext().getRefActaSubstanceActiveDao();

        testDatas.importActaSubstanceActive();
        long nbSubstanceActivesBeforeImport = refActaSubstanceActiveDao.count();


        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        String json = """
                {
                  "substancesActives": [
                    {
                      "idProduit": 3612,
                      "nomProduit": "ROYALTAC",
                      "nom_commun_SA": "metsulfuron methyle",
                      "concentration_valeur": "679",
                      "concentration_unite": "g/L",
                      "code_AMM": "2080125"
                    },
                    {
                      "idProduit": 4233,
                      "nomProduit": "GINKO",
                      "nom_commun_SA": "1-dodecanol",
                      "concentration_valeur": "132",
                      "concentration_unite": "mg/dif.",
                      "code_AMM": "2080125"
                    },
                    {
                      "idProduit": 5343,
                      "nomProduit": "GINKO DUO",
                      "nom_commun_SA": "1-dodecanol",
                      "concentration_valeur": "9,4",
                      "concentration_unite": "%",
                      "code_AMM": "2110179"
                    },
                    {
                      "idProduit": 4234,
                      "nomProduit": "ISOMATE-C",
                      "nom_commun_SA": "1-dodecanol",
                      "concentration_valeur": "63",
                      "concentration_unite": "mg/dif.",
                      "code_AMM": "9900123"
                    },
                    {
                      "idProduit": 4930,
                      "nomProduit": "AGRI STERYL",
                      "nom_commun_SA": "1-iso-thiazolinon chloromethyl",
                      "concentration_valeur": "5",
                      "concentration_unite": "%",
                      "code_AMM": "8900676"
                    },
                    {
                      "idProduit": 2762,
                      "nomProduit": "SMARTFRESH",
                      "nom_commun_SA": "1-methylcyclopropene",
                      "concentration_valeur": "3,3",
                      "concentration_unite": "%",
                      "code_AMM": "2050073"
                    },
                    {
                      "idProduit": 2763,
                      "nomProduit": "SMARTFRESH SMARTTABS",
                      "nom_commun_SA": "1-methylcyclopropene",
                      "concentration_valeur": "0,63",
                      "concentration_unite": "%",
                      "code_AMM": "2090131"
                    },
                    {
                      "idProduit": 5343,
                      "nomProduit": "GINKO DUO",
                      "nom_commun_SA": "1-tetradecanol",
                      "concentration_valeur": "1,7",
                      "concentration_unite": "%",
                      "code_AMM": "2110179"
                    }]}""";

        // procus csv for analyse
        try (InputStream is = importService.produceCsvImportResult(json, RefActaSubstanceActive.class)) {
            // re-inject it to save in DB
            ImportResult result = importService.importActaSubstanceActive(is);
            long nbSubstanceActivesAfterImport = refActaSubstanceActiveDao.count();
            long nbActiveSubstanceActivesAfterImport = refActaSubstanceActiveDao.forProperties(RefActaProduitRoot.PROPERTY_ACTIVE, true).count();

            Assertions.assertEquals(7, result.getCreated());
            Assertions.assertEquals(nbSubstanceActivesBeforeImport + result.getCreated(), nbSubstanceActivesAfterImport);
            Assertions.assertEquals(58, nbActiveSubstanceActivesAfterImport);
            Assertions.assertEquals(51, result.getUpdated());
        }
    }

    @Disabled("Ignoré l'API n'est plus accéssible")
    @Test
    public void testImportActaTraitementsProduit() throws IOException {
        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();

        testDatas.importActaTraitementsProduits();
        long nbTaiementsProduitBeforeImport = refActaTraitementsProduitDao.count();


        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        String json = """
                {
                  "traitements": [
                    {
                      "idProduit": 3648,
                      "nomProduit": "A mêtre à jour",
                      "idTraitement": 147,
                      "codeTraitement": "A1",
                      "code_AMM": "2080088",
                      "nomTraitement": "A mêtre à jour"
                    },
                    {
                      "idProduit": 4564,
                      "nomProduit": "ROSECLEAR ULTRA",
                      "idTraitement": 114,
                      "codeTraitement": "A1",
                      "code_AMM": "2100165",
                      "nomTraitement": "Associations - Traitement des parties aériennes"
                    },
                    {
                      "idProduit": 6796,
                      "nomProduit": "GAUCHO DUO FS",
                      "idTraitement": 115,
                      "codeTraitement": "A2",
                      "code_AMM": "2140039",
                      "nomTraitement": "Associations - Traitement des semences"
                    },
                    {
                      "idProduit": 6617,
                      "nomProduit": "AUSTRAL PLUS NET",
                      "idTraitement": 115,
                      "codeTraitement": "A2",
                      "code_AMM": "2140078",
                      "nomTraitement": "Associations - Traitement des semences"
                    },
                    {
                      "idProduit": 7725,
                      "nomProduit": "FERIAL DUO FS",
                      "idTraitement": 115,
                      "codeTraitement": "A2",
                      "code_AMM": "2140039",
                      "nomTraitement": "Associations - Traitement des semences"
                    }\
                   ]\
                }""";

        // procus csv for analyse
        try (InputStream is = importService.produceCsvImportResult(json, RefActaTraitementsProduit.class)) {
            // re-inject it to save in DB
            ImportResult result = importService.importActaTraitementsProduits(is);
            long nbTraitementsProduitAfterImport = refActaTraitementsProduitDao.count();

            Assertions.assertEquals(4, result.getCreated());
            Assertions.assertEquals(nbTaiementsProduitBeforeImport + result.getCreated(), nbTraitementsProduitAfterImport);
            Assertions.assertEquals(64, result.getUpdated());
        }
    }

    @Disabled("Ignoré l'API n'est plus accéssible")
    @Test
    public void testImportDosagesSPC() throws IOException {
        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        RefSolArvalisTopiaDao refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        HarvestingPriceTopiaDao priceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        CacheService cacheService = serviceFactory.newInstance(CacheService.class);

        ImportServiceImplForTest importService = new ImportServiceImplForTest(refLocationDao, refSolArvalisDao, refFertiMinUNIFATopiaDao, priceTopiaDao, cacheService, serviceContext);

        String json = """
                { \s
                   "dosagesSPC":[ \s
                      { \s
                         "idProduit":4565,
                         "nomProduit":"ROSECLEAR ULTRA GUN !",
                         "idTraitement":114,
                         "codeTraitement":"A1",
                         "id_culture":142,
                         "nom_culture":"rosier",
                         "remarque_culture":"",
                         "dosage_spc_valeur":"1",
                         "dosage_spc_unite":"L\\/10 m\\u00b2",
                         "dosage_spc_commentaire":""
                      },
                      { \s
                         "idProduit":4564,
                         "nomProduit":"ROSECLEAR ULTRA",
                         "idTraitement":114,
                         "codeTraitement":"A1",
                         "id_culture":142,
                         "nom_culture":"rosier",
                         "remarque_culture":"",
                         "dosage_spc_valeur":"20",
                         "dosage_spc_unite":"ml\\/L",
                         "dosage_spc_commentaire":""
                      },
                      { \s
                         "idProduit":4565,
                         "nomProduit":"ROSECLEAR ULTRA GUN !",
                         "idTraitement":114,
                         "codeTraitement":"A1",
                         "id_culture":142,
                         "nom_culture":"rosier",
                         "remarque_culture":"",
                         "dosage_spc_valeur":"1",
                         "dosage_spc_unite":"L\\/10 m\\u00b2",
                         "dosage_spc_commentaire":""
                      }]
                }""";

        // procus csv for analyse
        try (InputStream is = importService.produceCsvImportResult(json, RefActaDosageSpcRoot.class)) {
            // re-inject it to save in DB
            ImportResult result = importService.importRefActaDosageSpcRoot(is);

            Assertions.assertEquals(2, result.getCreated());
        }
    }

    @Test
    public void testInconsistentDosageUnitOnActDosageSacImport() throws IOException {

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/ACTA_GroupeCultures_errone.csv")) {
            importService.importActaGroupeCultures(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/ACTA_dosage_SPC_complet_errone.csv")) {

            ImportResult result = importService.importActaDosageSpc(stream);
            Assertions.assertTrue(result.hasErrors());
        }
    }

    @Disabled("Unactivated because it is now allow to record inconsistent Acta dosage SpC")
    @Test
    public void testPersistedInconsistentDosageUnitOnActDosageSpcImport() throws IOException {
        RefActaDosageSPCTopiaDao refActaDosageSPCDao = getPersistenceContext().getRefActaDosageSPCDao();
        refActaDosageSPCDao.create(
                RefActaDosageSPC.PROPERTY_ID_PRODUIT, "0",
                RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, 0,
                RefActaDosageSPC.PROPERTY_ID_CULTURE, 0,
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR, 0.0,
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_COMMENTAIRE, "duplicated Unit",
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE, PhytoProductUnit.L_HA);

        refActaDosageSPCDao.create(
                RefActaDosageSPC.PROPERTY_ID_PRODUIT, "0",
                RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, 0,
                RefActaDosageSPC.PROPERTY_ID_CULTURE, 0,
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR, 0.0,
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_COMMENTAIRE, "duplicated Unit 2",
                RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE, PhytoProductUnit.KG_HA);
        long nbBefore = refActaDosageSPCDao.count();

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/ACTA_GroupeCultures.csv")) {
            importService.importActaGroupeCultures(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/ACTA_dosage_SPC_complet.csv")) {
            ImportResult result = importService.importActaDosageSpc(stream);
            Assertions.assertTrue(result.hasErrors());
        }
        long nbAfter = refActaDosageSPCDao.count();
        Assertions.assertEquals(nbBefore, nbAfter);
    }


    @Test
    public void testImportDestination() throws IOException {
        testDatas.importRefEspeces();
        ImportResult result = testDatas.importDestination();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(117, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportQualityCriteria() throws IOException {
        testDatas.importRefEspeces();
        ImportResult result = testDatas.importQualityCriteria();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(36, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefSpeciesToSector() throws IOException {
        testDatas.importRefEspeces();
        ImportResult result = testDatas.importRefSpeciesToSector();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(549, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        RefSpeciesToSectorTopiaDao dao = getPersistenceContext().getRefSpeciesToSectorDao();
        Assertions.assertTrue(dao.forCode_qualifiant_AEEEquals(null).count() > 0);
    }

    @Test
    public void testImportRefHarvestingPrices() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importDestination();
        ImportResult result = testDatas.importRefHarvestingPrices();

        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(423, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefHarvestingPricesFailure() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importDestination();
        ImportResult result = testDatas.importRefHarvestingPricesAvecErreur();

        Assertions.assertTrue(result.hasErrors());

        RefHarvestingPrice duplicatedRefHarvestingPrice = new RefHarvestingPriceImpl();
        duplicatedRefHarvestingPrice.setCode_scenario("AAB");
        duplicatedRefHarvestingPrice.setCode_destination_A("D0009");
        duplicatedRefHarvestingPrice.setDestination("Circuit long / Frais (dupliqué)");
        duplicatedRefHarvestingPrice.setCode_espece_botanique("ZBK");
        duplicatedRefHarvestingPrice.setCode_qualifiant_AEE("H29");
        duplicatedRefHarvestingPrice.setProduitRecolte("Echalote");
        duplicatedRefHarvestingPrice.setOrganic(false);
        duplicatedRefHarvestingPrice.setMarketingPeriod(Calendar.JANUARY);
        duplicatedRefHarvestingPrice.setMarketingPeriodDecade(3);
        duplicatedRefHarvestingPrice.setPriceUnit(PriceUnit.EURO_KG);
        duplicatedRefHarvestingPrice.setPrice(10d);
        duplicatedRefHarvestingPrice.setCampaign(2013);
        duplicatedRefHarvestingPrice.setScenario("");

        // duplicated on campaign
        Assertions.assertEquals(String.format("Valeur en double trouvée (type=%s, L%d): %s", RefHarvestingPrice.class.getSimpleName(), 12, Referentials.GET_REF_HARVESTING_PRICE_CAMPAIGN_NATURAL_ID.apply(duplicatedRefHarvestingPrice)), result.getErrors().get(0));

        // doplicated on scenario
        Assertions.assertEquals(String.format("Valeur en double trouvée (type=%s, L%d): %s", RefHarvestingPrice.class.getSimpleName(), 14, Referentials.GET_REF_HARVESTING_PRICE_SCENARIO_NATURAL_ID.apply(duplicatedRefHarvestingPrice)), result.getErrors().get(1));
        Assertions.assertEquals(0, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportValidRefHarvestingPrice() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importDestination();

        ImportResult result = testDatas.importValidRefHarvestingPrices();
        Assertions.assertEquals(0, result.getErrors().size());
        Assertions.assertEquals(12, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefHarvestingPriceWhitUpdate() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importDestination();

        RefHarvestingPrice duplicatedRefHarvestingPriceForCampaign = new RefHarvestingPriceImpl();
        duplicatedRefHarvestingPriceForCampaign.setCode_scenario("AAA");
        duplicatedRefHarvestingPriceForCampaign.setCode_destination_A("D0009");
        duplicatedRefHarvestingPriceForCampaign.setDestination("Circuit long / Frais (dupliqué)");
        duplicatedRefHarvestingPriceForCampaign.setCode_espece_botanique("ZBK");
        duplicatedRefHarvestingPriceForCampaign.setCode_qualifiant_AEE("H29");
        duplicatedRefHarvestingPriceForCampaign.setProduitRecolte("Echalote");
        duplicatedRefHarvestingPriceForCampaign.setOrganic(false);
        duplicatedRefHarvestingPriceForCampaign.setMarketingPeriod(Calendar.JANUARY);
        duplicatedRefHarvestingPriceForCampaign.setMarketingPeriodDecade(3);
        duplicatedRefHarvestingPriceForCampaign.setPriceUnit(PriceUnit.EURO_KG);
        duplicatedRefHarvestingPriceForCampaign.setPrice(3d);
        duplicatedRefHarvestingPriceForCampaign.setCampaign(2014);

        getPersistenceContext().getRefHarvestingPriceDao().create(duplicatedRefHarvestingPriceForCampaign);

        RefHarvestingPrice duplicatedRefHarvestingPriceForScenario = new RefHarvestingPriceImpl();
        duplicatedRefHarvestingPriceForScenario.setCode_scenario("AAB");
        duplicatedRefHarvestingPriceForScenario.setCode_destination_A("D0009");
        duplicatedRefHarvestingPriceForScenario.setDestination("Circuit long / Frais (dupliqué)");
        duplicatedRefHarvestingPriceForScenario.setCode_espece_botanique("ZBK");
        duplicatedRefHarvestingPriceForScenario.setCode_qualifiant_AEE("H29");
        duplicatedRefHarvestingPriceForScenario.setProduitRecolte("Echalote");
        duplicatedRefHarvestingPriceForScenario.setOrganic(false);
        duplicatedRefHarvestingPriceForScenario.setMarketingPeriod(Calendar.JANUARY);
        duplicatedRefHarvestingPriceForScenario.setMarketingPeriodDecade(3);
        duplicatedRefHarvestingPriceForScenario.setPriceUnit(PriceUnit.EURO_KG);
        duplicatedRefHarvestingPriceForScenario.setPrice(3d);
        duplicatedRefHarvestingPriceForScenario.setCampaign(0);

        getPersistenceContext().getRefHarvestingPriceDao().create(duplicatedRefHarvestingPriceForScenario);

        ImportResult result = testDatas.importValidRefHarvestingPrices();

        Assertions.assertEquals(0, result.getErrors().size());
        Assertions.assertEquals(10, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(2, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefScenarioPriceWithDuplicateKey() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importDestination();
        ImportResult result = testDatas.importRefScenarioPricesWithDuplicatedKey();

        RefHarvestingPrice duplicatedRefHarvestingPrice = new RefHarvestingPriceImpl();
        duplicatedRefHarvestingPrice.setCode_scenario("CCC");
        duplicatedRefHarvestingPrice.setCode_destination_A("D0009");
        duplicatedRefHarvestingPrice.setDestination("Circuit long / Frais (dupliqué)");
        duplicatedRefHarvestingPrice.setCode_espece_botanique("ZBK");
        duplicatedRefHarvestingPrice.setCode_qualifiant_AEE("H29");
        duplicatedRefHarvestingPrice.setProduitRecolte("Echalote");
        duplicatedRefHarvestingPrice.setOrganic(false);
        duplicatedRefHarvestingPrice.setMarketingPeriod(Calendar.JANUARY);
        duplicatedRefHarvestingPrice.setMarketingPeriodDecade(3);
        duplicatedRefHarvestingPrice.setPriceUnit(PriceUnit.EURO_KG);
        duplicatedRefHarvestingPrice.setPrice(10d);
        duplicatedRefHarvestingPrice.setCampaign(2013);
        duplicatedRefHarvestingPrice.setScenario("");

        String errorMessage = String.format("Valeur en double trouvée (type=%s, L%d): %s", RefHarvestingPrice.class.getSimpleName(), 12, Referentials.GET_REF_HARVESTING_PRICE_CAMPAIGN_NATURAL_ID.apply(duplicatedRefHarvestingPrice) + ";" + Referentials.GET_REF_HARVESTING_PRICE_SCENARIO_NATURAL_ID.apply(duplicatedRefHarvestingPrice));

        Assertions.assertTrue(result.hasErrors());
        Assertions.assertEquals(1, result.getErrors().size());
        Assertions.assertEquals(errorMessage, result.getErrors().get(0));
        Assertions.assertEquals(0, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefMarketingDestination() throws IOException {
        ImportResult result = testDatas.importRefMarketingDestination();

        Assertions.assertEquals(25, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefWorkItemInputEdi() throws IOException {
        ImportResult result = testDatas.importRefInterventionTypeItemInputEdi();

        Assertions.assertEquals(33, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Disabled // temporairement désactivé le temps que l'on revienne sur le patch appliqué sur le #11027
    public void testImportRefPrixPhytoDuplicatedKey() throws IOException {

        testDatas.importActaTraitementsProduits();

        ImportResult result = testDatas.importRefPrixPhytoAndDuplicatedKeysOnCamapaignsCSV();

        Assertions.assertTrue(result.hasErrors());
        long duplicatedCount = result.getErrors().stream().filter(s -> s.contains("Valeur en double trouvée")).count();
        Assertions.assertEquals(1, duplicatedCount);

        result = testDatas.importRefPrixPhytoAndDuplicatedKeysOnScenariosCSV();

        Assertions.assertTrue(result.hasErrors());
        duplicatedCount = result.getErrors().stream().filter(s -> s.contains("Valeur en double trouvée")).count();
        Assertions.assertEquals(1, duplicatedCount);
    }

    @Test
    public void testRefInputUnitPriceUnitConverter() throws IOException {

        ImportResult result = testDatas.importRefInputUnitPriceUnitConverterCSV();

        Assertions.assertFalse(result.hasErrors());

        Assertions.assertEquals(45, result.getCreated());

        result = testDatas.importRefInputUnitPriceUnitConverterCSV();

        Assertions.assertFalse(result.hasErrors());

        RefInputUnitPriceUnitConverterTopiaDao refInputUnitPriceUnitConverterDao = getPersistenceContext().getRefInputUnitPriceUnitConverterDao();

        Assertions.assertEquals(45, result.getUpdated());
        Assertions.assertEquals(0, refInputUnitPriceUnitConverterDao.forConvertionRateEquals(0d).count());

    }

    @Test
    public void testRefInputUnitPriceUnitConverterWithError() throws IOException {

        ImportResult result = testDatas.importRefInputUnitPriceUnitConverterCsvWithError();

        Assertions.assertTrue(result.hasErrors());

        Assertions.assertEquals("Valeur en double trouvée (type=RefInputUnitPriceUnitConverter, L4): phytoProductUnit=null capacityUnit=null mineralProductUnit=L_HA organicProductUnit=null seedPlantUnit=null potInputUnit=null substrateInputUnit=null priceUnit=EURO_HA", result.getErrors().get(0));

    }

    @Test
    public void testRefInputUnitPriceUnitConverterWithDefaultValues() throws IOException {

        ImportResult result = testDatas.importRefInputUnitPriceUnitConverterV0();

        Assertions.assertFalse(result.hasErrors());

        Assertions.assertEquals(8, result.getCreated());

        Assertions.assertFalse(result.hasErrors());

        RefInputUnitPriceUnitConverterTopiaDao refInputUnitPriceUnitConverterDao = getPersistenceContext().getRefInputUnitPriceUnitConverterDao();

        Assertions.assertEquals(0, refInputUnitPriceUnitConverterDao.forConvertionRateEquals(0d).count());

    }

    @Test
    public void testImportAGS_Amortissement() throws IOException {
        ImportResult result = testDatas.importAGS_Amortissement();
        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(38, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefPrixEspeceWithCampaignsAndScenarios() throws IOException {

        //String codeEspeceBotanique, String codeQualifiantAEE, String libelleEspeceBotanique, String codeDestinationAee
        testDatas.createRefEspece(getPersistenceContext().getRefEspeceDao(), "ZAP", "ZAN", "Julius (Betterave Sucrière)", "");
        testDatas.createRefEspece(getPersistenceContext().getRefEspeceDao(), "ZDH", "ZMV", "Chill (Orge de Printemps)", "");
        testDatas.createRefEspece(getPersistenceContext().getRefEspeceDao(), "ZDU", "ZLT", "Prairie (  Cultures fourragères/Prairie/ Pelouse/ Gazon temporaire)", "");
        testDatas.createRefEspece(getPersistenceContext().getRefEspeceDao(), "ZDU", "ZLT", "Prairie (  Cultures fourragères/Prairie/ Pelouse/ Gazon temporaire)", "");


        // import du référentiel avec uniquement des campagnes
        ImportResult result = testDatas.importRefPrixEspeceCampaign();
        Assertions.assertEquals(12, result.getCreated());
        Assertions.assertFalse(result.hasErrors());

        RefPrixEspeceTopiaDao rpEspeceDao = getPersistenceContext().getRefPrixEspeceDao();
        Assertions.assertEquals(12, rpEspeceDao.count());

        List<RefPrixEspece> zduZltRpes = rpEspeceDao.forCode_espece_botaniqueEquals("ZDU").addEquals(RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, "ZLT").setOrderByArguments(RefPrixEspece.PROPERTY_CAMPAIGN).findAll();
        RefPrixEspece zduZltRpes2016 = zduZltRpes.getFirst();

        Assertions.assertEquals(SeedType.SEMENCES_DE_FERME, zduZltRpes2016.getSeedType());

        Double expectedPrice = 150d;
        Assertions.assertEquals(expectedPrice, zduZltRpes2016.getPrice());

        Assertions.assertEquals("", zduZltRpes2016.getCode_scenario());
        Assertions.assertEquals("", zduZltRpes2016.getScenario());

        Integer expectedCampaign = 2016;
        Assertions.assertEquals(expectedCampaign, zduZltRpes2016.getCampaign());

        Assertions.assertTrue(zduZltRpes2016.isTreatment());

        Assertions.assertTrue(zduZltRpes2016.isActive());

        RefPrixEspece zduZltRpes2017 = zduZltRpes.get(1);
        Assertions.assertFalse(zduZltRpes2017.isActive());

        // import du référentiel avec uniquement des scénarios
        result = testDatas.importRefPrixEspeceScenario();

        Assertions.assertEquals(18, result.getCreated());
        Assertions.assertFalse(result.hasErrors());

        zduZltRpes = rpEspeceDao.forCode_espece_botaniqueEquals("ZDU")
                .addEquals(RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, "ZLT")
                .addEquals(RefPrixEspece.PROPERTY_CODE_SCENARIO, "Code_Scenario_Agrosyst_1")
                .setOrderByArguments(RefPrixEspece.PROPERTY_CAMPAIGN).findAll();

        Assertions.assertEquals(1, zduZltRpes.size());

        RefPrixEspece zduZltRpesC2016 = zduZltRpes.getFirst();

        Assertions.assertEquals(SeedType.SEMENCES_DE_FERME, zduZltRpesC2016.getSeedType());

        Assertions.assertEquals(expectedPrice, zduZltRpesC2016.getPrice());

        Assertions.assertEquals("Code_Scenario_Agrosyst_1", zduZltRpesC2016.getCode_scenario());
        Assertions.assertEquals("Scénario 2016", zduZltRpesC2016.getScenario());

        Assertions.assertFalse(zduZltRpesC2016.isTreatment());

        Assertions.assertTrue(zduZltRpesC2016.isActive());

        // import du référentiel avec campagnes et scénarios
        result = testDatas.importRefPrixEspeceWithScenariosAndCampaigns();

        Assertions.assertEquals(18, result.getUpdated());

        Assertions.assertEquals(12 + 18, rpEspeceDao.count());

        Assertions.assertTrue(zduZltRpesC2016.isActive());

        zduZltRpes = rpEspeceDao.forCode_espece_botaniqueEquals("ZDU")
                .addEquals(RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, "ZLT")
                .addEquals(RefPrixEspece.PROPERTY_CODE_SCENARIO, "Code_Scenario_Agrosyst_1")
                .setOrderByArguments(RefPrixEspece.PROPERTY_CAMPAIGN).findAll();

        Assertions.assertEquals(1, zduZltRpes.size());

        zduZltRpesC2016 = zduZltRpes.getFirst();

        // on valide que le prix a bien été mis à jour
        expectedPrice = 15d;
        Assertions.assertEquals(expectedPrice, zduZltRpesC2016.getPrice());
    }

    @Test
    public void testImportRefAnimalType() throws IOException {
        ImportResult result = testDatas.importAnimalType();
        Assertions.assertFalse(result.hasErrors());
        Assertions.assertEquals(12, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefActaProduitRootCsv() throws IOException {
        ImportResult result;
        testDatas.getDefaultFranceCountry();
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refactaproduitroot.csv")) {
            result = importService.importRefActaProduitRoot(stream);
        }
        Assertions.assertEquals(2526, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        Assertions.assertEquals(0, result.getIgnored());
    }

    @Test
    public void testImportInt() {
        String value = "2013.0";
        value = value.split("\\D+")[0];
        int objectValue = Integer.parseInt(value);
        Assertions.assertEquals(2013, objectValue);
        value = "2013,0";
        value = value.split("\\D+")[0];
        objectValue = Integer.parseInt(value);
        Assertions.assertEquals(2013, objectValue);
    }

    @Test
    public void testHandleMAAResponseForImport() throws IOException {
        testDatas.getDefaultFranceCountry();
        ImportResult refActaTraitementsProduitImportResult = new ImportResult();
        ImportResult refMAADosesRefParGroupeCibleImportResult = new ImportResult();
        ImportResult refMAADBioControleImportResult = new ImportResult();
        InputStream jsonStream = ImportServiceTest.class.getResourceAsStream("/otherImports/maa_response.json");
        String json = IOUtils.toString(jsonStream, Charset.defaultCharset());

        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importAllActaTraitementsProduits();
        testDatas.importRefActaProduitRoot();

        Map<String, Integer> idTraitementsByCode = getPersistenceContext().getRefActaTraitementsProduitsCategDao()
                .findAll()
                .stream()
                .collect(Collectors.toMap(RefActaTraitementsProduitsCateg::getCode_traitement,
                        RefActaTraitementsProduitsCateg::getId_traitement));

        Collection<RefGroupeCibleTraitement> refMAACodeTraitementToHistoricCodeTraitements = new ArrayList<>();

        Collection<RefActaProduitRoot> refActaProduitRoots = testDatas.getAllRefActaProduitRoot();
        RefCountry france = testDatas.getDefaultFranceCountry();

        importService.handleMAAResponseForImport(
                json,
                refActaTraitementsProduitImportResult,
                refMAADosesRefParGroupeCibleImportResult,
                refMAADBioControleImportResult,
                2019,
                idTraitementsByCode,
                refMAACodeTraitementToHistoricCodeTraitements,
                refActaProduitRoots,
                france);

        Assertions.assertEquals(9, refActaTraitementsProduitImportResult.getCreated());
        Assertions.assertEquals(0, refActaTraitementsProduitImportResult.getUpdated());
        Assertions.assertEquals(0, refActaTraitementsProduitImportResult.getDeleted());
        Assertions.assertEquals(0, refActaTraitementsProduitImportResult.getIgnored());

        Assertions.assertEquals(200, refMAADosesRefParGroupeCibleImportResult.getCreated());
        Assertions.assertEquals(0, refMAADosesRefParGroupeCibleImportResult.getUpdated());
        Assertions.assertEquals(0, refMAADosesRefParGroupeCibleImportResult.getDeleted());
        Assertions.assertEquals(0, refMAADosesRefParGroupeCibleImportResult.getIgnored());

        Assertions.assertEquals(8, refMAADBioControleImportResult.getCreated());
        Assertions.assertEquals(0, refMAADBioControleImportResult.getUpdated());
        Assertions.assertEquals(0, refMAADBioControleImportResult.getDeleted());
        Assertions.assertEquals(0, refMAADBioControleImportResult.getIgnored());

        refActaTraitementsProduitImportResult = new ImportResult();
        refMAADosesRefParGroupeCibleImportResult = new ImportResult();
        refMAADBioControleImportResult = new ImportResult();

        importService.handleMAAResponseForImport(
                json,
                refActaTraitementsProduitImportResult,
                refMAADosesRefParGroupeCibleImportResult,
                refMAADBioControleImportResult,
                2019,
                idTraitementsByCode,
                refMAACodeTraitementToHistoricCodeTraitements,
                refActaProduitRoots,
                france);

        Assertions.assertEquals(0, refActaTraitementsProduitImportResult.getCreated());
        Assertions.assertEquals(9, refActaTraitementsProduitImportResult.getUpdated());
        Assertions.assertEquals(0, refActaTraitementsProduitImportResult.getDeleted());
        Assertions.assertEquals(0, refActaTraitementsProduitImportResult.getIgnored());

        Assertions.assertEquals(0, refMAADosesRefParGroupeCibleImportResult.getCreated());
        Assertions.assertEquals(200, refMAADosesRefParGroupeCibleImportResult.getUpdated());
        Assertions.assertEquals(0, refMAADosesRefParGroupeCibleImportResult.getDeleted());
        Assertions.assertEquals(0, refMAADosesRefParGroupeCibleImportResult.getIgnored());

        Assertions.assertEquals(0, refMAADBioControleImportResult.getCreated());
        Assertions.assertEquals(8, refMAADBioControleImportResult.getUpdated());
        Assertions.assertEquals(0, refMAADBioControleImportResult.getDeleted());
        Assertions.assertEquals(0, refMAADBioControleImportResult.getIgnored());
    }

    @Test
    public void testImportRefCiblesAgrosystGroupesCiblesMAACsv() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/maa/refCiblesAgrosystGroupesCiblesMAA.csv")) {
            result = importService.importRefCiblesAgrosystGroupesCiblesMAA(stream);
        }
        Assertions.assertEquals(50, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        Assertions.assertEquals(0, result.getIgnored());
    }

    @Test
    public void testImportRefCiblesAgrosystGroupesCiblesMAACsvWithMissingCibleEdiId() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/maa/refCiblesAgrosystGroupesCiblesMAA_missingCibleEdiId.csv")) {
            LOGGER.warn("Expected import error 'cause: Unable to parse value '' (column 'cible_edi_ref_id', line 43)' ;)");
            result = importService.importRefCiblesAgrosystGroupesCiblesMAA(stream);
        }
        Assertions.assertEquals(0, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(1, result.getErrors().size());

    }

    @Test
    public void testImportRefCiblesAgrosystGroupesCiblesMAACsvWithMissingCodeGroupeCible() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/maa/refCiblesAgrosystGroupesCiblesMAA_missingCodeGroupeCible.csv")) {
            result = importService.importRefCiblesAgrosystGroupesCiblesMAA(stream);
        }
        Assertions.assertEquals(0, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(1, result.getErrors().size());
        Assertions.assertEquals("""
                Import Failed on line 80, cause: Unable to parse value '' (column 'code_groupe_cible_maa', line 79)
                La valeur est obligatoire
                 Import annulé""", result.getErrors().get(0));

    }

    @Test
    public void testImportRefFeedbackRouter() throws IOException {
        String[] email0s = {"marie.coquet@acta.asso.fr", "mathilde.lefevre@adage35.org", "david.bouille@bretagne.chambagri.fr", "maylis.carre@civam.org",
                "philippe.tresch@idele.fr", "francois.cena@smb.chambagri.fr", "thomas.achkar@apca.chamagri.fr", "maxime.lienard@apca.chambagri.fr", "Ardavan.SOLEYMANI@astredhor.fr", "Maxime.PAOLUCCI@astredhor.fr", "laurent.deliere@inrae.fr", "cathy.eckert@ctifl.fr", "aurelie.lequeux-sauvage@apca.chambagri.fr", "baptiste.labeyrie@ctifl.fr", "jl.sagnes@agri82.fr"};
        for (String email0 : email0s) {
            testDatas.createUser(email0);
        }

        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refFeedbackRouter.csv")) {
            result = importService.importRefFeedbackRouter(stream);
        }
        Assertions.assertEquals(168, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(1, result.getErrors().size());
    }

    @Test
    public void testImportRefMAABiocontroleCsv() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/maa/refMAABiocontrole.csv")) {
            result = importService.importRefMAABiocontrole(stream);
        }
        Assertions.assertEquals(12, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        Assertions.assertEquals(0, result.getIgnored());
    }

    @Test
    public void testImportRefMAADosesRefParGroupeCibleCsv() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/maa/refMAADosesRefParGroupeCible.csv")) {
            result = importService.importRefMAADosesRefParGroupeCible(stream);
        }
        Assertions.assertEquals(96, result.getCreated());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
        Assertions.assertEquals(0, result.getIgnored());
    }

    @Test
    public void testImportRefInputUnitPriceUnitConverter() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importDestination();

        ImportResult result = testDatas.importValidRefHarvestingPrices();
        Assertions.assertEquals(0, result.getErrors().size());
        Assertions.assertEquals(12, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefPot() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refPot.csv")) {
            result = importService.importRefPot(stream);
        }
        Assertions.assertEquals(0, result.getErrors().size());
        Assertions.assertEquals(8, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefOtherInput() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refOtherInput.csv")) {
            result = importService.importRefOtherInputCSV(stream);
        }
        Assertions.assertEquals(0, result.getErrors().size());
        Assertions.assertEquals(72, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefActaSubstanceActive() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refActaSubstanceActive.csv")) {
            result = importService.importActaSubstanceActive(stream);
        }
        Assertions.assertEquals(1, result.getErrors().size());
        Assertions.assertEquals(1, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }

    @Test
    public void testImportRefSubstrate() throws IOException {
        ImportResult result;
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refSubstrate.csv")) {
            result = importService.importRefSubstrate(stream);
        }
        Assertions.assertEquals(0, result.getErrors().size());
        Assertions.assertEquals(29, result.getCreated());
        Assertions.assertEquals(0, result.getIgnored());
        Assertions.assertEquals(0, result.getUpdated());
        Assertions.assertEquals(0, result.getDeleted());
    }


}
