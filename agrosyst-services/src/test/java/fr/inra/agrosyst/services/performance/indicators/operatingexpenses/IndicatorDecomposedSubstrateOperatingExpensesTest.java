package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.UnitType;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrateTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSubstrateTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import static fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses.SUBSTRAT;
import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorDecomposedSubstrateOperatingExpensesTest extends AbstractIndicatorDecomposedOperatingExpensesTest {
    private DomainSubstrateInputTopiaDao substrateInputDao;
    private SubstrateInputUsageTopiaDao substrateInputUsageDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        super.setupServices();
        substrateInputDao = getPersistenceContext().getDomainSubstrateInputDao();
        substrateInputUsageDao = getPersistenceContext().getSubstrateInputUsageDao();
    }

    private void createRefPrixSubstrate(RefPrixSubstrateTopiaDao refPrixSubstratesDao, String caracteristic1, String caracteristic2, double v, PriceUnit euroM3, int i) {
        refPrixSubstratesDao.create(
                RefPrixSubstrate.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefPrixSubstrate.PROPERTY_CARACTERISTIC2, caracteristic2,
                RefInputPrice.PROPERTY_PRICE, v,
                RefInputPrice.PROPERTY_UNIT, euroM3,
                RefInputPrice.PROPERTY_CAMPAIGN, i,
                RefInputPrice.PROPERTY_CODE_SCENARIO, null,
                RefInputPrice.PROPERTY_SCENARIO, null,
                RefInputPrice.PROPERTY_ACTIVE, true);
    }

    protected Domain createEffectiveTestData() throws IOException {
        RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao = getPersistenceContext().getRefSolProfondeurIndigoDao();
        RefSolProfondeurIndigo solProfond = refSolProfondeurIndigoDao.forClasse_de_profondeur_INDIGOEquals(PerformanceServiceImpl.DEFAULT_DEEPEST_INDIGO).findUnique();

        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        DomainTopiaDao domainDao = getPersistenceContext().getDomainDao();
        RefLegalStatusTopiaDao refLegalStatusDao = getPersistenceContext().getRefLegalStatusDao();

        RefLocation location = testDatas.createRefLocation(refLocationDao);
        Domain d0 = testDatas.createSimpleDomain(domainDao, "fr.inra.agrosyst.api.entities.Domain_7fd194f6-7440-432f-a37b-fbf1d27fa95e", location, testDatas.createRefLegalStatus(refLegalStatusDao), "Baulon", 2017, "Annie Verssaire", "Lorem ipsum dolor sit amet", 100.0, 9.0, 1.0, true);
        GrowingPlan gp = testDatas.createGrowingPlan(d0);
        GrowingSystem gs = testDatas.createGrowingSystem(gp);
        String plotName = "Plot Baulon 2";
        String plotEdaplosId = "edaplosIssuerIdBaulon2";

        RefSolTextureGeppaTopiaDao refSolTextureGeppaDao = getPersistenceContext().getRefSolTextureGeppaDao();
        RefSolTextureGeppa solArgiloSaleuse = refSolTextureGeppaDao.forNaturalId("AS").findUnique();
        RefSolTextureGeppa solSableArgiloLimoneux = refSolTextureGeppaDao.forNaturalId("Sal").findUnique();
        Plot plot = testDatas.createPlot(solProfond, solArgiloSaleuse, solSableArgiloLimoneux, gs, Lists.newArrayList(location), plotName, plotEdaplosId);

        testDatas.createZone(plot, ZoneType.PRINCIPALE);

        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        // required imports
        refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "AAT20", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.AUTRE);
        refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "AAT04", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.AUTRE);

        RefEspece fraisier = testDatas.createRefEspece(refEspeceDao, "ZBV", "H50", "Fraisier", "");
        RefEspece sapin = testDatas.createRefEspece(refEspeceDao, "I44", "", "Sapin", "");
        RefEspece pivoine = testDatas.createRefEspece(refEspeceDao, "I98", "", "Pivoine", "");
        RefEspece chrysantheme = testDatas.createRefEspece(refEspeceDao, "E80", "", "Chrysanthème", "");

        CroppingPlanEntryTopiaDao croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        CroppingPlanEntry cultureFraisier = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "Culture_Fraisier_T0",
                CroppingPlanEntry.PROPERTY_CODE, "Culture_Fraisier_C0",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN,
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_NAME, "Culture Fraisier"
        );

        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        CroppingPlanSpecies especeFraisier = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "CPS_fraisier_topiaid0",
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_fraisier_code",
                CroppingPlanSpecies.PROPERTY_SPECIES, fraisier,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cultureFraisier
        );

        List<CroppingPlanSpecies> especesFraisiers = new ArrayList<>();
        especesFraisiers.add(especeFraisier);
        cultureFraisier.addAllCroppingPlanSpecies(especesFraisiers);

        getPersistenceContext().commit();

        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        refSpeciesToSectorDao.createByNaturalId(fraisier.getCode_espece_botanique(), fraisier.getCode_qualifiant_AEE(), Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId(sapin.getCode_espece_botanique(), sapin.getCode_qualifiant_AEE(), Sector.HORTICULTURE);
        refSpeciesToSectorDao.createByNaturalId(pivoine.getCode_espece_botanique(), pivoine.getCode_qualifiant_AEE(), Sector.HORTICULTURE);
        refSpeciesToSectorDao.createByNaturalId(chrysantheme.getCode_espece_botanique(), chrysantheme.getCode_qualifiant_AEE(), Sector.HORTICULTURE);

        List<EffectiveCropCycleNode> cycleNodes = new ArrayList<>();
        EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        EffectiveCropCycleNode nodeFraisier = effectiveCropCycleNodeDao.create(
                EffectiveCropCycleNode.PROPERTY_TOPIA_ID, "NODE_FRAISIER",
                EffectiveCropCycleNode.PROPERTY_RANK, 0,
                EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY, cultureFraisier,
                EffectiveCropCycleNode.PROPERTY_EDAPLOS_ISSUER_ID, "Edaplos_node0_id"
        );
        cycleNodes.add(nodeFraisier);

        EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao = getPersistenceContext().getEffectiveSeasonalCropCycleDao();
        effectiveSeasonalCropCycleDao.create(
                EffectiveSeasonalCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectiveSeasonalCropCycle.PROPERTY_NODES, cycleNodes);

        EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionDao = getPersistenceContext().getEffectiveCropCycleConnectionDao();
        effectiveCropCycleConnectionDao.create(
                EffectiveCropCycleConnection.PROPERTY_SOURCE, nodeFraisier,
                EffectiveCropCycleConnection.PROPERTY_TARGET, nodeFraisier
        );

        return d0;
    }

    protected Pair<Domain, PracticedCropCyclePhase> createPracticedTestData() throws IOException {
        RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao = getPersistenceContext().getRefSolProfondeurIndigoDao();
        RefSolProfondeurIndigo solProfond = refSolProfondeurIndigoDao.forClasse_de_profondeur_INDIGOEquals(PerformanceServiceImpl.DEFAULT_DEEPEST_INDIGO).findUnique();

        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        DomainTopiaDao domainDao = getPersistenceContext().getDomainDao();
        RefLegalStatusTopiaDao refLegalStatusDao = getPersistenceContext().getRefLegalStatusDao();

        RefLocation location = testDatas.createRefLocation(refLocationDao);
        Domain d0 = testDatas.createSimpleDomain(domainDao, "fr.inra.agrosyst.api.entities.Domain_7fd194f6-7440-432f-a37b-fbf1d27fa95e", location, testDatas.createRefLegalStatus(refLegalStatusDao), "Baulon", 2017, "Annie Verssaire", "Lorem ipsum dolor sit amet", 100.0, 9.0, 1.0, true);
        GrowingPlan gp = testDatas.createGrowingPlan(d0);
        GrowingSystem gs = testDatas.createGrowingSystem(gp);
        String plotName = "Plot Baulon 2";
        String plotEdaplosId = "edaplosIssuerIdBaulon2";

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorDecomposedOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, gs,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(d0.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        RefSolTextureGeppaTopiaDao refSolTextureGeppaDao = getPersistenceContext().getRefSolTextureGeppaDao();
        RefSolTextureGeppa solArgiloSaleuse = refSolTextureGeppaDao.forNaturalId("AS").findUnique();
        RefSolTextureGeppa solSableArgiloLimoneux = refSolTextureGeppaDao.forNaturalId("Sal").findUnique();
        Plot plot = testDatas.createPlot(solProfond, solArgiloSaleuse, solSableArgiloLimoneux, gs, Lists.newArrayList(location), plotName, plotEdaplosId);

        testDatas.createZone(plot, ZoneType.PRINCIPALE);

        // required imports
        refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "AAT20", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.AUTRE);
        refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "AAT04", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.AUTRE);

        RefEspece fraisier = testDatas.createRefEspece(refEspeceDao, "ZBV", "H50", "Fraisier", "");
        RefEspece sapin = testDatas.createRefEspece(refEspeceDao, "I44", "", "Sapin", "");
        RefEspece pivoine = testDatas.createRefEspece(refEspeceDao, "I98", "", "Pivoine", "");
        RefEspece chrysantheme = testDatas.createRefEspece(refEspeceDao, "E80", "", "Chrysanthème", "");

        CroppingPlanEntryTopiaDao croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        CroppingPlanEntry cultureFraisier = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "Culture_Fraisier_T0",
                CroppingPlanEntry.PROPERTY_CODE, "Culture_Fraisier_C0",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN,
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_NAME, "Culture Fraisier"
        );

        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        CroppingPlanSpecies especeFraisier = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "CPS_fraisier_topiaid0",
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_fraisier_code",
                CroppingPlanSpecies.PROPERTY_SPECIES, fraisier,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cultureFraisier
        );

        List<CroppingPlanSpecies> especesFraisiers = new ArrayList<>();
        especesFraisiers.add(especeFraisier);
        cultureFraisier.addAllCroppingPlanSpecies(especesFraisiers);

        getPersistenceContext().commit();

        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        refSpeciesToSectorDao.createByNaturalId(fraisier.getCode_espece_botanique(), fraisier.getCode_qualifiant_AEE(), Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId(sapin.getCode_espece_botanique(), sapin.getCode_qualifiant_AEE(), Sector.HORTICULTURE);
        refSpeciesToSectorDao.createByNaturalId(pivoine.getCode_espece_botanique(), pivoine.getCode_qualifiant_AEE(), Sector.HORTICULTURE);
        refSpeciesToSectorDao.createByNaturalId(chrysantheme.getCode_espece_botanique(), chrysantheme.getCode_qualifiant_AEE(), Sector.HORTICULTURE);

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureFraisier.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100.0d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        return Pair.of(d0, cropCyclePhase);
    }

    @Test
    public void inraeEffectiveWithPriceTest() throws IOException {
        inraeEffectiveTest(
                10.0d,
                0.05d,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Culture Fraisier (rang 1);;précédent non renseigné;intervention;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2017;Baulon;2250.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Culture Fraisier (rang 1);;précédent non renseigné;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, substrat (€/ha);2017;Baulon;3000.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;Plot Baulon 2;Zone principale;42.0;Culture Fraisier;;Fraisier;;Fraisier;;;Autre;intervention;01/04/2017;01/04/2017;Autre;Substrat organique naturel - Terre de Bruyère - Substrat organique naturel - Terre de Bruyère;;null;2017;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2000.0;0;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;Plot Baulon 2;Zone principale;42.0;Culture Fraisier;;Fraisier;;Fraisier;;;Autre;intervention;01/04/2017;01/04/2017;Autre;Substrat organique naturel - Terre de Bruyère - Substrat organique naturel - Terre de Bruyère;;null;2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, substrat (€/ha);4000.0;0;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;Plot Baulon 2;Zone principale;42.0;Culture Fraisier;;Fraisier;;Fraisier;;;Autre;intervention;01/04/2017;01/04/2017;Autre;Substrat organique transformé - Sciures de bois compostées - Substrat organique transformé - Sciures de bois compostées;;null;2017;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2500.0;0;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;Plot Baulon 2;Zone principale;42.0;Culture Fraisier;;Fraisier;;Fraisier;;;Autre;intervention;01/04/2017;01/04/2017;Autre;Substrat organique transformé - Sciures de bois compostées - Substrat organique transformé - Sciures de bois compostées;;null;2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, substrat (€/ha);2000.0;0;;;");
                });
    }

    @Test
    public void inraeEffectiveWithoutPriceTest() throws IOException {
        inraeEffectiveTest(
                null,
                null,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Culture Fraisier (rang 1);;précédent non renseigné;intervention;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2017;Baulon;3000.0;66;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;Plot Baulon 2;Zone principale;42.0;Culture Fraisier;;Fraisier;;Fraisier;;;Autre;intervention;01/04/2017;01/04/2017;Autre;Substrat organique naturel - Terre de Bruyère - Substrat organique naturel - Terre de Bruyère;;null;2017;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);4000.0;0;Intrant 'Substrat (Substrat organique naturel - Terre de Bruyère null)' : Prix non renseigné;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;Plot Baulon 2;Zone principale;42.0;Culture Fraisier;;Fraisier;;Fraisier;;;Autre;intervention;01/04/2017;01/04/2017;Autre;Substrat organique transformé - Sciures de bois compostées - Substrat organique transformé - Sciures de bois compostées;;null;2017;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2000.0;0;Intrant 'Substrat (Substrat organique transformé - Sciures de bois compostées null)' : Prix non renseigné;;");
                });
    }

    private void inraeEffectiveTest(Double price1, Double price2, Consumer<String> assertions) throws IOException {
        testDatas.createDemoRefLocation();
        testDatas.importSolTextureGeppa();
        testDatas.importSolProfondeurIndigo();
        testDatas.importRefInputUnitPriceUnitConverterCSV();
        testDatas.createRefInputUnitPriceUnitConverter();

        Domain d0 = createEffectiveTestData();

        RefInterventionAgrosystTravailEDI aat04 = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("AAT04").findUnique();

        RefSubstrateTopiaDao refSubstrateDao = getPersistenceContext().getRefSubstrateDao();
        RefSubstrate substratOrganiqueNaturelBruyere = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique naturel",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Terre de Bruyère",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.VOLUME,
                RefSubstrate.PROPERTY_ACTIVE, true);
        RefSubstrate substratOrganiqueTransformeSciuresBoisCompostees = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique transformé",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Sciures de bois compostées",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.MASS,
                RefSubstrate.PROPERTY_ACTIVE, true);

        EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        EffectiveCropCycleNode nodeFraisier = effectiveCropCycleNodeDao.forTopiaIdEquals("NODE_FRAISIER").findUnique();

        RefPrixSubstrateTopiaDao refPrixSubstratesDao = getPersistenceContext().getRefPrixSubstrateDao();
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 20.0, PriceUnit.EURO_M3, 2017);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 50.0, PriceUnit.EURO_M3, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 23.0, PriceUnit.EURO_M3, 2018);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1(), substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(), 0.04, PriceUnit.EURO_KG, 2017);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1(), substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(), 15.0, PriceUnit.EURO_T, 2016);

        CroppingPlanEntry cropFraisier = nodeFraisier.getCroppingPlanEntry();
        List<CroppingPlanSpecies> agrosystSpecies = cropFraisier.getCroppingPlanSpecies();
        List<EffectiveSpeciesStade> interventionSpeciesStades = new ArrayList<>();
        for (CroppingPlanSpecies especeFraisier : agrosystSpecies) {
            EffectiveSpeciesStade interventionSpeciesStadeFraisier = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especeFraisier);
            interventionSpeciesStades.add(interventionSpeciesStadeFraisier);
        }
        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, nodeFraisier,
                EffectiveIntervention.PROPERTY_NAME, "intervention",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.AUTRE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 4, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 4, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.5d,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.5d,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, interventionSpeciesStades
        );

        String objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueNaturelBruyere);
        var substratePrice1 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueNaturelBruyere.getCaracteristic1() + " - " + substratOrganiqueNaturelBruyere.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, price1
        );
        var substrateInput1 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueNaturelBruyere.getCaracteristic1() + " - " + substratOrganiqueNaturelBruyere.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.L_M2,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueNaturelBruyere,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice1,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage1 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput1,
                AbstractInputUsage.PROPERTY_QT_AVG, 20.0d
        );

        objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueTransformeSciuresBoisCompostees);
        var substratePrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1() + " - " + substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, price2
        );
        var substrateInput2 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1() + " - " + substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.KG_M2,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueTransformeSciuresBoisCompostees,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage2 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput2,
                AbstractInputUsage.PROPERTY_QT_AVG, 5.0d
        );

        //Cas particulier des intrants "SUBSTRAT":
        //ils sont conditionnés au fait qu'une action Autre à comme action principale (référentiel "Intervention Agrosyst Travail EDI") un reference_code égal à 'AAT27', 'AAT20' ou 'AAT04'
        OtherActionTopiaDao otherActionDao = getPersistenceContext().getOtherActionDao();
        otherActionDao.create(
                OtherAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                OtherAction.PROPERTY_MAIN_ACTION, aat04,
                OtherAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_POT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_SUBSTRATE_INPUT_USAGES, List.of(substrateUsage1, substrateUsage2)
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(SUBSTRAT));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertions.accept(content);
    }

    @Test
    public void inraePracticedTest() throws IOException {
        testDatas.createDemoRefLocation();
        testDatas.importSolTextureGeppa();
        testDatas.importSolProfondeurIndigo();
        testDatas.importRefInputUnitPriceUnitConverterCSV();
        testDatas.createRefInputUnitPriceUnitConverter();

        var pair = createPracticedTestData();
        var d0 = pair.getLeft();
        var cropCyclePhase = pair.getRight();

        RefInterventionAgrosystTravailEDI aat04 = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("AAT04").findUnique();

        RefSubstrateTopiaDao refSubstrateDao = getPersistenceContext().getRefSubstrateDao();
        RefSubstrate substratOrganiqueNaturelBruyere = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique naturel",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Terre de Bruyère",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.VOLUME,
                RefSubstrate.PROPERTY_ACTIVE, true);
        RefSubstrate substratOrganiqueTransformeSciuresBoisCompostees = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique transformé",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Sciures de bois compostées",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.MASS,
                RefSubstrate.PROPERTY_ACTIVE, true);

        RefPrixSubstrateTopiaDao refPrixSubstratesDao = getPersistenceContext().getRefPrixSubstrateDao();
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 20.0, PriceUnit.EURO_M3, 2017);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 50.0, PriceUnit.EURO_M3, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 23.0, PriceUnit.EURO_M3, 2018);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1(), substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(), 0.04, PriceUnit.EURO_KG, 2017);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1(), substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(), 15.0, PriceUnit.EURO_T, 2016);

        String cropFraisierCode = cropCyclePhase.getPracticedPerennialCropCycle().getCroppingPlanEntryCode();
        CroppingPlanEntryTopiaDao croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        CroppingPlanEntry cropFraisier = croppingPlanEntryDao.forCodeEquals(cropFraisierCode).findUnique();
        List<CroppingPlanSpecies> agrosystSpecies = cropFraisier.getCroppingPlanSpecies();
        List<PracticedSpeciesStade> interventionSpeciesStades = new ArrayList<>();
        for (CroppingPlanSpecies especeFraisier : agrosystSpecies) {
            PracticedSpeciesStade interventionSpeciesStadeFraisier = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, especeFraisier.getCode());
            interventionSpeciesStades.add(interventionSpeciesStadeFraisier);
        }
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.AUTRE,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0d,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.0d,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d,
                PracticedIntervention.PROPERTY_SPECIES_STADES, interventionSpeciesStades
        );

        String objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueNaturelBruyere);
        var substratePrice1 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueNaturelBruyere.getCaracteristic1() + " - " + substratOrganiqueNaturelBruyere.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, 10.0d
        );
        var substrateInput1 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueNaturelBruyere.getCaracteristic1() + " - " + substratOrganiqueNaturelBruyere.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.L_M2,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueNaturelBruyere,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice1,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage1 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput1,
                AbstractInputUsage.PROPERTY_QT_AVG, 20.0d
        );

        objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueTransformeSciuresBoisCompostees);
        var substratePrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1() + " - " + substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, 0.05d
        );
        var substrateInput2 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1() + " - " + substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.KG_M2,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueTransformeSciuresBoisCompostees,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage2 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput2,
                AbstractInputUsage.PROPERTY_QT_AVG, 5.0d
        );

        //Cas particulier des intrants "SUBSTRAT":
        //ils sont conditionnés au fait qu'une action Autre à comme action principale (référentiel "Intervention Agrosyst Travail EDI") un reference_code égal à 'AAT27', 'AAT20' ou 'AAT04'
        OtherActionTopiaDao otherActionDao = getPersistenceContext().getOtherActionDao();
        otherActionDao.create(
                OtherAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                OtherAction.PROPERTY_MAIN_ACTION, aat04,
                OtherAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_POT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_SUBSTRATE_INPUT_USAGES, List.of(substrateUsage1, substrateUsage2)
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(SUBSTRAT));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;gsName;Culture Fraisier;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2017;Baulon;4500.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;gsName;Culture Fraisier;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, substrat (€/ha);2017;Baulon;6000.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;épandage;Culture Fraisier;;Fraisier;;Fraisier;;Pleine production;Autre;intervention;03/04;03/04;Autre;Substrat organique naturel - Terre de Bruyère - Substrat organique naturel - Terre de Bruyère;;null;2017;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2000.0;0;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;épandage;Culture Fraisier;;Fraisier;;Fraisier;;Pleine production;Autre;intervention;03/04;03/04;Autre;Substrat organique naturel - Terre de Bruyère - Substrat organique naturel - Terre de Bruyère;;null;2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, substrat (€/ha);4000.0;0;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;épandage;Culture Fraisier;;Fraisier;;Fraisier;;Pleine production;Autre;intervention;03/04;03/04;Autre;Substrat organique transformé - Sciures de bois compostées - Substrat organique transformé - Sciures de bois compostées;;null;2017;Indicateur économique;Charges opérationnelles réelles substrat (€/ha);2500.0;0;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2017);;;gsName;;;épandage;Culture Fraisier;;Fraisier;;Fraisier;;Pleine production;Autre;intervention;03/04;03/04;Autre;Substrat organique transformé - Sciures de bois compostées - Substrat organique transformé - Sciures de bois compostées;;null;2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, substrat (€/ha);2000.0;0;;;");
    }
}
