package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Test specific à l'indicateur temps de travail manuel.
 */
public class IndicatorManualWorkTimeTest extends AbstractAgrosystTest {

    protected PracticedSystem psBaulon2013;

    protected List<CroppingPlanEntry> croppingPlanEntries;

    protected PerformanceServiceImplForTest performanceService;
    protected GrowingSystemService growingSystemService;
    protected DomainService domainService;
    protected ImportService importService;
    protected ReferentialService referentialService;

    @BeforeEach
    public void setupServices() throws Exception {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceServiceImpl.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);

        importService = serviceFactory.newService(ImportService.class);
        growingSystemService = serviceFactory.newService(GrowingSystemService.class);
        domainService = serviceFactory.newService(DomainService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        // required referential
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiMinUnifa();
        testDatas.importRefEspeces();
        // test datas
        psBaulon2013 = testDatas.createTestPraticedSystems();
        croppingPlanEntries = testDatas.findCroppingPlanEntries(psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain().getTopiaId());

        loginAsAdmin();
        alterSchema();
    }

    // SEO intervention : Not manual intervention : time = 0
    @Test
    public void testSEOEffectiveIntervention() throws Exception {
        testDatas.createTestEffectiveInterventions();

        // complete to add some other toolscoupling and an irrigation intervention
        Domain domainBaulon = testDatas.completeBaulonDomain();
        EffectiveIntervention intervention = testDatas.createBaulonIrrigationEffectiveIntervention();
        Collection<String> codeAmmBioControle = referentialService.getCodeAmmBioControle(domainBaulon.getCampaign());

        IndicatorManualWorkTime indicatorManualWorkTime = serviceFactory.newInstance(IndicatorManualWorkTime.class);

        PerformanceGlobalExecutionContext globalExecutionContext = null;
        PerformanceEffectiveInterventionExecutionContext interventionContext =
                new PerformanceEffectiveInterventionExecutionContext(
                        intervention,
                        domainBaulon.getCampaign(),
                        intervention.getToolCouplings().iterator().next(),
                        Collections.emptyList(),
                        null,
                        null,
                        Collections.emptyList(),
                        new HashSetValuedHashMap<>());
        PerformanceZoneExecutionContext zoneContext = null;

        List<ToolsCoupling> toolsCouplings = getPersistenceContext().getToolsCouplingDao().forDomainEquals(domainBaulon).findAll();
        PerformanceEffectiveDomainExecutionContext domainContext = new PerformanceEffectiveDomainExecutionContext(
                Pair.of(domainBaulon, domainBaulon),
                toolsCouplings,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                0.0d,
                0.0d,
                0.0d,
                codeAmmBioControle,
                Optional.empty(),
                Collections.emptyList());

        WriterContext writerContext = WriterContext.createNotUsedWriterContext();
        Double[] manualWorkTimes = indicatorManualWorkTime.manageIntervention(
                writerContext,
                globalExecutionContext,
                domainContext,
                zoneContext,
                null,
                interventionContext);
        Assertions.assertNull(manualWorkTimes);
    }

    @Test
    public void testSEOPractivedIntervention() throws Exception {
        testDatas.createTestEffectiveInterventions();

        // complete to add some other toolscoupling and an irrigation intervention
        testDatas.completeBaulonDomain();

        GrowingSystem growingSystem = testDatas.getGrowingSystemForName("Systeme de culture Baulon 1");
        PracticedIntervention intervention = testDatas.createBaulonSEOPracticedIntervention(growingSystem);

        IndicatorManualWorkTime indicatorManualWorkTime = serviceFactory.newInstance(IndicatorManualWorkTime.class);
        Double[] manualWorkTimes = performanceService.manageIntervention(indicatorManualWorkTime, growingSystem, intervention);
        Assertions.assertEquals(13, manualWorkTimes.length);
        Assertions.assertEquals(0d, manualWorkTimes[0].doubleValue());
    }

    //W09 intervention : manual intervention with TC

    @Test
    public void testW09EffectiveIntervention() throws Exception {
        testDatas.createTestEffectiveInterventions();

        // complete to add some other toolscoupling
        Domain domainBaulon = testDatas.completeBaulonDomain();
        EffectiveIntervention intervention = testDatas.createBaulonW09EffectiveIntervention(2.0, 0.8);
        Collection<String> codeAmmBioControle = referentialService.getCodeAmmBioControle(domainBaulon.getCampaign());

        IndicatorManualWorkTime indicatorManualWorkTime = serviceFactory.newInstance(IndicatorManualWorkTime.class);
        PerformanceGlobalExecutionContext globalExecutionContext = null;
        PerformanceEffectiveInterventionExecutionContext interventionContext = new PerformanceEffectiveInterventionExecutionContext(
                intervention,
                domainBaulon.getCampaign(),
                intervention.getToolCouplings().iterator().next(),
                Collections.emptyList(),
                null,
                null,
                Collections.emptyList(),
                new HashSetValuedHashMap<>());
        PerformanceZoneExecutionContext zoneContext = null;

        List<ToolsCoupling> toolsCouplings = getPersistenceContext().getToolsCouplingDao().forDomainEquals(domainBaulon).findAll();
        PerformanceEffectiveDomainExecutionContext domainContext = new PerformanceEffectiveDomainExecutionContext(
                Pair.of(domainBaulon, domainBaulon),
                toolsCouplings,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                0.0d,
                0.0d,
                0.0d,
                codeAmmBioControle,
                Optional.empty(),
                Collections.emptyList());

        WriterContext writerContext = WriterContext.createNotUsedWriterContext();
        Double[] manualWorkTimes = indicatorManualWorkTime.manageIntervention(
                writerContext,
                globalExecutionContext,
                domainContext,
                zoneContext,
                null,
                interventionContext);
        Assertions.assertEquals(13, manualWorkTimes.length);
        Assertions.assertEquals(1.52d, manualWorkTimes[12], 0.00001d); // Total
        Assertions.assertEquals(0d, manualWorkTimes[0], 0.00001d); // January
        Assertions.assertEquals(0.38d, manualWorkTimes[6], 0.00001d); // July
        Assertions.assertEquals(1.14d, manualWorkTimes[7], 0.00001d); // August
        Assertions.assertEquals(0d, manualWorkTimes[8], 0.00001d);
    }

    @Test
    public void testW09PracticedIntervention() throws Exception {
        testDatas.createTestEffectiveInterventions();

        // complete to add some other toolscoupling
        testDatas.completeBaulonDomain();

        GrowingSystem growingSystem = testDatas.getGrowingSystemForName("Systeme de culture Baulon 1");

        PracticedIntervention intervention = testDatas.createBaulonW09PracticedIntervention(growingSystem);

        IndicatorManualWorkTime indicatorManualWorkTime = serviceFactory.newInstance(IndicatorManualWorkTime.class);
        Double[] manualWorkTimes = performanceService.manageIntervention(indicatorManualWorkTime, growingSystem, intervention);

        Assertions.assertEquals(13, manualWorkTimes.length);
        Assertions.assertEquals(2.4d, manualWorkTimes[12], 0.00001d); // Total
        Assertions.assertEquals(0d, manualWorkTimes[0], 0.00001d); // January
        Assertions.assertEquals(0.96d, manualWorkTimes[6], 0.00001d); // July
        Assertions.assertEquals(1.44d, manualWorkTimes[7], 0.00001d); // August
        Assertions.assertEquals(0d, manualWorkTimes[8], 0.00001d); // September
    }

    //W12 intervention : manual intervention without TC

    @Test
    public void testW12EffectiveInterventionWithoutTC() throws Exception {
        testDatas.createTestEffectiveInterventions();

        // complete to add some other toolscoupling
        Domain domainBaulon = testDatas.completeBaulonDomain();
        EffectiveIntervention intervention = testDatas.createBaulonEntretienVigneEffectiveInterventionWithoutTC();
        Collection<String> codeAmmBioControle = referentialService.getCodeAmmBioControle(domainBaulon.getCampaign());

        IndicatorManualWorkTime indicatorManualWorkTime = serviceFactory.newInstance(IndicatorManualWorkTime.class);
        PerformanceGlobalExecutionContext globalExecutionContext = null;
        PerformanceEffectiveInterventionExecutionContext interventionContext = new PerformanceEffectiveInterventionExecutionContext(
                intervention,
                domainBaulon.getCampaign(),
                null,
                Collections.emptyList(),
                null,
                null,
                Collections.emptyList(),
                new HashSetValuedHashMap<>());
        PerformanceZoneExecutionContext zoneContext = null;

        List<ToolsCoupling> toolsCouplings = getPersistenceContext().getToolsCouplingDao().forDomainEquals(domainBaulon).findAll();
        PerformanceEffectiveDomainExecutionContext domainContext = new PerformanceEffectiveDomainExecutionContext(
                Pair.of(domainBaulon, domainBaulon),
                toolsCouplings,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                0.0d,
                0.0d,
                0.0d,
                codeAmmBioControle,
                Optional.empty(),
                Collections.emptyList());

        WriterContext writerContext = WriterContext.createNotUsedWriterContext();
        Double[] manualWorkTimes = indicatorManualWorkTime.manageIntervention(
                writerContext,
                globalExecutionContext,
                domainContext,
                zoneContext,
                null,
                interventionContext);
        Assertions.assertEquals(13, manualWorkTimes.length);
        Assertions.assertEquals(1.33d, manualWorkTimes[12], 0.00001d); // Total
        Assertions.assertEquals(0d, manualWorkTimes[0], 0.00001d); // January
        Assertions.assertEquals(1.064d, manualWorkTimes[6], 0.0001d); // July
        Assertions.assertEquals(0.266d, manualWorkTimes[7], 0.0001d); // August
        Assertions.assertEquals(0d, manualWorkTimes[8], 0.00001d); // September
    }

    @Test
    public void testW12PracticedInterventionWithoutTC() {
        // complete to add some other toolscoupling
        testDatas.completeBaulonDomain();

        GrowingSystem growingSystem = testDatas.getGrowingSystemForName("Systeme de culture Baulon 1");
        PracticedIntervention intervention = testDatas.createBaulonEntretienVignePracticedInterventionWithoutTC(growingSystem);

        IndicatorManualWorkTime indicatorManualWorkTime = serviceFactory.newInstance(IndicatorManualWorkTime.class);
        Double[] manualWorkTimes = performanceService.manageIntervention(indicatorManualWorkTime, growingSystem, intervention);

        Assertions.assertEquals(13, manualWorkTimes.length);
        Assertions.assertEquals(0.576d, manualWorkTimes[12], 0.00001d); // Total
        Assertions.assertEquals(0d, manualWorkTimes[0], 0.00001d); // January
        Assertions.assertEquals(0.4608d, manualWorkTimes[6], 0.00001d); // July
        Assertions.assertEquals(0.1152d, manualWorkTimes[7], 0.00001d); // August
        Assertions.assertEquals(0d, manualWorkTimes[8], 0.00001d); // September
    }
}
