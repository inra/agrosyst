package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.performance.indicators.qsa.IndicatorCopperFertilisationProduct;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class IndicatorCopperFertilisationProductTest extends AbstractIndicatorFertilizationTest {

    @Test
    public void testFertilisationRealise_12393() {
        // Constitution du jeu de données de test
        List<DonneesTestFertilisationMineRealise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorQsaCuivreFertilisation = "QSA Cuivre fertilisation";

        {
            var pair = createMelangeGranulePK();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 15.0, 2.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorQsaCuivreFertilisation, "1.8")
                    )
            ));
        }

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorCopperFertilisationProduct indicatorCopperFertilisationProduct = serviceFactory.newInstance(IndicatorCopperFertilisationProduct.class);
        // Exécution des tests
        for (DonneesTestFertilisationMineRealise d : donneesTestFertilisation) {
            this.testFertilisationRealise(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.transitCount(),
                    d.expectedIndicatorNameAndValue(),
                    "Substances actives",
                    indicatorMineralFertilization,
                    indicatorCopperFertilisationProduct
            );
        }
    }

    @Test
    public void testFertilisationSynthetise_12393() throws IOException {
        // Constitution du jeu de données de test
        List<DonneesTestFertilisationMineSynthetise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorQsaCuivreFertilisation = "QSA Cuivre fertilisation";

        {
            var pair = createMelangeGranulePK();

            final double qtAvg = 15.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 0.33, 5.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaCuivreFertilisation, new IndicatorInfo("1.485", inputName))
                    )
            ));
        }

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorCopperFertilisationProduct indicatorCopperFertilisationProduct = serviceFactory.newInstance(IndicatorCopperFertilisationProduct.class);
        for (DonneesTestFertilisationMineSynthetise d : donneesTestFertilisation) {
            this.testFertilisationSynthetise(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.temporalFrequency(),
                    d.expectedIndicatorNameAndInfo(),
                    "Substances actives",
                    indicatorMineralFertilization,
                    indicatorCopperFertilisationProduct
            );
        }
    }
}
