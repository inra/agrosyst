package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalControlActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.domain.DomainServiceImpl;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorOrganicProductsTest  extends AbstractAgrosystTest {
    private static final Function<CroppingPlanEntry, String> GET_CPE_NAME = input -> input == null ? null : input.getName();

    private AgrosystI18nService i18nService;
    private DomainInputStockUnitService domainInputStockUnitService;
    private DomainService domainService;
    private PerformanceServiceImplForTest performanceService;
    private EffectiveCropCycleService effectiveCropCycleService;

    private DomainTopiaDao domainDao;
    private ZoneTopiaDao zoneTopiaDao;
    private EffectiveCropCyclePhaseTopiaDao cropCyclePhaseDAO;
    private EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    private EffectiveInterventionTopiaDao effectiveInterventionDAO;
    private EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeTopiaDao;
    private BiologicalControlActionTopiaDao biologicalControlActionDao;
    private RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDIDAO;
    private RefActaTraitementsProduitTopiaDao refActaTraitementsProduitsTopiaDao;

    private BiologicalProductInputUsageTopiaDao biologicalProductInputUsageDao;

    private Map<String, String> messages;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        i18nService = serviceFactory.newService(AgrosystI18nService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);
        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        domainService = serviceFactory.newService(DomainService.class);

        // init dao
        domainDao = serviceContext.getPersistenceContext().getDomainDao();
        zoneTopiaDao = serviceContext.getPersistenceContext().getZoneDao();
        cropCyclePhaseDAO = serviceContext.getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = serviceContext.getPersistenceContext().getEffectivePerennialCropCycleDao();
        effectiveInterventionDAO = serviceContext.getPersistenceContext().getEffectiveInterventionDao();
        effectiveSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getEffectiveSpeciesStadeDao();
        biologicalControlActionDao = serviceContext.getPersistenceContext().getBiologicalControlActionDao();
        refActionAgrosystTravailEDIDAO = serviceContext.getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        refActaTraitementsProduitsTopiaDao = serviceContext.getPersistenceContext().getRefActaTraitementsProduitDao();
        biologicalProductInputUsageDao = serviceContext.getPersistenceContext().getBiologicalProductInputUsageDao();

        messages = i18nService.getMessages();

        // login
        loginAsAdmin();
        alterSchema();

        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaDosageSpc();
        testDatas.importActaGroupeCultures();
    }

    @Test
    public void testAvec1ProduitBiotique() throws IOException {
        Domain domain = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domain.getLocation().getRefCountry();

        RefActaTraitementsProduit refProduit = refActaTraitementsProduitsTopiaDao.forNaturalId("itab_0462", 162, france).findUnique();
        DomainPhytoProductInputDto domainProduitPhyto = testDatas.createDomainPhytoProductDto(refProduit, ProductType.BASIC_SUBSTANCES, PhytoProductUnit.G_HA, null, InputType.LUTTE_BIOLOGIQUE);
        final List<ProduitPhyto> produits = List.of(new ProduitPhyto(refProduit, domainProduitPhyto));

        this.executerTest(domain, produits, (contenu) -> {
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_aux_moyens_biologiques") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_macro_organismes") + ";2013;Baulon;0.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_biotiques") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_abiotiques") + ";2013;Baulon;0.0");
        }, Optional.empty());
    }

    @Test
    public void testAvec1ProduitAbiotique() throws IOException {
        Domain domain = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domain.getLocation().getRefCountry();

        RefActaTraitementsProduit refProduit = refActaTraitementsProduitsTopiaDao.forNaturalId("legifr_0001", 164, france).findUnique();
        DomainPhytoProductInputDto domainProduitPhyto = testDatas.createDomainPhytoProductDto(refProduit, ProductType.PLANT_EXTRACT, PhytoProductUnit.G_HA, null, InputType.LUTTE_BIOLOGIQUE);
        final List<ProduitPhyto> produits = List.of(new ProduitPhyto(refProduit, domainProduitPhyto));

        this.executerTest(domain, produits, (contenu) -> {
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_aux_moyens_biologiques") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_macro_organismes") + ";2013;Baulon;0.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_biotiques") + ";2013;Baulon;0.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_abiotiques") + ";2013;Baulon;1.0");
        }, Optional.empty());
    }

    @Test
    public void testAvec1ProduitMacroOrganisme() throws IOException {
        Domain domain = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domain.getLocation().getRefCountry();

        RefActaTraitementsProduit refProduit = refActaTraitementsProduitsTopiaDao.forNaturalId("a_AABCV", 161, france).findUnique();
        DomainPhytoProductInputDto domainProduitPhyto = testDatas.createDomainPhytoProductDto(refProduit, ProductType.MACRO_ORGANISMS, PhytoProductUnit.G_HA, null, InputType.LUTTE_BIOLOGIQUE);
        final List<ProduitPhyto> produits = List.of(new ProduitPhyto(refProduit, domainProduitPhyto));

        this.executerTest(domain, produits, (contenu) -> {
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_aux_moyens_biologiques") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_macro_organismes") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_biotiques") + ";2013;Baulon;0.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_abiotiques") + ";2013;Baulon;0.0");
        }, Optional.empty());
    }

    @Test
    public void testAvec1ProduitDeChaqueType() throws IOException {
        Domain domain = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domain.getLocation().getRefCountry();

        final List<ProduitData> produitsData = List.of(
                new ProduitData("a_AABCV", 161, ProductType.MACRO_ORGANISMS),
                new ProduitData("legifr_0001", 164, ProductType.PLANT_EXTRACT),
                new ProduitData("maaf_1201091", 165, ProductType.FERTILIZING_MATERIALS_AND_GROWING_MEDIA),
                new ProduitData("itab_0462", 162, ProductType.BASIC_SUBSTANCES)
        );

        List<ProduitPhyto> produits = new ArrayList<>();

        for (ProduitData produitData : produitsData) {
            RefActaTraitementsProduit refProduit = refActaTraitementsProduitsTopiaDao.forNaturalId(produitData.idProduit, produitData.idTraitement, france).findUnique();
            DomainPhytoProductInputDto domainProduitPhyto = testDatas.createDomainPhytoProductDto(refProduit, produitData.productType, PhytoProductUnit.G_HA, null, InputType.LUTTE_BIOLOGIQUE);
            produits.add(new ProduitPhyto(refProduit, domainProduitPhyto));
        }

        this.executerTest(domain, produits, (contenu) -> {
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_aux_moyens_biologiques") + ";2013;Baulon;4.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_macro_organismes") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_biotiques") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_abiotiques") + ";2013;Baulon;2.0");
        }, Optional.empty());
    }

    @Test
    public void testAvec1ProduitDeChaqueTypeAvecPsci82pourcents() throws IOException {
        Domain domain = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domain.getLocation().getRefCountry();

        final List<ProduitData> produitsData = List.of(
                new ProduitData("a_AABCV", 161, ProductType.MACRO_ORGANISMS),
                new ProduitData("legifr_0001", 164, ProductType.PLANT_EXTRACT),
                new ProduitData("maaf_1201091", 165, ProductType.FERTILIZING_MATERIALS_AND_GROWING_MEDIA),
                new ProduitData("itab_0462", 162, ProductType.BASIC_SUBSTANCES)
        );

        List<ProduitPhyto> produits = new ArrayList<>();

        for (ProduitData produitData : produitsData) {
            RefActaTraitementsProduit refProduit = refActaTraitementsProduitsTopiaDao.forNaturalId(produitData.idProduit, produitData.idTraitement, france).findUnique();
            DomainPhytoProductInputDto domainProduitPhyto = testDatas.createDomainPhytoProductDto(refProduit, produitData.productType, PhytoProductUnit.G_HA, null, InputType.LUTTE_BIOLOGIQUE);
            produits.add(new ProduitPhyto(refProduit, domainProduitPhyto));
        }

        final double psci = 0.82;
        this.executerTest(domain, produits, (contenu) -> {
            assertThat(contenu).usingComparator(comparator).isEqualTo(String.format(Locale.US, messages.get("Indicator.label.recours_aux_moyens_biologiques") + ";2013;Baulon;%.2f", psci * 4));
            assertThat(contenu).usingComparator(comparator).isEqualTo(String.format(Locale.US, messages.get("Indicator.label.recours_macro_organismes") + ";2013;Baulon;%.2f", psci * 1));
            assertThat(contenu).usingComparator(comparator).isEqualTo(String.format(Locale.US, messages.get("Indicator.label.recours_produits_biotiques") + ";2013;Baulon;%.2f", psci * 1));
            assertThat(contenu).usingComparator(comparator).isEqualTo(String.format(Locale.US, messages.get("Indicator.label.recours_produits_abiotiques") + ";2013;Baulon;%.2f", psci * 2));
        }, Optional.of(psci * 100));
    }

    @Test
    public void testAvec1ProduitAutre() throws IOException {
        Domain domain = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domain.getLocation().getRefCountry();

        RefActaTraitementsProduit refProduit = refActaTraitementsProduitsTopiaDao.forNaturalId("a_AAASB", 999905, france).findUnique();
        DomainPhytoProductInputDto domainProduitPhyto = testDatas.createDomainPhytoProductDto(refProduit, ProductType.OTHER, PhytoProductUnit.G_HA, null, InputType.LUTTE_BIOLOGIQUE);
        final List<ProduitPhyto> produits = List.of(new ProduitPhyto(refProduit, domainProduitPhyto));

        this.executerTest(domain, produits, (contenu) -> {
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_aux_moyens_biologiques") + ";2013;Baulon;1.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_macro_organismes") + ";2013;Baulon;0.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_biotiques") + ";2013;Baulon;0.0");
            assertThat(contenu).usingComparator(comparator).isEqualTo(messages.get("Indicator.label.recours_produits_abiotiques") + ";2013;Baulon;0.0");
        }, Optional.empty());
    }

    private void executerTest(Domain domain, List<ProduitPhyto> produits, Consumer<String> verifierContenuExport, Optional<Double> psci) {
        final List<DomainInputDto> domainInputs = new ArrayList<>(produits.stream().map(p -> p.produitPhyto).toList());

        final List<CroppingPlanEntry> croppingPlanEntriesForDomain = domainService.getCroppingPlanEntriesForDomain(domain);
        Map<String, CropPersistResult> persistedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(domain, domainInputs, persistedCropResult);

        final Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(domain);
        List<AbstractDomainInputStockUnit> domainPhytoInputs = domainInputStock.get(InputType.LUTTE_BIOLOGIQUE);
        Map<RefActaTraitementsProduit, DomainPhytoProductInput> domainInputPhytoByRefInput =
                domainPhytoInputs.stream()
                        .map(dp -> (DomainPhytoProductInput) dp)
                        .collect(Collectors.toMap(DomainPhytoProductInput::getRefInput, Function.identity()));

        // "Plot Baulon 1" (blé tendre d'hiver)
        List<Zone> zones = zoneTopiaDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = cropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        // ajout d'une intervention
        EffectiveIntervention intervention1 = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention 1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.LUTTE_BIOLOGIQUE,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.now(),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1);

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention1.addSpeciesStades(intervention1Stades);

        List<BiologicalProductInputUsage> productUsages = new ArrayList<>();
        for (ProduitPhyto produitPhyto : produits) {
            final DomainPhytoProductInput domainPhytoProductInput = domainInputPhytoByRefInput.get(produitPhyto.refProduit);
            final BiologicalProductInputUsage biologicalProductInputUsage = biologicalProductInputUsageDao.create(
                    BiologicalProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductInput,
                    BiologicalProductInputUsage.PROPERTY_TARGETS, null,
                    AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.LUTTE_BIOLOGIQUE,
                    AbstractInputUsage.PROPERTY_QT_AVG, 150.0
            );
            productUsages.add(biologicalProductInputUsage);
        }

        RefInterventionAgrosystTravailEDI actionW16 = refActionAgrosystTravailEDIDAO.forReference_codeEquals("W16").findAny();
        biologicalControlActionDao.create(
                BiologicalControlAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention1,
                BiologicalControlAction.PROPERTY_MAIN_ACTION, actionW16,
                BiologicalControlAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, psci.orElse(100.0),
                BiologicalControlAction.PROPERTY_BOILED_QUANTITY, 1d,
                BiologicalControlAction.PROPERTY_BIOLOGICAL_PRODUCT_INPUT_USAGES, productUsages);

        /*
         * Export de performance
         */
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        Performance performance = new PerformanceImpl();
        performance.setName("test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        IndicatorOrganicProducts indicatorOrganicProducts = serviceFactory.newInstance(IndicatorOrganicProducts.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOrganicProducts);
        String content = out.toString();

        verifierContenuExport.accept(content);
    }

    private record ProduitPhyto(RefActaTraitementsProduit refProduit, DomainPhytoProductInputDto produitPhyto) {}
    private record ProduitData(String idProduit, Integer idTraitement, ProductType productType) {}

}
