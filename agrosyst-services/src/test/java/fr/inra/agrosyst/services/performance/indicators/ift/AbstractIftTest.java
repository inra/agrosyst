package fr.inra.agrosyst.services.performance.indicators.ift;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.action.PhytoProductTargetTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.domain.DomainServiceImpl;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.platform.commons.util.StringUtils;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class AbstractIftTest extends AbstractAgrosystTest {

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected DomainService domainService;
    protected PerformanceServiceImplForTest performanceService;
    protected EffectiveCropCycleService effectiveCropCycleService;

    protected DomainTopiaDao domainDao;
    protected ZoneTopiaDao zoneDao;
    protected PlotTopiaDao plotDao;

    protected EffectiveCropCyclePhaseTopiaDao cropCyclePhaseDAO;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDAO;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeTopiaDao;

    protected PracticedCropCyclePhaseTopiaDao practicedCropCyclePhasesDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeTopiaDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionTopiaDao;

    protected PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionTopiaDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDIDAO;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitsTopiaDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected RefMarketingDestinationTopiaDao refMarketingDestinationDao;
    protected RefNuisibleEDITopiaDao refNuisibleEDIDao;

    protected PesticideProductInputUsageTopiaDao pesticideProductInputUsageDao;
    protected PhytoProductTargetTopiaDao phytoProductTargetDao;

    protected ImportService importService;
    protected ReferentialService referentialService;

    protected RefCountry france;
    protected Collection<IndicatorFilter> indicatorFilters;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        testDatas = serviceFactory.newInstance(TestDatas.class);

        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);

        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        domainService = serviceFactory.newService(DomainService.class);

        // init dao
        croppingPlanEntryDao = serviceContext.getPersistenceContext().getCroppingPlanEntryDao();
        croppingPlanSpeciesDao = serviceContext.getPersistenceContext().getCroppingPlanSpeciesDao();
        domainDao = serviceContext.getPersistenceContext().getDomainDao();
        zoneDao = serviceContext.getPersistenceContext().getZoneDao();
        plotDao = serviceContext.getPersistenceContext().getPlotDao();
        practicedCropCyclePhasesDao = serviceContext.getPersistenceContext().getPracticedCropCyclePhaseDao();
        cropCyclePhaseDAO = serviceContext.getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = serviceContext.getPersistenceContext().getEffectivePerennialCropCycleDao();
        practicedSeasonalCropCycleDao = serviceContext.getPersistenceContext().getPracticedSeasonalCropCycleDao();
        effectiveInterventionDAO = serviceContext.getPersistenceContext().getEffectiveInterventionDao();
        practicedInterventionDao = serviceContext.getPersistenceContext().getPracticedInterventionDao();
        effectiveSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getEffectiveSpeciesStadeDao();
        practicedSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getPracticedSpeciesStadeDao();
        pesticidesSpreadingActionTopiaDao = serviceContext.getPersistenceContext().getPesticidesSpreadingActionDao();
        refActionAgrosystTravailEDIDAO = serviceContext.getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        refActaTraitementsProduitsTopiaDao = serviceContext.getPersistenceContext().getRefActaTraitementsProduitDao();
        refEspeceDao = serviceContext.getPersistenceContext().getRefEspeceDao();
        refMarketingDestinationDao = serviceContext.getPersistenceContext().getRefMarketingDestinationDao();
        refNuisibleEDIDao = serviceContext.getPersistenceContext().getRefNuisibleEDIDao();
        practicedCropCycleNodeDao = serviceContext.getPersistenceContext().getPracticedCropCycleNodeDao();
        practicedCropCycleConnectionTopiaDao = serviceContext.getPersistenceContext().getPracticedCropCycleConnectionDao();

        pesticideProductInputUsageDao = serviceContext.getPersistenceContext().getPesticideProductInputUsageDao();
        phytoProductTargetDao = serviceContext.getPersistenceContext().getPhytoProductTargetDao();

        importService = serviceFactory.newService(ImportService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);

        // login
        loginAsAdmin();
        alterSchema();

        indicatorFilters = performanceService.getAllIndicatorFilters();

        testDatas.importCountries();

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refActaProduitRoot.csv")) {
            importService.importRefActaProduitRoot(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refActaTraitementsProduit.csv")) {
            importService.importActaTraitementsProduits(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refActaTraitementsProduitsCateg.csv")) {
            importService.importActaTraitementsProduitsCateg(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refCiblesAgrosystGroupesCiblesMAA.csv")) {
            importService.importRefCiblesAgrosystGroupesCiblesMAA(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refDosageSPC.csv")) {
            importService.importActaDosageSpc(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refIftEspece.csv")) {
            importService.importEspeces(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refIftMAABiocontrole.csv")) {
            importService.importRefMAABiocontrole(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refIftMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refDestination.csv")) {
            importService.importRefDestination(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refQualityCriteria.csv")) {
            importService.importRefQualityCriteria(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refSpeciesToSector.csv")) {
            importService.importRefSpeciesToSector(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refHarvestingPrice.csv")) {
            importService.importRefHarvestingPrice(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/ift/refNuisibleEDI.csv")) {
            importService.importNuisiblesEDI(stream);
        }

        testDatas.importRefConversionUnitesQSA();
        testDatas.importActaGroupeCultures();

        this.france = getPersistenceContext().getRefCountryDao().forTrigramEquals("fra").findUnique();

    }

    private Domain createDomainSubSystem(int campagne) throws IOException {
        testDatas.createBaulonSubSystem(true, campagne);

        // initialisation des paramètres de test
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();

        // culture composée des espèces 10, 11 et 12 au code culture MAA respectivement 10, 11 et 12
        CroppingPlanEntry cpe10_11_12 = testDatas.createCroppingPlanEntry(baulon, croppingPlanEntryDao, "10-11-12");

        RefEspece ref10 = refEspeceDao.forNaturalId("10", "", "ZFB", "").findUnique();
        RefEspece ref11 = refEspeceDao.forNaturalId("11", "", "ZFB", "").findUnique();
        RefEspece ref12 = refEspeceDao.forNaturalId("12", "", "ZFB", "").findUnique();
        CroppingPlanSpecies e10 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref10, cpe10_11_12);
        CroppingPlanSpecies e11 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref11, cpe10_11_12);
        CroppingPlanSpecies e12 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref12, cpe10_11_12);

        cpe10_11_12.addCroppingPlanSpecies(e10);
        cpe10_11_12.addCroppingPlanSpecies(e11);
        cpe10_11_12.addCroppingPlanSpecies(e12);

        // culture composée des espèces 20, 21 et 22 au code culture MAA respectivement 20, 21 et 22
        croppingPlanEntryDao.update(cpe10_11_12);

        CroppingPlanEntry cpe20_21_22 = testDatas.createCroppingPlanEntry(baulon, croppingPlanEntryDao, "20-21-22");

        RefEspece ref20 = refEspeceDao.forNaturalId("20", "", "", "").findUnique();
        RefEspece ref21 = refEspeceDao.forNaturalId("21", "", "", "").findUnique();
        RefEspece ref22 = refEspeceDao.forNaturalId("22", "", "", "").findUnique();
        CroppingPlanSpecies e20 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref20, cpe20_21_22);
        CroppingPlanSpecies e21 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref21, cpe20_21_22);
        CroppingPlanSpecies e22 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref22, cpe20_21_22);

        cpe20_21_22.addCroppingPlanSpecies(e20);
        cpe20_21_22.addCroppingPlanSpecies(e21);
        cpe20_21_22.addCroppingPlanSpecies(e22);

        croppingPlanEntryDao.update(cpe20_21_22);

        // culture sans espèce: Pas de culture -> pas de dose de référence -> IFT = PSCIphyto * 1
        testDatas.createCroppingPlanEntry(baulon, croppingPlanEntryDao, "NO_SPECIES");

        // culture composée des espèces 10 et 20 au code culture MAA respectivement 10 et 20
        CroppingPlanEntry cpe1020 = testDatas.createCroppingPlanEntry(baulon, croppingPlanEntryDao, "10-20");
        CroppingPlanSpecies cpe1020_e10 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref10, cpe1020);
        CroppingPlanSpecies cpe1020_e20 = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref20, cpe1020);
        cpe1020.addCroppingPlanSpecies(cpe1020_e10);
        cpe1020.addCroppingPlanSpecies(cpe1020_e20);

        CroppingPlanEntry cpeZapZao = testDatas.createCroppingPlanEntry(baulon, croppingPlanEntryDao, "zap-zao");
        RefEspece refZapZao = refEspeceDao.forNaturalId("ZAP", "ZAO", "", "").findUnique();
        CroppingPlanSpecies eZapZao = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, refZapZao, cpeZapZao);
        cpeZapZao.addCroppingPlanSpecies(eZapZao);
        croppingPlanEntryDao.update(cpeZapZao);

        CroppingPlanEntry cpe10 = testDatas.createCroppingPlanEntry(baulon, croppingPlanEntryDao, "10");
        CroppingPlanSpecies e10b = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref10, cpe10);
        cpe10.addCroppingPlanSpecies(e10b);
        croppingPlanEntryDao.update(cpe10);

        CroppingPlanEntry cpe20 = testDatas.createCroppingPlanEntry(baulon, croppingPlanEntryDao, "20");
        CroppingPlanSpecies e20b = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, ref20, cpe20);
        cpe20.addCroppingPlanSpecies(e20b);
        croppingPlanEntryDao.update(cpe20);

        RefActaTraitementsProduit herbi1 = refActaTraitementsProduitsTopiaDao.forNaturalId("1", 1, france).findUnique();
        DomainPhytoProductInputDto domainPhyto_herbi1 = testDatas.createDomainPhytoProductDto(herbi1, ProductType.HERBICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        RefActaTraitementsProduit fongi = refActaTraitementsProduitsTopiaDao.forNaturalId("2", 2, france).findUnique();
        DomainPhytoProductInputDto domainPhyto_herbi2 = testDatas.createDomainPhytoProductDto(fongi, ProductType.FUNGICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        RefActaTraitementsProduit insecticide = refActaTraitementsProduitsTopiaDao.forNaturalId("3", 3, france).findUnique();
        DomainPhytoProductInputDto domainPhyto_herbi3 = testDatas.createDomainPhytoProductDto(insecticide, ProductType.INSECTICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        RefActaTraitementsProduit herbi5 = refActaTraitementsProduitsTopiaDao.forNaturalId("5", 5, france).findUnique();
        DomainPhytoProductInputDto domainPhyto_herbi5 = testDatas.createDomainPhytoProductDto(herbi5, ProductType.BIOLOGICAL_FUNGICIDAL_SOILTREATMENT_MEANS, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        RefActaTraitementsProduit herbi_6400401 = refActaTraitementsProduitsTopiaDao.forNaturalId("3574", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhyto_herbi_6400401 = testDatas.createDomainPhytoProductDto(herbi_6400401, ProductType.HERBICIDAL, PhytoProductUnit.KG_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        final ArrayList<DomainInputDto> domainInputs = Lists.newArrayList(domainPhyto_herbi1, domainPhyto_herbi2, domainPhyto_herbi3, domainPhyto_herbi5, domainPhyto_herbi_6400401);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntries);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputs, pesristedCropResult);

        return baulon;
    }

    protected void testPour1ApplicationEnSynthetise(
            String usageId,
            double frequenceSpatiale,
            double frequenceTemporelle,
            double proportionSurfaceTraitee,
            RefActaTraitementsProduit produit,
            double quantite,
            String cropName,
            String codeGroupeCibleMaa,
            int campagne,
            String refDose,
            PhytoProductUnit refDoseUnit,
            PhytoProductUnit domainPhytoUnit,
            String expectedContent,
            AbstractIndicator indicator,
            Map<String, Double> indicatorExpectedValue,
            boolean shortCheck) throws IOException {

        createPraticedSystemWithUsageOfGivenProduct(
                campagne,
                usageId,
                produit,
                quantite,
                frequenceSpatiale,
                frequenceTemporelle,
                cropName,
                proportionSurfaceTraitee,
                codeGroupeCibleMaa);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(TestDatas.BAULON_TOPIA_ID),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);

        List<String> adventices = new ArrayList<>();
        if (StringUtils.isNotBlank(codeGroupeCibleMaa)) {
            Iterable<String> codeGroupeCibles = Splitter.on(";").trimResults().omitEmptyStrings().split(codeGroupeCibleMaa);
            codeGroupeCibles.forEach(c -> adventices.add("Adventices"));
        }

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicator);
        final String content = out.toString();
        for (Map.Entry<String, Double> indicatorToValue : indicatorExpectedValue.entrySet()) {
            if (!shortCheck) {
                assertThat(content).usingComparator(comparator).isEqualTo(String.format(
                        expectedContent,
                        usageId,
                        produit.getNom_produit(),
                        quantite,
                        AgrosystI18nService.getEnumTraduction(Locale.FRANCE, domainPhytoUnit),
                        String.join(", ", adventices),
                        campagne,
                        indicatorToValue.getKey(),
                        Double.valueOf(writer.getDf().format(indicatorToValue.getValue())),
                        produit.getNom_produit(),
                        produit.getNom_traitement(),
                        refDose,
                        AgrosystI18nService.getEnumTraduction(Locale.FRANCE, refDoseUnit)));
            } else {
                assertThat(content).usingComparator(comparator).isEqualTo(String.format(
                        expectedContent,
                        campagne,
                        indicatorToValue.getKey(),
                        Double.valueOf(writer.getDf().format(indicatorToValue.getValue())),
                        produit.getNom_produit(),
                        produit.getNom_traitement(),
                        refDose,
                        AgrosystI18nService.getEnumTraduction(Locale.FRANCE, refDoseUnit)));
            }
        }

    }

    private void createPraticedSystemWithUsageOfGivenProduct(
            int campagne,
            String usageId,
            RefActaTraitementsProduit produit,
            double quantite,
            double frequenceSpatiale,
            double frequenceTemporelle,
            String cropName,
            double treatedAreaPercent,
            String codeGroupeCibleMaa) throws IOException {

        Domain domain = createDomainSubSystem(campagne);

        final Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(domain);
        List<AbstractDomainInputStockUnit> domainPhytoInputs = domainInputStock.get(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        Map<RefActaTraitementsProduit, DomainPhytoProductInput> domainInputPhytoByRefInput =
                domainPhytoInputs.stream()
                        .map(dp -> ((DomainPhytoProductInput)dp))
                        .collect(Collectors.toMap(DomainPhytoProductInput::getRefInput, Function.identity()));

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null,"Systeme de culture Baulon 1");

        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(String.valueOf(campagne));
        ps = psdao.update(ps);

        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> cropByNames = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry crop = cropByNames.get(cropName);

        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );
        PracticedCropCycleNode nodeCulture_10_11_10 = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, crop.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeCulture_10_11_10);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection culture_10_11_10_Connection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeCulture_10_11_10,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeCulture_10_11_10,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);


        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, culture_10_11_10_Connection,
                PracticedIntervention.PROPERTY_NAME, "I-" + usageId,
                PracticedIntervention.PROPERTY_TOPIA_ID, "I-" + usageId,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, frequenceSpatiale,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, frequenceTemporelle);

        for (CroppingPlanSpecies species : CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies())) {
            final PracticedSpeciesStade speciesStade = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, species.getCode());
            intervention.addSpeciesStades(speciesStade);
        }
        practicedInterventionDao.update(intervention);

        final DomainPhytoProductInput domainPhytoProductInput = domainInputPhytoByRefInput.get(produit);

        Collection<PhytoProductTarget> phytoProductTargets = new ArrayList<>();
        if (StringUtils.isNotBlank(codeGroupeCibleMaa)) {
            Iterable<String> codeGroupeCibles = Splitter.on(";").trimResults().omitEmptyStrings().split(codeGroupeCibleMaa);
            for (String codeGroupCible : codeGroupeCibles) {
                PhytoProductTarget phytoProductTarget = phytoProductTargetDao.create(
                        PhytoProductTarget.PROPERTY_CODE_GROUPE_CIBLE_MAA, codeGroupCible,
                        PhytoProductTarget.PROPERTY_CATEGORY, BioAgressorType.MALADIE
                );
                phytoProductTargets.add(phytoProductTarget);
            }
        }

        final PesticideProductInputUsage inputUsage = pesticideProductInputUsageDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, usageId,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductInput,
                PesticideProductInputUsage.PROPERTY_TARGETS, phytoProductTargets,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, quantite
        );

        RefInterventionAgrosystTravailEDI actionSEX = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEX").findAny();
        pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, treatedAreaPercent,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(inputUsage)
        );
    }

    protected void testPour1ApplicationEnRealise(
            String usageId,
            double frequenceSpatiale,
            int nombreDePassage,
            double proportionSurfaceTraitee,
            RefActaTraitementsProduit produit,
            double quantite,
            String cropName,
            String codeGroupeCibleMaa,
            int campagne,
            String refDose,
            PhytoProductUnit refDoseUnit,
            PhytoProductUnit domainPhytoUnit,
            String expectedContent,
            AbstractIndicator indicator,
            Map<String, Double> indicatorExpectedValue,
            boolean shortCheck) throws IOException {

        createEffectiveZoneWithUsageOfGivenProduct(
                campagne,
                usageId,
                produit,
                quantite,
                frequenceSpatiale,
                nombreDePassage,
                cropName,
                proportionSurfaceTraitee,
                codeGroupeCibleMaa);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(false);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(TestDatas.BAULON_TOPIA_ID),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);

        List<String> adventices = new ArrayList<>();
        if (StringUtils.isNotBlank(codeGroupeCibleMaa)) {
            Iterable<String> codeGroupeCibles = Splitter.on(";").trimResults().omitEmptyStrings().split(codeGroupeCibleMaa);
            codeGroupeCibles.forEach(c -> adventices.add("Adventices"));
        }

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicator);
        final String content = out.toString();
        for (Map.Entry<String, Double> indicatorToValue : indicatorExpectedValue.entrySet()) {
            if (!shortCheck) {
                assertThat(content).usingComparator(comparator).isEqualTo(String.format(
                        expectedContent,
                        usageId,
                        campagne,
                        campagne,
                        produit.getNom_produit(),
                        quantite,
                        AgrosystI18nService.getEnumTraduction(Locale.FRANCE, domainPhytoUnit),
                        String.join(", ", adventices),
                        campagne,
                        indicatorToValue.getKey(),
                        Double.valueOf(writer.getDf().format(indicatorToValue.getValue())),
                        produit.getNom_produit(),
                        produit.getNom_traitement(),
                        refDose,
                        AgrosystI18nService.getEnumTraduction(Locale.FRANCE, refDoseUnit)));
            } else {
                assertThat(content).usingComparator(comparator).isEqualTo(String.format(
                        expectedContent,
                        campagne,
                        indicatorToValue.getKey(),
                        Double.valueOf(writer.getDf().format(indicatorToValue.getValue())),
                        produit.getNom_produit(),
                        produit.getNom_traitement(),
                        refDose,
                        AgrosystI18nService.getEnumTraduction(Locale.FRANCE, refDoseUnit)));
            }
        }

    }

    private void createEffectiveZoneWithUsageOfGivenProduct(
            int campagne,
            String usageId,
            RefActaTraitementsProduit produit,
            double quantite,
            double frequenceSpatiale,
            int nombreDePassage,
            String cropName,
            double treatedAreaPercent,
            String codeGroupeCibleMaa) throws IOException {

        Domain domain = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUniqueOrNull();
        if (domain == null) {
            domain = createDomainSubSystem(campagne);
        }

        final Map<InputType, List<AbstractDomainInputStockUnit>> domainInputStock = domainInputStockUnitService.loadDomainInputStock(domain);
        List<AbstractDomainInputStockUnit> domainPhytoInputs = domainInputStock.get(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        Map<RefActaTraitementsProduit, DomainPhytoProductInput> domainInputPhytoByRefInput =
                domainPhytoInputs.stream()
                        .map(dp -> ((DomainPhytoProductInput)dp))
                        .collect(Collectors.toMap(DomainPhytoProductInput::getRefInput, Function.identity()));

        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> cropByNames = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry crop = cropByNames.get(cropName);

        EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao = getPersistenceContext().getEffectiveSeasonalCropCycleDao();
        EffectiveInterventionTopiaDao effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeTopiaDao = getPersistenceContext().getEffectiveSpeciesStadeDao();

        Zone zone = zoneDao.forTopiaIdEquals("A_TOPIA_ID").findUnique();

        EffectiveCropCycleNode node = effectiveCropCycleNodeDao.create(
                EffectiveCropCycleNode.PROPERTY_RANK, 0,
                EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY, crop
        );

        List<EffectiveCropCycleNode> nodes = Lists.newArrayList(node);
        effectiveSeasonalCropCycleDao.create(
                EffectiveSeasonalCropCycle.PROPERTY_ZONE, zone,
                EffectiveSeasonalCropCycle.PROPERTY_NODES, nodes
        );

        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_TOPIA_ID,  "I-" + usageId + "-id",
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, node,
                EffectiveIntervention.PROPERTY_NAME, "I-" + usageId,
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.H_HA,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(campagne, 3, 4),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(campagne, 3, 4),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, nombreDePassage,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, frequenceSpatiale,
                EffectiveIntervention.PROPERTY_EDAPLOS_ISSUER_ID, "EDAPLOS_ID_1");


        for (CroppingPlanSpecies species : CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies())) {
            final EffectiveSpeciesStade speciesStade = effectiveSpeciesStadeTopiaDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, species);
            intervention.addSpeciesStades(speciesStade);
        }
        effectiveInterventionDao.update(intervention);

        final DomainPhytoProductInput domainPhytoProductInput = domainInputPhytoByRefInput.get(produit);

        Collection<PhytoProductTarget> phytoProductTargets = new ArrayList<>();
        if (StringUtils.isNotBlank(codeGroupeCibleMaa)) {
            Iterable<String> codeGroupeCibles = Splitter.on(";").trimResults().omitEmptyStrings().split(codeGroupeCibleMaa);
            for (String codeGroupCible : codeGroupeCibles) {
                PhytoProductTarget phytoProductTarget = phytoProductTargetDao.create(
                        PhytoProductTarget.PROPERTY_CODE_GROUPE_CIBLE_MAA, codeGroupCible,
                        PhytoProductTarget.PROPERTY_CATEGORY, BioAgressorType.MALADIE
                );
                phytoProductTargets.add(phytoProductTarget);
            }
        }

        final PesticideProductInputUsage inputUsage = pesticideProductInputUsageDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, usageId,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, domainPhytoProductInput,
                PesticideProductInputUsage.PROPERTY_TARGETS, phytoProductTargets,
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractInputUsage.PROPERTY_QT_AVG, quantite
        );

        RefInterventionAgrosystTravailEDI actionSEX = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEX").findAny();
        pesticidesSpreadingActionTopiaDao.create(
                PesticidesSpreadingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                PesticidesSpreadingAction.PROPERTY_MAIN_ACTION, actionSEX,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, treatedAreaPercent,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, Lists.newArrayList(inputUsage)
        );
    }

}
