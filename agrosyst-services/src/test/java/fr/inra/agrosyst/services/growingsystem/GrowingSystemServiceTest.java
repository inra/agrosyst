package fr.inra.agrosyst.services.growingsystem;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.MarketingDestinationObjective;
import fr.inra.agrosyst.api.entities.ProductionType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class GrowingSystemServiceTest extends AbstractAgrosystTest {

    protected GrowingSystemService service;
    protected GrowingPlanService gpService;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        service = serviceFactory.newService(GrowingSystemService.class);
        gpService = serviceFactory.newService(GrowingPlanService.class);
        
        TestDatas testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.createTestGrowingSystems();
        testDatas.importTypeAgriculture();

        loginAsAdmin();
        alterSchema();
    }

    /**
     * Test getFilteredGrowingPlans without and with some filters.
     */
    @Test
    public void getFilterGrowingSystems() {

        // null
        PaginationResult<GrowingSystem> result = service.getFilteredGrowingSystems(null);
        Assertions.assertEquals(7, result.getCount());

        // empty filter
        GrowingSystemFilter filter = new GrowingSystemFilter();
        result = service.getFilteredGrowingSystems(filter);
        Assertions.assertEquals(7, result.getCount());

        // name filter
        filter.setGrowingSystemName("Baulon");
        result = service.getFilteredGrowingSystems(filter);
        Assertions.assertEquals(2, result.getCount());
    }

    @Test
    public void duplicateGrowingSystem() {
        PaginationResult<GrowingSystem> result = service.getFilteredGrowingSystems(null);
        GrowingSystem gs0 = result.getElements().getFirst();
        ExtendContext extendContext = new ExtendContext(false);
        GrowingPlan growingPlan = gs0.getGrowingPlan();

        GrowingPlan clonedGrowingPlan = gpService.duplicateGrowingPlan(growingPlan.getTopiaId(),
                growingPlan.getDomain().getTopiaId(), true);

        GrowingSystem duplicatedGS = service.duplicateGrowingSystem(extendContext, clonedGrowingPlan, gs0);
        getCurrentTransaction().commit();
        Assertions.assertNotEquals(gs0.getTopiaId(), duplicatedGS.getTopiaId());
        Assertions.assertEquals(gs0.getCode(), duplicatedGS.getCode());

        Map<Sector, List<MarketingDestinationObjective>> objectives0 = service.loadMarketingDestinationObjective(gs0);
        Map<Sector, List<MarketingDestinationObjective>> duplicatedGSOobjectives = service.loadMarketingDestinationObjective(duplicatedGS);
        Assertions.assertEquals(objectives0.size(), duplicatedGSOobjectives.size());
        for (Sector sector : objectives0.keySet()) {
            List<MarketingDestinationObjective> objectives0MarketingDestinationObjectives = objectives0.get(sector);
            List<MarketingDestinationObjective> duplicatedGSOobjectivesMarketingDestinationObjectives = duplicatedGSOobjectives.get(sector);
            Assertions.assertEquals(objectives0MarketingDestinationObjectives.size(), duplicatedGSOobjectivesMarketingDestinationObjectives.size());
        }
    }

    @Test
    public void createOrUpdateMaraichageGrowingSystem() {
        GrowingSystem growingSystem = this.getMaraichageGrowingSystem();

        // valeur choisie arbirairement, l'objectif est qu'elle soit non nulle.
        growingSystem.setProduction(ProductionType.ABRIS);

        service.createOrUpdateGrowingSystem(growingSystem, null, null,
                getPersistenceContext().getRefTypeAgricultureDao().findAll().getFirst().getTopiaId(),
                Collections.emptyList(),
                service.loadMarketingDestinationObjective(growingSystem));
    }

    @Test
    public void createOrUpdateMaraichageGrowingSystemWithoutProduction() {
        GrowingSystem growingSystem = this.getMaraichageGrowingSystem();
        growingSystem.setProduction(null);

        Assertions.assertThrows(NullPointerException.class, () ->
            service.createOrUpdateGrowingSystem(growingSystem, null, null,
                    getPersistenceContext().getRefTypeAgricultureDao().findAll().getFirst().getTopiaId(),
                    Collections.emptyList(),
                    service.loadMarketingDestinationObjective(growingSystem))
        );
    }

    private GrowingSystem getMaraichageGrowingSystem() {
        final List<GrowingSystem> growingSystems = service.getFilteredGrowingSystems(null).getElements()
                .stream()
                .filter(gs -> gs.getSector() == Sector.MARAICHAGE)
                .toList();
        Assertions.assertFalse(growingSystems.isEmpty());

        final GrowingSystem growingSystem = growingSystems.getFirst();
        growingSystem.setActive(true);
        growingSystem.getGrowingPlan().setActive(true);
        growingSystem.getGrowingPlan().getDomain().setActive(true);
        growingSystem.setStartingDate(LocalDate.now());

        return growingSystem;
    }
}
