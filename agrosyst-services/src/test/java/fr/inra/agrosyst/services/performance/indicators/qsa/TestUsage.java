package fr.inra.agrosyst.services.performance.indicators.qsa;

import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;

public record TestUsage(RefActaTraitementsProduit produit,
                        DomainPhytoProductInputDto domainPhytoProduct,
                        Double quantite
                        ) {
}