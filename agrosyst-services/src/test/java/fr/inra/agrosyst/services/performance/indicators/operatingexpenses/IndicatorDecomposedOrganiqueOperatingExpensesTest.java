package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import static fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses.EPANDAGES_ORGANIQUES;
import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorDecomposedOrganiqueOperatingExpensesTest extends AbstractIndicatorDecomposedOperatingExpensesTest {
    private DomainOrganicProductInputTopiaDao organicProductInputDao;
    private OrganicProductInputUsageTopiaDao organicProductInputUsageDao;
    private OrganicFertilizersSpreadingActionTopiaDao organicFertilizersSpreadingActionDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        super.setupServices();
        organicProductInputDao = getPersistenceContext().getDomainOrganicProductInputDao();
        organicProductInputUsageDao = getPersistenceContext().getOrganicProductInputUsageDao();
        organicFertilizersSpreadingActionDao = getPersistenceContext().getOrganicFertilizersSpreadingActionDao();
    }

    @Test
    public void inraeEffectiveWithPriceTest()  throws IOException {
        inraeEffectiveTest(
                2.0d,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles épandage organique (€/ha);2013;Baulon;60.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, épandage organique (€/ha);2013;Baulon;15.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04/2013;03/04/2014;Épandage organique;Fumier de bovins (N: 5.267,P₂O₅: 2.3,K₂O: 8.267,CaO: ,MgO: ,S: );;null;2013;Indicateur économique;Charges opérationnelles réelles épandage organique (€/ha);60.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04/2013;03/04/2014;Épandage organique;Fumier de bovins (N: 5.267,P₂O₅: 2.3,K₂O: 8.267,CaO: ,MgO: ,S: );;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, épandage organique (€/ha);15.0;100;");
                });
    }

    @Test
    public void inraeEffectiveWithoutPriceTest()  throws IOException {
        inraeEffectiveTest(
                null,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles épandage organique (€/ha);2013;Baulon;15.0;75;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04/2013;03/04/2014;Épandage organique;Fumier de bovins (N: 5.267,P₂O₅: 2.3,K₂O: 8.267,CaO: ,MgO: ,S: );;null;2013;Indicateur économique;Charges opérationnelles réelles épandage organique (€/ha);15.0;50;Intrant 'Engrais/amendement organique (Fumier de bovins Epandage organique)' : Prix non renseigné;;");
                });
    }

    private void inraeEffectiveTest(Double price, Consumer<String> assertions) throws IOException {
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiOrga();
        testDatas.createRefInputUnitPriceUnitConverter();

        RefFertiOrgaTopiaDao refFertiOrgaDao = getPersistenceContext().getRefFertiOrgaDao();
        RefFertiOrga organicProduct = refFertiOrgaDao.forIdtypeeffluentEquals("SCA").findUnique();//"Fumier de bovins"

        RefInterventionAgrosystTravailEDI actionSEk = refInterventionAgrosystTravailEDIDao.forNaturalId("SEK").findUnique();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();
        int year = 2013;
        testDatas.createRefPrixFertiOrga(year, organicProduct.getIdtypeeffluent(), 0.5d);

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "intervention",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 4, 3),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign() + 1, 4, 3),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0d,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention.addSpeciesStades(intervention1Stades);

        String objectId = InputPriceService.GET_REF_FERTI_ORGA_OBJECT_ID.apply(organicProduct);
        var organicPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.ORGANIQUES_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Fumier de bovins",
                InputPrice.PROPERTY_PRICE, price
        );
        var organicInput = organicProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainOrganicProductInput.PROPERTY_INPUT_NAME, organicProduct.getLibelle(),
                DomainOrganicProductInput.PROPERTY_USAGE_UNIT, OrganicProductUnit.T_HA,
                DomainOrganicProductInput.PROPERTY_K2_O, 8.267,
                DomainOrganicProductInput.PROPERTY_P2_O5, 2.3,
                DomainOrganicProductInput.PROPERTY_N, 5.267,
                DomainOrganicProductInput.PROPERTY_REF_INPUT, organicProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, organicPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var organicUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, organicInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEk,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(EPANDAGES_ORGANIQUES));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertions.accept(content);
    }

    @Test
    public void inraePracticedTest() throws IOException {
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiOrga();
        testDatas.createRefInputUnitPriceUnitConverter();

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        RefFertiOrgaTopiaDao refFertiOrgaDao = getPersistenceContext().getRefFertiOrgaDao();
        RefFertiOrga organicProduct = refFertiOrgaDao.forIdtypeeffluentEquals("SCA").findUnique();//"Fumier de bovins"

        int year = 2013;
        testDatas.createRefPrixFertiOrga(year, organicProduct.getIdtypeeffluent(), 0.5d);

        RefInterventionAgrosystTravailEDI actionSEk = refInterventionAgrosystTravailEDIDao.forNaturalId("SEK").findUnique();

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100.0d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0d,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.0d,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(intervention1Stades);

        String objectId = InputPriceService.GET_REF_FERTI_ORGA_OBJECT_ID.apply(organicProduct);
        var organicPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domain,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.ORGANIQUES_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Fumier de bovins",
                InputPrice.PROPERTY_PRICE, 2.0d
        );
        var organicInput = organicProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                DomainOrganicProductInput.PROPERTY_INPUT_NAME, organicProduct.getLibelle(),
                DomainOrganicProductInput.PROPERTY_USAGE_UNIT, OrganicProductUnit.T_HA,
                DomainOrganicProductInput.PROPERTY_K2_O, 8.267,
                DomainOrganicProductInput.PROPERTY_P2_O5, 2.3,
                DomainOrganicProductInput.PROPERTY_N, 5.267,
                DomainOrganicProductInput.PROPERTY_REF_INPUT, organicProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, organicPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var organicUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, organicInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEk,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(EPANDAGES_ORGANIQUES));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles épandage organique (€/ha);2013, 2017;Baulon;60.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, épandage organique (€/ha);2013, 2017;Baulon;15.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04;03/04;Épandage organique;Fumier de bovins (N: 5.267,P₂O₅: 2.3,K₂O: 8.267,CaO: ,MgO: ,S: );;null;2013, 2017;Indicateur économique;Charges opérationnelles réelles épandage organique (€/ha);60.0;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04;03/04;Épandage organique;Fumier de bovins (N: 5.267,P₂O₅: 2.3,K₂O: 8.267,CaO: ,MgO: ,S: );;null;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, épandage organique (€/ha);15.0;100;;;");
    }
}
