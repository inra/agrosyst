package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.indicators.fertilization.AbstractIndicatorFertilizationTest;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorMineralFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorOrganicFertilization;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public class IndicatorCopperTotalProductTest extends AbstractIndicatorFertilizationTest {

    @Test
    public void testTotalFertilisationRealise_12394() {
        // Constitution du jeu de données de test
        List<DonneesTestFertilisationMineTotalRealise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorQsaCopperFertilisation = "QSA Cuivre total";

        {
            var pair = createSulfonitrate();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineTotalRealise(pair.getLeft(), pair.getRight(), 1500.0, 1.0, 1));
        }
        {
            var pair = createAmendementsEngraisAutres();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineTotalRealise(pair.getLeft(), pair.getRight(), 15.0, 1.0, 1));
        }
        {
            var pair = createSoufreMagnesie();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineTotalRealise(pair.getLeft(), pair.getRight(), 15.0, 5.0, 1));
        }

        for (DonneesTestFertilisationMineTotalRealise d : donneesTestFertilisation) {
            this.createMineralSpreadingEffectiveIntervention(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.transitCount()
            );
        }

        // Exécution des tests
        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorCopperFertilisationProduct indicatorCopperFertilisationProduct = serviceFactory.newInstance(IndicatorCopperFertilisationProduct.class);
        IndicatorCopperTotalProduct indicatorCopperTotalProduct = serviceFactory.newInstance(IndicatorCopperTotalProduct.class);
        this.testTotalRealise(
                "Substances actives",
                indicatorQsaCopperFertilisation,
                "0.0",
                indicatorMineralFertilization,
                indicatorCopperFertilisationProduct,
                indicatorCopperTotalProduct
        );
    }

    @Test
    public void testTotalEpandageOrganiqueRealise_12394() {
        final String indicatorQsaCuivreFertilisation = "QSA Cuivre total";

        List<DonneesTestFertilisationMineTotalRealise> donneesTestFertilisationMine = new LinkedList<>();

        {
            var pair = createMelangeGranulePK();
            donneesTestFertilisationMine.add(new DonneesTestFertilisationMineTotalRealise(pair.getLeft(), pair.getRight(), 15.0, 2.0, 1));
        }

        final List<DonneesTestFertilisationOrgaTotalRealise> donneesTestFertilisationOrga = new LinkedList<>();
        final List<TestDataRealise> testDatas = List.of(
                new TestDataRealise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 1, "91.5", "75.0", "105.0", "0.0", "0.0", "75.0")
        );
        for (TestDataRealise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S(),
                    DomainOrganicProductInput.PROPERTY_ORGANIC, false
            );

            donneesTestFertilisationOrga.add(new DonneesTestFertilisationOrgaTotalRealise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.transitCount()
            ));
        }

        for (DonneesTestFertilisationMineTotalRealise d : donneesTestFertilisationMine) {
            this.createMineralSpreadingEffectiveIntervention(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.transitCount()
            );
        }

        for (DonneesTestFertilisationOrgaTotalRealise d : donneesTestFertilisationOrga) {
            this.createOrganicFertilizerSpreadingEffectiveIntervention(
                    d.refFertiOrga(),
                    d.transitCount(),
                    d.spatialFrequency(),
                    d.qtAvg(),
                    d.domainOrganicProductInput()
            );
        }

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        IndicatorCopperFertilisationProduct indicatorCopperFertilisationProduct = serviceFactory.newInstance(IndicatorCopperFertilisationProduct.class);
        IndicatorCopperTotalProduct indicatorCopperTotalProduct = serviceFactory.newInstance(IndicatorCopperTotalProduct.class);
        this.testTotalRealise(
                "Substances actives",
                indicatorQsaCuivreFertilisation,
                "1.8",
                indicatorOrganicFertilization,
                indicatorMineralFertilization,
                indicatorCopperFertilisationProduct,
                indicatorCopperTotalProduct
        );
    }

    @Test
    public void testTotalFertilisationSynthetise_12394() throws IOException {
        // Constitution du jeu de données de test
        List<DonneesTestFertilisationMineTotalSynthetise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorQsaCuivreFertilisation = "QSA Cuivre total";

        {
            var pair = createAmmonitrate();
            final double qtAvg1 = 150.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineTotalSynthetise(pair.getLeft(), pair.getRight(), qtAvg1, 1.0, 1.0));
            final double qtAvg2 = 500.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineTotalSynthetise(pair.getLeft(), pair.getRight(), qtAvg2, 4.0, 1.0));
        }

        {
            var pair = createSulfonitrate();
            final double qtAvg = 1500.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineTotalSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 3.5, 1.0));
        }

        {
            var pair = createAmendementsEngraisAutres();
            final double qtAvg = 15.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineTotalSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 2.33, 2.0));
        }

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        PracticedCropCyclePhase phasePleineProduction = practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        for (DonneesTestFertilisationMineTotalSynthetise d : donneesTestFertilisation) {
            this.createMineralSpreadingPracticedInterventionTotal(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.temporalFrequency(),
                    bleConnection,
                    bleTendreHiver
            );
        }

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorCopperFertilisationProduct indicatorCopperFertilisationProduct = serviceFactory.newInstance(IndicatorCopperFertilisationProduct.class);
        IndicatorCopperTotalProduct indicatorCopperTotalProduct = serviceFactory.newInstance(IndicatorCopperTotalProduct.class);
        this.testTotalSynthetise(
                "Substances actives",
                indicatorQsaCuivreFertilisation,
                "0.0",
                indicatorMineralFertilization,
                indicatorCopperFertilisationProduct,
                indicatorCopperTotalProduct
        );
    }

    @Test
    public void testEpandageOrganiqueSynthetise_12394() throws IOException {
        final String indicatorQsaCuivreFertilisation = "QSA Cuivre total";

        List<DonneesTestFertilisationMineTotalSynthetise> donneesTestFertilisationMine = new LinkedList<>();

        {
            var pair = createAmmonitrate();
            final double qtAvg3 = 15.0;
            donneesTestFertilisationMine.add(new DonneesTestFertilisationMineTotalSynthetise(pair.getLeft(), pair.getRight(), qtAvg3, 2.0, 1.0));
        }

        {
            var pair = createSoufreMagnesie();
            final double qtAvg = 15.0;
            donneesTestFertilisationMine.add(new DonneesTestFertilisationMineTotalSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 1.66, 1.0));
        }

        {
            var pair = createMelangeGranulePK();
            final double qtAvg = 15.0;
            donneesTestFertilisationMine.add(new DonneesTestFertilisationMineTotalSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 0.33, 5.));
        }

        final List<DonneesTestFertilisationOrgaTotalSynthetise> donneesTestFertilisationOrga = new LinkedList<>();
        final List<TestDataSynthetise> testSynthDatas = List.of(
                new TestDataSynthetise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 2, "91.5", "75.0", "105.0", "0.0", "0.0", "75.0")
        );

        for (TestDataSynthetise testData : testSynthDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, ""
            );

            final String inputName = domainOrganicProductInput.getInputName();
            donneesTestFertilisationOrga.add(new DonneesTestFertilisationOrgaTotalSynthetise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.temporalFrequency(),
                    inputName
            ));
        }

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        for (DonneesTestFertilisationMineTotalSynthetise d : donneesTestFertilisationMine) {
            this.createMineralSpreadingPracticedInterventionTotal(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.temporalFrequency(),
                    bleConnection,
                    bleTendreHiver
            );
        }

        for (DonneesTestFertilisationOrgaTotalSynthetise d : donneesTestFertilisationOrga) {
            this.createOrganicFertilizerSpreadingPracticedInterventionTotal(
                    d.temporalFrequency(),
                    d.spatialFrequency(),
                    d.qtAvg(),
                    d.domainOrganicProductInput(),
                    bleConnection,
                    bleTendreHiver
            );
        }

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        IndicatorCopperFertilisationProduct indicatorCopperFertilisationProduct = serviceFactory.newInstance(IndicatorCopperFertilisationProduct.class);
        IndicatorCopperTotalProduct indicatorCopperTotalProduct = serviceFactory.newInstance(IndicatorCopperTotalProduct.class);
        this.testTotalSynthetise(
                "Substances actives",
                indicatorQsaCuivreFertilisation,
                "1.485",
                indicatorMineralFertilization,
                indicatorOrganicFertilization,
                indicatorCopperFertilisationProduct,
                indicatorCopperTotalProduct
        );
    }
}
