package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorHRI1_G2_Test extends AbstractQSATest {

    @Test
    public void testQuantitesSubstancesActivesRealise_12308() throws IOException {

        StringWriter out = launchPerformanceTest(IndicatorHRI1_G2.class, false);

        final String content = out.toString();

        // Vérifications indicateurs HRI1  du réalisé
        // I1 réalisé
        assertThat(content).usingComparator(comparator).isEqualTo(";PREDATOR (12.0 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;34.56;");//amm = 2000088
        // I2
        assertThat(content).usingComparator(comparator).isEqualTo(";DUAL GOLD SAFENEUR (2.1 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.378;");// amm = 9800259
        // I3
        assertThat(content).usingComparator(comparator).isEqualTo(";TOMENTAN (5.0 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;64.0;");// Prosulfocarbe/800g/l 2220302
        // I4 2130242
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 4;15/03/2013;15/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (2.5 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.0;");
        // I5
        assertThat(content).usingComparator(comparator).isEqualTo(";MASTOR WG (0.5 kg/ha);;non;2013;Substances actives;HRI-1 Groupe 2;2.0;");// amm = 2190597 Boscalid 500g/kg
        // I6 réalisé
        assertThat(content).usingComparator(comparator).isEqualTo(";VECALITEPI (0.63 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2;2.016;");//amm = 2160397
        // I-7
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 7;12/03/2013;12/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (null L/ha);;non;2013;Substances actives;HRI-1 Groupe 2;34.56;");
        // I-8 réalisé
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 8;13/03/2013;13/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (null L/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.378;");
        // I-9 réalisé
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 9;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (null L/ha);;N;;2013;Réalisé;Substances actives;HRI-1 Groupe 2;64.0;");
        // I-10 réalisé
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 10;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (null L/ha);;N;;2013;Réalisé;Substances actives;HRI-1 Groupe 2;0.0;");
        // I-11 réalisé
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 11;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (null kg/ha);;N;;2013;Réalisé;Substances actives;HRI-1 Groupe 2;2.0;");
        // I-12 réalisé
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 12;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (null L/ha);;N;;2013;Réalisé;Substances actives;HRI-1 Groupe 2;2.016;");
        // 13 TS 2090091
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 13;26/04/2013;26/04/2013;Semis;OTANA (0.2 kg/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 13;26/04/2013;26/04/2013;Semis;OTANA (0.2 kg/ha);;non;2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;100;;;");
        // 14 TS 2090091 pas de dose
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 14;26/04/2013;26/04/2013;Semis;OTANA (null kg/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.0;0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 14;26/04/2013;26/04/2013;Semis;OTANA (null kg/ha);;non;2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;");
        // 15 TS 0.2 l/ha
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 15;26/04/2013;26/04/2013;Semis;OTANA (0.2 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 15;26/04/2013;26/04/2013;Semis;OTANA (0.2 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;");
        // 16 TS 0.2 Unite/ha
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 16;26/04/2013;26/04/2013;Semis;OTANA (0.2 unité/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 16;26/04/2013;26/04/2013;Semis;OTANA (0.2 unité/ha);;non;2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;");
        // 17 TS 200 ml/ha
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 17;26/04/2013;26/04/2013;Semis;OTANA (200.0 mL/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 17;26/04/2013;26/04/2013;Semis;OTANA (200.0 mL/ha);;non;2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;");
        // 18
        assertThat(content).usingComparator(comparator).isEqualTo(";GAUCHO DUO FS (1.0 L/ha);;non;2013;Substances actives;HRI-1 Groupe 2;0.4;");// imidaclopride350g/l64 + prothioconazole 50g/l 8 => 2140039
    }

    @Test
    public void testQuantitesSubstancesActivesSynthetise_12308() throws IOException {

        StringWriter out = launchPerformanceTest(IndicatorHRI1_G2.class, true);
        final String content = out.toString();

        // Vérifications indicateur de QSA Totale
        // I1 2140086
        assertThat(content).usingComparator(comparator).isEqualTo("LONGRUN (3.0 kg/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;6.912;");
        // I2 2140086
        assertThat(content).usingComparator(comparator).isEqualTo("LONGRUN (3.0 L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;6.912;");
        // I3 2140086
        assertThat(content).usingComparator(comparator).isEqualTo("LONGRUN (3.0 unité/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;0.0;");
        // I4 2140086
        assertThat(content).usingComparator(comparator).isEqualTo("LONGRUN (3000.0 g/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;6.912;");
        // I5 8700118
        assertThat(content).usingComparator(comparator).isEqualTo("ATHLET (3.6 L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;2.88;");
        // I6 2130083
        assertThat(content).usingComparator(comparator).isEqualTo("TARAK (0.11 L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;0.0;");
        // I7 2140086
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention_7_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (null kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;6.912;");
        // I8 8700118
        assertThat(content).usingComparator(comparator).isEqualTo("ATHLET (null L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;2.88;");
        // I9 2130083
        assertThat(content).usingComparator(comparator).isEqualTo("TARAK (null L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;0.0;");
        // I10 2090050 TS
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention_10_ID;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention_10_ID;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;100;");
        // I11 2090050 TS
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention_11_ID;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention_11_ID;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;100;");
    }

    // I2 synthétisé
    @Test
    public void testQuantiteGlyphosateUniteCompatible_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();// AMM: 2140086
        DomainPhytoProductInputDto domainPhytoProductLongrunLHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitLongrun, domainPhytoProductLongrunLHa, 3.0, "HRI-1 Groupe 2;6.912;", 100.0, 25d);
    }

    // I3 synthétisé
    @Test
    public void testQuantiteGlyphosateUniteNonCompatible_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();// AMM: 2140086
        DomainPhytoProductInputDto domainPhytoProductLongrunUniteHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.UNITE_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitLongrun, domainPhytoProductLongrunUniteHa, 3.0, "HRI-1 Groupe 2;0.0;", 100.0, 25d);
    }

    // I4 synthétisé
    @Test
    public void testQuantiteGlyphosateUniteCompatibleDifferente_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();// AMM: 2140086
        DomainPhytoProductInputDto domainPhytoProductLongrunGHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.G_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitLongrun, domainPhytoProductLongrunGHa, 3000.0, "HRI-1 Groupe 2;6.912;", 100.0, 25d);
    }

    // I15
    @Test
    public void testQuantitethiamethoxamUniteCompatible_12311() throws IOException {
        RefActaTraitementsProduit produitTara25Wg = refActaTraitementsProduitsTopiaDao.forNaturalId("7976", 153, france).findUnique();// 2090091
        DomainPhytoProductInputDto domainPhytoProductTara25Wg = testDatas.createDomainPhytoProductDto(produitTara25Wg, ProductType.INSECTICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitTara25Wg, domainPhytoProductTara25Wg, 0.2, List.of("HRI-1 Groupe 2;0.0;"), 0.5, 1.0, 100.0, 25d, false);
    }

    //I16
    @Test
    public void testQuantitethiamethoxamUniteIncompatible_12311() throws IOException {
        RefActaTraitementsProduit produitTara25Wg = refActaTraitementsProduitsTopiaDao.forNaturalId("7976", 153, france).findUnique(); // 2090091
        DomainPhytoProductInputDto domainPhytoProductTara25Wg = testDatas.createDomainPhytoProductDto(produitTara25Wg, ProductType.INSECTICIDAL, PhytoProductUnit.UNITE_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitTara25Wg, domainPhytoProductTara25Wg, 0.2, List.of("HRI-1 Groupe 2;0.0;"), 0.5, 1.0, 100.0, 25d, false);
    }

    // I17
    @Test
    public void testQuantitethiamethoxamUniteCompatibleDifferente_12311() throws IOException {
        RefActaTraitementsProduit produitTara25Wg = refActaTraitementsProduitsTopiaDao.forNaturalId("7976", 153, france).findUnique();// 2090091
        DomainPhytoProductInputDto domainPhytoProductTara25Wg = testDatas.createDomainPhytoProductDto(produitTara25Wg, ProductType.INSECTICIDAL, PhytoProductUnit.ML_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitTara25Wg, domainPhytoProductTara25Wg, 200, List.of("HRI-1 Groupe 2;0.0;"), 0.5, 1.0, 100.0, 25d, false);
    }

    @Test
    public void testProduitHypothetique_avec_4_SA_nonTS() throws IOException {
        RefActaTraitementsProduit produitDegazonMouse = refActaTraitementsProduitsTopiaDao.forNaturalId("maaf_2000387", 147, france).findUnique();
        DomainPhytoProductInputDto domainDegazonMouse = testDatas.createDomainPhytoProductDto(produitDegazonMouse, ProductType.FUNGICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        String interventionExpectedResult_HTS = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;I_TEST_000;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DEGAZONMOUSSE (0.2 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.256;";
        String interventionExpectedResult = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;I_TEST_000;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DEGAZONMOUSSE (0.2 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;0.256;";
        List<String> expectedResults = List.of(interventionExpectedResult, interventionExpectedResult_HTS);
        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitDegazonMouse, domainDegazonMouse, 0.2d, expectedResults, 1.0, 1.0, 80.0d, 25d, false);
    }

    @Test
    public void testProduitHypothetique2_avec_4_SA_sans_TS() throws IOException {
        RefActaTraitementsProduit produitProfilerPrevasion = refActaTraitementsProduitsTopiaDao.forNaturalId("maaf_2100181", 140, france).findUnique();
        DomainPhytoProductInputDto domainProfilerPrevasion = testDatas.createDomainPhytoProductDto(produitProfilerPrevasion, ProductType.FUNGICIDAL, PhytoProductUnit.  G_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        String interventionExpectedResult_HTS = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;I_TEST_000;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREVASION (0.5 g/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.002;";
        String interventionExpectedResult = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;I_TEST_000;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREVASION (0.5 g/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;0.002;";
        List<String> expectedResults = List.of(interventionExpectedResult, interventionExpectedResult_HTS);

        this.testPour1ApplicationEnSynthetiseIndicatorHRI1_G2(produitProfilerPrevasion, domainProfilerPrevasion, 0.5d, expectedResults, 1.0, 1.0, 80.0d, 25d, false);
    }

    @Test
    public void testProduitsHypothetique_avec_4_SA_sans_TS() throws IOException {
        RefActaTraitementsProduit produitDegazonMousse = refActaTraitementsProduitsTopiaDao.forNaturalId("maaf_2000387", 147, france).findUnique();
        DomainPhytoProductInputDto domainProduitDegazonMousse = testDatas.createDomainPhytoProductDto(produitDegazonMousse, ProductType.FUNGICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        RefActaTraitementsProduit produitProfilerPrevasion = refActaTraitementsProduitsTopiaDao.forNaturalId("maaf_2100181", 140, france).findUnique();
        DomainPhytoProductInputDto domainProfilerPrevasion = testDatas.createDomainPhytoProductDto(produitProfilerPrevasion, ProductType.FUNGICIDAL, PhytoProductUnit.  G_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        String usageProduitDegazonMousseResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DEGAZONMOUSSE (0.2 L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;0.256;";
        String usage_HTS_ProduitDegazonMousseResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DEGAZONMOUSSE (0.2 L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.256;";
        String usageProfilerPrevasionResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREVASION (0.5 g/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;0.002;";
        String usage_HTS_ProfilerPrevasionResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREVASION (0.5 g/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.002;";
        String interventionResult = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;I_TEST_000;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREVASION (0.5 g/ha), DEGAZONMOUSSE (0.2 L/ha);2;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;0.258;";
        String interventionResult_HTS = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention;I_TEST_000;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREVASION (0.5 g/ha), DEGAZONMOUSSE (0.2 L/ha);2;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.258;";

        List<String> expectedResults = List.of(usageProduitDegazonMousseResult, usage_HTS_ProduitDegazonMousseResult, usageProfilerPrevasionResult, usage_HTS_ProfilerPrevasionResult, interventionResult, interventionResult_HTS);
        TestUsage usageProduitDegazonMousse = new TestUsage(produitDegazonMousse, domainProduitDegazonMousse, 0.2d);
        TestUsage usageProfilerPrevasion = new TestUsage(produitProfilerPrevasion, domainProfilerPrevasion, 0.5d);
        List<TestUsage> testUsages = List.of(usageProduitDegazonMousse, usageProfilerPrevasion);

        this.testPour1ApplicationPlusieursIntrantsSansTS_EnSynthetiseIndicatorHRI1_G2(testUsages, expectedResults, 1.0d, 1.0d, 80.0d, 25.0d);
    }

    @Test
    public void testProduitsHypothetique_avec_4_SA_avec_TS() throws IOException {
        RefActaTraitementsProduit produitDegazonMousse = refActaTraitementsProduitsTopiaDao.forNaturalId("maaf_2000387", 147, france).findUnique();
        DomainPhytoProductInputDto domainProduitDegazonMousse = testDatas.createDomainPhytoProductDto(produitDegazonMousse, ProductType.FUNGICIDAL, PhytoProductUnit.L_HA, null, InputType.TRAITEMENT_SEMENCE);
        RefActaTraitementsProduit produitProfilerPrevasion = refActaTraitementsProduitsTopiaDao.forNaturalId("maaf_2100181", 140, france).findUnique();
        DomainPhytoProductInputDto domainProfilerPrevasion = testDatas.createDomainPhytoProductDto(produitProfilerPrevasion, ProductType.FUNGICIDAL, PhytoProductUnit.  G_HA, null, InputType.TRAITEMENT_SEMENCE);

        String usageProduitDegazonMousseResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Semis;Intervention;03/04;03/04;Semis;DEGAZONMOUSSE (0.2 L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;0.32;";
        String usage_HTS_ProduitDegazonMousseResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Semis;Intervention;03/04;03/04;Semis;DEGAZONMOUSSE (0.2 L/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;";
        String usageProfilerPrevasionResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Semis;Intervention;03/04;03/04;Semis;PREVASION (0.5 g/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2;0.003;";
        String usage_HTS_ProfilerPrevasionResult = "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Semis;Intervention;03/04;03/04;Semis;PREVASION (0.5 g/ha);;non;2012, 2013;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;";
        String interventionResult = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Semis;Intervention;I_TEST_000;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2;0.323;";
        String interventionResult_HTS = "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Semis;Intervention;I_TEST_000;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;HRI-1 Groupe 2 hors traitement de semence;0.0;";

        CroppingPlanEntry croppingPlanEntry = getPersistenceContext().getCroppingPlanEntryDao().forTopiaIdEquals("ble-id").findUnique();
        List<Pair<RefActaTraitementsProduit , PhytoProductUnit>> productAndUnits = new ArrayList<>();
        productAndUnits.add(Pair.of(produitDegazonMousse, PhytoProductUnit.L_HA));
        productAndUnits.add(Pair.of(produitProfilerPrevasion, PhytoProductUnit.G_HA));

        TestUsage usageProduitDegazonMousse = new TestUsage(produitDegazonMousse, domainProduitDegazonMousse, 0.2d);
        TestUsage usageProfilerPrevasion = new TestUsage(produitProfilerPrevasion, domainProfilerPrevasion, 0.5d);
        List<TestUsage> testUsages = List.of(usageProduitDegazonMousse, usageProfilerPrevasion);

        DomainSeedLotInput domainSeedLot = testDatas.createDomainSeedLot(baulon, croppingPlanEntry, SeedPlantUnit.KG_PAR_HA, productAndUnits);
        Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput = domainSeedLot.getDomainSeedSpeciesInput();
        List<SeedSpeciesTestUsage> seedSpeciesTestUsages = new ArrayList<>();
        for (DomainSeedSpeciesInput seedSpeciesInput : domainSeedSpeciesInput) {
            SeedSpeciesTestUsage seedSpeciesTestUsage = new SeedSpeciesTestUsage(seedSpeciesInput.getSpeciesSeed(), seedSpeciesInput, 10.0, testUsages);
            seedSpeciesTestUsages.add(seedSpeciesTestUsage);
        }
        SeedSLotTestUsage seedSLotTestUsage = new SeedSLotTestUsage(croppingPlanEntry, domainSeedLot, seedSpeciesTestUsages);

        List<String> expectedResults = List.of(usageProduitDegazonMousseResult, usage_HTS_ProduitDegazonMousseResult, usageProfilerPrevasionResult, usage_HTS_ProfilerPrevasionResult, interventionResult, interventionResult_HTS);

        this.testPour1ApplicationPlusieursIntrantsAvecTS_EnSynthetiseIndicatorHRI1_G2(seedSLotTestUsage, expectedResults, 1.0d, 1.0d);
    }
}
