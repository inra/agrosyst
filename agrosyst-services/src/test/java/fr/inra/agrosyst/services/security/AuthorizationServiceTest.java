package fr.inra.agrosyst.services.security;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.ComputedUserPermissionTopiaDao;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.security.AgrosystAccessDeniedException;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.security.UserRoleDto;
import fr.inra.agrosyst.api.services.security.UserRoleEntityDto;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AuthorizationServiceTest extends AbstractAgrosystTest {

    protected UserService userService;
    protected BusinessAuthorizationService authorizationService;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected GrowingPlanTopiaDao growingPlanDao;
    protected ComputedUserPermissionTopiaDao computedUserPermissionDao;
    protected GrowingPlanService growingPlanService;
    protected DomainTopiaDao domainTopiaDao;

    protected String growingPlanCode;
    protected String growingPlanId;
    protected String growingSystemId;
    protected String domainId;

    protected AgrosystUser testUser;

    @BeforeEach
    public void setupServices() throws IOException {
        userService = serviceFactory.newService(UserService.class);
        authorizationService = serviceFactory.newService(BusinessAuthorizationService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        growingSystemDao = getPersistenceContext().getGrowingSystemDao();
        growingPlanDao = getPersistenceContext().getGrowingPlanDao();
        domainTopiaDao = getPersistenceContext().getDomainDao();
        computedUserPermissionDao = getPersistenceContext().getComputedUserPermissionDao();
        growingPlanService = serviceFactory.newService(GrowingPlanService.class);

        Domain domain = testDatas.createSingleTestDomain();
        GrowingSystem growingSystem = growingSystemDao.findAll().iterator().next();
        GrowingPlan growingPlan = growingSystem.getGrowingPlan();

        this.domainId = domain.getTopiaId();
        this.growingSystemId = growingSystem.getTopiaId();
        this.growingPlanId = growingPlan.getTopiaId();
        this.growingPlanCode = growingPlan.getCode();

        testUser = testDatas.createSingleTestUser();
        getConfig().setBusinessCachingEnabled(true);
    }


    @Test
    public void testGrowingPlanResponsible() {

        loginAsTest();

        try {
            authorizationService.checkDomainReadable(domainId);
            Assertions.fail("Should have failed");
        } catch (AgrosystAccessDeniedException aade) {
            // normal
        }

        loginAsAdmin();
        alterSchema();

        List<UserRoleDto> roles = new ArrayList<>();
        UserRoleDto userRoleDto = new UserRoleDto();
        roles.add(userRoleDto);
        userRoleDto.setType(RoleType.GROWING_PLAN_RESPONSIBLE);
        UserRoleEntityDto userRoleEntityDto = new UserRoleEntityDto();
        userRoleEntityDto.setIdentifier(growingPlanCode);
        userRoleDto.setEntity(userRoleEntityDto);

        authorizationService.saveUserRoles(testUser.getTopiaId(), roles);

        loginAsTest();

        authorizationService.checkDomainReadable(domainId);

        Assertions.assertTrue(authorizationService.isGrowingPlanWritable(growingPlanId));
        authorizationService.checkCreateOrUpdateGrowingPlan(growingPlanId);
        Assertions.assertTrue(authorizationService.isGrowingSystemWritable(growingSystemId));
        authorizationService.checkCreateOrUpdateGrowingSystem(growingSystemId);

        Assertions.assertFalse(authorizationService.isDomainWritable(domainId));
        try {
            authorizationService.checkCreateOrUpdateDomain(domainId);
            Assertions.fail("Should have failed");
        } catch (AgrosystAccessDeniedException aade) {
            // normal
        }

    }

    @Test
    public void testCreateGrowingPlan(){
        loginAsAdmin();
        alterSchema();

        Domain domain = domainTopiaDao.forTopiaIdEquals(domainId).findUnique();
        GrowingPlan growingPlan = growingPlanDao.newInstance();
        growingPlan.setName("TEST");
        growingPlan.setType(TypeDEPHY.DEPHY_EXPE);
        growingPlan.setDomain(domain);
    
        growingPlan = growingPlanService.createOrUpdateGrowingPlan(growingPlan);
        
        Assertions.assertNotNull(growingPlan);

    }

    protected void loginAsTest() {
        loginUser(testUser.getEmail(), "azerty");
    }

}
