package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2018 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainIrrigationInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainPotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.IrrigationUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.SeedPriceTopiaDao;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.UnitType;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.action.PotInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsage;
import fr.inra.agrosyst.api.entities.action.SubstrateInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterImpl;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPotTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMinTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrigTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhytoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixPotTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrateTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSubstrateTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import fr.inra.agrosyst.services.performance.indicators.IndicatorStandardisedGrossIncome;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorVintageTargetIFT;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static fr.inra.agrosyst.api.entities.action.SeedType.SEMENCES_CERTIFIEES;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by davidcosse on 16/07/18. Rewritten for local à intrant by ggley on 07/03/2024.
 */
public class IndicatorOperatingExpensesTest extends AbstractAgrosystTest {
    private PerformanceServiceImplForTest performanceService;
    private EffectiveCropCycleService effectiveCropCycleService;
    private DomainService domainService;
    protected ImportService importService;

    private ZoneTopiaDao zoneDao;
    private InputPriceTopiaDao priceDao;
    private SeedPriceTopiaDao seedPriceDao;
    private CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    private CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    private RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    private OrganicFertilizersSpreadingActionTopiaDao organicFertilizersSpreadingActionDao;
    private DomainOrganicProductInputTopiaDao organicProductInputDao;
    private OrganicProductInputUsageTopiaDao organicProductInputUsageDao;
    private DomainOtherInputTopiaDao otherInputDao;
    private OtherProductInputUsageTopiaDao otherProductInputUsageDao;
    private DomainIrrigationInputTopiaDao irrigationInputDao;
    private IrrigationInputUsageTopiaDao irrigationInputUsageDao;
    private DomainMineralProductInputTopiaDao mineralProductInputDao;
    private MineralProductInputUsageTopiaDao mineralProductInputUsageDao;
    private DomainPhytoProductInputTopiaDao phytoProductInputDao;
    private PesticideProductInputUsageTopiaDao pesticideProductInputUsageDao;
    private SeedProductInputUsageTopiaDao seedProductInputUsageDao;
    private DomainSeedLotInputTopiaDao seedLotInputDao;
    private SeedLotInputUsageTopiaDao seedLotInputUsageDao;
    private DomainSeedSpeciesInputTopiaDao seedSpeciesInputDao;
    private SeedSpeciesInputUsageTopiaDao seedSpeciesInputUsageDao;
    private DomainSubstrateInputTopiaDao substrateInputDao;
    private SubstrateInputUsageTopiaDao substrateInputUsageDao;
    private DomainPotInputTopiaDao potInputDao;
    private PotInputUsageTopiaDao potInputUsageDao;
    private EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDAO;
    private EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    private EffectiveInterventionTopiaDao effectiveInterventionDao;
    private EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;
    private PracticedCropCyclePhaseTopiaDao practicedCropCyclePhaseDAO;
    private PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao;
    private PracticedInterventionTopiaDao practicedInterventionDao;
    private PracticedSpeciesStadeTopiaDao practicedSpeciesStadeDao;
    private GrowingSystemTopiaDao growingSystemDao;
    private PracticedSystemTopiaDao practicedSystemDao;
    private RefEspeceTopiaDao refEspeceDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);
        domainService = serviceFactory.newService(DomainService.class);
        importService = serviceFactory.newService(ImportService.class);

        loginAsAdmin();
        alterSchema();

        zoneDao = getPersistenceContext().getZoneDao();
        priceDao = getPersistenceContext().getInputPriceDao();
        seedPriceDao = getPersistenceContext().getSeedPriceDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        organicFertilizersSpreadingActionDao = getPersistenceContext().getOrganicFertilizersSpreadingActionDao();
        organicProductInputDao = getPersistenceContext().getDomainOrganicProductInputDao();
        organicProductInputUsageDao = getPersistenceContext().getOrganicProductInputUsageDao();
        otherInputDao = getPersistenceContext().getDomainOtherInputDao();
        otherProductInputUsageDao = getPersistenceContext().getOtherProductInputUsageDao();
        irrigationInputDao = getPersistenceContext().getDomainIrrigationInputDao();
        irrigationInputUsageDao = getPersistenceContext().getIrrigationInputUsageDao();
        mineralProductInputDao = getPersistenceContext().getDomainMineralProductInputDao();
        mineralProductInputUsageDao = getPersistenceContext().getMineralProductInputUsageDao();
        phytoProductInputDao = getPersistenceContext().getDomainPhytoProductInputDao();
        pesticideProductInputUsageDao = getPersistenceContext().getPesticideProductInputUsageDao();
        seedProductInputUsageDao = getPersistenceContext().getSeedProductInputUsageDao();
        seedLotInputDao = getPersistenceContext().getDomainSeedLotInputDao();
        seedLotInputUsageDao = getPersistenceContext().getSeedLotInputUsageDao();
        seedSpeciesInputDao = getPersistenceContext().getDomainSeedSpeciesInputDao();
        seedSpeciesInputUsageDao = getPersistenceContext().getSeedSpeciesInputUsageDao();
        substrateInputDao = getPersistenceContext().getDomainSubstrateInputDao();
        substrateInputUsageDao = getPersistenceContext().getSubstrateInputUsageDao();
        potInputDao = getPersistenceContext().getDomainPotInputDao();
        potInputUsageDao = getPersistenceContext().getPotInputUsageDao();
        effectiveCropCyclePhaseDAO = getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = getPersistenceContext().getEffectivePerennialCropCycleDao();
        effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        effectiveSpeciesStadeDao = getPersistenceContext().getEffectiveSpeciesStadeDao();
        testDatas = serviceFactory.newInstance(TestDatas.class);

        practicedCropCyclePhaseDAO = getPersistenceContext().getPracticedCropCyclePhaseDao();
        practicedPerennialCropCycleDao = getPersistenceContext().getPracticedPerennialCropCycleDao();
        practicedInterventionDao = getPersistenceContext().getPracticedInterventionDao();
        practicedSpeciesStadeDao = getPersistenceContext().getPracticedSpeciesStadeDao();
        growingSystemDao = getPersistenceContext().getGrowingSystemDao();
        practicedSystemDao = getPersistenceContext().getPracticedSystemDao();
        refEspeceDao = getPersistenceContext().getRefEspeceDao();

        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();
    }

    @Test
    public void inraeEffectiveFertiOrgaTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiOrga();

        RefFertiOrgaTopiaDao refFertiOrgaDao = getPersistenceContext().getRefFertiOrgaDao();
        RefFertiOrga organicProduct = refFertiOrgaDao.forIdtypeeffluentEquals("SCA").findUnique();//"Fumier de bovins"
        RefOtherInputTopiaDao refOtherInputDao = getPersistenceContext().getRefOtherInputDao();
        if (refOtherInputDao.count() == 0) {
            testDatas.importRefOtherInput();
        }
        RefOtherInput otherProduct = refOtherInputDao.findAll().getFirst();

        testDatas.createRefInputUnitPriceUnitConverter();
        int year = 2016;
        testDatas.createRefPrixFertiOrga(year, organicProduct.getIdtypeeffluent(), 0.0d);

        // Epandage organique
        RefInterventionAgrosystTravailEDI actionSEk = refInterventionAgrosystTravailEDIDao.forNaturalId("SEK").findUnique();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i1 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(year, 11, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(year + 1, 2, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 2.0
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i1.addSpeciesStades(intervention1Stades);

        var organicPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, organicProduct.getIdtypeeffluent(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.ORGANIQUES_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Fumier de bovins",
                InputPrice.PROPERTY_PRICE, 2d
        );
        var organicInput = organicProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainOrganicProductInput.PROPERTY_INPUT_NAME, organicProduct.getLibelle(),
                DomainOrganicProductInput.PROPERTY_USAGE_UNIT, OrganicProductUnit.T_HA,
                DomainOrganicProductInput.PROPERTY_K2_O, 8.267,
                DomainOrganicProductInput.PROPERTY_P2_O5, 2.3,
                DomainOrganicProductInput.PROPERTY_N, 5.267,
                DomainOrganicProductInput.PROPERTY_REF_INPUT, organicProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, organicPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var organicUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, organicInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        var otherPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, organicProduct.getIdtypeeffluent(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.OTHER_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Autre Fumier de bovins",
                InputPrice.PROPERTY_PRICE, 2d
        );
        var otherInput = otherInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.AUTRE,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Autre Fumier de bovins",
                DomainOtherInput.PROPERTY_USAGE_UNIT, OtherProductInputUnit.T_HA,
                DomainOtherInput.PROPERTY_REF_INPUT, otherProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, otherPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var otherUsage = otherProductInputUsageDao.create(
                OtherProductInputUsage.PROPERTY_INPUT_TYPE, InputType.AUTRE,
                OtherProductInputUsage.PROPERTY_DOMAIN_OTHER_INPUT, otherInput,
                OtherProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i1,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEk,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, List.of(otherUsage)
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;120.0;");
    }

    @Test
    public void inraeEffectiveFertiOrgaWithMissingDataTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiOrga();

        RefFertiOrgaTopiaDao refFertiOrgaDao = getPersistenceContext().getRefFertiOrgaDao();
        RefFertiOrga organicProduct = refFertiOrgaDao.forIdtypeeffluentEquals("SCA").findUnique();//"Fumier de bovins"
        RefOtherInputTopiaDao refOtherInputDao = getPersistenceContext().getRefOtherInputDao();
        if (refOtherInputDao.count() == 0) {
            testDatas.importRefOtherInput();
        }
        RefOtherInput otherProduct = refOtherInputDao.findAll().getFirst();

        testDatas.createRefInputUnitPriceUnitConverter();
        int year = 2016;
        testDatas.createRefPrixFertiOrga(year, organicProduct.getIdtypeeffluent(), 0.0d);

        // Epandage organique
        RefInterventionAgrosystTravailEDI actionSEk = refInterventionAgrosystTravailEDIDao.forNaturalId("SEK").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i1 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(year, 11, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(year + 1, 2, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 2.0
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i1.addSpeciesStades(intervention1Stades);

        var organicPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, organicProduct.getIdtypeeffluent(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.ORGANIQUES_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Fumier de bovins",
                InputPrice.PROPERTY_PRICE, null
        );
        var organicInput = organicProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainOrganicProductInput.PROPERTY_INPUT_NAME, organicProduct.getLibelle(),
                DomainOrganicProductInput.PROPERTY_USAGE_UNIT, OrganicProductUnit.T_HA,
                DomainOrganicProductInput.PROPERTY_K2_O, 8.267,
                DomainOrganicProductInput.PROPERTY_P2_O5, 2.3,
                DomainOrganicProductInput.PROPERTY_N, 5.267,
                DomainOrganicProductInput.PROPERTY_REF_INPUT, organicProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, organicPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var organicUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, organicInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        var otherPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, organicProduct.getIdtypeeffluent(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.OTHER_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Autre Fumier de bovins",
                InputPrice.PROPERTY_PRICE, 2d
        );
        var otherInput = otherInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.AUTRE,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Autre Fumier de bovins",
                DomainOtherInput.PROPERTY_USAGE_UNIT, OtherProductInputUnit.T_HA,
                DomainOtherInput.PROPERTY_REF_INPUT, otherProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, otherPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var otherUsage = otherProductInputUsageDao.create(
                OtherProductInputUsage.PROPERTY_INPUT_TYPE, InputType.AUTRE,
                OtherProductInputUsage.PROPERTY_DOMAIN_OTHER_INPUT, otherInput,
                OtherProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i1,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEk,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, List.of(otherUsage)
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;0.0;75;Donnée référentiel manquante : taux de conversion manquant pour l'unité de prix 'EURO_T' avec l'unité d'utilisation 'T_HA'.;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;i1;01/11/2016;01/02/2017;Épandage organique;Conditionnement - barquette - fraise - Autre Fumier de bovins;;null;2013;Indicateur économique;Charges opérationnelles réelles (€/ha);0.0;100;;;");
    }

    protected IndicatorFilter createOperatingExpenseFilter() {
        IndicatorFilter indicatorOperatingExpensesFilter = new IndicatorFilterImpl();
        indicatorOperatingExpensesFilter.setClazz(IndicatorOperatingExpenses.class.getSimpleName());
        indicatorOperatingExpensesFilter.setComputeReal(true);
        indicatorOperatingExpensesFilter.setComputeStandardized(true);
        return indicatorOperatingExpensesFilter;
    }

    @Test
    public void inraePracticedFertiOrgaTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiOrga();

        RefFertiOrgaTopiaDao refFertiOrgaDao = getPersistenceContext().getRefFertiOrgaDao();
        RefFertiOrga organicProduct = refFertiOrgaDao.forIdtypeeffluentEquals("SCA").findUnique();//"Fumier de bovins"

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        testDatas.createRefInputUnitPriceUnitConverter();
        int year = domain.getCampaign();
        testDatas.createRefPrixFertiOrga(year, organicProduct.getIdtypeeffluent(), 0.0d);

        // Epandage organique
        RefInterventionAgrosystTravailEDI actionSEk = refInterventionAgrosystTravailEDIDao.forNaturalId("SEK").findUnique();

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDAO.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        //psci = intervention.getTemporalFrequency() * intervention.getSpatialFrequency();
        PracticedIntervention i1 = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "01/11",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "01/11",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        i1.addSpeciesStades(intervention1Stades);

        var organicPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, organicProduct.getIdtypeeffluent(),
                InputPrice.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.ORGANIQUES_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Fumier de bovins",
                InputPrice.PROPERTY_PRICE, 2d
        );
        var organicInput = organicProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                DomainOrganicProductInput.PROPERTY_INPUT_NAME, organicProduct.getLibelle(),
                DomainOrganicProductInput.PROPERTY_USAGE_UNIT, OrganicProductUnit.T_HA,
                DomainOrganicProductInput.PROPERTY_K2_O, 8.267,
                DomainOrganicProductInput.PROPERTY_P2_O5, 2.3,
                DomainOrganicProductInput.PROPERTY_N, 5.267,
                DomainOrganicProductInput.PROPERTY_REF_INPUT, organicProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, organicPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var organicUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, organicInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, i1,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEk,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute practiced
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013, 2017;Baulon;60.0;100;;");
    }

    @Test
    public void inraePracticedFertiOrgaWithMissingDataTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiOrga();

        RefFertiOrgaTopiaDao refFertiOrgaDao = getPersistenceContext().getRefFertiOrgaDao();
        RefFertiOrga organicProduct = refFertiOrgaDao.forIdtypeeffluentEquals("SCA").findUnique();//"Fumier de bovins"

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        testDatas.createRefInputUnitPriceUnitConverter();
        int year = domain.getCampaign();
        testDatas.createRefPrixFertiOrga(year, organicProduct.getIdtypeeffluent(), 0.0d);

        // Epandage organique
        RefInterventionAgrosystTravailEDI actionSEk = refInterventionAgrosystTravailEDIDao.forNaturalId("SEK").findUnique();


        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDAO.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        //psci = intervention.getTemporalFrequency() * intervention.getSpatialFrequency();
        PracticedIntervention i1 = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "01/11",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "01/11",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        i1.addSpeciesStades(intervention1Stades);

        var organicPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, organicProduct.getIdtypeeffluent(),
                InputPrice.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.ORGANIQUES_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Fumier de bovins",
                InputPrice.PROPERTY_PRICE, null
        );
        var organicInput = organicProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                DomainOrganicProductInput.PROPERTY_INPUT_NAME, organicProduct.getLibelle(),
                DomainOrganicProductInput.PROPERTY_USAGE_UNIT, OrganicProductUnit.T_HA,
                DomainOrganicProductInput.PROPERTY_K2_O, 8.267,
                DomainOrganicProductInput.PROPERTY_P2_O5, 2.3,
                DomainOrganicProductInput.PROPERTY_N, 5.267,
                DomainOrganicProductInput.PROPERTY_REF_INPUT, organicProduct,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, organicPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var organicUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, organicInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, 30.0
        );

        organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, i1,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEk,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute practiced
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        // (one input and) one price are missing
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013, 2017;Baulon;0.0;83;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;i1;01/11;01/11;Épandage organique;Fumier de bovins (N: 5.267,P₂O₅: 2.3,K₂O: 8.267,CaO: ,MgO: ,S: );;null;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);0.0;50;Intrant 'Engrais/amendement organique (Fumier de bovins Epandage organique)' : Prix non renseigné;;");
    }

    @Test
    public void inraeEffectiveIrrigTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        testDatas.createRefInputUnitPriceUnitConverter();

        // Irrigation
        RefInterventionAgrosystTravailEDI actionIrrigaction = refInterventionAgrosystTravailEDIDao.forNaturalId("SEO").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixIrrig(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i1 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.IRRIGATION,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 4, 3),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign() + 1, 4, 3),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i1.addSpeciesStades(intervention1Stades);

        String objectId = InputPriceService.GET_IRRIGATION_OBJECT_ID.apply(zpPlotBaulon1.getPlot().getDomain().getCampaign());
        var irrigationPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.IRRIGATION_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Eau",
                InputPrice.PROPERTY_PRICE, null
        );
        var irrigationInput = irrigationInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainIrrigationInput.PROPERTY_INPUT_NAME, "Eau",
                DomainIrrigationInput.PROPERTY_USAGE_UNIT, IrrigationUnit.MM,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, irrigationPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var irrigationUsage = irrigationInputUsageDao.create(
                IrrigationInputUsage.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT, irrigationInput,
                IrrigationInputUsage.PROPERTY_QT_AVG, 50.d
        );

        IrrigationActionTopiaDao irrigActionDao = getPersistenceContext().getIrrigationActionDao();
        irrigActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i1,
                AbstractAction.PROPERTY_MAIN_ACTION, actionIrrigaction,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, 50.d,
                IrrigationAction.PROPERTY_IRRIGATION_INPUT_USAGE, irrigationUsage,
                IrrigationAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();


        // Un prix est manquant sur les 4 données vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;40.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Irrigation;i1;03/04/2013;03/04/2014;Irrigation;N/A;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);40.0;100;;;");
    }

    @Test
    public void inraePracticedIrrigTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixIrrig(domain.getCampaign());

        // Epandage organique
        RefInterventionAgrosystTravailEDI actionIrrigaction = refInterventionAgrosystTravailEDIDao.forNaturalId("SEO").findUnique();

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDAO.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        //psci = intervention.getTemporalFrequency() * intervention.getSpatialFrequency();
        PracticedIntervention i8 = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        i8.addSpeciesStades(intervention1Stades);

        String objectId = InputPriceService.GET_IRRIGATION_OBJECT_ID.apply(baulon1.getGrowingPlan().getDomain().getCampaign());
        var irrigationPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.IRRIGATION_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Eau",
                InputPrice.PROPERTY_PRICE, null
        );
        var irrigationInput = irrigationInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                DomainIrrigationInput.PROPERTY_INPUT_NAME, "Eau",
                DomainIrrigationInput.PROPERTY_USAGE_UNIT, IrrigationUnit.MM,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, irrigationPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var irrigationUsage = irrigationInputUsageDao.create(
                IrrigationInputUsage.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT, irrigationInput,
                IrrigationInputUsage.PROPERTY_QT_AVG, 50.d
        );

        IrrigationActionTopiaDao irrigActionDao = getPersistenceContext().getIrrigationActionDao();
        irrigActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, i8,
                AbstractAction.PROPERTY_MAIN_ACTION, actionIrrigaction,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, 50.d,
                IrrigationAction.PROPERTY_IRRIGATION_INPUT_USAGE, irrigationUsage,
                IrrigationAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013, 2017;Baulon;40.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;i1;03/04;03/04;Irrigation;N/A;;null;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);40.0;100;;;");
    }

    @Test
    public void inraePracticedIrrigWithMissingPriceTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixIrrig(domain.getCampaign());

        // Epandage organique
        RefInterventionAgrosystTravailEDI actionIrrigaction = refInterventionAgrosystTravailEDIDao.forNaturalId("SEO").findUnique();

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDAO.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        //psci = intervention.getTemporalFrequency() * intervention.getSpatialFrequency();
        PracticedIntervention i8 = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        i8.addSpeciesStades(intervention1Stades);

        var irrigationPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, "Eau",
                InputPrice.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.IRRIGATION_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Eau",
                InputPrice.PROPERTY_PRICE, null
        );
        var irrigationInput = irrigationInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                DomainIrrigationInput.PROPERTY_INPUT_NAME, "Eau",
                DomainIrrigationInput.PROPERTY_USAGE_UNIT, IrrigationUnit.MM,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, irrigationPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var irrigationUsage = irrigationInputUsageDao.create(
                IrrigationInputUsage.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT, irrigationInput,
                IrrigationInputUsage.PROPERTY_QT_AVG, 50.d
        );

        IrrigationActionTopiaDao irrigActionDao = getPersistenceContext().getIrrigationActionDao();
        irrigActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, i8,
                AbstractAction.PROPERTY_MAIN_ACTION, actionIrrigaction,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, 50.d,
                IrrigationAction.PROPERTY_IRRIGATION_INPUT_USAGE, irrigationUsage,
                IrrigationAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // Un prix est manquant sur les 4 données vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013, 2017;Baulon;40.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;i1;03/04;03/04;Irrigation;N/A;;null;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);40.0;100;;;");
    }

    @Test
    public void inraeEffectiveMineralFertiliserTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        testDatas.createRefInputUnitPriceUnitConverter();

        // Irrigation
        RefInterventionAgrosystTravailEDI actionMineral = refInterventionAgrosystTravailEDIDao.forNaturalId("SEM").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixFertiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i4 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.8
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i4.addSpeciesStades(intervention1Stades);

        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Compacté",
                RefFertiMinUNIFA.PROPERTY_CATEG, 442,
                RefFertiMinUNIFA.PROPERTY_K2_O, 40.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 6.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 12.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT."
        );

        String objectId = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(mineralProduct);
        var mineralPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. K2O(40%), MgO(6%), SO3(12%)",
                InputPrice.PROPERTY_PRICE, 400.0
        );
        var mineralInput = mineralProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainMineralProductInput.PROPERTY_INPUT_NAME, mineralProduct.getType_produit(),
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.T_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, mineralPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var mineralUsage = mineralProductInputUsageDao.create(
                MineralProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, mineralInput,
                MineralProductInputUsage.PROPERTY_QT_AVG, 2d
        );

        MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao = getPersistenceContext().getMineralFertilizersSpreadingActionDao();
        mineralFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i4,
                AbstractAction.PROPERTY_MAIN_ACTION, actionMineral,
                MineralFertilizersSpreadingAction.PROPERTY_MINERAL_PRODUCT_INPUT_USAGES, List.of(mineralUsage),
                MineralFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;640.0;100;;");
    }

    @Test
    public void inraeEffectiveMineralFertiliserWithMissingPriceTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        testDatas.createRefInputUnitPriceUnitConverter();

        // Irrigation
        RefInterventionAgrosystTravailEDI actionMineral = refInterventionAgrosystTravailEDIDao.forNaturalId("SEM").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixFertiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i4 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.8
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i4.addSpeciesStades(intervention1Stades);

        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Compacté",
                RefFertiMinUNIFA.PROPERTY_CATEG, 442,
                RefFertiMinUNIFA.PROPERTY_K2_O, 40.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 6.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 12.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT."
        );

        String objectId = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(mineralProduct);
        var mineralPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. K2O(40%), MgO(6%), SO3(12%)",
                InputPrice.PROPERTY_PRICE, null
        );
        var mineralInput = mineralProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainMineralProductInput.PROPERTY_INPUT_NAME, mineralProduct.getType_produit(),
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.T_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, mineralPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var mineralUsage = mineralProductInputUsageDao.create(
                MineralProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, mineralInput,
                MineralProductInputUsage.PROPERTY_QT_AVG, 2d
        );

        MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao = getPersistenceContext().getMineralFertilizersSpreadingActionDao();
        mineralFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i4,
                AbstractAction.PROPERTY_MAIN_ACTION, actionMineral,
                MineralFertilizersSpreadingAction.PROPERTY_MINERAL_PRODUCT_INPUT_USAGES, List.of(mineralUsage),
                MineralFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // Un prix est manquant sur les 4 données vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;0.07424;83;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Application de produits minéraux;i1;01/06/2013;01/06/2013;Application de produits minéraux;AMENDEMENTS-ENGRAIS SIDERURGIQUES PHOSPHOPOT. (2.0 t/ha);;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);0.07424;50;Intrant 'Engrais/amendement (organo)minéral (categ=442 forme=Compacte n=0.000000 p2O5=0.000000 k2O=40.000000 bore=0.000000 calcium=0.000000 fer=0.000000 manganese=0.000000 molybdene=0.000000 mgO=6.000000 oxyde_de_sodium=0.000000 sO3=12.000000 cuivre=0.000000 zinc=0.000000 Fertilisation minérale)' : Prix non renseigné;;");
    }

    @Test
    public void inraeEffectivePhytoProductTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        final RefCountry france = testDatas.getDefaultFranceCountry();
        testDatas.importActaTraitementsProduits();

        testDatas.createRefInputUnitPriceUnitConverter();

        // Irrigation
        RefInterventionAgrosystTravailEDI phytoProductAgrosystIntervention = refInterventionAgrosystTravailEDIDao.forNaturalId("SEX").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixPesticidiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i7 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Application de produits avec AMM / Traitement phytosanitaire ",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 7, 15),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 7, 15),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.05
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i7.addSpeciesStades(intervention1Stades);

        RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        RefActaTraitementsProduit metarexIno = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Divers - Molluscicides",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "METAREX INO",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "D5",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "D5",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "5680",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 121, //metarexIno 5680_121
                RefActaTraitementsProduit.PROPERTY_CODE__AMM, "2130075"
        );

        RefActaTraitementsProduit betapost = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Herbicide - Inconnu",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "BETAPOST",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 999900, //betapost a_AAADA_999900
                RefActaTraitementsProduit.PROPERTY_CODE__AMM, "9000858"
        );

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(metarexIno);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Divers - Molluscicides",
                InputPrice.PROPERTY_PRICE, 8.0
        );
        var phytoInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Divers - Molluscicides",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.KG_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, metarexIno,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, DomainInputStockUnitService.getPhytoInputKey(metarexIno, PhytoProductUnit.KG_HA)
        );
        var pesticideUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 10.0
        );

        objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(betapost);
        var phytoPrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Herbicide - Inconnu",
                InputPrice.PROPERTY_PRICE, 220.0
        );
        var phytoInput2 = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Herbicide - Inconnu",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, betapost,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage2 = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput2,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 2.0
        );

        PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao = getPersistenceContext().getPesticidesSpreadingActionDao();
        pesticidesSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i7,
                AbstractAction.PROPERTY_MAIN_ACTION, phytoProductAgrosystIntervention,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 0.1,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, List.of(pesticideUsage, pesticideUsage2),
                PesticidesSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Application de produits avec AMM / Traitement phytosanitaire ;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;26.0;100;");


        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refActaTraitementsProduitsCateg.csv")) {
            importService.importActaTraitementsProduitsCateg(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refCompositionSubstancesActivesParNumeroAMM.csv")) {
            importService.importRefCompositionSubstancesActivesParNumeroAMM(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refSubstancesActivesCommissionEuropeenne.csv")) {
            importService.importRefSubstancesActivesCommissionEuropeenneCSV(stream);
        }

        // calcul avec dose de référence
        pesticideUsage.setQtAvg(null);
        pesticideUsage2.setQtAvg(null);
        pesticideProductInputUsageDao.update(pesticideUsage);
        pesticideProductInputUsageDao.update(pesticideUsage2);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Application de produits avec AMM / Traitement phytosanitaire ;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;26.0;");

    }

    @Test
    public void inraeEffectivePhytoProductWithMissingPriceTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        final RefCountry france = testDatas.getDefaultFranceCountry();
        testDatas.importActaTraitementsProduits();
        testDatas.createRefInputUnitPriceUnitConverter();

        // Irrigation
        RefInterventionAgrosystTravailEDI phytoProductAgrosystIntervention = refInterventionAgrosystTravailEDIDao.forNaturalId("SEX").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixPesticidiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i7 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Application de produits avec AMM / Traitement phytosanitaire ",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 7, 15),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 7, 15),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.05
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i7.addSpeciesStades(intervention1Stades);

        RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        RefActaTraitementsProduit metarexIno = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Divers - Molluscicides",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "METAREX INO",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "D5",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "D5",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "5680",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 121 //metarexIno 5680_121
        );

        RefActaTraitementsProduit betapost = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Herbicide - Inconnu",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "BETAPOST",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 999900 //betapost a_AAADA_999900
        );

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(metarexIno);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Divers - Molluscicides",
                InputPrice.PROPERTY_PRICE, null
        );
        var phytoInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Divers - Molluscicides",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.KG_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, metarexIno,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 10.0
        );

        objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(betapost);
        var phytoPrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Herbicide - Inconnu",
                InputPrice.PROPERTY_PRICE, null
        );
        var phytoInput2 = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Herbicide - Inconnu",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, betapost,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage2 = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput2,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 2.0
        );

        PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao = getPersistenceContext().getPesticidesSpreadingActionDao();
        pesticidesSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i7,
                AbstractAction.PROPERTY_MAIN_ACTION, phytoProductAgrosystIntervention,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 0.1,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, List.of(pesticideUsage, pesticideUsage2),
                PesticidesSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        // Two price missing : completion is 81.81%
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Application de produits avec AMM / Traitement phytosanitaire ;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;39.0;77;;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Application de produits avec AMM / Traitement phytosanitaire ;15/07/2013;15/07/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Herbicide - Inconnu (2.0 L/ha);;non;2013;Indicateur économique;Charges opérationnelles réelles (€/ha);25.5;50;Intrant 'Lutte chimique et biocontrôle (produits avec AMM) (BETAPOST Traitement phytosanitaire)' : Prix non renseigné;;");
    }

    @Test
    public void inraeEffectiveSeedingCropPriceAndProductTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        // Semis classique
        RefInterventionAgrosystTravailEDI semisClassic = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();
        Domain d0 = zpPlotBaulon1.getPlot().getDomain();
        RefCountry france = d0.getLocation().getRefCountry();

        //5255;Pragma SX;147;H1;Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés);N;ACTA 2013;t
        //4789;PROFILER;140;F1;Fongicides - Traitement des parties aériennes;N;ACTA 2017;t
        RefActaTraitementsProduit product = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("5255", 147, france).findUnique();

        //J10;Grandes cultures;;ZAP;Betterave;ZAN;Sucrière;;;;;;;;12;;BEAVX ;Beta;vulgaris;5;14;BETTERAVE SUCRIERE;BETA VULGARIS L.;AgroEDI 2016;725;betterave sucrière;;1025;Betterave industrielle et fourragère;t
        //J10;Grandes cultures;;ZAP;Betterave;ZLF;Fourrager / Fourrage;;;;;;;;12;;BEAVX ;Beta;vulgaris;6;14;BETTERAVE FOURRAGERE;BETA VULGARIS L.;AgroEDI 2016;91;betterave fourragère;;1025;Betterave industrielle et fourragère;t
        RefEspece juliusBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZAN", "", "").findUnique();
        RefEspece pythonBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZLF", "", "").findUnique();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixEspece(d0.getCampaign(), juliusBetteraveSucriere, pythonBetteraveSucriere);
        createRefPrixProduct(d0.getCampaign(), product);

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        CroppingPlanEntry betterave = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "betteraveSucriere_ID0",
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "betteraveSucriere_CODE0",
                CroppingPlanEntry.PROPERTY_NAME, "Betterave Sucrière",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies spM2JuliusBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_SPECIES, juliusBetteraveSucriere
        );

        CroppingPlanSpecies spM2PythonBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_SPECIES, pythonBetteraveSucriere
        );

        betterave.setCroppingPlanSpecies(Lists.newArrayList(spM2JuliusBetteraveSucriere, spM2PythonBetteraveSucriere));

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i2_semisclassique = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(d0.getCampaign(), 4, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(d0.getCampaign(), 4, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.3
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade spM2JuliusBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2JuliusBetteraveSucriere);
        i2_semisclassique.addSpeciesStades(spM2JuliusBetteraveSpeciesStade);
        EffectiveSpeciesStade spM2PythonBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2PythonBetteraveSucriere);
        i2_semisclassique.addSpeciesStades(spM2PythonBetteraveSpeciesStade);

        String displayName = betterave.getName();
        List<String> speciesNames = new ArrayList<>();

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(product);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Traitement : Barky (id-traitt = 147, id_produit = 5255)",
                InputPrice.PROPERTY_PRICE, 35d,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var traitement = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.G_Q,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_REF_INPUT, product,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, phytoPrice
        );
        var traitementUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, traitement,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 600.0
        );
        objectId = InputPriceService.GET_SPECIES_SEED_OBJECT_ID.apply(spM2JuliusBetteraveSucriere.getSpecies(), spM2JuliusBetteraveSucriere.getVariety());
        var seedSpeciesInput1 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2JuliusBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2JuliusBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, true,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedSpeciesInput.PROPERTY_SPECIES_PHYTO_INPUTS, List.of(traitement)
        );
        var speciesInputUsage1 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 1000000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput1,
                SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, List.of(traitementUsage),
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2JuliusBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        var seedSpeciesInput2 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2PythonBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2PythonBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var speciesInputUsage2 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 0d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput2,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2PythonBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        displayName = displayName + " (" + String.join(", ", speciesNames) + ")";
        var seedPrice = seedPriceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, displayName,
                InputPrice.PROPERTY_PRICE, 150.0,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS,
                SeedPrice.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedPrice.PROPERTY_INCLUDED_TREATMENT, false,
                SeedPrice.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                SeedPrice.PROPERTY_CHEMICAL_TREATMENT, true,
                SeedPrice.PROPERTY_ORGANIC, false
        );
        var seedLotInput = seedLotInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                DomainSeedLotInput.PROPERTY_CROP_SEED, betterave,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, List.of(seedSpeciesInput1, seedSpeciesInput2),
                DomainSeedLotInput.PROPERTY_USAGE_UNIT, SeedPlantUnit.GRAINES_PAR_HA,
                DomainSeedLotInput.PROPERTY_ORGANIC, false,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Julius",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedLotInput.PROPERTY_SEED_PRICE, seedPrice,
                DomainSeedLotInput.PROPERTY_SEED_COATED_TREATMENT, false
        );
        var seedLotUsage = seedLotInputUsageDao.create(
                SeedLotInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, seedLotInput,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, List.of(speciesInputUsage1, speciesInputUsage2)
        );
        SeedingActionUsageTopiaDao seedingActionDao = getPersistenceContext().getSeedingActionUsageDao();
        seedingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i2_semisclassique,
                AbstractAction.PROPERTY_MAIN_ACTION, semisClassic,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotUsage),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;1.079937E8;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);2013;Baulon;1.379901E8;100;;");


        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refActaTraitementsProduitsCateg.csv")) {
            importService.importActaTraitementsProduitsCateg(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refCompositionSubstancesActivesParNumeroAMM.csv")) {
            importService.importRefCompositionSubstancesActivesParNumeroAMM(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refSubstancesActivesCommissionEuropeenne.csv")) {
            importService.importRefSubstancesActivesCommissionEuropeenneCSV(stream);
        }

        // calcul avec dose de référence
        traitementUsage.setQtAvg(null);
        seedProductInputUsageDao.update(traitementUsage);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;1.079937E8;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);2013;Baulon;1.379901E8;100;;");

    }

    @Test
    public void inraeEffectiveSeedingCropPriceAndProductTestWithMissingPrice() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        int year = 2017;

        // Semis classique
        RefInterventionAgrosystTravailEDI semisClassic = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();
        Domain d0 = zpPlotBaulon1.getPlot().getDomain();
        RefCountry france = d0.getLocation().getRefCountry();

        //5255;Pragma SX;147;H1;Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés);N;ACTA 2013;t
        //4789;PROFILER;140;F1;Fongicides - Traitement des parties aériennes;N;ACTA 2017;t
        RefActaTraitementsProduit product = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("5255", 147, france).findUnique();

        //J10;Grandes cultures;;ZAP;Betterave;ZAN;Sucrière;;;;;;;;12;;BEAVX ;Beta;vulgaris;5;14;BETTERAVE SUCRIERE;BETA VULGARIS L.;AgroEDI 2016;725;betterave sucrière;;1025;Betterave industrielle et fourragère;t
        //J10;Grandes cultures;;ZAP;Betterave;ZLF;Fourrager / Fourrage;;;;;;;;12;;BEAVX ;Beta;vulgaris;6;14;BETTERAVE FOURRAGERE;BETA VULGARIS L.;AgroEDI 2016;91;betterave fourragère;;1025;Betterave industrielle et fourragère;t
        RefEspece juliusBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZAN", "", "").findUnique();
        RefEspece pythonBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZLF", "", "").findUnique();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixEspece(d0.getCampaign(), juliusBetteraveSucriere, pythonBetteraveSucriere);
        createRefPrixProduct(d0.getCampaign(), product);

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry betterave = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "betteraveSucriere_ID0",
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "betteraveSucriere_CODE0",
                CroppingPlanEntry.PROPERTY_NAME, "Betterave Sucrière",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies spM2JuliusBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_SPECIES, juliusBetteraveSucriere
        );

        CroppingPlanSpecies spM2PythonBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_SPECIES, pythonBetteraveSucriere
        );

        betterave.setCroppingPlanSpecies(Lists.newArrayList(spM2JuliusBetteraveSucriere, spM2PythonBetteraveSucriere));

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        EffectiveIntervention i2_semisclassique = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(year, 4, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(year, 4, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.3
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade spM2JuliusBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2JuliusBetteraveSucriere);
        i2_semisclassique.addSpeciesStades(spM2JuliusBetteraveSpeciesStade);
        EffectiveSpeciesStade spM2PythonBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2PythonBetteraveSucriere);
        i2_semisclassique.addSpeciesStades(spM2PythonBetteraveSpeciesStade);

        String displayName = betterave.getName();
        List<String> speciesNames = new ArrayList<>();

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(product);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Traitement : Barky (id-traitt = 147, id_produit = 5255)",
                InputPrice.PROPERTY_PRICE, 35d,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var traitement = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.G_Q,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_REF_INPUT, product,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, phytoPrice
        );
        var traitementUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, traitement,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 600.0
        );
        var seedSpeciesInput1 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2JuliusBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2JuliusBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, true,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedSpeciesInput.PROPERTY_SPECIES_PHYTO_INPUTS, List.of(traitement)
        );
        var speciesInputUsage1 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 1000000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput1,
                SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, List.of(traitementUsage),
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2JuliusBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        var seedSpeciesInput2 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2PythonBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2PythonBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var speciesInputUsage2 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 0d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput2,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2PythonBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        displayName = displayName + " (" + String.join(", ", speciesNames) + ")";
        var seedLotPrice = seedPriceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, betterave.getTopiaId(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, displayName,
                InputPrice.PROPERTY_PRICE, null,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var seedLotInput = seedLotInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                DomainSeedLotInput.PROPERTY_CROP_SEED, betterave,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, List.of(seedSpeciesInput1, seedSpeciesInput2),
                DomainSeedLotInput.PROPERTY_USAGE_UNIT, SeedPlantUnit.UNITE_PAR_HA,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Julius",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedLotInput.PROPERTY_SEED_PRICE, seedLotPrice,
                DomainSeedLotInput.PROPERTY_SEED_COATED_TREATMENT, false
        );

        var seedLotUsage = seedLotInputUsageDao.create(
                SeedLotInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, seedLotInput,
                SeedLotInputUsage.PROPERTY_QT_AVG, 600.0,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, List.of(speciesInputUsage1, speciesInputUsage2));

        SeedingActionUsageTopiaDao seedingActionDao = getPersistenceContext().getSeedingActionUsageDao();
        seedingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i2_semisclassique,
                AbstractAction.PROPERTY_MAIN_ACTION, semisClassic,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotUsage),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04/2017;01/04/2017;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);3.9E7;50;Aucun prix de semis renseigné pour la culture : Betterave Sucrière;;");


        traitementUsage.setQtAvg(null);
        seedProductInputUsageDao.update(traitementUsage);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04/2017;01/04/2017;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);3.9E7;50;Aucun prix de semis renseigné pour la culture : Betterave Sucrière;;");

    }

    protected void createRefPrixIrrig(int year) {
        RefPrixIrrigTopiaDao refInputPriceDao = getPersistenceContext().getRefPrixIrrigDao();

        refInputPriceDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3,
                RefInputPrice.PROPERTY_PRICE, 0.08
        );

        refInputPriceDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3,
                RefInputPrice.PROPERTY_PRICE, 0.08
        );

        refInputPriceDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3,
                RefInputPrice.PROPERTY_PRICE, 0.08
        );
    }

    protected void createRefPrixEspece(int year, RefEspece juliusBetteraveSucriere, RefEspece pythonBetteraveSucriere) {

        RefPrixEspeceTopiaDao refPrixEspeceDao = getPersistenceContext().getRefPrixEspeceDao();

        final String juliusTid0 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + (year - 1)
                + ",scenario:" + (year - 1)
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID,
                juliusTid0,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year - 1,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 231.5,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year - 1)
        );

        final String juliusTid1 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + (year - 1)
                + ",scenario:" + (year - 1)
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefEspece.PROPERTY_TOPIA_ID, juliusTid1,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year - 1,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 150.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year - 1)
        );

        final String pythonBetteraveSucriere0 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + (year - 1)
                + ",scenario:" + (year - 1)
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere0,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year - 1,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 150.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year - 1)
        );

        final String pythonBetteraveSucriere1 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + true
                + ",campagne:" + (year - 1)
                + ",scenario:" + (year - 1)
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere1,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year - 1,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 160.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, true,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year - 1)
        );

        final String pythonBetteraveSucriere2 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + (year - 1)
                + ",scenario:" + (year - 1)
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere2,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year - 1,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 225.679183498524,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year - 1)
        );

        final String juliusTid2 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + year
                + ",scenario:" + year
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid2,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 246.5,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year)
        );

        final String juliusTid3 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + year
                + ",scenario:" + year
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid3,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 130.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year)
        );

        final String pythonBetteraveSucriere3 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + year
                + ",scenario:" + year
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere3,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 240.679183498524,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year)
        );

        final String pythonBetteraveSucriere4 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + year
                + ",scenario:" + year
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere4,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 130.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year)
        );

        final String pythonBetteraveSucriere5 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + true
                + ",campagne:" + year
                + ",scenario:" + year
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere5,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, year,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 160.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, true,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(year)
        );
        final String juliusTid4 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid4,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 346.5,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String juliusTid5 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid5,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 100.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String pythonBetteraveSucriere6 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere6,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 2340.679183498524,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String pythonBetteraveSucriere7 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere7,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 100.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String pythonBetteraveSucriere8 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + true
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere8,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 160.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, true,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );
    }

    protected void createRefPrixProduct(int year, RefActaTraitementsProduit product) {
        //5255;Pragma SX;147;H1;Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés);N;ACTA 2013;t
        //4789;PROFILER;140;F1;Fongicides - Traitement des parties aériennes;N;ACTA 2017;t

        //"Traitement : Barky (id-traitt = 147, id_produit = 5255)",// dans le doc: Traitement : Barky (id-traitt = 141, id_produit = 3083)

        RefPrixPhytoTopiaDao refPrixPhytoDao = getPersistenceContext().getRefPrixPhytoDao();

        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ACTIVE, true,
                RefPrixPhyto.PROPERTY_CAMPAIGN, year - 1,
                RefPrixPhyto.PROPERTY_SCENARIO, String.valueOf(year - 1),
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, product.getId_produit() + "_" + product.getId_traitement(),
                RefPrixPhyto.PROPERTY_ID_PRODUIT, product.getId_produit(),
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, product.getId_traitement(),
                RefPrixPhyto.PROPERTY_PRICE, 40.0,
                RefPrixPhyto.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ACTIVE, true,
                RefPrixPhyto.PROPERTY_CAMPAIGN, year,
                RefPrixPhyto.PROPERTY_SCENARIO, String.valueOf(year),
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, product.getId_produit() + "_" + product.getId_traitement(),
                RefPrixPhyto.PROPERTY_ID_PRODUIT, product.getId_produit(),
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, product.getId_traitement(),
                RefPrixPhyto.PROPERTY_PRICE, 55.0,
                RefPrixPhyto.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ACTIVE, true,
                RefPrixPhyto.PROPERTY_CAMPAIGN, null,
                RefPrixPhyto.PROPERTY_SCENARIO, "Y",
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, product.getId_produit() + "_" + product.getId_traitement(),
                RefPrixPhyto.PROPERTY_ID_PRODUIT, product.getId_produit(),
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, product.getId_traitement(),
                RefPrixPhyto.PROPERTY_PRICE, 155.0,
                RefPrixPhyto.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
    }

    protected void createRefPrixFertiMin(int year) {
        RefPrixFertiMinTopiaDao refPrixFertiMinDao = getPersistenceContext().getRefPrixFertiMinDao();

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.K2_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.MG_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.S_O3
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.K2_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.MG_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.S_O3
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.K2_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.MG_O
        );

        refPrixFertiMinDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T,
                RefInputPrice.PROPERTY_PRICE, 0.08,
                RefPrixFertiMin.PROPERTY_CATEG, 442,
                RefPrixFertiMin.PROPERTY_FORME, "Compacté",
                RefPrixFertiMin.PROPERTY_ELEMENT, FertiMinElement.S_O3
        );
    }

    protected void createRefPrixPesticidiMin(int year) {
        RefPrixPhytoTopiaDao refPrixPhytoDao = getPersistenceContext().getRefPrixPhytoDao();
        // METAREX INO
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_SCENARIO, "2016",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG,
                RefInputPrice.PROPERTY_PRICE, 12.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "5680",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 121,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "5680_121"
        );
        // BETAPOST
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_SCENARIO, "2016",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 240.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 999900,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "a_AAADA_999900"
        );
        // METAREX INO
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_SCENARIO, "2017",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG,
                RefInputPrice.PROPERTY_PRICE, 27.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "5680",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 121,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "5680_121"
        );
        // BETAPOST
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_SCENARIO, "2017",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 255.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 999900,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "a_AAADA_999900"
        );
        // METAREX INO
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, null,
                RefInputPrice.PROPERTY_SCENARIO, "Y",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG,
                RefInputPrice.PROPERTY_PRICE, 127.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "5680",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 121,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "5680_121"
        );
        // BETAPOST
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, null,
                RefInputPrice.PROPERTY_SCENARIO, "Y",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 355.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 999900,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "a_AAADA_999900"
        );
        // HURRICANE
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_SCENARIO, "2016",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 10.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "2870",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 126,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "2870_126"
        );
        // HURRICANE
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_SCENARIO, "2017",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 25.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "2870",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 126,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "2870_126"
        );
        // HURRICANE
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, null,
                RefInputPrice.PROPERTY_SCENARIO, "Y",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 125.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "2870",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 126,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "2870_126"
        );
    }

    @Test
    public void inraePracticedSeedingCropTest() throws IOException, DomainExtendException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        Zone zpPlotBaulon2017 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN + " DESC").findFirst();
        Domain d2015 = zpPlotBaulon2017.getPlot().getDomain();
        RefCountry france = d2015.getLocation().getRefCountry();

        //5255;Pragma SX;147;H1;Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés);N;ACTA 2013;t
        //4789;PROFILER;140;F1;Fongicides - Traitement des parties aériennes;N;ACTA 2017;t

        RefActaTraitementsProduit pragmaSX = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("5255", 147, france).findUnique();
        RefActaTraitementsProduit profiler = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("4789", 140, france).findUnique();

        //J10;Grandes cultures;;ZAP;Betterave;ZAN;Sucrière;;;;;;;;12;;BEAVX ;Beta;vulgaris;5;14;BETTERAVE SUCRIERE;BETA VULGARIS L.;AgroEDI 2016;725;betterave sucrière;;1025;Betterave industrielle et fourragère;t
        //J10;Grandes cultures;;ZAP;Betterave;ZLF;Fourrager / Fourrage;;;;;;;;12;;BEAVX ;Beta;vulgaris;6;14;BETTERAVE FOURRAGERE;BETA VULGARIS L.;AgroEDI 2016;91;betterave fourragère;;1025;Betterave industrielle et fourragère;t
        RefEspece juliusBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZAN", "", "").findUnique();
        RefEspece pythonBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZLF", "", "").findUnique();

        testDatas.createRefInputUnitPriceUnitConverter();

        domainService.extendDomain(d2015.getTopiaId(), 2016);
        Domain d2017 = domainService.extendDomain(d2015.getTopiaId(), 2017);

        createRefPrixEspece(d2017.getCampaign(), juliusBetteraveSucriere, pythonBetteraveSucriere);
        createRefPrixProduct(d2017.getCampaign(), pragmaSX);
        createRefPrixProduct(d2017.getCampaign(), profiler);

        // Semis classique
        RefInterventionAgrosystTravailEDI semisClassic = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();
        zpPlotBaulon2017 = zoneDao.newQueryBuilder()
                .addEquals(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, 2017)
                .addEquals(Zone.PROPERTY_ACTIVE, true)
                .setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN)
                .findFirst();

        GrowingSystem gs2017 = zpPlotBaulon2017.getPlot().getGrowingSystem();

        CroppingPlanEntry betterave = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "betteraveSucriere_ID0",
                CroppingPlanEntry.PROPERTY_DOMAIN, d2017,
                CroppingPlanEntry.PROPERTY_CODE, "betteraveSucriere_CODE0",
                CroppingPlanEntry.PROPERTY_NAME, "Betterave Sucrière",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies spM2JuliusBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_SPECIES, juliusBetteraveSucriere
        );

        CroppingPlanSpecies spM2PythonBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_SPECIES, pythonBetteraveSucriere
        );

        betterave.setCroppingPlanSpecies(Lists.newArrayList(spM2JuliusBetteraveSucriere, spM2PythonBetteraveSucriere));

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "Semis",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, gs2017,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply("2016,2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true);

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, betterave.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDAO.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedIntervention i2_semisClassique = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "01/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "01/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 0.3,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade spM2JuliusBetteraveSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, spM2JuliusBetteraveSucriere.getCode());
        i2_semisClassique.addSpeciesStades(spM2JuliusBetteraveSpeciesStade);
        PracticedSpeciesStade spM2PythonBetteraveSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, spM2PythonBetteraveSucriere.getCode());
        i2_semisClassique.addSpeciesStades(spM2PythonBetteraveSpeciesStade);

        String displayName = betterave.getName();
        List<String> speciesNames = new ArrayList<>();

        var objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(pragmaSX);
        var pragmaSXPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Traitement : Barky (id-traitt = 141, id_produit = 3083)",
                InputPrice.PROPERTY_PRICE, null,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var pragmaSXInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.G_Q,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_REF_INPUT, pragmaSX,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, objectId,
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, pragmaSXPrice
        );
        var pragmaSXInputUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, pragmaSXInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 600.0
        );

        objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(profiler);
        var profilerPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Traitement des parties aériennes",
                InputPrice.PROPERTY_PRICE, null,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var profilerInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_UNITE_SEMENCES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_REF_INPUT, pragmaSX,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, objectId,
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, profilerPrice
        );
        var profilerInputUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, profilerInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 0.2
        );

        var seedSpeciesInput1 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2JuliusBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2JuliusBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, true,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedSpeciesInput.PROPERTY_SPECIES_PHYTO_INPUTS, List.of(pragmaSXInput, profilerInput)
        );
        var speciesInputUsage1 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 1000000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput1,
                SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, List.of(pragmaSXInputUsage, profilerInputUsage),
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2JuliusBetteraveSucriere.getSpecies().getLibelle_espece_botanique());

        var seedSpeciesInput2 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2PythonBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2PythonBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var speciesInputUsage2 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 20000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput2,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2PythonBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        displayName = displayName + " (" + String.join(", ", speciesNames) + ")";
        var seedLotPrice = seedPriceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, betterave.getCode(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, displayName,
                InputPrice.PROPERTY_PRICE, 270.0,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS,
                SeedPrice.PROPERTY_CHEMICAL_TREATMENT, true,
                SeedPrice.PROPERTY_INCLUDED_TREATMENT, true
        );
        var seedLotInput = seedLotInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                DomainSeedLotInput.PROPERTY_CROP_SEED, betterave,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, List.of(seedSpeciesInput1, seedSpeciesInput2),
                DomainSeedLotInput.PROPERTY_USAGE_UNIT, SeedPlantUnit.GRAINES_PAR_HA,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Julius",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedLotInput.PROPERTY_SEED_PRICE, seedLotPrice,
                DomainSeedLotInput.PROPERTY_SEED_COATED_TREATMENT, true
        );

        var seedLotUsage = seedLotInputUsageDao.create(
                SeedLotInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, seedLotInput,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, List.of(speciesInputUsage1, speciesInputUsage2)
        );
        SeedingActionUsageTopiaDao seedingActionDao = getPersistenceContext().getSeedingActionUsageDao();
        seedingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, i2_semisClassique,
                AbstractAction.PROPERTY_MAIN_ACTION, semisClassic,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotUsage),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon2017.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(true);
        writer.setPerformance(performance);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Couëron les bains;Systeme de culture Couëron les bains 1;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2016, 2017;Couëron les bains;8.262E7;");

        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Betterave Sucrière;;null;2016, 2017;Indicateur économique;Charges opérationnelles réelles (€/ha);8.1E7;50;Donnée référentiel manquante : prix standardisé.;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Betterave Sucrière;;null;2016, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);0.0;50;Donnée référentiel manquante : prix standardisé.;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Betterave Sucrière;;null;2016, 2017;Indicateur économique;Charges opérationnelles réelles (€/ha);1620000.0;");
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refActaTraitementsProduitsCateg.csv")) {
            importService.importActaTraitementsProduitsCateg(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refCompositionSubstancesActivesParNumeroAMM.csv")) {
            importService.importRefCompositionSubstancesActivesParNumeroAMM(stream);
        }

        // calcul avec dose de référence
        profilerInputUsage.setQtAvg(null);
        seedProductInputUsageDao.update(profilerInputUsage);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("Couëron les bains;Systeme de culture Couëron les bains 1;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2016, 2017;Couëron les bains;8.262E7");
    }

    @Test
    public void inraePracticedSeedingCropAndProductTest() throws IOException, DomainExtendException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        Zone zpPlotBaulon2017 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN + " DESC").findFirst();
        Domain d2015 = zpPlotBaulon2017.getPlot().getDomain();
        RefCountry france = d2015.getLocation().getRefCountry();

        domainService.extendDomain(d2015.getTopiaId(), 2016);
        Domain d2017 = domainService.extendDomain(d2015.getTopiaId(), 2017);
        //5255;Pragma SX;147;H1;Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés);N;ACTA 2013;t
        //4789;PROFILER;140;F1;Fongicides - Traitement des parties aériennes;N;ACTA 2017;t

        RefActaTraitementsProduit pragmaSX = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("5255", 147, france).findUnique();
        RefActaTraitementsProduit profiler = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("4789", 140, france).findUnique();

        //J10;Grandes cultures;;ZAP;Betterave;ZAN;Sucrière;;;;;;;;12;;BEAVX ;Beta;vulgaris;5;14;BETTERAVE SUCRIERE;BETA VULGARIS L.;AgroEDI 2016;725;betterave sucrière;;1025;Betterave industrielle et fourragère;t
        //J10;Grandes cultures;;ZAP;Betterave;ZLF;Fourrager / Fourrage;;;;;;;;12;;BEAVX ;Beta;vulgaris;6;14;BETTERAVE FOURRAGERE;BETA VULGARIS L.;AgroEDI 2016;91;betterave fourragère;;1025;Betterave industrielle et fourragère;t
        RefEspece juliusBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZAN", "", "").findUnique();
        RefEspece pythonBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZLF", "", "").findUnique();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixEspece(d2017.getCampaign(), juliusBetteraveSucriere, pythonBetteraveSucriere);
        createRefPrixProduct(d2017.getCampaign(), pragmaSX);
        createRefPrixProduct(d2017.getCampaign(), profiler);

        // Semis classique
        RefInterventionAgrosystTravailEDI semisClassic = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUnique();

        zpPlotBaulon2017 = zoneDao.newQueryBuilder()
                .addEquals(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN, 2017)
                .addEquals(Zone.PROPERTY_ACTIVE, true)
                .setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_DOMAIN + "." + Domain.PROPERTY_CAMPAIGN)
                .findFirst();

        GrowingSystem gs2017 = zpPlotBaulon2017.getPlot().getGrowingSystem();

        CroppingPlanEntry betterave = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "betteraveSucriere_ID0",
                CroppingPlanEntry.PROPERTY_DOMAIN, d2017,
                CroppingPlanEntry.PROPERTY_CODE, "betteraveSucriere0_code",
                CroppingPlanEntry.PROPERTY_NAME, "Betterave Sucrière",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies spM2JuliusBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_SPECIES, juliusBetteraveSucriere
        );

        CroppingPlanSpecies spM2PythonBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_SPECIES, pythonBetteraveSucriere
        );

        betterave.setCroppingPlanSpecies(Lists.newArrayList(spM2JuliusBetteraveSucriere, spM2PythonBetteraveSucriere));

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "Semis",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, gs2017,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply("2016,2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true);

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, betterave.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDAO.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedIntervention i2_semisclassique = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "01/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "01/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 0.3,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade spM2JuliusBetteraveSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, spM2JuliusBetteraveSucriere.getCode());
        i2_semisclassique.addSpeciesStades(spM2JuliusBetteraveSpeciesStade);
        PracticedSpeciesStade spM2PythonBetteraveSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, spM2PythonBetteraveSucriere.getCode());
        i2_semisclassique.addSpeciesStades(spM2PythonBetteraveSpeciesStade);

        String displayName = betterave.getName();
        List<String> speciesNames = new ArrayList<>();

        var objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(pragmaSX);
        var pragmaSXPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Traitement : Barky (id-traitt = 141, id_produit = 3083)",
                InputPrice.PROPERTY_PRICE, 35.0,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var pragmaSXInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.G_Q,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_REF_INPUT, pragmaSX,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, objectId,
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, pragmaSXPrice
        );
        var pragmaSXInputUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, pragmaSXInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 600.0
        );

        objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(profiler);
        var profilerPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Traitement des parties aériennes",
                InputPrice.PROPERTY_PRICE, 10.0,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var profilerInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_UNITE_SEMENCES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_REF_INPUT, pragmaSX,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, objectId,
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, profilerPrice
        );
        var profilerInputUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, profilerInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 0.2
        );

        var seedSpeciesInput1 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2JuliusBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2JuliusBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, true,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedSpeciesInput.PROPERTY_SPECIES_PHYTO_INPUTS, List.of(pragmaSXInput, profilerInput)
        );
        var speciesInputUsage1 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 1000000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput1,
                SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, List.of(pragmaSXInputUsage, profilerInputUsage),
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2JuliusBetteraveSucriere.getSpecies().getLibelle_espece_botanique());

        var seedSpeciesInput2 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2PythonBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2PythonBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var speciesInputUsage2 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 20000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput2,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2PythonBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        displayName = displayName + " (" + String.join(", ", speciesNames) + ")";
        var seedLotPrice = seedPriceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, betterave.getCode(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, displayName,
                InputPrice.PROPERTY_PRICE, 46.0,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS,
                SeedPrice.PROPERTY_CHEMICAL_TREATMENT, true,
                SeedPrice.PROPERTY_INCLUDED_TREATMENT, true
        );
        var seedLotInput = seedLotInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                DomainSeedLotInput.PROPERTY_CROP_SEED, betterave,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, List.of(seedSpeciesInput1, seedSpeciesInput2),
                DomainSeedLotInput.PROPERTY_USAGE_UNIT, SeedPlantUnit.GRAINES_PAR_HA,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon2017.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Julius",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedLotInput.PROPERTY_SEED_PRICE, seedLotPrice,
                DomainSeedLotInput.PROPERTY_SEED_COATED_TREATMENT, false
        );

        var seedLotUsage = seedLotInputUsageDao.create(
                SeedLotInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, seedLotInput,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, List.of(speciesInputUsage1, speciesInputUsage2)
        );
        SeedingActionUsageTopiaDao seedingActionDao = getPersistenceContext().getSeedingActionUsageDao();
        seedingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, i2_semisclassique,
                AbstractAction.PROPERTY_MAIN_ACTION, semisClassic,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotUsage),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon2017.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(true);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Betterave Sucrière;;null;2016, 2017;Indicateur économique;Charges opérationnelles réelles (€/ha);1.38E7;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Betterave Sucrière;;null;2016, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);4.2E7;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Pragma SX (600.0 g/q);;non;2016, 2017;Indicateur économique;Charges opérationnelles réelles (€/ha);6.29937E7;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Pragma SX (600.0 g/q);;non;2016, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);8.549145E7;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Pragma SX (0.2 L/unité de semences);;non;2016, 2017;Indicateur économique;Charges opérationnelles réelles (€/ha);0.0;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Pragma SX (0.2 L/unité de semences);;non;2016, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);0.0;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Betterave Sucrière;;null;2016, 2017;Indicateur économique;Charges opérationnelles réelles (€/ha);276000.0;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Couëron les bains (2017);;;Systeme de culture Couëron les bains 1;;Agriculture conventionnelle;Semis;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;i1;01/04;01/04;Semis;Betterave Sucrière;;null;2016, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);840000.0;");

        assertThat(content).usingComparator(comparator)
                .isEqualTo("Couëron les bains;Systeme de culture Couëron les bains 1;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2016, 2017;Couëron les bains;7.70697E7;");

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refActaTraitementsProduitsCateg.csv")) {
            importService.importActaTraitementsProduitsCateg(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refCompositionSubstancesActivesParNumeroAMM.csv")) {
            importService.importRefCompositionSubstancesActivesParNumeroAMM(stream);
        }

        // calcul avec dose de référence
        profilerInputUsage.setQtAvg(null);
        seedProductInputUsageDao.update(profilerInputUsage);
        pragmaSXInputUsage.setQtAvg(null);
        seedProductInputUsageDao.update(pragmaSXInputUsage);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("Couëron les bains;Systeme de culture Couëron les bains 1;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2016, 2017;Couëron les bains;7.70697E7;");

    }

    @Test
    public void inraeEffectiveSeedingCropTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        int year = 2017;

        // Semis classique
        RefInterventionAgrosystTravailEDI semisClassic = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();
        Domain d0 = zpPlotBaulon1.getPlot().getDomain();
        RefCountry france = d0.getLocation().getRefCountry();

        //5255;Pragma SX;147;H1;Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés);N;ACTA 2013;t
        //4789;PROFILER;140;F1;Fongicides - Traitement des parties aériennes;N;ACTA 2017;t
        RefActaTraitementsProduit product = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("5255", 147, france).findUnique();

        //J10;Grandes cultures;;ZAP;Betterave;ZAN;Sucrière;;;;;;;;12;;BEAVX ;Beta;vulgaris;5;14;BETTERAVE SUCRIERE;BETA VULGARIS L.;AgroEDI 2016;725;betterave sucrière;;1025;Betterave industrielle et fourragère;t
        //J10;Grandes cultures;;ZAP;Betterave;ZLF;Fourrager / Fourrage;;;;;;;;12;;BEAVX ;Beta;vulgaris;6;14;BETTERAVE FOURRAGERE;BETA VULGARIS L.;AgroEDI 2016;91;betterave fourragère;;1025;Betterave industrielle et fourragère;t
        RefEspece juliusBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZAN", "", "").findUnique();
        RefEspece pythonBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZLF", "", "").findUnique();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixEspece(d0.getCampaign(), juliusBetteraveSucriere, pythonBetteraveSucriere);
        createRefPrixProduct(d0.getCampaign(), product);

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry betterave = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "betteraveSucriere_ID0",
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "betteraveSucriere_CODE0",
                CroppingPlanEntry.PROPERTY_NAME, "Betterave Sucrière",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies spM2JuliusBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_SPECIES, juliusBetteraveSucriere
        );

        CroppingPlanSpecies spM2PythonBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_SPECIES, pythonBetteraveSucriere
        );

        betterave.setCroppingPlanSpecies(Lists.newArrayList(spM2JuliusBetteraveSucriere, spM2PythonBetteraveSucriere));

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        EffectiveIntervention i3_semisclassique = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(year, 4, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(year, 4, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.3
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade spM2JuliusBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2JuliusBetteraveSucriere);
        i3_semisclassique.addSpeciesStades(spM2JuliusBetteraveSpeciesStade);
        EffectiveSpeciesStade spM2PythonBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2PythonBetteraveSucriere);
        i3_semisclassique.addSpeciesStades(spM2PythonBetteraveSpeciesStade);

        String displayName = betterave.getName();
        List<String> speciesNames = new ArrayList<>();

        var seedSpeciesInput1 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2JuliusBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2JuliusBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var speciesInputUsage1 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 0d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput1,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2JuliusBetteraveSucriere.getSpecies().getLibelle_espece_botanique());

        var seedSpeciesInput2 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2PythonBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2PythonBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var speciesInputUsage2 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 20000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput2,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2PythonBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        displayName = displayName + " (" + String.join(", ", speciesNames) + ")";
        var seedLotPrice = seedPriceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, betterave.getCode(),
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, displayName,
                InputPrice.PROPERTY_PRICE, 300.0,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var seedLotInput = seedLotInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                DomainSeedLotInput.PROPERTY_CROP_SEED, betterave,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, List.of(seedSpeciesInput1, seedSpeciesInput2),
                DomainSeedLotInput.PROPERTY_USAGE_UNIT, SeedPlantUnit.GRAINES_PAR_HA,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Julius",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedLotInput.PROPERTY_SEED_PRICE, seedLotPrice,
                DomainSeedLotInput.PROPERTY_SEED_COATED_TREATMENT, false
        );

        var seedLotUsage = seedLotInputUsageDao.create(
                SeedLotInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, seedLotInput,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, List.of(speciesInputUsage1, speciesInputUsage2)
        );
        SeedingActionUsageTopiaDao seedingActionDao = getPersistenceContext().getSeedingActionUsageDao();
        seedingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i3_semisclassique,
                AbstractAction.PROPERTY_MAIN_ACTION, semisClassic,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotUsage),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Betterave Sucrière;PLEINE_PRODUCTION;i1;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;1800000.0;100;");
    }

    @Test
    public void inraeEffectivePhytoProductI6Test() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        final RefCountry france = testDatas.getDefaultFranceCountry();
        testDatas.importActaTraitementsProduits();
        testDatas.createRefInputUnitPriceUnitConverter();

        // Irrigation
        RefInterventionAgrosystTravailEDI phytoProductAgrosystIntervention = refInterventionAgrosystTravailEDIDao.forNaturalId("SEX").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixPesticidiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i6 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Application de produits avec AMM / Traitement phytosanitaire ",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 14),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 14),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 2,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i6.addSpeciesStades(intervention1Stades);

        RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();

        RefActaTraitementsProduit hurricane = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Divers - Divers - Adjuvants mouillants non ioniques",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "HURRICANE",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "D8ba",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "D8ba",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "2870",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 126,
                RefActaTraitementsProduit.PROPERTY_CODE__AMM, "2030085"
        );

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(hurricane);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Divers - Divers - Adjuvants mouillants non ioniques",
                InputPrice.PROPERTY_PRICE, 3.0
        );
        var phytoInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Herbicide - Inconnu",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_HL,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, hurricane,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 0.2);

        PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao = getPersistenceContext().getPesticidesSpreadingActionDao();
        pesticidesSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i6,
                AbstractAction.PROPERTY_MAIN_ACTION, phytoProductAgrosystIntervention,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 33.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 0.1,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, List.of(pesticideUsage),
                PesticidesSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Application de produits avec AMM / Traitement phytosanitaire ;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;3.960000000000001;100;");

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refActaTraitementsProduitsCateg.csv")) {
            importService.importActaTraitementsProduitsCateg(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refCompositionSubstancesActivesParNumeroAMM.csv")) {
            importService.importRefCompositionSubstancesActivesParNumeroAMM(stream);
        }

        // calcul avec dose de référence
        pesticideUsage.setQtAvg(null);
        pesticideProductInputUsageDao.update(pesticideUsage);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Application de produits avec AMM / Traitement phytosanitaire ;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;3.960000000000001;");

    }

    @Test
    public void inraeEffectivePhytoProductWithV0RefInputUnitConverter() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        final RefCountry france = testDatas.getDefaultFranceCountry();
        testDatas.importActaTraitementsProduits();
        testDatas.importRefInputUnitPriceUnitConverterV0();

        // Irrigation
        RefInterventionAgrosystTravailEDI phytoProductAgrosystIntervention = refInterventionAgrosystTravailEDIDao.forNaturalId("SEX").findUnique();

        // psci: intervention.getTransitCount() * intervention.getSpatialFrequency();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixPesticidiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        EffectiveIntervention i6 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Application de produits avec AMM / Traitement phytosanitaire ",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 14),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 6, 14),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 2,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        i6.addSpeciesStades(intervention1Stades);

        RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();

        RefActaTraitementsProduit hurricane = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Divers - Divers - Adjuvants mouillants non ioniques",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "HURRICANE",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "D8ba",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "D8ba",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "2870",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 126
        );

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(hurricane);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Divers - Divers - Adjuvants mouillants non ioniques",
                InputPrice.PROPERTY_PRICE, 3.0
        );
        var phytoInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "HURRICANE",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_HL,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, hurricane,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 0.2);

        PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao = getPersistenceContext().getPesticidesSpreadingActionDao();
        pesticidesSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i6,
                AbstractAction.PROPERTY_MAIN_ACTION, phytoProductAgrosystIntervention,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 33.0,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 0.1,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, List.of(pesticideUsage),
                PesticidesSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorOperatingExpensesFilter = createOperatingExpenseFilter();
        indicatorFilters.add(indicatorOperatingExpensesFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator)
                .isEqualTo("Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Application de produits avec AMM / Traitement phytosanitaire ;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;0.0;85;Donnée référentiel manquante : taux de conversion manquant pour l'unité de prix 'EURO_L' avec l'unité d'utilisation 'L_HL'.;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Application de produits avec AMM / Traitement phytosanitaire ;14/06/2013;14/06/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);HURRICANE (0.2 L/hL);;non;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);0.0;100;;;");

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refActaTraitementsProduitsCateg.csv")) {
            importService.importActaTraitementsProduitsCateg(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refMAADosesRefParGroupeCible.csv")) {
            importService.importRefMAADosesRefParGroupeCible(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/operatingexpenses/refCompositionSubstancesActivesParNumeroAMM.csv")) {
            importService.importRefCompositionSubstancesActivesParNumeroAMM(stream);
        }

        // calcul avec dose de référence
        pesticideUsage.setQtAvg(null);
        pesticideProductInputUsageDao.update(pesticideUsage);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);

        IndicatorVintageTargetIFT indicatorVintageTargetIFT = serviceFactory.newInstance(IndicatorVintageTargetIFT.class);
        indicatorVintageTargetIFT.init(new ArrayList<>());// liste des ifts à afficher, ici aucun

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorVintageTargetIFT, indicatorOperatingExpenses);
        content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Application de produits avec AMM / Traitement phytosanitaire ;Indicateur économique;Charges opérationnelles réelles (€/ha);2013;Baulon;0.0;78;Donnée référentiel manquante : taux de conversion manquant pour l'unité de prix 'EURO_L' avec l'unité d'utilisation 'L_HL'");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Application de produits avec AMM / Traitement phytosanitaire ;14/06/2013;14/06/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);HURRICANE (null L/hL)");

    }

    protected Domain getRef10936DataTest() throws IOException {
        RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao = getPersistenceContext().getRefSolProfondeurIndigoDao();
        RefSolProfondeurIndigo solProfond = refSolProfondeurIndigoDao.forClasse_de_profondeur_INDIGOEquals(PerformanceServiceImpl.DEFAULT_DEEPEST_INDIGO).findUnique();

        RefLocationTopiaDao refLocationDao = getPersistenceContext().getRefLocationDao();
        DomainTopiaDao domainDao = getPersistenceContext().getDomainDao();
        RefLegalStatusTopiaDao refLegalStatusDao = getPersistenceContext().getRefLegalStatusDao();

        RefLocation location = testDatas.createRefLocation(refLocationDao);
        Domain d0 = testDatas.createSimpleDomain(domainDao, "fr.inra.agrosyst.api.entities.Domain_7fd194f6-7440-432f-a37b-fbf1d27fa95e", location, testDatas.createRefLegalStatus(refLegalStatusDao), "Baulon", 2017, "Annie Verssaire", "Lorem ipsum dolor sit amet", 100.0, 9.0, 1.0, true);
        GrowingPlan gp = testDatas.createGrowingPlan(d0);
        GrowingSystem gs = testDatas.createGrowingSystem(gp);
        String plotName = "Plot Baulon 2";
        String plotEdaplosId = "edaplosIssuerIdBaulon2";

        RefSolTextureGeppaTopiaDao refSolTextureGeppaDao = getPersistenceContext().getRefSolTextureGeppaDao();
        RefSolTextureGeppa solArgiloSaleuse = refSolTextureGeppaDao.forNaturalId("AS").findUnique();
        RefSolTextureGeppa solSableArgiloLimoneux = refSolTextureGeppaDao.forNaturalId("Sal").findUnique();
        Plot plot = testDatas.createPlot(solProfond, solArgiloSaleuse, solSableArgiloLimoneux, gs, Lists.newArrayList(location), plotName, plotEdaplosId);

        testDatas.createZone(plot, ZoneType.PRINCIPALE);

        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        // required imports
        RefInterventionAgrosystTravailEDI aat20 = refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "AAT20", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.AUTRE);
        RefInterventionAgrosystTravailEDI aat04 = refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "AAT04", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.AUTRE);

        RefEspece fraisier = testDatas.createRefEspece(refEspeceDao, "ZBV", "H50", "Fraisier", "");
        RefEspece sapin = testDatas.createRefEspece(refEspeceDao, "I44", "", "Sapin", "");
        RefEspece pivoine = testDatas.createRefEspece(refEspeceDao, "I98", "", "Pivoine", "");
        RefEspece chrysantheme = testDatas.createRefEspece(refEspeceDao, "E80", "", "Chrysanthème", "");

        CroppingPlanEntryTopiaDao croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        CroppingPlanEntry cultureFraisier = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "Culture_Fraisier_T0",
                CroppingPlanEntry.PROPERTY_CODE, "Culture_Fraisier_C0",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN,
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_NAME, "Culture Fraisier"
        );

        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        CroppingPlanSpecies especeFraisier = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "CPS_fraisier_topiaid0",
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_fraisier_code",
                CroppingPlanSpecies.PROPERTY_SPECIES, fraisier,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cultureFraisier
        );

        List<CroppingPlanSpecies> especesFraisiers = new ArrayList<>();
        especesFraisiers.add(especeFraisier);
        cultureFraisier.addAllCroppingPlanSpecies(especesFraisiers);

        getPersistenceContext().commit();

        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        refSpeciesToSectorDao.createByNaturalId(fraisier.getCode_espece_botanique(), fraisier.getCode_qualifiant_AEE(), Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId(sapin.getCode_espece_botanique(), sapin.getCode_qualifiant_AEE(), Sector.HORTICULTURE);
        refSpeciesToSectorDao.createByNaturalId(pivoine.getCode_espece_botanique(), pivoine.getCode_qualifiant_AEE(), Sector.HORTICULTURE);
        refSpeciesToSectorDao.createByNaturalId(chrysantheme.getCode_espece_botanique(), chrysantheme.getCode_qualifiant_AEE(), Sector.HORTICULTURE);

        List<EffectiveCropCycleNode> cycleNodes = new ArrayList<>();
        EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        EffectiveCropCycleNode nodeFraisier = effectiveCropCycleNodeDao.create(
                EffectiveCropCycleNode.PROPERTY_TOPIA_ID, "NODE_FRAISIER",
                EffectiveCropCycleNode.PROPERTY_RANK, 0,
                EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY, cultureFraisier,
                EffectiveCropCycleNode.PROPERTY_EDAPLOS_ISSUER_ID, "Edaplos_node0_id"
        );
        cycleNodes.add(nodeFraisier);

        EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao = getPersistenceContext().getEffectiveSeasonalCropCycleDao();
        effectiveSeasonalCropCycleDao.create(
                EffectiveSeasonalCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectiveSeasonalCropCycle.PROPERTY_NODES, cycleNodes);

        EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionDao = getPersistenceContext().getEffectiveCropCycleConnectionDao();
        effectiveCropCycleConnectionDao.create(
                EffectiveCropCycleConnection.PROPERTY_TARGET, nodeFraisier,
                EffectiveCropCycleConnection.PROPERTY_TARGET, nodeFraisier
        );

        return d0;
    }

    private void createI8Intervention(EffectiveCropCycleNode nodeFraisier, Domain d0) {

        RefInterventionAgrosystTravailEDI aat27 = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("AAT27").findUnique();

        RefSubstrateTopiaDao refSubstrateDao = getPersistenceContext().getRefSubstrateDao();

        RefSubstrate substratOrganiqueNaturelBruyere = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique naturel",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Terre de Bruyère",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.VOLUME,
                RefSubstrate.PROPERTY_ACTIVE, true);

        RefSubstrate substratOrganiqueTransformeSciuresBoisCompostees = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique transformé",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Sciures de bois compostées",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.MASS,
                RefSubstrate.PROPERTY_ACTIVE, true);

        RefPrixSubstrateTopiaDao refPrixSubstratesDao = getPersistenceContext().getRefPrixSubstrateDao();
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 20.0, PriceUnit.EURO_M3, 2017);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 50.0, PriceUnit.EURO_M3, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelBruyere.getCaracteristic1(), substratOrganiqueNaturelBruyere.getCaracteristic2(), 23.0, PriceUnit.EURO_M3, 2018);

        // Substrat organique transformé - Sciures de bois compostées
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1(), substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(), 0.04, PriceUnit.EURO_KG, 2017);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1(), substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(), 15.0, PriceUnit.EURO_T, 2016);

        CroppingPlanEntry cropFraisier = nodeFraisier.getCroppingPlanEntry();
        List<CroppingPlanSpecies> agrosystSpecies = cropFraisier.getCroppingPlanSpecies();
        List<EffectiveSpeciesStade> i8SpeciesStades = new ArrayList<>();
        for (CroppingPlanSpecies especeFraisier : agrosystSpecies) {
            EffectiveSpeciesStade i8SpeciesStadeFraisier = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especeFraisier);
            i8SpeciesStades.add(i8SpeciesStadeFraisier);
        }
        // psci transitCount * spatialFrequency
        EffectiveIntervention i8 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, nodeFraisier,
                EffectiveIntervention.PROPERTY_NAME, "i8_fraisier",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.AUTRE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 4, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 4, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.5,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.5,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, i8SpeciesStades
        );

        String objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueNaturelBruyere);
        var substratePrice1 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueNaturelBruyere.getCaracteristic1() + " - " + substratOrganiqueNaturelBruyere.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, 10.0
        );
        var substrateInput1 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueNaturelBruyere.getCaracteristic1() + " - " + substratOrganiqueNaturelBruyere.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.L_M2,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueNaturelBruyere,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice1,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage1 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput1,
                AbstractInputUsage.PROPERTY_QT_AVG, 20.0
        );

        objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueTransformeSciuresBoisCompostees);
        var substratePrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1() + " - " + substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, 0.05
        );
        var substrateInput2 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic1() + " - " + substratOrganiqueTransformeSciuresBoisCompostees.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.KG_M2,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueTransformeSciuresBoisCompostees,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage2 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput2,
                AbstractInputUsage.PROPERTY_QT_AVG, 5.0
        );

        //Cas particulier des intrants "SUBSTRAT":
        //ils sont conditionnés au fait qu'une action Autre à comme action principale (référentiel "Intervention Agrosyst Travail EDI") un reference_code égal à 'AAT27', 'AAT20' ou 'AAT04'
        OtherActionTopiaDao otherActionDao = getPersistenceContext().getOtherActionDao();
        otherActionDao.create(
                OtherAction.PROPERTY_EFFECTIVE_INTERVENTION, i8,
                OtherAction.PROPERTY_MAIN_ACTION, aat27,
                OtherAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_POT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_SUBSTRATE_INPUT_USAGES, List.of(substrateUsage1, substrateUsage2)
        );
    }

    private void createI9Intervention(EffectiveCropCycleNode nodeFraisier, Domain d0) {

        RefInterventionAgrosystTravailEDI aat27 = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("AAT27").findUnique();

        RefPotTopiaDao refPotDao = getPersistenceContext().getRefPotDao();
        RefPot potGrand = refPotDao.create(
                RefPot.PROPERTY_CARACTERISTIC1, "Pot grand (5 L)",
                RefPot.PROPERTY_VOLUME, 5.0,
                RefPot.PROPERTY_ACTIVE, true
        );

        RefPot potPetit = refPotDao.create(
                RefPot.PROPERTY_CARACTERISTIC1, "Pot petit (5 L)",
                RefPot.PROPERTY_VOLUME, 0.1,
                RefPot.PROPERTY_ACTIVE, true
        );

        RefPot potMoyen = refPotDao.create(
                RefPot.PROPERTY_CARACTERISTIC1, "Pot moyen (0.5 L)",
                RefPot.PROPERTY_VOLUME, 0.5,
                RefPot.PROPERTY_ACTIVE, true
        );

        RefPrixPotTopiaDao refPrixPotDao = getPersistenceContext().getRefPrixPotDao();
        //RefPrixPot
        // Pot grand (5 L)
        createRefPrixPot(potGrand, refPrixPotDao, 0.4, 2017);
        createRefPrixPot(potGrand, refPrixPotDao, 0.6, 2016);
        // Pot petit (0.1 L)
        createRefPrixPot(potPetit, refPrixPotDao, 0.02, 2017);
        createRefPrixPot(potPetit, refPrixPotDao, 0.04, 2016);
        // Pot moyen (0.5 L)
        createRefPrixPot(potMoyen, refPrixPotDao, 0.07, 2017);
        createRefPrixPot(potMoyen, refPrixPotDao, 0.09, 2016);

        RefSubstrateTopiaDao refSubstrateDao = getPersistenceContext().getRefSubstrateDao();

        RefSubstrate substratOrganiqueNaturelVegetal = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique naturel",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Terre végétale",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.VOLUME,
                RefSubstrate.PROPERTY_ACTIVE, true
        );

        RefSubstrate substratOrganiqueNaturelFibresNoixCoco = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique naturel",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Fibres de noix de coco",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.MASS,
                RefSubstrate.PROPERTY_ACTIVE, true
        );

        RefPrixSubstrateTopiaDao refPrixSubstratesDao = getPersistenceContext().getRefPrixSubstrateDao();
        // Substrat organique naturel - Terre végétale
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 30.0, PriceUnit.EURO_M3, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 24.0, PriceUnit.EURO_M3, 2017);

        // Substrat organique naturel - Fibres de noix de coco
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelFibresNoixCoco.getCaracteristic1(), substratOrganiqueNaturelFibresNoixCoco.getCaracteristic2(), 2.1, PriceUnit.EURO_KG, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelFibresNoixCoco.getCaracteristic1(), substratOrganiqueNaturelFibresNoixCoco.getCaracteristic2(), 1.8, PriceUnit.EURO_KG, 2017);

        // Substrat organique naturel - Terre végétale
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 30.0, PriceUnit.EURO_M3, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 24.0, PriceUnit.EURO_M3, 2017);

        //Cas particulier des intrants "POT":
        //ils sont conditionnés au fait qu'une action Autre à comme action principale (référentiel "Intervention Agrosyst Travail EDI") un reference_code égal à 'AAT20' ou 'AAT04'
        CroppingPlanEntry cropFraisier = nodeFraisier.getCroppingPlanEntry();
        List<CroppingPlanSpecies> agrosystSpecies = cropFraisier.getCroppingPlanSpecies();
        List<EffectiveSpeciesStade> i9SpeciesStades = new ArrayList<>();
        for (CroppingPlanSpecies especeFraisier : agrosystSpecies) {
            EffectiveSpeciesStade i9SpeciesStadeFraisier = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especeFraisier);
            i9SpeciesStades.add(i9SpeciesStadeFraisier);
        }

        EffectiveIntervention i9 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, nodeFraisier,
                EffectiveIntervention.PROPERTY_NAME, "i9_fraisier",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.AUTRE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 5, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 5, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.5,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, i9SpeciesStades
        );

        String objectId = InputPriceService.GET_REF_POT_OBJECT_ID.apply(potGrand);
        var potPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_POT,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.POT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, potGrand.getCaracteristic1(),
                InputPrice.PROPERTY_PRICE, 0.5
        );
        var potInput = potInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.POT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainPotInput.PROPERTY_INPUT_NAME, potGrand.getCaracteristic1(),
                DomainPotInput.PROPERTY_USAGE_UNIT, PotInputUnit.POTS_M2,
                DomainPotInput.PROPERTY_REF_INPUT, potGrand,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, potPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var potUsage = potInputUsageDao.create(
                PotInputUsage.PROPERTY_INPUT_TYPE, InputType.POT,
                PotInputUsage.PROPERTY_DOMAIN_POT_INPUT, potInput,
                AbstractInputUsage.PROPERTY_QT_AVG, 5.0
        );

        objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueNaturelVegetal);
        var substratePrice1 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueNaturelVegetal.getCaracteristic1() + " - " + substratOrganiqueNaturelVegetal.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, 25.0
        );
        var substrateInput1 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueNaturelVegetal.getCaracteristic1() + " - " + substratOrganiqueNaturelVegetal.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.L_POT,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueNaturelVegetal,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice1,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage1 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput1,
                AbstractInputUsage.PROPERTY_QT_AVG, 4.0
        );

        objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueNaturelFibresNoixCoco);
        var substratePrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueNaturelFibresNoixCoco.getCaracteristic1() + " - " + substratOrganiqueNaturelFibresNoixCoco.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, 2.0
        );
        var substrateInput2 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueNaturelFibresNoixCoco.getCaracteristic1() + " - " + substratOrganiqueNaturelFibresNoixCoco.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.G_POT,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueNaturelFibresNoixCoco,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage2 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput2,
                AbstractInputUsage.PROPERTY_QT_AVG, 500.0
        );

        //Cas particulier des intrants "SUBSTRAT":
        //ils sont conditionnés au fait qu'une action Autre à comme action principale (référentiel "Intervention Agrosyst Travail EDI") un reference_code égal à 'AAT27', 'AAT20' ou 'AAT04'
        OtherActionTopiaDao otherActionDao = getPersistenceContext().getOtherActionDao();
        otherActionDao.create(
                OtherAction.PROPERTY_EFFECTIVE_INTERVENTION, i9,
                OtherAction.PROPERTY_MAIN_ACTION, aat27,
                OtherAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_POT_INPUT_USAGES, List.of(potUsage),
                OtherAction.PROPERTY_SUBSTRATE_INPUT_USAGES, List.of(substrateUsage1, substrateUsage2)
        );
    }

    private void createI10Intervention(EffectiveCropCycleNode nodeFraisier, Domain d0) {
        RefInterventionAgrosystTravailEDI aat27 = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("AAT27").findUnique();

        RefPotTopiaDao refPotDao = getPersistenceContext().getRefPotDao();
        RefPot potGrand = refPotDao.create(
                RefPot.PROPERTY_CARACTERISTIC1, "Pot grand (5 L)",
                RefPot.PROPERTY_VOLUME, 5.0,
                RefPot.PROPERTY_ACTIVE, true
        );

        RefPot potPetit = refPotDao.create(
                RefPot.PROPERTY_CARACTERISTIC1, "Pot petit (0.1 L)",
                RefPot.PROPERTY_VOLUME, 0.1,
                RefPot.PROPERTY_ACTIVE, true
        );

        RefPot potMoyen = refPotDao.create(
                RefPot.PROPERTY_CARACTERISTIC1, "Pot moyen (0.5 L)",
                RefPot.PROPERTY_VOLUME, 0.5,
                RefPot.PROPERTY_ACTIVE, true
        );

        RefPrixPotTopiaDao refPrixPotDao = getPersistenceContext().getRefPrixPotDao();
        //RefPrixPot
        // Pot petit (0.1 L)
        createRefPrixPot(potPetit, refPrixPotDao, 0.02, 2017);
        createRefPrixPot(potPetit, refPrixPotDao, 0.04, 2016);
        // Pot moyen (0.5 L)
        createRefPrixPot(potMoyen, refPrixPotDao, 0.09, 2017);
        createRefPrixPot(potMoyen, refPrixPotDao, 0.05, 2016);
        // Pot grand (5 L)
        createRefPrixPot(potGrand, refPrixPotDao, 0.4, 2017);
        createRefPrixPot(potGrand, refPrixPotDao, 0.6, 2016);

        RefSubstrateTopiaDao refSubstrateDao = getPersistenceContext().getRefSubstrateDao();

        RefSubstrate substratOrganiqueNaturelVegetal = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique naturel",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Terre végétale",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.VOLUME,
                RefSubstrate.PROPERTY_ACTIVE, true
        );

        RefSubstrate substratOrganiqueNaturelFibresNoixCoco = refSubstrateDao.create(
                RefSubstrate.PROPERTY_CARACTERISTIC1, "Substrat organique naturel",
                RefSubstrate.PROPERTY_CARACTERISTIC2, "Fibres de noix de coco",
                RefSubstrate.PROPERTY_UNIT_TYPE, UnitType.MASS,
                RefSubstrate.PROPERTY_ACTIVE, true
        );

        RefPrixSubstrateTopiaDao refPrixSubstratesDao = getPersistenceContext().getRefPrixSubstrateDao();
        // Substrat organique naturel - Terre végétale
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 30.0, PriceUnit.EURO_M3, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 24.0, PriceUnit.EURO_M3, 2017);

        // Substrat organique naturel - Fibres de noix de coco
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelFibresNoixCoco.getCaracteristic1(), substratOrganiqueNaturelFibresNoixCoco.getCaracteristic2(), 2.1, PriceUnit.EURO_KG, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelFibresNoixCoco.getCaracteristic1(), substratOrganiqueNaturelFibresNoixCoco.getCaracteristic2(), 1.8, PriceUnit.EURO_KG, 2017);

        // Substrat organique naturel - Terre végétale
        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 30.0, PriceUnit.EURO_M3, 2016);

        createRefPrixSubstrate(refPrixSubstratesDao, substratOrganiqueNaturelVegetal.getCaracteristic1(), substratOrganiqueNaturelVegetal.getCaracteristic2(), 24.0, PriceUnit.EURO_M3, 2017);

        //Cas particulier des intrants "POT":
        //ils sont conditionnés au fait qu'une action Autre à comme action principale (référentiel "Intervention Agrosyst Travail EDI") un reference_code égal à 'AAT20' ou 'AAT04'
        CroppingPlanEntry cropFraisier = nodeFraisier.getCroppingPlanEntry();
        List<CroppingPlanSpecies> agrosystSpecies = cropFraisier.getCroppingPlanSpecies();
        List<EffectiveSpeciesStade> i9SpeciesStades = new ArrayList<>();
        for (CroppingPlanSpecies especeFraisier : agrosystSpecies) {
            EffectiveSpeciesStade i9SpeciesStadeFraisier = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especeFraisier);
            i9SpeciesStades.add(i9SpeciesStadeFraisier);
        }

        EffectiveIntervention i10 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, nodeFraisier,
                EffectiveIntervention.PROPERTY_NAME, "i10_fraisier",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.AUTRE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 5, 2),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 5, 2),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.5,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, i9SpeciesStades
        );

        String objectId = InputPriceService.GET_REF_POT_OBJECT_ID.apply(potPetit);
        var potPrice1 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_POT,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.POT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, potPetit.getCaracteristic1(),
                InputPrice.PROPERTY_PRICE, 0.05
        );
        var potInput1 = potInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.POT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainPotInput.PROPERTY_INPUT_NAME, potPetit.getCaracteristic1(),
                DomainPotInput.PROPERTY_USAGE_UNIT, PotInputUnit.POTS_M2,
                DomainPotInput.PROPERTY_REF_INPUT, potPetit,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, potPrice1,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var potUsage1 = potInputUsageDao.create(
                PotInputUsage.PROPERTY_INPUT_TYPE, InputType.POT,
                PotInputUsage.PROPERTY_DOMAIN_POT_INPUT, potInput1,
                AbstractInputUsage.PROPERTY_QT_AVG, 25.0
        );

        objectId = InputPriceService.GET_REF_POT_OBJECT_ID.apply(potMoyen);
        var potPrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_POT,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.POT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, potMoyen.getCaracteristic1(),
                InputPrice.PROPERTY_PRICE, 0.1
        );
        var potInput2 = potInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.POT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainPotInput.PROPERTY_INPUT_NAME, potMoyen.getCaracteristic1(),
                DomainPotInput.PROPERTY_USAGE_UNIT, PotInputUnit.POTS_M2,
                DomainPotInput.PROPERTY_REF_INPUT, potMoyen,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, potPrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var potUsage2 = potInputUsageDao.create(
                PotInputUsage.PROPERTY_INPUT_TYPE, InputType.POT,
                PotInputUsage.PROPERTY_DOMAIN_POT_INPUT, potInput2,
                AbstractInputUsage.PROPERTY_QT_AVG, 10.0
        );

        objectId = InputPriceService.GET_REF_SUBSTRAT_OBJECT_ID.apply(substratOrganiqueNaturelVegetal);
        var substratePrice1 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, d0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SUBSTRATE_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, substratOrganiqueNaturelVegetal.getCaracteristic1() + " - " + substratOrganiqueNaturelVegetal.getCaracteristic2(),
                InputPrice.PROPERTY_PRICE, 25.0
        );
        var substrateInput1 = substrateInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, d0,
                DomainSubstrateInput.PROPERTY_INPUT_NAME, substratOrganiqueNaturelVegetal.getCaracteristic1() + " - " + substratOrganiqueNaturelVegetal.getCaracteristic2(),
                DomainSubstrateInput.PROPERTY_USAGE_UNIT, SubstrateInputUnit.L_POT,
                DomainSubstrateInput.PROPERTY_REF_INPUT, substratOrganiqueNaturelVegetal,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, substratePrice1,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var substrateUsage1 = substrateInputUsageDao.create(
                SubstrateInputUsage.PROPERTY_INPUT_TYPE, InputType.SUBSTRAT,
                SubstrateInputUsage.PROPERTY_DOMAIN_SUBSTRATE_INPUT, substrateInput1,
                AbstractInputUsage.PROPERTY_QT_AVG, 0.2
        );

        //Cas particulier des intrants "SUBSTRAT":
        //ils sont conditionnés au fait qu'une action Autre à comme action principale (référentiel "Intervention Agrosyst Travail EDI") un reference_code égal à 'AAT27', 'AAT20' ou 'AAT04'
        OtherActionTopiaDao otherActionDao = getPersistenceContext().getOtherActionDao();
        AbstractAction actionAutreFraisierI10 = otherActionDao.create(
                OtherAction.PROPERTY_EFFECTIVE_INTERVENTION, i10,
                OtherAction.PROPERTY_MAIN_ACTION, aat27,
                OtherAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList(),
                OtherAction.PROPERTY_POT_INPUT_USAGES, List.of(potUsage1, potUsage2),
                OtherAction.PROPERTY_SUBSTRATE_INPUT_USAGES, List.of(substrateUsage1)
        );
    }

    private void createRefPrixPot(RefPot potGrand, RefPrixPotTopiaDao refPrixPotDao, double v, int i) {
        refPrixPotDao.create(
                RefPrixPot.PROPERTY_CARACTERISTIC1, potGrand.getCaracteristic1(),
                RefInputPrice.PROPERTY_PRICE, v,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_POT,
                RefInputPrice.PROPERTY_CAMPAIGN, i,
                RefInputPrice.PROPERTY_CODE_SCENARIO, null,
                RefInputPrice.PROPERTY_SCENARIO, null,
                RefInputPrice.PROPERTY_ACTIVE, true);
    }

    private void createRefPrixSubstrate(RefPrixSubstrateTopiaDao refPrixSubstratesDao, String caracteristic1, String caracteristic2, double v, PriceUnit euroM3, int i) {
        refPrixSubstratesDao.create(
                RefPrixSubstrate.PROPERTY_CARACTERISTIC1, caracteristic1,
                RefPrixSubstrate.PROPERTY_CARACTERISTIC2, caracteristic2,
                RefInputPrice.PROPERTY_PRICE, v,
                RefInputPrice.PROPERTY_UNIT, euroM3,
                RefInputPrice.PROPERTY_CAMPAIGN, i,
                RefInputPrice.PROPERTY_CODE_SCENARIO, null,
                RefInputPrice.PROPERTY_SCENARIO, null,
                RefInputPrice.PROPERTY_ACTIVE, true);
    }

    @Test
    public void createInraeStandardisedGrossIncome_intervention8_10936() throws IOException {
        testDatas.createDemoRefLocation();
        testDatas.importSolTextureGeppa();
        testDatas.importSolProfondeurIndigo();
        testDatas.importRefInputUnitPriceUnitConverterCSV();
        //RefInputUnitPriceUnitConverter
        testDatas.createRefInputUnitPriceUnitConverter();

        Domain d0 = getRef10936DataTest();

        EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        EffectiveCropCycleNode nodeFraisier = effectiveCropCycleNodeDao.forTopiaIdEquals("NODE_FRAISIER").findUnique();
        createI8Intervention(nodeFraisier, d0);

        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(false);
        indicatorFilter.setWithoutAutoConsumed(false);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);
        indicatorOperatingExpenses.init(indicatorFilter);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content)
                .contains("Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Charges opérationnelles réelles (€/ha);2017;Baulon;2250.0;");
        assertThat(content)
                .contains("Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);2017;Baulon;3000.0;100;");
    }

    @Test
    public void createInraeStandardisedGrossIncome_Intervention9_10936() throws IOException {
        testDatas.createDemoRefLocation();
        testDatas.importSolTextureGeppa();
        testDatas.importSolProfondeurIndigo();
        testDatas.importRefInputUnitPriceUnitConverterCSV();
        //RefInputUnitPriceUnitConverter
        testDatas.createRefInputUnitPriceUnitConverter();

        Domain d0 = getRef10936DataTest();

        EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        EffectiveCropCycleNode nodeFraisier = effectiveCropCycleNodeDao.forTopiaIdEquals("NODE_FRAISIER").findUnique();
        createI9Intervention(nodeFraisier, d0);

        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(false);
        indicatorFilter.setWithoutAutoConsumed(false);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);
        indicatorOperatingExpenses.init(indicatorFilter);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content)
                .contains("Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Charges opérationnelles réelles (€/ha);2017;Baulon;80000.0;100;");
        assertThat(content)
                .contains("Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);2017;Baulon;69800.0;");
    }

    //createI10Intervention
    @Test
    public void createInraeStandardisedGrossIncome_Intervention10_10936() throws IOException {
        testDatas.createDemoRefLocation();
        testDatas.importSolTextureGeppa();
        testDatas.importSolProfondeurIndigo();
        testDatas.importRefInputUnitPriceUnitConverterCSV();
        //RefInputUnitPriceUnitConverter
        testDatas.createRefInputUnitPriceUnitConverter();

        Domain d0 = getRef10936DataTest();

        EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        EffectiveCropCycleNode nodeFraisier = effectiveCropCycleNodeDao.forTopiaIdEquals("NODE_FRAISIER").findUnique();

        createI10Intervention(nodeFraisier, d0);

        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(false);
        indicatorFilter.setWithoutAutoConsumed(false);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);

        IndicatorOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorOperatingExpenses.class);
        indicatorOperatingExpenses.init(indicatorFilter);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content)
                .contains("Baulon;gsName;Plot Baulon 2;Zone principale;Culture Fraisier (rang 1);;précédent non renseigné;i10_fraisier;Indicateur économique;Charges opérationnelles réelles (€/ha);2017;Baulon;24250.0;100;;");
        assertThat(content)
                .contains("Baulon;gsName;Plot Baulon 2;Zone principale;Culture Fraisier (rang 1);;précédent non renseigné;i10_fraisier;Indicateur économique;Charges opérationnelles standardisées, millésimé (€/ha);2017;Baulon;15680.0;100;;");
    }
}
