package fr.inra.agrosyst.services.users;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.security.AuthenticationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.api.services.users.UserDto;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

/**
 * @author cosse
 */
public class UserServiceTest extends AbstractAgrosystTest {

    UserService service;
    AuthenticationService authService;

    @BeforeEach
    public void setupServices() throws TopiaException {
        service = serviceFactory.newService(UserService.class);
        authService = serviceFactory.newService(AuthenticationService.class);

        TestDatas testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.createTestUsers();
    }

    @Test
    public void testLoadUser() {
        AuthenticatedUser user = authService.login("jenvoie@atoit.fr", "passera");
        Assertions.assertNotNull(user);
    }

    @Test
    public void testCreateValidUser() {
        UserDto user = new UserDto();
        user.setEmail("beta@tester.com");
        user.setFirstName("bebe");
        user.setLastName("tata");
        UserDto updatedUser = service.createUser(user, "betaTesterPassword");
        Assertions.assertEquals(updatedUser.getEmail(), user.getEmail());
    }

    @Test
    public void testEmailInUse() {
        Assertions.assertTrue(service.isEmailInUse("jenvoie@atoit.fr", null));
        AuthenticatedUser user = authService.login("jenvoie@atoit.fr", "passera");
        Assertions.assertFalse(service.isEmailInUse("jenvoie@atoit.fr", user.getTopiaId()));
        Assertions.assertFalse(service.isEmailInUse("pouette@toto.fr", null));
        Assertions.assertFalse(service.isEmailInUse("pouette@toto.fr", "azertyuj"));
    }

}
