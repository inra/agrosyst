package fr.inra.agrosyst.services.report;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.CategoryObjective;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeImpl;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.Section;
import fr.inra.agrosyst.api.entities.managementmode.SectionImpl;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.Strategy;
import fr.inra.agrosyst.api.entities.managementmode.StrategyImpl;
import fr.inra.agrosyst.api.entities.referential.RefAdventiceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.report.ArboAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboAdventiceMasterImpl;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropAdventiceMasterImpl;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMaster;
import fr.inra.agrosyst.api.entities.report.ArboCropPestMasterImpl;
import fr.inra.agrosyst.api.entities.report.ArboPestMaster;
import fr.inra.agrosyst.api.entities.report.ArboPestMasterImpl;
import fr.inra.agrosyst.api.entities.report.CropPestMaster;
import fr.inra.agrosyst.api.entities.report.CropPestMasterImpl;
import fr.inra.agrosyst.api.entities.report.DamageLevel;
import fr.inra.agrosyst.api.entities.report.DiseaseAttackRate;
import fr.inra.agrosyst.api.entities.report.FoodMaster;
import fr.inra.agrosyst.api.entities.report.FoodMasterImpl;
import fr.inra.agrosyst.api.entities.report.GlobalMasterLevelQualifier;
import fr.inra.agrosyst.api.entities.report.IftEstimationMethod;
import fr.inra.agrosyst.api.entities.report.MasterScale;
import fr.inra.agrosyst.api.entities.report.PestMaster;
import fr.inra.agrosyst.api.entities.report.PestMasterImpl;
import fr.inra.agrosyst.api.entities.report.PestMasterLevelQualifier;
import fr.inra.agrosyst.api.entities.report.PestPressure;
import fr.inra.agrosyst.api.entities.report.PestPressureImpl;
import fr.inra.agrosyst.api.entities.report.PressureEvolution;
import fr.inra.agrosyst.api.entities.report.PressureScale;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystemImpl;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.report.ReportRegional;
import fr.inra.agrosyst.api.entities.report.ReportRegionalImpl;
import fr.inra.agrosyst.api.entities.report.ReportRegionalTopiaDao;
import fr.inra.agrosyst.api.entities.report.SectorSpecies;
import fr.inra.agrosyst.api.entities.report.StressLevel;
import fr.inra.agrosyst.api.entities.report.VerseMaster;
import fr.inra.agrosyst.api.entities.report.VerseMasterImpl;
import fr.inra.agrosyst.api.entities.report.VitiPestMaster;
import fr.inra.agrosyst.api.entities.report.VitiPestMasterImpl;
import fr.inra.agrosyst.api.entities.report.YieldLoss;
import fr.inra.agrosyst.api.entities.report.YieldLossCause;
import fr.inra.agrosyst.api.entities.report.YieldLossImpl;
import fr.inra.agrosyst.api.entities.report.YieldObjective;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.report.ReportExportOption;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemCollections;
import fr.inra.agrosyst.api.services.report.ReportGrowingSystemSection;
import fr.inra.agrosyst.api.services.report.ReportService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.EntityUsageService;
import fr.inra.agrosyst.services.common.EntityUsageServiceMock;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class ReportServiceTest extends AbstractAgrosystTest {

    protected ReportService reportService;
    protected DomainService domainService;
    protected GrowingSystemService growingSystemService;
    protected final ReportPdfExport reportPdfExport = new ReportPdfExport();
    protected ReportXlsExport reportXlsExport;
    protected AgrosystI18nService i18nService;
    protected ReferentialService referentialService;
    protected TestDatas testDatas;

    protected CroppingPlanEntryTopiaDao croppingPlanEntryTopiaDao;
    protected RefNuisibleEDITopiaDao refNuisibleEDIDao;
    protected RefAdventiceTopiaDao refAdventiceTopiaDao;

    protected ManagementModeTopiaDao managementModeDao;

    protected NetworkTopiaDao networkTopiaDao;
    protected DomainTopiaDao domainTopiaDao;
    protected GrowingPlanTopiaDao growingPlanTopiaDao;
    protected GrowingSystemTopiaDao growingSystemTopiaDao;
    protected ReportGrowingSystemTopiaDao reportGrowingSystemTopiaDao;
    protected ReportRegionalTopiaDao reportRegionalTopiaDao;

    protected RefNuisibleEDI maladieAlternariose;
    protected RefNuisibleEDI ravageurAcarien;
    protected RefBioAgressor adventiceBrome;

    protected static final String LOREM_COMMENT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec consequat maximus ex nec fermentum. ";

    @BeforeEach
    public void prepareTest() throws IOException {
        testDatas = serviceFactory.newInstance(TestDatas.class);
        i18nService = serviceFactory.newService(AgrosystI18nService.class);
        reportService = serviceFactory.newService(ReportService.class);
        domainService = serviceContext.newService(DomainService.class);
        growingSystemService = serviceContext.newService(GrowingSystemService.class);
        referentialService = serviceContext.newService(ReferentialService.class);

        croppingPlanEntryTopiaDao = getPersistenceContext().getCroppingPlanEntryDao();
        refNuisibleEDIDao = getPersistenceContext().getRefNuisibleEDIDao();
        refAdventiceTopiaDao = getPersistenceContext().getRefAdventiceDao();
        reportGrowingSystemTopiaDao = getPersistenceContext().getReportGrowingSystemDao();
        reportRegionalTopiaDao = getPersistenceContext().getReportRegionalDao();
        networkTopiaDao =  getPersistenceContext().getNetworkDao();
        managementModeDao = getPersistenceContext().getManagementModeDao();
        domainTopiaDao = getPersistenceContext().getDomainDao();
        growingPlanTopiaDao = getPersistenceContext().getGrowingPlanDao();
        growingSystemTopiaDao = getPersistenceContext().getGrowingSystemDao();

        testDatas.createTestGrowingSystems();
        testDatas.createTestNetworks();
        testDatas.createTestManagementModes();
        testDatas.importNuisiblesEDI();
        testDatas.importAdventices();

        maladieAlternariose = refNuisibleEDIDao.forReference_labelEquals("Alternariose de la carotte").findUnique();
        ravageurAcarien = refNuisibleEDIDao.forReference_labelEquals("Acarien").findUnique();
        adventiceBrome = refAdventiceTopiaDao.forAdventiceEquals("Brome mou").findUnique();

        alterSchema();
        reportXlsExport = new ReportXlsExport(referentialService.getGroupesCiblesParCode());
    }

    protected ReportRegional createReportRegional() {

        ReportRegional reportRegional = new ReportRegionalImpl();
        reportRegional.setCode(UUID.randomUUID().toString());
        reportRegional.addAllSectors(Arrays.asList(Sector.MARAICHAGE, Sector.ARBORICULTURE, Sector.VITICULTURE));
        reportRegional.addSectorSpecies(SectorSpecies.POMMIER_POIRIER);
        reportRegional.setName("Test regional");
        reportRegional.setCampaign(2017);
        reportRegional.setAuthor("Me");
        reportRegional.setHighlights("Faits marquants");
        reportRegional.setNumberOfDaysWithPrimaryContaminations(42);
        reportRegional.addNetworks(networkTopiaDao.forNameEquals("R0").findUnique());

        PestPressure pestPressure1 = new PestPressureImpl();
        pestPressure1.addAgressors(ravageurAcarien);
        pestPressure1.setCrops("Blé noir (pour les galettes)");
        pestPressure1.setPressureEvolution(PressureEvolution.HIGHER);
        pestPressure1.setPressureScale(PressureScale.NONE);
        pestPressure1.setComment(LOREM_COMMENT);
        reportRegional.addPestPressures(pestPressure1);

        PestPressure diseasePressure1 = new PestPressureImpl();
        diseasePressure1.addAgressors(maladieAlternariose);
        diseasePressure1.setCrops("Blé blanc");
        diseasePressure1.setPressureEvolution(PressureEvolution.MUCH_HIGHER);
        diseasePressure1.setPressureScale(PressureScale.MODERATE);
        diseasePressure1.setComment(LOREM_COMMENT);
        reportRegional.addDiseasePressures(diseasePressure1);

        return reportRegionalTopiaDao.create(reportRegional);
    }

    protected ReportGrowingSystem createReportGrowingSystem() {
        ReportGrowingSystem reportGrowingSystem = new ReportGrowingSystemImpl();
        reportGrowingSystem.setCode(UUID.randomUUID().toString());
        reportGrowingSystem.addAllSectors(Arrays.asList(Sector.MARAICHAGE, Sector.ARBORICULTURE, Sector.VITICULTURE));
        reportGrowingSystem.setName("Test BdC");
        reportGrowingSystem.setAuthor("me");

        // regional
        reportGrowingSystem.setReportRegional(createReportRegional());
        reportGrowingSystem.setHighlightsEvolutions(Strings.repeat(LOREM_COMMENT, 4));
        reportGrowingSystem.setHighlightsMeasures(Strings.repeat(LOREM_COMMENT, 5));
        reportGrowingSystem.setHighlightsPerformances(Strings.repeat(LOREM_COMMENT, 6));
        reportGrowingSystem.setHighlightsTeachings(Strings.repeat(LOREM_COMMENT, 7));

        // growing system / domain / cropping plan entry
        GrowingSystem growingSystem = growingSystemTopiaDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = growingSystem.getGrowingPlan().getDomain();
        CroppingPlanEntry croppingPlanBle = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAny();
        reportGrowingSystem.setGrowingSystem(growingSystem);

        // highlights
        reportGrowingSystem.setIftEstimationMethod(IftEstimationMethod.ESTIMATED_2012);

        // all adventicePestMasters (1 of 2, 1 of 2)
        CropPestMaster adventicePestMasters1 = new CropPestMasterImpl();
        adventicePestMasters1.addCrops(croppingPlanBle);
        PestMaster adventicePestMaster1 = new PestMasterImpl();
        adventicePestMaster1.setAgressor(adventiceBrome);
        adventicePestMaster1.setMasterScale(MasterScale.HIGH);
        adventicePestMaster1.setPressureScale(PressureScale.HIGH);
        adventicePestMaster1.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        adventicePestMaster1.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster1.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster adventicePestMaster2 = new PestMasterImpl();
        adventicePestMaster2.setAgressor(adventiceBrome);
        adventicePestMaster2.setMasterScale(MasterScale.MODERATE);
        adventicePestMaster2.setPressureScale(PressureScale.LOW);
        adventicePestMaster2.setQualifier(PestMasterLevelQualifier.SATISFAIT);
        adventicePestMaster2.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster2.setPressureFarmerComment(LOREM_COMMENT);
        adventicePestMasters1.addAllPestMasters(Arrays.asList(adventicePestMaster1, adventicePestMaster2));
        adventicePestMasters1.setIftMain(12.0);
        adventicePestMasters1.setIftOther(2.0);
        adventicePestMasters1.setAdviserComments(LOREM_COMMENT);
        adventicePestMasters1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        
        CropPestMaster adventicePestMasters2 = new CropPestMasterImpl();
        adventicePestMasters2.addCrops(croppingPlanBle);
        PestMaster adventicePestMaster3 = new PestMasterImpl();
        adventicePestMaster3.setAgressor(adventiceBrome);
        adventicePestMaster3.setMasterScale(MasterScale.HIGH);
        adventicePestMaster3.setPressureScale(PressureScale.HIGH);
        adventicePestMaster3.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        adventicePestMaster3.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster3.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster adventicePestMaster4 = new PestMasterImpl();
        adventicePestMaster4.setAgressor(adventiceBrome);
        adventicePestMaster4.setMasterScale(MasterScale.HIGH);
        adventicePestMaster4.setPressureScale(PressureScale.HIGH);
        adventicePestMaster4.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        adventicePestMaster4.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster4.setPressureFarmerComment(LOREM_COMMENT);
        adventicePestMasters2.addAllPestMasters(Arrays.asList(adventicePestMaster3, adventicePestMaster4));
        adventicePestMasters2.setIftMain(12.0);
        adventicePestMasters2.setIftOther(2.0);
        adventicePestMasters2.setAdviserComments(LOREM_COMMENT);
        adventicePestMasters2.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        reportGrowingSystem.addAllCropAdventiceMasters(Arrays.asList(adventicePestMasters1, adventicePestMasters2));

        // all diseasePestMasters (1 of 1)
        CropPestMaster diseasePestMasters1 = new CropPestMasterImpl();
        diseasePestMasters1.addCrops(croppingPlanBle);
        PestMaster diseasePestMaster1 = new PestMasterImpl();
        diseasePestMaster1.setAgressor(maladieAlternariose);
        diseasePestMaster1.setMasterScale(MasterScale.HIGH);
        diseasePestMaster1.setPressureScale(PressureScale.HIGH);
        diseasePestMaster1.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        diseasePestMaster1.setResultFarmerComment(LOREM_COMMENT);
        diseasePestMaster1.setPressureFarmerComment(LOREM_COMMENT);
        diseasePestMasters1.setPestMasters(Collections.singleton(diseasePestMaster1));
        diseasePestMasters1.setIftMain(42.0);
        diseasePestMasters1.setIftOther(2.0);
        diseasePestMasters1.setAdviserComments(LOREM_COMMENT);
        diseasePestMasters1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        reportGrowingSystem.setCropDiseaseMasters(Collections.singleton(diseasePestMasters1));

        // all cropPestMasters (1 of 3)
        CropPestMaster pestPestMasters1 = new CropPestMasterImpl();
        pestPestMasters1.addCrops(croppingPlanBle);
        PestMaster pestPestMaster1 = new PestMasterImpl();
        pestPestMaster1.setAgressor(ravageurAcarien);
        pestPestMaster1.setMasterScale(MasterScale.NONE);
        pestPestMaster1.setPressureScale(PressureScale.MODERATE);
        pestPestMaster1.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        pestPestMaster1.setResultFarmerComment(LOREM_COMMENT);
        pestPestMaster1.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster pestPestMaster2 = new PestMasterImpl();
        pestPestMaster2.setAgressor(ravageurAcarien);
        pestPestMaster2.setMasterScale(MasterScale.NONE);
        pestPestMaster2.setPressureScale(PressureScale.HIGH);
        pestPestMaster2.setQualifier(PestMasterLevelQualifier.SATISFAIT);
        pestPestMaster2.setResultFarmerComment(LOREM_COMMENT);
        pestPestMaster2.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster pestPestMaster3 = new PestMasterImpl();
        pestPestMaster3.setAgressor(ravageurAcarien);
        pestPestMaster3.setMasterScale(MasterScale.NONE);
        pestPestMaster3.setPressureScale(PressureScale.HIGH);
        pestPestMaster3.setQualifier(PestMasterLevelQualifier.SATISFAIT);
        pestPestMaster3.setResultFarmerComment(LOREM_COMMENT);
        pestPestMaster3.setPressureFarmerComment(LOREM_COMMENT);
        pestPestMasters1.addAllPestMasters(Arrays.asList(pestPestMaster1, pestPestMaster2, pestPestMaster3));
        pestPestMasters1.setIftMain(5.0);
        pestPestMasters1.setIftOther(2.0);
        pestPestMasters1.setAdviserComments(LOREM_COMMENT);
        pestPestMasters1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        reportGrowingSystem.addCropPestMasters(pestPestMasters1);

        // verse
        VerseMaster verseMaster1 = new VerseMasterImpl();
        verseMaster1.addCrops(croppingPlanBle);
        verseMaster1.setMasterScale(MasterScale.MODERATE);
        verseMaster1.setIftMain(2.0);
        verseMaster1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        reportGrowingSystem.addVerseMasters(verseMaster1);

        // food
        List<FoodMaster> foodMasters = new ArrayList<>();
    
        FoodMaster foodMaster1 = new FoodMasterImpl();
        foodMaster1.addCrops(croppingPlanBle);
        foodMaster1.setAzoteStress(StressLevel.STRESS_IMPACT_YIELD_PROFIT);
        foodMaster1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        foodMasters.add(foodMaster1);

        // yield
        final List<YieldLoss> yieldLosses = new ArrayList<>();
        
        YieldLoss yieldLoss1 = new YieldLossImpl();
        yieldLoss1.setYieldObjective(YieldObjective.FROM_50_TO_75);
        yieldLoss1.addCrops(croppingPlanBle);
        yieldLoss1.setCause1(YieldLossCause.ECHAUDAGE);
        yieldLoss1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        yieldLosses.add(yieldLoss1);
        reportGrowingSystem.setYieldLosses(yieldLosses);

        // arbo disease
        ArboCropPestMaster arboCropDiseaseMaster1 = new ArboCropPestMasterImpl();
        arboCropDiseaseMaster1.addCrops(croppingPlanBle);
        ArboPestMaster arboPestMaster1 = new ArboPestMasterImpl();
        arboPestMaster1.setAgressor(maladieAlternariose);
        arboPestMaster1.setPercentAffectedPlots(DamageLevel.LESS_0_5);
        arboPestMaster1.setResultFarmerComment("Test char bizarre : < > ' \" & $ $$ été à @");
        arboCropDiseaseMaster1.addPestMasters(arboPestMaster1);
        arboCropDiseaseMaster1.setTreatmentCount(1.0);
        arboCropDiseaseMaster1.setChemicalPestIFT(2.0);
        arboCropDiseaseMaster1.setBioControlPestIFT(2.0);
        reportGrowingSystem.addArboCropDiseaseMasters(arboCropDiseaseMaster1);

        // abro adventice
        ArboCropAdventiceMaster arboCropAdventiceMaster1 = new ArboCropAdventiceMasterImpl();
        arboCropAdventiceMaster1.addCrops(croppingPlanBle);
        ArboAdventiceMaster arboAdventiceMaster1 = new ArboAdventiceMasterImpl();
        arboAdventiceMaster1.setAgressor(adventiceBrome);
        arboCropAdventiceMaster1.addPestMasters(arboAdventiceMaster1);
        arboCropAdventiceMaster1.setTreatmentCount(1.0);
        arboCropAdventiceMaster1.setChemicalPestIFT(2.0);
        arboCropAdventiceMaster1.setBioControlPestIFT(2.0);
        reportGrowingSystem.addArboCropAdventiceMasters(arboCropAdventiceMaster1);

        // arbo pest
        ArboCropPestMaster arboCropPestMaster2 = new ArboCropPestMasterImpl();
        arboCropPestMaster2.addCrops(croppingPlanBle);
        ArboPestMaster arboPestMaster2 = new ArboPestMasterImpl();
        arboPestMaster2.setAgressor(ravageurAcarien);
        arboPestMaster2.setPercentAffectedPlots(DamageLevel.MORE_50);
        arboCropPestMaster2.addPestMasters(arboPestMaster2);
        arboCropPestMaster2.setTreatmentCount(1.0);
        arboCropPestMaster2.setChemicalPestIFT(2.0);
        arboCropPestMaster2.setBioControlPestIFT(2.0);
        reportGrowingSystem.addArboCropPestMasters(arboCropPestMaster2);

        // arbo food
        FoodMaster arboFoodMaster = new FoodMasterImpl();
        arboFoodMaster.addCrops(croppingPlanBle);
        arboFoodMaster.setAzoteStress(StressLevel.STRESS_IMPACT_YIELD_PROFIT);
        foodMaster1.setSector(Sector.ARBORICULTURE);
        foodMasters.add(arboFoodMaster);

        // arbo yield
        YieldLoss arboYieldLoss1 = new YieldLossImpl();
        arboYieldLoss1.setYieldObjective(YieldObjective.FROM_50_TO_75);
        arboYieldLoss1.addCrops(croppingPlanBle);
        arboYieldLoss1.setCause1(YieldLossCause.COULURE);
        arboYieldLoss1.setSector(Sector.ARBORICULTURE);
        reportGrowingSystem.getYieldLosses().add(arboYieldLoss1);

        // viti disease
        VitiPestMaster vitiDiseaseMaster1 = new VitiPestMasterImpl();
        vitiDiseaseMaster1.setAgressor(maladieAlternariose);
        vitiDiseaseMaster1.setGrapeDiseaseAttackRate(DiseaseAttackRate.FROM_0_TO_10_2_INTENSITY);
        vitiDiseaseMaster1.setTreatmentCount(1.0);
        vitiDiseaseMaster1.setChemicalFungicideIFT(2.0);
        vitiDiseaseMaster1.setBioControlFungicideIFT(2.0);
        reportGrowingSystem.addVitiDiseaseMasters(vitiDiseaseMaster1);

        // viti pest
        VitiPestMaster vitiPestMaster1 = new VitiPestMasterImpl();
        vitiPestMaster1.setAgressor(maladieAlternariose);
        vitiPestMaster1.setGrapeDiseaseAttackRate(DiseaseAttackRate.FROM_70_TO_100_2_INTENSITY);
        vitiPestMaster1.setTreatmentCount(2.0);
        vitiPestMaster1.setChemicalFungicideIFT(2.0);
        vitiPestMaster1.setBioControlFungicideIFT(2.0);
        reportGrowingSystem.addVitiPestMasters(vitiPestMaster1);

        // viti adventice

        // viti food
        FoodMaster vitiFoodMaster = new FoodMasterImpl();
        vitiFoodMaster.addCrops(croppingPlanBle);
        vitiFoodMaster.setAzoteStress(StressLevel.STRESS_IMPACT_YIELD_PROFIT);
        vitiFoodMaster.setSector(Sector.VITICULTURE);
        foodMasters.add(vitiFoodMaster);
        
        reportGrowingSystem.setFoodMasters(foodMasters);

        // viti yield

        return reportGrowingSystemTopiaDao.create(reportGrowingSystem);
    }

    protected ReportGrowingSystem createReportGrowingSystemFromService() {
        ReportGrowingSystem reportGrowingSystem = new ReportGrowingSystemImpl();
        reportGrowingSystem.setCode(UUID.randomUUID().toString());
        reportGrowingSystem.addAllSectors(Arrays.asList(Sector.GRANDES_CULTURES, Sector.MARAICHAGE, Sector.ARBORICULTURE, Sector.VITICULTURE));
        reportGrowingSystem.setName("Test BdC");
        reportGrowingSystem.setAuthor("me");

        // regional
        ReportRegional reportRegional = createReportRegional();
        //reportGrowingSystem.setReportRegional(reportRegional);
        reportGrowingSystem.setHighlightsEvolutions(Strings.repeat(LOREM_COMMENT, 4));
        reportGrowingSystem.setHighlightsMeasures(Strings.repeat(LOREM_COMMENT, 5));
        reportGrowingSystem.setHighlightsPerformances(Strings.repeat(LOREM_COMMENT, 6));
        reportGrowingSystem.setHighlightsTeachings(Strings.repeat(LOREM_COMMENT, 7));

        // growing system / domain / cropping plan entry
        GrowingSystem growingSystem = growingSystemTopiaDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = growingSystem.getGrowingPlan().getDomain();
        CroppingPlanEntry croppingPlanBle = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAny();
        //reportGrowingSystem.setGrowingSystem(growingSystem);

        // highlights
        reportGrowingSystem.setIftEstimationMethod(IftEstimationMethod.ESTIMATED_2012);

        // all adventicePestMasters (1 of 2, 1 of 2)
        CropPestMaster adventicePestMasters1 = new CropPestMasterImpl();
        adventicePestMasters1.addCrops(croppingPlanBle);
        PestMaster adventicePestMaster1 = new PestMasterImpl();
        adventicePestMaster1.setAgressor(adventiceBrome);
        adventicePestMaster1.setMasterScale(MasterScale.HIGH);
        adventicePestMaster1.setPressureScale(PressureScale.HIGH);
        adventicePestMaster1.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        adventicePestMaster1.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster1.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster adventicePestMaster2 = new PestMasterImpl();
        adventicePestMaster2.setAgressor(adventiceBrome);
        adventicePestMaster2.setMasterScale(MasterScale.MODERATE);
        adventicePestMaster2.setPressureScale(PressureScale.LOW);
        adventicePestMaster2.setQualifier(PestMasterLevelQualifier.SATISFAIT);
        adventicePestMaster2.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster2.setPressureFarmerComment(LOREM_COMMENT);
        adventicePestMasters1.addAllPestMasters(Arrays.asList(adventicePestMaster1, adventicePestMaster2));
        adventicePestMasters1.setIftMain(12.0);
        adventicePestMasters1.setIftOther(2.0);
        adventicePestMasters1.setAdviserComments(LOREM_COMMENT);

        CropPestMaster adventicePestMasters2 = new CropPestMasterImpl();
        adventicePestMasters2.addCrops(croppingPlanBle);
        PestMaster adventicePestMaster3 = new PestMasterImpl();
        adventicePestMaster3.setAgressor(adventiceBrome);
        adventicePestMaster3.setMasterScale(MasterScale.HIGH);
        adventicePestMaster3.setPressureScale(PressureScale.HIGH);
        adventicePestMaster3.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        adventicePestMaster3.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster3.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster adventicePestMaster4 = new PestMasterImpl();
        adventicePestMaster4.setAgressor(adventiceBrome);
        adventicePestMaster4.setMasterScale(MasterScale.HIGH);
        adventicePestMaster4.setPressureScale(PressureScale.HIGH);
        adventicePestMaster4.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        adventicePestMaster4.setResultFarmerComment(LOREM_COMMENT);
        adventicePestMaster4.setPressureFarmerComment(LOREM_COMMENT);
        adventicePestMasters2.addAllPestMasters(Arrays.asList(adventicePestMaster3, adventicePestMaster4));
        adventicePestMasters2.setIftMain(12.0);
        adventicePestMasters2.setIftOther(2.0);
        adventicePestMasters2.setAdviserComments(LOREM_COMMENT);

        // all diseasePestMasters (1 of 1)
        CropPestMaster diseasePestMasters1 = new CropPestMasterImpl();
        diseasePestMasters1.addCrops(croppingPlanBle);
        PestMaster diseasePestMaster1 = new PestMasterImpl();
        diseasePestMaster1.setAgressor(maladieAlternariose);
        diseasePestMaster1.setMasterScale(MasterScale.HIGH);
        diseasePestMaster1.setPressureScale(PressureScale.HIGH);
        diseasePestMaster1.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        diseasePestMaster1.setResultFarmerComment(LOREM_COMMENT);
        diseasePestMaster1.setPressureFarmerComment(LOREM_COMMENT);
        diseasePestMasters1.addPestMasters(diseasePestMaster1);
        diseasePestMasters1.setIftMain(42.0);
        diseasePestMasters1.setIftOther(2.0);
        diseasePestMasters1.setAdviserComments(LOREM_COMMENT);

        // all cropPestMasters (1 of 3)
        CropPestMaster pestPestMasters1 = new CropPestMasterImpl();
        pestPestMasters1.addCrops(croppingPlanBle);
        pestPestMasters1.setSpecies(new ArrayList<>(croppingPlanBle.getCroppingPlanSpecies()));
        PestMaster pestPestMaster1 = new PestMasterImpl();
        pestPestMaster1.setAgressor(ravageurAcarien);
        pestPestMaster1.setMasterScale(MasterScale.NONE);
        pestPestMaster1.setPressureScale(PressureScale.MODERATE);
        pestPestMaster1.setQualifier(PestMasterLevelQualifier.NON_SATISFAIT);
        pestPestMaster1.setResultFarmerComment(LOREM_COMMENT);
        pestPestMaster1.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster pestPestMaster2 = new PestMasterImpl();
        pestPestMaster2.setAgressor(ravageurAcarien);
        pestPestMaster2.setMasterScale(MasterScale.NONE);
        pestPestMaster2.setPressureScale(PressureScale.HIGH);
        pestPestMaster2.setQualifier(PestMasterLevelQualifier.SATISFAIT);
        pestPestMaster2.setResultFarmerComment(LOREM_COMMENT);
        pestPestMaster2.setPressureFarmerComment(LOREM_COMMENT);
        PestMaster pestPestMaster3 = new PestMasterImpl();
        pestPestMaster3.setAgressor(ravageurAcarien);
        pestPestMaster3.setMasterScale(MasterScale.NONE);
        pestPestMaster3.setPressureScale(PressureScale.HIGH);
        pestPestMaster3.setQualifier(PestMasterLevelQualifier.SATISFAIT);
        pestPestMaster3.setResultFarmerComment(LOREM_COMMENT);
        pestPestMaster3.setPressureFarmerComment(LOREM_COMMENT);
        pestPestMasters1.addAllPestMasters(Arrays.asList(pestPestMaster1, pestPestMaster2, pestPestMaster3));
        pestPestMasters1.setIftMain(5.0);
        pestPestMasters1.setIftOther(2.0);
        pestPestMasters1.setAdviserComments(LOREM_COMMENT);

        // verse
        VerseMaster verseMaster1 = new VerseMasterImpl();
        verseMaster1.addCrops(croppingPlanBle);
        verseMaster1.setMasterScale(MasterScale.MODERATE);
        verseMaster1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        verseMaster1.setIftMain(2.0);

        // food
        FoodMaster vitiFoodMaster = new FoodMasterImpl();
        vitiFoodMaster.addCrops(croppingPlanBle);
        vitiFoodMaster.setAzoteStress(StressLevel.STRESS_IMPACT_YIELD_PROFIT);
        vitiFoodMaster.setSector(Sector.VITICULTURE);

        FoodMaster foodMaster2 = new FoodMasterImpl();
        foodMaster2.addCrops(croppingPlanBle);
        foodMaster2.setAzoteStress(StressLevel.STRESS_IMPACT_YIELD_PROFIT);
        foodMaster2.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);

        // yield
        YieldLoss yieldLoss1 = new YieldLossImpl();
        yieldLoss1.setYieldObjective(YieldObjective.FROM_50_TO_75);
        yieldLoss1.addCrops(croppingPlanBle);
        yieldLoss1.setSector(ReportGrowingSystemCollections.NONE_DEPHY_EXPE);
        yieldLoss1.setCause1(YieldLossCause.ECHAUDAGE);

        // arbo disease
        ArboCropPestMaster arboCropDiseaseMaster1 = new ArboCropPestMasterImpl();
        arboCropDiseaseMaster1.addCrops(croppingPlanBle);
        ArboPestMaster arboPestMaster1 = new ArboPestMasterImpl();
        arboPestMaster1.setAgressor(maladieAlternariose);
        arboPestMaster1.setPercentAffectedPlots(DamageLevel.LESS_0_5);
        arboPestMaster1.setResultFarmerComment("Test char bizarre : < > ' \" & $ $$ été à @");
        arboCropDiseaseMaster1.addPestMasters(arboPestMaster1);
        arboCropDiseaseMaster1.setTreatmentCount(1.0);
        arboCropDiseaseMaster1.setChemicalPestIFT(2.0);
        arboCropDiseaseMaster1.setBioControlPestIFT(2.0);

        reportGrowingSystem.setArboChemicalFungicideIFT(1.1);
        reportGrowingSystem.setArboBioControlFungicideIFT(1.2);
        reportGrowingSystem.setArboCopperQuantity(1.3);
        reportGrowingSystem.setArboDiseaseQualifier(GlobalMasterLevelQualifier.SATISFAIT);

        // abro adventice
        ArboCropAdventiceMaster arboCropAdventiceMaster1 = new ArboCropAdventiceMasterImpl();
        arboCropAdventiceMaster1.addCrops(croppingPlanBle);
        ArboAdventiceMaster arboAdventiceMaster1 = new ArboAdventiceMasterImpl();
        arboAdventiceMaster1.setAgressor(adventiceBrome);
        arboCropAdventiceMaster1.addPestMasters(arboAdventiceMaster1);
        arboCropAdventiceMaster1.setTreatmentCount(1.0);
        arboCropAdventiceMaster1.setChemicalPestIFT(2.0);
        arboCropAdventiceMaster1.setBioControlPestIFT(2.0);

        // arbo pest
        ArboCropPestMaster arboCropPestMaster2 = new ArboCropPestMasterImpl();
        arboCropPestMaster2.addCrops(croppingPlanBle);
        ArboPestMaster arboPestMaster2 = new ArboPestMasterImpl();
        arboPestMaster2.setAgressor(ravageurAcarien);
        arboPestMaster2.setPercentAffectedPlots(DamageLevel.MORE_50);
        arboCropPestMaster2.addPestMasters(arboPestMaster2);
        arboCropPestMaster2.setTreatmentCount(1.0);
        arboCropPestMaster2.setChemicalPestIFT(2.0);
        arboCropPestMaster2.setBioControlPestIFT(2.0);

        reportGrowingSystem.setArboChemicalPestIFT(4.5);
        reportGrowingSystem.setArboBioControlPestIFT(5.5);
        reportGrowingSystem.setArboPestQualifier(GlobalMasterLevelQualifier.SATISFAIT);

        // arbo yield
        YieldLoss arboYieldLoss = new YieldLossImpl();
        arboYieldLoss.setYieldObjective(YieldObjective.FROM_50_TO_75);
        arboYieldLoss.addCrops(croppingPlanBle);
        arboYieldLoss.setCause1(YieldLossCause.COULURE);
        arboYieldLoss.setSector(Sector.ARBORICULTURE);

        // viti disease
        VitiPestMaster vitiDiseaseMaster1 = new VitiPestMasterImpl();
        vitiDiseaseMaster1.setAgressor(maladieAlternariose);
        vitiDiseaseMaster1.setGrapeDiseaseAttackRate(DiseaseAttackRate.FROM_0_TO_10_2_INTENSITY);
        vitiDiseaseMaster1.setTreatmentCount(1.0);
        vitiDiseaseMaster1.setChemicalFungicideIFT(2.0);
        vitiDiseaseMaster1.setBioControlFungicideIFT(2.0);

        // viti pest
        VitiPestMaster vitiPestMaster1 = new VitiPestMasterImpl();
        vitiPestMaster1.setAgressor(maladieAlternariose);
        vitiPestMaster1.setGrapeDiseaseAttackRate(DiseaseAttackRate.FROM_70_TO_100_2_INTENSITY);
        vitiPestMaster1.setTreatmentCount(2.0);
        vitiPestMaster1.setChemicalFungicideIFT(2.0);
        vitiPestMaster1.setBioControlFungicideIFT(2.0);

        reportGrowingSystem.setVitiDiseaseChemicalFungicideIFT(6.5);
        reportGrowingSystem.setVitiDiseaseBioControlFungicideIFT(6.6);
        reportGrowingSystem.setVitiDiseaseCopperQuantity(6.7);
        reportGrowingSystem.setVitiDiseaseQualifier(GlobalMasterLevelQualifier.TRES_SATISFAIT);

        reportGrowingSystem.setVitiDiseaseChemicalFungicideIFT(7.0);
        reportGrowingSystem.setVitiDiseaseBioControlFungicideIFT(7.1);
        reportGrowingSystem.setVitiDiseaseCopperQuantity(7.2);
        reportGrowingSystem.setVitiDiseaseQualifier(GlobalMasterLevelQualifier.TRES_SATISFAIT);

        reportGrowingSystem.setVitiPestChemicalPestIFT(6.1);
        reportGrowingSystem.setVitiPestBioControlPestIFT(6.2);
        reportGrowingSystem.setVitiPestQualifier(GlobalMasterLevelQualifier.TRES_SATISFAIT);

        // viti yield
        ReportGrowingSystemCollections reportGrowingSystemCollections = new ReportGrowingSystemCollections();
        
        reportGrowingSystemCollections.addVitiFoodMasters(new ArrayList<>(Collections.singletonList(vitiFoodMaster)));
        reportGrowingSystemCollections.setVitiDiseaseMasters(new ArrayList<>(Collections.singletonList(vitiDiseaseMaster1)));
        reportGrowingSystemCollections.setVitiPestMasters(new ArrayList<>(Collections.singletonList(vitiPestMaster1)));
        reportGrowingSystemCollections.addArboYieldLosses(new ArrayList<>(Collections.singletonList(arboYieldLoss)));
        reportGrowingSystemCollections.setArboPestMasters(new ArrayList<>(Collections.singletonList(arboCropPestMaster2)));
        reportGrowingSystemCollections.setArboDiseaseMasters(new ArrayList<>(Collections.singletonList(arboCropDiseaseMaster1)));
        reportGrowingSystemCollections.setArboAdventiceMasters(new ArrayList<>(Collections.singletonList(arboCropAdventiceMaster1)));
        reportGrowingSystemCollections.addArboFoodMasters(null);
        reportGrowingSystemCollections.addNoneDephyExpeCropAdventiceMasters(new ArrayList<>(Arrays.asList(adventicePestMasters1, adventicePestMasters2)));
        reportGrowingSystemCollections.addNoneDephyExpeCropDiseaseMasters(new ArrayList<>(Collections.singletonList(diseasePestMasters1)));
        reportGrowingSystemCollections.addNoneDephyExpeCropPestMasters(new ArrayList<>(Collections.singletonList(pestPestMasters1)));
        reportGrowingSystemCollections.addNoneDephyExpeFoodMasters(new ArrayList<>(Collections.singletonList(foodMaster2)));
        reportGrowingSystemCollections.addNoneDephyExpeVerseMasters(new ArrayList<>(Collections.singletonList(verseMaster1)));
        reportGrowingSystemCollections.addNoneDephyExpeYieldLosses(new ArrayList<>(Collections.singletonList(yieldLoss1)));

        return reportService.createOrUpdateReportGrowingSystem(
                reportGrowingSystem, growingSystem.getTopiaId(), reportRegional.getTopiaId(),reportGrowingSystemCollections, false);
    }

    protected ManagementMode createManagementMode() {
        // growing system / domain / cropping plan entry
        GrowingSystem growingSystem = growingSystemTopiaDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = growingSystem.getGrowingPlan().getDomain();
        CroppingPlanEntry croppingPlanBle = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAny();

        // new management mode
        ManagementMode managementMode = new ManagementModeImpl();

        Section section1 = new SectionImpl();
        section1.setSectionType(SectionType.ADVENTICES);
        section1.setAgronomicObjective("Objectif ambitieux !");
        section1.setBioAgressor(adventiceBrome);
        section1.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section1.setExpectedResult("Toto");
        Strategy strategy1 = new StrategyImpl();
        strategy1.addCrops(croppingPlanBle);
        Strategy strategy2 = new StrategyImpl();
        strategy2.addCrops(croppingPlanBle);
        strategy2.setExplanation("Test");
        section1.addAllStrategies(Arrays.asList(strategy1, strategy2));

        Section section2 = new SectionImpl();
        section2.setSectionType(SectionType.MALADIES);
        section2.setAgronomicObjective("Objectif ambitieux !");
        section2.setBioAgressor(maladieAlternariose);
        section2.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section2.setExpectedResult("Toto");
        section2.addAllStrategies(Arrays.asList(strategy1, strategy2));

        Section section3 = new SectionImpl();
        section3.setSectionType(SectionType.RAVAGEURS);
        section3.setAgronomicObjective("Objectif ambitieux !");
        section3.setBioAgressor(ravageurAcarien);
        section3.setExpectedResult("Toto");
        section3.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section3.addAllStrategies(Arrays.asList(strategy1, strategy2));

        Section section4 = new SectionImpl();
        section4.setSectionType(SectionType.MAITRISE_DES_DOMMAGES_PHYSIQUES);
        section4.setAgronomicObjective("Objectif ambitieux !");
        section4.setExpectedResult("Toto");
        section4.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section4.addAllStrategies(Arrays.asList(strategy1, strategy2));

        Section section5 = new SectionImpl();
        section5.setSectionType(SectionType.TRAVAIL_DU_SOL);
        section5.setAgronomicObjective("Objectif ambitieux !");
        section5.setExpectedResult("Toto");
        section5.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section5.addAllStrategies(Arrays.asList(strategy1, strategy2));

        Section section6 = new SectionImpl();
        section6.setSectionType(SectionType.CYCLE_PLURIANNUEL_DE_CULTURE);
        section6.setAgronomicObjective("Objectif ambitieux !");
        section6.setExpectedResult("Toto");
        section6.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section6.addAllStrategies(Arrays.asList(strategy1, strategy2));

        Section section7 = new SectionImpl();
        section7.setSectionType(SectionType.PRODUCTION);
        section7.setAgronomicObjective("Objectif ambitieux !");
        section7.setExpectedResult("Toto");
        section7.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section7.addAllStrategies(Arrays.asList(strategy1, strategy2));

        Section section8 = new SectionImpl();
        section8.setSectionType(SectionType.FERTILITE_SOL_CULTURES);
        section8.setAgronomicObjective("Objectif ambitieux !");
        section8.setExpectedResult("Toto");
        section8.setCategoryObjective(CategoryObjective.MINIMISER_LES_PERTES);
        section8.addAllStrategies(Arrays.asList(strategy1, strategy2));

        managementMode.addAllSections(Arrays.asList(section1, section2, section3, section4, section5, section6, section7, section8));

        return managementMode;
    }

    /**
     * Test le contenu html d'un export PDF.
     *
     * @throws IOException in case of error
     * @throws TemplateException in case of error
     */
    @Test
    public void testExportPdfContent() throws IOException, TemplateException {
        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystem();
        ManagementMode managementMode = createManagementMode();
        String htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(new ReportExportContext(
                true, getAllSectionsForAllSectors(false), Arrays.asList(SectionType.values()),
                reportGrowingSystem, managementMode, referentialService.getGroupesCiblesParCode()));
        assertThat(htmlContent).contains("Test BdC");

        assertThat(htmlContent).contains("Test char bizarre : &lt; &gt; &#39; &quot; &amp; $ $$ été à @");
    }

    /**
     * Test que le flux qui contient des pdf dans un zip fonctionne bien.
     *
     */
    @Test
    public void testExportPdfFile() {
        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystem();
        Map<ReportGrowingSystem, ManagementMode> managementModes = new HashMap<>();
        managementModes.put(reportGrowingSystem, createManagementMode());
        InputStream is = reportPdfExport.exportPdfReportGrowingSystems(
                new ReportExportOption(true, getAllSectionsForAllSectors(false), Arrays.asList(SectionType.values())),
                        Collections.singletonList(reportGrowingSystem), managementModes, referentialService.getGroupesCiblesParCode()).toInputStream();
        assertThat(is).isNotNull();
    }

    @Test
    public void testReportGrowingSystemExportsPdfFile() {
        ReportGrowingSystem reportGrowingSystem0 = createReportGrowingSystem();
        ReportGrowingSystem reportGrowingSystem1 = createReportGrowingSystem();
        Map<ReportGrowingSystem, ManagementMode> managementModes = new HashMap<>();
        managementModes.put(reportGrowingSystem0, createManagementMode());
        InputStream is = reportPdfExport.exportPdfReportGrowingSystems(new ReportExportOption(
                true,
                getAllSectionsForAllSectors(false),
                Arrays.asList(SectionType.values())),
                Arrays.asList(reportGrowingSystem1, reportGrowingSystem0),
                managementModes,
                referentialService.getGroupesCiblesParCode()).toInputStream();
        assertThat(is).isNotNull();
    }

    /**
     * Test que les titres de toutes les sections demandées sont bien présentes ou absentes.
     */
    @Test
    public void testExportAllSectionTitle() throws IOException, TemplateException {
        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystem();
        ManagementMode managementMode = createManagementMode();

        // presence
        for (ReportGrowingSystemSection reportGrowingSystemSection : ReportGrowingSystemSection.values()) {
            String htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(
                    new ReportExportContext(
                            true,
                            getSectionForAllSectors(reportGrowingSystemSection, false),
                            Arrays.asList(SectionType.values()),
                            reportGrowingSystem,
                            managementMode,
                            referentialService.getGroupesCiblesParCode()
                    )
            );
            assertThat(htmlContent).contains(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystemSection));
        }

        // absence
        String htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(new ReportExportContext(
                true, null, null,
                reportGrowingSystem, managementMode, referentialService.getGroupesCiblesParCode()));
        for (ReportGrowingSystemSection reportGrowingSystemSection : ReportGrowingSystemSection.values()) {
            assertThat(htmlContent).doesNotContain(AgrosystI18nService.getEnumTraductionWithDefaultLocale(reportGrowingSystemSection));
        }
    }

    /**
     * Test que la partie "arbo" tu report regional est bien cachée si non concernée.
     */
    @Test
    public void testExportArboRegional() throws IOException, TemplateException {
        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystem();
        ManagementMode managementMode = createManagementMode();

        // présence
        String htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(new ReportExportContext(true,
                null, null, reportGrowingSystem, managementMode, referentialService.getGroupesCiblesParCode()));
        assertThat(htmlContent).contains("Tavelure");

        // absence
        reportGrowingSystem.getReportRegional().setSectors(Collections.singleton(Sector.MARAICHAGE));
        htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(new ReportExportContext(true,
                null, null, reportGrowingSystem, managementMode, referentialService.getGroupesCiblesParCode()));
        assertThat(htmlContent).doesNotContain("Tavelure");
    }

    @Test
    public void testCreateReportGrowingSystem() {
        loginAsAdmin();

        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystemFromService();

        validCreateOrUpdate(reportGrowingSystem);
    
        // GRANDES_CULTURES, MARAICHAGE, ARBORICULTURE, VITICULTURE
        ReportGrowingSystemCollections reportGrowingSystemCollections = new ReportGrowingSystemCollections();
        reportGrowingSystemCollections.addVitiFoodMasters(reportGrowingSystem.getFoodMasters().stream().filter(foodMaster -> Sector.VITICULTURE.equals(foodMaster.getSector())).collect(Collectors.toList()));
        reportGrowingSystemCollections.setVitiDiseaseMasters(new ArrayList<>(reportGrowingSystem.getVitiDiseaseMasters()));
        reportGrowingSystemCollections.setVitiPestMasters(new ArrayList<>(reportGrowingSystem.getVitiPestMasters()));
        reportGrowingSystemCollections.addArboYieldLosses(reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss -> Sector.ARBORICULTURE.equals(yieldLoss.getSector())).collect(Collectors.toList()));
        reportGrowingSystemCollections.setArboPestMasters(new ArrayList<>(reportGrowingSystem.getArboCropPestMasters()));
        reportGrowingSystemCollections.setArboDiseaseMasters(new ArrayList<>(reportGrowingSystem.getArboCropDiseaseMasters()));
        reportGrowingSystemCollections.setArboAdventiceMasters(new ArrayList<>(reportGrowingSystem.getArboCropAdventiceMasters()));
        reportGrowingSystemCollections.addNoneDephyExpeCropAdventiceMasters(new ArrayList<>(reportGrowingSystem.getCropAdventiceMasters()));
        reportGrowingSystemCollections.addNoneDephyExpeCropDiseaseMasters(new ArrayList<>(reportGrowingSystem.getCropDiseaseMasters()));
        reportGrowingSystemCollections.addNoneDephyExpeCropPestMasters(new ArrayList<>(reportGrowingSystem.getCropPestMasters()));
        reportGrowingSystemCollections.addNoneDephyExpeFoodMasters(reportGrowingSystem.getFoodMasters().stream().filter(foodMaster -> !Sector.VITICULTURE.equals(foodMaster.getSector()) && !Sector.ARBORICULTURE.equals(foodMaster.getSector())).collect(Collectors.toList()));
        reportGrowingSystemCollections.addNoneDephyExpeVerseMasters(new ArrayList<>(reportGrowingSystem.getVerseMasters()));
        reportGrowingSystemCollections.addNoneDephyExpeYieldLosses(reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss -> !Sector.ARBORICULTURE.equals(yieldLoss.getSector()) && !Sector.VITICULTURE.equals(yieldLoss.getSector())).collect(Collectors.toList()));

        reportGrowingSystem = reportService.createOrUpdateReportGrowingSystem(
                reportGrowingSystem,
                reportGrowingSystem.getGrowingSystem().getTopiaId(),
                reportGrowingSystem.getReportRegional().getTopiaId(),
                reportGrowingSystemCollections,
                false);

        validCreateOrUpdate(reportGrowingSystem);
        // if null no change must be done
        reportGrowingSystemCollections = new ReportGrowingSystemCollections();
        reportGrowingSystemCollections.addVitiFoodMasters(null);
        reportGrowingSystemCollections.setVitiDiseaseMasters(null);
        reportGrowingSystemCollections.setVitiPestMasters(null);
        reportGrowingSystemCollections.setVitiPestMasters(null);
        reportGrowingSystemCollections.addVitiYieldLosses(null);
        
        reportGrowingSystemCollections.addArboYieldLosses(null);
        reportGrowingSystemCollections.setArboPestMasters(null);
        reportGrowingSystemCollections.setArboDiseaseMasters(null);
        reportGrowingSystemCollections.setArboAdventiceMasters(null);
        
        reportGrowingSystemCollections.addNoneDephyExpeCropAdventiceMasters(null);
        reportGrowingSystemCollections.addNoneDephyExpeCropDiseaseMasters(null);
        reportGrowingSystemCollections.addNoneDephyExpeCropPestMasters(null);
        reportGrowingSystemCollections.addNoneDephyExpeFoodMasters(null);
        reportGrowingSystemCollections.addNoneDephyExpeVerseMasters(null);
        reportGrowingSystemCollections.addNoneDephyExpeYieldLosses(null);

        reportGrowingSystem = reportService.createOrUpdateReportGrowingSystem(
                reportGrowingSystem,
                reportGrowingSystem.getGrowingSystem().getTopiaId(),
                reportGrowingSystem.getReportRegional().getTopiaId(),
                reportGrowingSystemCollections,
                false);

        validCreateOrUpdate(reportGrowingSystem);

        // empty list: all sub entities must be removed
        reportGrowingSystemCollections = new ReportGrowingSystemCollections();
        reportGrowingSystemCollections.addVitiFoodMasters(new ArrayList<>());
        reportGrowingSystemCollections.setVitiDiseaseMasters(new ArrayList<>());
        reportGrowingSystemCollections.setVitiPestMasters(new ArrayList<>());
        reportGrowingSystemCollections.setVitiPestMasters(new ArrayList<>());
        reportGrowingSystemCollections.setArboPestMasters(new ArrayList<>());
        reportGrowingSystemCollections.addArboYieldLosses(new ArrayList<>());
        reportGrowingSystemCollections.setArboDiseaseMasters(new ArrayList<>());
        reportGrowingSystemCollections.setArboAdventiceMasters(new ArrayList<>());
        reportGrowingSystemCollections.addNoneDephyExpeCropAdventiceMasters(new ArrayList<>());
        reportGrowingSystemCollections.addNoneDephyExpeCropDiseaseMasters(new ArrayList<>());
        reportGrowingSystemCollections.addNoneDephyExpeCropPestMasters(new ArrayList<>());
        reportGrowingSystemCollections.addNoneDephyExpeFoodMasters(new ArrayList<>());
        reportGrowingSystemCollections.addNoneDephyExpeVerseMasters(new ArrayList<>());
        reportGrowingSystemCollections.addNoneDephyExpeYieldLosses(new ArrayList<>());
        reportGrowingSystemCollections.addVitiYieldLosses(new ArrayList<>());

        reportGrowingSystem = reportService.createOrUpdateReportGrowingSystem(
                reportGrowingSystem, reportGrowingSystem.getGrowingSystem().getTopiaId(),
                reportGrowingSystem.getReportRegional().getTopiaId(),
                reportGrowingSystemCollections, false);

        Assertions.assertEquals(0, reportGrowingSystem.getFoodMasters().stream().filter(foodMaster -> Sector.VITICULTURE.equals(foodMaster.getSector())).count());
        Assertions.assertEquals(0, reportGrowingSystem.getVitiDiseaseMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getVitiPestMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss -> Sector.ARBORICULTURE.equals(yieldLoss.getSector())).count());
        Assertions.assertEquals(0, reportGrowingSystem.getArboCropPestMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getArboCropDiseaseMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getArboCropAdventiceMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getCropAdventiceMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getCropDiseaseMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getCropPestMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getFoodMasters().stream().filter(foodMaster -> !Sector.ARBORICULTURE.equals(foodMaster.getSector()) && !Sector.VITICULTURE.equals(foodMaster.getSector())).count());
        Assertions.assertEquals(0, reportGrowingSystem.getVerseMasters().size());
        Assertions.assertEquals(0, reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss -> !Sector.ARBORICULTURE.equals(yieldLoss.getSector()) && !Sector.VITICULTURE.equals(yieldLoss.getSector())).count());
    }

    protected void validCreateOrUpdate(ReportGrowingSystem reportGrowingSystem) {
        Assertions.assertEquals(1, reportGrowingSystem.getFoodMasters().stream().filter(foodMaster -> Sector.VITICULTURE.equals(foodMaster.getSector())).count());
        Assertions.assertEquals(1, reportGrowingSystem.getVitiDiseaseMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getVitiPestMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss -> Sector.ARBORICULTURE.equals(yieldLoss.getSector())).count());
        Assertions.assertEquals(1, reportGrowingSystem.getArboCropPestMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getArboCropDiseaseMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getArboCropAdventiceMasters().size());
        Assertions.assertEquals(2, reportGrowingSystem.getCropAdventiceMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getCropDiseaseMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getCropPestMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getFoodMasters().stream().filter(foodMaster -> !Sector.ARBORICULTURE.equals(foodMaster.getSector()) && !Sector.VITICULTURE.equals(foodMaster.getSector())).count());
        Assertions.assertEquals(1, reportGrowingSystem.getVerseMasters().size());
        Assertions.assertEquals(1, reportGrowingSystem.getYieldLosses().stream().filter(yieldLoss -> !Sector.ARBORICULTURE.equals(yieldLoss.getSector()) && !Sector.VITICULTURE.equals(yieldLoss.getSector())).count());
    }

    /**
     * Test que les titres de toutes les sections demandées sont bien présentes ou abentes.
     */
    @Test
    public void testExportAllArboVitiSection() throws IOException, TemplateException {
        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystem();
        ManagementMode managementMode = createManagementMode();

        // all
        reportGrowingSystem.setSectors(Collections.singleton(Sector.MARAICHAGE));
        String htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(new ReportExportContext(
                true, getAllSectionsForSectors(null), Arrays.asList(SectionType.values()),
                reportGrowingSystem, managementMode, referentialService.getGroupesCiblesParCode()));
        assertThat(htmlContent).containsPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Cultures assolées\\)");
        assertThat(htmlContent).doesNotContainPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Arboriculture\\)");
        assertThat(htmlContent).doesNotContainPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Viticulture\\)");

        // arbo
        reportGrowingSystem.setSectors(Collections.singleton(Sector.ARBORICULTURE));
        htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(new ReportExportContext(
                true, getAllSectionsForSectors(Sector.ARBORICULTURE), Arrays.asList(SectionType.values()),
                reportGrowingSystem, managementMode, referentialService.getGroupesCiblesParCode()));
        assertThat(htmlContent).doesNotContainPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Cultures assolées\\)");
        assertThat(htmlContent).containsPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Arboriculture\\)");
        assertThat(htmlContent).doesNotContainPattern("Bilan des pratiques et État sanitaire des cultures (Viticulture)");

        // viti
        reportGrowingSystem.setSectors(Collections.singleton(Sector.VITICULTURE));
        htmlContent = reportPdfExport.exportPdfGrowingSystemHtmlContent(new ReportExportContext(
                true, getAllSectionsForSectors(Sector.VITICULTURE), Arrays.asList(SectionType.values()),
                reportGrowingSystem, managementMode, referentialService.getGroupesCiblesParCode()));
        assertThat(htmlContent).doesNotContainPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Cultures assolées\\)");
        assertThat(htmlContent).doesNotContainPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Arboriculture\\)");
        assertThat(htmlContent).containsPattern("Bilan des pratiques et État sanitaire des cultures\\s+\\(Viticulture\\)");
    }

    /**
     * Test l'export XLS des report regional.
     *
     */
    @Test
    public void testExportXlsReportRegional() {
        ReportRegional reportRegional = createReportRegional();
        try (InputStream is = reportXlsExport.exportXlsReportRegionals(Collections.singletonList(reportRegional)).toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    /**
     * Test l'export XLS des report growing system
     *
     */
    @Test
    public void testExportXlsReportGrowingSystem() {
        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystem();
        try(InputStream is = reportXlsExport.exportXlsReportGrowingSystems(Collections.singletonList(reportGrowingSystem)).toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    public void testCropUsageOnReportGrowingSystem() {
        ReportGrowingSystem reportGrowingSystem = createReportGrowingSystem();
        Collection<CropPestMaster> cpm = reportGrowingSystem.getCropAdventiceMasters();
        Set<CroppingPlanEntry> crops = new HashSet<>();
        for (CropPestMaster cropPestMaster : cpm) {
            crops.addAll(cropPestMaster.getCrops());
        }

        Set<String> cpeIds = crops.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toSet());

        LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions = Maps.newLinkedHashMap();
        usageCheckFunctions.put("forEffectiveSpeciesStadesResults", ids -> croppingPlanEntryTopiaDao.getCPEUsedForCropPestMaster(cpeIds));
        EntityUsageService eus0 = serviceFactory.newService(EntityUsageService.class);
        EntityUsageServiceMock eus1 = new EntityUsageServiceMock(eus0);
        Map<String, Boolean> result = eus1.computeUsageMap(cpeIds, usageCheckFunctions);

        Assertions.assertFalse(result.isEmpty());
        for (String cpeId : cpeIds) {
            Assertions.assertTrue(result.get(cpeId));
        }
    }

    private Map<String, Collection<String>> getAllSectionsForAllSectors(boolean expe) {
        Map<String, Collection<String>> result = new LinkedHashMap<>();
        Collection<String> allSections = Arrays.stream(ReportGrowingSystemSection.values())
                .map(Object::toString)
                .collect(Collectors.toList());
        if (expe) {
            result.put(Sector.ARBORICULTURE.toString(), allSections);
            result.put("null", allSections);
            result.put(Sector.VITICULTURE.toString(), allSections);
        } else {
            for (Sector sector : Sector.values()) {
                result.put(sector.toString(), allSections);
            }
        }
        return result;
    }

    private Map<String, Collection<String>> getAllSectionsForSectors(Sector sector) {
        Map<String, Collection<String>> result = new LinkedHashMap<>();
        Collection<String> allSections = Arrays.stream(ReportGrowingSystemSection.values())
                .map(Object::toString)
                .collect(Collectors.toList());
        result.put(String.valueOf(sector), allSections);
        return result;
    }

    private Map<String, Collection<String>> getSectionForAllSectors(ReportGrowingSystemSection section, boolean expe) {
        Map<String, Collection<String>> result = new LinkedHashMap<>();
        Collection<String> sectionList = Collections.singletonList(section.toString());
        if (!expe) {
            result.put(Sector.ARBORICULTURE.toString(), sectionList);
            result.put("null", sectionList);
            result.put(Sector.VITICULTURE.toString(), sectionList);
        } else {
            for (Sector sector : Sector.values()) {
                result.put(sector.toString(), sectionList);
            }
        }
        return result;
    }
}
