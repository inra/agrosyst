package fr.inra.agrosyst.services.growingplan;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2044 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanFilter;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

public class GrowingPlanServiceTest extends AbstractAgrosystTest {

    protected GrowingPlanService service;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        service = serviceFactory.newService(GrowingPlanService.class);
        
        TestDatas testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.createTestGrowingPlans();

        loginAsAdmin();
        alterSchema();
    }

    /**
     * Test getFilteredGrowingPlans without and with some filters.
     */
    @Test
    public void getFilterGrowingPlans() {

        // null
        PaginationResult<GrowingPlan> result = service.getFilteredGrowingPlans(null);
        Assertions.assertEquals(5, result.getCount());

        // empty filter
        GrowingPlanFilter filter = new GrowingPlanFilter();
        result = service.getFilteredGrowingPlans(filter);
        Assertions.assertEquals(5, result.getCount());

        // name filter
        filter.setGrowingPlanName("Baulon");
        result = service.getFilteredGrowingPlans(filter);
        Assertions.assertEquals(1, result.getCount());
    }

    /**
     * Test getDuplicateGrowingPlan to detect duplication code violation.
     */
    @Test
    public void testDuplicateGrowingPlan() {

        // test growing plan to duplicate
        GrowingPlanFilter filter = new GrowingPlanFilter();
        filter.setGrowingPlanName("Baulon");
        GrowingPlan growingPlan = service.getFilteredGrowingPlans(filter).getElements().getFirst();
        
        // test duplication
        GrowingPlan duplicate = service.duplicateGrowingPlan(growingPlan.getTopiaId(),
                growingPlan.getDomain().getTopiaId(), true);
        Assertions.assertNotNull(duplicate);
    }
    
    /**
     * Test growing plan xls export.
     * @throws IOException 
     */
    @Test
    public void testExportXls() throws IOException {
        // empty filter
        GrowingPlanFilter filter = new GrowingPlanFilter();
        List<GrowingPlan> growingPlans = service.getFilteredGrowingPlans(filter).getElements();
        List<String> gpIds = growingPlans.stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
        try (InputStream is = service.exportGrowingPlanAsXls(gpIds).content().toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        }
    }

    /**
     * Test growing plan xls modele export.
     * 
     * @throws IOException 
     */
    @Test
    public void testExportXlsEmpty() throws IOException {
        try (InputStream is = service.exportGrowingPlanAsXls(null).content().toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        }
    }

}
