package fr.inra.agrosyst.services;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by davidcosse on 29/03/17.
 */
@Getter
@Setter
public class HarvestinActionTestData {

    private Domain domain;
    
    private PracticedSystem practicedSystem;
    
    private Zone zone;
    private final Map<Pair<String, String>, RefEspece> especes = new HashMap<>();
    private final Map<Pair<String, String>, Set<CroppingPlanEntry>> crops = new HashMap<>();
    private final Map<Pair<String, String>, CroppingPlanSpecies> species = new HashMap<>();
    private final Map<Pair<String, String>, RefDestination> destinations = new HashMap<>();
    private final Map<Pair<String, String>, RefQualityCriteria> qualityCriteria = new HashMap<>();
    private final Map<Pair<String, String>, RefHarvestingPrice> harvestingPrices = new HashMap<>();
    private final Map<Pair<String, String>, RefSpeciesToSector> refSpeciesToSectors = new HashMap<>();
    private RefHarvestingPrice[] RefHarvestingPrices;
    

    public void addEspece(RefEspece espece) {
        especes.put(Pair.of(espece.getCode_espece_botanique(), espece.getCode_qualifiant_AEE()), espece);
    }

    public void addEspeces(RefEspece ... especes) {
        for (RefEspece espece : especes) {
            addEspece(espece);
        }
    }

    public void addSpecies(CroppingPlanSpecies spe) {
        Pair<String, String> codeEBQ = Pair.of(spe.getSpecies().getCode_espece_botanique(), spe.getSpecies().getCode_qualifiant_AEE());
        species.put(codeEBQ, spe);
        CroppingPlanEntry croppingPlanEntry = spe.getCroppingPlanEntry();
        croppingPlanEntry.addCroppingPlanSpecies(spe);
        addCrop(croppingPlanEntry, codeEBQ);
    }

    public void addSpecies(CroppingPlanSpecies ... species) {
        for (CroppingPlanSpecies espece0 : species) {
            addSpecies(espece0);
        }
    }

    public void addDestination(RefDestination refDestination) {
        destinations.put(Pair.of(refDestination.getCode_espece_botanique(), refDestination.getCode_qualifiant_AEE()), refDestination);
    }

    public void addDestinations(RefDestination ... refDestinations) {
        for (RefDestination destination : refDestinations) {
            addDestination(destination);
        }
    }

    public void addQualityCriteria(RefQualityCriteria refQualityCriteria) {
        qualityCriteria.put(Pair.of(refQualityCriteria.getCode_espece_botanique(), refQualityCriteria.getCode_qualifiant_AEE()), refQualityCriteria);
    }

    public void addQualityCriteria(RefQualityCriteria ... refQualityCriteria) {
        for (RefQualityCriteria qualityCriteria0 : refQualityCriteria) {
            addQualityCriteria(qualityCriteria0);
        }
    }

    public void addHarvestingPrice(RefHarvestingPrice refHarvestingPrice) {
        harvestingPrices.put(Pair.of(refHarvestingPrice.getCode_espece_botanique(), refHarvestingPrice.getCode_qualifiant_AEE()), refHarvestingPrice);
    }

    public void addHarvestingPrices(RefHarvestingPrice ... refHarvestingPrices) {
        for (RefHarvestingPrice harvestingPrice : refHarvestingPrices) {
            addHarvestingPrice(harvestingPrice);
        }
    }

    public void addRefHarvestingPrices(RefHarvestingPrice... RefHarvestingPrices) {
        this.RefHarvestingPrices = RefHarvestingPrices;
    }

    public void addCrop(CroppingPlanEntry croppingPlanEntry, Pair<String, String> codeEBQ) {
        Set<CroppingPlanEntry> cropsForEBQ = crops.computeIfAbsent(codeEBQ, k -> Sets.newHashSet());
        cropsForEBQ.add(croppingPlanEntry);
    }
    

    public void addSpeciesToSectors(RefSpeciesToSector ... refSpeciesToSectors) {
        for (RefSpeciesToSector refSpeciesToSector : refSpeciesToSectors) {
            addSpeciesToSector(refSpeciesToSector);
        }
    }

    private void addSpeciesToSector(RefSpeciesToSector refSpeciesToSector) {
        refSpeciesToSectors.put(Pair.of(refSpeciesToSector.getCode_espece_botanique(), refSpeciesToSector.getCode_qualifiant_AEE()), refSpeciesToSector);
    }
    
}
