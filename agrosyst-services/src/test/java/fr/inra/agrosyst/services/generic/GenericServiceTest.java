package fr.inra.agrosyst.services.generic;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.WaterFlowDistance;
import fr.inra.agrosyst.api.entities.Zoning;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.services.generic.GenericEntityService;
import fr.inra.agrosyst.api.services.generic.GenericFilter;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class GenericServiceTest extends AbstractAgrosystTest {

    protected GenericEntityService service;

    @BeforeEach
    public void setupServices() throws TopiaException {
        service = serviceFactory.newService(GenericEntityService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        loginAsAdmin();
        alterSchema();
    }

    @Test
    public void testCountEnum() throws Exception {
        Map<String, Long> map = service.countEntities(
                DomainType.class,
                Sector.class,
                PracticedSystemSource.class,
                WaterFlowDistance.class,
                Zoning.class,
                MaxSlope.class
        );
        Assertions.assertEquals(3L, (long)map.get(DomainType.class.getName()));
        Assertions.assertEquals(7L, (long)map.get(Sector.class.getName()));
        Assertions.assertEquals(4L, (long)map.get(WaterFlowDistance.class.getName()));

        Assertions.assertEquals(0L, (long)service.countEntities(RefLocation.class).get(RefLocation.class.getName()));

    }

    @Test
    public void testGetEntitiesEnum() throws Exception {
        PaginationResult<?> map = service.listEntities(Sector.class, null);
        Assertions.assertEquals(7, map.getElements().size());
    }

    /**
     * Test que les filtres généric sur les entiers ne font pas de "like".
     */
    @Test
    public void testGenericSearchFilterLowerInt() throws IOException {
        // import required by test:
        testDatas.importNuisiblesEDI();

        GenericFilter filter = new GenericFilter();
        Map<String, String> propertyNameAndValues = new HashMap<>();
        propertyNameAndValues.put("reference_id", "18352");
        filter.setPropertyNamesAndValues(propertyNameAndValues);

        PaginationResult<RefNuisibleEDI> result = (PaginationResult<RefNuisibleEDI>)service.listEntities(RefNuisibleEDI.class, filter);
        Assertions.assertEquals(1, result.getCount());

        // test with no result (if with like, may return result)
        propertyNameAndValues.put("reference_id", "18");
        result = (PaginationResult<RefNuisibleEDI>)service.listEntities(RefNuisibleEDI.class, filter);
        Assertions.assertEquals(0, result.getCount());
    }

    /**
     * Test que les filtres généric sur les chaines font des "like".
     */
    @Test
    public void testGenericSearchFilterLike() throws IOException {
        // import required by test:
        testDatas.importNuisiblesEDI();

        GenericFilter filter = new GenericFilter();
        Map<String, String> propertyNameAndValues = new HashMap<>();
        propertyNameAndValues.put("reference_label", "Acarien");
        filter.setPropertyNamesAndValues(propertyNameAndValues);

        PaginationResult<RefNuisibleEDI> result = (PaginationResult<RefNuisibleEDI>)service.listEntities(RefNuisibleEDI.class, filter);
        Assertions.assertEquals(16, result.getCount());
    }
    
    /**
     * Test que la recherche sur des champs hérité fonctionne.
     */
    @Test
    public void testGenericSearchFilterInheritance() throws IOException {
        // import required by test:
        testDatas.importMaterielsTracteur();

        GenericFilter filter = new GenericFilter();
        Map<String, String> propertyNameAndValues = new HashMap<>();
        propertyNameAndValues.put("typeMateriel1", "TRACTEURS CLASSIQUES");
        filter.setPropertyNamesAndValues(propertyNameAndValues);

        PaginationResult<RefNuisibleEDI> result = (PaginationResult<RefNuisibleEDI>)service.listEntities(RefMaterielTraction.class, filter);
        Assertions.assertEquals(50, result.getCount());
    }

    /**
     * Test que les filtres généric sur les collections fonctionne.
     */
    @Test
    public void testGenericSearchFilterCollection() throws IOException {
        // import required by test:
        testDatas.importNuisiblesEDI();

        GenericFilter filter = new GenericFilter();
        Map<String, String> propertyNameAndValues = new HashMap<>();
        filter.setPropertyNamesAndValues(propertyNameAndValues);

        // one filter
        propertyNameAndValues.put("sectors", "VITICULTURE");
        PaginationResult<RefNuisibleEDI> result = (PaginationResult<RefNuisibleEDI>)service.listEntities(RefNuisibleEDI.class, filter);
        Assertions.assertEquals(3, result.getCount());

        // invalid filter
        propertyNameAndValues.put("sectors", "VITICUTURE");
        result = (PaginationResult<RefNuisibleEDI>)service.listEntities(RefNuisibleEDI.class, filter);
        Assertions.assertEquals(50, result.getCount()); // = no filter

        // two filter
        propertyNameAndValues.put("sectors", "VITICULTURE,GRANDES_CULTURES");
        result = (PaginationResult<RefNuisibleEDI>)service.listEntities(RefNuisibleEDI.class, filter);
        Assertions.assertEquals(1, result.getCount());
    }
}
