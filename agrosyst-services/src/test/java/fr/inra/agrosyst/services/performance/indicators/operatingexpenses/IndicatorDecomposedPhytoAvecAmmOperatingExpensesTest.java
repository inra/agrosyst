package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhytoTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import static fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES;
import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorDecomposedPhytoAvecAmmOperatingExpensesTest extends AbstractIndicatorDecomposedOperatingExpensesTest {
    private DomainPhytoProductInputTopiaDao phytoProductInputDao;
    private PesticideProductInputUsageTopiaDao pesticideProductInputUsageDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        super.setupServices();
        phytoProductInputDao = getPersistenceContext().getDomainPhytoProductInputDao();
        pesticideProductInputUsageDao = getPersistenceContext().getPesticideProductInputUsageDao();
    }

    private void createRefPrixPesticidiMin(int year) {
        RefPrixPhytoTopiaDao refPrixPhytoDao = getPersistenceContext().getRefPrixPhytoDao();
        // METAREX INO
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_SCENARIO, "2016",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG,
                RefInputPrice.PROPERTY_PRICE, 12.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "5680",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 121,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "5680_121"
        );
        // BETAPOST
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_SCENARIO, "2016",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 240.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 999900,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "a_AAADA_999900"
        );
        // METAREX INO
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_SCENARIO, "2017",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG,
                RefInputPrice.PROPERTY_PRICE, 27.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "5680",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 121,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "5680_121"
        );
        // BETAPOST
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_SCENARIO, "2017",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 255.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 999900,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "a_AAADA_999900"
        );
        // METAREX INO
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, null,
                RefInputPrice.PROPERTY_SCENARIO, "Y",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG,
                RefInputPrice.PROPERTY_PRICE, 127.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "5680",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 121,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "5680_121"
        );
        // BETAPOST
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, null,
                RefInputPrice.PROPERTY_SCENARIO, "Y",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 355.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 999900,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "a_AAADA_999900"
        );
        // HURRICANE
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_SCENARIO, "2016",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 10.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "2870",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 126,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "2870_126"
        );
        // HURRICANE
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_SCENARIO, "2017",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 25.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "2870",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 126,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "2870_126"
        );
        // HURRICANE
        refPrixPhytoDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, null,
                RefInputPrice.PROPERTY_SCENARIO, "Y",
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefInputPrice.PROPERTY_PRICE, 125.0,
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "2870",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 126,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "2870_126"
        );
    }

    @Test
    public void inraeEffectiveWithPriceTest()  throws IOException {
        inraeEffectiveTest(
                8.0d,
                220.0d,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);2013;Baulon;520.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, produits phyto avec AMM (€/ha);2013;Baulon;780.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04/2013;03/04/2014;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Divers - Molluscicides (10.0 kg/ha);;non;2013;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);80.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04/2013;03/04/2014;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Divers - Molluscicides (10.0 kg/ha);;non;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, produits phyto avec AMM (€/ha);270.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04/2013;03/04/2014;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Herbicide - Inconnu (2.0 L/ha);;non;2013;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);440.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04/2013;03/04/2014;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Herbicide - Inconnu (2.0 L/ha);;non;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, produits phyto avec AMM (€/ha);510.0;100;;;");
                });
    }

    @Test
    public void inraeEffectiveWithoutPriceTest()  throws IOException {
        inraeEffectiveTest(
                null,
                null,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);2013;Baulon;780.0;71;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04/2013;03/04/2014;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Divers - Molluscicides (10.0 kg/ha);;non;2013;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);270.0;50;Intrant 'Lutte chimique et biocontrôle (produits avec AMM) (METAREX INO Traitement phytosanitaire)' : Prix non renseigné;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04/2013;03/04/2014;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Herbicide - Inconnu (2.0 L/ha);;non;2013;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);510.0;50;Intrant 'Lutte chimique et biocontrôle (produits avec AMM) (BETAPOST Traitement phytosanitaire)' : Prix non renseigné;;");
                });
    }

    private void inraeEffectiveTest(Double price1, Double price2, Consumer<String> assertions) throws IOException {
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        final RefCountry france = testDatas.getDefaultFranceCountry();
        testDatas.importActaTraitementsProduits();
        testDatas.createRefInputUnitPriceUnitConverter();

        RefInterventionAgrosystTravailEDI phytoProductAgrosystIntervention = refInterventionAgrosystTravailEDIDao.forNaturalId("SEX").findUnique();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixPesticidiMin(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "intervention",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 4, 3),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign() + 1, 4, 3),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0d,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention.addSpeciesStades(intervention1Stades);

        RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        RefActaTraitementsProduit metarexIno = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Divers - Molluscicides",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "METAREX INO",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "D5",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "D5",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "5680",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 121 //metarexIno 5680_121
        );

        RefActaTraitementsProduit betapost = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Herbicide - Inconnu",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "BETAPOST",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 999900 //betapost a_AAADA_999900
        );

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(metarexIno);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Divers - Molluscicides",
                InputPrice.PROPERTY_PRICE, price1
        );
        var phytoInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Divers - Molluscicides",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.KG_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, metarexIno,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 10.0d
        );

        objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(betapost);
        var phytoPrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Herbicide - Inconnu",
                InputPrice.PROPERTY_PRICE, price2
        );
        var phytoInput2 = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Herbicide - Inconnu",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, betapost,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage2 = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput2,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 2.0d
        );

        PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao = getPersistenceContext().getPesticidesSpreadingActionDao();
        pesticidesSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, phytoProductAgrosystIntervention,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0d,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 0.1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, List.of(pesticideUsage, pesticideUsage2),
                PesticidesSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(APPLICATION_DE_PRODUITS_PHYTOSANITAIRES));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertions.accept(content);
    }

    @Test
    public void inraePracticedTest() throws IOException {
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        final RefCountry france = testDatas.getDefaultFranceCountry();
        testDatas.importActaTraitementsProduits();
        testDatas.createRefInputUnitPriceUnitConverter();

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        createRefPrixPesticidiMin(domain.getCampaign());

        testDatas.createRefInputUnitPriceUnitConverter();

        RefInterventionAgrosystTravailEDI phytoProductAgrosystIntervention = refInterventionAgrosystTravailEDIDao.forNaturalId("SEX").findUnique();

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100.0d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0d,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.0d,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(intervention1Stades);

        RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        RefActaTraitementsProduit metarexIno = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Divers - Molluscicides",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "METAREX INO",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "D5",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "D5",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "5680",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 121 //metarexIno 5680_121
        );

        RefActaTraitementsProduit betapost = refActaTraitementsProduitDao.create(
                RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, france,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT, "Herbicide - Inconnu",
                RefActaTraitementsProduit.PROPERTY_ACTIVE, true,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT, "BETAPOST",
                RefActaTraitementsProduit.PROPERTY_NODU, false,
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, "a_AAABU",
                RefActaTraitementsProduit.PROPERTY_ID_PRODUIT, "a_AAADA",
                RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, 999900 //betapost a_AAADA_999900
        );

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(metarexIno);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domain,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Divers - Molluscicides",
                InputPrice.PROPERTY_PRICE, 8.0d
        );
        var phytoInput = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Divers - Molluscicides",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.KG_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, metarexIno,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 10.0d
        );

        objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(betapost);
        var phytoPrice2 = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domain,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Herbicide - Inconnu",
                InputPrice.PROPERTY_PRICE, 220.0d
        );
        var phytoInput2 = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "Herbicide - Inconnu",
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.L_HA,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, betapost,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, phytoPrice2,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var pesticideUsage2 = pesticideProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, phytoInput2,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 2.0d
        );

        PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao = getPersistenceContext().getPesticidesSpreadingActionDao();
        pesticidesSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, phytoProductAgrosystIntervention,
                PesticidesSpreadingAction.PROPERTY_PROPORTION_OF_TREATED_SURFACE, 100.0d,
                PesticidesSpreadingAction.PROPERTY_BOILED_QUANTITY, 0.1d,
                PesticidesSpreadingAction.PROPERTY_PESTICIDE_PRODUCT_INPUT_USAGES, List.of(pesticideUsage, pesticideUsage2),
                PesticidesSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(APPLICATION_DE_PRODUITS_PHYTOSANITAIRES));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);2013, 2017;Baulon;520.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, produits phyto avec AMM (€/ha);2013, 2017;Baulon;780.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Divers - Molluscicides (10.0 kg/ha);;non;2013, 2017;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);80.0;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Divers - Molluscicides (10.0 kg/ha);;non;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, produits phyto avec AMM (€/ha);270.0;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Herbicide - Inconnu (2.0 L/ha);;non;2013, 2017;Indicateur économique;Charges opérationnelles réelles produits phyto avec AMM (€/ha);440.0;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);intervention;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Herbicide - Inconnu (2.0 L/ha);;non;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, produits phyto avec AMM (€/ha);510.0;100;;;");
    }
}
