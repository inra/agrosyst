package fr.inra.agrosyst.services.performance.indicators.qsa;

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;

import java.util.List;

public record SeedSLotTestUsage(CroppingPlanEntry crop,
                                DomainSeedLotInput domainSeedLotInput,
                                List<SeedSpeciesTestUsage> speciesTestUsages
                        ) {
}