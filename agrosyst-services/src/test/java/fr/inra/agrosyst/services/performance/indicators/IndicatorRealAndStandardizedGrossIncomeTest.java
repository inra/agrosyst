package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterImpl;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVarieteTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGevesTopiaDao;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by davidcosse on 29/09/16.
 */
public class IndicatorRealAndStandardizedGrossIncomeTest extends AbstractAgrosystTest {
    private PerformanceServiceImplForTest performanceService;
    private ZoneTopiaDao zoneDao;
    private RefEspeceTopiaDao refEspeceDao;
    private CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    private CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    private HarvestingActionValorisationTopiaDao harvestingActionValorisationDao;
    private HarvestingActionTopiaDao harvestingActionDao;
    private RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    private EffectiveCropCyclePhaseTopiaDao cropCyclePhaseDAO;
    private EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    private EffectiveInterventionTopiaDao effectiveInterventionDao;
    private EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;
    private DomainTopiaDao domainDao;
    private RefLocationTopiaDao refLocationDao;
    private RefLegalStatusTopiaDao refLegalStatusDao;
    private RefSpeciesToSectorTopiaDao refSpeciesToSectorDao;
    private RefEspeceToVarieteTopiaDao refEspeceToVarieteDao;
    private RefVarieteGevesTopiaDao refVarieteGevesDao;
    private RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao;
    private RefSolTextureGeppaTopiaDao refSolTextureGeppaDao;
    private HarvestingPriceTopiaDao harvestingPriceDao;
    private PracticedCropCyclePhaseTopiaDao practicedCropCyclePhaseDao;
    private PracticedSystemTopiaDao practicedSystemDao;
    private PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao;
    private PracticedInterventionTopiaDao practicedInterventionDao;
    private PracticedSpeciesStadeTopiaDao practicedSpeciesStadeDao;
    protected PricesService pricesService;
    
    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        pricesService = serviceFactory.newService(PricesService.class);
        
        loginAsAdmin();
        alterSchema();
        
        zoneDao = getPersistenceContext().getZoneDao();
        refEspeceDao = getPersistenceContext().getRefEspeceDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        harvestingActionValorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();
        harvestingActionDao = getPersistenceContext().getHarvestingActionDao();
        refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        cropCyclePhaseDAO = getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectivePerennialCropCycleDao = getPersistenceContext().getEffectivePerennialCropCycleDao();
        effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        effectiveSpeciesStadeDao = getPersistenceContext().getEffectiveSpeciesStadeDao();
        domainDao = getPersistenceContext().getDomainDao();
        refLocationDao = getPersistenceContext().getRefLocationDao();
        refLegalStatusDao = getPersistenceContext().getRefLegalStatusDao();
        refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        refEspeceToVarieteDao = getPersistenceContext().getRefEspeceToVarieteDao();
        refVarieteGevesDao = getPersistenceContext().getRefVarieteGevesDao();
        refSolProfondeurIndigoDao = getPersistenceContext().getRefSolProfondeurIndigoDao();
        refSolTextureGeppaDao = getPersistenceContext().getRefSolTextureGeppaDao();
        harvestingPriceDao = getPersistenceContext().getHarvestingPriceDao();
        
        practicedCropCyclePhaseDao= getPersistenceContext().getPracticedCropCyclePhaseDao();
        practicedSystemDao = getPersistenceContext().getPracticedSystemDao();
        practicedPerennialCropCycleDao = getPersistenceContext().getPracticedPerennialCropCycleDao();
        practicedInterventionDao = getPersistenceContext().getPracticedInterventionDao();
        practicedSpeciesStadeDao = getPersistenceContext().getPracticedSpeciesStadeDao();
        
        testDatas = serviceFactory.newInstance(TestDatas.class);
        
        testDatas.createDemoRefLocation();
        testDatas.importSolTextureGeppa();
        testDatas.importSolProfondeurIndigo();
        
    }
    
    /**

                          Date de mise à jour #12473        Revue des te
                          Changements         retirer le tau

       Déjà crée
       Domaine            Baulon 2017
       Dispositif         Dispositif test ift
       Sdc nom            SdC GC
       Parcelle nom       Plot Baulon 2
       zone nom           Zone principale

       culture
       id                 nom                 type          espece      variete
       cpePomme           Pomme               main          Pommier     -

       cycle de culture
       id_culture         type                phase
       cpePomme           perenne             pleine product

       INTERVENTIONS
       id_culture         intervention        type          date debut  date fin    debit de chanb passagefq spatialdebut_pedebut_decadedebut_campafin_periode fin_decade  fin_campagne
       cpePomme           i1                  recolte         01/08/2017  01/08/2017         0,8         1       0,8August             2       2017August                 3        2017

                                                                                                                                                                                                                   PRODUIT BRUT                        reel : 1) te
       Valorisations des r                                                                                                                                                                                                                                             si prix reel
       id_valorisation    esp                 intervention  rendement morendement moculture bio destinatio%_valorise%_autoco%_nonvalo   prix_recoltprix_unite  unite_cohereprix_ref    prix_ref_uniunite_coheretx_conversion_pardefreel avec aureel sans austd avec autstd sans aut
       evd_a              pomme               i1            UNITE_HA              30non         d1Pomme          100       0           0         50EURO_HA     non                 1100EURO_T      non                            1          50          50       33000       33000
       evd_b              pomme               i1            UNITE_HA               5non         d2Pomme          100       0           0         10EURO_L      non                  200EURO_T      non                            1          50          50        1000        1000

                                                                                                                                                                                                               Comme il n'y a pas d
       PRODUIT BRUT par in
       intervention       psci                reel avec autoreel sans austd avec autstd sans aut
       i1                                  0,8            80          80       27200       27200
       SOMME :                                            80          80       27200       27200

       valeur attendue mis                    a ajouter     a ajouter          27200       27200

       -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
       Intervention ;EVD (espèce - variété - destination);PSCi;rendement EVD;prix EVD;% commercialisé;% autoconsommé;taux conversion;% non valorisé;Produit brut stand - scénario s (échelle EVD);Produit brut stand - scénario s (échelle intervention)
       1            ;a (Pomme - Var 1 - Desti 1)         ;0,8 ;30           ;1100    ;100            ;0             ;0              ;0             ;26400                              ;27200
       1            ;b (Pomme - Var 1 - Desti 2)         ;0,8 ;5            ;200     ;100            ;0             ;0              ;0             ;800                                ;27200
       2            ;c                                   ;1   ;5            ;100     ;100            ;0             ;0              ;0             ;500                                ;500
       3            ;d                                   ;1   ;5            ;100     ;50             ;50            ;0              ;0             ;500                                ;500
       4            ;e                                   ;1   ;5            ;112     ;80             ;0             ;20             ;0             ;448                                ;448
     */
    @Test
    public void createInraStandardisedGrossIncomeWithSelfConsumedResultTest() throws IOException {
        // required imports
        RefInterventionAgrosystTravailEDI mainActionRecolte = refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "SER", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.RECOLTE);
        
        RefLocation location = testDatas.createRefLocation(refLocationDao);
        Domain d0 = testDatas.createSimpleDomain(
                domainDao,
                testDatas.getDomainId("Baulon", 2017),
                location,
                testDatas.createRefLegalStatus(refLegalStatusDao),
                "Baulon",
                2017,
                "Annie Verssaire",
                "Lorem ipsum dolor sit amet",
                100.0d,
                9.0d,
                1.0d,
                true
                );
        GrowingPlan gp = testDatas.createGrowingPlan(d0);
        GrowingSystem gs = testDatas.createGrowingSystem(gp);
        String plotName = "Plot Baulon 2";
        String plotEdaplosId = "edaplosIssuerIdBaulon2";
        
        RefEspece pomme = testDatas.createRefEspece(refEspeceDao, "G21", "H36", "Pommier", null);
        RefEspece  ecpecePourC = testDatas.createRefEspece(refEspeceDao, "ecpecePourC", "", "ecpecePourC", null);
        RefEspece  ecpecePourD = testDatas.createRefEspece(refEspeceDao, "ecpecePourD", "", "ecpecePourD", null);
        RefEspece  ecpecePourE = testDatas.createRefEspece(refEspeceDao, "ecpecePourE", "", "ecpecePourE", null);
        
        refSpeciesToSectorDao.createByNaturalId(pomme.getCode_espece_botanique(), pomme.getCode_qualifiant_AEE(), Sector.ARBORICULTURE);
        refSpeciesToSectorDao.createByNaturalId(ecpecePourC.getCode_espece_botanique(), ecpecePourC.getCode_qualifiant_AEE(), Sector.ARBORICULTURE);
        refSpeciesToSectorDao.createByNaturalId(ecpecePourD.getCode_espece_botanique(), ecpecePourD.getCode_qualifiant_AEE(), Sector.ARBORICULTURE);
        refSpeciesToSectorDao.createByNaturalId(ecpecePourE.getCode_espece_botanique(), ecpecePourE.getCode_qualifiant_AEE(), Sector.ARBORICULTURE);
        
        RefDestination d1Pomme = testDatas.createRefDestination(pomme, "d1Pomme", Sector.ARBORICULTURE, YealdUnit.UNITE_HA, null, null);
        RefDestination d2Pomme = testDatas.createRefDestination(pomme, "d2Pomme", Sector.ARBORICULTURE, YealdUnit.TONNE_HA, null, null);
        RefDestination dC = testDatas.createRefDestination(pomme, "dc", Sector.ARBORICULTURE, YealdUnit.TONNE_HA, null, null);
        RefDestination dD = testDatas.createRefDestination(pomme, "dd", Sector.ARBORICULTURE, YealdUnit.TONNE_HA, null, null);
        RefDestination dE = testDatas.createRefDestination(pomme, "de", Sector.ARBORICULTURE, YealdUnit.TONNE_HA, null, null);
        
        testDatas.createRefHarvestingPrice(pomme, d1Pomme, false, Calendar.AUGUST, 2, 1.1, PriceUnit.EURO_T, 2016, null);
        testDatas.createRefHarvestingPrice(pomme, d2Pomme, false, Calendar.AUGUST, 2, 200, PriceUnit.EURO_T, 2016, null);
        testDatas.createRefHarvestingPrice(pomme, dC, false, Calendar.AUGUST, 2, 100, PriceUnit.EURO_T, 2018, null);
        testDatas.createRefHarvestingPrice(pomme, dD, false, Calendar.AUGUST, 2, 100, PriceUnit.EURO_T, 2018, null);
        testDatas.createRefHarvestingPrice(pomme, dE, false, Calendar.AUGUST, 2, 112, PriceUnit.EURO_T, 2018, null);

        RefSolProfondeurIndigo solProfond = refSolProfondeurIndigoDao.forClasse_de_profondeur_INDIGOEquals(PerformanceServiceImpl.DEFAULT_DEEPEST_INDIGO).findUnique();
        
        testDatas.importSolTextureGeppa();
        RefSolTextureGeppa solArgiloSaleuse = refSolTextureGeppaDao.forNaturalId("AS").findUnique();
        RefSolTextureGeppa solSableArgiloLimoneux = refSolTextureGeppaDao.forNaturalId("Sal").findUnique();
        Plot plot = testDatas.createPlot(solProfond, solArgiloSaleuse, solSableArgiloLimoneux, gs, Lists.newArrayList(location), plotName, plotEdaplosId);
        
        testDatas.createZone(plot, ZoneType.PRINCIPALE);
        
        // intervention.getTemporalFrequency() * intervention.getSpatialFrequency()
        
        List<Zone> zones = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();
        
        CroppingPlanEntry cpePomme = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "CPE0",
                CroppingPlanEntry.PROPERTY_NAME, "Pomme",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );
        
        CroppingPlanSpecies especePomme = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, "e_p",
                CroppingPlanSpecies.PROPERTY_SPECIES, pomme,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpePomme,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + pomme.getLibelle_espece_botanique()
        );
        
        cpePomme.addCroppingPlanSpecies(especePomme);
        croppingPlanEntryDao.update(cpePomme);
        
        EffectiveCropCyclePhase cropCyclePhase = cropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);
        
        cropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 20,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);
        
        
        effectivePerennialCropCycleDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cpePomme,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);
        
        
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especePomme);
        
        EffectiveIntervention i1 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 8, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 8, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.8,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.8,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(intervention1Stades)
        );
        
        //PSCi: (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i. PSCi est calculé sur la base de données saisies par l’utilisateur.
        // pci = 0.6
        HarvestingActionValorisation evd_a = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePomme.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d1Pomme,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 30,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0
        );
        
        HarvestingActionValorisation evd_b = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePomme.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d2Pomme,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 5,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0
        );
        
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_a, evd_b),
                HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION, i1,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );
        
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePomme, d1Pomme),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_a,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "a",
                HarvestingPrice.PROPERTY_PRICE, 50.0
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePomme, d2Pomme),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_b,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "d",
                HarvestingPrice.PROPERTY_PRICE, 10.0
        );
        
        IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = serviceFactory.newInstance(IndicatorStandardisedGrossIncome.class);
        
        testDatas.createRefHarvestingPrice("RSP_PommeD1", pomme.getCode_espece_botanique(), pomme.getCode_qualifiant_AEE(), d1Pomme.getCode_destination_A(), false, evd_a.getBeginMarketingPeriod()-1, evd_a.getEndingMarketingPeriodDecade(), PriceUnit.EURO_T, 1100, 2017);
        testDatas.createRefHarvestingPrice("RSP_PommeD1", pomme.getCode_espece_botanique(), pomme.getCode_qualifiant_AEE(), d1Pomme.getCode_destination_A(), false, evd_a.getBeginMarketingPeriod(), evd_a.getEndingMarketingPeriodDecade(), PriceUnit.EURO_T, 1100, 2017);
        testDatas.createRefHarvestingPrice("RSP_PommeD2", pomme.getCode_espece_botanique(), pomme.getCode_qualifiant_AEE(), d2Pomme.getCode_destination_A(), false, evd_b.getBeginMarketingPeriod()-1, evd_b.getEndingMarketingPeriodDecade(), PriceUnit.EURO_T, 150, 2017);
        testDatas.createRefHarvestingPrice("RSP_PommeD2", pomme.getCode_espece_botanique(), pomme.getCode_qualifiant_AEE(), d2Pomme.getCode_destination_A(), false, evd_b.getBeginMarketingPeriod(), evd_b.getEndingMarketingPeriodDecade(), PriceUnit.EURO_T, 200, 2017);
        testDatas.createRefHarvestingPrice("RSP_PommeD1", pomme.getCode_espece_botanique(), pomme.getCode_qualifiant_AEE(), d1Pomme.getCode_destination_A(), false, evd_a.getBeginMarketingPeriod()+1, evd_a.getEndingMarketingPeriodDecade(), PriceUnit.EURO_T, 1200, 2017);
        testDatas.createRefHarvestingPrice("RSP_PommeD2", pomme.getCode_espece_botanique(), pomme.getCode_qualifiant_AEE(), d2Pomme.getCode_destination_A(), false, evd_b.getBeginMarketingPeriod()+1, evd_b.getEndingMarketingPeriodDecade(), PriceUnit.EURO_T, 300, 2017);
    
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
    
        IndicatorFilter indicatorGrossIncomeFilter = new IndicatorFilterImpl();
        indicatorGrossIncomeFilter.setClazz("IndicatorGrossIncome");
        indicatorGrossIncomeFilter.setComputeReal(true);
        indicatorGrossIncomeFilter.setComputeStandardized(true);
        indicatorGrossIncomeFilter.setWithAutoConsumed(true);
        indicatorGrossIncomeFilter.setWithoutAutoConsumed(true);
        indicatorStandardisedGrossIncome.init(indicatorGrossIncomeFilter);
        indicatorFilters.add(indicatorGrossIncomeFilter);
    
        IndicatorGrossIncome indicatorGrossIncome = serviceFactory.newInstance(IndicatorGrossIncome.class);
    
        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(
                performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);
    
        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorGrossIncome, indicatorStandardisedGrossIncome);
        String content = out.toString();
    
        // ces valeurs ont été fournies par l'INRA et sont vérifiées
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Produit brut standardisé sans l'autoconsommation, millésimé (€/ha);2017;Baulon;27200.0;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Produit brut standardisé avec l'autoconsommation, millésimé (€/ha);2017;Baulon;27200.0;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Produit brut réel sans l’autoconsommation (€/ha);2017;Baulon;80.0;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Produit brut réel avec l’autoconsommation (€/ha);2017;Baulon;80.0;100;;");
    }
    
    @Test
    public void createInraStandardisedGrossIncomeWithoutAutoConsum_9959_0() throws IOException {
        
        RefInterventionAgrosystTravailEDI mainActionRecolte = refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "SER", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.RECOLTE);
        
        RefLocation location = testDatas.createRefLocation(refLocationDao);
        
        RefEspece vigne = testDatas.createRefEspece(refEspeceDao, "ZMO", "", "Vigne", null);
        RefEspece  poireau1 = testDatas.createRefEspece(refEspeceDao, "ZDN", "", "Poireau", "ZMP");
        RefEspece  poireau2 = testDatas.createRefEspece(refEspeceDao, "ZDN", "", "Poireau", "H58");
        
        refEspeceToVarieteDao.create(
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI, poireau1.getCode_espece_botanique(),
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL, "20906",
                RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE, "INRA");
        
        RefVarieteGeves varitePoireauArmor = refVarieteGevesDao.create(RefVarieteGeves.PROPERTY_GROUPE, 10, RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, 20906, RefVarieteGeves.PROPERTY_NOM__FRANCAIS, "Poireau", RefVarieteGeves.PROPERTY_DENOMINATION, "Armor");
        RefVarieteGeves varitePoireauAzur = refVarieteGevesDao.create(RefVarieteGeves.PROPERTY_GROUPE, 10, RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, 20906, RefVarieteGeves.PROPERTY_NOM__FRANCAIS, "Poireau", RefVarieteGeves.PROPERTY_DENOMINATION, "Azur");
        
        
        refSpeciesToSectorDao.createByNaturalId(vigne.getCode_espece_botanique(), vigne.getCode_qualifiant_AEE(), Sector.VITICULTURE);
        refSpeciesToSectorDao.createByNaturalId(poireau1.getCode_espece_botanique(), poireau1.getCode_qualifiant_AEE(), Sector.MARAICHAGE);
        
        RefDestination d1_VITI_00001 = testDatas.createRefDestination(vigne, "Toutes appellations / Tous modes de commercialisation", Sector.VITICULTURE, YealdUnit.HL_VIN_HA, null, "VITI_00001");
        RefDestination d2_VITI_00010 = testDatas.createRefDestination(vigne, "Circuit Long / Frais", Sector.VITICULTURE, YealdUnit.KG_RAISIN_HA, null, "VITI_00010");
        RefDestination d3_MARA_00089 = testDatas.createRefDestination(poireau1, "Toutes catégories - tous calibres", Sector.MARAICHAGE, YealdUnit.TONNE_HA, null, "MARA_00089");
        RefDestination d4_MARA_00090 = testDatas.createRefDestination(poireau1, "Toutes catégories - tous calibres", Sector.MARAICHAGE, YealdUnit.UNITE_HA, null, "MARA_00090");
        
        // Strategy Prices
        
        // limit to 3 decade
        
        // vigne VITI_00001
        testDatas.createRefHarvestingPrice(
                vigne, d1_VITI_00001, false, Calendar.AUGUST, 3, 1960, PriceUnit.EURO_KG_GRAPE, 2017, "spVigne_VITI_00001");
        
        
        // vigne VITI_00010
        testDatas.createRefHarvestingPrice(
                vigne, d2_VITI_00010, false, Calendar.AUGUST, 3, 2230, PriceUnit.EURO_KG_GRAPE, 2017, "spVigne_VITI_00010");
        
        
        // poireau _MARA_00089
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.APRIL, 1, 1735, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 1, 95, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 2, 825, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 3, 800, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.JUNE, 1, 1600, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.JUNE, 2, 100, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        
        
        // poireau MARA_00090
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.APRIL, 1, 1690, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.MAY, 1, 80, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.MAY, 3, 835, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 1, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 2, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 3, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        
        // vigne VITI_00001 bio
        testDatas.createRefHarvestingPrice(
                vigne, d1_VITI_00001, true,  Calendar.AUGUST, 3, 2000, PriceUnit.EURO_KG_GRAPE, 2017,  "spVigne_bio_VITI_00001");
        // vigne VITI_00010 bio
        testDatas.createRefHarvestingPrice(
                vigne, d2_VITI_00010, true, Calendar.AUGUST, 3, 2040, PriceUnit.EURO_KG_GRAPE, 2017,  "spVigne_bio_VITI_00010");
        // poireau _MARA_00089 bio
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, true, Calendar.MAY, 1, 295, PriceUnit.EURO_T, 2017, "spPoireau_bio_MARA_00089");

        Domain d0 = testDatas.createSimpleDomain(
                domainDao,
                testDatas.getDomainId("Baulon", 2017),
                location,
                testDatas.createRefLegalStatus(refLegalStatusDao),
                "Baulon",
                2017,
                "Annie Verssaire",
                "Lorem ipsum dolor sit amet",
                100.0d,
                9.0d,
                1.0d,
                true
        );
        GrowingPlan gp = testDatas.createGrowingPlan(d0);
        GrowingSystem gs = testDatas.createGrowingSystem(gp);
        
        CroppingPlanEntry cpeVigne1Var1 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "CPE0",
                CroppingPlanEntry.PROPERTY_NAME, "Vigne",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );
        
        CroppingPlanSpecies cpeVigne = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, "e_p",
                CroppingPlanSpecies.PROPERTY_SPECIES, vigne,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpeVigne1Var1,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + vigne.getLibelle_espece_botanique()
        );
        cpeVigne1Var1.addCroppingPlanSpecies(cpeVigne);
        
        CroppingPlanEntry cpePoireau0 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "CPE4",
                CroppingPlanEntry.PROPERTY_NAME, "Poireau",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );
        
        CroppingPlanSpecies especePoireauArmor0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, "e_p0",
                CroppingPlanSpecies.PROPERTY_SPECIES, poireau1,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpePoireau0,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + poireau1.getLibelle_espece_botanique() + " " + varitePoireauArmor.getDenomination(),
                CroppingPlanSpecies.PROPERTY_VARIETY, varitePoireauArmor
        );
        cpePoireau0.addCroppingPlanSpecies(especePoireauArmor0);
        
        CroppingPlanSpecies especePoireauAzur = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, "e_p2",
                CroppingPlanSpecies.PROPERTY_SPECIES, poireau2,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpePoireau0,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + poireau2.getLibelle_espece_botanique()+ " " + varitePoireauAzur.getDenomination(),
                CroppingPlanSpecies.PROPERTY_VARIETY, varitePoireauAzur
        );
        cpePoireau0.addCroppingPlanSpecies(especePoireauAzur);
        croppingPlanEntryDao.updateAll(Lists.newArrayList(cpeVigne1Var1, cpePoireau0));

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorGrossIncome_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "PS_SDC_TEST",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, gs,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply("2016,2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true);
        
        
        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cpePoireau0.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
                );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_TOPIA_ID, "inter1_Id",
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "inter1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "31/8",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "25/9",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.6
        );
        
        PracticedSpeciesStade especePoireauArmorSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode());
        PracticedSpeciesStade intervention1StadesSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode());
        PracticedIntervention i4b = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_TOPIA_ID, "i4b_Id",
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i4b",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "9/4",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "22/6",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.1,
                PracticedIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(especePoireauArmorSpeciesStade, intervention1StadesSpeciesStade));
        
        //MARA_00089 rendement total 20%
        //MARA_00089 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 20%
        HarvestingActionValorisation evd_l14 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l14_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 20,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        //MARA_00089 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 0%
        HarvestingActionValorisation evd_l14bis = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l14bis_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 0,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        // MARA_00090 rendement total 40%
        // MARA_00090 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 32%
        HarvestingActionValorisation evd_l15 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l15_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 32,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        
        // MARA_00090 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 8
        HarvestingActionValorisation evd_l16 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l16_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        // poireau i4a
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_l14, evd_l14bis, evd_l15, evd_l16),
                HarvestingAction.PROPERTY_PRACTICED_INTERVENTION, i4b,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l16,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0,
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l15,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0,
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );
        // no price for this valorisation
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l14bis,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );

        // poireau
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l14,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 300.0,
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );
        
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(true);
        indicatorFilter.setWithoutAutoConsumed(true);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);
    
        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
    
        IndicatorGrossIncome indicatorGrossIncome = serviceFactory.newInstance(IndicatorGrossIncome.class);
    
        IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = serviceFactory.newInstance(IndicatorStandardisedGrossIncome.class);
        indicatorStandardisedGrossIncome.init(indicatorFilter);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorGrossIncome, indicatorStandardisedGrossIncome);
        String content = out.toString();

        // Données validées par l'INRAE
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut standardisé avec l'autoconsommation, millésimé (€/ha);1827.5;90;;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut standardisé sans l'autoconsommation, millésimé (€/ha);1343.333;90;;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut réel sans l’autoconsommation (€/ha);450.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut réel avec l’autoconsommation (€/ha);600.0;");
    }


    @Test
    public void testEchelleSynthetise_12473_mixte_et_non_mixte() throws IOException {

        RefInterventionAgrosystTravailEDI mainActionRecolte = refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "SER", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.RECOLTE);

        RefLocation location = testDatas.createRefLocation(refLocationDao);

        RefEspece vigne = testDatas.createRefEspece(refEspeceDao, "ZMO", "", "Vigne", null);
        RefEspece  poireau1 = testDatas.createRefEspece(refEspeceDao, "ZDN", "", "Poireau", "ZMP");
        RefEspece  poireau2 = testDatas.createRefEspece(refEspeceDao, "ZDN", "", "Poireau", "H58");

        refEspeceToVarieteDao.create(
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI, poireau1.getCode_espece_botanique(),
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL, "20906",
                RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE, "INRA");

        RefVarieteGeves varitePoireauArmor = refVarieteGevesDao.create(RefVarieteGeves.PROPERTY_GROUPE, 10, RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, 20906, RefVarieteGeves.PROPERTY_NOM__FRANCAIS, "Poireau", RefVarieteGeves.PROPERTY_DENOMINATION, "Armor");
        RefVarieteGeves varitePoireauAzur = refVarieteGevesDao.create(RefVarieteGeves.PROPERTY_GROUPE, 10, RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, 20906, RefVarieteGeves.PROPERTY_NOM__FRANCAIS, "Poireau", RefVarieteGeves.PROPERTY_DENOMINATION, "Azur");


        refSpeciesToSectorDao.createByNaturalId(vigne.getCode_espece_botanique(), vigne.getCode_qualifiant_AEE(), Sector.VITICULTURE);
        refSpeciesToSectorDao.createByNaturalId(poireau1.getCode_espece_botanique(), poireau1.getCode_qualifiant_AEE(), Sector.MARAICHAGE);

        RefDestination d1_VITI_00001 = testDatas.createRefDestination(vigne, "Toutes appellations / Tous modes de commercialisation", Sector.VITICULTURE, YealdUnit.HL_VIN_HA, null, "VITI_00001");
        RefDestination d2_VITI_00010 = testDatas.createRefDestination(vigne, "Circuit Long / Frais", Sector.VITICULTURE, YealdUnit.KG_RAISIN_HA, null, "VITI_00010");
        RefDestination d3_MARA_00089 = testDatas.createRefDestination(poireau1, "Toutes catégories - tous calibres", Sector.MARAICHAGE, YealdUnit.TONNE_HA, null, "MARA_00089");
        RefDestination d4_MARA_00090 = testDatas.createRefDestination(poireau1, "Toutes catégories - tous calibres", Sector.MARAICHAGE, YealdUnit.UNITE_HA, null, "MARA_00090");

        // Strategy Prices

        // limit to 3 decade

        // vigne VITI_00001
        testDatas.createRefHarvestingPrice(
                vigne, d1_VITI_00001, false, Calendar.AUGUST, 3, 1960, PriceUnit.EURO_KG_GRAPE, 2017, "spVigne_VITI_00001");


        // vigne VITI_00010
        testDatas.createRefHarvestingPrice(
                vigne, d2_VITI_00010, false, Calendar.AUGUST, 3, 2230, PriceUnit.EURO_KG_GRAPE, 2017, "spVigne_VITI_00010");


        // poireau _MARA_00089
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.APRIL, 1, 1735, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 1, 95, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 2, 825, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 3, 800, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.JUNE, 1, 1600, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.JUNE, 2, 100, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");


        // poireau MARA_00090
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.APRIL, 1, 1690, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.MAY, 1, 80, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.MAY, 3, 835, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 1, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 2, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 3, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");

        // vigne VITI_00001 bio
        testDatas.createRefHarvestingPrice(
                vigne, d1_VITI_00001, true,  Calendar.AUGUST, 3, 2000, PriceUnit.EURO_KG_GRAPE, 2017,  "spVigne_bio_VITI_00001");
        // vigne VITI_00010 bio
        testDatas.createRefHarvestingPrice(
                vigne, d2_VITI_00010, true, Calendar.AUGUST, 3, 2040, PriceUnit.EURO_KG_GRAPE, 2017,  "spVigne_bio_VITI_00010");
        // poireau _MARA_00089 bio
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, true, Calendar.MAY, 1, 295, PriceUnit.EURO_T, 2017, "spPoireau_bio_MARA_00089");

        Domain d0 = testDatas.createSimpleDomain(
                domainDao,
                testDatas.getDomainId("Baulon", 2017),
                location,
                testDatas.createRefLegalStatus(refLegalStatusDao),
                "Baulon",
                2017,
                "Annie Verssaire",
                "Lorem ipsum dolor sit amet",
                100.0d,
                9.0d,
                1.0d,
                true
        );
        GrowingPlan gp = testDatas.createGrowingPlan(d0);
        GrowingSystem gs = testDatas.createGrowingSystem(gp);

        CroppingPlanEntry cpePoireau0 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "cpe_poireau_0",
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "CPE4",
                CroppingPlanEntry.PROPERTY_NAME, "Poireau",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN,
                CroppingPlanEntry.PROPERTY_MIX_SPECIES, true
        );

        CroppingPlanSpecies especePoireauArmor0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "poireau1_varitePoireauArmor_0",
                CroppingPlanSpecies.PROPERTY_CODE, "e_p0",
                CroppingPlanSpecies.PROPERTY_SPECIES, poireau1,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpePoireau0,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + poireau1.getLibelle_espece_botanique() + " " + varitePoireauArmor.getDenomination(),
                CroppingPlanSpecies.PROPERTY_VARIETY, varitePoireauArmor
        );
        cpePoireau0.addCroppingPlanSpecies(especePoireauArmor0);

        CroppingPlanSpecies especePoireauAzur = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "poireau2_varitePoireauAzur_0",
                CroppingPlanSpecies.PROPERTY_CODE, "e_p2",
                CroppingPlanSpecies.PROPERTY_SPECIES, poireau2,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpePoireau0,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + poireau2.getLibelle_espece_botanique()+ " " + varitePoireauAzur.getDenomination(),
                CroppingPlanSpecies.PROPERTY_VARIETY, varitePoireauAzur
        );
        cpePoireau0.addCroppingPlanSpecies(especePoireauAzur);
        croppingPlanEntryDao.updateAll(Lists.newArrayList(cpePoireau0));

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorGrossIncome_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "PS_SDC_TEST",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, gs,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply("2016,2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true);


        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cpePoireau0.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedSpeciesStade especePoireauArmorSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode());
        PracticedSpeciesStade intervention1StadesSpeciesStade = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode());
        PracticedIntervention i4b = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_TOPIA_ID, "i4b_Id",
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "i4b",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "9/4",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "22/6",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.1,
                PracticedIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(especePoireauArmorSpeciesStade, intervention1StadesSpeciesStade));

        //MARA_00089 rendement total 20%
        //MARA_00089 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 20%
        HarvestingActionValorisation evd_l14 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l14_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 20,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );

        //MARA_00089 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 0%
        HarvestingActionValorisation evd_l14bis = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l14bis_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 0,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );

        // MARA_00090 rendement total 40%
        // MARA_00090 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 32%
        HarvestingActionValorisation evd_l15 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l15_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 32,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );


        // MARA_00090 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 8
        HarvestingActionValorisation evd_l16 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l16_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );

        // poireau i4a
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_l14, evd_l14bis, evd_l15, evd_l16),
                HarvestingAction.PROPERTY_PRACTICED_INTERVENTION, i4b,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l16,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0,
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l15,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0,
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );
        // no price for this valorisation
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l14bis,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );

        // poireau
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l14,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 300.0,
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );


        PracticedSpeciesStade especePoireauArmorSpeciesStadeI1 = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode());
        PracticedSpeciesStade intervention1StadesSpeciesStadeI1 = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode());
        PracticedIntervention inter1 = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_TOPIA_ID, "inter1_Id",
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "inter1",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "31/8",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "25/9",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.6,
                PracticedIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(especePoireauArmorSpeciesStadeI1, intervention1StadesSpeciesStadeI1)
        );

        HarvestingActionValorisation evd_l17 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l17_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 20,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l17,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, PricesService.GET_HARVESTING_PRICE_DISPLAY_NAME.apply(especePoireauArmor0),
                HarvestingPrice.PROPERTY_PRICE, null,
                HarvestingPrice.PROPERTY_DOMAIN, d0
        );

        HarvestingActionValorisation evd_l18 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_TOPIA_ID, "evd_l18_Id",
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 32,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );

        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_l17, evd_l18),
                HarvestingAction.PROPERTY_PRACTICED_INTERVENTION, inter1,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );

        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(true);
        indicatorFilter.setWithoutAutoConsumed(true);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);

        IndicatorGrossIncome indicatorGrossIncome = serviceFactory.newInstance(IndicatorGrossIncome.class);

        IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = serviceFactory.newInstance(IndicatorStandardisedGrossIncome.class);
        indicatorStandardisedGrossIncome.init(indicatorFilter);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorGrossIncome, indicatorStandardisedGrossIncome);
        String content = out.toString();

        // Données validées par l'INRAE
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut réel sans l’autoconsommation (€/ha);15858.0;82;;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut réel avec l’autoconsommation (€/ha);20806.0;82;;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut standardisé sans l'autoconsommation, millésimé (€/ha);17644.667;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut standardisé avec l'autoconsommation, millésimé (€/ha);23261.0;100;;");

        especePoireauArmor0.setSpeciesArea(70);
        especePoireauAzur.setSpeciesArea(30);
        croppingPlanSpeciesDao.update(especePoireauArmor0);
        croppingPlanSpeciesDao.update(especePoireauAzur);

        cpePoireau0.setMixSpecies(false);
        cpePoireau0.setMixVariety(false);
        croppingPlanEntryDao.update(cpePoireau0);

        out = new StringWriter();
        writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);

        indicatorStandardisedGrossIncome.init(indicatorFilter);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorGrossIncome, indicatorStandardisedGrossIncome);
        content = out.toString();

        // Données validées par l'INRAE
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut réel sans l’autoconsommation (€/ha);9217.4;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut réel avec l’autoconsommation (€/ha);10797.8;83;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut standardisé sans l'autoconsommation, millésimé (€/ha);10414.6;");
        assertThat(content).usingComparator(comparator).isEqualTo("practicedSystemSheet;Baulon;;;gsName;;;NON;PS_SDC_TEST;NON;;2016, 2017;Synthétisé;Indicateur économique;Produit brut standardisé avec l'autoconsommation, millésimé (€/ha);12409.367;");
    }

//
//                                           Date de mise à jour   #12473                           Revue des test unita
//
//
//  Domaine                                  Baulon 2017
//  Dispositif                               Dispositif test ift
//  Sdc nom                                  SdC GC
//  Parcelle nom                             Plot Baulon 2
//  zone nom                                 Zone principale
//
//  culture
//  id                                       nom                   type                             espece              variete              Melange ?
//  cpeVigne1Var2                            Vigne                 main                             vigne               -                    non
//  cpePoireau0                              Poireau               main                             poireau1            varitePoireauArmor   non
//  cpePoireau0                              Poireau               main                             poireau2            varitePoireauAzur    non
//
//  cycle de culture
//  id_culture                               type                  phase
//  cpeVigne1Var1                            perenne               Pleine production
//  cpePoireau0                              perenne               Pleine production
//
//
//
//  INTERVENTIONS
//  id_culture                               intervention          type                             date debut          date fin             debit de chantier                   nb passagefq spatialdebut_pedebut_decadedebut_campafin_periode fin_decade  fin_campagne
//  cpeVigne1Var1                            i1b                   recolte                                    31/08/2017           25/09/2017                                 0,8         1       0,6August             3       2017September              3        2017
//  cpeVigne1Var1                            i1                    recolte                                    31/08/2017           25/09/2017                                 0,8         1       0,6August             3       2017September              3        2017
//  cpePoireau0                              i4a                   recolte                                    04/04/2017           06/04/2017                                 0,8         1       0,1April              1       2017April                  1        2017
//  cpePoireau0                              i4b                   recolte                                    09/04/2017           22/06/2017                                 0,8         1       0,1April              1       2017June                   3        2017
//
//                                                                                                                                                                                                                                                                                                                  PRODUIT BRUT par EspVartDestination                                               reel : 1) test si
//  Valorisations des recoltes                                                                                                                                                                                                                                                                                      PB avec = rendement * (%valor + % autoconso)
//  id_valorisation                          esp                   intervention                     rendement moyen_unitrendement moyen      culture bio                         destinatio%_valorise%_autoco%_nonvalo   prix_recoltprix_unite  unite_cohereprix_ref    prix_ref_uni unite_coheretx_conversion       reel avec autoconso                          reel sans autoconsostd avec autoconsostd sans autoconso
//  evd_l2                                   vigne                 i1                               HL_VIN_HA                              50non                                 d1_VITI_00       100       0           0        100EURO_KG_GRAPnon                 1960EURO_KG_GRAP non                          135                                       675000             675000          13230000          13230000
//  evd_l3                                   vigne                 i1                               KG_RAISIN_HA                           10non                                 d2_VITI_00        50      40          10        200EURO_KG_GRAPnon                 2230EURO_KG_GRAP non                                                                      1800               1000             20070             11150
//  evd_l4                                   poireau1              i4a                              TONNE_HA                               20non                                 d3_MARA_00       100       0           0        300EURO_T      oui                 1735EURO_T       oui                                                                      6000               6000             34700             34700
//  evd_l4bis                                poireau2              i4a                              TONNE_HA                                0non                                 d3_MARA_00       100       0           0-          EURO_T      oui                 1735EURO_T       oui                                                                         0                  0                 0                 0
//  evd_l5                                   poireau1              i4a                              UNITE_HA                               32non                                 d4_MARA_00        50      50           0        150EURO_UNITE  oui                 1690EURO_UNITE   oui                                                                      4800               2400             54080             27040
//  evd_l6                                   poireau2              i4a                              UNITE_HA                                8non                                 d4_MARA_00        50      50           0        150EURO_UNITE  oui                 1690EURO_UNITE   oui                                                                      1200                600             13520              6760
//  evd_l7                                   vigne                 i1b                              HL_VIN_HA                              50oui                                 d1_VITI_00       100       0           0        250EURO_KG_GRAPnon                 2000EURO_KG_GRAP non                          135                                      1687500            1687500          13500000          13500000
//  evd_l8                                   vigne                 i1b                              KG_RAISIN_HA                           10oui                                 d2_VITI_00        50      40          10        350EURO_KG_GRAPnon                 2040EURO_KG_GRAP non                                                                      3150               1750             18360             10200
//  evd_l14                                  poireau1              i4b                              TONNE_HA                               20non                                 d3_MARA_00       100       0           0        300EURO_T      oui                  ###EURO_T       oui                                                                      6000               6000  17183,3333333333  17183,3333333333
//  evd_l14bis                               poireau2              i4b                              TONNE_HA                                0non                                 d3_MARA_00       100       0           0-          EURO_T      oui                  ###EURO_T       oui                                                                         0                  0                 0                 0
//  evd_l15                                  poireau1              i4b                              UNITE_HA                               32non                                 d4_MARA_00        50      50           0        150EURO_UNITE  oui                  ###EURO_UNITE   oui                                                                      4800               2400  15493,3333333333  7746,66666666667
//  evd_l16                                  poireau2              i4b                              UNITE_HA                                8non                                 d4_MARA_00        50      50           0        150EURO_UNITE  oui                  ###EURO_UNITE   oui                                                                      1200                600  3873,33333333333  1936,66666666667
//
//                                                                                                                                                                                                                                                          Prix ref : f
//  PRODUIT BRUT par interventions / destinat                      Rappel : comme la culture n'est p
//
//  intervention                             destination           reel avec autoconso              reel sans autoconso std avec autoconso   std sans autoconso                            MOYENNE.SI
//  i1                                       d1_VITI_00001                                    675000              675000             13230000                            13230000
//  i1                                       d2_VITI_00010                                      1800                1000                20070                               11150
//  i1b                                      d1_VITI_00001                                   1687500             1687500             13500000                            13500000
//  i1b                                      d2_VITI_00010                                      3150                1750                18360                               10200
//  i4a                                      d3_MARA_00089                                      3000                3000                17350                               17350
//  i4a                                      d4_MARA_00090                                      3000                1500                33800                               16900
//  i4b                                      d3_MARA_00089                                      3000                3000     8591,66666666667                    8591,66666666667
//  i4b                                      d4_MARA_00090                                      3000                1500     9683,33333333333                    4841,66666666667
//
//
//  PRODUIT BRUT par interventions                                 Somme des PB par destination * le
//  intervention                             psci                  reel avec autoconso              reel sans autoconso std avec autoconso   std sans autoconso
//  i1b                                                         0,6                          1014390             1013550              8111016                             8106120
//  i1                                                          0,6                           406080              405600              7950042                             7944690
//  i4a                                                         0,1                              600                 450                 5115                                3425
//  i4b                                                         0,1                              600                 450               1827,5                    1343,33333333333
//
//
//  valeur attendue mise dans le code                                                        1421670             1420050           16068000,5                    16055578,3333333
    @Test
    public void createInraStandardisedGrossIncome_9960_0() throws IOException {
        
        Domain d0 = getRef9960DataTest();
        
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorGrossIncome.class.getSimpleName());
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(true);
        indicatorFilter.setWithoutAutoConsumed(true);
        
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);
        
        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(
                performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);
    
        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);
    
        IndicatorGrossIncome indicatorGrossIncome = serviceFactory.newInstance(IndicatorGrossIncome.class);
    
        IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = serviceFactory.newInstance(IndicatorStandardisedGrossIncome.class);
        indicatorStandardisedGrossIncome.init(indicatorFilter);
    
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorGrossIncome, indicatorStandardisedGrossIncome);
        String content = out.toString();

        // Données validées par l'INRAE
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;;;gsName;;;Plot Baulon 2;Zone principale;Poireau, Vigne;Armor, Azur;;2017;Réalisé;Indicateur économique;Produit brut standardisé avec l'autoconsommation, millésimé (€/ha);1.60680005E7;");
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;;;gsName;;;Plot Baulon 2;Zone principale;Poireau, Vigne;Armor, Azur;;2017;Réalisé;Indicateur économique;Produit brut standardisé sans l'autoconsommation, millésimé (€/ha);1.6055578333E7;");
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;;;gsName;;;Plot Baulon 2;Zone principale;Poireau, Vigne;Armor, Azur;;2017;Réalisé;Indicateur économique;Produit brut réel avec l’autoconsommation (€/ha);1421670.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;;;gsName;;;Plot Baulon 2;Zone principale;Poireau, Vigne;Armor, Azur;;2017;Réalisé;Indicateur économique;Produit brut réel sans l’autoconsommation (€/ha);1420050.0;");
    }
    
    @Test
    public void createInraStandardisedGrossIncome_withAutoConsume() throws IOException {
        
        Domain d0 = getRef9960DataTest();
        
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(true);
        indicatorFilter.setWithoutAutoConsumed(false);
        
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);
        
        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);
    
        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
    
        IndicatorGrossIncome indicatorGrossIncome = serviceFactory.newInstance(IndicatorGrossIncome.class);
    
        IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = serviceFactory.newInstance(IndicatorStandardisedGrossIncome.class);
        indicatorStandardisedGrossIncome.init(indicatorFilter);
    
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorGrossIncome, indicatorStandardisedGrossIncome);
        String content = out.toString();
        // Données validées par l'INRAE
        assertThat(content).usingComparator(comparator).isEqualTo("zoneSheet;Baulon;gsName;Plot Baulon 2;Zone principale;Indicateur économique;Produit brut standardisé avec l'autoconsommation, millésimé (€/ha);2017;Baulon;1.60680005E7;94;;");
        assertThat(content).doesNotContain("Produit brut standardisé sans l’autoconsommation (€/ha);");
    }
    
    @Test
    public void createInraStandardisedGrossIncome_withoutAutoConsume() throws IOException {
        
        Domain d0 = getRef9960DataTest();
        
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(false);
        indicatorFilter.setWithoutAutoConsumed(true);
        
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);
        
        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(
                performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);
    
        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);
    
        IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = serviceFactory.newInstance(IndicatorStandardisedGrossIncome.class);
        indicatorStandardisedGrossIncome.init(indicatorFilter);
    
        IndicatorGrossIncome indicatorGrossIncome = serviceFactory.newInstance(IndicatorGrossIncome.class);
    
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorGrossIncome, indicatorStandardisedGrossIncome);
        String content = out.toString();
        // Données validées par l'INRAE
        assertThat(content).contains("zoneSheet;Baulon;;;gsName;;;Plot Baulon 2;Zone principale;Poireau, Vigne;Armor, Azur;;2017;Réalisé;Indicateur économique;Produit brut standardisé sans l'autoconsommation, millésimé (€/ha);1.6055578333E7;");
        assertThat(content).doesNotContain("Produit brut standardisé avec l'autoconsommation");
    }
    
    @Test
    public void createInraStandardisedGrossIncome_notComputer() throws IOException {
        
        Domain d0 = getRef9960DataTest();
        
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setClazz(IndicatorStandardisedGrossIncome.class.getSimpleName());
        indicatorFilter.setComputeStandardized(true);
        indicatorFilter.setWithAutoConsumed(false);
        indicatorFilter.setWithoutAutoConsumed(false);
        
        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        indicatorFilters.add(indicatorFilter);
        
        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(d0.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);
        
        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        
        IndicatorStandardisedGrossIncome indicatorStandardisedGrossIncome = serviceFactory.newInstance(IndicatorStandardisedGrossIncome.class);
        indicatorStandardisedGrossIncome.init(indicatorFilter);
        
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorStandardisedGrossIncome);
        String content = out.toString();
        
        assertThat(content).doesNotContain("Produit brut standardisé sans autoconsommation, millésimé (€/ha)");
        assertThat(content).doesNotContain("Produit brut standardisé avec autoconsommation, millésimé (€/ha)");
    }
    
    protected Domain getRef9960DataTest() throws IOException {
        RefSolProfondeurIndigo solProfond = refSolProfondeurIndigoDao.forClasse_de_profondeur_INDIGOEquals(PerformanceServiceImpl.DEFAULT_DEEPEST_INDIGO).findUnique();
        
        RefLocation location = testDatas.createRefLocation(refLocationDao);
        Domain d0 = testDatas.createSimpleDomain(
                domainDao,
                testDatas.getDomainId("Baulon", 2017),
                location,
                testDatas.createRefLegalStatus(refLegalStatusDao),
                "Baulon",
                2017,
                "Annie Verssaire",
                "Lorem ipsum dolor sit amet",
                100.0d,
                9.0d,
                1.0d,
                true
        );
        GrowingPlan gp = testDatas.createGrowingPlan(d0);
        GrowingSystem gs = testDatas.createGrowingSystem(gp);
        String plotName = "Plot Baulon 2";
        String plotEdaplosId = "edaplosIssuerIdBaulon2";
        
        
        testDatas.importSolTextureGeppa();
        RefSolTextureGeppa solArgiloSaleuse = refSolTextureGeppaDao.forNaturalId("AS").findUnique();
        RefSolTextureGeppa solSableArgiloLimoneux = refSolTextureGeppaDao.forNaturalId("Sal").findUnique();
        Plot plot = testDatas.createPlot(solProfond, solArgiloSaleuse, solSableArgiloLimoneux, gs, Lists.newArrayList(location), plotName, plotEdaplosId);
        
        testDatas.createZone(plot, ZoneType.PRINCIPALE);
        
        List<Zone> zones = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();
        
        // required imports
        RefInterventionAgrosystTravailEDI mainActionRecolte = refInterventionAgrosystTravailEDIDao.create(RefInterventionAgrosystTravailEDI.PROPERTY_REFERENCE_CODE, "SER", RefInterventionAgrosystTravailEDI.PROPERTY_INTERVENTION_AGROSYST, AgrosystInterventionType.RECOLTE);
        RefEspece vigne = testDatas.createRefEspece(refEspeceDao, "ZMO", "", "Vigne", null);
        RefEspece  poireau1 = testDatas.createRefEspece(refEspeceDao, "ZDN", "", "Poireau", "ZMP");
        RefEspece  poireau2 = testDatas.createRefEspece(refEspeceDao, "ZDN", "", "Poireau", "H58");
        
        refEspeceToVarieteDao.create(
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI, poireau1.getCode_espece_botanique(),
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL, "20906",
                RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE, "INRA");
        
        RefVarieteGeves varitePoireauArmor = refVarieteGevesDao.create(RefVarieteGeves.PROPERTY_GROUPE, 10, RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, 20906, RefVarieteGeves.PROPERTY_NOM__FRANCAIS, "Poireau", RefVarieteGeves.PROPERTY_DENOMINATION, "Armor");
        RefVarieteGeves varitePoireauAzur = refVarieteGevesDao.create(RefVarieteGeves.PROPERTY_GROUPE, 10, RefVarieteGeves.PROPERTY_NUM__ESPECE__BOTANIQUE, 20906, RefVarieteGeves.PROPERTY_NOM__FRANCAIS, "Poireau", RefVarieteGeves.PROPERTY_DENOMINATION, "Azur");
        
        
        refSpeciesToSectorDao.createByNaturalId(vigne.getCode_espece_botanique(), vigne.getCode_qualifiant_AEE(), Sector.VITICULTURE);
        refSpeciesToSectorDao.createByNaturalId(poireau1.getCode_espece_botanique(), poireau1.getCode_qualifiant_AEE(), Sector.MARAICHAGE);
        
        RefDestination d1_VITI_00001 = testDatas.createRefDestination(vigne, "Toutes appellations / Tous modes de commercialisation", Sector.VITICULTURE, YealdUnit.HL_VIN_HA, null, "VITI_00001");
        RefDestination d2_VITI_00010 = testDatas.createRefDestination(vigne, "Circuit Long / Frais", Sector.VITICULTURE, YealdUnit.KG_RAISIN_HA, null, "VITI_00010");
        RefDestination d3_MARA_00089 = testDatas.createRefDestination(poireau1, "Toutes catégories - tous calibres", Sector.MARAICHAGE, YealdUnit.TONNE_HA, null, "MARA_00089");
        RefDestination d4_MARA_00090 = testDatas.createRefDestination(poireau1, "Toutes catégories - tous calibres", Sector.MARAICHAGE, YealdUnit.UNITE_HA, null, "MARA_00090");
        
        // Strategy Prices
        
        // limit to 3 decade
        
        // vigne VITI_00001
        testDatas.createRefHarvestingPrice(
                vigne, d1_VITI_00001, false, Calendar.AUGUST, 3, 1960, PriceUnit.EURO_KG_GRAPE, 2017, "spVigne_VITI_00001");
        
        
        // vigne VITI_00010
        testDatas.createRefHarvestingPrice(
                vigne, d2_VITI_00010, false, Calendar.AUGUST, 3, 2230, PriceUnit.EURO_KG_GRAPE, 2017, "spVigne_VITI_00010");
        
        
        // poireau _MARA_00089
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.APRIL, 1, 1735, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 1, 95, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 2, 825, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.MAY, 3, 800, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.JUNE, 1, 1600, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, false, Calendar.JUNE, 2, 100, PriceUnit.EURO_T, 2017, "spPoireau_MARA_00089");
        
        
        // poireau MARA_00090
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.APRIL, 1, 1690, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.MAY, 1, 80, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.MAY, 3, 835, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 1, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 2, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        testDatas.createRefHarvestingPrice(
                poireau1, d4_MARA_00090, false, Calendar.JUNE, 3, 100, PriceUnit.EURO_UNITE, 2017, "spPoireau_MARA_00090");
        
        // vigne VITI_00001 bio
        testDatas.createRefHarvestingPrice(
                vigne, d1_VITI_00001, true,  Calendar.AUGUST, 3, 2000, PriceUnit.EURO_KG_GRAPE, 2017,  "spVigne_bio_VITI_00001");
        // vigne VITI_00010 bio
        testDatas.createRefHarvestingPrice(
                vigne, d2_VITI_00010, true, Calendar.AUGUST, 3, 2040, PriceUnit.EURO_KG_GRAPE, 2017,  "spVigne_bio_VITI_00010");
        // poireau _MARA_00089 bio
        testDatas.createRefHarvestingPrice(
                poireau1, d3_MARA_00089, true, Calendar.MAY, 1, 295, PriceUnit.EURO_T, 2017, "spPoireau_bio_MARA_00089");
        
        // unité incohérente
        YealdUnit yu = d1_VITI_00001.getYealdUnit();//HL_VIN_HA
        double convertionRate = 135d;
        testDatas.createRefHarvestingPriceConverter(d1_VITI_00001, yu, PriceUnit.EURO_KG_GRAPE, convertionRate);
        
        // unité cohérente
        yu = d2_VITI_00010.getYealdUnit();//KG_RAISIN_HA
        convertionRate = 1d;
        testDatas.createRefHarvestingPriceConverter(d2_VITI_00010, yu, PriceUnit.EURO_KG_GRAPE, convertionRate);
        
        CroppingPlanEntry cpeVigne1Var1 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "CPE0",
                CroppingPlanEntry.PROPERTY_NAME, "Vigne",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );
        
        CroppingPlanSpecies cpeVigne = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, "e_p",
                CroppingPlanSpecies.PROPERTY_SPECIES, vigne,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpeVigne1Var1,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + vigne.getLibelle_espece_botanique()
        );
        
        cpeVigne1Var1.addCroppingPlanSpecies(cpeVigne);
        
        CroppingPlanEntry cpePoireau0 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "CPE4",
                CroppingPlanEntry.PROPERTY_NAME, "Poireau",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );
        
        CroppingPlanSpecies especePoireauArmor0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, "e_p0",
                CroppingPlanSpecies.PROPERTY_SPECIES, poireau1,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpePoireau0,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + poireau1.getLibelle_espece_botanique() + " " + varitePoireauArmor.getDenomination(),
                CroppingPlanSpecies.PROPERTY_VARIETY, varitePoireauArmor
        );
        
        CroppingPlanSpecies especePoireauAzur = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, "e_p2",
                CroppingPlanSpecies.PROPERTY_SPECIES, poireau2,
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpePoireau0,
                CroppingPlanSpecies.PROPERTY_CODE, "CPS_" + poireau2.getLibelle_espece_botanique()+ " " + varitePoireauAzur.getDenomination(),
                CroppingPlanSpecies.PROPERTY_VARIETY, varitePoireauAzur
        );
        
        cpePoireau0.addCroppingPlanSpecies(especePoireauArmor0);
        cpePoireau0.addCroppingPlanSpecies(especePoireauAzur);
        croppingPlanEntryDao.updateAll(Lists.newArrayList(cpeVigne1Var1, cpePoireau0));
        
        EffectiveCropCyclePhase cropCyclePhaseVigne = cropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);
        
        EffectiveCropCyclePhase cropCyclePhasePoireau = cropCyclePhaseDAO.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 20,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);
        
        
        effectivePerennialCropCycleDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cpeVigne1Var1,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhaseVigne,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);
        
        effectivePerennialCropCycleDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cpePoireau0,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhasePoireau,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);
        
        EffectiveSpeciesStade intervention1bStades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, cpeVigne);
        
        EffectiveIntervention i1b = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhaseVigne,
                EffectiveIntervention.PROPERTY_NAME, "i1b",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 8, 31),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 9, 25),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.8,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.6,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(intervention1bStades)
        );
        
        
        HarvestingActionValorisation evd_l2 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpeVigne.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.SEPTEMBER,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d1_VITI_00001,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 50,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.HL_VIN_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0
        );
        
        HarvestingActionValorisation evd_l3 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpeVigne.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.SEPTEMBER,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d2_VITI_00010,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 10,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.KG_RAISIN_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 40,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 10
        );
        
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, cpeVigne);
        
        EffectiveIntervention i1 = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhaseVigne,
                EffectiveIntervention.PROPERTY_NAME, "i1",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 8, 31),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 9, 25),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.8,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.6,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(intervention1Stades)
        );
        
        
        // vigne i1a
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_l2, evd_l3),
                HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION, i1,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(cpeVigne, d1_VITI_00001),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l2,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG_GRAPE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(Vigne1 - Var 1 - Toutes appellations / Tous modes de commercialisation)",
                HarvestingPrice.PROPERTY_PRICE, 100.0
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(cpeVigne, d2_VITI_00010),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l3,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG_GRAPE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(Vigne1 - Var 1 - Circuit Long / Frais)",
                HarvestingPrice.PROPERTY_PRICE, 200.0
        );
        
        EffectiveSpeciesStade i4aSpeciesStadeP1V1 = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especePoireauArmor0);
        EffectiveSpeciesStade i4aSpeciesStadeP2V2 = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especePoireauAzur);
        
        EffectiveIntervention i4a = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhasePoireau,
                EffectiveIntervention.PROPERTY_NAME, "i4a",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 4, 4),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 4, 6),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.8,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.1,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(i4aSpeciesStadeP1V1, i4aSpeciesStadeP2V2)
        );
        
        //MARA_00089 rendement total 20%
        //MARA_00089 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 20%
        HarvestingActionValorisation evd_l4 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 20,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        //MARA_00089 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 0%
        HarvestingActionValorisation evd_l4bis = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 0,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        // MARA_00090 rendement total 40%
        // MARA_00090 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 32%
        HarvestingActionValorisation evd_l5 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 32,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        // MARA_00090 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 8
        HarvestingActionValorisation evd_l6 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        // poireau i4a
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_l4, evd_l4bis, evd_l5, evd_l6),
                HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION, i4a,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );
        
        // no price for this valorisation
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l4bis,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)"
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l5,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0
        );
        
        // poireau
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l4,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 300.0
        );


        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l6,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0
        );
        
        HarvestingActionValorisation evd_l7 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpeVigne.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.SEPTEMBER,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d1_VITI_00001,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 50,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.HL_VIN_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, true,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0
        );
        
        HarvestingActionValorisation evd_l8 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpeVigne.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.AUGUST,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.SEPTEMBER,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d2_VITI_00010,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 10,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.KG_RAISIN_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, true,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 40,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 10
        );
        
        // vigne i1b
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_l7, evd_l8),
                HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION, i1b,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );
        
        // price on organic crop
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(cpeVigne, d1_VITI_00001),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l7,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG_GRAPE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(Vigne1 - Var 1 - Toutes appellations / Tous modes de commercialisation)",
                HarvestingPrice.PROPERTY_PRICE, 250.0
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(cpeVigne, d2_VITI_00010),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l8,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG_GRAPE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(Vigne1 - Var 1 - Circuit Long / Frais)",
                HarvestingPrice.PROPERTY_PRICE, 350.0
        );
        
        
        EffectiveSpeciesStade i4bSpeciesStadeP1V1 = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especePoireauArmor0);
        EffectiveSpeciesStade i4bSpeciesStadeP2V2 = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, especePoireauAzur);
        
        EffectiveIntervention i4b = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhasePoireau,
                EffectiveIntervention.PROPERTY_NAME, "i4b",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.RECOLTE,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(2017, 4, 9),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(2017, 6, 22),
                EffectiveIntervention.PROPERTY_WORK_RATE, 0.8,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.1,
                EffectiveIntervention.PROPERTY_SPECIES_STADES, Lists.newArrayList(i4bSpeciesStadeP1V1, i4bSpeciesStadeP2V2)
        );
        
        //MARA_00089 rendement total 20%
        //MARA_00089 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 20%
        HarvestingActionValorisation evd_l14 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 20,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        //MARA_00089 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 0%
        HarvestingActionValorisation evd_l14bis = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d3_MARA_00089,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 0,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 0,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        // no price for this valorisation
        
        // MARA_00090 rendement total 40%
        // MARA_00090 (poireau1 - Var1 - Toutes catégories - tous calibres) rendement 32%
        HarvestingActionValorisation evd_l15 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauArmor0.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 32,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        // MARA_00090 (poireau2 - Var2 - Toutes catégories - tous calibres) rendement 8
        HarvestingActionValorisation evd_l16 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, especePoireauAzur.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, Calendar.APRIL,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, Calendar.JUNE,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2017,
                HarvestingActionValorisation.PROPERTY_DESTINATION, d4_MARA_00090,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.UNITE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 50,
                HarvestingActionValorisation.PROPERTY_SELF_CONSUMED_PERSENT, 50,
                HarvestingActionValorisation.PROPERTY_NO_VALORISATION_PERCENT, 0
        );
        
        // poireau i4a
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_VALORISATIONS, Lists.newArrayList(evd_l14, evd_l14bis, evd_l15, evd_l16),
                HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION, i4b,
                HarvestingAction.PROPERTY_MAIN_ACTION, mainActionRecolte
        );
        
        // poireau
        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l14,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 300.0
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d3_MARA_00089),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l14bis,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_T,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)"
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauArmor0, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l15,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau1 - Var1 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0
        );

        harvestingPriceDao.create(
                HarvestingPrice.PROPERTY_DOMAIN, d0,
                HarvestingPrice.PROPERTY_OBJECT_ID, PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(especePoireauAzur, d4_MARA_00090),
                HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, evd_l16,
                HarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                HarvestingPrice.PROPERTY_DISPLAY_NAME, "(poireau2 - Var2 - Toutes catégories - tous calibres)",
                HarvestingPrice.PROPERTY_PRICE, 150.0
        );
        return d0;
    }
    
    
}
