package fr.inra.agrosyst.services.domain;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainImpl;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;

import java.util.Collection;
import java.util.List;

class DomainServiceImplTest extends DomainServiceImpl {

    public DomainServiceImplTest() {
    }

    public DomainServiceImplTest(DomainServiceImpl domainService) {
        super.context = domainService.getContext();
    }

    public boolean validDomainPreconditions(Domain domain, String locationId) {
        boolean isValid = true;
        try {
            super.validPreconditions(domain, locationId, null, null, null, null, null);
        } catch (IllegalArgumentException e) {
            isValid = false;
        }
        return isValid;
    }

    public boolean validDomainGeoPointsPreconditions(List<GeoPoint> geoPoints) {
        boolean isValid = true;

        Domain mock = getValidDomain();

        try {
            super.validPreconditions(mock, "locationId", geoPoints, null, null, null, null);
        } catch (NullPointerException e) {
            isValid = false;
        }
        return isValid;
    }

    public boolean validDomainToolsCouplingsPreconditions(List<ToolsCoupling> toolsCouplings) {
        boolean isValid = true;

        Domain mock = getValidDomain();

        try {
            super.validPreconditions(mock, "locationId", null, toolsCouplings, null, null, null);
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;
    }

    public boolean validDomainCropsPreconditions(Collection<CroppingPlanEntryDto> croppingPlanEntryDtos) {
        boolean isValid = true;

        Domain mock = getValidDomain();

        try {
            super.validPreconditions(mock, "locationId", null, null, croppingPlanEntryDtos, null, null);
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;
    }

    protected Domain getValidDomain() {
        Domain mock = new DomainImpl();
        mock.setCampaign(2014);
        mock.setType(DomainType.DOMAINE_EXPERIMENTAL);
        mock.setName("MOCK");
        mock.setMainContact("MAIN CONTACT");
        return mock;
    }

    public void setDomainSpeciesToArea(Domain domain, Collection<CroppingPlanEntryDto> croppingPlanDtos) {
        super.setDomainSpeciesToArea(domain, croppingPlanDtos);
    }
}
