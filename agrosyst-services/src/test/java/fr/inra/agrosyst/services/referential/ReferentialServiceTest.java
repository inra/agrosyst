package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.AgrosystTopiaApplicationContext;
import fr.inra.agrosyst.api.entities.AgrosystTopiaPersistenceContext;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.DestinationContext;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPCTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitImpl;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAA;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFAImpl;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterImpl;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCibleImpl;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCibleTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteurTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutilTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTractionTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClassTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnits;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnitsTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgricultureTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.domain.inputStock.search.DomainPhytoProductInputSearchResults;
import fr.inra.agrosyst.api.services.domain.inputStock.search.PhytoProductInputFilter;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.referential.TypeMaterielFilter;
import fr.inra.agrosyst.api.services.security.AuthenticationService;
import fr.inra.agrosyst.api.services.users.UserService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.AgrosystConfigurationHelper;
import fr.inra.agrosyst.services.AgrosystServiceConfig;
import fr.inra.agrosyst.services.HarvestinActionTestData;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.boot.Metadata;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test du service referentiel.
 *
 * @author Eric Chatellier
 */
public class ReferentialServiceTest extends AbstractAgrosystTest {

    protected ReferentialService referentialService;
    protected AuthenticationService authenticationService;
    protected UserService userService;
    protected ImportService importService;
    protected AgrosystI18nService i18nService;

    private RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;
    private HarvestingActionValorisationTopiaDao harvestingActionValorisationDao;
    private RefDestinationTopiaDao refDestinationDao;
    private CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    private RefHarvestingPriceTopiaDao refHarvestingPriceDao;
    private RefEspeceTopiaDao refEspeceDao;
    private RefSeedUnitsTopiaDao refSeedUnitsDao;
    private CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    private RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao;
    private RefTypeAgricultureTopiaDao refTypeAgricultureTopiaDao;

    @BeforeEach
    public void setupServices() {
        loginAsAdmin();
        alterSchema();

        authenticationService = serviceFactory.newService(AuthenticationService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        userService = serviceFactory.newService(UserService.class);
        importService = serviceFactory.newService(ImportService.class);
        i18nService = serviceFactory.newService(AgrosystI18nService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        harvestingActionValorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();
        refDestinationDao = getPersistenceContext().getRefDestinationDao();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        refHarvestingPriceDao = getPersistenceContext().getRefHarvestingPriceDao();
        refEspeceDao = getPersistenceContext().getRefEspeceDao();
        refSeedUnitsDao = getPersistenceContext().getRefSeedUnitsDao();
        refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        refTypeAgricultureTopiaDao = getPersistenceContext().getRefTypeAgricultureDao();
    }

    /**
     * Test de la recherche des valeurs de typeMateriel1 parmis tous les materiels disponibles.
     */
    @Test
    public void testGetTypeMateriel1List() throws IOException {
        testDatas.importMateriels();
        TypeMaterielFilter filter = new TypeMaterielFilter(false);
        Map<MaterielType, List<Pair<String, String>>> values = referentialService.getWithoutPetitMaterielTypeMateriel1List(false);
        Assertions.assertEquals(4, values.size());
        Assertions.assertEquals(5, values.get(MaterielType.TRACTEUR).size());
        Assertions.assertEquals(7, values.get(MaterielType.AUTOMOTEUR).size());
        Assertions.assertEquals(10, values.get(MaterielType.OUTIL).size());
        Assertions.assertEquals(4, values.get(MaterielType.IRRIGATION).size());
    }

    /**
     * Test de la recherche des valeurs de typeMateriel2 parmis tous les materiels disponibles.
     */
    @Test
    public void testGetTypeMateriel2List() throws IOException {
        testDatas.importMateriels();
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "TRACTEURS SPECIALISES 4RM VIGNE");
        filter.setType(MaterielType.TRACTEUR);
        List<Pair<String, String>> values = referentialService.getTypeMateriel2List(filter);
        Assertions.assertEquals(1, values.size());
    }

    /**
     * Test de la recherche des valeurs de typeMateriel3 parmis tous les materiels disponibles.
     */
    @Test
    public void testGetTypeMateriel3List() throws IOException {
        testDatas.importMateriels();
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "TRACTEURS SPECIALISES 4RM VIGNE", "Tracteurs Spécialisés Vigne 4 RM avec cabine confort");
        filter.setType(MaterielType.TRACTEUR);
        List<Pair<String, String>> values = referentialService.getTypeMateriel3List(filter);
        Assertions.assertEquals(6, values.size());
    }

    /**
     * Test de la recherche des valeurs de typeMateriel4 parmis tous les materiels disponibles.
     */
    @Test
    public void testGetTypeMateriel4List() throws IOException {
        testDatas.importMateriels();
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "TRACTEURS SPECIALISES 4RM VIGNE", "Tracteurs Spécialisés Vigne 4 RM avec cabine confort",
                "46" +
                        " " +
                        "à" +
                        " " +
                        "55 ch");
        filter.setType(MaterielType.TRACTEUR);
        List<Pair<String, String>> values = referentialService.getTypeMateriel4List(filter);
        Assertions.assertEquals(1, values.size());
    }

    /**
     * Test de la recherche des valeurs de typeMateriel4 parmis tous les materiels disponibles.
     */
    @Test
    public void testFindSpecies() {
        InputStream especes = ReferentialServiceTest.class.getResourceAsStream("/referentiels/refEspece.csv");
        importService.importEspeces(especes);
        InputStream geves = ReferentialServiceTest.class.getResourceAsStream("/referentiels/test/varietes_GEVES.csv");
        importService.importVarietesGeves(geves);
        InputStream plantGrape = ReferentialServiceTest.class.getResourceAsStream("/referentiels/test/varietes_PlantGrape.csv");
        importService.importVarietesPlantGrape(plantGrape);
        InputStream culturesVarietes = ReferentialServiceTest.class.getResourceAsStream("/referentiels/test/cultures_varietes.csv");
        importService.importEspecesToVarietes(culturesVarietes);

        { // Carottes
            List<RefEspece> species = referentialService.findSpecies("grosti");
            Assertions.assertEquals(6, species.size());

            species = referentialService.findSpecies("grostide stolonifèr");
            Assertions.assertEquals(1, species.size());

            RefEspece espece = species.getFirst();
            List<RefVariete> varietes = referentialService.getVarietes(espece.getTopiaId(), "h");
            Assertions.assertEquals(2, varietes.size());
        }

        { // Vigne
            List<RefEspece> species = referentialService.findSpecies("Vigne");
            Assertions.assertEquals(2, species.size());

            RefEspece espece = species.getFirst();
            Assertions.assertEquals("ZMO", espece.getCode_espece_botanique());
            List<RefVariete> varietes = referentialService.getVarietes(espece.getTopiaId(), "lima");
            Assertions.assertEquals(2, varietes.size());
            Assertions.assertTrue(varietes.get(0) instanceof RefVarieteGeves);
            Assertions.assertTrue(varietes.get(1) instanceof RefVarietePlantGrape);
        }

    }

    @Test
    public void testTranslateEspeces() throws IOException {
        testDatas.insertReferentialTranslations(getPersistenceContext());

        InputStream especes = ReferentialServiceTest.class.getResourceAsStream("/referentiels/refEspece.csv");
        importService.importEspeces(especes);

        List<RefEspece> species = referentialService.findSpecies("grostide stolonifèr");
        Assertions.assertEquals(1, species.size());
        RefEspece espece = species.getFirst();
        Assertions.assertTrue(espece.getLibelle_espece_botanique_Translated().endsWith("stolonifère i18n"));

        getPersistenceContext().commit();

        RefEspece refEspece = refEspeceDao.forTopiaIdEquals(espece.getTopiaId()).findUnique();
        Assertions.assertFalse(refEspece.getLibelle_espece_botanique().endsWith("stolonifère i18n"));

        refEspeceDao.clear();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);
        i18nService.fillRefEspeceTranslations(List.of(espece.getTopiaId()), translationMap);
        ReferentialEntityTranslation entityTranslation = translationMap.getEntityTranslation(espece.getTopiaId());
        String libelle = entityTranslation.getPropertyTranslation(RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "");
        Assertions.assertTrue(libelle.endsWith("stolonifère i18n"));

    }

    /**
     * Certains types de matériel sont écrits avec des espaces en trop mais références
     * la même chose.
     * Test de non régression: #3133
     */
    @Test
    public void testMaterielTypeWithAdditionalSpaces() throws IOException {
        testDatas.importFrTradRefMateriels();
        testDatas.importMaterielsOutilsVitiArbo();

        TypeMaterielFilter filter = new TypeMaterielFilter(false, "Broyeur");
        filter.setType(MaterielType.OUTIL);
        List<Pair<String, String>> values = referentialService.getTypeMateriel2List(filter);
        List<String> results = values.stream().map(Pair::getKey).toList();
        Assertions.assertTrue(results.contains("Broyeur porté VL"));
        Assertions.assertFalse(results.contains("Broyeur porté  VL"));
    }

    @Test
    public void testReferenceDose() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importActaDosageSpc();
        testDatas.importActaGroupeCultures();
        testDatas.importCountries();

        List<RefEspece> species = referentialService.findSpecies("Ble tendre");

        testDatas.importActaTraitementsProduits();
        testDatas.getDefaultFranceCountry();

        RefEspece espece = species.stream().filter(input -> {
            return "ZFB".equals(input.getCode_type_saisonnier_AEE()); // Hiver
        }).findFirst().get();
        final RefCountry france = testDatas.getDefaultFranceCountry();

        { // Dispo pour id_culture 23 (blé tendre hiver) et id_culture 21 (blé)
            var result = refActaTraitementsProduitDao.forNaturalId("5166", 147, france).findUnique();
            ReferenceDoseDTO referenceDoseDTO = referentialService.computeLocalizedReferenceDoseForIFTLegacy(result.getTopiaId(), Sets.newHashSet(espece.getTopiaId()));
            Assertions.assertEquals(240d, referenceDoseDTO.getValue(), 0.001d);
            Assertions.assertEquals(PhytoProductUnit.G_HA, referenceDoseDTO.getUnit());
        }

        { // Dispo pour id_culture 21 (blé) seulement
            var result = refActaTraitementsProduitDao.forNaturalId("4632", 140, france).findUnique();
            ReferenceDoseDTO referenceDoseDTO = referentialService.computeLocalizedReferenceDoseForIFTLegacy(result.getTopiaId(), Sets.newHashSet(espece.getTopiaId()));
            Assertions.assertEquals(1.5d, referenceDoseDTO.getValue(), 0.001d);
            Assertions.assertEquals(PhytoProductUnit.L_HA, referenceDoseDTO.getUnit());
        }

        { // Dispo pour id_culture 21 (blé) : 2 valeurs, 1 et 1,5, on prend le min
            var result = refActaTraitementsProduitDao.forNaturalId("4629", 140, france).findUnique();
            ReferenceDoseDTO doseDTO = referentialService.computeLocalizedReferenceDoseForIFTLegacy(result.getTopiaId(), Sets.newHashSet(espece.getTopiaId()));
            Assertions.assertEquals(1d, doseDTO.getValue(), 0.001d);
            Assertions.assertEquals(PhytoProductUnit.L_HA, doseDTO.getUnit());
        }

        // TODO DCossé 03/09/14 test does not work 
        species = referentialService.findSpecies("Vigne");
        espece = species.stream().filter(input -> "J09".equals(input.getCode_categorie_de_cultures()) && "H61".equals(input.getCode_destination_AEE())).findFirst().get();
        // without couple phyto product x Zones cultivées
        {
            var result = refActaTraitementsProduitDao.forNaturalId("3494", 147, france).findUnique();
            ReferenceDoseDTO referenceDoseDTO = referentialService.computeLocalizedReferenceDoseForIFTLegacy(result.getTopiaId(), Sets.newHashSet(espece.getTopiaId()));
            Assertions.assertEquals(3.2d, referenceDoseDTO.getValue(), 0.001d);
            Assertions.assertEquals(PhytoProductUnit.L_HA, referenceDoseDTO.getUnit());
        }

        // value found from couple phyto product x Zones cultivées
        {
            var result = refActaTraitementsProduitDao.forNaturalId("34941", 147, france).findUnique();
            ReferenceDoseDTO referenceDoseDTO = referentialService.computeLocalizedReferenceDoseForIFTLegacy(result.getTopiaId(), Sets.newHashSet(espece.getTopiaId()));
            Assertions.assertEquals(2.4d, referenceDoseDTO.getValue(), 0.001d);
            Assertions.assertEquals(PhytoProductUnit.L_HA, referenceDoseDTO.getUnit());
        }
    }

    @Test
    public void testCacheOnActaTraitementsProduitsCateg() throws IOException {
        Assertions.assertFalse(getConfig().isBusinessCachingEnabled());
        getConfig().setBusinessCachingEnabled(true);
        Assertions.assertTrue(getConfig().isBusinessCachingEnabled());

        testDatas.importActaTraitementsProduitsCateg();

        Map<AgrosystInterventionType, List<ProductType>> map = referentialService.getAllActiveActaTreatmentProductTypes();
        Assertions.assertEquals(3, map.size());

        Map<AgrosystInterventionType, List<ProductType>> map2 = referentialService.getAllActiveActaTreatmentProductTypes();
        Assertions.assertEquals(3, map2.size());

        Assertions.assertTrue(map == map2); // Want to compare references !
    }

    @Test
    public void testCacheOnFindAllActiveAgrosystActions() throws IOException {
        Assertions.assertFalse(getConfig().isBusinessCachingEnabled());
        getConfig().setBusinessCachingEnabled(true);
        Assertions.assertTrue(getConfig().isBusinessCachingEnabled());

        testDatas.importInterventionAgrosystTravailEdi();

        List<RefInterventionAgrosystTravailEDI> list = referentialService.getAllActiveAgrosystActions();
        Assertions.assertEquals(78, list.size());

        List<RefInterventionAgrosystTravailEDI> list2 = referentialService.getAllActiveAgrosystActions();
        Assertions.assertEquals(78, list2.size());

        Assertions.assertSame(list, list2); // Want to compare references !

        List<RefInterventionAgrosystTravailEDI> list3 = referentialService.getAllActiveAgrosystActions(null);
        Assertions.assertEquals(78, list3.size());

        Assertions.assertSame(list, list3); // Want to compare references !

        List<RefInterventionAgrosystTravailEDI> list4 = referentialService.getAllActiveAgrosystActions(AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        Assertions.assertEquals(1, list4.size());

        Assertions.assertNotSame(list, list4); // Want to compare references !

    }

    @Test
    public void testGetDestinationContext() throws IOException {
        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

        testDatas.createTestDomains();

        List<RefEspece> wineRefSpecies = refEspecesDao.forLibelle_espece_botaniqueEquals("Vigne").findAll();

        RefEspece bleTendre = refEspecesDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        RefEspece refErable = refEspecesDao.forNaturalId("I19", "", "", "").findUnique();
        RefEspece refSapin = refEspecesDao.forNaturalId("I44", "", "", "").findUnique();

        CroppingPlanSpecies sp0 = croppingPlanSpeciesDao.forSpeciesEquals(bleTendre).findAny();
        CroppingPlanSpecies sp1 = croppingPlanSpeciesDao.forSpeciesEquals(refErable).findAny();
        CroppingPlanSpecies sp2 = croppingPlanSpeciesDao.forSpeciesEquals(refSapin).findAny();
        CroppingPlanSpecies sp3 = croppingPlanSpeciesDao.forSpeciesIn(wineRefSpecies).findAny();

        refSpeciesToSectorDao.createByNaturalId(sp0.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp0.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp1.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp1.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp2.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp2.getSpecies().getCode_qualifiant_AEE()), Sector.POLYCULTURE_ELEVAGE);
        refSpeciesToSectorDao.createByNaturalId(sp3.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp3.getSpecies().getCode_qualifiant_AEE()), Sector.VITICULTURE);

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        testDatas.createRefDestination(null, "valide 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "valide 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(sp1.getSpecies(), "ignore 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "ignore 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(testDatas.createRandomizedRefEspece("ignore_3"), "ignore 3", Sector.GRANDES_CULTURES, YealdUnit.TONNE_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "ignore 4", Sector.POLYCULTURE_ELEVAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "ignore 5", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(null, "ignore 6", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 7", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.WINE, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 8", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.BRANDY, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 9", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.GRAPE, null);

        RefQualityCriteriaTopiaDao refQualityCriteriasDao = getPersistenceContext().getRefQualityCriteriaDao();
        RefQualityCriteria qc0 = testDatas.createRefQualityCriteria(null, "Valide 0", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        RefQualityCriteria qc1 = testDatas.createRefQualityCriteria(sp0.getSpecies(), "Valide 1", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        RefQualityCriteria qc2 = testDatas.createRefQualityCriteria(sp2.getSpecies(), "ignore 1", Sector.POLYCULTURE_ELEVAGE, WineValorisation.BRANDY);
        RefQualityCriteria qc3 = testDatas.createRefQualityCriteria(sp3.getSpecies(), "ignore 2", Sector.VITICULTURE, WineValorisation.BRANDY);
        RefQualityCriteria qc4 = testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore 3", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        RefQualityCriteria qc5 = testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore 4", Sector.MARAICHAGE, WineValorisation.BRANDY);

        createRefQualityCriteriaClass(qc0);
        createRefQualityCriteriaClass(qc0);
        createRefQualityCriteriaClass(qc1);
        createRefQualityCriteriaClass(qc2);

        Set<String> speciesCodes = Sets.newHashSet(sp0.getCode());

        DestinationContext destinationContext = referentialService.getDestinationContext(speciesCodes, null, null, null, null);
        Assertions.assertEquals(2, destinationContext.getDestinationsByDestinationLabels().size());
        Assertions.assertEquals(2, destinationContext.getQualityCriteria().size());
        Map<String, List<RefQualityCriteriaClass>> qualityCriteriaClasses = destinationContext.getQualityCriteriaClasses();
        Assertions.assertEquals(2, qualityCriteriaClasses.size());
        Assertions.assertEquals(2, qualityCriteriaClasses.get(qc0.getCode()).size());
        Assertions.assertNotNull(qualityCriteriaClasses.get(qc1.getCode()));
        Assertions.assertNull(qualityCriteriaClasses.get(qc3.getCode()));
        Assertions.assertNull(qualityCriteriaClasses.get(qc4.getCode()));
        Assertions.assertNull(qualityCriteriaClasses.get(qc5.getCode()));
    }

    private RefQualityCriteriaClass createRefQualityCriteriaClass(RefQualityCriteria qc0) {
        RefQualityCriteriaClassTopiaDao dao = getPersistenceContext().getRefQualityCriteriaClassDao();
        RefQualityCriteriaClass result = dao.create(
                RefQualityCriteriaClass.PROPERTY_CODE, UUID.randomUUID().toString(),
                RefQualityCriteriaClass.PROPERTY_REF_QUALITY_CRITERIA_CODE, qc0.getCode(),
                RefQualityCriteriaClass.PROPERTY_CLASSE, "CL0",
                RefQualityCriteriaClass.PROPERTY_ACTIVE, true
        );
        return result;
    }

    @Test
    public void testGetDestinationContextWithMoreThanOneSpecies() throws IOException {
        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

        testDatas.createTestDomains();

        List<RefEspece> wineRefSpecies = refEspecesDao.forLibelle_espece_botaniqueEquals("Vigne").findAll();

        RefEspece bleTendre = refEspecesDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        RefEspece refErable = refEspecesDao.forNaturalId("I19", "", "", "").findUnique();
        RefEspece refSapin = refEspecesDao.forNaturalId("I44", "", "", "").findUnique();

        CroppingPlanSpecies sp0 = croppingPlanSpeciesDao.forSpeciesEquals(bleTendre).findAny();
        CroppingPlanSpecies sp1 = croppingPlanSpeciesDao.forSpeciesEquals(refErable).findAny();
        CroppingPlanSpecies sp2 = croppingPlanSpeciesDao.forSpeciesEquals(refSapin).findAny();
        CroppingPlanSpecies sp3 = croppingPlanSpeciesDao.forSpeciesIn(wineRefSpecies).findAny();

        refSpeciesToSectorDao.createByNaturalId(sp0.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp0.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp1.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp1.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp2.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp2.getSpecies().getCode_qualifiant_AEE()), Sector.POLYCULTURE_ELEVAGE);
        refSpeciesToSectorDao.createByNaturalId(sp3.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp3.getSpecies().getCode_qualifiant_AEE()), Sector.VITICULTURE);

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        testDatas.createRefDestination(null, "valide 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "valide 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(sp1.getSpecies(), "ignore 1 - not research species", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "ignore 2 - sector not good", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(testDatas.createRandomizedRefEspece("ignore_0"), "ignore 3 - not research species", Sector.GRANDES_CULTURES, YealdUnit.TONNE_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "valide 3", Sector.POLYCULTURE_ELEVAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "ignore 4 - sector not good", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(null, "ignore 5  - sector not good", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 6 - not research species", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.WINE, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 7 - not research species", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.BRANDY, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 8 - not research species", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.GRAPE, null);

        RefQualityCriteriaTopiaDao refQualityCriteriasDao = getPersistenceContext().getRefQualityCriteriaDao();
        testDatas.createRefQualityCriteria(null, "Valide 0", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp0.getSpecies(), "Valide 1", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp2.getSpecies(), "Valide 2", Sector.POLYCULTURE_ELEVAGE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp3.getSpecies(), "ignore 1", Sector.VITICULTURE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore 2", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore 3", Sector.MARAICHAGE, WineValorisation.BRANDY);

        Set<String> speciesCodes = Sets.newHashSet(sp0.getCode(), sp2.getCode());

        DestinationContext destinationContext = referentialService.getDestinationContext(speciesCodes, null, null, null, null);
        Assertions.assertEquals(3, destinationContext.getDestinationsByDestinationLabels().size());
        Assertions.assertEquals(3, destinationContext.getQualityCriteria().size());
    }

    @Test
    public void testGetDestinationContext_INRA_TEST() throws IOException {
        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

//        Filière       Code_esp    Code_qualifiant     Intitulé_espèces                        Critère
//        Maraichage                                    Toutes                                  C1
//        Maraichage                                    Toutes	                                C11
//        Maraichage    A                               choux (tous les qualifiants de choux)   C2
//        Maraichage    A                               choux (tous les qualifiants de choux)   C3
//        Maraichage    A                               choux (tous les qualifiants de choux)   C4
//        Maraichage    A           X                   Chou rutabaga                           C5
//        Maraichage    A           Y                   Chou fleur                              C6
//        Maraichage    B                               carotte                                 C7
//        …             …            …                  …                                       …


        RefEspece chouFleur = testDatas.createRefEspece(refEspecesDao, "A", "Y", "chouFleur", null);
        RefEspece chouNavetRutabaga = testDatas.createRefEspece(refEspecesDao, "A", "X", "chouNavetRutabaga", null);
        RefEspece chou = testDatas.createRefEspece(refEspecesDao, "A", "", "chou", null);
        RefEspece carotte = testDatas.createRefEspece(refEspecesDao, "B", "", "carotte", null);
        RefEspece chouChinois = testDatas.createRefEspece(refEspecesDao, "Z", "", "chouChinois", null);

        refSpeciesToSectorDao.createByNaturalId(chouFleur.getCode_espece_botanique(), chouFleur.getCode_qualifiant_AEE(), Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId(chouNavetRutabaga.getCode_espece_botanique(), chouNavetRutabaga.getCode_qualifiant_AEE(), Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId(chou.getCode_espece_botanique(), null, Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId(carotte.getCode_espece_botanique(), null, Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId(chouChinois.getCode_espece_botanique(), chouChinois.getCode_qualifiant_AEE(), Sector.GRANDES_CULTURES);

        String d1Label = "D1 - Toutes";
        String d11Label = "D11 - Toutes";
        String d2Label = "D2 - choux (tous les qualifiants de choux)";
        String d3Label = "D3 - choux (tous les qualifiants de choux)";
        String d4Label = "D4 - choux (tous les qualifiants de choux)";
        String d5Label = "D5 - Chou rutabaga";
        String d6Label = "D6 - Chou fleur";
        String d7Label = "D7 - carotte";

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        testDatas.createRefDestination(null, d1Label, Sector.MARAICHAGE, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(null, d11Label, Sector.MARAICHAGE, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(chou, d2Label, Sector.MARAICHAGE, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(chou, d3Label, Sector.MARAICHAGE, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(chou, d4Label, Sector.MARAICHAGE, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(chouNavetRutabaga, d5Label, Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(chouFleur, d6Label, Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(carotte, d7Label, Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(testDatas.createRandomizedRefEspece("ignore_0"), "not research species", Sector.GRANDES_CULTURES, YealdUnit.TONNE_HA, null, null);

        RefQualityCriteriaTopiaDao refQualityCriteriasDao = getPersistenceContext().getRefQualityCriteriaDao();
        String c1Label = "C1 - Toutes";
        testDatas.createRefQualityCriteria(null, c1Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        String c11Label = "C11 - Toutes";
        testDatas.createRefQualityCriteria(null, c11Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        String c2Label = "C2 - choux (tous les qualifiants de choux)";
        testDatas.createRefQualityCriteria(chou, c2Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        String c3Label = "C3 - choux (tous les qualifiants de choux)";
        testDatas.createRefQualityCriteria(chou, c3Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        String c4Label = "C4 - choux (tous les qualifiants de choux)";
        testDatas.createRefQualityCriteria(chou, c4Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        String c5Label = "C5 - Chou rutabaga";
        testDatas.createRefQualityCriteria(chouNavetRutabaga, c5Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        String c6Label = "C6 - Chou fleur";
        testDatas.createRefQualityCriteria(chouFleur, c6Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        String c7Label = "C7 - carotte";
        testDatas.createRefQualityCriteria(carotte, c7Label, Sector.MARAICHAGE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore_0"), "not research species", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);

        RefLocation loc0 = testDatas.createRefLocation(getPersistenceContext().getRefLocationDao());
        RefLegalStatus exploitantAgricoleStatus = testDatas.createRefLegalStatus(getPersistenceContext().getRefLegalStatusDao());
        Domain d0 = testDatas.createSimpleDomain(getPersistenceContext().getDomainDao(), "fr.inra.agrosyst.api.entities.Domain_7fd194f6-7440-432f-a37b-fbf1d27fa95e", loc0, exploitantAgricoleStatus, "Couëron les bains", 2015, "Annie Verssaire", "Lorem ipsum dolor sit amet", 100.0, 9.0, 1.0, true);

        CroppingPlanEntryTopiaDao cpeDao = getPersistenceContext().getCroppingPlanEntryDao();
        CroppingPlanEntry cpe = testDatas.createCroppingPlanEntry(d0, cpeDao, "CHOUX");

        CroppingPlanSpecies chouFleurSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chouFleur, cpe);
        CroppingPlanSpecies chouChinoisSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chouChinois, cpe);
        CroppingPlanSpecies chouNavetRutabagaSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chouNavetRutabaga, cpe);
        CroppingPlanSpecies chouSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chou, cpe);
        CroppingPlanSpecies carotteSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, carotte, cpe);

        getPersistenceContext().commit();

//        > On déclare une intervention de récolte sur un chou-fleur (code esp A, code qualif Y)
//        Voici le fonctionnement du code pour remonter la liste des critères adaptée à la demande
//        1. En se basant sur le code esp A * code qualif Y, on recherche la filière à laquelle appartient le chou fleur
//        Réponse: maraichage
//        2. On remonte déjà tous les critères qui concernent toutes les espèces de cette filière
//        Réponse : C1 et C11
//        3. On remonte tous les critères qui concernent le couple code esp * code qualifiant, soit A*Y
//        Réponse C6
//        4. On remonte en plus de celui-ci tous les critères qui portent sur le code esp A qui n'ont pas de qualifiant spécifié
//        Réponse : C2, C3, C4

        // EXECUTE
        Set<String> speciesCodes = Sets.newHashSet(chouFleurSpecies.getCode());
        DestinationContext destinationContext = referentialService.getDestinationContext(speciesCodes, null, null, null, null);

        Assertions.assertNotNull(destinationContext);
        Map<String, List<RefDestination>> destinationByLabels = destinationContext.getDestinationsByDestinationLabels();

        // valid destination
        Assertions.assertEquals(6, destinationByLabels.size());
        List<String> expectedDestination = Lists.newArrayList(d1Label, d11Label, d6Label, d2Label, d3Label, d4Label);
        List<String> unexpectedDestination = Lists.newArrayList(d5Label, d7Label);
        Assertions.assertTrue(destinationByLabels.keySet().containsAll(expectedDestination));
        int validSize = destinationByLabels.keySet().size();
        unexpectedDestination.forEach(destinationByLabels.keySet()::remove);
        Assertions.assertEquals(validSize, destinationByLabels.keySet().size());

        // valid QualityCriteria
        Map<String, RefQualityCriteria> qualityCriteriaByIds = destinationContext.getQualityCriteria();
        Assertions.assertNotNull(qualityCriteriaByIds);
        List<String> expectedQualityCriteria = Lists.newArrayList(c1Label, c11Label, c6Label, c2Label, c3Label, c4Label);
        List<String> unexpectedQualityCriteria = Lists.newArrayList(c5Label, c7Label);
        Collection<RefQualityCriteria> qualityCriterias = qualityCriteriaByIds.values();
        Map<String, RefQualityCriteria> qualityCriteriaByLabels = Maps.newHashMap(Maps.uniqueIndex(qualityCriterias, input -> {
            assert input != null;
            return input.getQualityCriteriaLabel();
        }));
        Set<String> qualityCriteriaLabels = qualityCriteriaByLabels.keySet();
        Assertions.assertTrue(qualityCriteriaLabels.containsAll(expectedQualityCriteria));

        validSize = qualityCriteriaLabels.size();
        unexpectedQualityCriteria.forEach(qualityCriteriaLabels::remove);
        Assertions.assertEquals(validSize, qualityCriteriaLabels.size());

        // IN THIS TEST CAROTTE IS INCLUDED
        speciesCodes = Sets.newHashSet(chouFleurSpecies.getCode(), carotteSpecies.getCode());
        destinationContext = referentialService.getDestinationContext(speciesCodes, null, null, null, null);

        Assertions.assertNotNull(destinationContext);
        destinationByLabels = destinationContext.getDestinationsByDestinationLabels();

        // valid destination
        Assertions.assertEquals(7, destinationByLabels.size());
        expectedDestination = Lists.newArrayList(d1Label, d11Label, d6Label, d2Label, d3Label, d4Label, d7Label);
        unexpectedDestination = Lists.newArrayList(d5Label);
        Assertions.assertTrue(destinationByLabels.keySet().containsAll(expectedDestination));
        validSize = destinationByLabels.keySet().size();
        unexpectedDestination.forEach(destinationByLabels.keySet()::remove);
        Assertions.assertEquals(validSize, destinationByLabels.keySet().size());

        // valid QualityCriteria
        qualityCriteriaByIds = destinationContext.getQualityCriteria();
        Assertions.assertNotNull(qualityCriteriaByIds);
        expectedQualityCriteria = Lists.newArrayList(c1Label, c11Label, c6Label, c2Label, c3Label, c4Label, c7Label);
        unexpectedQualityCriteria = Lists.newArrayList(c5Label);
        qualityCriterias = qualityCriteriaByIds.values();
        qualityCriteriaByLabels = Maps.newHashMap(Maps.uniqueIndex(qualityCriterias, input -> {
            assert input != null;
            return input.getQualityCriteriaLabel();
        }));
        qualityCriteriaLabels = qualityCriteriaByLabels.keySet();
        Assertions.assertTrue(qualityCriteriaLabels.containsAll(expectedQualityCriteria));

        validSize = qualityCriteriaLabels.size();
        unexpectedQualityCriteria.forEach(qualityCriteriaLabels::remove);
        Assertions.assertEquals(validSize, qualityCriteriaLabels.size());
    }

    @Test
    public void testGetDestinationContextForWineSpecies_INRA_TEST() throws IOException {
        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

//        Filière       Code_esp  Code_qualifiant  Intitulé_espèces  Valorisation_viti_culture  Critère
//        VITICULTURE    ZMO                                vigne                               C1
//        VITICULTURE    ZMO                                vigne                               C11
//        VITICULTURE    ZMO                                vigne                               C2
//        VITICULTURE    ZMO                                vigne    Vin                        C3
//        VITICULTURE    ZMO                                vigne    Vin                        C4
//        VITICULTURE    ZMO                                vigne    Raisin de table            C5
//        VITICULTURE    ZMO                                vigne    Raisin de table            C6
//        VITICULTURE    ZMO                                vigne    Eau-de-vie                 C7
//        …             …            …                  …                                       …


        RefEspece vigne = testDatas.createRefEspece(refEspecesDao, "ZMO", "", "vigne", null);

        refSpeciesToSectorDao.createByNaturalId(vigne.getCode_espece_botanique(), null, Sector.VITICULTURE);

        String d1Label = "D1";
        String d11Label = "D11";
        String d2Label = "D2";
        String d3Label = "D3";
        String d4Label = "D4";
        String d5Label = "D5";
        String d6Label = "D6";
        String d7Label = "D7";

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        testDatas.createRefDestination(vigne, d1Label, Sector.VITICULTURE, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(vigne, d11Label, Sector.VITICULTURE, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(vigne, d2Label, Sector.VITICULTURE, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(vigne, d3Label, Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.WINE, null);
        testDatas.createRefDestination(vigne, d4Label, Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.WINE, null);
        testDatas.createRefDestination(vigne, d5Label, Sector.VITICULTURE, YealdUnit.UNITE_M2, WineValorisation.GRAPE, null);
        testDatas.createRefDestination(vigne, d6Label, Sector.VITICULTURE, YealdUnit.UNITE_M2, WineValorisation.GRAPE, null);
        testDatas.createRefDestination(vigne, d7Label, Sector.VITICULTURE, YealdUnit.UNITE_M2, WineValorisation.BRANDY, null);
        testDatas.createRefDestination(testDatas.createRandomizedRefEspece("ignore_0"), "not research species", Sector.GRANDES_CULTURES, YealdUnit.TONNE_HA, null, null);

        RefQualityCriteriaTopiaDao refQualityCriteriasDao = getPersistenceContext().getRefQualityCriteriaDao();
        String c1Label = "C1";
        testDatas.createRefQualityCriteria(vigne, c1Label, Sector.VITICULTURE, null);
        String c11Label = "C11";
        testDatas.createRefQualityCriteria(vigne, c11Label, Sector.VITICULTURE, null);
        String c2Label = "C2";
        testDatas.createRefQualityCriteria(vigne, c2Label, Sector.VITICULTURE, null);
        String c3Label = "C3";
        testDatas.createRefQualityCriteria(vigne, c3Label, Sector.VITICULTURE, WineValorisation.WINE);
        String c4Label = "C4";
        testDatas.createRefQualityCriteria(vigne, c4Label, Sector.VITICULTURE, WineValorisation.WINE);
        String c5Label = "C5";
        testDatas.createRefQualityCriteria(vigne, c5Label, Sector.VITICULTURE, WineValorisation.GRAPE);
        String c6Label = "C6";
        testDatas.createRefQualityCriteria(vigne, c6Label, Sector.VITICULTURE, WineValorisation.GRAPE);
        String c7Label = "C7";
        testDatas.createRefQualityCriteria(vigne, c7Label, Sector.VITICULTURE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore_0"), "not research species", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);

        RefLocation loc0 = testDatas.createRefLocation(getPersistenceContext().getRefLocationDao());
        RefLegalStatus exploitantAgricoleStatus = testDatas.createRefLegalStatus(getPersistenceContext().getRefLegalStatusDao());
        Domain d0 = testDatas.createSimpleDomain(getPersistenceContext().getDomainDao(), "fr.inra.agrosyst.api.entities.Domain_7fd194f6-7440-432f-a37b-fbf1d27fa95e", loc0, exploitantAgricoleStatus, "Couëron les bains", 2015, "Annie Verssaire", "Lorem ipsum dolor sit amet", 100.0, 9.0, 1.0, true);

        CroppingPlanEntryTopiaDao cpeDao = getPersistenceContext().getCroppingPlanEntryDao();
        CroppingPlanEntry cpe = cpeDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "CPE0",
                CroppingPlanEntry.PROPERTY_NAME, "VIGNE",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies chouFleurSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, vigne, cpe);

        getPersistenceContext().commit();

//        > On déclare une intervention de récolte sur Vigne (code esp ZMO)
//        > On coche la valorisation Vin et Eau-de-vie
//        Voici le fonctionnement pour remonter la liste de critères de qualité adaptée
//        1. On détecte le code ZMO : on sait d'ores et déjà que la filière est viticulture
//        2. On détecte la (les) valorisation(s) cochées par l'utilisateur
//        Réponse : Vin + Eau de vie
//        3. On remonte déjà tous les critères qui concernant ces valorisations
//        Réponse : C3, C4, C7
//        4. On remonte également tous les critères qui concernent toutes les valorisations, c’est-à-dire sans valorisation de précisée dans la colonne correspondante
//        Réponse : C1, C11, C2

        // EXECUTE
        Set<String> speciesCodes = Sets.newHashSet(chouFleurSpecies.getCode());
        Set<WineValorisation> wineValorisations = Sets.newHashSet(WineValorisation.WINE, WineValorisation.BRANDY);
        DestinationContext destinationContext = referentialService.getDestinationContext(speciesCodes, wineValorisations, null, null, null);

        Assertions.assertNotNull(destinationContext);
        Map<String, List<RefDestination>> destinationByLabels = destinationContext.getDestinationsByDestinationLabels();

        // valid destination
        Assertions.assertEquals(6, destinationByLabels.size());
        List<String> expectedDestination = Lists.newArrayList(d3Label, d4Label, d7Label, d1Label, d11Label, d2Label);
        List<String> unexpectedDestination = Lists.newArrayList(d5Label, d6Label);
        Assertions.assertTrue(destinationByLabels.keySet().containsAll(expectedDestination));
        int validSize = destinationByLabels.keySet().size();
        unexpectedDestination.forEach(destinationByLabels.keySet()::remove);
        Assertions.assertEquals(validSize, destinationByLabels.keySet().size());

        // valid QualityCriteria
        Map<String, RefQualityCriteria> qualityCriteriaByIds = destinationContext.getQualityCriteria();
        Assertions.assertNotNull(qualityCriteriaByIds);
        List<String> expectedQualityCriteria = Lists.newArrayList(c3Label, c4Label, c7Label, c1Label, c11Label, c2Label);
        List<String> unexpectedQualityCriteria = Lists.newArrayList(c5Label, c6Label);
        Collection<RefQualityCriteria> qualityCriterias = qualityCriteriaByIds.values();
        Map<String, RefQualityCriteria> qualityCriteriaByLabels = Maps.newHashMap(Maps.uniqueIndex(qualityCriterias, input -> {
            assert input != null;
            return input.getQualityCriteriaLabel();
        }));
        Set<String> qualityCriteriaLabels = qualityCriteriaByLabels.keySet();
        Assertions.assertTrue(qualityCriteriaLabels.containsAll(expectedQualityCriteria));

        validSize = qualityCriteriaLabels.size();
        unexpectedQualityCriteria.forEach(qualityCriteriaLabels::remove);
        Assertions.assertEquals(validSize, qualityCriteriaLabels.size());

    }

    @Test
    public void testGetDestinationContextWithWineSpeciesWithoutWineValorisation() throws IOException {
        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

        testDatas.createTestDomains();

        List<RefEspece> wineRefSpecies = refEspecesDao.forLibelle_espece_botaniqueEquals("Vigne").findAll();

        RefEspece bleTendre = refEspecesDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        RefEspece refErable = refEspecesDao.forNaturalId("I19", "", "", "").findUnique();
        RefEspece refSapin = refEspecesDao.forNaturalId("I44", "", "", "").findUnique();

        CroppingPlanSpecies sp0 = croppingPlanSpeciesDao.forSpeciesEquals(bleTendre).findAny();
        CroppingPlanSpecies sp1 = croppingPlanSpeciesDao.forSpeciesEquals(refErable).findAny();
        CroppingPlanSpecies sp2 = croppingPlanSpeciesDao.forSpeciesEquals(refSapin).findAny();
        CroppingPlanSpecies sp3 = croppingPlanSpeciesDao.forSpeciesIn(wineRefSpecies).findAny();

        refSpeciesToSectorDao.createByNaturalId(sp0.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp0.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp1.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp1.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp2.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp2.getSpecies().getCode_qualifiant_AEE()), Sector.POLYCULTURE_ELEVAGE);
        refSpeciesToSectorDao.createByNaturalId(sp3.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp3.getSpecies().getCode_qualifiant_AEE()), Sector.VITICULTURE);

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        testDatas.createRefDestination(null, "valide 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "valide 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(sp1.getSpecies(), "ignore 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "ignore 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(testDatas.createRandomizedRefEspece("ignore_0"), "ignore 3", Sector.GRANDES_CULTURES, YealdUnit.TONNE_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "valide 3", Sector.POLYCULTURE_ELEVAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "ignore 4", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);

        testDatas.createRefDestination(null, "ignore 5", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 6", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.WINE, null);//pas valide
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 7", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.BRANDY, null);//pas valide
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 8", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.GRAPE, null);//pas valide

        RefQualityCriteriaTopiaDao refQualityCriteriasDao = getPersistenceContext().getRefQualityCriteriaDao();
        testDatas.createRefQualityCriteria(null, "Valide 0", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp0.getSpecies(), "Valide 1", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp2.getSpecies(), "Valide 2", Sector.POLYCULTURE_ELEVAGE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp3.getSpecies(), "Ignore", Sector.VITICULTURE, WineValorisation.BRANDY);// TODO: 22/02/17 valider le fait que si l'on à pas de valorisation vigne celle ci n'est pas valide
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore", Sector.MARAICHAGE, WineValorisation.BRANDY);

        Set<String> speciesCodes = Sets.newHashSet(sp0.getCode(), sp2.getCode(), sp3.getCode());

        DestinationContext destinationContext = referentialService.getDestinationContext(speciesCodes, null, null, null, null);
        Assertions.assertEquals(3, destinationContext.getDestinationsByDestinationLabels().size());
        Assertions.assertEquals(3, destinationContext.getQualityCriteria().size());
    }

    @Test
    public void testGetDestinationContextWithWineValorisation() throws IOException {

        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

        testDatas.createTestDomains();

        List<RefEspece> wineRefSpecies = refEspecesDao.forLibelle_espece_botaniqueEquals("Vigne").findAll();

        RefEspece bleTendre = refEspecesDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        RefEspece refErable = refEspecesDao.forNaturalId("I19", "", "", "").findUnique();
        RefEspece refSapin = refEspecesDao.forNaturalId("I44", "", "", "").findUnique();

        CroppingPlanSpecies sp0 = croppingPlanSpeciesDao.forSpeciesEquals(bleTendre).findAny();
        CroppingPlanSpecies sp1 = croppingPlanSpeciesDao.forSpeciesEquals(refErable).findAny();
        CroppingPlanSpecies sp2 = croppingPlanSpeciesDao.forSpeciesEquals(refSapin).findAny();
        CroppingPlanSpecies sp3 = croppingPlanSpeciesDao.forSpeciesIn(wineRefSpecies).findAny();

        refSpeciesToSectorDao.createByNaturalId(sp0.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp0.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp1.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp1.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp2.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp2.getSpecies().getCode_qualifiant_AEE()), Sector.POLYCULTURE_ELEVAGE);
        refSpeciesToSectorDao.createByNaturalId(sp3.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp3.getSpecies().getCode_qualifiant_AEE()), Sector.VITICULTURE);

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        testDatas.createRefDestination(null, "valide 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "valide 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(sp1.getSpecies(), "ignore 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "ignore 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(testDatas.createRandomizedRefEspece("ignore_0"), "ignore 3", Sector.GRANDES_CULTURES, YealdUnit.TONNE_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "valide 3", Sector.POLYCULTURE_ELEVAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "ignore 4", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(null, "ignore 5", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 6", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.WINE, null);
        testDatas.createRefDestination(sp3.getSpecies(), "valide 4", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.BRANDY, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 7", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.GRAPE, null);

        RefQualityCriteriaTopiaDao refQualityCriteriasDao = getPersistenceContext().getRefQualityCriteriaDao();
        testDatas.createRefQualityCriteria(null, "Valide 0", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp0.getSpecies(), "Valide 1", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp2.getSpecies(), "Valide 2", Sector.POLYCULTURE_ELEVAGE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp3.getSpecies(), "Valide 3", Sector.VITICULTURE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore", Sector.MARAICHAGE, WineValorisation.BRANDY);

        Set<String> speciesCodes = Sets.newHashSet(sp0.getCode(), sp2.getCode(), sp3.getCode());

        DestinationContext destinationContext = referentialService.getDestinationContext(speciesCodes, Sets.newHashSet(WineValorisation.BRANDY), null, null, null);
        Assertions.assertEquals(4, destinationContext.getDestinationsByDestinationLabels().size());
        Assertions.assertEquals(4, destinationContext.getQualityCriteria().size());
    }

    @Test
    public void testGetDestinationContextWith2WineValorisation() throws IOException {

        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

        testDatas.createTestDomains();

        List<RefEspece> wineRefSpecies = refEspecesDao.forLibelle_espece_botaniqueEquals("Vigne").findAll();

        RefEspece bleTendre = refEspecesDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        RefEspece refErable = refEspecesDao.forNaturalId("I19", "", "", "").findUnique();
        RefEspece refSapin = refEspecesDao.forNaturalId("I44", "", "", "").findUnique();

        CroppingPlanSpecies sp0 = croppingPlanSpeciesDao.forSpeciesEquals(bleTendre).findAny();
        CroppingPlanSpecies sp1 = croppingPlanSpeciesDao.forSpeciesEquals(refErable).findAny();
        CroppingPlanSpecies sp2 = croppingPlanSpeciesDao.forSpeciesEquals(refSapin).findAny();
        CroppingPlanSpecies sp3 = croppingPlanSpeciesDao.forSpeciesIn(wineRefSpecies).findAny();

        refSpeciesToSectorDao.createByNaturalId(sp0.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp0.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp1.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp1.getSpecies().getCode_qualifiant_AEE()), Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(sp2.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp2.getSpecies().getCode_qualifiant_AEE()), Sector.POLYCULTURE_ELEVAGE);
        refSpeciesToSectorDao.createByNaturalId(sp3.getSpecies().getCode_espece_botanique(), Strings.emptyToNull(sp3.getSpecies().getCode_qualifiant_AEE()), Sector.VITICULTURE);

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        testDatas.createRefDestination(null, "valide 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "valide 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(sp1.getSpecies(), "ignore 1", Sector.GRANDES_CULTURES, YealdUnit.HL_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "ignore 2", Sector.GRANDES_CULTURES, YealdUnit.KG_M2, null, null);
        testDatas.createRefDestination(testDatas.createRandomizedRefEspece("ignore_0"), "ignore 3", Sector.GRANDES_CULTURES, YealdUnit.TONNE_HA, null, null);
        testDatas.createRefDestination(sp2.getSpecies(), "valide 4", Sector.POLYCULTURE_ELEVAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp0.getSpecies(), "ignore 4", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(null, "ignore 5", Sector.MARAICHAGE, YealdUnit.UNITE_M2, null, null);
        testDatas.createRefDestination(sp3.getSpecies(), "valide 5", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.WINE, null);
        testDatas.createRefDestination(sp3.getSpecies(), "valide 6", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.BRANDY, null);
        testDatas.createRefDestination(sp3.getSpecies(), "ignore 6", Sector.VITICULTURE, YealdUnit.KG_M2, WineValorisation.GRAPE, null);

        RefQualityCriteriaTopiaDao refQualityCriteriasDao = getPersistenceContext().getRefQualityCriteriaDao();
        testDatas.createRefQualityCriteria(null, "Valide 0", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp0.getSpecies(), "Valide 1", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp2.getSpecies(), "Valide 2", Sector.POLYCULTURE_ELEVAGE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(sp3.getSpecies(), "Valide 3", Sector.VITICULTURE, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore", Sector.GRANDES_CULTURES, WineValorisation.BRANDY);
        testDatas.createRefQualityCriteria(testDatas.createRandomizedRefEspece("ignore"), "Ignore", Sector.MARAICHAGE, WineValorisation.BRANDY);

        Set<String> speciesCodes = Sets.newHashSet(sp0.getCode(), sp2.getCode(), sp3.getCode());

        DestinationContext destinationContext = referentialService.getDestinationContext(speciesCodes, Sets.newHashSet(WineValorisation.WINE, WineValorisation.BRANDY), null, null, null);
        Assertions.assertEquals(5, destinationContext.getDestinationsByDestinationLabels().size());
        Assertions.assertEquals(4, destinationContext.getQualityCriteria().size());
    }

    @Test
    public void testLoadCodeEspeceBotaniqueCodeQualifantForDomain() throws IOException {
        CroppingPlanEntryTopiaDao cpeDao = getPersistenceContext().getCroppingPlanEntryDao();
        CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();

        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();

        RefEspece chouFleur = testDatas.createRefEspece(refEspecesDao, "A", "Y", "chouFleur", null);
        RefEspece chouNavetRutabaga = testDatas.createRefEspece(refEspecesDao, "A", "X", "chouNavetRutabaga", null);
        RefEspece chou = testDatas.createRefEspece(refEspecesDao, "A", "", "chou", null);
        RefEspece carotte = testDatas.createRefEspece(refEspecesDao, "B", "", "carotte", null);
        RefEspece chouChinois = testDatas.createRefEspece(refEspecesDao, "Z", "", "chouChinois", null);
        RefEspece autre = testDatas.createRefEspece(refEspecesDao, "AUTRE", "", "AUTRE", null);

        RefLocation loc0 = testDatas.createRefLocation(getPersistenceContext().getRefLocationDao());
        RefLegalStatus exploitantAgricoleStatus = testDatas.createRefLegalStatus(getPersistenceContext().getRefLegalStatusDao());
        Domain d0 = testDatas.createSimpleDomain(getPersistenceContext().getDomainDao(), "fr.inra.agrosyst.api.entities.Domain_7fd194f6-7440-432f-a37b-fbf1d27fa95e", loc0, exploitantAgricoleStatus, "Couëron les bains", 2015, "Annie Verssaire", "Lorem ipsum dolor sit amet", 100.0, 9.0, 1.0, true);

        CroppingPlanEntry cpe = testDatas.createCroppingPlanEntry(d0, cpeDao, "CHOUX");

        CroppingPlanSpecies chouFleurSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chouFleur, cpe);
        CroppingPlanSpecies chouChinoisSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chouChinois, cpe);
        CroppingPlanSpecies chouNavetRutabagaSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chouNavetRutabaga, cpe);

        CroppingPlanEntry cpe1 = testDatas.createCroppingPlanEntry(d0, cpeDao, "CHOUX1");

        CroppingPlanSpecies chouSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, chou, cpe1);
        CroppingPlanSpecies carotteSpecies = testDatas.createCroppingPlanSpecies(croppingPlanSpeciesDao, carotte, cpe1);

        getPersistenceContext().commit();
        List<CroppingPlanSpecies> D0DomainSpecies = Lists.newArrayList(
                chouFleurSpecies, chouChinoisSpecies, chouNavetRutabagaSpecies, chouSpecies, carotteSpecies);

        Map<String, List<Pair<String, String>>> res = referentialService.getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Sets.newHashSet(d0.getTopiaId()));
        Assertions.assertTrue(res.size() > 0);
        for (CroppingPlanSpecies d0DomainSpecy : D0DomainSpecies) {
            String speciesCode = d0DomainSpecy.getCode();
            String codeEspeceBotanique = d0DomainSpecy.getSpecies().getCode_espece_botanique();
            String codeQualifiantAEE = d0DomainSpecy.getSpecies().getCode_qualifiant_AEE();
            List<Pair<String, String>> codeEspeceBotaniqueCodeQualifiantAEE = res.get(speciesCode);
            Pair<String, String> exepected = Pair.of(codeEspeceBotanique, codeQualifiantAEE);
            Assertions.assertTrue(codeEspeceBotaniqueCodeQualifiantAEE.contains(exepected));
        }
    }

    protected HarvestinActionTestData createTestData() throws IOException {
        AgrosystTopiaPersistenceContext persistenceContext = getPersistenceContext();
        HarvestinActionTestData result = testDatas.createHarvestingActionTestData();
        persistenceContext.commit();
        return result;
    }

    @Test
    public void testLoadHarvestingPriceOnManyPeriods() throws IOException {

        HarvestinActionTestData testsData = createTestData();

        Pair<String, String> echaloteCEBQ = Pair.of("ZBK", "H29");
        RefDestination destination = testsData.getDestinations().get(echaloteCEBQ);

        CroppingPlanEntry cpe = testsData.getCrops().get(echaloteCEBQ).iterator().next();
        CroppingPlanSpecies species = testsData.getSpecies().get(echaloteCEBQ);

        int beginMarketingPeriod = 0;
        int beginMarketingPeriodDecade = 2;
        int beginMarketingPeriodCampaign = 2013;
        int endingMarketingPeriod = 2;
        int endingMarketingPeriodDecade = 2;
        int endingMarketingPeriodCampaign = 2013;
        double cropYealdAverage = 120;

        Collection<HarvestingActionValorisation> valorisations = testDatas.createHarvestingActionValorisationsForCrop(
                harvestingActionValorisationDao,
                destination,
                cpe,
                cropYealdAverage,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                beginMarketingPeriodCampaign,
                endingMarketingPeriod,
                endingMarketingPeriodDecade,
                endingMarketingPeriodCampaign,
                testsData.getRefSpeciesToSectors());

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        String practicedSystemCampaigns = "2013";
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForPracticedSystemValorisations(valorisations, practicedSystemCampaigns, codeEspBotCodeQualiBySpeciesCode);

        boolean valorisationFound = false;
        for (HarvestingActionValorisation valorisation : valorisations) {
            if (valorisation.getSpeciesCode().equals(species.getCode())) {
                valorisationFound = true;
                List<RefHarvestingPrice> harvestingPrices = prices.get(valorisation);
                Assertions.assertEquals(7, harvestingPrices.size());
            }
        }
        Assertions.assertTrue(valorisationFound);

    }

    @Test
    public void testLoadHarvestingPriceOnTwoPeriods() throws IOException {
        HarvestinActionTestData testsData = createTestData();

        Pair<String, String> echaloteCEBQ = Pair.of("ZBK", "H29");
        CroppingPlanEntry cpe = testsData.getCrops().get(echaloteCEBQ).iterator().next();
        CroppingPlanSpecies species = testsData.getSpecies().get(echaloteCEBQ);
        RefDestination destination = testsData.getDestinations().get(echaloteCEBQ);

        int beginMarketingPeriod = 0;
        int beginMarketingPeriodDecade = 1;
        int beginMarketingPeriodCampaign = 2013;
        int endingMarketingPeriod = 0;
        int endingMarketingPeriodDecade = 3;
        int endingMarketingPeriodCampaign = 2013;
        double cropYealdAverage = 120;

        Collection<HarvestingActionValorisation> valorisations = testDatas.createHarvestingActionValorisationsForCrop(
                harvestingActionValorisationDao,
                destination,
                cpe,
                cropYealdAverage,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                beginMarketingPeriodCampaign,
                endingMarketingPeriod,
                endingMarketingPeriodDecade,
                endingMarketingPeriodCampaign,
                testsData.getRefSpeciesToSectors());

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        String practicedSystemCampaigns = "2013";
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForPracticedSystemValorisations(valorisations, practicedSystemCampaigns, codeEspBotCodeQualiBySpeciesCode);
        boolean valorisationFound = false;
        for (HarvestingActionValorisation valorisation : valorisations) {
            if (valorisation.getSpeciesCode().equals(species.getCode())) {
                valorisationFound = true;
                List<RefHarvestingPrice> harvestingPrices = prices.get(valorisation);
                Assertions.assertEquals(2, harvestingPrices.size());
            }
        }
        Assertions.assertTrue(valorisationFound);
    }

    @Test
    public void testLoadHarvestingPriceOnManyYears() throws IOException {
        HarvestinActionTestData testsData = createTestData();

        Pair<String, String> echaloteCEBQ = Pair.of("ZBK", "H29");
        CroppingPlanEntry cpe = testsData.getCrops().get(echaloteCEBQ).iterator().next();
        CroppingPlanSpecies species = testsData.getSpecies().get(echaloteCEBQ);
        RefDestination destination = testsData.getDestinations().get(echaloteCEBQ);

        int beginMarketingPeriod = 0;
        int beginMarketingPeriodDecade = 3;
        int beginMarketingPeriodCampaign = 2013;
        int endingMarketingPeriod = 2;
        int endingMarketingPeriodDecade = 2;
        int endingMarketingPeriodCampaign = 2015;
        double cropYealdAverage = 120;

        Collection<HarvestingActionValorisation> valorisations = testDatas.createHarvestingActionValorisationsForCrop(
                harvestingActionValorisationDao,
                destination,
                cpe,
                cropYealdAverage,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                beginMarketingPeriodCampaign,
                endingMarketingPeriod,
                endingMarketingPeriodDecade,
                endingMarketingPeriodCampaign,
                testsData.getRefSpeciesToSectors());

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForValorisations(valorisations, codeEspBotCodeQualiBySpeciesCode);
        boolean valorisationFound = false;
        for (HarvestingActionValorisation valorisation : valorisations) {
            if (valorisation.getSpeciesCode().equals(species.getCode())) {
                valorisationFound = true;
                List<RefHarvestingPrice> harvestingPrices = prices.get(valorisation);
                Assertions.assertEquals(30, harvestingPrices.size());
            }
        }
        Assertions.assertTrue(valorisationFound);
    }

    @Test
    public void testLoadHarvestingPriceOnNextYears() throws IOException {
        HarvestinActionTestData testsData = createTestData();

        Pair<String, String> echaloteCEBQ = Pair.of("ZBK", "H29");
        CroppingPlanEntry cpe = testsData.getCrops().get(echaloteCEBQ).iterator().next();
        CroppingPlanSpecies species = testsData.getSpecies().get(echaloteCEBQ);
        RefDestination destination = testsData.getDestinations().get(echaloteCEBQ);

        int beginMarketingPeriod = 0;
        int beginMarketingPeriodDecade = 3;
        int beginMarketingPeriodCampaign = 2013;
        int endingMarketingPeriod = 2;
        int endingMarketingPeriodDecade = 2;
        int endingMarketingPeriodCampaign = 2014;
        double cropYealdAverage = 120;

        Collection<HarvestingActionValorisation> valorisations = testDatas.createHarvestingActionValorisationsForCrop(
                harvestingActionValorisationDao,
                destination,
                cpe,
                cropYealdAverage,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                beginMarketingPeriodCampaign,
                endingMarketingPeriod,
                endingMarketingPeriodDecade,
                endingMarketingPeriodCampaign,
                testsData.getRefSpeciesToSectors());

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForValorisations(valorisations, codeEspBotCodeQualiBySpeciesCode);
        boolean valorisationFound = false;
        for (HarvestingActionValorisation valorisation : valorisations) {
            if (valorisation.getSpeciesCode().equals(species.getCode())) {
                valorisationFound = true;
                List<RefHarvestingPrice> harvestingPrices = prices.get(valorisation);
                Assertions.assertEquals(13, harvestingPrices.size());
            }
        }
        Assertions.assertTrue(valorisationFound);
    }

    @Test
    public void testLoadHarvestingPriceOnNextYearsForNotMainValorisation() throws IOException {
        HarvestinActionTestData testsData = createTestData();

        Pair<String, String> echaloteCEBQ = Pair.of("ZBK", "H29");
        CroppingPlanEntry cpe = testsData.getCrops().get(echaloteCEBQ).iterator().next();
        CroppingPlanSpecies species = testsData.getSpecies().get(echaloteCEBQ);
        RefDestination destination = testsData.getDestinations().get(echaloteCEBQ);

        Collection<HarvestingActionValorisation> valorisations = new ArrayList<>();
        HarvestingActionValorisation valorisation = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, species.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 0,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2013,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2014,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 50,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA
        );
        valorisations.add(valorisation);

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForValorisations(valorisations, codeEspBotCodeQualiBySpeciesCode);
        List<RefHarvestingPrice> harvestingPrices = prices.get(valorisation);
        Assertions.assertEquals(13, harvestingPrices.size());
    }

    @Test
    public void testLoadPracticedHarvestingPrice() throws IOException {
        HarvestinActionTestData testsData = createTestData();

        Pair<String, String> echaloteCEBQ = Pair.of("ZBK", "H29");
        CroppingPlanEntry cpe = testsData.getCrops().get(echaloteCEBQ).iterator().next();
        CroppingPlanSpecies species = testsData.getSpecies().get(echaloteCEBQ);
        RefDestination destination = testsData.getDestinations().get(echaloteCEBQ);

        Collection<HarvestingActionValorisation> valorisations = new ArrayList<>();
        HarvestingActionValorisation valorisation = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, species.getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 0,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2013,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2014,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 50,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA
        );
        valorisations.add(valorisation);

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        String practicedSystemCampaigns = "2013, 2014";
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForPracticedSystemValorisations(valorisations, practicedSystemCampaigns, codeEspBotCodeQualiBySpeciesCode);
        List<RefHarvestingPrice> harvestingPrices = prices.get(valorisation);
        Assertions.assertEquals(10, harvestingPrices.size());
    }

    @Test
    public void testRefFertiminUnifaPriceObjectId() {

        String objectId = "categ=110 forme=Granule n=33.500000 p2O5=0.000000 k2O=0.000000 bore=0.000000 calcium=0.000000" +
                " fer=0.000000 manganese=0.000000 molybdene=0.000000 mgO=0.000000 oxyde_de_sodium=0.000000 sO3=0.000000 cuivre=0.000000 zinc=0.000000";

        RefFertiMinUNIFA rfmu = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_TOPIA_ID, UUID.randomUUID().toString(),
                RefFertiMinUNIFA.PROPERTY_CATEG, 110,
                RefFertiMinUNIFA.PROPERTY_FORME, "Granule",
                RefFertiMinUNIFA.PROPERTY_ACTIVE, true,
                RefFertiMinUNIFA.PROPERTY_N, 33.5,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMMONITRATE Dosage > 28 % sans SO3"
        );
        RefFertiMinUNIFA refFertiMinUNIFA = refFertiMinUNIFADao.forTopiaIdEquals(rfmu.getTopiaId()).findUnique();
        String result = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refFertiMinUNIFA);

        Assertions.assertEquals(objectId, result);
    }

    @Test
    public void testLoadRefStrategyPrices() throws IOException {
        HarvestinActionTestData testData = createTestData();

        if (refTypeAgricultureTopiaDao.count() == 0) {
            testDatas.importTypeAgriculture();
        }

        HarvestingActionValorisationTopiaDao valorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();
        RefHarvestingPriceTopiaDao scenarioPriceDao = getPersistenceContext().getRefHarvestingPriceDao();

        GrowingPlan gp = testDatas.createGrowingPlan(testData.getDomain());
        GrowingSystem gs = testDatas.createGrowingSystem(gp);
        PracticedSystem ps = testDatas.createPracticedSystem(gs);

        Map<Pair<String, String>, Set<CroppingPlanEntry>> crops = testData.getCrops();
        Set<CroppingPlanEntry> echaloteHollandeCrops = crops.get(Pair.of("ZBK", "H29"));
        CroppingPlanEntry crop = echaloteHollandeCrops.iterator().next();

        Map<Pair<String, String>, RefDestination> destinations = testData.getDestinations();
        RefDestination destination = destinations.get(Pair.of("ZBK", "H29"));

        Map<Pair<String, String>, RefSpeciesToSector> refSpeciesToSectors = testData.getRefSpeciesToSectors();

        Map<String, CroppingPlanSpecies> cpsByCodes = CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies()).stream().collect(Collectors.toMap((CroppingPlanSpecies::getCode), Function.identity()));
        Collection<HarvestingActionValorisation> valorisations = testDatas.createHarvestingActionValorisationsForCrop(
                valorisationDao,
                destination,
                crop,
                100.0,
                0,
                1,
                2017,
                1,
                1,
                2017,
                refSpeciesToSectors);

        for (HarvestingActionValorisation valorisation : valorisations) {
            CroppingPlanSpecies croppingPlanSpecies = cpsByCodes.get(valorisation.getSpeciesCode());
            testDatas.createHarvestingPrice(
                    valorisation,
                    ps,
                    null,
                    PriceUnit.EURO_KG,
                    2.6,
                    null,
                    croppingPlanSpecies);
        }

        List<RefHarvestingPrice> refScenarioPrices = scenarioPriceDao.findAll();
        Set<String> scenarioCodes = refScenarioPrices.stream().map(RefHarvestingPrice::getCode_scenario).collect(Collectors.toSet());
        scenarioCodes.remove("");

        Map<String, List<RefHarvestingPrice>> scenarioPrices = scenarioPriceDao.getAllScenarioPricesForGrowingSystemByPracticedSystemIds(ps.getGrowingSystem(), scenarioCodes);
        Assertions.assertEquals(2, scenarioPrices.get(ps.getTopiaId()).size());
    }

    @Test
    public void testAgressorForSectors() throws IOException {
        testDatas.importNuisiblesEDI();

        Set<BioAgressorType> bioAgressorTypes = new HashSet<>();
        bioAgressorTypes.add(BioAgressorType.MALADIE);
        assertThat(referentialService.getBioAgressors(bioAgressorTypes)).hasSize(13);
        assertThat(referentialService.getBioAgressors(bioAgressorTypes, Collections.singleton(Sector.POLYCULTURE_ELEVAGE))).hasSize(0);
        assertThat(referentialService.getBioAgressors(bioAgressorTypes, Collections.singleton(Sector.ARBORICULTURE))).hasSize(10);
        assertThat(referentialService.getBioAgressors(bioAgressorTypes,
                Arrays.asList(Sector.ARBORICULTURE, Sector.POLYCULTURE_ELEVAGE, Sector.GRANDES_CULTURES))).hasSize(10);
    }

    @Test
    public void testGetBioAgressors() {
        List<RefBioAgressor> bioAgressors = referentialService.getBioAgressors(Collections.singleton(BioAgressorType.ADVENTICE), null);
        assertThat(bioAgressors.isEmpty()).isTrue();
    }

    @Test
    public void testSetPriceUnitsByInputUnit() {

        ReferentialServiceImpl rsi = new ReferentialServiceImpl();
        InputPriceCategory categ = InputPriceCategory.MINERAL_INPUT;

        RefInputUnitPriceUnitConverter ipc0 = new RefInputUnitPriceUnitConverterImpl();
        ipc0.setMineralProductUnit(MineralProductUnit.KG_HA);
        ipc0.setPriceUnit(PriceUnit.EURO_HA);

        RefInputUnitPriceUnitConverter ipc1 = new RefInputUnitPriceUnitConverterImpl();
        ipc1.setMineralProductUnit(MineralProductUnit.L_HA);
        ipc1.setPriceUnit(PriceUnit.EURO_HA);

        RefInputUnitPriceUnitConverter ipc2 = new RefInputUnitPriceUnitConverterImpl();
        ipc2.setMineralProductUnit(MineralProductUnit.T_HA);
        ipc2.setPriceUnit(PriceUnit.EURO_HA);

        RefInputUnitPriceUnitConverter ipc3 = new RefInputUnitPriceUnitConverterImpl();
        ipc3.setPhytoProductUnit(PhytoProductUnit.T_HA);
        ipc3.setPriceUnit(PriceUnit.EURO_HA);

        RefInputUnitPriceUnitConverter ipc4 = new RefInputUnitPriceUnitConverterImpl();
        ipc4.setPhytoProductUnit(PhytoProductUnit.T_HA);
        ipc4.setPriceUnit(PriceUnit.EURO_T);

        List<RefInputUnitPriceUnitConverter> allowedConvertions = Lists.newArrayList(ipc0, ipc1, ipc2, ipc3, ipc4);
        Map<Enum<?>, List<PriceUnit>> result = rsi.setPriceUnitsByProductUnit(categ, allowedConvertions, MineralProductUnit.class);

        List<PriceUnit> pu0 = result.get(MineralProductUnit.KG_HA);
        Assertions.assertEquals(1, pu0.size());
        Assertions.assertEquals(PriceUnit.EURO_HA, pu0.get(0));

        result = rsi.setPriceUnitsByProductUnit(categ, allowedConvertions, PhytoProductUnit.class);

        List<PriceUnit> pu1 = result.get(PhytoProductUnit.KG_HA);
        Assertions.assertNull(pu1);

        Map<Enum<?>, List<PriceUnit>> result1 = rsi.setPriceUnitsByProductUnit(categ, allowedConvertions, PhytoProductUnit.class);
        List<PriceUnit> pu2 = result1.get(PhytoProductUnit.T_HA);
        Assertions.assertEquals(2, pu2.size());
    }

    protected void createRefFertiminunifaReferentialProducts() {
        refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS AUTRES",
                RefFertiMinUNIFA.PROPERTY_TOPIA_ID, UUID.randomUUID().toString(),
                RefFertiMinUNIFA.PROPERTY_CATEG, 253,
                RefFertiMinUNIFA.PROPERTY_FORME, "Compacté",
                RefFertiMinUNIFA.PROPERTY_ACTIVE, true,
                RefFertiMinUNIFA.PROPERTY_N, 33.5,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );

        refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS AUTRES",
                RefFertiMinUNIFA.PROPERTY_TOPIA_ID, UUID.randomUUID().toString(),
                RefFertiMinUNIFA.PROPERTY_CATEG, 253,
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_ACTIVE, true,
                RefFertiMinUNIFA.PROPERTY_N, 33.5,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );

        refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS AUTRES",
                RefFertiMinUNIFA.PROPERTY_TOPIA_ID, UUID.randomUUID().toString(),
                RefFertiMinUNIFA.PROPERTY_CATEG, 253,
                RefFertiMinUNIFA.PROPERTY_FORME, "Pulvérulent",
                RefFertiMinUNIFA.PROPERTY_ACTIVE, true,
                RefFertiMinUNIFA.PROPERTY_N, 33.5,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );

        refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS NK",
                RefFertiMinUNIFA.PROPERTY_TOPIA_ID, UUID.randomUUID().toString(),
                RefFertiMinUNIFA.PROPERTY_CATEG, 516,
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_ACTIVE, true,
                RefFertiMinUNIFA.PROPERTY_N, 33.5,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );

        refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS NP",
                RefFertiMinUNIFA.PROPERTY_TOPIA_ID, UUID.randomUUID().toString(),
                RefFertiMinUNIFA.PROPERTY_CATEG, 515,
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_ACTIVE, true,
                RefFertiMinUNIFA.PROPERTY_N, 33.5,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );

        refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS NPK < OU = 10 % N",
                RefFertiMinUNIFA.PROPERTY_TOPIA_ID, UUID.randomUUID().toString(),
                RefFertiMinUNIFA.PROPERTY_CATEG, 535,
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_ACTIVE, true,
                RefFertiMinUNIFA.PROPERTY_N, 33.5,
                RefFertiMinUNIFA.PROPERTY_SOURCE, "UNIFA"
        );
    }

    @Test
    void isValidRefFertiMinProduct() {
        createRefFertiminunifaReferentialProducts();

        RefFertiMinUNIFA product = new RefFertiMinUNIFAImpl();
        product.setCateg(535);
        product.setForme("Granulé");
        product.setType_produit("AMENDEMENTS-ENGRAIS NPK < OU = 10 % N");
        product.setN(6.0);
        product.setP2O5(3.0);
        product.setK2O(12.0);
        product.setActive(true);
        boolean result = referentialService.isValidRefFertiMinProduct(product);
        Assertions.assertTrue(result);
    }

    @Test
    void isValidRefFertiMinProductMissingTypeCateg() {
        createRefFertiminunifaReferentialProducts();

        RefFertiMinUNIFA product = new RefFertiMinUNIFAImpl();
        product.setForme("Granulé");
        product.setType_produit("AMENDEMENTS-ENGRAIS NPK < OU = 10 % N");
        product.setN(6.0);
        product.setP2O5(3.0);
        product.setK2O(12.0);
        product.setActive(true);
        boolean result = referentialService.isValidRefFertiMinProduct(product);
        Assertions.assertFalse(result);
    }

    @Test
    void isValidRefFertiMinProductMissingForme() {
        createRefFertiminunifaReferentialProducts();

        RefFertiMinUNIFA product = new RefFertiMinUNIFAImpl();
        product.setCateg(535);
        product.setType_produit("AMENDEMENTS-ENGRAIS NPK < OU = 10 % N");
        product.setN(6.0);
        product.setP2O5(3.0);
        product.setK2O(12.0);
        product.setActive(true);
        boolean result = referentialService.isValidRefFertiMinProduct(product);
        Assertions.assertFalse(result);
    }

    @Test
    void isValidRefFertiMinProductNoCompo() {
        RefFertiMinUNIFA product = new RefFertiMinUNIFAImpl();
        product.setCateg(535);
        product.setForme("Granulé");
        product.setType_produit("AMENDEMENTS-ENGRAIS NPK < OU = 10 % N");
        product.setActive(true);
        boolean result = referentialService.isValidRefFertiMinProduct(product);
        Assertions.assertFalse(result);
    }

    @Test
    void isValidRefFertiMinProductCompoInf0() {
        createRefFertiminunifaReferentialProducts();

        RefFertiMinUNIFA product = new RefFertiMinUNIFAImpl();
        product.setCateg(535);
        product.setForme("Granulé");
        product.setType_produit("AMENDEMENTS-ENGRAIS NPK < OU = 10 % N");
        product.setN(-26.0);
        product.setP2O5(3.0);
        product.setK2O(12.0);
        product.setActive(true);
        boolean result = referentialService.isValidRefFertiMinProduct(product);
        Assertions.assertFalse(result);
    }

    @Test
    void isValidRefFertiMinProductNoTypeProduitNotFound() {
        createRefFertiminunifaReferentialProducts();

        RefFertiMinUNIFA product = new RefFertiMinUNIFAImpl();
        product.setCateg(535);
        product.setForme("Granulé");
        product.setType_produit("TYPE DOES NOT MATCH ANY EXISTING");
        product.setN(-26.0);
        product.setP2O5(3.0);
        product.setK2O(12.0);
        product.setActive(true);
        boolean result = referentialService.isValidRefFertiMinProduct(product);
        Assertions.assertFalse(result);
    }

    @Test
    public void testFindEspeceAndVariete() {
        testDatas.createRefEspeceToVariete();
        testDatas.createRefVariete();
        testDatas.createRefEspecesLienCulturesEdi();

        RefEspeceTopiaDao refEspecesDao = getPersistenceContext().getRefEspeceDao();
        List<Pair<RefEspece, RefVariete>> res = refEspecesDao.findEspeceAndVariete();

        Assertions.assertEquals(refEspecesDao.forActiveEquals(Boolean.TRUE).count(), res.size());

        testDatas.createRefEspece(refEspecesDao, "ZMO", "", "", "H61", "ZMO H61");
        testDatas.createRefVarietePlanGrape(257, UUID.randomUUID().toString(), UUID.randomUUID().toString());

        getPersistenceContext().getRefEspeceToVarieteDao().create(
                RefEspeceToVariete.PROPERTY_ACTIVE, true,
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_AUTRE_REFERENTIEL, "ZMO",
                RefEspeceToVariete.PROPERTY_CODE_ESPECE_EDI, "ZMO",
                RefEspeceToVariete.PROPERTY_REFERENTIEL_SOURCE, "plantgrape",
                RefEspeceToVariete.PROPERTY_SOURCE, "Agrosyst 2013"
        );
        res = refEspecesDao.findEspeceAndVariete();
        Assertions.assertEquals(refEspecesDao.forActiveEquals(Boolean.TRUE).count(), res.size());
    }

    @Test
    public void testGetEquipmentlUsageRangeForEquipments() {

        RefMaterielTractionTopiaDao tractorDao = getPersistenceContext().getRefMaterielTractionDao();

        List<RefMateriel> equipments = new ArrayList<>();

        final RefMaterielTraction traction = tractorDao.create(
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL1, "TRACTEURS CLASSIQUES",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL2, "Catég. B = 96 à 135 ch = Catégorie A + suspension du pont avant de série",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL3, "126 à 135 ch",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL4, "130 ch",
                RefMaterielTraction.PROPERTY_UNITE, "heure",
                RefMaterielTraction.PROPERTY_UNITE_PAR_AN, 900.0,
                RefMaterielTraction.PROPERTY_ACTIVE, true
        );
        equipments.add(traction);

        tractorDao.create(
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL1, "TRACTEURS CLASSIQUES",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL2, "Catég. B = 96 à 135 ch = Catégorie A + suspension du pont avant de série",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL3, "126 à 135 ch",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL4, "130 ch",
                RefMaterielTraction.PROPERTY_UNITE, "heure",
                RefMaterielTraction.PROPERTY_UNITE_PAR_AN, 700.0,
                RefMaterielTraction.PROPERTY_ACTIVE, true
        );
        tractorDao.create(
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL1, "TRACTEURS CLASSIQUES",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL2, "Catég. B = 96 à 135 ch = Catégorie A + suspension du pont avant de série",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL3, "126 à 135 ch",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL4, "130 ch",
                RefMaterielTraction.PROPERTY_UNITE, "heure",
                RefMaterielTraction.PROPERTY_UNITE_PAR_AN, 500.0,
                RefMaterielTraction.PROPERTY_ACTIVE, true
        );

        RefMaterielAutomoteurTopiaDao refMaterielAutomoteurDao = getPersistenceContext().getRefMaterielAutomoteurDao();
        equipments.add(refMaterielAutomoteurDao.create(
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL1, "MOISSONNEUSES-BATTEUSES",
                RefMaterielTraction.PROPERTY_TYPE_MATERIEL2, "MB < 110 ch d'occasion",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL3, "4 secoueurs",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL4, "coupe < 370 cm",
                RefMaterielAutomoteur.PROPERTY_UNITE, "ha",
                RefMaterielAutomoteur.PROPERTY_UNITE_PAR_AN, 50.0,
                RefMaterielAutomoteur.PROPERTY_ACTIVE, true
        ));
        refMaterielAutomoteurDao.create(
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL1, "MOISSONNEUSES-BATTEUSES",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL2, "MB < 110 ch d'occasion",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL3, "4 secoueurs",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL4, "coupe < 370 cm",
                RefMaterielAutomoteur.PROPERTY_UNITE, "ha",
                RefMaterielAutomoteur.PROPERTY_UNITE_PAR_AN, 75.0,
                RefMaterielAutomoteur.PROPERTY_ACTIVE, true
        );
        refMaterielAutomoteurDao.create(
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL1, "MOISSONNEUSES-BATTEUSES",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL2, "MB < 110 ch d'occasion",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL3, "4 secoueurs",
                RefMaterielAutomoteur.PROPERTY_TYPE_MATERIEL4, "coupe < 370 cm",
                RefMaterielAutomoteur.PROPERTY_UNITE, "ha",
                RefMaterielAutomoteur.PROPERTY_UNITE_PAR_AN, 100.0,
                RefMaterielAutomoteur.PROPERTY_ACTIVE, true
        );

        RefMaterielOutilTopiaDao refMaterielOutilDao = getPersistenceContext().getRefMaterielOutilDao();
        final RefMaterielOutil refMaterielOutil = refMaterielOutilDao.create(
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL1, "Charrue",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL2, "Charrue décavaillonneuse VE",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL3, "mécanique",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL4, "1 rg complet - 2 corps",
                RefMaterielOutil.PROPERTY_UNITE, "ha",
                RefMaterielOutil.PROPERTY_UNITE_PAR_AN, 15.0,
                RefMaterielOutil.PROPERTY_ACTIVE, true
        );
        equipments.add(refMaterielOutil);
        refMaterielOutilDao.create(
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL1, "Charrue",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL2, "Charrue décavaillonneuse VE",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL3, "mécanique",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL4, "1 rg complet - 2 corps",
                RefMaterielOutil.PROPERTY_UNITE, "ha",
                RefMaterielOutil.PROPERTY_UNITE_PAR_AN, 30.0,
                RefMaterielOutil.PROPERTY_ACTIVE, true
        );
        refMaterielOutilDao.create(
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL1, "Charrue",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL2, "Charrue décavaillonneuse VE",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL3, "mécanique",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL4, "1 rg complet - 2 corps",
                RefMaterielOutil.PROPERTY_UNITE, "ha",
                RefMaterielOutil.PROPERTY_UNITE_PAR_AN, 22.5,
                RefMaterielOutil.PROPERTY_ACTIVE, true
        );

        RefMaterielIrrigationTopiaDao refMaterielIrrigationDao = getPersistenceContext().getRefMaterielIrrigationDao();
        final RefMaterielIrrigation refMaterielIrrigation = refMaterielIrrigationDao.create(
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL1, "Enrouleurs",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL2, "Enrouleur 90 mm x 350 m",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL3, "40 m3/h",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL4, "",
                RefMaterielOutil.PROPERTY_UNITE, "ha",
                RefMaterielOutil.PROPERTY_UNITE_PAR_AN, 120.0,
                RefMaterielOutil.PROPERTY_ACTIVE, true
        );
        equipments.add(refMaterielIrrigation);

        refMaterielIrrigationDao.create(
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL1, "Enrouleurs",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL2, "Enrouleur 90 mm x 350 m",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL3, "40 m3/h",
                RefMaterielOutil.PROPERTY_TYPE_MATERIEL4, "",
                RefMaterielOutil.PROPERTY_UNITE, "m3",
                RefMaterielOutil.PROPERTY_UNITE_PAR_AN, 36000.0,
                RefMaterielOutil.PROPERTY_ACTIVE, true
        );

        Collection<EquipmentUsageRange> equipmentUsageRanges = referentialService.getEquipmentlUsageRangeForEquipments(equipments);
        Assertions.assertEquals(equipments.size(), equipmentUsageRanges.size());
        Map<RefMateriel, EquipmentUsageRange> res0 = equipmentUsageRanges.stream().collect(Collectors.toMap(EquipmentUsageRange::getRefMateriel, Function.identity()));
        EquipmentUsageRange equipmentUsageRange = res0.get(traction);
        Assertions.assertEquals(500.0, equipmentUsageRange.getLowestValueByH());
        Assertions.assertEquals(900.0, equipmentUsageRange.getHighestValueByH());

        EquipmentUsageRange equipmentUsageRangeForOutil = res0.get(refMaterielOutil);
        Assertions.assertEquals(15.0, equipmentUsageRangeForOutil.getLowestValueByHa());
        Assertions.assertEquals(30.0, equipmentUsageRangeForOutil.getHighestValueByHa());

        EquipmentUsageRange equipmentUsageRangeForIrrig = res0.get(refMaterielIrrigation);
        Assertions.assertEquals(120.0, equipmentUsageRangeForIrrig.getLowestValueByHa());
        Assertions.assertEquals(120.0, equipmentUsageRangeForIrrig.getHighestValueByHa());
    }

    @Disabled
    @Test
    public void showCreateSchema() {
        AgrosystServiceConfig serviceConfig = getConfig();

        //Properties properties = new Properties();
        //properties.setProperty(Environment.DIALECT, PostgreSQL94Dialect.class.getName());
        //properties.setProperty(HibernateAvailableSettings.SHOW_SQL, "false");
        //properties.setProperty(HibernateAvailableSettings.CONNECTION_PROVIDER, "org.hibernate.hikaricp.internal.HikariCPConnectionProvider");

        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        TopiaConfiguration topiaConfiguration = topiaConfigurationBuilder
                .forTestDatabase(this.getClass(), "showCreateSchema")
                .useHibernateUpdate()
                .doNotValidateSchemaOnStartup()
                .useHikariConnectionPool()
                .build();

        AgrosystTopiaApplicationContext applicationContext = AgrosystConfigurationHelper.newApplicationContext(getConfig(), topiaConfiguration);
        applicationContext.applicationInit();

        Metadata metaData = applicationContext.getHibernateProvider().getMetaData();
        new SchemaExport()
                .execute(EnumSet.of(TargetType.DATABASE, TargetType.STDOUT), SchemaExport.Action.CREATE, metaData);

    }

    @Test
    public void testOrderBy() throws IOException {
        testDatas.importActaDosageSpc();
        final RefActaDosageSPCTopiaDao refActaDosageSPCDao = getPersistenceContext().getRefActaDosageSPCDao();
        List<RefActaDosageSPC> results = refActaDosageSPCDao.forProperties(
                RefActaDosageSPC.PROPERTY_ACTIVE, true
        ).setOrderByArguments(RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR + " ASC").findAll();
        RefActaDosageSPC result = results.stream().filter(refActaDosageSPC -> refActaDosageSPC.getDosage_spc_valeur() != null).findFirst().orElse(results.stream().findFirst().orElse(null));
        Assertions.assertNotNull(result);
        Assertions.assertNotNull(result.getDosage_spc_valeur());
        Assertions.assertEquals(0.02, result.getDosage_spc_valeur(), 0.001);
    }

    @Test
    public void testGetDoseForInputWithValidCropAndTargetAndMaxRefDose_1142_T1() throws IOException {

        createRefDoseDataTest();
        testDatas.importCountries();
        RefCountry france = testDatas.getDefaultFranceCountry();

        final RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        final RefNuisibleEDITopiaDao refNuisibleEDIDao = getPersistenceContext().getRefNuisibleEDIDao();
        final RefEspeceTopiaDao refEspeceDao = getPersistenceContext().getRefEspeceDao();

        final RefEspece pomme0 = refEspeceDao.forNaturalId("G21", "H36", "", "").findUnique();
        final RefNuisibleEDI travelure0 = refNuisibleEDIDao.forNaturalId(19629).findUnique();
        final RefActaTraitementsProduit horizonArbo0 = refActaTraitementsProduitDao.forNaturalId("3226", 140, france).findUnique();

        String refInputId = horizonArbo0.getTopiaId();
        Set<String> refEspeceIds = new HashSet<>();
        refEspeceIds.add(pomme0.getTopiaId());
        Set<String> refTargetIds = new HashSet<>();
        refTargetIds.add(String.valueOf(travelure0.getReference_id()));

        ReferenceDoseDTO dose = referentialService.computeLocalizedReferenceDoseForIFCCibleNonMillesime(
                refInputId,
                refEspeceIds,
                refTargetIds,
                null
        );
        Assertions.assertNotNull(dose);
        Assertions.assertEquals(0.4, dose.getValue());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, dose.getUnit());
    }

    @Test
    public void testGetDoseForInputWithoutTarget_1142_T2() throws IOException {

        final RefCountry france = testDatas.getDefaultFranceCountry();
        createRefDoseDataTest();

        final RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        final RefEspeceTopiaDao refEspeceDao = getPersistenceContext().getRefEspeceDao();

        final RefActaTraitementsProduit horizonArbo0 = refActaTraitementsProduitDao.forNaturalId("3226", 140, france).findUnique();
        final RefEspece pomme0 = refEspeceDao.forNaturalId("G21", "H36", "", "").findUnique();

        String refInputId = horizonArbo0.getTopiaId();
        Set<String> refEspeceIds = new HashSet<>();
        refEspeceIds.add(pomme0.getTopiaId());

        ReferenceDoseDTO dose = referentialService.computeLocalizedReferenceDoseForIFCCibleNonMillesime(
                refInputId,
                refEspeceIds,
                null,
                null
        );
        Assertions.assertNotNull(dose);
        Assertions.assertEquals(0.4, dose.getValue());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, dose.getUnit());
    }

    @Test
    public void testGetDoseIfNoMaaCropRefPresent_1142_T3() throws IOException {

        createRefDoseDataTest();
        RefCountry france = testDatas.getDefaultFranceCountry();

        final RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        final RefNuisibleEDITopiaDao refNuisibleEDIDao = getPersistenceContext().getRefNuisibleEDIDao();
        final RefEspeceTopiaDao refEspeceDao = getPersistenceContext().getRefEspeceDao();

        final RefActaTraitementsProduit horizonArbo0 = refActaTraitementsProduitDao.forNaturalId("3226", 140, france).findUnique();
        final RefEspece pomme0 = refEspeceDao.forNaturalId("G21", "H36", "", "").findUnique();
        final RefNuisibleEDI moniliose = refNuisibleEDIDao.forNaturalId(19122).findUnique();

        String refInputId = horizonArbo0.getTopiaId();
        Set<String> refEspeceIds = new HashSet<>();
        refEspeceIds.add(pomme0.getTopiaId());

        Set<String> refTargetIds = new HashSet<>();
        refTargetIds.add(String.valueOf(moniliose.getReference_id()));

        final RefMAADosesRefParGroupeCibleTopiaDao refMAADosesRefParGroupeCibleDao = getPersistenceContext().getRefMAADosesRefParGroupeCibleDao();
        List<RefMAADosesRefParGroupeCible> refMaaDoses = refMAADosesRefParGroupeCibleDao.findAll();
        refMaaDoses.forEach(refMaaDose -> refMaaDose.setActive(false));
        refMAADosesRefParGroupeCibleDao.updateAll(refMaaDoses);

        // pas de dose MAA pour la culture, on retombe sur la dose Acta de référence
        ReferenceDoseDTO dose = referentialService.computeLocalizedReferenceDoseForIFCCibleNonMillesime(
                refInputId,
                refEspeceIds,
                refTargetIds,
                null
        );
        Assertions.assertNotNull(dose);
        Assertions.assertEquals(0.3, dose.getValue());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, dose.getUnit());
    }

    @Test
    public void testGetDoseForInputWithBadTarget_1142_T4() throws IOException {

        createRefDoseDataTest();
        RefCountry france = testDatas.getDefaultFranceCountry();

        final RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        final RefNuisibleEDITopiaDao refNuisibleEDIDao = getPersistenceContext().getRefNuisibleEDIDao();
        final RefEspeceTopiaDao refEspeceDao = getPersistenceContext().getRefEspeceDao();

        final RefActaTraitementsProduit horizonArbo0 = refActaTraitementsProduitDao.forNaturalId("3226", 140, france).findUnique();
        final RefEspece pomme0 = refEspeceDao.forNaturalId("G21", "H36", "", "").findUnique();
        final RefNuisibleEDI moniliose = refNuisibleEDIDao.forNaturalId(19122).findUnique();

        String refInputId = horizonArbo0.getTopiaId();
        Set<String> refEspeceIds = new HashSet<>();
        refEspeceIds.add(pomme0.getTopiaId());

        Set<String> refTargetIds = new HashSet<>();
        refTargetIds.add(String.valueOf(moniliose.getReference_id()));

        ReferenceDoseDTO dose = referentialService.computeLocalizedReferenceDoseForIFCCibleNonMillesime(
                refInputId,
                refEspeceIds,
                refTargetIds,
                null
        );
        Assertions.assertNotNull(dose);
        Assertions.assertEquals(0.4, dose.getValue());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, dose.getUnit());
    }

    @Test
    public void testGetDoseForInputWithBadAndGoodTarget_1142_T5() throws IOException {

        createRefDoseDataTest();
        final RefCountry france = testDatas.getDefaultFranceCountry();

        final RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        final RefNuisibleEDITopiaDao refNuisibleEDIDao = getPersistenceContext().getRefNuisibleEDIDao();
        final RefEspeceTopiaDao refEspeceDao = getPersistenceContext().getRefEspeceDao();

        final RefActaTraitementsProduit horizonArbo0 = refActaTraitementsProduitDao.forNaturalId("3226", 140, france).findUnique();
        final RefEspece pomme0 = refEspeceDao.forNaturalId("G21", "H36", "", "").findUnique();
        final RefNuisibleEDI moniliose = refNuisibleEDIDao.forNaturalId(19122).findUnique();
        final RefNuisibleEDI travelure = refNuisibleEDIDao.forNaturalId(19629).findUnique();

        String refInputId = horizonArbo0.getTopiaId();
        int idTraitement = horizonArbo0.getId_traitement();
        Set<String> refEspeceIds = new HashSet<>();
        refEspeceIds.add(pomme0.getTopiaId());

        Set<String> refTargetIds = new HashSet<>();
        refTargetIds.add(String.valueOf(moniliose.getReference_id()));
        refTargetIds.add(String.valueOf(travelure.getReference_id()));

        ReferenceDoseDTO dose = referentialService.computeLocalizedReferenceDoseForIFCCibleNonMillesime(
                refInputId,
                refEspeceIds,
                refTargetIds,
                null
        );
        Assertions.assertNotNull(dose);
        Assertions.assertEquals(0.4, dose.getValue());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, dose.getUnit());
    }

    private void createRefDoseDataTest() throws IOException {

        testDatas.importActaGroupeCulturesForIftTest();
        testDatas.importRefMaaDosesRefParGroupeCible();
        testDatas.importNuisiblesEdiIftCible();
        testDatas.importActaDosageSpcIFT();

        Collection<Sector> travelureSectors = new ArrayList<>();
        travelureSectors.add(Sector.ARBORICULTURE);

        final RefNuisibleEDITopiaDao refNuisibleEDIDao = getPersistenceContext().getRefNuisibleEDIDao();
        final RefEspeceTopiaDao refEspeceDao = getPersistenceContext().getRefEspeceDao();
        final RefCultureMAATopiaDao refCultureMAADao = getPersistenceContext().getRefCultureMAADao();

        refNuisibleEDIDao.create(
                RefNuisibleEDI.PROPERTY_TOPIA_ID, "fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI_5ec08a85-ab63-4ec1-ab6d-c4d02c5ccdde",
                RefNuisibleEDI.PROPERTY_MAIN, false,
                RefNuisibleEDI.PROPERTY_ACTIVE, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, BioAgressorType.MALADIE,
                RefNuisibleEDI.PROPERTY_SECTORS, travelureSectors,
                RefNuisibleEDI.PROPERTY_REFERENCE_LABEL, "Travelure",
                RefNuisibleEDI.PROPERTY_REFERENCE_CODE, "000000VENUSP",
                RefNuisibleEDI.PROPERTY_REFERENCE_ID, 19629,
                RefNuisibleEDI.PROPERTY_REPOSITORY_ID, 22
        );

        Collection<Sector> monilioseSectors = new ArrayList<>();
        monilioseSectors.add(Sector.ARBORICULTURE);
        refNuisibleEDIDao.create(
                RefNuisibleEDI.PROPERTY_TOPIA_ID, "fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI_6f9b621a-adbf-463e-88b0-af44f92ff064",
                RefNuisibleEDI.PROPERTY_MAIN, false,
                RefNuisibleEDI.PROPERTY_ACTIVE, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, BioAgressorType.MALADIE,
                RefNuisibleEDI.PROPERTY_SECTORS, monilioseSectors,
                RefNuisibleEDI.PROPERTY_REFERENCE_LABEL, "Moniliose  des arbres fruitiers et des fruits",
                RefNuisibleEDI.PROPERTY_REFERENCE_CODE, "MONILAMONIFG",
                RefNuisibleEDI.PROPERTY_REFERENCE_ID, 19122,
                RefNuisibleEDI.PROPERTY_REPOSITORY_ID, 22
        );

        RefEspece especePomme = refEspeceDao.create(
                RefEspece.PROPERTY_TOPIA_ID, "fr.inra.agrosyst.api.entities.referential.RefEspece_c66e6c6b-8873-408d-a918-066454f0addc",
                RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, "G21",
                RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, "H36",
                RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, "",
                RefEspece.PROPERTY_CODE_DESTINATION__AEE, "",
                RefEspece.PROPERTY_CODE_CATEGORIE_DE_CULTURES, "J01",
                RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "Pommier",
                RefEspece.PROPERTY_LIBELLE_CATEGORIE_DE_CULTURES, "Cultures fruitières",
                RefEspece.PROPERTY_CODE_ESPECE__EPPO, "MABSD",
                RefEspece.PROPERTY_GENRE, "Malus",
                RefEspece.PROPERTY_ESPECE, "domestica",
                RefEspece.PROPERTY_ID_CULTURE_ACTA, 171,
                RefEspece.PROPERTY_NOM_CULTURE_ACTA, "pommier"
        );

        RefCultureMAA culturesMaa = refCultureMAADao.create(
                RefCultureMAA.PROPERTY_TOPIA_ID, "fr.inra.agrosyst.api.entities.referential.RefLienCulturesEdiActa_876537b4-2390-4832-b9d6-13fac9a476da",
                RefCultureMAA.PROPERTY_CODE_CULTURE_MAA, "1163",
                RefCultureMAA.PROPERTY_CULTURE_MAA, " Pommier",
                RefCultureMAA.PROPERTY_ACTIVE, true
        );

        especePomme.addCulturesMaa(culturesMaa);
        refEspeceDao.update(especePomme);

        final RefCiblesAgrosystGroupesCiblesMAATopiaDao refCiblesAgrosystGroupesCiblesMAADao = getPersistenceContext().getRefCiblesAgrosystGroupesCiblesMAADao();
        RefCiblesAgrosystGroupesCiblesMAA c0 = refCiblesAgrosystGroupesCiblesMAADao.create(
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID, "19629",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA, "151",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA, "Tavelure",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_REFERENCE_PARAM, BioAgressorType.MALADIE);
        c0.setActive(true);
        refCiblesAgrosystGroupesCiblesMAADao.update(c0);

        RefCiblesAgrosystGroupesCiblesMAA c1 = refCiblesAgrosystGroupesCiblesMAADao.create(
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID, "19629",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA, "84",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA, "Maladies diverses",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_REFERENCE_PARAM, BioAgressorType.MALADIE);
        c1.setActive(true);
        refCiblesAgrosystGroupesCiblesMAADao.update(c1);

        RefCiblesAgrosystGroupesCiblesMAA c2 = refCiblesAgrosystGroupesCiblesMAADao.create(
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID, "19122",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA, "94",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_GROUPE_CIBLE_MAA, "Monilioses",
                RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_REFERENCE_PARAM, BioAgressorType.MALADIE
        );
        c2.setActive(true);
        refCiblesAgrosystGroupesCiblesMAADao.update(c2);

        RefCountry france = testDatas.getDefaultFranceCountry();

        final RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        RefActaTraitementsProduit horizonArbo = new RefActaTraitementsProduitImpl();
        horizonArbo.setRefCountry(france);
        horizonArbo.setId_produit("3226");
        horizonArbo.setId_traitement(140);
        horizonArbo.setActive(true);
        horizonArbo.setCode_AMM("9300186");
        horizonArbo.setCode_traitement_maa("S5");
        refActaTraitementsProduitDao.create(horizonArbo);

        RefMAADosesRefParGroupeCibleTopiaDao refMAADosesRefParGroupeCiblesDao = getPersistenceContext().getRefMAADosesRefParGroupeCibleDao();
        RefMAADosesRefParGroupeCible maaDosePommierTavelure2017 = refMAADosesRefParGroupeCiblesDao.createByNaturalId("9300186", "1163", "151", 2017, "S5");
        maaDosePommierTavelure2017.setActive(true);
        maaDosePommierTavelure2017.setCulture_maa("Pommier");
        maaDosePommierTavelure2017.setTraitement_maa("Fongicides bactericides");
        maaDosePommierTavelure2017.setDose_ref_maa(0.3);
        maaDosePommierTavelure2017.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);

        RefMAADosesRefParGroupeCible maaDosePommierTavelure2018 = refMAADosesRefParGroupeCiblesDao.createByNaturalId("9300186", "1163", "151", 2018, "S5");
        maaDosePommierTavelure2018.setActive(true);
        maaDosePommierTavelure2018.setCulture_maa("Pommier");
        maaDosePommierTavelure2018.setTraitement_maa("Fongicides bactericides");
        maaDosePommierTavelure2018.setDose_ref_maa(0.3);
        maaDosePommierTavelure2018.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);

        RefMAADosesRefParGroupeCible maaDosePommierTavelure2015 = refMAADosesRefParGroupeCiblesDao.createByNaturalId("9300186", "1163", "151", 2015, "S5");
        maaDosePommierTavelure2015.setActive(true);
        maaDosePommierTavelure2015.setCulture_maa("Pommier");
        maaDosePommierTavelure2015.setTraitement_maa("Fongicides bactericides");
        maaDosePommierTavelure2015.setDose_ref_maa(0.3);
        maaDosePommierTavelure2015.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);

        RefMAADosesRefParGroupeCible maaDosePommierTavelure2016 = refMAADosesRefParGroupeCiblesDao.createByNaturalId("9300186", "1163", "151", 2016, "S5");
        maaDosePommierTavelure2016.setActive(true);
        maaDosePommierTavelure2016.setCulture_maa("Pommier");
        maaDosePommierTavelure2016.setTraitement_maa("Fongicides bactericides");
        maaDosePommierTavelure2016.setDose_ref_maa(0.3);
        maaDosePommierTavelure2016.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);

        RefMAADosesRefParGroupeCible maaDosePommierTavelure2019 = refMAADosesRefParGroupeCiblesDao.createByNaturalId("9300186", "1163", "151", 2019, "S5");
        maaDosePommierTavelure2019.setActive(true);
        maaDosePommierTavelure2019.setCulture_maa("Pommier");
        maaDosePommierTavelure2019.setTraitement_maa("Fongicides bactericides");
        maaDosePommierTavelure2019.setDose_ref_maa(0.4);
        maaDosePommierTavelure2019.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);
    }

    @Test
    public void testSortWithUnits() {
        Optional<RefMAADosesRefParGroupeCible> result;
        List<RefMAADosesRefParGroupeCible> doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa = getRefMAADosesRefParGroupeCible();
        Integer maxCampaign = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream().filter(refMAADosesRefParGroupeCible -> refMAADosesRefParGroupeCible.getCampagne() != null).map(RefMAADosesRefParGroupeCible::getCampagne).max(Integer::compareTo).orElse(-1);
        Optional<RefMAADosesRefParGroupeCible> betsUnitDose = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .filter(dose -> REFERENCE_DOSE_UNIT_ORDER.contains(dose.getUnit_dose_ref_maa()))
                .min(Comparator.comparingInt(dose -> REFERENCE_DOSE_UNIT_ORDER.indexOf(dose.getUnit_dose_ref_maa())));
        if (betsUnitDose.isPresent()) {
            PhytoProductUnit bestUnit = betsUnitDose.get().getUnit_dose_ref_maa();
            result = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                    .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                    .filter(dose -> bestUnit.equals(dose.getUnit_dose_ref_maa()))
                    .min(Comparator.comparing(
                            refMAADosesRefParGroupeCible ->
                                    refMAADosesRefParGroupeCible.getDose_ref_maa() != null ?
                                            refMAADosesRefParGroupeCible.getDose_ref_maa() : Double.MAX_VALUE));
        } else {
            result = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                    .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                    .min(Comparator.comparing(
                            refMAADosesRefParGroupeCible ->
                                    refMAADosesRefParGroupeCible.getDose_ref_maa() != null ?
                                            refMAADosesRefParGroupeCible.getDose_ref_maa() : Double.MAX_VALUE));
        }

        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals(2020, result.get().getCampagne());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, result.get().getUnit_dose_ref_maa());
        Assertions.assertEquals(5.0, result.get().getDose_ref_maa());
    }

    @Test
    public void testSortMaxDoseRef() {
        Optional<RefMAADosesRefParGroupeCible> result;
        List<RefMAADosesRefParGroupeCible> doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa = getRefMAADosesRefParGroupeCible();
        Integer maxCampaign = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream().filter(refMAADosesRefParGroupeCible -> refMAADosesRefParGroupeCible.getCampagne() != null).map(RefMAADosesRefParGroupeCible::getCampagne).max(Integer::compareTo).orElse(-1);

        // first method
        result = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .max(Comparator.comparing(
                        refMAADosesRefParGroupeCible ->
                                refMAADosesRefParGroupeCible.getDose_ref_maa() != null ?
                                        refMAADosesRefParGroupeCible.getDose_ref_maa() : -Double.MAX_VALUE));

        // second method
        Optional<RefMAADosesRefParGroupeCible> result2;
        Comparator<RefMAADosesRefParGroupeCible> maxComparator =
                Comparator.<RefMAADosesRefParGroupeCible, Boolean>comparing(
                                dose -> true
                        )
                        .reversed()
                        .thenComparing(
                                Comparator.nullsFirst(
                                        Comparator.comparing(
                                                d -> d.getDose_ref_maa() == null ?
                                                        -Double.MAX_VALUE :
                                                        d.getDose_ref_maa())));

        result2 = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .max(maxComparator);

        // third method
        Optional<RefMAADosesRefParGroupeCible> result3;
        Comparator<RefMAADosesRefParGroupeCible> maxComparator2 =
                Comparator.<RefMAADosesRefParGroupeCible, Boolean>comparing(
                                dose -> true
                        )
                        .reversed()
                        .thenComparing(Comparator.nullsFirst(Comparator.comparingDouble(RefMAADosesRefParGroupeCible::getDose_ref_maa)));

        result3 = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                .filter(dose -> Objects.nonNull(dose.getDose_ref_maa())) // without filtering on non Null there is an NPE
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .max(maxComparator2);

        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals(maxCampaign, result.get().getCampagne());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, result.get().getUnit_dose_ref_maa());
        Assertions.assertEquals(6.0, result.get().getDose_ref_maa());

        Assertions.assertTrue(result2.isPresent());
        Assertions.assertEquals(maxCampaign, result2.get().getCampagne());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, result2.get().getUnit_dose_ref_maa());
        Assertions.assertEquals(6.0, result2.get().getDose_ref_maa());

        Assertions.assertTrue(result3.isPresent());
        Assertions.assertEquals(maxCampaign, result3.get().getCampagne());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, result3.get().getUnit_dose_ref_maa());
        Assertions.assertEquals(6.0, result3.get().getDose_ref_maa());
    }

    @Test
    public void testSortMinDoseRef() {
        Optional<RefMAADosesRefParGroupeCible> result;
        List<RefMAADosesRefParGroupeCible> doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa = getRefMAADosesRefParGroupeCible();
        Integer maxCampaign = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream().filter(refMAADosesRefParGroupeCible -> refMAADosesRefParGroupeCible.getCampagne() != null).map(RefMAADosesRefParGroupeCible::getCampagne).max(Integer::compareTo).orElse(-1);

        // first method
        result = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .min(Comparator.comparing(
                        refMAADosesRefParGroupeCible ->
                                refMAADosesRefParGroupeCible.getDose_ref_maa() != null ?
                                        refMAADosesRefParGroupeCible.getDose_ref_maa() : Double.MAX_VALUE));

        // second method
        Optional<RefMAADosesRefParGroupeCible> result2;
        Comparator<RefMAADosesRefParGroupeCible> minComparator =
                Comparator.<RefMAADosesRefParGroupeCible, Boolean>comparing(
                        dose -> true
                ).thenComparing(
                        Comparator.nullsLast(
                                Comparator.comparing(
                                        d -> d.getDose_ref_maa() == null ?
                                                Double.MAX_VALUE :
                                                d.getDose_ref_maa())));

        result2 = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .min(minComparator);

        // third method
        Optional<RefMAADosesRefParGroupeCible> result3;
        Comparator<RefMAADosesRefParGroupeCible> minComparator2 =
                Comparator.<RefMAADosesRefParGroupeCible, Boolean>comparing(
                                dose -> true
                        )
                        .thenComparing(Comparator.nullsLast(
                                Comparator.comparingDouble(RefMAADosesRefParGroupeCible::getDose_ref_maa)));

        result3 = doseRefsMatchingCodeAmmTraitementMaaOptionalCodeCultureMaa.stream()
                .filter(dose -> Objects.nonNull(dose.getDose_ref_maa())) // without filtering on non Null there is an NPE
                .filter(dose -> maxCampaign.equals(dose.getCampagne()))
                .min(minComparator2);

        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals(maxCampaign, result.get().getCampagne());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, result.get().getUnit_dose_ref_maa());
        Assertions.assertEquals(5.0, result.get().getDose_ref_maa());

        Assertions.assertTrue(result2.isPresent());
        Assertions.assertEquals(maxCampaign, result2.get().getCampagne());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, result2.get().getUnit_dose_ref_maa());
        Assertions.assertEquals(5.0, result2.get().getDose_ref_maa());

        Assertions.assertTrue(result3.isPresent());
        Assertions.assertEquals(maxCampaign, result3.get().getCampagne());
        Assertions.assertEquals(PhytoProductUnit.KG_HA, result3.get().getUnit_dose_ref_maa());
        Assertions.assertEquals(5.0, result3.get().getDose_ref_maa());
    }

    private List<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCible() {
        List<RefMAADosesRefParGroupeCible> res = new ArrayList<>();
        RefMAADosesRefParGroupeCible r0 = new RefMAADosesRefParGroupeCibleImpl();
        r0.setDose_ref_maa(1.0);
        r0.setUnit_dose_ref_maa(PhytoProductUnit.G_L);
        r0.setCampagne(2019);
        res.add(r0);

        RefMAADosesRefParGroupeCible r1 = new RefMAADosesRefParGroupeCibleImpl();
        r1.setDose_ref_maa(1.0);
        r1.setUnit_dose_ref_maa(PhytoProductUnit.KG_HL);
        r1.setCampagne(2019);
        res.add(r1);

        RefMAADosesRefParGroupeCible r2 = new RefMAADosesRefParGroupeCibleImpl();
        r2.setDose_ref_maa(10.0);
        r2.setUnit_dose_ref_maa(PhytoProductUnit.L_HA);
        r2.setCampagne(2019);
        res.add(r2);

        RefMAADosesRefParGroupeCible r3 = new RefMAADosesRefParGroupeCibleImpl();
        r3.setDose_ref_maa(5.0);
        r3.setUnit_dose_ref_maa(PhytoProductUnit.L_HA);
        r3.setCampagne(2019);
        res.add(r3);

        RefMAADosesRefParGroupeCible r4 = new RefMAADosesRefParGroupeCibleImpl();
        r4.setDose_ref_maa(6.0);
        r4.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);
        r4.setCampagne(2020);
        res.add(r4);

        RefMAADosesRefParGroupeCible r5 = new RefMAADosesRefParGroupeCibleImpl();
        r5.setDose_ref_maa(5.0);
        r5.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);
        r5.setCampagne(2020);
        res.add(r5);

        RefMAADosesRefParGroupeCible r6 = new RefMAADosesRefParGroupeCibleImpl();
        r6.setDose_ref_maa(null);
        r6.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);
        r6.setCampagne(2020);
        res.add(r6);

        RefMAADosesRefParGroupeCible r7 = new RefMAADosesRefParGroupeCibleImpl();
        r7.setDose_ref_maa(5.0);
        r7.setUnit_dose_ref_maa(PhytoProductUnit.KG_HA);
        r7.setCampagne(null);
        res.add(r7);

        return res;
    }

    protected static final List<PhytoProductUnit> REFERENCE_DOSE_UNIT_ORDER = Arrays.asList(
            PhytoProductUnit.L_HA,
            PhytoProductUnit.KG_HA,
            PhytoProductUnit.G_HA,
            PhytoProductUnit.L_HL,
            PhytoProductUnit.KG_HL,
            PhytoProductUnit.G_HL
    );

    @Test
    public void testLoadGlobalRefHarvestingPriceForDestination() throws IOException {
        loadTestValues0();
        RefDestination destination = refDestinationDao.forCode_destination_AEquals("D0106").findUnique();
        CroppingPlanEntry cpe = croppingPlanEntryDao.forCodeEquals("CroppingPlanEntry-code-echalote-0").findAny();

        RefEspece species0 = cpe.getCroppingPlanSpecies().getFirst().getSpecies();
        refHarvestingPriceDao.create(
                RefHarvestingPrice.PROPERTY_CODE_SCENARIO, "",
                RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, destination.getCode_destination_A(),
                RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, species0.getCode_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, species0.getCode_qualifiant_AEE(),
                RefHarvestingPrice.PROPERTY_ORGANIC, false,
                RefHarvestingPrice.PROPERTY_CAMPAIGN, 2010,
                RefHarvestingPrice.PROPERTY_ACTIVE, true,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, -1,//HarvestingPeriod.TOUTES
                RefHarvestingPrice.PROPERTY_PRICE, 159.0,
                RefHarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA
        );
        refHarvestingPriceDao.create(
                RefHarvestingPrice.PROPERTY_CODE_SCENARIO, "",
                RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, destination.getCode_destination_A(),
                RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, species0.getCode_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, species0.getCode_qualifiant_AEE(),
                RefHarvestingPrice.PROPERTY_ORGANIC, false,
                RefHarvestingPrice.PROPERTY_CAMPAIGN, 2011,
                RefHarvestingPrice.PROPERTY_ACTIVE, true,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, -1, //HarvestingPeriod.TOUTES
                RefHarvestingPrice.PROPERTY_PRICE, 170.0,
                RefHarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA
        );

        HarvestingActionValorisation mainValUser0 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpe.getCroppingPlanSpecies(0).getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 0,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2013,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2015,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA);

        ArrayList<HarvestingActionValorisation> valorisations = Lists.newArrayList(mainValUser0);
        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> loadedPrices =
                referentialService.getRefHarvestingPricesForValorisations(valorisations, codeEspBotCodeQualiBySpeciesCode);
        List<RefHarvestingPrice> pricesForValorisation = loadedPrices.get(mainValUser0);
        // no prices are found for range like:
        // 1st try: (2013, 2014, 2015)
        // 2nd try: (2012, 2013, 2014)
        // 3rd try: (2011, 2012, 2013) 1 price found
        Assertions.assertEquals(1, pricesForValorisation.size());
    }

    @Test
    public void testLoadDefaultCampaignsRefHarvestingPrice() throws IOException {
        loadTestValues0();
        RefDestination destination = refDestinationDao.forCode_destination_AEquals("D0106").findUnique();
        CroppingPlanEntry cpe = croppingPlanEntryDao.forCodeEquals("CroppingPlanEntry-code-echalote-0").findAny();

        RefEspece species0 = cpe.getCroppingPlanSpecies().getFirst().getSpecies();
        refHarvestingPriceDao.create(
                RefHarvestingPrice.PROPERTY_CODE_SCENARIO, "",
                RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, species0.getCode_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, species0.getCode_qualifiant_AEE(),
                RefHarvestingPrice.PROPERTY_PRODUIT_RECOLTE, species0.getLibelle_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, destination.getCode_destination_A(),
                RefHarvestingPrice.PROPERTY_DESTINATION, destination.getDestination(),
                RefHarvestingPrice.PROPERTY_ORGANIC, false,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, Calendar.JANUARY,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE, 4,
                RefHarvestingPrice.PROPERTY_CAMPAIGN, 2010,
                RefHarvestingPrice.PROPERTY_ACTIVE, true,
                RefHarvestingPrice.PROPERTY_PRICE, 159.0,
                RefHarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA
        );
        refHarvestingPriceDao.create(
                RefHarvestingPrice.PROPERTY_CODE_SCENARIO, "",
                RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, species0.getCode_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, species0.getCode_qualifiant_AEE(),
                RefHarvestingPrice.PROPERTY_PRODUIT_RECOLTE, species0.getLibelle_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, destination.getCode_destination_A(),
                RefHarvestingPrice.PROPERTY_DESTINATION, destination.getDestination(),
                RefHarvestingPrice.PROPERTY_ORGANIC, false,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, Calendar.JANUARY,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD_DECADE, 1,
                RefHarvestingPrice.PROPERTY_CAMPAIGN, 2011,
                RefHarvestingPrice.PROPERTY_ACTIVE, true,
                RefHarvestingPrice.PROPERTY_PRICE, 170.0,
                RefHarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA
        );

        HarvestingActionValorisation mainValUser0 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpe.getCroppingPlanSpecies(0).getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 0,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2013,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2015,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA);

        ArrayList<HarvestingActionValorisation> valorisations = Lists.newArrayList(mainValUser0);
        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> loadedPrices =
                referentialService.getRefHarvestingPricesForValorisations(valorisations, codeEspBotCodeQualiBySpeciesCode);
        List<RefHarvestingPrice> pricesForValorisation = loadedPrices.get(mainValUser0);
        // no prices are found for range like:
        // 1st try: (2013, 2014, 2015)
        // 2nd try: (2012, 2013, 2014)
        // 3rd try: (2011, 2012, 2013)
        // 2 prices found for range (2010, 2011, 2012)
        Assertions.assertEquals(2, pricesForValorisation.size());
    }

    @Test
    public void testLoadAllPeriodRefHarvestingPrice() throws IOException {
        loadTestValues0();
        RefDestination destination = refDestinationDao.forCode_destination_AEquals("D0106").findUnique();
        CroppingPlanEntry cpe = croppingPlanEntryDao.forCodeEquals("CroppingPlanEntry-code-echalote-0").findAny();

        RefEspece species0 = cpe.getCroppingPlanSpecies().getFirst().getSpecies();
        refHarvestingPriceDao.create(
                RefHarvestingPrice.PROPERTY_CODE_SCENARIO, "",
                RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, species0.getCode_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, species0.getCode_qualifiant_AEE(),
                RefHarvestingPrice.PROPERTY_PRODUIT_RECOLTE, species0.getLibelle_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, destination.getCode_destination_A(),
                RefHarvestingPrice.PROPERTY_DESTINATION, destination.getDestination(),
                RefHarvestingPrice.PROPERTY_ORGANIC, false,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, -1, //HarvestingPeriod.TOUTES
                RefHarvestingPrice.PROPERTY_CAMPAIGN, 2013,
                RefHarvestingPrice.PROPERTY_ACTIVE, true,
                RefHarvestingPrice.PROPERTY_PRICE, 180.0,
                RefHarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA
        );

        HarvestingActionValorisation mainValUser0 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpe.getCroppingPlanSpecies().getFirst().getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 0,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2013,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2015,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA);

        ArrayList<HarvestingActionValorisation> valorisations = Lists.newArrayList(mainValUser0);
        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> loadedPrices =
                referentialService.getRefHarvestingPricesForValorisations(valorisations, codeEspBotCodeQualiBySpeciesCode);

        List<RefHarvestingPrice> pricesForValorisation = loadedPrices.get(mainValUser0);
        Assertions.assertEquals(1, pricesForValorisation.size());
    }

    @Test
    public void testLoadOrganicRefHarvestingPrice() throws IOException {
        loadTestValues0();
        RefDestination destination = refDestinationDao.forCode_destination_AEquals("D0106").findUnique();
        CroppingPlanEntry cpe = croppingPlanEntryDao.forCodeEquals("CroppingPlanEntry-code-echalote-0").findAny();

        RefEspece species0 = cpe.getCroppingPlanSpecies().getFirst().getSpecies();
        refHarvestingPriceDao.create(
                RefHarvestingPrice.PROPERTY_CODE_SCENARIO, "",
                RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, species0.getCode_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, species0.getCode_qualifiant_AEE(),
                RefHarvestingPrice.PROPERTY_PRODUIT_RECOLTE, species0.getLibelle_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, destination.getCode_destination_A(),
                RefHarvestingPrice.PROPERTY_DESTINATION, destination.getDestination(),
                RefHarvestingPrice.PROPERTY_ORGANIC, false,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, -1, // HarvestingPeriod.TOUTES
                RefHarvestingPrice.PROPERTY_CAMPAIGN, 2013,
                RefHarvestingPrice.PROPERTY_ACTIVE, true,
                RefHarvestingPrice.PROPERTY_PRICE, 180.0,
                RefHarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA
        );

        refHarvestingPriceDao.create(
                RefHarvestingPrice.PROPERTY_CODE_SCENARIO, "",
                RefHarvestingPrice.PROPERTY_CODE_ESPECE_BOTANIQUE, species0.getCode_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_QUALIFIANT__AEE, species0.getCode_qualifiant_AEE(),
                RefHarvestingPrice.PROPERTY_PRODUIT_RECOLTE, species0.getLibelle_espece_botanique(),
                RefHarvestingPrice.PROPERTY_CODE_DESTINATION__A, destination.getCode_destination_A(),
                RefHarvestingPrice.PROPERTY_DESTINATION, destination.getDestination(),
                RefHarvestingPrice.PROPERTY_ORGANIC, true,
                RefHarvestingPrice.PROPERTY_MARKETING_PERIOD, -1, // HarvestingPeriod.TOUTES
                RefHarvestingPrice.PROPERTY_CAMPAIGN, 2013,
                RefHarvestingPrice.PROPERTY_ACTIVE, true,
                RefHarvestingPrice.PROPERTY_PRICE, 190.0,
                RefHarvestingPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA
        );

        HarvestingActionValorisation mainValUser0 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpe.getCroppingPlanSpecies(0).getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 0,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2013,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2015,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, true);

        Set<String> speciesCodes = Sets.newHashSet(mainValUser0.getSpeciesCode());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);
        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> loadedPrices =
                referentialService.getRefHarvestingPricesForValorisations(Lists.newArrayList(mainValUser0), codeEspBotCodeQualiBySpeciesCode);

        List<RefHarvestingPrice> pricesForValorisation = loadedPrices.get(mainValUser0);
        Assertions.assertEquals(1, pricesForValorisation.size());
        Assertions.assertTrue(pricesForValorisation.getFirst().isOrganic());

        HarvestingActionValorisation mainValUser1 = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, cpe.getCroppingPlanSpecies(0).getCode(),
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 0,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 3,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2013,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 2,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2015,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 8,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.TONNE_HA,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false);


        ArrayList<HarvestingActionValorisation> valorisations = Lists.newArrayList(mainValUser1);
        speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        loadedPrices =
                referentialService.getRefHarvestingPricesForValorisations(valorisations, codeEspBotCodeQualiBySpeciesCode);

        pricesForValorisation = loadedPrices.get(mainValUser1);
        Assertions.assertEquals(1, pricesForValorisation.size());
        Assertions.assertFalse(pricesForValorisation.getFirst().isOrganic());
    }

    @Disabled
    @Test
    public void testGetActaTraitementsProduitDtos() throws IOException {
        RefCountry france = testDatas.getDefaultFranceCountry();
        testDatas.importRefEspeces();
        testDatas.importActaTraitementsProduits();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaDosageSpc();
        testDatas.importAllActaSubstanceActive();

        // id_traitement : 140 (dosageSPC: 5 dosages 1 unité), (traitementProduitCateg: type intrant: 'Phytosanitaire', action:'application_de_produits_phytosanitaires'
        // id_produit: Abacus SP,Abilis,Abnakis,ACANTO,Acanto prima,ACAPELA 250 SC,Acarius,Acarius new,Acrobat M DG,Actiol,Addax,Addax DG,Adério,ADEXAR,Pyros EW,Opus,Fandango S,PROFILER,Kolthior,Thiovit jet microbilles,Arolle,Oceor Xpro

        PhytoProductInputFilter filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                ProductType.FUNGICIDAL,
                null,
                "Abacus SP",
                null,
                null,
                "2100040",
                "epoxiconazole",
                null,
                false);

        DomainPhytoProductInputSearchResults result = referentialService.getDomainPhytoProductInputSearchResults(filter);
        Assertions.assertNotNull(result);

        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);
        Assertions.assertNotNull(result);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNull();// more than one choice
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits()).containsExactlyInAnyOrder(PhytoProductUnit.G_HA, PhytoProductUnit.KG_HA, PhytoProductUnit.L_HA);// more than one choice
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(9);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(104);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(77);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(54);

        // Test witout country
        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);
        Assertions.assertNotNull(result);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(104);

        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                ProductType.FUNGICIDAL,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);
        Assertions.assertNotNull(result);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNull();// more than one choice
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits()).containsExactlyInAnyOrder(PhytoProductUnit.KG_HA, PhytoProductUnit.L_HA);// more than one choice
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(27);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(25);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(18);

        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                ProductType.FUNGICIDAL,
                null,
                "Acar",
                null,
                null,
                null,
                null,
                null,
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNull();// more than one choice
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits()).isEmpty();// 2 results but no product units found
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(2);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(2);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(1);// epoxiconazole

        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                ProductType.FUNGICIDAL,
                null,
                null,
                "Acarius",
                null,
                null,
                null,
                null,
                false);
        result = referentialService.getDomainPhytoProductInputSearchResults(filter);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNotEmpty();
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits().size()).isEqualTo(0);// 0 is this case
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(1);// epoxiconazole

        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                ProductType.FUNGICIDAL,
                null,
                null,
                "Acanto prima",
                null,
                null,
                null,
                null,
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNotEmpty();
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(2);// cyprodinil and picoxystrobine


        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                ProductType.FUNGICIDAL,
                null,
                "Acanto pr",
                null,
                null,
                null,
                null,
                null,
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNotEmpty();
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(2);// cyprodinil and picoxystrobine

        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                null,
                ProductType.FUNGICIDAL,
                null,
                null,
                null,
                null,
                null,
                null,
                "cyprodinil",
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNotEmpty();
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(1);// cyprodinil

        final List<RefEspece> refEspeces = refEspeceDao.forId_culture_actaEquals(18).findAll();
        List<String> refActaDosageSpc_id_culture = refEspeces.stream().map(TopiaEntity::getTopiaId).collect(Collectors.toList());
        filter = new PhytoProductInputFilter(
                AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                france.getTopiaId(),
                refActaDosageSpc_id_culture,
                ProductType.FUNGICIDAL,
                null,
                null,
                "Abacus SP",
                null,
                null,
                null,
                null,
                false);

        result = referentialService.getDomainPhytoProductInputSearchResults(filter);

        org.assertj.core.api.Assertions.assertThat(result.refActaTraitementsProduitId()).isNotEmpty();
        org.assertj.core.api.Assertions.assertThat(result.phytoProductUnits().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productTypes().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.productName().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.code_AMMs().size()).isEqualTo(1);
        org.assertj.core.api.Assertions.assertThat(result.activeSubstances().size()).isEqualTo(2);// cyprodinil

    }

    @Test
    public void findAllforCroppingPlanSpeciesIn() {
        RefEspece reA = refEspeceDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, "A",
                RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, "A",
                RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, "",
                RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, "",
                RefEspece.PROPERTY_CODE_DESTINATION__AEE, "",
                RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "A",
                RefSeedUnits.PROPERTY_ACTIVE, true
        );
        RefEspece reAB = refEspeceDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, "B",
                RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, "A",
                RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, "A",
                RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, "",
                RefEspece.PROPERTY_CODE_DESTINATION__AEE, "",
                RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "A",
                RefSeedUnits.PROPERTY_ACTIVE, true
        );
        RefEspece reB = refEspeceDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, "C",
                RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, "A",
                RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, "AB",
                RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, "",
                RefEspece.PROPERTY_CODE_DESTINATION__AEE, "",
                RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "AB",
                RefSeedUnits.PROPERTY_ACTIVE, true
        );
        RefEspece reBB = refEspeceDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, "D",
                RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, "B",
                RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, "B",
                RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, "",
                RefEspece.PROPERTY_CODE_DESTINATION__AEE, "",
                RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "BB",
                RefSeedUnits.PROPERTY_ACTIVE, true
        );

        refSeedUnitsDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, "RSU_0",
                TopiaEntity.PROPERTY_TOPIA_VERSION, 0L,
                RefSeedUnits.PROPERTY_CODE_ESPECE_BOTANIQUE, "A",
                RefSeedUnits.PROPERTY_CODE_QUALIFIANT__AEE, "",
                RefSeedUnits.PROPERTY_SEED_PLANT_UNIT, SeedPlantUnit.PLANTS_PAR_HA,
                RefSeedUnits.PROPERTY_ACTIVE, true
        );

        refSeedUnitsDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, "RSU_1",
                TopiaEntity.PROPERTY_TOPIA_VERSION, 0L,
                RefSeedUnits.PROPERTY_CODE_ESPECE_BOTANIQUE, "A",
                RefSeedUnits.PROPERTY_CODE_QUALIFIANT__AEE, "AB",
                RefSeedUnits.PROPERTY_SEED_PLANT_UNIT, SeedPlantUnit.UNITE_PAR_HA,
                RefSeedUnits.PROPERTY_ACTIVE, true
        );

        refSeedUnitsDao.create(
                TopiaEntity.PROPERTY_TOPIA_ID, "RSU_2",
                TopiaEntity.PROPERTY_TOPIA_VERSION, 0L,
                RefSeedUnits.PROPERTY_CODE_ESPECE_BOTANIQUE, "A",
                RefSeedUnits.PROPERTY_CODE_QUALIFIANT__AEE, "AC",
                RefSeedUnits.PROPERTY_SEED_PLANT_UNIT, SeedPlantUnit.G_PAR_M2,
                RefSeedUnits.PROPERTY_ACTIVE, true
        );

        List<SeedPlantUnit> seedPlantUnits = refSeedUnitsDao.findAllforRefEspeceIn(Lists.newArrayList(reA, reAB, reB, reBB));
        Assertions.assertTrue(seedPlantUnits.containsAll(List.of(SeedPlantUnit.PLANTS_PAR_HA, SeedPlantUnit.UNITE_PAR_HA)));
        Assertions.assertEquals(2, seedPlantUnits.size());
    }
}
