package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.services.performance.indicators.fertilization.AbstractIndicatorFertilizationTest;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorMineralFertilization;
import fr.inra.agrosyst.services.performance.indicators.fertilization.IndicatorOrganicFertilization;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class IndicatorSulfurFertilisationProductTest extends AbstractIndicatorFertilizationTest {

    @Test
    public void testFertilisationRealise_12393() {
        // Constitution du jeu de données de test
        List<DonneesTestFertilisationMineRealise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorQsaSoufreFertilisation = "QSA Soufre fertilisation";

        {
            var pair = createSulfonitrate();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 1500.0, 1.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, "360.0")
                    )
            ));
        }

        {
            var pair = createAmendementsEngraisAutres();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 15.0, 1.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, "3.15")
                    )
            ));
        }

        {
            var pair = createSoufreMagnesie();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 15.0, 5.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, "25.5")
                    )
            ));
        }

        {
            var pair = createMelangeGranulePK();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 15.0, 2.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, "3.6")
                    )
            ));
        }

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorSulfurFertilisationProduct indicatorSulfurFertilisationProduct = serviceFactory.newInstance(IndicatorSulfurFertilisationProduct.class);
        // Exécution des tests
        for (DonneesTestFertilisationMineRealise d : donneesTestFertilisation) {
            this.testFertilisationRealise(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.transitCount(),
                    d.expectedIndicatorNameAndValue(),
                    "Substances actives",
                    indicatorMineralFertilization,
                    indicatorSulfurFertilisationProduct
            );
        }
    }

    @Test
    public void testEpandageOrganiqueRealise_12393() {
        final String indicatorQsaSoufreFertilisation = "QSA Soufre fertilisation";

        final List<DonneesTestFertilisationOrgaRealise> donneesTestFertilisation = new LinkedList<>();
        final List<TestDataRealise> testDatas = List.of(
                new TestDataRealise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 1, "91.5", "75.0", "105.0", "0.0", "0.0", "75.0")
        );
        for (TestDataRealise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S(),
                    DomainOrganicProductInput.PROPERTY_ORGANIC, false
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationOrgaRealise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.transitCount(),
                    Map.of(
                            indicatorQsaSoufreFertilisation, testData.expectedS()
                    )
            ));
        }

        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        IndicatorSulfurFertilisationProduct indicatorSulfurFertilisationProduct = serviceFactory.newInstance(IndicatorSulfurFertilisationProduct.class);
        for (DonneesTestFertilisationOrgaRealise donneesTestFertilisationRealise : donneesTestFertilisation) {
            this.testEpandageOrganiqueRealise(
                    donneesTestFertilisationRealise,
                    "Substances actives",
                    indicatorOrganicFertilization,
                    indicatorSulfurFertilisationProduct);
        }
    }

    @Test
    public void testFertilisationSynthetise_12393() throws IOException {
        // Constitution du jeu de données de test
        List<DonneesTestFertilisationMineSynthetise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorQsaSoufreFertilisation = "QSA Soufre fertilisation";

        {
            var pair = createAmmonitrate();

            final double qtAvg1 = 150.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg1, 1.0, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, new IndicatorInfo("24.0", inputName))
                    )
            ));
            final double qtAvg2 = 500.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg2, 4.0, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, new IndicatorInfo("320.0", inputName))
                    )
            ));
            final double qtAvg3 = 15.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg3, 2.0, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, new IndicatorInfo("4.8", inputName))
                    )
            ));
        }

        {
            var pair = createSulfonitrate();

            final double qtAvg = 1500.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 3.5, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, new IndicatorInfo("1260.0", inputName))
                    )
            ));
        }

        {
            var pair = createAmendementsEngraisAutres();

            final double qtAvg = 15.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 2.33, 2.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, new IndicatorInfo("14.679", inputName))
                    )
            ));
        }

        {
            var pair = createSoufreMagnesie();

            final double qtAvg = 15.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 1.66, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, new IndicatorInfo("8.466", inputName))
                    )
            ));
        }

        {
            var pair = createMelangeGranulePK();

            final double qtAvg = 15.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 0.33, 5.0,
                    Map.ofEntries(
                            Map.entry(indicatorQsaSoufreFertilisation, new IndicatorInfo("2.97", inputName))
                    )
            ));
        }

        IndicatorMineralFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorMineralFertilization.class);
        IndicatorSulfurFertilisationProduct indicatorSulfurFertilisationProduct = serviceFactory.newInstance(IndicatorSulfurFertilisationProduct.class);
        for (DonneesTestFertilisationMineSynthetise d : donneesTestFertilisation) {
            this.testFertilisationSynthetise(
                    d.mineralProduct(),
                    d.domainMineralProductInput(),
                    d.qtAvg(),
                    d.spatialFrequency(),
                    d.temporalFrequency(),
                    d.expectedIndicatorNameAndInfo(),
                    "Substances actives",
                    indicatorMineralFertilization,
                    indicatorSulfurFertilisationProduct
            );
        }
    }

    @Test
    public void testEpandageOrganiqueSynthetise_12393() throws IOException {
        final String indicatorQsaSoufreFertilisation = "QSA Soufre fertilisation";

        final List<DonneesTestFertilisationOrgaSynthetise> donneesTestFertilisation = new LinkedList<>();
        final List<TestDataSynthetise> testDatas = List.of(
                new TestDataSynthetise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 1, "91.5", "75.0", "105.0", "0.0", "0.0", "75.0")
        );
        for (TestDataSynthetise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S(),
                    DomainOrganicProductInput.PROPERTY_ORGANIC, false
            );

            final String inputName = domainOrganicProductInput.getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationOrgaSynthetise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.temporalFrequency(),
                    inputName,
                    Map.of(
                            indicatorQsaSoufreFertilisation, testData.expectedS()
                    )));
        }

        IndicatorOrganicFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        IndicatorSulfurFertilisationProduct indicatorSulfurFertilisationProduct = serviceFactory.newInstance(IndicatorSulfurFertilisationProduct.class);
        for (DonneesTestFertilisationOrgaSynthetise testFertilisationOrgaSynthetise : donneesTestFertilisation) {
            this.testEpandageOrganiqueSynthetise(
                    testFertilisationOrgaSynthetise,
                    "Substances actives",
                    indicatorMineralFertilization,
                    indicatorSulfurFertilisationProduct
            );
        }
    }
}
