package fr.inra.agrosyst.services.edaplos;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.EquipmentTopiaDao;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GroundTopiaDao;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SeedPriceTopiaDao;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.ZoneType;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.QualityCriteria;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.edaplos.EdaplosImport;
import fr.inra.agrosyst.api.entities.edaplos.EdaplosImportTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnection;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceImpl;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.TypeCulture;
import fr.inra.agrosyst.api.entities.referential.VitesseCouv;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingResult;
import fr.inra.agrosyst.api.services.edaplos.EdaplosParsingStatus;
import fr.inra.agrosyst.api.services.edaplos.EdaplosResultLog;
import fr.inra.agrosyst.api.services.edaplos.EdaplosResultLog.EdaplosResultLevel;
import fr.inra.agrosyst.api.services.edaplos.EdaplosService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.domain.DomainServiceTest;
import fr.inra.agrosyst.services.effective.EffectiveCropCycleServiceTest;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.assertj.core.data.Offset;
import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class EdaplosServiceTest extends AbstractAgrosystTest {

    private static final Log LOGGER = LogFactory.getLog(EdaplosServiceTest.class);

    protected DomainService domainService;
    protected EdaplosServiceImpl edaplosService;
    protected PlotService plotService;
    protected ReferentialService referentialService;
    protected PricesService pricesService;
    protected TestDatas testDatas;
    protected DomainTopiaDao domainTopiaDao;
    protected PlotTopiaDao plotTopiaDao;
    protected ZoneTopiaDao zoneTopiaDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryTopiaDao;
    protected EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeTopiaDao;
    protected EffectiveCropCycleConnectionTopiaDao effectiveCropCycleConnectionTopiaDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleTopiaDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected AbstractActionTopiaDao abstractActionTopiaDao;
    protected MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao;
    protected SeedingActionUsageTopiaDao seedingActionUsageDao;
    protected HarvestingActionTopiaDao harvestingActionTopiaDao;
    protected InputPriceTopiaDao inputPriceDao;
    protected OtherActionTopiaDao otherActionDao;
    protected PesticidesSpreadingActionTopiaDao pesticidesSpreadingActionDao;

    protected PesticideProductInputUsageTopiaDao pesticideProductInputUsagesDao;
    protected EquipmentTopiaDao equipmentTopiaDao;
    protected ToolsCouplingTopiaDao toolsCouplingTopiaDao;
    protected GroundTopiaDao groundTopiaDao;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeTopiaDao;
    protected HarvestingPriceTopiaDao harvestingPriceTopiaDao;
    protected EdaplosImportTopiaDao edaplosImportTopiaDao;

    protected SeedPriceTopiaDao seedPriceTopiaDao;

    protected TopiaJpaSupport jpaSupport;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        domainService = serviceFactory.newService(DomainService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        plotService = serviceFactory.newService(PlotService.class);
        pricesService = serviceFactory.newService(PricesService.class);
        edaplosService = (EdaplosServiceImpl) serviceFactory.newService(EdaplosService.class);
        loginAsAdmin();
        alterSchema();

        domainTopiaDao = getPersistenceContext().getDomainDao();
        plotTopiaDao = getPersistenceContext().getPlotDao();
        croppingPlanEntryTopiaDao = getPersistenceContext().getCroppingPlanEntryDao();
        effectiveCropCycleNodeTopiaDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        effectiveCropCycleConnectionTopiaDao = getPersistenceContext().getEffectiveCropCycleConnectionDao();
        effectivePerennialCropCycleTopiaDao = getPersistenceContext().getEffectivePerennialCropCycleDao();
        effectiveSeasonalCropCycleTopiaDao = getPersistenceContext().getEffectiveSeasonalCropCycleDao();
        effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        abstractActionTopiaDao = getPersistenceContext().getAbstractActionDao();
        mineralFertilizersSpreadingActionDao = getPersistenceContext().getMineralFertilizersSpreadingActionDao();
        pesticidesSpreadingActionDao = getPersistenceContext().getPesticidesSpreadingActionDao();
        pesticideProductInputUsagesDao = getPersistenceContext().getPesticideProductInputUsageDao();
        harvestingActionTopiaDao = getPersistenceContext().getHarvestingActionDao();
        inputPriceDao = getPersistenceContext().getInputPriceDao();
        otherActionDao = getPersistenceContext().getOtherActionDao();
        harvestingPriceTopiaDao = getPersistenceContext().getHarvestingPriceDao();
        seedingActionUsageDao = getPersistenceContext().getSeedingActionUsageDao();

        equipmentTopiaDao = getPersistenceContext().getEquipmentDao();
        toolsCouplingTopiaDao = getPersistenceContext().getToolsCouplingDao();

        zoneTopiaDao = getPersistenceContext().getZoneDao();
        groundTopiaDao = getPersistenceContext().getGroundDao();
        effectiveSpeciesStadeTopiaDao = getPersistenceContext().getEffectiveSpeciesStadeDao();

        edaplosImportTopiaDao = getPersistenceContext().getEdaplosImportDao();

        seedPriceTopiaDao = getPersistenceContext().getSeedPriceDao();

        jpaSupport = getPersistenceContext().getJpaSupport();

        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.createTestNetworks();

        commonEdaplosReferentialImport();
    }

    protected void commonEdaplosReferentialImport() throws IOException {
        // referentials required for tests
        testDatas.importStadesEDI();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importRefEspeces();
        testDatas.importFertiOrga();
        testDatas.importAllRefInterventionTypeItemInputEdi();
        testDatas.importSolArvalis();
        testDatas.importCommunesFrance();
        testDatas.importCultureEdiGroupeCouvSol();
        testDatas.importRefEdaplosTypeTraitement();
        testDatas.importRefActaProduitRoot();
        testDatas.importDestination(true);
        testDatas.importSpeciesToSector(true);
        testDatas.importEspecesToVarietes(true);
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importNuisiblesEDI(true);
        testDatas.importQualityCriteria();
        testDatas.importEspeceOtherTools();
        testDatas.importFrTradRefIntrants();
        testDatas.createDefaultRefInputs();
    }

    protected void referenceDoseEdaplosReferentialImport() throws IOException {
        // dose de reference:
        testDatas.importAllActaDosageSpc();
        testDatas.importAllActaGroupeCultures();
    }

    protected void assertErrorMessage(EdaplosParsingResult result, String message) {
        assertLevelMessage(result, message, EdaplosResultLevel.ERROR, true);
    }

    protected void assertWarningMessage(EdaplosParsingResult result, String message) {
        assertLevelMessage(result, message, EdaplosResultLevel.WARNING, true);
    }

    protected void assertInfoMessage(EdaplosParsingResult result, String message) {
        assertLevelMessage(result, message, EdaplosResultLevel.INFO, true);
    }

    protected void assertInfoMessageAbsent(EdaplosParsingResult result, String message) {
        assertLevelMessage(result, message, EdaplosResultLevel.INFO, false);
    }

    protected void assertErrorMessageAbsent(EdaplosParsingResult result, String message) {
        assertLevelMessage(result, message, EdaplosResultLevel.ERROR, false);
    }

    protected void assertLevelMessage(EdaplosParsingResult result, String message, EdaplosResultLevel level, boolean present) {
        Set<EdaplosResultLog> messages = result.getMessages().stream()
                .filter(edaplosResultLog -> edaplosResultLog.getLevel() == level).collect(Collectors.toSet());

        Set<String> textMessages = result.getMessages().stream().map(EdaplosResultLog::getLabel).collect(Collectors.toSet());

        if (messages.stream().anyMatch(edaplosResultLog -> StringUtils.stripAccents(edaplosResultLog.getLabel().toUpperCase()).equals(StringUtils.stripAccents(message.toUpperCase())))) {
            Assertions.assertTrue(present);
        } else {
            StringBuilder receivedMessage = new StringBuilder();
            for (EdaplosResultLog edaplosResultLog : messages) {
                receivedMessage.append(edaplosResultLog.getLabel()).append("\n");
            }
            if (present && messages.isEmpty()) {
                LOGGER.error("Expected log level:" + level);
                for (EdaplosResultLevel edaplosResultLevel : EdaplosResultLevel.values()) {
                    if (!edaplosResultLevel.equals(level)) {
                        messages = result.getMessages().stream()
                                .filter(edaplosResultLog -> edaplosResultLog.getLevel() == edaplosResultLevel).collect(Collectors.toSet());
                        for (EdaplosResultLog edaplosResultLog : messages) {
                            LOGGER.error("For level '" + edaplosResultLevel + "':" + edaplosResultLog.getLabel());
                        }
                    }

                }

            } else if (present) {
                LOGGER.error("Expected a message with:'" + message + "' but got: '" + receivedMessage + "'");

            } else if (textMessages.stream().anyMatch(tmessage -> StringUtils.stripAccents(tmessage).equals(StringUtils.stripAccents(message.toUpperCase())))) {
                LOGGER.error("Expected not having message with:'" + message + "' and got: '" + receivedMessage + "'");
            }
            Assertions.assertFalse(present);
        }
    }

    /**
     * Test d'import du fichier au format eDaplos.
     * L'import ne se fait pas car le numéro de siret n'existe pas et la balise PartyContact est manquante
     */
    @Test
    public void testImportEdaplosMissingIssuerElement() {
        int persistedDomainNb = domainTopiaDao.findAll().size();

        // parse edaplos file
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_pb_siret_et_partycontact.zip");
        EdaplosParsingResult result = edaplosService.validEdaplosData(eDaplosStream, "eDaplos_pb_siret_et_partycontact");
        Assertions.assertEquals(EdaplosParsingStatus.FAIL, result.getEdaplosParsingStatus());
        assertErrorMessage(result, "Le SIRET du domaine est une information obligatoire or il est manquant dans le document");
        assertWarningMessage(result, "Aucune information trouvée concernant l'interlocuteur du domaine 'EARL ISAGRI (siret_a_compléter_1)'. Son nom sera 'À compléter'");

        // launch import, the persisted domain number must stay still
        eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_pb_siret_et_partycontact.zip");
        edaplosService.importEdaplos(eDaplosStream, "eDaplos_pb_siret_et_partycontact");
        List<Domain> persistedDomains = domainTopiaDao.findAll();
        Assertions.assertEquals(persistedDomainNb, persistedDomains.size());
    }

    @Test
    public void testTrim() {
        String t = " un chapeau bleu !! ";
        Assertions.assertEquals("unchapeaubleu!!", t.replaceAll(" ", ""));
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine au mauvais siret
     */
    @Test
    public void testImportEdaplosInvalidDomain() {
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_invalid_domain.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);

        Assertions.assertEquals(EdaplosParsingStatus.FAIL, result.getEdaplosParsingStatus());
        assertErrorMessage(result, "Le SIRET du domaine est une information obligatoire or il n'est pas valide dans le document.");

        Domain domain = domainTopiaDao.forSiretEquals("1234567890").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et d'un parcelle invalide.
     */
    @Test
    public void testImportEdaplosInvalidPlot() {
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_invalid_plot.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);

        //assertInfoMessage(result, "Le domaine ayant le numéro de siret :77556351300016, est à créer pour la campagne : 2001");
        String label = "La parcelle 'ID parcelle DAPLOS (14.79 ha)' sera créée sur le domaine 'EARL ISAGRI' pour la campagne '2001'";
        assertInfoMessage(result, label);
        // EdaplosParsingResult result, String label, EdaplosResultLevel level, String path
        EdaplosResultLog message = result.getMessages().stream()
                .filter(edaplosResultLog -> edaplosResultLog.getLevel() == EdaplosResultLevel.INFO)
                .filter(edaplosResultLog -> StringUtils.stripAccents(edaplosResultLog.getLabel().toUpperCase()).equals(StringUtils.stripAccents(label.toUpperCase())))
                .findFirst()
                .orElse(null);
        Assertions.assertNotNull(message);
        Assertions.assertEquals("Domaine : EARL ISAGRI - 2001 (siret = 77556351300016)", message.getPath().trim());

        assertWarningMessage(result, "La description du type de sol de la parcelle 'ID parcelle DAPLOS (14.79 ha)' du domaine 'EARL ISAGRI - 2001 (77556351300016)' est incomplète. Le type de sol ne sera donc pas importé.");
        assertWarningMessage(result, "La parcelle 'ID parcelle DAPLOS (14.79 ha)' du domaine 'EARL ISAGRI - 2001 (77556351300016)' ne contient aucune information sur les cultures, les ITK et le matériel. Veuillez vérifier les données exportées.");

        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        assertThat(plot.getName()).isEqualTo("ID parcelle DAPLOS");
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();
        assertThat(effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).count()).isEqualTo(0);
        assertThat(effectivePerennialCropCycleTopiaDao.forZoneEquals(zone).count()).isEqualTo(0);
    }

    /**
     * Test d'import du fichier au format eDaplos sur un domaine et parcelle existants.
     */
    @Test
    public void testImportEdaplosUpdateDomainAndPlot() {
        // 2014-10-15. eancelet : pour le moment ce test ne fait pas grand chose, ça me permet de voir les logs

        RefLocation location = referentialService.getActiveCommunes("01500").getFirst();
        Domain d1 = domainService.newDomain();
        d1.setName("Test eDaplos import");
        d1.setMainContact("eDaplos support team");
        d1.setSiret("77556351300016");
        d1.setType(DomainType.DOMAINE_EXPERIMENTAL);
        d1.setCampaign(2001);
        Domain domain = domainService.createOrUpdateDomain(d1, location.getTopiaId(), null, null, null, null, null, null, null, null, null, null, null);

        List<Domain> persistedDomains = domainTopiaDao.forProperties(Domain.PROPERTY_NAME, "Test eDaplos import", Domain.PROPERTY_CAMPAIGN, 2001).findAll();
        Assertions.assertEquals(1, persistedDomains.size());

        // create test plot
        Plot p1 = plotService.getPlot(null);
        p1.setName("Cimetierre/ 07 08");
        p1.setArea(14.79);
        List<Zone> plotZones = plotService.getPlotZones(p1);
        plotZones.getFirst().setArea(14.79);
        Plot plot = plotService.createOrUpdatePlot(p1, domain.getTopiaId(), location.getTopiaId(), null, null, null, null, null, null, null, plotZones, null);
        // plot.eDaplosIssuerId must be null
        Assertions.assertNull(plot.geteDaplosIssuerId());
        Date plotTopiaCreateDate = plot.getTopiaCreateDate();
        Assertions.assertNotNull(plotTopiaCreateDate);
        Assertions.assertEquals(0, plot.getTopiaVersion());

        List<Plot> persistedPlots = plotTopiaDao.forProperties(Plot.PROPERTY_NAME, "Cimetierre/ 07 08").findAll();
        Assertions.assertEquals(1, persistedPlots.size());
        Assertions.assertEquals(1, plotService.getPlotZones(plot).size());

        // import edaplos file
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOSV2.3_domain_and_plot_only.zip");
        edaplosService.validEdaplosData(eDaplosStream, "eDAPLOSV2.3_domain_and_plot_only");
        eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOSV2.3_domain_and_plot_only.zip");
        edaplosService.importEdaplos(eDaplosStream, "eDAPLOSV2.3_domain_and_plot_only");

        persistedDomains = domainTopiaDao.forProperties(Domain.PROPERTY_SIRET, "77556351300016", Domain.PROPERTY_CAMPAIGN, 2001).findAll();
        Assertions.assertEquals(1, persistedDomains.size());
        // the domain must have been updated
        Assertions.assertEquals(persistedDomains.getFirst().getTopiaVersion(), 0); //FIXME echatellier was 1
        // the domain name shouldn't have change
        Assertions.assertEquals("Test eDaplos import", persistedDomains.getFirst().getName());

        // some assert on persisted plots
        List<Plot> plots = plotService.findAllForDomain(domain);
        Assertions.assertEquals(1, plots.size());
        Assertions.assertEquals(14.79, plots.getFirst().getArea(), 0.00001);
        Assertions.assertEquals(plotTopiaCreateDate, plots.getFirst().getTopiaCreateDate());
        Assertions.assertEquals(0, plots.getFirst().getTopiaVersion());
        // the plot eDaplosId must not be set
        Assertions.assertNull(plots.getFirst().geteDaplosIssuerId());
        // the plot name shouldn't have change
        Assertions.assertEquals("Cimetierre/ 07 08", plots.getFirst().getName());

    }

    /**
     * Test d'import du fichier au format eDaplos sur un domaine (creation d'une parcelle non existante).
     */
    @Test
    public void testImportEdaplosCreateOnlyPlot() {

        // create test domain
        RefLocation location = referentialService.getActiveCommunes("01500").getFirst();
        Domain d1 = domainService.newDomain();
        d1.setName("Test eDaplos import");
        d1.setMainContact("eDaplos support team");
        d1.setSiret("77556351300016");
        d1.setType(DomainType.DOMAINE_EXPERIMENTAL);
        d1.setCampaign(2001);
        // TODO eancelet 2016-05-04 : ajouter des cultures
        Domain domain = domainService.createOrUpdateDomain(d1, location.getTopiaId(), null, null, null, null, null, null, null, null, null, null, null);

        List<Domain> persistedDomains = domainTopiaDao.forProperties(Domain.PROPERTY_SIRET, "77556351300016", Domain.PROPERTY_CAMPAIGN, 2001).findAll();
        Assertions.assertEquals(1, persistedDomains.size());
        Assertions.assertEquals(persistedDomains.getFirst().getTopiaVersion(), 0);

        // ploy should not exists at this step
        List<Plot> plots = plotService.findAllForDomain(domain);
        Assertions.assertEquals(0, plots.size());

        // import plot into previous domain
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOSV2.3_domain_and_plot_only.zip");
        edaplosService.validEdaplosData(eDaplosStream, "eDAPLOSV2.3_domain_and_plot_only");
        eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOSV2.3_domain_and_plot_only.zip");
        edaplosService.importEdaplos(eDaplosStream, "eDAPLOSV2.3_domain_and_plot_only");

        persistedDomains = domainTopiaDao.forProperties(Domain.PROPERTY_SIRET, "77556351300016", Domain.PROPERTY_CAMPAIGN, 2001).findAll();
        Assertions.assertEquals(1, persistedDomains.size());
        Assertions.assertEquals(persistedDomains.getFirst().getTopiaVersion(), 0); // FIXME echatellier was 1

        // some assert on persisted plots
        plots = plotService.findAllForDomain(domain);
        Assertions.assertEquals(1, plots.size());
        Assertions.assertEquals(14.79, plots.getFirst().getArea(), 0.00001);
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles.
     */
    @Test
    public void testImportEdaplosCreateDomainAndPlotAndSeasonalCycle() {

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_valid_domain_and_plot.zip");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        Assertions.assertEquals(EdaplosParsingStatus.FAIL, results.getEdaplosParsingStatus());
        assertInfoMessage(results, "La parcelle 'Cimetierre/ 07 08 (14.79 ha)' sera créée sur le domaine 'EARL ISAGRI' pour la campagne '2001'");
        assertErrorMessage(results, "Format du rang non valide (entier obligatoire) pour l'occupation du sol 'ID_occ_sol_inter', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)'");
        assertErrorMessage(results, "Format du rang non valide (entier obligatoire) pour l'occupation du sol 'occ_sol_perenne', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)'");

        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        assertThat(domain.getName()).isEqualTo("EARL ISAGRI");
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(3);
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        assertThat(plot.geteDaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        assertThat(plot.getName()).isEqualTo("Cimetierre/ 07 08");
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();

        // perennial
        EffectivePerennialCropCycle perennialCropCycle = effectivePerennialCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(perennialCropCycle).isNotNull();
        EffectiveCropCyclePhase phase = perennialCropCycle.getPhase();
        assertThat(phase.getEdaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        List<EffectiveIntervention> phaseIntervention = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();
        assertThat(phaseIntervention).hasSize(6);
        phaseIntervention.forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );

        // seasonal
        EffectiveSeasonalCropCycle seasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(seasonalCropCycle).isNotNull();
        Collection<EffectiveCropCycleNode> perennialNodes = seasonalCropCycle.getNodes();
        assertThat(perennialNodes).hasSize(1);
        List<EffectiveIntervention> nodeInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(perennialNodes.iterator().next()).findAll();
        assertThat(nodeInterventions).hasSize(9);
        nodeInterventions.forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );
        List<EffectiveCropCycleConnection> connections = effectiveCropCycleConnectionTopiaDao.findAllByEffectiveSeasonalCropCycle(seasonalCropCycle);
        assertThat(connections).hasSize(1);
        assertThat(connections.getFirst().getEdaplosIssuerId()).isEqualTo("ID_occ_sol_inter");

        // Actions
        List<AbstractAction> actions = abstractActionTopiaDao.findAll();
        assertThat(actions).hasSize(15);
        assertThat(mineralFertilizersSpreadingActionDao.count()).isEqualTo(5);

        assertThat(seedingActionUsageDao.count()).isEqualTo(6);
        assertThat(pesticidesSpreadingActionDao.count()).isEqualTo(2);
        List<HarvestingAction> harvestingActions = harvestingActionTopiaDao.findAll();
        assertThat(harvestingActions).hasSize(2);
        Assertions.assertEquals(YealdUnit.TONNE_HA, harvestingActions.getFirst().getValorisations().iterator().next().getYealdUnit());

        // re-import, nothing changed
        getCurrentTransaction().commit();
        eDaplosStream = DomainServiceTest.class.getResourceAsStream("/edaplos/eDaplos_valid_domain_and_plot.zip");
        edaplosService.loadAndSave(eDaplosStream);

        domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        assertThat(domain.getName()).isEqualTo("EARL ISAGRI");
        croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(3);
        plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        assertThat(plot.geteDaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        assertThat(plot.getName()).isEqualTo("Cimetierre/ 07 08");
        zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();

        // perennial
        perennialCropCycle = effectivePerennialCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(perennialCropCycle).isNotNull();
        phase = perennialCropCycle.getPhase();
        assertThat(phase.getEdaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        phaseIntervention = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();
        assertThat(phaseIntervention).hasSize(6);
        phaseIntervention.forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );

        // seasonal
        seasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(seasonalCropCycle).isNotNull();
        perennialNodes = seasonalCropCycle.getNodes();
        assertThat(perennialNodes).hasSize(1);
        nodeInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(perennialNodes.iterator().next()).findAll();
        assertThat(nodeInterventions).hasSize(9);
        connections = effectiveCropCycleConnectionTopiaDao.findAllByEffectiveSeasonalCropCycle(seasonalCropCycle);
        assertThat(connections).hasSize(1);
        assertThat(connections.getFirst().getEdaplosIssuerId()).isEqualTo("ID_occ_sol_inter");

        // Actions
        actions = abstractActionTopiaDao.findAll();
        assertThat(actions).hasSize(15);
        assertThat(mineralFertilizersSpreadingActionDao.count()).isEqualTo(5);
        assertThat(seedingActionUsageDao.count()).isEqualTo(6);
        assertThat(pesticidesSpreadingActionDao.count()).isEqualTo(2);
        harvestingActions = harvestingActionTopiaDao.findAll();
        assertThat(harvestingActions).hasSize(2);
        Assertions.assertEquals(YealdUnit.TONNE_HA, harvestingActions.getFirst().getValorisations().iterator().next().getYealdUnit());
    }


    /**
     * Test d'import du fichier au format eDaplos ou la CI est positionnée en
     * début de fichier (avant les cultures assolées). Il s'agit de vérifier que
     * ces infos sont bien traitées et les noeuds correctement liés.
     */
    @Test
    public void testImportEdaplosIntermediateCropAtFirst() {

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_exemple_with_intermediate_first.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        Assertions.assertEquals(EdaplosParsingStatus.FAIL, results.getEdaplosParsingStatus());
        //assertInfoMessage(results, "Le domaine ayant le numéro de siret :77556351300016, est à créer pour la campagne : 2001");
        assertInfoMessage(results, "La parcelle 'Cimetierre/ 07 08 (14.79 ha)' sera créée sur le domaine 'EARL ISAGRI' pour la campagne '2001'");

        assertErrorMessage(results, "Format du rang non valide (entier obligatoire) pour l'occupation du sol 'ID_occ_sol_inter', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)'");
        assertErrorMessage(results, "Format du rang non valide (entier obligatoire) pour l'occupation du sol 'occ_sol_perenne', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)'");

        assertWarningMessage(results, "La description du type de sol de la parcelle 'Cimetierre/ 07 08 (14.79 ha)' du domaine 'EARL ISAGRI - 2001 (77556351300016)' est incomplète. Le type de sol ne sera donc pas importé.");

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        assertThat(domain.getName()).isEqualTo("EARL ISAGRI");
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(3);
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        assertThat(plot.geteDaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        assertThat(plot.getName()).isEqualTo("Cimetierre/ 07 08");
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();

        // perennial
        EffectivePerennialCropCycle perennialCropCycle = effectivePerennialCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(perennialCropCycle).isNotNull();
        EffectiveCropCyclePhase phase = perennialCropCycle.getPhase();
        assertThat(phase.getEdaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        List<EffectiveIntervention> phaseInterventions = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();
        assertThat(phaseInterventions).hasSize(6);

        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(jpaSupport,
                effectiveInterventionDao,
                effectiveInterventionDao.forTopiaIdIn(
                                phaseInterventions.stream().map(EffectiveIntervention::getTopiaId).toList())
                        .findAll());


        // seasonal
        EffectiveSeasonalCropCycle seasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(seasonalCropCycle).isNotNull();
        Collection<EffectiveCropCycleNode> perennialNodes = seasonalCropCycle.getNodes();
        assertThat(perennialNodes).hasSize(1);
        List<EffectiveIntervention> nodeInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(perennialNodes.iterator().next()).findAll();
        assertThat(nodeInterventions).hasSize(9);

        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(jpaSupport, effectiveInterventionDao, nodeInterventions);

        List<EffectiveCropCycleConnection> connections = effectiveCropCycleConnectionTopiaDao.findAllByEffectiveSeasonalCropCycle(seasonalCropCycle);
        assertThat(connections).hasSize(1);
        assertThat(connections.getFirst().getEdaplosIssuerId()).isEqualTo("ID_occ_sol_inter");

        // Actions
        List<AbstractAction> actions = abstractActionTopiaDao.findAll();
        assertThat(actions).hasSize(15);
        assertThat(mineralFertilizersSpreadingActionDao.count()).isEqualTo(5);
        assertThat(pesticidesSpreadingActionDao.count()).isEqualTo(2);
        List<HarvestingAction> harvestingActions = harvestingActionTopiaDao.findAll();
        assertThat(harvestingActions).hasSize(2);
    }

    /**
     * Test d'import d'un .zip
     * Ce fichier contient deux xml. L'un n'est pas importé car il manque la campagne, l'autre est importé.
     * Mène à la création d'un domaine avec une parcelle sur lequel il y a un cycle assolé avec un node et une connection ainsi qu'une culture prérenne
     * Test d'un réimport qui ne doit rien modifier
     */
    @Test
    public void testImportEdaplosInvalidZipFolder() {

        // import domain with croppingplanentry, plot, cropcycles
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOSV2.3_valid_and_invalid.zip");
        EdaplosParsingResult results = edaplosService.validEdaplosData(eDaplosStream, "eDAPLOSV2.3_valid_and_invalid");

        Assertions.assertEquals(EdaplosParsingStatus.FAIL, results.getEdaplosParsingStatus());

        // FIXME rewrite it
        /*List<String> globalErrorMessages = results.getGlobalErrorMessages();
        Assertions.assertEquals(1, globalErrorMessages.size()); // il existe une campagne invalide, ceci est testé avant de parser les domaines
        Assertions.assertEquals(0, results.getGlobalInfoMessages().size());
        Assertions.assertEquals(1, results.getGlobalWarningMessages().size());
        Assertions.assertEquals(2, results.getEdomainsFromSiretAndCampaign().size());
        Assertions.assertEquals(EdaplosParsingStatus.FAIL, results.getEdaplosParsingStatus()); // dû à la parcelle sans campagne

        EdaplosDomainDto domainTest_invalid = results.getEdomainFromSiretAndCampaign("77556351300016",9999);
        // no campaign is replaced by 9999
        Assertions.assertEquals(EdaplosDomainImportStatus.CREATE, domainTest_invalid.getStatus());
        Assertions.assertEquals(EdaplosParsingStatus.FAIL, domainTest_invalid.getEdaplosParsingStatus());
        Assertions.assertEquals(1, domainTest_invalid.getEplots().size());
        Assertions.assertNotNull(domainTest_invalid.getDomain().getCode());

        EdaplosDomainDto domainTest_valid = results.getEdomainFromSiretAndCampaign("77556351300016",2001);
        Assertions.assertEquals(EdaplosDomainImportStatus.CREATE, domainTest_valid.getStatus());
        Assertions.assertEquals(0, domainTest_valid.getDomainErrorMessages().size());
        Assertions.assertEquals(1, domainTest_valid.getDomainInfoMessages().size());
        Assertions.assertNotNull(domainTest_valid.getDomain().getCode());
        Assertions.assertEquals(domainTest_invalid.getDomain().getCode(), domainTest_valid.getDomain().getCode());
        Assertions.assertEquals(3, domainTest_valid.getCroppingPlanEntries().size());
        // the fourth crop of type ABA01 is not imported
        Assertions.assertEquals(1, domainTest_valid.getEplots().size());
        EdaplosPlotDto plotTest = domainTest_valid.getEplots().iterator().next();
        Assertions.assertEquals(EdaplosParsingStatus.FAIL, plotTest.getEdaplosParsingStatus());
        Assertions.assertEquals(0, plotTest.getPlotErrorMessages().size());
        Assertions.assertEquals(1, plotTest.getPlotInfoMessages().size());
        Assertions.assertEquals("CREATE", plotTest.getStatus());
        Assertions.assertEquals(1, plotTest.getZones().size());
        Assertions.assertEquals(1,plotTest.getPerennialCycleDtos().size());
        Assertions.assertNotEquals(null, plotTest.getSeasonalCycleDto());
        Assertions.assertEquals(1, plotTest.getSeasonalCycleDto().getNodeDtos().size());
        // the OS associated de the crop of type ABA01 is not parsed
        Assertions.assertEquals(3, plotTest.getSoilOccErrorMessages().size());
        // il manque à ID la fenêtre temporelle + 1 action
        // il manque une action à 2599856659852365
        Assertions.assertEquals(11, plotTest.getSoilOccInfoMessages().size());
        // 1 + le nb d'interventions non importée car aucune n'a de qualifiant ou les qualifiants ne conviennent pas
        edaplosService.importEdaplos(results);
        // eancelet 2015-04-23 : ça ne remonte pas les exceptions lorsque des valeurs ne sont pas retrouvées dans les référentiels, POURQUOI ?
        Domain domain = domainDao.forProperties(Domain.PROPERTY_SIRET, "77556351300016", Domain.PROPERTY_CAMPAIGN, 2001).findUniqueOrNull();
        Assertions.assertEquals(null, domain);*/
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles.
     */
    @Test
    public void testImportEdaplosInraXmlFile() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOSV2.3_valid_exemple_inra_1.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        assertThat(domain.getName()).isEqualTo("EARL ISAGRI");
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(3);
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        assertThat(plot.geteDaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        assertThat(plot.getName()).isEqualTo("Cimetierre/ 07 08");
        assertThat(plot.getLatitude()).isEqualTo(47.902964);
        assertThat(plot.getLongitude()).isEqualTo(1.9092510000000402);
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();
        assertThat(zone.getLatitude()).isEqualTo(47.902964);
        assertThat(zone.getLongitude()).isEqualTo(1.9092510000000402);

        // Check ground
        Ground domainPlotGroud = groundTopiaDao.forDomainEquals(domain).findUniqueOrNull();
        Assertions.assertNotNull(domainPlotGroud);
        Assertions.assertEquals("ABF00", domainPlotGroud.getRefSolArvalis().getSol_calcaire_typeCode());
        Assertions.assertEquals("ABF13", domainPlotGroud.getRefSolArvalis().getSol_hydromorphie_typeCode());
        Assertions.assertEquals("ABF08", domainPlotGroud.getRefSolArvalis().getSol_pierrosite_typeCode());
        Assertions.assertEquals("ZGI", domainPlotGroud.getRefSolArvalis().getSol_profondeur_typeCode());
        Assertions.assertEquals("ABF04", domainPlotGroud.getRefSolArvalis().getSol_texture_typeCode());
        // On the plot
        Ground plotGroud = plot.getGround();
        Assertions.assertEquals("ABF00", plotGroud.getRefSolArvalis().getSol_calcaire_typeCode());
        Assertions.assertEquals("ABF13", plotGroud.getRefSolArvalis().getSol_hydromorphie_typeCode());
        Assertions.assertEquals("ABF08", plotGroud.getRefSolArvalis().getSol_pierrosite_typeCode());
        Assertions.assertEquals("ZGI", plotGroud.getRefSolArvalis().getSol_profondeur_typeCode());
        Assertions.assertEquals("ABF04", plotGroud.getRefSolArvalis().getSol_texture_typeCode());

        // Check Tools Coupling
        List<ToolsCoupling> domainToolsCouplings = toolsCouplingTopiaDao.forDomainEquals(domain).findAll();
        Assertions.assertEquals(1, domainToolsCouplings.size());
        List<Equipment> domainEquipments = equipmentTopiaDao.findAllByDomainOrdered(domain);
        Assertions.assertEquals(2, domainEquipments.size());
        ToolsCoupling domainToolCoupling = toolsCouplingTopiaDao.forDomainEquals(domain).findUniqueOrNull();
        assertThat(domainToolCoupling).isNotNull();

        // seasonal
        EffectiveSeasonalCropCycle seasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(seasonalCropCycle).isNotNull();
        Collection<EffectiveCropCycleNode> perennialNodes = seasonalCropCycle.getNodes();
        assertThat(perennialNodes).hasSize(1);
        List<EffectiveIntervention> nodeInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(perennialNodes.iterator().next()).findAll();
        assertThat(nodeInterventions).hasSize(11);
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                nodeInterventions);

        List<EffectiveCropCycleConnection> connections = effectiveCropCycleConnectionTopiaDao.findAllByEffectiveSeasonalCropCycle(seasonalCropCycle);
        assertThat(connections).hasSize(1);
        EffectiveCropCycleConnection effectiveCropCycleConnection = connections.getFirst();
        assertThat(effectiveCropCycleConnection.getEdaplosIssuerId()).isEqualTo("ID_occ_sol_inter");
        Assertions.assertEquals("Agrostide des chiens - Chiendent Velu", effectiveCropCycleConnection.getIntermediateCroppingPlanEntry().getName());

        // perennial
        EffectivePerennialCropCycle perennialCropCycle = effectivePerennialCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(perennialCropCycle).isNotNull();
        assertThat(perennialCropCycle.getPlantingYear()).isEqualTo(2011);
        EffectiveCropCyclePhase phase = perennialCropCycle.getPhase();
        assertThat(phase.getEdaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        List<EffectiveIntervention> phaseIntervention = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).findAll();
        assertThat(phaseIntervention).hasSize(0);

        // intervention
        EffectiveIntervention interventionWithTc = effectiveInterventionDao.forEdaplosIssuerIdEquals("2599856659852365").findUniqueOrNull();
        assertThat(interventionWithTc).isNotNull();
        Assertions.assertEquals("Intrant", interventionWithTc.getName());
        Assertions.assertEquals("PLAN PREVISIONNEL DE FUMURE (Campagne 07 08) sur Cimetiere / 07 08", interventionWithTc.getComment());
        Assertions.assertNotNull(interventionWithTc.getToolCouplings());
        Assertions.assertEquals(1, interventionWithTc.getToolCouplings().size());
        Assertions.assertNotNull(interventionWithTc.getToolCouplings().iterator().next().getTractor());
        Assertions.assertNotNull(interventionWithTc.getToolCouplings().iterator().next().getEquipments());
        Assertions.assertEquals(1, interventionWithTc.getToolCouplings().iterator().next().getEquipments().size());

        // intervention
        EffectiveIntervention interventionWithInput = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_TraItement 1").findUniqueOrNull();
        assertThat(interventionWithInput).isNotNull();
        // il y a aussi une action de récolte
        List<PesticidesSpreadingAction> interventionWithInputActions = pesticidesSpreadingActionDao.forEffectiveInterventionEquals(interventionWithInput).findAll();
        assertThat(interventionWithInputActions).hasSize(1);
        PesticidesSpreadingAction action = interventionWithInputActions.getFirst();
        assertThat(action.getPesticideProductInputUsages()).hasSize(2);
        assertWarningMessage(results, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', intervention 'Intervention (19/04/2008)', pour un intrant 'ZJK', impossible de trouver le produit correspondant au code AMM '9900430' ! Vous devrez rajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.");
        assertWarningMessage(results, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', intervention 'Intervention (19/04/2008)', pour un intrant 'ZQG', impossible de trouver le produit correspondant au code AMM '940076' ! Vous devrez rajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.");

        // Depuis le #10402, si le nom du traitement remonté du référentiel n'est pas le même que celui saisie dans eDaplos, il faut remonté l'information à l'utilisateur en Warning, en précisant le produit utilisé
        assertWarningMessage(results, "L'intrant déclaré sur la culture 'Blé Tendre d'hiver', intervention 'Intervention (19/04/2008)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' "
                + "n'a pas pu être retrouvé avec le nom fourni 'ATLANTIS'. Utilisation du produit 'Atlantis WG' - traitement 'Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)'.");

        // Actions
        List<AbstractAction> actions = abstractActionTopiaDao.findAll();
        assertThat(actions).hasSize(12);
        assertThat(mineralFertilizersSpreadingActionDao.count()).isEqualTo(4);
        assertThat(seedingActionUsageDao.count()).isEqualTo(7);
        assertThat(pesticidesSpreadingActionDao.count()).isEqualTo(1);
        assertThat(otherActionDao.count()).isEqualTo(0);
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles
     */
    @Test
    public void testImportEdaplosXmlFile() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOSV2.3_valid_exemple.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, result.getEdaplosParsingStatus());

        // test que le stade 'début de l'hinibition de la graine' a été correctement importé
        List<EffectiveSpeciesStade> speciesStades = effectiveSpeciesStadeTopiaDao.findAll();
        Assertions.assertTrue(speciesStades.stream()
                .filter(stade -> stade.getMinStade() != null)
                .anyMatch(stade -> stade.getMinStade().getAee().equals("06BBCH0000")));

        // test la surface d'intervention culturale
        EffectiveIntervention interventionSemis1 = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_semis 1").findUnique();
        assertThat(interventionSemis1.getSpatialFrequency()).isEqualTo(0.42);
    }

    @Test
    public void testImportEdaplos3TillageActions() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_example_with_3_tillage_actions.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, result.getEdaplosParsingStatus());
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles
     */
    @Test
    public void testImportEdaplosIntrantPhytoSanitaire() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_intrants_phyto.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // test application produit phyto
        EffectiveIntervention interventionPhyto = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        assertThat(interventionPhyto.getType()).isEqualTo(AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        LocalDate d = EdaplosUtils.parseInterventionDate("2008-04-24T00:00:00+02:00");
        Assertions.assertEquals(interventionPhyto.getStartInterventionDate(), d);
        assertThat(interventionPhyto.getSpatialFrequency()).isEqualTo(0.42);
        assertThat(interventionPhyto.getTransitCount()).isEqualTo(13);
        assertThat(interventionPhyto.getWorkRate()).isEqualTo(7);
        assertThat(interventionPhyto.getWorkRateUnit()).isEqualTo(MaterielWorkRateUnit.H_HA);
        PesticidesSpreadingAction actionPhyto = (PesticidesSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto).findUnique();
        assertThat(actionPhyto.getProportionOfTreatedSurface()).isEqualTo(51);
        Collection<PesticideProductInputUsage> pesticideProductInputUsages = actionPhyto.getPesticideProductInputUsages();
        assertThat(pesticideProductInputUsages).isNotEmpty();
        PesticideProductInputUsage pesticideProductInputUsage = pesticideProductInputUsages.iterator().next();
        DomainPhytoProductInput inputPhyto = pesticideProductInputUsage.getDomainPhytoProductInput();
        assertThat(inputPhyto.getRefInput()).isNotNull();
        assertThat(inputPhyto.getProductType()).isNotNull();
        assertThat(pesticideProductInputUsage.getQtAvg()).isEqualTo(1.44);
        assertThat(inputPhyto.getUsageUnit()).isEqualTo(PhytoProductUnit.L_HA);
        // caracteristique technique
        assertThat(actionPhyto.getBoiledQuantity()).isEqualTo(5);
        // test application produit phyto(price)
        InputPrice phytoInputPrice = inputPhyto.getInputPrice();
        assertThat(phytoInputPrice.getPrice()).isEqualTo(7000);
        assertThat(phytoInputPrice.getPriceUnit()).isEqualTo(PriceUnit.EURO_L);
        // cibles
        assertThat(pesticideProductInputUsage.getTargets()).hasSize(2);

        // test semis
        EffectiveIntervention interventionPhyto2 = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 2").findUnique();
        assertThat(interventionPhyto2.getType()).isEqualTo(AgrosystInterventionType.SEMIS);
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionPhyto2));
        SeedingActionUsage actionPhyto2 = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto2).findUnique();
        //SeedingProductInput inputPhyto2 = seedingProductInputTopiaDao.forSeedingActionEquals(actionPhyto2).findUniqueOrNull();
        Assertions.assertNotNull(actionPhyto2);
        List<SeedLotInputUsage> seedLotInputUsages = Lists.newArrayList(actionPhyto2.getSeedLotInputUsage());
        assertThat(seedLotInputUsages).hasSize(1);
        SeedLotInputUsage seedLotInputUsage = seedLotInputUsages.getFirst();
        List<SeedSpeciesInputUsage> seedingSpeciesUsages = Lists.newArrayList(seedLotInputUsage.getSeedingSpecies());
        assertThat(seedingSpeciesUsages).hasSize(1);
        SeedSpeciesInputUsage firstActionSpecies = seedingSpeciesUsages.getFirst();
        assertThat(firstActionSpecies.getQtAvg()).isEqualTo(18);
        DomainSeedLotInput domainSeedLotInput = seedLotInputUsage.getDomainSeedLotInput();
        DomainSeedSpeciesInput domainSeedSpeciesInput = firstActionSpecies.getDomainSeedSpeciesInput();
        assertThat(domainSeedLotInput.getUsageUnit()).isEqualTo(SeedPlantUnit.PLANTS_PAR_HA);
        assertThat(domainSeedSpeciesInput.isBiologicalSeedInoculation()).isFalse();
        assertThat(domainSeedSpeciesInput.isChemicalTreatment()).isTrue();
        Assertions.assertNotNull(domainSeedSpeciesInput.getSpeciesPhytoInputs());
        ArrayList<DomainPhytoProductInput> domainPhytoProductInputs = Lists.newArrayList(domainSeedSpeciesInput.getSpeciesPhytoInputs());
        DomainPhytoProductInput domainPhytoProductInput1 = domainPhytoProductInputs.getFirst();
        assertThat(domainPhytoProductInput1.getRefInput()).isNotNull();
        assertThat(domainPhytoProductInput1.getProductType()).isNotNull();
        Assertions.assertNotNull(firstActionSpecies.getSeedProductInputUsages());
        ArrayList<SeedProductInputUsage> seedProductInputUsages = Lists.newArrayList(firstActionSpecies.getSeedProductInputUsages());
        SeedProductInputUsage seedProductInputUsage = seedProductInputUsages.getFirst();
        assertThat(seedProductInputUsage.getQtAvg()).isNull();
        assertThat(domainPhytoProductInput1.getUsageUnit()).isNotNull();
        assertWarningMessage(result, "Le nombre de passage renseigné pour l'intervention 'Intervention (26/04/2008)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' n'est pas cohérent (cela doit être un nombre entier >=1). Prendra la valeur 1 par défaut.");

        // test lutte bio
        EffectiveIntervention interventionPhyto3 = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 3").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionPhyto3));
        assertThat(interventionPhyto3.getType()).isEqualTo(AgrosystInterventionType.LUTTE_BIOLOGIQUE);
        BiologicalControlAction actionPhyto3 = (BiologicalControlAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto3).findUnique();
        Collection<BiologicalProductInputUsage> biologicalProductInputUsages = actionPhyto3.getBiologicalProductInputUsages();
        assertThat(biologicalProductInputUsages).isNotEmpty();
        BiologicalProductInputUsage biologicalProductInputUsage = biologicalProductInputUsages.iterator().next();
        DomainPhytoProductInput domainPhytoProductInput = biologicalProductInputUsage.getDomainPhytoProductInput();
        assertThat(domainPhytoProductInput.getRefInput()).isNotNull();
        assertThat(domainPhytoProductInput.getProductType()).isNotNull();
        assertThat(biologicalProductInputUsage.getQtAvg()).isCloseTo(4.225, Percentage.withPercentage(0.1));
        assertThat(domainPhytoProductInput.getUsageUnit()).isEqualTo(PhytoProductUnit.L_HA);
        // test lutte bio (price)
        InputPrice bioInputPrice = domainPhytoProductInput.getInputPrice();
        assertThat(bioInputPrice).isNull();
    }

    /**
     * Evo #10640
     */
    @Test
    public void testImportEdaplosIntrantPhytoSanitaireWithOtherTypeThanRef() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/Evo10460-edaplos-produits_phyto_avec_types_differents_du_ref.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        EffectiveIntervention intervention = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 3").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(intervention));
        Assertions.assertNotEquals(AgrosystInterventionType.LUTTE_BIOLOGIQUE, intervention.getType());// ref #10491 #13: bien que le type d'intrant réel correspond à LUTTE_BIOLOGIQUE il a été décidé de conserver le type d'origine
        Assertions.assertEquals(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX, intervention.getType());

        // Messages auparavant renvoyés pour les produits avec un type de traitement non concordant avec le reférentiel
        assertInfoMessageAbsent(result, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', intervention 'Intervention (24/04/2008)', pour un intrant 'ZQG', impossible de trouver le produit correspondant au code AMM '2060078' ! Vous devrez rajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.");
        assertInfoMessageAbsent(result, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', intervention 'Intervention (26/04/2008)', pour un intrant 'ZIW', impossible de trouver le produit correspondant au code AMM '2010513' ! Vous devrez rajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.");
        assertInfoMessage(result, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', intervention 'Intervention (24/04/2008)', pour l'intrant dont le type eDaplos est 'ZQG', import du produit 'Mix-In' avec le traitement 'Divers - Divers - Huiles adjuvantes' issu d'agrosyst au lieu de celui fourni.");
        assertInfoMessage(result, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', intervention 'Intervention (26/04/2008)', pour l'intrant dont le type eDaplos est 'ZIW', import du produit 'DIPEL DF' avec le traitement 'Moyens biologiques - Préparations bactériennes' issu d'agrosyst au lieu de celui fourni.");
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles
     */
    @Test
    public void testImportEdaplosIntrantPhytoSanitaireError() throws IOException {
        testDatas.importAllActaTraitementsProduits(); // huge
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        referenceDoseEdaplosReferentialImport();

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_intrants_phyto_error.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(1);
        assertThat(croppingPlanEntries.getFirst().getName()).isEqualTo("Tournessol");

        // test application produit phyto
        EffectiveIntervention interventionPhyto = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionPhyto));
        assertThat(interventionPhyto.getType()).isEqualTo(AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        PesticidesSpreadingAction actionPhyto = (PesticidesSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto).findUnique();
        assertThat(actionPhyto.getProportionOfTreatedSurface()).isEqualTo(51);
        Assertions.assertNotNull(actionPhyto.getPesticideProductInputUsages());
        assertThat(actionPhyto.getPesticideProductInputUsages().size()).isEqualTo(1);
        List<PesticideProductInputUsage> pesticideProductInputUsages = new ArrayList<>(actionPhyto.getPesticideProductInputUsages());
        PesticideProductInputUsage inputPhyto = pesticideProductInputUsages.getFirst();
        DomainPhytoProductInput domainPhytoProductInput = inputPhyto.getDomainPhytoProductInput();
        assertThat(domainPhytoProductInput.getRefInput().getNom_produit()).isEqualTo("Amistar Top");
        assertThat(domainPhytoProductInput.getInputName()).isEqualTo("Amistar Top");

        // price
        InputPrice phytoInputPrice = domainPhytoProductInput.getInputPrice();
        assertThat(phytoInputPrice).isNotNull();
        assertThat(phytoInputPrice.getPrice()).isNull();
        assertInfoMessage(result, "Attention, le prix de l'intrant associé à l'intervention 'Intervention (24/04/2008)', occupation du sol 'Rang 1 (Tournessol)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' ne peut-être importé (soit l'unité de la dose associée ne correspond pas à l'unité de la dose de référence, soit l'intrant associé ne peut pas être importé). Il devra être complété manuellement après import.");

        // amm manquant
        // depuis le #10540, l'absence d'AMM provoque uniquement un warning
        assertWarningMessage(result, "L'intrant déclaré sur la culture 'Tournessol', intervention 'Intervention (24/04/2008)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' n'a pas de code d'identification (AMM) renseigné. Vous devrez ajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.");
    }

    /**
     * Test d'import du fichier avec conversion des unités des produits phyto : l/ha -> L/m²
     */
    @Test
    public void testImportEdaplosIntrantPhytoSanitaireConvUnits() throws IOException {
        testDatas.importAllActaTraitementsProduits(); // huge

        referenceDoseEdaplosReferentialImport();

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_intrants_phyto_conv_unit.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.FAIL);

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(1);
        assertThat(croppingPlanEntries.getFirst().getName()).isEqualTo("Concombre");

        // test application produit phyto
        EffectiveIntervention interventionPhyto = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionPhyto));
        assertThat(interventionPhyto.getType()).isEqualTo(AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        PesticidesSpreadingAction actionPhyto = (PesticidesSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto).findUnique();
        assertThat(actionPhyto.getProportionOfTreatedSurface()).isEqualTo(51);
        Collection<PesticideProductInputUsage> pesticideProductInputUsages = actionPhyto.getPesticideProductInputUsages();
        assertThat(pesticideProductInputUsages).isNotEmpty();
        PesticideProductInputUsage pesticideProductInputUsage = pesticideProductInputUsages.iterator().next();
        DomainPhytoProductInput domainPhytoProductInput = pesticideProductInputUsage.getDomainPhytoProductInput();

        assertThat(domainPhytoProductInput.getRefInput().getNom_produit()).isEqualTo("Elweiss Pro");
        assertThat(domainPhytoProductInput.getInputName()).isEqualTo("ELWEISS PRO (parties aériennes)"); // Depuis le #10402, le nom fourni en commentaire eDaplos est utilisé si différent du nom du produit phyto
        assertThat(pesticideProductInputUsage.getQtAvg()).isEqualTo(1.2345, Offset.offset(0.0001)); // convertit depuis 1.44
    }

    @Test
    public void testImportEdaplosIntrantEpandageOrganique() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_intrants_organique.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // test application produit phyto
        EffectiveIntervention interventionOrganic = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionOrganic));
        assertThat(interventionOrganic.getType()).isEqualTo(AgrosystInterventionType.EPANDAGES_ORGANIQUES);
        OrganicFertilizersSpreadingAction actionOrganic = (OrganicFertilizersSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionOrganic).findUnique();
        List<OrganicProductInputUsage> organicProductInputUsages = new ArrayList<>(actionOrganic.getOrganicProductInputUsages());
        assertThat(organicProductInputUsages.size()).isEqualTo(1);
        OrganicProductInputUsage inputUsage = organicProductInputUsages.getFirst();
        DomainOrganicProductInput domainOrganicProductInput = inputUsage.getDomainOrganicProductInput();
        assertThat(domainOrganicProductInput).isNotNull();
        assertThat(domainOrganicProductInput.getRefInput().getLibelle()).isEqualTo("Lisier de caprins");
        assertThat(domainOrganicProductInput.getUsageUnit()).isEqualTo(OrganicProductUnit.M_CUB_HA);
        assertThat(inputUsage.getQtAvg()).isCloseTo(3.380, Percentage.withPercentage(1));
        assertThat(domainOrganicProductInput.getK2O()).isEqualTo(2.475);
        assertThat(domainOrganicProductInput.getN()).isEqualTo(12);
        assertThat(domainOrganicProductInput.getP2O5()).isEqualTo(42);

        // price
        InputPrice inputPrice = domainOrganicProductInput.getInputPrice();
        assertThat(inputPrice.getPrice()).isNotNull();
        assertThat(inputPrice.getPrice()).isEqualTo(1234);
        assertThat(inputPrice.getPriceUnit()).isEqualTo(PriceUnit.EURO_M3);
    }

    @Test
    public void testImportEdaplosIntrantFertilisants() throws IOException {
        testDatas.importAllFertiMinUnifa(); // huge referential
        testDatas.importAllMateriels(); // huge referential
        testDatas.importRefActaProduitRoot(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_intrants_fertilisant.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // test application produit phyto
        EffectiveIntervention interventionOrganic = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        assertThat(interventionOrganic.getType()).isEqualTo(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        MineralFertilizersSpreadingAction actionMineral = (MineralFertilizersSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionOrganic).findUnique();
        assertThat(actionMineral.getMineralProductInputUsages()).isNotEmpty();
        List<MineralProductInputUsage> inputMineral = new ArrayList<>(actionMineral.getMineralProductInputUsages());
        MineralProductInputUsage inputMineral1 = inputMineral.getFirst();
        MineralProductInputUsage inputMineral2 = inputMineral.get(1);

        // first one
        DomainMineralProductInput domainMineralProductInputMineral1 = inputMineral1.getDomainMineralProductInput();
        assertThat(domainMineralProductInputMineral1).isNotNull();
        RefFertiMinUNIFA refInputMineral1 = domainMineralProductInputMineral1.getRefInput();


        assertThat(domainMineralProductInputMineral1.getUsageUnit()).isEqualTo(MineralProductUnit.L_HA);
        assertThat(inputMineral1.getQtAvg()).isCloseTo(3.380, Percentage.withPercentage(1));
        assertThat(refInputMineral1).isNotNull();
        assertThat(refInputMineral1.getType_produit()).isEqualTo("AUTRES");
        assertThat(refInputMineral1.getBore()).isEqualTo(8.0);
        assertThat(refInputMineral1.getCalcium()).isEqualTo(4.0);
        assertThat(refInputMineral1.getCuivre()).isEqualTo(9.0);
        assertThat(refInputMineral1.getFer()).isEqualTo(10.0);
        assertThat(refInputMineral1.getK2O()).isEqualTo(6.0);
        assertThat(refInputMineral1.getMgO()).isEqualTo(5.0);
        assertThat(refInputMineral1.getManganese()).isEqualTo(11.0);
        assertThat(refInputMineral1.getMolybdene()).isEqualTo(13.0);
        assertThat(refInputMineral1.getN()).isEqualTo(3.0);
        assertThat(refInputMineral1.getOxyde_de_sodium()).isEqualTo(7.0);
        assertThat(refInputMineral1.getP2O5()).isEqualTo(1.0);
        assertThat(refInputMineral1.getsO3()).isEqualTo(2.0);
        assertThat(refInputMineral1.getZinc()).isEqualTo(12.0);

        // price of first one
        String mineralPriceId1 = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refInputMineral1);
        InputPrice phytoInputPrice = inputPriceDao.forObjectIdEquals(mineralPriceId1).findUniqueOrNull();
        assertThat(phytoInputPrice).isNull();

        // second one
        DomainMineralProductInput domainMineralProductInputMineral2 = inputMineral2.getDomainMineralProductInput();
        assertThat(domainMineralProductInputMineral2).isNotNull();
        RefFertiMinUNIFA refInputMineral2 = domainMineralProductInputMineral2.getRefInput();

        assertThat(domainMineralProductInputMineral2.getUsageUnit()).isEqualTo(MineralProductUnit.KG_HA);
        assertThat(inputMineral2.getQtAvg()).isCloseTo(3.448, Percentage.withPercentage(1));
        assertThat(inputMineral2.getDomainMineralProductInput().getInputName()).isEqualTo("Chaux vive in test");
        assertThat(refInputMineral2).isNotNull();
        assertThat(refInputMineral2.getType_produit()).isEqualTo("CHAUX");
        assertThat(refInputMineral2.getCalcium()).isEqualTo(45);
        assertThat(refInputMineral2.getMgO()).isEqualTo(5);
        assertThat(refInputMineral2.getFer()).isEqualTo(0d);

        // price of second one
        String mineralPriceId2 = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refInputMineral2);
        InputPrice phytoInputPrice2 = inputPriceDao.forObjectIdEquals(mineralPriceId2).findUniqueOrNull();
        assertThat(phytoInputPrice2.getPrice()).isEqualTo(4321);
        assertThat(phytoInputPrice2.getPriceUnit()).isEqualTo(PriceUnit.EURO_KG);
    }

    @Test
    public void testImportEdaplosIntrantFertilisantsAnno10465() throws IOException {
        testDatas.importAllFertiMinUnifa(); // huge referential
        testDatas.importAllMateriels(); // huge referential
        testDatas.importRefActaProduitRoot(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOS_Anno_10465_mauvais_dosage_engrais.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // test application produit phyto
        EffectiveIntervention interventionOrganic = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionOrganic));
        assertThat(interventionOrganic.getType()).isEqualTo(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        MineralFertilizersSpreadingAction actionMineral = (MineralFertilizersSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionOrganic).findUnique();
        assertThat(actionMineral.getMineralProductInputUsages()).isNotEmpty();
        List<MineralProductInputUsage> inputMineral = new ArrayList<>(actionMineral.getMineralProductInputUsages());
        MineralProductInputUsage inputMineral1 = inputMineral.get(1);
        MineralProductInputUsage inputMineral2 = inputMineral.get(2);

        // first one
        DomainMineralProductInput domainMineralProductInputMineral1 = inputMineral1.getDomainMineralProductInput();
        assertThat(domainMineralProductInputMineral1).isNotNull();
        RefFertiMinUNIFA refInputMineral1 = domainMineralProductInputMineral1.getRefInput();

        assertThat(refInputMineral1).isNotNull();
        assertThat(domainMineralProductInputMineral1.getUsageUnit()).isEqualTo(MineralProductUnit.KG_HA);
        assertThat(inputMineral1.getQtAvg()).isCloseTo(312, Percentage.withPercentage(1));
        assertThat(inputMineral1.getDomainMineralProductInput().getInputName()).isEqualTo("15-05-20");
        assertThat(refInputMineral1.getType_produit()).isEqualTo("AUTRES");
        assertThat(refInputMineral1.getBore()).isEqualTo(0.0);
        assertThat(refInputMineral1.getCalcium()).isEqualTo(0.0);
        assertThat(refInputMineral1.getCuivre()).isEqualTo(0.0);
        assertThat(refInputMineral1.getFer()).isEqualTo(0.0);
        assertThat(refInputMineral1.getK2O()).isEqualTo(20.0);
        assertThat(refInputMineral1.getMgO()).isEqualTo(0.0);
        assertThat(refInputMineral1.getManganese()).isEqualTo(0.0);
        assertThat(refInputMineral1.getMolybdene()).isEqualTo(0.0);
        assertThat(refInputMineral1.getN()).isEqualTo(15.0);
        assertThat(refInputMineral1.getOxyde_de_sodium()).isEqualTo(0.0);
        assertThat(refInputMineral1.getP2O5()).isEqualTo(5.0);
        assertThat(refInputMineral1.getsO3()).isEqualTo(0.0);
        assertThat(refInputMineral1.getZinc()).isEqualTo(0.0);

        // second one
        DomainMineralProductInput domainMineralProductInputMineral2 = inputMineral2.getDomainMineralProductInput();
        assertThat(domainMineralProductInputMineral2).isNotNull();
        RefFertiMinUNIFA refInputMineral2 = domainMineralProductInputMineral2.getRefInput();

        assertThat(domainMineralProductInputMineral2.getUsageUnit()).isEqualTo(MineralProductUnit.KG_HA);
        assertThat(inputMineral2.getQtAvg()).isCloseTo(3.448, Percentage.withPercentage(1));
        assertThat(inputMineral2.getDomainMineralProductInput().getInputName()).isEqualTo("Chaux vive in test");
        assertThat(refInputMineral2.getType_produit()).isEqualTo("CHAUX");
        assertThat(refInputMineral2.getCalcium()).isEqualTo(45);
        assertThat(refInputMineral2.getMgO()).isEqualTo(5);
        assertThat(refInputMineral2.getFer()).isEqualTo(0d);

        // price of second one
        String mineralPriceId2 = InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(refInputMineral2);
        InputPrice phytoInputPrice2 = inputPriceDao.forObjectIdEquals(mineralPriceId2).findUniqueOrNull();
        assertThat(phytoInputPrice2.getPrice()).isEqualTo(4321);
        assertThat(phytoInputPrice2.getPriceUnit()).isEqualTo(PriceUnit.EURO_KG);
    }

    @Test
    public void testImportEdaplosIntrantFertilisantWithMissingComposition() throws IOException {
        testDatas.importAllFertiMinUnifa(); // huge referential
        testDatas.importAllMateriels(); // huge referential
        testDatas.importRefActaProduitRoot(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_intrants_fertilisant_missing_compo.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);//Change since #10520 : just return a warning with a default value for Nitrogen

        assertWarningMessage(result, "La composition chimique de l'intrant '2060118', intervention 'Intervention (24/04/2008)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' est manquante. L'intrant sera importé avec une teneur par défaut de 0,999% pour tous les éléments chimiques. Veuillez corriger la composition chimique après import.");
        assertWarningMessage(result, "La composition chimique de l'intrant 'Chaux vive in test (234567890)', intervention 'Intervention (24/04/2008)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' est manquante. L'intrant sera importé avec une teneur par défaut de 0,999% pour tous les éléments chimiques. Veuillez corriger la composition chimique après import.");

        // test application produit phyto
        EffectiveIntervention interventionOrganic = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionOrganic));
        assertThat(interventionOrganic.getType()).isEqualTo(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        MineralFertilizersSpreadingAction actionMineral = (MineralFertilizersSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionOrganic).findUnique();
        Collection<MineralProductInputUsage> inputMineral = actionMineral.getMineralProductInputUsages();
        MineralProductInputUsage inputMineral1 = inputMineral.iterator().next();

        // first one
        DomainMineralProductInput domainMineralProductInput = inputMineral1.getDomainMineralProductInput();
        RefFertiMinUNIFA mineralProduct = domainMineralProductInput.getRefInput();

        assertThat(domainMineralProductInput.getUsageUnit()).isEqualTo(MineralProductUnit.L_HA);
        assertThat(inputMineral1.getQtAvg()).isCloseTo(3.380, Percentage.withPercentage(1));
        assertThat(mineralProduct).isNotNull();
        assertThat(mineralProduct.getType_produit()).isEqualTo("AUTRES");
        assertThat(mineralProduct.getBore()).isEqualTo(0.999);
        assertThat(mineralProduct.getCalcium()).isEqualTo(0.999);
        assertThat(mineralProduct.getCuivre()).isEqualTo(0.999);
        assertThat(mineralProduct.getFer()).isEqualTo(0.999);
        assertThat(mineralProduct.getK2O()).isEqualTo(0.999);
        assertThat(mineralProduct.getMgO()).isEqualTo(0.999);
        assertThat(mineralProduct.getManganese()).isEqualTo(0.999);
        assertThat(mineralProduct.getMolybdene()).isEqualTo(0.999);
        assertThat(mineralProduct.getN()).isEqualTo(0.999);
        assertThat(mineralProduct.getOxyde_de_sodium()).isEqualTo(0.999);
        assertThat(mineralProduct.getP2O5()).isEqualTo(0.999);
        assertThat(mineralProduct.getsO3()).isEqualTo(0.999);
        assertThat(mineralProduct.getZinc()).isEqualTo(0.999);
    }

    @Test
    public void testImportEdaplosIntrantIrrigation() throws IOException {
        testDatas.importAllFertiMinUnifa(); // huge referential
        testDatas.importAllVarieteGeves(); // huge referential
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_intrants_irrigation.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // test application produit phyto
        EffectiveIntervention interventionOrganic = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionOrganic));
        assertThat(interventionOrganic.getType()).isEqualTo(AgrosystInterventionType.IRRIGATION);
        IrrigationAction actionIrrigation = (IrrigationAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionOrganic).findUnique();
        assertThat(actionIrrigation.getComment()).isEqualTo("Commentaire action irrigation");
        assertThat(actionIrrigation.getWaterQuantityAverage()).isEqualTo(789);

        IrrigationInputUsage irrigationInputUsage = actionIrrigation.getIrrigationInputUsage();
        assertThat(irrigationInputUsage).isNotNull();
        assertThat(irrigationInputUsage.getDomainIrrigationInput()).isNotNull();
        assertThat(irrigationInputUsage.getQtAvg()).isEqualTo(789);

    }

    @Test
    public void testImportEdaplosHarvesting() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/harvesting/eDAPLOSV2.3_harvesting.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(1);
        CroppingPlanEntry croppingPlanEntry = croppingPlanEntries.getFirst();
        assertThat(croppingPlanEntry.getCroppingPlanSpecies()).hasSize(1);
        CroppingPlanSpecies croppingPlanSpecies = croppingPlanEntry.getCroppingPlanSpecies().getFirst();
        assertThat(croppingPlanSpecies).isNotNull();

        // test application produit phyto
        EffectiveIntervention interventionOrganic = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                List.of(interventionOrganic));
        assertThat(interventionOrganic.getType()).isEqualTo(AgrosystInterventionType.RECOLTE);
        HarvestingAction actionHarvesting = (HarvestingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionOrganic).findUniqueOrNull();
        Collection<HarvestingActionValorisation> harvestingValorisations = actionHarvesting.getValorisations();

        // first
        Iterator<HarvestingActionValorisation> iterator = harvestingValorisations.iterator();
        HarvestingActionValorisation firstValorisation = iterator.next();
        assertThat(firstValorisation.getDestination().getDestination()).isEqualTo(ActionService.DEFAULT_DESTINATION_NAME);
        assertThat(firstValorisation.getDestination().getYealdUnit()).isEqualTo(YealdUnit.TONNE_HA);
        assertThat(firstValorisation.getDestination().getSector()).isEqualTo(Sector.GRANDES_CULTURES);
        assertThat(firstValorisation.getYealdAverage()).isCloseTo(0.98, Percentage.withPercentage(1));
        assertThat(firstValorisation.getYealdUnit()).isEqualTo(YealdUnit.TONNE_HA);
        QualityCriteria humiditeCriteria = firstValorisation.getQualityCriteria().iterator().next();
        assertThat(humiditeCriteria.getQuantitativeValue()).isEqualTo(88); // 100 - 12

        // price
        final String firstValorisationPriceId = PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(croppingPlanSpecies, firstValorisation.getDestination());
        HarvestingPrice phytoInputPrice1 = harvestingPriceTopiaDao.forObjectIdEquals(firstValorisationPriceId)
                .addEquals(HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, firstValorisation).findUniqueOrNull();
        assertThat(phytoInputPrice1.getPrice()).isNull();

        // second
        HarvestingActionValorisation secondValorisation = iterator.next();
        assertThat(secondValorisation.getDestination().getDestination()).isEqualTo(ActionService.DEFAULT_DESTINATION_NAME);
        assertThat(secondValorisation.getDestination().getYealdUnit()).isEqualTo(YealdUnit.KG_M2);
        assertThat(secondValorisation.getDestination().getSector()).isEqualTo(Sector.GRANDES_CULTURES);
        assertThat(secondValorisation.getYealdAverage()).isEqualTo(14.5);
        assertThat(secondValorisation.getYealdUnit()).isEqualTo(YealdUnit.KG_M2);

        assertInfoMessage(result, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', intervention 'Intervention (24/04/2008)', le SubordinateTypeCode 'ZJ4' n'est pas géré par Agrosyst. Contacter l'équipe Agrosyst pour plus d'informations.");

        // price
        String secondValorisationPriceId = PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(croppingPlanSpecies, secondValorisation.getDestination());
        HarvestingPrice phytoInputPrice2 = harvestingPriceTopiaDao.forObjectIdEquals(secondValorisationPriceId)
                .addEquals(HarvestingPrice.PROPERTY_HARVESTING_ACTION_VALORISATION, secondValorisation).findUniqueOrNull();
        assertThat(phytoInputPrice2.getPrice()).isEqualTo(55);
        assertThat(phytoInputPrice2.getPriceUnit()).isEqualTo(PriceUnit.EURO_KG);

        // test message
        assertWarningMessage(result, "Aucun rendement déclaré dans l'intervention de récolte 'Intervention (01/01/2001)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)'. La destination '" + ActionService.DEFAULT_DESTINATION_NAME + "' est mise par défaut avec un rendement de 9999t/ha. A corriger.");
    }

    @Test
    public void testImportEdaplosVariety() throws IOException {
        testDatas.importAllVarieteGeves(); // huge referential

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/crop/eDAPLOSV2.3_variety.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.FAIL);

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        assertThat(domain.getName()).isEqualTo("EARL ISAGRI");
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(3);
        // crop with variety = 512 C447
        CroppingPlanEntry croppingPlanEntry = croppingPlanEntries.getFirst();
        assertThat(croppingPlanEntry.getCroppingPlanSpecies()).hasSize(1);
        CroppingPlanSpecies croppingPlanSpecies = croppingPlanEntry.getCroppingPlanSpecies().getFirst();
        assertThat(((RefVarieteGeves) croppingPlanSpecies.getVariety()).getDenomination()).isEqualTo("Aerobic");
        // crop with variety = 1020394 (via num dossier)
        CroppingPlanEntry croppingPlanEntry2 = croppingPlanEntries.get(1);
        assertThat(croppingPlanEntry2.getCroppingPlanSpecies()).hasSize(1);
        CroppingPlanSpecies croppingPlanSpecies2_1 = croppingPlanEntry2.getCroppingPlanSpecies().getFirst();
        assertThat(((RefVarieteGeves) croppingPlanSpecies2_1.getVariety()).getDenomination()).isEqualTo("Chevron");

        // test message
        assertWarningMessage(result, "L'occupation du sol ayant pour identifiant : 'soil occupation id 3', possède une variété '123456789', '?' dont le nom n'est pas présent dans le référentiel variété d'Agrosyst. Vous devrez completer la variété de l'espèce Blé Tendre d'hiver");
    }

    @Test
    public void testImportEdaplosVarietyAsCsv() throws IOException {
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/crop/eDAPLOSV2.3_variety.xml");
        InputStream export = edaplosService.exportCSVEdaplosReport(eDaplosStream, "eDAPLOSV2.3_variety");
        StringWriter writer = new StringWriter();
        IOUtils.copy(export, writer, StandardCharsets.UTF_8);
        String theString = writer.toString();

        assertThat(theString).contains("Level;Message");
        assertThat(theString).contains("L'occupation du sol ayant pour identifiant : 'soil occupation id 3', possède une variété '123456789', '?' dont le nom n'est pas présent dans le référentiel variété d'Agrosyst");
        assertThat(theString).contains("INFO;L'occupation du sol ayant pour identifiant");
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles.
     */
    @Test
    public void testImportEdaplosWithErrorsInPlot() throws IOException {
        testDatas.importAllMateriels(); // huge referential

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_example_with_plot_error.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        Assertions.assertEquals(EdaplosParsingStatus.FAIL, results.getEdaplosParsingStatus());

        assertErrorMessage(results, "Sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', l'intervention 'Intrant (07/03/2008)' a un outil sans identifiant. Import impossible.");
        assertWarningMessage(results, "Certains matériels ne sont pas reconnus dans Agrosyst. Code du matériel : ''. Descriptif du matériel : 'PULVERISATEURS AUTOMOTEURS - Capacité 170 ch'. Veuillez contacter l'équipe Agrosyst pour les ajouter.");
        assertWarningMessage(results, "L'intervention 'Intrant (07/03/2008)' avec comme edaplosIssuerId '2599856659852365' réalisée sur occupation du sol 'Rang 1 (Blé Tendre d'hiver)',  la parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' comporte du matériel non reconnu. Aucune combinaison d'outils sera associée à cette intervention.");

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();
    }

    /**
     * Test validant l'unicité des id d'interventions sur importeDaplos.
     */
    @Test
    public void testImportEdaplosXmlWithDuplicatedInterventionId() {
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_exemple_with_duplicated_intervention_id.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.FAIL, results.getEdaplosParsingStatus());
        assertErrorMessage(results, "Les identifiants eDaplos suivants et correspondant à des interventions ne sont pas uniques dans le fichier d'import : ID_intervention. Cela n'est pas autorisé dans le format eDaplos, merci de contacter le support de votre outil d'export eDaplos.");
    }

    /**
     * Test validant l'unicité d'un type d'action au sein d'une intervention
     * lors de l'import eDaplos.
     */
    @Test
    public void testImportEdaplosXmlWithDuplicatedActionTypeInIntervention() {

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_exemple_with_action_error.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        assertErrorMessage(results, "L'intervention 'Intervention (13/10/2007)' portant sur occupation du sol 'Rang 1 (Blé Tendre d'hiver)' parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' a plusieurs actions de même type 'SEMIS' ce qui n'est pas autorisé dans Agrosyst. Import impossible");

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        assertThat(domain.getName()).isEqualTo("EARL ISAGRI");
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(2);
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        assertThat(plot.geteDaplosIssuerId()).isEqualTo("ID parcelle DAPLOS");
        assertThat(plot.getName()).isEqualTo("Cimetierre/ 07 08");
        assertThat(plot.getLatitude()).isEqualTo(47.902964);
        assertThat(plot.getLongitude()).isEqualTo(1.9092510000000402);
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();
        assertThat(zone.getType()).isEqualTo(ZoneType.PRINCIPALE);
        assertThat(zone.getLatitude()).isEqualTo(47.902964);
        assertThat(zone.getLongitude()).isEqualTo(1.9092510000000402);

        // seasonal
        EffectiveSeasonalCropCycle seasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(seasonalCropCycle).isNotNull();
        Collection<EffectiveCropCycleNode> perennialNodes = seasonalCropCycle.getNodes();
        assertThat(perennialNodes).hasSize(1);
        List<EffectiveIntervention> nodeInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(perennialNodes.iterator().next()).findAll();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                nodeInterventions);
        assertThat(nodeInterventions).hasSize(10);

        // intervention
        EffectiveIntervention effectiveIntervention = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_semis 1").findUniqueOrNull();
        assertThat(effectiveIntervention).isNotNull();
        List<AbstractAction> actions = abstractActionTopiaDao.forEffectiveInterventionEquals(effectiveIntervention).findAll();
        assertThat(actions).hasSize(1); // Un seul : le second n'a pas été gardé car duplicat du type

    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles.
     */
    @Test
    public void testImportEdaplosInvalidDocumentType() {

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_example_with_invalid_document_type.xml");
        EdaplosParsingResult results = edaplosService.validEdaplosData(eDaplosStream, "eDAPLOS_example_with_invalid_document_type");
        Assertions.assertEquals(EdaplosParsingStatus.FAIL, results.getEdaplosParsingStatus());

        assertErrorMessage(results, "Le TypeCode du document n'est pas le bon aaa12");
    }

    /**
     * Test d'import du fichier eDaplos : un intrant phyto est fourni avec une action : les deux matchent.
     */
    @Test
    public void testImportEdaplosWithPhytoInputAndAction() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_exemple_with_phyto_input_and_action.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results.getEdaplosParsingStatus());

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        assertThat(domain).isNotNull();
        assertThat(domain.getName()).isEqualTo("EARL ISAGRI");
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(1);
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();

        // seasonal
        EffectiveSeasonalCropCycle seasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(seasonalCropCycle).isNotNull();
        Collection<EffectiveCropCycleNode> perennialNodes = seasonalCropCycle.getNodes();
        assertThat(perennialNodes).hasSize(1);
        List<EffectiveIntervention> nodeInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(perennialNodes.iterator().next()).findAll();
        assertThat(nodeInterventions).hasSize(1);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(nodeInterventions);

        // intervention
        EffectiveIntervention effectiveIntervention = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_TraItement 1").findUniqueOrNull();
        assertThat(effectiveIntervention).isNotNull();
        List<AbstractAction> actions = abstractActionTopiaDao.forEffectiveInterventionEquals(effectiveIntervention).findAll();
        Assertions.assertEquals(1, actions.size());
        PesticidesSpreadingAction pesticidesSpreadingAction = (PesticidesSpreadingAction) actions.getFirst();
        CollectionUtils.isNotEmpty(pesticidesSpreadingAction.getPesticideProductInputUsages());
    }

    private void validateEffectiveInterventiinsTopiaCreateDatepersisted(List<EffectiveIntervention> effectiveInterventions) {
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(jpaSupport, effectiveInterventionDao, effectiveInterventions);
    }

    /**
     * Test d'import du fichier au format eDaplos pour vérifier qu'un ToolCoupling et les equipements ne sont pas dupliqués.
     */
    @Test
    public void testImportEdaplosIncompleteCropInputChemical() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_withoutCropInputChemical.xml");
        EdaplosParsingResult results = edaplosService.validEdaplosData(eDaplosStream, "eDAPLOS_withoutCropInputChemical");
        Assertions.assertEquals(0, results.getErrorMessages().size());
    }

    /**
     * Test d'import du fichier eDaplos : un intrant phyto est fourni avec aucune action : une action phyto est créée.
     */
    @Test
    @Disabled  // n'est plus valide depuis le #11891
    public void testImportEdaplosWithPhytoInputNoAction() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        final RefCountry france = testDatas.getDefaultFranceCountry();
        getPersistenceContext().getRefActaProduitRootDao().create(
                RefActaProduitRoot.PROPERTY_REF_COUNTRY, france,
                RefActaProduitRoot.PROPERTY_ACTIVE, true,
                RefActaProduitRoot.PROPERTY_AMM, "9900430",
                RefActaProduitRoot.PROPERTY_NOM_PRODUIT, "PRAGMA",
                RefActaProduitRoot.PROPERTY_ID_PRODUIT, "a_AAAIW"
        );

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_exemple_with_phyto_input_no_action.xml");
        edaplosService.loadAndSave(eDaplosStream);

        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        Assertions.assertEquals("EARL ISAGRI", domain.getName());
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();

        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUnique();
        assertThat(zone).isNotNull();

        EffectiveCropCycleNode node = effectiveCropCycleNodeTopiaDao.forEdaplosIssuerIdEquals("ID 2").findUnique();
        EffectiveIntervention intervention = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(node).findUnique();
        assertThat(intervention).isNotNull();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(intervention));

        Assertions.assertEquals(1, pesticidesSpreadingActionDao.findAll().size());
//        Assertions.assertEquals(4, pesticideProductInputTopiaDao.findAll().size());
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles.
     * <p>
     * Ignore car le test est bon, mais pas le code.
     */
    @Test
    @Disabled // n'est plus valide depuis le #11891
    public void testImportEdaplosXmlFileWithSeveralPlots() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        final RefCountry france = testDatas.getDefaultFranceCountry();
        getPersistenceContext().getRefActaProduitRootDao().create(
                RefActaProduitRoot.PROPERTY_REF_COUNTRY, france,
                RefActaProduitRoot.PROPERTY_ACTIVE, true,
                RefActaProduitRoot.PROPERTY_AMM, "9900430",
                RefActaProduitRoot.PROPERTY_NOM_PRODUIT, "PRAGMA",
                RefActaProduitRoot.PROPERTY_ID_PRODUIT, "a_AAAIW"
        );

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_example_with_two_plots.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results.getEdaplosParsingStatus());

        // check domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<Plot> plots = plotTopiaDao.forDomainEquals(domain).findAll();
        assertThat(plots).hasSize(2);

        // check plots
        Plot plot = plots.stream().filter(p -> "ID parcelle DAPLOS".equals(p.geteDaplosIssuerId())).findFirst().orElse(null);
        assertThat(plot).isNotNull();
        assertThat(plot.getLatitude()).isEqualTo(47.902964);
        assertThat(plot.getLongitude()).isEqualTo(1.9092510000000402);
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUnique();
        assertThat(zone.getType()).isEqualTo(ZoneType.PRINCIPALE);
        assertThat(zone.getLatitude()).isEqualTo(47.902964);
        assertThat(zone.getLongitude()).isEqualTo(1.9092510000000402);
        EffectiveSeasonalCropCycle effectiveSeasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUnique();
        assertThat(effectiveSeasonalCropCycle).isNotNull();

        // Check ground
        List<Ground> domainGrounds = groundTopiaDao.forDomainEquals(domain).findAll();
        assertThat(domainGrounds).hasSize(1);
        Ground domainGround = domainGrounds.getFirst();
        Assertions.assertEquals("ABF00", domainGround.getRefSolArvalis().getSol_calcaire_typeCode());
        Assertions.assertEquals("ABF13", domainGround.getRefSolArvalis().getSol_hydromorphie_typeCode());
        Assertions.assertEquals("ABF08", domainGround.getRefSolArvalis().getSol_pierrosite_typeCode());
        Assertions.assertEquals("ZGI", domainGround.getRefSolArvalis().getSol_profondeur_typeCode());
        Assertions.assertEquals("ABF04", domainGround.getRefSolArvalis().getSol_texture_typeCode());
        // On the plot
        Ground plotGround = plot.getGround();
        Assertions.assertNotNull(plotGround);
        Assertions.assertEquals("ABF00", plotGround.getRefSolArvalis().getSol_calcaire_typeCode());
        Assertions.assertEquals("ABF13", plotGround.getRefSolArvalis().getSol_hydromorphie_typeCode());
        Assertions.assertEquals("ABF08", plotGround.getRefSolArvalis().getSol_pierrosite_typeCode());
        Assertions.assertEquals("ZGI", plotGround.getRefSolArvalis().getSol_profondeur_typeCode());
        Assertions.assertEquals("ABF04", plotGround.getRefSolArvalis().getSol_texture_typeCode());

        // Check Tools Coupling
        List<ToolsCoupling> domainToolCoupling = toolsCouplingTopiaDao.forDomainEquals(domain).findAll();
        assertThat(domainToolCoupling).hasSize(1);
        List<Equipment> domainEquipments = equipmentTopiaDao.forDomainEquals(domain).findAll();
        assertThat(domainEquipments).hasSize(2);

        // interventions
        EffectiveCropCycleNode firstNode = effectiveSeasonalCropCycle.getNodes().iterator().next();
        List<EffectiveIntervention> interventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(firstNode).findAll();
        assertThat(interventions).hasSize(10);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(interventions);

        // an intervention
        EffectiveIntervention intervention1 = interventions.stream().filter(i -> i.getEdaplosIssuerId().equals("2599856659852365")).findFirst().orElse(null);
        assertThat(intervention1).isNotNull();
        assertThat(intervention1.getName()).isEqualTo("Intrant");
        assertThat(intervention1.getComment()).isEqualTo("PLAN PREVISIONNEL DE FUMURE (Campagne 07-08) sur Cimetiere / 07-08");
        assertThat(intervention1.getToolCouplings()).hasSize(1);
        ToolsCoupling toolsCoupling = intervention1.getToolCouplings().iterator().next();
        assertThat(toolsCoupling.getTractor()).isNotNull();
        assertThat(toolsCoupling.getEquipments()).isNotNull();
        assertThat(toolsCoupling.getEquipments()).hasSize(1);
        assertThat(toolsCoupling.getMainsActions()).hasSize(1);

        // another intervention
        EffectiveIntervention intervention2 = interventions.stream().filter(i -> i.getEdaplosIssuerId().equals("GUID_Traitement 1")).findFirst().orElse(null);
        List<AbstractAction> intervention2Actions = abstractActionTopiaDao.forEffectiveInterventionEquals(intervention2).findAll();
        assertThat(intervention2Actions).hasSize(1);

        List<PesticideProductInputUsage> pesticideProductInputUsages = new ArrayList<>();
        for (AbstractAction intervention2Action : intervention2Actions) {
            PesticidesSpreadingAction intervention2Action1 = (PesticidesSpreadingAction) intervention2Action;
            pesticideProductInputUsages.addAll(intervention2Action1.getPesticideProductInputUsages());
        }

        assertThat(pesticideProductInputUsages).hasSize(4);

    }

    /**
     * Test d'import du fichier au format eDaplos ou la CI est positionnée en
     * début de fichier (avant les cultures assolées). Il s'agit de vérifier que
     * ces infos sont bien traitées et les noeuds correctement liés.
     */
    @Test
    public void testImportEdaplosWithRanksError() {
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_exemple_with_rank_errors.xml");
        EdaplosParsingResult results = edaplosService.validEdaplosData(eDaplosStream, "eDAPLOS_exemple_with_rank_errors");

        assertErrorMessage(results, "La surface concernée par l'intervention 'Intervention (13/10/2007)' sur occupation du sol 'Rang 1 (Blé Tendre d'hiver)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' est incorrecte. Import impossible.");
    }

    /**
     * Test d'import du fichier eDaplos : aucune entrée temporelle pour l'intervention.
     */
    @Test
    @Disabled // n'est plus valide depuis le #11891
    public void testImportEdaplosZipWithTwoYears() throws IOException {
        testDatas.createDomainWIthSiret(null, 2011);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_example_file_for_year_2010.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        assertErrorMessage(results, "L'unité PCD utilisée pour déclarer la densité de semis/plantation sur l'intervention 'Intervention (01/01/2010)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', " +
                "domaine 'EARL ISAGRI - 2010 (77556351300016)' n'est pas autorisée par Agrosyst. " +
                "Merci de remplacer celle ci par une unité universelle dans votre logiciel pour pouvoir importer.");

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2010).findUnique();
        assertThat(domain).isNotNull();
        Assertions.assertEquals("EARL ISAGRI", domain.getName());
        Plot plot = plotTopiaDao.forDomainEquals(domain).findUnique();
        assertThat(plot).isNotNull();
        Zone zone = zoneTopiaDao.forPlotEquals(plot).findUniqueOrNull();
        assertThat(zone).isNotNull();

        // seasonal
        EffectiveSeasonalCropCycle seasonalCropCycle = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone).findUniqueOrNull();
        assertThat(seasonalCropCycle).isNotNull();
        Collection<EffectiveCropCycleNode> perennialNodes = seasonalCropCycle.getNodes();
        assertThat(perennialNodes).hasSize(1);
        List<EffectiveIntervention> nodeInterventions = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(perennialNodes.iterator().next()).findAll();
        assertThat(nodeInterventions).hasSize(2);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(nodeInterventions);

        InputStream eDaplosZipStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS-two-yeas-import.zip");
        edaplosService.loadAndSave(eDaplosZipStream);

        // domain 2011
        Domain domain2011 = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2010).findUnique();
        Assertions.assertNotNull(domain2011.getCode());
        Assertions.assertEquals(domain.getCode(), domain2011.getCode());
        List<CroppingPlanEntry> croppingPlanEntries2011 = croppingPlanEntryTopiaDao.forDomainEquals(domain2011).findAll();
        assertThat(croppingPlanEntries2011).hasSize(1);
        Plot plot2011 = plotTopiaDao.forDomainEquals(domain2011).findUnique();
        assertThat(plot2011).isNotNull();
        Zone zone2011 = zoneTopiaDao.forPlotEquals(plot2011).findUniqueOrNull();
        assertThat(zone2011).isNotNull();

        // cycles
        EffectivePerennialCropCycle perennialCropCycle2011 = effectivePerennialCropCycleTopiaDao.forZoneEquals(zone2011).findUniqueOrNull();
        assertThat(perennialCropCycle2011).isNull();
        EffectiveSeasonalCropCycle seasonalCropCycle2011 = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone2011).findUniqueOrNull();
        assertThat(seasonalCropCycle2011).isNotNull();

        // domaine 2012
        Domain domain2012 = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2012).findUnique();
        Assertions.assertNotNull(domain2012.getCode());
        Assertions.assertEquals(domain.getCode(), domain2012.getCode());
        List<CroppingPlanEntry> croppingPlanEntries2012 = croppingPlanEntryTopiaDao.forDomainEquals(domain2011).findAll();
        assertThat(croppingPlanEntries2012).hasSize(1);
        Plot plot2012 = plotTopiaDao.forDomainEquals(domain2012).findUnique();
        assertThat(plot2012).isNotNull();
        Zone zone2012 = zoneTopiaDao.forPlotEquals(plot2012).findUniqueOrNull();
        assertThat(zone2012).isNotNull();

        // cycles
        EffectivePerennialCropCycle perennialCropCycle2012 = effectivePerennialCropCycleTopiaDao.forZoneEquals(zone2012).findUniqueOrNull();
        assertThat(perennialCropCycle2012).isNull();
        EffectiveSeasonalCropCycle seasonalCropCycle2012 = effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zone2012).findUniqueOrNull();
        assertThat(seasonalCropCycle2012).isNotNull();
    }

    @Test
    public void testValidSiretWithWhiteSpaces() {
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_valid_SIRET.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        assertErrorMessageAbsent(results, "Le SIRET du domaine est une information obligatoire or il est manquant dans le document");
        assertErrorMessageAbsent(results, " Le SIRET du domaine est une information obligatoire or il n'est pas valide dans le document.");
        assertErrorMessageAbsent(results, "Le SIRET n'est pas valide et devra être corrigé sur votre domaine.");
        //assertInfoMessage(results, "Le domaine ayant le numéro de siret :44211670300046, est à créer pour la campagne : 2001");
    }

    /**
     * Test d'import du fichier eDaplos : l'import d'un cycle sans culture.
     */
    @Test
    public void testImportEdaplosCycleWithoutCroppingPlan() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/misc/eDAPLOSV2.3_cyclewithoutcrop.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results.getEdaplosParsingStatus());

        assertInfoMessage(results, "Aucune culture n'est déclarée à l'emplacement de rang 1 sur la parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)', campagne 2001");
    }

    /**
     * Test d'import du fichier eDaplos : import des communes par code insee.
     */
    @Test
    public void testImportEdaplosCommuneCodeInsee() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/misc/eDAPLOSV2.3_localisationinseecode.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results.getEdaplosParsingStatus());

        Domain domainB = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        RefLocation location = domainB.getLocation();

        assertThat(location.getCommune()).isEqualTo("Artannes-sur-Indre");
    }

    /**
     * Test d'import du fichier eDaplos : import des communes par code postal multiple.
     */
    @Test
    @Disabled // n'est plus valide depuis le #11891
    public void testImportEdaplosCommuneCodePostalMultiple() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/misc/eDAPLOSV2.3_localisationcodepostalmultiple.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results.getEdaplosParsingStatus());

        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        RefLocation location = domain.getLocation();
        assertThat(location.getCommune()).isEqualTo("Château-Thébaud");
        assertWarningMessage(results, "Le code postal '44690' correspond à plusieurs communes. 'Château-Thébaud' a été sélectionné par défaut.");
    }

    /**
     * Test d'import du fichier eDaplos : import d'une culture définies avec 3 espèces varietés
     * En premier sont importés les semances
     * Puis est déclaré une action de récolte
     */
    @Test
    public void testImportEdaplosCroppingPlanMultipleVarieties() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/crop/eDAPLOS_multiplespecies.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results.getEdaplosParsingStatus());

        EffectiveIntervention seedingIntervention = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_TraItement 1").findUnique();
        assertThat(seedingIntervention.getType()).isEqualTo(AgrosystInterventionType.SEMIS);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention));

        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        assertThat(croppingPlanEntries).hasSize(1);
        CroppingPlanEntry croppingPlanEntry = croppingPlanEntries.getFirst();
        assertThat(croppingPlanEntry.getCroppingPlanSpecies()).hasSize(3);


        HarvestingAction actionHarvesting = harvestingActionTopiaDao.forEffectiveInterventionEquals(seedingIntervention).findUniqueOrNull();
        Collection<HarvestingActionValorisation> harvestingValorisations = actionHarvesting.getValorisations();

        // first
        HarvestingActionValorisation firstValorisation = harvestingValorisations.iterator().next();
        assertThat(firstValorisation.getDestination().getDestination()).isEqualTo(ActionService.DEFAULT_DESTINATION_NAME);
        assertThat(firstValorisation.getDestination().getYealdUnit()).isEqualTo(YealdUnit.KG_M2);
        assertThat(firstValorisation.getDestination().getSector()).isEqualTo(Sector.GRANDES_CULTURES);
        assertThat(firstValorisation.getYealdAverage()).isCloseTo(14.5 / 3, Percentage.withPercentage(1));
        assertThat(firstValorisation.getYealdUnit()).isEqualTo(YealdUnit.KG_M2);
    }

    /**
     * Test d'import de trois fichiers edaplos de filiation des domaines.
     * 2011 : domaine initial
     * 2013 : domaine copié
     * 2014 : domaine copié et parcelle modifié la surface a changée et culture modifiée
     */
    @Test
    @Disabled // n'est plus valide depuis le #11891
    public void testImportEdaplosExtendDomain() throws IOException {
        testDatas.createDomainWIthSiret(null, 2011);
        testDatas.importAllMateriels(); // huge referential

        // first one
        InputStream eDaplosStream2011 = EdaplosServiceTest.class.getResourceAsStream("/edaplos/domain/eDAPLOS_filliation_domain2011.xml");
        EdaplosParsingResult results2011 = edaplosService.loadAndSave(eDaplosStream2011);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results2011.getEdaplosParsingStatus());
        EdaplosResultLog siretWarningLog = null;
        for (EdaplosResultLog edaplosWarning : results2011.getWarningMessages()) {
            if (edaplosWarning.getPath().equals("domain 'token', contact '86546451651844'")) {
                siretWarningLog = edaplosWarning;
            }
        }
        Assertions.assertNotNull(siretWarningLog);

        // 2011
        Domain domain2011 = domainTopiaDao.forSiretEquals("86546451651844").addEquals(Domain.PROPERTY_CAMPAIGN, 2011).findUnique();
        Plot plot2011 = plotTopiaDao.forDomainEquals(domain2011).findUnique();
        assertThat(plot2011.getArea()).isEqualTo(14.79);
        CroppingPlanEntry croppingPlanEntry2011 = croppingPlanEntryTopiaDao.forDomainEquals(domain2011).findUnique();
        assertThat(croppingPlanEntry2011).isNotNull();
        ToolsCoupling toolsCoupling2011 = toolsCouplingTopiaDao.forDomainEquals(domain2011).findUnique();
        assertThat(toolsCoupling2011.getEquipments()).hasSize(1);
        assertThat(toolsCoupling2011.getTractor()).isNotNull();
        List<Equipment> equipments2011 = equipmentTopiaDao.forDomainEquals(domain2011).findAll();
        assertThat(equipments2011).hasSize(2);

        // valid main action affectation
        EffectiveIntervention effectiveIntervention = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(effectiveSeasonalCropCycleTopiaDao.forZoneEquals(zoneTopiaDao.forPlotEquals(plot2011).findAll().get(0)).findAll().getFirst().getNodes().iterator().next()).findAll().getFirst();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(effectiveIntervention));
        AbstractAction action = abstractActionTopiaDao.forEffectiveInterventionEquals(effectiveIntervention).findAll().getFirst();
        ToolsCoupling tc = effectiveIntervention.getToolCouplings().iterator().next();
        Assertions.assertEquals(action.getToolsCouplingCode(), tc.getCode());

        // import 2013
        InputStream eDaplosStream2013 = EdaplosServiceTest.class.getResourceAsStream("/edaplos/domain/eDAPLOS_filliation_domain2013.xml");
        EdaplosParsingResult results2013 = edaplosService.loadAndSave(eDaplosStream2013);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results2013.getEdaplosParsingStatus());

        // 2012 (non prolongé)
        Domain domain2012 = domainTopiaDao.forSiretEquals("86546451651844").addEquals(Domain.PROPERTY_CAMPAIGN, 2012).findUniqueOrNull();
        assertThat(domain2012).isNull();

        // 2013 (juste prolongé)
        Domain domain2013 = domainTopiaDao.forSiretEquals("86546451651844").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        assertThat(domain2013.getCode()).isEqualTo(domain2011.getCode());
        Plot plot2013 = plotTopiaDao.forDomainEquals(domain2013).findUnique();
        assertThat(plot2013.getCode()).isEqualTo(plot2011.getCode());
        assertThat(plot2013.getArea()).isEqualTo(14.79);
        CroppingPlanEntry croppingPlanEntry2013 = croppingPlanEntryTopiaDao.forDomainEquals(domain2013).findUnique();
        assertThat(croppingPlanEntry2013.getCode()).isEqualTo(croppingPlanEntry2011.getCode());
        ToolsCoupling toolsCoupling2013 = toolsCouplingTopiaDao.forDomainEquals(domain2013).findUnique();
        assertThat(toolsCoupling2013.getCode()).isEqualTo(toolsCoupling2011.getCode());
        assertThat(toolsCoupling2013.getEquipments()).hasSize(1);
        assertThat(toolsCoupling2013.getEquipments().iterator().next().getRefMateriel()).isEqualTo(toolsCoupling2011.getEquipments().iterator().next().getRefMateriel());
        List<Equipment> equipments2013 = equipmentTopiaDao.forDomainEquals(domain2013).findAll();
        assertThat(equipments2013).hasSize(2);

        // import 2014
        InputStream eDaplosStream2014 = EdaplosServiceTest.class.getResourceAsStream("/edaplos/domain/eDAPLOS_filliation_domain2014.xml");
        EdaplosParsingResult results2014 = edaplosService.loadAndSave(eDaplosStream2014);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results2014.getEdaplosParsingStatus());

        // 2014 (prolongé et modifié)
        Domain domain2014 = domainTopiaDao.forSiretEquals("86546451651844").addEquals(Domain.PROPERTY_CAMPAIGN, 2014).findUnique();
        assertThat(domain2014.getCode()).isEqualTo(domain2013.getCode());
        List<Plot> plots2014 = plotTopiaDao.forDomainEquals(domain2014).findAll();
        Plot plot2014_1 = plots2014.getFirst();
        assertThat(plot2014_1.getArea()).isEqualTo(51.42);// surface mise à jour
        assertThat(plot2014_1.getCode()).isEqualTo(plot2013.getCode());
        List<CroppingPlanEntry> croppingPlanEntries2014 = croppingPlanEntryTopiaDao.forDomainEquals(domain2014).findAll();
        assertThat(croppingPlanEntries2014).hasSize(2);
        CroppingPlanEntry croppingPlanEntry2014_1 = croppingPlanEntries2014.getFirst();
        CroppingPlanEntry croppingPlanEntry2014_2 = croppingPlanEntries2014.get(1);
        assertThat(croppingPlanEntry2014_1.getCode()).isEqualTo(croppingPlanEntry2011.getCode());
        assertThat(croppingPlanEntry2014_2.getCode()).isNotEqualTo(croppingPlanEntry2011.getCode());
        ToolsCoupling toolsCoupling2014 = toolsCouplingTopiaDao.forDomainEquals(domain2014).findUnique();
        assertThat(toolsCoupling2014.getCode()).isEqualTo(toolsCoupling2011.getCode());
        assertThat(toolsCoupling2014.getEquipments()).hasSize(1);
        assertThat(toolsCoupling2014.getEquipments().iterator().next().getRefMateriel()).isEqualTo(toolsCoupling2011.getEquipments().iterator().next().getRefMateriel());
        List<Equipment> equipments2014 = equipmentTopiaDao.forDomainEquals(domain2014).findAll();
        assertThat(equipments2014).hasSize(2);
    }

    /**
     * Test que l'utilisation de petit matériel est problematique.
     */
    @Test
    public void testImportEdaplosPetitMaterielInfo() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.createDomainWIthSiret(null, 2011);

        // first one
        InputStream eDaplosStream2011 = EdaplosServiceTest.class.getResourceAsStream("/edaplos/domain/eDAPLOS_materiel_petitmateriel.xml");
        EdaplosParsingResult results2011 = edaplosService.loadAndSave(eDaplosStream2011);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results2011.getEdaplosParsingStatus());

        // 2011
        Domain domain2011 = domainTopiaDao.forSiretEquals("86546451651844").addEquals(Domain.PROPERTY_CAMPAIGN, 2011).findUnique();
        CroppingPlanEntry croppingPlanEntry2011 = croppingPlanEntryTopiaDao.forDomainEquals(domain2011).findUnique();
        assertThat(croppingPlanEntry2011).isNotNull();
        List<Equipment> equipments2011 = equipmentTopiaDao.forDomainEquals(domain2011).findAll();

        // check
        assertThat(equipments2011).hasSize(2);
    }

    /**
     * Test la remonté de l'erreur sur l'absence de destination.
     * L'erreur est présente deux fois dans le fichier importé mais doit être visible uniquement une seul fois.
     */
    @Test
    public void testGlobalErrorMessage() throws IOException {
        testDatas.importAllMateriels();
        testDatas.createDomainWIthSiret(null, 2011);

        InputStream eDaplosStream2011 = EdaplosServiceTest.class.getResourceAsStream("/edaplos/domain/eDAPLOS_materiel_petitmateriel.xml");
        EdaplosParsingResult results2011 = edaplosService.loadAndSave(eDaplosStream2011);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results2011.getEdaplosParsingStatus());
        assertWarningMessage(results2011, "La destination de la récolte est manquante sur toutes les interventions de récolte car elle n'est pas encore gérée dans eDaplos. La destination 'À compléter' a été mise par défaut et devra être modifiée systématiquement après import.");
        Assertions.assertEquals(1L, results2011.getWarningMessages().stream()
                .map(EdaplosResultLog::getLabel)
                .filter(label -> label.contentEquals("La destination de la récolte est manquante sur toutes les interventions de récolte car elle n'est pas encore gérée dans eDaplos. La destination 'À compléter' a été mise par défaut et devra être modifiée systématiquement après import.")).count());
    }

    /**
     * Test d'import du fichier eDaplos : creation d'une entrée de log.
     */
    @Test
    public void testImportEdaplosImportLogEntry() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        // get user
        loginUser(TEST_E_MAIL, TEST_PWD);
//        userDto.setEmail(null); // to disable sending email

        // import
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/crop/eDAPLOS_multiplespecies.xml");
        EdaplosParsingResult results = edaplosService.importEdaplos(eDaplosStream, "eDAPLOS_multiplespecies.xml");
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, results.getEdaplosParsingStatus());

        // test results
        List<EdaplosImport> allEntries = edaplosImportTopiaDao.findAll();
        assertThat(allEntries).hasSize(1);

        EdaplosImport edaplosImport = allEntries.getFirst();
        assertThat(edaplosImport.getAgrosystUser().getEmail()).isEqualTo("admin@test.fr");
        assertThat(edaplosImport.getImportDate()).isNotNull();
        assertThat(edaplosImport.getImportFileName()).isEqualTo("eDAPLOS_multiplespecies.xml");
        assertThat(edaplosImport.getDocumentIdentification()).isEqualTo("token");
        assertThat(edaplosImport.getDocumentTypeCode()).isEqualTo("415");
        assertThat(edaplosImport.getIssuerName()).isEqualTo("EARL ISAGRI");
        assertThat(edaplosImport.getIssuerIdentification()).isEqualTo("77556351300016");
        assertThat(edaplosImport.getSoftwareVersion()).isEqualTo("6.5");
        assertThat(edaplosImport.getSoftwareName()).isEqualTo("ISAMARGE");
        assertThat(edaplosImport.getCampaigns()).isEqualTo("2001");
    }

    /**
     * Test que l'on se sert bien de la correspondance du fichier "refEspeceOtherTools" pour les especes inconnues.
     * <p>
     * ZCQ;ZLU; n'existe pas dans refEspece.
     * Remplacé par ZCQ;;
     */
    @Test
    public void testImportEdaplosVarietyOtherTools() {

        RefEspece especeAgrosyst = new RefEspeceImpl();
        especeAgrosyst.setCode_espece_botanique("ZCQ-A");
        especeAgrosyst.setLibelle_espece_botanique("Luzerne");
        especeAgrosyst.setCode_qualifiant_AEE("ZLU-A");
        especeAgrosyst.setLibelle_qualifiant_AEE("temporaire");
        especeAgrosyst.setCode_type_saisonnier_AEE("ZCQ-A");
        especeAgrosyst.setLibelle_type_saisonnier_AEE("Printemps");
        especeAgrosyst.setCode_destination_AEE("D0");
        especeAgrosyst.setLibelle_destination_BBCH("Déshydraté");
        especeAgrosyst.setActive(true);

        getPersistenceContext().getRefEspeceDao().create(especeAgrosyst);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/crop/eDAPLOS_codeothertools.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.FAIL);
        assertWarningMessage(result,
                "L'occupation du sol ayant pour identifiant : soil occupation id 1, possède une espèce qui n'a pas été retrouvée dans le référentiel des espèces Agrosyst mais une correspondance a été établie avec une espèce présente dans Agrosyst." +
                        " Caractéristiques de la culture à importer: Espèce botanique 'ZCQ - Luzerne', Qualifiant 'ZLU-Edaplos - Temporaire-E', Type saisonnier 'ZCQ-Edaplos - Printemps'." +
                        " Nouvelles caractéristiques de la culture après import dans Agrosyst : Espèce botanique 'ZCQ-A-Luzerne', Qualifiant 'ZLU-A - temporaire-A', Type saisonnier 'ZCQ-A - Printemps'.");

        // domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<CroppingPlanEntry> croppingPlanEntries = croppingPlanEntryTopiaDao.forDomainEquals(domain).findAll();
        CroppingPlanEntry croppingPlanEntry = croppingPlanEntries.getFirst();
        CroppingPlanSpecies croppingPlanSpecies = croppingPlanEntry.getCroppingPlanSpecies().getFirst();
        assertThat((croppingPlanSpecies.getSpecies().getLibelle_espece_botanique())).isEqualTo("Luzerne");
        assertThat((croppingPlanSpecies.getSpecies().getLibelle_destination_BBCH())).isEqualTo("Déshydraté");
    }

    /**
     * Dans ce test, nous avons deux parcelles avec le même sol Arvalys identifié de deux façons différentes (cf #10058) :
     * - La combinaison des cinq composants du sol (calcaire, hydromorphie, pierrosite, profondeur, texture)
     * - l'identifiant du sol Arvalys
     * <p>
     * Dans les deux identifications du sol, il s'agit du même sol : on ne doit donc retrouver qu'un seul sol arvalys sur le domaine, et chaque parcelle définit le même.
     */
    @Test
    public void testImportEdaplosXmlFileWithSolArvalysId() {

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_example_arvalys_id_as_technical_characteristic.xml");
        edaplosService.loadAndSave(eDaplosStream);

        // check domain
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<Plot> plots = plotTopiaDao.forDomainEquals(domain).findAll();
        assertThat(plots).hasSize(2);

        // check plots

        // Check ground
        List<Ground> domainGrounds = groundTopiaDao.forDomainEquals(domain).findAll();
        assertThat(domainGrounds).hasSize(1);
        Ground domainGround = domainGrounds.getFirst();
        Assertions.assertEquals("MP0036000", domainGround.getRefSolArvalis().getId_type_sol());
        Assertions.assertEquals("ABF00", domainGround.getRefSolArvalis().getSol_calcaire_typeCode());
        Assertions.assertEquals("ABF13", domainGround.getRefSolArvalis().getSol_hydromorphie_typeCode());
        Assertions.assertEquals("ABF08", domainGround.getRefSolArvalis().getSol_pierrosite_typeCode());
        Assertions.assertEquals("ZGI", domainGround.getRefSolArvalis().getSol_profondeur_typeCode());
        Assertions.assertEquals("ABF04", domainGround.getRefSolArvalis().getSol_texture_typeCode());

        // On the plots
        Ground firstPlotGround = plots.getFirst().getGround();
        Assertions.assertNotNull(firstPlotGround);
        Assertions.assertEquals("MP0036000", firstPlotGround.getRefSolArvalis().getId_type_sol());
        Assertions.assertEquals("ABF00", firstPlotGround.getRefSolArvalis().getSol_calcaire_typeCode());
        Assertions.assertEquals("ABF13", firstPlotGround.getRefSolArvalis().getSol_hydromorphie_typeCode());
        Assertions.assertEquals("ABF08", firstPlotGround.getRefSolArvalis().getSol_pierrosite_typeCode());
        Assertions.assertEquals("ZGI", firstPlotGround.getRefSolArvalis().getSol_profondeur_typeCode());
        Assertions.assertEquals("ABF04", firstPlotGround.getRefSolArvalis().getSol_texture_typeCode());

        Ground secondPlotGround = plots.get(1).getGround();
        Assertions.assertNotNull(secondPlotGround);
        Assertions.assertEquals("MP0036000", secondPlotGround.getRefSolArvalis().getId_type_sol());
        Assertions.assertEquals("ABF00", secondPlotGround.getRefSolArvalis().getSol_calcaire_typeCode());
        Assertions.assertEquals("ABF13", secondPlotGround.getRefSolArvalis().getSol_hydromorphie_typeCode());
        Assertions.assertEquals("ABF08", secondPlotGround.getRefSolArvalis().getSol_pierrosite_typeCode());
        Assertions.assertEquals("ZGI", secondPlotGround.getRefSolArvalis().getSol_profondeur_typeCode());
        Assertions.assertEquals("ABF04", secondPlotGround.getRefSolArvalis().getSol_texture_typeCode());
    }

    /**
     * Dans ce test, nous avons deux parcelles avec le même sol Arvalys identifié de deux façons différentes (cf #10058) :
     * - La combinaison des cinq composants du sol (calcaire, hydromorphie, pierrosite, profondeur, texture)
     * - l'identifiant du sol Arvalys
     * <p>
     * Dans les deux identifications du sol, il s'agit du même sol : on ne doit donc retrouver qu'un seul sol arvalys sur le domaine, et chaque parcelle définit le même.
     */
    @Test
    public void testImportEdaplosXmlFileWithNoEffluentGiven() {

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/edaplos_example_NoEffluentGiven.xml");
        EdaplosParsingResult results = edaplosService.loadAndSave(eDaplosStream);

        assertWarningMessage(results, "Le qualifiant EDI '' de l'intrant 'ZJB', intervention 'Intrant (15/03/2017)', occupation du sol 'Rang 2 (Maïs)', parcelle 'PETIT LOURMEL (2.44 ha)', domaine 'GAEC RESCAN-GEFFROY - 2017 (40969833900010)' est inconnu du référentiel Utilisation du fertilisant organique par défault idtypeeffluent: 'NO_PRODUCT'.");
    }

    @Test
    public void testParse() {
        int integ = NumberUtils.createNumber("12.2").intValue();
        Assertions.assertEquals(12, integ);
    }


    @Test
    public void searchCommune() throws IOException {

        testDatas.importCommunesFrance();

        String researchedCommuneName = "BOIGNEVILLE";
        RefLocationTopiaDao locationDao = getPersistenceContext().getRefLocationDao();

        testDatas.createLocation(locationDao, "91720", RefLocation.PROPERTY_COMMUNE, "Boigneville", RefLocation.PROPERTY_DEPARTEMENT, "91", RefLocation.PROPERTY_CODE_POSTAL, "91720");

        testDatas.createLocation(locationDao, "91721", RefLocation.PROPERTY_COMMUNE, "Boigneville nord", RefLocation.PROPERTY_DEPARTEMENT, "91", RefLocation.PROPERTY_CODE_POSTAL, "91720");

        RefLocation Location = edaplosService.searchCommune("91720", researchedCommuneName);

        Assertions.assertEquals("Boigneville", (Location.getCommune()));
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles
     */
    @Test
    public void testImportEdaplosIntrantInactiveProduct() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/intrants/eDAPLOSV2.3_inactive_productRoot.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // On ne retrouve pas notre intrant 'KHOLTHIOR' car inactif dans le référentiel : on a un autre choix existant avec le même code AMM
        assertWarningMessage(result, "L'intrant déclaré sur la culture 'Blé Tendre d'hiver', intervention 'Intervention (26/04/2008)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' n'a pas pu être retrouvé avec le nom fourni 'KOLTHIOR'. Utilisation du produit 'Thiovit jet microbilles' - traitement 'Fongicides - Traitement des parties aériennes'.");

    }

    /**
     * Test d'import du fichier au format eDaplos.
     * L'import ne se fait pas car le numéro de siret n'existe pas et la balise PartyContact est manquante
     */
    @Test
    public void test10420() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);
        // parse edaplos file
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_variety_code_with_an_E.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        Assertions.assertEquals(EdaplosParsingStatus.SUCCESS, result.getEdaplosParsingStatus());
    }

    /**
     * Test d'import du fichier au format eDaplos avec création d'un domaine et des parcelles
     */
    @Test
    public void testImportEdaplosIntrantUnknowSpecies() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        int persistedDomainNb = domainTopiaDao.findAll().size();

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS_example_with_missing_species.xml");
        EdaplosParsingResult result = edaplosService.importEdaplos(eDaplosStream, "eDAPLOS_example_with_missing_species");
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.FAIL);
        List<Domain> persistedDomains = domainTopiaDao.findAll();
        Assertions.assertEquals(persistedDomainNb, persistedDomains.size());


    }

    @Test
    public void testImportEdaplosPluriAnnualCrop() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("19350700100017", 2013);

        final RefCountry france = testDatas.getDefaultFranceCountry();
        getPersistenceContext().getRefLocationDao().create(
                RefLocation.PROPERTY_REF_COUNTRY, france,
                RefLocation.PROPERTY_CODE_INSEE, "35650",
                RefLocation.PROPERTY_CODE_POSTAL, "35650",
                RefLocation.PROPERTY_COMMUNE, "LE RHEU",
                RefLocation.PROPERTY_DEPARTEMENT, "Ille-et-Vilaine",
                RefLocation.PROPERTY_LATITUDE, 48.0974,
                RefLocation.PROPERTY_LONGITUDE, -1.7815,
                RefLocation.PROPERTY_ACTIVE, true
        );

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDAPLOS-pluri-annual-crops.xml");
        EdaplosParsingResult result = edaplosService.importEdaplos(eDaplosStream, "eDAPLOS-pluri-annual-crops.xml");
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);
        {
            EffectiveIntervention interventionOnPrairieCrop = effectiveInterventionDao.forEdaplosIssuerIdEquals("mesparcelles_4079243").findUniqueOrNull();
            validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionOnPrairieCrop));
            EffectiveCropCycleNode node = interventionOnPrairieCrop.getEffectiveCropCycleNode();
            EffectiveCropCyclePhase phase = interventionOnPrairieCrop.getEffectiveCropCyclePhase();
            Assertions.assertNotNull(node);
            Assertions.assertNull(phase);
        }
        {
            EffectiveIntervention interventionOnPrairieCrop = effectiveInterventionDao.forEdaplosIssuerIdEquals("mesparcelles_4077181").findUniqueOrNull();
            validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionOnPrairieCrop));
            HarvestingAction harvestingAction = harvestingActionTopiaDao.forEffectiveInterventionEquals(interventionOnPrairieCrop).findUnique();
            assertThat(harvestingAction).isNotNull();
            List<HarvestingPrice> harvestingPrices = harvestingPriceTopiaDao.forHarvestingActionValorisationIn(harvestingAction.getValorisations()).findAll();
            assertThat(harvestingPrices).isNotEmpty();
            Assertions.assertTrue(harvestingPrices.stream().allMatch(p -> Objects.nonNull(p.getPrice())));
        }
        {
            EffectiveIntervention interventionOnPrairieCrop = effectiveInterventionDao.forEdaplosIssuerIdEquals("mesparcelles_3296457").findUniqueOrNull();
            validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionOnPrairieCrop));
            HarvestingAction harvestingAction = harvestingActionTopiaDao.forEffectiveInterventionEquals(interventionOnPrairieCrop).findUnique();
            assertThat(harvestingAction).isNotNull();
            List<HarvestingPrice> harvestingPrices = harvestingPriceTopiaDao.forHarvestingActionValorisationIn(harvestingAction.getValorisations()).findAll();
            assertThat(harvestingPrices).isNotEmpty();
            Assertions.assertTrue(harvestingPrices.stream().allMatch(p -> Objects.isNull(p.getPrice())));
        }
    }

    @Test
    public void testImportMultiInputs() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();

        testDatas.createLocation(getPersistenceContext().getRefLocationDao(), "62840", RefLocation.PROPERTY_CODE_POSTAL, "62840", RefLocation.PROPERTY_COMMUNE, "Fleurbaix", RefLocation.PROPERTY_DEPARTEMENT, "Pas-de-Calais");
        testDatas.createDomainWIthSiret("52407682500016", 2018);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos-multi-inputs.xml");
        EdaplosParsingResult result = edaplosService.importEdaplos(eDaplosStream, "eDaplos-multi-inputs.xml");
        EffectiveIntervention interventionOnPrairieCrop = effectiveInterventionDao.forEdaplosIssuerIdEquals("6895580").findUniqueOrNull();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionOnPrairieCrop));
        PesticidesSpreadingAction pesticidesSpreadingAction = pesticidesSpreadingActionDao.forEffectiveInterventionEquals(interventionOnPrairieCrop).findUniqueOrNull();
        assertThat(pesticidesSpreadingAction.getPesticideProductInputUsages()).hasSize(1);

        PesticideProductInputUsage pesticideProductInput = pesticidesSpreadingAction.getPesticideProductInputUsages().iterator().next();
        Assertions.assertEquals(1.0, pesticideProductInput.getQtAvg());
        Assertions.assertEquals(PhytoProductUnit.L_HA, pesticideProductInput.getDomainPhytoProductInput().getUsageUnit());

        MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = mineralFertilizersSpreadingActionDao.forEffectiveInterventionEquals(interventionOnPrairieCrop).findUniqueOrNull();
        assertThat(mineralFertilizersSpreadingAction.getMineralProductInputUsages()).hasSize(1);
        MineralProductInputUsage mineralProductInput = mineralFertilizersSpreadingAction.getMineralProductInputUsages().iterator().next();
        Assertions.assertEquals(50.0, mineralProductInput.getDomainMineralProductInput().getRefInput().getsO3());
        Assertions.assertEquals(23.0, mineralProductInput.getDomainMineralProductInput().getRefInput().getN());
        Assertions.assertEquals(1.3, mineralProductInput.getQtAvg());
        Assertions.assertEquals(MineralProductUnit.KG_HA, mineralProductInput.getDomainMineralProductInput().getUsageUnit());

        assertWarningMessage(result, "Sur la parcelle 'p8-50 (10.0 ha)', occupation du sol 'Rang 1 (Blé Tendre d'Hiver)', intervention 'Intrant (21/03/2018)', pour un intrant 'ZIU', impossible de trouver le produit correspondant au code AMM '2140257' ! Vous devrez rajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.");
        // action produit phyto renseigné dans le fichier
        Assertions.assertEquals(AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, interventionOnPrairieCrop.getType());
    }

    @Test
    public void testInterventionType() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("52407682500016", 2018);

        testDatas.createLocation(getPersistenceContext().getRefLocationDao(), "62840", RefLocation.PROPERTY_CODE_POSTAL, "62840", RefLocation.PROPERTY_COMMUNE, "Fleurbaix", RefLocation.PROPERTY_DEPARTEMENT, "Pas-de-Calais");

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos-multi-inputs-no-work.xml");
        EdaplosParsingResult result = edaplosService.importEdaplos(eDaplosStream, "eDaplos-multi-inputs-no-work.xml");
        EffectiveIntervention interventionOnPrairieCrop = effectiveInterventionDao.forEdaplosIssuerIdEquals("6895580").findUniqueOrNull();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionOnPrairieCrop));
        PesticidesSpreadingAction pesticidesSpreadingAction = pesticidesSpreadingActionDao.forEffectiveInterventionEquals(interventionOnPrairieCrop).findUniqueOrNull();
        assertThat(pesticidesSpreadingAction.getPesticideProductInputUsages()).hasSize(1);
        List<PesticideProductInputUsage> pesticideProductInputs = new ArrayList<>(pesticidesSpreadingAction.getPesticideProductInputUsages());


        PesticideProductInputUsage pesticideProductInput = pesticideProductInputs.getFirst();
        Assertions.assertEquals(1.0, pesticideProductInput.getQtAvg());
        Assertions.assertEquals(PhytoProductUnit.L_HA, pesticideProductInput.getDomainPhytoProductInput().getUsageUnit());

        MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = mineralFertilizersSpreadingActionDao.forEffectiveInterventionEquals(interventionOnPrairieCrop).findUniqueOrNull();
        assertThat(mineralFertilizersSpreadingAction.getMineralProductInputUsages()).hasSize(1);
        MineralProductInputUsage mineralProductInput = mineralFertilizersSpreadingAction.getMineralProductInputUsages().iterator().next();
        Assertions.assertEquals(50.0, mineralProductInput.getDomainMineralProductInput().getRefInput().getsO3());
        Assertions.assertEquals(23.0, mineralProductInput.getDomainMineralProductInput().getRefInput().getN());
        Assertions.assertEquals(1.3, mineralProductInput.getQtAvg());
        Assertions.assertEquals(MineralProductUnit.KG_HA, mineralProductInput.getDomainMineralProductInput().getUsageUnit());

        assertWarningMessage(result, "Sur la parcelle 'p8-50 (10.0 ha)', occupation du sol 'Rang 1 (Blé Tendre d'Hiver)', intervention 'Intrant (21/03/2018)', pour un intrant 'ZIU', impossible de trouver le produit correspondant au code AMM '2140257' ! Vous devrez rajouter manuellement ce produit dans Agrosyst après l'import, ainsi que les prix éventuels s'y rattachant.");
        // Intrant mineral en premier
        Assertions.assertEquals(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX, interventionOnPrairieCrop.getType());
    }

    @Test
    public void testIntermediateInterventionType() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.createDomainWIthSiret("52407682500016", 2018);

        testDatas.createLocation(getPersistenceContext().getRefLocationDao(), "62840", RefLocation.PROPERTY_CODE_POSTAL, "62840", RefLocation.PROPERTY_COMMUNE, "Fleurbaix", RefLocation.PROPERTY_DEPARTEMENT, "Pas-de-Calais");

        getPersistenceContext().getRefCultureEdiGroupeCouvSolDao().create(
                RefCultureEdiGroupeCouvSol.PROPERTY_ACTIVE, true,
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZDL",
                RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "Phacélie",
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_QUALIFIANT_AEE, "",
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_TYPE_SAISONNIER_AEE, "",
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_DESTINATION_AEE, "",
                RefCultureEdiGroupeCouvSol.PROPERTY_TAUX_COUVERTURE_MAX, 0.95,
                RefCultureEdiGroupeCouvSol.PROPERTY_VITESSE_COUV, VitesseCouv.MOYENNE,
                RefCultureEdiGroupeCouvSol.PROPERTY_TYPE_CULTURE, TypeCulture.ANNUELLE
        );

        getPersistenceContext().getRefCultureEdiGroupeCouvSolDao().create(
                RefCultureEdiGroupeCouvSol.PROPERTY_ACTIVE, true,
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZBB",
                RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_ESPECE_BOTANIQUE, "Chou",
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_QUALIFIANT_AEE, "ZBD",
                RefCultureEdiGroupeCouvSol.PROPERTY_LIBELLE_QUALIFIANT_AEE, "Fleur",
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_TYPE_SAISONNIER_AEE, "",
                RefCultureEdiGroupeCouvSol.PROPERTY_CODE_DESTINATION_AEE, "",
                RefCultureEdiGroupeCouvSol.PROPERTY_TAUX_COUVERTURE_MAX, 0.95,
                RefCultureEdiGroupeCouvSol.PROPERTY_VITESSE_COUV, VitesseCouv.MOYENNE,
                RefCultureEdiGroupeCouvSol.PROPERTY_TYPE_CULTURE, TypeCulture.ANNUELLE
        );

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos-intermediate_interventions.xml");
        EdaplosParsingResult result = edaplosService.importEdaplos(eDaplosStream, "eDaplos-intermediate_interventions.xml");
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);
        EffectiveIntervention interventionOnPrairieCrop = effectiveInterventionDao.forEdaplosIssuerIdEquals("6895429").findUniqueOrNull();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionOnPrairieCrop));
        Assertions.assertTrue(interventionOnPrairieCrop.isIntermediateCrop());

        List<EffectiveCropCycleNode> allNodes = getPersistenceContext().getEffectiveCropCycleNodeDao().findAll();
        List<EffectiveSeasonalCropCycle> effectiveCropCycles = getPersistenceContext().getEffectiveSeasonalCropCycleDao().findAll();

        Assertions.assertEquals(1, effectiveCropCycles.size());
        Assertions.assertTrue(effectiveCropCycles.getFirst().getNodes().containsAll(allNodes));

        List<Plot> plots = plotTopiaDao.findAll();
        for (Plot plot : plots) {
            List<Zone> zones = zoneTopiaDao.forPlotEquals(plot).findAll();
            Assertions.assertEquals(1, zones.size());
            Zone mainZone = zones.getFirst();
            Assertions.assertTrue(plot.getArea() > 0d);
            Assertions.assertEquals(plot.getArea(), mainZone.getArea());
        }
    }

    /**
     * Test d'import du fichier au format eDaplos avec gestion de la cible phyto
     */
    @Test
    public void testCiblePhyto_10508() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.importAdventices();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/edaplos-039004905_test_10508.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);
        assertWarningMessage(result, "La cible de l'intrant FANDANGO S (2060118), code '000000AEE509' intervention 'Intervention (24/04/2008)', occupation du sol 'Rang 1 (Blé Tendre d'hiver)', parcelle 'Cimetierre/ 07 08 (14.79 ha)', domaine 'EARL ISAGRI - 2001 (77556351300016)' n'est pas reconnue. Veuillez contacter l'équipe Agrosyst. Une cible par défaut a été utilisée (code '000000AEE021')");

        // test application produit phyto
        EffectiveIntervention interventionPhyto = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Traitement 1").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionPhyto));
        assertThat(interventionPhyto.getType()).isEqualTo(AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        LocalDate d = EdaplosUtils.parseInterventionDate("2008-04-24T00:00:00+02:00");
        Assertions.assertEquals(interventionPhyto.getStartInterventionDate(), d);
        assertThat(interventionPhyto.getSpatialFrequency()).isEqualTo(0.42);
        assertThat(interventionPhyto.getTransitCount()).isEqualTo(13);
        assertThat(interventionPhyto.getWorkRate()).isEqualTo(7);
        assertThat(interventionPhyto.getWorkRateUnit()).isEqualTo(MaterielWorkRateUnit.H_HA);
        PesticidesSpreadingAction actionPhyto = (PesticidesSpreadingAction) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto).findUnique();
        assertThat(actionPhyto.getProportionOfTreatedSurface()).isEqualTo(51);

        Assertions.assertNotNull(actionPhyto.getPesticideProductInputUsages());
        assertThat(actionPhyto.getPesticideProductInputUsages().size()).isEqualTo(1);
        List<PesticideProductInputUsage> pesticideProductInputUsages = Lists.newArrayList(actionPhyto.getPesticideProductInputUsages());

        PesticideProductInputUsage inputPhytoUsage = pesticideProductInputUsages.getFirst();
        DomainPhytoProductInput domainPhytoProductInput = inputPhytoUsage.getDomainPhytoProductInput();
        assertThat(domainPhytoProductInput).isNotNull();
        assertThat(domainPhytoProductInput.getRefInput()).isNotNull();
        assertThat(domainPhytoProductInput.getProductType()).isNotNull();
        assertThat(inputPhytoUsage.getQtAvg()).isEqualTo(1.44);
        assertThat(domainPhytoProductInput.getUsageUnit()).isEqualTo(PhytoProductUnit.L_HA);
        // caracteristique technique
        assertThat(actionPhyto.getBoiledQuantity()).isEqualTo(5);
        // test application produit phyto(price)
//        Price phytoInputPrice = harvestingPriceTopiaDao.forObjectIdEquals(
//                InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(inputPhyto.getPhytoProduct())).findUniqueOrNull();
//        assertThat(phytoInputPrice.getPrice()).isEqualTo(7000);
//        assertThat(phytoInputPrice.getPriceUnit()).isEqualTo(PriceUnit.EURO_L);
        // cibles
//        Collection<PhytoProductTarget> inputTargets = inputPhyto.getTargets();
//        assertThat(inputTargets).hasSize(5);

//        List<String> refNuisibleCodes = inputTargets.stream()
//                .map(PhytoProductTarget::getTarget)
//                .filter(ref -> ref != null && ref.getReferenceParam() != BioAgressorType.ADVENTICE)
//                .map(ref -> ((RefNuisibleEDI) ref).getReference_code())
//                .collect(Collectors.toList());
//        assertThat(refNuisibleCodes).hasSize(2);
//        assertThat(refNuisibleCodes).contains("AEE091AEE092"); // default one
//        assertThat(refNuisibleCodes).contains("ALTESPALTEDA"); // default one

//        List<String> refAdventiceIdentifiants = inputTargets.stream()
//                .map(PhytoProductTarget::getTarget)
//                .filter(ref -> ref != null && ref.getReferenceParam() == BioAgressorType.ADVENTICE)
//                .map(ref -> ((RefAdventice) ref).getIdentifiant())
//                .collect(Collectors.toList());
//        assertThat(refAdventiceIdentifiants).hasSize(3);
//        assertThat(refAdventiceIdentifiants).contains(EdaplosPersister.GENERIC_AGRESSOR_ID); // default one cause in test data, "000000AEE509" code does not exist
//        assertThat(refAdventiceIdentifiants).contains("000000AEE505");
//        assertThat(refAdventiceIdentifiants).contains("AMAAL"); // found from typeCode AMASSAMAAL

    }

    /**
     * Test d'import du fichier au format eDaplos avec gestion de la cible phyto
     */
    @Test
    public void testCheckboxTaitementChimiqueInoculationBio_10543() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.importAdventices();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_Ano_10543_checkboxes_SEMIS.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // test application produit phyto
        EffectiveIntervention interventionPhyto01 = effectiveInterventionDao.forEdaplosIssuerIdEquals("ID_10543_1").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionPhyto01));
        SeedingActionUsage actionPhyto = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto01).findUnique();
        Assertions.assertNotNull(actionPhyto.getSeedLotInputUsage());
        List<SeedLotInputUsage> seedLotInputUsages = Lists.newArrayList(actionPhyto.getSeedLotInputUsage());
        SeedLotInputUsage seedLotInputUsage = seedLotInputUsages.getFirst();
        Assertions.assertNotNull(seedLotInputUsage.getSeedingSpecies());
        ArrayList<SeedSpeciesInputUsage> seedSpeciesInputUsages = Lists.newArrayList(seedLotInputUsage.getSeedingSpecies());
        Assertions.assertTrue(CollectionUtils.isNotEmpty(seedSpeciesInputUsages));
        for (SeedSpeciesInputUsage seedSpeciesInputUsage : seedSpeciesInputUsages) {
            DomainSeedSpeciesInput domainSeedSpeciesInput = seedSpeciesInputUsage.getDomainSeedSpeciesInput();
            Assertions.assertTrue(domainSeedSpeciesInput.isChemicalTreatment());
            Assertions.assertTrue(CollectionUtils.isNotEmpty(domainSeedSpeciesInput.getSpeciesPhytoInputs()));
            assertThat(domainSeedSpeciesInput.getSpeciesPhytoInputs().size()).isEqualTo(1);
        }

        // test application Moyens bio
        EffectiveIntervention interventionBio = effectiveInterventionDao.forEdaplosIssuerIdEquals("ID_10543_2").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionBio));
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionBio));
        SeedingActionUsage actionPhyto02 = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionBio).findUnique();
        Assertions.assertNotNull(actionPhyto02.getSeedLotInputUsage());
        seedLotInputUsages = Lists.newArrayList(actionPhyto02.getSeedLotInputUsage());
        seedLotInputUsage = seedLotInputUsages.getFirst();
        Assertions.assertNotNull(seedLotInputUsage.getSeedingSpecies());
        seedSpeciesInputUsages = Lists.newArrayList(seedLotInputUsage.getSeedingSpecies());
        Assertions.assertTrue(CollectionUtils.isNotEmpty(seedSpeciesInputUsages));
        for (SeedSpeciesInputUsage seedSpeciesInputUsage : seedSpeciesInputUsages) {
            DomainSeedSpeciesInput domainSeedSpeciesInput = seedSpeciesInputUsage.getDomainSeedSpeciesInput();
            Assertions.assertTrue(domainSeedSpeciesInput.isBiologicalSeedInoculation());
            Assertions.assertTrue(CollectionUtils.isNotEmpty(domainSeedSpeciesInput.getSpeciesPhytoInputs()));
            assertThat(domainSeedSpeciesInput.getSpeciesPhytoInputs().size()).isEqualTo(1);
        }

        // test application produit phyto depuis insecticide
        EffectiveIntervention interventionPhyto03 = effectiveInterventionDao.forEdaplosIssuerIdEquals("ID_10543_3").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionPhyto03));
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(interventionPhyto03));
        SeedingActionUsage actionPhytoInsecticidal = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(interventionPhyto03).findUnique();
        Assertions.assertNotNull(actionPhytoInsecticidal.getSeedLotInputUsage());
        seedLotInputUsages = Lists.newArrayList(actionPhytoInsecticidal.getSeedLotInputUsage());
        seedLotInputUsage = seedLotInputUsages.getFirst();
        Assertions.assertNotNull(seedLotInputUsage.getSeedingSpecies());
        seedSpeciesInputUsages = Lists.newArrayList(seedLotInputUsage.getSeedingSpecies());
        Assertions.assertTrue(CollectionUtils.isNotEmpty(seedSpeciesInputUsages));
        for (SeedSpeciesInputUsage seedSpeciesInputUsage : seedSpeciesInputUsages) {
            DomainSeedSpeciesInput domainSeedSpeciesInput = seedSpeciesInputUsage.getDomainSeedSpeciesInput();
            Assertions.assertTrue(domainSeedSpeciesInput.isChemicalTreatment());
            Assertions.assertTrue(CollectionUtils.isNotEmpty(domainSeedSpeciesInput.getSpeciesPhytoInputs()));
        }
    }

    /**
     * Test d'import du fichier au format eDaplos avec plusieurs type de semaances
     */
    @Test
    public void testSeveralSeedType_10543() throws IOException {
        testDatas.importAllMateriels(); // huge referential
        testDatas.importAllActaTraitementsProduits();
        testDatas.importAdventices();
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_Evo_10783_Types_Semence.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // First intervention, SeedType : two ZQD = SEMENCES_DE_FERME
        EffectiveIntervention seedingIntervention1 = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Semis 1").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention1));
        assertThat(seedingIntervention1.getType()).isEqualTo(AgrosystInterventionType.SEMIS);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention1));

        SeedingActionUsage seedingActionDoubleZQD = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(seedingIntervention1).findUnique();
        assertThat(seedingActionDoubleZQD.getSeedType()).isEqualTo(SeedType.SEMENCES_DE_FERME);

        // Second intervention : ZQD + ZQA = SEMENCES_DE_FERME_ET_CERTIFIEES
        EffectiveIntervention seedingIntervention2 = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Semis 2").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention2));
        assertThat(seedingIntervention2.getType()).isEqualTo(AgrosystInterventionType.SEMIS);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention2));

        SeedingActionUsage seedingActionZQD_ZQA = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(seedingIntervention2).findUnique();
        assertThat(seedingActionZQD_ZQA.getSeedType()).isEqualTo(SeedType.SEMENCES_DE_FERME_ET_CERTIFIEES);

        // Second intervention : U50 = SEMENCES_DE_FERME
        EffectiveIntervention seedingIntervention3 = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Semis 3").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention3));
        assertThat(seedingIntervention3.getType()).isEqualTo(AgrosystInterventionType.SEMIS);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention3));

        SeedingActionUsage seedingActionU50 = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(seedingIntervention3).findUnique();
        assertThat(seedingActionU50.getSeedType()).isEqualTo(SeedType.SEMENCES_DE_FERME);

        // Second intervention : ZQA = SEMENCES_CERTIFIEES
        EffectiveIntervention seedingIntervention4 = effectiveInterventionDao.forEdaplosIssuerIdEquals("GUID_Semis 4").findUnique();
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention4));
        assertThat(seedingIntervention4.getType()).isEqualTo(AgrosystInterventionType.SEMIS);
        validateEffectiveInterventiinsTopiaCreateDatepersisted(List.of(seedingIntervention4));

        SeedingActionUsage seedingActionZQA = (SeedingActionUsage) abstractActionTopiaDao.forEffectiveInterventionEquals(seedingIntervention4).findUnique();
        assertThat(seedingActionZQA.getSeedType()).isEqualTo(SeedType.SEMENCES_CERTIFIEES);

    }

    /**
     * Test d'import du fichier au format eDaplos avec plusieurs type de semaances
     */
    @Test
    public void testImportInactivePlot_10543() throws IOException {
        testDatas.createDomainWIthSiret("77556351300016", 2001);

        // Step one : import plot
        InputStream eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_Evo_10787_inactive-plot.xml");
        EdaplosParsingResult result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // some assert on persisted plots
        Domain domain = domainTopiaDao.forSiretEquals("77556351300016").addEquals(Domain.PROPERTY_CAMPAIGN, 2001).findUnique();
        List<Plot> plots = plotService.findAllForDomain(domain);
        assertThat(plots.size()).isEqualTo(1);
        assertThat(plots.getFirst().isActive()).isTrue();

        // Unactivate plot
        plotService.updateActivatePlotStatus(Collections.singletonList(plots.getFirst().getTopiaId()), false);
        getPersistenceContext().getHibernateSupport().getHibernateSession().clear();//XXX Force Hibernate cache refresh to have to inactive status

        // Step two : re-import plot
        eDaplosStream = EdaplosServiceTest.class.getResourceAsStream("/edaplos/eDaplos_Evo_10787_inactive-plot.xml");
        result = edaplosService.loadAndSave(eDaplosStream);
        assertThat(result.getEdaplosParsingStatus()).isEqualTo(EdaplosParsingStatus.SUCCESS);

        // some assert on persisted plots
        plots = plotService.findAllForDomain(domain);
        assertThat(plots.size()).isEqualTo(2);
        assertThat(plots.getFirst().isActive()).isFalse();
        assertThat(plots.get(1).isActive()).isTrue();
    }
}
