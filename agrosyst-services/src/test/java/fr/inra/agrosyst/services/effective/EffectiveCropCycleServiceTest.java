package fr.inra.agrosyst.services.effective;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractAgrosystTopiaDao;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.PhytoProductTargetTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.action.PesticidesSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.SeedingActionUsageDto;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.effective.CopyPasteZoneByCampaigns;
import fr.inra.agrosyst.api.services.effective.CopyPasteZoneDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleNodeDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleSpeciesDto;
import fr.inra.agrosyst.api.services.effective.EffectiveInterventionDto;
import fr.inra.agrosyst.api.services.effective.EffectivePerennialCropCycleDto;
import fr.inra.agrosyst.api.services.effective.EffectiveSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.effective.EffectiveZoneFilter;
import fr.inra.agrosyst.api.services.effective.TargetedZones;
import fr.inra.agrosyst.api.services.input.AbstractInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductTargetDto;
import fr.inra.agrosyst.api.services.input.SeedLotInputUsageDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.measurement.MeasurementService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class EffectiveCropCycleServiceTest extends AbstractAgrosystTest {

    public static final String INTERVENTION_COPY_PREFIX = "COPY OF:";
    protected DomainService domainService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected MeasurementService measurementService;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected ReferentialService referentialService;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected AbstractDomainInputStockUnitTopiaDao domainInputStockUnitDao;
    protected HarvestingActionValorisationTopiaDao valorisationDao;
    protected SeedSpeciesInputUsageTopiaDao seedSpeciesInputUsageDao;
    protected EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected PlotTopiaDao plotDao;
    protected ZoneTopiaDao zoneDao;
    protected AbstractActionTopiaDao actionDao;

    protected TopiaJpaSupport jpaSupport;


    public static final Function<CroppingPlanSpecies, String> GET_CROPPING_PLAN_SPECIES_CODE = CroppingPlanSpecies::getCode;

    protected static final Function<SpeciesStadeDto, String> GET_SPECIES_STADE_SPECIES_CODE = input -> input == null ? null : input.getSpeciesCode();

    protected static final Function<EffectiveInterventionDto, String> GET_INTERVENTION_BY_NAME = input -> input == null ? null : input.getName();

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        domainService = serviceFactory.newService(DomainService.class);
        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        measurementService = serviceFactory.newService(MeasurementService.class);
        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        toolsCouplingDao = getPersistenceContext().getToolsCouplingDao();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        valorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();
        seedSpeciesInputUsageDao = getPersistenceContext().getSeedSpeciesInputUsageDao();
        effectiveCropCycleNodeDao = getPersistenceContext().getEffectiveCropCycleNodeDao();
        effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        effectiveSeasonalCropCycleDao = getPersistenceContext().getEffectiveSeasonalCropCycleDao();
        plotDao = getPersistenceContext().getPlotDao();
        zoneDao = getPersistenceContext().getZoneDao();
        actionDao = getPersistenceContext().getAbstractActionDao();
        domainInputStockUnitDao = getPersistenceContext().getAbstractDomainInputStockUnitDao();

        jpaSupport = getPersistenceContext().getJpaSupport();

        loginAsAdmin();
        alterSchema();
    }

    /**
     * Test la requete HQL de recherche de la derniere culture de l'année précédente.
     */
    @Test
    public void testPreviousYearCroppingPlanEntry() throws IOException {
        testDatas.importRefEspeces();

        // test datas
        testDatas.createTestEffectiveInterventions();

        // get test zone
        EffectiveZoneFilter plotFilter = new EffectiveZoneFilter();
        plotFilter.setPageSize(1);
        Zone zoneBaulon = effectiveCropCycleService.getFilteredZones(plotFilter).getElements().getFirst();
        Assertions.assertEquals("TEST_PLOT_2", zoneBaulon.getPlot().getName());

        // test get cropping plan entry
        CroppingPlanEntry croppingPlanEntry = effectiveCropCycleService.getPreviousCampaignCroppingPlanEntry(zoneBaulon.getTopiaId());
        // assert null just to test hql
        Assertions.assertNull(croppingPlanEntry);
    }

    @Test
    public void testOrderBy() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createTestEffectiveInterventions();

        // get test zone
        EffectiveZoneFilter plotFilter = new EffectiveZoneFilter();
        plotFilter.setPageSize(DaoUtils.NO_PAGE_LIMIT);//all elements
        PaginationResult<Zone> zones = effectiveCropCycleService.getFilteredZones(plotFilter);
        Assertions.assertEquals(6, zones.getElements().size());
    }

    /**
     * This test valid that duplicated Effective crop cycles works.
     * It valid that duplication does not create inconsistencies with crops and species on species stades and seeding action
     */
    @Test
    public void testDuplicationAndCopy() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createTestEffectiveInterventionsRelatedToBadSpeciesZones();

        // "Plot Baulon 1"
        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCroppingPlanEntries = Maps.uniqueIndex(croppingPlanEntries, Entities.GET_TOPIA_ID::apply);
        Map<String, CroppingPlanSpecies> zpPlotBaulon1SpeciesByCodes = new HashMap<>();
        for (CroppingPlanEntry croppingPlanEntry : indexedCroppingPlanEntries.values()) {
            List<CroppingPlanSpecies> specieses = croppingPlanEntry.getCroppingPlanSpecies();
            for (CroppingPlanSpecies species : specieses) {
                zpPlotBaulon1SpeciesByCodes.put(species.getCode(), species);
            }
        }

        List<EffectivePerennialCropCycleDto> fromPerennialCropCycleDtos = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycleDtos.getFirst();
        final List<EffectiveCropCyclePhaseDto> fromPhaseDtos = fromEffectivePerennialCropCycleDto.getPhaseDtos();

        EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDao = getPersistenceContext().getEffectiveCropCyclePhaseDao();
        List<String> fromPhaseIds = new ArrayList<>();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromPhaseDtos) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
            fromPhaseIds.add(fromEffectiveCropCyclePhaseDto.getTopiaId());
        }

        Assertions.assertTrue(effectiveCropCyclePhaseDao.forTopiaIdIn(fromPhaseIds).exists());

        List<EffectiveCropCycleSpeciesDto> fromEffectiveCropCycleSpeciesDtos = fromEffectivePerennialCropCycleDto.getSpeciesDtos();
        List<String> fromEffectiveCropCycleSpeciesIds = new ArrayList<>();
        for (EffectiveCropCycleSpeciesDto effectiveCropCycleSpeciesDto : fromEffectiveCropCycleSpeciesDtos) {
            Assertions.assertNotNull(effectiveCropCycleSpeciesDto.getCroppingPlanSpeciesId());
            final String topiaId = effectiveCropCycleSpeciesDto.getTopiaId();
            if (StringUtils.isNotBlank(topiaId)) {
                fromEffectiveCropCycleSpeciesIds.add(topiaId);
            }
        }

        EffectiveCropCycleSpeciesTopiaDao effectiveCropCycleSpeciesDao = getPersistenceContext().getEffectiveCropCycleSpeciesDao();
        if (CollectionUtils.isNotEmpty(fromEffectiveCropCycleSpeciesIds)) {
            Assertions.assertTrue(effectiveCropCycleSpeciesDao.forTopiaIdIn(fromEffectiveCropCycleSpeciesIds).exists());
        }

        CroppingPlanEntry fromPhaseCroppingPlanEntry = indexedCroppingPlanEntries.get(fromEffectivePerennialCropCycleDto.getCroppingPlanEntryId());
        Map<String, CroppingPlanSpecies> phaseSpeciesByCodes = Maps.uniqueIndex(fromPhaseCroppingPlanEntry.getCroppingPlanSpecies(), GET_CROPPING_PLAN_SPECIES_CODE::apply);


        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        List<EffectiveInterventionDto> fromPhaseInterventions = fromPhaseDtos.getFirst().getInterventions();

        EffectiveSeasonalCropCycleDto effectiveSeasonalCropCycleDto = fromSeasonalCropCycles.getFirst();
        List<EffectiveCropCycleNodeDto> fromNodesDtos = effectiveSeasonalCropCycleDto.getNodeDtos();
        List<EffectiveInterventionDto> fromNodeInterventions = new ArrayList<>();

        effectiveInterventionDao.forTopiaIdIn(fromNodesDtos.stream().map(EffectiveCropCycleNodeDto::getInterventions)
                .flatMap(Collection::stream).map(EffectiveInterventionDto::getTopiaId).toList()).findAll().forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );
        for (EffectiveCropCycleNodeDto fromNodeDto : fromNodesDtos) {
            Assertions.assertNotNull(fromNodeDto.getEdaplosIssuerId());
            List<EffectiveInterventionDto> interventions = fromNodeDto.getInterventions();
            Assertions.assertTrue(CollectionUtils.isNotEmpty(interventions));
            fromNodeInterventions.addAll(interventions);
        }

        // START DUPLICATION
        effectiveCropCycleService.duplicateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), zpPlotBaulon1.getTopiaId());

        List<EffectivePerennialCropCycleDto> perennialDuplicatedCycles = validEffectivePerennialCropCycle(
                zpPlotBaulon1,
                indexedCroppingPlanEntries,
                phaseSpeciesByCodes,
                fromPhaseInterventions,
                true,
                fromPerennialCropCycleDtos.size() * 2,
                fromPhaseDtos.size(),
                fromEffectiveCropCycleSpeciesDtos.size(),
                true);
        List<EffectiveSeasonalCropCycleDto> seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        validSeasonalCropCycle(
                indexedCroppingPlanEntries,
                zpPlotBaulon1SpeciesByCodes,
                fromNodeInterventions,
                seasonalDuplicatedCycle,
                true,
                2,
                true);

        // prepare interventions copy
        // prepare not intermediate interventions copy
        EffectiveCropCycleNodeDto nodeDto = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> notIntermediateInterventions = new ArrayList<>();
        for (EffectiveInterventionDto effectiveInterventionDto : nodeDto.getInterventions()) {
            if (!effectiveInterventionDto.isIntermediateCrop()) {
                notIntermediateInterventions.add(effectiveInterventionDto);
            }
        }
        int nbExpectedInterventions = nodeDto.getInterventions().size() + notIntermediateInterventions.size();
        String cropId = nodeDto.getCroppingPlanEntryId();
        TargetedZones notIntermediateTargetedZone = new TargetedZones();
        notIntermediateTargetedZone.setNodes(Lists.newArrayList(nodeDto.getNodeId()));
        notIntermediateTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        List<TargetedZones> notIntermediateTargetedZones = Lists.newArrayList(notIntermediateTargetedZone);
        // copy to same node, it should be exactly the same data
        for (EffectiveInterventionDto fromEffectiveInterventionDto : notIntermediateInterventions) {
            fromEffectiveInterventionDto.setName(INTERVENTION_COPY_PREFIX + fromEffectiveInterventionDto.getName());
        }

        effectiveCropCycleService.copyInterventions(notIntermediateTargetedZones, notIntermediateInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());

        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectiveCropCycleNodeDto targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> toInterventionDtos = targetedNode.getInterventions();

        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(notIntermediateInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, true);

        // copy to an other similar crop
        EffectiveCropCycleNodeDto nodeDto2 = seasonalDuplicatedCycle.getFirst().getNodeDtos().get(1);

        nbExpectedInterventions = nodeDto2.getInterventions().size() + notIntermediateInterventions.size();
        cropId = nodeDto2.getCroppingPlanEntryId();
        notIntermediateTargetedZone = new TargetedZones();
        notIntermediateTargetedZone.setNodes(Lists.newArrayList(nodeDto2.getNodeId()));
        notIntermediateTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        notIntermediateTargetedZones = Lists.newArrayList(notIntermediateTargetedZone);
        // copy to same node, it should be exactly the same data
        for (EffectiveInterventionDto fromEffectiveInterventionDto : notIntermediateInterventions) {
            fromEffectiveInterventionDto.setName("TO_OTHER_CROP_" + fromEffectiveInterventionDto.getName());
        }
        effectiveCropCycleService.copyInterventions(notIntermediateTargetedZones, notIntermediateInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());

        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().get(1);
        toInterventionDtos = targetedNode.getInterventions();

        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(notIntermediateInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, true);

        // Intermediate intervention copy
        // copy to same node, it should be exactly the same data
        targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        toInterventionDtos = targetedNode.getInterventions();

        String intermediateCropId = null;
        List<EffectiveCropCycleConnectionDto> connectionDtos = seasonalDuplicatedCycle.getFirst().getConnectionDtos();
        for (EffectiveCropCycleConnectionDto connectionDto : connectionDtos) {
            if (nodeDto.getNodeId().contentEquals(connectionDto.getTargetId())) {
                intermediateCropId = connectionDto.getIntermediateCroppingPlanEntryId();
            }
        }
        TargetedZones intermediateTargetedZone = new TargetedZones();
        intermediateTargetedZone.setNodes(Lists.newArrayList(nodeDto.getNodeId()));
        intermediateTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        List<TargetedZones> intermediateTargetedZones = Lists.newArrayList(intermediateTargetedZone);

        List<String> interventionIds = new ArrayList<>();
        List<EffectiveInterventionDto> intermediateInterventions = new ArrayList<>();
        for (EffectiveInterventionDto toInterventionDto : toInterventionDtos) {
            interventionIds.add(toInterventionDto.getTopiaId());
            if (toInterventionDto.isIntermediateCrop()) {
                toInterventionDto.setName(INTERVENTION_COPY_PREFIX + toInterventionDto.getName());
                intermediateInterventions.add(toInterventionDto);
            }
        }

        effectiveInterventionDao.forTopiaIdIn(interventionIds).findAll().forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );

        nbExpectedInterventions = targetedNode.getInterventions().size() + intermediateInterventions.size();
        effectiveCropCycleService.copyInterventions(intermediateTargetedZones, intermediateInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());
        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        toInterventionDtos = targetedNode.getInterventions();
        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(intermediateInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, true);

        interventionIds = toInterventionDtos.stream().map(EffectiveInterventionDto::getTopiaId).toList();
        effectiveInterventionDao.forTopiaIdIn(interventionIds).findAll().forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );

        // prepare perennial interventions copy
        fromPerennialCropCycleDtos = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        fromEffectivePerennialCropCycleDto = fromPerennialCropCycleDtos.getFirst();
        fromPhaseCroppingPlanEntry = indexedCroppingPlanEntries.get(fromEffectivePerennialCropCycleDto.getCroppingPlanEntryId());

        EffectiveCropCyclePhaseDto phaseDto = fromPhaseDtos.getFirst();
        Assertions.assertNotNull(phaseDto);
        fromPhaseInterventions = phaseDto.getInterventions();

        nbExpectedInterventions = fromPhaseInterventions.size() * 2;
        cropId = fromPhaseCroppingPlanEntry.getTopiaId();
        TargetedZones perennialTargetedZone = new TargetedZones();
        perennialTargetedZone.setPhases(Lists.newArrayList(phaseDto.getTopiaId()));
        perennialTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        List<TargetedZones> perennialTargetedZones = Lists.newArrayList(perennialTargetedZone);
        // copy to same node, it should be exactly the same data
        for (EffectiveInterventionDto fromEffectiveInterventionDto : fromPhaseInterventions) {
            fromEffectiveInterventionDto.setName(INTERVENTION_COPY_PREFIX + fromEffectiveInterventionDto.getName());
        }

        effectiveCropCycleService.copyInterventions(perennialTargetedZones, fromPhaseInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());

        List<EffectivePerennialCropCycleDto> toPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto toEffectivePerennialCropCycleDto = toPerennialCropCycles.getFirst();
        EffectiveCropCyclePhaseDto toPhaseDto = toEffectivePerennialCropCycleDto.getPhaseDtos().getFirst();
        Assertions.assertNotNull(toPhaseDto);
        toInterventionDtos = toPhaseDto.getInterventions();
        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(fromPhaseInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, true);

        interventionIds = toInterventionDtos.stream().map(EffectiveInterventionDto::getTopiaId).toList();
        effectiveInterventionDao.forTopiaIdIn(interventionIds).findAll().forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );

        // remove cycle removing all cycle's nodes
        for (EffectiveSeasonalCropCycleDto seasonalCropCycleDto : seasonalDuplicatedCycle) {
            seasonalCropCycleDto.setNodeDtos(new ArrayList<>());
        }

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), seasonalDuplicatedCycle, perennialDuplicatedCycles);

        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        Assertions.assertTrue(seasonalDuplicatedCycle.isEmpty());

    }

    /**
     * This test valid that duplicated Effective crop cycles works.
     * It valid that duplication does not create inconsistencies with crops and species on species stades and seeding action
     */
    @Test
    public void testDuplicationAndCopyWithInputUsages() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createTestEffectiveInterventionsRelatedToBadSpeciesZones();

        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCroppingPlanEntries = Maps.uniqueIndex(croppingPlanEntries, Entities.GET_TOPIA_ID::apply);
        Map<String, CroppingPlanSpecies> zpPlotBaulon1SpeciesByCodes = new HashMap<>();
        for (CroppingPlanEntry croppingPlanEntry : indexedCroppingPlanEntries.values()) {
            List<CroppingPlanSpecies> specieses = croppingPlanEntry.getCroppingPlanSpecies();
            for (CroppingPlanSpecies species : specieses) {
                zpPlotBaulon1SpeciesByCodes.put(species.getCode(), species);
            }
        }
        // ici
        List<EffectivePerennialCropCycleDto> fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycles.getFirst();
        final List<EffectiveCropCyclePhaseDto> fromPhaseDtos = fromEffectivePerennialCropCycleDto.getPhaseDtos();

        EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDao = getPersistenceContext().getEffectiveCropCyclePhaseDao();
        List<String> fromPhaseIds = new ArrayList<>();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromPhaseDtos) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
            fromPhaseIds.add(fromEffectiveCropCyclePhaseDto.getTopiaId());
        }

        Assertions.assertTrue(effectiveCropCyclePhaseDao.forTopiaIdIn(fromPhaseIds).exists());

        List<EffectiveCropCycleSpeciesDto> fromEffectiveCropCycleSpeciesDtos = fromEffectivePerennialCropCycleDto.getSpeciesDtos();
        List<String> fromEffectiveCropCycleSpeciesIds = new ArrayList<>();
        for (EffectiveCropCycleSpeciesDto effectiveCropCycleSpeciesDto : fromEffectiveCropCycleSpeciesDtos) {
            Assertions.assertNotNull(effectiveCropCycleSpeciesDto.getCroppingPlanSpeciesId());
            final String topiaId = effectiveCropCycleSpeciesDto.getTopiaId();
            if (StringUtils.isNotBlank(topiaId)) {
                fromEffectiveCropCycleSpeciesIds.add(topiaId);
            }
        }

        EffectiveCropCycleSpeciesTopiaDao effectiveCropCycleSpeciesDao = getPersistenceContext().getEffectiveCropCycleSpeciesDao();
        if (CollectionUtils.isNotEmpty(fromEffectiveCropCycleSpeciesIds)) {
            Assertions.assertTrue(effectiveCropCycleSpeciesDao.forTopiaIdIn(fromEffectiveCropCycleSpeciesIds).exists());
        }

        CroppingPlanEntry fromPhaseCroppingPlanEntry = indexedCroppingPlanEntries.get(fromEffectivePerennialCropCycleDto.getCroppingPlanEntryId());
        Map<String, CroppingPlanSpecies> phaseSpeciesByCodes = Maps.uniqueIndex(fromPhaseCroppingPlanEntry.getCroppingPlanSpecies(), GET_CROPPING_PLAN_SPECIES_CODE::apply);


        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        List<EffectiveInterventionDto> fromPhaseInterventions = fromPhaseDtos.getFirst().getInterventions();

        EffectiveSeasonalCropCycleDto effectiveSeasonalCropCycleDto = fromSeasonalCropCycles.getFirst();
        List<EffectiveCropCycleNodeDto> fromNodesDtos = effectiveSeasonalCropCycleDto.getNodeDtos();
        List<EffectiveInterventionDto> fromNodeInterventions = new ArrayList<>();
        for (EffectiveCropCycleNodeDto fromNodeDto : fromNodesDtos) {
            Assertions.assertNotNull(fromNodeDto.getEdaplosIssuerId());
            List<EffectiveInterventionDto> interventions = fromNodeDto.getInterventions();
            Assertions.assertTrue(CollectionUtils.isNotEmpty(interventions));
            fromNodeInterventions.addAll(interventions);
        }

        // START DUPLICATION
        effectiveCropCycleService.duplicateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), zpPlotBaulon1.getTopiaId());

        List<EffectivePerennialCropCycleDto> perennialDuplicatedCycles = validEffectivePerennialCropCycle(
                zpPlotBaulon1,
                indexedCroppingPlanEntries,
                phaseSpeciesByCodes,
                fromPhaseInterventions,
                true,
                fromPerennialCropCycles.size() * 2,
                fromPhaseDtos.size(),
                fromEffectiveCropCycleSpeciesDtos.size(), true);
        List<EffectiveSeasonalCropCycleDto> seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        validSeasonalCropCycle(
                indexedCroppingPlanEntries,
                zpPlotBaulon1SpeciesByCodes,
                fromNodeInterventions,
                seasonalDuplicatedCycle,
                true,
                2,
                true);

        // prepare interventions copy
        // prepare not intermediate interventions copy
        EffectiveCropCycleNodeDto nodeDto = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> notIntermediateInterventions = new ArrayList<>();
        for (EffectiveInterventionDto effectiveInterventionDto : nodeDto.getInterventions()) {
            if (!effectiveInterventionDto.isIntermediateCrop()) {
                notIntermediateInterventions.add(effectiveInterventionDto);
            }
        }
        int nbExpectedInterventions = nodeDto.getInterventions().size() + notIntermediateInterventions.size();
        String cropId = nodeDto.getCroppingPlanEntryId();
        TargetedZones notIntermediateTargetedZone = new TargetedZones();
        notIntermediateTargetedZone.setNodes(Lists.newArrayList(nodeDto.getNodeId()));
        notIntermediateTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        List<TargetedZones> notIntermediateTargetedZones = Lists.newArrayList(notIntermediateTargetedZone);
        // copy to same node, it should be exactly the same data
        for (EffectiveInterventionDto fromEffectiveInterventionDto : notIntermediateInterventions) {
            fromEffectiveInterventionDto.setName(INTERVENTION_COPY_PREFIX + fromEffectiveInterventionDto.getName());
        }

        effectiveCropCycleService.copyInterventions(notIntermediateTargetedZones, notIntermediateInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());

        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectiveCropCycleNodeDto targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> toInterventionDtos = targetedNode.getInterventions();

        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(notIntermediateInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, true);

        // copy to an other similar crop
        EffectiveCropCycleNodeDto nodeDto2 = seasonalDuplicatedCycle.getFirst().getNodeDtos().get(1);

        nbExpectedInterventions = nodeDto2.getInterventions().size() + notIntermediateInterventions.size();
        cropId = nodeDto2.getCroppingPlanEntryId();
        notIntermediateTargetedZone = new TargetedZones();
        notIntermediateTargetedZone.setNodes(Lists.newArrayList(nodeDto2.getNodeId()));
        notIntermediateTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        notIntermediateTargetedZones = Lists.newArrayList(notIntermediateTargetedZone);
        // copy to same node, it should be exactly the same data
        for (EffectiveInterventionDto fromEffectiveInterventionDto : notIntermediateInterventions) {
            fromEffectiveInterventionDto.setName("TO_OTHER_CROP_" + fromEffectiveInterventionDto.getName());
        }
        effectiveCropCycleService.copyInterventions(notIntermediateTargetedZones, notIntermediateInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());

        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().get(1);
        toInterventionDtos = targetedNode.getInterventions();

        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(notIntermediateInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, false);

        // Intermediate intervention copy
        // copy to same node, it should be exactly the same data
        targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        toInterventionDtos = targetedNode.getInterventions();

        String intermediateCropId = null;
        List<EffectiveCropCycleConnectionDto> connectionDtos = seasonalDuplicatedCycle.getFirst().getConnectionDtos();
        for (EffectiveCropCycleConnectionDto connectionDto : connectionDtos) {
            if (nodeDto.getNodeId().contentEquals(connectionDto.getTargetId())) {
                intermediateCropId = connectionDto.getIntermediateCroppingPlanEntryId();
            }
        }
        TargetedZones intermediateTargetedZone = new TargetedZones();
        intermediateTargetedZone.setNodes(Lists.newArrayList(nodeDto.getNodeId()));
        intermediateTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        List<TargetedZones> intermediateTargetedZones = Lists.newArrayList(intermediateTargetedZone);

        List<EffectiveInterventionDto> intermediateInterventions = new ArrayList<>();
        for (EffectiveInterventionDto toInterventionDto : toInterventionDtos) {
            if (toInterventionDto.isIntermediateCrop()) {
                toInterventionDto.setName(INTERVENTION_COPY_PREFIX + toInterventionDto.getName());
                intermediateInterventions.add(toInterventionDto);
            }
        }
        nbExpectedInterventions = targetedNode.getInterventions().size() + intermediateInterventions.size();
        effectiveCropCycleService.copyInterventions(intermediateTargetedZones, intermediateInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());
        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        targetedNode = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        toInterventionDtos = targetedNode.getInterventions();
        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(intermediateInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, true);

        // prepare perennial interventions copy
        fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        fromEffectivePerennialCropCycleDto = fromPerennialCropCycles.getFirst();
        fromPhaseCroppingPlanEntry = indexedCroppingPlanEntries.get(fromEffectivePerennialCropCycleDto.getCroppingPlanEntryId());

        EffectiveCropCyclePhaseDto phaseDto = fromPhaseDtos.getFirst();
        Assertions.assertNotNull(phaseDto);
        fromPhaseInterventions = phaseDto.getInterventions();

        nbExpectedInterventions = fromPhaseInterventions.size() * 2;
        cropId = fromPhaseCroppingPlanEntry.getTopiaId();
        TargetedZones perennialTargetedZone = new TargetedZones();
        perennialTargetedZone.setPhases(Lists.newArrayList(phaseDto.getTopiaId()));
        perennialTargetedZone.setZoneId(zpPlotBaulon1.getTopiaId());
        List<TargetedZones> perennialTargetedZones = Lists.newArrayList(perennialTargetedZone);
        // copy to same node, it should be exactly the same data
        for (EffectiveInterventionDto fromEffectiveInterventionDto : fromPhaseInterventions) {
            fromEffectiveInterventionDto.setName(INTERVENTION_COPY_PREFIX + fromEffectiveInterventionDto.getName());
        }

        effectiveCropCycleService.copyInterventions(perennialTargetedZones, fromPhaseInterventions);
        validNonDuplicatedCode(zpPlotBaulon1.getPlot().getDomain());

        List<EffectivePerennialCropCycleDto> toPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto toEffectivePerennialCropCycleDto = toPerennialCropCycles.getFirst();
        EffectiveCropCyclePhaseDto toPhaseDto = toEffectivePerennialCropCycleDto.getPhaseDtos().getFirst();
        Assertions.assertNotNull(toPhaseDto);
        toInterventionDtos = toPhaseDto.getInterventions();
        Assertions.assertEquals(nbExpectedInterventions, toInterventionDtos.size());
        validOnlyCopiedInterventions(fromPhaseInterventions, toInterventionDtos, zpPlotBaulon1SpeciesByCodes, true);

        // remove cycle removing all cycle's nodes
        for (EffectiveSeasonalCropCycleDto seasonalCropCycleDto : seasonalDuplicatedCycle) {
            seasonalCropCycleDto.setNodeDtos(new ArrayList<>());
        }

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), seasonalDuplicatedCycle, perennialDuplicatedCycles);

        seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        Assertions.assertTrue(seasonalDuplicatedCycle.isEmpty());

    }

    protected void validSeasonalCropCycle(
            Map<String, CroppingPlanEntry> indexedCroppingPlanEntries,
            Map<String, CroppingPlanSpecies> zpPlotBaulon1SpeciesByCodes,
            List<EffectiveInterventionDto> fromNodeInterventions,
            List<EffectiveSeasonalCropCycleDto> seasonalDuplicatedCycle,
            boolean isDuplicatedTest,
            int nbExpectedSeasonalCycle,
            boolean isDuplication) throws IOException {

        testDatas.importRefEspeces();
        Assertions.assertEquals(nbExpectedSeasonalCycle, seasonalDuplicatedCycle.size());
        Assertions.assertFalse(seasonalDuplicatedCycle.getFirst().getNodeDtos().isEmpty());
        List<EffectiveCropCycleNodeDto> nodes = seasonalDuplicatedCycle.getFirst().getNodeDtos();
        List<EffectiveInterventionDto> toNodeInterventions = new ArrayList<>();
        for (EffectiveCropCycleNodeDto node : nodes) {
            CroppingPlanEntry croppingPlanEntry = indexedCroppingPlanEntries.get(node.getCroppingPlanEntryId());
            Assertions.assertNotNull(croppingPlanEntry);
            toNodeInterventions.addAll(node.getInterventions());

        }
        validDuplicatedInterventions(
                fromNodeInterventions, toNodeInterventions, zpPlotBaulon1SpeciesByCodes, isDuplicatedTest, isDuplication);

    }

    protected List<EffectivePerennialCropCycleDto> validEffectivePerennialCropCycle(
            Zone zpPlotBaulon1,
            Map<String, CroppingPlanEntry> indexedCroppingPlanEntries,
            Map<String, CroppingPlanSpecies> phaseSpeciesByCodes,
            List<EffectiveInterventionDto> fromPhaseInterventions,
            boolean isDuplicatedTest,
            int nbCycles,
            int nbFromPhases,
            int nbFromSpecies,
            boolean isDuplication) throws IOException {

        testDatas.importRefEspeces();
        List<EffectivePerennialCropCycleDto> perennialDuplicatedCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        Assertions.assertEquals(nbCycles, perennialDuplicatedCycles.size());

        EffectivePerennialCropCycleDto perennialCropCycleDto = perennialDuplicatedCycles.getFirst();

        for (EffectivePerennialCropCycleDto perennialDuplicatedCycle : perennialDuplicatedCycles) {
            Assertions.assertNotNull(indexedCroppingPlanEntries.get(perennialCropCycleDto.getCroppingPlanEntryId()));

            List<EffectiveCropCyclePhaseDto> phaseDtos = perennialDuplicatedCycle.getPhaseDtos();
            Assertions.assertEquals(nbFromPhases, phaseDtos.size());
            for (EffectiveCropCyclePhaseDto phaseDto : phaseDtos) {
                List<SpeciesStadeDto> speciesStadeDtos = phaseDto.getInterventions().getFirst().getSpeciesStadesDtos();
                Assertions.assertEquals(1, speciesStadeDtos.size());
                List<EffectiveInterventionDto> perennialInterventions = phaseDto.getInterventions();
                validDuplicatedInterventions(fromPhaseInterventions, perennialInterventions, phaseSpeciesByCodes, isDuplicatedTest, isDuplication);
            }

            List<EffectiveCropCycleSpeciesDto> effectiveCropCycleSpeciesDtos = perennialDuplicatedCycle.getSpeciesDtos();
            Assertions.assertEquals(nbFromSpecies, effectiveCropCycleSpeciesDtos.size());
        }

        return perennialDuplicatedCycles;
    }

    protected void validOnlyCopiedInterventions(
            List<EffectiveInterventionDto> fromInterventions,
            List<EffectiveInterventionDto> toInterventions,
            Map<String, CroppingPlanSpecies> fromSpeciesByCode,
            boolean isDuplication) {

        for (EffectiveInterventionDto copiedInterventionDto : toInterventions) {
            if (copiedInterventionDto.getName().contains(INTERVENTION_COPY_PREFIX)) {
                String fromName = StringUtils.remove(copiedInterventionDto.getName(), INTERVENTION_COPY_PREFIX);
                for (EffectiveInterventionDto fromInterventionDto : fromInterventions) {
                    if (fromInterventionDto.getName().contains(fromName)) {
                        validFromToIntervention(fromInterventionDto, copiedInterventionDto, fromSpeciesByCode, isDuplication);
                    }
                }
            }
        }

        if (CollectionUtils.isNotEmpty(toInterventions)) {
            validateTopiaCreateDatepersisted(
                    jpaSupport,
                    effectiveInterventionDao,
                    effectiveInterventionDao.forTopiaIdIn(
                                    toInterventions.stream().map(EffectiveInterventionDto::getTopiaId).toList())
                            .findAll());
        }
    }

    protected void validInterventions(
            List<EffectiveInterventionDto> fromInterventions,
            List<EffectiveInterventionDto> toInterventions,
            Map<String, CroppingPlanSpecies> fromSpeciesByCode,
            boolean isDuplication) {

        for (EffectiveInterventionDto copiedInterventionDto : toInterventions) {
            String fromName = StringUtils.remove(copiedInterventionDto.getName(), INTERVENTION_COPY_PREFIX);
            for (EffectiveInterventionDto fromInterventionDto : fromInterventions) {
                if (fromInterventionDto.getName().contains(fromName)) {
                    validFromToIntervention(fromInterventionDto, copiedInterventionDto, fromSpeciesByCode, isDuplication);
                }
            }
        }
    }

    public void validDuplicatedInterventions(
            List<EffectiveInterventionDto> fromInterventionDtos,
            List<EffectiveInterventionDto> toInterventionDtos,
            Map<String, CroppingPlanSpecies> zpPlotBaulon1SpeciesByCode,
            boolean isDuplicatedTest,
            boolean isDuplication) {

        if (isDuplicatedTest) {
            Assertions.assertNotNull(toInterventionDtos);
            Assertions.assertFalse(toInterventionDtos.isEmpty());

            Map<String, EffectiveInterventionDto> toInterventionsByNames = Maps.uniqueIndex(toInterventionDtos, GET_INTERVENTION_BY_NAME::apply);
            for (EffectiveInterventionDto fromInterventionDto : fromInterventionDtos) {
                EffectiveInterventionDto toIntervention = toInterventionsByNames.get(fromInterventionDto.getName());
                validFromToIntervention(
                        fromInterventionDto,
                        toIntervention,
                        zpPlotBaulon1SpeciesByCode,
                        isDuplication);
            }
        }

        if (CollectionUtils.isNotEmpty(toInterventionDtos)) {
            validateTopiaCreateDatepersisted(
                    jpaSupport,
                    effectiveInterventionDao,
                    effectiveInterventionDao.forTopiaIdIn(
                            toInterventionDtos.stream().map(EffectiveInterventionDto::getTopiaId).toList())
                            .findAll());

        }
    }


    public static <D extends AbstractAgrosystTopiaDao>  void validateTopiaCreateDatepersisted(
            TopiaJpaSupport jpaSupport,
            D dao,
            List<? extends TopiaEntity> entities) {

        dao.forTopiaIdIn(entities.stream().map(TopiaEntity::getTopiaId).toList())
                .findAll()
                .forEach(
                        e -> {
                            Date topiaCreateDate = jpaSupport.findUnique(
                                    "SELECT e." + TopiaEntity.PROPERTY_TOPIA_CREATE_DATE +
                                            " FROM " + dao.getEntityClass().getName() + " e WHERE e.topiaId = :topiaId",
                                    DaoUtils.asArgsMap("topiaId", ((TopiaEntity) e).getTopiaId()));
                            Assertions.assertNotNull(topiaCreateDate);
                        });
    }

    protected void validFromToIntervention(
            EffectiveInterventionDto fromIntervention,
            EffectiveInterventionDto toIntervention,
            Map<String, CroppingPlanSpecies> fromSpeciesByCode, boolean isDuplication) {

        if (!TestDatas.INVALID_INTERVENTION_NAME.contains(fromIntervention.getName())) {
            Assertions.assertNotNull(toIntervention);

            Set<String> fromToolsCouplingsCodes = fromIntervention.getToolsCouplingCodes();

            List<SpeciesStadeDto> fromSpeciesStadeDtos = fromIntervention.getSpeciesStadesDtos();

            Set<String> toToolsCouplingsCodes = toIntervention.getToolsCouplingCodes();
            List<SpeciesStadeDto> toSpeciesStadeDtos = toIntervention.getSpeciesStadesDtos();
            Assertions.assertEquals(fromSpeciesStadeDtos.size(), toSpeciesStadeDtos.size());
            Assertions.assertEquals(fromIntervention.getComment(), toIntervention.getComment());
            Assertions.assertEquals(fromIntervention.getStartInterventionDate(), toIntervention.getStartInterventionDate());
            Assertions.assertEquals(fromIntervention.isIntermediateCrop(), toIntervention.isIntermediateCrop());
            Assertions.assertEquals(fromIntervention.getOtherToolSettings(), toIntervention.getOtherToolSettings());
            Assertions.assertEquals(fromIntervention.getWorkRate(), toIntervention.getWorkRate());
            Assertions.assertEquals(fromIntervention.getWorkRateUnit(), toIntervention.getWorkRateUnit());
            Assertions.assertEquals(fromIntervention.getProgressionSpeed(), toIntervention.getProgressionSpeed());
            Assertions.assertEquals(fromIntervention.getInvolvedPeopleCount(), toIntervention.getInvolvedPeopleCount());
            Assertions.assertEquals(fromIntervention.getEndInterventionDate(), toIntervention.getEndInterventionDate());
            Assertions.assertEquals(fromIntervention.getTransitCount(), toIntervention.getTransitCount());
            Assertions.assertEquals(fromIntervention.getTransitVolume(), toIntervention.getTransitVolume());
            Assertions.assertEquals(fromIntervention.getTransitVolumeUnit(), toIntervention.getTransitVolumeUnit());
            Assertions.assertEquals(fromIntervention.getNbBalls(), toIntervention.getNbBalls());
            Assertions.assertEquals(fromIntervention.getSpatialFrequency(), toIntervention.getSpatialFrequency());

            validSpeciesStades(fromSpeciesStadeDtos, fromIntervention, toIntervention, fromSpeciesByCode);
            validToolsCouplingCode(fromToolsCouplingsCodes, toToolsCouplingsCodes);
            validActionAndUsages(fromIntervention, toIntervention, isDuplication);
        }

    }

    protected void validActionAndUsages(
            EffectiveInterventionDto fromInterventionDto,
            EffectiveInterventionDto toInterventionDto,
            Boolean isDuplication) {

        if (CollectionUtils.isNotEmpty(fromInterventionDto.getActionDtos())) {
            org.assertj.core.api.Assertions.assertThat(toInterventionDto.getActionDtos()).isNotEmpty();

            Map<String, AbstractActionDto> fromActionDtoByMainActionId = new HashMap<>();

            fromInterventionDto.getActionDtos().forEach(
                    fromActionDto -> fromActionDtoByMainActionId.put(fromActionDto.getMainActionId(), fromActionDto)
            );
            Map<String, AbstractActionDto> toActionDtoByMainActionId = new HashMap<>();
            toInterventionDto.getActionDtos().forEach(
                    toActionDto -> toActionDtoByMainActionId.put(toActionDto.getMainActionId(), toActionDto)
            );
            Assertions.assertEquals(fromActionDtoByMainActionId.values().size(), toActionDtoByMainActionId.values().size());

            for (Map.Entry<String, AbstractActionDto> mainActionIdToActionDto : fromActionDtoByMainActionId.entrySet()) {
                AbstractActionDto toActionDto = toActionDtoByMainActionId.get(mainActionIdToActionDto.getKey());
                Assertions.assertNotNull(toActionDto);
                AbstractActionDto fromActionDto = mainActionIdToActionDto.getValue();
                MultiValuedMap<InputType, AbstractInputUsageDto> fromUsageDtoByTypes = testDatas.getActionDtoUsageDtoByTypes(fromActionDto);
                MultiValuedMap<InputType, AbstractInputUsageDto> toUsageDtoByTypes = testDatas.getActionDtoUsageDtoByTypes(toActionDto);
                for (InputType fromInputType : fromUsageDtoByTypes.keySet()) {
                    Map<String, AbstractInputUsageDto> fromUsageDtoByCode = fromUsageDtoByTypes.get(fromInputType).stream().collect(Collectors.toMap((AbstractInputUsageDto::getCode), Function.identity()));
                    Map<String, AbstractInputUsageDto> toUsageDtosByCode = toUsageDtoByTypes.get(fromInputType).stream().collect(Collectors.toMap((AbstractInputUsageDto::getCode), Function.identity()));
                    for (Map.Entry<String, AbstractInputUsageDto> codeToFromFusage : fromUsageDtoByCode.entrySet()) {
                        AbstractInputUsageDto toUsageDto = toUsageDtosByCode.get(codeToFromFusage.getKey());
                        if (isDuplication && toUsageDto == null) {
                            // TODO validate
                            continue;
                        }
                        testDatas.compaireAndValideUsage(
                                codeToFromFusage.getValue(),
                                toUsageDto
                        );
                    }
                }
            }
        }
    }

    protected void validSpeciesStades(
            List<SpeciesStadeDto> fromSpeciesStades,
            EffectiveInterventionDto fromInterventionDto,
            EffectiveInterventionDto toIntervention,
            Map<String, CroppingPlanSpecies> fromSpeciesByCode) {
        List<SpeciesStadeDto> toSpeciesStades = toIntervention.getSpeciesStadesDtos();
        // comme la copie est sur le même noeud on doit avoir exactement les mêmes données
        Map<String, SpeciesStadeDto> speciesStadeDtoBySpeciesCodes = Maps.uniqueIndex(fromSpeciesStades, GET_SPECIES_STADE_SPECIES_CODE::apply);
        for (SpeciesStadeDto toSpeciesStade : toSpeciesStades) {
            Assertions.assertTrue(StringUtils.isNoneBlank(toSpeciesStade.getSpeciesCode()));
            CroppingPlanSpecies species = fromSpeciesByCode.get(toSpeciesStade.getSpeciesCode());
            if (!toIntervention.getName().contains("Baulon seasonnal intervention_n1")) {
                Assertions.assertNotNull(species);
                if (fromInterventionDto.getFromCropCode().equals(toIntervention.getFromCropCode()) &&
                        fromInterventionDto.isIntermediateCrop() == toIntervention.isIntermediateCrop()) {
                    SpeciesStadeDto fromSpeciesStade = speciesStadeDtoBySpeciesCodes.get(toSpeciesStade.getSpeciesCode());
                    Assertions.assertNotNull(fromSpeciesStade);
                    if (fromSpeciesStade.getStadeMin() != null) {
                        Assertions.assertTrue(fromSpeciesStade.getStadeMin().getTopiaId().contentEquals(toSpeciesStade.getStadeMin().getTopiaId()));
                    }
                    if (fromSpeciesStade.getStadeMax() != null) {
                        Assertions.assertTrue(fromSpeciesStade.getStadeMax().getTopiaId().contentEquals(toSpeciesStade.getStadeMax().getTopiaId()));
                    }
                }
            } else {
                Assertions.assertNull(toSpeciesStade.getStadeMax());
                Assertions.assertNull(toSpeciesStade.getStadeMin());
                Assertions.assertNotNull(toSpeciesStade.getSpeciesCode());
            }
        }
    }

    protected void validToolsCouplingCode(Set<String> fromToolsCouplingsCodes, Set<String> toToolsCouplingsCodes) {
        for (String fromToolsCouplingsCode : CollectionUtils.emptyIfNull(fromToolsCouplingsCodes)) {
            Assertions.assertTrue(toToolsCouplingsCodes.contains(fromToolsCouplingsCode));
        }
    }

    /**
     * Test the following cases:
     * must not be return zones where:
     * - zone.active = false
     * - zone.plot.active = false
     * - zone.domain.active = false
     * - zone.growingSystem.active = false
     */
    @Test
    public void testGetZonesForCopy() throws IOException {
        testDatas.importRefEspeces();
        testDatas.createTestZones();
        CopyPasteZoneByCampaigns copyPasteZoneByCampaigns = effectiveCropCycleService.getAvailableZonesForCopy("ZONE_TEST_ID");
        HashMap<Integer, List<CopyPasteZoneDto>> zonesByCampaigns = copyPasteZoneByCampaigns.getZonesByCampaigns();
        List<CopyPasteZoneDto> zones2014 = zonesByCampaigns.get(2014);
        Assertions.assertEquals(4, zones2014.size());
        for (CopyPasteZoneDto copyPasteZoneDto : zones2014) {
            Assertions.assertTrue(copyPasteZoneDto.getZoneName().contains("OK"));
        }
    }

    @Test
    public void testUpdateEffectiveCropCycles() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createEffectiveInterventions();

        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCroppingPlanEntries = Maps.uniqueIndex(croppingPlanEntries, Entities.GET_TOPIA_ID::apply);
        Map<String, CroppingPlanSpecies> zpPlotBaulon1SpeciesByCodes = new HashMap<>();
        for (CroppingPlanEntry croppingPlanEntry : indexedCroppingPlanEntries.values()) {
            List<CroppingPlanSpecies> specieses = croppingPlanEntry.getCroppingPlanSpecies();
            for (CroppingPlanSpecies species : specieses) {
                zpPlotBaulon1SpeciesByCodes.put(species.getCode(), species);
            }
        }

        List<EffectivePerennialCropCycleDto> fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycles.getFirst();
        final List<EffectiveCropCyclePhaseDto> fromPhaseDtos = fromEffectivePerennialCropCycleDto.getPhaseDtos();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromPhaseDtos) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
        }

        List<EffectiveCropCycleSpeciesDto> fromEffectiveCropCycleSpeciesDtos = fromEffectivePerennialCropCycleDto.getSpeciesDtos();
        List<String> fromEffectiveCropCycleSpeciesIds = new ArrayList<>();
        for (EffectiveCropCycleSpeciesDto effectiveCropCycleSpeciesDto : fromEffectiveCropCycleSpeciesDtos) {
            Assertions.assertNotNull(effectiveCropCycleSpeciesDto.getCroppingPlanSpeciesId());
            final String topiaId = effectiveCropCycleSpeciesDto.getTopiaId();
            if (StringUtils.isNotBlank(topiaId)) {
                fromEffectiveCropCycleSpeciesIds.add(topiaId);
            }
        }

        EffectiveCropCycleSpeciesTopiaDao effectiveCropCycleSpeciesDao = getPersistenceContext().getEffectiveCropCycleSpeciesDao();
        if (CollectionUtils.isNotEmpty(fromEffectiveCropCycleSpeciesIds)) {
            Assertions.assertTrue(effectiveCropCycleSpeciesDao.forTopiaIdIn(fromEffectiveCropCycleSpeciesIds).exists());
        }

        CroppingPlanEntry fromPhaseCroppingPlanEntry = indexedCroppingPlanEntries.get(fromEffectivePerennialCropCycleDto.getCroppingPlanEntryId());
        Map<String, CroppingPlanSpecies> phaseSpeciesByCodes = Maps.uniqueIndex(fromPhaseCroppingPlanEntry.getCroppingPlanSpecies(), GET_CROPPING_PLAN_SPECIES_CODE::apply);


        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        List<EffectiveInterventionDto> fromPhaseInterventions = fromEffectivePerennialCropCycleDto.getPhaseDtos().getFirst().getInterventions();

        EffectiveSeasonalCropCycleDto effectiveSeasonalCropCycleDto = fromSeasonalCropCycles.getFirst();
        List<EffectiveCropCycleNodeDto> fromNodesDtos = effectiveSeasonalCropCycleDto.getNodeDtos();
        List<EffectiveInterventionDto> fromNodeInterventions = new ArrayList<>();
        for (EffectiveCropCycleNodeDto fromNodeDto : fromNodesDtos) {
            List<EffectiveInterventionDto> interventions = fromNodeDto.getInterventions();
            fromNodeInterventions.addAll(interventions);
        }

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        List<EffectiveSeasonalCropCycleDto> seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        validEffectivePerennialCropCycle(
                zpPlotBaulon1,
                indexedCroppingPlanEntries,
                phaseSpeciesByCodes,
                fromPhaseInterventions,
                false,
                fromPerennialCropCycles.size(),
                fromPhaseDtos.size(),
                fromEffectiveCropCycleSpeciesDtos.size(),
                false);
        validSeasonalCropCycle(
                indexedCroppingPlanEntries,
                zpPlotBaulon1SpeciesByCodes,
                fromNodeInterventions,
                seasonalDuplicatedCycle,
                false,
                1,
                false);

    }

    @Test
    public void testRemoveInterventionFromNode() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createEffectiveInterventions();

        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);

        List<EffectivePerennialCropCycleDto> fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycles.getFirst();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromEffectivePerennialCropCycleDto.getPhaseDtos()) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
        }
        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        EffectiveCropCycleNodeDto nodeDto = fromSeasonalCropCycles.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> interventionDtos = nodeDto.getInterventions();

        effectiveInterventionDao.forTopiaIdIn(interventionDtos.stream().map(EffectiveInterventionDto::getTopiaId).toList()).findAll().forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );

        EffectiveInterventionDto removedInterventionDto = interventionDtos.remove(0);

        Collection<AbstractActionDto> actionsToRemoves = removedInterventionDto.getActionDtos();
        List<String> removedActionIds = Lists.newArrayList(removedInterventionDto.getActionDtos().stream()
                .filter(ca -> ca.getTopiaId().isPresent()).map(ca -> ca.getTopiaId().get()).toList());

        List<AbstractActionDto> harvestingActionDtos = actionsToRemoves.stream()
                .filter(abstractAction -> AgrosystInterventionType.RECOLTE.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();

        List<String> valorisationIds = harvestingActionDtos.stream()
                .map(ha -> ((HarvestingActionDto) ha).getValorisationDtos())
                .flatMap(Collection::stream)
                .map(HarvestingActionValorisationDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        List<AbstractActionDto> seedingActionDtos = actionsToRemoves.stream()
                .filter(abstractAction -> AgrosystInterventionType.SEMIS.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();

        List<String> seedingActionSpecieIds = new ArrayList<>();
        for (AbstractActionDto seedingActionDto : seedingActionDtos) {
            seedingActionSpecieIds.addAll(((SeedingActionUsageDto) seedingActionDto).getSeedLotInputUsageDtos()
                    .stream()
                    .map(SeedLotInputUsageDto::getSeedingSpeciesDtos)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .flatMap(Collection::stream)
                    .map(AbstractInputUsageDto::getInputUsageTopiaId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList());
        }

        AbstractActionTopiaDao actionDao = getPersistenceContext().getAbstractActionDao();
        Assertions.assertTrue(actionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertTrue(valorisationDao.forTopiaIdIn(valorisationIds).exists());
        Assertions.assertTrue(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);


        effectiveInterventionDao.forTopiaIdIn(effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId())
                .stream().map(EffectiveSeasonalCropCycleDto::getNodeDtos)
                .flatMap(Collection::stream)
                .map(EffectiveCropCycleNodeDto::getInterventions)
                .flatMap(Collection::stream)
                .map(EffectiveInterventionDto::getTopiaId).toList()).findAll()
                .forEach(
                i -> Assertions.assertNotNull(i.getTopiaCreateDate())
        );

        Assertions.assertFalse(effectiveInterventionDao.forTopiaIdEquals(removedInterventionDto.getTopiaId()).exists());
        Assertions.assertFalse(actionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertFalse(valorisationDao.forTopiaIdIn(valorisationIds).exists());
        Assertions.assertFalse(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

    }

    @Test
    public void testRemoveInputFromNode() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createEffectiveInterventions();

        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);

        List<EffectivePerennialCropCycleDto> fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycles.getFirst();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromEffectivePerennialCropCycleDto.getPhaseDtos()) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
        }
        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        EffectiveCropCycleNodeDto nodeDto = fromSeasonalCropCycles.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> interventionDtos = nodeDto.getInterventions();
        EffectiveInterventionDto currentInterventionDto = interventionDtos.getFirst();

        Collection<AbstractActionDto> currentActionDtos = currentInterventionDto.getActionDtos();
        List<String> currentActionIds = Lists.newArrayList(currentActionDtos.stream()
                .filter(ca -> ca.getTopiaId().isPresent()).map(ca -> ca.getTopiaId().get()).toList());

        final List<AbstractActionDto> phytoAbstractActionDtos = currentActionDtos.stream().filter(ca -> AgrosystInterventionType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES.equals(ca.getMainActionInterventionAgrosyst())).toList();

        final List<PhytoProductTargetDto> phytoProductTargetDtos = phytoAbstractActionDtos.stream().map(aa -> ((PesticidesSpreadingActionDto) aa).getPhytoProductInputUsageDtos())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMap(Collection::stream)
                .map(PhytoProductInputUsageDto::getTargets)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMap(Collection::stream)
                .toList();

        Assertions.assertFalse(phytoProductTargetDtos.isEmpty());

        List<AbstractActionDto> harvestingActionDtos = currentActionDtos.stream()
                .filter(abstractAction -> AgrosystInterventionType.RECOLTE.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();

        List<String> valorisationIds = harvestingActionDtos.stream()
                .map(ha -> ((HarvestingActionDto) ha).getValorisationDtos())
                .flatMap(Collection::stream)
                .map(HarvestingActionValorisationDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        List<AbstractActionDto> seedingActionDtos = currentActionDtos.stream()
                .filter(abstractAction -> AgrosystInterventionType.SEMIS.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();
        List<String> seedingActionSpecieIds = new ArrayList<>();
        for (AbstractActionDto seedingActionDto : seedingActionDtos) {
            seedingActionSpecieIds.addAll(((SeedingActionUsageDto) seedingActionDto).getSeedLotInputUsageDtos()
                    .stream()
                    .map(SeedLotInputUsageDto::getSeedingSpeciesDtos)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .flatMap(Collection::stream)
                    .map(AbstractInputUsageDto::getInputUsageTopiaId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList());
        }

        AbstractActionTopiaDao actionDao = getPersistenceContext().getAbstractActionDao();
        Assertions.assertTrue(actionDao.forTopiaIdIn(currentActionIds).exists());

        Assertions.assertTrue(valorisationDao.forTopiaIdIn(valorisationIds).exists());

        Assertions.assertTrue(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        EffectiveInterventionTopiaDao interventionDao = getPersistenceContext().getEffectiveInterventionDao();
        Assertions.assertTrue(interventionDao.forTopiaIdEquals(currentInterventionDto.getTopiaId()).exists());
        Assertions.assertTrue(actionDao.forTopiaIdIn(currentActionIds).exists());
        Assertions.assertTrue(valorisationDao.forTopiaIdIn(valorisationIds).exists());

        Assertions.assertTrue(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

        List<String> targetIds = phytoProductTargetDtos.stream().map(PhytoProductTargetDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
        PhytoProductTargetTopiaDao phytoProductTargetDao = getPersistenceContext().getPhytoProductTargetDao();
        Assertions.assertFalse(phytoProductTargetDao.forTopiaIdIn(targetIds).findAll().isEmpty());

    }

    @Test
    public void testRemoveActionFromNode() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createEffectiveInterventions();

        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        List<EffectivePerennialCropCycleDto> fromPerennialCropCycleDtos = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycleDtos.getFirst();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromEffectivePerennialCropCycleDto.getPhaseDtos()) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
        }
        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycleDtos = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycleDtos, fromPerennialCropCycleDtos);

        fromSeasonalCropCycleDtos = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        EffectiveCropCycleNodeDto nodeDto = fromSeasonalCropCycleDtos.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> interventionDtos = nodeDto.getInterventions();
        EffectiveInterventionDto currentInterventionDto = interventionDtos.getFirst();

        Collection<AbstractActionDto> currentActionDtos = currentInterventionDto.getActionDtos();
        List<String> currentActionIds = Lists.newArrayList(currentActionDtos.stream()
                .filter(ca -> ca.getTopiaId().isPresent()).map(ca -> ca.getTopiaId().get()).toList());

        List<AbstractActionDto> harvestingActionDtos = currentActionDtos.stream()
                .filter(abstractAction -> AgrosystInterventionType.RECOLTE.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();
        List<String> removedActionIds = harvestingActionDtos.stream()
                .filter(aa -> aa.getTopiaId().isPresent())
                .map(aa -> aa.getTopiaId().get()).toList();


        List<String> valorisationIds = harvestingActionDtos.stream()
                .map(ha -> ((HarvestingActionDto) ha).getValorisationDtos())
                .flatMap(Collection::stream)
                .map(HarvestingActionValorisationDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        List<AbstractActionDto> seedingActionDtos = currentActionDtos.stream()
                .filter(abstractAction -> AgrosystInterventionType.SEMIS.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();
        List<String> seedingActionSpecieIds = new ArrayList<>();
        for (AbstractActionDto seedingActionDto : seedingActionDtos) {
            seedingActionSpecieIds.addAll(((SeedingActionUsageDto) seedingActionDto).getSeedLotInputUsageDtos()
                    .stream()
                    .map(SeedLotInputUsageDto::getSeedingSpeciesDtos)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .flatMap(Collection::stream)
                    .map(AbstractInputUsageDto::getInputUsageTopiaId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList());
        }

        AbstractActionTopiaDao actionDao = getPersistenceContext().getAbstractActionDao();
        Assertions.assertTrue(actionDao.forTopiaIdIn(currentActionIds).exists());

        Assertions.assertTrue(valorisationDao.forTopiaIdIn(valorisationIds).exists());

        Assertions.assertTrue(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycleDtos, fromPerennialCropCycleDtos);

        EffectiveInterventionTopiaDao interventionDao = getPersistenceContext().getEffectiveInterventionDao();
        Assertions.assertTrue(interventionDao.forTopiaIdEquals(currentInterventionDto.getTopiaId()).exists());

        Assertions.assertTrue(actionDao.forTopiaIdIn(currentActionIds).exists());
        Assertions.assertTrue(valorisationDao.forTopiaIdIn(valorisationIds).exists());
        Assertions.assertTrue(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

        currentActionDtos.removeAll(harvestingActionDtos);

        List<AbstractActionDto> newInterventionActionDtos = new ArrayList<>(currentInterventionDto.getActionDtos());
        for (AbstractActionDto seedingActionDto : seedingActionDtos) {
            newInterventionActionDtos.remove(seedingActionDto);
            final SeedingActionUsageDto persistedActionDto = (SeedingActionUsageDto) seedingActionDto;
            final Collection<SeedLotInputUsageDto> persistedSeedLotInputUsageDtos = persistedActionDto.getSeedLotInputUsageDtos();
            List<SeedLotInputUsageDto> newSeedLotInputUsages = new ArrayList<>();
            persistedSeedLotInputUsageDtos.forEach(
                    sliu -> newSeedLotInputUsages.add(sliu.toBuilder().seedingSpeciesDtos(null).build())
            );
            final SeedingActionUsageDto newSeedingActionDto = persistedActionDto.toBuilder()
                    .seedLotInputUsageDtos(newSeedLotInputUsages)
                    .build();
            newInterventionActionDtos.add(newSeedingActionDto);
            currentInterventionDto.setActionDtos(newInterventionActionDtos);

            seedingActionSpecieIds.addAll(((SeedingActionUsageDto) seedingActionDto).getSeedLotInputUsageDtos()
                    .stream()
                    .map(SeedLotInputUsageDto::getSeedingSpeciesDtos)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .flatMap(Collection::stream)
                    .map(AbstractInputUsageDto::getInputUsageTopiaId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList());
        }

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycleDtos, fromPerennialCropCycleDtos);
        Assertions.assertFalse(actionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertFalse(valorisationDao.forTopiaIdIn(valorisationIds).exists());
        Assertions.assertFalse(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());
    }

    @Test
    public void testRemoveNode() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createEffectiveInterventions();

        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);

        List<EffectivePerennialCropCycleDto> fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycles.getFirst();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromEffectivePerennialCropCycleDto.getPhaseDtos()) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
        }

        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());

        EffectiveCropCycleNodeDto nodeDto = fromSeasonalCropCycles.getFirst().getNodeDtos().remove(0);
        List<EffectiveInterventionDto> interventionDtos = nodeDto.getInterventions();

        List<String> removedInterventionIds = new ArrayList<>();
        List<AbstractActionDto> actionsToRemoves = new ArrayList<>();
        for (EffectiveInterventionDto removedInterventionDto : interventionDtos) {
            removedInterventionIds.add(removedInterventionDto.getTopiaId());
            actionsToRemoves.addAll(removedInterventionDto.getActionDtos());
        }
        List<String> removedActionIds = Lists.newArrayList(actionsToRemoves.stream()
                .filter(ca -> ca.getTopiaId().isPresent()).map(ca -> ca.getTopiaId().get()).toList());

        List<AbstractActionDto> harvestingActionDtos = actionsToRemoves.stream()
                .filter(abstractAction -> AgrosystInterventionType.RECOLTE.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();

        List<String> valorisationIds = harvestingActionDtos.stream()
                .map(ha -> ((HarvestingActionDto) ha).getValorisationDtos())
                .flatMap(Collection::stream)
                .map(HarvestingActionValorisationDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();

        List<AbstractActionDto> seedingActionDtos = actionsToRemoves.stream()
                .filter(abstractAction -> AgrosystInterventionType.SEMIS.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();
        List<String> seedingActionSpecieIds = new ArrayList<>();
        for (AbstractActionDto seedingActionDto : seedingActionDtos) {
            seedingActionSpecieIds.addAll(((SeedingActionUsageDto) seedingActionDto).getSeedLotInputUsageDtos()
                    .stream()
                    .map(SeedLotInputUsageDto::getSeedingSpeciesDtos)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .flatMap(Collection::stream)
                    .map(AbstractInputUsageDto::getInputUsageTopiaId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList());
        }

        EffectiveInterventionTopiaDao interventionDao = getPersistenceContext().getEffectiveInterventionDao();
        Assertions.assertTrue(interventionDao.forTopiaIdIn(removedInterventionIds).exists());

        AbstractActionTopiaDao actionDao = getPersistenceContext().getAbstractActionDao();
        Assertions.assertTrue(actionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertTrue(valorisationDao.forTopiaIdIn(valorisationIds).exists());
        Assertions.assertTrue(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        Assertions.assertFalse(interventionDao.forTopiaIdIn(removedInterventionIds).exists());
        Assertions.assertFalse(actionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertFalse(valorisationDao.forTopiaIdIn(valorisationIds).exists());
        Assertions.assertFalse(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

    }

    @Test
    public void testRemovePerennialCycle() throws IOException {
        testDatas.importRefEspeces();
        // test datas
        testDatas.createEffectiveInterventions();

        getPersistenceContext().commit();

        // "Plot Baulon 2"
        // "Plot Couëron les bains 1"
        // "Plot Couëron les bains 2"
        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();

        testDatas.getCroppingPlanEntriesForZone(zpPlotBaulon1);

        List<EffectivePerennialCropCycleDto> fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto fromEffectivePerennialCropCycleDto = fromPerennialCropCycles.getFirst();
        for (EffectiveCropCyclePhaseDto fromEffectiveCropCyclePhaseDto : fromEffectivePerennialCropCycleDto.getPhaseDtos()) {
            Assertions.assertNotNull(fromEffectiveCropCyclePhaseDto.getEdaplosIssuerId());
        }

        List<EffectiveSeasonalCropCycleDto> fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        fromSeasonalCropCycles = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        fromPerennialCropCycles = effectiveCropCycleService.getAllEffectivePerennialCropCyclesDtos(zpPlotBaulon1.getTopiaId());
        EffectivePerennialCropCycleDto effectivePerennialCropCycleDto = fromPerennialCropCycles.remove(0);
        EffectiveCropCyclePhaseDto currentPhase = effectivePerennialCropCycleDto.getPhaseDtos().getFirst();

        List<EffectiveInterventionDto> interventionDtos = currentPhase.getInterventions();

        List<String> removedInterventionIds = new ArrayList<>();
        List<AbstractActionDto> actionsToRemoves = new ArrayList<>();

        for (EffectiveInterventionDto removedInterventionDto : interventionDtos) {
            removedInterventionIds.add(removedInterventionDto.getTopiaId());
            actionsToRemoves.addAll(removedInterventionDto.getActionDtos());
        }
        List<String> removedActionIds = Lists.newArrayList(actionsToRemoves.stream()
                .filter(ca -> ca.getTopiaId().isPresent()).map(ca -> ca.getTopiaId().get()).toList());


        List<AbstractActionDto> harvestingActionDtos = actionsToRemoves.stream()
                .filter(abstractAction -> AgrosystInterventionType.RECOLTE.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();

        List<String> valorisationIds = harvestingActionDtos.stream()
                .map(ha -> ((HarvestingActionDto) ha).getValorisationDtos())
                .flatMap(Collection::stream)
                .map(HarvestingActionValorisationDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();


        List<AbstractActionDto> seedingActionDtos = actionsToRemoves.stream()
                .filter(abstractAction -> AgrosystInterventionType.SEMIS.equals(abstractAction.getMainActionInterventionAgrosyst()))
                .toList();
        List<String> seedingActionSpecieIds = new ArrayList<>();
        for (AbstractActionDto seedingActionDto : seedingActionDtos) {
            seedingActionSpecieIds.addAll(((SeedingActionUsageDto) seedingActionDto).getSeedLotInputUsageDtos()
                    .stream()
                    .map(SeedLotInputUsageDto::getSeedingSpeciesDtos)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .flatMap(Collection::stream)
                    .map(AbstractInputUsageDto::getInputUsageTopiaId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .toList());
        }

        EffectiveInterventionTopiaDao interventionDao = getPersistenceContext().getEffectiveInterventionDao();
        Assertions.assertTrue(interventionDao.forTopiaIdIn(removedInterventionIds).exists());

        AbstractActionTopiaDao actionDao = getPersistenceContext().getAbstractActionDao();
        Assertions.assertTrue(actionDao.forTopiaIdIn(removedActionIds).exists());

        Assertions.assertTrue(valorisationDao.forTopiaIdIn(valorisationIds).exists());


        Assertions.assertTrue(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

        effectiveCropCycleService.updateEffectiveCropCycles(zpPlotBaulon1.getTopiaId(), fromSeasonalCropCycles, fromPerennialCropCycles);

        Assertions.assertFalse(interventionDao.forTopiaIdIn(removedInterventionIds).exists());
        Assertions.assertFalse(actionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertFalse(valorisationDao.forTopiaIdIn(valorisationIds).exists());
        Assertions.assertFalse(seedSpeciesInputUsageDao.forTopiaIdIn(seedingActionSpecieIds).exists());

    }

    protected void validNonDuplicatedCode(Domain domain) {

        List<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domain).findAll();

        Set<String> allCodes = new HashSet<>();
        domainInputStockUnits.forEach(disu -> {
            org.assertj.core.api.Assertions.assertThat(disu.getCode()).isNotEmpty();
            org.assertj.core.api.Assertions.assertThat(allCodes.contains(disu.getCode())).isFalse();
            allCodes.add(disu.getCode());
        });

    }

    @Test
    public void testCopyEffectiveInterventionAndUsagesOnDomainWithoutInput() throws IOException, DomainExtendException {

        String prefixId = "2012_";
        Domain d2012 = testDatas.createDomainInraContextRef9696();
        testDatas.createZoneInraContextRef9696(prefixId, d2012);

        // Domaine cible
        Domain d2024 = domainService.extendDomain(d2012.getTopiaId(), 2024);
        CroppingPlanEntry fromCourgetteCrop = croppingPlanEntryDao.forTopiaIdEquals("d0_cp0").findUnique();
        Assertions.assertEquals(d2012, fromCourgetteCrop.getDomain());

        // création de l'intervention et des intrants sur 2012
        String interventionId = prefixId + "_2016_effectiveMineralProductInterventionOnNode";
        EffectiveInterventionTopiaDao effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        EffectiveIntervention effectiveIntervention = effectiveInterventionDao.forTopiaIdEquals(interventionId).findUnique();
        // add some usages
        testDatas.createEffectiveMineralProductUsages(d2012, effectiveIntervention);
        testDatas.createEffectivePhytoProductUsages(d2012, effectiveIntervention);
        testDatas.createEffectiveSeedLotUsages(d2012, fromCourgetteCrop, effectiveIntervention);

        Plot plot2012 = plotDao.forDomainEquals(d2012).findUnique();
        Zone zone2012 = zoneDao.forPlotEquals(plot2012).findUnique();
        List<EffectiveSeasonalCropCycleDto> seasonalDuplicatedCycle2012 = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zone2012.getTopiaId());

        // prepare interventions copy
        // prepare not intermediate interventions copy
        EffectiveCropCycleNodeDto nodeDto2012 = seasonalDuplicatedCycle2012.getFirst().getNodeDtos().getFirst();
        EffectiveInterventionDto effectiveInterventionDto2012 = nodeDto2012.getInterventions().getFirst();

        List<String> allActionId_2012 = effectiveInterventionDto2012.getActionDtos().stream().map(AbstractActionDto::getTopiaId).filter(Optional::isPresent).map(Optional::get).toList();
        List<AbstractAction> allAbstractActions_2012 = actionDao.forTopiaIdIn(allActionId_2012).findAll();
        Collection<AbstractInputUsage> allUusages_2012 = testDatas.getInputUsagesForActions(allAbstractActions_2012);
        HashSetValuedHashMap<InputType, AbstractDomainInputStockUnit> domainInputUsedByTypes_2012 = testDatas.getUsageDomainInputByTypes(allUusages_2012);

        MultiValuedMap<InputType, String> domainInputUsedCodesUsedByTypes_2012 = new HashSetValuedHashMap<>();
        domainInputUsedByTypes_2012.keySet().forEach(inputType_2012 ->
                domainInputUsedCodesUsedByTypes_2012.putAll(
                        inputType_2012,
                        domainInputUsedByTypes_2012.get(inputType_2012)
                                .stream()
                                .map(AbstractDomainInputStockUnit::getCode)
                                .toList()));

        // suppression des intrants présente en 2024 pour vlider qu'ils seront recréé
        domainInputStockUnitService.deleteDomainInputStockUnit(d2024);
        // on valide le fait que tous les intrants aient été supprimés.
        Assertions.assertEquals(0, domainInputStockUnitDao.forDomainEquals(d2024).count());

        Plot plot2024 = plotDao.forDomainEquals(d2024).findUnique();
        Zone zone2024 = zoneDao.forPlotEquals(plot2024).findUnique();

        // add 2024 data: 1 seasonal cycle with 1 node
        EffectiveCropCycleNode n_courgette_2014 = createNodeForZone(d2024, zone2024);
        getPersistenceContext().commit();

        // do copy
        TargetedZones targetedZone = new TargetedZones();
        targetedZone.setZoneId(zone2024.getTopiaId());
        targetedZone.setNodes(List.of(n_courgette_2014.getTopiaId()));
        boolean success = effectiveCropCycleService.copyInterventions(List.of(targetedZone), List.of(effectiveInterventionDto2012));
        Assertions.assertTrue(success);
        validNonDuplicatedCode(zone2024.getPlot().getDomain());

        // vérification de la copier
        MultiValuedMap<InputType, String> inputStockUnits2024Codes = new HashSetValuedHashMap<>();
        List<DomainInputDto> inputStockUnits2024 = domainService.getInputStockUnits(d2024.getTopiaId());
        inputStockUnits2024.forEach(di2024 -> inputStockUnits2024Codes.put(di2024.getInputType(), di2024.getCode()));

        // on s'assure que les codes sont conservés d'un domaine à l'autre
        MapIterator<InputType, String> inputStockUnits2012CodesIterator = domainInputUsedCodesUsedByTypes_2012.mapIterator();
        while (inputStockUnits2012CodesIterator.hasNext()) {
            InputType inputType_2012 = inputStockUnits2012CodesIterator.next();
            Assertions.assertTrue(inputStockUnits2024Codes.get(inputType_2012).contains(inputStockUnits2012CodesIterator.getValue()));
        }

        // on s'assure que tous les usages coller font références à des intrants du même code
        MultiValuedMap<AgrosystInterventionType, AbstractActionDto> allDomainInputByAgrosystInterventionTypes_2024 = testDatas.getZoneActionDtos(zone2024);
        Assertions.assertTrue(CollectionUtils.isNotEmpty(allDomainInputByAgrosystInterventionTypes_2024.values()));
        Set<InputType> inputTypes_2012 = domainInputUsedByTypes_2012.keySet();
        inputTypes_2012.forEach(inputType_2012 -> allDomainInputByAgrosystInterventionTypes_2024.values().forEach(aDto ->
                {
                    Collection<AbstractInputUsageDto> inputUsageDtos_2024 = testDatas.getActionDtoUsageDtoByTypes(aDto).get(inputType_2012);
                    Set<DomainInputDto> domainInputDtos_2024 = testDatas.compaireAndValideUsage(inputUsageDtos_2024).get(inputType_2012);
                    Collection<String> domainInputCodes_2012 = domainInputUsedCodesUsedByTypes_2012.get(inputType_2012);
                    domainInputDtos_2024.forEach(domainInputDto_2024 -> Assertions.assertTrue(domainInputCodes_2012.contains(domainInputDto_2024.getCode())));
                }
        ));

        List<EffectiveSeasonalCropCycleDto> fromSeasonalCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zone2024.getTopiaId());
        EffectiveCropCycleNodeDto fromNodeDto = fromSeasonalCycle.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> fromInterventionDtos = fromNodeDto.getInterventions();

        List<EffectiveSeasonalCropCycleDto> seasonalDuplicatedCycle = effectiveCropCycleService.getAllEffectiveSeasonalCropCyclesDtos(zone2024.getTopiaId());
        EffectiveCropCycleNodeDto targetedNodeDto = seasonalDuplicatedCycle.getFirst().getNodeDtos().getFirst();
        List<EffectiveInterventionDto> toInterventionDtos = targetedNodeDto.getInterventions();
        Assertions.assertEquals(1, toInterventionDtos.size());
        Map<String, CroppingPlanSpecies> fromCroppingPlanSpeciesByCodes = fromCourgetteCrop.getCroppingPlanSpecies().stream().collect(Collectors.toMap(CroppingPlanSpecies::getCode, Function.identity()));
        validInterventions(fromInterventionDtos, toInterventionDtos, fromCroppingPlanSpeciesByCodes, false);

        success = effectiveCropCycleService.copyInterventions(List.of(targetedZone), List.of(effectiveInterventionDto2012));
        Assertions.assertTrue(success);
        validNonDuplicatedCode(zone2024.getPlot().getDomain());
    }

    private EffectiveCropCycleNode createNodeForZone(Domain d2024, Zone zone2024) {
        CroppingPlanEntry d0_cp0_2024 = croppingPlanEntryDao.forDomainEquals(d2024).addEquals(CroppingPlanEntry.PROPERTY_CODE, "49ec4062-3f7a-4d97-a865-64e865dfe93b").findUnique();

        EffectiveCropCycleNode n_courgette_2014 = effectiveCropCycleNodeDao.create(
                EffectiveCropCycleNode.PROPERTY_TOPIA_ID, "2024_eccn_courgette",
                EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY, d0_cp0_2024,
                EffectiveCropCycleNode.PROPERTY_RANK, 0
        );

        effectiveSeasonalCropCycleDao.create(
                EffectiveSeasonalCropCycle.PROPERTY_ZONE, zone2024,
                EffectiveSeasonalCropCycle.PROPERTY_NODES, Lists.newArrayList(n_courgette_2014)
        );
        return n_courgette_2014;
    }

}
