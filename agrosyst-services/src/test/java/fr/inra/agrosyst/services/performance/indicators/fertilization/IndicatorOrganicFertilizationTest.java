package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class IndicatorOrganicFertilizationTest extends AbstractIndicatorFertilizationTest {

    private final String indicatorN = "Quantité de N organique apportée (en kg/ha)";
    private final String indicatorP2O5 = "Quantité de P2O5 organique apportée (en kg/ha)";
    private final String indicatorK2O = "Quantité de K2O organique apportée (en kg/ha)";

    @Test
    public void testRealise() {
        final List<DonneesTestFertilisationOrgaRealise> donneesTestFertilisation = new LinkedList<>();
        final List<TestDataRealise> testDatas = List.of(
                new TestDataRealise("SDY", OrganicProductUnit.T_HA, 5, 3.5, 1, "35.0", "12.25", "7.0", "0.0", "0.0", "0.0"),
                new TestDataRealise("SDR", OrganicProductUnit.T_HA, 5, 1.0, 1, "147.5", "228.5", "25.0", "75.0", "0.0", "0.0"),
                new TestDataRealise("SDV", OrganicProductUnit.M_CUB_HA, 15, 2.0, 1, "1.5", "6.0", "12.0", "0.0", "60.0", "0.0"),
                new TestDataRealise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 1, "91.5", "75.0", "105.0", "0.0", "0.0", "75.0"),
                new TestDataRealise("SCM", OrganicProductUnit.M_CUB_HA, 15, 1.0, 1, "102.0", "142.5", "82.5", "0.0", "0.0", "0.0")
        );
        for (TestDataRealise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S()
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationOrgaRealise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.transitCount(),
                    Map.of(
                            indicatorN, testData.expectedN(),
                            indicatorP2O5, testData.expectedP(),
                            indicatorK2O, testData.expectedK()
                    )
            ));
        }

        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        for (DonneesTestFertilisationOrgaRealise donneesTestFertilisationRealise : donneesTestFertilisation) {
            this.testEpandageOrganiqueRealise(donneesTestFertilisationRealise, "Fertilisation organique", indicatorOrganicFertilization);
        }
    }

    @Test
    public void testUserElementValueRealise() {
        final List<DonneesTestFertilisationOrgaRealise> donneesTestFertilisation = new LinkedList<>();
        final List<TestUserElementDataRealise> testDatas = List.of(
                new TestUserElementDataRealise("SDY", OrganicProductUnit.T_HA, 5, 3.5, 1, 4, 13, 14, 0.0d,0.0d,0.0d,false, "70.0", "227.5", "245.0", "0.0", "0.0", "0.0"),
                new TestUserElementDataRealise("SDR", OrganicProductUnit.T_HA, 5, 1.0, 1, 6, 10, 1, 8.0d,0.0d,0.0d,false, "30.0", "50.0", "5.0", "40.0", "0.0", "0.0"),
                new TestUserElementDataRealise("SDV", OrganicProductUnit.M_CUB_HA, 15, 2.0, 1, 13, 15, 2, 0.0d,10.0d,0.0d,false, "390.0", "450.0", "60.0", "0.0", "300.0", "0.0"),
                new TestUserElementDataRealise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 1, 13, 9, 13, 0.0d,0.0d,0.0d,false, "195.0", "135.0", "195.0", "0.0", "0.0", "45.0"),
                new TestUserElementDataRealise("SCM", OrganicProductUnit.M_CUB_HA, 15, 1.0, 1, 1, 5, 10, 0.0d,0.0d,0.0d,false, "15.0", "75.0", "150.0", "0.0", "0.0", "0.0")
        );

        for (TestUserElementDataRealise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, testData.n(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, testData.p2O5(),
                    DomainOrganicProductInput.PROPERTY_K2_O, testData.k2O(),
                    DomainOrganicProductInput.PROPERTY_CA_O, testData.caO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, testData.mgO(),
                    DomainOrganicProductInput.PROPERTY_S, testData.s()
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationOrgaRealise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.transitCount(),
                    Map.of(
                            indicatorN, testData.expectedN(),
                            indicatorP2O5, testData.expectedP(),
                            indicatorK2O, testData.expectedK()
                    )
            ));
        }

        IndicatorOrganicFertilization indicatorOrganicFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        for (DonneesTestFertilisationOrgaRealise donneesTestFertilisationRealise : donneesTestFertilisation) {
            this.testEpandageOrganiqueRealise(donneesTestFertilisationRealise, "Fertilisation organique", indicatorOrganicFertilization);
        }
    }

    @Test
    public void testSynthetise() throws IOException {
        final List<DonneesTestFertilisationOrgaSynthetise> donneesTestFertilisation = new LinkedList<>();
        final List<TestDataSynthetise> testDatas = List.of(
                new TestDataSynthetise("SDY", OrganicProductUnit.T_HA, 5, 4.0, 1.0, "40.0", "14.0", "8.0", "0.0", "0.0", "0.0"),
                new TestDataSynthetise("SDY", OrganicProductUnit.T_HA, 5, 3.0, 6.0, "180.0", "63.0", "36.0", "0.0", "0.0", "0.0"),
                new TestDataSynthetise("SDR", OrganicProductUnit.T_HA, 5, 2.0, 1.0, "295.0", "457.0", "50.0", "150.0", "0.0", "0.0"),
                new TestDataSynthetise("SDV", OrganicProductUnit.M_CUB_HA, 15, 1.0, 6.0, "4.5", "18.0", "36.0", "0.0", "180.0", "0.0"),
                new TestDataSynthetise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 2.0, "183.0", "150.0", "210.0", "0.0", "0.0", "150.0"),
                new TestDataSynthetise("SCM", OrganicProductUnit.M_CUB_HA, 15, 1.5, 1.0, "153.0", "213.75", "123.75", "0.0", "0.0", "0.0")
        );
        for (TestDataSynthetise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, refInput.getTeneur_Ferti_Orga_N_total(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, refInput.getTeneur_Ferti_Orga_P(),
                    DomainOrganicProductInput.PROPERTY_K2_O, refInput.getTeneur_Ferti_Orga_K(),
                    DomainOrganicProductInput.PROPERTY_CA_O, refInput.getTeneur_Ferti_Orga_CaO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, refInput.getTeneur_Ferti_Orga_MgO(),
                    DomainOrganicProductInput.PROPERTY_S, refInput.getTeneur_Ferti_Orga_S()
            );

            final String inputName = domainOrganicProductInput.getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationOrgaSynthetise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.temporalFrequency(),
                    inputName,
                    Map.of(
                            indicatorN, testData.expectedN(),
                            indicatorP2O5, testData.expectedP(),
                            indicatorK2O, testData.expectedK()
                    )));
        }

        IndicatorOrganicFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        for (DonneesTestFertilisationOrgaSynthetise donneesTestFertilisationRealise : donneesTestFertilisation) {
            this.testEpandageOrganiqueSynthetise(donneesTestFertilisationRealise, "Fertilisation organique", indicatorMineralFertilization);
        }
    }

    @Test
    public void testUserElementValueSynthetise() throws IOException {
        final List<DonneesTestFertilisationOrgaSynthetise> donneesTestFertilisation = new LinkedList<>();
        final List<TestUserElementDataSynthetise> testDatas = List.of(
                new TestUserElementDataSynthetise("SDY", OrganicProductUnit.T_HA, 5, 3.0, 6.0, 10, 12, 6, 0.0d,0.0d,0.0d,false, "900.0", "1080.0", "540.0", "0.0", "0.0", "0.0"),
                new TestUserElementDataSynthetise("SDR", OrganicProductUnit.T_HA, 5, 2.0, 1.0, 1, 8, 7, 13.0d,0.0d,0.0d,false, "10.0", "80.0", "70.0", "130.0", "0.0", "0.0"),
                new TestUserElementDataSynthetise("SDV", OrganicProductUnit.M_CUB_HA, 15, 1.0, 6.0,  7, 2, 8, 0.0d,7.0d,0.0d,false, "630.0", "180.0", "720.0", "0.0", "630.0", "0.0"),
                new TestUserElementDataSynthetise("SCH", OrganicProductUnit.T_HA, 15, 1.0, 2.0,  7, 13, 11, 0.0d,0.0d,8.0d,false, "210.0", "390.0", "330.0", "0.0", "0.0", "240.0"),
                new TestUserElementDataSynthetise("SCM", OrganicProductUnit.M_CUB_HA, 15, 1.5, 1.0,  8, 12, 5, 0.0d,0.0d,0.0d,false, "180.0", "270.0", "112.5", "0.0", "0.0", "0.0")
        );
        for (TestUserElementDataSynthetise testData : testDatas) {
            final RefFertiOrga refInput = this.refFertiOrgaTopiaDao.findByIdtypeeffluent(testData.id());
            final DomainOrganicProductInput domainOrganicProductInput = this.domainOrganicProductInputDao.create(
                    DomainOrganicProductInput.PROPERTY_REF_INPUT, refInput,
                    DomainOrganicProductInput.PROPERTY_USAGE_UNIT, testData.organicProductUnit(),
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, refInput.getLibelle(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainOrganicProductInput.PROPERTY_INPUT_KEY, "",
                    DomainOrganicProductInput.PROPERTY_N, testData.n(),
                    DomainOrganicProductInput.PROPERTY_P2_O5, testData.p2O5(),
                    DomainOrganicProductInput.PROPERTY_K2_O, testData.k2O(),
                    DomainOrganicProductInput.PROPERTY_CA_O, testData.caO(),
                    DomainOrganicProductInput.PROPERTY_MG_O, testData.mgO(),
                    DomainOrganicProductInput.PROPERTY_S, testData.s()
            );

            final String inputName = domainOrganicProductInput.getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationOrgaSynthetise(
                    refInput,
                    domainOrganicProductInput,
                    testData.qtAvg(),
                    testData.spatialFrequency(),
                    testData.temporalFrequency(),
                    inputName,
                    Map.of(
                            indicatorN, testData.expectedN(),
                            indicatorP2O5, testData.expectedP(),
                            indicatorK2O, testData.expectedK()
                    )));
        }

        IndicatorOrganicFertilization indicatorMineralFertilization = serviceFactory.newInstance(IndicatorOrganicFertilization.class);
        for (DonneesTestFertilisationOrgaSynthetise donneesTestFertilisationRealise : donneesTestFertilisation) {
            this.testEpandageOrganiqueSynthetise(donneesTestFertilisationRealise, "Fertilisation organique", indicatorMineralFertilization);
        }
    }
}
