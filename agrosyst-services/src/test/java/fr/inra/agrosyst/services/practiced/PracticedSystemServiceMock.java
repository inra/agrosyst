package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2015 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedPerennialCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;

import java.util.List;

/**
 * Created by davidcosse on 20/05/15.
 */
public class PracticedSystemServiceMock extends PracticedSystemServiceImpl {

    protected final PracticedSystemService practicedSystemService;

    public PracticedSystemServiceMock(PracticedSystemService practicedSystemService) {
        this.practicedSystemService = practicedSystemService;
    }

    protected List<PracticedPerennialCropCycleDto> convertPerennialCropCyclesToDto(
            List<PracticedPerennialCropCycle> practicedPerennialCropCycles, PracticedSystem practicedSystem) {
        List<PracticedPerennialCropCycleDto> result = ((PracticedSystemServiceImpl)practicedSystemService).convertPerennialCropCyclesToDto(practicedPerennialCropCycles, practicedSystem);
        return result;
    }
}
