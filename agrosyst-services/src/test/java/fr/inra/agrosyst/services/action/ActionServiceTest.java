package fr.inra.agrosyst.services.action;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainIrrigationInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInputTopiaDao;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.LivestockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.SeedPriceTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PhytoProductTargetTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.action.HarvestingActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import fr.inra.agrosyst.api.services.action.IrrigationActionDto;
import fr.inra.agrosyst.api.services.action.MineralFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.QualityCriteriaDto;
import fr.inra.agrosyst.api.services.action.SeedingActionUsageDto;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainIrrigationInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycles;
import fr.inra.agrosyst.api.services.input.IrrigationInputUsageDto;
import fr.inra.agrosyst.api.services.input.MineralProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductTargetDto;
import fr.inra.agrosyst.api.services.input.SeedLotInputUsageDto;
import fr.inra.agrosyst.api.services.input.SeedSpeciesInputUsageDto;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystems;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputBinder;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class ActionServiceTest extends AbstractAgrosystTest {

    protected ActionService actionService;
    protected DomainService domainService;
    protected AgrosystI18nService i18nService;
    protected PracticedSystemService practicedSystemService;
    protected PricesService pricesService;

    protected AbstractActionTopiaDao abstractActionDao;
    protected AbstractDomainInputStockUnitTopiaDao domainInputStockUnitDao;
    protected DomainIrrigationInputTopiaDao domainIrrigationInputDao;
    protected DomainMineralProductInputTopiaDao domainMineralProductInputTopiaDao;
    protected DomainPhytoProductInputTopiaDao domainPhytoProductInputDao;
    protected DomainSeedLotInputTopiaDao domainSeedLotInputDao;
    protected DomainSeedSpeciesInputTopiaDao domainSeedSpeciesInputDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    protected InputPriceTopiaDao inputPriceDao;
    protected LivestockUnitTopiaDao livestockUnitTopiaDao;
    protected MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao;
    protected PhytoProductTargetTopiaDao phytoProductTargetDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao;
    protected RefDestinationTopiaDao refDestinationDao;
    protected RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    protected RefNuisibleEDITopiaDao refNuisibleEDIDao;
    protected SeedPriceTopiaDao seedPriceDao;
    protected ZoneTopiaDao zoneDao;

    @BeforeEach
    public void prepareTest() {
        actionService = serviceFactory.newService(ActionService.class);
        domainService = serviceFactory.newService(DomainService.class);
        i18nService = serviceFactory.newService(AgrosystI18nService.class);
        practicedSystemService = serviceFactory.newService(PracticedSystemService.class);
        pricesService = serviceFactory.newService(PricesService.class);

        abstractActionDao = getPersistenceContext().getAbstractActionDao();
        domainInputStockUnitDao = getPersistenceContext().getAbstractDomainInputStockUnitDao();
        domainIrrigationInputDao = getPersistenceContext().getDomainIrrigationInputDao();
        domainMineralProductInputTopiaDao = getPersistenceContext().getDomainMineralProductInputDao();
        domainPhytoProductInputDao = getPersistenceContext().getDomainPhytoProductInputDao();
        domainSeedLotInputDao = getPersistenceContext().getDomainSeedLotInputDao();
        domainSeedSpeciesInputDao = getPersistenceContext().getDomainSeedSpeciesInputDao();
        effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        effectivePerennialCropCycleDao = getPersistenceContext().getEffectivePerennialCropCycleDao();
        inputPriceDao = getPersistenceContext().getInputPriceDao();
        livestockUnitTopiaDao = getPersistenceContext().getLivestockUnitDao();
        mineralFertilizersSpreadingActionDao = getPersistenceContext().getMineralFertilizersSpreadingActionDao();
        phytoProductTargetDao = getPersistenceContext().getPhytoProductTargetDao();
        refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        refDestinationDao = getPersistenceContext().getRefDestinationDao();
        refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        refNuisibleEDIDao = getPersistenceContext().getRefNuisibleEDIDao();
        seedPriceDao = getPersistenceContext().getSeedPriceDao();
        zoneDao = getPersistenceContext().getZoneDao();

        testDatas = serviceFactory.newInstance(TestDatas.class);
        loginAsAdmin();
        alterSchema();
    }

    @Test
    public void testPracticedMineralActionUsagePersistance() throws IOException {

        PracticedSystem psBaulon2013 = testDatas.createTestPraticedSystems();
        final Domain domain = psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain();

        testDatas.createInputStorage(domain);

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        PracticedIntervention practicedIntervention = testDatas.createPracticedIntervention(psBaulon2013, croppingPlanEntries.getFirst());

        Collection<AbstractActionDto> actionDtos = new ArrayList<>();

        DomainMineralProductInput dmpiEntity = domainMineralProductInputTopiaDao.forDomainEquals(domain)
                .addEquals(TopiaEntity.PROPERTY_TOPIA_ID, "DomainMineralProductInput-315_Pulverulent")
                .findUnique();

        DomainMineralProductInputDto domainMPinputDto = DomainInputBinder.domainMineralProductInputEntityToDtoBinder(dmpiEntity);

        MineralProductInputUsageDto mineralProductInputUsageDto = MineralProductInputUsageDto.builder()
                .domainMineralProductInputDto(domainMPinputDto)
                .qtAvg(1.3)
                .inputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)
                .productName(dmpiEntity.getInputName())
                .code(dmpiEntity.getCode())
                .build();

        Collection<MineralProductInputUsageDto> mineralProductInputUsageDtos = new ArrayList<>();
        mineralProductInputUsageDtos.add(mineralProductInputUsageDto);

        RefInterventionAgrosystTravailEDI refActionMineral = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEM").findAnyOrNull();

        MineralFertilizersSpreadingActionDto mfsaDto = MineralFertilizersSpreadingActionDto.builder()
                .mainActionInterventionAgrosyst(refActionMineral.getIntervention_agrosyst())
                .burial(true)
                .localizedSpreading(true)
                .mainActionId(refActionMineral.getTopiaId())
                .mainActionReference_code(refActionMineral.getReference_code())
                .mineralProductInputUsageDtos(mineralProductInputUsageDtos)
                .build();

        actionDtos.add(mfsaDto);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domain).findAll();
        // create
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                actionDtos,
                domainInputStockUnits,
                psBaulon2013, null, null, null);

        Assertions.assertThat(abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().count()).isEqualTo(1);
        Optional<AbstractAction> persistedAction = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().findAny();
        Assertions.assertThat(persistedAction.isPresent()).isTrue();

        MineralFertilizersSpreadingAction action = (MineralFertilizersSpreadingAction) persistedAction.get();

        final Optional<MineralProductInputUsage> actual = action.getMineralProductInputUsages().stream().findAny();
        Assertions.assertThat(actual.isPresent()).isEqualTo(true);
        MineralProductInputUsage mineralProductInputUsage = actual.get();
        Assertions.assertThat(mineralProductInputUsage.getDomainMineralProductInput().getTopiaId()).isEqualTo(dmpiEntity.getTopiaId());

        Assertions.assertThat(mineralProductInputUsage.getQtAvg()).isEqualTo(mineralProductInputUsageDto.getQtAvg());
        Assertions.assertThat(mineralProductInputUsage.getInputType()).isEqualTo(mineralProductInputUsageDto.getInputType());

        Collection<AbstractActionDto> resultDto = actionService.loadPracticedActionsAndUsages(practicedIntervention);
        Assertions.assertThat(resultDto).isNotEmpty();
        Assertions.assertThat(resultDto.iterator().hasNext()).isTrue();

        final AbstractActionDto abstractActionDto = resultDto.iterator().next();
        Assertions.assertThat(abstractActionDto).isInstanceOf(MineralFertilizersSpreadingActionDto.class);
        MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto = (MineralFertilizersSpreadingActionDto) abstractActionDto;

        final Optional<String> optionalTopiaId = mineralFertilizersSpreadingActionDto.getTopiaId();
        Assertions.assertThat(optionalTopiaId.isPresent()).isTrue();
        String thePersistedActionId = optionalTopiaId.get();

        final Optional<Collection<MineralProductInputUsageDto>> optionnalPersisteMineralProductInputUsageDtos = mineralFertilizersSpreadingActionDto.getMineralProductInputUsageDtos();
        Assertions.assertThat(optionnalPersisteMineralProductInputUsageDtos.isPresent()).isTrue();

        final Collection<MineralProductInputUsageDto> persistedMineralProductInputUsageDtos = optionnalPersisteMineralProductInputUsageDtos.get();
        Assertions.assertThat(persistedMineralProductInputUsageDtos.size()).isEqualTo(1);

        final MineralProductInputUsageDto productInputUsageDto = persistedMineralProductInputUsageDtos.iterator().next();
        final Optional<String> theOptionalInputUsageTopiaId = productInputUsageDto.getInputUsageTopiaId();
        Assertions.assertThat(theOptionalInputUsageTopiaId.isPresent()).isTrue();
        String thePersistedInputId = theOptionalInputUsageTopiaId.get();

        final double updatedQtAvg = 2.6;

        final MineralProductInputUsageDto mineralProductInputUsageDto1 = productInputUsageDto.toBuilder()
                .qtAvg(updatedQtAvg)
                .productName(productInputUsageDto.getDomainMineralProductInputDto().getInputName())
                .build();


        final Collection<MineralProductInputUsageDto> mineralProductInputUsageDtos1 = new ArrayList<>();
        mineralProductInputUsageDtos1.add(mineralProductInputUsageDto1);

        final MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto1 = mineralFertilizersSpreadingActionDto.toBuilder()
                .mineralProductInputUsageDtos(mineralProductInputUsageDtos1)
                .build();

        actionDtos.clear();
        actionDtos.add(mineralFertilizersSpreadingActionDto1);
        // update
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                actionDtos,
                domainInputStockUnits,
                psBaulon2013, null, null, null);

        Collection<AbstractActionDto> updatedActionDtos = actionService.loadPracticedActionsAndUsages(practicedIntervention);

        Assertions.assertThat(updatedActionDtos).isNotEmpty();
        Assertions.assertThat(updatedActionDtos.iterator().hasNext()).isTrue();

        final AbstractActionDto persistedAbstractActionDto = updatedActionDtos.iterator().next();
        Assertions.assertThat(persistedAbstractActionDto).isInstanceOf(MineralFertilizersSpreadingActionDto.class);
        MineralFertilizersSpreadingActionDto updatedMineralFertilizersSpreadingActionDto = (MineralFertilizersSpreadingActionDto) persistedAbstractActionDto;

        final Optional<String> optionalMineralFertiliserId = updatedMineralFertilizersSpreadingActionDto.getTopiaId();
        Assertions.assertThat(optionalMineralFertiliserId.isPresent()).isTrue();
        Assertions.assertThat(optionalMineralFertiliserId.get()).isEqualTo(thePersistedActionId);

        final Optional<Collection<MineralProductInputUsageDto>> optionalUpdatedPersisteMineralProductInputUsageDtos = updatedMineralFertilizersSpreadingActionDto.getMineralProductInputUsageDtos();
        Assertions.assertThat(optionalUpdatedPersisteMineralProductInputUsageDtos.isPresent()).isTrue();

        final Collection<MineralProductInputUsageDto> persistedUpdatedMineralProductInputUsageDtos = optionalUpdatedPersisteMineralProductInputUsageDtos.get();
        Assertions.assertThat(persistedUpdatedMineralProductInputUsageDtos.size()).isEqualTo(1);

        final MineralProductInputUsageDto productUpdatedInputUsageDto = persistedUpdatedMineralProductInputUsageDtos.iterator().next();
        Assertions.assertThat(productUpdatedInputUsageDto.getQtAvg()).isEqualTo(updatedQtAvg);

        final Optional<String> optionalInputUsageTopiaId = productUpdatedInputUsageDto.getInputUsageTopiaId();
        Assertions.assertThat(optionalInputUsageTopiaId.isPresent()).isTrue();
        Assertions.assertThat(optionalInputUsageTopiaId.get()).isEqualTo(thePersistedInputId);
    }

    @Test
    public void testPracticedIrrigActionPersistance() throws IOException {

        // cas nominal
        PracticedSystem psBaulon2013 = testDatas.createTestPraticedSystems();
        final Domain domain = psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain();

        testDatas.createInputStorage(domain);

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        PracticedIntervention practicedIntervention = testDatas.createPracticedIntervention(psBaulon2013, croppingPlanEntries.getFirst());

        Collection<AbstractActionDto> actionDtos = new ArrayList<>();

        DomainIrrigationInput domainIrrigationInput = domainIrrigationInputDao.forDomainEquals(domain).findUnique();

        DomainIrrigationInputDto domainIrrigationInputDto = DomainInputBinder.domainIrrigationInputDtoBinder(domainIrrigationInput);

        IrrigationInputUsageDto irrigationInputUsageDto = IrrigationInputUsageDto.builder()
                .qtAvg(30.0)
                .inputType(InputType.IRRIGATION)
                .domainIrrigationInputDto(domainIrrigationInputDto)
                .productName(domainIrrigationInputDto.getInputName())
                .code(domainIrrigationInput.getCode())
                .build();

        RefInterventionAgrosystTravailEDI refIrrigation = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEO").findUnique();

        IrrigationActionDto irrigationActionDto = IrrigationActionDto.builder()
                .mainActionInterventionAgrosyst(refIrrigation.getIntervention_agrosyst())
                .mainActionId(refIrrigation.getTopiaId())
                .waterQuantityAverage(30.0)
                .irrigationInputUsageDto(irrigationInputUsageDto)
                .mainActionReference_code(refIrrigation.getReference_code())
                .build();

        actionDtos.add(irrigationActionDto);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domain).findAll();
        // create
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                actionDtos,
                domainInputStockUnits,
                psBaulon2013, null, null, null);

        Assertions.assertThat(abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().count()).isEqualTo(1);
        Optional<AbstractAction> persistedAction = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().findAny();
        Assertions.assertThat(persistedAction.isPresent()).isTrue();

        IrrigationAction action = (IrrigationAction) persistedAction.get();

        IrrigationInputUsage irrigationInputUsage = action.getIrrigationInputUsage();
        Assertions.assertThat(irrigationInputUsage).isNotNull();

    }

    @Test
    public void testPracticedIrrigActionPersistanceWithoutDomainInput() throws IOException {

        // cas nominal
        PracticedSystem psBaulon2013 = testDatas.createTestPraticedSystems();
        final Domain domain = psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain();

        testDatas.createInputStorage(domain);

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        PracticedIntervention practicedIntervention = testDatas.createPracticedIntervention(psBaulon2013, croppingPlanEntries.getFirst());

        Collection<AbstractActionDto> actionDtos = new ArrayList<>();

        DomainIrrigationInput domainIrrigationInput = domainIrrigationInputDao.forDomainEquals(domain).findUnique();
        domainIrrigationInputDao.delete(domainIrrigationInput);

        RefInterventionAgrosystTravailEDI refIrrigation = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEO").findUnique();

        IrrigationActionDto irrigationActionDto = IrrigationActionDto.builder()
                .mainActionInterventionAgrosyst(refIrrigation.getIntervention_agrosyst())
                .mainActionId(refIrrigation.getTopiaId())
                .waterQuantityAverage(30.0)
                .mainActionReference_code(refIrrigation.getReference_code())
                .build();

        actionDtos.add(irrigationActionDto);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domain).findAll();
        // create
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                actionDtos,
                domainInputStockUnits,
                psBaulon2013, null, null, null);

        Assertions.assertThat(abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().count()).isEqualTo(1);
        Optional<AbstractAction> persistedAction = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().findAny();
        Assertions.assertThat(persistedAction.isPresent()).isTrue();

        IrrigationAction action = (IrrigationAction) persistedAction.get();

        IrrigationInputUsage irrigationInputUsage = action.getIrrigationInputUsage();
        Assertions.assertThat(irrigationInputUsage).isNotNull();
        Assertions.assertThat(irrigationInputUsage.getDomainIrrigationInput()).isNotNull();
        Assertions.assertThat(irrigationInputUsage.getQtAvg()).isEqualTo(irrigationActionDto.getWaterQuantityAverage());

    }

    @Test
    public void testEffectiveMineralActionUsagePersistance() throws IOException {

        testDatas.createTestEffectiveInterventions();

        final Zone baulonZone = zoneDao.forTopiaIdEquals("A_TOPIA_ID").findUnique();
        final Domain domain_Baulon_2013 = baulonZone.getPlot().getDomain();

        final DomainMineralProductInput dmpi = domainMineralProductInputTopiaDao.forTopiaIdEquals("DomainMineralProductInput-315_Pulverulent").findUnique();
        DomainMineralProductInputDto dmiDto = DomainInputBinder.domainMineralProductInputEntityToDtoBinder(dmpi);

        EffectiveIntervention effectiveIntervention = effectiveInterventionDao.forTopiaIdEquals("interBaulon1_topiaId").findUnique();

        Collection<AbstractActionDto> actionDtos = new ArrayList<>();

        MineralProductInputUsageDto mineralProductInputUsageDto = MineralProductInputUsageDto.builder()
                .domainMineralProductInputDto(dmiDto)
                .qtAvg(1.3)
                .inputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)
                .productName(dmiDto.getInputName())
                .code(dmpi.getCode())
                .build();

        Collection<MineralProductInputUsageDto> mineralProductInputUsageDtos = new ArrayList<>();
        mineralProductInputUsageDtos.add(mineralProductInputUsageDto);

        RefInterventionAgrosystTravailEDI refActionMineral = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEM").findAnyOrNull();

        MineralFertilizersSpreadingActionDto mfsaDto = MineralFertilizersSpreadingActionDto.builder()
                .mainActionInterventionAgrosyst(refActionMineral.getIntervention_agrosyst())
                .burial(true)
                .localizedSpreading(true)
                .mainActionId(refActionMineral.getTopiaId())
                .mainActionReference_code(refActionMineral.getReference_code())
                .mineralProductInputUsageDtos(mineralProductInputUsageDtos)
                .build();

        actionDtos.add(mfsaDto);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domain_Baulon_2013).findAll();

        EffectivePerennialCropCycle perennialCropCycle = effectivePerennialCropCycleDao.forPhaseEquals(effectiveIntervention.getEffectiveCropCyclePhase()).findUnique();

        // create
        actionService.createOrUpdateEffectiveInterventionActionAndUsages(
                effectiveIntervention,
                actionDtos,
                domainInputStockUnits,
                baulonZone,
                null,
                null,
                null,
                perennialCropCycle.getCroppingPlanEntry(),
                false);

        List<AbstractAction> abstractActions = abstractActionDao.forEffectiveInterventionEquals(effectiveIntervention).findAll();
        Assertions.assertThat((long) abstractActions.size()).isEqualTo(1);
        Optional<AbstractAction> persistedAction = abstractActions.stream()
                .filter(aa -> aa instanceof MineralFertilizersSpreadingAction).findAny();

        Assertions.assertThat(persistedAction.isPresent()).isTrue();

        MineralFertilizersSpreadingAction action = (MineralFertilizersSpreadingAction) persistedAction.get();

        final Optional<MineralProductInputUsage> actual = action.getMineralProductInputUsages().stream().findAny();
        Assertions.assertThat(actual.isPresent()).isEqualTo(true);
        MineralProductInputUsage mineralProductInputUsage = actual.get();
        Assertions.assertThat(mineralProductInputUsage.getDomainMineralProductInput().getTopiaId()).isEqualTo(dmpi.getTopiaId());

        Assertions.assertThat(mineralProductInputUsage.getQtAvg()).isEqualTo(mineralProductInputUsageDto.getQtAvg());
        Assertions.assertThat(mineralProductInputUsage.getInputType()).isEqualTo(mineralProductInputUsageDto.getInputType());

        Collection<AbstractActionDto> resultDto = actionService.loadEffectiveActionsAndUsages(effectiveIntervention);
        Assertions.assertThat(resultDto).isNotEmpty();
        Assertions.assertThat(resultDto.iterator().hasNext()).isTrue();

        final AbstractActionDto abstractActionDto = resultDto.iterator().next();
        Assertions.assertThat(abstractActionDto).isInstanceOf(MineralFertilizersSpreadingActionDto.class);
        MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto = (MineralFertilizersSpreadingActionDto) abstractActionDto;

        final Optional<String> optionalTopiaId = mineralFertilizersSpreadingActionDto.getTopiaId();
        Assertions.assertThat(optionalTopiaId.isPresent()).isTrue();
        String thePersistedActionId = optionalTopiaId.get();

        final Optional<Collection<MineralProductInputUsageDto>> optionnalPersisteMineralProductInputUsageDtos = mineralFertilizersSpreadingActionDto.getMineralProductInputUsageDtos();
        Assertions.assertThat(optionnalPersisteMineralProductInputUsageDtos.isPresent()).isTrue();

        final Collection<MineralProductInputUsageDto> persistedMineralProductInputUsageDtos = optionnalPersisteMineralProductInputUsageDtos.get();
        Assertions.assertThat(persistedMineralProductInputUsageDtos.size()).isEqualTo(1);

        final MineralProductInputUsageDto productInputUsageDto = persistedMineralProductInputUsageDtos.iterator().next();
        final Optional<String> theOptionalInputUsageTopiaId = productInputUsageDto.getInputUsageTopiaId();
        Assertions.assertThat(theOptionalInputUsageTopiaId.isPresent()).isTrue();
        String thePersistedInputId = theOptionalInputUsageTopiaId.get();

        final double updatedQtAvg = 2.6;

        final MineralProductInputUsageDto mineralProductInputUsageDto1 = productInputUsageDto.toBuilder()
                .qtAvg(updatedQtAvg)
                .productName(productInputUsageDto.getDomainMineralProductInputDto().getInputName())
                .build();


        final Collection<MineralProductInputUsageDto> mineralProductInputUsageDtos1 = new ArrayList<>();
        mineralProductInputUsageDtos1.add(mineralProductInputUsageDto1);

        final MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto1 = mineralFertilizersSpreadingActionDto.toBuilder()
                .mineralProductInputUsageDtos(mineralProductInputUsageDtos1)
                .build();

        actionDtos.clear();
        actionDtos.add(mineralFertilizersSpreadingActionDto1);
        // update
        actionService.createOrUpdateEffectiveInterventionActionAndUsages(
                effectiveIntervention,
                actionDtos,
                domainInputStockUnits,
                baulonZone,
                null,
                null,
                null,
                perennialCropCycle.getCroppingPlanEntry(),
                false);

        Collection<AbstractActionDto> updatedActionDtos = actionService.loadEffectiveActionsAndUsages(effectiveIntervention);

        Assertions.assertThat(updatedActionDtos).isNotEmpty();
        Assertions.assertThat(updatedActionDtos.iterator().hasNext()).isTrue();

        final AbstractActionDto persistedAbstractActionDto = updatedActionDtos.iterator().next();
        Assertions.assertThat(persistedAbstractActionDto).isInstanceOf(MineralFertilizersSpreadingActionDto.class);
        MineralFertilizersSpreadingActionDto updatedMineralFertilizersSpreadingActionDto = (MineralFertilizersSpreadingActionDto) persistedAbstractActionDto;

        final Optional<String> optionalMineralFertiliserId = updatedMineralFertilizersSpreadingActionDto.getTopiaId();
        Assertions.assertThat(optionalMineralFertiliserId.isPresent()).isTrue();
        Assertions.assertThat(optionalMineralFertiliserId.get()).isEqualTo(thePersistedActionId);

        final Optional<Collection<MineralProductInputUsageDto>> optionalUpdatedPersisteMineralProductInputUsageDtos = updatedMineralFertilizersSpreadingActionDto.getMineralProductInputUsageDtos();
        Assertions.assertThat(optionalUpdatedPersisteMineralProductInputUsageDtos.isPresent()).isTrue();

        final Collection<MineralProductInputUsageDto> persistedUpdatedMineralProductInputUsageDtos = optionalUpdatedPersisteMineralProductInputUsageDtos.get();
        Assertions.assertThat(persistedUpdatedMineralProductInputUsageDtos.size()).isEqualTo(1);

        final MineralProductInputUsageDto productUpdatedInputUsageDto = persistedUpdatedMineralProductInputUsageDtos.iterator().next();
        Assertions.assertThat(productUpdatedInputUsageDto.getQtAvg()).isEqualTo(updatedQtAvg);

        final Optional<String> optionalInputUsageTopiaId = productUpdatedInputUsageDto.getInputUsageTopiaId();
        Assertions.assertThat(optionalInputUsageTopiaId.isPresent()).isTrue();
        Assertions.assertThat(optionalInputUsageTopiaId.get()).isEqualTo(thePersistedInputId);
    }

    @Test
    public void testPracticedSeedingUsageActionPersistance() throws IOException {

        PracticedSystem psBaulon2013 = testDatas.createTestPraticedSystems();
        final Domain domainBaulon = psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain();
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domainBaulon.getTopiaId());
        PracticedIntervention practicedIntervention = testDatas.createPracticedIntervention(psBaulon2013, croppingPlanEntries.getFirst());

        final Collection<PracticedSpeciesStade> speciesStades = practicedIntervention.getSpeciesStades();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);

        List<SpeciesStadeDto> speciesStadeDtos = new ArrayList<>();
        speciesStades.forEach(
                speciesStade -> speciesStadeDtos.add(PracticedSystems.getDtoForPracticedSpeciesStade(speciesStade, translationMap)));


        Map<String, CroppingPlanEntry> cropByIds = Maps.uniqueIndex(croppingPlanEntries, TopiaEntity::getTopiaId);

        CroppingPlanEntry wheat = cropByIds.get("ble-id");

        DomainSeedLotInput domainSeedLotInput = domainSeedLotInputDao.newInstance();
        domainSeedLotInput.setCode(UUID.randomUUID().toString());
        domainSeedLotInput.setInputType(InputType.SEMIS);
        domainSeedLotInput.setDomain(domainBaulon);
        domainSeedLotInput.setUsageUnit(SeedPlantUnit.GRAINES_PAR_HA);
        domainSeedLotInput.setOrganic(false);
        domainSeedLotInput.setCropSeed(wheat);
        domainSeedLotInput.setInputName(wheat.getName());
        domainSeedLotInput.setInputKey(DomainInputStockUnitService.getLotCropSeedInputKey(wheat, domainSeedLotInput.isOrganic(), domainSeedLotInput.getUsageUnit()));

        SeedPrice lotPrice = seedPriceDao.newInstance();
        lotPrice.setIncludedTreatment(true);
        lotPrice.setOrganic(domainSeedLotInput.isOrganic());
        lotPrice.setSeedType(SeedType.SEMENCES_CERTIFIEES);

        lotPrice.setCategory(InputPriceCategory.SEEDING_INPUT);
        lotPrice.setDomain(domainBaulon);
        lotPrice.setDisplayName(wheat.getName());
        lotPrice.setObjectId(wheat.getCode());
        lotPrice.setPrice(1.1);
        lotPrice.setPriceUnit(PriceUnit.EURO_HA);
        lotPrice.setSourceUnit(SeedPlantUnit.GRAINES_PAR_HA.name());

        domainSeedLotInput.setSeedPrice(lotPrice);
        seedPriceDao.create(lotPrice);

        RefActaTraitementsProduit produitAcanto = refActaTraitementsProduitDao.forAll().findAny();

        Map<String, DomainSeedSpeciesInput> domainSeedSpeciesInputByCropIds = Maps.newHashMap();

        final List<CroppingPlanSpecies> wheatCroppingPlanSpecies = wheat.getCroppingPlanSpecies();
        final int wheatCroppingPlanSpeciesSize = wheatCroppingPlanSpecies.size();
        boolean isCompagne = CollectionUtils.isNotEmpty(wheatCroppingPlanSpecies) && wheatCroppingPlanSpecies.stream().map(CroppingPlanSpecies::getCompagne).anyMatch(Objects::nonNull);
        InputType inputType = isCompagne ? InputType.PLAN_COMPAGNE : InputType.SEMIS;

        for (CroppingPlanSpecies cps : wheatCroppingPlanSpecies) {

            final RefEspece refEspece = cps.getSpecies();
            final RefVariete variety = cps.getVariety();
            final String varietyName = variety != null ? " (" + variety.getLabel() + ")" : "";

            DomainSeedSpeciesInput domainSeedSpeciesInput = domainSeedSpeciesInputDao.newInstance();
            domainSeedSpeciesInput.setCode(UUID.randomUUID().toString());
            domainSeedSpeciesInput.setInputName(varietyName);
            domainSeedSpeciesInput.setInputType(inputType);
            domainSeedSpeciesInput.setDomain(domainBaulon);
            domainSeedSpeciesInput.setOrganic(domainSeedLotInput.isOrganic());
            domainSeedSpeciesInput.setSpeciesSeed(cps);
            domainSeedSpeciesInput.setInputKey(DomainInputStockUnitService.getLotSpeciesInputKey(refEspece.getCode_espece_botanique(), refEspece.getCode_qualifiant_AEE(), false, false, domainSeedLotInput.isOrganic(), varietyName, domainSeedLotInput.getUsageUnit(), lotPrice.getSeedType()));
            domainSeedSpeciesInput.setSeedType(SeedType.SEMENCES_CERTIFIEES);

            SeedPrice seedPrice = seedPriceDao.newInstance();
            seedPrice.setIncludedTreatment(true);
            seedPrice.setOrganic(domainSeedLotInput.isOrganic());
            seedPrice.setSeedType(SeedType.SEMENCES_CERTIFIEES);

            seedPrice.setCategory(InputPriceCategory.SEEDING_INPUT);
            seedPrice.setDomain(domainBaulon);
            seedPrice.setDisplayName(refEspece.getLibelle_espece_botanique());
            seedPrice.setObjectId(cps.getCode());
            seedPrice.setPrice(0.5);
            seedPrice.setPriceUnit(PriceUnit.EURO_HA);
            seedPrice.setSourceUnit(SeedPlantUnit.GRAINES_PAR_HA.name());
            seedPrice = seedPriceDao.create(seedPrice);

            domainSeedSpeciesInput.setSeedPrice(seedPrice);

            domainSeedSpeciesInputDao.create(domainSeedSpeciesInput);
            domainSeedLotInput.addDomainSeedSpeciesInput(domainSeedSpeciesInput);

            domainSeedSpeciesInputByCropIds.put(domainSeedSpeciesInput.getTopiaId(), domainSeedSpeciesInput);

            DomainPhytoProductInput phytoInput = domainPhytoProductInputDao.newInstance();
            phytoInput.setCode(UUID.randomUUID().toString());
            phytoInput.setInputType(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
            phytoInput.setDomain(domainBaulon);
            phytoInput.setProductType(ProductType.HERBICIDAL);
            phytoInput.setRefInput(produitAcanto);
            phytoInput.setUsageUnit(PhytoProductUnit.L_HA);// from RefActaDose
            phytoInput.setInputName(produitAcanto.getNom_produit());
            phytoInput.setInputKey(DomainInputStockUnitService.getPhytoInputKey(produitAcanto, PhytoProductUnit.L_HA));
            domainPhytoProductInputDao.create(phytoInput);

            domainSeedSpeciesInput.addSpeciesPhytoInputs(phytoInput);
        }

        domainSeedLotInput = domainSeedLotInputDao.create(domainSeedLotInput);

        List<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domainBaulon.getTopiaId());
        DomainSeedLotInputDto domainSeedLotInputDto = DomainInputBinder.bindToSeedingDto(domainSeedLotInput, cropDtos);
        final CroppingPlanEntryDto cropSeedDto = domainSeedLotInputDto.getCropSeedDto();
        Assertions.assertThat(cropSeedDto.getTopiaId()).isEqualTo(wheat.getTopiaId());

        List<DomainSeedSpeciesInputDto> domainSeedSpeciesInputDtos = domainSeedLotInputDto.getSpeciesInputs();

        getCurrentTransaction().commit();
        // Usage declaration start

        List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos = new ArrayList<>();

        RefNuisibleEDI refNuisibleEDI = refNuisibleEDIDao.create(
                RefNuisibleEDI.PROPERTY_ACTIVE, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, BioAgressorType.ADVENTICE,
                RefNuisibleEDI.PROPERTY_MAIN, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_LABEL, "Nuisible"
        );

        final String refNuisibleEDITopiaId = refNuisibleEDI.getTopiaId();
        for (DomainSeedSpeciesInputDto domainSeedSpeciesInputDto : domainSeedSpeciesInputDtos) {

            List<PhytoProductInputUsageDto> phytoProductInputUsageDtos = new ArrayList<>();
            domainSeedSpeciesInputDto.getSpeciesPhytoInputDtos().forEach(
                    dPhytoProductInputDto -> {

                        PhytoProductTargetDto targetDto = PhytoProductTargetDto.builder()
                                .refBioAgressorTargetId(refNuisibleEDITopiaId)
                                .build();

                        PhytoProductInputUsageDto phytoProductInputUsageDto = PhytoProductInputUsageDto.builder()
                                .inputType(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES)
                                .domainPhytoProductInputDto(dPhytoProductInputDto)
                                .targets(ImmutableList.<PhytoProductTargetDto>builder().add(targetDto).build())
                                .code(dPhytoProductInputDto.getCode())
                                .build();

                        phytoProductInputUsageDtos.add(phytoProductInputUsageDto);
                    }

            );


            SeedSpeciesInputUsageDto seedSpeciesInputUsageDto = SeedSpeciesInputUsageDto.builder()
                    .domainSeedSpeciesInputDto(domainSeedSpeciesInputDto)
                    .inputType(domainSeedSpeciesInputDto.getInputType())
                    .usageUnit(domainSeedLotInputDto.getUsageUnit())
                    .speciesIdentifier(domainSeedSpeciesInputDto.getSpeciesSeedDto().getCode())
                    .seedProductInputDtos(ImmutableList.<PhytoProductInputUsageDto>builder().addAll(phytoProductInputUsageDtos).build())
                    .code(domainSeedSpeciesInputDto.getCode())
                    .build();
            seedSpeciesInputUsageDtos.add(seedSpeciesInputUsageDto);
        }

        final double deepness = 2.3;
        SeedLotInputUsageDto seedLotInputUsageDto = SeedLotInputUsageDto.builder()
                .domainSeedLotInputDto(domainSeedLotInputDto)
                .inputType(domainSeedLotInputDto.getInputType())
                .deepness(deepness)
                .croppingPlanEntryIdentifier(domainSeedLotInputDto.getCropSeedDto().getCode())
                .seedingSpeciesDtos(ImmutableList.copyOf(seedSpeciesInputUsageDtos))
                .code(domainSeedLotInputDto.getCode())
                .build();

        RefInterventionAgrosystTravailEDI refSeedingAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEW").findAnyOrNull();
        final double yealdTarget = 64.6;
        final YealdUnit tonneHa = YealdUnit.TONNE_HA;
        SeedingActionUsageDto seedingActionUsageDto = SeedingActionUsageDto.builder()
                .mainActionId(refSeedingAction.getTopiaId())
                .mainActionReference_code(refSeedingAction.getReference_code())
                .mainActionInterventionAgrosyst(refSeedingAction.getIntervention_agrosyst())
                .deepness(deepness)
                .yealdTarget(yealdTarget)
                .seedType(SeedType.SEMENCES_CERTIFIEES)
                .yealdUnit(tonneHa)
                .seedLotInputUsageDtos(ImmutableList.of(seedLotInputUsageDto))
                .build();

        Collection<AbstractActionDto> actionDtos = Collections.singletonList(seedingActionUsageDto);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain()).findAll();
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                actionDtos,
                domainInputStockUnits,
                psBaulon2013, null, null, speciesStadeDtos);

        Collection<AbstractActionDto> persistedActionDtos = actionService.loadPracticedActionsAndUsages(practicedIntervention);

        getPersistenceContext().commit();

        Assertions.assertThat(persistedActionDtos.size()).isEqualTo(1);
        SeedingActionUsageDto persistedSeedingActionUsageDto = (SeedingActionUsageDto) persistedActionDtos.iterator().next();
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionId()).isEqualTo(refSeedingAction.getTopiaId());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionReference_code()).isEqualTo(refSeedingAction.getReference_code());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionInterventionAgrosyst()).isEqualTo(refSeedingAction.getIntervention_agrosyst());
        Assertions.assertThat(persistedSeedingActionUsageDto.getDeepness()).isEqualTo(deepness);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdTarget()).isEqualTo(yealdTarget);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdUnit()).isEqualTo(tonneHa);

        Assertions.assertThat(persistedSeedingActionUsageDto.getSeedLotInputUsageDtos()).isNotEmpty();
        SeedLotInputUsageDto persistedSeedLotInputUsageDto = persistedSeedingActionUsageDto.getSeedLotInputUsageDtos().stream().iterator().next();
        Assertions.assertThat(persistedSeedLotInputUsageDto.getInputType()).isEqualTo(seedLotInputUsageDto.getInputType());
        final Optional<List<SeedSpeciesInputUsageDto>> seedingSpeciesDtos = persistedSeedLotInputUsageDto.getSeedingSpeciesDtos();
        Assertions.assertThat(seedingSpeciesDtos.isPresent()).isTrue();

        final List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos1 = seedingSpeciesDtos.get();
        Assertions.assertThat(seedSpeciesInputUsageDtos1.size()).isEqualTo(wheatCroppingPlanSpeciesSize);
        seedSpeciesInputUsageDtos1.forEach(
                ssu -> {
                    final String domainSeedSpeciesId = ssu.getDomainSeedSpeciesInputDto().getTopiaId().get();
                    final DomainSeedSpeciesInput domainSeedSpeciesInput = domainSeedSpeciesInputByCropIds.remove(
                            domainSeedSpeciesId);
                    Assertions.assertThat(domainSeedSpeciesId).isEqualTo(
                            domainSeedSpeciesInput.getTopiaId());
                    Assertions.assertThat(ssu.getSpeciesIdentifier()).isEqualTo(domainSeedSpeciesInput.getSpeciesSeed().getCode());
                    Assertions.assertThat(ssu.getSeedProductInputDtos().isPresent()).isTrue();
                }
        );
        Assertions.assertThat(domainSeedSpeciesInputByCropIds.isEmpty()).isTrue();

        // domainSeedSpeciesInputByRefEspeceId
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                actionDtos,
                domainInputStockUnits,
                psBaulon2013, null, null, speciesStadeDtos);

        persistedActionDtos = actionService.loadPracticedActionsAndUsages(practicedIntervention);

        Assertions.assertThat(persistedActionDtos.size()).isEqualTo(1);
        persistedSeedingActionUsageDto = (SeedingActionUsageDto) persistedActionDtos.iterator().next();
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionId()).isEqualTo(refSeedingAction.getTopiaId());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionReference_code()).isEqualTo(refSeedingAction.getReference_code());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionInterventionAgrosyst()).isEqualTo(refSeedingAction.getIntervention_agrosyst());
        Assertions.assertThat(persistedSeedingActionUsageDto.getDeepness()).isEqualTo(deepness);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdTarget()).isEqualTo(yealdTarget);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdUnit()).isEqualTo(tonneHa);

        Assertions.assertThat(persistedSeedingActionUsageDto.getSeedLotInputUsageDtos()).isNotEmpty();
        persistedSeedLotInputUsageDto = persistedSeedingActionUsageDto.getSeedLotInputUsageDtos().stream().iterator().next();
        Assertions.assertThat(persistedSeedLotInputUsageDto.getInputType()).isEqualTo(seedLotInputUsageDto.getInputType());

    }

    @Test
    public void testEffectiveSeedingUsageActionPersistance() throws IOException {

        EffectiveIntervention effectiveIntervention = testDatas.createBaulonEffectiveIntervention();
        final Collection<EffectiveSpeciesStade> speciesStades = effectiveIntervention.getSpeciesStades();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);

        CroppingPlanEntry croppingPlanEntry = speciesStades.stream().findAny().get().getCroppingPlanSpecies().getCroppingPlanEntry();
        List<SpeciesStadeDto> speciesStadeDtos = new ArrayList<>();
        speciesStades.forEach(
                speciesStade -> speciesStadeDtos.add(EffectiveCropCycles.getDtoForEffectiveSpeciesStade(speciesStade, translationMap)));

        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        final Domain domainBaulon = zpPlotBaulon1.getPlot().getDomain();
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domainBaulon.getTopiaId());

        Map<String, CroppingPlanEntry> cropByIds = Maps.uniqueIndex(croppingPlanEntries, TopiaEntity::getTopiaId);

        CroppingPlanEntry wheat = cropByIds.get("ble-id");

        i18nService.fillCroppingPlanEntryTranslationMaps(croppingPlanEntries, translationMap);

        DomainSeedLotInput domainSeedLotInput = domainSeedLotInputDao.newInstance();
        domainSeedLotInput.setCode(UUID.randomUUID().toString());
        domainSeedLotInput.setInputType(InputType.SEMIS);
        domainSeedLotInput.setDomain(domainBaulon);
        domainSeedLotInput.setUsageUnit(SeedPlantUnit.GRAINES_PAR_HA);
        domainSeedLotInput.setOrganic(false);
        domainSeedLotInput.setCropSeed(wheat);
        domainSeedLotInput.setInputName(wheat.getName());
        domainSeedLotInput.setInputKey(DomainInputStockUnitService.getLotCropSeedInputKey(wheat, domainSeedLotInput.isOrganic(), domainSeedLotInput.getUsageUnit()));

        SeedPrice lotPrice = seedPriceDao.newInstance();
        lotPrice.setIncludedTreatment(true);
        lotPrice.setOrganic(domainSeedLotInput.isOrganic());
        lotPrice.setSeedType(SeedType.SEMENCES_CERTIFIEES);

        lotPrice.setCategory(InputPriceCategory.SEEDING_INPUT);
        lotPrice.setDomain(domainBaulon);
        lotPrice.setDisplayName(wheat.getName());
        lotPrice.setObjectId(wheat.getCode());
        lotPrice.setPrice(1.1);
        lotPrice.setPriceUnit(PriceUnit.EURO_HA);
        lotPrice.setSourceUnit(SeedPlantUnit.GRAINES_PAR_HA.name());

        domainSeedLotInput.setSeedPrice(lotPrice);
        seedPriceDao.create(lotPrice);

        RefActaTraitementsProduit produitAcanto = refActaTraitementsProduitDao.forAll().findAny();

        Map<String, DomainSeedSpeciesInput> domainSeedSpeciesInputByCropIds = Maps.newHashMap();

        final List<CroppingPlanSpecies> wheatCroppingPlanSpecies = wheat.getCroppingPlanSpecies();
        final int wheatCroppingPlanSpeciesSize = wheatCroppingPlanSpecies.size();
        boolean isCompagne = CollectionUtils.isNotEmpty(wheatCroppingPlanSpecies) && wheatCroppingPlanSpecies.stream().map(CroppingPlanSpecies::getCompagne).anyMatch(Objects::nonNull);
        InputType inputType = isCompagne ? InputType.PLAN_COMPAGNE : InputType.SEMIS;

        for (CroppingPlanSpecies cps : wheatCroppingPlanSpecies) {

            final RefEspece refEspece = cps.getSpecies();
            final RefVariete variety = cps.getVariety();
            final String varietyName = variety != null ? " (" + variety.getLabel() + ")" : "";

            DomainSeedSpeciesInput domainSeedSpeciesInput = domainSeedSpeciesInputDao.newInstance();
            domainSeedSpeciesInput.setCode(UUID.randomUUID().toString());
            domainSeedSpeciesInput.setInputKey(DomainInputStockUnitService.getLotSpeciesInputKey(refEspece.getCode_espece_botanique(), refEspece.getCode_qualifiant_AEE(), false, false, domainSeedLotInput.isOrganic(), varietyName, domainSeedLotInput.getUsageUnit(), lotPrice.getSeedType()));
            domainSeedSpeciesInput.setInputName(varietyName);
            domainSeedSpeciesInput.setInputType(inputType);
            domainSeedSpeciesInput.setDomain(domainBaulon);
            domainSeedSpeciesInput.setOrganic(domainSeedLotInput.isOrganic());
            domainSeedSpeciesInput.setSpeciesSeed(cps);
            domainSeedSpeciesInput.setSeedType(SeedType.SEMENCES_CERTIFIEES);

            SeedPrice seedPrice = seedPriceDao.newInstance();
            seedPrice.setIncludedTreatment(true);
            seedPrice.setOrganic(domainSeedLotInput.isOrganic());
            seedPrice.setSeedType(SeedType.SEMENCES_CERTIFIEES);

            seedPrice.setCategory(InputPriceCategory.SEEDING_INPUT);
            seedPrice.setDomain(domainBaulon);
            seedPrice.setDisplayName(refEspece.getLibelle_espece_botanique());
            seedPrice.setObjectId(cps.getCode());
            seedPrice.setPrice(0.5);
            seedPrice.setPriceUnit(PriceUnit.EURO_HA);
            seedPrice.setSourceUnit(SeedPlantUnit.GRAINES_PAR_HA.name());
            seedPrice = seedPriceDao.create(seedPrice);

            domainSeedSpeciesInput.setSeedPrice(seedPrice);

            domainSeedSpeciesInputDao.create(domainSeedSpeciesInput);
            domainSeedLotInput.addDomainSeedSpeciesInput(domainSeedSpeciesInput);

            domainSeedSpeciesInputByCropIds.put(domainSeedSpeciesInput.getTopiaId(), domainSeedSpeciesInput);

            DomainPhytoProductInput phytoInput = domainPhytoProductInputDao.newInstance();
            phytoInput.setCode(UUID.randomUUID().toString());
            phytoInput.setInputType(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
            phytoInput.setDomain(domainBaulon);
            phytoInput.setProductType(ProductType.HERBICIDAL);
            phytoInput.setRefInput(produitAcanto);
            phytoInput.setUsageUnit(PhytoProductUnit.L_HA);// from RefActaDose
            phytoInput.setInputName(produitAcanto.getNom_produit());
            phytoInput.setInputKey(DomainInputStockUnitService.getPhytoInputKey(produitAcanto, PhytoProductUnit.L_HA));
            domainPhytoProductInputDao.create(phytoInput);

            domainSeedSpeciesInput.addSpeciesPhytoInputs(phytoInput);
        }

        DomainPhytoProductInput phytoInput = domainPhytoProductInputDao.newInstance();
        phytoInput.setCode(UUID.randomUUID().toString());
        phytoInput.setInputType(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);
        phytoInput.setDomain(domainBaulon);
        phytoInput.setProductType(ProductType.HERBICIDAL);
        phytoInput.setRefInput(produitAcanto);
        phytoInput.setUsageUnit(PhytoProductUnit.L_HA);// from RefActaDose
        phytoInput.setInputName(produitAcanto.getNom_produit());
        phytoInput.setInputKey(DomainInputStockUnitService.getPhytoInputKey(produitAcanto, PhytoProductUnit.L_HA));

        domainPhytoProductInputDao.create(phytoInput);

        domainSeedLotInput = domainSeedLotInputDao.create(domainSeedLotInput);

        List<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domainBaulon.getTopiaId());
        DomainSeedLotInputDto domainSeedLotInputDto = DomainInputBinder.bindToSeedingDto(domainSeedLotInput, cropDtos);
        final CroppingPlanEntryDto cropSeedDto = domainSeedLotInputDto.getCropSeedDto();
        Assertions.assertThat(cropSeedDto.getTopiaId()).isEqualTo(wheat.getTopiaId());

        List<DomainSeedSpeciesInputDto> domainSeedSpeciesInputDtos = domainSeedLotInputDto.getSpeciesInputs();

        getCurrentTransaction().commit();
        // Usage declaration start

        List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos = new ArrayList<>();

        RefNuisibleEDI refNuisibleEDI = refNuisibleEDIDao.create(
                RefNuisibleEDI.PROPERTY_ACTIVE, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, BioAgressorType.ADVENTICE,
                RefNuisibleEDI.PROPERTY_MAIN, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_LABEL, "Nuisible"
        );

        final String refNuisibleEDITopiaId = refNuisibleEDI.getTopiaId();
        for (DomainSeedSpeciesInputDto domainSeedSpeciesInputDto : domainSeedSpeciesInputDtos) {

            List<PhytoProductInputUsageDto> phytoProductInputUsageDtos = new ArrayList<>();
            domainSeedSpeciesInputDto.getSpeciesPhytoInputDtos().forEach(
                    dPhytoProductInputDto -> {

                        PhytoProductTargetDto targetDto = PhytoProductTargetDto.builder()
                                .refBioAgressorTargetId(refNuisibleEDITopiaId)
                                .build();

                        PhytoProductInputUsageDto phytoProductInputUsageDto = PhytoProductInputUsageDto.builder()
                                .inputType(InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES)
                                .domainPhytoProductInputDto(dPhytoProductInputDto)
                                .targets(ImmutableList.<PhytoProductTargetDto>builder().add(targetDto).build())
                                .code(dPhytoProductInputDto.getCode())
                                .build();

                        phytoProductInputUsageDtos.add(phytoProductInputUsageDto);
                    }

            );

            SeedSpeciesInputUsageDto seedSpeciesInputUsageDto = SeedSpeciesInputUsageDto.builder()
                    .domainSeedSpeciesInputDto(domainSeedSpeciesInputDto)
                    .inputType(domainSeedSpeciesInputDto.getInputType())
                    .usageUnit(domainSeedLotInputDto.getUsageUnit())
                    .speciesIdentifier(domainSeedSpeciesInputDto.getSpeciesSeedDto().getTopiaId())
                    .seedProductInputDtos(ImmutableList.<PhytoProductInputUsageDto>builder().addAll(phytoProductInputUsageDtos).build())
                    .code(domainSeedSpeciesInputDto.getCode())
                    .build();
            seedSpeciesInputUsageDtos.add(seedSpeciesInputUsageDto);
        }

        final double deepness = 2.3;
        SeedLotInputUsageDto seedLotInputUsageDto = SeedLotInputUsageDto.builder()
                .domainSeedLotInputDto(domainSeedLotInputDto)
                .inputType(domainSeedLotInputDto.getInputType())
                .deepness(deepness)
                .croppingPlanEntryIdentifier(domainSeedLotInputDto.getCropSeedDto().getTopiaId())
                .seedingSpeciesDtos(ImmutableList.copyOf(seedSpeciesInputUsageDtos))
                .code(domainSeedLotInputDto.getCode())
                .build();

        RefInterventionAgrosystTravailEDI refSeedingAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEW").findAnyOrNull();
        final double yealdTarget = 64.6;
        final YealdUnit tonneHa = YealdUnit.TONNE_HA;
        SeedingActionUsageDto seedingActionUsageDto = SeedingActionUsageDto.builder()
                .mainActionId(refSeedingAction.getTopiaId())
                .mainActionReference_code(refSeedingAction.getReference_code())
                .mainActionInterventionAgrosyst(refSeedingAction.getIntervention_agrosyst())
                .deepness(deepness)
                .yealdTarget(yealdTarget)
                .seedType(SeedType.SEMENCES_CERTIFIEES)
                .yealdUnit(tonneHa)
                .seedLotInputUsageDtos(ImmutableList.of(seedLotInputUsageDto))
                .build();

        Collection<AbstractActionDto> actionDtos = Collections.singletonList(seedingActionUsageDto);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domainBaulon).findAll();
        actionService.createOrUpdateEffectiveInterventionActionAndUsages(
                effectiveIntervention,
                actionDtos,
                domainInputStockUnits,
                zpPlotBaulon1, null, null, speciesStadeDtos, croppingPlanEntry, false);

        Collection<AbstractActionDto> persistedActionDtos = actionService.loadEffectiveActionsAndUsages(effectiveIntervention);

        getPersistenceContext().commit();

        Assertions.assertThat(persistedActionDtos.size()).isEqualTo(1);
        SeedingActionUsageDto persistedSeedingActionUsageDto = (SeedingActionUsageDto) persistedActionDtos.iterator().next();
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionId()).isEqualTo(refSeedingAction.getTopiaId());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionReference_code()).isEqualTo(refSeedingAction.getReference_code());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionInterventionAgrosyst()).isEqualTo(refSeedingAction.getIntervention_agrosyst());
        Assertions.assertThat(persistedSeedingActionUsageDto.getDeepness()).isEqualTo(deepness);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdTarget()).isEqualTo(yealdTarget);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdUnit()).isEqualTo(tonneHa);

        Assertions.assertThat(persistedSeedingActionUsageDto.getSeedLotInputUsageDtos()).isNotEmpty();
        SeedLotInputUsageDto persistedSeedLotInputUsageDto = persistedSeedingActionUsageDto.getSeedLotInputUsageDtos().stream().iterator().next();
        Assertions.assertThat(persistedSeedLotInputUsageDto.getInputType()).isEqualTo(seedLotInputUsageDto.getInputType());
        final Optional<List<SeedSpeciesInputUsageDto>> seedingSpeciesDtos = persistedSeedLotInputUsageDto.getSeedingSpeciesDtos();
        Assertions.assertThat(seedingSpeciesDtos.isPresent()).isTrue();

        final List<SeedSpeciesInputUsageDto> seedSpeciesInputUsageDtos1 = seedingSpeciesDtos.get();
        Assertions.assertThat(seedSpeciesInputUsageDtos1.size()).isEqualTo(wheatCroppingPlanSpeciesSize);
        seedSpeciesInputUsageDtos1.forEach(
                ssu -> {
                    final String domainSeedSpeciesId = ssu.getDomainSeedSpeciesInputDto().getTopiaId().get();
                    final DomainSeedSpeciesInput domainSeedSpeciesInput = domainSeedSpeciesInputByCropIds.remove(
                            domainSeedSpeciesId);
                    Assertions.assertThat(domainSeedSpeciesId).isEqualTo(
                            domainSeedSpeciesInput.getTopiaId());
                    Assertions.assertThat(ssu.getSpeciesIdentifier()).isEqualTo(domainSeedSpeciesInput.getSpeciesSeed().getTopiaId());
                    Assertions.assertThat(ssu.getSeedProductInputDtos().isPresent()).isTrue();
                }
        );
        Assertions.assertThat(domainSeedSpeciesInputByCropIds.isEmpty()).isTrue();

        // domainSeedSpeciesInputByRefEspeceId
        actionService.createOrUpdateEffectiveInterventionActionAndUsages(
                effectiveIntervention,
                actionDtos,
                domainInputStockUnits,
                zpPlotBaulon1, null, null, speciesStadeDtos, croppingPlanEntry, false);

        persistedActionDtos = actionService.loadEffectiveActionsAndUsages(effectiveIntervention);

        Assertions.assertThat(persistedActionDtos.size()).isEqualTo(1);
        persistedSeedingActionUsageDto = (SeedingActionUsageDto) persistedActionDtos.iterator().next();
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionId()).isEqualTo(refSeedingAction.getTopiaId());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionReference_code()).isEqualTo(refSeedingAction.getReference_code());
        Assertions.assertThat(persistedSeedingActionUsageDto.getMainActionInterventionAgrosyst()).isEqualTo(refSeedingAction.getIntervention_agrosyst());
        Assertions.assertThat(persistedSeedingActionUsageDto.getDeepness()).isEqualTo(deepness);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdTarget()).isEqualTo(yealdTarget);
        Assertions.assertThat(persistedSeedingActionUsageDto.getYealdUnit()).isEqualTo(tonneHa);

        Assertions.assertThat(persistedSeedingActionUsageDto.getSeedLotInputUsageDtos()).isNotEmpty();
        persistedSeedLotInputUsageDto = persistedSeedingActionUsageDto.getSeedLotInputUsageDtos().stream().iterator().next();
        Assertions.assertThat(persistedSeedLotInputUsageDto.getInputType()).isEqualTo(seedLotInputUsageDto.getInputType());

    }

    @Test
    public void testHarvestingActionPersistance() throws IOException {

        PracticedSystem psBaulon2013 = testDatas.createTestPraticedSystems();

        Domain domainBaulon = psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain();

        LivestockUnit lsLivestockUnit = livestockUnitTopiaDao.forDomainEquals(domainBaulon).findAny();

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain().getTopiaId());
        Map<String, CroppingPlanEntry> cropByIds = Maps.uniqueIndex(croppingPlanEntries, TopiaEntity::getTopiaId);
        CroppingPlanEntry acrop = cropByIds.get("melange-variete-ble-id");// there is more than one species

        PracticedIntervention practicedIntervention = testDatas.createPracticedIntervention(psBaulon2013, acrop);

        final Collection<PracticedSpeciesStade> speciesStades = practicedIntervention.getSpeciesStades();

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);

        List<SpeciesStadeDto> speciesStadeDtos = new ArrayList<>();
        speciesStades.forEach(
                speciesStade -> speciesStadeDtos.add(PracticedSystems.getDtoForPracticedSpeciesStade(speciesStade, translationMap)));


        Collection<AbstractActionDto> actionDtos = new ArrayList<>();

        RefInterventionAgrosystTravailEDI refActionPaturage = refInterventionAgrosystTravailEDIDao
                .forReference_codeEquals(ActionServiceImpl.PASTURE)
                .findAnyOrNull();

        String cattleCode = lsLivestockUnit.getCattles().iterator().next().getCode();

        List<HarvestingActionValorisationDto> valorisationDtos = new ArrayList<>();

        Map<CroppingPlanSpecies, RefQualityCriteria> refQualityCriterias = new HashMap<>();

        Assertions.assertThat(acrop).isNotNull();
        Assertions.assertThat(acrop.getCroppingPlanSpecies()).isNotNull();
        Assertions.assertThat(acrop.getCroppingPlanSpecies().size()).isGreaterThan(1);

        acrop.getCroppingPlanSpecies().forEach(
                cps -> {
                    RefQualityCriteria rcq = testDatas.createRefQualityCriteria(
                            cps.getSpecies(), cps.getSpecies().getLibelle_espece_botanique(), Sector.GRANDES_CULTURES, null);
                    refQualityCriterias.put(cps, rcq);
                });

        Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = new HashMap<>();

        Map<String, List<Sector>> sectorByCodeEspeceBotaniqueCodeQualifiant = practicedSystemService.getSectorByCodeEspceBotaniqueCodeQualifiant(psBaulon2013.getGrowingSystem().getTopiaId(), psBaulon2013.getCampaigns());

        RefDestination destination0 = refDestinationDao.forCode_destination_AEquals("D0036").findUnique();
        RefDestination destination1 = refDestinationDao.forCode_destination_AEquals("D0037").findUnique();
        acrop.getCroppingPlanSpecies().forEach(
                cps -> {
                    final Pair<String, String> codeEspeceBotaniqueCodeQualifantAEE = Pair.of(cps.getSpecies().getCode_espece_botanique(), cps.getSpecies().getCode_qualifiant_AEE());
                    final ArrayList<Pair<String, String>> codeEspeceBotaniqueCodeQualifantAEEs = new ArrayList<>();
                    codeEspeceBotaniqueCodeQualifantAEEs.add(codeEspeceBotaniqueCodeQualifantAEE);

                    speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee.put(cps.getCode(), codeEspeceBotaniqueCodeQualifantAEEs);

                    Collection<QualityCriteriaDto> qualityCriteriaDtos = new ArrayList<>();
                    RefQualityCriteria refQualityCriteria = refQualityCriterias.get(cps);
                    if (refQualityCriteria != null) {
                        qualityCriteriaDtos.add(QualityCriteriaDto.builder()
                                .refQualityCriteriaId(refQualityCriteria.getTopiaId())
                                .binaryValue(true)
                                .build());
                    }
                    HarvestingPriceDto pdto = HarvestingPriceDto.builder()
                            .practicedSystemId(psBaulon2013.getTopiaId())
                            .objectId(PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(cps, destination0))
                            .displayName("HarvestingPrice")
                            .sourceUnit(PriceUnit.EURO_T.name())
                            .priceUnit(PriceUnit.EURO_T)
                            .category(InputPriceCategory.HARVESTING_ACTION)
                            .price(1.256)
                            .build();

                    HarvestingActionValorisationDto valDto0 = HarvestingActionValorisationDto.builder()
                            .beginMarketingPeriod(5)
                            .beginMarketingPeriodDecade(6)
                            .beginMarketingPeriodCampaign(2022)
                            .endingMarketingPeriod(6)
                            .endingMarketingPeriodDecade(7)
                            .endingMarketingPeriodCampaign(2022)
                            .speciesCode(cps.getCode())
                            .destinationId(destination0.getTopiaId())
                            .destinationName(destination0.getDestination())
                            .salesPercent(100)
                            .selfConsumedPersent(0)
                            .noValorisationPercent(0)
                            .yealdAverage(86.9)
                            .yealdUnit(YealdUnit.TONNE_HA)
                            .organicCrop(false)
                            .qualityCriteriaDtos(qualityCriteriaDtos)
                            .priceDto(pdto)
                            .build();
                    valorisationDtos.add(valDto0);

                    HarvestingActionValorisationDto valDto1 = HarvestingActionValorisationDto.builder()
                            .beginMarketingPeriod(5)
                            .beginMarketingPeriodDecade(7)
                            .beginMarketingPeriodCampaign(2022)
                            .endingMarketingPeriod(6)
                            .endingMarketingPeriodDecade(8)
                            .endingMarketingPeriodCampaign(2022)
                            .speciesCode(cps.getCode())
                            .destinationId(destination1.getTopiaId())
                            .destinationName(destination1.getDestination())
                            .salesPercent(100)
                            .selfConsumedPersent(0)
                            .noValorisationPercent(0)
                            .yealdAverage(86.9)
                            .yealdUnit(YealdUnit.KG_M2)
                            .organicCrop(false)
                            .qualityCriteriaDtos(qualityCriteriaDtos)
                            .priceDto(pdto)
                            .build();
                    valorisationDtos.add(valDto1);
                }
        );

        HarvestingActionDto actionDto = HarvestingActionDto
                .builder()
                .practicedInterventionId(practicedIntervention.getTopiaId())
                .cattleCode(cattleCode)
                .pasturingAtNight(false)
                .mainActionId(refActionPaturage.getTopiaId())
                .mainActionReference_code(refActionPaturage.getReference_code())
                .mainActionInterventionAgrosyst(refActionPaturage.getIntervention_agrosyst())
                .valorisationDtos(valorisationDtos)
                .build();


        actionDtos.add(actionDto);

        Collection<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain()).findAll();

        // test CREATE
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                actionDtos,
                domainInputStockUnits,
                psBaulon2013,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspeceBotaniqueCodeQualifiant,
                speciesStadeDtos
        );


        Assertions.assertThat(abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().count()).isEqualTo(1);
        Optional<AbstractAction> persistedAction = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().findAny();
        Assertions.assertThat(persistedAction.isPresent()).isTrue();

        HarvestingAction action = (HarvestingAction) persistedAction.get();

        List<HarvestingActionValorisation> valorisations = new ArrayList<>(action.getValorisations());
        Assertions.assertThat(valorisations.size()).isEqualTo(acrop.getCroppingPlanSpecies().size() * 2);
        int cropIndex = 0;
        for (int i = 0; i < valorisations.size(); i++) {
            RefDestination d = (i % 2 == 0) ? destination0 : destination1;
            CroppingPlanSpecies cps = acrop.getCroppingPlanSpecies().get(cropIndex);
            HarvestingActionValorisation valorisation = valorisations.get(i);
            Assertions.assertThat(valorisation.getDestination()).isEqualTo(d);
            Assertions.assertThat(valorisation.getSpeciesCode()).isEqualTo(cps.getCode());
            Assertions.assertThat(valorisation.getQualityCriteria().size()).isEqualTo(1);

            final HarvestingPrice priceForValorisation = pricesService.getPriceForValorisation(valorisation);
            Assertions.assertThat(priceForValorisation).isNotNull();

            if ((i % 2 != 0)) cropIndex = cropIndex + 1;
        }

        Collection<AbstractActionDto> persistedActionDtos = actionService.loadPracticedActionsAndUsages(practicedIntervention);

        // test UPDATE
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                persistedActionDtos,
                domainInputStockUnits,
                psBaulon2013, speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee, sectorByCodeEspeceBotaniqueCodeQualifiant, speciesStadeDtos);

        domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain()).findAll();
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                persistedActionDtos,
                domainInputStockUnits,
                psBaulon2013, speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee, sectorByCodeEspeceBotaniqueCodeQualifiant, speciesStadeDtos);


        Assertions.assertThat(abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().count()).isEqualTo(1);
        persistedAction = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).stream().findAny();
        Assertions.assertThat(persistedAction.isPresent()).isTrue();

        action = (HarvestingAction) persistedAction.get();
        valorisations = new ArrayList<>(action.getValorisations());

        Assertions.assertThat(valorisations.size()).isEqualTo(acrop.getCroppingPlanSpecies().size() * 2);
        cropIndex = 0;
        for (int i = 0; i < valorisations.size(); i++) {
            RefDestination d = (i % 2 == 0) ? destination0 : destination1;
            CroppingPlanSpecies cps = acrop.getCroppingPlanSpecies().get(cropIndex);
            HarvestingActionValorisation valorisation = valorisations.get(i);
            Assertions.assertThat(valorisation.getDestination()).isEqualTo(d);
            Assertions.assertThat(valorisation.getSpeciesCode()).isEqualTo(cps.getCode());
            Assertions.assertThat(valorisation.getQualityCriteria().size()).isEqualTo(1);

            final HarvestingPrice priceForValorisation = pricesService.getPriceForValorisation(valorisation);
            Assertions.assertThat(priceForValorisation).isNotNull();

            if ((i % 2 != 0)) cropIndex = cropIndex + 1;
        }

        // Update with remove Valorisation Quality criteria and price

        List<HarvestingActionValorisationDto> updatedValorisationDtos = new ArrayList<>();
        AbstractActionDto persistedActionDto = persistedActionDtos.iterator().next();

        HarvestingActionDto actionDto_ = (HarvestingActionDto) persistedActionDto;
        List<HarvestingActionValorisationDto> actionValorisationDtos = new ArrayList<>(actionDto_.getValorisationDtos());

        HarvestingActionValorisationDto val0 = actionValorisationDtos.remove(0);
        updatedValorisationDtos.add(val0);

        HarvestingActionValorisationDto val1 = actionValorisationDtos.remove(1);
        HarvestingActionValorisationDto val11 = val1.toBuilder()
                .qualityCriteriaDtos(null)
                .priceDto(null)
                .build();
        updatedValorisationDtos.add(val11);

        actionDto_ = actionDto_.toBuilder()
                .valorisationDtos(updatedValorisationDtos)
                .build();

        persistedActionDtos.clear();
        persistedActionDtos.add(actionDto_);

        // test REMOVE
        actionService.createOrUpdatePracticedInterventionActionAndUsages(
                practicedIntervention,
                persistedActionDtos,
                domainInputStockUnits,
                psBaulon2013,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspeceBotaniqueCodeQualifiant,
                speciesStadeDtos);

        final HarvestingActionValorisationTopiaDao valorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();

        final List<String> topiaIds = actionValorisationDtos.stream().map(
                        HarvestingActionValorisationDto::getTopiaId)
                .filter(Optional::isPresent)
                .map(Optional::get).toList();

        Assertions.assertThat(topiaIds).isNotEmpty();

        final boolean exists = valorisationDao.forTopiaIdIn(topiaIds).exists();
        Assertions.assertThat(exists).isFalse();
    }
}
