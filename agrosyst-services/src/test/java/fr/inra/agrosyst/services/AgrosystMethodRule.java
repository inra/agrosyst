package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.referential.ImportServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.helpers.LogLog;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class AgrosystMethodRule implements BeforeEachCallback, AfterEachCallback, AfterAllCallback {

    private final static Log LOGGER = LogFactory.getLog(AgrosystMethodRule.class);

    private ExtensionContext.Store getStore(ExtensionContext context) {
        return context.getStore(ExtensionContext.Namespace.create(getClass(), context));
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        Method testMethod = context.getTestMethod().get();
        TestServiceContext testServiceContext = new TestServiceContext(testMethod);
        testServiceContext.beforeEachTest();
        getStore(context).put(testMethod, testServiceContext);

        Optional<Object> testInstance = context.getTestInstance();
        Method method = AbstractAgrosystTest.class.getMethod("setServiceContext", TestServiceContext.class);
        method.invoke(testInstance.get(), testServiceContext);
    }

    @Override
    public void afterEach(ExtensionContext context) {
        Method testMethod = context.getTestMethod().get();
        TestServiceContext testServiceContext = getStore(context).remove(testMethod, TestServiceContext.class);
        testServiceContext.afterEachTest();
        recursiveDelete(null, false);
        Runtime.getRuntime().gc();
    }

    @Override
    public void afterAll(ExtensionContext context) throws Exception {
        recursiveDelete(null, true);
        Runtime.getRuntime().gc();
    }

    public static void recursiveDelete(File fileOrDir, boolean removeDirectories) {
        if (fileOrDir == null) {
            String javaIoTmpDir = ImportServiceImpl.JAVA_IO_TMPDIR;
            final File sysTempDir = new File(Objects.requireNonNull(javaIoTmpDir));
            for (File innerFile : Objects.requireNonNull(sysTempDir.listFiles())) {
                recursiveDelete(innerFile, removeDirectories);
            }
        } else if (fileOrDir.isDirectory()) {
            // recursively delete contents
            try {
                for (File innerFile : Objects.requireNonNull(fileOrDir.listFiles())) {
                    recursiveDelete(innerFile, removeDirectories);
                }
                if (removeDirectories) {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("remove dir:" + fileOrDir.getAbsolutePath() + "/" + fileOrDir.getName());
                    }
                    fileOrDir.delete();
                }

            } catch (Exception e) {
                LogLog.error("failed to remove directory content as null", e);
            }
        } else {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("remove file:" + fileOrDir.getAbsolutePath() + "/" + fileOrDir.getName());
            }
            fileOrDir.delete();
        }

    }

}
