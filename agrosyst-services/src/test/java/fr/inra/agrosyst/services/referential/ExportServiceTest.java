package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.referential.ExportService;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Test du service d'export.
 * 
 * @author Eric Chatellier
 */
public class ExportServiceTest extends AbstractAgrosystTest {
    
    private static final Log LOGGER = LogFactory.getLog(AbstractAgrosystTest.class);
    
    protected ImportService importService;
    protected ExportService exportService;
    protected ReferentialService referentialService;

    @BeforeEach
    public void setupServices() {
        importService = serviceFactory.newService(ImportService.class);
        exportService = serviceFactory.newService(ExportService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
    }

    /**
     * Test de contenu du fichier csv d'export de certains sol arvalis.
     * 
     */
    @Test
    public void testExportSolsArvalis() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importSolArvalis();
        
        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportSolArvalisCSV(null);
             InputStream regionsStream = ImportServiceTest.class.getResourceAsStream("/referentiels/test/sols_regions.csv")) {

            ImportResult result2 = importService.importSolArvalisCSV(is, regionsStream);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }
    
    /**
     * Test de contenu du fichier csv d'export de certains sol arvalis.
     * 
     */
    @Test
    public void testExportOrientationEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importOrientationEdi();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportOrientationEdiCSV(null)) {
            ImportResult result2 = importService.importOrientationEdiCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }
    
    /**
     * Test de contenu du fichier csv d'export des materiels BCMA (automoteur).
     * 
     */
    @Test
    public void testExportMaterielAutomoteur() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importMaterielsAutomoteur();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportMaterielAutomoteursCSV(null)) {
            ImportResult result2 = importService.importMaterielAutomoteursCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }
    
    /**
     * Test de contenu du fichier csv d'export des materiels BCMA (irrigation).
     * 
     */
    @Test
    public void testExportMaterielIrrigation() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importMaterielsIrrigation();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportMaterielIrrigationCSV(null)){
            ImportResult result2 = importService.importMaterielIrrigationCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }
    
    /**
     * Test de contenu du fichier csv d'export des materiels BCMA (traction).
     * 
     */
    @Test
    public void testExportMaterielOutils() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importMaterielsOutils();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportMaterielOutilsCSV(null)) {
            ImportResult result2 = importService.importMaterielOutilsCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }
    
    /**
     * Test de contenu du fichier csv d'export des materiels BCMA (traction).
     * 
     */
    @Test
    public void testExportMaterielTraction() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importMaterielsTracteur();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportMaterielTracteursCSV(null)) {
            ImportResult result2 = importService.importMaterielTracteursCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test de l'export des statut legals.
     */
    @Test
    public void testExportLegalStatus() throws IOException {

        ImportResult result = testDatas.importLegalStatus();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportLegalStatusCSV(null)){
            ImportResult result2 = importService.importLegalStatusCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test de l'export des especes.
     */
    @Test
    public void testExportEspeces() throws IOException {

        ImportResult result = testDatas.importRefEspeces();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try ( InputStream is = exportService.exportEspeces(null)){
            ImportResult result2 = importService.importEspeces(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test de l'export des varietes geves.
     * 
     * FIXME echatellier 20130913 test fails because on null entites retreived by TopiaDao !?!
     */
    @Test
    @Disabled
    public void testExportVarieteGeves() throws IOException {

        ImportResult result = testDatas.importVarieteGeves();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportEspecesToVarietes(null)) {
            ImportResult result2 = importService.importVarietesGeves(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test de l'export des variete plant grappe.
     */
    @Test
    public void testExportVarietePlantGrape() throws IOException {

        ImportResult result = testDatas.importVarietePlantGrape();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportVarietesPlantGrape(null)){
            ImportResult result2 = importService.importVarietesPlantGrape(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test de l'export des clones plant grape.
     */
    @Test
    public void testExportClonesPlantGrape() throws IOException {

        ImportResult result = testDatas.importClonesPlantGrape();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportClonesPlantGrape(null)){
            ImportResult result2 = importService.importClonesPlantGrape(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test de l'export de especes to varietes.
     */
    @Test
    public void testExportEspecesToVarietes() throws IOException {

        ImportResult result = testDatas.importEspecesToVarietes();
        
        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportEspecesToVarietes(null)) {
            ImportResult result2 = importService.importEspecesToVarietes(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test de l'export du referentiel OTEX.
     */
    @Test
    public void testExportOTEX() throws IOException {

        ImportResult result = testDatas.importOTEX();
        
        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportOtexCSV(null)) {
            ImportResult result2 = importService.importOtexCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test du résultat de l'export du référentiel parcelle zonage edi.
     */
    @Test
    public void testExportParcelleZonageEdi() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importParcelleZonageEdi();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportZonageParcelleEdi(null)){
            ImportResult result2 = importService.importZonageParcelleEdi(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test du résultat de l'export du référentiel sol profondeur indigo.
     */
    @Test
    public void testExportSolProfondeurIndigo() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importSolProfondeurIndigo();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportSolProfondeurIndigo(null)){
            ImportResult result2 = importService.importSolProfondeurIndigo(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    /**
     * Test du résultat de l'export du référentiel sol texture geppa.
     */
    @Test
    public void testExportSolTextureGeppa() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importSolTextureGeppa();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try ( InputStream is = exportService.exportSolTextureGeppa(null)) {
            ImportResult result2 = importService.importSolTextureGeppa(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }
    
    /**
     * Test du résultat de l'export du référentiel sol texture geppa.
     */
    @Test
    public void testExportSolCaracteristiquesIndigo() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importSolCaracteristiquesIndigo();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportSolCarateristiquesIndigo(null)) {
            ImportResult result2 = importService.importSolCarateristiquesIndigo(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        }
    }

    @Test
    public void testExportUniteEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importUnitesEdi();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportUniteEDI(null)) {
            ImportResult result2 = importService.importUniteEDI(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportFertyMinUnifa() throws IOException {
        // categ=109 forme=Granulé n=27,000000 p2O5=null k2O=null bore=null calcium=null fer=null manganese=null molybdene=null mgO=null oxyde_de_sodium=null sO3=null cuivre=null zinc=null

        // make import to have some data to export
        ImportResult result = testDatas.importFertiMinUnifa();

        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportFertiMinUNIFA(null)) {
            
            ImportResult result2 = importService.importFertiMinUNIFA(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportAdventices() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importAdventices();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportAdventices(null)) {

            ImportResult result2 = importService.importAdventices(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportNuisiblesEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importNuisiblesEDI();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportNuisiblesEDI(null)) {

            ImportResult result2 = importService.importNuisiblesEDI(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportFertiOrga() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importFertiOrga();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportFertiOrga(null)) {

            ImportResult result2 = importService.importFertiOrga(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportStadeEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importStadesEDI();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportStadesEdiCSV(null)) {

            ImportResult result2 = importService.importStadesEdiCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportActionAgrosystTravailEdi() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importInterventionAgrosystTravailEdi();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportInterventionAgrosystTravailEdiCSV(null)){

            ImportResult result2 = importService.importInterventionAgrosystTravailEdiCSV(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportStationMeteo() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importStationMeteo();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportStationMeteo(null)) {

            ImportResult result2 = importService.importStationMeteo(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportMesure() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importMesure();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportMesure(null)) {

            ImportResult result2 = importService.importMesure(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportSupportOrganeEdi() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importSupportOrganeEdi();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportSupportOrganeEDI(null)) {

            ImportResult result2 = importService.importSupportOrganeEDI(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportStadeNuisibleEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importStadeNuisibleEDI();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportStadeNuisibleEDI(null)) {

            ImportResult result2 = importService.importStadeNuisibleEDI(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportTypeNotationEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importTypeNotationEDI();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportTypeNotationEDI(null)) {

            ImportResult result2 = importService.importTypeNotationEDI(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportValeurQualitativeEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importValeurQualitativeEDI();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportValeurQualitativeEDI(null)) {

            ImportResult result2 = importService.importValeurQualitativeEDI(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportUnitesQualifiantEDI() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importUnitesQualifiantEDI();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportUnitesQualifiantEDI(null)) {

            ImportResult result2 = importService.importUnitesQualifiantEDI(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportActaTraitementsProduits() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importActaTraitementsProduits();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportActaTraitementsProducts(null)) {

            ImportResult result2 = importService.importActaTraitementsProduits(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportElementVoisinage() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importElementVoisinage();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportElementVoisinage(null)){

            ImportResult result2 = importService.importElementVoisinage(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportPhytoSubstanceActiveIphy() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importPhytoSubstanceActiveIphy();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportPhytoSubstanceActiveIphy(null)) {

            ImportResult result2 = importService.importPhytoSubstanceActiveIphy(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());

        }
    }

    @Test
    public void testExportTypeAgriculture() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importTypeAgriculture();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportTypeAgriculture(null)){

            ImportResult result2 = importService.importTypeAgriculture(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());

        }
    }

    @Test
    public void testExportDosageSpc() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importActaDosageSpc();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportActaDosageSpc(null)) {

            ImportResult result2 = importService.importActaDosageSpc(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportTraitSdC() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importTraitSdC();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportTraitSdC(null)){

            ImportResult result2 = importService.importTraitSdC(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportCultureEdiGroupeCouvSol() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importCultureEdiGroupeCouvSol();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportCultureEdiGroupeCouvSol(null)) {

            ImportResult result2 = importService.importCultureEdiGroupeCouvSol(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportCouvSolPerenne() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importCouvSolPerenne();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportCouvSolPerenne(null)) {

            ImportResult result2 = importService.importCouvSolPerenne(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
    
    @Test
    public void testExportCouvSolAnnuelle() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importCouvSolAnnuelle();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportCouvSolAnnuelle(null)) {

            ImportResult result2 = importService.importCouvSolAnnuelle(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportZoneClimatiqueIphy() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importZoneClimatiqueIphy();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportZoneClimatiqueIphy(null)) {

            ImportResult result2 = importService.importZoneClimatiqueIphy(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportDestination() throws IOException {
        testDatas.importRefEspeces();
        // make import to have some data to export
        ImportResult result = testDatas.importDestination();

        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportDestination(null)) {

            ImportResult result2 = importService.importRefDestination(is);

            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportRefHarvestingPrices() throws IOException {
        testDatas.importRefEspeces();
        testDatas.importDestination();
        // make import to have some data to export
        ImportResult result = testDatas.importValidRefHarvestingPrices();

        ImportResult result2;
        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportRefHarvestingPrice(null)) {
            result2 = importService.importRefHarvestingPrice(is);
        }

        Assertions.assertEquals(0, result2.getCreated());
        Assertions.assertEquals(result.getCreated(), result2.getUpdated());
        Assertions.assertFalse(result2.hasErrors());
    }
    
    @Test
    public void testExportAGS_Amortissement() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importAGS_Amortissement();
        
        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportAgsAmortissement(null)) {
            
            ImportResult result2 = importService.importAgsAmortissement(is);
            
            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }

    @Test
    public void testExportRefFeedbackRouter()throws IOException {
    
        String[] email0s = {"marie.coquet@acta.asso.fr", "mathilde.lefevre@adage35.org", "david.bouille@bretagne.chambagri.fr", "maylis.carre@civam.org",
                "philippe.tresch@idele.fr", "francois.cena@smb.chambagri.fr", "thomas.achkar@apca.chamagri.fr", "maxime.lienard@apca.chambagri.fr", "Ardavan.SOLEYMANI@astredhor.fr", "Maxime.PAOLUCCI@astredhor.fr", "laurent.deliere@inrae.fr", "cathy.eckert@ctifl.fr", "aurelie.lequeux-sauvage@apca.chambagri.fr", "baptiste.labeyrie@ctifl.fr", "jl.sagnes@agri82.fr"};
        for (String email0 : email0s) {
            testDatas.createUser(email0);
        }
        
        // make import to have some data to export
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refFeedbackRouter.csv")) {
            importService.importRefFeedbackRouter(stream);
        }
    
        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportRefFeedbackRouter(null)) {
        
            ImportResult result2 = importService.importRefFeedbackRouter(is);
    
            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(168, result2.getUpdated());
            Assertions.assertEquals(0, result2.getDeleted());
            Assertions.assertEquals(0, result2.getIgnored());
            Assertions.assertEquals(0, result2.getErrors().size());
        }
    }
    
    @Test
    public void testExportOtherInput() throws IOException {
        // make import to have some data to export
        ImportResult result = testDatas.importRefOtherInput();
        
        // test export
        // soyons fou, on réimporte le fichier exporté
        try (InputStream is = exportService.exportRefOtherInput(null)) {
            
            ImportResult result2 = importService.importRefOtherInputCSV(is);
            
            Assertions.assertEquals(0, result2.getCreated());
            Assertions.assertEquals(result.getCreated(), result2.getUpdated());
            Assertions.assertFalse(result2.hasErrors());
        }
    }
}
