package fr.inra.agrosyst.services.plots;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.BufferStrip;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainImpl;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.WaterFlowDistance;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.performance.PerformanceTopiaDao;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.edaplos.EdaplosPersisterMock;
import fr.inra.agrosyst.services.edaplos.PlotFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

class PlotTopiaDaoTest extends AbstractAgrosystTest {
    
    protected DomainService domainService;
    protected ReferentialService referentialService;
    protected PlotService plotService;
    
    protected DomainTopiaDao domainDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected PerformanceTopiaDao performanceTopiaDao;
    protected ZoneTopiaDao zoneTopiaDao;
    protected PlotTopiaDao plotTopiaDao;
    protected AgrosystUserTopiaDao userTopiaDao;
    
    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        loginAsAdmin();
        alterSchema();
    
        domainDao = getPersistenceContext().getDomainDao();
        growingSystemDao = getPersistenceContext().getGrowingSystemDao();
        performanceTopiaDao = getPersistenceContext().getPerformanceDao();
        zoneTopiaDao = getPersistenceContext().getZoneDao();
        plotTopiaDao = getPersistenceContext().getPlotDao();
        userTopiaDao = getPersistenceContext().getAgrosystUserDao();
        
        domainService = serviceFactory.newService(DomainService.class);
        plotService = serviceFactory.newService(PlotService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        
        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.createTestPlots();
    }
    
    @Test
    void findAllByDomainId() {
        Domain domainBaulon = domainDao.forNameEquals("Baulon").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        List<Plot> plots = plotTopiaDao.findAllByDomainId(domainBaulon.getTopiaId());
        Assertions.assertEquals(4, plots.size());
    }
    
    @Test
    void findAllOrderByNameForDomain() {
        Domain domainBaulon = domainDao.forNameEquals("Baulon").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        List<Plot> plots = plotTopiaDao.findAllOrderByNameForDomain(domainBaulon);
        Assertions.assertEquals(4, plots.size());
        List<String> expectedResult = plots.stream().map(Plot::getName).sorted().collect(Collectors.toList());
        List<String> result = plots.stream().map(Plot::getName).collect(Collectors.toList());
        Assertions.assertEquals(expectedResult, result);
    }
    
    @Test
    void findAllFreePlotInDomain() {
        Domain domainBaulon = domainDao.forNameEquals("Baulon").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        
        List<Plot> expectedResult = new ArrayList<>();
        expectedResult.add(plotTopiaDao.create(Plot.PROPERTY_DOMAIN, domainBaulon,
                Plot.PROPERTY_NAME, "freePlot0",
                Plot.PROPERTY_CODE, UUID.randomUUID().toString(),
                Plot.PROPERTY_WATER_FLOW_DISTANCE, WaterFlowDistance.LESS_THAN_THREE,
                Plot.PROPERTY_MAX_SLOPE, MaxSlope.TWO_TO_FIVE,
                Plot.PROPERTY_BUFFER_STRIP, BufferStrip.BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW));
    
        expectedResult.add(plotTopiaDao.create(Plot.PROPERTY_DOMAIN, domainBaulon,
                Plot.PROPERTY_NAME, "freePlot1",
                Plot.PROPERTY_CODE, UUID.randomUUID().toString(),
                Plot.PROPERTY_WATER_FLOW_DISTANCE, WaterFlowDistance.LESS_THAN_THREE,
                Plot.PROPERTY_MAX_SLOPE, MaxSlope.TWO_TO_FIVE,
                Plot.PROPERTY_BUFFER_STRIP, BufferStrip.BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW));
    
        Domain domainBouineliere = domainDao.forNameEquals("La Bouinelière").addEquals(Domain.PROPERTY_CAMPAIGN, 2014).findUnique();
        Plot oneFreePlot = plotTopiaDao.create(Plot.PROPERTY_DOMAIN, domainBouineliere,
                Plot.PROPERTY_NAME, "freePlot2",
                Plot.PROPERTY_CODE, UUID.randomUUID().toString(),
                Plot.PROPERTY_WATER_FLOW_DISTANCE, WaterFlowDistance.LESS_THAN_THREE,
                Plot.PROPERTY_MAX_SLOPE, MaxSlope.TWO_TO_FIVE,
                Plot.PROPERTY_BUFFER_STRIP, BufferStrip.BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW);
        
        List<Plot> result = plotTopiaDao.findAllFreePlotInDomain(domainBaulon.getTopiaId());
        Assertions.assertTrue(result.containsAll(expectedResult));
        
        Assertions.assertFalse(result.contains(oneFreePlot));
    }
    
    @Test
    void findAllRelatedPlots() {
        Domain domainBaulon = domainDao.forNameEquals("Baulon").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        
        Binder<Domain, Domain> domainBinder = BinderFactory.newBinder(Domain.class);

        Domain domainWithSameCode0 = new DomainImpl();
        domainBinder.copyExcluding(domainBaulon, domainWithSameCode0, Domain.PROPERTY_TOPIA_ID, Domain.PROPERTY_CAMPAIGN, Domain.PROPERTY_WEATHER_STATIONS);
        domainWithSameCode0.setCampaign(2014);
        domainWithSameCode0 = domainDao.create(domainWithSameCode0);
    
        Domain domainWithSameCode1 = new DomainImpl();
        domainBinder.copyExcluding(domainBaulon, domainWithSameCode1, Domain.PROPERTY_TOPIA_ID, Domain.PROPERTY_CAMPAIGN, Domain.PROPERTY_WEATHER_STATIONS);
        domainWithSameCode1.setCampaign(2015);
        domainWithSameCode1 = domainDao.create(domainWithSameCode1);
    
        LinkedHashMap<Integer, String> expectedResult = new LinkedHashMap<>();
        final String plotsCode = "SameCode_" + domainBaulon.getCode();
        final Plot plotDomainBaulon = plotTopiaDao.create(Plot.PROPERTY_DOMAIN, domainBaulon,
                Plot.PROPERTY_NAME, "freePlot0",
                Plot.PROPERTY_CODE, plotsCode,
                Plot.PROPERTY_WATER_FLOW_DISTANCE, WaterFlowDistance.LESS_THAN_THREE,
                Plot.PROPERTY_MAX_SLOPE, MaxSlope.TWO_TO_FIVE,
                Plot.PROPERTY_BUFFER_STRIP, BufferStrip.BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW);
        expectedResult.put(plotDomainBaulon.getDomain().getCampaign(), plotDomainBaulon.getTopiaId());
    
        final Plot plotDomainWithSameCode0 = plotTopiaDao.create(Plot.PROPERTY_DOMAIN, domainWithSameCode0,
                Plot.PROPERTY_NAME, "freePlot1",
                Plot.PROPERTY_CODE, plotsCode,
                Plot.PROPERTY_WATER_FLOW_DISTANCE, WaterFlowDistance.LESS_THAN_THREE,
                Plot.PROPERTY_MAX_SLOPE, MaxSlope.TWO_TO_FIVE,
                Plot.PROPERTY_BUFFER_STRIP, BufferStrip.BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW);
        expectedResult.put(plotDomainWithSameCode0.getDomain().getCampaign(), plotDomainWithSameCode0.getTopiaId());
    
        final Plot plotDomainWithSameCode1 = plotTopiaDao.create(Plot.PROPERTY_DOMAIN, domainWithSameCode1,
                Plot.PROPERTY_NAME, "freePlot1",
                Plot.PROPERTY_CODE, plotsCode,
                Plot.PROPERTY_WATER_FLOW_DISTANCE, WaterFlowDistance.LESS_THAN_THREE,
                Plot.PROPERTY_MAX_SLOPE, MaxSlope.TWO_TO_FIVE,
                Plot.PROPERTY_BUFFER_STRIP, BufferStrip.BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW);
        expectedResult.put(plotDomainWithSameCode1.getDomain().getCampaign(), plotDomainWithSameCode1.getTopiaId());

        Domain domainBouineliere = domainDao.forNameEquals("La Bouinelière").addEquals(Domain.PROPERTY_CAMPAIGN, 2014).findUnique();
        plotTopiaDao.create(Plot.PROPERTY_DOMAIN, domainBouineliere,
                Plot.PROPERTY_NAME, "freePlot2",
                Plot.PROPERTY_CODE, "SameCode_" + domainBouineliere.getCode(),
                Plot.PROPERTY_WATER_FLOW_DISTANCE, WaterFlowDistance.LESS_THAN_THREE,
                Plot.PROPERTY_MAX_SLOPE, MaxSlope.TWO_TO_FIVE,
                Plot.PROPERTY_BUFFER_STRIP, BufferStrip.BUFFER_STRIP_NOT_NEXT_TO_WATER_FLOW);

        LinkedHashMap<Integer, String> result = plotTopiaDao.findAllRelatedPlotIdsByCampaigns(plotsCode);
        Assertions.assertEquals(expectedResult, result);
    }
    
    @Test
    void getDomainPlotTotalArea() {
        Domain domainBaulon = domainDao.forNameEquals("Baulon").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        List<Plot> plots = plotTopiaDao.forDomainEquals(domainBaulon).addEquals(Plot.PROPERTY_ACTIVE, true).findAll();
        double expected = plots.stream().mapToDouble(Plot::getArea).sum();
        
        double result = plotTopiaDao.getDomainPlotTotalArea(domainBaulon.getTopiaId());
        Assertions.assertEquals(expected, result);
    }
    
    @Test
    void countAllGrowingSystemsForPlots() {
        Domain domainBaulon = domainDao.forNameEquals("Baulon").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        List<Plot> plots = plotTopiaDao.forDomainEquals(domainBaulon).addEquals(Plot.PROPERTY_ACTIVE, true).findAll();
    
        Domain domainBouineliere = domainDao.forNameEquals("La Bouinelière").addEquals(Domain.PROPERTY_CAMPAIGN, 2014).findUnique();
        plots.addAll(plotTopiaDao.forDomainEquals(domainBouineliere).addEquals(Plot.PROPERTY_ACTIVE, true).findAll());
        
        Set<GrowingSystem> growingSystems = plots.stream().map(Plot::getGrowingSystem).filter(Objects::nonNull).collect(Collectors.toSet());
    
        long result = plotTopiaDao.countAllGrowingSystemsForPlots(plots.stream().map(Plot::getTopiaId).collect(Collectors.toSet()));
        Assertions.assertEquals(growingSystems.size(), result);
        
    }
    
    @Test
    void findAllForFilter() {
        EdaplosPersisterMock edaplosPersister = new EdaplosPersisterMock();
        Domain domainBaulon = domainDao.forNameEquals("Baulon").addEquals(Domain.PROPERTY_CAMPAIGN, 2013).findUnique();
        List<Plot> plots = plotTopiaDao.forDomainEquals(domainBaulon).addEquals(Plot.PROPERTY_ACTIVE, true).findAll();
    
        final String origin = "Plot Baulon 1";
        
        String[] names = {origin, "PlotBaulon1", " plotbaulon1 ", "Plot Baulon 2"};
        Double[] areas = {31d, 31d, 32d, 31d};
        List<Plot> plotsToUpdate = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            Plot plot = plots.get(i);
            String name = names[i];
            plot.setName(name);
            plot.setArea(areas[i]);
            
            plotsToUpdate.add(plot);
        }
        plots = Lists.newArrayList(plotTopiaDao.updateAll(plotsToUpdate));
        
        Plot plot0 = plots.getFirst();
        PlotFilter filter = new PlotFilter(domainBaulon, 31d);
        
        List<Plot> result = plotTopiaDao.findAllForFilter(filter);
        result = result.stream().filter(plot2 -> edaplosPersister.isSameDespiteWhitespacesOrAccents(origin, plot2.getName())).collect(Collectors.toList());
        
        Assertions.assertTrue(result.containsAll(Lists.newArrayList(plot0, plots.get(1))));
        Assertions.assertEquals(2, result.size());
    
        filter.removeAreaFilter();
        result = plotTopiaDao.findAllForFilter(filter);
        result = result.stream().filter(plot2 -> edaplosPersister.isSameDespiteWhitespacesOrAccents(origin, plot2.getName())).collect(Collectors.toList());
    
        Assertions.assertTrue(result.containsAll(Lists.newArrayList(plot0, plots.get(1), plots.get(2))));
        Assertions.assertEquals(3, result.size());
    }
}
