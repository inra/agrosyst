package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;

import java.util.LinkedHashMap;
import java.util.Set;
import java.util.function.Function;

public class EntityUsageServiceMock extends EntityUsageService {

    public EntityUsageServiceMock(EntityUsageService entityUsageService) {
        domainDao = entityUsageService.domainDao;
        zoneDao = entityUsageService.zoneDao;
        croppingPlanEntryDao = entityUsageService.croppingPlanEntryDao;
        croppingPlanSpeciesDao = entityUsageService.croppingPlanSpeciesDao;
        practicedCropCycleNodeDao = entityUsageService.practicedCropCycleNodeDao;
        practicedSpeciesStadeDao = entityUsageService.practicedSpeciesStadeDao;
    }

    public ImmutableMap<String, Boolean> computeUsageMap(Set<String> allIds, LinkedHashMap<String, Function<Set<String>, Set<String>>> usageCheckFunctions) {
        return super.computeUsageMap(allIds, usageCheckFunctions);
    }
}
