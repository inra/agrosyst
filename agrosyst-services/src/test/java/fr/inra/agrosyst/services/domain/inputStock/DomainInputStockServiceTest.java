package fr.inra.agrosyst.services.domain.inputStock;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryImpl;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.FuelUnit;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainFuelInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.services.referential.MineralProductType;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.domain.DomainServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class DomainInputStockServiceTest extends AbstractAgrosystTest {

    protected DomainInputStockUnitService domainInputStockUnitService;
    protected DomainService domainService;

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected DomainTopiaDao domainDao;
    protected DomainSeedLotInputTopiaDao domainSeedLotInputDao;
    protected DomainSeedSpeciesInputTopiaDao domainSeedSpeciesInputDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitsDao;
    protected AbstractDomainInputStockUnitTopiaDao domainInputStockUnitDao;
    protected AbstractPhytoProductInputUsageTopiaDao abstractPhytoProductInputUsageDao;
    protected SeedProductInputUsageTopiaDao seedProductInputUsageDao;
    protected SeedSpeciesInputUsageTopiaDao seedSpeciesInputUsageDao;
    protected DomainPhytoProductInputTopiaDao domainPhytoProductInputTopiaDao;

    protected RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;

    @BeforeEach
    public void prepareTest() {

        testDatas = serviceFactory.newInstance(TestDatas.class);

        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        domainDao = getPersistenceContext().getDomainDao();
        domainSeedLotInputDao = getPersistenceContext().getDomainSeedLotInputDao();
        refActaTraitementsProduitsDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        domainInputStockUnitDao = getPersistenceContext().getAbstractDomainInputStockUnitDao();
        abstractPhytoProductInputUsageDao = getPersistenceContext().getAbstractPhytoProductInputUsageDao();
        domainPhytoProductInputTopiaDao = getPersistenceContext().getDomainPhytoProductInputDao();
        seedProductInputUsageDao = getPersistenceContext().getSeedProductInputUsageDao();
        domainSeedSpeciesInputDao = getPersistenceContext().getDomainSeedSpeciesInputDao();
        seedSpeciesInputUsageDao = getPersistenceContext().getSeedSpeciesInputUsageDao();

        refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();

        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        domainService = serviceFactory.newService(DomainService.class);

        loginAsAdmin();
        alterSchema();
    }

    @Test
    public void testBuilder() {

        DomainFuelInputDto domainInputDto = DomainFuelInputDto.builder()
                .inputType(InputType.CARBURANT)
                .key(DomainFuelInputDto.FINAL_KEY)
                .inputName("fr.inra.agrosyst.api.entities.tradeName.FUEL")
                .usageUnit(FuelUnit.L_HA)
                .topiaId("a_fuel_topia_id").build();

        Assertions.assertThat(domainInputDto.getInputType()).isEqualTo(InputType.CARBURANT);
        Assertions.assertThat(domainInputDto.getKey()).isEqualTo(DomainFuelInputDto.FINAL_KEY);
        Assertions.assertThat(domainInputDto.getInputName()).isEqualTo("fr.inra.agrosyst.api.entities.tradeName.FUEL");
        Assertions.assertThat(domainInputDto.getUsageUnit()).isEqualTo(FuelUnit.L_HA);
        Assertions.assertThat(domainInputDto.getTopiaId().get()).isEqualTo("a_fuel_topia_id");
        Assertions.assertThat(domainInputDto.getPrice()).isEqualTo(Optional.empty());
        Assertions.assertThat(domainInputDto.getTradeName()).isEqualTo(Optional.empty());
    }

    @Test
    public void testGsonDeserialize() {

        String json =
                "{ " +
                        "   \"usageUnit\":\"L_HA\"," +
                        "   \"inputType\":\"CARBURANT\"," +
                        "   \"key\":\"FUEL\"," +
                        "   \"inputName\":\"fr.inra.agrosyst.api.entities.tradeName.FUEL\"," +
                        "   \"topiaId\":\"a_fuel_topia_id\"" +
                        "}";
        final AgrosystGsonSupplier agrosystGsonSupplier = new AgrosystGsonSupplier();
        Gson gson = agrosystGsonSupplier.get();
        DomainFuelInputDto result = gson.fromJson(json, DomainFuelInputDto.class);
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.getTopiaId().get()).isEqualTo("a_fuel_topia_id");
    }

    @Test
    public void testGsonDeserializeWithNullParameters() {

        String json =
                "{ " +
                        "   \"usageUnit\":\"L_HA\"," +
                        "   \"inputType\":\"CARBURANT\"," +
                        "   \"key\":\"FUEL\"," +
                        "   \"inputName\":\"fr.inra.agrosyst.api.entities.tradeName.FUEL\"" +
                        "}";
        final AgrosystGsonSupplier agrosystGsonSupplier = new AgrosystGsonSupplier();
        Gson gson = agrosystGsonSupplier.get();
        DomainFuelInputDto result = gson.fromJson(json, DomainFuelInputDto.class);
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.getTopiaId().isPresent()).isEqualTo(false);
    }

    @Test
    public void tesGsonSerialiseDeserialize() {
        DomainFuelInputDto domainInputDto = DomainFuelInputDto.builder()
                .inputType(InputType.CARBURANT)
                .key(DomainFuelInputDto.FINAL_KEY)
                .inputName("fr.inra.agrosyst.api.entities.tradeName.FUEL")
                .usageUnit(FuelUnit.L_HA)
                .topiaId("a_fuel_topia_id").build();

        final AgrosystGsonSupplier agrosystGsonSupplier = new AgrosystGsonSupplier();
        Gson gson = agrosystGsonSupplier.get();
        String json = gson.toJson(domainInputDto);

        DomainFuelInputDto result = gson.fromJson(json, DomainFuelInputDto.class);
        Assertions.assertThat(result).isNotNull();
    }

    @Test
    public void testCreateSeedingLotWithoutSpeciesAndPhytoDomainInput() throws IOException {
        testDatas.createTestDomains();
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();

        Collection<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(baulon.getTopiaId());
        Optional<CroppingPlanEntryDto> aCropDto = cropDtos.stream().filter(cpe -> !cpe.getSpecies().isEmpty()).findAny();
        CroppingPlanEntryDto cpeDto = aCropDto.get();

        DomainSeedLotInputDto lotDto = getDomainSeedLotInputDto(cpeDto, new ArrayList<>(), true);

        Collection<DomainInputDto> domainInputDtos = Lists.newArrayList(lotDto);

        // create
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputDtos, pesristedCropResult);

        DomainSeedLotInput domainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(baulon).findUnique();
        Assertions.assertThat(domainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

        String lotEntityTopiaId = domainSeedLotInputEntity.getTopiaId();
        Assertions.assertThat(domainSeedLotInputEntity.getDomain()).isEqualTo(baulon);
        validateCommonDtoFields(lotDto, domainSeedLotInputEntity.getUsageUnit(), domainSeedLotInputEntity.getCropSeed().getTopiaId(), domainSeedLotInputEntity.isOrganic());

        Assertions.assertThat(domainSeedLotInputEntity.getDomainSeedSpeciesInput()).isNull();

        // validate dto From Entity
        Collection<DomainInputDto> lotToUpdates = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());

        Assertions.assertThat(lotToUpdates.size()).isEqualTo(1);
        Optional<DomainInputDto> inputDtoOptional = lotToUpdates.stream().findAny();
        DomainSeedLotInputDto domainSeedLotInputDto = (DomainSeedLotInputDto) inputDtoOptional.get();
        validateCommonDtoFields(lotDto, domainSeedLotInputDto.getUsageUnit(), domainSeedLotInputDto.getCropSeedDto().getTopiaId(), domainSeedLotInputDto.isOrganic());


        // update
        DomainSeedLotInputDto updatedDomainSeedLotInputDto = domainSeedLotInputDto.toBuilder().inputName("un autre lot pour culture bio").build();
        Map<String, CropPersistResult> pesristedCropResult_ = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, Lists.newArrayList(updatedDomainSeedLotInputDto), pesristedCropResult_);

        DomainSeedLotInput updatedSomainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(baulon).findUnique();
        Assertions.assertThat(updatedSomainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

        Assertions.assertThat(updatedSomainSeedLotInputEntity.getTopiaId()).isEqualTo(lotEntityTopiaId);
        validateCommonDtoFields(updatedDomainSeedLotInputDto, updatedSomainSeedLotInputEntity.getUsageUnit(), updatedSomainSeedLotInputEntity.getCropSeed().getTopiaId(), updatedSomainSeedLotInputEntity.isOrganic());

    }

    private void validateCommonDtoFields(
            DomainSeedLotInputDto lotDto,
            SeedPlantUnit domainSeedLotInputDto,
            String domainSeedLotInputDto1,
            boolean domainSeedLotInputDto5) {

        Assertions.assertThat(domainSeedLotInputDto).isEqualTo(lotDto.getUsageUnit());
        Assertions.assertThat(domainSeedLotInputDto1).isEqualTo(lotDto.getCropSeedDto().getTopiaId());
        Assertions.assertThat(domainSeedLotInputDto5).isEqualTo(lotDto.isOrganic());
    }

    @Test
    public void testCreateSeedingLotWithSpeciesDomainInput() throws IOException {
        testDatas.createTestDomains();
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = baulon.getLocation().getRefCountry();

        Collection<DomainInputDto> domainInputDtos = new ArrayList<>();

        Collection<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(baulon.getTopiaId());
        Optional<CroppingPlanEntryDto> aCropDto = cropDtos.stream().filter(dto -> dto.getSpecies().size() > 1).findAny();
        CroppingPlanEntryDto cpeDto = aCropDto.get();
        CroppingPlanEntry crop = croppingPlanEntryDao.forTopiaIdEquals(cpeDto.getTopiaId()).findUnique();

        InputPriceDto priceDto = InputPriceDto.builder()
                .category(InputPriceCategory.SEEDING_INPUT)
                .objectId("FAKE")
                .displayName("FAKE")
                .sourceUnit(SeedPlantUnit.KG_PAR_HA.name())
                .price(0.4)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .seedType(SeedType.SEMENCES_DE_FERME)
                .biologicalSeedInoculation(false)
                .chemicalTreatment(false)
                .includedTreatment(false)
                .organic(true).build();

        Collection<CroppingPlanSpeciesDto> speciesDtos = cpeDto.getSpecies();
        ArrayList<DomainSeedSpeciesInputDto> speciesInputs = getDomainSeedSpeciesInputDtos(crop, cpeDto, priceDto, france, false);

        DomainSeedLotInputDto lotDto = getDomainSeedLotInputDto(cpeDto, speciesInputs, false);

        domainInputDtos.add(lotDto);

        final List<CroppingPlanEntry> croppingPlanEntriesForDomain = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputDtos, pesristedCropResult);
        getPersistenceContext().commit();

        DomainSeedLotInput domainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(baulon).findUnique();

        Assertions.assertThat(domainSeedLotInputEntity.getDomain()).isEqualTo(baulon);
        validateCommonDtoFields(lotDto, domainSeedLotInputEntity.getUsageUnit(), domainSeedLotInputEntity.getCropSeed().getTopiaId(), domainSeedLotInputEntity.isOrganic());

        Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = domainSeedLotInputEntity.getDomainSeedSpeciesInput();
        Assertions.assertThat(domainSeedLotInputEntity.sizeDomainSeedSpeciesInput()).isEqualTo(speciesDtos.size());

        for (DomainSeedSpeciesInput domainSeedSpeciesInput : domainSeedSpeciesInputs) {
            SeedPrice seedPrice = domainSeedSpeciesInput.getSeedPrice();
            Assertions.assertThat(seedPrice.getPrice()).isEqualTo(priceDto.getPrice());
        }

        // remove ALL
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, new ArrayList<>(), pesristedCropResult);
        getPersistenceContext().commit();

        Assertions.assertThat(domainSeedLotInputDao.forDomainEquals(baulon).exists()).isFalse();
        Map<InputType, List<AbstractDomainInputStockUnit>> inputTypeListMap = domainInputStockUnitService.loadDomainInputStock(baulon);
        Assertions.assertThat(inputTypeListMap.values().isEmpty()).isTrue();

    }

    @Test
    public void testCreateSeedingLotWithSpeciesDomainInput0() throws IOException {
        testDatas.createTestDomains();
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = baulon.getLocation().getRefCountry();

        Collection<DomainInputDto> domainInputDtos = new ArrayList<>();

        Collection<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(baulon.getTopiaId());
        Optional<CroppingPlanEntryDto> aCropDto = cropDtos.stream().filter(dto -> dto.getSpecies().size() > 1).findAny();
        CroppingPlanEntryDto cpeDto = aCropDto.get();
        CroppingPlanEntry crop = croppingPlanEntryDao.forTopiaIdEquals(cpeDto.getTopiaId()).findUnique();

        InputPriceDto priceDto = InputPriceDto.builder()
                .category(InputPriceCategory.SEEDING_INPUT)
                .objectId("FAKE")
                .displayName("FAKE")
                .sourceUnit(SeedPlantUnit.KG_PAR_HA.name())
                .price(0.4)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .seedType(SeedType.SEMENCES_DE_FERME)
                .biologicalSeedInoculation(false)
                .chemicalTreatment(false)
                .includedTreatment(false)
                .organic(true).build();

        ArrayList<DomainSeedSpeciesInputDto> domainSpeciesInputDtos = getDomainSeedSpeciesInputDtos(crop, cpeDto, null, france, true);
        ArrayList<DomainSeedSpeciesInputDto> domainSpeciesInputWithPricesDtos = getDomainSeedSpeciesInputDtos(crop, cpeDto, priceDto, france, true);
        Map<String, DomainSeedSpeciesInputDto> domainSpeciesInputWithPricesDtoBySpeciesIds = domainSpeciesInputWithPricesDtos.stream()
                .collect(Collectors.toMap((sidto -> sidto.getSpeciesSeedDto().getSpeciesId() + "_" + sidto.getSpeciesSeedDto().getVarietyId()), Function.identity()));

        DomainSeedLotInputDto lotDto = getDomainSeedLotInputDto(cpeDto, Lists.newArrayList(domainSpeciesInputDtos.get(0)), true);

        domainInputDtos.add(lotDto);

        final List<CroppingPlanEntry> croppingPlanEntriesForDomain = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputDtos, pesristedCropResult);
        getPersistenceContext().commit();

        DomainSeedLotInput domainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(baulon).findUnique();

        Assertions.assertThat(domainSeedLotInputEntity.getDomain()).isEqualTo(baulon);
        validateCommonDtoFields(lotDto, domainSeedLotInputEntity.getUsageUnit(), domainSeedLotInputEntity.getCropSeed().getTopiaId(), domainSeedLotInputEntity.isOrganic());

        Assertions.assertThat(domainSeedLotInputEntity.sizeDomainSeedSpeciesInput()).isEqualTo(1);

        Assertions.assertThat(domainSeedLotInputEntity.getSeedPrice()).isNotNull();

        List<DomainInputDto> persistedDomainInputDtos = new ArrayList<>(domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId()));
        DomainSeedLotInputDto persistedDomainInputDto = (DomainSeedLotInputDto) persistedDomainInputDtos.stream().filter(di -> di instanceof DomainSeedLotInputDto).findAny().get();

        // add 1 species + set prices to species
        List<DomainSeedSpeciesInputDto> twoSpeciesInputs0 = new ArrayList<>(persistedDomainInputDto.getSpeciesInputs());
        twoSpeciesInputs0.add(domainSpeciesInputDtos.get(1));

        List<DomainSeedSpeciesInputDto> twoSpeciesInputs = new ArrayList<>();
        for (DomainSeedSpeciesInputDto twoSpeciesInput : twoSpeciesInputs0) {
            String speciesId = twoSpeciesInput.getSpeciesSeedDto().getSpeciesId() + "_" + twoSpeciesInput.getSpeciesSeedDto().getVarietyId();
            Optional<InputPriceDto> seedPrice = domainSpeciesInputWithPricesDtoBySpeciesIds.get(speciesId).getSeedPrice();
            twoSpeciesInputs.add(twoSpeciesInput.toBuilder().seedPrice(seedPrice.get()).build());
        }

        DomainSeedLotInputDto domainSeedLotInputDtoWithTwoSpecies = persistedDomainInputDto.toBuilder().speciesInputs(twoSpeciesInputs).build();
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, Lists.newArrayList(domainSeedLotInputDtoWithTwoSpecies), pesristedCropResult);
        getPersistenceContext().commit();

        domainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(baulon).findUnique();

        Assertions.assertThat(domainSeedLotInputEntity.getDomain()).isEqualTo(baulon);
        validateCommonDtoFields(lotDto, domainSeedLotInputEntity.getUsageUnit(), domainSeedLotInputEntity.getCropSeed().getTopiaId(), domainSeedLotInputEntity.isOrganic());

        Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = domainSeedLotInputEntity.getDomainSeedSpeciesInput();
        Assertions.assertThat(domainSeedLotInputEntity.sizeDomainSeedSpeciesInput()).isEqualTo(2);

        for (DomainSeedSpeciesInput domainSeedSpeciesInput : domainSeedSpeciesInputs) {
            SeedPrice seedPrice = domainSeedSpeciesInput.getSeedPrice();
            Assertions.assertThat(seedPrice.getPrice()).isEqualTo(priceDto.getPrice());
        }
        Assertions.assertThat(domainSeedLotInputEntity.getSeedPrice()).isNull();

        // remove ALL
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, new ArrayList<>(), pesristedCropResult);
        getPersistenceContext().commit();

        Assertions.assertThat(domainSeedLotInputDao.forDomainEquals(baulon).exists()).isFalse();
        Map<InputType, List<AbstractDomainInputStockUnit>> inputTypeListMap = domainInputStockUnitService.loadDomainInputStock(baulon);
        Assertions.assertThat(inputTypeListMap.values().isEmpty()).isTrue();

    }

    public static DomainSeedLotInputDto getDomainSeedLotInputDto(
            CroppingPlanEntryDto cpeDto,
            Collection<DomainSeedSpeciesInputDto> speciesInputs,
            boolean organic) {

        CroppingPlanEntry fakeCpe = new CroppingPlanEntryImpl();
        fakeCpe.setCode(cpeDto.getCode());

        boolean priceBySpecies = CollectionUtils.emptyIfNull(speciesInputs)
                .stream()
                .filter(dss -> dss.getSeedPrice().isPresent())
                .map(p -> p.getSeedPrice().get()).anyMatch(p -> p.getPrice() != null);

        // if one species is declare with treatment it can't be organic
        organic = organic && CollectionUtils.emptyIfNull(speciesInputs)
                .stream()
                .anyMatch(dss -> dss.isBiologicalSeedInoculation() || dss.isChemicalTreatment());

        InputPriceDto priceDto = null;
        if (!priceBySpecies) {
            priceDto = InputPriceDto.builder()
                    .category(InputPriceCategory.SEEDING_INPUT)
                    .objectId(cpeDto.getTopiaId() + false)
                    .displayName("FAKE")
                    .sourceUnit(SeedPlantUnit.KG_PAR_HA.name())
                    .price(0.4)
                    .priceUnit(PriceUnit.EURO_HA)
                    .phytoProductType(null)
                    .seedType(SeedType.SEMENCES_DE_FERME)
                    .biologicalSeedInoculation(false)
                    .chemicalTreatment(false)
                    .includedTreatment(false)
                    .organic(organic).build();
        }

        DomainSeedLotInputDto lotDto = DomainSeedLotInputDto.builder()
                .cropSeedDto(cpeDto)
                .organic(organic)
                .inputType(InputType.SEMIS)
                .inputName("un lot pour culture bio")
                .key(DomainInputStockUnitService.getLotCropSeedInputKey(fakeCpe, organic, SeedPlantUnit.KG_PAR_HA))
                .speciesInputs(ImmutableList.copyOf(speciesInputs))
                .usageUnit(SeedPlantUnit.KG_PAR_HA)
                .price(priceDto)
                .build();
        return lotDto;
    }

    @Test
    public void testPersistMineralInput() throws IOException {
        testDatas.createTestDomains();
        testDatas.createRefFertiMinUnifaTestDataRefs9695();
        RefFertiMinUNIFA rfmu = refFertiMinUNIFADao.forTopiaIdEquals("315_Pulverulent_n:3_k2o:7_calcium:21_type_produit:AUTRES").findUnique();

        InputPriceDto priceDto = InputPriceDto.builder()
                .objectId(InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(rfmu))
                .price(12.3)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .sourceUnit(MineralProductUnit.KG_HA.name())
                .displayName("315_Pulverulent")
                .category(InputPriceCategory.MINERAL_INPUT)
                .build();

        MineralProductType mpt = new MineralProductType(rfmu.getCateg(), Pair.of(rfmu.getType_produit(), rfmu.getType_produit()), Pair.of(rfmu.getForme(), rfmu.getForme()));

        DomainMineralProductInputDto input = DomainMineralProductInputDto.builder()
                .inputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)
                .refInputId(rfmu.getTopiaId())
                .forme(rfmu.getForme())
                .usageUnit(MineralProductUnit.KG_HA)
                .phytoEffect(true)
                .inputName("315_Pulverulent")
                .tradeName(mpt.getLabel())
                .tradeNameTranslated(mpt.getTranslatedLabel())
                .categ(rfmu.getCateg())
                .forme(rfmu.getForme())
                .n(rfmu.getN())
                .p2o5(rfmu.getP2O5())
                .k2o(rfmu.getK2O())
                .bore(rfmu.getBore())
                .calcium(rfmu.getCalcium())
                .fer(rfmu.getFer())
                .manganese(rfmu.getManganese())
                .molybdene(rfmu.getMolybdene())
                .mgo(rfmu.getMgO())
                .oxyde_de_sodium(rfmu.getOxyde_de_sodium())
                .so3(rfmu.getsO3())
                .cuivre(rfmu.getCuivre())
                .zinc(rfmu.getZinc())
                .key(DomainInputStockUnitService.getMineralInputKey(rfmu, true, MineralProductUnit.KG_HA))
                .type_produit(rfmu.getType_produit())
                .price(priceDto)
                .build();

        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain0 = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain0);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, Lists.newArrayList(input), pesristedCropResult);

        Collection<DomainInputDto> inputs = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());

        Assertions.assertThat(inputs).isUnmodifiable();
        Assertions.assertThat(inputs).isNotNull();
        Assertions.assertThat(inputs.size()).isEqualTo(1);

        for (DomainInputDto domainInputDto : inputs) {
            Assertions.assertThat((DomainMineralProductInputDto) domainInputDto).isEqualTo(input);
            Assertions.assertThat(domainInputDto.getPrice().isPresent()).isTrue();
            Assertions.assertThat(domainInputDto.getPrice().get()).isEqualTo(priceDto);
        }

        DomainInputDto domainInputDtoToUpdate = inputs.stream().findAny().get().toBuilder().price(null).build();
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain1 = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult1 = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain1);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, Lists.newArrayList(domainInputDtoToUpdate), pesristedCropResult1);

        inputs = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());

        Assertions.assertThat(inputs).isUnmodifiable();
        Assertions.assertThat(inputs).isNotNull();
        Assertions.assertThat(inputs.size()).isEqualTo(1);

        for (DomainInputDto domainInputDto : inputs) {
            Assertions.assertThat((DomainMineralProductInputDto) domainInputDto).isEqualTo(input);
            Assertions.assertThat(domainInputDto.getPrice().isPresent()).isFalse();
        }
    }

    @Test
    public void testPersistNewMineralInput() throws IOException {
        testDatas.createTestDomains();

        DomainMineralProductInputDto inputDto = DomainMineralProductInputDto.builder()
                .inputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)
                .usageUnit(MineralProductUnit.KG_HA)
                .forme("Pulvérulent")
                .formeTranslated("Pulvérulent - i18N")
                .inputName("315_Pulverulent")
                .categ(315)
                .n(3d)
                .k2o(7d)
                .calcium(21d)
                .tradeName("un Pulvérulent")
                .tradeNameTranslated("un Pulvérulent - i18N")
                .type_produit("Autre")
                .type_produit_Translated("Autre - i18N")
                .phytoEffect(true)
                .build();

        InputPriceDto priceDto = InputPriceDto.builder()
                .objectId(InputPriceService.GET_REF_FERTI_MIN_UNIFA_DTO_OBJECT_ID.apply(inputDto))
                .price(12.3)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .sourceUnit(MineralProductUnit.KG_HA.name())
                .displayName("315_Pulverulent")
                .category(InputPriceCategory.MINERAL_INPUT)
                .build();

        inputDto = inputDto
                .toBuilder()
                .price(priceDto)
                .build();

        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain0 = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain0);

        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, Lists.newArrayList(inputDto), pesristedCropResult);

        Collection<DomainInputDto> inputs = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());

        Assertions.assertThat(inputs).isUnmodifiable();
        Assertions.assertThat(inputs).isNotNull();
        Assertions.assertThat(inputs.size()).isEqualTo(1);

        for (DomainInputDto domainInputDto : inputs) {
            DomainMineralProductInputDto asGivenOne = ((DomainMineralProductInputDto) domainInputDto).toBuilder()
                    .topiaId(null)
                    .refInputId(null)
                    .build();
            Assertions.assertThat(asGivenOne).isEqualTo(inputDto);
            Assertions.assertThat(domainInputDto.getPrice().isPresent()).isTrue();
            Assertions.assertThat(domainInputDto.getPrice().get()).isEqualTo(priceDto);
        }

        DomainInputDto domainInputDtoToUpdate = inputs.stream().findAny().get().toBuilder().price(null).build();
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain1 = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult1 = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain1);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, Lists.newArrayList(domainInputDtoToUpdate), pesristedCropResult1);

        inputs = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());

        Assertions.assertThat(inputs).isUnmodifiable();
        Assertions.assertThat(inputs).isNotNull();
        Assertions.assertThat(inputs.size()).isEqualTo(1);

        String objectId = "categ=%d forme=%s n=%f p2O5=%f k2O=%f bore=%f calcium=%f fer=%f manganese=%f molybdene=%f mgO=%f oxyde_de_sodium=%f sO3=%f cuivre=%f zinc=%f";
        objectId = String.format(
                objectId,
                315,
                "Pulvérulent",
                3d,
                0d,
                7d,
                0d,
                21d,
                0d,
                0d,
                0d,
                0d,
                0d,
                0d,
                0d,
                0d);
        objectId = objectId.replace(',', '.');
        objectId = StringUtils.stripAccents(objectId);

        String key = objectId + DomainInputBinder.KEY_SEPARATOR + true + DomainInputBinder.KEY_SEPARATOR + MineralProductUnit.KG_HA;

        for (DomainInputDto domainInputDto : inputs) {
            //Assertions.assertThat(domainInputDto)
            final DomainMineralProductInputDto domainMineralProductInputDto = (DomainMineralProductInputDto) domainInputDto;
            Assertions.assertThat(domainMineralProductInputDto.getKey()).isEqualTo(key);
            Assertions.assertThat(DomainInputStockUnitService.getMineralInputDtoKey(domainMineralProductInputDto, ((DomainMineralProductInputDto) domainInputDto).getUsageUnit())).isEqualTo(key);
            Assertions.assertThat(domainMineralProductInputDto.getRefInputId()).isNotEmpty();
            Assertions.assertThat(domainMineralProductInputDto).isEqualTo(inputDto);
            Assertions.assertThat(domainInputDto.getPrice().isPresent()).isFalse();
        }
    }

    @Test
    public void testOrganicKey() {
        final String idTypeeffluent = DomainInputStockUnitService.getOrganicProductInputKey("idTypeeffluent", OrganicProductUnit.KG_HA, 1d, 2d, 3d, 4d, 5d, null, true);
        String expected = "Idtypeeffluent=idTypeeffluent;usageUnit=KG_HA;n=1.000000;p2O5=2.000000;k2O=3.000000;caO=4.000000;mgO=5.000000;s=null;organic=true";
        Assertions.assertThat(idTypeeffluent).isEqualTo(expected);
    }

    public void validateDomainInputDto(DomainInputDto toDomainInputDto, DomainInputDto fromDomainInputDto) {
        Assertions.assertThat(toDomainInputDto.getInputType()).isEqualTo(fromDomainInputDto.getInputType());
        Assertions.assertThat(toDomainInputDto.getInputName()).isEqualTo(fromDomainInputDto.getInputName());
        Assertions.assertThat(toDomainInputDto.getKey()).isEqualTo(fromDomainInputDto.getKey());
        Assertions.assertThat(toDomainInputDto.getPrice()).isEqualTo(fromDomainInputDto.getPrice());

    }

    @Test
    public void testCopySeedingLot() throws IOException, DomainExtendException {
        testDatas.createTestDomains();
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = baulon.getLocation().getRefCountry();

        Collection<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(baulon.getTopiaId());
        Optional<CroppingPlanEntryDto> aCropDto = cropDtos.stream().filter(dto -> dto.getSpecies().size() > 1).findAny();
        CroppingPlanEntryDto cpeDto = aCropDto.get();
        CroppingPlanEntry crop = croppingPlanEntryDao.forTopiaIdEquals(cpeDto.getTopiaId()).findUnique();

        InputPriceDto priceDto = InputPriceDto.builder()
                .category(InputPriceCategory.SEEDING_INPUT)
                .objectId("FAKE")
                .displayName("FAKE")
                .sourceUnit(SeedPlantUnit.KG_PAR_HA.name())
                .price(0.4)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .seedType(SeedType.SEMENCES_DE_FERME)
                .biologicalSeedInoculation(false)
                .chemicalTreatment(false)
                .includedTreatment(false)
                .organic(false)
                .build();

        ArrayList<DomainSeedSpeciesInputDto> speciesInputs = getDomainSeedSpeciesInputDtos(crop, cpeDto, priceDto, france, false);

        DomainSeedLotInputDto lotDto = getDomainSeedLotInputDto(cpeDto, speciesInputs, false);

        Collection<DomainInputDto> domainInputDtos = Lists.newArrayList(lotDto);

        // create
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> persistedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, domainInputDtos, persistedCropResult);

        DomainSeedLotInput domainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(baulon).findUnique();
        Assertions.assertThat(domainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

        final Collection<DomainInputDto> createdBaulonInputs = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());
        Assertions.assertThat(createdBaulonInputs.size()).isEqualTo(domainInputDtos.size());
        Map<String, DomainInputDto> toInputDtoByCodes = new HashMap<>();
        createdBaulonInputs.forEach(i -> {
                    DomainSeedLotInputDto dto = (DomainSeedLotInputDto) i;
                    toInputDtoByCodes.put(dto.getCropSeedDto().getCode(), i);
                }
        );
        for (DomainInputDto fromInput : domainInputDtos) {
            DomainSeedLotInputDto fromLotdto = (DomainSeedLotInputDto) fromInput;
            DomainSeedLotInputDto toLotDto = (DomainSeedLotInputDto) toInputDtoByCodes.get(fromLotdto.getCropSeedDto().getCode());
            Assertions.assertThat(toLotDto).isNotNull();
            Assertions.assertThat(toLotDto.getUsageUnit()).isEqualTo(fromLotdto.getUsageUnit());
            Assertions.assertThat(toLotDto.getInputName()).isEqualTo(fromLotdto.getInputName());
            Assertions.assertThat(toLotDto.getCropSeedDto().getCode()).isEqualTo(fromLotdto.getCropSeedDto().getCode());

            Assertions.assertThat(toLotDto.getSpeciesInputs().size()).isEqualTo(fromLotdto.getSpeciesInputs().size());
            Assertions.assertThat(toLotDto.isOrganic()).isEqualTo(fromLotdto.isOrganic());

            validateDomainInputDto(toLotDto, fromLotdto);

            Map<String, DomainSeedSpeciesInputDto> toSpeciesInputBySpeciesCodes = toLotDto.getSpeciesInputs().stream().collect(
                    Collectors.toMap(dssi -> dssi.getSpeciesSeedDto().getCode(), Function.identity()));

            for (DomainSeedSpeciesInputDto fromSpeciesInput : fromLotdto.getSpeciesInputs()) {
                DomainSeedSpeciesInputDto toSpeciesInput = toSpeciesInputBySpeciesCodes.get(fromSpeciesInput.getSpeciesSeedDto().getCode());
                Assertions.assertThat(toSpeciesInput).isNotNull();
                Assertions.assertThat(toSpeciesInput.getSeedType()).isEqualTo(fromSpeciesInput.getSeedType());
                Assertions.assertThat(toSpeciesInput.isBiologicalSeedInoculation()).isEqualTo(fromSpeciesInput.isBiologicalSeedInoculation());
                Assertions.assertThat(toSpeciesInput.isChemicalTreatment()).isEqualTo(fromSpeciesInput.isChemicalTreatment());
                Assertions.assertThat(toSpeciesInput.isOrganic()).isEqualTo(fromSpeciesInput.isOrganic());

                validateDomainInputDto(toSpeciesInput, fromSpeciesInput);

                Optional<InputPriceDto> fromOptSeedPrice = fromSpeciesInput.getSeedPrice();
                Optional<InputPriceDto> toOptSeedPrice = toSpeciesInput.getSeedPrice();
                Assertions.assertThat(toOptSeedPrice.isPresent()).isEqualTo(fromOptSeedPrice.isPresent());
                if (fromOptSeedPrice.isPresent()) {
                    InputPriceDto fromSeedPriceDto = fromOptSeedPrice.get();
                    InputPriceDto toSeedPriceDto = toOptSeedPrice.get();
                    Assertions.assertThat(toSeedPriceDto.getSeedType()).isEqualTo(fromSeedPriceDto.getSeedType());
                    Assertions.assertThat(toSeedPriceDto.getObjectId()).isEqualTo(fromSeedPriceDto.getObjectId());
                    Assertions.assertThat(toSeedPriceDto.getCategory()).isEqualTo(fromSeedPriceDto.getCategory());
                    Assertions.assertThat(toSeedPriceDto.getSourceUnit()).isEqualTo(fromSeedPriceDto.getSourceUnit());
                    Assertions.assertThat(toSeedPriceDto.getPhytoProductType()).isEqualTo(fromSeedPriceDto.getPhytoProductType());
                    Assertions.assertThat(toSeedPriceDto.getPrice()).isEqualTo(fromSeedPriceDto.getPrice());
                    Assertions.assertThat(toSeedPriceDto.getPriceUnit()).isEqualTo(fromSeedPriceDto.getPriceUnit());
                    Assertions.assertThat(toSeedPriceDto.isBiologicalSeedInoculation()).isEqualTo(fromSeedPriceDto.isBiologicalSeedInoculation());
                    Assertions.assertThat(toSeedPriceDto.isChemicalTreatment()).isEqualTo(fromSeedPriceDto.isChemicalTreatment());
                    Assertions.assertThat(toSeedPriceDto.isIncludedTreatment()).isEqualTo(fromSeedPriceDto.isIncludedTreatment());

                }

                Collection<DomainPhytoProductInputDto> fromSpeciesPhytoInputDtos = CollectionUtils.emptyIfNull(fromSpeciesInput.getSpeciesPhytoInputDtos());
                Collection<DomainPhytoProductInputDto> toSpeciesPhytoInputDtos = CollectionUtils.emptyIfNull(toSpeciesInput.getSpeciesPhytoInputDtos());
                Assertions.assertThat(fromSpeciesPhytoInputDtos).size().isEqualTo(toSpeciesPhytoInputDtos.size());

                Map<String, DomainPhytoProductInputDto> toSpeciesPhytoInputDtosByRefInputIds = toSpeciesPhytoInputDtos.stream().collect(
                        Collectors.toMap(DomainPhytoProductInputDto::getRefInputId, Function.identity()));

                for (DomainPhytoProductInputDto fromSpeciesPhytoInputDto : fromSpeciesPhytoInputDtos) {
                    DomainPhytoProductInputDto toSpeciesPhytoInputDto = toSpeciesPhytoInputDtosByRefInputIds.get(fromSpeciesPhytoInputDto.getRefInputId());
                    Assertions.assertThat(toSpeciesPhytoInputDto).isNotNull();
                    Assertions.assertThat(toSpeciesPhytoInputDto.getUsageUnit()).isEqualTo(fromSpeciesPhytoInputDto.getUsageUnit());
                    Assertions.assertThat(toSpeciesPhytoInputDto.getAmmCode()).isEqualTo(fromSpeciesPhytoInputDto.getAmmCode());
                    Assertions.assertThat(toSpeciesPhytoInputDto.getProductType()).isEqualTo(fromSpeciesPhytoInputDto.getProductType());

                    validateDomainInputDto(toSpeciesPhytoInputDto, fromSpeciesPhytoInputDto);
                }

                Assertions.assertThat(toSpeciesInput.getSeedType()).isEqualTo(fromSpeciesInput.getSeedType());
            }

        }

        String lotEntityTopiaId = domainSeedLotInputEntity.getTopiaId();

        Domain domainLaBouineliere = domainDao.forTopiaIdEquals("fr.inra.agrosyst.api.entities.Domain_7fd194f6-7440-432f-a37b-fbf1d27fa95e").findUnique();

        domainInputStockUnitService.copyInputStocks(baulon.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainLaBouineliere.getTopiaId()), false);
        // there are no crop on domainLaBouineliere so copie can't be donne
        Assertions.assertThat(domainSeedLotInputDao.forDomainEquals(domainLaBouineliere).exists()).isFalse();

        validNonDuplicatedCode(domainLaBouineliere);

        // add similar crop to domainLaBouineliere
        createNewCropToGivenDomain(croppingPlanEntriesForDomain, domainLaBouineliere, false);
        final Domain extendDomainLaBouineliere = domainService.extendDomain(domainLaBouineliere.getTopiaId(), domainLaBouineliere.getCampaign() + 1);

        domainInputStockUnitService.copyInputStocks(
                baulon.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainLaBouineliere.getTopiaId()), false);
        validNonDuplicatedCode(domainLaBouineliere);

        DomainSeedLotInput copiedDomainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(domainLaBouineliere).findUnique();
        Assertions.assertThat(copiedDomainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

        // update, there should not have more has forceCopy is false and inputs already exists
        domainInputStockUnitService.copyInputStocks(baulon.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainLaBouineliere.getTopiaId()), false);
        copiedDomainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(domainLaBouineliere).findUnique();
        Assertions.assertThat(copiedDomainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));
        validNonDuplicatedCode(domainLaBouineliere);

        // update, there should be 1 more has forceCopy is true and inputs are added
        domainInputStockUnitService.copyInputStocks(baulon.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainLaBouineliere.getTopiaId()), true);
        Assertions.assertThat(domainSeedLotInputDao.forDomainEquals(domainLaBouineliere).count()).isEqualTo(2);
        validNonDuplicatedCode(domainLaBouineliere);

        // copie inputs to duplicated inputs
        domainInputStockUnitService.copyInputStocks(
                baulon.getTopiaId(),
                Lists.newArrayList(lotEntityTopiaId),
                Lists.newArrayList(extendDomainLaBouineliere.getTopiaId()),
                true);

        validNonDuplicatedCode(domainLaBouineliere);

        copiedDomainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(extendDomainLaBouineliere).findUnique();
        Assertions.assertThat(copiedDomainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

        final Collection<DomainInputDto> duplicatedBaulonInputs = domainInputStockUnitService.loadDomainInputStockDtos(extendDomainLaBouineliere.getTopiaId());
        Assertions.assertThat(duplicatedBaulonInputs.size()).isEqualTo(domainInputDtos.size());
        duplicatedBaulonInputs.forEach(i -> {
                    DomainSeedLotInputDto dto = (DomainSeedLotInputDto) i;
                    toInputDtoByCodes.put(dto.getCropSeedDto().getCode(), i);
                }
        );

        for (DomainInputDto fromInput : domainInputDtos) {
            DomainSeedLotInputDto dto = (DomainSeedLotInputDto) fromInput;
            DomainInputDto toInputDto = toInputDtoByCodes.get(dto.getCropSeedDto().getCode());
            Assertions.assertThat(toInputDto).isNotNull();
        }

    }

    @Test
    public void testCopySeedingLotOnSameDomaineCode() throws IOException, DomainExtendException {
        testDatas.createTestDomains();
        Domain domainBaulon_2013 = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        RefCountry france = domainBaulon_2013.getLocation().getRefCountry();

        Collection<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domainBaulon_2013.getTopiaId());
        Optional<CroppingPlanEntryDto> aCropDto = cropDtos.stream().filter(dto -> dto.getSpecies().size() > 1).findAny();
        CroppingPlanEntryDto cpeDto = aCropDto.get();
        CroppingPlanEntry crop = croppingPlanEntryDao.forTopiaIdEquals(cpeDto.getTopiaId()).findUnique();

        InputPriceDto priceDto = InputPriceDto.builder()
                .category(InputPriceCategory.SEEDING_INPUT)
                .objectId("FAKE")
                .displayName("FAKE")
                .sourceUnit(SeedPlantUnit.KG_PAR_HA.name())
                .price(0.4)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .seedType(SeedType.SEMENCES_DE_FERME)
                .biologicalSeedInoculation(false)
                .chemicalTreatment(false)
                .includedTreatment(false)
                .organic(false)
                .build();

        ArrayList<DomainSeedSpeciesInputDto> speciesInputs = getDomainSeedSpeciesInputDtos(crop, cpeDto, priceDto, france, false);

        DomainSeedLotInputDto lotDto = getDomainSeedLotInputDto(cpeDto, speciesInputs, false);

        Collection<DomainInputDto> domainInputDtos = Lists.newArrayList(lotDto);

        // create
        final List<CroppingPlanEntry> croppingPlanEntriesForDomain = domainService.getCroppingPlanEntriesForDomain(domainBaulon_2013);
        Map<String, CropPersistResult> persistedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain);
        domainInputStockUnitService.createOrUpdateDomainInputStock(domainBaulon_2013, domainInputDtos, persistedCropResult);

        DomainSeedLotInput domainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(domainBaulon_2013).findUnique();
        Assertions.assertThat(domainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

        final Collection<DomainInputDto> createdBaulonInputs = domainInputStockUnitService.loadDomainInputStockDtos(domainBaulon_2013.getTopiaId());
        Assertions.assertThat(createdBaulonInputs.size()).isEqualTo(domainInputDtos.size());
        Map<String, DomainInputDto> toInputDtoByCodes = new HashMap<>();
        createdBaulonInputs.forEach(i -> {
                    DomainSeedLotInputDto dto = (DomainSeedLotInputDto) i;
                    toInputDtoByCodes.put(dto.getCropSeedDto().getCode(), i);
                }
        );
        for (DomainInputDto fromInput : domainInputDtos) {
            DomainSeedLotInputDto fromLotdto = (DomainSeedLotInputDto) fromInput;
            DomainSeedLotInputDto toLotDto = (DomainSeedLotInputDto) toInputDtoByCodes.get(fromLotdto.getCropSeedDto().getCode());
            Assertions.assertThat(toLotDto).isNotNull();
            Assertions.assertThat(toLotDto.getUsageUnit()).isEqualTo(fromLotdto.getUsageUnit());
            Assertions.assertThat(toLotDto.getInputName()).isEqualTo(fromLotdto.getInputName());
            Assertions.assertThat(toLotDto.getCropSeedDto().getCode()).isEqualTo(fromLotdto.getCropSeedDto().getCode());

            Assertions.assertThat(toLotDto.getSpeciesInputs().size()).isEqualTo(fromLotdto.getSpeciesInputs().size());
            Assertions.assertThat(toLotDto.isOrganic()).isEqualTo(fromLotdto.isOrganic());

            validateDomainInputDto(toLotDto, fromLotdto);

            Map<String, DomainSeedSpeciesInputDto> toSpeciesInputBySpeciesCodes = toLotDto.getSpeciesInputs().stream().collect(
                    Collectors.toMap(dssi -> dssi.getSpeciesSeedDto().getCode(), Function.identity()));

            for (DomainSeedSpeciesInputDto fromSpeciesInput : fromLotdto.getSpeciesInputs()) {
                DomainSeedSpeciesInputDto toSpeciesInput = toSpeciesInputBySpeciesCodes.get(fromSpeciesInput.getSpeciesSeedDto().getCode());
                Assertions.assertThat(toSpeciesInput).isNotNull();
                Assertions.assertThat(toSpeciesInput.getSeedType()).isEqualTo(fromSpeciesInput.getSeedType());
                Assertions.assertThat(toSpeciesInput.isBiologicalSeedInoculation()).isEqualTo(fromSpeciesInput.isBiologicalSeedInoculation());
                Assertions.assertThat(toSpeciesInput.isChemicalTreatment()).isEqualTo(fromSpeciesInput.isChemicalTreatment());
                Assertions.assertThat(toSpeciesInput.isOrganic()).isEqualTo(fromSpeciesInput.isOrganic());

                validateDomainInputDto(toSpeciesInput, fromSpeciesInput);

                Optional<InputPriceDto> fromOptSeedPrice = fromSpeciesInput.getSeedPrice();
                Optional<InputPriceDto> toOptSeedPrice = toSpeciesInput.getSeedPrice();
                Assertions.assertThat(toOptSeedPrice.isPresent()).isEqualTo(fromOptSeedPrice.isPresent());
                if (fromOptSeedPrice.isPresent()) {
                    InputPriceDto fromSeedPriceDto = fromOptSeedPrice.get();
                    InputPriceDto toSeedPriceDto = toOptSeedPrice.get();
                    Assertions.assertThat(toSeedPriceDto.getSeedType()).isEqualTo(fromSeedPriceDto.getSeedType());
                    Assertions.assertThat(toSeedPriceDto.getObjectId()).isEqualTo(fromSeedPriceDto.getObjectId());
                    Assertions.assertThat(toSeedPriceDto.getCategory()).isEqualTo(fromSeedPriceDto.getCategory());
                    Assertions.assertThat(toSeedPriceDto.getSourceUnit()).isEqualTo(fromSeedPriceDto.getSourceUnit());
                    Assertions.assertThat(toSeedPriceDto.getPhytoProductType()).isEqualTo(fromSeedPriceDto.getPhytoProductType());
                    Assertions.assertThat(toSeedPriceDto.getPrice()).isEqualTo(fromSeedPriceDto.getPrice());
                    Assertions.assertThat(toSeedPriceDto.getPriceUnit()).isEqualTo(fromSeedPriceDto.getPriceUnit());
                    Assertions.assertThat(toSeedPriceDto.isBiologicalSeedInoculation()).isEqualTo(fromSeedPriceDto.isBiologicalSeedInoculation());
                    Assertions.assertThat(toSeedPriceDto.isChemicalTreatment()).isEqualTo(fromSeedPriceDto.isChemicalTreatment());
                    Assertions.assertThat(toSeedPriceDto.isIncludedTreatment()).isEqualTo(fromSeedPriceDto.isIncludedTreatment());

                }

                Collection<DomainPhytoProductInputDto> fromSpeciesPhytoInputDtos = CollectionUtils.emptyIfNull(fromSpeciesInput.getSpeciesPhytoInputDtos());
                Collection<DomainPhytoProductInputDto> toSpeciesPhytoInputDtos = CollectionUtils.emptyIfNull(toSpeciesInput.getSpeciesPhytoInputDtos());
                Assertions.assertThat(fromSpeciesPhytoInputDtos).size().isEqualTo(toSpeciesPhytoInputDtos.size());

                Map<String, DomainPhytoProductInputDto> toSpeciesPhytoInputDtosByRefInputIds = toSpeciesPhytoInputDtos.stream().collect(
                        Collectors.toMap(DomainPhytoProductInputDto::getRefInputId, Function.identity()));

                for (DomainPhytoProductInputDto fromSpeciesPhytoInputDto : fromSpeciesPhytoInputDtos) {
                    DomainPhytoProductInputDto toSpeciesPhytoInputDto = toSpeciesPhytoInputDtosByRefInputIds.get(fromSpeciesPhytoInputDto.getRefInputId());
                    Assertions.assertThat(toSpeciesPhytoInputDto).isNotNull();
                    Assertions.assertThat(toSpeciesPhytoInputDto.getUsageUnit()).isEqualTo(fromSpeciesPhytoInputDto.getUsageUnit());
                    Assertions.assertThat(toSpeciesPhytoInputDto.getAmmCode()).isEqualTo(fromSpeciesPhytoInputDto.getAmmCode());
                    Assertions.assertThat(toSpeciesPhytoInputDto.getProductType()).isEqualTo(fromSpeciesPhytoInputDto.getProductType());

                    validateDomainInputDto(toSpeciesPhytoInputDto, fromSpeciesPhytoInputDto);
                }

                Assertions.assertThat(toSpeciesInput.getSeedType()).isEqualTo(fromSpeciesInput.getSeedType());
            }

        }

        String lotEntityTopiaId = domainSeedLotInputEntity.getTopiaId();


        Domain domainBaulon_2014 = domainService.extendDomain(domainBaulon_2013.getTopiaId(), 2014);
        validNonDuplicatedCode(domainBaulon_2014);

        domainInputStockUnitService.copyInputStocks(domainBaulon_2013.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainBaulon_2014.getTopiaId()), false);
        // there are duplicated crop on domainLaBouineliere so copie can't be donne
        Assertions.assertThat(domainSeedLotInputDao.forDomainEquals(domainBaulon_2014).exists()).isTrue();

        validNonDuplicatedCode(domainBaulon_2014);

        // add similar crop to domainLaBouineliere
        createNewCropToGivenDomain(croppingPlanEntriesForDomain, domainBaulon_2014, false);
        final Domain extendDomainLaBouineliere = domainService.extendDomain(domainBaulon_2014.getTopiaId(), domainBaulon_2014.getCampaign() + 1);

        domainInputStockUnitService.copyInputStocks(
                domainBaulon_2013.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainBaulon_2014.getTopiaId()), false);
        validNonDuplicatedCode(domainBaulon_2014);

        DomainSeedLotInput copiedDomainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(domainBaulon_2014).findUnique();
        Assertions.assertThat(copiedDomainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

        // update, there should not have more has forceCopy is false and inputs already exists
        domainInputStockUnitService.copyInputStocks(domainBaulon_2013.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainBaulon_2014.getTopiaId()), false);
        copiedDomainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(domainBaulon_2014).findUnique();
        Assertions.assertThat(copiedDomainSeedLotInputEntity.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));
        validNonDuplicatedCode(domainBaulon_2014);

        // update, there should be 1 more has forceCopy is true and inputs are added
        domainInputStockUnitService.copyInputStocks(domainBaulon_2013.getTopiaId(), Lists.newArrayList(lotEntityTopiaId), Lists.newArrayList(domainBaulon_2014.getTopiaId()), true);
        Assertions.assertThat(domainSeedLotInputDao.forDomainEquals(domainBaulon_2014).count()).isEqualTo(2);
        validNonDuplicatedCode(domainBaulon_2014);

        // copie inputs to duplicated inputs
        domainInputStockUnitService.copyInputStocks(
                domainBaulon_2013.getTopiaId(),
                Lists.newArrayList(lotEntityTopiaId),
                Lists.newArrayList(extendDomainLaBouineliere.getTopiaId()),
                true);

        validNonDuplicatedCode(domainBaulon_2014);

        List<DomainSeedLotInput> domainSeedLotInputs = domainSeedLotInputDao.forDomainEquals(extendDomainLaBouineliere).findAll();

        for (DomainSeedLotInput domainSeedLotInput : domainSeedLotInputs) {
            Assertions.assertThat(domainSeedLotInput.getTopiaId()).contains((DomainSeedLotInput.class.getSimpleName()));

            final Collection<DomainInputDto> duplicatedBaulonInputs = domainInputStockUnitService.loadDomainInputStockDtos(extendDomainLaBouineliere.getTopiaId());
            Assertions.assertThat(duplicatedBaulonInputs.size()).isEqualTo(domainInputDtos.size() + 1);
            duplicatedBaulonInputs.forEach(i -> {
                        DomainSeedLotInputDto dto = (DomainSeedLotInputDto) i;
                        toInputDtoByCodes.put(dto.getCropSeedDto().getCode(), i);
                    }
            );

            for (DomainInputDto fromInput : domainInputDtos) {
                DomainSeedLotInputDto dto = (DomainSeedLotInputDto) fromInput;
                DomainInputDto toInputDto = toInputDtoByCodes.get(dto.getCropSeedDto().getCode());
                Assertions.assertThat(toInputDto).isNotNull();
            }

        }

    }

    protected void validNonDuplicatedCode(Domain domain) {

        List<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domain).findAll();

        Set<String> allCodes = new HashSet<>();
        domainInputStockUnits.forEach(disu -> {
            Assertions.assertThat(disu.getCode()).isNotEmpty();
            Assertions.assertThat(allCodes.contains(disu.getCode())).isFalse();
            allCodes.add(disu.getCode());
        });

    }

    @Test
    public void testAddPhytoProductToDomainSeedSpecies() throws IOException {
        testDatas.createTestDomains();
        Domain domainBaulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        Map<InputType, List<AbstractDomainInputStockUnit>> baulonInputs = testDatas.createInputStorage(domainBaulon);
        List<AbstractDomainInputStockUnit> abstractDomainInputStockUnits = baulonInputs.get(InputType.SEMIS);

        DomainSeedLotInput baulonMainEntry = null;
        for (AbstractDomainInputStockUnit abstractDomainInputStockUnit : abstractDomainInputStockUnits) {
            DomainSeedLotInput domainSeedLotInput = (DomainSeedLotInput) abstractDomainInputStockUnit;
            if (domainSeedLotInput.getCropSeed().getTopiaId().contentEquals("ble-id")) {
                baulonMainEntry = domainSeedLotInput;
            }
        }
        Assertions.assertThat(baulonMainEntry).isNotNull();
        DomainSeedSpeciesInput domainSeedSpeciesInput = baulonMainEntry.getDomainSeedSpeciesInput().iterator().next();
        Assertions.assertThat(domainSeedSpeciesInput.getSpeciesPhytoInputs().size()).isEqualTo(1);

        RefCountry france = domainBaulon.getLocation().getRefCountry();
        RefActaTraitementsProduit produitAbsolu = refActaTraitementsProduitsDao.forNaturalId("5164", 147, france).findUniqueOrNull();

        InputPrice produitAbsoluPrice = testDatas.createInputPrice(
                domainBaulon,
                InputPriceCategory.PHYTO_TRAITMENT_INPUT,
                "Herbicide - Inconnu",
                InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(produitAbsolu),
                null,
                PriceUnit.EURO_L,
                PhytoProductUnit.G_HA.name());

        DomainPhytoProductInput domainPhytoProductInput = testDatas.getOrCreateDomainPhytoProductInput(
                produitAbsolu,
                InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES,
                ProductType.HERBICIDAL,
                PhytoProductUnit.KG_HA,
                domainBaulon,
                produitAbsoluPrice);

        domainInputStockUnitService.duplicateAndAddPhytoProductToDomainSeedSpecies(
                domainBaulon,
                domainSeedSpeciesInput,
                domainPhytoProductInput,
                false);

        validNonDuplicatedCode(domainBaulon);

        Map<InputType, List<AbstractDomainInputStockUnit>> baulonInputWithSeedPhytoAdded = domainInputStockUnitService.loadDomainInputStock(domainBaulon);
        List<AbstractDomainInputStockUnit> updatedAbstractDomainInputStockUnits = baulonInputWithSeedPhytoAdded.get(InputType.SEMIS);
        DomainSeedLotInput newBaulonMainEntry = null;
        for (AbstractDomainInputStockUnit abstractDomainInputStockUnit : updatedAbstractDomainInputStockUnits) {
            DomainSeedLotInput domainSeedLotInput = (DomainSeedLotInput) abstractDomainInputStockUnit;
            if (domainSeedLotInput.getCropSeed().getTopiaId().contentEquals("ble-id")) {
                newBaulonMainEntry = domainSeedLotInput;
            }
        }
        Assertions.assertThat(newBaulonMainEntry).isNotNull();
        DomainSeedSpeciesInput newDomainSeedSpeciesInput = newBaulonMainEntry.getDomainSeedSpeciesInput().iterator().next();
        Assertions.assertThat(newDomainSeedSpeciesInput.getSpeciesPhytoInputs().size()).isEqualTo(2);
        Assertions.assertThat(newDomainSeedSpeciesInput.getSpeciesPhytoInputs()
                        .stream()
                        .map(AbstractDomainInputStockUnit::getInputKey))
                .contains(domainPhytoProductInput.getInputKey());

    }

    private Collection<CroppingPlanEntry> createNewCropToGivenDomain(Collection<CroppingPlanEntry> croppingPlanEntriesForDomain, Domain domainLaBouineliere, boolean preserveCode) {
        Collection<CroppingPlanEntry> domainLaBouineliereCrops = new ArrayList<>();
        Binder<CroppingPlanSpecies, CroppingPlanSpecies> cpsBinder = BinderFactory.newBinder(CroppingPlanSpecies.class);
        Binder<CroppingPlanEntry, CroppingPlanEntry> cpeBinder = BinderFactory.newBinder(CroppingPlanEntry.class);
        for (CroppingPlanEntry fromCroppingPlanEntry : croppingPlanEntriesForDomain) {
            final CroppingPlanEntry toCpe = croppingPlanEntryDao.newInstance();
            cpeBinder.copyExcluding(fromCroppingPlanEntry, toCpe,
                    TopiaEntity.PROPERTY_TOPIA_ID,
                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                    CroppingPlanEntry.PROPERTY_CODE,
                    CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES,
                    CroppingPlanEntry.PROPERTY_DOMAIN
            );
            if (preserveCode) {
                toCpe.setCode(fromCroppingPlanEntry.getCode());
            } else {
                toCpe.setCode(UUID.randomUUID().toString());
            }
            toCpe.setDomain(domainLaBouineliere);
            croppingPlanEntryDao.create(toCpe);
            domainLaBouineliereCrops.add(toCpe);

            CollectionUtils.emptyIfNull(fromCroppingPlanEntry.getCroppingPlanSpecies())
                    .forEach(cps -> {
                        final CroppingPlanSpecies toCps = croppingPlanSpeciesDao.newInstance();
                        cpsBinder.copyExcluding(cps, toCps,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION,
                                CroppingPlanSpecies.PROPERTY_CODE,
                                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY
                        );
                        toCps.setCode(UUID.randomUUID().toString());
                        toCps.setCroppingPlanEntry(toCpe);
                        toCpe.addCroppingPlanSpecies(toCps);
                        croppingPlanSpeciesDao.create(toCps);
                    });
            croppingPlanEntryDao.update(toCpe);
        }
        return domainLaBouineliereCrops;
    }

    private ArrayList<DomainSeedSpeciesInputDto> getDomainSeedSpeciesInputDtos(
            CroppingPlanEntry crop,
            CroppingPlanEntryDto cpeDto,
            InputPriceDto priceDto,
            RefCountry france,
            boolean organic) throws IOException {

        testDatas.importActaTraitementsProduits();

        ArrayList<DomainSeedSpeciesInputDto> speciesInputs = new ArrayList<>();
        Collection<CroppingPlanSpeciesDto> speciesDtos = CollectionUtils.emptyIfNull(cpeDto.getSpecies());
        Map<String, CroppingPlanSpeciesDto> dtoByIds = speciesDtos.stream().collect(
                Collectors.toMap(
                        CroppingPlanSpeciesDto::getTopiaId, Function.identity()));

        Collection<CroppingPlanSpecies> croppingPlanSpecies = CollectionUtils.emptyIfNull(crop.getCroppingPlanSpecies());

        for (CroppingPlanSpecies species : croppingPlanSpecies) {
            final RefEspece refEspece = species.getSpecies();
            final RefVariete variety = species.getVariety();
            InputPriceDto speciesPriceDto = null;
            SeedType seedType = SeedType.SEMENCES_CERTIFIEES;


            ArrayList<DomainPhytoProductInputDto> speciesPhytoInputDtos = new ArrayList<>();
            if (!organic) {
                RefActaTraitementsProduit produitArchipel = refActaTraitementsProduitsDao.forNaturalId("5166", 147, france).findUnique();
                DomainPhytoProductInputDto domainPhytoProductArchipelDto = testDatas.createDomainPhytoProductDto(
                        produitArchipel, ProductType.HERBICIDAL, PhytoProductUnit.G_HA, null, InputType.TRAITEMENT_SEMENCE);
                speciesPhytoInputDtos.add(domainPhytoProductArchipelDto);
            }

            final String varietyName = variety != null ? variety.getLabel() : "";

            boolean biologicalSeedInoculation = false;
            boolean chemicalTreatment = !organic;

            Map<SeedType, String> seedTypeTranslations = new HashMap<>();
            for (SeedType type : SeedType.values()) {
                seedTypeTranslations.put(type, type.name());
            }
            String speciesInputName = DomainInputStockUnitServiceImpl.getLotSpeciesInputName(refEspece, variety, seedType, seedTypeTranslations);
            List<String> cropSuffixes = DomainInputStockUnitServiceImpl.getCropSuffixes(biologicalSeedInoculation, chemicalTreatment, Language.FRENCH.getLocale());

            if (!cropSuffixes.isEmpty()) {
                speciesInputName += ", " + String.join(", ", cropSuffixes);
            }

            if (priceDto != null) {
                speciesPriceDto = priceDto.toBuilder()
                        .chemicalTreatment(chemicalTreatment)
                        .biologicalSeedInoculation(biologicalSeedInoculation)
                        .includedTreatment(chemicalTreatment) // for test true if product
                        .seedType(seedType)
                        .displayName(refEspece.getLibelle_espece_botanique())
                        .build();
                final String objectId = InputPriceService.GET_SPECIES_DTO_SEED_OBJECT_ID.apply(species, speciesPriceDto);
                speciesPriceDto = speciesPriceDto.toBuilder()
                        .objectId(objectId)
                        .build();
            }

            DomainSeedSpeciesInputDto lotSpeciesDto = DomainSeedSpeciesInputDto.builder()
                    .code(UUID.randomUUID().toString())
                    .key(DomainInputStockUnitService.getLotSpeciesInputKey(refEspece.getCode_espece_botanique(), refEspece.getCode_qualifiant_AEE(), biologicalSeedInoculation, chemicalTreatment, organic, varietyName, SeedPlantUnit.KG_PAR_HA, seedType))
                    .inputName(speciesInputName)
                    .speciesSeedDto(dtoByIds.get(species.getTopiaId()))
                    .inputType(InputType.SEMIS)
                    .organic(organic)
                    .biologicalSeedInoculation(biologicalSeedInoculation)
                    .chemicalTreatment(chemicalTreatment)
                    .seedType(seedType)
                    .seedPrice(speciesPriceDto)
                    .speciesPhytoInputDtos(speciesPhytoInputDtos)
                    .build();
            speciesInputs.add(lotSpeciesDto);

        }
        return speciesInputs;
    }

    @Test
    public void testCopyMineralInput() throws IOException, DomainExtendException {
        testDatas.createTestDomains();

        DomainMineralProductInputDto inputDto = DomainMineralProductInputDto.builder()
                .inputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)
                .usageUnit(MineralProductUnit.KG_HA)
                .forme("Pulvérulent")
                .formeTranslated("Pulvérulent - i18N")
                .inputName("315_Pulverulent")
                .categ(315)
                .n(3d)
                .k2o(7d)
                .calcium(21d)
                .tradeName("un Pulvérulent")
                .tradeNameTranslated("un Pulvérulent - i18N")
                .type_produit("Autre")
                .type_produit_Translated("Autre - i18N")
                .phytoEffect(true)
                .build();

        InputPriceDto priceDto = InputPriceDto.builder()
                .objectId(InputPriceService.GET_REF_FERTI_MIN_UNIFA_DTO_OBJECT_ID.apply(inputDto))
                .price(12.3)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .sourceUnit(MineralProductUnit.KG_HA.name())
                .displayName("315_Pulverulent")
                .category(InputPriceCategory.MINERAL_INPUT)
                .build();

        inputDto = inputDto
                .toBuilder()
                .price(priceDto)
                .build();

        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        Domain duplicatedDomainBaulon = domainService.extendDomain(baulon.getTopiaId(), baulon.getCampaign() + 1);
        validNonDuplicatedCode(duplicatedDomainBaulon);

        final List<CroppingPlanEntry> croppingPlanEntriesForDomain0 = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CropPersistResult> pesristedCropResult = DomainServiceImpl.getCropPersistResultForPersistedOnes(croppingPlanEntriesForDomain0);

        domainInputStockUnitService.createOrUpdateDomainInputStock(baulon, Lists.newArrayList(inputDto), pesristedCropResult);

        validNonDuplicatedCode(baulon);
        Collection<DomainInputDto> inputs = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());
        Collection<DomainInputDto> duplicatedDomainBaulonInputs = domainInputStockUnitService.loadDomainInputStockDtos(baulon.getTopiaId());
        final Set<String> copiedInputIds = inputs.stream().map(i -> i.getTopiaId().orElse(null)).collect(Collectors.toSet());


        domainInputStockUnitService.copyInputStocks(baulon.getTopiaId(), copiedInputIds, Lists.newArrayList(duplicatedDomainBaulon.getTopiaId()), false);
        validNonDuplicatedCode(duplicatedDomainBaulon);

        final Collection<DomainInputDto> duplicatedBaulonInputs = domainInputStockUnitService.loadDomainInputStockDtos(duplicatedDomainBaulon.getTopiaId());
        Assertions.assertThat(duplicatedBaulonInputs.size()).isEqualTo(copiedInputIds.size() + duplicatedDomainBaulonInputs.size());
        Map<String, DomainInputDto> toInputDtoByCodes = Maps.uniqueIndex(duplicatedBaulonInputs, DomainInputDto::getCode);
        for (DomainInputDto fromInput : inputs) {
            DomainInputDto toInputDto = toInputDtoByCodes.get(fromInput.getCode());
            Assertions.assertThat(toInputDto).isNotNull();
            Assertions.assertThat(toInputDto).isEqualTo(fromInput);
        }
    }

    @Test
    public void addPhytoToSpeciesWithUsages() throws IOException {
        testDatas.createEffectiveInterventions();

        Domain domainBaulon = domainService.getDomain(TestDatas.BAULON_TOPIA_ID);
        Collection<DomainInputDto> domainInputDtos = domainInputStockUnitService.loadDomainInputStockDtoByCodes(domainBaulon.getTopiaId()).values();
        List<DomainInputDto> seedingDomainInputDtosA = domainInputDtos.stream().filter(diDto -> InputType.SEMIS.equals(diDto.getInputType())).toList();

        Assertions.assertThat(seedingDomainInputDtosA).isNotEmpty();

        List<CroppingPlanEntry> baulonCroppingPlanEntries = croppingPlanEntryDao.forDomainEquals(domainBaulon).findAll();
        Map<String, CropPersistResult> cropsByOriginalIds = DomainServiceImpl.getCropPersistResultForPersistedOnes(baulonCroppingPlanEntries);
        domainInputStockUnitService.createOrUpdateDomainInputStock(
                domainBaulon,
                domainInputDtos,
                cropsByOriginalIds
        );

        getPersistenceContext().commit();

        domainInputDtos = domainInputStockUnitService.loadDomainInputStockDtoByCodes(domainBaulon.getTopiaId()).values();
        List<DomainInputDto> seedingDomainInputDtosB = domainInputDtos.stream().filter(diDto -> InputType.SEMIS.equals(diDto.getInputType())).toList();

        Assertions.assertThat(seedingDomainInputDtosA).isEqualTo(seedingDomainInputDtosB);

        // ajout d'un phyto aux espèces
        seedingDomainInputDtosB.stream().map(
                        adiDto -> ((DomainSeedLotInputDto) adiDto).getSpeciesInputs()
                ).flatMap(Collection::stream)
                .filter(ssiDto -> CollectionUtils.isNotEmpty(ssiDto.getSpeciesPhytoInputDtos()))
                .forEach(ssiDto -> {
                    Collection<DomainPhytoProductInputDto> speciesPhytoInputDtos = ssiDto.getSpeciesPhytoInputDtos();
                    DomainPhytoProductInputDto model = ssiDto.getSpeciesPhytoInputDtos().iterator().next();

                    InputPriceDto modelPrice;
                    Optional<InputPriceDto> optionalPrice = model.getPrice();
                    if (optionalPrice.isPresent()) {
                        modelPrice = optionalPrice.get().toBuilder().topiaId(null).build();
                    } else {
                        modelPrice = InputPriceDto.builder()
                                .objectId("FICTIF")
                                .price(12.3)
                                .priceUnit(PriceUnit.EURO_HA)
                                .phytoProductType(null)
                                .sourceUnit(PhytoProductUnit.KG_HA.name())
                                .displayName("IN Copy Of " + model.getInputName())
                                .category(InputPriceCategory.SEEDING_TREATMENT_INPUT)
                                .build();
                    }

                    DomainPhytoProductInputDto newDomainPhytoProductInputDto = DomainPhytoProductInputDto.builder()
                            .refInputId(model.getRefInputId())
                            .productType(model.getProductType())
                            .usageUnit(model.getUsageUnit())
                            .inputType(model.getInputType())
                            .key(model.getKey())
                            .inputName("IN Copy Of " + model.getInputName())
                            .tradeName("TN Copy Of " + model.getInputName())
                            .ammCode(model.getAmmCode().orElse(null))
                            .price(modelPrice)
                            .build();
                    speciesPhytoInputDtos.add(newDomainPhytoProductInputDto);
                });
        domainInputStockUnitService.createOrUpdateDomainInputStock(
                domainBaulon,
                domainInputDtos,
                cropsByOriginalIds
        );

        getPersistenceContext().commit();

        domainInputDtos = domainInputStockUnitService.loadDomainInputStockDtoByCodes(domainBaulon.getTopiaId()).values();
        List<DomainInputDto> seedingDomainInputDtosC = domainInputDtos.stream().filter(diDto -> InputType.SEMIS.equals(diDto.getInputType())).toList();

        Collection<DomainPhytoProductInputDto> newProducts = new ArrayList<>();
        // on vérifie que le nouveau produit a bien été créé
        seedingDomainInputDtosC.stream().map(adiDto -> ((DomainSeedLotInputDto) adiDto).getSpeciesInputs())
                .flatMap(Collection::stream)
                .filter(ssiDto -> CollectionUtils.isNotEmpty(ssiDto.getSpeciesPhytoInputDtos()))
                .forEach(ssiDto -> {
                    Optional<DomainPhytoProductInputDto> newProductFound = ssiDto.getSpeciesPhytoInputDtos().stream()
                            .filter(spiDto -> spiDto.getInputName().contains("IN Copy Of "))
                            .findAny();
                    Assertions.assertThat(newProductFound).isPresent();
                    newProducts.add(newProductFound.get());
                });
        Assertions.assertThat(newProducts).isNotEmpty();

        Set<String> newPhytoProductIds = newProducts.stream()
                .filter(dppiDto -> dppiDto.getTopiaId().isPresent())
                .map(dppiDto -> dppiDto.getTopiaId().get())
                .collect(Collectors.toSet());

        Assertions.assertThat(newPhytoProductIds).isNotEmpty();
        List<DomainPhytoProductInput> allNewDomainPhytoProducts = domainPhytoProductInputTopiaDao.forTopiaIdIn(newPhytoProductIds).findAll();
        List<AbstractPhytoProductInputUsage> allPhytoUsages = abstractPhytoProductInputUsageDao.forDomainPhytoProductInputIn(allNewDomainPhytoProducts).findAll();

        Assertions.assertThat(allPhytoUsages).isNotEmpty();

        // retrait des produits des espèces, mais utilisés donc non retirés
        seedingDomainInputDtosC.stream().map(
                        adiDto -> ((DomainSeedLotInputDto) adiDto).getSpeciesInputs()
                ).flatMap(Collection::stream)
                .filter(ssiDto -> CollectionUtils.isNotEmpty(ssiDto.getSpeciesPhytoInputDtos()))
                .forEach(ssiDto -> {
                    Collection<DomainPhytoProductInputDto> speciesPhytoInputDtos = ssiDto.getSpeciesPhytoInputDtos();
                    speciesPhytoInputDtos.clear();
                });

        domainInputStockUnitService.createOrUpdateDomainInputStock(
                domainBaulon,
                domainInputDtos,
                cropsByOriginalIds
        );

        getPersistenceContext().commit();

        domainInputDtos = domainInputStockUnitService.loadDomainInputStockDtoByCodes(domainBaulon.getTopiaId()).values();
        List<DomainInputDto> seedingDomainInputDtosD = domainInputDtos.stream().filter(diDto -> InputType.SEMIS.equals(diDto.getInputType())).toList();

        Set<String> seedingDomainPhytoIds = new HashSet<>();

        seedingDomainInputDtosD.stream().map(adiDto -> ((DomainSeedLotInputDto) adiDto).getSpeciesInputs())
                .flatMap(Collection::stream)
                .forEach(ssiDto -> {
                    Assertions.assertThat(ssiDto.getSpeciesPhytoInputDtos()).isNotEmpty();
                    ssiDto.getSpeciesPhytoInputDtos()
                            .forEach(dp -> seedingDomainPhytoIds.add(dp.getTopiaId().get()));
                });
        List<String> domainSpeciesIds = seedingDomainInputDtosD.stream().map(adiDto -> ((DomainSeedLotInputDto) adiDto).getSpeciesInputs())
                .flatMap(Collection::stream)
                .filter(dsli -> dsli.getTopiaId().isPresent())
                .map(dsli -> dsli.getTopiaId().get())
                .toList();

        // suppression des usages
        List<DomainSeedSpeciesInput> domainSeedSpeciesInputs = domainSeedSpeciesInputDao.forTopiaIdIn(domainSpeciesIds).findAll();
        List<SeedSpeciesInputUsage> seedSpeciesInputUsages = seedSpeciesInputUsageDao.forDomainSeedSpeciesInputIn(domainSeedSpeciesInputs).findAll();
        seedSpeciesInputUsages.forEach(SeedSpeciesInputUsage::clearSeedProductInputUsages);
        seedSpeciesInputUsageDao.updateAll(seedSpeciesInputUsages);

        getPersistenceContext().commit();

        // retrait des produits des espèces non utilisés
        seedingDomainInputDtosD.stream().map(
                        adiDto -> ((DomainSeedLotInputDto) adiDto).getSpeciesInputs()
                ).flatMap(Collection::stream)
                .filter(ssiDto -> CollectionUtils.isNotEmpty(ssiDto.getSpeciesPhytoInputDtos()))
                .forEach(ssiDto -> {
                    Collection<DomainPhytoProductInputDto> speciesPhytoInputDtos = ssiDto.getSpeciesPhytoInputDtos();
                    speciesPhytoInputDtos.clear();
                });

        domainInputStockUnitService.createOrUpdateDomainInputStock(
                domainBaulon,
                domainInputDtos,
                cropsByOriginalIds
        );

        getPersistenceContext().commit();

        domainInputDtos = domainInputStockUnitService.loadDomainInputStockDtoByCodes(domainBaulon.getTopiaId()).values();
        List<DomainInputDto> seedingDomainInputDtosE = domainInputDtos.stream().filter(diDto -> InputType.SEMIS.equals(diDto.getInputType())).toList();

        // on vérifie que les produits ont bien été retirés
        seedingDomainInputDtosE.stream().map(adiDto -> ((DomainSeedLotInputDto) adiDto).getSpeciesInputs())
                .flatMap(Collection::stream)
                .forEach(ssiDto -> Assertions.assertThat(ssiDto.getSpeciesPhytoInputDtos()).isEmpty());

        List<DomainPhytoProductInput> domainPhytoProductInputs = domainPhytoProductInputTopiaDao.forTopiaIdIn(seedingDomainPhytoIds).findAll();
        Assertions.assertThat(domainPhytoProductInputs).isEmpty();

    }
}
