package fr.inra.agrosyst.services.managementmode;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.managementmode.CategoryObjective;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRule;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCrop;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleCropImpl;
import fr.inra.agrosyst.api.entities.managementmode.DecisionRuleImpl;
import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeCategory;
import fr.inra.agrosyst.api.entities.managementmode.ManagementModeTopiaDao;
import fr.inra.agrosyst.api.entities.managementmode.Section;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.Strategy;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeDto;
import fr.inra.agrosyst.api.services.managementmode.ManagementModeService;
import fr.inra.agrosyst.api.services.managementmode.SectionDto;
import fr.inra.agrosyst.api.services.managementmode.StrategyDto;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class ManagementModeServiceTest extends AbstractAgrosystTest {

    protected DomainService domainService;
    protected GrowingSystemService growingSystemService;
    protected ManagementModeService service;

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        service = serviceFactory.newService(ManagementModeService.class);
        growingSystemService = serviceFactory.newService(GrowingSystemService.class);
        domainService = serviceFactory.newService(DomainService.class);

        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.createTestManagementModes();

        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();

        loginAsAdmin();
        alterSchema();
    }

    /**
     * Test de la duplication des modes de gestion et des regles.
     * @throws DomainExtendException
     */
    @Test
    public void testExtendManagementModeAndRules() throws DomainExtendException {
        // get specific growing system
        RefNuisibleEDI refNuisibleEDI = testDatas.refNuisibleEDIDao.create(
                RefNuisibleEDI.PROPERTY_ACTIVE, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, BioAgressorType.ADVENTICE,
                RefNuisibleEDI.PROPERTY_MAIN, true,
                RefNuisibleEDI.PROPERTY_REFERENCE_LABEL, "Nuisible"
        );
        
        GrowingSystemFilter filter = new GrowingSystemFilter();
        filter.setGrowingSystemName("Systeme de culture Baulon 1");
        List<GrowingSystem> growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem growingSystem = growingSystems.getFirst();
        GrowingPlan growingPlan = growingSystem.getGrowingPlan();
        Domain domain = growingPlan.getDomain();

        // get this growing system management mode
        ManagementMode managementMode = service.getManagementModeForGrowingSystem(growingSystem);
        
        // create new rule
        DecisionRule rule = new DecisionRuleImpl();
        rule.setName("Rule baulon");
        rule.setInterventionType(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        rule.setActive(true);
        rule.setCode(UUID.randomUUID().toString());
        rule.setVersionNumber(0);

        List<String> cropCodes = new ArrayList<>();
        List<DecisionRuleCrop> decisionRuleCrops = new ArrayList<>();
        List<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domain.getTopiaId());
        List<String> cropIds = new ArrayList<>();
        for (CroppingPlanEntryDto cropDto : cropDtos) {
            String cropCode = cropDto.getCode();
            cropCodes.add(cropCode);
            DecisionRuleCrop decisionRuleCrop = new DecisionRuleCropImpl();
            decisionRuleCrop.setCroppingPlanEntryCode(cropCode);
            decisionRuleCrops.add(decisionRuleCrop);

            cropIds.add(cropDto.getTopiaId());
        }

        rule = service.createOrUpdateDecisionRule(rule, growingPlan.getDomain().getCode(), null, decisionRuleCrops);

        RefStrategyLever refStrategyLever = getPersistenceContext().getRefStrategyLeverDao().create(
                RefStrategyLever.PROPERTY_ACTIVE, true,
                RefStrategyLever.PROPERTY_LEVER, "Aération des arbres",
                RefStrategyLever.PROPERTY_CODE, "ARBO_MALA_ATTE_00043",
                RefStrategyLever.PROPERTY_SECTOR, Sector.ARBORICULTURE,
                RefStrategyLever.PROPERTY_SECTION_TYPE, SectionType.MALADIES,
                RefStrategyLever.PROPERTY_STRATEGY_TYPE, StrategyType.ATTENUATION
        );

        StrategyDto strategyDto = new StrategyDto();
        strategyDto.setCropIds(cropIds);
        strategyDto.setExplanation("A Kadoc");
        strategyDto.setStrategyType(StrategyType.BIOCONTROL);
        strategyDto.setDecisionRuleIds(Collections.singleton(rule.getTopiaId()));
        strategyDto.setRefStrategyLever(refStrategyLever);
        List<StrategyDto> managementModeStrategies = Collections.singletonList(strategyDto);

        // add section, strategie and rule
        SectionDto sectionDto = new SectionDto();
        sectionDto.setSectionType(SectionType.MALADIES);
        sectionDto.setStrategiesDto(managementModeStrategies);
        sectionDto.setCategoryObjective(CategoryObjective.MINIMISER_LES_DOMMAGES);
        sectionDto.setBioAgressorTopiaId(refNuisibleEDI.getTopiaId());
        
        List<SectionDto> managementModeSectionDtos = Collections.singletonList(sectionDto);

        ManagementMode persistedManagementMode = service.createOrUpdateManagementMode(managementMode, growingSystem.getTopiaId(), managementModeSectionDtos);

        // extends this domain
        domainService.extendDomain(domain.getTopiaId(), 2016);
        filter.setCampaign(2016);
        growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        Assertions.assertEquals(1, growingSystems.size()); // another one in 2013

        ManagementMode extendedManagementMode = service.getManagementModeForGrowingSystem(growingSystem);
        Assertions.assertEquals(1, extendedManagementMode.getSections().size());
        Collection<Section> extendedSections = extendedManagementMode.getSections();
        Collection<Section> fromSections = persistedManagementMode.getSections();

        Assertions.assertEquals(1, fromSections.size());
        Assertions.assertEquals(1, extendedSections.size());

        for (Section fromSection : fromSections) {
            for (Section extendedSection : extendedSections) {
                Assertions.assertEquals(SectionType.MALADIES, extendedSection.getSectionType());
                Assertions.assertEquals(fromSection.getSectionType(), extendedSection.getSectionType());
                List<Strategy> fromStrategies = fromSection.getStrategies();
                List<Strategy> extendedStrategies = fromSection.getStrategies();

                Assertions.assertEquals(1, fromStrategies.size());
                Assertions.assertEquals(1, extendedStrategies.size());

                for (Strategy fromStrategy : fromStrategies) {

                    for (Strategy extendedStrategy : extendedStrategies) {
                        Assertions.assertEquals(cropCodes.size(), extendedStrategy.getCrops().size());
                        Assertions.assertEquals(fromStrategy.getCrops().size(), extendedStrategy.getCrops().size());

                        Assertions.assertEquals("A Kadoc", extendedStrategy.getExplanation());
                        Assertions.assertEquals(fromStrategy.getExplanation(), extendedStrategy.getExplanation());

                        Collection<DecisionRule> fromRules = fromStrategy.getRules();
                        Collection<DecisionRule> extendedRules = extendedStrategy.getRules();
                        Assertions.assertEquals(1, fromRules.size());
                        Assertions.assertEquals(1, extendedRules.size());
                        for (DecisionRule fromRule : fromRules) {
                            for (DecisionRule extendedRule : extendedRules) {
                                Assertions.assertEquals(cropCodes.size(), fromRule.getDecisionRuleCrop().size());
                                Assertions.assertEquals(cropCodes.size(), extendedRule.getDecisionRuleCrop().size());
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * https://forge.codelutin.com/issues/4409
     */
    @Test
    public void testSameDecisionRuleName() throws DomainExtendException {
        // get specific growing system
        DomainFilter df = new DomainFilter();
        df.setDomainName("Baulon");
        PaginationResult<Domain> domains = domainService.getFilteredDomains(df);
        Domain domain = domains.getElements().iterator().next();

        // create new rule
        DecisionRule rule = new DecisionRuleImpl();
        rule.setName("Rule baulon");
        rule.setInterventionType(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        rule.setActive(true);
        rule.setCode(UUID.randomUUID().toString());
        rule.setVersionNumber(0);

        DecisionRule decisionRule = service.createOrUpdateDecisionRule(rule, domain.getCode(), null, null);

        DecisionRule newVersion = service.createNewDecisionRuleVersion(decisionRule.getCode(), "pouette");

        newVersion.setName("azerty");
        service.createOrUpdateDecisionRule(newVersion, domain.getCode(), null, null);

        List<DecisionRule> list = service.getRelatedDecisionRules(newVersion.getCode());
        Assertions.assertEquals(2, list.size());
        for (DecisionRule dr : list) {
            Assertions.assertEquals("azerty", dr.getName());
        }
    }

    @Test
    public void testLoadManagementModes(){
        GrowingSystemFilter filter = new GrowingSystemFilter();
        List<GrowingSystem> growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem gs0 = growingSystems.stream()
                .filter(gs -> gs.getName().contains("Efficience"))
                .findFirst()
                .get();
        ManagementModeTopiaDao managementModeDao = getPersistenceContext().getManagementModeDao();

        // un prévu est déjà présent
        GrowingSystem gs1 = growingSystems.stream()
                .filter(gs -> gs.getName().contains("Baulon 1"))
                .findFirst()
                .get();
        ManagementMode mm2 = managementModeDao.create(
                ManagementMode.PROPERTY_CATEGORY, ManagementModeCategory.PLANNED,
                ManagementMode.PROPERTY_GROWING_SYSTEM, gs1
        );

        GrowingSystem gs2 = growingSystems.stream()
                .filter(gs -> gs.getName().contains("Baulon 2"))
                .findFirst()
                .get();
        ManagementMode mm4 = managementModeDao.create(
                ManagementMode.PROPERTY_CATEGORY, ManagementModeCategory.PLANNED,
                ManagementMode.PROPERTY_GROWING_SYSTEM, gs2
        );

        PaginationResult<ManagementModeDto> res0 = service.loadWritableManagementModesForGrowingSystemIds(
                Arrays.asList(gs0.getTopiaId(), gs2.getTopiaId()));

        List<ManagementModeDto> results = res0.getElements();
        Assertions.assertEquals(2, results.size());

        boolean foundGs0 = false, foundGs2 = false;
        for (ManagementModeDto result : results) {

            if (result.getGrowingSystem().getTopiaId().equals(gs0.getTopiaId())) {
                foundGs0 = true;
                Assertions.assertNotNull(result.getPlannedManagementModeId());
            }
            if (result.getGrowingSystem().getTopiaId().equals(gs2.getTopiaId())) {
                foundGs2 = true;
                Assertions.assertNotNull(result.getObservedManagementModeId());
                Assertions.assertNotNull(result.getPlannedManagementModeId());
            }
        }
        Assertions.assertTrue(foundGs0 && foundGs2);

    }

    /**
     * Test de la duplication des modes de gestion et des regles.
     */
    @Test
    public void testCreateDecisionRuleWithCrops(){
        // get specific growing system
        GrowingSystemFilter filter = new GrowingSystemFilter();
        filter.setGrowingSystemName("Systeme de culture Baulon 1");
        List<GrowingSystem> growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem growingSystem = growingSystems.getFirst();
        Domain domain = growingSystem.getGrowingPlan().getDomain();

        List<DecisionRuleCrop> decisionRuleCrops = new ArrayList<>();
        List<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domain.getTopiaId());
        for (CroppingPlanEntryDto cropDto : cropDtos) {
            String cropCode = cropDto.getCode();
            DecisionRuleCrop decisionRuleCrop = new DecisionRuleCropImpl();
            decisionRuleCrop.setCroppingPlanEntryCode(cropCode);
            decisionRuleCrops.add(decisionRuleCrop);
        }
        // create new rule
        DecisionRule rule = new DecisionRuleImpl();
        rule.setName("Rule baulon");
        rule.setInterventionType(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        rule.setActive(true);
        rule.setCode(UUID.randomUUID().toString());
        rule.setVersionNumber(0);
        rule = service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, decisionRuleCrops);

        Assertions.assertEquals(cropDtos.size(), rule.getDecisionRuleCrop().size());

        decisionRuleCrops.remove(decisionRuleCrops.size() -1 );
        rule = service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, decisionRuleCrops);

        Assertions.assertEquals(cropDtos.size()-1, rule.getDecisionRuleCrop().size());

        rule = service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, null);

        Assertions.assertEquals(0, rule.getDecisionRuleCrop().size());

        decisionRuleCrops = new ArrayList<>();
        List<DecisionRuleCrop> secondPart = new ArrayList<>();

        for (int i = 0; i < cropDtos.size(); i++) {
            CroppingPlanEntryDto cropDto = cropDtos.get(i);
            String cropCode = cropDto.getCode();
            DecisionRuleCrop decisionRuleCrop = new DecisionRuleCropImpl();
            decisionRuleCrop.setCroppingPlanEntryCode(cropCode);
            if (i < cropDtos.size()/2) {
             decisionRuleCrops.add(decisionRuleCrop);
            } else {
             secondPart.add(decisionRuleCrop);
            }
        }

        rule = service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, decisionRuleCrops);

        Assertions.assertEquals(decisionRuleCrops.size(), rule.getDecisionRuleCrop().size());

        decisionRuleCrops.addAll(secondPart);

        rule = service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, decisionRuleCrops);

        Assertions.assertEquals(decisionRuleCrops.size(), rule.getDecisionRuleCrop().size());
    }


    @Test
    public void testDuplicateRule() throws DomainExtendException {
        // get specific growing system
        GrowingSystemFilter filter = new GrowingSystemFilter();
        filter.setGrowingSystemName("Systeme de culture Baulon 1");
        List<GrowingSystem> growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem growingSystem = growingSystems.getFirst();
        Domain domain = growingSystem.getGrowingPlan().getDomain();

        List<DecisionRuleCrop> decisionRuleCrops = new ArrayList<>();
        List<CroppingPlanEntryDto> cropDtos = domainService.getCroppingPlanDtos(domain.getTopiaId());
        for (CroppingPlanEntryDto cropDto : cropDtos) {
            String cropCode = cropDto.getCode();
            DecisionRuleCrop decisionRuleCrop = new DecisionRuleCropImpl();
            decisionRuleCrop.setCroppingPlanEntryCode(cropCode);
            decisionRuleCrops.add(decisionRuleCrop);
        }
        // create new rule
        DecisionRule rule = new DecisionRuleImpl();
        rule.setName("Rule baulon");
        rule.setInterventionType(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        rule.setActive(true);
        rule.setCode(UUID.randomUUID().toString());
        rule.setVersionNumber(0);
        rule = service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, decisionRuleCrops);

        DecisionRule duplicatedRule = service.duplicateDecisionRule(rule.getTopiaId());

        Assertions.assertNotEquals(rule.getCode(), duplicatedRule.getCode());
        Assertions.assertEquals(1, duplicatedRule.getVersionNumber());
        Assertions.assertEquals(rule.getDomainCode(), duplicatedRule.getDomainCode());
        Assertions.assertEquals(rule.getName(), duplicatedRule.getName());
        Assertions.assertEquals(rule.getDomainValidity(), duplicatedRule.getDomainValidity());
        Assertions.assertEquals(rule.getExpectedResult(), duplicatedRule.getExpectedResult());
        Assertions.assertEquals(rule.getDecisionCriteria(), duplicatedRule.getDecisionCriteria());
        Assertions.assertEquals(rule.getVersionReason(), duplicatedRule.getVersionReason());
        Assertions.assertEquals(rule.getDecisionRuleCrop().size(), duplicatedRule.getDecisionRuleCrop().size());
    }

    @Test
    public void testUnvalideCropOnManagementMode() {
        // get specific growing system
    
        RefAdventice refAdventice = testDatas.refAdventiceDao.create(
                RefAdventice.PROPERTY_ACTIVE, true,
                RefAdventice.PROPERTY_IDENTIFIANT, "AdvTest0",
                RefAdventice.PROPERTY_MAIN, true,
                RefAdventice.PROPERTY_ADVENTICE, "Adventice"
        );
        
        GrowingSystemFilter filter = new GrowingSystemFilter();
        filter.setGrowingSystemName("Systeme de culture Baulon 1");
        List<GrowingSystem> growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem growingSystem = growingSystems.getFirst();

        filter.setGrowingSystemName("Systeme de culture Couëron les bains 1");
        List<GrowingSystem> growingSystems2 = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem growingSystem2 = growingSystems2.getFirst();
        Domain domain2 = growingSystem2.getGrowingPlan().getDomain();

        CroppingPlanEntry baulonGombo0 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain2,
                CroppingPlanEntry.PROPERTY_CODE, UUID.randomUUID().toString(),
                CroppingPlanEntry.PROPERTY_NAME, "unvalid0",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanEntry baulonGombo1 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain2,
                CroppingPlanEntry.PROPERTY_CODE, UUID.randomUUID().toString(),
                CroppingPlanEntry.PROPERTY_NAME, "unvalid1",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanEntry baulonGombo2 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain2,
                CroppingPlanEntry.PROPERTY_CODE, UUID.randomUUID().toString(),
                CroppingPlanEntry.PROPERTY_NAME, "unvalid2",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanEntry baulonGombo3 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain2,
                CroppingPlanEntry.PROPERTY_CODE, UUID.randomUUID().toString(),
                CroppingPlanEntry.PROPERTY_NAME, "unvalid3",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);

        List<CroppingPlanEntry> otherCrops = Arrays.asList(baulonGombo0, baulonGombo1, baulonGombo2, baulonGombo3);

        // get this growing system management mode
        ManagementMode managementMode = service.getManagementModeForGrowingSystem(growingSystem);

        List<String> cropIds = new ArrayList<>();
        for (CroppingPlanEntry crop : otherCrops) {
            cropIds.add(crop.getTopiaId());
        }

        RefStrategyLever refStrategyLever = getPersistenceContext().getRefStrategyLeverDao().create(
                RefStrategyLever.PROPERTY_ACTIVE, true,
                RefStrategyLever.PROPERTY_LEVER, "Aération des arbres",
                RefStrategyLever.PROPERTY_CODE, "ARBO_MALA_ATTE_00043",
                RefStrategyLever.PROPERTY_SECTOR, Sector.ARBORICULTURE,
                RefStrategyLever.PROPERTY_SECTION_TYPE, SectionType.MALADIES,
                RefStrategyLever.PROPERTY_STRATEGY_TYPE, StrategyType.ATTENUATION
        );

        StrategyDto strategyDto = new StrategyDto();
        strategyDto.setCropIds(cropIds);
        strategyDto.setExplanation("A Kadoc");
        strategyDto.setStrategyType(StrategyType.BIOCONTROL);
        strategyDto.setRefStrategyLever(refStrategyLever);
        List<StrategyDto> managementModeStrategies = Collections.singletonList(strategyDto);

        // add section, strategie and rule
        SectionDto sectionDto = new SectionDto();
        sectionDto.setSectionType(SectionType.MALADIES);
        sectionDto.setStrategiesDto(managementModeStrategies);
        sectionDto.setCategoryObjective(CategoryObjective.MINIMISER_LES_DOMMAGES);
        sectionDto.setBioAgressorTopiaId(refAdventice.getTopiaId());
        
        List<SectionDto> managementModeSectionDtos = Collections.singletonList(sectionDto);

        ManagementMode persistedManagementMode = service.createOrUpdateManagementMode(managementMode, growingSystem.getTopiaId(), managementModeSectionDtos);
        Assertions.assertEquals(1, persistedManagementMode.getSections().size());
        Collection<Section> sections = persistedManagementMode.getSections();
        for (Section section : sections) {
            List<Strategy> strategies = section.getStrategies();
            for (Strategy strategy : strategies) {
                Assertions.assertNull(strategy.getCrops());
            }
        }
    }

    @Test
    public void testActive() {
        GrowingSystemFilter filter = new GrowingSystemFilter();
        filter.setGrowingSystemName("Systeme de culture Baulon 1");
        List<GrowingSystem> growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem growingSystem = growingSystems.getFirst();
        Domain domain = growingSystem.getGrowingPlan().getDomain();

        DecisionRule rule = new DecisionRuleImpl();
        rule.setName("Rule baulon");
        rule.setInterventionType(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        rule.setActive(true);
        rule.setCode(UUID.randomUUID().toString());
        rule.setVersionNumber(0);
        rule = service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, null);

        Assertions.assertNotNull(rule);

        DomainTopiaDao domainDao = getPersistenceContext().getDomainDao();
        domain.setActive(false);
        domainDao.update(domain);

        boolean active = service.isDecisionRuleActive(rule);

        Assertions.assertFalse(active);

        boolean failed = false;
        try {
            service.createOrUpdateDecisionRule(rule, growingSystem.getGrowingPlan().getDomain().getCode(), null, null);
        } catch (Exception e) {
            Assertions.assertEquals("Update decision rule with id '" + rule.getTopiaId() + "' is not allowed because it is unactivated or the domains related to it or unactivated", e.getMessage());
            failed = true;
        }
        Assertions.assertTrue(failed);
    }

    @Test
    public void testAvailableDomainForDuplication() {
        GrowingSystemFilter filter = new GrowingSystemFilter();
        filter.setGrowingSystemName("Systeme de culture Baulon 1");
        List<GrowingSystem> growingSystems = growingSystemService.getFilteredGrowingSystems(filter).getElements();
        GrowingSystem growingSystem = growingSystems.getFirst();
        Domain domain = growingSystem.getGrowingPlan().getDomain();

        // get this growing system management mode
        ManagementMode managementMode = service.getManagementModeForGrowingSystem(growingSystem);

        List<GrowingSystem> availableGsForDuplication = service.getAvailableGsForDuplication(growingSystem.getTopiaId(),null);

        Assertions.assertEquals(1, growingSystems.size());

        for (GrowingSystem gSystem : availableGsForDuplication) {
            gSystem.setActive(false);
        }
        GrowingSystemTopiaDao gsDao =  getPersistenceContext().getGrowingSystemDao();
        gsDao.update(growingSystem);

        availableGsForDuplication = service.getAvailableGsForDuplication(growingSystem.getTopiaId(),null);

        Assertions.assertEquals(0, availableGsForDuplication.size());

        for (GrowingSystem gSystem : availableGsForDuplication) {
            gSystem.setActive(true);
        }
        gsDao.update(growingSystem);

        GrowingPlan gp = growingSystem.getGrowingPlan();
        gp.setActive(false);

        GrowingPlanTopiaDao gpDao =  getPersistenceContext().getGrowingPlanDao();
        gpDao.update(gp);

        availableGsForDuplication = service.getAvailableGsForDuplication(growingSystem.getTopiaId(),null);

        Assertions.assertEquals(0, availableGsForDuplication.size());

        gp.setActive(true);
        gpDao.update(gp);

        Domain d = gp.getDomain();
        d.setActive(false);

        DomainTopiaDao domainDao = getPersistenceContext().getDomainDao();
        domainDao.update(d);

        Assertions.assertEquals(0, availableGsForDuplication.size());
    }

    @Test
    public void testGetGrowingSystemsForManagementMode() {
        List<GrowingSystem> growingSystems = service.getGrowingSystemsForManagementMode(null);
        Assertions.assertEquals(6, growingSystems.size());

        GrowingSystemTopiaDao gsDao =  getPersistenceContext().getGrowingSystemDao();
        GrowingSystem gsBaulon = null;
        for (GrowingSystem growingSystem : growingSystems) {
            if (growingSystem.getName().equals("Systeme de culture Baulon 1")) {
                gsBaulon = growingSystem;
                gsBaulon.setActive(false);
                gsDao.update(gsBaulon);
            }
        }

        growingSystems = service.getGrowingSystemsForManagementMode(null);

        Assertions.assertEquals(5, growingSystems.size());

        gsBaulon.setActive(true);
        gsDao.update(gsBaulon);

        GrowingPlan gp = gsBaulon.getGrowingPlan();
        gp.setActive(false);

        GrowingPlanTopiaDao gpDao =  getPersistenceContext().getGrowingPlanDao();
        gpDao.update(gp);

        growingSystems = service.getGrowingSystemsForManagementMode(null);

        Assertions.assertEquals(4, growingSystems.size());
        gp.setActive(true);
        gpDao.update(gp);

        Domain d = gp.getDomain();
        d.setActive(false);

        DomainTopiaDao domainDao = getPersistenceContext().getDomainDao();
        domainDao.update(d);

        growingSystems = service.getGrowingSystemsForManagementMode(null);

        Assertions.assertEquals(4, growingSystems.size());

    }

}
