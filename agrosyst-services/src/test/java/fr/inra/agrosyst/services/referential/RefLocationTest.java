package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;


/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class RefLocationTest {

    @Test
    public void testNormalize() throws TopiaException {
        Assertions.assertEquals("le blanc", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("Le blanc"));
        Assertions.assertEquals("le blanc", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("  Le   blanc  "));
        Assertions.assertEquals("le blanc", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("Le-blanc"));
        Assertions.assertEquals("le blanc", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("Le-Blanc"));

        Assertions.assertEquals("la chapelle-sur-erdre", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("La chapelle sur erdre "));
        Assertions.assertEquals("la chapelle-sur-erdre", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("La-chapelle_sur,erdre."));

        Assertions.assertEquals("varennes-sur-fouzon", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("Varennes Sur_Fouzon"));

        Assertions.assertEquals("steenvoorde", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("Steenvoorde"));
        Assertions.assertEquals("vendenheim", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("vendenheim"));
        Assertions.assertEquals("nort-sur-erdre", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("Nort,sur-erdre"));
        Assertions.assertEquals("la chapelle-d'armentières", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("La-chapelle-d'Armentières"));
        Assertions.assertEquals("la chapelle-d'armentières", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("La chapelle-d'Armentières"));
        Assertions.assertEquals("la chapelle-d'armentières", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("La _ , - chapelle  d'Armentières"));
        Assertions.assertEquals("la chapelle-d'armentières", RefLocationTopiaDao.NORMALIZE_COMMUNE_FOR_SEARCH.apply("La _ , - chapelle  d   Armentières"));
    }

}
