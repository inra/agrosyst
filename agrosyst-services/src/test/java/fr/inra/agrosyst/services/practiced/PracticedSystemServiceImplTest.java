package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.EquipmentTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceImpl;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.BiologicalControlActionImpl;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionImpl;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionImpl;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsageImpl;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.QualityCriteria;
import fr.inra.agrosyst.api.entities.action.QualityCriteriaImpl;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpecies;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPCTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutilTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTractionTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefOtherInputTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.services.action.AbstractActionDto;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.action.BiologicalControlActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionDto;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.action.MineralFertilizersSpreadingActionDto;
import fr.inra.agrosyst.api.services.action.QualityCriteriaDto;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.input.AbstractInputUsageDto;
import fr.inra.agrosyst.api.services.input.MineralProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.OtherProductInputUsageDto;
import fr.inra.agrosyst.api.services.input.PhytoProductInputUsageDto;
import fr.inra.agrosyst.api.services.itk.Itk;
import fr.inra.agrosyst.api.services.itk.SpeciesStadeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleNodeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCyclePhaseDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleSpeciesDto;
import fr.inra.agrosyst.api.services.practiced.PracticedInterventionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPerennialCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSeasonalCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.action.ActionServiceImpl;
import fr.inra.agrosyst.services.action.ActionServiceImplMock;
import fr.inra.agrosyst.services.common.CommonService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.util.beans.BinderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author David Cossé
 */
public class PracticedSystemServiceImplTest extends AbstractAgrosystTest {


    protected ActionServiceImplMock actionService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected DomainService domainService;
    protected GrowingPlanService growingPlanService;
    protected GrowingSystemService growingSystemService;
    protected PracticedSystemService practicedSystemService;
    protected PracticedSystemServiceMock practicedSystemServiceMock;

    protected ImportService importService;
    protected ReferentialService referentialService;
    protected PricesService pricesService;

    protected RefStadeEDITopiaDao refStadeEDITopiaDao;

    protected GrowingSystemTopiaDao growingSystemTopiaDao;

    protected EquipmentTopiaDao equipmentDao;

    protected RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao;

    protected PracticedSystemTopiaDao practicedSystemTopiaDao;

    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleTopiaDao;

    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionTopiaDao;

    protected PracticedInterventionTopiaDao practicedInterventionTopiaDao;

    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeTopiaDao;

    protected PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleTopiaDao;

    protected PracticedPlotTopiaDao practicedPlotDao;

    protected PracticedCropCycleSpeciesTopiaDao practicedCropCycleSpeciesTopiaDao;

    protected RefFertiMinUNIFATopiaDao refFertiMinUNIFATopiaDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao;
    protected RefOtherInputTopiaDao refOtherInputDao;

    protected RefMaterielTractionTopiaDao refMaterielTractionDao;
    protected RefMaterielOutilTopiaDao refMaterielOutilDao;

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;

    protected AbstractDomainInputStockUnitTopiaDao domainInputStockUnitDao;
    protected AbstractActionTopiaDao abstractActionDao;

    protected AbstractInputUsageTopiaDao abstractInputUsageDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;

    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;

    protected RefEspeceTopiaDao refEspeceDao;

    protected RefDestinationTopiaDao refDestinationDao;

    protected RefQualityCriteriaTopiaDao refQualityCriteriaDao;

    protected HarvestingActionValorisationTopiaDao harvestingActionValorisationDao;

    protected List<CroppingPlanEntry> croppingPlanEntries;

    protected List<GrowingSystem> growingSystems;


    protected static Map<AgrosystInterventionType, List<RefInterventionAgrosystTravailEDI>> GET_REF_INTERVENTION_TRAVAIL_EDI_BY_TYPE(List<RefInterventionAgrosystTravailEDI> refActionAgrosystTravailEDIs) {
        Map<AgrosystInterventionType, List<RefInterventionAgrosystTravailEDI>> result = new HashMap<>();
        if (refActionAgrosystTravailEDIs != null) {
            for (RefInterventionAgrosystTravailEDI refActionAgrosystTravailEDI : refActionAgrosystTravailEDIs) {
                AgrosystInterventionType type = refActionAgrosystTravailEDI.getIntervention_agrosyst();
                List<RefInterventionAgrosystTravailEDI> travail = result.computeIfAbsent(type, k -> Lists.newArrayList());
                travail.add(refActionAgrosystTravailEDI);
            }
        }
        return result;
    }

    @BeforeEach
    public void prepareTest() throws IOException {
        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.importActaTraitementsProduits();
        testDatas.createCroppingPlanEntry();
        testDatas.createTestGrowingSystems();
        ActionService actionService0 = serviceFactory.newService(ActionService.class);
        actionService = new ActionServiceImplMock((ActionServiceImpl) actionService0);
        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        domainService = serviceFactory.newService(DomainService.class);
        growingPlanService = serviceFactory.newService(GrowingPlanService.class);
        growingSystemService = serviceFactory.newService(GrowingSystemService.class);
        practicedSystemService = serviceFactory.newService(PracticedSystemService.class);
        importService = serviceFactory.newService(ImportService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        pricesService = serviceFactory.newService(PricesService.class);
        loginAsAdmin();
        alterSchema();

        refStadeEDITopiaDao = getPersistenceContext().getRefStadeEDIDao();
        refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        growingSystemTopiaDao = getPersistenceContext().getGrowingSystemDao();
        equipmentDao = getPersistenceContext().getEquipmentDao();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        practicedSystemTopiaDao = getPersistenceContext().getPracticedSystemDao();
        practicedSeasonalCropCycleTopiaDao = getPersistenceContext().getPracticedSeasonalCropCycleDao();
        practicedCropCycleConnectionTopiaDao = getPersistenceContext().getPracticedCropCycleConnectionDao();
        practicedInterventionTopiaDao = getPersistenceContext().getPracticedInterventionDao();
        practicedCropCycleNodeTopiaDao = getPersistenceContext().getPracticedCropCycleNodeDao();
        practicedPerennialCropCycleTopiaDao = getPersistenceContext().getPracticedPerennialCropCycleDao();
        practicedPlotDao = getPersistenceContext().getPracticedPlotDao();
        domainInputStockUnitDao = getPersistenceContext().getAbstractDomainInputStockUnitDao();
        abstractActionDao = getPersistenceContext().getAbstractActionDao();
        abstractInputUsageDao = getPersistenceContext().getAbstractInputUsageDao();
        practicedCropCycleConnectionDao = getPersistenceContext().getPracticedCropCycleConnectionDao();
        toolsCouplingDao = getPersistenceContext().getToolsCouplingDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        refEspeceDao = getPersistenceContext().getRefEspeceDao();
        refDestinationDao = getPersistenceContext().getRefDestinationDao();
        refQualityCriteriaDao = getPersistenceContext().getRefQualityCriteriaDao();
        harvestingActionValorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();
        practicedCropCycleSpeciesTopiaDao = getPersistenceContext().getPracticedCropCycleSpeciesDao();
        croppingPlanEntries = croppingPlanEntryDao.findAll();
        growingSystems = growingSystemTopiaDao.forActiveEquals(true).findAll();
        refFertiMinUNIFATopiaDao = getPersistenceContext().getRefFertiMinUNIFADao();
        refActaTraitementsProduitDao = getPersistenceContext().getRefActaTraitementsProduitDao();
        refOtherInputDao = getPersistenceContext().getRefOtherInputDao();
        refMaterielTractionDao = getPersistenceContext().getRefMaterielTractionDao();
        refMaterielOutilDao = getPersistenceContext().getRefMaterielOutilDao();

        practicedSystemServiceMock = new PracticedSystemServiceMock(practicedSystemService);

        // referentials required for tests
        testDatas.importStadesEDI();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importRefEspeces();
        testDatas.importFertiMinUnifa();
        testDatas.importDestination();
        testDatas.importQualityCriteria();
        testDatas.importRefSpeciesToSector();
        testDatas.importRefHarvestingPrices();

    }

    @Test
    public void testCreateSeasonnalCycle() {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-seasonnal";

        GrowingSystem gs = growingSystems.getFirst();

        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedCropCycleNodeDto practicedCropCycleNodeDto1 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto1.setX(0);
        practicedCropCycleNodeDto1.setY(0);
        practicedCropCycleNodeDto1.setNodeId(PracticedSystemService.NEW_NODE_PREFIX + UUID.randomUUID());
        practicedCropCycleNodeDto1.setCroppingPlanEntryCode(croppingPlanEntries.getFirst().getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto2 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto2.setX(1);
        practicedCropCycleNodeDto2.setY(0);
        practicedCropCycleNodeDto2.setNodeId(PracticedSystemService.NEW_NODE_PREFIX + UUID.randomUUID());
        practicedCropCycleNodeDto2.setCroppingPlanEntryCode(croppingPlanEntries.get(1).getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto3 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto3.setX(2);
        practicedCropCycleNodeDto3.setY(0);
        practicedCropCycleNodeDto3.setNodeId(PracticedSystemService.NEW_NODE_PREFIX + UUID.randomUUID());
        practicedCropCycleNodeDto3.setEndCycle(true);
        practicedCropCycleNodeDto3.setCroppingPlanEntryCode(croppingPlanEntries.get(2).getCode());

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto1 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto1.setSourceId(practicedCropCycleNodeDto1.getNodeId());
        practicedCropCycleConnectionDto1.setTargetId(practicedCropCycleNodeDto2.getNodeId());

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto2 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto2.setSourceId(practicedCropCycleNodeDto2.getNodeId());
        practicedCropCycleConnectionDto2.setTargetId(practicedCropCycleNodeDto3.getNodeId());

        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = Lists.newArrayList(practicedCropCycleNodeDto1, practicedCropCycleNodeDto2, practicedCropCycleNodeDto3);
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = Lists.newArrayList(practicedCropCycleConnectionDto1, practicedCropCycleConnectionDto2);

        PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto = new PracticedSeasonalCropCycleDto();
        practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(cropCycleConnectionDtos);
        practicedSeasonalCropCycleDto.setCropCycleNodeDtos(cropCycleNodeDtos);

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = Lists.newArrayList(practicedSeasonalCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // Creation Test
        PracticedSystem persistedPS = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, null, practicedSeasonalCropCycleDtos);

        List<PracticedSeasonalCropCycleDto> persistedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(persistedPS.getTopiaId());
        final PracticedSeasonalCropCycleDto seasonnalCropCycle = persistedSeasonalCropCycleDtos.getFirst();
        Assertions.assertNotNull(seasonnalCropCycle);
        List<PracticedCropCycleConnectionDto> persistedConnections = seasonnalCropCycle.getCropCycleConnectionDtos();

        List<PracticedCropCycleNodeDto> persistedNodesDtos = seasonnalCropCycle.getCropCycleNodeDtos();
        Map<String, PracticedCropCycleNodeDto> nodeByIds = persistedNodesDtos.stream().collect(Collectors.toMap(PracticedCropCycleNodeDto::getNodeId, Function.identity()));

        Collection<Pair<Integer, Integer>> connexionSourceTargets = new ArrayList<>();

        for (PracticedCropCycleConnectionDto persistedConnection : persistedConnections) {
            PracticedCropCycleNodeDto nodeS = nodeByIds.get(persistedConnection.getSourceId());
            PracticedCropCycleNodeDto nodeT = nodeByIds.get(persistedConnection.getTargetId());
            connexionSourceTargets.add(Pair.of(nodeS.getX(), nodeT.getX()));
        }

        Collection<Pair<Integer, Integer>> resultConnexionSourceTargets = new ArrayList<>();
        resultConnexionSourceTargets.add(Pair.of(0, 1));
        resultConnexionSourceTargets.add(Pair.of(1, 2));
        resultConnexionSourceTargets.add(Pair.of(2, 0));

        Assertions.assertTrue(connexionSourceTargets.containsAll(resultConnexionSourceTargets));
        Assertions.assertEquals(resultConnexionSourceTargets.size(), connexionSourceTargets.size());
    }

    @Test
    public void createOrUpdatePracticedSeasonalCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-seasonnal";

        GrowingSystem gs = growingSystems.getFirst();
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();

        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedCropCycleNodeDto practicedCropCycleNodeDto1 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto1.setX(1);
        practicedCropCycleNodeDto1.setY(1);
        practicedCropCycleNodeDto1.setInitNodeFrequency(50.0);
        practicedCropCycleNodeDto1.setNodeId("new-node-1");
        CroppingPlanEntry croppingPlanEntry = croppingPlanEntries.getFirst();
        practicedCropCycleNodeDto1.setCroppingPlanEntryCode(croppingPlanEntry.getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto2 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto2.setX(2);
        practicedCropCycleNodeDto2.setY(2);
        practicedCropCycleNodeDto2.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto2.setNodeId("new-node-2");
        practicedCropCycleNodeDto2.setCroppingPlanEntryCode(croppingPlanEntries.get(1).getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto3 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto3.setX(3);
        practicedCropCycleNodeDto3.setY(3);
        practicedCropCycleNodeDto3.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto3.setNodeId("new-node-3");
        practicedCropCycleNodeDto3.setCroppingPlanEntryCode(croppingPlanEntries.get(2).getCode());

        String interventionName = "intervention-test-0";

        // create the Actions
        List<CroppingPlanSpecies> species = croppingPlanEntry.getCroppingPlanSpecies();

        List<AbstractAction> actions = createActions(species);

        createInputs(actions, domain);

        Map<AgrosystInterventionType, AbstractAction> indexedActions = getAgrosystInterventionTypeAbstractActionMap(actions);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, croppingPlanEntry);

        List<PracticedInterventionDto> interventions = Lists.newArrayList(interventionDto);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto1 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto1.setSourceId("new-node-1");
        practicedCropCycleConnectionDto1.setTargetId("new-node-1");
        practicedCropCycleConnectionDto1.setCroppingPlanEntryFrequency(50.0);
        practicedCropCycleConnectionDto1.setInterventions(interventions);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto2 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto2.setSourceId("new-node-2");
        practicedCropCycleConnectionDto2.setTargetId("new-node-2");
        practicedCropCycleConnectionDto2.setCroppingPlanEntryFrequency(25.0);
        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto3 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto3.setSourceId("new-node-3");
        practicedCropCycleConnectionDto3.setTargetId("new-node-3");
        practicedCropCycleConnectionDto3.setCroppingPlanEntryFrequency(25.0);

        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = Lists.newArrayList(practicedCropCycleNodeDto1, practicedCropCycleNodeDto2, practicedCropCycleNodeDto3);
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = Lists.newArrayList(practicedCropCycleConnectionDto1, practicedCropCycleConnectionDto2, practicedCropCycleConnectionDto3);

        PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto = new PracticedSeasonalCropCycleDto();
        practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(cropCycleConnectionDtos);
        practicedSeasonalCropCycleDto.setCropCycleNodeDtos(cropCycleNodeDtos);

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = Lists.newArrayList(practicedSeasonalCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // Creation Test
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, null, practicedSeasonalCropCycleDtos);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        PracticedSystem persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        List<PracticedSeasonalCropCycle> practicedSeasonalCropCycles = practicedSeasonalCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertEquals(1L, practicedSeasonalCropCycles.size());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycles.getFirst();
        Assertions.assertNotNull(practicedSeasonalCropCycle);

        Collection<PracticedCropCycleNode> practicedCropCycleNodes = practicedSeasonalCropCycle.getCropCycleNodes();
        Assertions.assertEquals(3L, practicedCropCycleNodes.size());

        List<PracticedCropCycleConnection> practicedCropCycleConnections = practicedCropCycleConnectionTopiaDao.findAllByCropCycle(practicedSeasonalCropCycle.getTopiaId());
        Assertions.assertEquals(3L, practicedCropCycleConnections.size());

        List<PracticedIntervention> practicedInterventions = new ArrayList<>();
        for (PracticedCropCycleConnection practicedCropCycleConnection : practicedCropCycleConnections) {
            Assertions.assertNotNull(practicedCropCycleConnection.getSource());
            Assertions.assertNotNull(practicedCropCycleConnection.getTarget());
            List<PracticedIntervention> persistedInterventions = practicedInterventionTopiaDao.forPracticedCropCycleConnectionEquals(practicedCropCycleConnection).findAll();
            if (persistedInterventions != null) {
                practicedInterventions.addAll(persistedInterventions);
            }
        }
        Assertions.assertEquals(1L, practicedInterventions.size());

        PracticedIntervention practicedIntervention = practicedInterventions.getFirst();
        Assertions.assertEquals(interventionName, practicedIntervention.getName());

        List<AbstractAction> persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
        Assertions.assertEquals(3L, persistedActions.size());

        for (AbstractAction persistedAction : persistedActions) {
            if (persistedAction instanceof HarvestingAction) {
                Collection<HarvestingActionValorisation> valorisations = ((HarvestingAction) persistedAction).getValorisations();
                Assertions.assertTrue(CollectionUtils.isNotEmpty(valorisations));
                for (HarvestingActionValorisation valorisation : valorisations) {
                    Assertions.assertNotNull(valorisation.getQualityCriteria());
                }
            }
        }
        AbstractAction abstractAction = persistedActions.getFirst();

        int nbActionChecked = getNbValidPersistedActions(indexedActions, persistedActions);
        Assertions.assertEquals(3, nbActionChecked);

        // suppression test
        // The methode tested for suppression
        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, null, null);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        practicedSeasonalCropCycles = practicedSeasonalCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertTrue(practicedSeasonalCropCycles.isEmpty());

        Assertions.assertFalse(practicedInterventionTopiaDao.forTopiaIdEquals(practicedIntervention.getTopiaId()).exists());

        Assertions.assertFalse(abstractActionDao.forTopiaIdEquals(abstractAction.getTopiaId()).exists());

    }

    @Test
    public void createOrUpdatePracticedSeasonalCropCycleWithInvalidSpecies() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-seasonnal";

        GrowingSystem gs = growingSystems.getFirst();
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();

        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedCropCycleNodeDto practicedCropCycleNodeDto1 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto1.setX(1);
        practicedCropCycleNodeDto1.setY(1);
        practicedCropCycleNodeDto1.setInitNodeFrequency(50.0);
        practicedCropCycleNodeDto1.setNodeId("new-node-1");
        CroppingPlanEntry croppingPlanEntry = croppingPlanEntries.getFirst();
        practicedCropCycleNodeDto1.setCroppingPlanEntryCode(croppingPlanEntry.getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto2 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto2.setX(2);
        practicedCropCycleNodeDto2.setY(2);
        practicedCropCycleNodeDto2.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto2.setNodeId("new-node-2");
        practicedCropCycleNodeDto2.setCroppingPlanEntryCode(croppingPlanEntries.get(1).getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto3 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto3.setX(3);
        practicedCropCycleNodeDto3.setY(3);
        practicedCropCycleNodeDto3.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto3.setNodeId("new-node-3");
        practicedCropCycleNodeDto3.setCroppingPlanEntryCode(croppingPlanEntries.get(2).getCode());

        String interventionName = "intervention-test-0";

        // create the Actions
        List<CroppingPlanSpecies> species = croppingPlanEntry.getCroppingPlanSpecies();

        List<AbstractAction> actions = createActions(species);
        createInputs(actions, domain);

        Map<AgrosystInterventionType, AbstractAction> indexedActions = getAgrosystInterventionTypeAbstractActionMap(actions);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, croppingPlanEntry);

        // add invalid speciesStades
        SpeciesStadeDto unvalidSpeciesStadesDto = new SpeciesStadeDto();
        unvalidSpeciesStadesDto.setSpeciesCode("WRONG_SPECIES_STADES");
        interventionDto.getSpeciesStadesDtos().add(unvalidSpeciesStadesDto);

        List<PracticedInterventionDto> interventions = Lists.newArrayList(interventionDto);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto1 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto1.setSourceId("new-node-1");
        practicedCropCycleConnectionDto1.setTargetId("new-node-1");
        practicedCropCycleConnectionDto1.setCroppingPlanEntryFrequency(50.0);
        practicedCropCycleConnectionDto1.setInterventions(interventions);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto2 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto2.setSourceId("new-node-2");
        practicedCropCycleConnectionDto2.setTargetId("new-node-2");
        practicedCropCycleConnectionDto2.setCroppingPlanEntryFrequency(25.0);
        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto3 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto3.setSourceId("new-node-3");
        practicedCropCycleConnectionDto3.setTargetId("new-node-3");
        practicedCropCycleConnectionDto3.setCroppingPlanEntryFrequency(25.0);

        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = Lists.newArrayList(practicedCropCycleNodeDto1, practicedCropCycleNodeDto2, practicedCropCycleNodeDto3);
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = Lists.newArrayList(practicedCropCycleConnectionDto1, practicedCropCycleConnectionDto2, practicedCropCycleConnectionDto3);

        PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto = new PracticedSeasonalCropCycleDto();
        practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(cropCycleConnectionDtos);
        practicedSeasonalCropCycleDto.setCropCycleNodeDtos(cropCycleNodeDtos);

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = Lists.newArrayList(practicedSeasonalCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // Creation Test
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, null, practicedSeasonalCropCycleDtos);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        PracticedSystem persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        List<PracticedSeasonalCropCycle> practicedSeasonalCropCycles = practicedSeasonalCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertEquals(1L, practicedSeasonalCropCycles.size());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycles.getFirst();
        Assertions.assertNotNull(practicedSeasonalCropCycle);

        Collection<PracticedCropCycleNode> practicedCropCycleNodes = practicedSeasonalCropCycle.getCropCycleNodes();
        Assertions.assertEquals(3L, practicedCropCycleNodes.size());

        List<PracticedCropCycleConnection> practicedCropCycleConnections = practicedCropCycleConnectionTopiaDao.findAllByCropCycle(practicedSeasonalCropCycle.getTopiaId());
        Assertions.assertEquals(3L, practicedCropCycleConnections.size());

        List<PracticedIntervention> practicedInterventions = new ArrayList<>();
        for (PracticedCropCycleConnection practicedCropCycleConnection : practicedCropCycleConnections) {
            Assertions.assertNotNull(practicedCropCycleConnection.getSource());
            Assertions.assertNotNull(practicedCropCycleConnection.getTarget());
            List<PracticedIntervention> persitedInterverventions = practicedInterventionTopiaDao.forPracticedCropCycleConnectionEquals(practicedCropCycleConnection).findAll();
            if (persitedInterverventions != null) {
                practicedInterventions.addAll(persitedInterverventions);
            }
        }
        Assertions.assertEquals(1L, practicedInterventions.size());

        PracticedIntervention practicedIntervention = practicedInterventions.getFirst();
        Assertions.assertEquals(interventionName, practicedIntervention.getName());

        Collection<PracticedSpeciesStade> practicedSpeciesStades = practicedIntervention.getSpeciesStades();
        Assertions.assertFalse(practicedSpeciesStades.stream().anyMatch(pss -> pss.getSpeciesCode().contentEquals("WRONG_SPECIES_STADES")));

        List<AbstractAction> persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
        Assertions.assertEquals(3L, persistedActions.size());

        for (AbstractAction persistedAction : persistedActions) {
            if (persistedAction instanceof HarvestingAction) {
                Collection<HarvestingActionValorisation> valorisations = ((HarvestingAction) persistedAction).getValorisations();
                Assertions.assertTrue(CollectionUtils.isNotEmpty(valorisations));
                for (HarvestingActionValorisation valorisation : valorisations) {
                    Assertions.assertNotNull(valorisation.getQualityCriteria());
                }
            }
        }
        AbstractAction abstractAction = persistedActions.getFirst();

        int nbActionChecked = getNbValidPersistedActions(indexedActions, persistedActions);
        Assertions.assertEquals(3, nbActionChecked);

        // suppression test
        // The methode tested for suppression
        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, null, null);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        practicedSeasonalCropCycles = practicedSeasonalCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertTrue(practicedSeasonalCropCycles.isEmpty());

        Assertions.assertFalse(practicedInterventionTopiaDao.forTopiaIdEquals(practicedIntervention.getTopiaId()).exists());

        Assertions.assertFalse(abstractActionDao.forTopiaIdEquals(abstractAction.getTopiaId()).exists());

    }

    @Test
    public void removeInterventionFromPracticedSeasonalCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-seasonnal";

        GrowingSystem gs = growingSystems.getFirst();
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();

        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedCropCycleNodeDto practicedCropCycleNodeDto1 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto1.setX(1);
        practicedCropCycleNodeDto1.setY(1);
        practicedCropCycleNodeDto1.setInitNodeFrequency(50.0);
        practicedCropCycleNodeDto1.setNodeId("new-node-1");
        CroppingPlanEntry firstCroppingPlanEntry = croppingPlanEntries.getFirst();
        practicedCropCycleNodeDto1.setCroppingPlanEntryCode(firstCroppingPlanEntry.getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto2 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto2.setX(2);
        practicedCropCycleNodeDto2.setY(2);
        practicedCropCycleNodeDto2.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto2.setNodeId("new-node-2");
        practicedCropCycleNodeDto2.setCroppingPlanEntryCode(croppingPlanEntries.get(1).getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto3 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto3.setX(3);
        practicedCropCycleNodeDto3.setY(3);
        practicedCropCycleNodeDto3.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto3.setNodeId("new-node-3");
        practicedCropCycleNodeDto3.setCroppingPlanEntryCode(croppingPlanEntries.get(2).getCode());

        String interventionName = "intervention-test-0";

        // create the Actions
        List<CroppingPlanSpecies> species = firstCroppingPlanEntry.getCroppingPlanSpecies();

        List<AbstractAction> actions = createActions(species);
        createInputs(actions, domain);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCroppingPlanEntry);

        List<AbstractAction> actions1 = createActions(species);
        createInputs(actions1, domain);

        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionName + "_1", actions1, domainCode, 0, firstCroppingPlanEntry);

        List<PracticedInterventionDto> interventions = Lists.newArrayList(interventionDto, interventionDto1);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto1 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto1.setSourceId("new-node-1");
        practicedCropCycleConnectionDto1.setTargetId("new-node-1");
        practicedCropCycleConnectionDto1.setCroppingPlanEntryFrequency(50.0);
        practicedCropCycleConnectionDto1.setInterventions(interventions);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto2 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto2.setSourceId("new-node-2");
        practicedCropCycleConnectionDto2.setTargetId("new-node-2");
        practicedCropCycleConnectionDto2.setCroppingPlanEntryFrequency(25.0);
        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto3 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto3.setSourceId("new-node-3");
        practicedCropCycleConnectionDto3.setTargetId("new-node-3");
        practicedCropCycleConnectionDto3.setCroppingPlanEntryFrequency(25.0);

        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = Lists.newArrayList(practicedCropCycleNodeDto1, practicedCropCycleNodeDto2, practicedCropCycleNodeDto3);
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = Lists.newArrayList(practicedCropCycleConnectionDto1, practicedCropCycleConnectionDto2, practicedCropCycleConnectionDto3);

        PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto = new PracticedSeasonalCropCycleDto();
        practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(cropCycleConnectionDtos);
        practicedSeasonalCropCycleDto.setCropCycleNodeDtos(cropCycleNodeDtos);

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = Lists.newArrayList(practicedSeasonalCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // Creation Test
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, null, practicedSeasonalCropCycleDtos);

        PracticedSystem persistedPracticedSystem = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findUnique();

        practicedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(persistedPracticedSystem.getTopiaId());
        practicedSeasonalCropCycleDto = practicedSeasonalCropCycleDtos.getFirst();
        cropCycleConnectionDtos = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();
        PracticedCropCycleConnectionDto connectionDto = cropCycleConnectionDtos.getFirst();
        List<PracticedInterventionDto> interventionDtos = connectionDto.getInterventions();
        interventionDto1 = interventionDtos.remove(1);
        List<String> removedActionIds = interventionDto1.getActionDtos().stream().map(AbstractActionDto::getTopiaId).filter(Optional::isPresent).map(Optional::get).toList();
        Collection<String> removedInputIds = getActionDtosInputUsageIds(interventionDto1.getActionDtos());

        Assertions.assertTrue(abstractActionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertTrue(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());

        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, null, practicedSeasonalCropCycleDtos);

        Assertions.assertFalse(practicedInterventionTopiaDao.forTopiaIdEquals(interventionDto1.getTopiaId()).exists());

        Assertions.assertFalse(abstractActionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertFalse(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());
    }

    protected Collection<String> getActionDtosInputUsageIds(Collection<AbstractActionDto> abstractActionDtos) {
        Collection<String> removeInputUsageIds = new ArrayList<>();
        for (AbstractActionDto actionDto : abstractActionDtos) {
            final AgrosystInterventionType mainActionInterventionAgrosyst = actionDto.getMainActionInterventionAgrosyst();
            if (AgrosystInterventionType.RECOLTE.equals(mainActionInterventionAgrosyst)) {
                Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos = ((HarvestingActionDto) actionDto).getOtherProductInputUsageDtos();
                optionalOtherProductInputUsageDtos.ifPresent(
                        otherProductInputUsageDtos -> removeInputUsageIds.addAll(
                                otherProductInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

            } else if (AgrosystInterventionType.LUTTE_BIOLOGIQUE.equals(mainActionInterventionAgrosyst)) {
                Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos = ((BiologicalControlActionDto) actionDto).getOtherProductInputUsageDtos();
                optionalOtherProductInputUsageDtos.ifPresent(
                        otherProductInputUsageDtos -> removeInputUsageIds.addAll(
                                otherProductInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

                Optional<Collection<PhytoProductInputUsageDto>> optionalPhytoProductInputUsageDtos = ((BiologicalControlActionDto) actionDto).getPhytoProductInputUsageDtos();
                optionalPhytoProductInputUsageDtos.ifPresent(
                        otherInputUsageDtos -> removeInputUsageIds.addAll(
                                otherInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

            } else if (AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.equals(mainActionInterventionAgrosyst)) {
                Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos = ((MineralFertilizersSpreadingActionDto) actionDto).getOtherProductInputUsageDtos();
                optionalOtherProductInputUsageDtos.ifPresent(
                        otherProductInputUsageDtos -> removeInputUsageIds.addAll(
                                otherProductInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

                Optional<Collection<MineralProductInputUsageDto>> productInputUsageDtos = ((MineralFertilizersSpreadingActionDto) actionDto).getMineralProductInputUsageDtos();
                productInputUsageDtos.ifPresent(
                        productInputUsageDtos1 -> removeInputUsageIds.addAll(
                                productInputUsageDtos1.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));
            }
        }
        return removeInputUsageIds;
    }


    @Test
    public void removeActionsFromPracticedSeasonalCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-seasonnal";

        GrowingSystem gs = growingSystems.getFirst();
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();

        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedCropCycleNodeDto practicedCropCycleNodeDto1 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto1.setX(1);
        practicedCropCycleNodeDto1.setY(1);
        practicedCropCycleNodeDto1.setInitNodeFrequency(50.0);
        practicedCropCycleNodeDto1.setNodeId("new-node-1");
        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        practicedCropCycleNodeDto1.setCroppingPlanEntryCode(firstCrop.getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto2 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto2.setX(2);
        practicedCropCycleNodeDto2.setY(2);
        practicedCropCycleNodeDto2.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto2.setNodeId("new-node-2");
        practicedCropCycleNodeDto2.setCroppingPlanEntryCode(croppingPlanEntries.get(1).getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto3 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto3.setX(3);
        practicedCropCycleNodeDto3.setY(3);
        practicedCropCycleNodeDto3.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto3.setNodeId("new-node-3");
        practicedCropCycleNodeDto3.setCroppingPlanEntryCode(croppingPlanEntries.get(2).getCode());

        String interventionName = "intervention-test-0";

        // create the Actions
        List<CroppingPlanSpecies> species = firstCrop.getCroppingPlanSpecies();

        List<AbstractAction> actions = createActions(species);
        createInputs(actions, domain);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCrop);

        List<AbstractAction> actions1 = createActions(species);
        createInputs(actions1, domain);

        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionName + "_1", actions1, domainCode, 0, firstCrop);

        List<PracticedInterventionDto> interventions = Lists.newArrayList(interventionDto, interventionDto1);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto1 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto1.setSourceId("new-node-1");
        practicedCropCycleConnectionDto1.setTargetId("new-node-1");
        practicedCropCycleConnectionDto1.setCroppingPlanEntryFrequency(50.0);
        practicedCropCycleConnectionDto1.setInterventions(interventions);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto2 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto2.setSourceId("new-node-2");
        practicedCropCycleConnectionDto2.setTargetId("new-node-2");
        practicedCropCycleConnectionDto2.setCroppingPlanEntryFrequency(25.0);
        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto3 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto3.setSourceId("new-node-3");
        practicedCropCycleConnectionDto3.setTargetId("new-node-3");
        practicedCropCycleConnectionDto3.setCroppingPlanEntryFrequency(25.0);

        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = Lists.newArrayList(practicedCropCycleNodeDto1, practicedCropCycleNodeDto2, practicedCropCycleNodeDto3);
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = Lists.newArrayList(practicedCropCycleConnectionDto1, practicedCropCycleConnectionDto2, practicedCropCycleConnectionDto3);

        PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto = new PracticedSeasonalCropCycleDto();
        practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(cropCycleConnectionDtos);
        practicedSeasonalCropCycleDto.setCropCycleNodeDtos(cropCycleNodeDtos);

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = Lists.newArrayList(practicedSeasonalCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // Creation Test
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, null, practicedSeasonalCropCycleDtos);

        PracticedSystem persistedPracticedSystem = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findUnique();

        practicedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(persistedPracticedSystem.getTopiaId());
        practicedSeasonalCropCycleDto = practicedSeasonalCropCycleDtos.getFirst();
        cropCycleConnectionDtos = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();
        PracticedCropCycleConnectionDto connectionDto = cropCycleConnectionDtos.getFirst();
        List<PracticedInterventionDto> interventionDtos = connectionDto.getInterventions();
        interventionDto1 = interventionDtos.get(1);

        Collection<AbstractActionDto> actionDtos = interventionDto1.getActionDtos();
        Collection<String> removedInputIds = new ArrayList<>();
        final String[] removeActionId = new String[1];

        actionDtos.stream().filter(aDto -> AgrosystInterventionType.LUTTE_BIOLOGIQUE.equals(aDto.getMainActionInterventionAgrosyst())).forEach(aDto -> {
            final BiologicalControlActionDto aDto1 = (BiologicalControlActionDto) aDto;
            removeActionId[0] = aDto1.getTopiaId().orElse(null);
            if (aDto1.getOtherProductInputUsageDtos().isPresent()) {
                removedInputIds.addAll(aDto1.getOtherProductInputUsageDtos().get()
                        .stream().filter(iu -> iu.getInputUsageTopiaId().isPresent())
                        .map(opiu -> opiu.getInputUsageTopiaId().get()).toList());
            }
            if (aDto1.getPhytoProductInputUsageDtos().isPresent()) {
                removedInputIds.addAll(aDto1.getPhytoProductInputUsageDtos().get()
                        .stream().filter(iu -> iu.getInputUsageTopiaId().isPresent())
                        .map(opiu -> opiu.getInputUsageTopiaId().get()).toList());
            }
        });
        Collection<AbstractActionDto> newActionDtos = actionDtos.stream().filter(aDto -> !AgrosystInterventionType.LUTTE_BIOLOGIQUE.equals(aDto.getMainActionInterventionAgrosyst())).toList();
        interventionDto1.setActionDtos(newActionDtos);

        Assertions.assertTrue(abstractActionDao.forTopiaIdIn(Arrays.stream(removeActionId).toList()).exists());
        Assertions.assertTrue(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());

        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, null, practicedSeasonalCropCycleDtos);

        Assertions.assertTrue(practicedInterventionTopiaDao.forTopiaIdEquals(interventionDto1.getTopiaId()).exists());

        Assertions.assertFalse(abstractActionDao.forTopiaIdIn(Arrays.stream(removeActionId).toList()).exists());
        Assertions.assertFalse(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());
    }

    @Test
    public void removeInputsFromPracticedSeasonalCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-seasonnal";

        GrowingSystem gs = growingSystems.getFirst();
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();

        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedCropCycleNodeDto practicedCropCycleNodeDto1 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto1.setX(1);
        practicedCropCycleNodeDto1.setY(1);
        practicedCropCycleNodeDto1.setInitNodeFrequency(50.0);
        practicedCropCycleNodeDto1.setNodeId("new-node-1");
        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        practicedCropCycleNodeDto1.setCroppingPlanEntryCode(firstCrop.getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto2 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto2.setX(2);
        practicedCropCycleNodeDto2.setY(2);
        practicedCropCycleNodeDto2.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto2.setNodeId("new-node-2");
        practicedCropCycleNodeDto2.setCroppingPlanEntryCode(croppingPlanEntries.get(1).getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto3 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto3.setX(3);
        practicedCropCycleNodeDto3.setY(3);
        practicedCropCycleNodeDto3.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto3.setNodeId("new-node-3");
        practicedCropCycleNodeDto3.setCroppingPlanEntryCode(croppingPlanEntries.get(2).getCode());

        String interventionName = "intervention-test-0";

        // create the Actions
        List<CroppingPlanSpecies> species = firstCrop.getCroppingPlanSpecies();

        List<AbstractAction> actions = createActions(species);
        createInputs(actions, domain);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCrop);

        List<AbstractAction> actions1 = createActions(species);
        createInputs(actions1, domain);

        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionName + "_1", actions1, domainCode, 0, firstCrop);

        List<PracticedInterventionDto> interventions = Lists.newArrayList(interventionDto, interventionDto1);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto1 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto1.setSourceId("new-node-1");
        practicedCropCycleConnectionDto1.setTargetId("new-node-1");
        practicedCropCycleConnectionDto1.setCroppingPlanEntryFrequency(50.0);
        practicedCropCycleConnectionDto1.setInterventions(interventions);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto2 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto2.setSourceId("new-node-2");
        practicedCropCycleConnectionDto2.setTargetId("new-node-2");
        practicedCropCycleConnectionDto2.setCroppingPlanEntryFrequency(25.0);
        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto3 = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto3.setSourceId("new-node-3");
        practicedCropCycleConnectionDto3.setTargetId("new-node-3");
        practicedCropCycleConnectionDto3.setCroppingPlanEntryFrequency(25.0);

        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = Lists.newArrayList(practicedCropCycleNodeDto1, practicedCropCycleNodeDto2, practicedCropCycleNodeDto3);
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = Lists.newArrayList(practicedCropCycleConnectionDto1, practicedCropCycleConnectionDto2, practicedCropCycleConnectionDto3);

        PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto = new PracticedSeasonalCropCycleDto();
        practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(cropCycleConnectionDtos);
        practicedSeasonalCropCycleDto.setCropCycleNodeDtos(cropCycleNodeDtos);

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = Lists.newArrayList(practicedSeasonalCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // Creation Test
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, null, practicedSeasonalCropCycleDtos);

        PracticedSystem persistedPracticedSystem = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findUnique();

        practicedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(persistedPracticedSystem.getTopiaId());
        practicedSeasonalCropCycleDto = practicedSeasonalCropCycleDtos.getFirst();
        cropCycleConnectionDtos = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();
        PracticedCropCycleConnectionDto connectionDto = cropCycleConnectionDtos.getFirst();
        List<PracticedInterventionDto> interventionDtos = connectionDto.getInterventions();
        interventionDto1 = interventionDtos.get(1);
        Collection<String> removedInputIds = new ArrayList<>();
        final Collection<AbstractActionDto> abstractActionDtos = removeUsageFromAbstractActionDtos(removedInputIds, interventionDto1.getActionDtos(), null);
        interventionDto1.setActionDtos(abstractActionDtos);

        Assertions.assertTrue(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());

        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, null, practicedSeasonalCropCycleDtos);

        Assertions.assertTrue(practicedInterventionTopiaDao.forTopiaIdEquals(interventionDto1.getTopiaId()).exists());

        Assertions.assertFalse(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());
    }

    @Test
    public void testInterventionRankOrderOnConnection() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-seasonnal";

        GrowingSystem gs = growingSystems.getFirst();
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();

        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        String cropCode = firstCrop.getCode();

        PracticedCropCycleNodeDto practicedCropCycleNodeDto1 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto1.setX(1);
        practicedCropCycleNodeDto1.setY(1);
        practicedCropCycleNodeDto1.setInitNodeFrequency(50.0);
        practicedCropCycleNodeDto1.setNodeId("new-node-1");
        practicedCropCycleNodeDto1.setCroppingPlanEntryCode(cropCode);

        PracticedCropCycleNodeDto practicedCropCycleNodeDto2 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto2.setX(2);
        practicedCropCycleNodeDto2.setY(2);
        practicedCropCycleNodeDto2.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto2.setNodeId("new-node-2");
        practicedCropCycleNodeDto2.setCroppingPlanEntryCode(croppingPlanEntries.get(1).getCode());

        PracticedCropCycleNodeDto practicedCropCycleNodeDto3 = new PracticedCropCycleNodeDto();
        practicedCropCycleNodeDto3.setX(3);
        practicedCropCycleNodeDto3.setY(3);
        practicedCropCycleNodeDto3.setInitNodeFrequency(25.0);
        practicedCropCycleNodeDto3.setNodeId("new-node-3");
        practicedCropCycleNodeDto3.setCroppingPlanEntryCode(croppingPlanEntries.get(2).getCode());

        String[] interventionName = {"intervention-test-1", "intervention-test-2", "intervention-test-3"};

        List<CroppingPlanSpecies> species = firstCrop.getCroppingPlanSpecies();

        // create the Actions
        List<AbstractAction> actions = createActions(species);
        createInputs(actions, domain);

        getPersistenceContext().commit();

        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionName[0], actions, domainCode, 0, firstCrop);

        actions = createActions(species);
        createInputs(actions, domain);
        PracticedInterventionDto interventionDto2 = createInterventionDto(interventionName[1], actions, domainCode, 2, firstCrop);

        actions = createActions(species);
        createInputs(actions, domain);
        PracticedInterventionDto interventionDto3 = createInterventionDto(interventionName[2], actions, domainCode, 4, firstCrop);

        List<PracticedInterventionDto> interventions = Lists.newArrayList(interventionDto1, interventionDto2, interventionDto3);

        PracticedCropCycleConnectionDto practicedCropCycleConnectionDto = new PracticedCropCycleConnectionDto();
        practicedCropCycleConnectionDto.setSourceId("new-node-1");
        practicedCropCycleConnectionDto.setTargetId("new-node-1");
        practicedCropCycleConnectionDto.setCroppingPlanEntryFrequency(50.0);
        practicedCropCycleConnectionDto.setInterventions(interventions);


        List<PracticedCropCycleNodeDto> cropCycleNodeDtos = Lists.newArrayList(practicedCropCycleNodeDto1, practicedCropCycleNodeDto2, practicedCropCycleNodeDto3);
        List<PracticedCropCycleConnectionDto> cropCycleConnectionDtos = Lists.newArrayList(practicedCropCycleConnectionDto);

        PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto = new PracticedSeasonalCropCycleDto();
        practicedSeasonalCropCycleDto.setCropCycleConnectionDtos(cropCycleConnectionDtos);
        practicedSeasonalCropCycleDto.setCropCycleNodeDtos(cropCycleNodeDtos);

        List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos = Lists.newArrayList(practicedSeasonalCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // Creation Test
        PracticedSystem persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, null, practicedSeasonalCropCycleDtos);

        practicedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(persistedPracticedSystem.getTopiaId());
        practicedSeasonalCropCycleDto = practicedSeasonalCropCycleDtos.getFirst();
        cropCycleConnectionDtos = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();
        PracticedCropCycleConnectionDto connectionDto = cropCycleConnectionDtos.getFirst();
        List<PracticedInterventionDto> interventionDtos = connectionDto.getInterventions();

        Assertions.assertEquals(3L, interventionDtos.size());
        for (int i = 0; i < 3; i++) {
            Assertions.assertEquals(interventionDtos.get(i).getName(), interventionName[i]);
            Assertions.assertEquals(i, interventionDtos.get(i).getRank());
        }

        // there are 3
        List<PracticedInterventionDto> reorderedInterventions = Lists.newArrayList(interventionDtos.get(1), interventionDtos.get(0), interventionDtos.get(2));
        String[] interventionName2 = {interventionDtos.get(1).getName(), interventionDtos.getFirst().getName(), interventionDtos.get(2).getName()};
        connectionDto.setInterventions(reorderedInterventions);

        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(
                persistedPracticedSystem, null, practicedSeasonalCropCycleDtos);

        practicedSeasonalCropCycleDtos = practicedSystemService.getAllPracticedSeasonalCropCycles(persistedPracticedSystem.getTopiaId());
        practicedSeasonalCropCycleDto = practicedSeasonalCropCycleDtos.getFirst();
        cropCycleConnectionDtos = practicedSeasonalCropCycleDto.getCropCycleConnectionDtos();
        connectionDto = cropCycleConnectionDtos.getFirst();
        interventionDtos = connectionDto.getInterventions();

        Assertions.assertEquals(3L, interventionDtos.size());
        for (int i = 0; i < 3; i++) {
            Assertions.assertEquals(interventionDtos.get(i).getName(), interventionName2[i]);
            Assertions.assertEquals(i, interventionDtos.get(i).getRank());
        }
    }

    protected Map<AgrosystInterventionType, AbstractAction> getAgrosystInterventionTypeAbstractActionMap(List<AbstractAction> actions) {
        Map<AgrosystInterventionType, AbstractAction> indexedActions = Maps.newHashMapWithExpectedSize(actions.size());
        for (AbstractAction action : actions) {
            indexedActions.put(action.getMainAction().getIntervention_agrosyst(), action);
        }
        return indexedActions;
    }

    /**
     * action 0: HarvestingAction
     * action 1: MineralFertilizersSpreadingAction
     *
     * @return La liste des actions
     */
    protected List<AbstractAction> createActions(List<CroppingPlanSpecies> croppingPlanSpecies) {
        List<RefInterventionAgrosystTravailEDI> refActionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.forActiveEquals(true).findAll();
        Map<AgrosystInterventionType, List<RefInterventionAgrosystTravailEDI>> travailByType = GET_REF_INTERVENTION_TRAVAIL_EDI_BY_TYPE(refActionAgrosystTravailEDIs);

        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();

        List<RefInterventionAgrosystTravailEDI> travailEDIs = travailByType.get(AgrosystInterventionType.RECOLTE);
        HarvestingAction harvestingAction = new HarvestingActionImpl();
        harvestingAction.setTopiaId(ActionService.NEW_ACTION_PREFIX + UUID.randomUUID());
        harvestingAction.setMainAction(travailEDIs.get(0));

        double globalYealdAverage = 60.0;

        Collection<HarvestingActionValorisation> valorisations = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(croppingPlanSpecies)) {
            double speciesYealAverage = globalYealdAverage / croppingPlanSpecies.size();
            for (CroppingPlanSpecies species : croppingPlanSpecies) {
                String codeEspeceBotanique = species.getSpecies().getCode_espece_botanique();
                String codeQalifiantAee = species.getSpecies().getCode_qualifiant_AEE();
                List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE = new ArrayList<>();

                codeEspeceBotaniquesCodeQualifiantAEE.add(Pair.of(codeEspeceBotanique, codeQalifiantAee));

                Map<Pair<String, String>, List<Sector>> sectors = refSpeciesToSectorDao.loadSectorsByCodeEspeceBotaniqueCodeQualifiant(codeEspeceBotaniquesCodeQualifiantAEE);
                List<Sector> sectorsForSpecies = sectors.get(Pair.of(codeEspeceBotanique, codeQalifiantAee));

                if (CollectionUtils.isEmpty(sectorsForSpecies)) {
                    refSpeciesToSectorDao.createByNaturalId(codeEspeceBotanique, Strings.emptyToNull(codeQalifiantAee), Sector.MARAICHAGE);
                    refSpeciesToSectorDao.createByNaturalId(codeEspeceBotanique, Strings.emptyToNull(codeQalifiantAee), Sector.GRANDES_CULTURES);
                    refSpeciesToSectorDao.createByNaturalId(codeEspeceBotanique, Strings.emptyToNull(codeQalifiantAee), Sector.POLYCULTURE_ELEVAGE);
                    refSpeciesToSectorDao.createByNaturalId(codeEspeceBotanique, Strings.emptyToNull(codeQalifiantAee), Sector.VITICULTURE);
                    sectorsForSpecies = Lists.newArrayList(Sector.MARAICHAGE, Sector.GRANDES_CULTURES, Sector.POLYCULTURE_ELEVAGE, Sector.VITICULTURE);
                }

                List<RefDestination> destinations = refDestinationDao.forProperties(RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, codeEspeceBotanique,
                        RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, null,
                        RefDestination.PROPERTY_SECTOR, sectorsForSpecies.get(0)).findAll();

                if (destinations.size() < 2) {
                    RefDestination d0 = testDatas.createRefDestination(
                            species.getSpecies(), "valide 1", sectorsForSpecies.get(0), YealdUnit.HL_HA, null, null);
                    RefDestination d1 = testDatas.createRefDestination(
                            species.getSpecies(), "valide 2", sectorsForSpecies.get(0), YealdUnit.HL_HA, null, null);
                    destinations = Lists.newArrayList(d0, d1);
                }

                RefDestination d0 = destinations.getFirst();
                HarvestingActionValorisation speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(d0);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSalesPercent(50);
                speciesValorisation.setNoValorisationPercent(25);
                speciesValorisation.setSelfConsumedPersent(25);

                List<QualityCriteria> allSpeciesQualityCriteria = getQualityCriterias(d0);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);

                valorisations.add(speciesValorisation);

                RefDestination d1 = destinations.get(1);
                speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(d1);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSalesPercent(25);
                speciesValorisation.setNoValorisationPercent(25);
                speciesValorisation.setSelfConsumedPersent(50);

                allSpeciesQualityCriteria = getQualityCriterias(d1);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);

                valorisations.add(speciesValorisation);
            }
        }

        harvestingAction.setValorisations(valorisations);

        travailEDIs = travailByType.get(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = new MineralFertilizersSpreadingActionImpl();
        mineralFertilizersSpreadingAction.setTopiaId(ActionService.NEW_ACTION_PREFIX + UUID.randomUUID());
        mineralFertilizersSpreadingAction.setBurial(true);
        mineralFertilizersSpreadingAction.setLocalizedSpreading(true);
        mineralFertilizersSpreadingAction.setMainAction(travailEDIs.get(0));

        travailEDIs = travailByType.get(AgrosystInterventionType.LUTTE_BIOLOGIQUE);
        BiologicalControlAction biologicalControlAction = new BiologicalControlActionImpl();
        biologicalControlAction.setTopiaId(ActionService.NEW_ACTION_PREFIX + UUID.randomUUID());
        biologicalControlAction.setBoiledQuantity(12.5);
        biologicalControlAction.setProportionOfTreatedSurface(5.6);
        biologicalControlAction.setMainAction(travailEDIs.get(0));

        return Lists.newArrayList(harvestingAction, mineralFertilizersSpreadingAction, biologicalControlAction);
    }

    private List<QualityCriteria> getQualityCriterias(RefDestination destination) {
        RefQualityCriteria qc0 = testDatas.createRefQualityCriteria(null, "Valide 0", destination.getSector(), destination.getWineValorisation());
        QualityCriteria qualityCriteria = new QualityCriteriaImpl();
        qualityCriteria.setQuantitativeValue(20.3);
        qualityCriteria.setRefQualityCriteria(qc0);
        return Lists.newArrayList(qualityCriteria);
    }

    private List<QualityCriteria> getQualityCriterias(RefQualityCriteria refQualityCriteria) {
        QualityCriteria qualityCriteria = new QualityCriteriaImpl();
        qualityCriteria.setQuantitativeValue(20.3);
        qualityCriteria.setRefQualityCriteria(refQualityCriteria);
        return Lists.newArrayList(qualityCriteria);
    }

    protected void createInputs(List<AbstractAction> actions, Domain domain) throws IOException {
        Map<InputType, List<AbstractDomainInputStockUnit>> inputTypeListMap = domainInputStockUnitService.loadDomainInputStock(domain);

        for (AbstractAction action : actions) {
            if (action instanceof MineralFertilizersSpreadingAction mfsa) {

                if (refFertiMinUNIFATopiaDao.count() == 0) {
                    testDatas.importFertiMinUnifa();
                }
                RefFertiMinUNIFA refInput = refFertiMinUNIFATopiaDao.findAll().getFirst();

                List<AbstractDomainInputStockUnit> abstractDomainInputStockUnits = new ArrayList<>(CollectionUtils.emptyIfNull(inputTypeListMap.get(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX)));
                if (CollectionUtils.isEmpty(abstractDomainInputStockUnits)) {
                    DomainMineralProductInputTopiaDao domainMineralProductInputDao = getPersistenceContext().getDomainMineralProductInputDao();
                    DomainMineralProductInput domainMineralProductInput = domainMineralProductInputDao.newInstance();
                    domainMineralProductInput.setRefInput(refInput);
                    domainMineralProductInput.setInputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
                    domainMineralProductInput.setDomain(domain);
                    domainMineralProductInput.setUsageUnit(MineralProductUnit.KG_HA);
                    domainMineralProductInput.setCode(UUID.randomUUID().toString());
                    domainMineralProductInput.setPhytoEffect(true);
                    domainMineralProductInput.setInputKey(DomainInputStockUnitService.getMineralInputKey(
                            refInput, domainMineralProductInput.isPhytoEffect(), MineralProductUnit.KG_HA));
                    domainMineralProductInput.setInputName("APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX");
                    domainMineralProductInputDao.create(domainMineralProductInput);
                    abstractDomainInputStockUnits.add(domainMineralProductInput);
                }
                for (AbstractDomainInputStockUnit abstractDomainInputStockUnit : abstractDomainInputStockUnits) {
                    MineralProductInputUsage mineralProductInputUsage = new MineralProductInputUsageImpl();
                    mineralProductInputUsage.setDomainMineralProductInput((DomainMineralProductInput) abstractDomainInputStockUnit);
                    mineralProductInputUsage.setInputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
                    mineralProductInputUsage.setQtAvg(12.0 + abstractDomainInputStockUnits.indexOf(abstractDomainInputStockUnit));
                    mfsa.addMineralProductInputUsages(mineralProductInputUsage);
                }

            } else if (action instanceof BiologicalControlAction bca) {
                if (refActaTraitementsProduitDao.count() == 0) {
                    testDatas.importActaTraitementsProduits();
                }
                RefActaTraitementsProduit phytoProduct = refActaTraitementsProduitDao.findAll().getFirst();

                List<AbstractDomainInputStockUnit> biologicalInputStockUnits = new ArrayList<>(CollectionUtils.emptyIfNull(inputTypeListMap.get(InputType.LUTTE_BIOLOGIQUE)));
                if (CollectionUtils.isEmpty(biologicalInputStockUnits)) {
                    DomainPhytoProductInputTopiaDao domainPhytoProductInputDao = getPersistenceContext().getDomainPhytoProductInputDao();
                    DomainPhytoProductInput domainPhytoProductInput = domainPhytoProductInputDao.newInstance();
                    domainPhytoProductInput.setDomain(domain);
                    domainPhytoProductInput.setRefInput(phytoProduct);
                    domainPhytoProductInput.setUsageUnit(PhytoProductUnit.KG_HA);
                    domainPhytoProductInput.setCode(UUID.randomUUID().toString());
                    domainPhytoProductInput.setInputKey(DomainInputStockUnitService.getPhytoInputKey(
                            phytoProduct, PhytoProductUnit.KG_HA));
                    domainPhytoProductInput.setInputName("LUTTE_BIOLOGIQUE");
                    domainPhytoProductInput.setInputType(InputType.LUTTE_BIOLOGIQUE);
                    domainPhytoProductInput.setProductType(ProductType.ADJUVANTS);
                    domainPhytoProductInputDao.create(domainPhytoProductInput);
                    biologicalInputStockUnits.add(domainPhytoProductInput);
                }
                for (AbstractDomainInputStockUnit biologicalInputStockUnit : biologicalInputStockUnits) {
                    BiologicalProductInputUsage biologicalProductInputUsage = new BiologicalProductInputUsageImpl();
                    biologicalProductInputUsage.setDomainPhytoProductInput((DomainPhytoProductInput) biologicalInputStockUnit);
                    biologicalProductInputUsage.setInputType(InputType.LUTTE_BIOLOGIQUE);
                    biologicalProductInputUsage.setQtAvg(2.1 + biologicalInputStockUnits.indexOf(biologicalInputStockUnit));
                    bca.addBiologicalProductInputUsages(biologicalProductInputUsage);
                }

                List<AbstractDomainInputStockUnit> otherDomainInputStockUnits = new ArrayList<>(CollectionUtils.emptyIfNull(inputTypeListMap.get(InputType.AUTRE)));
                if (CollectionUtils.isEmpty(otherDomainInputStockUnits)) {
                    if (refOtherInputDao.count() == 0) {
                        testDatas.importRefOtherInput();
                    }
                    RefOtherInput refOtherInput = refOtherInputDao.findAll().getFirst();

                    DomainOtherInputTopiaDao domainOtherInputDao = getPersistenceContext().getDomainOtherInputDao();
                    DomainOtherInput domainOtherInput = domainOtherInputDao.newInstance();
                    domainOtherInput.setDomain(domain);
                    domainOtherInput.setInputType(InputType.AUTRE);
                    domainOtherInput.setRefInput(refOtherInput);
                    domainOtherInput.setCode(UUID.randomUUID().toString());
                    domainOtherInput.setUsageUnit(OtherProductInputUnit.KG_HA);
                    domainOtherInput.setInputKey(DomainInputStockUnitService.getOtherInputKey(refOtherInput, OtherProductInputUnit.KG_HA));
                    domainOtherInput.setInputName("Produit pour traitmement biologique");
                    domainOtherInputDao.create(domainOtherInput);
                    otherDomainInputStockUnits.add(domainOtherInput);
                }
                for (AbstractDomainInputStockUnit otherDomainInputStockUnit : otherDomainInputStockUnits) {
                    OtherProductInputUsage otherProductInputUsage = new OtherProductInputUsageImpl();
                    otherProductInputUsage.setDomainOtherInput((DomainOtherInput) otherDomainInputStockUnit);
                    otherProductInputUsage.setInputType(InputType.AUTRE);
                    otherProductInputUsage.setQtAvg(2.1 + otherDomainInputStockUnits.indexOf(otherDomainInputStockUnit));
                    bca.addOtherProductInputUsages(otherProductInputUsage);
                }
            } else if (action instanceof HarvestingAction ha) {
                List<AbstractDomainInputStockUnit> abstractDomainInputStockUnits = new ArrayList<>(CollectionUtils.emptyIfNull(inputTypeListMap.get(InputType.AUTRE)));
                if (CollectionUtils.isEmpty(abstractDomainInputStockUnits)) {
                    if (refOtherInputDao.count() == 0) {
                        testDatas.importRefOtherInput();
                    }
                    RefOtherInput refOtherInput = refOtherInputDao.findAll().getFirst();

                    DomainOtherInputTopiaDao domainOtherInputDao = getPersistenceContext().getDomainOtherInputDao();
                    DomainOtherInput domainOtherInput = domainOtherInputDao.newInstance();
                    domainOtherInput.setDomain(domain);
                    domainOtherInput.setInputType(InputType.AUTRE);
                    domainOtherInput.setRefInput(refOtherInput);
                    domainOtherInput.setCode(UUID.randomUUID().toString());
                    domainOtherInput.setUsageUnit(OtherProductInputUnit.KG_HA);
                    domainOtherInput.setInputKey(DomainInputStockUnitService.getOtherInputKey(refOtherInput, OtherProductInputUnit.KG_HA));
                    domainOtherInput.setInputName("Autre");
                    domainOtherInputDao.create(domainOtherInput);
                    abstractDomainInputStockUnits.add(domainOtherInput);
                }
                for (AbstractDomainInputStockUnit abstractDomainInputStockUnit : abstractDomainInputStockUnits) {
                    OtherProductInputUsage otherProductInputUsage = new OtherProductInputUsageImpl();
                    otherProductInputUsage.setDomainOtherInput((DomainOtherInput) abstractDomainInputStockUnit);
                    otherProductInputUsage.setInputType(InputType.AUTRE);
                    otherProductInputUsage.setQtAvg(2.1 + abstractDomainInputStockUnits.indexOf(abstractDomainInputStockUnit));
                    ha.addOtherProductInputUsages(otherProductInputUsage);
                }
            }
        }
    }

    @Deprecated
    protected String changeValorisationObjectId(HarvestingActionValorisation valorisation, AbstractAction action) {
        // also replace Price's objectId
        // objectid is json value for:
        //  0: actionId,
        //  1: SpeciesCode,
        //  2: ValorisationId

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        String actionId = action.getTopiaId();
        String speciesCode = valorisation.getSpeciesCode();
        String valorisationId = valorisation.getTopiaId();

        List<String> objectIdList = Lists.newArrayList(actionId, speciesCode, valorisationId);


        return gson.toJson(objectIdList);
    }

    protected List<HarvestingPrice> createHarvestingPrices(
            Domain domain,
            PracticedSystem practicedSystem,
            List<AbstractAction> actions,
            List<CroppingPlanSpecies> croppingPlanSpecies) {
        List<HarvestingPrice> prices = new ArrayList<>();
        Gson gson = new AgrosystGsonSupplier().get();
        for (AbstractAction action : actions) {
            if (action instanceof HarvestingAction harvestingAction) {
                Collection<HarvestingActionValorisation> valorisations = harvestingAction.getValorisations();
                final Map<String, CroppingPlanSpecies> croppingPlanSpeciesByCode = croppingPlanSpecies.stream().collect(
                        Collectors.toMap(CroppingPlanSpecies::getCode, Function.identity()));
                if (valorisations != null) {
                    for (HarvestingActionValorisation valorisation : valorisations) {
                        valorisation.getSpeciesCode();
                        final CroppingPlanSpecies croppingPlanSpecies1 = croppingPlanSpeciesByCode.get(valorisation.getSpeciesCode());
                        final RefDestination destination = valorisation.getDestination();
                        final String objectId = PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(croppingPlanSpecies1, destination);
                        String displayName = PricesService.GET_HARVESTING_PRICE_DISPLAY_NAME.apply(croppingPlanSpecies1);

                        HarvestingPrice p = new HarvestingPriceImpl();
                        p.setDomain(domain);
                        p.setPracticedSystem(practicedSystem);
                        p.setZone(null);
                        p.setHarvestingActionValorisation(valorisation);
                        p.setPrice(2.0);
                        p.setObjectId(objectId);
                        p.setDisplayName(displayName);
                        p.setPriceUnit(PriceUnit.EURO_HA);

                        prices.add(p);
                    }
                }
            }
        }
        return prices;
    }

    protected void compareMineralFertilizersSpreadingActions(MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction, MineralFertilizersSpreadingAction persistedMineralFertilizersSpreadingAction) {
        Assertions.assertEquals(mineralFertilizersSpreadingAction.isBurial(), persistedMineralFertilizersSpreadingAction.isBurial());
        Assertions.assertEquals(mineralFertilizersSpreadingAction.isLocalizedSpreading(), persistedMineralFertilizersSpreadingAction.isLocalizedSpreading());

        Assertions.assertEquals(CollectionUtils.size(mineralFertilizersSpreadingAction.getMineralProductInputUsages()), CollectionUtils.size(persistedMineralFertilizersSpreadingAction.getMineralProductInputUsages()));
        Assertions.assertEquals(CollectionUtils.size(mineralFertilizersSpreadingAction.getOtherProductInputUsages()), CollectionUtils.size(persistedMineralFertilizersSpreadingAction.getOtherProductInputUsages()));
    }

    protected void compareHarvestingActions(HarvestingAction harvestingAction, HarvestingAction persistedHarvestingAction) {
        Assertions.assertSame(harvestingAction.getMainAction().getTopiaId(), persistedHarvestingAction.getMainAction().getTopiaId());

        Assertions.assertEquals(CollectionUtils.size(harvestingAction.getOtherProductInputUsages()), CollectionUtils.size(persistedHarvestingAction.getOtherProductInputUsages()));
    }

    protected void compareBiologicalControlActions(BiologicalControlAction biologicalControlAction, BiologicalControlAction persistedBiologicalControlAction) {
        Assertions.assertEquals(biologicalControlAction.getBoiledQuantity(), persistedBiologicalControlAction.getBoiledQuantity());
        Assertions.assertEquals(biologicalControlAction.getProportionOfTreatedSurface(), persistedBiologicalControlAction.getProportionOfTreatedSurface());
        Assertions.assertSame(biologicalControlAction.getMainAction().getTopiaId(), persistedBiologicalControlAction.getMainAction().getTopiaId());

        Assertions.assertEquals(CollectionUtils.size(biologicalControlAction.getBiologicalProductInputUsages()), CollectionUtils.size(persistedBiologicalControlAction.getBiologicalProductInputUsages()));
        Assertions.assertEquals(CollectionUtils.size(biologicalControlAction.getOtherProductInputUsages()), CollectionUtils.size(persistedBiologicalControlAction.getOtherProductInputUsages()));
    }

    protected void compareMineralFertilizersSpreadingActions(MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction, MineralFertilizersSpreadingActionDto abstractActionDto) {
        Assertions.assertEquals(mineralFertilizersSpreadingAction.isBurial(), abstractActionDto.isBurial());
        Assertions.assertEquals(mineralFertilizersSpreadingAction.isLocalizedSpreading(), abstractActionDto.isLocalizedSpreading());

        Assertions.assertEquals(CollectionUtils.size(mineralFertilizersSpreadingAction.getMineralProductInputUsages()), CollectionUtils.size(abstractActionDto.getMineralProductInputUsageDtos().orElse(new ArrayList<>())));
        Assertions.assertEquals(CollectionUtils.size(mineralFertilizersSpreadingAction.getOtherProductInputUsages()), CollectionUtils.size(abstractActionDto.getOtherProductInputUsageDtos().orElse(new ArrayList<>())));
    }

    protected void compareHarvestingActions(HarvestingAction harvestingAction, HarvestingActionDto abstractActionDto) {
        Assertions.assertSame(harvestingAction.getMainAction().getTopiaId(), abstractActionDto.getMainActionId());

        Assertions.assertEquals(CollectionUtils.size(harvestingAction.getOtherProductInputUsages()), CollectionUtils.size(abstractActionDto.getOtherProductInputUsageDtos().orElse(new ArrayList<>())));
    }

    protected void compareBiologicalControlActions(BiologicalControlAction biologicalControlAction, BiologicalControlActionDto abstractActionDto) {
        Assertions.assertEquals(biologicalControlAction.getBoiledQuantity(), abstractActionDto.getBoiledQuantity());
        Assertions.assertEquals(biologicalControlAction.getProportionOfTreatedSurface(), abstractActionDto.getProportionOfTreatedSurface());
        Assertions.assertSame(biologicalControlAction.getMainAction().getTopiaId(), abstractActionDto.getMainActionId());

        Assertions.assertEquals(CollectionUtils.size(biologicalControlAction.getBiologicalProductInputUsages()), CollectionUtils.size(abstractActionDto.getPhytoProductInputUsageDtos().orElse(new ArrayList<>())));
        Assertions.assertEquals(CollectionUtils.size(biologicalControlAction.getOtherProductInputUsages()), CollectionUtils.size(abstractActionDto.getOtherProductInputUsageDtos().orElse(new ArrayList<>())));
    }


    @Test
    public void createOrUpdatePracticedPerennialCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-Pernennial";
        String interventionName = "intervention-test-1";
        GrowingSystem gs = growingSystems.get(1);
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();
        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedPerennialCropCycleDto practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();

        PracticedCropCyclePhaseDto practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry aCrop = croppingPlanEntries.getFirst();
        String cropCode = aCrop.getCode();
        String speciesCode0 = "000";
        String speciesCode1 = "001";
        String speciesCode2 = "002";

        PracticedPerennialCropCycle practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(cropCode);
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        List<String> refSpecesCodes = Lists.newArrayList("I44", "I19", "E02");
        List<RefEspece> especes = refEspeceDao.forCode_espece_botaniqueIn(refSpecesCodes).findAll();
        CroppingPlanSpecies species0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, aCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(0),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0
        );
        CroppingPlanSpecies species1 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, aCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(1),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1
        );
        CroppingPlanSpecies species2 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, aCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(2),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode2
        );
        List<CroppingPlanSpecies> croppingPlanSpecies = Lists.newArrayList(species0, species1, species2);
        aCrop.addAllCroppingPlanSpecies(croppingPlanSpecies);

        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto0 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto0.setCode(speciesCode0);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto1 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto1.setCode(speciesCode1);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto2 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto2.setCode(speciesCode2);
        List<PracticedCropCycleSpeciesDto> cropCycleSpeciesDtos = Lists.newArrayList(practicedCropCycleSpeciesDto0, practicedCropCycleSpeciesDto1, practicedCropCycleSpeciesDto2);
        practicedPerennialCropCycleDto.setSpeciesDto(cropCycleSpeciesDtos);


        List<AbstractAction> actions = createActions(croppingPlanSpecies);
        Map<AgrosystInterventionType, AbstractAction> indexedActions = getAgrosystInterventionTypeAbstractActionMap(actions);
        createInputs(actions, domain);

        createHarvestingPrices(domain, practicedSystem, actions, croppingPlanSpecies);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, aCrop);
        List<PracticedInterventionDto> interventionDtos = Lists.newArrayList(interventionDto);
        practicedCropCyclePhaseDto.setInterventions(interventionDtos);

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = Lists.newArrayList(practicedPerennialCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());
        getPersistenceContext().commit();

        // TEST PRACTICED_SYSTEM CREATION
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        PracticedSystem persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertEquals(1L, practicedPerennialCropCycles.size());
        PracticedPerennialCropCycle practicedPerennialCropCycle = practicedPerennialCropCycles.getFirst();

        Collection<PracticedCropCyclePhase> cropCyclePhases = practicedPerennialCropCycle.getCropCyclePhases();
        Assertions.assertEquals(1L, cropCyclePhases.size());

        PracticedCropCyclePhase cropCyclePhase = cropCyclePhases.iterator().next();

        List<PracticedIntervention> persistedInterventions = practicedInterventionTopiaDao.forPracticedCropCyclePhaseEquals(cropCyclePhase).findAll();

        Assertions.assertEquals(1L, persistedInterventions.size());

        PracticedIntervention practicedIntervention = persistedInterventions.getFirst();
        Assertions.assertEquals(interventionName, practicedIntervention.getName());

        Collection<AbstractActionDto> persistedActionDtos = actionService.loadPracticedActionsAndUsages(practicedIntervention);
        Assertions.assertEquals(3L, persistedActionDtos.size());


        int nbActionChecked = getNbValidPersistedActions(indexedActions, persistedActionDtos);
        Assertions.assertEquals(3, nbActionChecked);

        List<PracticedCropCycleSpecies> practicedCropCycleSpecieses = practicedCropCycleSpeciesTopiaDao.forCroppingPlanSpeciesCodeEquals(speciesCode0).findAll();
        Assertions.assertEquals(1L, practicedCropCycleSpecieses.size());
        PracticedPerennialCropCycle persistedPracticedPerennialCropCycle = practicedCropCycleSpecieses.getFirst().getCycle();
        Assertions.assertEquals(persistedPracticedPerennialCropCycle.getTopiaId(), practicedPerennialCropCycle.getTopiaId());

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        List<PracticedCropCyclePhaseDto> phases = practicedPerennialCropCycleDto.getCropCyclePhaseDtos();
        PracticedCropCyclePhaseDto phase = phases.getFirst();

        interventionDtos = phase.getInterventions();
        Assertions.assertEquals(1L, interventionDtos.size());

        interventionDto = interventionDtos.getFirst();
        final ArrayList<AbstractActionDto> abstractActionDtos = Lists.newArrayList(interventionDto.getActionDtos());
        Assertions.assertEquals(actions.size(), abstractActionDtos.size());

        // TEST PRACTICED_SYSTEM UPDATE
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertEquals(1L, practicedPerennialCropCycles.size());
        practicedPerennialCropCycle = practicedPerennialCropCycles.getFirst();

        cropCyclePhases = practicedPerennialCropCycle.getCropCyclePhases();
        Assertions.assertEquals(1L, cropCyclePhases.size());

        cropCyclePhase = cropCyclePhases.iterator().next();

        persistedInterventions = practicedInterventionTopiaDao.forPracticedCropCyclePhaseEquals(cropCyclePhase).findAll();

        Assertions.assertEquals(1L, persistedInterventions.size());

        practicedIntervention = persistedInterventions.getFirst();
        Assertions.assertEquals(interventionName, practicedIntervention.getName());

        persistedActionDtos = actionService.loadPracticedActionsAndUsages(practicedIntervention);
        Assertions.assertEquals(3L, persistedActionDtos.size());

        nbActionChecked = getNbValidPersistedActions(indexedActions, persistedActionDtos);
        Assertions.assertEquals(3, nbActionChecked);

        // suppression test
        // The methode tested for suppression
        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, null, null);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertTrue(practicedPerennialCropCycles.isEmpty());

        Assertions.assertFalse(practicedInterventionTopiaDao.forTopiaIdEquals(practicedIntervention.getTopiaId()).exists());

        final Set<String> actionIds = persistedActionDtos.stream().map(AbstractActionDto::getTopiaId).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toSet());
        Assertions.assertFalse(abstractActionDao.forTopiaIdIn(actionIds).exists());


    }

    @Test
    public void createOrUpdatePracticedPerennialCropCycleWithUnvalidSpecies() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-Pernennial";
        String interventionName = "intervention-test-1";
        GrowingSystem gs = growingSystems.get(1);
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();
        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedPerennialCropCycleDto practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();

        PracticedCropCyclePhaseDto practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        String cropCode = firstCrop.getCode();
        String speciesCode0 = "000";
        String speciesCode1 = "001";
        String speciesCode2 = "002";

        PracticedPerennialCropCycle practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(cropCode);
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        List<String> refSpecesCodes = Lists.newArrayList("I44", "I19", "E02");
        List<RefEspece> especes = refEspeceDao.forCode_espece_botaniqueIn(refSpecesCodes).findAll();
        CroppingPlanSpecies species0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(0),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0
        );
        CroppingPlanSpecies species1 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(1),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1
        );
        CroppingPlanSpecies species2 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(2),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode2
        );
        List<CroppingPlanSpecies> croppingPlanSpecies = Lists.newArrayList(species0, species1, species2);
        firstCrop.addAllCroppingPlanSpecies(croppingPlanSpecies);

        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto0 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto0.setCode(speciesCode0);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto1 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto1.setCode(speciesCode1);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto2 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto2.setCode(speciesCode2);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto3 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto3.setCode("WRONG_SPECIES_CODE");
        List<PracticedCropCycleSpeciesDto> cropCycleSpeciesDtos = Lists.newArrayList(practicedCropCycleSpeciesDto0, practicedCropCycleSpeciesDto1, practicedCropCycleSpeciesDto2, practicedCropCycleSpeciesDto3);
        practicedPerennialCropCycleDto.setSpeciesDto(cropCycleSpeciesDtos);


        List<AbstractAction> actions = createActions(croppingPlanSpecies);
        Map<AgrosystInterventionType, AbstractAction> indexedActions = getAgrosystInterventionTypeAbstractActionMap(actions);
        createInputs(actions, domain);

        List<HarvestingPrice> prices = createHarvestingPrices(domain, practicedSystem, actions, croppingPlanSpecies);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCrop);
        List<PracticedInterventionDto> interventionDtos = Lists.newArrayList(interventionDto);

        SpeciesStadeDto wrongSpeciesStade = new SpeciesStadeDto();
        wrongSpeciesStade.setSpeciesCode("WRONG_SPECIES_CODE");
        interventionDto.getSpeciesStadesDtos().add(wrongSpeciesStade);

        practicedCropCyclePhaseDto.setInterventions(interventionDtos);

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = Lists.newArrayList(practicedPerennialCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // TEST PRACTICED_SYSTEM CREATION
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        PracticedSystem persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertEquals(1L, practicedPerennialCropCycles.size());
        PracticedPerennialCropCycle practicedPerennialCropCycle = practicedPerennialCropCycles.getFirst();

        Assertions.assertFalse(practicedPerennialCropCycles
                .stream()
                .map(PracticedPerennialCropCycle::getPracticedCropCycleSpecies)
                .flatMap(Collection::stream)
                .anyMatch(ppcc -> ppcc.getCroppingPlanSpeciesCode()
                        .contentEquals("WRONG_SPECIES_CODE")));

        Collection<PracticedCropCyclePhase> cropCyclePhases = practicedPerennialCropCycle.getCropCyclePhases();
        Assertions.assertEquals(1L, cropCyclePhases.size());

        PracticedCropCyclePhase cropCyclePhase = cropCyclePhases.iterator().next();

        List<PracticedIntervention> persistedInterventions = practicedInterventionTopiaDao.forPracticedCropCyclePhaseEquals(cropCyclePhase).findAll();

        Assertions.assertEquals(1L, persistedInterventions.size());

        PracticedIntervention practicedIntervention = persistedInterventions.getFirst();
        Assertions.assertEquals(interventionName, practicedIntervention.getName());

        Assertions.assertFalse(practicedIntervention.getSpeciesStades().stream().anyMatch(practicedSpeciesStade -> practicedSpeciesStade.getSpeciesCode().contentEquals("WRONG_SPECIES_CODE")));

        List<AbstractAction> persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
        Assertions.assertEquals(3L, persistedActions.size());


        int nbActionChecked = getNbValidPersistedActions(indexedActions, persistedActions);
        Assertions.assertEquals(3, nbActionChecked);

        List<PracticedCropCycleSpecies> practicedCropCycleSpecieses = practicedCropCycleSpeciesTopiaDao.forCroppingPlanSpeciesCodeEquals(speciesCode0).findAll();
        Assertions.assertEquals(1L, practicedCropCycleSpecieses.size());
        PracticedPerennialCropCycle persistedPracticedPerennialCropCycle = practicedCropCycleSpecieses.getFirst().getCycle();
        Assertions.assertEquals(persistedPracticedPerennialCropCycle.getTopiaId(), practicedPerennialCropCycle.getTopiaId());

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        List<PracticedCropCyclePhaseDto> phases = practicedPerennialCropCycleDto.getCropCyclePhaseDtos();
        PracticedCropCyclePhaseDto phase = phases.getFirst();

        interventionDtos = phase.getInterventions();
        Assertions.assertEquals(1L, interventionDtos.size());

        interventionDto = interventionDtos.getFirst();
        final ArrayList<AbstractActionDto> abstractActionDtos = Lists.newArrayList(interventionDto.getActionDtos());
        Assertions.assertEquals(actions.size(), abstractActionDtos.size());

        // TEST PRACTICED_SYSTEM UPDATE
        practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertEquals(1L, practicedPerennialCropCycles.size());
        practicedPerennialCropCycle = practicedPerennialCropCycles.getFirst();

        cropCyclePhases = practicedPerennialCropCycle.getCropCyclePhases();
        Assertions.assertEquals(1L, cropCyclePhases.size());

        cropCyclePhase = cropCyclePhases.iterator().next();

        persistedInterventions = practicedInterventionTopiaDao.forPracticedCropCyclePhaseEquals(cropCyclePhase).findAll();

        Assertions.assertEquals(1L, persistedInterventions.size());

        practicedIntervention = persistedInterventions.getFirst();
        Assertions.assertEquals(interventionName, practicedIntervention.getName());

        persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
        Assertions.assertEquals(3L, persistedActions.size());

        nbActionChecked = getNbValidPersistedActions(indexedActions, persistedActions);
        Assertions.assertEquals(3, nbActionChecked);

        // suppression test
        // The methode tested for suppression
        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, null, null);

        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(1L, persistedPracticedSystems.size());
        persistedPracticedSystem = persistedPracticedSystems.getFirst();
        Assertions.assertEquals(campains, persistedPracticedSystem.getCampaigns());

        practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();
        Assertions.assertTrue(practicedPerennialCropCycles.isEmpty());

        Assertions.assertFalse(practicedInterventionTopiaDao.forTopiaIdEquals(practicedIntervention.getTopiaId()).exists());

        for (AbstractAction persistedAction : persistedActions) {
            Assertions.assertFalse(abstractActionDao.forTopiaIdEquals(persistedAction.getTopiaId()).exists());
        }

    }

    @Test
    public void removeInterventionFromPracticedPerennialCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-Pernennial";
        String interventionName = "intervention-test-1";
        GrowingSystem gs = growingSystems.get(1);
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();
        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedPerennialCropCycleDto practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();

        PracticedCropCyclePhaseDto practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        String cropCode = firstCrop.getCode();
        String speciesCode0 = "000";
        String speciesCode1 = "001";
        String speciesCode2 = "002";

        PracticedPerennialCropCycle practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(cropCode);
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        List<String> refSpecesCodes = Lists.newArrayList("I44", "I19", "E02");
        List<RefEspece> especes = refEspeceDao.forCode_espece_botaniqueIn(refSpecesCodes).findAll();
        CroppingPlanSpecies species0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(0),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0
        );
        CroppingPlanSpecies species1 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(1),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1
        );
        CroppingPlanSpecies species2 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(2),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode2
        );
        List<CroppingPlanSpecies> croppingPlanSpecies = Lists.newArrayList(species0, species1, species2);
        firstCrop.addAllCroppingPlanSpecies(croppingPlanSpecies);

        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto0 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto0.setCode(speciesCode0);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto1 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto1.setCode(speciesCode1);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto2 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto2.setCode(speciesCode2);
        List<PracticedCropCycleSpeciesDto> cropCycleSpeciesDtos = Lists.newArrayList(practicedCropCycleSpeciesDto0, practicedCropCycleSpeciesDto1, practicedCropCycleSpeciesDto2);
        practicedPerennialCropCycleDto.setSpeciesDto(cropCycleSpeciesDtos);

        List<AbstractAction> actions = createActions(croppingPlanSpecies);
        createInputs(actions, domain);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCrop);

        List<CroppingPlanSpecies> croppingPlanSpecies1 = Lists.newArrayList(species0, species1);
        List<AbstractAction> actions1 = createActions(croppingPlanSpecies1);
        createInputs(actions1, domain);

        List<AbstractAction> actionPrices = Lists.newArrayList(actions);
        actionPrices.addAll(actions1);

        List<HarvestingPrice> prices = createHarvestingPrices(domain, practicedSystem, actionPrices, croppingPlanSpecies);

        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionName + "_1", actions1, domainCode, 0, firstCrop);

        List<PracticedInterventionDto> interventionDtos = Lists.newArrayList(interventionDto, interventionDto1);
        practicedCropCyclePhaseDto.setInterventions(interventionDtos);

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = Lists.newArrayList(practicedPerennialCropCycleDto);

        // TEST PRACTICED_SYSTEM CREATION
        PracticedSystem persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);

        // TEST PRACTICED_SYSTEM UPDATE
        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);

        PracticedPerennialCropCycleDto cycleDto = practicedPerennialCropCycleDtos.getFirst();
        PracticedCropCyclePhaseDto phaseDto = cycleDto.getCropCyclePhaseDtos().getFirst();
        final List<PracticedInterventionDto> interDtos = phaseDto.getInterventions();
        interventionDto1 = interDtos.getFirst();
        interDtos.remove(interventionDto1);

        String removedInterventionId = interventionDto1.getTopiaId();

        Collection<AbstractActionDto> actionDtos = interventionDto1.getActionDtos();
        List<String> removedActionIds = actionDtos.stream().filter(actionDto -> actionDto.getTopiaId().isPresent()).map(dto -> dto.getTopiaId().get()).toList();
        Collection<String> removedInputIds = getActionDtosInputUsageIds(actionDtos);

        // TEST REMOVE INPUT
        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, practicedPerennialCropCycleDtos, null);

        Assertions.assertFalse(practicedInterventionTopiaDao.forTopiaIdEquals(removedInterventionId).exists());
        Assertions.assertFalse(abstractActionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertFalse(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());

    }

    @Test
    public void removeActionsFromPracticedPerennialCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-Pernennial";
        String interventionName = "intervention-test-1";
        GrowingSystem gs = growingSystems.get(1);
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();
        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedPerennialCropCycleDto practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();

        PracticedCropCyclePhaseDto practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        String cropCode = firstCrop.getCode();
        String speciesCode0 = "000";
        String speciesCode1 = "001";
        String speciesCode2 = "002";

        PracticedPerennialCropCycle practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(cropCode);
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        List<String> refSpecesCodes = Lists.newArrayList("I44", "I19", "E02");
        List<RefEspece> especes = refEspeceDao.forCode_espece_botaniqueIn(refSpecesCodes).findAll();
        CroppingPlanSpecies species0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(0),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0
        );
        CroppingPlanSpecies species1 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(1),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1
        );
        CroppingPlanSpecies species2 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(2),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode2
        );
        List<CroppingPlanSpecies> croppingPlanSpecies = Lists.newArrayList(species0, species1, species2);
        firstCrop.addAllCroppingPlanSpecies(croppingPlanSpecies);

        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto0 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto0.setCode(speciesCode0);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto1 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto1.setCode(speciesCode1);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto2 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto2.setCode(speciesCode2);
        List<PracticedCropCycleSpeciesDto> cropCycleSpeciesDtos = Lists.newArrayList(practicedCropCycleSpeciesDto0, practicedCropCycleSpeciesDto1, practicedCropCycleSpeciesDto2);
        practicedPerennialCropCycleDto.setSpeciesDto(cropCycleSpeciesDtos);

        List<AbstractAction> actions = createActions(croppingPlanSpecies);
        createInputs(actions, domain);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCrop);

        List<CroppingPlanSpecies> croppingPlanSpecies1 = Lists.newArrayList(species0, species1);
        List<AbstractAction> actions1 = createActions(croppingPlanSpecies1);
        createInputs(actions1, domain);

        List<AbstractAction> actionPrices = Lists.newArrayList(actions);
        actionPrices.addAll(actions1);

        List<HarvestingPrice> prices = createHarvestingPrices(domain, practicedSystem, actionPrices, croppingPlanSpecies);

        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionName + "_1", actions1, domainCode, 0, firstCrop);

        List<PracticedInterventionDto> interventionDtos = Lists.newArrayList(interventionDto, interventionDto1);
        practicedCropCyclePhaseDto.setInterventions(interventionDtos);

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = Lists.newArrayList(practicedPerennialCropCycleDto);

        // TEST PRACTICED_SYSTEM CREATION
        PracticedSystem persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);

        // TEST PRACTICED_SYSTEM UPDATE
        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);

        PracticedPerennialCropCycleDto cycleDto = practicedPerennialCropCycleDtos.getFirst();
        PracticedCropCyclePhaseDto phaseDto = cycleDto.getCropCyclePhaseDtos().getFirst();
        final List<PracticedInterventionDto> interDtos = phaseDto.getInterventions();
        interventionDto1 = interDtos.getFirst();

        final AbstractActionDto mineralFertilizerAction = interventionDto1.getActionDtos().stream().filter(abstractActionDto -> AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.equals(abstractActionDto.getMainActionInterventionAgrosyst())).findFirst().orElse(null);
        Assertions.assertNotNull(mineralFertilizerAction);
        Assertions.assertTrue(mineralFertilizerAction.getTopiaId().isPresent());
        interventionDto1.getActionDtos().remove(mineralFertilizerAction);

        final Collection<String> removedInputIds = getActionDtosInputUsageIds(List.of(mineralFertilizerAction));

        List<String> removedActionIds = Lists.newArrayList(mineralFertilizerAction.getTopiaId().orElse(null));

        // TEST REMOVE INPUT
        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, practicedPerennialCropCycleDtos, null);

        Assertions.assertFalse(abstractActionDao.forTopiaIdIn(removedActionIds).exists());
        Assertions.assertFalse(abstractInputUsageDao.forTopiaIdIn(removedInputIds).exists());

    }

    @Test
    public void removeInputsFromPracticedPerennialCropCycle() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-Pernennial";
        String interventionName = "intervention-test-1";
        GrowingSystem gs = growingSystems.get(1);
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();
        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedPerennialCropCycleDto practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();

        PracticedCropCyclePhaseDto practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        String cropCode = firstCrop.getCode();
        String speciesCode0 = "000";
        String speciesCode1 = "001";
        String speciesCode2 = "002";

        PracticedPerennialCropCycle practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(cropCode);
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        List<String> refSpecesCodes = Lists.newArrayList("I44", "I19", "E02");
        List<RefEspece> especes = refEspeceDao.forCode_espece_botaniqueIn(refSpecesCodes).findAll();
        CroppingPlanSpecies species0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(0),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0
        );
        CroppingPlanSpecies species1 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(1),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1
        );
        CroppingPlanSpecies species2 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(2),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode2
        );
        List<CroppingPlanSpecies> croppingPlanSpecies = Lists.newArrayList(species0, species1, species2);
        firstCrop.addAllCroppingPlanSpecies(croppingPlanSpecies);

        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto0 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto0.setCode(speciesCode0);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto1 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto1.setCode(speciesCode1);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto2 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto2.setCode(speciesCode2);
        List<PracticedCropCycleSpeciesDto> cropCycleSpeciesDtos = Lists.newArrayList(practicedCropCycleSpeciesDto0, practicedCropCycleSpeciesDto1, practicedCropCycleSpeciesDto2);
        practicedPerennialCropCycleDto.setSpeciesDto(cropCycleSpeciesDtos);

        List<AbstractAction> actions = createActions(croppingPlanSpecies);
        createInputs(actions, domain);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCrop);

        List<CroppingPlanSpecies> croppingPlanSpecies1 = Lists.newArrayList(species0, species1);
        List<AbstractAction> actions1 = createActions(croppingPlanSpecies1);
        createInputs(actions1, domain);

        List<AbstractAction> actionPrices = Lists.newArrayList(actions);
        actionPrices.addAll(actions1);

        List<HarvestingPrice> prices = createHarvestingPrices(domain, practicedSystem, actionPrices, croppingPlanSpecies);

        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionName + "_1", actions1, domainCode, 0, firstCrop);

        List<PracticedInterventionDto> interventionDtos = Lists.newArrayList(interventionDto, interventionDto1);
        practicedCropCyclePhaseDto.setInterventions(interventionDtos);

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = Lists.newArrayList(practicedPerennialCropCycleDto);

        // TEST PRACTICED_SYSTEM CREATION
        PracticedSystem persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);

        // TEST PRACTICED_SYSTEM UPDATE
        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        practicedPerennialCropCycles = practicedPerennialCropCycleTopiaDao.forPracticedSystemEquals(persistedPracticedSystem).findAll();

        // test Modification
        // convert to DTO
        practicedPerennialCropCycleDtos = practicedSystemServiceMock.convertPerennialCropCyclesToDto(practicedPerennialCropCycles, persistedPracticedSystem);

        PracticedPerennialCropCycleDto cycleDto = practicedPerennialCropCycleDtos.getFirst();
        PracticedCropCyclePhaseDto phaseDto = cycleDto.getCropCyclePhaseDtos().getFirst();
        final List<PracticedInterventionDto> interDtos = phaseDto.getInterventions();
        interventionDto1 = interDtos.getFirst();

        Collection<String> removeInputUsageIds = new ArrayList<>();
        Collection<AbstractActionDto> actionDtos = interventionDto1.getActionDtos();
        Collection<AbstractActionDto> newActionDtos = removeUsageFromAbstractActionDtos(removeInputUsageIds, actionDtos, null);
        interventionDto1.setActionDtos(newActionDtos);

        // TEST REMOVE INPUT
        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, practicedPerennialCropCycleDtos, null);

        Assertions.assertFalse(abstractInputUsageDao.forTopiaIdIn(removeInputUsageIds).exists());

    }

    private static Collection<AbstractActionDto> removeUsageFromAbstractActionDtos(
            Collection<String> removeInputUsageIds,
            Collection<AbstractActionDto> actionDtos,
            AgrosystInterventionType onlyForGivenType) {

        Collection<AbstractActionDto> newActionDtos = new ArrayList<>();
        for (AbstractActionDto actionDto : actionDtos) {
            AgrosystInterventionType mainActionInterventionAgrosyst = actionDto.getMainActionInterventionAgrosyst();
            if (AgrosystInterventionType.RECOLTE.equals(mainActionInterventionAgrosyst)
                    && (onlyForGivenType == null || AgrosystInterventionType.RECOLTE.equals(onlyForGivenType))) {
                Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos = ((HarvestingActionDto) actionDto).getOtherProductInputUsageDtos();
                optionalOtherProductInputUsageDtos.ifPresent(
                        otherProductInputUsageDtos -> removeInputUsageIds.addAll(
                                otherProductInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

                HarvestingActionDto harvestingActionDto = ((HarvestingActionDto) actionDto).toBuilder()
                        .otherProductInputUsageDtos(new ArrayList<>())
                        .build();
                newActionDtos.add(harvestingActionDto);
            } else if (AgrosystInterventionType.LUTTE_BIOLOGIQUE.equals(mainActionInterventionAgrosyst)
                    && (onlyForGivenType == null || AgrosystInterventionType.LUTTE_BIOLOGIQUE.equals(onlyForGivenType))) {
                Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos = ((BiologicalControlActionDto) actionDto).getOtherProductInputUsageDtos();
                optionalOtherProductInputUsageDtos.ifPresent(
                        otherProductInputUsageDtos -> removeInputUsageIds.addAll(
                                otherProductInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

                Optional<Collection<PhytoProductInputUsageDto>> optionalPhytoProductInputUsageDtos = ((BiologicalControlActionDto) actionDto).getPhytoProductInputUsageDtos();
                optionalPhytoProductInputUsageDtos.ifPresent(
                        otherInputUsageDtos -> removeInputUsageIds.addAll(
                                otherInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

                BiologicalControlActionDto biologicalControlActionDto = ((BiologicalControlActionDto) actionDto).toBuilder()
                        .phytoProductInputUsageDtos(new ArrayList<>())
                        .otherProductInputUsageDtos(new ArrayList<>())
                        .build();
                newActionDtos.add(biologicalControlActionDto);
            } else if (AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.equals(mainActionInterventionAgrosyst)
                    && (onlyForGivenType == null || AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.equals(onlyForGivenType))) {
                Optional<Collection<OtherProductInputUsageDto>> optionalOtherProductInputUsageDtos = ((MineralFertilizersSpreadingActionDto) actionDto).getOtherProductInputUsageDtos();
                optionalOtherProductInputUsageDtos.ifPresent(
                        otherProductInputUsageDtos -> removeInputUsageIds.addAll(
                                otherProductInputUsageDtos.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

                Optional<Collection<MineralProductInputUsageDto>> productInputUsageDtos = ((MineralFertilizersSpreadingActionDto) actionDto).getMineralProductInputUsageDtos();
                productInputUsageDtos.ifPresent(
                        productInputUsageDtos1 -> removeInputUsageIds.addAll(
                                productInputUsageDtos1.stream()
                                        .map(AbstractInputUsageDto::getInputUsageTopiaId)
                                        .filter(Optional::isPresent)
                                        .map(Optional::get).toList()));

                MineralFertilizersSpreadingActionDto mineralFertilizersSpreadingActionDto = ((MineralFertilizersSpreadingActionDto) actionDto).toBuilder()
                        .mineralProductInputUsageDtos(new ArrayList<>())
                        .otherProductInputUsageDtos(new ArrayList<>())
                        .build();
                newActionDtos.add(mineralFertilizersSpreadingActionDto);
            }
        }
        return newActionDtos;
    }


    @Test
    public void testInterventionRankOnPhase() throws IOException {
        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-Pernennial";
        GrowingSystem gs = growingSystems.get(1);
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();
        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedPerennialCropCycleDto practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();

        PracticedCropCyclePhaseDto practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);

        CroppingPlanEntry croppingPlanEntry = croppingPlanEntries.getFirst();
        String speciesCode = "001";

        PracticedPerennialCropCycle practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(croppingPlanEntry.getCode());
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        List<String> refSpecesCodes = Lists.newArrayList("I44", "I19", "E02");
        List<RefEspece> especes = refEspeceDao.forCode_espece_botaniqueIn(refSpecesCodes).findAll();
        CroppingPlanSpecies species0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, croppingPlanEntry,
                CroppingPlanSpecies.PROPERTY_SPECIES, especes.get(0),
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode
        );
        croppingPlanEntry.addCroppingPlanSpecies(species0);

        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto.setCode(speciesCode);
        List<PracticedCropCycleSpeciesDto> cropCycleSpeciesDtos = Lists.newArrayList(practicedCropCycleSpeciesDto);
        practicedPerennialCropCycleDto.setSpeciesDto(cropCycleSpeciesDtos);

        List<AbstractAction> actions = createActions(Lists.newArrayList(species0));
        createInputs(actions, domain);

        String[] interventionNames = {"I1", "I2", "I3"};
        // rank is not correct it will be correct after saving
        PracticedInterventionDto interventionDto1 = createInterventionDto(interventionNames[0], actions, domainCode, 0, croppingPlanEntry);

        actions = createActions(Lists.newArrayList(species0));
        createInputs(actions, domain);
        PracticedInterventionDto interventionDto2 = createInterventionDto(interventionNames[1], actions, domainCode, 2, croppingPlanEntry);

        actions = createActions(Lists.newArrayList(species0));
        createInputs(actions, domain);
        PracticedInterventionDto interventionDto3 = createInterventionDto(interventionNames[2], actions, domainCode, 4, croppingPlanEntry);

        List<PracticedInterventionDto> interventionDtos = Lists.newArrayList(interventionDto1, interventionDto2, interventionDto3);
        practicedCropCyclePhaseDto.setInterventions(interventionDtos);

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = Lists.newArrayList(practicedPerennialCropCycleDto);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();
        Assertions.assertEquals(0L, persistedPracticedSystems.size());

        // TEST PRACTICED_SYSTEM CREATION
        PracticedSystem persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, practicedPerennialCropCycleDtos, null);

        practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(persistedPracticedSystem.getTopiaId());
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        practicedCropCyclePhaseDto = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().getFirst();
        interventionDtos = practicedCropCyclePhaseDto.getInterventions();

        Assertions.assertEquals(3L, interventionDtos.size());
        for (int i = 0; i < 3; i++) {
            Assertions.assertEquals(interventionDtos.get(i).getName(), interventionNames[i]);
            Assertions.assertEquals(i, interventionDtos.get(i).getRank());
        }

        List<PracticedInterventionDto> reorderedInterventions = Lists.newArrayList(interventionDtos.get(1), interventionDtos.get(0), interventionDtos.get(2));
        String[] interventionName2 = {interventionDtos.get(1).getName(), interventionDtos.getFirst().getName(), interventionDtos.get(2).getName()};
        practicedCropCyclePhaseDto.setInterventions(reorderedInterventions);

        practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, practicedPerennialCropCycleDtos, null);

        practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(persistedPracticedSystem.getTopiaId());
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        practicedCropCyclePhaseDto = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().getFirst();
        interventionDtos = practicedCropCyclePhaseDto.getInterventions();

        Assertions.assertEquals(3L, interventionDtos.size());
        for (int i = 0; i < 3; i++) {
            Assertions.assertEquals(interventionDtos.get(i).getName(), interventionName2[i]);
            Assertions.assertEquals(i, interventionDtos.get(i).getRank());
        }

    }

    protected int getNbValidPersistedActions(Map<AgrosystInterventionType, AbstractAction> indexedActions, List<AbstractAction> persistedActions) {
        int nbActionChecked = 0;
        for (AbstractAction persistedAction : persistedActions) {
            if (persistedAction instanceof HarvestingAction persistedHarvestingAction) {
                compareHarvestingActions(
                        (HarvestingAction) indexedActions.get(AgrosystInterventionType.RECOLTE),
                        persistedHarvestingAction);
                nbActionChecked++;
            } else if (persistedAction instanceof MineralFertilizersSpreadingAction persistedMineralFertilizersSpreadingAction) {
                compareMineralFertilizersSpreadingActions(
                        (MineralFertilizersSpreadingAction) indexedActions.get(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX),
                        persistedMineralFertilizersSpreadingAction);
                nbActionChecked++;
            } else if (persistedAction instanceof BiologicalControlAction biologicalControlAction) {
                compareBiologicalControlActions(
                        (BiologicalControlAction) indexedActions.get(AgrosystInterventionType.LUTTE_BIOLOGIQUE),
                        biologicalControlAction);
                nbActionChecked++;
            }
        }
        return nbActionChecked;
    }

    protected int getNbValidPersistedActions(Map<AgrosystInterventionType, AbstractAction> indexedActions, Collection<AbstractActionDto> abstractActionDtos) {
        int nbActionChecked = 0;
        for (AbstractActionDto abstractActionDto : abstractActionDtos) {
            if (AgrosystInterventionType.RECOLTE.equals(abstractActionDto.getMainActionInterventionAgrosyst())) {
                compareHarvestingActions(
                        (HarvestingAction) indexedActions.get(AgrosystInterventionType.RECOLTE),
                        (HarvestingActionDto) abstractActionDto);
                nbActionChecked++;
            } else if (AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX.equals(abstractActionDto.getMainActionInterventionAgrosyst())) {
                compareMineralFertilizersSpreadingActions(
                        (MineralFertilizersSpreadingAction) indexedActions.get(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX),
                        (MineralFertilizersSpreadingActionDto) abstractActionDto);
                nbActionChecked++;
            } else if (AgrosystInterventionType.LUTTE_BIOLOGIQUE.equals(abstractActionDto.getMainActionInterventionAgrosyst())) {
                compareBiologicalControlActions(
                        (BiologicalControlAction) indexedActions.get(AgrosystInterventionType.LUTTE_BIOLOGIQUE),
                        (BiologicalControlActionDto) abstractActionDto);
                nbActionChecked++;
            }
        }
        return nbActionChecked;
    }


    private PracticedInterventionDto createInterventionDto(
            String interventionName,
            List<AbstractAction> actions,
            String domainCode,
            int rank,
            CroppingPlanEntry croppingPlanEntry) {
        PracticedInterventionDto interventionDto = new PracticedInterventionDto();


        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);
        List<RefStadeEDI> refStadeEDIs = refStadeEDITopiaDao.forActiveEquals(true).findAll();

        List<SpeciesStadeDto> speciesStadeDtos = new ArrayList<>();
        for (CroppingPlanSpecies cps : CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies())) {
            SpeciesStadeDto speciesStadesDto = new SpeciesStadeDto();
            speciesStadesDto.setSpeciesCode(cps.getCode());
            speciesStadesDto.setStadeMin(Itk.getDtoForRefStadeEdi(refStadeEDIs.get(0), translationMap));
            speciesStadesDto.setStadeMax(Itk.getDtoForRefStadeEdi(refStadeEDIs.get(2), translationMap));
            speciesStadeDtos.add(speciesStadesDto);
        }

        PracticedIntervention intervention = new PracticedInterventionImpl();
        intervention.setTopiaId(PracticedSystemService.NEW_INTERVENTION_PREFIX + UUID.randomUUID());
        intervention.setName(interventionName);
        intervention.setType(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);
        intervention.setStartingPeriodDate("3/03");
        intervention.setEndingPeriodDate("4/03");
        intervention.setRank(rank);

        BinderFactory.newBinder(PracticedIntervention.class, PracticedInterventionDto.class).copy(intervention, interventionDto);

        List<AbstractActionDto> abstractActionDtos = actionService.transformeEntitiesToDtos(actions);
        interventionDto.setFromCropCode(croppingPlanEntry.getCode());
        interventionDto.setActionDtos(abstractActionDtos);
        interventionDto.setSpeciesStadesDtos(speciesStadeDtos);
        interventionDto.setDomainId(domainCode);
        return interventionDto;
    }

    @Test
    public void testCampaignChangeOnPracticedSystem() throws IOException, DomainExtendException {
        // required data for test
        PracticedSystem psBaulon2013 = testDatas.createTestPraticedSystems();

        GrowingSystem gs0 = psBaulon2013.getGrowingSystem();

        GrowingPlan growingPlan = gs0.getGrowingPlan();

        Domain d0 = growingPlan.getDomain();
        Domain extendedDomain = domainService.extendDomain(d0.getTopiaId(), 2019);

        GrowingPlan clonedGrowingPlan = getPersistenceContext().getGrowingPlanDao().forDomainEquals(extendedDomain).findUnique();

        GrowingSystem growingSystem_2019 = growingSystemTopiaDao.forGrowingPlanEquals(clonedGrowingPlan).addEquals(GrowingSystem.PROPERTY_CODE, gs0.getCode()).findUnique();

        psBaulon2013.setCampaigns("2019");

        PracticedSystem psBaulon2019 = practicedSystemService.createOrUpdatePracticedSystem(psBaulon2013, null, null);

        org.assertj.core.api.Assertions.assertThat(growingSystem_2019).isEqualTo(psBaulon2019.getGrowingSystem());

    }

    @Test
    public void testLoadHarvestingPriceOnSamePeriode() throws IOException {
        testDatas.createCroppingPlanEntryAndSpeciesForHarvestingPrices();
        RefDestination destination = refDestinationDao.forCode_destination_AEquals("D0106").findUnique();

        CroppingPlanEntry cpe = croppingPlanEntryDao.forCodeEquals("CroppingPlanEntry-code-echalote-0").findUnique();

        double cropYealdAverage = 120;
        int beginMarketingPeriod = 0;
        int beginMarketingPeriodDecade = 1;
        int beginMarketingPeriodCampaign = 2013;
        int endingMarketingPeriod = 0;
        int endingMarketingPeriodDecade = 2;
        int endingMarketingPeriodCampaign = 2013;

        Collection<HarvestingActionValorisation> valorisations = testDatas.createHarvestingActionValorisationsForCrop(
                harvestingActionValorisationDao,
                destination,
                cpe,
                cropYealdAverage,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                beginMarketingPeriodCampaign,
                endingMarketingPeriod,
                endingMarketingPeriodDecade,
                endingMarketingPeriodCampaign, null);

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        String practicedSystemCampaigns = "2013";
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForPracticedSystemValorisations(
                valorisations, practicedSystemCampaigns, codeEspBotCodeQualiBySpeciesCode);
        List<RefHarvestingPrice> harvestingPrices = prices.get(valorisations.iterator().next());
        Assertions.assertEquals(1, harvestingPrices.size());
    }

    @Test
    public void testLoadHarvestingPriceManyResults() throws IOException {
        testDatas.createCroppingPlanEntryAndSpeciesForHarvestingPrices();
        RefDestination destination = refDestinationDao.forCode_destination_AEquals("D0106").findUnique();

        CroppingPlanEntry cpe = croppingPlanEntryDao.forCodeEquals("CroppingPlanEntry-code-echalote-0").findUnique();

        double cropYealdAverage = 120;

        int beginMarketingPeriod = 0;
        int beginMarketingPeriodDecade = 1;
        int beginMarketingPeriodCampaign = 2013;
        int endingMarketingPeriod = 0;
        int endingMarketingPeriodDecade = 3;
        int endingMarketingPeriodCampaign = 2013;

        Collection<HarvestingActionValorisation> valorisations = testDatas.createHarvestingActionValorisationsForCrop(
                harvestingActionValorisationDao,
                destination,
                cpe,
                cropYealdAverage,
                beginMarketingPeriod,
                beginMarketingPeriodDecade,
                beginMarketingPeriodCampaign,
                endingMarketingPeriod,
                endingMarketingPeriodDecade,
                endingMarketingPeriodCampaign, null);

        Set<String> speciesCodes = valorisations.stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        String practicedSystemCampaigns = "2013";
        Map<HarvestingActionValorisation, List<RefHarvestingPrice>> prices = referentialService.getRefHarvestingPricesForPracticedSystemValorisations(
                valorisations, practicedSystemCampaigns, codeEspBotCodeQualiBySpeciesCode);
        List<RefHarvestingPrice> harvestingPrices = prices.get(valorisations.iterator().next());
        Assertions.assertEquals(2, harvestingPrices.size());
    }

    @Test
    public void testCycleWithInvalidValorisations() throws IOException {

        RefEspece refEspeceAAA = testDatas.createRefEspece(refEspeceDao, "AAA", "", "refEspeceAAA", null);
        RefEspece refEspeceAAA_AAA = testDatas.createRefEspece(refEspeceDao, "AAA", "AAA", "refEspeceAAA_AAA", null);
        RefEspece refEspeceAAA_BBB = testDatas.createRefEspece(refEspeceDao, "AAA", "BBB", "refEspeceAAA_BBB", null);
        RefEspece refEspeceBBB = testDatas.createRefEspece(refEspeceDao, "BBB", "", "refEspeceBBB", null);
        RefEspece refEspeceBBB_BBB = testDatas.createRefEspece(refEspeceDao, "BBB", "BBB", "refEspeceBBB_BBB", null);
        RefEspece refEspeceZMO = testDatas.createRefEspece(refEspeceDao, ReferentialService.WINE, "", "Vigne", null);

        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        refSpeciesToSectorDao.createByNaturalId("AAA", null, Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId("AAA", "AAA", Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId("AAA", "BBB", Sector.MARAICHAGE);
        refSpeciesToSectorDao.createByNaturalId("BBB", null, Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId("BBB", "BBB", Sector.GRANDES_CULTURES);
        refSpeciesToSectorDao.createByNaturalId(ReferentialService.WINE, null, Sector.VITICULTURE);

        RefDestination destinationAAA = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, refEspeceAAA.getLibelle_espece_botanique(),
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceAAA.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, Strings.emptyToNull(refEspeceAAA.getCode_qualifiant_AEE()),
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destinationAAA",
                RefDestination.PROPERTY_SECTOR, Sector.GRANDES_CULTURES,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_WINE_VALORISATION, null,
                RefDestination.PROPERTY_ACTIVE, true);

        RefDestination destinationAAA_AAA = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, refEspeceAAA_AAA.getLibelle_espece_botanique(),
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceAAA_AAA.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, Strings.emptyToNull(refEspeceAAA_AAA.getCode_qualifiant_AEE()),
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destinationAAA_AAA",
                RefDestination.PROPERTY_SECTOR, Sector.GRANDES_CULTURES,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_WINE_VALORISATION, null,
                RefDestination.PROPERTY_ACTIVE, true);

        RefDestination destinationAAA_BBB = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, refEspeceAAA_BBB.getLibelle_espece_botanique(),
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceAAA_BBB.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, Strings.emptyToNull(refEspeceAAA_BBB.getCode_qualifiant_AEE()),
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destinationAAA_BBB",
                RefDestination.PROPERTY_SECTOR, Sector.MARAICHAGE,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_WINE_VALORISATION, null,
                RefDestination.PROPERTY_ACTIVE, true);

        RefDestination destinationBBB = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, refEspeceBBB.getLibelle_espece_botanique(),
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceBBB.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, Strings.emptyToNull(refEspeceBBB.getCode_qualifiant_AEE()),
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destinationBBB",
                RefDestination.PROPERTY_SECTOR, Sector.GRANDES_CULTURES,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_WINE_VALORISATION, null,
                RefDestination.PROPERTY_ACTIVE, true);

        RefDestination destinationBBB_BBB = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, refEspeceBBB_BBB.getLibelle_espece_botanique(),
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceBBB_BBB.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, Strings.emptyToNull(refEspeceBBB_BBB.getCode_qualifiant_AEE()),
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destinationBBB_BBB",
                RefDestination.PROPERTY_SECTOR, Sector.GRANDES_CULTURES,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_WINE_VALORISATION, null,
                RefDestination.PROPERTY_ACTIVE, true);

        RefDestination destinationZMO = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, refEspeceZMO.getLibelle_espece_botanique(),
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceZMO.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, Strings.emptyToNull(refEspeceZMO.getCode_qualifiant_AEE()),
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "Circuit Court / Frais",
                RefDestination.PROPERTY_SECTOR, Sector.VITICULTURE,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_WINE_VALORISATION, WineValorisation.GRAPE,
                RefDestination.PROPERTY_ACTIVE, true);


        Map<RefEspece, RefDestination> destinationForRefEspece = new HashMap<>();
        destinationForRefEspece.put(refEspeceAAA, destinationAAA);
        destinationForRefEspece.put(refEspeceAAA_AAA, destinationAAA_AAA);
        destinationForRefEspece.put(refEspeceAAA_BBB, destinationAAA_BBB);
        destinationForRefEspece.put(refEspeceBBB, destinationBBB);
        destinationForRefEspece.put(refEspeceBBB_BBB, destinationBBB_BBB);
        destinationForRefEspece.put(refEspeceZMO, destinationZMO);

        RefDestination invalidDestination_AAA = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, refEspeceAAA.getLibelle_espece_botanique(),
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refEspeceAAA.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, Strings.emptyToNull(refEspeceAAA.getCode_qualifiant_AEE()),
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destinationBBB_BBB",
                RefDestination.PROPERTY_SECTOR, Sector.POLYCULTURE_ELEVAGE,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_WINE_VALORISATION, null,
                RefDestination.PROPERTY_ACTIVE, true);

        Map<RefEspece, Pair<RefDestination, RefQualityCriteria>> refEspeceRefDestinationRefQualityCriteria = new HashMap<>();
        for (Map.Entry<RefEspece, RefDestination> refEspeceRefDestinationEntry : destinationForRefEspece.entrySet()) {
            RefEspece espece = refEspeceRefDestinationEntry.getKey();
            RefDestination destination = refEspeceRefDestinationEntry.getValue();
            RefQualityCriteria rqc = testDatas.createRefQualityCriteria(
                    espece, "RQC_" + destination.getCode_espece_botanique() + "_" + destination.getCode_espece_botanique(),
                    destination.getSector(), destination.getWineValorisation());
            refEspeceRefDestinationRefQualityCriteria.put(espece, Pair.of(destination, rqc));
        }

        RefQualityCriteria invalidQualityCriteria_AAA = testDatas.createRefQualityCriteria(
                refEspeceAAA, "RQC_" + refEspeceAAA.getCode_espece_botanique() + "_" + refEspeceAAA.getCode_espece_botanique(),
                invalidDestination_AAA.getSector(), invalidDestination_AAA.getWineValorisation());

        getPersistenceContext().commit();

        String campains = "2011, 2012, 2013";
        String practicedSystemName = "test-Pernennial";
        String interventionName = "intervention-test-1";
        GrowingSystem gs = growingSystems.get(1);
        Domain domain = gs.getGrowingPlan().getDomain();
        String domainCode = domain.getCode();
        PracticedSystem practicedSystem = new PracticedSystemImpl();
        practicedSystem.setName(practicedSystemName);
        practicedSystem.setGrowingSystem(gs);
        practicedSystem.setCampaigns(campains);

        PracticedPerennialCropCycleDto practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();

        PracticedCropCyclePhaseDto practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);


        CroppingPlanEntry cpeVigne = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain,
                CroppingPlanEntry.PROPERTY_CODE, "CPE_ZMO_0",
                CroppingPlanEntry.PROPERTY_NAME, "Vigne",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanEntry firstCrop = croppingPlanEntries.getFirst();
        String cropCode = firstCrop.getCode();
        String speciesCode0 = "speciesAAA";
        String speciesCode1 = "speciesAAA_AAA";
        String speciesCode2 = "speciesBBB";
        String speciesCode3 = "speciesZMO";

        PracticedPerennialCropCycle practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(cropCode);
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);
        List<PracticedCropCyclePhaseDto> cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        CroppingPlanSpecies species0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceAAA,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0
        );
        CroppingPlanSpecies species1 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceAAA_AAA,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1
        );
        CroppingPlanSpecies species2 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, firstCrop,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceBBB,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode2
        );

        List<CroppingPlanSpecies> croppingPlanSpecies = Lists.newArrayList(species0, species1, species2);
        firstCrop.addAllCroppingPlanSpecies(croppingPlanSpecies);

        CroppingPlanSpecies speciesVigne = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpeVigne,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceZMO,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode3
        );
        cpeVigne.addCroppingPlanSpecies(speciesVigne);

        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto0 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto0.setCode(speciesCode0);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto1 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto1.setCode(speciesCode1);
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto2 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto2.setCode(speciesCode2);
        List<PracticedCropCycleSpeciesDto> cropCycleSpeciesDtos = Lists.newArrayList(practicedCropCycleSpeciesDto0, practicedCropCycleSpeciesDto1, practicedCropCycleSpeciesDto2);
        practicedPerennialCropCycleDto.setSpeciesDto(cropCycleSpeciesDtos);

        List<RefInterventionAgrosystTravailEDI> refActionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.forActiveEquals(true).findAll();
        Map<AgrosystInterventionType, List<RefInterventionAgrosystTravailEDI>> travailByType = GET_REF_INTERVENTION_TRAVAIL_EDI_BY_TYPE(refActionAgrosystTravailEDIs);

        List<RefInterventionAgrosystTravailEDI> harvestingTravailEDIs = travailByType.get(AgrosystInterventionType.RECOLTE);
        HarvestingAction harvestingAction = new HarvestingActionImpl();
        harvestingAction.setTopiaId(ActionService.NEW_ACTION_PREFIX + UUID.randomUUID());
        harvestingAction.setMainAction(harvestingTravailEDIs.get(0));

        double globalYealdAverage = 60.0;

        Collection<HarvestingActionValorisation> valorisations = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(croppingPlanSpecies)) {
            double speciesYealAverage = globalYealdAverage / croppingPlanSpecies.size();
            for (CroppingPlanSpecies species : croppingPlanSpecies) {

                RefDestination d = refEspeceRefDestinationRefQualityCriteria.get(species.getSpecies()).getLeft();
                RefQualityCriteria rqc = refEspeceRefDestinationRefQualityCriteria.get(species.getSpecies()).getRight();

                HarvestingActionValorisation speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(d);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSelfConsumedPersent(50);
                speciesValorisation.setSalesPercent(25);
                speciesValorisation.setNoValorisationPercent(25);

                List<QualityCriteria> allSpeciesQualityCriteria = getQualityCriterias(rqc);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);
                valorisations.add(speciesValorisation);
                // cette valorisation est en double et ne doit pas être sauvegardée
                speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(d);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSalesPercent(100);

                allSpeciesQualityCriteria = getQualityCriterias(rqc);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);
                valorisations.add(speciesValorisation);

                // invalid
                speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(invalidDestination_AAA);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSelfConsumedPersent(100);

                allSpeciesQualityCriteria = getQualityCriterias(invalidQualityCriteria_AAA);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);

                valorisations.add(speciesValorisation);
            }
        }

        harvestingAction.setValorisations(valorisations);

        MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = new MineralFertilizersSpreadingActionImpl();
        mineralFertilizersSpreadingAction.setTopiaId(ActionService.NEW_ACTION_PREFIX + UUID.randomUUID());
        mineralFertilizersSpreadingAction.setBurial(true);
        mineralFertilizersSpreadingAction.setLocalizedSpreading(true);
        mineralFertilizersSpreadingAction.setMainAction(travailByType.get(AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX).get(0));

        BiologicalControlAction biologicalControlAction = new BiologicalControlActionImpl();
        biologicalControlAction.setTopiaId(ActionService.NEW_ACTION_PREFIX + UUID.randomUUID());
        biologicalControlAction.setBoiledQuantity(12.5);
        biologicalControlAction.setProportionOfTreatedSurface(5.6);
        biologicalControlAction.setMainAction(travailByType.get(AgrosystInterventionType.LUTTE_BIOLOGIQUE).get(0));

        List<AbstractAction> actions = Lists.newArrayList(harvestingAction, mineralFertilizersSpreadingAction, biologicalControlAction);

        createInputs(actions, domain);

        createHarvestingPrices(domain, practicedSystem, actions, croppingPlanSpecies);

        PracticedInterventionDto interventionDto = createInterventionDto(interventionName, actions, domainCode, 0, firstCrop);
        List<PracticedInterventionDto> interventionDtos = Lists.newArrayList(interventionDto);
        practicedCropCyclePhaseDto.setInterventions(interventionDtos);

        List<PracticedSystem> persistedPracticedSystems;
        persistedPracticedSystems = practicedSystemTopiaDao.forNameEquals(practicedSystemName).findAll();

        // test 1: Creation
        Assertions.assertEquals(0L, persistedPracticedSystems.size());
        PracticedSystem persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(practicedSystem, Lists.newArrayList(practicedPerennialCropCycleDto), null);

        List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(persistedPracticedSystem.getTopiaId());
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        practicedCropCyclePhaseDto = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().getFirst();
        interventionDtos = practicedCropCyclePhaseDto.getInterventions();

        Assertions.assertEquals(1L, interventionDtos.size());

        List<AbstractAction> persistedActions = null;
        HarvestingAction persisteHarvestingAction1 = null;

        for (PracticedInterventionDto interventionDto1 : interventionDtos) {
            final PracticedIntervention practicedIntervention = practicedInterventionTopiaDao.forTopiaIdEquals(interventionDto1.getTopiaId()).findUnique();
            persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
            for (AbstractAction int0Action : persistedActions) {
                if (int0Action instanceof HarvestingAction) {
                    persisteHarvestingAction1 = (HarvestingAction) int0Action;
                    Collection<HarvestingActionValorisation> valorisations1 = persisteHarvestingAction1.getValorisations();
                    Assertions.assertEquals(croppingPlanSpecies.size(), valorisations1.size());
                    for (HarvestingActionValorisation valorisation : valorisations1) {
                        Assertions.assertEquals(1, valorisation.getQualityCriteria().size());
                    }
                }
            }
        }
        Assertions.assertNotNull(persisteHarvestingAction1);

        // Test 2: update add new bad valorisation
        Collection<HarvestingActionValorisation> valorisations1 = persisteHarvestingAction1.getValorisations();
        HarvestingActionValorisation speciesValorisation = harvestingActionValorisationDao.newInstance();
        speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
        speciesValorisation.setSpeciesCode(speciesCode0);
        speciesValorisation.setDestination(invalidDestination_AAA);
        speciesValorisation.setYealdAverage(12);
        speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
        speciesValorisation.setBeginMarketingPeriod(2);
        speciesValorisation.setBeginMarketingPeriodDecade(1);
        speciesValorisation.setBeginMarketingPeriodCampaign(2011);
        speciesValorisation.setEndingMarketingPeriod(4);
        speciesValorisation.setEndingMarketingPeriodDecade(4);
        speciesValorisation.setEndingMarketingPeriodCampaign(2013);

        speciesValorisation.setQualityCriteria(getQualityCriterias(invalidQualityCriteria_AAA));

        valorisations1.add(speciesValorisation);

        createHarvestingPrices(domain, practicedSystem, persistedActions, croppingPlanSpecies);

        // Test 2: update add new bad valorisation
        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, Lists.newArrayList(practicedPerennialCropCycleDto), null);

        practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(persistedPracticedSystem.getTopiaId());
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        practicedCropCyclePhaseDto = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().getFirst();
        interventionDtos = practicedCropCyclePhaseDto.getInterventions();

        Assertions.assertEquals(1L, interventionDtos.size());

        persisteHarvestingAction1 = null;

        for (PracticedInterventionDto interventionDto1 : interventionDtos) {
            final PracticedIntervention practicedIntervention = practicedInterventionTopiaDao.forTopiaIdEquals(interventionDto1.getTopiaId()).findUnique();
            persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
            for (AbstractAction int0Action : persistedActions) {
                if (int0Action instanceof HarvestingAction) {
                    persisteHarvestingAction1 = (HarvestingAction) int0Action;
                    valorisations1 = persisteHarvestingAction1.getValorisations();
                    Assertions.assertEquals(croppingPlanSpecies.size(), valorisations1.size());
                    for (HarvestingActionValorisation valorisation : valorisations1) {
                        Assertions.assertEquals(1, valorisation.getQualityCriteria().size());
                    }
                }
            }
        }
        Assertions.assertNotNull(persisteHarvestingAction1);


        // Test 3: Update 1 valid valorisation and set it bad destination
        PracticedInterventionDto practicedInterventionDto = interventionDtos.getFirst();
        Collection<AbstractActionDto> actionDtos1 = practicedInterventionDto.getActionDtos().stream().filter(dto -> !AgrosystInterventionType.RECOLTE.equals(dto.getMainActionInterventionAgrosyst())).collect(Collectors.toList());
        Optional<AbstractActionDto> abstractActionDto = practicedInterventionDto.getActionDtos().stream().filter(dto -> AgrosystInterventionType.RECOLTE.equals(dto.getMainActionInterventionAgrosyst())).findAny();

        Assertions.assertTrue(abstractActionDto.isPresent());
        HarvestingActionDto harvestingActionDto1 = (HarvestingActionDto) abstractActionDto.get();
        List<HarvestingActionValorisationDto> newValorisationDtoListWithOneMoreInvalid = new ArrayList<>();
        List<HarvestingActionValorisationDto> valorisationDtos = harvestingActionDto1.getValorisationDtos();
        boolean idbadDestinationAdded = false;
        for (HarvestingActionValorisationDto valorisationDto : valorisationDtos) {
            if (!idbadDestinationAdded && !invalidDestination_AAA.getTopiaId().equals(valorisationDto.getDestinationId())) {
                newValorisationDtoListWithOneMoreInvalid.add(valorisationDto.toBuilder()
                        .destinationId(invalidDestination_AAA.getTopiaId())
                        .build());
                idbadDestinationAdded = true;

            } else {
                newValorisationDtoListWithOneMoreInvalid.add(valorisationDto);
            }
        }
        harvestingActionDto1 = harvestingActionDto1.toBuilder().valorisationDtos(newValorisationDtoListWithOneMoreInvalid).build();
        actionDtos1.add(harvestingActionDto1);
        practicedInterventionDto.setActionDtos(actionDtos1);


        // Test 3: Update 1 valid valorisation and set it bad destination
        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, Lists.newArrayList(practicedPerennialCropCycleDto), null);

        practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(persistedPracticedSystem.getTopiaId());
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        practicedCropCyclePhaseDto = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().getFirst();
        interventionDtos = practicedCropCyclePhaseDto.getInterventions();

        Assertions.assertEquals(1L, interventionDtos.size());

        persisteHarvestingAction1 = null;

        for (PracticedInterventionDto interventionDto1 : interventionDtos) {
            final PracticedIntervention practicedIntervention = practicedInterventionTopiaDao.forTopiaIdEquals(interventionDto1.getTopiaId()).findUnique();
            persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
            for (AbstractAction int0Action : persistedActions) {
                if (int0Action instanceof HarvestingAction) {
                    persisteHarvestingAction1 = (HarvestingAction) int0Action;
                    valorisations1 = persisteHarvestingAction1.getValorisations();
                    Assertions.assertEquals((croppingPlanSpecies.size()) - 1, valorisations1.size());
                    for (HarvestingActionValorisation valorisation : valorisations1) {
                        Assertions.assertEquals(1, valorisation.getQualityCriteria().size());
                    }
                }
            }
        }
        Assertions.assertNotNull(persisteHarvestingAction1);


        // Test 4: Update valid valorisation set it bad quality criteria


        practicedInterventionDto = interventionDtos.getFirst();
        actionDtos1 = practicedInterventionDto.getActionDtos().stream().filter(dto -> !AgrosystInterventionType.RECOLTE.equals(dto.getMainActionInterventionAgrosyst())).collect(Collectors.toList());
        abstractActionDto = practicedInterventionDto.getActionDtos().stream().filter(dto -> AgrosystInterventionType.RECOLTE.equals(dto.getMainActionInterventionAgrosyst())).findAny();

        Assertions.assertTrue(abstractActionDto.isPresent());
        harvestingActionDto1 = (HarvestingActionDto) abstractActionDto.get();
        newValorisationDtoListWithOneMoreInvalid = new ArrayList<>();
        valorisationDtos = harvestingActionDto1.getValorisationDtos();
        String validValorisationBadQualityCriteriaId = null;
        boolean idbadQualityCriteriaAdded = false;
        for (HarvestingActionValorisationDto valorisationDto : valorisationDtos) {
            final Optional<Collection<QualityCriteriaDto>> qualityCriteriaDtos = valorisationDto.getQualityCriteriaDtos();
            if (!idbadQualityCriteriaAdded && qualityCriteriaDtos.isPresent() && CollectionUtils.isNotEmpty(qualityCriteriaDtos.get())) {
                final Collection<QualityCriteriaDto> qualityCriteriaDtos1 = qualityCriteriaDtos.get();
                final Optional<QualityCriteriaDto> qualityCriteriaDto = qualityCriteriaDtos1.stream().findAny();
                final QualityCriteriaDto qualityCriteriaDto1 = qualityCriteriaDto.get().toBuilder().refQualityCriteriaId(invalidQualityCriteria_AAA.getTopiaId()).build();

                newValorisationDtoListWithOneMoreInvalid.add(valorisationDto.toBuilder()
                        .qualityCriteriaDtos(Lists.newArrayList(qualityCriteriaDto1))
                        .build());
                idbadQualityCriteriaAdded = true;
                validValorisationBadQualityCriteriaId = valorisationDto.getTopiaId().get();

            } else {
                newValorisationDtoListWithOneMoreInvalid.add(valorisationDto);
            }
        }
        harvestingActionDto1 = harvestingActionDto1.toBuilder().valorisationDtos(newValorisationDtoListWithOneMoreInvalid).build();
        actionDtos1.add(harvestingActionDto1);
        practicedInterventionDto.setActionDtos(actionDtos1);

        Assertions.assertNotNull(validValorisationBadQualityCriteriaId);

        valorisations1 = persisteHarvestingAction1.getValorisations();

        final HarvestingActionValorisation validToBadValorisation = valorisations1.iterator().next();

        Collection<QualityCriteria> qualityCriterias = validToBadValorisation.getQualityCriteria();
        for (QualityCriteria qc : qualityCriterias) {
            qc.setRefQualityCriteria(invalidQualityCriteria_AAA);
        }
        // Test 4: Update valid valorisation set it bad quality criteria
        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, Lists.newArrayList(practicedPerennialCropCycleDto), null);

        practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(persistedPracticedSystem.getTopiaId());
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        practicedCropCyclePhaseDto = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().getFirst();
        interventionDtos = practicedCropCyclePhaseDto.getInterventions();

        Assertions.assertEquals(1L, interventionDtos.size());

        persisteHarvestingAction1 = null;

        boolean valorisationWitBadQualityCriteriaFound = false;
        for (PracticedInterventionDto interventionDto1 : interventionDtos) {
            final PracticedIntervention practicedIntervention = practicedInterventionTopiaDao.forTopiaIdEquals(interventionDto1.getTopiaId()).findUnique();
            persistedActions = abstractActionDao.forPracticedInterventionEquals(practicedIntervention).findAll();
            for (AbstractAction int0Action : persistedActions) {
                if (int0Action instanceof HarvestingAction) {
                    persisteHarvestingAction1 = (HarvestingAction) int0Action;
                    valorisations1 = persisteHarvestingAction1.getValorisations();
                    Assertions.assertEquals((croppingPlanSpecies.size()) - 1, valorisations1.size());
                    for (HarvestingActionValorisation valorisation : valorisations1) {
                        if (valorisation.getTopiaId().equals(validValorisationBadQualityCriteriaId)) {
                            valorisationWitBadQualityCriteriaFound = true;
                            Assertions.assertEquals(0, valorisation.getQualityCriteria().size());
                        }
                    }
                }
            }
        }
        Assertions.assertNotNull(persisteHarvestingAction1);
        Assertions.assertTrue(valorisationWitBadQualityCriteriaFound);

        // Test 5: Destination et critères de qualité sur vigne
        croppingPlanSpecies = Lists.newArrayList(speciesVigne);

        harvestingAction = new HarvestingActionImpl();
        harvestingAction.setTopiaId(ActionService.NEW_ACTION_PREFIX + UUID.randomUUID());
        harvestingAction.setMainAction(harvestingTravailEDIs.get(0));
        harvestingAction.setWineValorisations(Lists.newArrayList(WineValorisation.GRAPE));

        valorisations = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(croppingPlanSpecies)) {
            double speciesYealAverage = globalYealdAverage / croppingPlanSpecies.size();
            for (CroppingPlanSpecies species : croppingPlanSpecies) {

                RefDestination d = refEspeceRefDestinationRefQualityCriteria.get(species.getSpecies()).getLeft();
                RefQualityCriteria rqc = refEspeceRefDestinationRefQualityCriteria.get(species.getSpecies()).getRight();

                speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(d);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSalesPercent(50);
                speciesValorisation.setNoValorisationPercent(50);

                List<QualityCriteria> allSpeciesQualityCriteria = getQualityCriterias(rqc);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);
                valorisations.add(speciesValorisation);
                // cette valorisation est en double et ne doit pas être sauvegardée
                speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(d);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSelfConsumedPersent(100);

                allSpeciesQualityCriteria = getQualityCriterias(rqc);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);
                valorisations.add(speciesValorisation);

                speciesValorisation = harvestingActionValorisationDao.newInstance();
                speciesValorisation.setTopiaId(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION + UUID.randomUUID());
                speciesValorisation.setSpeciesCode(species.getCode());
                speciesValorisation.setDestination(invalidDestination_AAA);
                speciesValorisation.setYealdAverage(speciesYealAverage);
                speciesValorisation.setYealdUnit(YealdUnit.HL_HA);
                speciesValorisation.setBeginMarketingPeriod(2);
                speciesValorisation.setBeginMarketingPeriodDecade(1);
                speciesValorisation.setBeginMarketingPeriodCampaign(2011);
                speciesValorisation.setEndingMarketingPeriod(4);
                speciesValorisation.setEndingMarketingPeriodDecade(4);
                speciesValorisation.setEndingMarketingPeriodCampaign(2013);
                speciesValorisation.setSalesPercent(100);

                allSpeciesQualityCriteria = getQualityCriterias(invalidQualityCriteria_AAA);
                speciesValorisation.setQualityCriteria(allSpeciesQualityCriteria);

                valorisations.add(speciesValorisation);
            }
        }

        harvestingAction.setValorisations(valorisations);

        actions = Lists.newArrayList(harvestingAction);

        createInputs(actions, domain);

        createHarvestingPrices(domain, practicedSystem, actions, croppingPlanSpecies);

        interventionDto = createInterventionDto("Récolte Vigne", actions, domainCode, 0, cpeVigne);
        interventionDtos = Lists.newArrayList(interventionDto);

        practicedCropCyclePhaseDto = new PracticedCropCyclePhaseDto();
        practicedCropCyclePhaseDto.setDuration(10);
        practicedCropCyclePhaseDto.setType(CropCyclePhaseType.PLEINE_PRODUCTION);
        practicedCropCyclePhaseDto.setInterventions(interventionDtos);
        cropCyclePhaseDtos = Lists.newArrayList(practicedCropCyclePhaseDto);

        practicedPerennialCropCycle0 = new PracticedPerennialCropCycleImpl();
        practicedPerennialCropCycle0.setCroppingPlanEntryCode(cpeVigne.getCode());
        practicedPerennialCropCycle0.setWeedType(WeedType.PARTIEL);


        practicedPerennialCropCycleDto = new PracticedPerennialCropCycleDto();
        practicedPerennialCropCycleDto.setCropCyclePhaseDtos(cropCyclePhaseDtos);
        practicedPerennialCropCycleDto.setPracticedPerennialCropCycle(practicedPerennialCropCycle0);

        List<PracticedCropCycleSpeciesDto> speciesDto = new ArrayList<>();
        PracticedCropCycleSpeciesDto practicedCropCycleSpeciesDto3 = new PracticedCropCycleSpeciesDto();
        practicedCropCycleSpeciesDto3.setCode(speciesCode3);
        speciesDto.add(practicedCropCycleSpeciesDto3);
        practicedPerennialCropCycleDto.setSpeciesDto(speciesDto);


        persistedPracticedSystem = practicedSystemService.createOrUpdatePracticedSystem(persistedPracticedSystem, Lists.newArrayList(practicedPerennialCropCycleDto), null);

        practicedPerennialCropCycleDtos = practicedSystemService.getAllPracticedPerennialCropCycles(persistedPracticedSystem.getTopiaId());
        practicedPerennialCropCycleDto = practicedPerennialCropCycleDtos.getFirst();
        practicedCropCyclePhaseDto = practicedPerennialCropCycleDto.getCropCyclePhaseDtos().getFirst();
        interventionDtos = practicedCropCyclePhaseDto.getInterventions();

        Assertions.assertEquals(1L, interventionDtos.size());

        HarvestingActionDto harvestingActionDto = null;
        for (PracticedInterventionDto interventionDto1 : interventionDtos) {
            final Collection<AbstractActionDto> actionDtos = interventionDto1.getActionDtos();
            for (AbstractActionDto int0Action : actionDtos) {
                if (AgrosystInterventionType.RECOLTE.equals(int0Action.getMainActionInterventionAgrosyst())) {
                    harvestingActionDto = (HarvestingActionDto) int0Action;
                    Assertions.assertEquals(1L, harvestingActionDto.getValorisationDtos().size());
                }
            }
        }
        Assertions.assertNotNull(harvestingActionDto);
    }

    protected void validNonDuplicatedCode(Domain domain) {

        List<AbstractDomainInputStockUnit> domainInputStockUnits = domainInputStockUnitDao.forDomainEquals(domain).findAll();

        Set<String> allCodes = new HashSet<>();
        domainInputStockUnits.forEach(disu -> {
            org.assertj.core.api.Assertions.assertThat(disu.getCode()).isNotEmpty();
            org.assertj.core.api.Assertions.assertThat(allCodes.contains(disu.getCode())).isFalse();
            allCodes.add(disu.getCode());
        });

    }

    @Test
    public void TestDuplicatePracticedSystemWithUsages() throws IOException, DomainExtendException {
        testDatas.setGson(serviceContext.getGson());
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importFertiMinUnifa();
        testDatas.importRefEspeces();

        testDatas.importAllActaTraitementsProduits();
        testDatas.importActaTraitementsProduits();

        testDatas.importRefActaProduitRoot();
        testDatas.importDestination(true);
        testDatas.importFertiOrga();

        testDatas.importRefPrixPhyto();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaGroupeCultures();
        testDatas.importActaDosageSpc();

        feedUpReferentials();

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        final Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();

        testDatas.createEquipmentsAndToolsCouplingForDomain(domain);

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.addColzaPoisFeveroleTriticaleCropsToDomain(domain, testDatas.findCroppingPlanEntries(domain.getTopiaId()));
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));

        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);
        testDatas.createPracticedSeasonalCropCycleWithNotUseCropInCycle(croppingPlanEntries, ps);

        final PracticedCropCycleConnection connOrgeColsa = practicedCropCycleConnectionDao.forTopiaIdEquals("connOrgeColsaId").findUnique();
        final PracticedCropCycleConnection connColsaBle = practicedCropCycleConnectionDao.forTopiaIdEquals("connColsaBleId").findUnique();

        testDatas.createINRAPracticedInterventions(domain, cropByNames, connOrgeColsa, connColsaBle, ps, true);
        Domain extendedDomain = domainService.extendDomain(domain.getTopiaId(), domain.getCampaign() + 1);
        final LinkedHashMap<Integer, String> relatedGrowingSystemIdsByCampaigns = growingSystemService.getRelatedGrowingSystemIdsByCampaigns(ps.getGrowingSystem().getCode());
        final String extendedGrowingSystemId = relatedGrowingSystemIdsByCampaigns.get(extendedDomain.getCampaign());
        Assertions.assertNotNull(extendedGrowingSystemId);
        Assertions.assertNotNull(extendedDomain);

        final PracticedSystem extendedPracticedSystem = practicedSystemService.duplicatePracticedSystemWithUsages(ps.getTopiaId(), extendedGrowingSystemId);
        Assertions.assertNotNull(extendedPracticedSystem);
        validNonDuplicatedCode(extendedDomain);

        List<PracticedSeasonalCropCycle> fromCycle = practicedSeasonalCropCycleTopiaDao.forPracticedSystemEquals(ps).findAll();
        List<PracticedCropCycleNode> fromCropCycleNodes = practicedCropCycleNodeTopiaDao.forPracticedSeasonalCropCycleIn(fromCycle).findAll();
        List<PracticedCropCycleConnection> fromPracticedCropCycleConnections = practicedCropCycleConnectionDao.forTargetIn(fromCropCycleNodes).findAll();
        List<PracticedIntervention> fromPracticedInterventions = practicedInterventionTopiaDao.forPracticedCropCycleConnectionIn(fromPracticedCropCycleConnections).findAll();
        List<AbstractAction> fromActions = abstractActionDao.forPracticedInterventionIn(fromPracticedInterventions).findAll();

        List<PracticedSeasonalCropCycle> duplicatedCycle = practicedSeasonalCropCycleTopiaDao.forPracticedSystemEquals(extendedPracticedSystem).findAll();
        List<PracticedCropCycleNode> practicedCropCycleNodes = practicedCropCycleNodeTopiaDao.forPracticedSeasonalCropCycleIn(duplicatedCycle).findAll();
        List<PracticedCropCycleConnection> practicedCropCycleConnections = practicedCropCycleConnectionDao.forTargetIn(practicedCropCycleNodes).findAll();
        List<PracticedIntervention> practicedInterventions = practicedInterventionTopiaDao.forPracticedCropCycleConnectionIn(practicedCropCycleConnections).findAll();
        List<AbstractAction> duplicatedAbstractActions = abstractActionDao.forPracticedInterventionIn(practicedInterventions).findAll();

        Map<String, AbstractInputUsage> fromUsagesByCode = getUsagesByCode(fromActions);
        Map<String, AbstractInputUsage> duplicatedUsagesByCode = getUsagesByCode(duplicatedAbstractActions);

        Assertions.assertEquals(fromUsagesByCode.keySet(), duplicatedUsagesByCode.keySet());
    }

    private Map<String, AbstractInputUsage> getUsagesByCode(List<AbstractAction> duplicatedAbstractActions) {
        Map<String, AbstractInputUsage> usageByCodes = new HashMap<>();
        duplicatedAbstractActions.forEach(
                aa -> {
                    if (aa instanceof BiologicalControlAction biologicalControlAction) {
                        Collection<BiologicalProductInputUsage> biologicalProductInputUsages = biologicalControlAction.getBiologicalProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, biologicalProductInputUsages);
                        Collection<OtherProductInputUsage> otherProductInputUsages = biologicalControlAction.getOtherProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                    } else if (aa instanceof SeedingActionUsage seedingActionUsage) {
                        Collection<SeedLotInputUsage> seedLotInputUsage = seedingActionUsage.getSeedLotInputUsage();
                        addInputUsageToInputUsagesByCode(usageByCodes, seedLotInputUsage);
                        seedLotInputUsage.forEach(
                                slu -> {
                                    Collection<SeedSpeciesInputUsage> seedSpeciesInputUsages = CollectionUtils.emptyIfNull(slu.getSeedingSpecies());
                                    addInputUsageToInputUsagesByCode(usageByCodes, seedSpeciesInputUsages);
                                    seedSpeciesInputUsages.forEach(ssi -> {
                                        Collection<SeedProductInputUsage> seedProductInputUsages = ssi.getSeedProductInputUsages();
                                        addInputUsageToInputUsagesByCode(usageByCodes, seedProductInputUsages);
                                    });
                                }
                        );
                        Collection<OtherProductInputUsage> otherProductInputUsages = seedingActionUsage.getOtherProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                    } else if (aa instanceof PesticidesSpreadingAction pesticidesSpreadingAction) {
                        Collection<PesticideProductInputUsage> pesticideProductInputUsages = pesticidesSpreadingAction.getPesticideProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, pesticideProductInputUsages);
                        Collection<OtherProductInputUsage> otherProductInputUsages = pesticidesSpreadingAction.getOtherProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                    } else if (aa instanceof OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction) {
                        Collection<OrganicProductInputUsage> organicProductInputUsages = organicFertilizersSpreadingAction.getOrganicProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, organicProductInputUsages);
                        Collection<OtherProductInputUsage> otherProductInputUsages = organicFertilizersSpreadingAction.getOtherProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                    } else if (aa instanceof HarvestingAction harvestingAction) {
                        Collection<OtherProductInputUsage> otherProductInputUsages = harvestingAction.getOtherProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                    } else if (aa instanceof OtherAction otherAction) {
                        Collection<OtherProductInputUsage> otherProductInputUsages = otherAction.getOtherProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                    } else if (aa instanceof MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction) {
                        Collection<MineralProductInputUsage> mineralProductInputUsages = mineralFertilizersSpreadingAction.getMineralProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, mineralProductInputUsages);
                        Collection<OtherProductInputUsage> otherProductInputUsages = mineralFertilizersSpreadingAction.getOtherProductInputUsages();
                        addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                    }
                }
        );
        return usageByCodes;
    }

    protected void addInputUsageToInputUsagesByCode(Map<String, AbstractInputUsage> usageByCodes, Collection<? extends AbstractInputUsage> usages) {
        if (CollectionUtils.isNotEmpty(usages)) {
            usages.forEach(u -> {
                if (u instanceof BiologicalProductInputUsage biologicalProductInputUsage) {
                    usageByCodes.put(biologicalProductInputUsage.getDomainPhytoProductInput().getCode(), u);
                } else if (u instanceof SeedingActionUsage seedingActionUsage) {
                    Collection<SeedLotInputUsage> seedLotInputUsage = CollectionUtils.emptyIfNull(seedingActionUsage.getSeedLotInputUsage());
                    seedLotInputUsage.forEach(
                            slu -> {
                                usageByCodes.put(slu.getDomainSeedLotInput().getCode(), slu);
                                Collection<SeedSpeciesInputUsage> seedSpeciesInputUsages = CollectionUtils.emptyIfNull(slu.getSeedingSpecies());
                                seedSpeciesInputUsages.forEach(ssi -> {
                                    usageByCodes.put(ssi.getDomainSeedSpeciesInput().getCode(), ssi);
                                    Collection<SeedProductInputUsage> seedProductInputUsages = CollectionUtils.emptyIfNull(ssi.getSeedProductInputUsages());
                                    seedProductInputUsages.forEach(spu ->
                                            usageByCodes.put(spu.getDomainPhytoProductInput().getCode(), spu));
                                });
                            }
                    );
                    Collection<OtherProductInputUsage> otherProductInputUsages = seedingActionUsage.getOtherProductInputUsages();
                    addInputUsageToInputUsagesByCode(usageByCodes, otherProductInputUsages);
                } else if (u instanceof PesticideProductInputUsage pesticideProductInputUsage) {
                    usageByCodes.put(pesticideProductInputUsage.getDomainPhytoProductInput().getCode(), u);
                } else if (u instanceof OrganicProductInputUsage organicProductInputUsage) {
                    usageByCodes.put(organicProductInputUsage.getDomainOrganicProductInput().getCode(), u);
                } else if (u instanceof OtherProductInputUsage otherProductInputUsage) {
                    usageByCodes.put(otherProductInputUsage.getDomainOtherInput().getCode(), u);
                } else if (u instanceof MineralProductInputUsage mineralProductInputUsage) {
                    usageByCodes.put(mineralProductInputUsage.getDomainMineralProductInput().getCode(), u);
                }
            });
        }
    }

    protected void feedUpReferentials() throws IOException {
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefEspece.csv")) {
            importService.importEspeces(stream);
        }
        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefActaDosageSPC.csv")) {
            importService.importActaDosageSpc(stream);
        }
        // depuis la correction sur ImportServiceImpl pour ne pas mettre à true toute les nouvelles lignes importées mais tenir compte de l'attribut Actif
        final RefActaDosageSPCTopiaDao refActaDosageSPCDao = getPersistenceContext().getRefActaDosageSPCDao();
        RefActaDosageSPC refActaDosageSPC = refActaDosageSPCDao.forProperties(RefActaDosageSPC.PROPERTY_ID_CULTURE, "5091", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, 147, RefActaDosageSPC.PROPERTY_ID_CULTURE, 29, RefActaDosageSPC.PROPERTY_ACTIVE, false).findUniqueOrNull();
        if (refActaDosageSPC != null) {
            refActaDosageSPC.setActive(true);
            refActaDosageSPCDao.update(refActaDosageSPC);
            getPersistenceContext().commit();
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefActaTraitementsProduit.csv")) {
            importService.importActaTraitementsProduits(stream);
        }

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/performance/exportRefActaGroupeCultures.csv")) {
            importService.importActaGroupeCultures(stream);
        }
    }
}
