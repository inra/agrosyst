package fr.inra.agrosyst.services.context;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.context.NavigationContextService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.util.List;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class NavigationContextTest extends AbstractAgrosystTest {

    protected NavigationContextService service;

    @BeforeEach
    public void setupServices() throws TopiaException {
        service = serviceFactory.newService(NavigationContextService.class);
    }

    @Test
    public void getAllCampaign() throws TopiaException, IOException {
        // Force domains creation
        serviceFactory.newInstance(TestDatas.class).createTestDomains();

        List<Integer> allCampaigns = service.getAllCampaigns();
        Assertions.assertEquals(3, allCampaigns.size());
        Assertions.assertEquals(2013, allCampaigns.getFirst().intValue());
        Assertions.assertEquals(2014, allCampaigns.get(1).intValue());
        Assertions.assertEquals(2015, allCampaigns.get(2).intValue());
    }
}
