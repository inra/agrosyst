package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorActiveSubstanceAmountTest extends AbstractQSATest {

    @Test
    public void testQuantitesSubstancesActivesRealise_12307() {
        testDatas.createEffectiveDataSetForQSAIndicators();

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);


        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        IndicatorActiveSubstanceAmount indicatorActiveSubstanceAmount = serviceFactory.newInstance(IndicatorActiveSubstanceAmount.class);
        indicatorActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorActiveSubstanceAmount);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Glyphosate;2013;Baulon;4.32;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active S-métolachlore;2013;Baulon;0.96075;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Prosulfocarbe;2013;Baulon;8.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Chlortoluron;2013;Baulon;0.75;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Diflufenican;2013;Baulon;0.05;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Lambda-Cyhalothrine;2013;Baulon;0.0;");

        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (0.5 kg/ha);;non;2013;Substances actives spécifiques;Quantité de la substance active Boscalid;0.25;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (678.0 L/ha);;non;2013;Substances actives spécifiques;Quantité de la substance active Boscalid;339.0;100;;;");

        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;Intervention 5;Substances actives spécifiques;Quantité de la substance active Boscalid;2013;Baulon;339.25;100;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Fluopyram;2013;Baulon;0.252;");
    }

    @Test
    public void testQuantitesSubstancesActivesRealise_12970() {
        testDatas.createEffectiveDataSetForQSAIndicatorsWithNewSubstances();

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);


        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        IndicatorActiveSubstanceAmount indicatorActiveSubstanceAmount = serviceFactory.newInstance(IndicatorActiveSubstanceAmount.class);
        indicatorActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorActiveSubstanceAmount);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Bixafen;2013;Baulon;4.32;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Dicamba;2013;Baulon;0.96075;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Mancozeb;2013;Baulon;1.1529;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Phosmet;2013;Baulon;8.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Dimethenamid-P;2013;Baulon;0.75;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Pendimethalin;2013;Baulon;0.05;");

        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Tebuconazole;2013;Baulon;0.025;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Flufenacet;2013;Baulon;0.35;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Aclonifen;2013;Baulon;0.05;");

        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG_2 (0.5 kg/ha);;non;2013;Substances actives spécifiques;Quantité de la substance active Beflutamid;0.25;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG_2 (0.5 kg/ha);;non;2013;Substances actives spécifiques;Quantité de la substance active Isoproturon;0.225;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG_2 (678.0 L/ha);;non;2013;Substances actives spécifiques;Quantité de la substance active Beflutamid;339.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG_2 (678.0 L/ha);;non;2013;Substances actives spécifiques;Quantité de la substance active Isoproturon;305.1;100;;;");

        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Isoxaben;2013;Baulon;0.252;");
    }

    @Test
    public void testQuantitesSubstancesActivesSynthetise_12307() throws IOException {
        testDatas.createPracticedDataSetForQSAIndicators();

        /*
         * Calcul des indicateurs
         */

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute synthétisé
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        IndicatorActiveSubstanceAmount indicatorActiveSubstanceAmount = serviceFactory.newInstance(IndicatorActiveSubstanceAmount.class);
        indicatorActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorActiveSubstanceAmount);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Glyphosate;0.864;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active S-métolachlore;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Prosulfocarbe;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Chlortoluron;0.9;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Diflufenican;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Lambda-Cyhalothrine;0.055;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Boscalid;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Fluopyram;0.0;");
    }

    @Test
    public void testQuantitesSubstancesActivesSynthetise_12970() throws IOException {
        testDatas.createPracticedDataSetForQSAIndicatorsWithNewSubstances();

        /*
         * Calcul des indicateurs
         */

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute synthétisé
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        IndicatorActiveSubstanceAmount indicatorActiveSubstanceAmount = serviceFactory.newInstance(IndicatorActiveSubstanceAmount.class);
        indicatorActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorActiveSubstanceAmount);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Bixafen;0.864;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Dicamba;0.02;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Mancozeb;0.9;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Phosmet;0.36;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Pendimethalin;0.055;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Flufenacet;0.048;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité de la substance active Aclonifen;0.144;");
    }

    @Test
    public void testQuantiteGlyphosateUniteCompatible_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductLongrunLHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorActiveSubstancesAmount(produitLongrun, domainPhytoProductLongrunLHa, 3, "Quantité de la substance active Glyphosate;0.864;");
    }

    @Test
    public void testQuantiteGlyphosateUniteNonCompatible_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductLongrunUniteHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.UNITE_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorActiveSubstancesAmount(produitLongrun, domainPhytoProductLongrunUniteHa, 3, "Quantité de la substance active Glyphosate;0.0;");
    }

    @Test
    public void testQuantiteGlyphosateUniteCompatibleDifferente_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductLongrunGHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.G_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorActiveSubstancesAmount(produitLongrun, domainPhytoProductLongrunGHa, 3000, "Quantité de la substance active Glyphosate;0.864;");
    }

    @Test
    @Disabled("Test à écrire une fois que la prise en compte des doses de référence est implémentée")
    void testQuantitesSubstancesActivesAvecDosesReference() {
        // TODO
    }

}
