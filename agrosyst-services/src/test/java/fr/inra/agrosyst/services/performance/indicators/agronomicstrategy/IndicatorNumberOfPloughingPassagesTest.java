package fr.inra.agrosyst.services.performance.indicators.agronomicstrategy;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public class IndicatorNumberOfPloughingPassagesTest extends AbstractAgronomicStrategyTest {

    @Test
    public void testNumberOfPloughingPassagesRealise_12343() throws IOException {
        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        List<Zone> zones = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create all intervention
        RefInterventionAgrosystTravailEDI labour = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEP").findAny();
        RefInterventionAgrosystTravailEDI travailDeSurface = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("V96").findAny();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));
        final ToolsCoupling travailSolTc = toolsCouplingByName.get("travailsol");
        final ToolsCoupling bineuse = toolsCouplingByName.get("Bineuse");
        final ToolsCoupling cultivateur = toolsCouplingByName.get("Cultivateur");

        var datas = List.of(
                new EffectiveTestData(travailSolTc, labour, 0.5d, 1, "0.5", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.66d, 1, "0.66", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.5d, 1, "0.0", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.66d, 1, "0.0", null, 100),
                new EffectiveTestData(cultivateur, travailDeSurface, 0.45d, 1, "0.0", null, 100),
                new EffectiveTestData(cultivateur, travailDeSurface, 0.45d, 1, "0.0", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.25d, 1, "0.25", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.75d, 1, "0.75", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.25d, 1, "0.0", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.75d, 1, "0.0", null, 100),
                new EffectiveTestData(travailSolTc, labour, 1.0d, 1, "1.0", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.33d, 1, "0.33", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 1.0d, 1, "0.0", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.33d, 1, "0.0", null, 100),
                new EffectiveTestData(travailSolTc, labour, 1.0d, 1, "1.0", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.45d, 1, "0.45", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.5d, 1, "0.5", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 1.0d, 1, "0.0", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.45d, 1, "0.0", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.45d, 1, "0.45", null, 100),
                new EffectiveTestData(travailSolTc, labour, 0.45d, 1, "0.45", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.66d, 1, "0.0", null, 100),
                new EffectiveTestData(bineuse, travailDeSurface, 0.75d, 1, "0.0", null, 100),
                new EffectiveTestData(cultivateur, travailDeSurface, 0.33d, 1, "0.0", null, 100),
                new EffectiveTestData(cultivateur, travailDeSurface, 0.1d, 1, "0.0", null, 100));

        for (var i = 0; i < datas.size(); i++) {
            var data = datas.get(i);
            createEffectiveIntervention(
                    null, cropCyclePhase,
                    data.toolsCoupling(),
                    data.mainAction(),
                    "intervention_" + i,
                    LocalDate.of(2023, 9, 15),
                    LocalDate.of(2023, 9, 15),
                    data.spatialFrequency(),
                    data.transitCount()
            );
        }

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorNumberOfPloughingPassages indicatorNumberOfPloughingPassages = serviceFactory.newInstance(IndicatorNumberOfPloughingPassages.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorNumberOfPloughingPassages);
        final String content = out.toString();

        for (var i = 0; i < datas.size(); i++) {
            var data = datas.get(i);
            assertEffectiveIntervention(content, "intervention_" + i, "Nombre de passages de labour", data.result(), String.valueOf(data.completion()));
        }
    }

    @Test
    public void testNumberOfPloughingPassagesSynthetise_12343() throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create all intervention
        RefInterventionAgrosystTravailEDI labour = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEP").findAny();
        RefInterventionAgrosystTravailEDI travailDeSurface = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("V96").findAny();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));
        final ToolsCoupling travailSolTc = toolsCouplingByName.get("travailsol");
        final ToolsCoupling bineuse = toolsCouplingByName.get("Bineuse");
        final ToolsCoupling cultivateur = toolsCouplingByName.get("Cultivateur");

        var datas = List.of(
                new PracticedTestData(cultivateur, travailDeSurface, 0.33d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.66d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.75d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.33d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 1.0d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.45d, 1.0d, "0.0", null),
                new PracticedTestData(travailSolTc, labour, 0.66d, 1.0d, "0.66", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.5d, 1.0d, "0.0", null),
                new PracticedTestData(travailSolTc, labour, 0.25d, 1.0d, "0.25", null),
                new PracticedTestData(travailSolTc, labour, 0.75d, 1.0d, "0.75", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.25d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.66d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.25d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.5d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.66d, 1.0d, "0.0", null),
                new PracticedTestData(travailSolTc, labour, 1.0d, 1.0d, "1.0", null),
                new PracticedTestData(travailSolTc, labour, 0.33d, 1.0d, "0.33", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.75d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 1.0d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.75d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 1.0d, 1.0d, "0.0", null),
                new PracticedTestData(travailSolTc, labour, 1.0d, 1.0d, "1.0", null),
                new PracticedTestData(travailSolTc, labour, 0.45d, 1.0d, "0.45", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.33d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 1.0d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.33d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 1.0d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.45d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.5d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.45d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.25d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 1.0d, 1.0d, "0.0", null),
                new PracticedTestData(travailSolTc, labour, 0.66d, 1.0d, "0.66", null),
                new PracticedTestData(travailSolTc, labour, 0.25d, 1.0d, "0.25", null),
                new PracticedTestData(bineuse, travailDeSurface, 0.75d, 1.0d, "0.0", null),
                new PracticedTestData(bineuse, travailDeSurface, 1.0d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 0.33d, 1.0d, "0.0", null),
                new PracticedTestData(cultivateur, travailDeSurface, 1.0d, 1.0d, "0.0", null)
        );

        for (var i = 0; i < datas.size(); i++) {
            var data = datas.get(i);
            createPracticedIntervention(
                    bleConnection,
                    bleTendreHiver,
                    data.toolsCoupling(),
                    data.mainAction(),
                    "intervention_" + i,
                    "15/09",
                    "15/09",
                    data.spatialFrequency(),
                    data.temporalFrequency()
            );
        }

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorNumberOfPloughingPassages indicatorNumberOfPloughingPassages = serviceFactory.newInstance(IndicatorNumberOfPloughingPassages.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorNumberOfPloughingPassages);
        final String content = out.toString();

        for (var i = 0; i < datas.size(); i++) {
            var data = datas.get(i);
            assertPracticedIntervention(content, "intervention_" + i, "Nombre de passages de labour", data.result(), "100");
        }
    }
}
