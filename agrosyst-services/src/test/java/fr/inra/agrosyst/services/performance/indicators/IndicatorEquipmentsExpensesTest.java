package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainFuelInput;
import fr.inra.agrosyst.api.entities.FuelUnit;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilterImpl;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import lombok.Setter;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author cosse (cosse@codelutin.com)
 */
public class IndicatorEquipmentsExpensesTest extends AbstractAgrosystTest {

    @Setter
    protected ImportService importService;
    protected PerformanceServiceImplForTest performanceService;
    protected DomainService domainService;
    protected GrowingSystemService growingSystemService;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);

        importService = serviceFactory.newService(ImportService.class);
        domainService = serviceFactory.newService(DomainService.class);
        growingSystemService = serviceFactory.newService(GrowingSystemService.class);
        testDatas = serviceFactory.newInstance(TestDatas.class);
        testDatas.setGson(serviceContext.getGson());

        // required referential
        testDatas.importRefEspeces();

        loginAsAdmin();
        alterSchema();
    }

    /*
    voire fichier test: refs #10316
                                                             | ETA/CUMA | Utilisation  type | Utilisation réelle (estimée) |Prix  d'achat  (Euros) | Puissance (chISO) | DonnéeAmmortissement 1 | DonnéeAmmortissement 2 | Tx ammortissement global (%) | C (L/ch) | Tx charge (%) | Débit chantier (Ha/h)                         | Réparation coût | Rép. unité  | Pneu cout | Pneu Unité | Lubrifiant coût | Lub. Unité  |
    Tracteur	 Classique  catég.  B.,  130  ch 	         |   non    | 500  h/an 	    |550	                       | 65750                 | 120               | 14                     | 0,09                   | 7,4                          | 0,24     |      	       |  	                                           | 2,7             | Euro/h 	   |  1,326    |  Euro/h    | 0,397	          | Euro/h      |
    Charrue	     5  corps  portée,  boulon  de  rupture      |   non    | 150  ha/an        |50	                           | 13250                 | 	               | 10                     | 0,1 	                 | 8,11                         |   	   | 80 	       | 0,9 	                                       | 7,47            | Euros/ha    |           |            |                 |             |
    Déchaumeur	 Rapide,  4m,  disques  indé,  chassis  fixe |   non    | 250  ha/an        |200	                       | 22100                 | 	               | 7                      | 0,15                   | 11,3                         |   	   | 80 	       | 3,5                                           | 3,03            | Euros/ha    |           |            |                 |             |
    Semoir 3m, Cannelures, gravitaire et à socs              |   non    | 120 ha/an         |150                           | 6500                  |                   | 10                     | 0,12                   | 8,71                         |          | 40            | 1,4                                           | 1,06            | Euros/ha    |           |            |                 |             |
    Pulvérisateur Porté, 1500 L, 24m DPAE                    |   non    | 600 ha/an         |550                           | 33600                 |                   | 10                     | 0,12                   | 8,71                         |          | 40            | 6                                             | 1,03            | Euros/ha    |           |            |                 |             |
    Moissonneuse 190 à 210 ch, 5 secoueurs, coupe 5m         |   oui    | 160 ha/an         |150                           | 131800                | 200               | 15                     | 0,1                    | 7,35                         | 0,21     | 70            | 1,33                                          | 13,85           | Euros/ha    |           |            | 0,56            | Euro/ha     |
    Rouleau Spire 4m                                         |   non    | 125 ha/an         |100                           | 7750                  |                   | 10                     | 0,1                    | 8,11                         |          | 40            | 2,5                                           | 0,65            | Euros/ha    |           |            |                 |             |
    Epandeur Fumier 16 à 18 t, 20m3, 2 hérissons, 3 essieux  |   non    | 400 Voy/an        |450                           | 47100                 |                   | 7                      | 15                     | 11,3                         |          | 40            | 3 Voy/h, 17 m3/Voy, 20 t/ha de fumier apporté | 1,11            | Euros/Voy   |           |            |                 |             |
    Presse  chambre variable 120 x 160                       |   non    | 1200 bal/an       |1300                          | 32200                 |                   | 10                     | 0,15                   | 9,4                          |          | 80            | 30 Bal/h                                      | 0,53            | Euros/bal   |           |            |                 |             |

    Calculs	                                      | Labour	| Déchaumage | Semoir  | Rouleau + semoir                  | Pulvérisateur | Moisson                              | Rouleau  | Epandage fumier | Pressage
    Charges fixes 	                              | 20,58	| 12,52	     | 10,09   | 15,12                             | 6,80          | 48,44                                | 8,57     | 15,30           | 9,08        Euros par ha
    Coût carburant                                | 16,64	| 4,28	     | 5,35    | 5,35                              | 1,25          | 14,37                                | 3,00     | 2,94            | 7,49        Euros par ha
    Coût réparation 	                          | 10,47	| 3,80	     | 2,99    | 3,64                              | 1,48          | 13,85                                | 1,73     | 2,36            | 9,30        Euros par ha
    Cout lubrifiant 	                          | 0,44	| 0,11	     | 0,28    | 0,28                              | 0,07          | 0,56                                 | 0,16     | 0,16            | 0,20        Euros par ha
    Cout pneumatiques 	                          | 1,47	| 0,38	     | 0,95    | 0,95                              | 0,22          | 0,00                                 | 0,53     | 0,52            | 0,66        Euros par ha
    Coût intervention  (hors  main  d'œuvre) 	  | 47,81	| 21,09	     | 19,66   | 25,34                             | 9,81          | 77,21                                | 13,98    | 21,27           | 26,73       Euros par ha
    Temps de travail (=d'utilisation du matériel) | 1,11    | 0,29       | 0,71    | 0,71                              | 0,17          | 0,75                                 | 0,40     | 0,39            | 0,50        h/ha
    Consommation de carburant                     | 25,6    | 6,58       | 8,23    | 8,23                              | 1,92          | 22,11                                | 4,61     | 4,52            | 11,52       L/ha
                                                                                     Utilisation réelle du rouleau plus                  Matériel en CUMA.                                                   15 balles à presser par ha
                                                                                     faible que la plus faible valeur                    Le temps d'utilisation est 200 ha,
                                                                                     du référentiel                                      valeur la plus élevée du référentiel
    */
    @Test
    public void testInraResult() throws IOException {

        RefSolTextureGeppaTopiaDao refSolTextureGeppaDao = getPersistenceContext().getRefSolTextureGeppaDao();
        refSolTextureGeppaDao.create(
                RefSolTextureGeppa.PROPERTY_ABBREVIATION_CLASSES_TEXTURALES__GEPAA, "LAS",
                RefSolTextureGeppa.PROPERTY_CLASSES_TEXTURALES__GEPAA, "limon argilo-sableux",
                RefSolTextureGeppa.PROPERTY_ABBREVIATION__INDIGO, "AL",
                RefSolTextureGeppa.PROPERTY_CLASSE__INDIGO, "argilo-limoneux",
                RefSolTextureGeppa.PROPERTY_TEXTURE_IPHY, "argileux",
                RefSolTextureGeppa.PROPERTY_ACTIVE, true
        );

        RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao = getPersistenceContext().getRefSolProfondeurIndigoDao();
        refSolProfondeurIndigoDao.create(
                RefSolProfondeurIndigo.PROPERTY_CLASSE_DE_PROFONDEUR__INDIGO, "Profond",
                RefSolProfondeurIndigo.PROPERTY_PROFONDEUR, "90-120 cm",
                RefSolProfondeurIndigo.PROPERTY_LIBELLE_CLASSE, "Profond (90-120 cm )",
                RefSolProfondeurIndigo.PROPERTY_PROFONDEUR_PAR_DEFAUT, 105,
                RefSolProfondeurIndigo.PROPERTY_ACTIVE, true
        );

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/AGS_20190122_REF_Ammortissement.csv")){
            importService.importAgsAmortissement(stream);
        }

        Domain domainTest2018 = testDatas.createPerformanceINRADomain(2018);

        GrowingPlan growingPlanTest2018 = testDatas.createINRAGrowingPlan(domainTest2018);

        GrowingSystem growingSystem2018 = testDatas.createINRAGrowingSystem(growingPlanTest2018);

        PracticedSystem practicedSystem = testDatas.createPracticedSystemInraContextRef10210(domainTest2018, growingSystem2018);

        serviceContext.getPersistenceContext().commit();

        getPersistenceContext().getRefPrixCarbuDao().create(
                RefPrixCarbu.PROPERTY_CAMPAIGN, 2018,
                RefPrixCarbu.PROPERTY_PRICE, 0.75,
                RefPrixCarbu.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefPrixCarbu.PROPERTY_ACTIVE, true
        );

        String objectId = InputPriceService.GET_FUEL_OBJECT_ID.apply(2018);
        var price = getPersistenceContext().getInputPriceDao().create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domainTest2018,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.FUEL_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "fuel",
                InputPrice.PROPERTY_PRICE, 0.65,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L
        );
        getPersistenceContext().getDomainFuelInputDao().create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.CARBURANT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domainTest2018,
                DomainFuelInput.PROPERTY_INPUT_NAME, "fuel",
                DomainFuelInput.PROPERTY_USAGE_UNIT, FuelUnit.L_HA,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, price,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        IndicatorToolUsageTime indicatorToolUsageTime = serviceFactory.newInstance(IndicatorToolUsageTime.class);
        indicatorToolUsageTime.setLocale(Language.FRENCH.getLocale());
        indicatorToolUsageTime.setTranslatedMonth(performanceService.getTranslatedMonths());
        IndicatorFuelConsumption indicatorFuelConsumption = serviceFactory.newInstance(IndicatorFuelConsumption.class);
        indicatorFuelConsumption.setLocale(Language.FRENCH.getLocale());
        IndicatorEquipmentsExpenses indicatorEquipmentsExpenses = serviceFactory.newInstance(IndicatorEquipmentsExpenses.class);
        indicatorEquipmentsExpenses.setLocale(Language.FRENCH.getLocale());

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Lists.newArrayList(domainTest2018.getTopiaId()),
                Lists.newArrayList(practicedSystem.getGrowingSystem().getTopiaId()),
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute practiced
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorToolUsageTime, indicatorFuelConsumption, indicatorEquipmentsExpenses);
        String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Test charges Méca_NMJ (2018);;;Test_Charges_Méca;;Agriculture conventionnelle;Test_Charges_Méca;Blé (rang 2);;Blé tendre;Camp remy;Blé tendre;Camp remy;Colza;Travail du sol;Labour;labourInterventionOnBle;01/07;01/07;Travail du sol;;0;;;non;;2018;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);49.601;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Test charges Méca_NMJ (2018);;;Test_Charges_Méca;;Agriculture conventionnelle;Test_Charges_Méca;Blé (rang 2);;Blé tendre;Camp remy;Blé tendre;Camp remy;Colza;Travail du sol;Labour;labourInterventionOnBle;01/07;01/07;Travail du sol;;0;;;non;;2018;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);52.161;100;;;");
    }

    /*
     voire fichier test: refs #12367

     Sdc nom: Systeme de culture Baulon
     Synthétisé nom: PS: Systeme de culture Baulon 1 2013

     Rotation:
     rang0   | rang1           | rang2 | rang3
     colza   | pois (absente)  | blé   | orge

     Chemins:
     nom_connection | culture source | culture cible | poids
     connColzaPois1 | colza          | pois          | 0,25
     connPoisBle    | pois           | blé           | 0,6

     RefmaterielOutils:
     typemateriel1                    | typemateriel2                                                | typemateriel3          | typemateriel4              | uniteparan   | unite          active          idtypemateriel          codetype          idsoustypemateriel          commentairesurmateriel          millesime          coderef          prixneufunite          prixmoyenachat          chargesfixesparan          chargesfixesparunitedevolumedetravailannuelunite          chargesfixesparunitedevolumedetravailannuel          energieunite          coutenergieparunitedetravail          couttotalunite          couttotalparunitedetravailannuel          code_materiel_gestim          masse          duree_vie_theorique          codeedi          source          chargesfixesannuelleunite          reparationsunite          reparationsparunitedetravailannuel          carburantcoutunite          carburantparunitedetravail          lubrifiantcoutunite          lubrifiantparunitedetravail          couttotalaveccarburantparunitedetravailannuel          couttotalsanscarburantparunitedetravailannuel          puissancechiso          volumecarterhuilemoteurunite          volumecarterhuilemoteur          performanceunite          performance          performancecouttotalunite          performancecouttotalaveccarburantparh          performancecouttotalsanscarburantparh          donneestauxdechargemoteur          donneesamortissement1          donneesamortissement2          donneestransport1unite          donneestransport1          donneestransport2unite          donneestransport2          donneestransport3unite          donneestransport3          puissancechisounite          pneuscoutunite          pneusparunitedetravail          volumecarterhuilebvunite          volumecarterhuilebv          pneusavtaille          pneusavprix          pneusavprixunite          pneusavdureevieunite          pneusavdureevie          pneusartaille          pneusarprixunite          pneusarprix          pneusardureevieunite          pneusardureevie          performancecouttotal          donneespuissance1adequateunite          donneespuissance1adequate          donneestauxdechargemoteur          petitmateriel
     EPANDEURS & ACCESSOIRES          | Epandeur lisier 15,5 m3 Standard + Rampe à pendillards 12 m  | Tonne 15,5 m3 Standard |  Rampe de 12 m pendillards | 700.0        | Voy            VRAI            EPANDEURS & ACCESSOIRES          40          15.5 m3 + rampe à pendillards 12 M          Epandeur lisier 15.5 m3 Standard + Rampe à pendillards 12 mTonne 15.5 m3 Standard Rampe de 12 m pendillards          2018          4023001623          €          70500.0          7966.62723518973          €/voy          11.380896050271                              €/voy          13.700896050271          GES5          7350.0          14                    BCMA 2018          €/an          €/voy          2.32        VOY_H          2.0          € / h                                        0.07          0.15          m          12          M3          15.5          mn          10                                                                                                                                                                27.4017921005421          ch                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                150.0                              0.4                                0

     Outils:
     type            nom                 Type materiel 1          type materiel 2                                                type materiel 2          Type materiel 4           utilisationannuelle          TauxDeChargeMoteur          puissance
     outils          charrue             CHARRUES                 3 corps,                                                       portée,                  boulon de rupture         40 ha                        0,8                         90
     tracteur        tracteur classique  TRACTEURS CLASSIQUES     Catégorie B = Catégorie A + suspension du pont avant de série  106 à 115 ch             110 ch                    500 h                        0,35                        110
     outils          pulverisateur       Pulvériseur Viti         Pulvériseur Viti VL                                            20 disques, porté        0                         30 ha                        0,4                         70
     outils          epandeur            EPANDEURS & ACCESSOIRES  Epandeur lisier 15,5 m3 Standard + Rampe à pendillards 12 m    Tonne 15,5 m3 Standard   Rampe de 12 m pendillards 700.0                        0,4                         150

     Combinaison outils:
     comb_nom          action                                    tracteur_nom                outil_nom          debit_chantier          debit_chantier_unit          volvoy_m3voy                    TauxDeChargeMoteur          puissance          calcul_conso_ref
     travailsol        Labour                                    tracteur classique          charrue            0,9                     ha_h                                                         0,4                         110                0,24
     pulverisation     Application de produits avec AMM          tracteur classique          pulverisateur      2                       ha_h                                                         0,4                         110                0,24
     epandeur          Epandage organique                        tracteur classique          epandeur           2                       voy_h                        10                              0,4                         110                0,24

     Intrants
     type             nom_intrant               nom_usage                 unite_application          compo
     engrais          Fumier de bovins          Fumier de bovins          m3/ha                      N : 5 ; P2O5 : 2; K2O : 8

     Interventions USAGE
     culture                    nom_connection       nom_intervention             combinaison_outils          fq_spatiale          fq_temporelle          type_produit                  debit_chantier          debit_chantier_unit          nom_produit          proportion_surface_traitee          dose_applique          unite          Temps_utilisation_materiel (h/ha)          Consommation_carburant (l/ha)    Commentaire
     blé tendre d'hiver         connPoisBle          travail sol charrue          travailsol                  1                    1                       -                            -                       -                            -                    -                                   -                      -              1,111111111                                11,73333333
     blé tendre d'hiver         connPoisBle          travail sol charrue 2        travailsol                  1                    1                       -                            0,006666666667          ha_h                         -                    -                                   -                      -              150                                        1584                             On prend en priorité le debit de chantier de l'intervention. Si pas renseigné, prendre le debit de chantier de la comb outils
     blé tendre d'hiver         connPoisBle          traitement phyto             pulverisation               1                    1                       phyto                        -                       -                            Atlantis WG          100                                 500                    g/ha           0,5                                        5,28
     blé tendre d'hiver         connPoisBle          Epandage                     epandeur                    1                    1                       engrais/amendementorganique  -                       -                            Fumier de bovins     100                                 30                     m3/ha          150                                        1584


*/
    @Test
    public void testInraFuelConsumptionResult() throws IOException {

        GrowingSystem baulonSubSystem = testDatas.createBaulonSubSystem(false, 2013);
        // pour ne pas créer tous les autres domains
        PracticedSystem practicedSystem = testDatas.createINRATestPraticedSystems(baulonSubSystem, null);
        Domain domainTest2013 = practicedSystem.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domainTest2013.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        testDatas.createEquipmentsAndToolsCouplingForDomain(domainTest2013);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        practicedSystem.setCampaigns(psCampaings);
        practicedSystem = psdao.update(practicedSystem);

        //     *     • Colza     : 1 traitement phytosanitaire avec Colzor Trio, 2 L/ha
        //     *     • Féverole  : 1 traitement phytosanitaire avec Kerb Flo, 1 L/ha
        //     *     • Blé       : 1 traitement phytosanitaire avec Atlantis WG, 500 g/ha
        //     *     • Triticale : 1 traitement phytosanitaire avec Atlantis WG, 250 g/ha
        //     *     • Orge      : 1 traitement phytosanitaire avec Bofix, 3 L/ha
        //     *
        //     *     id_produit	nom_produit	id_traitement	code_traitement	nom_traitement	                                                            NODU 2013	Source
        //     *     5194	        COLZOR TRIO	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     3690	        KERB FLO	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     5167	        ATLANTIS WG	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     5091	        BOFIX	    147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.addColzaPoisFeveroleTriticaleCropsToDomain(domainTest2013, testDatas.findCroppingPlanEntries(domainTest2013.getTopiaId()));

        testDatas.createPracticedSeasonalCropCycleWithNotUseCropInCycle(croppingPlanEntries, practicedSystem);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/AGS_20190122_REF_Ammortissement.csv")){
            importService.importAgsAmortissement(stream);
        }

        testDatas.createINRA_12298_PracticedInterventions(domainTest2013);

        serviceContext.getPersistenceContext().commit();

        getPersistenceContext().getRefPrixCarbuDao().create(
                RefPrixCarbu.PROPERTY_CAMPAIGN, 2013,
                RefPrixCarbu.PROPERTY_PRICE, 0.723404255319149,
                RefPrixCarbu.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefPrixCarbu.PROPERTY_ACTIVE, true
        );

        String objectId = InputPriceService.GET_FUEL_OBJECT_ID.apply(2013);
        var price = getPersistenceContext().getInputPriceDao().create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domainTest2013,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.FUEL_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "fuel",
                InputPrice.PROPERTY_PRICE, 0.65,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L
        );
        getPersistenceContext().getDomainFuelInputDao().create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.CARBURANT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domainTest2013,
                DomainFuelInput.PROPERTY_INPUT_NAME, "fuel",
                DomainFuelInput.PROPERTY_USAGE_UNIT, FuelUnit.L_HA,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, price,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );


        IndicatorToolUsageTime indicatorToolUsageTime = serviceFactory.newInstance(IndicatorToolUsageTime.class);
        indicatorToolUsageTime.setLocale(Language.FRENCH.getLocale());
        indicatorToolUsageTime.setTranslatedMonth(performanceService.getTranslatedMonths());
        IndicatorFuelConsumption indicatorFuelConsumption = serviceFactory.newInstance(IndicatorFuelConsumption.class);
        indicatorFuelConsumption.setLocale(Language.FRENCH.getLocale());

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Lists.newArrayList(domainTest2013.getTopiaId()),
                Lists.newArrayList(practicedSystem.getGrowingSystem().getTopiaId()),
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute practiced
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorToolUsageTime, indicatorFuelConsumption);
        String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Travail du sol;travail sol charrue;travail_sol_charrue_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Résultats socio-techniques;Consommation de carburant (l/ha);23.467;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Travail du sol;travail sol charrue 2;travail_sol_charrue_2_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Résultats socio-techniques;Consommation de carburant (l/ha);3168.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);traitement phyto;traitement_phyto_id;25/08;25/08;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);;0;;;non;;2012, 2013;Synthétisé;Résultats socio-techniques;Consommation de carburant (l/ha);5.28;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Epandage;epandage_id;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Résultats socio-techniques;Consommation de carburant (l/ha);15.84;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Travail du sol;travail sol charrue;connBleOrge_travail_sol_charrue_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Résultats socio-techniques;Consommation de carburant (l/ha);14.08;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Travail du sol;Epandage;connBleOrge_epandage_id;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Résultats socio-techniques;Consommation de carburant (l/ha);9.504;100;;;");
    }



    /*
     voire fichier test: refs #10316

     Sdc nom: Systeme de culture Baulon
     Synthétisé nom: PS: Systeme de culture Baulon 1 2013

     Rotation:
     rang0   | rang1           | rang2 | rang3
     colza   | pois (absente)  | blé   | orge

     Chemins:
     nom_connection | culture source | culture cible | poids
     connColzaPois1 | colza          | pois          | 0,25
     connPoisBle    | pois           | blé           | 0,6

     RefmaterielOutils:
     typemateriel1                    | typemateriel2                                                | typemateriel3          | typemateriel4              | uniteparan   | unite          active          idtypemateriel          codetype          idsoustypemateriel          commentairesurmateriel          millesime          coderef          prixneufunite          prixmoyenachat          chargesfixesparan          chargesfixesparunitedevolumedetravailannuelunite          chargesfixesparunitedevolumedetravailannuel          energieunite          coutenergieparunitedetravail          couttotalunite          couttotalparunitedetravailannuel          code_materiel_gestim          masse          duree_vie_theorique          codeedi          source          chargesfixesannuelleunite          reparationsunite          reparationsparunitedetravailannuel          carburantcoutunite          carburantparunitedetravail          lubrifiantcoutunite          lubrifiantparunitedetravail          couttotalaveccarburantparunitedetravailannuel          couttotalsanscarburantparunitedetravailannuel          puissancechiso          volumecarterhuilemoteurunite          volumecarterhuilemoteur          performanceunite          performance          performancecouttotalunite          performancecouttotalaveccarburantparh          performancecouttotalsanscarburantparh          donneestauxdechargemoteur          donneesamortissement1          donneesamortissement2          donneestransport1unite          donneestransport1          donneestransport2unite          donneestransport2          donneestransport3unite          donneestransport3          puissancechisounite          pneuscoutunite          pneusparunitedetravail          volumecarterhuilebvunite          volumecarterhuilebv          pneusavtaille          pneusavprix          pneusavprixunite          pneusavdureevieunite          pneusavdureevie          pneusartaille          pneusarprixunite          pneusarprix          pneusardureevieunite          pneusardureevie          performancecouttotal          donneespuissance1adequateunite          donneespuissance1adequate          donneestauxdechargemoteur          petitmateriel
     EPANDEURS & ACCESSOIRES          | Epandeur lisier 15,5 m3 Standard + Rampe à pendillards 12 m  | Tonne 15,5 m3 Standard |  Rampe de 12 m pendillards | 700.0        | Voy            VRAI            EPANDEURS & ACCESSOIRES          40          15.5 m3 + rampe à pendillards 12 M          Epandeur lisier 15.5 m3 Standard + Rampe à pendillards 12 mTonne 15.5 m3 Standard Rampe de 12 m pendillards          2018          4023001623          €          70500.0          7966.62723518973          €/voy          11.380896050271                              €/voy          13.700896050271          GES5          7350.0          14                    BCMA 2018          €/an          €/voy          2.32        VOY_H          2.0          € / h                                        0.07          0.15          m          12          M3          15.5          mn          10                                                                                                                                                                27.4017921005421          ch                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                150.0                              0.4                                0

     Outils:
     type            nom                 Type materiel 1          type materiel 2                                                type materiel 2          Type materiel 4           utilisationannuelle          TauxDeChargeMoteur          puissance
     outils          charrue             CHARRUES                 3 corps,                                                       portée,                  boulon de rupture         40 ha                        0,8                         90
     tracteur        tracteur classique  TRACTEURS CLASSIQUES     Catégorie B = Catégorie A + suspension du pont avant de série  106 à 115 ch             110 ch                    500 h                        0,35                        110
     outils          pulverisateur       Pulvériseur Viti         Pulvériseur Viti VL                                            20 disques, porté        0                         30 ha                        0,4                         70
     outils          epandeur            EPANDEURS & ACCESSOIRES  Epandeur lisier 15,5 m3 Standard + Rampe à pendillards 12 m    Tonne 15,5 m3 Standard   Rampe de 12 m pendillards 700.0                        0,4                         150

     Combinaison outils:
     comb_nom          action                                    tracteur_nom                outil_nom          debit_chantier          debit_chantier_unit          volvoy_m3voy                    TauxDeChargeMoteur          puissance          calcul_conso_ref
     travailsol        Labour                                    tracteur classique          charrue            0,9                     ha_h                                                         0,4                         110                0,24
     pulverisation     Application de produits avec AMM          tracteur classique          pulverisateur      2                       ha_h                                                         0,4                         110                0,24
     epandeur          Epandage organique                        tracteur classique          epandeur           2                       voy_h                        10                              0,4                         110                0,24

     Intrants
     type             nom_intrant               nom_usage                 unite_application          compo
     engrais          Fumier de bovins          Fumier de bovins          m3/ha                      N : 5 ; P2O5 : 2; K2O : 8

     Interventions USAGE
     culture                    nom_connection       nom_intervention             combinaison_outils          fq_spatiale          fq_temporelle          type_produit                  debit_chantier          debit_chantier_unit          nom_produit          proportion_surface_traitee          dose_applique          unite          Temps_utilisation_materiel (h/ha)          Consommation_carburant (l/ha)    Commentaire
     blé tendre d'hiver         connPoisBle          travail sol charrue          travailsol                  1                    1                       -                            -                       -                            -                    -                                   -                      -              1,111111111                                11,73333333
     blé tendre d'hiver         connPoisBle          travail sol charrue 2        travailsol                  1                    1                       -                            0,006666666667          ha_h                         -                    -                                   -                      -              150                                        1584                             On prend en priorité le debit de chantier de l'intervention. Si pas renseigné, prendre le debit de chantier de la comb outils
     blé tendre d'hiver         connPoisBle          traitement phyto             pulverisation               1                    1                       phyto                        -                       -                            Atlantis WG          100                                 500                    g/ha           0,5                                        5,28
     blé tendre d'hiver         connPoisBle          Epandage                     epandeur                    1                    1                       engrais/amendementorganique  -                       -                            Fumier de bovins     100                                 30                     m3/ha          150                                        1584
*/
    @Test
    public void testInraeEquipmentsExpensesResult_12426() throws IOException {

        GrowingSystem baulonSubSystem = testDatas.createBaulonSubSystem(false, 2013);
        // pour ne pas créer tous les autres domains
        PracticedSystem practicedSystem = testDatas.createINRATestPraticedSystems(baulonSubSystem, null);
        Domain domainTest2013 = practicedSystem.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domainTest2013.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        testDatas.createEquipmentsAndToolsCouplingForDomain(domainTest2013);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        practicedSystem.setCampaigns(psCampaings);
        practicedSystem = psdao.update(practicedSystem);

        //     *     • Colza     : 1 traitement phytosanitaire avec Colzor Trio, 2 L/ha
        //     *     • Féverole  : 1 traitement phytosanitaire avec Kerb Flo, 1 L/ha
        //     *     • Blé       : 1 traitement phytosanitaire avec Atlantis WG, 500 g/ha
        //     *     • Triticale : 1 traitement phytosanitaire avec Atlantis WG, 250 g/ha
        //     *     • Orge      : 1 traitement phytosanitaire avec Bofix, 3 L/ha
        //     *
        //     *     id_produit	nom_produit	id_traitement	code_traitement	nom_traitement	                                                            NODU 2013	Source
        //     *     5194	        COLZOR TRIO	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     3690	        KERB FLO	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     5167	        ATLANTIS WG	147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        //     *     5091	        BOFIX	    147	            H1              Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés)	N	        ACTA 2017
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.addColzaPoisFeveroleTriticaleCropsToDomain(domainTest2013, testDatas.findCroppingPlanEntries(domainTest2013.getTopiaId()));

        testDatas.createPracticedSeasonalCropCycleWithNotUseCropInCycleBis(croppingPlanEntries, practicedSystem);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/AGS_20190122_REF_Ammortissement.csv")){
            importService.importAgsAmortissement(stream);
        }

        testDatas.createINRA_12426_PracticedInterventions(domainTest2013);

        serviceContext.getPersistenceContext().commit();

        getPersistenceContext().getRefPrixCarbuDao().create(
                RefPrixCarbu.PROPERTY_CAMPAIGN, 2013,
                RefPrixCarbu.PROPERTY_PRICE, 0.723404255319149,
                RefPrixCarbu.PROPERTY_UNIT, PriceUnit.EURO_L,
                RefPrixCarbu.PROPERTY_ACTIVE, true
        );

        String objectId = InputPriceService.GET_FUEL_OBJECT_ID.apply(2013);
        var price = getPersistenceContext().getInputPriceDao().create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domainTest2013,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.FUEL_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "fuel",
                InputPrice.PROPERTY_PRICE, 0.75,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_L
        );
        getPersistenceContext().getDomainFuelInputDao().create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.CARBURANT,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domainTest2013,
                DomainFuelInput.PROPERTY_INPUT_NAME, "fuel",
                DomainFuelInput.PROPERTY_USAGE_UNIT, FuelUnit.L_HA,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, price,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        IndicatorToolUsageTime indicatorToolUsageTime = serviceFactory.newInstance(IndicatorToolUsageTime.class);
        indicatorToolUsageTime.setLocale(Language.FRENCH.getLocale());
        IndicatorFilter indicatorFilter = new IndicatorFilterImpl();
        indicatorFilter.setDetailedByMonth(false);
        indicatorToolUsageTime.init(indicatorFilter, performanceService.getTranslatedMonths());
        IndicatorFuelConsumption indicatorFuelConsumption = serviceFactory.newInstance(IndicatorFuelConsumption.class);
        indicatorFuelConsumption.setLocale(Language.FRENCH.getLocale());
        IndicatorEquipmentsExpenses indicatorEquipmentsExpenses = serviceFactory.newInstance(IndicatorEquipmentsExpenses.class);
        indicatorEquipmentsExpenses.setLocale(Language.FRENCH.getLocale());

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Lists.newArrayList(domainTest2013.getTopiaId()),
                Lists.newArrayList(practicedSystem.getGrowingSystem().getTopiaId()),
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute practiced
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorToolUsageTime, indicatorFuelConsumption, indicatorEquipmentsExpenses);
        String content = out.toString();

        // intervention 1 : travail_sol_charrue_id
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Travail du sol;travail sol charrue;travail_sol_charrue_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);48.8;83;Utilisation annuelle précise du matériel 'charrue 3 corps';;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Travail du sol;travail sol charrue;travail_sol_charrue_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);48.175;83;Utilisation annuelle précise du matériel 'charrue 3 corps';;");

        // intervention 2 : travail_sol_charrue_2_id
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Travail du sol;travail sol charrue 2;travail_sol_charrue_2_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);4633.894;83;Utilisation annuelle précise du matériel 'charrue 3 corps';;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Travail du sol;travail sol charrue 2;travail_sol_charrue_2_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);4549.639;83;Utilisation annuelle précise du matériel 'charrue 3 corps';;");

        // intervention 3 : traitement_phyto_id
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);traitement phyto;traitement_phyto_id;25/08;25/08;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATLANTIS WG (0.5 kg/ha);1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);27.515;83;Utilisation annuelle précise du matériel 'Pulvériseur Viti';;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);traitement phyto;traitement_phyto_id;25/08;25/08;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATLANTIS WG (0.5 kg/ha);1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);27.375;83;Utilisation annuelle précise du matériel 'Pulvériseur Viti';;");

        // intervention 4 : epandage_id
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Epandage;epandage_id;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);74.142;87;Utilisation annuelle précise du matériel 'epandeur';;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 3);;Blé tendre;;;;Pois;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Epandage;epandage_id;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);73.721;87;Utilisation annuelle précise du matériel 'epandeur';;");

        // intervention 5 : connBleOrge_travail_sol_charrue_id
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Travail du sol;travail sol charrue;connBleOrge_travail_sol_charrue_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);25.292;83;Utilisation annuelle précise du matériel 'charrue 3 corps';;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Travail du sol;travail sol charrue;connBleOrge_travail_sol_charrue_id;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);24.917;83;Utilisation annuelle précise du matériel 'charrue 3 corps';;");

        // intervention 6 : connBleOrge_epandage_id_sans_dose
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Epandage sans dose;connBleOrge_epandage_id_sans_dose;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);26.067;87;Utilisation annuelle précise du matériel 'epandeur';;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Epandage sans dose;connBleOrge_epandage_id_sans_dose;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);25.899;87;Utilisation annuelle précise du matériel 'epandeur';;");

        // intervention 7 : connBleOrge_epandage_id_sans_volume
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Epandage sans volume;connBleOrge_epandage_id_sans_volume;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);40.077;75;Volume par voyage, Utilisation annuelle précise du matériel 'epandeur';;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Epandage sans volume;connBleOrge_epandage_id_sans_volume;25/08;25/08;Épandage organique;Fumier de bovins (N: 5.0,P₂O₅: 2.0,K₂O: 8.0,CaO: ,MgO: ,S: );1;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);39.849;75;Volume par voyage, Utilisation annuelle précise du matériel 'epandeur';;");

        // intervention 8 : connBleOrge_travail_sol_charrue_id_sans_debit
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Travail du sol;travail sol charrue sans debit;connBleOrge_travail_sol_charrue_id_sans_debit;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation réelles (€/ha);45.378;66;Utilisation annuelle précise du matériel 'charrue 3 corps', Débit de chantier, le calcul est fait avec la valeur de 1 ha/h par défaut;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Orge (rang 4);;Blé tendre, Orge;;;;Blé;Travail du sol;travail sol charrue sans debit;connBleOrge_travail_sol_charrue_id_sans_debit;25/08;25/08;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Indicateur économique;Charges de mécanisation standardisées (€/ha);44.816;66;Utilisation annuelle précise du matériel 'charrue 3 corps', Débit de chantier, le calcul est fait avec la valeur de 1 ha/h par défaut;;");
    }
}
