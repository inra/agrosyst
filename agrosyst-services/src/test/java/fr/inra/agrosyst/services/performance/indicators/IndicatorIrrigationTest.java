package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationActionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Voir {@link IndicatorIrrigation} pour la formule de calcul de l'indicateur.
 */
public class IndicatorIrrigationTest extends AbstractAgrosystTest {

    protected DomainService domainService;
    protected PerformanceServiceImplForTest performanceService;
    protected EffectiveCropCycleService effectiveCropCycleService;

    protected DomainTopiaDao domainDao;
    protected ZoneTopiaDao zoneTopiaDao;

    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDAO;
    protected EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDao;

    protected PracticedCropCyclePhaseTopiaDao practicedCropCyclePhasesDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeTopiaDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionTopiaDao;

    protected IrrigationActionTopiaDao irrigationActionTopiaDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDIDAO;

    protected Domain baulon;
    protected RefCountry france;
    protected Collection<IndicatorFilter> indicatorFilters;


    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        testDatas = serviceFactory.newInstance(TestDatas.class);

        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);

        domainService = serviceFactory.newService(DomainService.class);

        // init dao
        domainDao = serviceContext.getPersistenceContext().getDomainDao();
        zoneTopiaDao = serviceContext.getPersistenceContext().getZoneDao();
        practicedCropCyclePhasesDao = serviceContext.getPersistenceContext().getPracticedCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = serviceContext.getPersistenceContext().getEffectivePerennialCropCycleDao();
        practicedSeasonalCropCycleDao = serviceContext.getPersistenceContext().getPracticedSeasonalCropCycleDao();
        effectiveInterventionDAO = serviceContext.getPersistenceContext().getEffectiveInterventionDao();
        practicedInterventionDao = serviceContext.getPersistenceContext().getPracticedInterventionDao();
        practicedSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getPracticedSpeciesStadeDao();
        refActionAgrosystTravailEDIDAO = serviceContext.getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        practicedCropCycleNodeDao = serviceContext.getPersistenceContext().getPracticedCropCycleNodeDao();
        practicedCropCycleConnectionTopiaDao = serviceContext.getPersistenceContext().getPracticedCropCycleConnectionDao();
        effectiveCropCyclePhaseDao = serviceContext.getPersistenceContext().getEffectiveCropCyclePhaseDao();

        irrigationActionTopiaDao = serviceContext.getPersistenceContext().getIrrigationActionDao();

        testDatas.createTestDomains();

        // login
        loginAsAdmin();
        alterSchema();

        indicatorFilters = performanceService.getAllIndicatorFilters();

        // initialisation des paramètres de test
        this.baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        this.france = baulon.getLocation().getRefCountry();

        testDatas.createTestPlots();
        testDatas.importRefCompositionSubstancesActivesParNumeroAMM();
        testDatas.importRefSubstancesActivesCommissionEuropeenne();
        testDatas.importPhrasesRisqueEtClassesMentionDangerParAmm();
        testDatas.importRefConversionUnitesQSA();
        testDatas.importActaTraitementsProduits();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaGroupeCultures();
    }

    @Test
    public void testsIrrigationRealise_12357() {
        this.testEnRealise(15.0, 1.0, 1, "15.0");
        this.testEnRealise(25.0, 1.0, 2, "50.0");
        this.testEnRealise(55.0, 2.5, 1, "137.5");
    }

    private void testEnRealise(double waterAverageQuantity, double spatialFrequency, int transitCount, String expectedIndicatorValue) {
        List<Zone> zones = zoneTopiaDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        EffectiveIntervention intervention = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "Intervention irrigation",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.IRRIGATION,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, transitCount,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency);

        RefInterventionAgrosystTravailEDI actionSEO = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEO").findAny();
        irrigationActionTopiaDao.create(
                IrrigationAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                IrrigationAction.PROPERTY_MAIN_ACTION, actionSEO,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, waterAverageQuantity
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorIrrigation indicatorIrrigation = serviceFactory.newInstance(IndicatorIrrigation.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorIrrigation);
        final String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo(
                "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;" +
                        "HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;" +
                        "Blé;;Blé tendre;;;;Pleine production;Irrigation;Intervention irrigation;Irrigation;;;N;;" +
                        "2013;Réalisé;Performance sociale et consommation;Consommation d'eau (mm);" + expectedIndicatorValue
        );
    }

    @Test
    public void testsIrrigationSynthetise_12357() throws IOException {
        this.testEnSynthetise(35, 3.0, 2.0, "210.0");
        this.testEnSynthetise(45, 1.5, 1.5, "101.25");
        this.testEnSynthetise(15, 1.0, 1.0, "15.0");
    }

    private void testEnSynthetise(double waterAverageQuantity, double spatialFrequency, double temporalFrequency, String expectedIndicatorValue) throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null,"Systeme de culture Baulon 1");
        final Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        PracticedCropCyclePhase phasePleineProduction = practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_NAME, "Intervention irrigation",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.IRRIGATION,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        RefInterventionAgrosystTravailEDI actionSEO = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEO").findAny();
        irrigationActionTopiaDao.create(
                IrrigationAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                IrrigationAction.PROPERTY_MAIN_ACTION, actionSEO,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, waterAverageQuantity
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorIrrigation indicatorIrrigation = serviceFactory.newInstance(IndicatorIrrigation.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorIrrigation);
        final String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo(
                "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;" +
                        "Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Irrigation;" +
                        "Intervention irrigation;" + intervention.getTopiaId() + ";03/04;03/04;Irrigation;;0;;;non;;" +
                        "2012, 2013;Synthétisé;Performance sociale et consommation;Consommation d'eau (mm);" + expectedIndicatorValue
        );
    }

}
