package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * @author kmorin : kmorin@codelutin.com
 * @since 0.8
 */
public class InseeAndOsmCommunesComparison {

    protected String inseeCode;

    protected boolean inInseeReferential;

    protected boolean inOsmReferential;

    protected String inseePostCode;

    protected String osmPostCode;

    protected String inseeName;

    protected String osmName;

    public String getInseeCode() {
        return inseeCode;
    }

    public void setInseeCode(String inseeCode) {
        this.inseeCode = inseeCode;
    }

    public boolean isInInseeReferential() {
        return inInseeReferential;
    }

    public void setInInseeReferential(boolean inInseeReferential) {
        this.inInseeReferential = inInseeReferential;
    }

    public boolean isInOsmReferential() {
        return inOsmReferential;
    }

    public void setInOsmReferential(boolean inOsmReferential) {
        this.inOsmReferential = inOsmReferential;
    }

    public String getInseePostCode() {
        return inseePostCode;
    }

    public void setInseePostCode(String inseePostCode) {
        this.inseePostCode = inseePostCode;
    }

    public String getOsmPostCode() {
        return osmPostCode;
    }

    public void setOsmPostCode(String osmPostCode) {
        this.osmPostCode = osmPostCode;
    }

    public String getInseeName() {
        return inseeName;
    }

    public void setInseeName(String inseeName) {
        this.inseeName = inseeName;
    }

    public String getOsmName() {
        return osmName;
    }

    public void setOsmName(String osmName) {
        this.osmName = osmName;
    }
}
