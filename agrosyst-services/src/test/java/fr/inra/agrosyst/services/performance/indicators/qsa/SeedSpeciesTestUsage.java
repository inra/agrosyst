package fr.inra.agrosyst.services.performance.indicators.qsa;

import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;

import java.util.List;

public record SeedSpeciesTestUsage(CroppingPlanSpecies species,
                                   DomainSeedSpeciesInput domainSeedSpeciesInput,
                                   Double quantite,
                                   List<TestUsage> speciesProductUsages
                        ) {
}