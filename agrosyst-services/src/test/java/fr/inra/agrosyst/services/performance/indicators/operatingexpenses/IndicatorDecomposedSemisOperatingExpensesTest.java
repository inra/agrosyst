package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInputTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.SeedPriceTopiaDao;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.PesticideProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhytoTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import static fr.inra.agrosyst.api.entities.action.SeedType.SEMENCES_CERTIFIEES;
import static fr.inra.agrosyst.api.entities.action.SeedType.SEMENCES_DE_FERME;
import static fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses.SEMIS;
import static fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses.TRAITEMENT_SEMENCE;
import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorDecomposedSemisOperatingExpensesTest extends AbstractIndicatorDecomposedOperatingExpensesTest {
    private CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    private CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    private DomainPhytoProductInputTopiaDao phytoProductInputDao;
    private SeedProductInputUsageTopiaDao seedProductInputUsageDao;
    private DomainSeedSpeciesInputTopiaDao seedSpeciesInputDao;
    private SeedSpeciesInputUsageTopiaDao seedSpeciesInputUsageDao;
    private SeedPriceTopiaDao seedPriceDao;
    private DomainSeedLotInputTopiaDao seedLotInputDao;
    private SeedLotInputUsageTopiaDao seedLotInputUsageDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        super.setupServices();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        phytoProductInputDao = getPersistenceContext().getDomainPhytoProductInputDao();
        seedProductInputUsageDao = getPersistenceContext().getSeedProductInputUsageDao();
        seedSpeciesInputDao = getPersistenceContext().getDomainSeedSpeciesInputDao();
        seedSpeciesInputUsageDao = getPersistenceContext().getSeedSpeciesInputUsageDao();
        seedPriceDao = getPersistenceContext().getSeedPriceDao();
        seedLotInputDao = getPersistenceContext().getDomainSeedLotInputDao();
        seedLotInputUsageDao = getPersistenceContext().getSeedLotInputUsageDao();
    }

    protected void createRefPrixEspece(RefEspece juliusBetteraveSucriere, RefEspece pythonBetteraveSucriere) {

        RefPrixEspeceTopiaDao refPrixEspeceDao = getPersistenceContext().getRefPrixEspeceDao();

        final String juliusTid0 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + 2012
                + ",scenario:" + 2012
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID,
                juliusTid0,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2012,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 231.5,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2012)
        );

        final String juliusTid1 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + 2012
                + ",scenario:" + 2012
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefEspece.PROPERTY_TOPIA_ID, juliusTid1,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2012,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 150.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO,  String.valueOf(2012)
        );

        final String pythonBetteraveSucriere0 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + (2012)
                + ",scenario:" + (2012)
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere0,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2012,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 150.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO,  String.valueOf(2012)
        );

        final String pythonBetteraveSucriere1 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + true
                + ",campagne:" + (2012)
                + ",scenario:" + (2012)
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere1,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2012,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 160.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, true,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2012)
        );

        final String pythonBetteraveSucriere2 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + (2012)
                + ",scenario:" + (2012)
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere2,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2012,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 225.679183498524,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2012)
        );

        final String juliusTid2 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + 2013
                + ",scenario:" + 2013
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid2,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2013,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 246.5,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2013)
        );

        final String juliusTid3 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + 2013
                + ",scenario:" + 2013
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid3,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2013,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 130.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2013)
        );

        final String pythonBetteraveSucriere3 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + 2013
                + ",scenario:" + 20132
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere3,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2013,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 240.679183498524,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2013)
        );

        final String pythonBetteraveSucriere4 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + 2013
                + ",scenario:" + 2013
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere4,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2013,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 130.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2013)
        );

        final String pythonBetteraveSucriere5 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + true
                + ",campagne:" + 2013
                + ",scenario:" + 2013
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere5,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2013,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 160.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, true,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2013)
        );
        final String juliusTid4 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid4,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 346.5,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String juliusTid5 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid5,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 100.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String pythonBetteraveSucriere6 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SEMENCES_CERTIFIEES
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere6,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 2340.679183498524,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_CERTIFIEES,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String pythonBetteraveSucriere7 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + false
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere7,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 100.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, false,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String pythonBetteraveSucriere8 = pythonBetteraveSucriere.getCode_espece_botanique()
                + "_" + pythonBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + true
                + ",campagne:" + null
                + ",scenario:" + "Y"
                + ",seedType:" + SeedType.SEMENCES_DE_FERME
                + ",active:" + true;
        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, pythonBetteraveSucriere8,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, null,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 160.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, pythonBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, pythonBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, true,
                RefPrixEspece.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, "Y"
        );

        final String juliusTid6 = juliusBetteraveSucriere.getCode_espece_botanique()
                + "_" + juliusBetteraveSucriere.getCode_qualifiant_AEE()
                + "," + PriceUnit.EURO_UNITE
                + ",traitement:" + true
                + ",campagne:" + 2013
                + ",scenario:" + 2013
                + ",seedType:" + SEMENCES_DE_FERME
                + ",active:" + true;

        refPrixEspeceDao.create(
                RefPrixEspece.PROPERTY_TOPIA_ID, juliusTid6,
                RefPrixEspece.PROPERTY_ACTIVE, true,
                RefPrixEspece.PROPERTY_CAMPAIGN, 2013,
                RefPrixEspece.PROPERTY_UNIT, PriceUnit.EURO_UNITE,
                RefPrixEspece.PROPERTY_PRICE, 180.0,
                RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, juliusBetteraveSucriere.getCode_espece_botanique(),
                RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE, juliusBetteraveSucriere.getCode_qualifiant_AEE(),
                RefPrixEspece.PROPERTY_TREATMENT, true,
                RefPrixEspece.PROPERTY_SEED_TYPE, SEMENCES_DE_FERME,
                RefPrixEspece.PROPERTY_SCENARIO, String.valueOf(2013)
        );
    }

    protected void createRefPrixProduct(RefActaTraitementsProduit product) {
        //5255;Pragma SX;147;H1;Herbicides - Désherbants sélectifs et non sélectifs (traitements dirigés);N;ACTA 2013;t
        //4789;PROFILER;140;F1;Fongicides - Traitement des parties aériennes;N;ACTA 2017;t

        //"Traitement : Barky (id-traitt = 147, id_produit = 5255)",// dans le doc: Traitement : Barky (id-traitt = 141, id_produit = 3083)

        RefPrixPhytoTopiaDao refPrixPhytoDao = getPersistenceContext().getRefPrixPhytoDao();

        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ACTIVE, true,
                RefPrixPhyto.PROPERTY_CAMPAIGN, 2012,
                RefPrixPhyto.PROPERTY_SCENARIO, String.valueOf(2019),
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, product.getId_produit() + "_" + product.getId_traitement(),
                RefPrixPhyto.PROPERTY_ID_PRODUIT, product.getId_produit(),
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, product.getId_traitement(),
                RefPrixPhyto.PROPERTY_PRICE, 40.0,
                RefPrixPhyto.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ACTIVE, true,
                RefPrixPhyto.PROPERTY_CAMPAIGN, 2013,
                RefPrixPhyto.PROPERTY_SCENARIO, String.valueOf(2020),
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, product.getId_produit() + "_" + product.getId_traitement(),
                RefPrixPhyto.PROPERTY_ID_PRODUIT, product.getId_produit(),
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, product.getId_traitement(),
                RefPrixPhyto.PROPERTY_PRICE, 55.0,
                RefPrixPhyto.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ACTIVE, true,
                RefPrixPhyto.PROPERTY_CAMPAIGN, null,
                RefPrixPhyto.PROPERTY_SCENARIO, "Y",
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, product.getId_produit() + "_" + product.getId_traitement(),
                RefPrixPhyto.PROPERTY_ID_PRODUIT, product.getId_produit(),
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, product.getId_traitement(),
                RefPrixPhyto.PROPERTY_PRICE, 155.0,
                RefPrixPhyto.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
    }

    @Test
    public void inraeEffectiveWithoutPriceTest()  throws IOException {
        inraeEffectiveTest(
                null,
                null,
                null,
                null,
                1000000.0d,
                0.0d, (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);54000000.0;50;Aucun prix de semis renseigné pour l'espèce : Betterave;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);54000000.0;50;Aucun prix de semis renseigné pour l'espèce : Betterave;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);0.0;50;Aucun prix de semis renseigné pour l'espèce : Betterave;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);0.0;50;Aucun prix de semis renseigné pour l'espèce : Betterave;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles semis (€/ha);5.4E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);5.4E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);0.0;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);0.0;;100;;;");
                    });
    }

    @Test
    public void inraeEffectiveWithPriceTest()  throws IOException {
        inraeEffectiveTest(
                35.0d,
                150.0d,
                null,
                null,
                1000000.0d,
                0d, (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);45000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);39000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);0.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);0.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Pragma SX (600.0 g/q);;non;2013;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);62993700.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Pragma SX (600.0 g/q);;non;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);98990100.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles semis (€/ha);4.5E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);3.9E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);6.29937E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);9.89901E7;;100;;;");

                });
    }

    @Test
    public void inraeEffectiveWithSpeciesPriceTest()  throws IOException {
        inraeEffectiveTest(
                35.0d,
                null,
                100.0d,
                50.0d,
                1000000d,
                2000000d, (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);30000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);39000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);30000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);78000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Pragma SX (600.0 g/q);;non;2013;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);62993700.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Pragma SX (600.0 g/q);;non;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);98990100.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles semis (€/ha);6.0E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);1.17E8;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);6.29937E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);9.89901E7;;100;;;");

                });
    }

    @Test
    public void inraeEffectiveWithLotPriceTest()  throws IOException {
        inraeEffectiveTest(
                35.0d,
                150.0d,
                null,
                null,
                1000000d,
                2000000d, (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);45000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);39000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles réelles semis (€/ha);90000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Betterave Sucrière;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);78000000.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Pragma SX (600.0 g/q);;non;2013;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);62993700.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;01/04/2013;01/04/2013;Semis;Pragma SX (600.0 g/q);;non;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);98990100.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles semis (€/ha);1.35E8;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);1.17E8;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);6.29937E7;;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;Semis;Julius;;N;;2013;Réalisé;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);9.89901E7;;100;;;");

                });
    }

    private void inraeEffectiveTest(
            Double seedingTraitmentPrice,
            Double lotPrice,
            Double speciesPrice1,
            Double speciesPrice2,
            double species1Quantity,
            double species2Quantity,
            Consumer<String> assertions) throws IOException {
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        RefInterventionAgrosystTravailEDI semisClassic = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUnique();

        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();
        Domain d0 = zpPlotBaulon1.getPlot().getDomain();
        RefCountry france = d0.getLocation().getRefCountry();

        // Pragma SX
        RefActaTraitementsProduit product = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("5255", 147, france).findUnique();

        RefEspece juliusBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZAN", "", "").findUnique();
        RefEspece pythonBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZLF", "", "").findUnique();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixEspece(juliusBetteraveSucriere, pythonBetteraveSucriere);
        createRefPrixProduct(product);

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        CroppingPlanEntry betterave = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "betteraveSucriere_ID0",
                CroppingPlanEntry.PROPERTY_DOMAIN, d0,
                CroppingPlanEntry.PROPERTY_CODE, "betteraveSucriere_CODE0",
                CroppingPlanEntry.PROPERTY_NAME, "Betterave Sucrière",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies spM2JuliusBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_SPECIES, juliusBetteraveSucriere
        );

        CroppingPlanSpecies spM2PythonBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_SPECIES, pythonBetteraveSucriere
        );

        betterave.setCroppingPlanSpecies(Lists.newArrayList(spM2JuliusBetteraveSucriere, spM2PythonBetteraveSucriere));

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention i2_semisclassique = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "intervention",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(d0.getCampaign(), 4, 1),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(d0.getCampaign(), 4, 1),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 0.3
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade spM2JuliusBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2JuliusBetteraveSucriere);
        i2_semisclassique.addSpeciesStades(spM2JuliusBetteraveSpeciesStade);
        EffectiveSpeciesStade spM2PythonBetteraveSpeciesStade = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, spM2PythonBetteraveSucriere);
        i2_semisclassique.addSpeciesStades(spM2PythonBetteraveSpeciesStade);

        String displayName = betterave.getName();
        List<String> speciesNames = new ArrayList<>();

        // prix à l'espèce prix phyto non inclus au prix de la semence
        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(product);
        InputPrice phytoPrice = null;
        if (seedingTraitmentPrice != null) {
            phytoPrice = priceDao.create(
                    InputPrice.PROPERTY_OBJECT_ID, objectId,
                    InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                    InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                    InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                    InputPrice.PROPERTY_DISPLAY_NAME, "Traitement : Barky (id-traitt = 147, id_produit = 5255)",
                    InputPrice.PROPERTY_PRICE, seedingTraitmentPrice,
                    InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
            );
        }
        var traitement = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.G_Q,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainPhytoProductInput.PROPERTY_REF_INPUT, product,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, phytoPrice
        );
        var traitementUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, traitement,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 600.0
        );


        SeedPrice species1SeedPrice = null;
        if (speciesPrice1 != null) {
            objectId = InputPriceService.GET_SPECIES_SEED_OBJECT_ID.apply(spM2JuliusBetteraveSucriere.getSpecies(), spM2JuliusBetteraveSucriere.getVariety());
            String speciesDisplayName = displayName + " (" + String.join(", ", speciesNames) + ")";
            species1SeedPrice = seedPriceDao.create(
                    InputPrice.PROPERTY_OBJECT_ID, objectId,
                    InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                    InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                    InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                    InputPrice.PROPERTY_DISPLAY_NAME, speciesDisplayName,
                    InputPrice.PROPERTY_PRICE, speciesPrice1,
                    SeedPrice.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                    SeedPrice.PROPERTY_INCLUDED_TREATMENT, seedingTraitmentPrice == null,
                    SeedPrice.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                    SeedPrice.PROPERTY_CHEMICAL_TREATMENT, true,
                    SeedPrice.PROPERTY_ORGANIC, false
            );
        }

        var seedSpeciesInput1 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2JuliusBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_SEED_PRICE, species1SeedPrice,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2JuliusBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, true,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedSpeciesInput.PROPERTY_SPECIES_PHYTO_INPUTS, List.of(traitement)
        );

        var speciesInputUsage1 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, species1Quantity,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput1,
                SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, List.of(traitementUsage),
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2JuliusBetteraveSucriere.getSpecies().getLibelle_espece_botanique());

        SeedPrice species2SeedPrice = null;
        if (speciesPrice2 != null) {
            objectId = InputPriceService.GET_SPECIES_SEED_OBJECT_ID.apply(spM2PythonBetteraveSucriere.getSpecies(), spM2PythonBetteraveSucriere.getVariety());
            String speciesDisplayName = displayName + " (" + String.join(", ", speciesNames) + ")";
            species2SeedPrice = seedPriceDao.create(
                    InputPrice.PROPERTY_OBJECT_ID, objectId,
                    InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                    InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                    InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                    InputPrice.PROPERTY_DISPLAY_NAME, speciesDisplayName,
                    InputPrice.PROPERTY_PRICE, speciesPrice2,
                    SeedPrice.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                    SeedPrice.PROPERTY_INCLUDED_TREATMENT, false,
                    SeedPrice.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                    SeedPrice.PROPERTY_CHEMICAL_TREATMENT, true,
                    SeedPrice.PROPERTY_ORGANIC, false
            );
        }
        var seedSpeciesInput2 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2PythonBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2PythonBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                DomainSeedSpeciesInput.PROPERTY_SEED_PRICE, species2SeedPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        var speciesInputUsage2 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, species2Quantity,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput2,
                SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, null,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2PythonBetteraveSucriere.getSpecies().getLibelle_espece_botanique());

        SeedPrice lotSeedPrice = null;
        if (lotPrice != null && speciesPrice1 == null && speciesPrice2 == null) {
            String speciesDisplayName = displayName + " (" + String.join(", ", speciesNames) + ")";
            lotSeedPrice = seedPriceDao.create(
                    InputPrice.PROPERTY_OBJECT_ID, objectId,
                    InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                    InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                    InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                    InputPrice.PROPERTY_DISPLAY_NAME, speciesDisplayName,
                    InputPrice.PROPERTY_PRICE, lotPrice,
                    InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS,
                    SeedPrice.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                    SeedPrice.PROPERTY_INCLUDED_TREATMENT, false,
                    SeedPrice.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                    SeedPrice.PROPERTY_CHEMICAL_TREATMENT, true,
                    SeedPrice.PROPERTY_ORGANIC, false
            );
        }
        var seedLotInput = seedLotInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                DomainSeedLotInput.PROPERTY_CROP_SEED, betterave,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, List.of(seedSpeciesInput1, seedSpeciesInput2),
                DomainSeedLotInput.PROPERTY_USAGE_UNIT, SeedPlantUnit.GRAINES_PAR_HA,
                DomainSeedLotInput.PROPERTY_ORGANIC, false,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Julius",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedLotInput.PROPERTY_SEED_PRICE, lotSeedPrice,
                DomainSeedLotInput.PROPERTY_SEED_COATED_TREATMENT, seedingTraitmentPrice == null
        );
        var seedLotUsage = seedLotInputUsageDao.create(
                SeedLotInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, seedLotInput,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, List.of(speciesInputUsage1, speciesInputUsage2)
        );
        SeedingActionUsageTopiaDao seedingActionDao = getPersistenceContext().getSeedingActionUsageDao();
        seedingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, i2_semisclassique,
                AbstractAction.PROPERTY_MAIN_ACTION, semisClassic,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotUsage),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(List.of(SEMIS, TRAITEMENT_SEMENCE));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertions.accept(content);
    }

    @Test
    public void inraePracticedTest() throws IOException {
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();
        RefCountry france = domain.getLocation().getRefCountry();

        RefInterventionAgrosystTravailEDI semisClassic = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUnique();

        RefActaTraitementsProduit product = getPersistenceContext().getRefActaTraitementsProduitDao().forNaturalId("5255", 147, france).findUnique();

        RefEspece juliusBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZAN", "", "").findUnique();
        RefEspece pythonBetteraveSucriere = refEspeceDao.forNaturalId("ZAP", "ZLF", "", "").findUnique();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixEspece(juliusBetteraveSucriere, pythonBetteraveSucriere);
        createRefPrixProduct(product);

        CroppingPlanEntry betterave = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_TOPIA_ID, "betteraveSucriere_ID0",
                CroppingPlanEntry.PROPERTY_DOMAIN, domain,
                CroppingPlanEntry.PROPERTY_CODE, "betteraveSucriere_CODE0",
                CroppingPlanEntry.PROPERTY_NAME, "Betterave Sucrière",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN
        );

        CroppingPlanSpecies spM2JuliusBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAN",
                CroppingPlanSpecies.PROPERTY_SPECIES, juliusBetteraveSucriere
        );

        CroppingPlanSpecies spM2PythonBetteraveSucriere = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, betterave,
                CroppingPlanSpecies.PROPERTY_TOPIA_ID, "2017_m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_CODE, "m2_ZAP_ZAO",
                CroppingPlanSpecies.PROPERTY_SPECIES, pythonBetteraveSucriere
        );

        betterave.setCroppingPlanSpecies(Lists.newArrayList(spM2JuliusBetteraveSucriere, spM2PythonBetteraveSucriere));

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Betterave Sucrière");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100.0d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0d,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.0d,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(intervention1Stades);

        String displayName = betterave.getName();
        List<String> speciesNames = new ArrayList<>();

        String objectId = InputPriceService.GET_REF_ACTA_TRAITEMENTS_PRODUITS_OBJECT_ID.apply(product);
        var phytoPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domain,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_KG,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Traitement : Barky (id-traitt = 147, id_produit = 5255)",
                InputPrice.PROPERTY_PRICE, 35.0d,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS
        );
        var traitement = phytoProductInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                DomainPhytoProductInput.PROPERTY_USAGE_UNIT, PhytoProductUnit.G_Q,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                DomainPhytoProductInput.PROPERTY_REF_INPUT, product,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainPhytoProductInput.PROPERTY_INPUT_NAME, "",
                DomainPhytoProductInput.PROPERTY_INPUT_PRICE, phytoPrice
        );
        var traitementUsage = seedProductInputUsageDao.create(
                PesticideProductInputUsage.PROPERTY_INPUT_TYPE, InputType.TRAITEMENT_SEMENCE,
                PesticideProductInputUsage.PROPERTY_DOMAIN_PHYTO_PRODUCT_INPUT, traitement,
                PesticideProductInputUsage.PROPERTY_QT_AVG, 600.0
        );
        objectId = InputPriceService.GET_SPECIES_SEED_OBJECT_ID.apply(spM2JuliusBetteraveSucriere.getSpecies(), spM2JuliusBetteraveSucriere.getVariety());
        var seedSpeciesInput1 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2JuliusBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2JuliusBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, true,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedSpeciesInput.PROPERTY_SPECIES_PHYTO_INPUTS, List.of(traitement)
        );
        var speciesInputUsage1 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 1000000d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput1,
                SeedSpeciesInputUsage.PROPERTY_SEED_PRODUCT_INPUT_USAGES, List.of(traitementUsage),
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2JuliusBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        var seedSpeciesInput2 = seedSpeciesInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                DomainSeedSpeciesInput.PROPERTY_SPECIES_SEED, spM2PythonBetteraveSucriere,
                DomainSeedSpeciesInput.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                DomainSeedSpeciesInput.PROPERTY_CODE, spM2PythonBetteraveSucriere.getCode(),
                DomainSeedSpeciesInput.PROPERTY_CHEMICAL_TREATMENT, false,
                DomainSeedSpeciesInput.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                DomainSeedSpeciesInput.PROPERTY_INPUT_NAME, "",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var speciesInputUsage2 = seedSpeciesInputUsageDao.create(
                SeedSpeciesInputUsage.PROPERTY_QT_AVG, 0d,
                SeedSpeciesInputUsage.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, seedSpeciesInput2,
                SeedSpeciesInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS
        );
        speciesNames.add(spM2PythonBetteraveSucriere.getSpecies().getLibelle_espece_botanique());
        displayName = displayName + " (" + String.join(", ", speciesNames) + ")";
        var seedPrice = seedPriceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, domain,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_UNITE,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, displayName,
                InputPrice.PROPERTY_PRICE, 150.0d,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, ProductType.ADJUVANTS,
                SeedPrice.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedPrice.PROPERTY_INCLUDED_TREATMENT, false,
                SeedPrice.PROPERTY_BIOLOGICAL_SEED_INOCULATION, false,
                SeedPrice.PROPERTY_CHEMICAL_TREATMENT, true,
                SeedPrice.PROPERTY_ORGANIC, false
        );
        var seedLotInput = seedLotInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                DomainSeedLotInput.PROPERTY_CROP_SEED, betterave,
                DomainSeedLotInput.PROPERTY_DOMAIN_SEED_SPECIES_INPUT, List.of(seedSpeciesInput1, seedSpeciesInput2),
                DomainSeedLotInput.PROPERTY_USAGE_UNIT, SeedPlantUnit.GRAINES_PAR_HA,
                DomainSeedLotInput.PROPERTY_ORGANIC, false,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, domain,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Julius",
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, "",
                DomainSeedLotInput.PROPERTY_SEED_PRICE, seedPrice,
                DomainSeedLotInput.PROPERTY_SEED_COATED_TREATMENT, false
        );
        var seedLotUsage = seedLotInputUsageDao.create(
                SeedLotInputUsage.PROPERTY_INPUT_TYPE, InputType.SEMIS,
                SeedLotInputUsage.PROPERTY_DOMAIN_SEED_LOT_INPUT, seedLotInput,
                SeedLotInputUsage.PROPERTY_SEEDING_SPECIES, List.of(speciesInputUsage1, speciesInputUsage2)
        );
        SeedingActionUsageTopiaDao seedingActionDao = getPersistenceContext().getSeedingActionUsageDao();
        seedingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, semisClassic,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, List.of(seedLotUsage),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(List.of(SEMIS, TRAITEMENT_SEMENCE));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Betterave Sucrière;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles semis (€/ha);2013, 2017;Baulon;1.5E8;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Betterave Sucrière;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);2013, 2017;Baulon;1.3E8;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Betterave Sucrière;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);2013, 2017;Baulon;2.09979E8;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Betterave Sucrière;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);2013, 2017;Baulon;3.29967E8;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;03/04;03/04;Semis;Betterave Sucrière;;null;2013, 2017;Indicateur économique;Charges opérationnelles réelles semis (€/ha);1.5E8;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;03/04;03/04;Semis;Betterave Sucrière;;null;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, semis (€/ha);1.3E8;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;03/04;03/04;Semis;Pragma SX (600.0 g/q);;non;2013, 2017;Indicateur économique;Charges opérationnelles réelles traitements de semences (€/ha);2.09979E8;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Betterave Sucrière;;Betterave;;Betterave;;Pleine production;Semis;intervention;03/04;03/04;Semis;Pragma SX (600.0 g/q);;non;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, traitements de semences (€/ha);3.29967E8;100;;;");
    }
}
