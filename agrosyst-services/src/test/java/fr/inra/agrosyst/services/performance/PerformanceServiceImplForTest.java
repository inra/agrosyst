package fr.inra.agrosyst.services.performance;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceState;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.performancehelper.PerformancePracticedExecutionContextBuilder;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class PerformanceServiceImplForTest extends PerformanceServiceImpl {

    public PerformanceServiceImplForTest(PerformanceServiceImpl performanceService) {
        super.actaSubstanceActiveDao = performanceService.actaSubstanceActiveDao;
        super.actionDao = performanceService.actionDao;
        super.anonymizeService = performanceService.anonymizeService;
        super.authorizationService = performanceService.authorizationService;
        super.context = performanceService.getContext();
        super.domainDao = performanceService.domainDao;
        super.domainService = performanceService.domainService;
        super.domainInputStockUnitService = performanceService.domainInputStockUnitService;
        super.effectiveCropCycleConnectionDao = performanceService.effectiveCropCycleConnectionDao;
        super.effectiveInterventionDao = performanceService.effectiveInterventionDao;
        super.effectivePerennialCropCyclDao = performanceService.effectivePerennialCropCyclDao;
        super.effectiveSeasonalCropCycleDao = performanceService.effectiveSeasonalCropCycleDao;
        super.growingSystemDao = performanceService.growingSystemDao;
        super.indicatorFilterDao = performanceService.indicatorFilterDao;
        super.performanceDao = performanceService.performanceDao;
        super.performanceFileTopiaDao = performanceService.performanceFileTopiaDao;
        super.plotTopiaDao = performanceService.plotTopiaDao;
        super.practicedCropCycleConnectionDao = performanceService.practicedCropCycleConnectionDao;
        super.practicedInterventionDao = performanceService.practicedInterventionDao;
        super.practicedPerennialCropCycleDao = performanceService.practicedPerennialCropCycleDao;
        super.practicedPlotTopiaDao = performanceService.practicedPlotTopiaDao;
        super.practicedSeasonalCropCycleDao = performanceService.practicedSeasonalCropCycleDao;
        super.practicedSystemDao = performanceService.practicedSystemDao;
        super.practicedSystemService = performanceService.practicedSystemService;
        super.refInputPriceDao = performanceService.refInputPriceDao;
        super.inputPriceService = performanceService.inputPriceService;
        super.refActaTraitementsProduitsCategDao = performanceService.refActaTraitementsProduitsCategDao;
        super.refAgsAmortissementDao = performanceService.refAgsAmortissementDao;
        super.referentialService = performanceService.referentialService;
        super.refHarvestingPriceConverterDao = performanceService.refHarvestingPriceConverterDao;
        super.refHarvestingPriceDao = performanceService.refHarvestingPriceDao;
        super.refInputPriceService = performanceService.refInputPriceService;
        super.pricesService = performanceService.getPricesService();
        super.refInputUnitPriceUnitConverterDao = performanceService.refInputUnitPriceUnitConverterDao;
        super.refLocationDao = performanceService.refLocationDao;
        super.refSolProfondeurIndigosDao = performanceService.refSolProfondeurIndigosDao;
        super.refSolTextureGeppaDao = performanceService.refSolTextureGeppaDao;
        super.userDao = performanceService.userDao;
        super.zoneDao = performanceService.zoneDao;
        super.i18nService = performanceService.i18nService;
        super.croppingPlanSpeciesDao = performanceService.croppingPlanSpeciesDao;
    }
    
    // execute test into one thread
    public Performance createOrUpdatePerformance(Performance performance,
                                                 String name,
                                                 List<String> domainIds,
                                                 List<String> growingSystemIds,
                                                 List<String> plotIds,
                                                 List<String> zoneIds,
                                                 Collection<IndicatorFilter> indicatorFilters,
                                                 Set<String> scenarioCodes,
                                                 boolean computeOnReal,
                                                 boolean computeOnStandardised,
                                                 boolean exportToDb) {
    
    
        authorizationService.checkCreateOrUpdatePerformance(performance.getTopiaId());
        String userId = getSecurityContext().getUserId();
    
        Collection<Domain> domains;
        if (CollectionUtils.isNotEmpty(domainIds)) {
            domains = domainDao.forTopiaIdIn(domainIds).findAll();
        } else {
            domains = null;
        }
        performance.setDomains(domains);
    
        Collection<GrowingSystem> growingSystems;
        if (CollectionUtils.isNotEmpty(growingSystemIds)) {
            growingSystems = growingSystemDao.forTopiaIdIn(growingSystemIds).findAll();
        } else {
            growingSystems = null;
        }
        performance.setGrowingSystems(growingSystems);
    
        Collection<Plot> plots;
        if (CollectionUtils.isNotEmpty(plotIds)) {
            plots = plotTopiaDao.forTopiaIdIn(plotIds).findAll();
        } else {
            plots = null;
        }
        performance.setPlots(plots);
    
        Collection<Zone> zones;
        if (CollectionUtils.isNotEmpty(zoneIds)) {
            zones = zoneDao.forTopiaIdIn(zoneIds).findAll();
        } else {
            zones = null;
        }
        performance.setZones(zones);
    
        performance.setUpdateDate(context.getOffsetTime());
        performance.setComputeStatus(PerformanceState.GENERATING);
        
        if (performance.getExportType() == null) {
            performance.setExportType(ExportType.FILE);
        }

        boolean isScenarioCodeValid = isScenarioCodeValid(indicatorFilters, scenarioCodes);
        createOrUpdateIndicatorFilters(
                performance,
                indicatorFilters,
                computeOnReal,
                computeOnStandardised,
                scenarioCodes);
    
        if (!isScenarioCodeValid) {
            scenarioCodes = null;
        }
    
        performance.setScenarioCodes(scenarioCodes);
    
        if (performance.isPersisted()) {
            performance = performanceDao.update(performance);
        } else {
            AgrosystUser user = userDao.forTopiaIdEquals(userId).findUnique();
            performance.setAuthor(user);
            performance = performanceDao.create(performance);
        }
    
        getTransaction().commit();
    
        if (CollectionUtils.isEmpty(indicatorFilters)) {
            performance.setComputeStatus(PerformanceState.SUCCESS);
            performance = performanceDao.update(performance);
        
            getTransaction().commit();
        
        }
    
        return performance;
    }
    
    protected Set<String> getAllScenarioCodes() {
        Set<String> scenarioCodes = new HashSet<>();
        for (int i = 1; i <= 10; i++) {
            scenarioCodes.add("Code_Scenario_Agrosyst_" + i);
        }
        return scenarioCodes;
    }
    
    public Double[] manageIntervention(AbstractIndicator indicator,
                                       GrowingSystem growingSystem,
                                       PracticedIntervention intervention) {
        
        RefSolTextureGeppa defaultRefSolTextureGeppa = refSolTextureGeppaDao.forNaturalId("LAS").findUnique();
        RefSolProfondeurIndigo defaultSolDepth = refSolProfondeurIndigosDao.forNaturalId("Profond").findUnique();
        
        RefLocation defaultLocation = null;
        defaultLocation = getOrCreateRefLocation(defaultLocation);
    
        final List<RefHarvestingPriceConverter> allharvestingPriceConverters = refHarvestingPriceConverterDao.findAll();
        final PriceConverterKeysToRate priceConverterKeysToRate = new PriceConverterKeysToRate(allharvestingPriceConverters);
    
        PerformancePracticedExecutionContextBuilder performanceExecutionBuilder = new PerformancePracticedExecutionContextBuilder(
                anonymizeService,
                domainInputStockUnitService,
                domainService,
                inputPriceService,
                this,
                referentialService,
                refInputPriceService,
                pricesService,
                new ArrayList<>(),// TODO add Filter
                false,
                priceConverterKeysToRate,
                defaultRefSolTextureGeppa,
                Collections.emptySet(),
                defaultLocation,
                defaultSolDepth
        );
        
        PerformanceGlobalExecutionContext performanceGlobalExecutionContext = performanceExecutionBuilder.getPerformanceGlobalExecutionContext();
    
        Domain domain = growingSystem.getGrowingPlan().getDomain();
        PerformancePracticedDomainExecutionContext domainContext = performanceExecutionBuilder.buildPerformanceDomainExecutionContext(Pair.of(domain, domain), Lists.newArrayList(growingSystem));
    
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        Set<PerformanceGrowingSystemExecutionContext> performanceGrowingSystemExecutionContexts = domainContext.getPerformanceGrowingSystemExecutionContexts();
        for (PerformanceGrowingSystemExecutionContext performanceGrowingSystemExecutionContext : performanceGrowingSystemExecutionContexts) {
            Set<PerformancePracticedSystemExecutionContext> performancePracticedSystemExecutionContext = performanceGrowingSystemExecutionContext.getPracticedSystemExecutionContexts();
            String its = performanceGrowingSystemExecutionContext.getIts();
            String irs = performanceGrowingSystemExecutionContext.getIrs();
            for (PerformancePracticedSystemExecutionContext practicedSystemExecutionContext : performancePracticedSystemExecutionContext) {
                PracticedSystem practicedSystem = practicedSystemExecutionContext.getPracticedSystem();
                Set<PerformancePracticedCropExecutionContext> cropContext = practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts();
                for (PerformancePracticedCropExecutionContext practicedCropContextExecutionContext : cropContext) {
                    Set<PerformancePracticedInterventionExecutionContext> interventionExecutionContext = practicedCropContextExecutionContext.getInterventionExecutionContexts();
                    for (PerformancePracticedInterventionExecutionContext performancePracticedInterventionExecutionContext : interventionExecutionContext) {
                        CropWithSpecies cropWithSpecies = performancePracticedInterventionExecutionContext.getCropWithSpecies();
                        performancePracticedInterventionExecutionContext.setToolsCouplingWorkingTime(0.123456);// test value
                        PracticedIntervention practicedIntervention = performancePracticedInterventionExecutionContext.getIntervention();
                        if (practicedIntervention == intervention) {
                            final CropWithSpecies intermediateCropWithSpecies = practicedCropContextExecutionContext.getIntermediateCropWithSpecies();
                            CroppingPlanEntry intermediateCrop =
                                    intermediateCropWithSpecies != null ? intermediateCropWithSpecies.getCroppingPlanEntry() :
                                            null;
                            WriterContext writerContext = WriterContext.createPracticedSeasonalWriterContext(
                                    writer,
                                    its,
                                    irs,
                                    practicedSystem.getCampaigns(),
                                    domain,
                                    growingSystem,
                                    practicedSystem,
                                    cropWithSpecies.getCroppingPlanEntry(),
                                    practicedCropContextExecutionContext.getRank(),
                                    practicedCropContextExecutionContext.getSeasonalPreviousCrop(),
                                    intermediateCrop,
                                    intervention,
                                    practicedSystemExecutionContext.getCodeAmmBioControle(),
                                    performanceGlobalExecutionContext.getGroupesCiblesParCode()
                            );
                            Double[] result = indicator.manageIntervention(
                                    writerContext,
                                    performanceGlobalExecutionContext,
                                    domainContext,
                                    performanceGrowingSystemExecutionContext,
                                    practicedSystemExecutionContext,
                                    practicedCropContextExecutionContext,
                                    performancePracticedInterventionExecutionContext,
                                    null);
                            return result;
                        }
                    }
                }
            }
        }
        return null;
    }

    protected RefLocation getOrCreateRefLocation(RefLocation defaultLocation) {
        if (refLocationDao.count() == 0) {
            defaultLocation = refLocationDao.create(
                    RefLocation.PROPERTY_CODE_INSEE, "75056",
                    RefLocation.PROPERTY_COMMUNE, "Paris",
                    RefLocation.PROPERTY_DEPARTEMENT, "75",
                    RefLocation.PROPERTY_ACTIVE, true);
        } else {
            refLocationDao.forCodeInseeEquals("75056").findUnique();
        }
        return defaultLocation;
    }
    
    public List<PracticedCropCycleConnection> getPracticedConnectionsForNodes(Collection<PracticedCropCycleNode> nodes) {
        return super.getPracticedConnectionsForNodes(nodes);
    }

    public String[] getTranslatedMonths() {
        return super.getTranslatedMonths(Locale.FRANCE);
    }

}
