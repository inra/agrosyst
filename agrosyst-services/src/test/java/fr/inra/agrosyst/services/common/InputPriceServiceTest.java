package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesImpl;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationImpl;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceImpl;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMinTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhytoTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceFilter;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.ProductPriceSummary;
import fr.inra.agrosyst.api.services.common.RefInputPriceDto;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class InputPriceServiceTest extends AbstractAgrosystTest {

    protected DomainTopiaDao domainDao;
    protected InputPriceTopiaDao inputPriceDao;
    protected RefPrixFertiMinTopiaDao mineralInputRePriceDao;
    protected RefPrixPhytoTopiaDao refPrixPhytoDao;
    
    protected DomainService domainService;
    protected InputPriceService inputPriceService;
    
    @BeforeEach
    public void prepareTest() {
        
        testDatas = serviceFactory.newInstance(TestDatas.class);
        
        domainDao = getPersistenceContext().getDomainDao();
        inputPriceDao = getPersistenceContext().getInputPriceDao();
        mineralInputRePriceDao = getPersistenceContext().getRefPrixFertiMinDao();
        refPrixPhytoDao = getPersistenceContext().getRefPrixPhytoDao();
    
        domainService = serviceFactory.newService(DomainService.class);
        inputPriceService = serviceFactory.newService(InputPriceService.class);
        
        loginAsAdmin();
        alterSchema();
    }
    
    @Test
    public void testLoadPersistMineralInputUserInformation() throws IOException, DomainExtendException {
        testDatas.createTestDomains();
        
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        Domain domainCoueronLesBains = domainDao.forNameEquals("Couëron les bains").findUnique();
        Domain domainTestIFT = domainDao.forNameEquals("DOMAINE TEST IFT").findUnique();
        Domain domainDomaineDeVerchant = domainDao.forNameEquals("Domaine de verchant").findUnique();
        Domain domainLaBouineliere = domainDao.forNameEquals("La Bouinelière").findUnique();
    
        domainCoueronLesBains = domainService.extendDomain(domainCoueronLesBains.getTopiaId(), baulon.getCampaign());
        domainTestIFT = domainService.extendDomain(domainTestIFT.getTopiaId(), baulon.getCampaign());
        domainDomaineDeVerchant = domainService.extendDomain(domainDomaineDeVerchant.getTopiaId(), baulon.getCampaign());
        domainLaBouineliere = domainService.extendDomain(domainLaBouineliere.getTopiaId(), baulon.getCampaign());
        
        InputPrice priceBaulon = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, baulon,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 12.3,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        // other user prices
        InputPrice lowestPrice = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainCoueronLesBains,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 10.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);
        
        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainTestIFT,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 11.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);
        
        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainDomaineDeVerchant,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 13.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        InputPrice highestPrice = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainLaBouineliere,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 20.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        int campaign = priceBaulon.getDomain().getCampaign();

        Map<FertiMinElement, List<RefPrixFertiMin>> baulonCampaignRefPrixFertiMin = new HashMap<>();
        FertiMinElement n = FertiMinElement.N;
        RefPrixFertiMin pulverulentN = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, n,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 52.93666308655617,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(n, k -> new ArrayList<>()).add(pulverulentN);

        FertiMinElement p2O5 = FertiMinElement.P2_O5;
        RefPrixFertiMin pulverulentP2_O5 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, p2O5,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 87.07408416206262,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(p2O5, k -> new ArrayList<>()).add(pulverulentP2_O5);

        FertiMinElement k2O = FertiMinElement.K2_O;
        RefPrixFertiMin pulverulentK2_O = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, k2O,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 69.697521546961326,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(k2O, k -> new ArrayList<>()).add(pulverulentK2_O);

        FertiMinElement bore = FertiMinElement.BORE;
        RefPrixFertiMin pulverulentBORE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, bore,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 13.6476626947754,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(bore, k -> new ArrayList<>()).add(pulverulentBORE);

        FertiMinElement calcium = FertiMinElement.CALCIUM;
        RefPrixFertiMin pulverulentCALCIUM = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 56.159222731439047,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(calcium, k -> new ArrayList<>()).add(pulverulentCALCIUM);

        FertiMinElement fer = FertiMinElement.FER;
        RefPrixFertiMin pulverulentFER = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, fer,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 176.54874427131,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(fer, k -> new ArrayList<>()).add(pulverulentFER);

        FertiMinElement manganese = FertiMinElement.MANGANESE;
        RefPrixFertiMin pulverulentMANGANESE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, manganese,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 139.888542621448,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(manganese, k -> new ArrayList<>()).add(pulverulentMANGANESE);

        FertiMinElement molybdene = FertiMinElement.MOLYBDENE;
        RefPrixFertiMin pulverulentMOLYBDENE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, molybdene,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 160.298295142071,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(molybdene, k -> new ArrayList<>()).add(pulverulentMOLYBDENE);

        FertiMinElement mgO = FertiMinElement.MG_O;
        RefPrixFertiMin pulverulentMG_O = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, mgO,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 81.570318600368324,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(mgO, k -> new ArrayList<>()).add(pulverulentMG_O);

        FertiMinElement oxydeDeSodium = FertiMinElement.OXYDE_DE_SODIUM;
        RefPrixFertiMin pulverulentOXYDE_DE_SODIUM = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, oxydeDeSodium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 62.64992117323556,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(oxydeDeSodium, k -> new ArrayList<>()).add(pulverulentOXYDE_DE_SODIUM);

        FertiMinElement sO3 = FertiMinElement.S_O3;
        RefPrixFertiMin pulverulentS_O3 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, sO3,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 32.598691712707182,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(sO3, k -> new ArrayList<>()).add(pulverulentS_O3);

        FertiMinElement cuivre = FertiMinElement.CUIVRE;
        RefPrixFertiMin pulverulentCUIVRE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, cuivre,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 21.206159486709,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(cuivre, k -> new ArrayList<>()).add(pulverulentCUIVRE);

        FertiMinElement zinc = FertiMinElement.ZINC;
        RefPrixFertiMin pulverulentZINC = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, zinc,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 60.72953253895509,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(zinc, k -> new ArrayList<>()).add(pulverulentZINC);

        //campagne inférieure
        RefPrixFertiMin pulverulentCALCIUM2012 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, 2012,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 159222.0,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        //campagne supérieur
        RefPrixFertiMin pulverulentCALCIUMSup = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign() + 1,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 179333.0,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        // "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000"
        InputPriceFilter filter = InputPriceFilter.builder()
                .campaign(campaign)
                .objectId(priceBaulon.getObjectId())
                .displayName(priceBaulon.getDisplayName())
                .sourceUnit(priceBaulon.getSourceUnit())
                .priceUnit(priceBaulon.getPriceUnit())
                .category(priceBaulon.getCategory())
                .phytoProductType(priceBaulon.getPhytoProductType()).build();

        final ProductPriceSummary productPriceSummary = inputPriceService.loadPriceIndications(
                filter,
                priceBaulon.getDomain().getTopiaId()
        );

        // valide ObjectId to Qty by element
        Map<FertiMinElement, Double> expectedFertiMinElementWithQuantity = new HashMap<>();
        expectedFertiMinElementWithQuantity.put(n, 1.0);
        expectedFertiMinElementWithQuantity.put(p2O5, 2.0);
        expectedFertiMinElementWithQuantity.put(k2O, 3.0);
        expectedFertiMinElementWithQuantity.put(bore, 4.0);
        expectedFertiMinElementWithQuantity.put(calcium, 5.0);
        expectedFertiMinElementWithQuantity.put(fer, 6.0);
        expectedFertiMinElementWithQuantity.put(manganese, 7.0);
        expectedFertiMinElementWithQuantity.put(molybdene, 8.0);
        expectedFertiMinElementWithQuantity.put(mgO, 9.0);
        expectedFertiMinElementWithQuantity.put(oxydeDeSodium, 10.0);
        expectedFertiMinElementWithQuantity.put(sO3, 11.0);
        expectedFertiMinElementWithQuantity.put(cuivre, 12.0);
        expectedFertiMinElementWithQuantity.put(zinc, 13.0);
        Map<FertiMinElement, Double> fertiMinElementWithQuantity = InputPriceServiceImpl.OBJECT_ID_TO_FERTI_MIN_ELEMENTS_QUANTITY.apply(priceBaulon.getObjectId());
        Assertions.assertThat(fertiMinElementWithQuantity).isEqualTo(expectedFertiMinElementWithQuantity);

        double inputRefPrice = 0;
        for (Map.Entry<FertiMinElement, List<RefPrixFertiMin>> baulonRefPricesByElements : baulonCampaignRefPrixFertiMin.entrySet()) {
            FertiMinElement element = baulonRefPricesByElements.getKey();
            List<RefPrixFertiMin> refPrices = baulonRefPricesByElements.getValue();
            Double qtyFor100g = expectedFertiMinElementWithQuantity.get(element);
            double averageRefPriceForElement = (refPrices.stream().mapToDouble(RefInputPrice::getPrice).average().orElse(0) * qtyFor100g) / 100;
            inputRefPrice += averageRefPriceForElement;
        }

        Assertions.assertThat(productPriceSummary).isNotNull();
        Assertions.assertThat(productPriceSummary.lowerPrice()).isEqualTo(lowestPrice.getPrice());
        Assertions.assertThat(productPriceSummary.higherPrice()).isEqualTo(highestPrice.getPrice());
        Assertions.assertThat(productPriceSummary.medianPrice()).isEqualTo(12.0);
        Assertions.assertThat(productPriceSummary.averagePrice()).isEqualTo(13.5);

        Map<PriceUnit, RefInputPriceDto> refPricesByUnit = productPriceSummary.refInputPriceDtoByUnit();
        Assertions.assertThat(refPricesByUnit.size()).isEqualTo(1);
        Assertions.assertThat(refPricesByUnit.get(PriceUnit.EURO_KG).price()).isCloseTo(inputRefPrice, Percentage.withPercentage(1));
    }

    @Test
    public void testLoadPersistMineralInputUserInformationWithConvertiblePriceUnit() throws IOException, DomainExtendException {
        testDatas.createTestDomains();

        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        Domain domainCoueronLesBains = domainDao.forNameEquals("Couëron les bains").findUnique();
        Domain domainTestIFT = domainDao.forNameEquals("DOMAINE TEST IFT").findUnique();
        Domain domainDomaineDeVerchant = domainDao.forNameEquals("Domaine de verchant").findUnique();
        Domain domainLaBouineliere = domainDao.forNameEquals("La Bouinelière").findUnique();

        domainCoueronLesBains = domainService.extendDomain(domainCoueronLesBains.getTopiaId(), baulon.getCampaign());
        domainTestIFT = domainService.extendDomain(domainTestIFT.getTopiaId(), baulon.getCampaign());
        domainDomaineDeVerchant = domainService.extendDomain(domainDomaineDeVerchant.getTopiaId(), baulon.getCampaign());
        domainLaBouineliere = domainService.extendDomain(domainLaBouineliere.getTopiaId(), baulon.getCampaign());

        InputPrice priceBaulon = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, baulon,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 12.3,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        // other user prices
        InputPrice lowestPrice = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainCoueronLesBains,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 10.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainTestIFT,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 11.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainDomaineDeVerchant,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 13.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        InputPrice highestPrice = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainLaBouineliere,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 20.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        int campaign = priceBaulon.getDomain().getCampaign();

        Map<FertiMinElement, List<RefPrixFertiMin>> baulonCampaignRefPrixFertiMin = new HashMap<>();
        FertiMinElement n = FertiMinElement.N;
        RefPrixFertiMin pulverulentN = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, n,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 52.93666308655617,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(n, k -> new ArrayList<>()).add(pulverulentN);

        FertiMinElement p2O5 = FertiMinElement.P2_O5;
        RefPrixFertiMin pulverulentP2_O5 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, p2O5,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 87.07408416206262,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(p2O5, k -> new ArrayList<>()).add(pulverulentP2_O5);

        FertiMinElement k2O = FertiMinElement.K2_O;
        RefPrixFertiMin pulverulentK2_O = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, k2O,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 69.697521546961326/1000d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(k2O, k -> new ArrayList<>()).add(pulverulentK2_O);

        FertiMinElement bore = FertiMinElement.BORE;
        RefPrixFertiMin pulverulentBORE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, bore,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 13.6476626947754/100d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_Q
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(bore, k -> new ArrayList<>()).add(pulverulentBORE);

        FertiMinElement calcium = FertiMinElement.CALCIUM;
        RefPrixFertiMin pulverulentCALCIUM = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 56.159222731439047*0.001d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_G
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(calcium, k -> new ArrayList<>()).add(pulverulentCALCIUM);

        FertiMinElement fer = FertiMinElement.FER;
        RefPrixFertiMin pulverulentFER = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, fer,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 176.54874427131*0.000001d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_MG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(fer, k -> new ArrayList<>()).add(pulverulentFER);

        FertiMinElement manganese = FertiMinElement.MANGANESE;
        RefPrixFertiMin pulverulentMANGANESE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, manganese,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 139.888542621448,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(manganese, k -> new ArrayList<>()).add(pulverulentMANGANESE);

        FertiMinElement molybdene = FertiMinElement.MOLYBDENE;
        RefPrixFertiMin pulverulentMOLYBDENE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, molybdene,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 160.298295142071,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(molybdene, k -> new ArrayList<>()).add(pulverulentMOLYBDENE);

        FertiMinElement mgO = FertiMinElement.MG_O;
        RefPrixFertiMin pulverulentMG_O = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, mgO,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 81.570318600368324,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(mgO, k -> new ArrayList<>()).add(pulverulentMG_O);

        FertiMinElement oxydeDeSodium = FertiMinElement.OXYDE_DE_SODIUM;
        RefPrixFertiMin pulverulentOXYDE_DE_SODIUM = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, oxydeDeSodium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 62.64992117323556,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(oxydeDeSodium, k -> new ArrayList<>()).add(pulverulentOXYDE_DE_SODIUM);

        FertiMinElement sO3 = FertiMinElement.S_O3;
        RefPrixFertiMin pulverulentS_O3 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, sO3,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 32.598691712707182,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(sO3, k -> new ArrayList<>()).add(pulverulentS_O3);

        FertiMinElement cuivre = FertiMinElement.CUIVRE;
        RefPrixFertiMin pulverulentCUIVRE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, cuivre,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 21.206159486709,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(cuivre, k -> new ArrayList<>()).add(pulverulentCUIVRE);

        FertiMinElement zinc = FertiMinElement.ZINC;
        RefPrixFertiMin pulverulentZINC = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, zinc,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 60.72953253895509,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(zinc, k -> new ArrayList<>()).add(pulverulentZINC);

        //campagne inférieure
        RefPrixFertiMin pulverulentCALCIUM2012 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, 2012,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 159222.0,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        //campagne supérieur
        RefPrixFertiMin pulverulentCALCIUMSup = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign() + 1,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 179333.0,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        // "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000"
        InputPriceFilter filter = InputPriceFilter.builder()
                .campaign(campaign)
                .objectId(priceBaulon.getObjectId())
                .displayName(priceBaulon.getDisplayName())
                .sourceUnit(priceBaulon.getSourceUnit())
                .priceUnit(priceBaulon.getPriceUnit())
                .category(priceBaulon.getCategory())
                .phytoProductType(priceBaulon.getPhytoProductType()).build();

        final ProductPriceSummary productPriceSummary = inputPriceService.loadPriceIndications(
                filter,
                priceBaulon.getDomain().getTopiaId()
        );

        // valide ObjectId to Qty by element
        Map<FertiMinElement, Double> expectedFertiMinElementWithQuantity = new HashMap<>();
        expectedFertiMinElementWithQuantity.put(n, 1.0);
        expectedFertiMinElementWithQuantity.put(p2O5, 2.0);
        expectedFertiMinElementWithQuantity.put(k2O, 3.0);
        expectedFertiMinElementWithQuantity.put(bore, 4.0);
        expectedFertiMinElementWithQuantity.put(calcium, 5.0);
        expectedFertiMinElementWithQuantity.put(fer, 6.0);
        expectedFertiMinElementWithQuantity.put(manganese, 7.0);
        expectedFertiMinElementWithQuantity.put(molybdene, 8.0);
        expectedFertiMinElementWithQuantity.put(mgO, 9.0);
        expectedFertiMinElementWithQuantity.put(oxydeDeSodium, 10.0);
        expectedFertiMinElementWithQuantity.put(sO3, 11.0);
        expectedFertiMinElementWithQuantity.put(cuivre, 12.0);
        expectedFertiMinElementWithQuantity.put(zinc, 13.0);
        Map<FertiMinElement, Double> fertiMinElementWithQuantity = InputPriceServiceImpl.OBJECT_ID_TO_FERTI_MIN_ELEMENTS_QUANTITY.apply(priceBaulon.getObjectId());
        Assertions.assertThat(fertiMinElementWithQuantity).isEqualTo(expectedFertiMinElementWithQuantity);

        double inputRefPrice = 0;
        for (Map.Entry<FertiMinElement, List<RefPrixFertiMin>> baulonRefPricesByElements : baulonCampaignRefPrixFertiMin.entrySet()) {
            FertiMinElement element = baulonRefPricesByElements.getKey();
            List<RefPrixFertiMin> refPrices = baulonRefPricesByElements.getValue();
            Double qtyFor100g = expectedFertiMinElementWithQuantity.get(element);
            double averageRefPriceForElement = (refPrices.stream().mapToDouble(RefInputPrice::getPrice).average().orElse(0) * qtyFor100g) / 100;
            inputRefPrice += averageRefPriceForElement;
        }

        Assertions.assertThat(productPriceSummary).isNotNull();
        Assertions.assertThat(productPriceSummary.lowerPrice()).isEqualTo(lowestPrice.getPrice());
        Assertions.assertThat(productPriceSummary.higherPrice()).isEqualTo(highestPrice.getPrice());
        Assertions.assertThat(productPriceSummary.medianPrice()).isEqualTo(12.0);
        Assertions.assertThat(productPriceSummary.averagePrice()).isEqualTo(13.5);

        Map<PriceUnit, RefInputPriceDto> refPricesByUnit = productPriceSummary.refInputPriceDtoByUnit();
        Assertions.assertThat(refPricesByUnit.size()).isEqualTo(1);
        Assertions.assertThat(refPricesByUnit.get(PriceUnit.EURO_KG).price()).isCloseTo(inputRefPrice, Offset.offset(3.0));
    }

    @Test
    public void testLoadPersistMineralInputUserInformationWithNoConvertiblePriceUnit() throws IOException, DomainExtendException {
        testDatas.createTestDomains();

        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        Domain domainCoueronLesBains = domainDao.forNameEquals("Couëron les bains").findUnique();
        Domain domainTestIFT = domainDao.forNameEquals("DOMAINE TEST IFT").findUnique();
        Domain domainDomaineDeVerchant = domainDao.forNameEquals("Domaine de verchant").findUnique();
        Domain domainLaBouineliere = domainDao.forNameEquals("La Bouinelière").findUnique();

        domainCoueronLesBains = domainService.extendDomain(domainCoueronLesBains.getTopiaId(), baulon.getCampaign());
        domainTestIFT = domainService.extendDomain(domainTestIFT.getTopiaId(), baulon.getCampaign());
        domainDomaineDeVerchant = domainService.extendDomain(domainDomaineDeVerchant.getTopiaId(), baulon.getCampaign());
        domainLaBouineliere = domainService.extendDomain(domainLaBouineliere.getTopiaId(), baulon.getCampaign());

        InputPrice priceBaulon = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, baulon,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 12.3,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        // other user prices
        InputPrice lowestPrice = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainCoueronLesBains,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 10.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainTestIFT,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 11.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainDomaineDeVerchant,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 13.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        InputPrice highestPrice = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainLaBouineliere,
                InputPrice.PROPERTY_OBJECT_ID, "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000",
                InputPrice.PROPERTY_PRICE, 20.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_PHYTO_PRODUCT_TYPE, null,
                InputPrice.PROPERTY_SOURCE_UNIT, MineralProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "315_Pulverulent",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.MINERAL_INPUT);

        int campaign = priceBaulon.getDomain().getCampaign();

        Map<FertiMinElement, List<RefPrixFertiMin>> baulonCampaignRefPrixFertiMin = new HashMap<>();
        FertiMinElement n = FertiMinElement.N;
        RefPrixFertiMin pulverulentN = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, n,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 52.93666308655617,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(n, k -> new ArrayList<>()).add(pulverulentN);

        FertiMinElement p2O5 = FertiMinElement.P2_O5;
        RefPrixFertiMin pulverulentP2_O5 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, p2O5,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 87.07408416206262,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_L
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(p2O5, k -> new ArrayList<>()).add(pulverulentP2_O5);

        FertiMinElement k2O = FertiMinElement.K2_O;
        RefPrixFertiMin pulverulentK2_O = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, k2O,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 69.697521546961326/1000d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_T
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(k2O, k -> new ArrayList<>()).add(pulverulentK2_O);

        FertiMinElement bore = FertiMinElement.BORE;
        RefPrixFertiMin pulverulentBORE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, bore,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 13.6476626947754/100d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_Q
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(bore, k -> new ArrayList<>()).add(pulverulentBORE);

        FertiMinElement calcium = FertiMinElement.CALCIUM;
        RefPrixFertiMin pulverulentCALCIUM = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 56.159222731439047*0.001d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_G
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(calcium, k -> new ArrayList<>()).add(pulverulentCALCIUM);

        FertiMinElement fer = FertiMinElement.FER;
        RefPrixFertiMin pulverulentFER = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, fer,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 176.54874427131*0.000001d,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_MG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(fer, k -> new ArrayList<>()).add(pulverulentFER);

        FertiMinElement manganese = FertiMinElement.MANGANESE;
        RefPrixFertiMin pulverulentMANGANESE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, manganese,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 139.888542621448,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(manganese, k -> new ArrayList<>()).add(pulverulentMANGANESE);

        FertiMinElement molybdene = FertiMinElement.MOLYBDENE;
        RefPrixFertiMin pulverulentMOLYBDENE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, molybdene,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 160.298295142071,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(molybdene, k -> new ArrayList<>()).add(pulverulentMOLYBDENE);

        FertiMinElement mgO = FertiMinElement.MG_O;
        RefPrixFertiMin pulverulentMG_O = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, mgO,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 81.570318600368324,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(mgO, k -> new ArrayList<>()).add(pulverulentMG_O);

        FertiMinElement oxydeDeSodium = FertiMinElement.OXYDE_DE_SODIUM;
        RefPrixFertiMin pulverulentOXYDE_DE_SODIUM = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, oxydeDeSodium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 62.64992117323556,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(oxydeDeSodium, k -> new ArrayList<>()).add(pulverulentOXYDE_DE_SODIUM);

        FertiMinElement sO3 = FertiMinElement.S_O3;
        RefPrixFertiMin pulverulentS_O3 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, sO3,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 32.598691712707182,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(sO3, k -> new ArrayList<>()).add(pulverulentS_O3);

        FertiMinElement cuivre = FertiMinElement.CUIVRE;
        RefPrixFertiMin pulverulentCUIVRE = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, cuivre,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 21.206159486709,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(cuivre, k -> new ArrayList<>()).add(pulverulentCUIVRE);

        FertiMinElement zinc = FertiMinElement.ZINC;
        RefPrixFertiMin pulverulentZINC = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, zinc,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 60.72953253895509,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
        baulonCampaignRefPrixFertiMin.computeIfAbsent(zinc, k -> new ArrayList<>()).add(pulverulentZINC);

        //campagne inférieure
        RefPrixFertiMin pulverulentCALCIUM2012 = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, 2012,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 159222.0,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        //campagne supérieur
        RefPrixFertiMin pulverulentCALCIUMSup = mineralInputRePriceDao.create(
                RefPrixFertiMin.PROPERTY_CATEG, 315,
                RefPrixFertiMin.PROPERTY_FORME, "Pulverulent",
                RefPrixFertiMin.PROPERTY_ELEMENT, calcium,
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign() + 1,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 179333.0,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );

        // "categ=315 forme=Pulverulent n=1.000000 p2O5=2.000000 k2O=3.000000 bore=4.000000 calcium=5.000000 fer=6.000000 manganese=7.000000 molybdene=8.000000 mgO=9.000000 oxyde_de_sodium=10.000000 sO3=11.000000 cuivre=12.000000 zinc=13.000000"
        InputPriceFilter filter = InputPriceFilter.builder()
                .campaign(campaign)
                .objectId(priceBaulon.getObjectId())
                .displayName(priceBaulon.getDisplayName())
                .sourceUnit(priceBaulon.getSourceUnit())
                .priceUnit(priceBaulon.getPriceUnit())
                .category(priceBaulon.getCategory())
                .phytoProductType(priceBaulon.getPhytoProductType()).build();

        final ProductPriceSummary productPriceSummary = inputPriceService.loadPriceIndications(
                filter,
                priceBaulon.getDomain().getTopiaId()
        );

        // valide ObjectId to Qty by element
        Map<FertiMinElement, Double> expectedFertiMinElementWithQuantity = new HashMap<>();
        expectedFertiMinElementWithQuantity.put(n, 1.0);
        expectedFertiMinElementWithQuantity.put(p2O5, 2.0);
        expectedFertiMinElementWithQuantity.put(k2O, 3.0);
        expectedFertiMinElementWithQuantity.put(bore, 4.0);
        expectedFertiMinElementWithQuantity.put(calcium, 5.0);
        expectedFertiMinElementWithQuantity.put(fer, 6.0);
        expectedFertiMinElementWithQuantity.put(manganese, 7.0);
        expectedFertiMinElementWithQuantity.put(molybdene, 8.0);
        expectedFertiMinElementWithQuantity.put(mgO, 9.0);
        expectedFertiMinElementWithQuantity.put(oxydeDeSodium, 10.0);
        expectedFertiMinElementWithQuantity.put(sO3, 11.0);
        expectedFertiMinElementWithQuantity.put(cuivre, 12.0);
        expectedFertiMinElementWithQuantity.put(zinc, 13.0);
        Map<FertiMinElement, Double> fertiMinElementWithQuantity = InputPriceServiceImpl.OBJECT_ID_TO_FERTI_MIN_ELEMENTS_QUANTITY.apply(priceBaulon.getObjectId());
        Assertions.assertThat(fertiMinElementWithQuantity).isEqualTo(expectedFertiMinElementWithQuantity);

        Assertions.assertThat(productPriceSummary).isNotNull();
        Assertions.assertThat(productPriceSummary.lowerPrice()).isEqualTo(lowestPrice.getPrice());
        Assertions.assertThat(productPriceSummary.higherPrice()).isEqualTo(highestPrice.getPrice());
        Assertions.assertThat(productPriceSummary.medianPrice()).isEqualTo(12.0);
        Assertions.assertThat(productPriceSummary.averagePrice()).isEqualTo(13.5);

        Map<PriceUnit, RefInputPriceDto> refPricesByUnit = productPriceSummary.refInputPriceDtoByUnit();
        Assertions.assertThat(refPricesByUnit.size()).isEqualTo(1);
        Assertions.assertThat(refPricesByUnit.get(PriceUnit.EURO_HA).price()).isEqualTo(0d);
    }
    @Test
    public void testLoadPersistPrixPhytoInputUserInformation() throws IOException, DomainExtendException {
        testDatas.createTestDomains();
        
        Domain baulon = domainDao.forTopiaIdEquals(TestDatas.BAULON_TOPIA_ID).findUnique();
        Domain domainCoueronLesBains = domainDao.forNameEquals("Couëron les bains").findUnique();
        Domain domainTestIFT = domainDao.forNameEquals("DOMAINE TEST IFT").findUnique();
        Domain domainDomaineDeVerchant = domainDao.forNameEquals("Domaine de verchant").findUnique();
        Domain domainLaBouineliere = domainDao.forNameEquals("La Bouinelière").findUnique();
        
        domainCoueronLesBains = domainService.extendDomain(domainCoueronLesBains.getTopiaId(), baulon.getCampaign());
        domainTestIFT = domainService.extendDomain(domainTestIFT.getTopiaId(), baulon.getCampaign());
        domainDomaineDeVerchant = domainService.extendDomain(domainDomaineDeVerchant.getTopiaId(), baulon.getCampaign());
        domainLaBouineliere = domainService.extendDomain(domainLaBouineliere.getTopiaId(), baulon.getCampaign());
        
        InputPrice priceBaulon_4670_140 = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, baulon,
                InputPrice.PROPERTY_OBJECT_ID, "4670_140",
                InputPrice.PROPERTY_PRICE, 12.3,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_SOURCE_UNIT, PhytoProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "CUPROFIX M",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT);
        
        InputPrice lowestPrice_4670_140 = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainCoueronLesBains,
                InputPrice.PROPERTY_OBJECT_ID, "4670_140",
                InputPrice.PROPERTY_PRICE, 10.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_SOURCE_UNIT, PhytoProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "CUPROFIX M",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT);
        
        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainTestIFT,
                InputPrice.PROPERTY_OBJECT_ID, "4670_140",
                InputPrice.PROPERTY_PRICE, 11.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_SOURCE_UNIT, PhytoProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "CUPROFIX M",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT);
        
        inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainDomaineDeVerchant,
                InputPrice.PROPERTY_OBJECT_ID, "4670_140",
                InputPrice.PROPERTY_PRICE, 13.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_SOURCE_UNIT, PhytoProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "CUPROFIX M",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT);
        
        InputPrice highestPrice_4670_140 = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, domainLaBouineliere,
                InputPrice.PROPERTY_OBJECT_ID, "4670_140",
                InputPrice.PROPERTY_PRICE, 20.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_SOURCE_UNIT, PhytoProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "CUPROFIX M",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT);
        
        InputPrice priceBaulon_4200_153 = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, baulon,
                InputPrice.PROPERTY_OBJECT_ID, "4200_153",
                InputPrice.PROPERTY_PRICE, 12.3,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_SOURCE_UNIT, PhytoProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "BACTURA",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.SEEDING_TREATMENT_INPUT);
    
        final RefPrixPhyto refPriceBaulon_4670_140 = refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "4670",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 140,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "4670_140",
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign(),
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 8.45999999957169,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
    
        //campagne inférieure → ne doit pas remonter
        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "4200",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 153,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "4200_153",
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign() - 2,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 36.0606457056661,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
    
        //campagne supérieure → doit remonter
        final RefPrixPhyto refPriceBaulon_4200_153 = refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "4200",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 153,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "4200_153",
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign() + 1,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 36.0606457056661,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
    
        InputPrice priceBaulon_3210_140 = inputPriceDao.create(
                InputPrice.PROPERTY_DOMAIN, baulon,
                InputPrice.PROPERTY_OBJECT_ID, "3210_140",
                InputPrice.PROPERTY_PRICE, 11.0,
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_HA,
                InputPrice.PROPERTY_SOURCE_UNIT, PhytoProductUnit.KG_HA.name(),
                InputPrice.PROPERTY_DISPLAY_NAME,  "Soufrugec",
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.PHYTO_TRAITMENT_INPUT);
    
        //campagne inférieure → doit remonter
        final RefPrixPhyto refPriceBaulon_3210_140 = refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "3210",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 140,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "3210_140",
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign() - 1,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, 2.07805740616068,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
    
        //campagne supérieure → ne doit pas remonter
        refPrixPhytoDao.create(
                RefPrixPhyto.PROPERTY_ID_PRODUIT, "3210",
                RefPrixPhyto.PROPERTY_ID_TRAITEMENT, 140,
                RefPrixPhyto.PROPERTY_PHYTO_OBJECT_ID, "3210_140",
                RefInputPrice.PROPERTY_CAMPAIGN, baulon.getCampaign() + 1,
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_PRICE, Double.MAX_VALUE,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_KG
        );
    
        InputPriceFilter filter_4670_140 = InputPriceFilter.builder()
                .campaign(priceBaulon_4670_140.getDomain().getCampaign())
                .objectId(priceBaulon_4670_140.getObjectId())
                .displayName(priceBaulon_4670_140.getDisplayName())
                .sourceUnit(priceBaulon_4670_140.getSourceUnit())
                .priceUnit(priceBaulon_4670_140.getPriceUnit())
                .category(priceBaulon_4670_140.getCategory())
                .phytoProductType(priceBaulon_4670_140.getPhytoProductType()).build();
        
        final ProductPriceSummary productPriceBaulon_4670_140Summary = inputPriceService.loadPriceIndications(
                filter_4670_140,
                priceBaulon_4670_140.getDomain().getTopiaId()
        );
        
        Assertions.assertThat(productPriceBaulon_4670_140Summary).isNotNull();
        Assertions.assertThat(productPriceBaulon_4670_140Summary.lowerPrice()).isEqualTo(lowestPrice_4670_140.getPrice());
        Assertions.assertThat(productPriceBaulon_4670_140Summary.higherPrice()).isEqualTo(highestPrice_4670_140.getPrice());
        Assertions.assertThat(productPriceBaulon_4670_140Summary.medianPrice()).isEqualTo(12.0);
        Assertions.assertThat(productPriceBaulon_4670_140Summary.averagePrice()).isEqualTo(13.5);
        
        Map<PriceUnit, RefInputPriceDto> refPricesByUnit = productPriceBaulon_4670_140Summary.refInputPriceDtoByUnit();
        Assertions.assertThat(refPricesByUnit.size()).isEqualTo(1);
        Assertions.assertThat(refPricesByUnit.get(PriceUnit.EURO_KG).price()).isEqualTo(refPriceBaulon_4670_140.getPrice());
        Assertions.assertThat(refPricesByUnit.get(PriceUnit.EURO_KG).campaign()).isEqualTo(refPriceBaulon_4670_140.getCampaign());
    
        InputPriceFilter filter_4200_153 = InputPriceFilter.builder()
                .campaign(priceBaulon_4200_153.getDomain().getCampaign())
                .objectId(priceBaulon_4200_153.getObjectId())
                .displayName(priceBaulon_4200_153.getDisplayName())
                .sourceUnit(priceBaulon_4200_153.getSourceUnit())
                .priceUnit(priceBaulon_4200_153.getPriceUnit())
                .category(priceBaulon_4200_153.getCategory())
                .phytoProductType(priceBaulon_4200_153.getPhytoProductType()).build();
        
        final ProductPriceSummary productPriceBaulon_4200_153Summary = inputPriceService.loadPriceIndications(
                filter_4200_153,
                priceBaulon_4200_153.getDomain().getTopiaId()
        );
    
        Assertions.assertThat(productPriceBaulon_4200_153Summary).isNotNull();
        Assertions.assertThat(productPriceBaulon_4200_153Summary.lowerPrice()).isNull();
        Assertions.assertThat(productPriceBaulon_4200_153Summary.higherPrice()).isNull();
        Assertions.assertThat(productPriceBaulon_4200_153Summary.medianPrice()).isNull();
        Assertions.assertThat(productPriceBaulon_4200_153Summary.averagePrice()).isNull();
        Assertions.assertThat(productPriceBaulon_4200_153Summary.countedPrices()).isEqualTo(0);
    
        Map<PriceUnit, RefInputPriceDto> refPricesByUnitBaulon_4200_153 = productPriceBaulon_4200_153Summary.refInputPriceDtoByUnit();
        Assertions.assertThat(refPricesByUnitBaulon_4200_153.size()).isEqualTo(1);
        Assertions.assertThat(refPricesByUnitBaulon_4200_153.get(PriceUnit.EURO_KG).price()).isEqualTo(refPriceBaulon_4200_153.getPrice());
    
        InputPriceFilter filter_3210_140 = InputPriceFilter.builder()
                .campaign(priceBaulon_3210_140.getDomain().getCampaign())
                .objectId(priceBaulon_3210_140.getObjectId())
                .displayName(priceBaulon_3210_140.getDisplayName())
                .sourceUnit(priceBaulon_3210_140.getSourceUnit())
                .priceUnit(priceBaulon_3210_140.getPriceUnit())
                .category(priceBaulon_3210_140.getCategory())
                .phytoProductType(priceBaulon_3210_140.getPhytoProductType()).build();
    
        final ProductPriceSummary productPriceBaulon_3210_140Summary = inputPriceService.loadPriceIndications(
                filter_3210_140,
                priceBaulon_3210_140.getDomain().getTopiaId()
        );
    
        Assertions.assertThat(productPriceBaulon_3210_140Summary).isNotNull();
        Assertions.assertThat(productPriceBaulon_3210_140Summary.lowerPrice()).isNull();
        Assertions.assertThat(productPriceBaulon_3210_140Summary.higherPrice()).isNull();
        Assertions.assertThat(productPriceBaulon_3210_140Summary.medianPrice()).isNull();
        Assertions.assertThat(productPriceBaulon_3210_140Summary.averagePrice()).isNull();
        Assertions.assertThat(productPriceBaulon_3210_140Summary.countedPrices()).isEqualTo(0);

        Map<PriceUnit, RefInputPriceDto> refPricesByUnitBaulon_3210_140 = productPriceBaulon_3210_140Summary.refInputPriceDtoByUnit();
        Assertions.assertThat(refPricesByUnitBaulon_3210_140.size()).isEqualTo(1);
        Assertions.assertThat(refPricesByUnitBaulon_3210_140.get(PriceUnit.EURO_KG).price()).isEqualTo(refPriceBaulon_3210_140.getPrice());
    }
    
    @Test
    public void testHarverstingObjectId() {

        RefEspece espece = new RefEspeceImpl();
        espece.setCode_espece_botanique("code_espece_botanique_0");
        espece.setCode_qualifiant_AEE("code_qualifiant_AEE_0");
        espece.setCode_type_saisonnier_AEE("code_type_saisonnier_AEE");
        espece.setCode_destination_AEE("code_destination_AEE");
        espece.setActive(true);

        CroppingPlanSpecies cps = new CroppingPlanSpeciesImpl();
        cps.setTopiaId("cps_topiaid");
        cps.setCode("cps_code");
        cps.setSpecies(espece);

        RefDestination destination = new RefDestinationImpl();
        destination.setTopiaId("dest-id");
        destination.setActive(true);
        destination.setCode_destination_A("code_destination_A_0");

        String valorisationObjectId = PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(cps, destination);
        final List<String> elements = serviceContext.getGson().fromJson(valorisationObjectId, List.class);

        Assertions.assertThat(elements.get(0)).isEqualTo(cps.getSpecies().getCode_espece_botanique());
        Assertions.assertThat(elements.get(1)).isEqualTo(cps.getSpecies().getCode_qualifiant_AEE());
        Assertions.assertThat(elements.get(2)).isEqualTo(cps.getSpecies().getCode_type_saisonnier_AEE());
        Assertions.assertThat(elements.get(3)).isEqualTo(cps.getSpecies().getCode_destination_AEE());
    }

}
