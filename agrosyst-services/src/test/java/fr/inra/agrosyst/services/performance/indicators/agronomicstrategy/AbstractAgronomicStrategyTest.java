package fr.inra.agrosyst.services.performance.indicators.agronomicstrategy;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.PlotTopiaDao;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingTopiaDao;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.TillageActionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import org.junit.jupiter.api.BeforeEach;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class AbstractAgronomicStrategyTest extends AbstractAgrosystTest {

    protected final static String TRAVAIL_SOL = "travailsol";
    protected final static String BINEUSE = "Bineuse";
    protected final static String CULTIVATEUR = "Cultivateur";

    protected final static String LABOUR = "SEP";
    protected final static String TRAVAIL_DE_SURFACE = "V96";

    protected DomainService domainService;
    protected PerformanceServiceImplForTest performanceService;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected ImportService importService;

    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected PlotTopiaDao plotDao;
    protected GrowingSystemTopiaDao growingSystemDao;

    protected DomainTopiaDao domainDao;
    protected ZoneTopiaDao zoneDao;
    protected EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDao;
    protected EffectiveCropCycleNodeTopiaDao effectiveCropCycleNodeDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected ToolsCouplingTopiaDao toolsCouplingDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected TillageActionTopiaDao tillageActionDao;
    protected SeedingActionUsageTopiaDao seedingActionUsageDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeDao;
    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeDao;
    protected PracticedCropCyclePhaseTopiaDao practicedCropCyclePhasesDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;

    protected Domain baulon;
    protected RefCountry france;
    protected Collection<IndicatorFilter> indicatorFilters;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        testDatas = serviceFactory.newInstance(TestDatas.class);

        domainService = serviceFactory.newService(DomainService.class);
        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);
        importService = serviceFactory.newService(ImportService.class);

        // init dao
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();
        plotDao = getPersistenceContext().getPlotDao();
        growingSystemDao = getPersistenceContext().getGrowingSystemDao();

        domainDao = serviceContext.getPersistenceContext().getDomainDao();
        zoneDao = serviceContext.getPersistenceContext().getZoneDao();
        effectiveCropCycleNodeDao = serviceContext.getPersistenceContext().getEffectiveCropCycleNodeDao();
        effectivePerennialCropCycleDao = serviceContext.getPersistenceContext().getEffectivePerennialCropCycleDao();
        effectiveSeasonalCropCycleDao = serviceContext.getPersistenceContext().getEffectiveSeasonalCropCycleDao();
        effectiveCropCyclePhaseDao = serviceContext.getPersistenceContext().getEffectiveCropCyclePhaseDao();
        toolsCouplingDao = serviceContext.getPersistenceContext().getToolsCouplingDao();
        effectiveInterventionDao = serviceContext.getPersistenceContext().getEffectiveInterventionDao();
        tillageActionDao = serviceContext.getPersistenceContext().getTillageActionDao();
        seedingActionUsageDao = serviceContext.getPersistenceContext().getSeedingActionUsageDao();
        refInterventionAgrosystTravailEDIDao = serviceContext.getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        practicedSeasonalCropCycleDao = serviceContext.getPersistenceContext().getPracticedSeasonalCropCycleDao();
        practicedSpeciesStadeDao = serviceContext.getPersistenceContext().getPracticedSpeciesStadeDao();
        practicedCropCycleNodeDao = serviceContext.getPersistenceContext().getPracticedCropCycleNodeDao();
        practicedCropCyclePhasesDao = serviceContext.getPersistenceContext().getPracticedCropCyclePhaseDao();
        practicedCropCycleConnectionDao = serviceContext.getPersistenceContext().getPracticedCropCycleConnectionDao();
        practicedInterventionDao = serviceContext.getPersistenceContext().getPracticedInterventionDao();

        testDatas.createTestDomains();

        // login
        loginAsAdmin();
        alterSchema();

        indicatorFilters = performanceService.getAllIndicatorFilters();

        // initialisation des paramètres de test
        this.baulon = domainDao.forTopiaIdEquals("fr.inra.agrosyst.api.entities.Domain_bfcffdf9-a2aa-4fd7-a79f-4adee2aa8d3b").findUnique();
        this.france = baulon.getLocation().getRefCountry();

        testDatas.createTestPlots();
    }

    protected record EffectiveTestData(ToolsCoupling toolsCoupling, RefInterventionAgrosystTravailEDI mainAction,
                                       double spatialFrequency, int transitCount, String result, LocalDate dateSemis,
                                       int completion) {
    }

    protected record EffectiveWeedingTestData(String toolsCoupling, String mainAction, double spatialFrequency,
                                              int transitCount, String result, String completion, LocalDate dateSemis) {
    }

    protected record EffectiveTillageTypeTestData(int index, String toolsCoupling, String mainAction,
                                                  double spatialFrequency,
                                                  int transitCount, String result, String completion,
                                                  LocalDate dateSemis) {
    }

    protected record TillageCurrentScaleToPreviousScaleTestData(int index, int fromIndex, int toIndex,
                                                                String result) {
    }

    protected record EffectiveTcsTestData(String toolsCoupling, String mainAction, double spatialFrequency,
                                          int transitCount, String result, String completion, LocalDate dateSemis) {
    }

    protected record PracticedTestData(ToolsCoupling toolsCoupling, RefInterventionAgrosystTravailEDI mainAction,
                                       double spatialFrequency, double temporalFrequency, String result,
                                       LocalDate dateSemis) {
    }

    protected record PracticedWeedingTestData(int index, String toolsCoupling, String mainAction,
                                              double spatialFrequency,
                                              double temporalFrequency, String result, String completion,
                                              LocalDate dateSemis) {
    }

    protected record PracticedTillageTestData(int index, String toolsCoupling, String mainAction,
                                              double spatialFrequency,
                                              double temporalFrequency,
                                              String result,
                                              String completion,
                                              LocalDate tillageStartDate,
                                              LocalDate tillageEndingDate,
                                              LocalDate seedingStatingDate,
                                              LocalDate seedingEndingDate) {
    }

    protected record PracticedTillageDiffDayTestData(int index, String toolsCoupling, String mainAction,
                                              double spatialFrequency,
                                              double temporalFrequency,
                                              double result,
                                              LocalDate tillageStartDate,
                                              LocalDate tillageEndingDate,
                                              LocalDate seedingStatingDate,
                                              LocalDate seedingEndingDate) {
    }

    protected record PracticedTcsTestData(String toolsCoupling, String mainAction, double spatialFrequency,
                                          double temporalFrequency, String result, String completion,
                                          LocalDate dateSemis) {
    }

    protected void createEffectiveIntervention(EffectiveCropCycleNode node,
                                               EffectiveCropCyclePhase cropCyclePhase,
                                               ToolsCoupling toolsCoupling,
                                               RefInterventionAgrosystTravailEDI mainAction,
                                               String name,
                                               LocalDate startDate,
                                               LocalDate endDate,
                                               double spatialFrequency,
                                               int transitCount) {
        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, node,
                EffectiveIntervention.PROPERTY_NAME, name,
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.TRAVAIL_DU_SOL,
                EffectiveIntervention.PROPERTY_WORK_RATE, toolsCoupling != null ? toolsCoupling.getWorkRate() : null,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, toolsCoupling != null ? toolsCoupling.getWorkRateUnit() : null,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, startDate,
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, endDate,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, transitCount,
                EffectiveIntervention.PROPERTY_TOOL_COUPLINGS, toolsCoupling != null ? Lists.newArrayList(toolsCoupling) : new ArrayList<>()
        );
        tillageActionDao.create(
                AbstractAction.PROPERTY_TOOLS_COUPLING_CODE, toolsCoupling != null ? toolsCoupling.getCode() : null,
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, mainAction
        );
    }

    protected void createSemisEffectiveIntervention(EffectiveCropCycleNode node,
                                                    EffectiveCropCyclePhase cropCyclePhase,
                                                    ToolsCoupling toolsCoupling,
                                                    String name,
                                                    LocalDate startDate,
                                                    LocalDate endDate,
                                                    double spatialFrequency,
                                                    int transitCount) {
        RefInterventionAgrosystTravailEDI set = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUniqueOrNull();
        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_NODE, node,
                EffectiveIntervention.PROPERTY_NAME, name,
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                EffectiveIntervention.PROPERTY_WORK_RATE, toolsCoupling != null ? toolsCoupling.getWorkRate() : null,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, toolsCoupling != null ? toolsCoupling.getWorkRateUnit() : null,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, startDate,
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, endDate,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, transitCount
        );

        seedingActionUsageDao.create(
                AbstractAction.PROPERTY_TOOLS_COUPLING_CODE, toolsCoupling != null ? toolsCoupling.getCode() : null,
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, set,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, Collections.emptyList(),
                SeedingActionUsage.PROPERTY_SUBSTRATE_INPUT_USAGE, Collections.emptyList(),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );
    }

    protected void assertEffectiveIntervention(String content, String name, String indicatorName, String result, String completion) {
        if (result != null) {
            assertThat(content).usingComparator(comparator).isEqualTo(
                    "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Plot Baulon 1;Zone principale;Blé;;Blé tendre;;;;Pleine production;Travail du sol;" + name + ";Travail du sol;;;N;;2013;Réalisé;" +
                            "Stratégie agronomique;" + indicatorName + ";" + result + ";;" + completion + ";;;"
            );
        } else {
            assertThat(content.contains("interventionSheet;")).isFalse();
        }
    }

    protected void assertEffectiveSeasonalCrop(String content, String indicatorName, String result) {
        assertThat(content).usingComparator(comparator).isEqualTo(
                "croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Blé (rang 1);" +
                        "Blé tendre;;précédent non renseigné;;2013;Réalisé;Stratégie agronomique;" + indicatorName + ";" + result + ";"
        );
    }

    protected void assertEffectiveSeasonalPlot(String content, String indicatorName, String result) {
        // plotSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;;2013;Réalisé;Stratégie agronomique;Type de travail du sol;LABOUR;;100;
        assertThat(content).usingComparator(comparator).isEqualTo(
                "plotSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;;2013;Réalisé;Stratégie agronomique;" + indicatorName + ";" + result + ";"
        );
    }

    protected void assertEffectiveSeasonalSDC(String content, String indicatorName, String result) {
        // growingSystemSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;NON;;2013;Réalisé;Stratégie agronomique;Type de travail du sol;LABOUR_SYSTEMATIQUE;;0;;
        assertThat(content).usingComparator(comparator).isEqualTo(
                "growingSystemSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;NON;;2013;Réalisé;Stratégie agronomique;" + indicatorName + ";" + result + ";"
        );
    }

    protected void assertEffectivePerennialCrop(String content, String indicatorName, String result) {
        assertThat(content).usingComparator(comparator).isEqualTo(
                "croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Blé;Blé tendre;;Pleine production;;" +
                        "2013;Réalisé;Stratégie agronomique;" + indicatorName + ";" + result + ";"
        );
    }

    protected void assertPracticedSeasonalCrop(String content, String indicatorName, String result) {
        assertThat(content).usingComparator(comparator).isEqualTo(
                "croppingPlanSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);Blé tendre;;Blé;;0.0;2012, 2013;" +
                        "Synthétisé;Stratégie agronomique;" + indicatorName + ";" + result + ";"
        );
    }

    protected PracticedIntervention createPracticedIntervention(PracticedCropCycleConnection practicedCropCycleConnection,
                                                                CroppingPlanSpecies croppingPlanSpecies,
                                                                ToolsCoupling toolsCoupling,
                                                                RefInterventionAgrosystTravailEDI mainAction,
                                                                String name,
                                                                String startPeriodDate,
                                                                String endPeriodDate,
                                                                double spatialFrequency,
                                                                double temporalFrequency) {
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, practicedCropCycleConnection,
                PracticedIntervention.PROPERTY_TOPIA_ID, name,
                PracticedIntervention.PROPERTY_NAME, name,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.TRAVAIL_DU_SOL,
                PracticedIntervention.PROPERTY_WORK_RATE, toolsCoupling != null ? toolsCoupling.getWorkRate() : null,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, toolsCoupling != null ? toolsCoupling.getWorkRateUnit() : null,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, startPeriodDate,
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, endPeriodDate,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency,
                PracticedIntervention.PROPERTY_TOOLS_COUPLING_CODES, toolsCoupling != null ? Lists.newArrayList(toolsCoupling.getCode()) : new ArrayList<>());
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, croppingPlanSpecies.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        tillageActionDao.create(
                AbstractAction.PROPERTY_TOOLS_COUPLING_CODE, toolsCoupling != null ? toolsCoupling.getCode() : null,
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, mainAction
        );
        return intervention;
    }

    protected void createSemisPracticedIntervention(PracticedCropCycleConnection practicedCropCycleConnection,
                                                    ToolsCoupling toolsCoupling,
                                                    String name,
                                                    String startPeriodDate,
                                                    String endPeriodDate,
                                                    double spatialFrequency,
                                                    double temporalFrequency) {
        RefInterventionAgrosystTravailEDI set = refInterventionAgrosystTravailEDIDao.forNaturalId("SET").findUniqueOrNull();
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, practicedCropCycleConnection,
                PracticedIntervention.PROPERTY_NAME, name,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.SEMIS,
                PracticedIntervention.PROPERTY_WORK_RATE, toolsCoupling != null ? toolsCoupling.getWorkRate() : null,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, toolsCoupling != null ? toolsCoupling.getWorkRateUnit() : null,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, startPeriodDate,
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, endPeriodDate,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency
        );
        seedingActionUsageDao.create(
                AbstractAction.PROPERTY_TOOLS_COUPLING_CODE, toolsCoupling != null ? toolsCoupling.getCode() : null,
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, set,
                SeedingActionUsage.PROPERTY_SEED_TYPE, SeedType.SEMENCES_DE_FERME,
                SeedingActionUsage.PROPERTY_SEED_LOT_INPUT_USAGE, Collections.emptyList(),
                SeedingActionUsage.PROPERTY_SUBSTRATE_INPUT_USAGE, Collections.emptyList(),
                SeedingActionUsage.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );
    }

    protected void assertPracticedIntervention(String content, String name, String indicatorName, String result, String completion) {
        if (result != null) {
            assertThat(content).usingComparator(comparator).isEqualTo(
                    "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Travail du sol;" + name + ";" + name +
                            ";15/09;15/09;Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Stratégie agronomique;" + indicatorName + ";" + result + ";" + completion + ";;;"
            );
        } else {
            assertThat(content.contains("interventionSheet;")).isFalse();
        }
    }
}
