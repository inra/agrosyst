package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorToxicUserUsesTest extends AbstractQSATest {

    @Test
    public void testSynthetise12971() throws IOException {
        StringWriter out = launchPerformanceTest(IndicatorToxicUserUses.class, true);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 1;Intervention_1_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 1;Intervention_1_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 2;Intervention_2_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 2;Intervention_2_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 3;Intervention_3_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 unité/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 3;Intervention_3_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 unité/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 4;Intervention_4_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3000.0 g/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 4;Intervention_4_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3000.0 g/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 5;Intervention_5_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATHLET (3.6 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.5;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 5;Intervention_5_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATHLET (3.6 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.5;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 6;Intervention_6_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TARAK (0.11 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 6;Intervention_6_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TARAK (0.11 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 7;Intervention_7_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (null kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 7;Intervention_7_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (null kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 8;Intervention_8_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATHLET (null L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.5;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 8;Intervention_8_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATHLET (null L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.5;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 9;Intervention_9_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TARAK (null L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 9;Intervention_9_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TARAK (null L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 10;03/04;03/04;Semis;MALIS (12.0 kg/ha);;non;2012, 2013;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 10;03/04;03/04;Semis;MALIS (12.0 kg/ha);;non;2012, 2013;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;");
    }

    @Test
    public void testRealise12971() throws IOException {

        StringWriter out = launchPerformanceTest(IndicatorToxicUserUses.class, false);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 1;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (12.0 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 1;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (12.0 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 2;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (2.1 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 2;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (2.1 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 3;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (5.0 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 3;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (5.0 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 4;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (2.5 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.5;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 4;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (2.5 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.5;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (0.5 kg/ha);;non;2013;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (0.5 kg/ha);;non;2013;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 6;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (0.63 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 6;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (0.63 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 7;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 7;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 8;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 8;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 9;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 9;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 10;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.5;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 10;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.5;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 11;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (null kg/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 11;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (null kg/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 12;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 12;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (null L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 13;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 13;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 14;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 14;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 15;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 15;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 16;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 16;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 17;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 17;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 18;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);GAUCHO DUO FS (1.0 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\";1.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Intervention 18;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);GAUCHO DUO FS (1.0 L/ha);;N;;2013;Réalisé;Substances actives;Recours à des produits \"Toxiques utilisateurs\" (hors traitement de semence);1.0;;100;;;");
    }
}
