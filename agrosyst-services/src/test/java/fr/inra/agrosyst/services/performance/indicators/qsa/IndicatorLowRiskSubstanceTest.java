package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorLowRiskSubstanceTest extends AbstractQSATest {

    @Test
    public void testIndicatorLowRiskSubstancesSynthetise_12307() throws IOException {
        testDatas.createPracticedDataSetForQSAIndicators();

        /*
         * Calcul des indicateurs
         */

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        IndicatorLowRiskSubstances indicator = serviceFactory.newInstance(IndicatorLowRiskSubstances.class);
        indicator.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicator);
        final String content = out.toString();

        // Vérifications

        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 1;Intervention_1_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 1;Intervention_1_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 kg/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 3;Intervention_3_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 unité/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 3;Intervention_3_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 unité/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 4;Intervention_4_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3000.0 g/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 4;Intervention_4_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3000.0 g/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;Intervention_5_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATHLET (3.6 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque;0.9;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;Intervention_5_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATHLET (3.6 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.9;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 6;Intervention_6_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TARAK (0.11 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque;0.055;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 6;Intervention_6_ID;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TARAK (0.11 L/ha);1;;;non;;2012, 2013;Synthétisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.055;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 10;03/04;03/04;Semis;MALIS (12.0 kg/ha);;non;2012, 2013;Substances actives;QSA Substances à faible risque;0.048;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 10;03/04;03/04;Semis;MALIS (12.0 kg/ha);;non;2012, 2013;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;100;;;");
    }

    @Test
    public void testIndicatorLowRiskSubstancesRealise_12307() throws IOException {
        testDatas.createEffectiveDataSetForQSAIndicators();
        // import dose de référence
        testDatas.importActaDosageSpc();
        //testDatas.importRefMaaDosesRefParGroupeCible();
        /*
         * Calcul des indicateurs
         */

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(false);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);
        IndicatorLowRiskSubstances indicator = serviceFactory.newInstance(IndicatorLowRiskSubstances.class);
        indicator.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicator);
        final String content = out.toString();

        // Vérifications
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 1;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (12.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 1;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (12.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 2;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (2.1 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 2;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (2.1 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 3;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (5.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 3;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (5.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 4;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (2.5 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.8;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 4;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (2.5 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.8;;100;;;");

        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (0.5 kg/ha);;non;2013;Substances actives;QSA Substances à faible risque;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (0.5 kg/ha);;non;2013;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (678.0 L/ha);;non;2013;Substances actives;QSA Substances à faible risque;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (678.0 L/ha);;non;2013;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;100;;;");

        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 6;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (0.63 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 6;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (0.63 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 7;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 7;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);PREDATOR (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 8;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 8;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);DUAL GOLD SAFENEUR (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 9;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 9;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TOMENTAN (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 10;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.8;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 10;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);STEEL (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.8;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 11;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (null kg/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 11;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (null kg/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 12;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;50;;;");
//        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 12;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);VECALITEPI (null L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;50;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 13;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 13;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 14;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 14;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 15;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 15;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 16;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 16;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 17;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 17;Semis;Intrant de semis de test sur culture Blé;;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 18;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);GAUCHO DUO FS (1.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque;0.0;;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 18;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);GAUCHO DUO FS (1.0 L/ha);;N;;2013;Réalisé;Substances actives;QSA Substances à faible risque hors traitement de semence;0.0;;100;;;");

    }
}
