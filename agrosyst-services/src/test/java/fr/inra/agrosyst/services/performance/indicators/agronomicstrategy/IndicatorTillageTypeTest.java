package fr.inra.agrosyst.services.performance.indicators.agronomicstrategy;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorTillageTypeTest extends AbstractAgronomicStrategyTest {

    private static String TRAVAIL_SOL = "travailsol";
    private static String BINEUSE = "Bineuse";
    private static String CULTIVATEUR = "Cultivateur";

    private static String LABOUR = "SEP";
    private static String TRAVAIL_DE_SURFACE = "V96";

    private static Stream<EffectiveTillageTypeTestData> effectiveInterventionArgs() {
        return Stream.of(                                                                                                                                                                                                //      Date de semi ; Semi direct
                new EffectiveTillageTypeTestData(1, TRAVAIL_SOL, LABOUR, 0.5d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),              // 1    30/09/23       NON
                new EffectiveTillageTypeTestData(2, TRAVAIL_SOL, LABOUR, 0.66d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 2    30/09/23       NON
                new EffectiveTillageTypeTestData(3, BINEUSE, TRAVAIL_DE_SURFACE, 0.5d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),      // 3    30/09/23       NON
                new EffectiveTillageTypeTestData(4, BINEUSE, TRAVAIL_DE_SURFACE, 0.66d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),     // 4    30/09/23       NON
                new EffectiveTillageTypeTestData(5, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)), // 5    30/09/23       NON
                new EffectiveTillageTypeTestData(6, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)), // 6    30/09/23       NON
                new EffectiveTillageTypeTestData(7, TRAVAIL_SOL, LABOUR, 0.25d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 7    30/09/23       NON
                new EffectiveTillageTypeTestData(8, TRAVAIL_SOL, LABOUR, 0.75d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 8    30/09/23       NON
                new EffectiveTillageTypeTestData(9, BINEUSE, TRAVAIL_DE_SURFACE, 0.25d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),     // 9    30/09/23       NON
                new EffectiveTillageTypeTestData(10, BINEUSE, TRAVAIL_DE_SURFACE, 0.75d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),     // 10   30/09/23       NON
                new EffectiveTillageTypeTestData(11, TRAVAIL_SOL, LABOUR, 1.0d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),               // 11   01/09/23       NON
                new EffectiveTillageTypeTestData(12, TRAVAIL_SOL, LABOUR, 0.33d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),              // 12   01/09/23       NON
                new EffectiveTillageTypeTestData(13, BINEUSE, TRAVAIL_DE_SURFACE, 1.0d, 1, null, "100", LocalDate.of(2023, 9, 1)),       // 13   01/09/23       OUI (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new EffectiveTillageTypeTestData(14, BINEUSE, TRAVAIL_DE_SURFACE, 0.33d, 1, null, "100", LocalDate.of(2023, 9, 1)),      // 14   01/09/23       OUI (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new EffectiveTillageTypeTestData(15, TRAVAIL_SOL, LABOUR, 1.0d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),               // 15   01/09/23       NON
                new EffectiveTillageTypeTestData(16, TRAVAIL_SOL, LABOUR, 0.45d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),              // 16   01/09/23       NON
                new EffectiveTillageTypeTestData(17, TRAVAIL_SOL, LABOUR, 0.5d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),              // 17   30/09/23       NON
                new EffectiveTillageTypeTestData(18, BINEUSE, TRAVAIL_DE_SURFACE, 1.0d, 1, null, "100", LocalDate.of(2023, 9, 1)),       // 18   01/09/23       OUI (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new EffectiveTillageTypeTestData(19, BINEUSE, TRAVAIL_DE_SURFACE, 0.45d, 1, null, "100", LocalDate.of(2023, 9, 1)),      // 19   01/09/23       OUI (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new EffectiveTillageTypeTestData(58, TRAVAIL_SOL, LABOUR, 0.45d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 21   30/09/23       OUI
                new EffectiveTillageTypeTestData(59, TRAVAIL_SOL, LABOUR, 0.45d, 1, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 22   30/09/23       NON
                new EffectiveTillageTypeTestData(60, BINEUSE, TRAVAIL_DE_SURFACE, 0.66d, 1, null, "100", LocalDate.of(2023, 9, 1)),      // 23   01/09/23       NON (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new EffectiveTillageTypeTestData(61, BINEUSE, TRAVAIL_DE_SURFACE, 0.75d, 1, null, "100", LocalDate.of(2023, 9, 1)),      // 24   01/09/23       OUI (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new EffectiveTillageTypeTestData(62, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.33d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)), // 25   30/09/23       OUI
                new EffectiveTillageTypeTestData(63, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.1d, 1, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30))   // 26   30/09/23       NON
        );
    }

    @ParameterizedTest
    @MethodSource("effectiveInterventionArgs")
    public void testTillageTypeRealiseInterventionScale_12350(EffectiveTillageTypeTestData interventionData) throws IOException {
        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        List<Zone> zones = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create intervention
        RefInterventionAgrosystTravailEDI mainAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));
        final ToolsCoupling toolsCoupling = interventionData.toolsCoupling() != null ? toolsCouplingByName.get(interventionData.toolsCoupling()) : null;


        createEffectiveIntervention(
                null,
                cropCyclePhase,
                toolsCoupling,
                mainAction,
                "intervention_" + interventionData.index(),
                LocalDate.of(2023, 9, 15),
                LocalDate.of(2023, 9, 15),
                interventionData.spatialFrequency(),
                interventionData.transitCount()
        );
        createSemisEffectiveIntervention(
                null,
                cropCyclePhase,
                toolsCoupling,
                "intervention_semis_" + interventionData.index(),
                interventionData.dateSemis(),
                interventionData.dateSemis(),
                interventionData.spatialFrequency(),
                interventionData.transitCount()
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTillageType indicatorTillageType = serviceFactory.newInstance(IndicatorTillageType.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorTillageType);
        final String content = out.toString();

        assertEffectiveIntervention(content, "intervention_" + interventionData.index(), "Type de travail du sol", interventionData.result(), interventionData.completion());
    }

    private static Stream<TillageCurrentScaleToPreviousScaleTestData> effectiveCropArgs() {
        return Stream.of(
                new TillageCurrentScaleToPreviousScaleTestData(1, 1, 6, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(2, 7, 10, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(3, 11, 14, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(4, 15, 16, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(5, 17, 17, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(6, 18, 19, TillageType.SEMIS_DIRECT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(20, 58, 59, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(21, 60, 61, TillageType.SEMIS_DIRECT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(22, 62, 63, TillageType.TCS.name())
        );
    }

    private static Stream<TillageCurrentScaleToPreviousScaleTestData> effectivePlotArgs() {
        return Stream.of(
                new TillageCurrentScaleToPreviousScaleTestData(1, 1, 2, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(2, 3, 5, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(3, 6, 6, TillageType.SEMIS_DIRECT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(11, 20, 20, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(12, 21, 21, TillageType.SEMIS_DIRECT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(13, 22, 22, TillageType.TCS.name())
        );
    }

    private static Stream<TillageCurrentScaleToPreviousScaleTestData> effectiveSDCArgs() {
        return Stream.of(
                new TillageCurrentScaleToPreviousScaleTestData(1, 1, 3, TillageType.LABOUR_FREQUENT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(6, 11, 11, TillageType.LABOUR_SYSTEMATIQUE.name()),
                new TillageCurrentScaleToPreviousScaleTestData(7, 12, 12, TillageType.SEMIS_DIRECT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(8, 13, 20, TillageType.TCS.name())
        );
    }

    @ParameterizedTest
    @MethodSource("effectiveSDCArgs")
    public void testTillageTypeRealiseSdcScale_12350(TillageCurrentScaleToPreviousScaleTestData sdcData) throws IOException {
        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = croppingPlanEntryDao.forDomainEquals(baulon).findAll();
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        List<Zone> zones = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zoneModel = zones.getFirst();
        Plot plotModel = zoneModel.getPlot();
        GrowingSystem sdcModel = plotModel.getGrowingSystem();

        Binder<CroppingPlanEntry, CroppingPlanEntry> cropBinder = BinderFactory.newBinder(CroppingPlanEntry.class);
        Binder<CroppingPlanSpecies, CroppingPlanSpecies> speciesBinder = BinderFactory.newBinder(CroppingPlanSpecies.class);
        Binder<GrowingSystem, GrowingSystem> growingSystemBinder = BinderFactory.newBinder(GrowingSystem.class);
        Binder<Plot, Plot> plotBinder = BinderFactory.newBinder(Plot.class);
        Binder<Zone, Zone> zoneBinder = BinderFactory.newBinder(Zone.class);

        GrowingSystem currentSDC = growingSystemDao.newInstance();
        growingSystemBinder.copyExcluding(sdcModel, currentSDC, TopiaEntity.PROPERTY_TOPIA_ID, GrowingSystem.PROPERTY_CODE);
        currentSDC.setCode("sdcCode_" + sdcData.index());
        growingSystemDao.create(currentSDC);

        Stream<TillageCurrentScaleToPreviousScaleTestData> plotDatas = effectivePlotArgs().filter(p -> p.index() >= sdcData.fromIndex() && p.index() <= sdcData.toIndex());

        plotDatas.forEach(
                plotData -> {

                    Plot plot = plotDao.newInstance();
                    plotBinder.copyExcluding(plotModel, plot, TopiaEntity.PROPERTY_TOPIA_ID, Plot.PROPERTY_GROWING_SYSTEM, Plot.PROPERTY_NAME, Plot.PROPERTY_CODE, Plot.PROPERTY_E_DAPLOS_ISSUER_ID);
                    plot.setName("plotName_" + plotData.index());
                    plot.setCode("plotCode_" + plotData.index());
                    plot.setGrowingSystem(currentSDC);
                    plotDao.create(plot);

                    Zone zone = zoneDao.newInstance();
                    zoneBinder.copyExcluding(zoneModel, zone, TopiaEntity.PROPERTY_TOPIA_ID, Zone.PROPERTY_PLOT, Zone.PROPERTY_NAME, Zone.PROPERTY_CODE);
                    zone.setPlot(plot);
                    zone.setName("zoneName_" + plotData.index());
                    zone.setCode("zoneCode_" + plotData.index());
                    zoneDao.create(zone);

                    List<EffectiveCropCycleNode> nodes = Lists.newArrayList();
                    EffectiveSeasonalCropCycle effectiveSeasonalCropCycle = effectiveSeasonalCropCycleDao.create(
                            EffectiveSeasonalCropCycle.PROPERTY_ZONE, zone,
                            EffectiveSeasonalCropCycle.PROPERTY_NODES, nodes);

                    Stream<TillageCurrentScaleToPreviousScaleTestData> cropDatas = effectiveCropArgs();
                    cropDatas.filter(cropData -> cropData.index() >= plotData.fromIndex() && cropData.index() <= plotData.toIndex())
                            .forEach(
                                    cropData -> {
                                        CroppingPlanEntry cpe = croppingPlanEntryDao.newInstance();
                                        cropBinder.copyExcluding(cultureBle, cpe, TopiaEntity.PROPERTY_TOPIA_ID, CroppingPlanEntry.PROPERTY_CODE, CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);
                                        cpe.setName("cropName_" + cropData.index());
                                        cpe.setCode("cropCode_" + cropData.index());
                                        croppingPlanEntryDao.create(cpe);

                                        Collection<CroppingPlanSpecies> croppingPlanSpecies = CollectionUtils.emptyIfNull(cpe.getCroppingPlanSpecies());
                                        for (CroppingPlanSpecies croppingPlanSpecy : croppingPlanSpecies) {
                                            CroppingPlanSpecies cps = croppingPlanSpeciesDao.newInstance();
                                            speciesBinder.copyExcluding(croppingPlanSpecy, cps, TopiaEntity.PROPERTY_TOPIA_ID, CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY);
                                            cps.setCroppingPlanEntry(cpe);
                                            cpe.addCroppingPlanSpecies(cps);
                                            croppingPlanSpeciesDao.create(cps);
                                        }
                                        croppingPlanEntryDao.update(cpe);

                                        EffectiveCropCycleNode node = effectiveCropCycleNodeDao.create(
                                                EffectiveCropCycleNode.PROPERTY_RANK, 0,
                                                EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY, cpe
                                        );
                                        effectiveSeasonalCropCycle.addNodes(node);
                                        effectiveSeasonalCropCycleDao.update(effectiveSeasonalCropCycle);

                                        Stream<EffectiveTillageTypeTestData> interventionDatas = effectiveInterventionArgs();
                                        interventionDatas.forEach(
                                                interventionData -> {
                                                    if (interventionData.index() >= cropData.fromIndex() && interventionData.index() <= cropData.toIndex()) {
                                                        RefInterventionAgrosystTravailEDI mainAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
                                                        final ToolsCoupling toolsCoupling = interventionData.toolsCoupling() != null ? toolsCouplingByName.get(interventionData.toolsCoupling()) : null;

                                                        createEffectiveIntervention(
                                                                node,
                                                                null,
                                                                toolsCoupling,
                                                                mainAction,
                                                                "intervention_" + interventionData.index(),
                                                                LocalDate.of(2023, 9, 15),
                                                                LocalDate.of(2023, 9, 15),
                                                                interventionData.spatialFrequency(),
                                                                interventionData.transitCount()
                                                        );

                                                        createSemisEffectiveIntervention(
                                                                node,
                                                                null,
                                                                toolsCoupling,
                                                                "intervention_semis_" + interventionData.index(),
                                                                interventionData.dateSemis(),
                                                                interventionData.dateSemis(),
                                                                interventionData.spatialFrequency(),
                                                                interventionData.transitCount()
                                                        );
                                                    }
                                                }
                                        );

                                    }
                            );
                }
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTillageType indicatorTillageType = serviceFactory.newInstance(IndicatorTillageType.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorTillageType);
        final String content = out.toString();

        assertEffectiveSeasonalSDC(content, "Type de travail du sol", sdcData.result());
    }

    @ParameterizedTest
    @MethodSource("effectivePlotArgs")
    public void testTillageTypeRealisePlotScale_12350(TillageCurrentScaleToPreviousScaleTestData plotData) throws IOException {
        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        List<Zone> zones = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone p0 = zones.getFirst();

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = croppingPlanEntryDao.forDomainEquals(baulon).findAll();
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create intervention
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));

        List<EffectiveCropCycleNode> nodes = Lists.newArrayList();
        EffectiveSeasonalCropCycle effectiveSeasonalCropCycle = effectiveSeasonalCropCycleDao.create(
                EffectiveSeasonalCropCycle.PROPERTY_ZONE, p0,
                EffectiveSeasonalCropCycle.PROPERTY_NODES, nodes);

        Binder<CroppingPlanEntry, CroppingPlanEntry> cropBinder = BinderFactory.newBinder(CroppingPlanEntry.class);
        Binder<CroppingPlanSpecies, CroppingPlanSpecies> speciesBinder = BinderFactory.newBinder(CroppingPlanSpecies.class);

        Stream<TillageCurrentScaleToPreviousScaleTestData> cropDatas = effectiveCropArgs();

        cropDatas.filter(cropData -> cropData.index() >= plotData.fromIndex() && cropData.index() <= plotData.toIndex())
                .forEach(
                        cropData -> {
                            CroppingPlanEntry cpe = croppingPlanEntryDao.newInstance();
                            cropBinder.copyExcluding(cultureBle, cpe, TopiaEntity.PROPERTY_TOPIA_ID, CroppingPlanEntry.PROPERTY_CODE, CroppingPlanEntry.PROPERTY_CROPPING_PLAN_SPECIES);
                            cpe.setName("cropName_" + cropData.index());
                            cpe.setCode("cropCode_" + cropData.index());
                            croppingPlanEntryDao.create(cpe);

                            Collection<CroppingPlanSpecies> croppingPlanSpecies = CollectionUtils.emptyIfNull(cpe.getCroppingPlanSpecies());
                            for (CroppingPlanSpecies croppingPlanSpecy : croppingPlanSpecies) {
                                CroppingPlanSpecies cps = croppingPlanSpeciesDao.newInstance();
                                speciesBinder.copyExcluding(croppingPlanSpecy, cps, TopiaEntity.PROPERTY_TOPIA_ID, CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY);
                                cps.setCroppingPlanEntry(cpe);
                                cpe.addCroppingPlanSpecies(cps);
                                croppingPlanSpeciesDao.create(cps);
                            }
                            croppingPlanEntryDao.update(cpe);

                            EffectiveCropCycleNode node = effectiveCropCycleNodeDao.create(
                                    EffectiveCropCycleNode.PROPERTY_RANK, 0,
                                    EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY, cpe
                            );
                            effectiveSeasonalCropCycle.addNodes(node);
                            effectiveSeasonalCropCycleDao.update(effectiveSeasonalCropCycle);

                            Stream<EffectiveTillageTypeTestData> interventionDatas = effectiveInterventionArgs();
                            interventionDatas.forEach(
                                    interventionData -> {
                                        if (interventionData.index() >= cropData.fromIndex() && interventionData.index() <= cropData.toIndex()) {
                                            RefInterventionAgrosystTravailEDI mainAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
                                            final ToolsCoupling toolsCoupling = interventionData.toolsCoupling() != null ? toolsCouplingByName.get(interventionData.toolsCoupling()) : null;

                                            createEffectiveIntervention(
                                                    node,
                                                    null,
                                                    toolsCoupling,
                                                    mainAction,
                                                    "intervention_" + interventionData.index(),
                                                    LocalDate.of(2023, 9, 15),
                                                    LocalDate.of(2023, 9, 15),
                                                    interventionData.spatialFrequency(),
                                                    interventionData.transitCount()
                                            );

                                            createSemisEffectiveIntervention(
                                                    node,
                                                    null,
                                                    toolsCoupling,
                                                    "intervention_semis_" + interventionData.index(),
                                                    interventionData.dateSemis(),
                                                    interventionData.dateSemis(),
                                                    interventionData.spatialFrequency(),
                                                    interventionData.transitCount()
                                            );
                                        }
                                    }
                            );

                        }
                );

        effectiveSeasonalCropCycleDao.update(effectiveSeasonalCropCycle);

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTillageType indicatorTillageType = serviceFactory.newInstance(IndicatorTillageType.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorTillageType);
        final String content = out.toString();

        assertEffectiveSeasonalPlot(content, "Type de travail du sol", plotData.result());
    }

    @ParameterizedTest
    @MethodSource("effectiveCropArgs")
    public void testTillageTypeRealiseCropScale_12350(TillageCurrentScaleToPreviousScaleTestData data) throws IOException {
        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        List<Zone> zones = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        EffectiveCropCycleNode node = effectiveCropCycleNodeDao.create(
                EffectiveCropCycleNode.PROPERTY_RANK, 0,
                EffectiveCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle
        );
        List<EffectiveCropCycleNode> nodes = Lists.newArrayList(node);
        effectiveSeasonalCropCycleDao.create(
                EffectiveSeasonalCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectiveSeasonalCropCycle.PROPERTY_NODES, nodes);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create intervention
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));

        Stream<EffectiveTillageTypeTestData> effectiveTillageTypeTestDataStream = effectiveInterventionArgs();

        effectiveTillageTypeTestDataStream.forEach(
                interventionData -> {
                    if (interventionData.index() >= data.fromIndex() && interventionData.index() <= data.toIndex()) {
                        RefInterventionAgrosystTravailEDI mainAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
                        final ToolsCoupling toolsCoupling = interventionData.toolsCoupling() != null ? toolsCouplingByName.get(interventionData.toolsCoupling()) : null;

                        createEffectiveIntervention(
                                node,
                                null,
                                toolsCoupling,
                                mainAction,
                                "intervention_" + interventionData.index(),
                                LocalDate.of(2023, 9, 15),
                                LocalDate.of(2023, 9, 15),
                                interventionData.spatialFrequency(),
                                interventionData.transitCount()
                        );


                        createSemisEffectiveIntervention(
                                node,
                                null,
                                toolsCoupling,
                                "intervention_semis_" + interventionData.index(),
                                interventionData.dateSemis(),
                                interventionData.dateSemis(),
                                interventionData.spatialFrequency(),
                                interventionData.transitCount()
                        );
                    }
                }
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTillageType indicatorTillageType = serviceFactory.newInstance(IndicatorTillageType.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorTillageType);
        final String content = out.toString();

        assertEffectiveSeasonalCrop(content, "Type de travail du sol", data.result());
    }

    private static Stream<PracticedWeedingTestData> practicedArgs() {
        return Stream.of(
                new PracticedWeedingTestData(20, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),     // 1
                new PracticedWeedingTestData(21, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.66d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),     // 2
                new PracticedWeedingTestData(22, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.75d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),     // 3
                new PracticedWeedingTestData(23, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.33d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),     // 4
                new PracticedWeedingTestData(24, CULTIVATEUR, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),      // 5
                new PracticedWeedingTestData(25, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),     // 6
                new PracticedWeedingTestData(26, TRAVAIL_SOL, LABOUR, 0.66d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),              // 7
                new PracticedWeedingTestData(27, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.5d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),      // 8
                new PracticedWeedingTestData(28, TRAVAIL_SOL, LABOUR, 0.25d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),              // 9
                new PracticedWeedingTestData(29, TRAVAIL_SOL, LABOUR, 0.75d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),              // 10
                new PracticedWeedingTestData(30, BINEUSE, TRAVAIL_DE_SURFACE, 0.25d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)),// 11 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(31, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.66d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 1)),     // 12
                new PracticedWeedingTestData(32, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.25d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),    // 13
                new PracticedWeedingTestData(33, BINEUSE, TRAVAIL_DE_SURFACE, 0.5d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),         // 14
                new PracticedWeedingTestData(34, BINEUSE, TRAVAIL_DE_SURFACE, 0.66d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)),// 15 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(35, TRAVAIL_SOL, LABOUR, 1.0d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),               // 16
                new PracticedWeedingTestData(36, TRAVAIL_SOL, LABOUR, 0.33d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 1)),              // 17
                new PracticedWeedingTestData(37, BINEUSE, TRAVAIL_DE_SURFACE, 0.75d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)),         // 18
                new PracticedWeedingTestData(38, BINEUSE, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),     // 20
                new PracticedWeedingTestData(39, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.75d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),    // 19
                new PracticedWeedingTestData(40, CULTIVATEUR, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)), //  (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(41, TRAVAIL_SOL, LABOUR, 1.0d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),              // 21
                new PracticedWeedingTestData(42, TRAVAIL_SOL, LABOUR, 0.45d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 22
                new PracticedWeedingTestData(43, BINEUSE, TRAVAIL_DE_SURFACE, 0.33d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),        // 23
                new PracticedWeedingTestData(44, BINEUSE, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)), // 24 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(45, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.33d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),    // 25
                new PracticedWeedingTestData(46, CULTIVATEUR, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),     // 26
                new PracticedWeedingTestData(47, BINEUSE, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)),// 27 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(48, BINEUSE, TRAVAIL_DE_SURFACE, 0.5d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)), // 28 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(49, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),    // 29
                new PracticedWeedingTestData(50, BINEUSE, TRAVAIL_DE_SURFACE, 0.25d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)),// 30 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(51, BINEUSE, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)), // 31 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(52, TRAVAIL_SOL, LABOUR, 0.66d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 32
                new PracticedWeedingTestData(53, TRAVAIL_SOL, LABOUR, 0.25d, 1.0d, TillageType.LABOUR.name(), "100", LocalDate.of(2023, 9, 30)),             // 33
                new PracticedWeedingTestData(54, BINEUSE, TRAVAIL_DE_SURFACE, 0.75d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)),// 34 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(55, BINEUSE, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, null, "100", LocalDate.of(2023, 9, 1)), // 35 (pas de sortie, car à cette échelle l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS))
                new PracticedWeedingTestData(56, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.33d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30)),    // 36
                new PracticedWeedingTestData(57, CULTIVATEUR, TRAVAIL_DE_SURFACE, 1.0d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 9, 30))     // 37

        );
    }

    private static Stream<TillageCurrentScaleToPreviousScaleTestData> practicedCropArgs() {
        return Stream.of(
                new TillageCurrentScaleToPreviousScaleTestData(0, 20, 21, TillageType.TCS.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 22, 23, TillageType.TCS.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 24, 25, TillageType.TCS.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 26, 27, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 28, 32, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 33, 34, TillageType.TCS.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 35, 40, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 41, 46, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 47, 49, TillageType.TCS.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 50, 51, TillageType.SEMIS_DIRECT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 52, 53, TillageType.LABOUR.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 54, 55, TillageType.SEMIS_DIRECT.name()),
                new TillageCurrentScaleToPreviousScaleTestData(0, 56, 57, TillageType.TCS.name())
        );
    }

    @ParameterizedTest
    @MethodSource("practicedArgs")
    public void testTillageTypePracticedInterventionScale_12350(PracticedWeedingTestData interventionData) throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create all intervention
        RefInterventionAgrosystTravailEDI TravailDeSurface = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));
        final ToolsCoupling toolsCoupling = toolsCouplingByName.get(interventionData.toolsCoupling());

        createPracticedIntervention(
                bleConnection,
                bleTendreHiver,
                toolsCoupling,
                TravailDeSurface,
                "intervention_" + interventionData.index(),
                "15/09",
                "15/09",
                interventionData.spatialFrequency(),
                interventionData.temporalFrequency()
        );
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM");
        createSemisPracticedIntervention(
                bleConnection,
                toolsCoupling,
                "intervention_semis_" + interventionData.index(),
                interventionData.dateSemis().format(formatter),
                interventionData.dateSemis().format(formatter),
                interventionData.spatialFrequency(),
                interventionData.temporalFrequency()
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTillageType tillageType = serviceFactory.newInstance(IndicatorTillageType.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, tillageType);
        final String content = out.toString();

        assertPracticedIntervention(content, "intervention_" + interventionData.index(), "Type de travail du sol", interventionData.result(), interventionData.completion());
    }

    private static Stream<PracticedTillageTestData> practiced2Args() {
        return Stream.of(
                new PracticedTillageTestData(20, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 1, 15), LocalDate.of(2023, 1, 10), LocalDate.of(2023, 1, 1), LocalDate.of(2023, 1, 1)),
                new PracticedTillageTestData(20, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, TillageType.TCS.name(), "100", LocalDate.of(2023, 12, 31), LocalDate.of(2023, 1, 10), LocalDate.of(2023, 1, 1), LocalDate.of(2023, 1, 1))
        );}
    @ParameterizedTest
    @MethodSource("practiced2Args")
    public void testTillageTypePracticedInterventionScale_withCrossYearIntervention_2_12350(PracticedTillageTestData interventionData) throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create all intervention
        RefInterventionAgrosystTravailEDI TravailDeSurface = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));
        final ToolsCoupling toolsCoupling = toolsCouplingByName.get(interventionData.toolsCoupling());

        String name = "intervention_" + interventionData.index();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM");
        PracticedIntervention travailDuSolIntervention = createPracticedIntervention(
                bleConnection,
                bleTendreHiver,
                toolsCoupling,
                TravailDeSurface,
                name,
                interventionData.tillageStartDate().format(formatter),
                interventionData.tillageEndingDate().format(formatter),
                interventionData.spatialFrequency(),
                interventionData.temporalFrequency()
        );

        createSemisPracticedIntervention(
                bleConnection,
                toolsCoupling,
                "intervention_semis_" + interventionData.index(),
                interventionData.seedingStatingDate().format(formatter),
                interventionData.seedingEndingDate().format(formatter),
                interventionData.spatialFrequency(),
                interventionData.temporalFrequency()
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTillageType tillageType = serviceFactory.newInstance(IndicatorTillageType.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, tillageType);
        final String content = out.toString();

        String result = interventionData.result();
        String indicatorName = "Type de travail du sol";

        if (result != null) {
            assertThat(content).usingComparator(comparator).isEqualTo(
                    "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Travail du sol;" + travailDuSolIntervention.getName() + ";" + travailDuSolIntervention.getName() +
                            ";"+ travailDuSolIntervention.getStartingPeriodDate() + ";"+ travailDuSolIntervention.getEndingPeriodDate() + ";Travail du sol;;0;;;non;;2012, 2013;Synthétisé;Stratégie agronomique;" + indicatorName + ";" + result + ";" +  interventionData.completion() + ";;;"
            );
        } else {
            assertThat(content.contains("interventionSheet;")).isFalse();
        }
    }

    private static Stream<PracticedTillageDiffDayTestData> practiced3Args() {
        return Stream.of(
                new PracticedTillageDiffDayTestData(20, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, 195.5,  LocalDate.of(2023, 1, 15), LocalDate.of(2023, 1, 10), LocalDate.of(2023, 1, 1), LocalDate.of(2023, 1, 1)),
                new PracticedTillageDiffDayTestData(20, CULTIVATEUR, TRAVAIL_DE_SURFACE, 0.45d, 1.0d, 7.5, LocalDate.of(2023, 12, 31), LocalDate.of(2023, 1, 15), LocalDate.of(2023, 1, 1), LocalDate.of(2023, 1, 1))
        );}

    @ParameterizedTest
    @MethodSource("practiced3Args")
    public void testPracticedComputeAverageDayOfYear_12461(PracticedTillageDiffDayTestData interventionData) throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        // create all intervention
        RefInterventionAgrosystTravailEDI TravailDeSurface = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));
        final ToolsCoupling toolsCoupling = toolsCouplingByName.get(interventionData.toolsCoupling());

        String name = "intervention_" + interventionData.index();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM");
        PracticedIntervention travailDuSolIntervention = createPracticedIntervention(
                bleConnection,
                bleTendreHiver,
                toolsCoupling,
                TravailDeSurface,
                name,
                interventionData.tillageStartDate().format(formatter),
                interventionData.tillageEndingDate().format(formatter),
                interventionData.spatialFrequency(),
                interventionData.temporalFrequency()
        );

        TestInticatorComputeAverageDayOfYear testInticatorComputeAverageDayOfYear = new TestInticatorComputeAverageDayOfYear();
        Double averageDayOfYear = testInticatorComputeAverageDayOfYear.computeAverageDayOfYear(travailDuSolIntervention);

        Assertions.assertEquals(interventionData.result(), averageDayOfYear);
    }

    protected class TestInticatorComputeAverageDayOfYear extends Indicator {

        @Override
        public Double computeAverageDayOfYear(PracticedIntervention practicedIntervention) {
            return super.computeAverageDayOfYear(practicedIntervention);
        }

        @Override
        protected boolean isRelevant(ExportLevel atLevel) {
            return false;
        }

        @Override
        public String getIndicatorCategory() {
            return "";
        }

        @Override
        public String getIndicatorLabel(int i) {
            return "";
        }

        @Override
        protected boolean isDisplayed(ExportLevel atLevel, int i) {
            return false;
        }

        @Override
        public void computePracticed(IndicatorWriter writer, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedDomainExecutionContext domainContext) {

        }

        @Override
        public void computePracticed(IndicatorWriter writer, Domain domain) {

        }

        @Override
        public void resetPracticed(Domain domain) {

        }

        @Override
        public void computeEffective(IndicatorWriter writer, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext) {

        }

        @Override
        public void computeEffectiveCC(IndicatorWriter writer, Domain domain, PerformanceGrowingSystemExecutionContext growingSystemContext, Plot plot) {

        }

        @Override
        public void resetEffectiveCC() {

        }

        @Override
        public void computeEffective(IndicatorWriter writer, PerformanceEffectiveDomainExecutionContext domainContext, Optional<GrowingSystem> optionalGrowingSystem, PerformancePlotExecutionContext plotContext) {

        }

        @Override
        public void resetEffectiveZones() {

        }

        @Override
        public void computeEffective(IndicatorWriter writer, PerformanceEffectiveDomainExecutionContext domainExecutionContext, PerformanceGrowingSystemExecutionContext growingSystemContext) {

        }

        @Override
        public void resetEffectivePlots() {

        }

        @Override
        public void computeEffective(IndicatorWriter writer, Domain domain) {

        }

        @Override
        public void resetEffectiveGrowingSystems() {

        }
    }


    @ParameterizedTest
    @MethodSource("practicedCropArgs")
    public void testTillageTypePracticedCropScape_12350(TillageCurrentScaleToPreviousScaleTestData data) throws IOException {

        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        testDatas.createEquipmentsAndToolsCouplingForDomain(baulon);

        try (InputStream stream = TestDatas.class.getResourceAsStream("/referentiels/test/refCorrespondanceMaterielOutilsTS.csv")) {
            importService.importRefCorrespondanceMaterielOutilsTS(stream);
        }

        // create all intervention
        Stream<PracticedWeedingTestData> practicedTillageTypeTestDataStream = practicedArgs();

        practicedTillageTypeTestDataStream.forEach(
                interventionData -> {
                    if (interventionData.index() >= data.fromIndex() && interventionData.index() <= data.toIndex()) {

                        RefInterventionAgrosystTravailEDI mainAction = refInterventionAgrosystTravailEDIDao.forReference_codeEquals(interventionData.mainAction()).findAny();
                        List<ToolsCoupling> toolsCouplings = toolsCouplingDao.forDomainEquals(baulon).findAll();
                        Map<String, ToolsCoupling> toolsCouplingByName = toolsCouplings.stream().collect(Collectors.toMap(ToolsCoupling::getToolsCouplingName, Function.identity()));
                        final ToolsCoupling toolsCoupling = toolsCouplingByName.get(interventionData.toolsCoupling());

                        createPracticedIntervention(
                                bleConnection,
                                bleTendreHiver,
                                toolsCoupling,
                                mainAction,
                                "intervention_" + interventionData.index(),
                                "15/09",
                                "15/09",
                                interventionData.spatialFrequency(),
                                interventionData.temporalFrequency()
                        );
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM");
                        createSemisPracticedIntervention(
                                bleConnection,
                                toolsCoupling,
                                "intervention_semis_" + interventionData.index(),
                                interventionData.dateSemis().format(formatter),
                                interventionData.dateSemis().format(formatter),
                                interventionData.spatialFrequency(),
                                interventionData.temporalFrequency()
                        );
                    }
                }
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        IndicatorTillageType indicatorTillageType = serviceFactory.newInstance(IndicatorTillageType.class);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorTillageType);
        final String content = out.toString();

        assertPracticedSeasonalCrop(content, "Type de travail du sol", data.result());
    }

    private Calendar getCalendarFromInterventionDateString(final String interventionDate) {
        if (!interventionDate.matches("\\d{1,2}/\\d{1,2}")) {
            throw new IllegalArgumentException("L'argument '%s' ne correspond pas au format de date attendu dd/MM".formatted(interventionDate));
        }
        final String[] dayAndMonth = interventionDate.split("/");

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayAndMonth[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(dayAndMonth[1]) - 1);

        return calendar;
    }

}