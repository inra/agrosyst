package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.ZoneTopiaDao;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.MineralFertilization;
import fr.inra.agrosyst.api.entities.performance.OrganicFertilization;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNodeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStadeTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.PerformanceServiceImpl;
import fr.inra.agrosyst.services.performance.PerformanceServiceImplForTest;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AbstractIndicatorFertilizationTest extends AbstractAgrosystTest {
    protected DomainService domainService;
    protected PerformanceServiceImplForTest performanceService;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected DomainInputStockUnitService domainInputStockUnitService;

    protected DomainTopiaDao domainDao;
    protected ZoneTopiaDao zoneTopiaDao;

    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCycleTopiaDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDAO;
    protected EffectiveCropCyclePhaseTopiaDao effectiveCropCyclePhaseDao;

    protected PracticedCropCyclePhaseTopiaDao practicedCropCyclePhasesDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected PracticedSpeciesStadeTopiaDao practicedSpeciesStadeTopiaDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedCropCycleNodeTopiaDao practicedCropCycleNodeDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionTopiaDao;
    protected EffectiveSpeciesStadeTopiaDao effectiveSpeciesStadeDao;
    protected MineralProductInputUsageTopiaDao mineralProductInputUsageTopiaDao;
    protected DomainMineralProductInputTopiaDao mineralProductInputTopiaDao;

    protected IrrigationActionTopiaDao irrigationActionTopiaDao;
    protected MineralFertilizersSpreadingActionTopiaDao mineralFertilizersSpreadingActionDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDIDAO;
    protected OrganicProductInputUsageTopiaDao organicProductInputUsageDao;
    protected OrganicFertilizersSpreadingActionTopiaDao organicFertilizersSpreadingActionDao;
    protected RefFertiOrgaTopiaDao refFertiOrgaTopiaDao;

    protected Domain baulon;
    protected RefCountry france;
    protected Collection<IndicatorFilter> indicatorFilters;
    protected DomainOrganicProductInputTopiaDao domainOrganicProductInputDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        // use impl to allow protected method call (not exposed on interface)
        PerformanceServiceImpl performanceService = serviceFactory.newExpectedService(PerformanceService.class, PerformanceServiceImpl.class);
        this.performanceService = new PerformanceServiceImplForTest(performanceService);
        testDatas = serviceFactory.newInstance(TestDatas.class);

        effectiveCropCycleService = serviceFactory.newService(EffectiveCropCycleService.class);
        domainService = serviceFactory.newService(DomainService.class);
        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);

        // init dao
        domainDao = serviceContext.getPersistenceContext().getDomainDao();
        zoneTopiaDao = serviceContext.getPersistenceContext().getZoneDao();
        practicedCropCyclePhasesDao = serviceContext.getPersistenceContext().getPracticedCropCyclePhaseDao();
        effectivePerennialCropCycleTopiaDao = serviceContext.getPersistenceContext().getEffectivePerennialCropCycleDao();
        practicedSeasonalCropCycleDao = serviceContext.getPersistenceContext().getPracticedSeasonalCropCycleDao();
        effectiveInterventionDAO = serviceContext.getPersistenceContext().getEffectiveInterventionDao();
        practicedInterventionDao = serviceContext.getPersistenceContext().getPracticedInterventionDao();
        practicedSpeciesStadeTopiaDao = serviceContext.getPersistenceContext().getPracticedSpeciesStadeDao();
        refActionAgrosystTravailEDIDAO = serviceContext.getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        practicedCropCycleNodeDao = serviceContext.getPersistenceContext().getPracticedCropCycleNodeDao();
        practicedCropCycleConnectionTopiaDao = serviceContext.getPersistenceContext().getPracticedCropCycleConnectionDao();
        effectiveCropCyclePhaseDao = serviceContext.getPersistenceContext().getEffectiveCropCyclePhaseDao();
        effectiveSpeciesStadeDao = serviceContext.getPersistenceContext().getEffectiveSpeciesStadeDao();
        mineralProductInputUsageTopiaDao = serviceContext.getPersistenceContext().getMineralProductInputUsageDao();
        mineralProductInputTopiaDao = serviceContext.getPersistenceContext().getDomainMineralProductInputDao();
        organicProductInputUsageDao = serviceContext.getPersistenceContext().getOrganicProductInputUsageDao();
        domainOrganicProductInputDao = serviceContext.getPersistenceContext().getDomainOrganicProductInputDao();
        organicFertilizersSpreadingActionDao = serviceContext.getPersistenceContext().getOrganicFertilizersSpreadingActionDao();
        refFertiOrgaTopiaDao = serviceContext.getPersistenceContext().getRefFertiOrgaDao();
        irrigationActionTopiaDao = serviceContext.getPersistenceContext().getIrrigationActionDao();
        mineralFertilizersSpreadingActionDao = serviceContext.getPersistenceContext().getMineralFertilizersSpreadingActionDao();

        testDatas.createTestDomains();

        // login
        loginAsAdmin();
        alterSchema();

        indicatorFilters = performanceService.getAllIndicatorFilters();
        indicatorFilters.stream()
                .filter(indicatorFilter -> indicatorFilter.getClazz().equals(IndicatorOrganicFertilization.class.getSimpleName()))
                .findFirst()
                .ifPresent(indicatorFilter -> indicatorFilter.setOrganicFertilizations(Arrays.stream(OrganicFertilization.values()).toList()));
        indicatorFilters.stream()
                .filter(indicatorFilter -> indicatorFilter.getClazz().equals(IndicatorMineralFertilization.class.getSimpleName()))
                .findFirst()
                .ifPresent(indicatorFilter -> indicatorFilter.setMineralFertilizations(Arrays.stream(MineralFertilization.values()).toList()));

        // initialisation des paramètres de test
        this.baulon = domainDao.forTopiaIdEquals("fr.inra.agrosyst.api.entities.Domain_bfcffdf9-a2aa-4fd7-a79f-4adee2aa8d3b").findUnique();
        this.france = baulon.getLocation().getRefCountry();

        testDatas.createTestPlots();
        testDatas.importRefCompositionSubstancesActivesParNumeroAMM();
        testDatas.importRefSubstancesActivesCommissionEuropeenne();
        testDatas.importPhrasesRisqueEtClassesMentionDangerParAmm();
        testDatas.importRefConversionUnitesQSA();
        testDatas.importActaTraitementsProduits();
        testDatas.importActaTraitementsProduitsCateg();
        testDatas.importActaGroupeCultures();
        testDatas.importRefFertiOrga();
    }

    protected OrganicInterventionInfo createOrganicFertilizerSpreadingEffectiveIntervention(RefFertiOrga refFertiOrga, int transitCount, double spatialFrequency, double qtAvg, DomainOrganicProductInput domainOrganicProductInput) {
        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        List<Zone> zones = zoneTopiaDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        final String nomIntervention = "Intervention (%s, %s)".formatted(refFertiOrga.getLibelle(), UUID.randomUUID());
        EffectiveIntervention intervention = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, nomIntervention,
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, transitCount,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency);
        EffectiveSpeciesStade interventionStades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention.addSpeciesStades(interventionStades);

        final OrganicProductInputUsage organicProductInputUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, domainOrganicProductInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, qtAvg
        );

        RefInterventionAgrosystTravailEDI actionSEK = this.refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEK").findAny();
        final OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction = organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEK,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicProductInputUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        return new OrganicInterventionInfo(nomIntervention, intervention.getTopiaId(), organicProductInputUsage, organicFertilizersSpreadingAction, cultureBle);
    }

    protected OrganicInterventionInfo createOrganicFertilizerSpreadingPracticedIntervention(double temporalFrequency, double spatialFrequency, double qtAvg, DomainOrganicProductInput domainOrganicProductInput) throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        PracticedCropCyclePhase phasePleineProduction = practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        final String nomIntervention = "Intervention (%s, %s)".formatted(domainOrganicProductInput.getInputName(), UUID.randomUUID());
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_NAME, nomIntervention,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        final OrganicProductInputUsage organicProductInputUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, domainOrganicProductInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, qtAvg
        );

        RefInterventionAgrosystTravailEDI actionSEK = this.refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEK").findAny();
        final OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction = organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEK,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicProductInputUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        return new OrganicInterventionInfo(nomIntervention, intervention.getTopiaId(), organicProductInputUsage, organicFertilizersSpreadingAction, cultureBle);
    }

    protected void createOrganicFertilizerSpreadingPracticedInterventionTotal(
            double temporalFrequency,
            double spatialFrequency,
            double qtAvg,
            DomainOrganicProductInput domainOrganicProductInput,
            PracticedCropCycleConnection bleConnection,
            CroppingPlanSpecies bleTendreHiver) {
        final String nomIntervention = "Intervention (%s, %s)".formatted(domainOrganicProductInput.getInputName(), UUID.randomUUID());
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_NAME, nomIntervention,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        final OrganicProductInputUsage organicProductInputUsage = organicProductInputUsageDao.create(
                OrganicProductInputUsage.PROPERTY_INPUT_TYPE, InputType.EPANDAGES_ORGANIQUES,
                OrganicProductInputUsage.PROPERTY_DOMAIN_ORGANIC_PRODUCT_INPUT, domainOrganicProductInput,
                OrganicProductInputUsage.PROPERTY_QT_AVG, qtAvg
        );

        RefInterventionAgrosystTravailEDI actionSEK = this.refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEK").findAny();
        final OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction = organicFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEK,
                OrganicFertilizersSpreadingAction.PROPERTY_ORGANIC_PRODUCT_INPUT_USAGES, List.of(organicProductInputUsage),
                OrganicFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );
    }

    protected String createIrrigationEffectiveIntervention(double waterAverageQuantity, int azoteQuantity, double spatialFrequency, int transitCount) {
        List<Zone> zones = zoneTopiaDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        final String nomIntervention = "Intervention irrigation (%f, %d, %f, %d)".formatted(waterAverageQuantity, azoteQuantity, spatialFrequency, transitCount);
        EffectiveIntervention intervention = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, nomIntervention,
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.IRRIGATION,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, transitCount,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency);

        RefInterventionAgrosystTravailEDI actionSEO = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEO").findAny();
        irrigationActionTopiaDao.create(
                IrrigationAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                IrrigationAction.PROPERTY_MAIN_ACTION, actionSEO,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, waterAverageQuantity,
                IrrigationAction.PROPERTY_AZOTE_QUANTITY, azoteQuantity
        );

        return nomIntervention;
    }

    protected String createIrrigationPracticedIntervention(double waterAverageQuantity, int azoteQuantity, double spatialFrequency, double temporalFrequency) throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        final Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        PracticedCropCyclePhase phasePleineProduction = practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        final String nomIntervention = "Intervention irrigation (%f, %d, %f, %f)".formatted(waterAverageQuantity, azoteQuantity, spatialFrequency, temporalFrequency);
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_NAME, nomIntervention,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.IRRIGATION,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        RefInterventionAgrosystTravailEDI actionSEO = refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEO").findAny();
        irrigationActionTopiaDao.create(
                IrrigationAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                IrrigationAction.PROPERTY_MAIN_ACTION, actionSEO,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, waterAverageQuantity,
                IrrigationAction.PROPERTY_AZOTE_QUANTITY, azoteQuantity
        );

        return nomIntervention;
    }

    protected MineralInterventionInfo createMineralSpreadingEffectiveIntervention(RefFertiMinUNIFA mineralProduct,
                                                                                  DomainMineralProductInput domainMineralProductInput,
                                                                                  double qtAvg,
                                                                                  double spatialFrequency,
                                                                                  int transitCount) {
        // Déclaration d'une zone où du blé est cultivé. On appliquera les traitements sur cette culture.
        List<Zone> zones = zoneTopiaDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findAll();
        Zone zpPlotBaulon1 = zones.getFirst();

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION);

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        final String nomIntervention = "Intervention (%s, %s)".formatted(mineralProduct.getType_produit(), UUID.randomUUID());
        EffectiveIntervention intervention = effectiveInterventionDAO.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, nomIntervention,
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(baulon.getCampaign(), 3, 12),
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, transitCount,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency);
        EffectiveSpeciesStade interventionStades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention.addSpeciesStades(interventionStades);

        final MineralProductInputUsage mineralProductInputUsage = mineralProductInputUsageTopiaDao.create(
                MineralProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, domainMineralProductInput,
                MineralProductInputUsage.PROPERTY_QT_AVG, qtAvg
        );

        RefInterventionAgrosystTravailEDI actionSEM = this.refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEM").findAny();
        final MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = mineralFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEM,
                MineralFertilizersSpreadingAction.PROPERTY_MINERAL_PRODUCT_INPUT_USAGES, List.of(mineralProductInputUsage),
                MineralFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        return new MineralInterventionInfo(nomIntervention, intervention.getTopiaId(), mineralProductInputUsage, mineralFertilizersSpreadingAction, cultureBle);
    }

    protected MineralInterventionInfo createMineralSpreadingPracticedIntervention(RefFertiMinUNIFA mineralProduct,
                                                                                  DomainMineralProductInput domainMineralProductInput,
                                                                                  double qtAvg,
                                                                                  double spatialFrequency,
                                                                                  double temporalFrequency) throws IOException {
        PracticedSystem ps = testDatas.createINRATestPraticedSystems(null, "Systeme de culture Baulon 1");
        Domain domain = ps.getGrowingSystem().getGrowingPlan().getDomain();
        int domainCampaign = domain.getCampaign();
        Set<Integer> campaignSet = Sets.newHashSet();
        campaignSet.add(domainCampaign - 1);
        campaignSet.add(domainCampaign);

        String psCampaings = CommonService.ARRANGE_CAMPAIGNS_SET.apply(campaignSet);
        PracticedSystemTopiaDao psdao = getPersistenceContext().getPracticedSystemDao();

        ps.setCampaigns(psCampaings);
        ps = psdao.update(ps);

        final List<CroppingPlanEntry> croppingPlanEntries = domainService.getCroppingPlanEntriesForDomain(baulon);
        Map<String, CroppingPlanEntry> cropByNames = croppingPlanEntries.stream().collect(Collectors.toMap(CroppingPlanEntry::getName, Function.identity()));
        List<CroppingPlanEntry> crops = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);
        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());
        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSeasonalCropCycleDao.create(
                PracticedSeasonalCropCycle.PROPERTY_PRACTICED_SYSTEM, ps
        );

        final CroppingPlanEntry ble = cropByNames.get("Blé");
        PracticedCropCycleNode nodeBle = practicedCropCycleNodeDao.create(
                PracticedCropCycleNode.PROPERTY_PRACTICED_SEASONAL_CROP_CYCLE, practicedSeasonalCropCycle,
                PracticedCropCycleNode.PROPERTY_RANK, 1,
                PracticedCropCycleNode.PROPERTY_Y, 0,
                PracticedCropCycleNode.PROPERTY_CROPPING_PLAN_ENTRY_CODE, ble.getCode());
        practicedSeasonalCropCycle.addCropCycleNodes(nodeBle);
        PracticedCropCyclePhase phasePleineProduction = practicedCropCyclePhasesDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        final PracticedCropCycleConnection bleConnection = practicedCropCycleConnectionTopiaDao.create(
                PracticedCropCycleConnection.PROPERTY_SOURCE, nodeBle,
                PracticedCropCycleConnection.PROPERTY_TARGET, nodeBle,
                PracticedCropCycleConnection.PROPERTY_CROPPING_PLAN_ENTRY_FREQUENCY, 100);

        final String nomIntervention = "Intervention (%s, %s)".formatted(mineralProduct.getType_produit(), UUID.randomUUID());
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_NAME, nomIntervention,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        final MineralProductInputUsage mineralProductInputUsage = mineralProductInputUsageTopiaDao.create(
                MineralProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, domainMineralProductInput,
                MineralProductInputUsage.PROPERTY_QT_AVG, qtAvg
        );

        RefInterventionAgrosystTravailEDI actionSEM = this.refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEM").findAny();
        final MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = mineralFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEM,
                MineralFertilizersSpreadingAction.PROPERTY_MINERAL_PRODUCT_INPUT_USAGES, List.of(mineralProductInputUsage),
                MineralFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        return new MineralInterventionInfo(nomIntervention, intervention.getTopiaId(), mineralProductInputUsage, mineralFertilizersSpreadingAction, cultureBle);
    }

    protected void createMineralSpreadingPracticedInterventionTotal(RefFertiMinUNIFA mineralProduct,
                                                                    DomainMineralProductInput domainMineralProductInput,
                                                                    double qtAvg,
                                                                    double spatialFrequency,
                                                                    double temporalFrequency,
                                                                    PracticedCropCycleConnection bleConnection,
                                                                    CroppingPlanSpecies bleTendreHiver) throws IOException {
        final String nomIntervention = "Intervention (%s, %s)".formatted(mineralProduct.getType_produit(), UUID.randomUUID());
        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, bleConnection,
                PracticedIntervention.PROPERTY_NAME, nomIntervention,
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, spatialFrequency,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, temporalFrequency);
        final PracticedSpeciesStade speciesStadeBle = practicedSpeciesStadeTopiaDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(speciesStadeBle);

        final MineralProductInputUsage mineralProductInputUsage = mineralProductInputUsageTopiaDao.create(
                MineralProductInputUsage.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                MineralProductInputUsage.PROPERTY_DOMAIN_MINERAL_PRODUCT_INPUT, domainMineralProductInput,
                MineralProductInputUsage.PROPERTY_QT_AVG, qtAvg
        );

        RefInterventionAgrosystTravailEDI actionSEM = this.refActionAgrosystTravailEDIDAO.forReference_codeEquals("SEM").findAny();
        mineralFertilizersSpreadingActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionSEM,
                MineralFertilizersSpreadingAction.PROPERTY_MINERAL_PRODUCT_INPUT_USAGES, List.of(mineralProductInputUsage),
                MineralFertilizersSpreadingAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );
    }

    public List<DonneesTestFertilisationMineRealise> getDonneesTestFertilisationMineraleRealise() {
        List<DonneesTestFertilisationMineRealise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorN = "Quantité de N minéral apportée (en kg/ha)";
        final String indicatorP2O5 = "Quantité de P2O5 minéral apportée (en kg/ha)";
        final String indicatorK2O = "Quantité de K2O minéral apportée (en kg/ha)";
        final String indicatorB = "Quantité de B minéral apportée (en kg/ha)";
        final String indicatorCa = "Quantité de Ca minéral apportée (en kg/ha)";
        final String indicatorFe = "Quantité de Fe minéral apportée (en kg/ha)";
        final String indicatorMn = "Quantité de Mn minéral apportée (en kg/ha)";
        final String indicatorMo = "Quantité de Mo minéral apportée (en kg/ha)";
        final String indicatorNa2O = "Quantité de Na2O minéral apportée (en kg/ha)";
        final String indicatorSO3 = "Quantité de SO3 minéral apportée (en kg/ha)";
        final String indicatorCu = "Quantité de Cu minéral apportée (en kg/ha)";
        final String indicatorZn = "Quantité de Zn minéral apportée (en kg/ha)";
        final String indicatorMgO = "Quantité de MgO minéral apportée (en kg/ha)";

        {
            var pair = createSulfonitrate();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 1500.0, 1.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "300.0"),
                            Map.entry(indicatorP2O5, "0.0"),
                            Map.entry(indicatorK2O, "0.0"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "75.0"),
                            Map.entry(indicatorSO3, "360.0"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        {
            RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 109,
                    RefFertiMinUNIFA.PROPERTY_N, 20.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 11.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMMONITRATE Dosage < et = 28 % sans SO3"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "1112022000",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(mineralProduct, domainMineralProductInput, 15.0, 1.5, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "4.5"),
                            Map.entry(indicatorP2O5, "0.0"),
                            Map.entry(indicatorK2O, "0.0"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "0.0"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "2.475")
                    )
            ));
        }

        {
            RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 180,
                    RefFertiMinUNIFA.PROPERTY_N, 16.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 28.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "NITRATE DE CHAUX"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "1800071550",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(mineralProduct, domainMineralProductInput, 15.0, 1.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "2.4"),
                            Map.entry(indicatorP2O5, "0.0"),
                            Map.entry(indicatorK2O, "0.0"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "4.2"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "0.0"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        {
            RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Pulvérulent",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 211,
                    RefFertiMinUNIFA.PROPERTY_N, 0.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 19.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 29.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "SUPER NORMAL"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "2010101900",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(mineralProduct, domainMineralProductInput, 15.0, 3.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "0.0"),
                            Map.entry(indicatorP2O5, "8.55"),
                            Map.entry(indicatorK2O, "0.0"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "13.05"),
                            Map.entry(indicatorSO3, "0.0"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        {
            RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 110,
                    RefFertiMinUNIFA.PROPERTY_N, 33.5,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 60.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMMONITRATE Dosage > 28 % sans SO3"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "3110016000",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(mineralProduct, domainMineralProductInput, 15.0, 2.5, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "12.563"),
                            Map.entry(indicatorP2O5, "0.0"),
                            Map.entry(indicatorK2O, "22.5"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "0.0"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        {
            var pair = createAmendementsEngraisAutres();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 15.0, 1.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "0.0"),
                            Map.entry(indicatorP2O5, "2.1"),
                            Map.entry(indicatorK2O, "0.0"),
                            Map.entry(indicatorB, "0.45"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "3.15"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        {
            RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 531,
                    RefFertiMinUNIFA.PROPERTY_N, 6.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 3.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 3.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 10.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "TERNAIRES < OU = 10% N"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "60303009",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(mineralProduct, domainMineralProductInput, 15.0, 0.5, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "0.45"),
                            Map.entry(indicatorP2O5, "0.225"),
                            Map.entry(indicatorK2O, "0.225"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.75"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "0.0"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        {
            var pair = createSoufreMagnesie();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 15.0, 5.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "0.0"),
                            Map.entry(indicatorP2O5, "0.0"),
                            Map.entry(indicatorK2O, "0.0"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "3.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "25.5"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.75"),
                            Map.entry(indicatorMgO, "9.75")
                    )
            ));
        }

        {
            RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Compacté",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 443,
                    RefFertiMinUNIFA.PROPERTY_N, 0.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 10.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 20.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 5.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AUTRES AMENDEMENTS ENGRAIS POTASSIQUES"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "1020101",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(mineralProduct, domainMineralProductInput, 15.0, 2.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "0.0"),
                            Map.entry(indicatorP2O5, "3.0"),
                            Map.entry(indicatorK2O, "6.0"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "1.5"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "0.0"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "0.0"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        {
            var pair = createMelangeGranulePK();

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(pair.getLeft(), pair.getRight(), 15.0, 2.0, 1,
                    Map.ofEntries(
                            Map.entry(indicatorN, "0.0"),
                            Map.entry(indicatorP2O5, "6.9"),
                            Map.entry(indicatorK2O, "3.3"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "3.6"),
                            Map.entry(indicatorCu, "1.8"),
                            Map.entry(indicatorZn, "0.9"),
                            Map.entry(indicatorMgO, "1.5")
                    )
            ));
        }

        {
            RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 111,
                    RefFertiMinUNIFA.PROPERTY_N, 13.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 30.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 16.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 10.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMMONITRATE Dosage < et = 28 % avec SO3"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "1134033000",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            donneesTestFertilisation.add(new DonneesTestFertilisationMineRealise(mineralProduct, domainMineralProductInput, 15.0, 3.0, 1,
                    Map.ofEntries(Map.entry(indicatorN, "5.85"),
                            Map.entry(indicatorP2O5, "13.5"),
                            Map.entry(indicatorK2O, "0.0"),
                            Map.entry(indicatorB, "0.0"),
                            Map.entry(indicatorCa, "0.0"),
                            Map.entry(indicatorFe, "0.0"),
                            Map.entry(indicatorMn, "0.0"),
                            Map.entry(indicatorMo, "0.0"),
                            Map.entry(indicatorNa2O, "0.0"),
                            Map.entry(indicatorSO3, "7.2"),
                            Map.entry(indicatorCu, "0.0"),
                            Map.entry(indicatorZn, "4.5"),
                            Map.entry(indicatorMgO, "0.0")
                    )
            ));
        }

        return donneesTestFertilisation;
    }

    public List<DonneesTestFertilisationMineSynthetise> getDonneesTestFertilisationMineraleSynthetise() {
        List<DonneesTestFertilisationMineSynthetise> donneesTestFertilisation = new LinkedList<>();
        final String indicatorN = "Quantité de N minéral apportée (en kg/ha)";
        final String indicatorP2O5 = "Quantité de P2O5 minéral apportée (en kg/ha)";
        final String indicatorK2O = "Quantité de K2O minéral apportée (en kg/ha)";
        final String indicatorB = "Quantité de B minéral apportée (en kg/ha)";
        final String indicatorCa = "Quantité de Ca minéral apportée (en kg/ha)";
        final String indicatorFe = "Quantité de Fe minéral apportée (en kg/ha)";
        final String indicatorMn = "Quantité de Mn minéral apportée (en kg/ha)";
        final String indicatorMo = "Quantité de Mo minéral apportée (en kg/ha)";
        final String indicatorNa2O = "Quantité de Na2O minéral apportée (en kg/ha)";
        final String indicatorSO3 = "Quantité de SO3 minéral apportée (en kg/ha)";
        final String indicatorCu = "Quantité de Cu minéral apportée (en kg/ha)";
        final String indicatorZn = "Quantité de Zn minéral apportée (en kg/ha)";
        final String indicatorMgO = "Quantité de MgO minéral apportée (en kg/ha)";

        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();

        {
            var pair = createAmmonitrate();

            final double qtAvg1 = 150.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg1, 1.0, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("19.5", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("45.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("24.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("15.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
            final double qtAvg2 = 500.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg2, 4.0, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("260.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("600.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("320.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("200.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
            final double qtAvg3 = 15.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg3, 2.0, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("3.9", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("9.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("4.8", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("3.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
            final double qtAvg4 = 150.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg4, 6.0, 2.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("234.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("540.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("288.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("180.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        {
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 109,
                    RefFertiMinUNIFA.PROPERTY_N, 20.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 11.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMMONITRATE Dosage < et = 28 % sans SO3"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "1112022000",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            final double qtAvg = 15.0;
            final String inputName = domainMineralProductInput.getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(mineralProduct, domainMineralProductInput, qtAvg, 1.5, 2.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("9.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("4.95", inputName))
                    )
            ));
        }

        {
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Pulvérulent",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 211,
                    RefFertiMinUNIFA.PROPERTY_N, 0.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 19.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 29.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "SUPER NORMAL"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "2010101900",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            final String inputName = domainMineralProductInput.getInputName();
            final double qtAvg = 15.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(mineralProduct, domainMineralProductInput, qtAvg, 2.5, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("7.125", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("10.875", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        {
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 110,
                    RefFertiMinUNIFA.PROPERTY_N, 33.5,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 60.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMMONITRATE Dosage > 28 % sans SO3"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "3110016000",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            final String inputName = domainMineralProductInput.getInputName();
            final double qtAvg = 15.0;
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(mineralProduct, domainMineralProductInput, qtAvg, 2.5, 2.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("25.125", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("45.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        {
            var pair = createAmendementsEngraisAutres();

            final double qtAvg = 15.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 2.33, 2.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("9.786", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("2.097", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("14.679", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        {
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 531,
                    RefFertiMinUNIFA.PROPERTY_N, 6.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 3.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 3.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 10.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "TERNAIRES < OU = 10% N"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "60303009",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            final double qtAvg = 15.0;
            final String inputName = domainMineralProductInput.getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(mineralProduct, domainMineralProductInput, qtAvg, 1.33, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("1.197", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("0.599", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.599", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("1.995", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        {
            var pair = createSoufreMagnesie();

            final double qtAvg = 15.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 1.66, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.996", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("8.466", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.249", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("3.237", inputName))
                    )
            ));
        }

        {
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Compacté",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 443,
                    RefFertiMinUNIFA.PROPERTY_N, 0.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 10.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 20.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 5.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AUTRES AMENDEMENTS ENGRAIS POTASSIQUES"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "1020101",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            final double qtAvg = 15.0;
            final String inputName = domainMineralProductInput.getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(mineralProduct, domainMineralProductInput, qtAvg, 0.66, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("0.99", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("1.98", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.495", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        {
            var pair = createMelangeGranulePK();

            final double qtAvg = 15.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 0.33, 5.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("5.693", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("2.723", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("2.97", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("1.485", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.743", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("1.238", inputName))
                    )
            ));
        }

        {
            RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                    RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                    RefFertiMinUNIFA.PROPERTY_CATEG, 180,
                    RefFertiMinUNIFA.PROPERTY_N, 16.0,
                    RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                    RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CALCIUM, 28.0,
                    RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                    RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                    RefFertiMinUNIFA.PROPERTY_S_O3, 0.0,
                    RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                    RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                    RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "NITRATE DE CHAUX"
            );

            final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                    AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                    DomainMineralProductInput.PROPERTY_INPUT_NAME, "1800071550",
                    DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                    DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                    DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                    AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                    AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
            );

            final double qtAvg = 15.0;
            final String inputName = domainMineralProductInput.getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(mineralProduct, domainMineralProductInput, qtAvg, 2.0, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("4.8", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("8.4", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        {
            var pair = createSulfonitrate();

            final double qtAvg = 1500.0;
            final String inputName = pair.getRight().getInputName();
            donneesTestFertilisation.add(new DonneesTestFertilisationMineSynthetise(pair.getLeft(), pair.getRight(), qtAvg, 3.5, 1.0,
                    Map.ofEntries(
                            Map.entry(indicatorN, new IndicatorInfo("1050.0", inputName)),
                            Map.entry(indicatorP2O5, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorK2O, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorB, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorCa, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorFe, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMo, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorNa2O, new IndicatorInfo("262.5", inputName)),
                            Map.entry(indicatorSO3, new IndicatorInfo("1260.0", inputName)),
                            Map.entry(indicatorCu, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorZn, new IndicatorInfo("0.0", inputName)),
                            Map.entry(indicatorMgO, new IndicatorInfo("0.0", inputName))
                    )
            ));
        }

        return donneesTestFertilisation;
    }

    public record IndicatorInfo(String value, String inputName) {
    }

    protected record OrganicInterventionInfo(String interventionName, String interventionTopiaId,
                                             OrganicProductInputUsage organicProductInputUsage,
                                             OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction,
                                             CroppingPlanEntry croppingPlanEntry) {
    }

    protected record MineralInterventionInfo(String interventionName, String interventionTopiaId,
                                             MineralProductInputUsage mineralProductInputUsage,
                                             MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction,
                                             CroppingPlanEntry croppingPlanEntry) {
    }

    public record DonneesTestFertilisationMineRealise(RefFertiMinUNIFA mineralProduct,
                                                      DomainMineralProductInput domainMineralProductInput,
                                                      double qtAvg,
                                                      double spatialFrequency,
                                                      int transitCount,
                                                      Map<String, String> expectedIndicatorNameAndValue) {
    }

    public record DonneesTestFertilisationMineTotalRealise(RefFertiMinUNIFA mineralProduct,
                                                       DomainMineralProductInput domainMineralProductInput,
                                                       double qtAvg,
                                                       double spatialFrequency,
                                                       int transitCount) {
    }

    public record DonneesTestFertilisationOrgaRealise(RefFertiOrga refFertiOrga,
                                                      DomainOrganicProductInput domainOrganicProductInput,
                                                      double qtAvg,
                                                      double spatialFrequency,
                                                      int transitCount,
                                                      Map<String, String> expectedIndicatorNameAndValue) {
    }

    public record DonneesTestFertilisationOrgaTotalRealise(RefFertiOrga refFertiOrga,
                                                      DomainOrganicProductInput domainOrganicProductInput,
                                                      double qtAvg,
                                                      double spatialFrequency,
                                                      int transitCount) {
    }

    public record DonneesTestFertilisationMineSynthetise(RefFertiMinUNIFA mineralProduct,
                                                         DomainMineralProductInput domainMineralProductInput,
                                                         double qtAvg,
                                                         double spatialFrequency,
                                                         double temporalFrequency,
                                                         Map<String, IndicatorInfo> expectedIndicatorNameAndInfo) {
    }

    public record DonneesTestFertilisationMineTotalSynthetise(RefFertiMinUNIFA mineralProduct,
                                                         DomainMineralProductInput domainMineralProductInput,
                                                         double qtAvg,
                                                         double spatialFrequency,
                                                         double temporalFrequency) {
    }

    public record DonneesTestFertilisationOrgaSynthetise(RefFertiOrga refFertiOrga,
                                                         DomainOrganicProductInput domainOrganicProductInput,
                                                         double qtAvg,
                                                         double spatialFrequency,
                                                         double temporalFrequency,
                                                         String inputName,
                                                         Map<String, String> expectedIndicatorNameAndValue) {
    }

    public record DonneesTestFertilisationOrgaTotalSynthetise(RefFertiOrga refFertiOrga,
                                                         DomainOrganicProductInput domainOrganicProductInput,
                                                         double qtAvg,
                                                         double spatialFrequency,
                                                         double temporalFrequency,
                                                         String inputName) {
    }

    public record TestDataRealise(String id, OrganicProductUnit organicProductUnit, double qtAvg,
                                  double spatialFrequency, int transitCount, String expectedN, String expectedP,
                                  String expectedK, String expectedCaO, String expectedMgO, String expectedS) {
    }
    public record TestUserElementDataRealise(
            String id,
            OrganicProductUnit organicProductUnit,
            double qtAvg,
            double spatialFrequency,
            int transitCount,
            double n,
            double p2O5,
            double k2O,
            Double caO,
            Double mgO,
            Double s,
            boolean organic,
            String expectedN,
            String expectedP,
            String expectedK,
            String expectedCaO,
            String expectedMgO,
            String expectedS
    ) {
    }

    public record TestDataSynthetise(String id, OrganicProductUnit organicProductUnit, double qtAvg,
                                     double spatialFrequency, double temporalFrequency, String expectedN,
                                     String expectedP, String expectedK, String expectedCaO, String expectedMgO,
                                     String expectedS) {
    }

    public record TestUserElementDataSynthetise(
            String id,
            OrganicProductUnit organicProductUnit,
            double qtAvg,
            double spatialFrequency,
            double temporalFrequency,
            double n,
            double p2O5,
            double k2O,
            Double caO,
            Double mgO,
            Double s,
            boolean organic,
            String expectedN,
            String expectedP,
            String expectedK,
            String expectedCaO,
            String expectedMgO,
            String expectedS
    ) {
    }

    protected Pair<RefFertiMinUNIFA, DomainMineralProductInput> createSulfonitrate() {
        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_CATEG, 108,
                RefFertiMinUNIFA.PROPERTY_N, 20.0,
                RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 5.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 24.0,
                RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "SULFONITRATE"
        );

        final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                DomainMineralProductInput.PROPERTY_INPUT_NAME, "1080012000",
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        return Pair.of(mineralProduct, domainMineralProductInput);
    }

    protected Pair<RefFertiMinUNIFA, DomainMineralProductInput> createAmendementsEngraisAutres() {
        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_CATEG, 253,
                RefFertiMinUNIFA.PROPERTY_N, 0.0,
                RefFertiMinUNIFA.PROPERTY_P2_O5, 14.0,
                RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                RefFertiMinUNIFA.PROPERTY_BORE, 3.0,
                RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 21.0,
                RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                RefFertiMinUNIFA.PROPERTY_ZINC, 0.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMENDEMENTS-ENGRAIS AUTRES"
        );

        final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                DomainMineralProductInput.PROPERTY_INPUT_NAME, "2064801400",
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        return Pair.of(mineralProduct, domainMineralProductInput);
    }

    protected Pair<RefFertiMinUNIFA, DomainMineralProductInput> createSoufreMagnesie() {
        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_CATEG, 701,
                RefFertiMinUNIFA.PROPERTY_N, 0.0,
                RefFertiMinUNIFA.PROPERTY_P2_O5, 0.0,
                RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                RefFertiMinUNIFA.PROPERTY_MANGANESE, 4.0,
                RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 13.0,
                RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 34.0,
                RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                RefFertiMinUNIFA.PROPERTY_ZINC, 1.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "SOUFRE - MAGNESIE"
        );

        final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                DomainMineralProductInput.PROPERTY_INPUT_NAME, "7013413001",
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        return Pair.of(mineralProduct, domainMineralProductInput);
    }

    protected Pair<RefFertiMinUNIFA, DomainMineralProductInput> createMelangeGranulePK() {
        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_CATEG, 412,
                RefFertiMinUNIFA.PROPERTY_N, 0.0,
                RefFertiMinUNIFA.PROPERTY_P2_O5, 23.0,
                RefFertiMinUNIFA.PROPERTY_K2_O, 11.0,
                RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 5.0,
                RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 12.0,
                RefFertiMinUNIFA.PROPERTY_CUIVRE, 6.0,
                RefFertiMinUNIFA.PROPERTY_ZINC, 3.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "MELANGE DE GRANULES P K"
        );

        final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                DomainMineralProductInput.PROPERTY_INPUT_NAME, "2311022",
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        return Pair.of(mineralProduct, domainMineralProductInput);
    }

    protected void testFertilisationRealise(RefFertiMinUNIFA mineralProduct,
                                            DomainMineralProductInput domainMineralProductInput,
                                            double qtAvg,
                                            double spatialFrequency,
                                            int transitCount,
                                            Map<String, String> expectedIndicatorNameAndValue,
                                            String indicatorCategory,
                                            GenericIndicator... indicators) {
        final MineralInterventionInfo interventionInfo = this.createMineralSpreadingEffectiveIntervention(
                mineralProduct,
                domainMineralProductInput,
                qtAvg,
                spatialFrequency,
                transitCount
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicators);
        final String content = out.toString();

        expectedIndicatorNameAndValue.forEach((indicatorName, indicatorValue) ->
                assertThat(content).usingComparator(comparator).isEqualTo(
                        "interventionSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;" +
                                "Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé;;Blé tendre;;Blé tendre;;" +
                                "Pleine production;Application de produits minéraux;" + interventionInfo.interventionName() +
                                ";Application de produits minéraux;" + writer.getInputUsageProductName(interventionInfo.mineralProductInputUsage(), interventionInfo.mineralFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                                ";;N;;2013;Réalisé;" + indicatorCategory + ";" + indicatorName + ";" + indicatorValue
                )
        );
    }

    protected void testTotalRealise(String indicatorCategory,
                                    String indicatorName,
                                    String indicatorValue,
                                    GenericIndicator... indicators) {
        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicators);
        final String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo(
                "zoneSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;" +
                        "Agriculture conventionnelle;Plot Baulon 1;Zone principale;Blé tendre;;;2013;Réalisé;" +
                        indicatorCategory + ";" + indicatorName + ";" + indicatorValue + ";;0;;"
        );
    }

    protected void testEpandageOrganiqueRealise(DonneesTestFertilisationOrgaRealise donneesTestFertilisation,
                                                String indicatorCategory,
                                                GenericIndicator... indicators) {
        final OrganicInterventionInfo interventionInfo = this.createOrganicFertilizerSpreadingEffectiveIntervention(
                donneesTestFertilisation.refFertiOrga(),
                donneesTestFertilisation.transitCount(),
                donneesTestFertilisation.spatialFrequency(),
                donneesTestFertilisation.qtAvg(),
                donneesTestFertilisation.domainOrganicProductInput()
        );

        // Calcul de l'indicateur
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicators);
        final String content = out.toString();

        donneesTestFertilisation.expectedIndicatorNameAndValue().forEach((indicatorName, indicatorValue) -> {
            assertThat(content).usingComparator(comparator).isEqualTo(
                    "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                            "Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;" +
                            "Application de produits minéraux;" + interventionInfo.interventionName() + ";12/03/2013;12/03/2013;Épandage organique;" +
                            writer.getInputUsageProductName(interventionInfo.organicProductInputUsage(), interventionInfo.organicFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                            ";;null;2013;" + indicatorCategory + ";" + indicatorName + ";" + indicatorValue
            );
        });
    }

    protected Pair<RefFertiMinUNIFA, DomainMineralProductInput> createAmmonitrate() {
        RefFertiMinUNIFATopiaDao refFertiMinUNIFADao = getPersistenceContext().getRefFertiMinUNIFADao();
        RefFertiMinUNIFA mineralProduct = refFertiMinUNIFADao.create(
                RefFertiMinUNIFA.PROPERTY_FORME, "Granulé",
                RefFertiMinUNIFA.PROPERTY_CATEG, 111,
                RefFertiMinUNIFA.PROPERTY_N, 13.0,
                RefFertiMinUNIFA.PROPERTY_P2_O5, 30.0,
                RefFertiMinUNIFA.PROPERTY_K2_O, 0.0,
                RefFertiMinUNIFA.PROPERTY_BORE, 0.0,
                RefFertiMinUNIFA.PROPERTY_CALCIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_FER, 0.0,
                RefFertiMinUNIFA.PROPERTY_MANGANESE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MOLYBDENE, 0.0,
                RefFertiMinUNIFA.PROPERTY_MG_O, 0.0,
                RefFertiMinUNIFA.PROPERTY_OXYDE_DE_SODIUM, 0.0,
                RefFertiMinUNIFA.PROPERTY_S_O3, 16.0,
                RefFertiMinUNIFA.PROPERTY_CUIVRE, 0.0,
                RefFertiMinUNIFA.PROPERTY_ZINC, 10.0,
                RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, "AMMONITRATE Dosage < et = 28 % avec SO3"
        );

        final DomainMineralProductInput domainMineralProductInput = mineralProductInputTopiaDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon,
                DomainMineralProductInput.PROPERTY_INPUT_NAME, "1134033000",
                DomainMineralProductInput.PROPERTY_USAGE_UNIT, MineralProductUnit.KG_HA,
                DomainMineralProductInput.PROPERTY_REF_INPUT, mineralProduct,
                DomainMineralProductInput.PROPERTY_PHYTO_EFFECT, false,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );

        return Pair.of(mineralProduct, domainMineralProductInput);
    }

    protected void testFertilisationSynthetise(RefFertiMinUNIFA mineralProduct,
                                               DomainMineralProductInput domainMineralProductInput,
                                               double qtAvg,
                                               double spatialFrequency,
                                               double temporalFrequency,
                                               Map<String, IndicatorInfo> expectedIndicatorNameAndInfo,
                                               String indicatorCategory,
                                               GenericIndicator... indicators) throws IOException {
        final MineralInterventionInfo interventionInfo = this.createMineralSpreadingPracticedIntervention(
                mineralProduct,
                domainMineralProductInput,
                qtAvg,
                spatialFrequency,
                temporalFrequency
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicators);
        final String content = out.toString();

        expectedIndicatorNameAndInfo.forEach((indicatorName, indicatorInfo) ->
                assertThat(content).usingComparator(comparator).isEqualTo(
                        "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                                "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Application de produits minéraux;" +
                                interventionInfo.interventionName() + ";" + "03/04;03/04;Application de produits minéraux;" +
                                writer.getInputUsageProductName(interventionInfo.mineralProductInputUsage(), interventionInfo.mineralFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                                ";;null;2012, 2013;" + indicatorCategory + ";" + indicatorName + ";" + indicatorInfo.value()
                )
        );
    }

    protected void testTotalSynthetise(String indicatorCategory,
                                       String indicatorName,
                                       String indicatorValue,
                                       GenericIndicator... indicators) throws IOException {
        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicators);
        final String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo(
                "practicedSystemSheet;Baulon;;;Systeme de culture Baulon 1;HOF21095;" +
                        "Agriculture conventionnelle;NON;Efficience;NON;;2012, 2013;Synthétisé;" +
                        indicatorCategory + ";" + indicatorName + ";" + indicatorValue + ";0;;"
        );
    }

    public void testEpandageOrganiqueSynthetise(DonneesTestFertilisationOrgaSynthetise donneesTestFertilisation,
                                                String indicatorCategory,
                                                GenericIndicator... indicators) throws IOException {
        final OrganicInterventionInfo interventionInfo = this.createOrganicFertilizerSpreadingPracticedIntervention(
                donneesTestFertilisation.temporalFrequency(),
                donneesTestFertilisation.spatialFrequency(),
                donneesTestFertilisation.qtAvg(),
                donneesTestFertilisation.domainOrganicProductInput()
        );

        // Calcul des indicateurs de performance
        Performance performance = new PerformanceImpl();
        performance.setName("Performance synthétisée " + UUID.randomUUID());
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setPerformance(performance);
        writer.setUseOriginaleWriterFormat(false);

        performanceService.convertToWriter(performance, writer, indicatorFilters, indicators);
        final String content = out.toString();

        donneesTestFertilisation.expectedIndicatorNameAndValue().forEach((indicatorName, indicatorValue) ->
                assertThat(content).usingComparator(comparator).isEqualTo(
                        "inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;" +
                                "Efficience;Blé (rang 2);;Blé tendre;;Blé tendre;;Blé;Application de produits minéraux;" +
                                interventionInfo.interventionName() + ";" + "03/04;03/04;Épandage organique;" +
                                writer.getInputUsageProductName(interventionInfo.organicProductInputUsage(), interventionInfo.organicFertilizersSpreadingAction(), null, interventionInfo.croppingPlanEntry(), true) +
                                ";;null;2012, 2013;" + indicatorCategory + ";" + indicatorName + ";" + indicatorValue
                )
        );
    }
}
