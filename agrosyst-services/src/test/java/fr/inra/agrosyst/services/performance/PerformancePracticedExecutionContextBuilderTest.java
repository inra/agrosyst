package fr.inra.agrosyst.services.performance;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainImpl;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingImpl;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.referential.DestinationContext;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefBioAgressor;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShape;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.RefInputPriceService;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.search.DomainPhytoProductInputSearchResults;
import fr.inra.agrosyst.api.services.domain.inputStock.search.PhytoProductInputFilter;
import fr.inra.agrosyst.api.services.performance.EquipmentUsageRange;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.api.services.practiced.RefStadeEdiDto;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.api.services.referential.DomainReferentialInputs;
import fr.inra.agrosyst.api.services.referential.GroupeCibleDTO;
import fr.inra.agrosyst.api.services.referential.MaterielDto;
import fr.inra.agrosyst.api.services.referential.MineralProductType;
import fr.inra.agrosyst.api.services.referential.RefAnimalTypeDto;
import fr.inra.agrosyst.api.services.referential.RefCattleAnimalTypeDto;
import fr.inra.agrosyst.api.services.referential.RefCattleRationAlimentDto;
import fr.inra.agrosyst.api.services.referential.RefPotDto;
import fr.inra.agrosyst.api.services.referential.RefSubstrateDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.referential.TypeMaterielFilter;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.performance.performancehelper.PerformancePracticedExecutionContextBuilder;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

public class PerformancePracticedExecutionContextBuilderTest extends AbstractAgrosystTest {

    public static class PerformancePracticedExecutionContextBuilderMock extends PerformancePracticedExecutionContextBuilder {

        public PerformancePracticedExecutionContextBuilderMock(AnonymizeService anonymizeService,
                                                               DomainInputStockUnitService domainInputStockUnitService,
                                                               DomainService domainService,
                                                               InputPriceService inputPriceService,
                                                               CommonPerformanceService performanceService,
                                                               ReferentialService referentialService,
                                                               RefInputPriceService refInputPriceService,
                                                               PricesService pricesService,
                                                               Collection<IndicatorFilter> indicatorFilters,
                                                               boolean executeAsAdmin,
                                                               PriceConverterKeysToRate priceConverterKeysToRate,
                                                               RefSolTextureGeppa defaultRefSolTextureGeppa,
                                                               Set<String> scenarioCodes,
                                                               RefLocation defaultLocation,
                                                               RefSolProfondeurIndigo defaultSolDepth) {
            super(anonymizeService, domainInputStockUnitService, domainService, inputPriceService, performanceService, referentialService, refInputPriceService, pricesService, indicatorFilters, executeAsAdmin, priceConverterKeysToRate, defaultRefSolTextureGeppa, scenarioCodes, defaultLocation, defaultSolDepth);
        }

        @Override
        public Map<String, ToolsCoupling> getClosetToolsCoupling(List<ToolsCoupling> toolsCouplings, Domain domain) {
            return super.getClosetToolsCoupling(toolsCouplings, domain);
        }
    }

    @Test
    public void testGetClosetToolsCoupling() {
        ReferentialServiceMock rs = new ReferentialServiceMock();
        PerformancePracticedExecutionContextBuilderMock mockBuild = new PerformancePracticedExecutionContextBuilderMock(
                null, null, null, null, null, rs, null, null, null, true, null, null, null, null, null);

        Domain dRef = new DomainImpl();
        dRef.setCampaign(2024);

        Domain dRefM1 = new DomainImpl();
        dRefM1.setCampaign(2023);

        Domain dRefM2 = new DomainImpl();
        dRefM2.setCampaign(2022);

        Domain dRefM3 = new DomainImpl();
        dRefM3.setCampaign(2021);

        Domain dRefP1 = new DomainImpl();
        dRefP1.setCampaign(2025);

        Domain dRefP2 = new DomainImpl();
        dRefP2.setCampaign(2026);

        Domain dRefP3 = new DomainImpl();
        dRefP3.setCampaign(2027);

        List<ToolsCoupling> toolsCouplings = new ArrayList<>();
        String sameCode = "AAA000111";

        ToolsCoupling tcdRef = new ToolsCouplingImpl();
        tcdRef.setCode(sameCode);
        tcdRef.setDomain(dRef);
        toolsCouplings.add(tcdRef);

        ToolsCoupling tcdRefM3 = new ToolsCouplingImpl();
        tcdRefM3.setCode(sameCode);
        tcdRefM3.setDomain(dRefM3);
        toolsCouplings.add(tcdRefM3);

        ToolsCoupling tcdRefM1 = new ToolsCouplingImpl();
        tcdRefM1.setCode(sameCode);
        tcdRefM1.setDomain(dRefM1);
        toolsCouplings.add(tcdRefM1);

        ToolsCoupling tcdRefM2 = new ToolsCouplingImpl();
        tcdRefM2.setCode(sameCode);
        tcdRefM2.setDomain(dRefM2);
        toolsCouplings.add(tcdRefM2);

        ToolsCoupling tcdRefP3 = new ToolsCouplingImpl();
        tcdRefP3.setCode(sameCode);
        tcdRefP3.setDomain(dRefP3);
        toolsCouplings.add(tcdRefP3);

        ToolsCoupling tcdRefP2 = new ToolsCouplingImpl();
        tcdRefP2.setCode(sameCode);
        tcdRefP2.setDomain(dRefP2);
        toolsCouplings.add(tcdRefP2);

        ToolsCoupling tcdRefP1 = new ToolsCouplingImpl();
        tcdRefP1.setCode(sameCode);
        tcdRefP1.setDomain(dRefP1);
        toolsCouplings.add(tcdRefP1);

        Map<String, ToolsCoupling> closetToolsCoupling = mockBuild.getClosetToolsCoupling(toolsCouplings, dRef);

        // ici, on attend la combinaison d'outil avec la même campagne
        ToolsCoupling result = closetToolsCoupling.get(sameCode);
        Assertions.assertEquals(tcdRef.getDomain().getCampaign(), result.getDomain().getCampaign());

        // ici, on attend la combinaison d'outil à la campagne immédiatement inférieur
        toolsCouplings.remove(tcdRef);
        closetToolsCoupling = mockBuild.getClosetToolsCoupling(toolsCouplings, dRef);
        result = closetToolsCoupling.get(sameCode);
        Assertions.assertEquals(tcdRef.getDomain().getCampaign() - 1, result.getDomain().getCampaign());

        // ici, on attend la combinaison d'outil à la campagne immédiatement supérieure
        toolsCouplings.removeAll(List.of(tcdRefM1, tcdRefM2, tcdRefM3));
        closetToolsCoupling = mockBuild.getClosetToolsCoupling(toolsCouplings, dRef);
        result = closetToolsCoupling.get(sameCode);
        Assertions.assertEquals(tcdRef.getDomain().getCampaign() + 1, result.getDomain().getCampaign());
    }

    public static class ReferentialServiceMock implements ReferentialService {

        @Override
        public boolean isValidRefFertiMinProduct(RefFertiMinUNIFA product) {
            return false;
        }

        @Override
        public Map<MaterielType, List<Pair<String, String>>> getWithoutPetitMaterielTypeMateriel1List(boolean filterEdaplos) {
            return Map.of();
        }

        @Override
        public Map<MaterielType, List<Pair<String, String>>> getPetitMaterielTypeMateriel1List() {
            return Map.of();
        }

        @Override
        public List<String> getRefFertiMinUnifTypeProduitForCateg(Integer categ) {
            return List.of();
        }

        @Override
        public List<Pair<String, String>> getTypeMateriel2List(TypeMaterielFilter filter) {
            return List.of();
        }

        @Override
        public List<Pair<String, String>> getTypeMateriel3List(TypeMaterielFilter filter) {
            return List.of();
        }

        @Override
        public List<Pair<String, String>> getTypeMateriel4List(TypeMaterielFilter filter) {
            return List.of();
        }

        @Override
        public Map<String, List<MaterielDto>> getMaterielUniteMap(TypeMaterielFilter filter) {
            return Map.of();
        }

        @Override
        public RefMateriel getMateriel(String materielTopiaId) {
            return null;
        }

        @Override
        public Map<Integer, String> getSolArvalisRegions() {
            return Map.of();
        }

        @Override
        public List<RefSolArvalis> getSolArvalis(Integer regionCode) {
            return List.of();
        }

        @Override
        public List<RefEspece> findSpecies(String filter) {
            return List.of();
        }

        @Override
        public boolean validVarietesFromCodeEspeceEdi(RefVariete variete, String code_espece_botanique) {
            return false;
        }

        @Override
        public RefEspece getSpecies(String speciesId) {
            return null;
        }

        @Override
        public List<RefVariete> getVarietes(String speciesId, String filter) {
            return List.of();
        }

        @Override
        public RefLocation getRefLocation(String refLocationId) {
            return null;
        }

        @Override
        public List<RefLocation> getActiveCommunes(String filter) {
            return List.of();
        }

        @Override
        public List<RefLocation> getActiveCommunes(String filter, String refCountryId) {
            return List.of();
        }

        @Override
        public Map<Integer, String> getAllActiveOtex18Code() {
            return Map.of();
        }

        @Override
        public Map<Integer, String> getAllActiveCodeOtex70ByOtex18code(Integer otex18code) {
            return Map.of();
        }

        @Override
        public List<RefOrientationEDI> getAllReferentielEDI() {
            return List.of();
        }

        @Override
        public List<RefVariete> getGraftSupports(String speciesId, String term) {
            return List.of();
        }

        @Override
        public List<RefClonePlantGrape> getGraftClones(String speciesId, String varietyId, String term) {
            return List.of();
        }

        @Override
        public List<RefInterventionAgrosystTravailEDI> getAllActiveAgrosystActions() {
            return List.of();
        }

        @Override
        public List<RefInterventionAgrosystTravailEDI> getAllActiveAgrosystActions(AgrosystInterventionType interventionType) {
            return List.of();
        }

        @Override
        public List<RefParcelleZonageEDI> getAllActiveParcelleZonage() {
            return List.of();
        }

        @Override
        public List<RefSolTextureGeppa> getAllActiveSolTextures() {
            return List.of();
        }

        @Override
        public List<RefSolTextureGeppa> getAllNotFrenchActiveSolTextures() {
            return List.of();
        }

        @Override
        public List<RefSolProfondeurIndigo> getAllActiveSolProfondeurs() {
            return List.of();
        }

        @Override
        public List<RefSolCaracteristiqueIndigo> getAllActiveSolCaracteristiques() {
            return List.of();
        }

        @Override
        public List<MineralProductType> getAllActiveMineralProductTypes() {
            return List.of();
        }

        @Override
        public List<RefFertiMinUNIFA> getAllActiveRefFertiMinUnifaByCategAndShape(Integer categ, String fertilizerShape) {
            return List.of();
        }

        @Override
        public List<RefFertiOrga> getAllActiveOrganicProductTypes() {
            return List.of();
        }

        @Override
        public List<RefStadeEdiDto> getRefStadesEdi(Integer vegetativeProfile) {
            return List.of();
        }

        @Override
        public Map<String, String> getActaTreatementCodesAndNames() {
            return Map.of();
        }

        @Override
        public Map<AgrosystInterventionType, List<ProductType>> getAllActiveActaTreatmentProductTypes() {
            return Map.of();
        }

        @Override
        public List<RefElementVoisinage> getAllActiveElementVoisinages() {
            return List.of();
        }

        @Override
        public List<RefBioAgressor> getTreatmentTargets(BioAgressorType category) {
            return List.of();
        }

        @Override
        public List<RefTraitSdC> getAllActiveGrowingSystemCharacteristics() {
            return List.of();
        }

        @Override
        public List<RefActaTraitementsProduit> getActaTraitementsProduits(AgrosystInterventionType interventionType, ProductType productType) {
            return List.of();
        }

        @Override
        public RefBioAgressor getBioAgressor(String bioAgressorId) {
            return null;
        }

        @Override
        public RefDepartmentShape getDepartmentShape(String departmentCode) {
            return null;
        }

        @Override
        public Map<String, String> getAllRefStationMeteoMap() {
            return Map.of();
        }

        @Override
        public RefFertiMinUNIFA createOrUpdateRefMineralProductToInput(RefFertiMinUNIFA product) {
            return null;
        }

        @Override
        public RefFertiMinUNIFA createOrUpdateRefMineralProductInput(DomainMineralProductInputDto productDto) {
            return null;
        }

        @Override
        public Map<String, String> getCroppingPlanSpeciesCodeByRefEspeceAndVarietyKey(Collection<CroppingPlanSpecies> allCroppingPlanSpecies) {
            return Map.of();
        }

        @Override
        public DestinationContext getDestinationContext(Set<String> speciesCodes, Set<WineValorisation> wineValorisations, String growingSystemId, String campaigns, String zoneTopiaId) {
            return null;
        }

        @Override
        public Map<Pair<String, String>, List<Sector>> getSectorsByCodeEspeceBotaniqueCodeQualifiantAEE(List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE) {
            return Map.of();
        }

        @Override
        public List<Pair<String, String>> getAllCodeEspeceBotaniqueCodeQualifantForDomainIds(Set<String> domainIds) {
            return List.of();
        }

        @Override
        public Map<String, List<Pair<String, String>>> getAllCodeEspeceBotaniqueCodeQualifantBySpeciesCodeForDomainIds(Set<String> domainIds) {
            return Map.of();
        }

        @Override
        public Map<String, List<Sector>> getSectorsByCodeEspeceBotanique_CodeQualifiantAEE(List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE) {
            return Map.of();
        }

        @Override
        public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForPracticedSystemValorisations(Collection<HarvestingActionValorisation> valorisations, String practicedSystemCampaigns, List<RefHarvestingPrice> refHarvestingPrices, Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
            return null;
        }

        @Override
        public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForPracticedSystemValorisations(Collection<HarvestingActionValorisation> valorisations, String practicedSystemCampaigns, Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
            return null;
        }

        @Override
        public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisations(Collection<HarvestingActionValorisation> valorisations, List<RefHarvestingPrice> refHarvestingPrices, Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
            return null;
        }

        @Override
        public LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisations(Collection<HarvestingActionValorisation> valorisations, Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
            return null;
        }

        @Override
        public Map<String, Set<Pair<String, String>>> getCodeEspBotCodeQualiBySpeciesCode(Set<String> speciesCodes) {
            return Map.of();
        }

        @Override
        public EnumMap<Sector, List<RefMarketingDestination>> getRefMarketingDestinationsBySectorForSectors(Set<Sector> sectors) {
            return null;
        }

        @Override
        public List<RefTraitSdC> getTraitSdcForIdsIn(List<String> growingSystemCharacteristicIds) {
            return List.of();
        }

        @Override
        public RefTypeAgriculture getRefTypeAgricultureWithId(String typeAgricultureId) {
            return null;
        }

        @Override
        public List<RefTypeAgriculture> getAllTypeAgricultures() {
            return List.of();
        }

        @Override
        public List<RefTypeAgriculture> getIpmWorksTypeAgricultures() {
            return List.of();
        }

        @Override
        public List<RefStrategyLever> loadRefStrategyLeversForTerm(Sector sector, SectionType sectionType, StrategyType strategyType, String term) {
            return List.of();
        }

        @Override
        public List<RefStrategyLever> loadRefStrategyLevers(String growingSystemTopiaId, SectionType sectionType, StrategyType strategyType) {
            return List.of();
        }

        @Override
        public List<RefAnimalType> getAllActiveRefAnimalTypes() {
            return List.of();
        }

        @Override
        public List<RefAnimalTypeDto> getAllTranslatedActiveRefAnimalTypes() {
            return List.of();
        }

        @Override
        public List<RefLegalStatus> getAllRefActiveLegalStatus() {
            return List.of();
        }

        @Override
        public List<RefBioAgressor> getBioAgressors(Collection<BioAgressorType> bioAgressorType) {
            return List.of();
        }

        @Override
        public List<RefBioAgressor> getBioAgressors(Collection<BioAgressorType> bioAgressorType, Collection<Sector> sectors) {
            return List.of();
        }

        @Override
        public ExportResult exportAllMaterielXlsx() {
            return null;
        }

        @Override
        public ExportResult exportAllSpeciesXslx() {
            return null;
        }

        @Override
        public Collection<EquipmentUsageRange> getEquipmentlUsageRangeForEquipments(Collection<RefMateriel> materiels) {
            return List.of();
        }

        @Override
        public List<RefCattleAnimalTypeDto> getAllTranslatedActiveRefCattleAnimalTypes() {
            return List.of();
        }

        @Override
        public List<RefCattleRationAlimentDto> getAllTranslatedActiveRefCattleRationAliments() {
            return List.of();
        }

        @Override
        public List<RefSubstrateDto> getAllActiveRefSubstrates() {
            return List.of();
        }

        @Override
        public List<RefPotDto> getAllActiveRefPots() {
            return List.of();
        }

        @Override
        public List<RefOtherInput> getAllActiveRefOtherInputs() {
            return List.of();
        }

        @Override
        public ExportResult exportAllActiveRefOtherInputsToXlsx() {
            return null;
        }

        @Override
        public ExportResult exportAllActiveRefSubstratesToXlsx() {
            return null;
        }

        @Override
        public ReferenceDoseDTO computeReferenceDoseForIFT(String refInputId, Collection<String> refEspeceIds) {
            return null;
        }

        @Override
        public ReferenceDoseDTO computeLocalizedReferenceDoseForIFTLegacy(String refInputId, Set<String> refEspeceIds) {
            return null;
        }

        @Override
        public ReferenceDoseDTO computeReferenceDoseForIFTCibleNonMillesime(String refProductId, Collection<String> refEspeceIds, Collection<String> targetIds, Collection<String> groupesCibles) {
            return null;
        }

        @Override
        public ReferenceDoseDTO computeLocalizedReferenceDoseForIFCCibleNonMillesime(String refInputId, Set<String> refEspeceIds, Set<String> targetIds, Set<String> groupesCibles) {
            return null;
        }

        @Override
        public ReferenceDoseDTO computeReferenceDoseForIFCCibleMillesime(String refInputId, Set<String> refEspeceIds, Set<String> bioAgressorIdentifiantOrReference_id, Set<String> groupesCibles, Integer campaign) {
            return null;
        }

        @Override
        public ReferenceDoseDTO computeReferenceDoseForGivenProductAndActaSpecies(RefActaTraitementsProduit phytoProduct, Collection<RefEspece> refEspeces) {
            return null;
        }

        @Override
        public Optional<RefMAADosesRefParGroupeCible> computeMaaRefDose(Collection<String> bioAgressorIdentifiantOrReferenceIds, Collection<String> groupesCibles, Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> doseRefsSupplierByTargetId) {
            return Optional.empty();
        }

        @Override
        public Map<String, Boolean> getGroupesCiblesCodesAndGeneric(String property, String bioAgressorEdiIdsOrGroupesCible) {
            return Map.of();
        }

        @Override
        public List<RefCiblesAgrosystGroupesCiblesMAA> getRefCiblesAgrosystGroupesCiblesMAAS(String property, String bioAgressorEdiIdsOrGroupesCible) {
            return List.of();
        }

        @Override
        public ExportResult exportPhytoAndLutteBioActaTraitementProduitsXlsx() {
            return null;
        }

        @Override
        public InputStream exportCiblesGroupesCiblesMaaCSV() {
            return null;
        }

        @Override
        public Collection<String> getCodeAmmBioControle(int campagne) {
            return List.of();
        }

        @Override
        public Collection<String> getCodeAmmBioControle(String campagnes) {
            return List.of();
        }

        @Override
        public List<GroupeCibleDTO> getGroupesCibles() {
            return List.of();
        }

        @Override
        public Map<String, String> getGroupesCiblesParCode() {
            return Map.of();
        }

        @Override
        public Map<String, BioAgressorType> getGroupesCiblesReferenceParamParCode() {
            return Map.of();
        }

        @Override
        public Map<AgrosystInterventionType, String> getAgrosystInterventionTypeTranslationMap() {
            return Map.of();
        }

        @Override
        public <E extends ReferentialEntity> List<E> getAllActiveReferentialEntities(Class<E> clazz) {
            return List.of();
        }

        @Override
        public RefPot loadRefPot(String refPotId) {
            return null;
        }

        @Override
        public DomainReferentialInputs getDomainReferentialInputs(Collection<DomainInputDto> domainInputDtos) {
            return null;
        }

        @Override
        public Collection<RefDestination> loadDestinationsForIds(Set<String> destinationIds) {
            return List.of();
        }

        @Override
        public Collection<RefQualityCriteria> loadRefQualityCriteriaForIds(Set<String> qualityCriteriaRefIds) {
            return List.of();
        }

        @Override
        public Collection<RefQualityCriteriaClass> loadRefQualityCriteriaClassForIds(Set<String> qualityCriteriaRefQualityCriteriaClassIds) {
            return List.of();
        }

        @Override
        public Collection<RefInputUnitPriceUnitConverter> loadAllRefInputUnitPriceUnitConverter() {
            return List.of();
        }

        @Override
        public RefStationMeteo findRefStationMeteoByTopiaId(String topiaId) {
            return null;
        }

        @Override
        public DomainPhytoProductInputSearchResults getDomainPhytoProductInputSearchResults(PhytoProductInputFilter phytoProductInputFilter) {
            return null;
        }

        @Override
        public DomainPhytoProductInputSearchResults getDomainBiologicalControlInputsSearchResults(PhytoProductInputFilter phytoProductInputFilter) {
            return null;
        }

        @Override
        public RefInterventionAgrosystTravailEDI getRefInterventionAgrosystTravailEDI(String manActionId) {
            return null;
        }

        @Override
        public RefDestination getDestination(String topiaId) {
            return null;
        }

        @Override
        public RefQualityCriteria loadQualityCriteriaForId(String refQualityCriteriaId) {
            return null;
        }

        @Override
        public RefDestination loadDestinationForId(String destinationId) {
            return null;
        }

        @Override
        public Map<BioAgressorType, String> getTranslatedBioAgressorType() {
            return Map.of();
        }

        @Override
        public InputStream matchMaaWithFrenchMaa(InputStream contentStream) {
            return null;
        }

        @Override
        public List<RefEspece> getAllActiveRefEspeceWithGivenCodeEspeceBotanique(Set<String> codeEspeceBotaniques) {
            return List.of();
        }

        @Override
        public List<RefCompositionSubstancesActivesParNumeroAMM> getDomainRefCompositionSubstancesActivesParNumeroAMM(Domain domain) {
            return List.of();
        }

        @Override
        public Map<String, Double> getCoeffsConversionVersKgHa() {
            return Map.of();
        }

        @Override
        public MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> getSubstancesActivesCommissionEuropeenneByAmmCodeForDomain(Domain domain) {
            return null;
        }

        @Override
        public MultiValuedMap<String, RefPhrasesRisqueEtClassesMentionDangerParAMM> getRefPhrasesRisqueEtClassesMentionDangerParAMMForDomainByAMM(Domain domain) {
            return null;
        }

        @Override
        public Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> findRefCorrespondanceMaterielOutilsTSForTools(Set<RefMateriel> materiels) {
            return Map.of();
        }

        @Override
        public List<SeedPlantUnit> getSeedUnitsForSpecies(List<RefEspece> species) {
            return List.of();
        }

        @Override
        public Object getAllOrganicProductTypes(boolean includeInactive) {
            return null;
        }

        @Override
        public Map<HarvestingActionValorisationDto, List<RefHarvestingPrice>> getRefHarvestingPricesForValorisationDtos(
                Collection<HarvestingActionValorisationDto> valorisationDtos, Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
            return Map.of();
        }
    }

}
