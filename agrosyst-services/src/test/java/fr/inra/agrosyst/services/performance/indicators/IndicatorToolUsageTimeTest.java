package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionImpl;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.UUID;

/**
 * Test specific à l'indicateur Temps d'utilisation matériel.
 */
public class IndicatorToolUsageTimeTest extends AbstractAgrosystTest {

    /**
     * Test la répartition du temps d'utilisation sur des mois complets, même année.
     */
    @Test
    public void testPracticedMonth1() {
        IndicatorToolUsageTime indicatorToolUsageTime = new IndicatorToolUsageTime();
        PracticedIntervention intervention = new PracticedInterventionImpl();
        intervention.setTopiaId(PracticedSystemService.NEW_INTERVENTION_PREFIX + UUID.randomUUID());
        intervention.setStartingPeriodDate("1/3");
        intervention.setEndingPeriodDate("31/7");
        Double[] monthsRatio = indicatorToolUsageTime.getMonthsRatio(intervention, 50.0d);
        Assertions.assertEquals(13, monthsRatio.length);
        Assertions.assertEquals(0.0d, (double) monthsRatio[0]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[1]);
        Assertions.assertEquals(10.130719d, monthsRatio[2], 0.00001);
        Assertions.assertEquals(9.80392157d, monthsRatio[3], 0.00001);
        Assertions.assertEquals(10.130719d, monthsRatio[4], 0.00001);
        Assertions.assertEquals(9.80392157d, monthsRatio[5], 0.00001);
        Assertions.assertEquals(10.130719d, monthsRatio[6], 0.00001);
        Assertions.assertEquals(0.0d, (double) monthsRatio[7]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[8]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[9]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[10]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[11]);
        Assertions.assertEquals(50.0d, (double) monthsRatio[12]);
    }

    /**
     * Test l'apartenance à un mois pour une intervention si la période porte est à cheval sur deux
     * années.
     */
    @Test
    public void testPracticedMonth2() {
        IndicatorToolUsageTime indicatorToolUsageTime = new IndicatorToolUsageTime();
        PracticedIntervention intervention = new PracticedInterventionImpl();
        intervention.setTopiaId(PracticedSystemService.NEW_INTERVENTION_PREFIX + UUID.randomUUID());
        intervention.setStartingPeriodDate("1/7");
        intervention.setEndingPeriodDate("31/3");
        Double[] monthsRatio = indicatorToolUsageTime.getMonthsRatio(intervention, 50.0d);
        Assertions.assertEquals(13, monthsRatio.length);
        Assertions.assertEquals(5.65693431d, monthsRatio[0], 0.00000001);
        Assertions.assertEquals(5.10948905d, monthsRatio[1], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[2], 0.00000001);
        Assertions.assertEquals(0.0d, (double) monthsRatio[3]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[4]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[5]);
        Assertions.assertEquals(5.65693431d, monthsRatio[6], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[7], 0.00000001);
        Assertions.assertEquals(5.474452554744525d, monthsRatio[8], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[9], 0.00000001);
        Assertions.assertEquals(5.474452554744525d, monthsRatio[10], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[11], 0.00000001);
        Assertions.assertEquals(50.0d, (double) monthsRatio[12]);
    }

    /**
     * Test l'absence de temps d'usage sur chaque mois pour une intervention si les périodes ne sont pas renseignées.
     */
    @Test
    public void testPracticedMonthEmpty() {
        IndicatorToolUsageTime indicatorToolUsageTime = new IndicatorToolUsageTime();
        PracticedIntervention intervention = new PracticedInterventionImpl();
        Double[] monthsRatio = indicatorToolUsageTime.getMonthsRatio(intervention, 50.0d);
        Assertions.assertEquals(13, monthsRatio.length);
        Assertions.assertEquals(0.0d, (double) monthsRatio[0]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[1]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[2]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[3]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[4]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[5]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[6]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[7]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[8]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[9]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[10]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[11]);
        Assertions.assertEquals(50.0d, (double) monthsRatio[12]);
    }

    /**
     * Test la répartition du temps d'utilisation sur des mois complets, même
     * année, pour une intervention réalisée
     */
    @Test
    public void testEffectiveMonth1() {
        IndicatorToolUsageTime indicatorToolUsageTime = new IndicatorToolUsageTime();
        EffectiveIntervention intervention = new EffectiveInterventionImpl();
        intervention.setStartInterventionDate(LocalDate.of(2013, 3, 1));
        intervention.setEndInterventionDate(LocalDate.of(2013, 7, 31));
        Double[] monthsRatio = indicatorToolUsageTime.getMonthsRatio(intervention, 50.0d);
        Assertions.assertEquals(13, monthsRatio.length);
        Assertions.assertEquals(0.0d, (double) monthsRatio[0]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[1]);
        Assertions.assertEquals(10.130719d, monthsRatio[2], 0.00001);
        Assertions.assertEquals(9.80392157d, monthsRatio[3], 0.00001);
        Assertions.assertEquals(10.130719d, monthsRatio[4], 0.00001);
        Assertions.assertEquals(9.80392157d, monthsRatio[5], 0.00001);
        Assertions.assertEquals(10.130719d, monthsRatio[6], 0.00001);
        Assertions.assertEquals(0.0d, (double) monthsRatio[7]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[8]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[9]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[10]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[11]);
        Assertions.assertEquals(50.0d, (double) monthsRatio[12]);
    }

    /**
     * Test la répartition du temps d'utilisation sur des mois complets, à cheval
     * sur deux années, pour une intervention réalisée.
     */
    @Test
    public void testEffectiveMonth2() {
        IndicatorToolUsageTime indicatorToolUsageTime = new IndicatorToolUsageTime();
        EffectiveIntervention intervention = new EffectiveInterventionImpl();
        intervention.setStartInterventionDate(LocalDate.of(2013, 7, 1));
        intervention.setEndInterventionDate(LocalDate.of(2014, 3, 31));
        Double[] monthsRatio = indicatorToolUsageTime.getMonthsRatio(intervention, 50.0d);
        Assertions.assertEquals(13, monthsRatio.length);
        Assertions.assertEquals(5.65693431d, monthsRatio[0], 0.00000001);
        Assertions.assertEquals(5.10948905d, monthsRatio[1], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[2], 0.00000001);
        Assertions.assertEquals(0.0d, (double) monthsRatio[3]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[4]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[5]);
        Assertions.assertEquals(5.65693431d, monthsRatio[6], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[7], 0.00000001);
        Assertions.assertEquals(5.474452554744525d, monthsRatio[8], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[9], 0.00000001);
        Assertions.assertEquals(5.474452554744525d, monthsRatio[10], 0.00000001);
        Assertions.assertEquals(5.65693431d, monthsRatio[11], 0.00000001);
        Assertions.assertEquals(50.0d, (double) monthsRatio[12]);
    }

    /**
     * Test l'absence de temps d'usage sur chaque mois pour une intervention si
     * les périodes ne sont pas renseignées.
     */
    @Test
    public void testEffectiveMonthEmpty() {
        IndicatorToolUsageTime indicatorToolUsageTime = new IndicatorToolUsageTime();
        EffectiveIntervention intervention = new EffectiveInterventionImpl();
        Double[] monthsRatio = indicatorToolUsageTime.getMonthsRatio(intervention, 50.0d);
        Assertions.assertEquals(13, monthsRatio.length);
        Assertions.assertEquals(0.0d, (double) monthsRatio[0]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[1]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[2]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[3]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[4]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[5]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[6]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[7]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[8]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[9]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[10]);
        Assertions.assertEquals(0.0d, (double) monthsRatio[11]);
        Assertions.assertEquals(50.0d, (double) monthsRatio[12]);
    }
}
