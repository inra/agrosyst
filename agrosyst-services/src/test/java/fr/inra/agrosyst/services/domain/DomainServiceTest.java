package fr.inra.agrosyst.services.domain;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Aliment;
import fr.inra.agrosyst.api.entities.AlimentImpl;
import fr.inra.agrosyst.api.entities.Cattle;
import fr.inra.agrosyst.api.entities.CattleImpl;
import fr.inra.agrosyst.api.entities.CompagneType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryImpl;
import fr.inra.agrosyst.api.entities.CroppingPlanEntryTopiaDao;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesImpl;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainFuelInput;
import fr.inra.agrosyst.api.entities.DomainImpl;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInputTopiaDao;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.EquipmentImpl;
import fr.inra.agrosyst.api.entities.GeoPoint;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.LivestockUnit;
import fr.inra.agrosyst.api.entities.LivestockUnitImpl;
import fr.inra.agrosyst.api.entities.LivestockUnitTopiaDao;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Ration;
import fr.inra.agrosyst.api.entities.RationImpl;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.ToolsCouplingImpl;
import fr.inra.agrosyst.api.entities.WeatherStation;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCycleNode;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhaseTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAlimentTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAA;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceImpl;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOTEXTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSectorTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgricultureTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGevesTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrapeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.services.common.BinaryContent;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.UsageList;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlans;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.DomainExtendException;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.plot.PlotService;
import fr.inra.agrosyst.api.services.referential.MaterielDto;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.services.referential.TypeMaterielFilter;
import fr.inra.agrosyst.commons.gson.AgrosystGsonSupplier;
import fr.inra.agrosyst.services.AbstractAgrosystTest;
import fr.inra.agrosyst.services.TestDatas;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.domain.inputStock.DomainInputStockServiceTest;
import fr.inra.agrosyst.services.effective.EffectiveCropCycleServiceTest;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class DomainServiceTest extends AbstractAgrosystTest {

    private static final Log LOGGER = LogFactory.getLog(DomainServiceTest.class);

    protected DomainService domainService;
    protected AgrosystI18nService i18nService;
    protected DomainInputStockUnitService domainInputStockUnitService;
    protected PlotService plotService;
    protected ReferentialService referentialService;
    protected TestDatas testDatas;

    protected DomainSeedLotInputTopiaDao domainSeedLotInputDao;
    protected DomainTopiaDao domainDao;
    protected CroppingPlanEntryTopiaDao croppingPlanEntryDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected RefAnimalTypeTopiaDao refAnimalTypeDao;
    protected RefCultureMAATopiaDao refCultureMAADao;
    protected RefDestinationTopiaDao refDestinationDao;
    protected RefLegalStatusTopiaDao refLegalStatusDao;
    protected RefLocationTopiaDao refLocationDao;
    protected RefOTEXTopiaDao refOTEXDao;
    protected RefSolArvalisTopiaDao refSolArvalisDao;
    protected RefVarieteGevesTopiaDao refVarieteGevesDao;
    protected RefVarietePlantGrapeTopiaDao refVarietePlantGrapeDao;
    protected RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao;
    private RefTypeAgricultureTopiaDao refTypeAgricultureTopiaDao;

    protected TopiaJpaSupport jpaSupport;


    @BeforeEach
    public void setupServices() {
        domainService = serviceFactory.newService(DomainService.class);
        i18nService = serviceFactory.newService(AgrosystI18nService.class);
        domainInputStockUnitService = serviceFactory.newService(DomainInputStockUnitService.class);
        referentialService = serviceFactory.newService(ReferentialService.class);
        plotService = serviceFactory.newService(PlotService.class);
        loginAsAdmin();
        alterSchema();

        domainSeedLotInputDao = getPersistenceContext().getDomainSeedLotInputDao();
        domainDao = getPersistenceContext().getDomainDao();
        croppingPlanEntryDao = getPersistenceContext().getCroppingPlanEntryDao();
        effectiveInterventionDao = getPersistenceContext().getEffectiveInterventionDao();
        croppingPlanSpeciesDao = getPersistenceContext().getCroppingPlanSpeciesDao();

        refAnimalTypeDao = getPersistenceContext().getRefAnimalTypeDao();
        refEspeceDao = getPersistenceContext().getRefEspeceDao();
        refCultureMAADao = getPersistenceContext().getRefCultureMAADao();
        refDestinationDao = getPersistenceContext().getRefDestinationDao();
        refLegalStatusDao = getPersistenceContext().getRefLegalStatusDao();
        refLocationDao = getPersistenceContext().getRefLocationDao();
        refOTEXDao = getPersistenceContext().getRefOTEXDao();
        refSolArvalisDao = getPersistenceContext().getRefSolArvalisDao();
        refVarieteGevesDao = getPersistenceContext().getRefVarieteGevesDao();
        refVarietePlantGrapeDao = getPersistenceContext().getRefVarietePlantGrapeDao();
        refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        refTypeAgricultureTopiaDao = getPersistenceContext().getRefTypeAgricultureDao();

        jpaSupport = getPersistenceContext().getJpaSupport();

        testDatas = serviceFactory.newInstance(TestDatas.class);
    }

    @Test
    public void getAllDomainByDomainService() throws IOException {
        testDatas.createTestNetworks();
        List<Domain> domains = domainService.getAllDomains();
        Assertions.assertTrue(domains.size() > 0);
    }

    /**
     * Get getFilteredDomains without and with some filters.
     */
    @Test
    public void getFilterDomains() throws IOException {
        testDatas.createTestNetworks();
        // null
        PaginationResult<Domain> result = domainService.getFilteredDomains(null);
        Assertions.assertEquals(7, result.getCount());

        // empty
        DomainFilter domainFilter = new DomainFilter();
        result = domainService.getFilteredDomains(domainFilter);
        Assertions.assertEquals(7, result.getCount());

        // name filter
        domainFilter.setDomainName("Le clos");
        result = domainService.getFilteredDomains(domainFilter);
        Assertions.assertEquals(1, result.getCount());

        // name (case / accents)
        domainFilter.setDomainName("é");
        result = domainService.getFilteredDomains(domainFilter);
        Assertions.assertEquals(6, result.getCount());

        // test with navigation context
        domainFilter.setDomainName(null);

        // Set<Integer> campaigns, Set<String> networks, Set<String> domains, Set<String> growingPlans, Set<String> growingSystems
        NavigationContext context = new NavigationContext(Sets.newHashSet(2015), null, null, null, null);
        domainFilter.setNavigationContext(context);
        result = domainService.getFilteredDomains(domainFilter);
        Assertions.assertEquals(1, result.getCount());
        context = new NavigationContext(Sets.newHashSet(2014), null, null, null, null);
        domainFilter.setNavigationContext(context);
        result = domainService.getFilteredDomains(domainFilter);
        Assertions.assertEquals(5, result.getCount());
        context = new NavigationContext(Sets.newHashSet(2013), null, null, null, null);
        domainFilter.setNavigationContext(context);
        result = domainService.getFilteredDomains(domainFilter);
        Assertions.assertEquals(1, result.getCount());

        // test with navigation context domains
        context = new NavigationContext(null, null, null, null, null);
        domainFilter.setNavigationContext(context);

        Domain testDomain = result.getElements().getFirst();
        Set<String> domains = new HashSet<>();
        domains.add(testDomain.getTopiaId());
        context = new NavigationContext(null, null, domains, null, null);
        domainFilter.setNavigationContext(context);
        result = domainService.getFilteredDomains(domainFilter);
        Assertions.assertEquals(1, result.getCount());
    }

    @Test
    public void testSetDomainSpeciesToArea() {
        DomainServiceImplTest dst = new DomainServiceImplTest((DomainServiceImpl) domainService);
        Domain d = new DomainImpl();
        String fromSpeciesToArea = "{\"g20_h73__\":1.0,\"g21_h35__\":1.0,\"zcq___\":1.0}";
        d.setSpeciesToArea(fromSpeciesToArea);
        CroppingPlanEntryDto croppingPlanEntryDto = new CroppingPlanEntryDto();
        CroppingPlanSpeciesDto croppingPlanSpeciesDto1 = new CroppingPlanSpeciesDto();
        croppingPlanSpeciesDto1.setCode_espece_botanique("G20");
        croppingPlanSpeciesDto1.setCode_qualifiant_AEE("H73");
        croppingPlanSpeciesDto1.setCode_type_saisonier("");
        croppingPlanSpeciesDto1.setCode_destination_aee("");
        croppingPlanEntryDto.addSpecies(croppingPlanSpeciesDto1);
        CroppingPlanSpeciesDto croppingPlanSpeciesDto2 = new CroppingPlanSpeciesDto();
        croppingPlanSpeciesDto2.setCode_espece_botanique("G21");
        croppingPlanSpeciesDto2.setCode_qualifiant_AEE("H35");
        croppingPlanSpeciesDto2.setCode_type_saisonier("");
        croppingPlanSpeciesDto2.setCode_destination_aee("");
        croppingPlanEntryDto.addSpecies(croppingPlanSpeciesDto2);
        CroppingPlanSpeciesDto croppingPlanSpeciesDto3 = new CroppingPlanSpeciesDto();
        croppingPlanSpeciesDto3.setCode_espece_botanique("zcq");
        croppingPlanSpeciesDto3.setCode_qualifiant_AEE("");
        croppingPlanSpeciesDto3.setCode_type_saisonier("");
        croppingPlanSpeciesDto3.setCode_destination_aee("");
        croppingPlanEntryDto.addSpecies(croppingPlanSpeciesDto3);
        dst.setDomainSpeciesToArea(d, Lists.newArrayList(croppingPlanEntryDto));
        org.assertj.core.api.Assertions.assertThat(d.getSpeciesToArea()).isEqualTo(fromSpeciesToArea);
    }

    @Test
    public void testSetDomainSpeciesToAreaWithOneNotUsed() {
        DomainServiceImplTest dst = new DomainServiceImplTest((DomainServiceImpl) domainService);
        Domain d = new DomainImpl();
        String fromSpeciesToArea = "{\"g20_h73__\":1.0,\"g21_h35__\":1.0,\"zcq___\":1.0}";
        String toSpeciesToArea = "{\"g20_h73__\":1.0,\"g21_h35__\":1.0}";
        d.setSpeciesToArea(fromSpeciesToArea);
        CroppingPlanEntryDto croppingPlanEntryDto = new CroppingPlanEntryDto();
        CroppingPlanSpeciesDto croppingPlanSpeciesDto1 = new CroppingPlanSpeciesDto();
        croppingPlanSpeciesDto1.setCode_espece_botanique("G20");
        croppingPlanSpeciesDto1.setCode_qualifiant_AEE("H73");
        croppingPlanSpeciesDto1.setCode_type_saisonier("");
        croppingPlanSpeciesDto1.setCode_destination_aee("");
        croppingPlanEntryDto.addSpecies(croppingPlanSpeciesDto1);
        CroppingPlanSpeciesDto croppingPlanSpeciesDto2 = new CroppingPlanSpeciesDto();
        croppingPlanSpeciesDto2.setCode_espece_botanique("G21");
        croppingPlanSpeciesDto2.setCode_qualifiant_AEE("H35");
        croppingPlanSpeciesDto2.setCode_type_saisonier("");
        croppingPlanSpeciesDto2.setCode_destination_aee("");
        croppingPlanEntryDto.addSpecies(croppingPlanSpeciesDto2);
        dst.setDomainSpeciesToArea(d, Lists.newArrayList(croppingPlanEntryDto));
        org.assertj.core.api.Assertions.assertThat(d.getSpeciesToArea()).isEqualTo(toSpeciesToArea);
    }

    @Test
    public void testSetDomainSpeciesToAreaCropRemoved() {
        DomainServiceImplTest dst = new DomainServiceImplTest((DomainServiceImpl) domainService);
        Domain d = new DomainImpl();
        String fromSpeciesToArea = "{\"g20_h73__\":1.0,\"g21_h35__\":1.0,\"zcq___\":1.0}";
        String toSpeciesToArea = "{}";
        d.setSpeciesToArea(fromSpeciesToArea);
        CroppingPlanEntryDto croppingPlanEntryDto = new CroppingPlanEntryDto();
        dst.setDomainSpeciesToArea(d, Lists.newArrayList(croppingPlanEntryDto));
        org.assertj.core.api.Assertions.assertThat(d.getSpeciesToArea()).isEqualTo(toSpeciesToArea);
    }


    /**
     * Test la duplication en incluant les dispositifs et les systemes de cultures.
     */
    @Test
    public void testExtendDomains() throws Exception {
        testDatas.createTestNetworks();
        // get database TopiaDao
        DomainTopiaDao domainTopiaDao = getPersistenceContext().getDomainDao();
        GrowingPlanTopiaDao growingPlanTopiaDao = getPersistenceContext().getGrowingPlanDao();
        GrowingSystemTopiaDao growingSystemTopiaDao = getPersistenceContext().getGrowingSystemDao();
        NetworkTopiaDao networkTopiaDao = getPersistenceContext().getNetworkDao();

        // count networks
        String gsWithNetworks = "Systeme de culture Baulon 1";
        List<GrowingSystem> originalGS = growingSystemTopiaDao.forNameEquals(gsWithNetworks).findAll();
        Assertions.assertEquals(1, originalGS.size());
        Assertions.assertEquals(2, originalGS.getFirst().getNetworks().size());

        // test some counts
        Assertions.assertEquals(7, domainTopiaDao.count());
        Assertions.assertEquals(5, growingPlanTopiaDao.count());
        Assertions.assertEquals(7, growingSystemTopiaDao.count());
        Assertions.assertEquals(6, networkTopiaDao.count());

        // search domain to clone
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        List<Domain> domains2013 = domainService.getFilteredDomains(domainFilter).getElements();
        Assertions.assertEquals(1, domains2013.size());
        Domain firstDomain = domains2013.getFirst();
        Assertions.assertEquals(1, domainService.getRelatedDomains(firstDomain.getCode()).size());

        testDatas.createInputStorage(firstDomain);
        final Map<InputType, List<AbstractDomainInputStockUnit>> sourceDomainInputStockUnit = domainInputStockUnitService.loadDomainInputStock(firstDomain);
        // deep duplicated domain, growing plans and, growing systems
        Assertions.assertTrue(firstDomain.isActive());
        Domain extendedDomain = domainService.extendDomain(firstDomain.getTopiaId(), 2016);
        Assertions.assertTrue(extendedDomain.isActive());

        // test some counts after extends
        Assertions.assertEquals(8, domainTopiaDao.count());
        Assertions.assertEquals(6, growingPlanTopiaDao.count());
        Assertions.assertEquals(9, growingSystemTopiaDao.count());
        Assertions.assertEquals(2, domainService.getRelatedDomains(firstDomain.getCode()).size());
        Assertions.assertEquals(6, networkTopiaDao.count());

        GrowingPlanService growingPlanService = serviceFactory.newService(GrowingPlanService.class);
        List<GrowingPlan> extendedGrowingPlans = growingPlanService.findAllByDomain(extendedDomain);
        Assertions.assertEquals(1, extendedGrowingPlans.size());
        GrowingPlan extendedGP = extendedGrowingPlans.getFirst();
        LinkedHashMap<Integer, String> relatedGPs = growingPlanService.getRelatedGrowingPlans(extendedGP.getCode());
        Assertions.assertNotNull(relatedGPs);
        Assertions.assertEquals(2, relatedGPs.size());

        GrowingSystemService growingSystemService = serviceFactory.newService(GrowingSystemService.class);
        List<GrowingSystem> allGrowingSystems = growingSystemService.findAllByGrowingPlan(extendedGP);
        Assertions.assertNotNull(allGrowingSystems);
        Assertions.assertEquals(2, allGrowingSystems.size());
        GrowingSystem firstGs = allGrowingSystems.getFirst();
        LinkedHashMap<Integer, String> extendedGrowingSystems = growingSystemService.getRelatedGrowingSystemIdsByCampaigns(firstGs.getCode());
        Assertions.assertNotNull(extendedGrowingSystems);
        Assertions.assertEquals(2, extendedGrowingSystems.size());

        // network unchanged (non duplicated)
        List<GrowingSystem> duplicatedGrowingSystems = growingSystemTopiaDao.forNameEquals(gsWithNetworks).findAll();
        Assertions.assertEquals(2, duplicatedGrowingSystems.size());
        for (GrowingSystem duplicatedGrowingSystem : duplicatedGrowingSystems) {
            Assertions.assertEquals(2, duplicatedGrowingSystem.getNetworks().size());
        }

        // test weather stations
        Assertions.assertEquals(2, extendedDomain.getWeatherStations().size());
        Assertions.assertTrue(extendedDomain.getWeatherStations().contains(extendedDomain.getDefaultWeatherStation()));

        String fdc = firstDomain.getCode();
        List<Domain> allDomainsWithFdcCode = domainTopiaDao.forCodeEquals(fdc).findAll();
        Set<Integer> campaigns = Sets.newHashSet();
        for (Domain domain : allDomainsWithFdcCode) {
            campaigns.add(domain.getCampaign());
        }
        List<String> domainIds = domainService.getDomainIdsForGrowingSystemAndCampaigns(firstGs.getTopiaId(), campaigns, false);
        Assertions.assertEquals(allDomainsWithFdcCode.size(), domainIds.size());

        Map<InputType, List<AbstractDomainInputStockUnit>> extendedDomainInputStockUnit = domainInputStockUnitService.loadDomainInputStock(extendedDomain);
        for (InputType inputType : InputType.values()) {

            ArrayList<AbstractDomainInputStockUnit> sourceDomainInputStockUnits = new ArrayList<>(ListUtils.emptyIfNull(sourceDomainInputStockUnit.get(inputType)));
            ArrayList<AbstractDomainInputStockUnit> extendDomainInputStockUnits = new ArrayList<>(ListUtils.emptyIfNull(extendedDomainInputStockUnit.get(inputType)));

            if (CollectionUtils.isEmpty(sourceDomainInputStockUnits)) {
                Assertions.assertTrue(CollectionUtils.isEmpty(extendDomainInputStockUnits));
                continue;
            }
            if (InputType.SEMIS.equals(inputType)) {
                sourceDomainInputStockUnits = new ArrayList<>(sourceDomainInputStockUnits.stream().filter(sdid -> sdid instanceof DomainSeedLotInput).toList());
                extendDomainInputStockUnits = new ArrayList<>(extendDomainInputStockUnits.stream().filter(sdid -> sdid instanceof DomainSeedLotInput).toList());
            }
            org.assertj.core.api.Assertions.assertThat(sourceDomainInputStockUnits.size()).isEqualTo(extendDomainInputStockUnits.size());

            sourceDomainInputStockUnits.sort(Comparator.comparing(AbstractDomainInputStockUnit::getInputName));
            extendDomainInputStockUnits.sort(Comparator.comparing(AbstractDomainInputStockUnit::getInputName));
            for (int i = 0; i < sourceDomainInputStockUnits.size(); i++) {
                AbstractDomainInputStockUnit sourceDomainInput = sourceDomainInputStockUnits.get(i);
                AbstractDomainInputStockUnit extendDomainInput = extendDomainInputStockUnits.get(i);

                Assertions.assertEquals(0, ABSTRACT_DOMAIN_INPUT_COMPARATOR.compare(sourceDomainInput, extendDomainInput));

                switch (inputType) {
                    case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                        final DomainMineralProductInput sourceDomainProductInput = (DomainMineralProductInput) sourceDomainInput;
                        final DomainMineralProductInput extendDomainProductInput = (DomainMineralProductInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                        Assertions.assertEquals(sourceDomainProductInput.getRefInput(), extendDomainProductInput.getRefInput());
                    }
                    case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE -> {
                        final DomainPhytoProductInput sourceDomainProductInput = (DomainPhytoProductInput) sourceDomainInput;
                        final DomainPhytoProductInput extendDomainProductInput = (DomainPhytoProductInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                        Assertions.assertEquals(sourceDomainProductInput.getRefInput(), extendDomainProductInput.getRefInput());
                        Assertions.assertEquals(sourceDomainProductInput.getProductType(), extendDomainProductInput.getProductType());
                    }
                    case AUTRE -> {
                        final DomainOtherInput sourceDomainProductInput = (DomainOtherInput) sourceDomainInput;
                        final DomainOtherInput extendDomainProductInput = (DomainOtherInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                        Assertions.assertEquals(sourceDomainProductInput.getRefInput(), extendDomainProductInput.getRefInput());
                    }
                    case CARBURANT -> {
                        final DomainFuelInput sourceDomainProductInput = (DomainFuelInput) sourceDomainInput;
                        final DomainFuelInput extendDomainProductInput = (DomainFuelInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                    }
                    case EPANDAGES_ORGANIQUES -> {
                        final DomainOrganicProductInput sourceDomainProductInput = (DomainOrganicProductInput) sourceDomainInput;
                        final DomainOrganicProductInput extendDomainProductInput = (DomainOrganicProductInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                        Assertions.assertEquals(sourceDomainProductInput.getRefInput(), extendDomainProductInput.getRefInput());
                    }
                    case IRRIGATION -> {
                        final DomainIrrigationInput sourceDomainProductInput = (DomainIrrigationInput) sourceDomainInput;
                        final DomainIrrigationInput extendDomainProductInput = (DomainIrrigationInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                    }
                    case PLAN_COMPAGNE, SEMIS -> {
                        if (sourceDomainInput instanceof DomainSeedLotInput sourceDomainProductInput) {
                            final DomainSeedLotInput extendDomainProductInput = (DomainSeedLotInput) extendDomainInput;
                            Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                            Assertions.assertEquals(sourceDomainProductInput.isOrganic(), extendDomainProductInput.isOrganic());
                            final Collection<DomainSeedSpeciesInput> domainSeedSpeciesInput = sourceDomainProductInput.getDomainSeedSpeciesInput();
                            final ImmutableMap<String, DomainSeedSpeciesInput> sourceDomainSpeciesInputByCodes = Maps.uniqueIndex(sourceDomainProductInput.getDomainSeedSpeciesInput(), DomainSeedSpeciesInput::getCode);

                            final ImmutableMap<String, DomainSeedSpeciesInput> extendedDomainSpeciesInputByCodes = Maps.uniqueIndex(extendDomainProductInput.getDomainSeedSpeciesInput(), DomainSeedSpeciesInput::getCode);
                            Assertions.assertFalse(sourceDomainSpeciesInputByCodes.isEmpty());
                            Assertions.assertFalse(extendedDomainSpeciesInputByCodes.isEmpty());

                            for (Map.Entry<String, DomainSeedSpeciesInput> sourceDomainSpeciesInputForCode : sourceDomainSpeciesInputByCodes.entrySet()) {
                                final String code = sourceDomainSpeciesInputForCode.getKey();
                                final DomainSeedSpeciesInput sourceSpeciesInput = sourceDomainSpeciesInputForCode.getValue();
                                final DomainSeedSpeciesInput extendedSpeciesInput = extendedDomainSpeciesInputByCodes.get(code);

                                Assertions.assertEquals(sourceSpeciesInput.isBiologicalSeedInoculation(), extendedSpeciesInput.isBiologicalSeedInoculation());
                                Assertions.assertEquals(sourceSpeciesInput.isChemicalTreatment(), extendedSpeciesInput.isChemicalTreatment());
                                Assertions.assertEquals(sourceSpeciesInput.isOrganic(), extendedSpeciesInput.isOrganic());

                                final Collection<DomainPhytoProductInput> sourceSpeciesPhytoInputs = sourceSpeciesInput.getSpeciesPhytoInputs();
                                final Collection<DomainPhytoProductInput> extendedSpeciesPhytoInputs = extendedSpeciesInput.getSpeciesPhytoInputs();

                                Assertions.assertFalse(sourceSpeciesPhytoInputs.isEmpty());
                                Assertions.assertEquals(sourceSpeciesPhytoInputs.size(), extendedSpeciesPhytoInputs.size());

                                final ImmutableMap<String, DomainPhytoProductInput> sourceSpeciesPhytoInputByCodes = Maps.uniqueIndex(sourceSpeciesPhytoInputs, DomainPhytoProductInput::getCode);
                                final ImmutableMap<String, DomainPhytoProductInput> extendedSpeciesPhytoInputByCodes = Maps.uniqueIndex(extendedSpeciesPhytoInputs, DomainPhytoProductInput::getCode);

                                for (Map.Entry<String, DomainPhytoProductInput> sourceSpeciesPhytoInputForCode : sourceSpeciesPhytoInputByCodes.entrySet()) {
                                    final String phytoCode = sourceSpeciesPhytoInputForCode.getKey();
                                    final DomainPhytoProductInput sourceDomainPhytoProductInput = sourceSpeciesPhytoInputForCode.getValue();
                                    final DomainPhytoProductInput extendedDomainPhytoProductInput = extendedSpeciesPhytoInputByCodes.get(phytoCode);

                                    Assertions.assertNotNull(extendedDomainPhytoProductInput);
                                    Assertions.assertEquals(sourceDomainPhytoProductInput.getUsageUnit(), extendedDomainPhytoProductInput.getUsageUnit());
                                    Assertions.assertEquals(sourceDomainPhytoProductInput.getRefInput(), extendedDomainPhytoProductInput.getRefInput());
                                    Assertions.assertEquals(sourceDomainPhytoProductInput.getProductType(), extendedDomainPhytoProductInput.getProductType());
                                }

                            }

                            Assertions.assertEquals(CollectionUtils.size(domainSeedSpeciesInput), CollectionUtils.size(extendDomainProductInput.getDomainSeedSpeciesInput()));

                        }
                    }
                    case POT -> {
                        final DomainPotInput sourceDomainProductInput = (DomainPotInput) sourceDomainInput;
                        final DomainPotInput extendDomainProductInput = (DomainPotInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                        Assertions.assertEquals(sourceDomainProductInput.getRefInput(), extendDomainProductInput.getRefInput());
                    }
                    case SUBSTRAT -> {
                        final DomainSubstrateInput sourceDomainProductInput = (DomainSubstrateInput) sourceDomainInput;
                        final DomainSubstrateInput extendDomainProductInput = (DomainSubstrateInput) extendDomainInput;
                        Assertions.assertEquals(sourceDomainProductInput.getUsageUnit(), extendDomainProductInput.getUsageUnit());
                        Assertions.assertEquals(sourceDomainProductInput.getRefInput(), extendDomainProductInput.getRefInput());
                    }
                }
            }
        }
        //extendedDomain
    }

    protected static final Comparator<AbstractDomainInputStockUnit> ABSTRACT_DOMAIN_INPUT_COMPARATOR = (di1, di2) -> {

        boolean result = di1.getInputName().equals(di2.getInputName())
                && di1.getInputKey().equals(di2.getInputKey())
                && di1.getDomain().getCode().equals(di2.getDomain().getCode())
                && (di1.getDomain().getCampaign() != di2.getDomain().getCampaign())
                && di1.getInputName().equals(di2.getInputName())
                && di1.getInputType().equals(di2.getInputType());

        final InputPrice di1InputPrice = di1.getInputPrice();
        final InputPrice di2InputPrice = di2.getInputPrice();
        if (di1InputPrice != null) {
            result = result && (di2InputPrice != null);

        }

        return result ? 0 : 1;
    };

    /**
     * Test domain extend does not duplicate eDaplosIssuerId.
     */
    @Test
    public void testPlotDuplicate() throws Exception {
        testDatas.createTestPlots();

        // search domain to clone
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("Baulon");
        domainFilter.setCampaign(2013);
        List<Domain> domains2013 = domainService.getFilteredDomains(domainFilter).getElements();
        Domain domainBaulon2013 = domains2013.getFirst();
        // Check plot
        List<Plot> plots = plotService.findAllForDomain(domainBaulon2013);
        Map<String, String> growingSystemCodeByPlotCode = new HashMap<>();
        for (Plot plot : plots) {
            final GrowingSystem growingSystem = plot.getGrowingSystem();
            String growingSystemCode = growingSystem == null ? null : growingSystem.getCode();
            growingSystemCodeByPlotCode.put(plot.getCode(), growingSystemCode);
        }

        Assertions.assertEquals(4, plots.size());
        for (Plot plot2013 : plots) {
            Assertions.assertNotNull(plot2013.geteDaplosIssuerId());
        }

        // deep duplicated domain, growing plans and, growing systems
        Assertions.assertTrue(domainBaulon2013.isActive());
        Domain domainBaulon2016 = domainService.extendDomain(domainBaulon2013.getTopiaId(), 2016);
        Assertions.assertTrue(domainBaulon2016.isActive());

        // assert new plots have not eDaplosIssuerId
        plots = plotService.findAllForDomain(domainBaulon2016);

        Map<String, Plot> plotByCode = plots.stream().collect(Collectors.toMap(Plot::getCode, Function.identity()));
        for (Map.Entry<String, String> plotCodeToGrowingSystemCode : growingSystemCodeByPlotCode.entrySet()) {
            String plotCode = plotCodeToGrowingSystemCode.getKey();
            Plot duplicatedPlot = plotByCode.get(plotCode);
            Assertions.assertNotNull(duplicatedPlot);

            GrowingSystem duplicatedGrowingSystem = duplicatedPlot.getGrowingSystem();

            final String growingSystemCodeValue = plotCodeToGrowingSystemCode.getValue();
            if (growingSystemCodeValue == null) {
                Assertions.assertNull(duplicatedGrowingSystem);
            } else {
                Assertions.assertNotNull(duplicatedGrowingSystem);
                Assertions.assertEquals(growingSystemCodeValue, duplicatedGrowingSystem.getCode());
            }
        }

        Assertions.assertEquals(4, plots.size());
        // Check eDaplosIssuerId has not been deleted
        for (Plot plot2016 : plots) {
            Assertions.assertNull(plot2016.geteDaplosIssuerId());
        }
        // assert old plots still have eDaplosIssuerId
        plots = plotService.findAllForDomain(domainBaulon2013);
        for (Plot plot2013 : plots) {
            Assertions.assertNotNull(plot2013.geteDaplosIssuerId());
        }

    }

    /**
     * Test que la duplication sur une campagne existantes renvoie une exception.
     */
    @Test
    public void testExtendDomainsFail() throws Exception {
        testDatas.createTestNetworks();
        // search domain to clone
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("La Bouinelière");
        domainFilter.setCampaign(2014);
        List<Domain> domains = domainService.getFilteredDomains(domainFilter).getElements();
        Assertions.assertEquals(1, domains.size());
        Domain firstDomain = domains.getFirst();

        // deep duplicated domain, growing plans and, growing systems
        Assertions.assertThrows(DomainExtendException.class, () ->
                domainService.extendDomain(firstDomain.getTopiaId(), 2014)
        );
    }

    /**
     * Test de duplique un domaine sur plusieurs années et que la recherche
     * des domaines lié les retrouve bien.
     *
     * @throws DomainExtendException
     */
    @Test
    public void testRelatedDomains() throws DomainExtendException, IOException {
        testDatas.createTestNetworks();
        // search domain to clone
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("La Bouinelière");
        domainFilter.setCampaign(2014);
        List<Domain> domains = domainService.getFilteredDomains(domainFilter).getElements();
        Domain firstDomain = domains.getFirst();

        // extends domains
        domainService.extendDomain(firstDomain.getTopiaId(), 2015);
        domainService.extendDomain(firstDomain.getTopiaId(), 2016);

        // test related
        LinkedHashMap<Integer, String> relatedDomains = domainService.getRelatedDomains(firstDomain.getCode());
        Assertions.assertEquals(3, relatedDomains.size());
        Assertions.assertTrue(relatedDomains.containsValue(firstDomain.getTopiaId()));
    }

    /**
     * Test qu'un nom de domaine est déjà utilisé.
     */
    @Test
    public void testCheckDomainExistence() throws IOException {
        testDatas.createTestNetworks();
        Assertions.assertTrue(domainService.checkDomainExistence("Le clos Gautier"));
        Assertions.assertTrue(domainService.checkDomainExistence("le clos gautier"));
        Assertions.assertTrue(domainService.checkDomainExistence("Le clos gAutier"));
        Assertions.assertFalse(domainService.checkDomainExistence("Le clos Gatier"));
        Assertions.assertFalse(domainService.checkDomainExistence("Le clos gatier"));
    }

    /**
     * This test reproduces issue https://forge.codelutin.com/issues/3211 :
     * Impossible de supprimer une culture dans l'assolement d'un domaine
     */
    @Test
    public void testCreateUpdateRemoveCroppingPlanEntry() throws IOException {
        // import required by service:
        testDatas.createTestNetworks();
        testDatas.importEspecesToVarietes(true);
        testDatas.importVarieteGeves();

        final RefEspece espece = refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        final RefVariete josselin = refVarieteGevesDao.forDenominationEquals("Josselin").findUnique();

        // tests
        Domain domain = domainService.getAllDomains().getFirst();

        String expectedName;

        // TEST CREATE
        {
            List<CroppingPlanEntryDto> croppingPlan = new ArrayList<>();
            CroppingPlanEntryDto croppingPlanEntryDto = new CroppingPlanEntryDto();
            croppingPlanEntryDto.setName("Azerty");
            croppingPlanEntryDto.setType(CroppingEntryType.MAIN);
            croppingPlan.add(croppingPlanEntryDto);
            ArrayList<CroppingPlanSpeciesDto> species = new ArrayList<>();
            CroppingPlanSpeciesDto croppingPlanSpeciesDto = new CroppingPlanSpeciesDto();

            expectedName = espece.getLibelle_espece_botanique();
            croppingPlanSpeciesDto.setSpeciesId(espece.getTopiaId());
            croppingPlanSpeciesDto.setVarietyId(josselin.getTopiaId());
            species.add(croppingPlanSpeciesDto);
            croppingPlanEntryDto.setSpecies(species);
            domain = domainService.createOrUpdateDomain(
                    domain,
                    domain.getLocation().getTopiaId(),
                    null,
                    null,
                    croppingPlan,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    new ArrayList<>());
        }

        {
            UsageList<CroppingPlanEntryDto> croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());
            List<CroppingPlanEntryDto> croppingPlan = new ArrayList<>(croppingPlanEntryDtoAndUsage.getElements());

            Assertions.assertEquals(1, croppingPlan.size());
            CroppingPlanEntryDto croppingPlanEntry = croppingPlan.getFirst();
            Assertions.assertEquals("Azerty", croppingPlanEntry.getName());
            List<CroppingPlanSpeciesDto> croppingPlanSpeciesList = croppingPlanEntry.getSpecies();
            Assertions.assertEquals(1, croppingPlanSpeciesList.size());
            CroppingPlanSpeciesDto croppingPlanSpecies = croppingPlanSpeciesList.iterator().next();
            Assertions.assertEquals(expectedName, croppingPlanSpecies.getSpeciesEspece());
            Assertions.assertEquals(espece.getTopiaId(), croppingPlanSpecies.getSpeciesId());
            Assertions.assertEquals(josselin.getTopiaId(), croppingPlanSpecies.getVarietyId());

            // TEST UPDATE
            domain = domainService.createOrUpdateDomain(
                    domain,
                    domain.getLocation().getTopiaId(),
                    null,
                    null,
                    croppingPlan,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    new ArrayList<>());
        }

        {
            UsageList<CroppingPlanEntryDto> croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());
            List<CroppingPlanEntryDto> croppingPlan = croppingPlanEntryDtoAndUsage.getElements();

            Assertions.assertEquals(1, croppingPlan.size());
            CroppingPlanEntryDto croppingPlanEntry = croppingPlan.getFirst();
            Assertions.assertEquals("Azerty", croppingPlanEntry.getName());
            List<CroppingPlanSpeciesDto> croppingPlanSpeciesList = croppingPlanEntry.getSpecies();
            Assertions.assertEquals(1, croppingPlanSpeciesList.size());
            CroppingPlanSpeciesDto croppingPlanSpecies = croppingPlanSpeciesList.iterator().next();
            Assertions.assertEquals(expectedName, croppingPlanSpecies.getSpeciesEspece());
            Assertions.assertEquals(espece.getTopiaId(), croppingPlanSpecies.getSpeciesId());
            Assertions.assertEquals(josselin.getTopiaId(), croppingPlanSpecies.getVarietyId());
        }

        // TEST DELETE
        {
            List<CroppingPlanEntryDto> emptyCroppingPlan = new ArrayList<>();
            domainService.createOrUpdateDomain(
                    domain,
                    domain.getLocation().getTopiaId(),
                    null,
                    null,
                    emptyCroppingPlan,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    new ArrayList<>());
        }

        {
            UsageList<CroppingPlanEntryDto> croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());
            List<CroppingPlanEntryDto> croppingPlan = croppingPlanEntryDtoAndUsage.getElements();

            Assertions.assertEquals(0, croppingPlan.size());
        }
    }

    /**
     * Test domains xls modèle export.
     *
     * @throws IOException
     */
    @Test
    public void testExportXlsEmpty() throws IOException {

        ExportResult exportResult = domainService.exportDomainAsXls(null);
        BinaryContent content = exportResult.content();
        try (InputStream is = content.toInputStream()) {
            Assertions.assertTrue(is.available() > 1);
        }
    }

    /**
     * Test domains xls export.
     */
    @Test
    public void testExportXls() {
        // empty filter
        try {
            //FileUtil.createDirectoryIfNecessary(new File(ImportServiceImpl.JAVA_IO_TMPDIR));
            DomainFilter domainFilter = new DomainFilter();
            List<Domain> domains = domainService.getFilteredDomains(domainFilter).getElements();
            List<String> domainIds = domains.stream().map(Entities.GET_TOPIA_ID).collect(Collectors.toList());
            ExportResult exportResult = domainService.exportDomainAsXls(domainIds);
            BinaryContent content = exportResult.content();
            try (InputStream is = content.toInputStream()) {
                File file = File.createTempFile("export", "xlsx");
                file.deleteOnExit();
                FileUtils.copyInputStreamToFile(is, file);

                LOGGER.info("fr.inra.agrosyst.services.domain.DomainServiceTest.testExportXls output: " + file.getAbsolutePath());
                Assertions.assertNotNull(file);
            } catch (Exception e) {
                Assertions.fail("Import failed", e);
            }
        } catch (Exception e) {
            Assertions.fail("Import failed", e);
        }
    }

    /**
     * Test que la recherche case et accents insensitive fonctionne avec plusieurs accents.
     */
    @Test
    public void testMultipleAccentInDomainName() throws IOException {
        // location required for test
        testDatas.importCommunesFrance();

        RefLocation location = referentialService.getActiveCommunes("01500").getFirst();
        Domain d1 = domainService.newDomain();
        d1.setCampaign(2014);
        d1.setName("L'Inra a été fondé en 1946. Il contribuer à l'expertise, à la formation, " +
                "à la promotion de la culture scientifique et technique, au débat science/société");
        d1.setMainContact("test");
        d1.setType(DomainType.DOMAINE_EXPERIMENTAL);
        domainService.createOrUpdateDomain(
                d1,
                location.getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("fonde");
        Assertions.assertEquals(1, domainService.getFilteredDomains(domainFilter).getCount());

        domainFilter.setDomainName("a la p");
        Assertions.assertEquals(1, domainService.getFilteredDomains(domainFilter).getCount());
    }

    @Test
    public void testValidateOnDeprecatedMethodes() throws Exception {
        testDatas.createTestNetworks();

        Domain domain = domainService.getAllDomains().iterator().next();
        Assertions.assertFalse(domain.isValidated());
        Assertions.assertNull(domain.getValidationDate());
        Assertions.assertNotNull(domain.getUpdateDate());

        String domainId = domain.getTopiaId();

        List<CroppingPlanEntryDto> list = new ArrayList<>();
        CroppingPlanEntryDto dto = new CroppingPlanEntryDto();
        dto.setName("azerty");
        dto.setType(CroppingEntryType.MAIN);
        list.add(dto);
        CroppingPlanSpeciesDto speciesDto = new CroppingPlanSpeciesDto();
        RefEspece espece = referentialService.findSpecies("g").getFirst();
        speciesDto.setSpeciesId(espece.getTopiaId());
        ArrayList<CroppingPlanSpeciesDto> speciesList = Lists.newArrayList(speciesDto);
        dto.setSpecies(speciesList);
        domainService.createOrUpdateDomain(
                domain,
                domain.getLocation().getTopiaId(),
                null,
                null,
                list,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

        domain = domainService.validateAndCommit(domainId);
        Assertions.assertTrue(domain.isValidated());
        Assertions.assertNotNull(domain.getValidationDate());
        Assertions.assertEquals(domain.getUpdateDate(), domain.getValidationDate());

        UsageList<CroppingPlanEntryDto> croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());
        List<CroppingPlanEntryDto> croppingPlan = croppingPlanEntryDtoAndUsage.getElements();

        for (CroppingPlanEntryDto croppingPlanEntryDto : croppingPlan) {
            Assertions.assertTrue(croppingPlanEntryDto.isValidated());
            for (CroppingPlanSpeciesDto croppingPlanSpeciesDto : croppingPlanEntryDto.getSpecies()) {
                Assertions.assertTrue(croppingPlanSpeciesDto.isValidated());
            }
        }

    }

    @Test
    public void testValidate() throws Exception {
        testDatas.createTestNetworks();

        Domain domain = domainService.getAllDomains().iterator().next();
        Assertions.assertFalse(domain.isValidated());
        Assertions.assertNull(domain.getValidationDate());
        Assertions.assertNotNull(domain.getUpdateDate());

        String domainId = domain.getTopiaId();

        List<CroppingPlanEntryDto> croppingPlanEntryDtos = new ArrayList<>();
        CroppingPlanEntry crop = new CroppingPlanEntryImpl();
        crop.setName("azerty");
        crop.setType(CroppingEntryType.MAIN);

        CroppingPlanSpecies croppingPlanSpecies = new CroppingPlanSpeciesImpl();
        RefEspece espece = referentialService.findSpecies("g").getFirst();
        croppingPlanSpecies.setSpecies(espece);
        croppingPlanSpecies.setCroppingPlanEntry(crop);
        crop.addCroppingPlanSpecies(croppingPlanSpecies);

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);
        final CroppingPlanEntryDto croppingPlanEntryDto = CroppingPlans.getDtoForCroppingPlanEntry(crop, translationMap);
        croppingPlanEntryDtos.add(croppingPlanEntryDto);

        InputPriceDto priceDto = InputPriceDto.builder()
                .category(InputPriceCategory.SEEDING_INPUT)
                .objectId("FAKE")// will be change later on the test
                .displayName("FAKE")
                .sourceUnit(SeedPlantUnit.KG_PAR_HA.name())
                .price(0.4)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .seedType(SeedType.SEMENCES_DE_FERME)
                .biologicalSeedInoculation(false)
                .chemicalTreatment(false)
                .includedTreatment(false)
                .organic(true).build();

        Collection<DomainSeedSpeciesInputDto> domainSeedSpeciesInputDtos = getCropSpeciesSeedSpeciesInputDtos(
                crop.getCroppingPlanSpecies(), priceDto);
        DomainSeedLotInputDto lotDto = DomainInputStockServiceTest.getDomainSeedLotInputDto(
                croppingPlanEntryDto, domainSeedSpeciesInputDtos, true);

        Collection<DomainInputDto> domainInputStockUnits = Lists.newArrayList(lotDto);

        domainService.createOrUpdateDomain(
                domain,
                domain.getLocation().getTopiaId(),
                null,
                null,
                croppingPlanEntryDtos,
                null,
                null,
                null,
                null,
                null,
                null,
                domainInputStockUnits, new ArrayList<>());

        domain = domainService.validateAndCommit(domainId);
        Assertions.assertTrue(domain.isValidated());
        Assertions.assertNotNull(domain.getValidationDate());
        Assertions.assertEquals(domain.getUpdateDate(), domain.getValidationDate());

        UsageList<CroppingPlanEntryDto> croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());
        List<CroppingPlanEntryDto> croppingPlanDtos = croppingPlanEntryDtoAndUsage.getElements();

        for (CroppingPlanEntryDto croppingPlanEntryDto_ : croppingPlanDtos) {
            Assertions.assertTrue(croppingPlanEntryDto_.isValidated());
            for (CroppingPlanSpeciesDto croppingPlanSpeciesDto : croppingPlanEntryDto_.getSpecies()) {
                Assertions.assertTrue(croppingPlanSpeciesDto.isValidated());
            }
        }

    }

    /**
     * Il y a un problème potentiel lors de la suppression en meme temps d'un
     * materiel et d'un attelage. Si les materiels sont supprimmés par cascade avant les
     * attelage, une violation de contraintes est lancés.
     * Pour contourner cela, l'ordre des relation a été changé dans le mapping, et les suppression
     * se passe bien.
     * Ce test test la non regression de ce point.
     */
    @Test
    public void testMaterielAndToolsCouplingDeletion() throws IOException {
        // import required by tests
        testDatas.createTestNetworks();
        testDatas.importMateriels();
        testDatas.importInterventionAgrosystTravailEdi();

        // get domain
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("La Bouinelière");
        domainFilter.setCampaign(2014);
        List<Domain> domains = domainService.getFilteredDomains(domainFilter).getElements();
        Domain firstDomain = domains.getFirst();

        RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        List<RefInterventionAgrosystTravailEDI> refInterventionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.findAll();

        Set<RefInterventionAgrosystTravailEDI> mainsActions = Sets.newHashSet(refInterventionAgrosystTravailEDIs.get(0), refInterventionAgrosystTravailEDIs.get(1), refInterventionAgrosystTravailEDIs.get(2));
        // create tool coupling
        ToolsCoupling attelage = new ToolsCouplingImpl();
        attelage.setCode(UUID.randomUUID().toString());
        attelage.setToolsCouplingName("Test");
        attelage.setMainsActions(mainsActions);

        // link to equipment 1
        Equipment tractor = new EquipmentImpl();
        tractor.setName("tractor");
        tractor.setTopiaId("NEW-EQUIPMENT-" + 1);
        attelage.setTractor(tractor);

        Equipment coupling = new EquipmentImpl();
        coupling.setName("coupling");
        coupling.setTopiaId("NEW-EQUIPMENT-" + 2);
        attelage.addEquipments(coupling);

        List<Equipment> materiels = Lists.newArrayList(tractor, coupling);
        // test update to ADD materiel and tools coupling 
        firstDomain = domainService.createOrUpdateDomain(
                firstDomain,
                firstDomain.getLocation().getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                materiels,
                Collections.singletonList(attelage),
                null,
                null,
                null
        );

        long from = domainService.getEquipments(firstDomain.getTopiaId()).size();
        Assertions.assertEquals(2L, from);

        // test to delete materiel and tools coupling
        // test regresion for bug #4338
        domainService.createOrUpdateDomain(firstDomain, firstDomain.getLocation().getTopiaId(), null,
                null, null, null, null, null, new ArrayList<>(), new ArrayList<>(), null, null, null);
        long to = domainService.getEquipments(firstDomain.getTopiaId()).size();
        Assertions.assertEquals(0L, to);
    }

    /**
     * In this test the tools coupling is not valid as it is compose of a tractor that should be coupled with an equipment
     * at it is not exception must be throw up from service
     */
    @Test
    public void testMaterielAndToolsCouplingNotValidToolsCoupling() throws IOException {
        testDatas.createTestNetworks();
        // import required by tests
        testDatas.importMateriels();
        testDatas.importInterventionAgrosystTravailEdi();

        // get domain
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("La Bouinelière");
        domainFilter.setCampaign(2014);
        List<Domain> domains = domainService.getFilteredDomains(domainFilter).getElements();
        Domain firstDomain = domains.getFirst();

        // create materiel and
        Equipment mat = new EquipmentImpl();
        mat.setTopiaId("NEW-EQUIPMENT-" + 1);
        mat.setCode(UUID.randomUUID().toString());
        mat.setName("Test");
        RefMateriel refMateriel = getTractorVigne();
        mat.setRefMateriel(refMateriel);

        RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        List<RefInterventionAgrosystTravailEDI> refInterventionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.findAll();

        Set<RefInterventionAgrosystTravailEDI> mainsActions = Sets.newHashSet(refInterventionAgrosystTravailEDIs.get(0), refInterventionAgrosystTravailEDIs.get(1), refInterventionAgrosystTravailEDIs.get(2));
        // create tool coupling
        ToolsCoupling attelage = new ToolsCouplingImpl();
        attelage.setCode(UUID.randomUUID().toString());
        attelage.setToolsCouplingName("Test");
        attelage.setMainsActions(mainsActions);

        // link to equipment 1
        Equipment tractor = new EquipmentImpl();
        tractor.setTopiaId("NEW-EQUIPMENT-" + 1);
        attelage.setTractor(tractor);


        // test update to ADD materiel and tools coupling
        Assertions.assertThrows(RuntimeException.class, () ->
                domainService.createOrUpdateDomain(
                        firstDomain,
                        firstDomain.getLocation().getTopiaId(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        Collections.singletonList(mat),
                        Collections.singletonList(attelage),
                        null,
                        null,
                        null
                )
        );

    }

    @Test
    public void testAutomoteurMaterielAndToolsCouplingDeletion() throws IOException {

        testDatas.createTestNetworks();
        // import required by tests
        testDatas.importMateriels();
        testDatas.importInterventionAgrosystTravailEdi();

        // get domain
        DomainFilter domainFilter = new DomainFilter();
        domainFilter.setDomainName("La Bouinelière");
        domainFilter.setCampaign(2014);
        List<Domain> domains = domainService.getFilteredDomains(domainFilter).getElements();
        Domain firstDomain = domains.getFirst();

        // create materiel and
        Equipment automoteur = new EquipmentImpl();
        automoteur.setTopiaId("NEW-EQUIPMENT-" + 1);
        automoteur.setCode(UUID.randomUUID().toString());
        automoteur.setName("Automoteur");
        RefMateriel refMateriel = getAutomoteur();
        automoteur.setRefMateriel(refMateriel);

        Equipment matEquipment2 = new EquipmentImpl();
        matEquipment2.setTopiaId("NEW-EQUIPMENT-" + 2);
        matEquipment2.setCode(UUID.randomUUID().toString());
        matEquipment2.setName("Equipement 1");

        RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        List<RefInterventionAgrosystTravailEDI> refInterventionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.findAll();

        Set<RefInterventionAgrosystTravailEDI> mainsActions = Sets.newHashSet(refInterventionAgrosystTravailEDIs.get(0), refInterventionAgrosystTravailEDIs.get(1), refInterventionAgrosystTravailEDIs.get(2));
        // create tool coupling
        ToolsCoupling attelage = new ToolsCouplingImpl();
        attelage.setCode(UUID.randomUUID().toString());
        attelage.setToolsCouplingName("Test");
        attelage.setMainsActions(mainsActions);

        // link to equipment 1
        Equipment tractor = new EquipmentImpl();
        tractor.setName("Tractor");
        tractor.setTopiaId("NEW-EQUIPMENT-" + 1);
        attelage.setTractor(tractor);

        Equipment equipment1 = new EquipmentImpl();
        equipment1.setName("equipment1");
        equipment1.setTopiaId("NEW-EQUIPMENT-" + 2);
        attelage.addEquipments(equipment1);

        // test update to ADD materiel and tools coupling
        firstDomain = domainService.createOrUpdateDomain(
                firstDomain,
                firstDomain.getLocation().getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                Lists.newArrayList(automoteur, matEquipment2, tractor, equipment1),
                Collections.singletonList(attelage),
                null,
                null,
                null
        );
        long from = domainService.getEquipments(firstDomain.getTopiaId()).size();
        Assertions.assertEquals(4L, from);

        // test to delete materiel and tools coupling
        // test regresion for bug #4338
        domainService.createOrUpdateDomain(
                firstDomain,
                firstDomain.getLocation().getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                new ArrayList<>(),
                new ArrayList<>(),
                null,
                null,
                null
        );
        long to = domainService.getEquipments(firstDomain.getTopiaId()).size();
        Assertions.assertEquals(0L, to);
    }

    /**
     * this test create 5 crops into a domain
     * then check if CroppingPlanDtos are good as CroppingPlanDtos should have affected = false
     * the test create PracticedSystem targeting one crop.
     * then check if CroppingPlanDtos are good as the given crop should have affected = true and the other false.
     */
    @Test
    public void testAffectedCroppingPlan() throws IOException {

        testDatas.importCommunesFrance();

        if (refTypeAgricultureTopiaDao.count() == 0) {
            testDatas.importTypeAgriculture();
        }

        RefLocation location = referentialService.getActiveCommunes("01500").getFirst();
        Domain d1 = domainService.newDomain();
        d1.setCampaign(2014);
        d1.setName("Test CroppingPlan");
        d1.setMainContact("test");
        d1.setType(DomainType.DOMAINE_EXPERIMENTAL);
        List<CroppingPlanEntryDto> croppingPlanEntries = Lists.newArrayList(
                createCroppingPlanEntryDto("CP1", CroppingEntryType.MAIN),
                createCroppingPlanEntryDto("CPi", CroppingEntryType.INTERMEDIATE));
        Domain domain = domainService.createOrUpdateDomain(
                d1,
                location.getTopiaId(),
                null,
                null,
                croppingPlanEntries,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

        UsageList<CroppingPlanEntryDto> croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());
        List<CroppingPlanEntryDto> croppingPlanEntryDtos = croppingPlanEntryDtoAndUsage.getElements();
        Map<String, Boolean> areAffectedCPEs = croppingPlanEntryDtoAndUsage.getUsageMap();


        Assertions.assertEquals(2, croppingPlanEntryDtos.size());
        Assertions.assertFalse(areAffectedCPEs.get(croppingPlanEntryDtos.getFirst().getTopiaId()));
        Assertions.assertFalse(areAffectedCPEs.get(croppingPlanEntryDtos.get(1).getTopiaId()));

        croppingPlanEntryDtos = Lists.newArrayList(croppingPlanEntryDtoAndUsage.getElements());
        int nbNewCP = 6;
        for (int i = 2; i < nbNewCP; i++) {
            croppingPlanEntryDtos.add(createCroppingPlanEntryDto("CP" + i, CroppingEntryType.MAIN));
        }
        domain = domainService.createOrUpdateDomain(
                domain,
                location.getTopiaId(),
                null,
                null,
                croppingPlanEntryDtos,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());
        croppingPlanEntryDtos = croppingPlanEntryDtoAndUsage.getElements();
        areAffectedCPEs = croppingPlanEntryDtoAndUsage.getUsageMap();

        Assertions.assertEquals(nbNewCP, croppingPlanEntryDtos.size());
        for (CroppingPlanEntryDto croppingPlanEntryDto : croppingPlanEntries) {
            Assertions.assertNull(areAffectedCPEs.get(croppingPlanEntryDto.getTopiaId()));
        }

        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importActaTraitementsProduits();

        GrowingPlan growingPlan = testDatas.createGrowingPlan(domain);
        GrowingSystem gs = testDatas.createGrowingSystem(growingPlan);
        List<CroppingPlanEntryDto> affectedCPs = Lists.newArrayList(croppingPlanEntryDtos.get(0), croppingPlanEntryDtos.get(1));
        String affectedCPId = affectedCPs.getFirst().getTopiaId();
        String affectedCPId2 = affectedCPs.get(1).getTopiaId();

        // on affecte une principale et une intermediaire
        PracticedSystem ps = testDatas.createPracticedSystem(gs);
        List<CroppingPlanEntry> newCroppingPlanEntries = Lists.newArrayListWithCapacity(affectedCPs.size());
        for (CroppingPlanEntryDto croppingPlanEntryDto : affectedCPs) {
            newCroppingPlanEntries.add(croppingPlanEntryDao.forTopiaIdEquals(croppingPlanEntryDto.getTopiaId()).findUnique());
        }
        testDatas.createTestPracticedSystemPerennialAndSeasonalCycle(newCroppingPlanEntries, ps);

        croppingPlanEntryDtoAndUsage = domainService.getCroppingPlanEntryDtoAndUsage(domain.getTopiaId());

        List<CroppingPlanEntryDto> cpes = croppingPlanEntryDtoAndUsage.getElements();
        areAffectedCPEs = croppingPlanEntryDtoAndUsage.getUsageMap();

        Assertions.assertEquals(nbNewCP, cpes.size());

        for (CroppingPlanEntryDto croppingPlanEntryDto : cpes) {
            // seules deux specifiques doivent être affectée
            if (croppingPlanEntryDto.getTopiaId().equals(affectedCPId) || croppingPlanEntryDto.getTopiaId().equals(affectedCPId2)) {
                Assertions.assertTrue(areAffectedCPEs.get(croppingPlanEntryDto.getTopiaId()));
            } else {
                Assertions.assertFalse(areAffectedCPEs.get(croppingPlanEntryDto.getTopiaId()));
            }
        }
    }

    @Test
    public void testAffectedCroppingSpecies() throws IOException {
        testDatas.importActaTraitementsProduits();
        testDatas.importRefEspeces();
        testDatas.createTestPraticedSystems();

        PracticedSystem psBaulon2013 = testDatas.createTestPraticedSystems();
        Domain domain = psBaulon2013.getGrowingSystem().getGrowingPlan().getDomain();
        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        testDatas.createTestPracticedSystemPerennialAndSeasonalCycle(croppingPlanEntries, psBaulon2013);

        Map<String, Boolean> cpSpeciesUsages = domainService.getCroppingPlanSpeciesUsage(domain.getTopiaId());
        Assertions.assertEquals(7, cpSpeciesUsages.size());
    }

    protected CroppingPlanEntryDto createCroppingPlanEntryDto(String name, CroppingEntryType type) {
        CroppingPlanEntryDto croppingPlanEntryDto = new CroppingPlanEntryDto();
        croppingPlanEntryDto.setName(name);
        croppingPlanEntryDto.setType(type);
        return croppingPlanEntryDto;
    }

    @Test
    public void testToolsCouplingAffected() throws IOException {
        testDatas.importCommunesFrance();
        testDatas.importMateriels();
        testDatas.importInterventionAgrosystTravailEdi();

        RefLocation location = referentialService.getActiveCommunes("01500").getFirst();
        Domain d1 = domainService.newDomain();
        d1.setCampaign(2014);
        d1.setName("Test toolsCouplingAffected");
        d1.setMainContact("test");
        d1.setType(DomainType.DOMAINE_EXPERIMENTAL);

        RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        List<RefInterventionAgrosystTravailEDI> refInterventionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.findAll();

        Set<RefInterventionAgrosystTravailEDI> mainsActions = Sets.newHashSet(refInterventionAgrosystTravailEDIs.get(0), refInterventionAgrosystTravailEDIs.get(1), refInterventionAgrosystTravailEDIs.get(2));
        // create tool coupling
        ToolsCoupling attelage = new ToolsCouplingImpl();
        attelage.setCode(UUID.randomUUID().toString());
        attelage.setToolsCouplingName("Test");
        attelage.setMainsActions(mainsActions);

        // link to equipment 1
        Equipment tractor = new EquipmentImpl();
        tractor.setName("tractor");
        tractor.setTopiaId("NEW-EQUIPMENT-" + 1);
        RefMateriel refMateriel = getTractorVigne();
        tractor.setRefMateriel(refMateriel);
        attelage.setTractor(tractor);

        Equipment coupling = new EquipmentImpl();
        coupling.setName("coupling");
        coupling.setTopiaId("NEW-EQUIPMENT-" + 2);
        RefMateriel refMaterielBroy = getBroyeur();
        coupling.setRefMateriel(refMaterielBroy);
        attelage.addEquipments(coupling);

        List<Equipment> materiels = Lists.newArrayList(tractor, coupling);

        Domain domain = domainService.createOrUpdateDomain(
                d1,
                location.getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                materiels,
                Collections.singletonList(attelage),
                null,
                null,
                null
        );
        UsageList<ToolsCoupling> usageList = domainService.getToolsCouplingAndUsage(domain.getTopiaId());
        Assertions.assertEquals(1, usageList.getUsageMap().size());
    }

    @Test
    public void testToolsCouplingCopy() throws IOException {
        testDatas.importCommunesFrance();
        testDatas.importMateriels();
        testDatas.importInterventionAgrosystTravailEdi();

        RefLocation location = referentialService.getActiveCommunes("01500").getFirst();
        Domain fromDomain = domainService.newDomain();
        fromDomain.setName("Test toolsCouplingCopy source");
        fromDomain.setMainContact("test");
        fromDomain.setType(DomainType.DOMAINE_EXPERIMENTAL);
        fromDomain.setCampaign(2014);

        Domain toDomain = domainService.newDomain();
        toDomain.setName("Test toolsCouplingCopy target");
        toDomain.setMainContact("test");
        toDomain.setType(DomainType.DOMAINE_EXPERIMENTAL);
        toDomain.setCampaign(2015);

        // create materiel and
        Equipment mat = new EquipmentImpl();
        mat.setTopiaId("NEW-EQUIPMENT-" + 1);
        mat.setCode(UUID.randomUUID().toString());
        mat.setName("Test");
        RefMateriel refMateriel = getTractorVigne();
        mat.setRefMateriel(refMateriel);

        // create coupling equipment
        Equipment matBroy = new EquipmentImpl();
        matBroy.setTopiaId("NEW-EQUIPMENT-" + 2);
        matBroy.setCode(UUID.randomUUID().toString());
        matBroy.setName("Broyeur");
        RefMateriel refMaterielBroy = getBroyeur();
        matBroy.setRefMateriel(refMaterielBroy);

        RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        List<RefInterventionAgrosystTravailEDI> refInterventionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.findAll();

        Set<RefInterventionAgrosystTravailEDI> mainsActions = Sets.newHashSet(refInterventionAgrosystTravailEDIs.get(0), refInterventionAgrosystTravailEDIs.get(1), refInterventionAgrosystTravailEDIs.get(2));
        // create tool coupling

        // link to equipment 1
        Equipment tractor = new EquipmentImpl();
        tractor.setName("tractor");
        tractor.setTopiaId("NEW-EQUIPMENT-" + 3);
        tractor.setRefMateriel(refMateriel);

        Equipment coupling = new EquipmentImpl();
        coupling.setName("coupling");
        coupling.setTopiaId("NEW-EQUIPMENT-" + 4);

        ToolsCoupling attelage = new ToolsCouplingImpl();
        attelage.setCode(UUID.randomUUID().toString());
        attelage.setToolsCouplingName("Test");
        attelage.setMainsActions(mainsActions);
        attelage.setTractor(tractor);
        attelage.addEquipments(coupling);

        List<Equipment> materiels = Lists.newArrayList(mat, matBroy, coupling, tractor);

        Domain source = domainService.createOrUpdateDomain(
                fromDomain,
                location.getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                materiels,
                Collections.singletonList(attelage),
                null,
                null,
                null
        );

        // create domain not related to source one (code is different)
        Domain target = domainService.createOrUpdateDomain(toDomain, location.getTopiaId(), null, null, null, null, null, null, null, null, null, null, null);

        List<Equipment> equipments = domainService.getEquipments(source.getTopiaId());
        List<ToolsCoupling> toolsCouplingSource = domainService.getToolsCouplings(source.getTopiaId());

        domainService.copyTools(source.getTopiaId(), Lists.newArrayList(target.getTopiaId()), false, equipments, null);

        domainService.copyTools(source.getTopiaId(), Lists.newArrayList(target.getTopiaId()), true, equipments, Lists.newArrayList(toolsCouplingSource));
        toolsCouplingSource = domainService.getToolsCouplings(target.getTopiaId());
        Assertions.assertEquals(1, toolsCouplingSource.size());
        Equipment copiedTractor = toolsCouplingSource.getFirst().getTractor();
        Assertions.assertNotNull(copiedTractor);

        // create a domain with same code related to source one
        Domain relatedDomain = domainDao.create(
                Domain.PROPERTY_TOPIA_ID, "TARGETED_DOMAIN_ID",
                Domain.PROPERTY_NAME, "TAGETED_DOMAIN",
                Domain.PROPERTY_CODE, source.getCode(),
                Domain.PROPERTY_CAMPAIGN, source.getCampaign() + 1,
                Domain.PROPERTY_LOCATION, location,
                Domain.PROPERTY_MAIN_CONTACT, "INRAE",
                Domain.PROPERTY_TYPE, DomainType.DOMAINE_EXPERIMENTAL,
                Domain.PROPERTY_UPDATE_DATE, LocalDateTime.now()
        );

        equipments = domainService.getEquipments(source.getTopiaId());
        toolsCouplingSource = domainService.getToolsCouplings(source.getTopiaId());
        List<String> codes = new ArrayList<>();
        for (Equipment equipment : equipments) {
            codes.add(equipment.getCode());
        }

        // 2 equipments are copied and 0 tools couplings
        domainService.copyTools(source.getTopiaId(), Lists.newArrayList(relatedDomain.getTopiaId()), false, equipments, null);
        List<Equipment> targetedEquipement = domainService.getEquipments(relatedDomain.getTopiaId());
        Assertions.assertEquals(equipments.size(), targetedEquipement.size());

        // 0 equipments are copied (they already exist) and 1 tools coupling is copied
        domainService.copyTools(source.getTopiaId(), Lists.newArrayList(relatedDomain.getTopiaId()), true, equipments, Lists.newArrayList(toolsCouplingSource));

        // check if the number of equipment is correct
        targetedEquipement = domainService.getEquipments(relatedDomain.getTopiaId());
        Assertions.assertEquals(equipments.size(), targetedEquipement.size());

        // check if tools coupling has been created
        List<ToolsCoupling> toolsCouplingTarget = domainService.getToolsCouplings(relatedDomain.getTopiaId());
        Assertions.assertEquals(toolsCouplingSource.size(), toolsCouplingTarget.size());

        // check if tools coupling has relation with tractor equipment
        Assertions.assertNotNull(toolsCouplingTarget.getFirst().getTractor());
        Assertions.assertTrue(codes.contains(toolsCouplingTarget.getFirst().getTractor().getCode()));
    }


    @Test
    public void testToolsCouplingCreation() throws IOException {
        testDatas.importCommunesFrance();
        testDatas.importMateriels();
        testDatas.importInterventionAgrosystTravailEdi();

        RefLocation location = referentialService.getActiveCommunes("01500").getFirst();
        Domain d1 = domainService.newDomain();
        d1.setCampaign(2014);
        d1.setName("Test toolsCouplingAffected");
        d1.setMainContact("test");
        d1.setType(DomainType.DOMAINE_EXPERIMENTAL);

        // create materiel and
        Equipment mat = new EquipmentImpl();
        mat.setTopiaId("NEW-EQUIPMENT-" + 1);
        mat.setCode(UUID.randomUUID().toString());
        mat.setName("Test");
        RefMateriel refMateriel = getTractorVigne();
        mat.setRefMateriel(refMateriel);

        RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        List<RefInterventionAgrosystTravailEDI> refInterventionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.findAll();

        Set<RefInterventionAgrosystTravailEDI> mainsActions = Sets.newHashSet(refInterventionAgrosystTravailEDIs.get(0), refInterventionAgrosystTravailEDIs.get(1), refInterventionAgrosystTravailEDIs.get(2));
        // create tool coupling
        ToolsCoupling attelage = new ToolsCouplingImpl();
        attelage.setCode(UUID.randomUUID().toString());
        attelage.setToolsCouplingName("Test");
        attelage.setMainsActions(mainsActions);

        // link to equipment 1
        Equipment tractor = new EquipmentImpl();
        tractor.setTopiaId("NEW-EQUIPMENT-" + 1);
        attelage.setTractor(tractor);
        List<Equipment> equipments = Lists.newArrayList(mat);
        attelage.setEquipments(equipments);

        Domain domain = domainService.createOrUpdateDomain(
                d1,
                location.getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                Collections.singletonList(mat),
                Collections.singletonList(attelage),
                null,
                null,
                null
        );
        List<ToolsCoupling> toolsCouplings = domainService.getToolsCouplings(domain.getTopiaId());
        Assertions.assertEquals(1, toolsCouplings.size());
        for (ToolsCoupling toolsCoupling : toolsCouplings) {
            Assertions.assertFalse(toolsCoupling.getTopiaId().contains("NEW-EQUIPMENT-"));
            Collection<Equipment> tcEquipments = toolsCoupling.getEquipments();
            Assertions.assertEquals(1, tcEquipments.size());
            for (Equipment tcEquipment : tcEquipments) {
                Assertions.assertFalse(tcEquipment.getTopiaId().contains("NEW-EQUIPMENT-"));
            }
        }
    }

    @Test
    public void testMainDomainPreconditions() {
        Domain d = new DomainImpl();
        d.setType(DomainType.DOMAINE_EXPERIMENTAL);
        d.setCampaign(2014);
        d.setMainContact("MC");

        DomainServiceImplTest dst = new DomainServiceImplTest();
        Assertions.assertFalse(dst.validDomainPreconditions(d, "LOC"));

        d.setMainContact("Contact Principal");
        d.setName("Domain Test");
        d.setType(DomainType.DOMAINE_EXPERIMENTAL);

        Assertions.assertTrue(dst.validDomainPreconditions(d, "LOC"));
    }

    @Test
    public void testValidToolsCouplingDomainPreconditions() throws IOException {
        // import required by tests
        testDatas.importMateriels();
        testDatas.importInterventionAgrosystTravailEdi();
        RefInterventionAgrosystTravailEDITopiaDao refActionAgrosystTravailEDITopiaDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        List<RefInterventionAgrosystTravailEDI> refInterventionAgrosystTravailEDIs = refActionAgrosystTravailEDITopiaDao.findAll();
        Set<RefInterventionAgrosystTravailEDI> mainsActions = Sets.newHashSet(refInterventionAgrosystTravailEDIs.get(0), refInterventionAgrosystTravailEDIs.get(1), refInterventionAgrosystTravailEDIs.get(2));

        Equipment tractor = new EquipmentImpl();
        tractor.setName("Tracteur");
        tractor.setRefMateriel(getTractorVigne());

        Equipment tools0 = new EquipmentImpl();
        tools0.setName("Broyeur");
        tools0.setRefMateriel(getBroyeur());

        ToolsCoupling tc0 = new ToolsCouplingImpl();
        tc0.setToolsCouplingName("OK 1");
        tc0.setTractor(tractor);
        tc0.addEquipments(tools0);
        tc0.setMainsActions(mainsActions);

        DomainServiceImplTest dst = new DomainServiceImplTest();

        // 1 TC with one tractor and 1 equipment
        Assertions.assertTrue(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc0)));

        Equipment automotive = new EquipmentImpl();
        automotive.setName("Automoteur");
        automotive.setRefMateriel(getAutomoteur());

        ToolsCoupling tc1 = new ToolsCouplingImpl();
        tc1.setToolsCouplingName("OK 2");
        tc1.setTractor(automotive);
        tc1.setMainsActions(mainsActions);

        // 1 TC with one Automotive and 0 equipment
        Assertions.assertTrue(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc1)));

        ToolsCoupling tc2 = new ToolsCouplingImpl();
        tc2.setToolsCouplingName("OK 3");
        tc2.setTractor(automotive);
        tc2.addEquipments(tools0);
        tc2.setMainsActions(mainsActions);

        // 1 TC with one Automotive and 1 equipment
        Assertions.assertTrue(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc2)));

        Equipment tools1 = new EquipmentImpl();
        tools1.setName("Sous soleuse 2 corps");
        tools1.setRefMateriel(getToolsSubsoilers());

        ToolsCoupling tc3 = new ToolsCouplingImpl();
        tc3.setToolsCouplingName("OK 4");
        tc3.setTractor(tractor);
        tc3.addEquipments(tools0);
        tc3.addEquipments(tools1);
        tc3.setMainsActions(mainsActions);

        // 1 TC with one tractor and 2 tools equipments
        Assertions.assertTrue(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc3)));

        Equipment irrigReel = new EquipmentImpl();
        irrigReel.setName("enroul");
        irrigReel.setRefMateriel(getIrrigEnrouleur());

        Equipment irrigStation = new EquipmentImpl();
        irrigStation.setName("Station");
        irrigStation.setRefMateriel(getIrrigStation());

        ToolsCoupling tc4 = new ToolsCouplingImpl();
        tc4.setToolsCouplingName("OK 4");
        tc4.setTractor(tractor);
        tc4.addEquipments(irrigReel);
        tc4.addEquipments(irrigStation);
        tc4.setMainsActions(mainsActions);

        // 1 TC with one tractor and 2 irrigation equipments
        Assertions.assertTrue(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc4)));

        ToolsCoupling tc5 = new ToolsCouplingImpl();
        tc5.setToolsCouplingName("OK 5");
        tc5.setTractor(automotive);
        tc5.addEquipments(irrigReel);
        tc5.addEquipments(irrigStation);
        tc5.setMainsActions(mainsActions);

        // 1 tc with one automotive and 2 irrigation equipments
        Assertions.assertTrue(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc5)));

        ToolsCoupling tc6 = new ToolsCouplingImpl();
        tc6.setToolsCouplingName("OK 6");
        tc6.addEquipments(irrigReel);
        tc6.addEquipments(irrigStation);
        tc6.setMainsActions(mainsActions);

        // 1 tc with no tractor or automotive and 2 irrigation equipments
        Assertions.assertTrue(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc6)));

        ToolsCoupling tc7 = new ToolsCouplingImpl();
        tc7.setToolsCouplingName("FAILED 1");
        tc7.addEquipments(tools0);
        tc7.addEquipments(irrigStation);
        tc7.setMainsActions(mainsActions);

        // FAILED 1 tc with no tractor or automotive and 1 tools plus one irrigation equipment
        Assertions.assertFalse(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc7)));

        ToolsCoupling tc8 = new ToolsCouplingImpl();
        tc8.setToolsCouplingName("FAILED 2");
        tc8.setTractor(tractor);
        tc8.addEquipments(tools0);
        tc8.addEquipments(irrigStation);
        tc8.setMainsActions(mainsActions);

        // FAILED 1 tc with 1 tractor and 1 tools plus one irrigation equipment
        Assertions.assertFalse(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc8)));

        ToolsCoupling tc9 = new ToolsCouplingImpl();
        tc9.setToolsCouplingName("FAILED 9");
        tc9.setTractor(automotive);
        tc9.addEquipments(tools0);
        tc9.addEquipments(irrigStation);
        tc9.setMainsActions(mainsActions);

        // FAILED 1 tc with 1 automotive and 1 tools plus one irrigation equipment
        Assertions.assertFalse(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc9)));

        ToolsCoupling tc10 = new ToolsCouplingImpl();
        tc10.setTractor(automotive);
        tc10.addEquipments(tools0);
        tc10.addEquipments(tools1);
        tc10.setMainsActions(mainsActions);

        // FAILED 1 tc without name
        Assertions.assertFalse(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc10)));

        ToolsCoupling tc11 = new ToolsCouplingImpl();
        tc9.setToolsCouplingName("FAILED 11");
        tc11.setTractor(automotive);
        tc11.addEquipments(tools0);
        tc11.addEquipments(tools1);

        // FAILED 1 tc without main action
        Assertions.assertFalse(dst.validDomainToolsCouplingsPreconditions(Lists.newArrayList(tc11)));

    }

    @Test
    public void loadAllCropAndSpeciesFromDomainOnCampaign() throws IOException {
        testDatas.createTestNetworks();
        Domain domain = domainService.getAllDomains().getFirst();
        Binder<Domain, Domain> binder = BinderFactory.newBinder(Domain.class, Domain.class);
        Domain domainOnNextCampaign = new DomainImpl();
        binder.copyExcluding(domain, domainOnNextCampaign,
                Domain.PROPERTY_TOPIA_ID,
                Domain.PROPERTY_WEATHER_STATIONS);
        domainOnNextCampaign.setCampaign(domain.getCampaign() + 1);
        domainOnNextCampaign = domainDao.create(domainOnNextCampaign);

        long initialSize = croppingPlanEntryDao.forDomainEquals(domain).count();

        String cpe0Code = UUID.randomUUID().toString();
        String cpe1Code = UUID.randomUUID().toString();
        String cpe2Code = UUID.randomUUID().toString();

        String cps0_0Code = UUID.randomUUID().toString();
        String cps1_0Code = UUID.randomUUID().toString();
        String cps2_0Code = UUID.randomUUID().toString();
        String cps2_1Code = UUID.randomUUID().toString();

        CroppingPlanEntry cpe0 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain,
                CroppingPlanEntry.PROPERTY_CODE, cpe0Code,
                CroppingPlanEntry.PROPERTY_NAME, "cpe2",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanSpecies cps0_0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0,
                // blé tendre d'hiver
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps0_0Code);
        cpe0.addCroppingPlanSpecies(cps0_0);

        CroppingPlanEntry cpe1 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain,
                CroppingPlanEntry.PROPERTY_CODE, cpe1Code,
                CroppingPlanEntry.PROPERTY_NAME, "cpe0",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanSpecies cps1_0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0,
                // blé tendre d'hiver
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps1_0Code);
        cpe1.addCroppingPlanSpecies(cps1_0);

        CroppingPlanEntry cpe2 = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain,
                CroppingPlanEntry.PROPERTY_CODE, cpe2Code,
                CroppingPlanEntry.PROPERTY_NAME, "cpe1",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanSpecies cps2_0 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps2_0Code);
        cpe2.addCroppingPlanSpecies(cps2_0);
        CroppingPlanSpecies cps2_1 = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps2_1Code);
        cpe2.addCroppingPlanSpecies(cps2_1);

        CroppingPlanEntry cpe0b = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domainOnNextCampaign,
                CroppingPlanEntry.PROPERTY_CODE, cpe0Code,
                CroppingPlanEntry.PROPERTY_NAME, "cpe2",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanSpecies cps0_0b = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0b,
                // blé tendre d'hiver
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps0_0Code);
        cpe0b.addCroppingPlanSpecies(cps0_0b);

        CroppingPlanEntry cpe1b = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domainOnNextCampaign,
                CroppingPlanEntry.PROPERTY_CODE, cpe1Code,
                CroppingPlanEntry.PROPERTY_NAME, "cpe0",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanSpecies cps1_0b = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0b,
                // blé tendre d'hiver
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps1_0Code);
        cpe1b.addCroppingPlanSpecies(cps1_0b);

        CroppingPlanEntry cpe2b = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domainOnNextCampaign,
                CroppingPlanEntry.PROPERTY_CODE, cpe2Code,
                CroppingPlanEntry.PROPERTY_NAME, "cpe1",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);
        CroppingPlanSpecies cps2_0b = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0b,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps2_0Code);
        cpe2b.addCroppingPlanSpecies(cps2_0b);
        CroppingPlanSpecies cps2_1b = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe0b,
                CroppingPlanSpecies.PROPERTY_SPECIES, refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique(),
                CroppingPlanSpecies.PROPERTY_CODE, cps2_1Code);
        cpe2b.addCroppingPlanSpecies(cps2_1b);

        getCurrentTransaction().commit();

        Map<String, List<CroppingPlanSpecies>> result = domainService.getCroppingPlanSpeciesForDomainAndCampaignsByCropCode(domain.getCode(), Sets.newHashSet(domain.getCampaign(), domainOnNextCampaign.getCampaign()));
        Assertions.assertEquals(initialSize + 3, result.size());
        Assertions.assertEquals(2, result.get(cpe0Code).size());
        Assertions.assertEquals(2, result.get(cpe1Code).size());
        Assertions.assertEquals(4, result.get(cpe2Code).size());
    }

    protected RefMateriel getAutomoteur() {
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "Porteur Viti Automoteur", "Porteur Automoteur", "120 ch");
        filter.setType(MaterielType.AUTOMOTEUR);
        Map<String, List<MaterielDto>> values = referentialService.getMaterielUniteMap(filter);
        String firstTracteurId = values.keySet().iterator().next();
        return referentialService.getMateriel(firstTracteurId);
    }

    protected RefMateriel getBroyeur() {
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "Broyeur", "Broyeur porté VL", "Sarments au sol");
        filter.setType(MaterielType.OUTIL);
        Map<String, List<MaterielDto>> values = referentialService.getMaterielUniteMap(filter);
        String firstBroyeurId = values.keySet().iterator().next();
        return referentialService.getMateriel(firstBroyeurId);
    }

    protected RefMateriel getToolsSubsoilers() {
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "SOUS-SOLEUSES-DECOMPACTEURS", "Sous soleuse 2 corps", "portée,");
        filter.setType(MaterielType.OUTIL);
        Map<String, List<MaterielDto>> values = referentialService.getMaterielUniteMap(filter);
        String firstSubsoilersId = values.keySet().iterator().next();
        return referentialService.getMateriel(firstSubsoilersId);
    }

    protected RefMateriel getTractorVigne() {
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "TRACTEURS SPECIALISES 4RM VIGNE", "Tracteurs Spécialisés Vigne 4 RM avec cabine confort",
                "46" +
                        " " +
                        "à" +
                        " " +
                        "55 ch");
        filter.setType(MaterielType.TRACTEUR);
        filter.setUniteParAn(50);
        Map<String, List<MaterielDto>> values = referentialService.getMaterielUniteMap(filter);
        String firstTracteurId = values.keySet().iterator().next();
        return referentialService.getMateriel(firstTracteurId);
    }

    protected RefMateriel getIrrigEnrouleur() {
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "Enrouleurs", "Enrouleur 90 mm x 350 m", "40 m3/h");
        filter.setType(MaterielType.IRRIGATION);
        Map<String, List<MaterielDto>> values = referentialService.getMaterielUniteMap(filter);
        String reel = values.keySet().iterator().next();
        return referentialService.getMateriel(reel);
    }

    protected RefMateriel getIrrigStation() {
        TypeMaterielFilter filter = new TypeMaterielFilter(false, "Station", "Forage reserve", "Forage/Réserve 50 m + racor. EDF");
        filter.setType(MaterielType.IRRIGATION);
        Map<String, List<MaterielDto>> values = referentialService.getMaterielUniteMap(filter);
        String station = values.keySet().iterator().next();
        return referentialService.getMateriel(station);
    }

    @Test
    public void testLoadSectorsByCodeEspeceBotaniqueCodeQualifiant() throws IOException {
        testDatas.createTestNetworks();
        RefEspece espece = referentialService.findSpecies("g").getFirst();

        String speciesCode0 = "CS0";
        String speciesCode1 = "SC1";

        Domain domain = domainService.getAllDomains().getFirst();

        RefEspece r0 = refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        Binder<RefEspece, RefEspece> refEspeceBinder = BinderFactory.newBinder(RefEspece.class, RefEspece.class);
        Binder<RefCultureMAA, RefCultureMAA> cultureMAABinder = BinderFactory.newBinder(RefCultureMAA.class, RefCultureMAA.class);
        RefEspece r1 = refEspeceDao.newInstance();
        refEspeceBinder.copyExcluding(r0, r1, RefEspece.PROPERTY_TOPIA_ID, RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, RefEspece.PROPERTY_CULTURES_MAA);
        r1.setCode_qualifiant_AEE("CQ_AEE1");

        bindCultureMaa(r0, r1, cultureMAABinder);

        RefEspece r2 = new RefEspeceImpl();
        refEspeceBinder.copyExcluding(r0, r2, RefEspece.PROPERTY_TOPIA_ID, RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, RefEspece.PROPERTY_CULTURES_MAA);
        r2.setCode_qualifiant_AEE("CQ_AEE2");
        bindCultureMaa(r0, r2, cultureMAABinder);

        RefEspece r3 = new RefEspeceImpl();
        refEspeceBinder.copyExcluding(r0, r3, RefEspece.PROPERTY_TOPIA_ID, RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, RefEspece.PROPERTY_CULTURES_MAA);
        r3.setCode_qualifiant_AEE("CQ_AEE3");
        bindCultureMaa(r0, r3, cultureMAABinder);

        refEspeceDao.create(r1);
        refEspeceDao.create(r2);
        refEspeceDao.create(r3);

        CroppingPlanEntry cpe = croppingPlanEntryDao.create(
                CroppingPlanEntry.PROPERTY_DOMAIN, domain,
                CroppingPlanEntry.PROPERTY_CODE, "AAA",
                CroppingPlanEntry.PROPERTY_NAME, "EEE",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN);

        croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe,
                CroppingPlanSpecies.PROPERTY_SPECIES, r0,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0);

        croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe,
                CroppingPlanSpecies.PROPERTY_SPECIES, r0,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode0);

        croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe,
                CroppingPlanSpecies.PROPERTY_SPECIES, r1,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1);
        croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe,
                CroppingPlanSpecies.PROPERTY_SPECIES, r2,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1);
        croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe,
                CroppingPlanSpecies.PROPERTY_SPECIES, r3,
                CroppingPlanSpecies.PROPERTY_CODE, speciesCode1);

        CroppingPlanSpeciesDto speciesDto = new CroppingPlanSpeciesDto();
        speciesDto.setSpeciesId(espece.getTopiaId());

        List<Pair<String, String>> codeEspeceBotaniquesCodeQualifiantAEE = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifiantAEEForSpeciesCode(Sets.newHashSet(speciesCode0, speciesCode1));
        Assertions.assertEquals(4, codeEspeceBotaniquesCodeQualifiantAEE.size());

        RefSpeciesToSectorTopiaDao refSpeciesToSectorTopiaDao = getPersistenceContext().getRefSpeciesToSectorDao();
        refSpeciesToSectorTopiaDao.create(
                RefSpeciesToSector.PROPERTY_ACTIVE, true,
                RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZAR",
                RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, null,
                RefSpeciesToSector.PROPERTY_SECTOR, Sector.ARBORICULTURE
        );
        refSpeciesToSectorTopiaDao.create(
                RefSpeciesToSector.PROPERTY_ACTIVE, true,
                RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZOB",
                RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, null,
                RefSpeciesToSector.PROPERTY_SECTOR, Sector.CULTURES_TROPICALES
        );
        refSpeciesToSectorTopiaDao.create(
                RefSpeciesToSector.PROPERTY_ACTIVE, true,
                RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZAR",
                RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, "CQ_AEE1",
                RefSpeciesToSector.PROPERTY_SECTOR, Sector.GRANDES_CULTURES
        );
        refSpeciesToSectorTopiaDao.create(
                RefSpeciesToSector.PROPERTY_ACTIVE, true,
                RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZAR",
                RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, "CQ_AEE2",
                RefSpeciesToSector.PROPERTY_SECTOR, Sector.MARAICHAGE
        );
        refSpeciesToSectorTopiaDao.create(
                RefSpeciesToSector.PROPERTY_ACTIVE, true,
                RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZAR",
                RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, "CQ_AEE2",
                RefSpeciesToSector.PROPERTY_SECTOR, Sector.POLYCULTURE_ELEVAGE
        );
        refSpeciesToSectorTopiaDao.create(
                RefSpeciesToSector.PROPERTY_ACTIVE, true,
                RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZAR",
                RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, "CQ_AEE2",
                RefSpeciesToSector.PROPERTY_SECTOR, Sector.HORTICULTURE
        );
        refSpeciesToSectorTopiaDao.create(
                RefSpeciesToSector.PROPERTY_ACTIVE, true,
                RefSpeciesToSector.PROPERTY_CODE_ESPECE_BOTANIQUE, "ZAR",
                RefSpeciesToSector.PROPERTY_CODE_QUALIFIANT__AEE, "AUTRE",
                RefSpeciesToSector.PROPERTY_SECTOR, Sector.HORTICULTURE
        );

        RefSpeciesToSectorTopiaDao refSpeciesToSectorDao = getPersistenceContext().getRefSpeciesToSectorDao();
        Map<Pair<String, String>, List<Sector>> res = refSpeciesToSectorDao.loadSectorsByCodeEspeceBotaniqueCodeQualifiant(codeEspeceBotaniquesCodeQualifiantAEE);

        // to get complete list of sector you need to include values for (codeEspeceBotanique + codeQualifiantAee) and (codeEspeceBotanique + null)
        Assertions.assertEquals(3, res.get(Pair.of("ZAR", "CQ_AEE2")).size());
        Assertions.assertEquals(1, res.get(Pair.of("ZAR", null)).size());
        Assertions.assertEquals(1, res.get(Pair.of("ZAR", "CQ_AEE1")).size());
        Assertions.assertNull(res.get(Pair.of("ZAR", "AUTRE")));
    }

    public void bindCultureMaa(RefEspece rFrom, RefEspece rTo, Binder<RefCultureMAA, RefCultureMAA> cultureMAABinder) {
        Set<RefCultureMAA> r1Maa = new HashSet<>();
        rTo.setCulturesMaa(r1Maa);
        for (RefCultureMAA refCultureMAA : rFrom.getCulturesMaa()) {
            if (refCultureMAA == null) continue;
            RefCultureMAA cultureMAA1 = refCultureMAADao.newInstance();
            cultureMAABinder.copyExcluding(refCultureMAA, cultureMAA1, TopiaEntity.PROPERTY_TOPIA_ID);
            r1Maa.add(cultureMAA1);
        }
    }

    @Test
    public void getDomainForCodeTest() throws IOException {
        testDatas.createTestNetworks();
        DomainDto domainDto = domainService.getActiveOrUnactiveDomainForCode("domaine_baulon_code_0");
        Domain domain = domainDao.forTopiaIdEquals(domainDto.getTopiaId()).findUnique();
        Domain otherDomain = domainDao.newInstance();
        Binder<Domain, Domain> domainBinder = BinderFactory.newBinder(Domain.class, Domain.class);
        domainBinder.copyExcluding(domain, otherDomain, Domain.PROPERTY_TOPIA_ID, Domain.PROPERTY_WEATHER_STATIONS);
        domain.setActive(false);
        domainDao.update(domain);
        otherDomain.setCampaign(otherDomain.getCampaign() + 1);
        domainDao.create(otherDomain);

        domainDto = domainService.getActiveOrUnactiveDomainForCode("domaine_baulon_code_0");
        Assertions.assertEquals(otherDomain.getTopiaId(), domainDto.getTopiaId());
    }

    @Test
    public void testPersistCattle() throws IOException {

        testDatas.createTestNetworks();
        // tests
        final LivestockUnitTopiaDao livestockUnitDao = getPersistenceContext().getLivestockUnitDao();
        final RefCattleAnimalTypeTopiaDao refCattleAnimalTypeDao = getPersistenceContext().getRefCattleAnimalTypeDao();
        final RefCattleRationAlimentTopiaDao refCattleRationAlimentDao = getPersistenceContext().getRefCattleRationAlimentDao();

        List<LivestockUnit> livestockUnits = livestockUnitDao.findAll();
        livestockUnitDao.deleteAll(livestockUnits);

        List<RefCattleAnimalType> refCattleAnimalTypes = refCattleAnimalTypeDao.findAll();
        refCattleAnimalTypeDao.deleteAll(refCattleAnimalTypes);

        List<RefAnimalType> refAnimalTypes = refAnimalTypeDao.findAll();
        refAnimalTypeDao.deleteAll(refAnimalTypes);

        List<RefCattleRationAliment> refCattleRationAliments = refCattleRationAlimentDao.findAll();
        refCattleRationAlimentDao.deleteAll(refCattleRationAliments);

        getPersistenceContext().commit();

        // import required by service:
        // importRefCattleAnimalType import RefAnimalType as well
        if (refCattleAnimalTypeDao.count() == 0) {
            testDatas.importRefCattleAnimalType();
            getPersistenceContext().commit();
        }

        if (refCattleRationAlimentDao.count() == 0) {
            testDatas.importRefRationAliment();
            getPersistenceContext().commit();
        }

        livestockUnits = new ArrayList<>();

        RefAnimalType bovinsLait = refAnimalTypeDao.forAnimalTypeEquals("Bovins lait").findUnique();
        LivestockUnit lsLivestockUnitBovin = new LivestockUnitImpl();
        lsLivestockUnitBovin.setRefAnimalType(bovinsLait);
        livestockUnits.add(lsLivestockUnitBovin);

        Cattle cattleBovinsLaitMales0_1_an = new CattleImpl();
        RefCattleAnimalType bovinsLaitMales0_1_an = refCattleAnimalTypeDao.forAnimalTypeEquals("Mâles 0-1 an").findAny();
        cattleBovinsLaitMales0_1_an.setAnimalType(bovinsLaitMales0_1_an);
        cattleBovinsLaitMales0_1_an.setNumberOfHeads(40);
        lsLivestockUnitBovin.addCattles(cattleBovinsLaitMales0_1_an);

        Ration ration = new RationImpl();
        RefCattleRationAliment refAliment = refCattleRationAlimentDao.forAlimentTypeEquals("Ensilage herbe").findUnique();
        cattleBovinsLaitMales0_1_an.addRations(ration);

        Aliment aliment = new AlimentImpl();
        ration.addAliments(aliment);
        aliment.setAliment(refAliment);

        Domain domain = domainService.getAllDomains().getFirst();
        domain = domainService.createOrUpdateDomain(
                domain,
                domain.getLocation().getTopiaId(),
                null,
                null,
                null,
                41,
                null,
                null,
                null,
                null,
                livestockUnits,
                null,
                null
        );

        LivestockUnit persistedLivestockUnit = livestockUnitDao.forDomainEquals(domain).findUnique();
        Assertions.assertNotNull(persistedLivestockUnit);

        Collection<Cattle> persistedCattles = persistedLivestockUnit.getCattles();
        Assertions.assertEquals(1, persistedCattles.size());

        // sauvegarde de nouveau le domaine sans passer les attelier d'élevage en paramètre
        // ceux-ci ne doivent pas être supprimés

        domain = domainService.createOrUpdateDomain(
                domain,
                domain.getLocation().getTopiaId(),
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        persistedLivestockUnit = livestockUnitDao.forDomainEquals(domain).findUnique();
        Assertions.assertNotNull(persistedLivestockUnit);

        persistedCattles = persistedLivestockUnit.getCattles();
        Assertions.assertEquals(1, persistedCattles.size());

    }

    @Test
    public void testDuplicateCattle() throws IOException, DomainExtendException {
        testDatas.createTestNetworks();

        // tests
        final LivestockUnitTopiaDao livestockUnitDao = getPersistenceContext().getLivestockUnitDao();
        final RefCattleAnimalTypeTopiaDao refCattleAnimalTypeDao = getPersistenceContext().getRefCattleAnimalTypeDao();
        final RefCattleRationAlimentTopiaDao refCattleRationAlimentDao = getPersistenceContext().getRefCattleRationAlimentDao();

        List<LivestockUnit> livestockUnits = livestockUnitDao.findAll();
        livestockUnitDao.deleteAll(livestockUnits);

        List<RefCattleAnimalType> refCattleAnimalTypes = refCattleAnimalTypeDao.findAll();
        refCattleAnimalTypeDao.deleteAll(refCattleAnimalTypes);

        List<RefAnimalType> refAnimalTypes = refAnimalTypeDao.findAll();
        refAnimalTypeDao.deleteAll(refAnimalTypes);

        List<RefCattleRationAliment> refCattleRationAliments = refCattleRationAlimentDao.findAll();
        refCattleRationAlimentDao.deleteAll(refCattleRationAliments);

        // import required by service:
        // importRefCattleAnimalType import RefAnimalType as well
        testDatas.importRefCattleAnimalType();
        testDatas.importRefRationAliment();

        livestockUnits = new ArrayList<>();

        RefAnimalType bovinsLait = refAnimalTypeDao.forAnimalTypeEquals("Bovins lait").findUnique();
        LivestockUnit lsLivestockUnitBovin = new LivestockUnitImpl();
        lsLivestockUnitBovin.setRefAnimalType(bovinsLait);
        livestockUnits.add(lsLivestockUnitBovin);

        Cattle cattleBovinsLaitMales0_1_an = new CattleImpl();
        RefCattleAnimalType bovinsLaitMales0_1_an = refCattleAnimalTypeDao.forAnimalTypeEquals("Mâles 0-1 an").findAny();
        cattleBovinsLaitMales0_1_an.setAnimalType(bovinsLaitMales0_1_an);
        cattleBovinsLaitMales0_1_an.setNumberOfHeads(40);
        lsLivestockUnitBovin.addCattles(cattleBovinsLaitMales0_1_an);

        Ration ration = new RationImpl();
        RefCattleRationAliment refAliment = refCattleRationAlimentDao.forAlimentTypeEquals("Ensilage herbe").findUnique();
        cattleBovinsLaitMales0_1_an.addRations(ration);

        Aliment aliment = new AlimentImpl();
        ration.addAliments(aliment);
        aliment.setAliment(refAliment);

        Domain domain = domainService.getAllDomains().getFirst();
        domain = domainService.createOrUpdateDomain(
                domain,
                domain.getLocation().getTopiaId(),
                null,
                null,
                null,
                41,
                null,
                null,
                null,
                null,
                livestockUnits,
                null,
                null
        );

        LivestockUnit persistedLivestockUnit = livestockUnitDao.forDomainEquals(domain).findUnique();
        Assertions.assertNotNull(persistedLivestockUnit);

        Collection<Cattle> persistedCattles = persistedLivestockUnit.getCattles();
        Assertions.assertEquals(1, persistedCattles.size());

        String cattleCode2013 = Lists.newArrayList(persistedCattles).getFirst().getCode();

        Assertions.assertTrue(StringUtils.isNoneBlank(cattleCode2013));

        // sauvegarde de nouveau le domaine sans passer les attelier d'élevage en paramètre
        // ceux-ci ne doivent pas être supprimés

        domain = domainService.extendDomain(domain.getTopiaId(), 2014);

        persistedLivestockUnit = livestockUnitDao.forDomainEquals(domain).findUnique();
        Assertions.assertNotNull(persistedLivestockUnit);

        persistedCattles = persistedLivestockUnit.getCattles();
        Assertions.assertEquals(1, persistedCattles.size());

        String cattleCode2014 = Lists.newArrayList(persistedCattles).getFirst().getCode();

        Assertions.assertEquals(cattleCode2013, cattleCode2014);
    }

    @Test
    public void testCattleUsageOnEffectivePhase() throws IOException {

        final LivestockUnitTopiaDao livestockUnitDao = getPersistenceContext().getLivestockUnitDao();
        final RefCattleAnimalTypeTopiaDao refCattleAnimalTypeDao = getPersistenceContext().getRefCattleAnimalTypeDao();
        final RefCattleRationAlimentTopiaDao refCattleRationAlimentDao = getPersistenceContext().getRefCattleRationAlimentDao();

        List<LivestockUnit> livestockUnits = livestockUnitDao.findAll();
        livestockUnitDao.deleteAll(livestockUnits);

        List<RefCattleAnimalType> refCattleAnimalTypes = refCattleAnimalTypeDao.findAll();
        refCattleAnimalTypeDao.deleteAll(refCattleAnimalTypes);

        List<RefAnimalType> refAnimalTypes = refAnimalTypeDao.findAll();
        refAnimalTypeDao.deleteAll(refAnimalTypes);

        List<RefCattleRationAliment> refCattleRationAliments = refCattleRationAlimentDao.findAll();
        refCattleRationAlimentDao.deleteAll(refCattleRationAliments);

        // import required by service:
        // importRefCattleAnimalType import RefAnimalType as well
        testDatas.importRefCattleAnimalType();
        testDatas.importRefRationAliment();

        // test datas
        testDatas.createEffectiveInterventions();

        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();
        Domain baulon = zpPlotBaulon1.getPlot().getDomain();

        livestockUnits = new ArrayList<>();

        RefAnimalType bovinsLait = refAnimalTypeDao.forAnimalTypeEquals("Bovins lait").findUnique();
        LivestockUnit lsLivestockUnitBovin = new LivestockUnitImpl();
        lsLivestockUnitBovin.setRefAnimalType(bovinsLait);
        livestockUnits.add(lsLivestockUnitBovin);

        Cattle cattleBovinsLaitMales0_1_an = new CattleImpl();
        RefCattleAnimalType bovinsLaitMales0_1_an = refCattleAnimalTypeDao.forAnimalTypeEquals("Mâles 0-1 an").findAny();
        cattleBovinsLaitMales0_1_an.setAnimalType(bovinsLaitMales0_1_an);
        cattleBovinsLaitMales0_1_an.setNumberOfHeads(40);
        lsLivestockUnitBovin.addCattles(cattleBovinsLaitMales0_1_an);

        Ration ration = new RationImpl();
        RefCattleRationAliment refAliment = refCattleRationAlimentDao.forAlimentTypeEquals("Ensilage herbe").findUnique();
        cattleBovinsLaitMales0_1_an.addRations(ration);

        Aliment aliment = new AlimentImpl();
        ration.addAliments(aliment);
        aliment.setAliment(refAliment);

        baulon = domainService.createOrUpdateDomain(
                baulon,
                baulon.getLocation().getTopiaId(),
                null,
                null,
                null,
                41,
                null,
                null,
                null,
                null,
                livestockUnits,
                null,
                null
        );

        Map<String, UsageList> livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        UsageList<LivestockUnit> livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        UsageList<Cattle> cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        Map<String, Boolean> livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertFalse(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));

        Cattle cattle = cattleUsageList.getElements().getFirst();

        Map<String, Boolean> cattleUsageMap = cattleUsageList.getUsageMap();
        Assertions.assertFalse(cattleUsageMap.get(cattle.getTopiaId()));

        EffectiveCropCyclePhase phase = getPersistenceContext().getEffectiveCropCyclePhaseDao().forTopiaIdEquals("phase5_id").findUnique();
    
        EffectiveIntervention intervention = effectiveInterventionDao.forEffectiveCropCyclePhaseEquals(phase).addEquals(EffectiveIntervention.PROPERTY_TOPIA_ID, "BD_ID_4").findUnique();
        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                getPersistenceContext().getJpaSupport(),
                effectiveInterventionDao,
                List.of(intervention));
        RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        RefInterventionAgrosystTravailEDI paturage = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("W25").findAny();

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        HarvestingActionValorisationTopiaDao harvestingActionValorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();

        RefDestination destination = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, "libelle_espece_botanique",
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, "code_espece_botanique",
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, "code_qualifiant_AEE",
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destination",
                RefDestination.PROPERTY_SECTOR, Sector.POLYCULTURE_ELEVAGE,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_ACTIVE, true);

        HarvestingActionValorisation valorisation = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, "un code espece",
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2018,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2018,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.Q_HA_TO_STANDARD_HUMIDITY,
                HarvestingActionValorisation.PROPERTY_QUALITY_CRITERIA, new ArrayList<>(),
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 80d);

        Collection<HarvestingActionValorisation> valorisations = Lists.newArrayList(valorisation);

        HarvestingActionTopiaDao harvestingActionDao = getPersistenceContext().getHarvestingActionDao();
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                HarvestingAction.PROPERTY_MAIN_ACTION, paturage,
                HarvestingAction.PROPERTY_CATTLE_CODE, cattle.getCode(),
                HarvestingAction.PROPERTY_VALORISATIONS, valorisations
        );

        livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());
        cattleUsageMap = cattleUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertTrue(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));
        Assertions.assertTrue(cattleUsageMap.get(cattle.getTopiaId()));
    }

    @Test
    public void testCattleUsageOnEffectiveNode() throws IOException {

        final LivestockUnitTopiaDao livestockUnitDao = getPersistenceContext().getLivestockUnitDao();
        final RefAnimalTypeTopiaDao refAnimalTypeDao = getPersistenceContext().getRefAnimalTypeDao();
        final RefCattleAnimalTypeTopiaDao refCattleAnimalTypeDao = getPersistenceContext().getRefCattleAnimalTypeDao();
        final RefCattleRationAlimentTopiaDao refCattleRationAlimentDao = getPersistenceContext().getRefCattleRationAlimentDao();

        List<LivestockUnit> livestockUnits = livestockUnitDao.findAll();
        livestockUnitDao.deleteAll(livestockUnits);

        List<RefCattleAnimalType> refCattleAnimalTypes = refCattleAnimalTypeDao.findAll();
        refCattleAnimalTypeDao.deleteAll(refCattleAnimalTypes);

        List<RefAnimalType> refAnimalTypes = refAnimalTypeDao.findAll();
        refAnimalTypeDao.deleteAll(refAnimalTypes);

        List<RefCattleRationAliment> refCattleRationAliments = refCattleRationAlimentDao.findAll();
        refCattleRationAlimentDao.deleteAll(refCattleRationAliments);

        // import required by service:
        // importRefCattleAnimalType import RefAnimalType as well
        testDatas.importRefCattleAnimalType();
        testDatas.importRefRationAliment();

        // test datas
        testDatas.createEffectiveInterventions();

        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();
        Domain baulon = zpPlotBaulon1.getPlot().getDomain();

        livestockUnits = new ArrayList<>();

        RefAnimalType bovinsLait = refAnimalTypeDao.forAnimalTypeEquals("Bovins lait").findUnique();
        LivestockUnit lsLivestockUnitBovin = new LivestockUnitImpl();
        lsLivestockUnitBovin.setRefAnimalType(bovinsLait);
        livestockUnits.add(lsLivestockUnitBovin);

        Cattle cattleBovinsLaitMales0_1_an = new CattleImpl();
        RefCattleAnimalType bovinsLaitMales0_1_an = refCattleAnimalTypeDao.forAnimalTypeEquals("Mâles 0-1 an").findAny();
        cattleBovinsLaitMales0_1_an.setAnimalType(bovinsLaitMales0_1_an);
        cattleBovinsLaitMales0_1_an.setNumberOfHeads(40);
        lsLivestockUnitBovin.addCattles(cattleBovinsLaitMales0_1_an);

        Ration ration = new RationImpl();
        RefCattleRationAliment refAliment = refCattleRationAlimentDao.forAlimentTypeEquals("Ensilage herbe").findUnique();
        cattleBovinsLaitMales0_1_an.addRations(ration);

        Aliment aliment = new AlimentImpl();
        ration.addAliments(aliment);
        aliment.setAliment(refAliment);

        baulon = domainService.createOrUpdateDomain(
                baulon,
                baulon.getLocation().getTopiaId(),
                null,
                null,
                null,
                41,
                null,
                null,
                null,
                null,
                livestockUnits,
                null,
                null
        );

        Map<String, UsageList> livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        UsageList<LivestockUnit> livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        UsageList<Cattle> cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        Map<String, Boolean> livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertFalse(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));

        Cattle cattle = cattleUsageList.getElements().getFirst();

        Map<String, Boolean> cattleUsageMap = cattleUsageList.getUsageMap();
        Assertions.assertFalse(cattleUsageMap.get(cattle.getTopiaId()));

        EffectiveCropCycleNode node0 = getPersistenceContext().getEffectiveCropCycleNodeDao().forTopiaIdEquals("BD_ID_NODE_0").findUnique();
        
        EffectiveIntervention intervention = effectiveInterventionDao.forEffectiveCropCycleNodeEquals(node0).addEquals(EffectiveIntervention.PROPERTY_TOPIA_ID, "BD_ID_3").findUnique();

        EffectiveCropCycleServiceTest.validateTopiaCreateDatepersisted(
                jpaSupport,
                effectiveInterventionDao,
                effectiveInterventionDao.forEffectiveCropCycleNodeEquals(node0).findAll());

        RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        RefInterventionAgrosystTravailEDI paturage = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("W25").findAny();

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        HarvestingActionValorisationTopiaDao harvestingActionValorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();

        RefDestination destination = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, "libelle_espece_botanique",
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, "code_espece_botanique",
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, "code_qualifiant_AEE",
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destination",
                RefDestination.PROPERTY_SECTOR, Sector.POLYCULTURE_ELEVAGE,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_ACTIVE, true);

        HarvestingActionValorisation valorisation = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, "un code espece",
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2018,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2018,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.Q_HA_TO_STANDARD_HUMIDITY,
                HarvestingActionValorisation.PROPERTY_QUALITY_CRITERIA, new ArrayList<>(),
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 80d);

        Collection<HarvestingActionValorisation> valorisations = Lists.newArrayList(valorisation);

        HarvestingActionTopiaDao harvestingActionDao = getPersistenceContext().getHarvestingActionDao();
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                HarvestingAction.PROPERTY_MAIN_ACTION, paturage,
                HarvestingAction.PROPERTY_CATTLE_CODE, cattle.getCode(),
                HarvestingAction.PROPERTY_VALORISATIONS, valorisations
        );

        livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());
        cattleUsageMap = cattleUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertTrue(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));
        Assertions.assertTrue(cattleUsageMap.get(cattle.getTopiaId()));
    }

    @Test
    public void testCattleUsageOnPracticedPhase() throws IOException {

        final LivestockUnitTopiaDao livestockUnitDao = getPersistenceContext().getLivestockUnitDao();
        final RefCattleAnimalTypeTopiaDao refCattleAnimalTypeDao = getPersistenceContext().getRefCattleAnimalTypeDao();
        final RefCattleRationAlimentTopiaDao refCattleRationAlimentDao = getPersistenceContext().getRefCattleRationAlimentDao();

        List<LivestockUnit> livestockUnits = livestockUnitDao.findAll();
        livestockUnitDao.deleteAll(livestockUnits);

        List<RefCattleAnimalType> refCattleAnimalTypes = refCattleAnimalTypeDao.findAll();
        refCattleAnimalTypeDao.deleteAll(refCattleAnimalTypes);

        List<RefAnimalType> refAnimalTypes = refAnimalTypeDao.findAll();
        refAnimalTypeDao.deleteAll(refAnimalTypes);

        List<RefCattleRationAliment> refCattleRationAliments = refCattleRationAlimentDao.findAll();
        refCattleRationAlimentDao.deleteAll(refCattleRationAliments);

        // import required by service:
        // importRefCattleAnimalType import RefAnimalType as well
        testDatas.importRefCattleAnimalType();
        testDatas.importRefRationAliment();

        // test datas
        testDatas.createEffectiveInterventions();

        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();
        Domain baulon = zpPlotBaulon1.getPlot().getDomain();

        livestockUnits = new ArrayList<>();

        RefAnimalType bovinsLait = refAnimalTypeDao.forAnimalTypeEquals("Bovins lait").findUnique();
        LivestockUnit lsLivestockUnitBovin = new LivestockUnitImpl();
        lsLivestockUnitBovin.setRefAnimalType(bovinsLait);
        livestockUnits.add(lsLivestockUnitBovin);

        Cattle cattleBovinsLaitMales0_1_an = new CattleImpl();
        RefCattleAnimalType bovinsLaitMales0_1_an = refCattleAnimalTypeDao.forAnimalTypeEquals("Mâles 0-1 an").findAny();
        cattleBovinsLaitMales0_1_an.setAnimalType(bovinsLaitMales0_1_an);
        cattleBovinsLaitMales0_1_an.setNumberOfHeads(40);
        lsLivestockUnitBovin.addCattles(cattleBovinsLaitMales0_1_an);

        Ration ration = new RationImpl();
        RefCattleRationAliment refAliment = refCattleRationAlimentDao.forAlimentTypeEquals("Ensilage herbe").findUnique();
        cattleBovinsLaitMales0_1_an.addRations(ration);

        Aliment aliment = new AlimentImpl();
        ration.addAliments(aliment);
        aliment.setAliment(refAliment);

        baulon = domainService.createOrUpdateDomain(
                baulon,
                baulon.getLocation().getTopiaId(),
                null,
                null,
                null,
                41,
                null,
                null,
                null,
                null,
                livestockUnits,
                null,
                null);

        Map<String, UsageList> livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        UsageList<LivestockUnit> livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        UsageList<Cattle> cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        Map<String, Boolean> livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertFalse(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));

        Cattle cattle = cattleUsageList.getElements().getFirst();

        Map<String, Boolean> cattleUsageMap = cattleUsageList.getUsageMap();
        Assertions.assertFalse(cattleUsageMap.get(cattle.getTopiaId()));

        GrowingSystem growingSystem = zpPlotBaulon1.getPlot().getGrowingSystem();

        PracticedSystem practicedSystem = testDatas.createPracticedSystemWhithGSCampaign(growingSystem);

        CroppingPlanEntry aCrop = testDatas.findCroppingPlanEntries(baulon.getTopiaId()).getFirst();

        // cycle
        PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao = getPersistenceContext().getPracticedPerennialCropCycleDao();
        PracticedPerennialCropCycle ppcc = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, aCrop.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 30d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT);

        // phases
        PracticedCropCyclePhaseTopiaDao cropCyclePhaseDao = getPersistenceContext().getPracticedCropCyclePhaseDao();
        PracticedCropCyclePhase chardonnayPhasePleineProduction = cropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 0,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, ppcc);

        // phases
        PracticedCropCyclePhase phasePleineProduction = cropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 15,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, ppcc);

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        ppcc.addCropCyclePhases(chardonnayPhasePleineProduction);
        ppcc.addCropCyclePhases(phasePleineProduction);

        // premiere intervention sur un cycle
        PracticedInterventionTopiaDao practicedInterventionDao = getPersistenceContext().getPracticedInterventionDao();
        PracticedIntervention interBaulonEpandage = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, phasePleineProduction,
                PracticedIntervention.PROPERTY_NAME, "Middle intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                PracticedIntervention.PROPERTY_WORK_RATE, 0.9,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.H_HA,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "01/4",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "10/4",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1,// varie de +0 à 1
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.5);

        RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        RefInterventionAgrosystTravailEDI paturage = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("W25").findAny();

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        HarvestingActionValorisationTopiaDao harvestingActionValorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();

        RefDestination destination = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, "libelle_espece_botanique",
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, "code_espece_botanique",
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, "code_qualifiant_AEE",
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destination",
                RefDestination.PROPERTY_SECTOR, Sector.POLYCULTURE_ELEVAGE,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_ACTIVE, true);

        HarvestingActionValorisation valorisation = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, "un code espece",
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, 2018,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, 2018,
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.Q_HA_TO_STANDARD_HUMIDITY,
                HarvestingActionValorisation.PROPERTY_QUALITY_CRITERIA, new ArrayList<>(),
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 80d);

        Collection<HarvestingActionValorisation> valorisations = Lists.newArrayList(valorisation);

        HarvestingActionTopiaDao harvestingActionDao = getPersistenceContext().getHarvestingActionDao();
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_PRACTICED_INTERVENTION, interBaulonEpandage,
                HarvestingAction.PROPERTY_MAIN_ACTION, paturage,
                HarvestingAction.PROPERTY_CATTLE_CODE, cattle.getCode(),
                HarvestingAction.PROPERTY_VALORISATIONS, valorisations
        );

        livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());
        cattleUsageMap = cattleUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertTrue(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));
        Assertions.assertTrue(cattleUsageMap.get(cattle.getTopiaId()));
    }

    @Test
    public void testCattleUsageOnPracticedSeasonal() throws IOException {

        final LivestockUnitTopiaDao livestockUnitDao = getPersistenceContext().getLivestockUnitDao();
        final RefCattleAnimalTypeTopiaDao refCattleAnimalTypeDao = getPersistenceContext().getRefCattleAnimalTypeDao();
        final RefCattleRationAlimentTopiaDao refCattleRationAlimentDao = getPersistenceContext().getRefCattleRationAlimentDao();

        List<LivestockUnit> livestockUnits = livestockUnitDao.findAll();
        livestockUnitDao.deleteAll(livestockUnits);

        List<RefCattleAnimalType> refCattleAnimalTypes = refCattleAnimalTypeDao.findAll();
        refCattleAnimalTypeDao.deleteAll(refCattleAnimalTypes);

        List<RefAnimalType> refAnimalTypes = refAnimalTypeDao.findAll();
        refAnimalTypeDao.deleteAll(refAnimalTypes);

        List<RefCattleRationAliment> refCattleRationAliments = refCattleRationAlimentDao.findAll();
        refCattleRationAlimentDao.deleteAll(refCattleRationAliments);

        // import required by service:
        // importRefCattleAnimalType import RefAnimalType as well
        testDatas.importRefCattleAnimalType();
        testDatas.importRefRationAliment();

        // test datas
        testDatas.createEffectiveInterventions();

        List<Zone> zones = testDatas.getPlotNameOrderedZones();
        Zone zpPlotBaulon1 = zones.getFirst();
        Domain baulon = zpPlotBaulon1.getPlot().getDomain();

        livestockUnits = new ArrayList<>();

        RefAnimalType bovinsLait = refAnimalTypeDao.forAnimalTypeEquals("Bovins lait").findUnique();
        LivestockUnit lsLivestockUnitBovin = new LivestockUnitImpl();
        lsLivestockUnitBovin.setRefAnimalType(bovinsLait);
        livestockUnits.add(lsLivestockUnitBovin);

        Cattle cattleBovinsLaitMales0_1_an = new CattleImpl();
        RefCattleAnimalType bovinsLaitMales0_1_an = refCattleAnimalTypeDao.forAnimalTypeEquals("Mâles 0-1 an").findAny();
        cattleBovinsLaitMales0_1_an.setAnimalType(bovinsLaitMales0_1_an);
        cattleBovinsLaitMales0_1_an.setNumberOfHeads(40);
        lsLivestockUnitBovin.addCattles(cattleBovinsLaitMales0_1_an);

        Ration ration = new RationImpl();
        RefCattleRationAliment refAliment = refCattleRationAlimentDao.forAlimentTypeEquals("Ensilage herbe").findUnique();
        cattleBovinsLaitMales0_1_an.addRations(ration);

        Aliment aliment = new AlimentImpl();
        ration.addAliments(aliment);
        aliment.setAliment(refAliment);

        baulon = domainService.createOrUpdateDomain(
                baulon,
                baulon.getLocation().getTopiaId(),
                null,
                null,
                null,
                41,
                null,
                null,
                null,
                null,
                livestockUnits,
                null,
                null
        );

        Map<String, UsageList> livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        UsageList<LivestockUnit> livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        UsageList<Cattle> cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        Map<String, Boolean> livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertFalse(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));

        Cattle cattle = cattleUsageList.getElements().getFirst();

        Map<String, Boolean> cattleUsageMap = cattleUsageList.getUsageMap();
        Assertions.assertFalse(cattleUsageMap.get(cattle.getTopiaId()));

        GrowingSystem growingSystem = zpPlotBaulon1.getPlot().getGrowingSystem();

        testDatas.createPracticedSystemSeasonnalCycle(baulon, growingSystem);

        CroppingPlanEntry aCrop = testDatas.findCroppingPlanEntries(baulon.getTopiaId()).getFirst();

        // connection
        PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionDao = getPersistenceContext().getPracticedCropCycleConnectionDao();
        PracticedCropCycleConnection connection = practicedCropCycleConnectionDao.forTopiaIdEquals("ble-to-orge-id").findUnique();

        // premiere intervention sur un cycle
        PracticedInterventionTopiaDao practicedInterventionDao = getPersistenceContext().getPracticedInterventionDao();
        PracticedIntervention interBaulonEpandage = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_CONNECTION, connection,
                PracticedIntervention.PROPERTY_NAME, "Middle intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX,
                PracticedIntervention.PROPERTY_WORK_RATE, 0.9,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.H_HA,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "01/4",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "10/4",
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1,// varie de +0 à 1
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.5);

        RefInterventionAgrosystTravailEDITopiaDao refInterventionAgrosystTravailEDIDao = getPersistenceContext().getRefInterventionAgrosystTravailEDIDao();
        RefInterventionAgrosystTravailEDI paturage = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("W25").findAny();

        RefDestinationTopiaDao refDestinationDao = getPersistenceContext().getRefDestinationDao();
        HarvestingActionValorisationTopiaDao harvestingActionValorisationDao = getPersistenceContext().getHarvestingActionValorisationDao();

        RefDestination destination = refDestinationDao.create(
                RefDestination.PROPERTY_ESPECE, "libelle_espece_botanique",
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, "code_espece_botanique",
                RefDestination.PROPERTY_CODE_QUALIFIANT__AEE, "code_qualifiant_AEE",
                RefDestination.PROPERTY_CODE_DESTINATION__A, UUID.randomUUID().toString(),
                RefDestination.PROPERTY_DESTINATION, "destination",
                RefDestination.PROPERTY_SECTOR, Sector.POLYCULTURE_ELEVAGE,
                RefDestination.PROPERTY_YEALD_UNIT, YealdUnit.HL_HA,
                RefDestination.PROPERTY_ACTIVE, true);

        HarvestingActionValorisation valorisation = harvestingActionValorisationDao.create(
                HarvestingActionValorisation.PROPERTY_SPECIES_CODE, "un code espece",
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_BEGIN_MARKETING_PERIOD_CAMPAIGN, baulon.getCampaign(),
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD, 6,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_DECADE, 1,
                HarvestingActionValorisation.PROPERTY_ENDING_MARKETING_PERIOD_CAMPAIGN, baulon.getCampaign(),
                HarvestingActionValorisation.PROPERTY_DESTINATION, destination,
                HarvestingActionValorisation.PROPERTY_IS_ORGANIC_CROP, false,
                HarvestingActionValorisation.PROPERTY_YEALD_UNIT, YealdUnit.Q_HA_TO_STANDARD_HUMIDITY,
                HarvestingActionValorisation.PROPERTY_QUALITY_CRITERIA, new ArrayList<>(),
                HarvestingActionValorisation.PROPERTY_SALES_PERCENT, 100,
                HarvestingActionValorisation.PROPERTY_YEALD_AVERAGE, 80d);

        Collection<HarvestingActionValorisation> valorisations = Lists.newArrayList(valorisation);

        HarvestingActionTopiaDao harvestingActionDao = getPersistenceContext().getHarvestingActionDao();
        harvestingActionDao.create(
                HarvestingAction.PROPERTY_PRACTICED_INTERVENTION, interBaulonEpandage,
                HarvestingAction.PROPERTY_MAIN_ACTION, paturage,
                HarvestingAction.PROPERTY_CATTLE_CODE, cattle.getCode(),
                HarvestingAction.PROPERTY_VALORISATIONS, valorisations
        );

        livestockAndCattleUsageLists = domainService.getUsedLivestocksAndCattles(baulon.getTopiaId());

        livestockUsageList = livestockAndCattleUsageLists.get(LivestockUnit.class.getName());
        cattleUsageList = livestockAndCattleUsageLists.get(Cattle.class.getName());
        cattleUsageMap = cattleUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageList.getElements().size());

        livestockUsageMap = livestockUsageList.getUsageMap();

        Assertions.assertEquals(1, livestockUsageMap.size());

        Assertions.assertTrue(livestockUsageMap.get(lsLivestockUnitBovin.getTopiaId()));
        Assertions.assertTrue(cattleUsageMap.get(cattle.getTopiaId()));
    }


    @Test
    public void testCreateSeedingLotWithSpeciesDomainInput() throws IOException {

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);

        testDatas.createDemoRefLocation();
        testDatas.createDemoRefLegalStatus();

        testDatas.importSolArvalis();
        testDatas.importInterventionAgrosystTravailEdi();
        testDatas.importRefAnimalType();
        testDatas.importOTEX();

        testDatas.importRefEspeces();
        testDatas.importVarieteGeves();
        testDatas.importEspecesToVarietes(true);
        testDatas.importRefVarietePlantGrape();

        List<RefLocation> locations = refLocationDao.findAll();

        RefLegalStatus exploitantAgricoleStatus = refLegalStatusDao.forCode_INSEEEquals(16).findAny();


        // Ref Station Meteo
        // Ref Station Meteo
        RefStationMeteo refStationMeteo1 = testDatas.createRefStationMeteo("47320-Bourran", "47320", "Bourran", "47 038 002", "N44.33186 E0.41321");
        RefStationMeteo refStationMeteo2 = testDatas.createRefStationMeteo("84000-Avignon", "84000", "Avignon", "84 007 004", "N43.91616 E4.87705");

        WeatherStation wsBaulon1 = testDatas.createWeatherStation(refStationMeteo1);
        WeatherStation wsBaulon2 = testDatas.createWeatherStation(refStationMeteo2);

        final RefOTEX otex18 = refOTEXDao.forCode_OTEX_18_postesEquals(41).findAny();

        Domain domainBaulon = domainService.newDomain();
        domainBaulon.setName(TestDatas.BAULON_DOMAIN_NAME);
        domainBaulon.setSiret("442 116 703 00046");
        domainBaulon.setCampaign(2013);
        domainBaulon.setMainContact("Annie Verssaire");
        domainBaulon.setType(DomainType.DOMAINE_EXPERIMENTAL);
        domainBaulon.setDescription("Lorem ipsum dolor sit amet");
        domainBaulon.setWeatherStations(Lists.newArrayList(wsBaulon1, wsBaulon2));
        domainBaulon.setDefaultWeatherStation(wsBaulon1);
        domainBaulon.setUaaActionPart(100.0);
        domainBaulon.setOtherWorkForce(9.0);
        domainBaulon.setTemporaryEmployeesWorkForce(1.0);

        final GeoPoint geoPoint = domainService.newGpsData();
        geoPoint.setDescription("données GPS de Baulon");
        geoPoint.setLongitude(-1.5374922874999584);
        geoPoint.setLatitude(47.26270948296243);
        geoPoint.setName("gpsD1");

        List<GeoPoint> domainBaulonGeoPoints = Lists.newArrayList(geoPoint);

        RefCattleRationAliment refAliment = testDatas.createRefCattleRationAliment("Ensilage maïs", "Kg MS/animal/jour");
        Aliment anAliment = domainService.newAliment();
        anAliment.setAliment(refAliment);
        anAliment.setQuantity(12.23);

        RefAnimalType refAnimalType = refAnimalTypeDao.forAnimalTypeEquals("Bovins lait").findUnique();
        RefCattleAnimalType refCattleAnimalType = testDatas.getRefCattleAnimalType(refAnimalType);
        Ration aRation = domainService.newRation();
        aRation.setAliments(Lists.newArrayList(anAliment));
        aRation.setStartingHalfMonth(0);
        aRation.setEndingHalfMonth(1);

        List<Ration> rations = Lists.newArrayList(aRation);
        final Cattle cattle = domainService.newCattle();
        cattle.setAnimalType(refCattleAnimalType);
        cattle.setRations(rations);

        LivestockUnit livestockUnit = domainService.newLivestockUnit();
        livestockUnit.setRefAnimalType(refAnimalType);
        livestockUnit.setLivestockUnitSize(100);
        livestockUnit.setComment("1000000 L de lait");
        livestockUnit.setCattles(Lists.newArrayList(cattle));

        List<LivestockUnit> livestockUnits = Lists.newArrayList(livestockUnit);

        final RefEspece refEspeceBleDurHiver = refEspeceDao.forNaturalId("ZAQ", "", "ZFB", "").findUnique();
        final RefEspece refEspeceBleDurPrintemps = refEspeceDao.forNaturalId("ZAQ", "", "ZFA", "").findUnique();
        final RefEspece refEspeceTriticale = refEspeceDao.forNaturalId("ZMI", "", "", "").findUnique();
        final RefEspece refEspeceVigne = refEspeceDao.forNaturalId("ZMO", "", "", "H61").findUnique();
        final RefEspece refEspecePommier = refEspeceDao.forNaturalId("G21", "H36", "", "").findUnique();
        final RefEspece refEspeceBleTendre = refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLH").findUnique();
        final RefEspece refBleTendreHiver = refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        final RefEspece refEspeceSoja = refEspeceDao.forNaturalId("ZEL", "", "", "").findUnique();
        final RefEspece refEspecePhacelie = refEspeceDao.forNaturalId("ZDL", "", "", "").findUnique();
        final RefEspece refEspeceOrge = refEspeceDao.forNaturalId("ZDH", "ZMV", "ZFA", "").findUnique();
        final RefEspece refEspeceCourgette = refEspeceDao.forNaturalId("ZBI", "", "", "").findUnique();
        final RefEspece refGombo = refEspeceDao.forNaturalId("I26", "", "", "").findUnique();
        final RefEspece refSapin = refEspeceDao.forNaturalId("I44", "", "", "").findUnique();
        final RefEspece refErable = refEspeceDao.forNaturalId("I19", "", "", "").findUnique();


        // num_Dossier
//        final RefVariete refVarieteCamelia = refVarieteGevesDao.forNaturalId(92791).findUnique();
//        final RefVariete refVarieteByblos = refVarieteGevesDao.forNaturalId(1008599).findUnique();
//        final RefVariete refVarieteBlackBeauty = refVarieteGevesDao.forNaturalId(51084).findUnique();
//        final RefVariete refVarieteAtenon = refVarieteGevesDao.forNaturalId(1013530).findUnique();
//        final RefVariete refVarieteAtoudur = refVarieteGevesDao.forNaturalId(1027573).findUnique();
//        final RefVariete refVarieteAgrilac = refVarieteGevesDao.forNaturalId(1011196).findUnique();
//        final RefVariete refVarieteBelleDeBoskoop = refVarieteGevesDao.forNaturalId(92430).findUnique();
//        final RefVariete refVarieteKamichis = refVarieteGevesDao.forNaturalId(1025870).findUnique();
        // codeVar
//        final RefVariete refVarieteSavagninBlancB = refVarietePlantGrapeDao.forNaturalId(257).findUnique();
        final RefVariete josselin = refVarieteGevesDao.forDenominationEquals("Josselin").findUnique();
        final RefVariete barok = refVarieteGevesDao.forDenominationEquals("Barok").findUnique();

        CroppingPlanEntry baulonMainEntry = createCroppingPlanEntry("Blé", CroppingEntryType.MAIN);
        CroppingPlanEntry baulonInterEntry = createCroppingPlanEntry("Orge", CroppingEntryType.INTERMEDIATE);
        CroppingPlanEntry baulonMelangeVarietesBle = createCroppingPlanEntry("Mélange variétés blé", CroppingEntryType.MAIN);
        CroppingPlanEntry baulonGombo = createCroppingPlanEntry("Gombo", CroppingEntryType.MAIN);
        CroppingPlanEntry baulonSapin = createCroppingPlanEntry("Sapin", CroppingEntryType.MAIN);
        CroppingPlanEntry baulonErable = createCroppingPlanEntry("Erable", CroppingEntryType.MAIN);

        CroppingPlanSpecies baulonMainSpecies = createCroppingPlanSpecies(refBleTendreHiver, baulonMainEntry, null);
        baulonMainEntry.addCroppingPlanSpecies(baulonMainSpecies);

        CroppingPlanSpecies baulonInterSpecies = createCroppingPlanSpecies(refBleTendreHiver, baulonInterEntry, null);
        baulonInterEntry.addCroppingPlanSpecies(baulonInterSpecies);

        CroppingPlanSpecies baulonbaulonMelangeVarietesBleJosselinSpecies = createCroppingPlanSpecies(refBleTendreHiver, baulonMelangeVarietesBle, josselin);
        CroppingPlanSpecies baulonbaulonMelangeVarietesBleBarokSpecies = createCroppingPlanSpecies(refBleTendreHiver, baulonMelangeVarietesBle, barok);
        baulonMelangeVarietesBle.addCroppingPlanSpecies(baulonbaulonMelangeVarietesBleJosselinSpecies);
        baulonMelangeVarietesBle.addCroppingPlanSpecies(baulonbaulonMelangeVarietesBleBarokSpecies);

        CroppingPlanSpecies gombo = createCroppingPlanSpecies(refGombo, baulonGombo, null);
        baulonGombo.addCroppingPlanSpecies(gombo);

        CroppingPlanSpecies sapin = createCroppingPlanSpecies(refSapin, baulonSapin, null);
        baulonSapin.addCroppingPlanSpecies(sapin);

        CroppingPlanSpecies erable = createCroppingPlanSpecies(refErable, baulonErable, null);
        baulonErable.addCroppingPlanSpecies(erable);

        i18nService.fillCroppingPlanEntryTranslationMaps(
                Lists.newArrayList(baulonMainEntry, baulonInterEntry, baulonMelangeVarietesBle, baulonGombo, baulonSapin, baulonErable),
                translationMap);

        CroppingPlanEntryDto baulonMainEntryDto = CroppingPlans.getDtoForCroppingPlanEntry(baulonMainEntry, translationMap);
        CroppingPlanEntryDto baulonInterEntryDto = CroppingPlans.getDtoForCroppingPlanEntry(baulonInterEntry, translationMap);
        CroppingPlanEntryDto baulonMelangeVarietesBleDto = CroppingPlans.getDtoForCroppingPlanEntry(baulonMelangeVarietesBle, translationMap);
        CroppingPlanEntryDto baulonGomboDto = CroppingPlans.getDtoForCroppingPlanEntry(baulonGombo, translationMap);
        CroppingPlanEntryDto baulonSapinDto = CroppingPlans.getDtoForCroppingPlanEntry(baulonSapin, translationMap);
        CroppingPlanEntryDto baulonErableDto = CroppingPlans.getDtoForCroppingPlanEntry(baulonErable, translationMap);

        List<CroppingPlanEntryDto> croppingPlanEntryDtos = Lists.newArrayList(
                baulonMainEntryDto,
                baulonInterEntryDto,
                baulonMelangeVarietesBleDto,
                baulonGomboDto,
                baulonSapinDto,
                baulonErableDto);

        InputPriceDto priceDto = InputPriceDto.builder()
                .category(InputPriceCategory.SEEDING_INPUT)
                .objectId("FAKE")// will be change later on the test
                .displayName("FAKE")
                .sourceUnit(SeedPlantUnit.KG_PAR_HA.name())
                .price(0.4)
                .priceUnit(PriceUnit.EURO_HA)
                .phytoProductType(null)
                .seedType(SeedType.SEMENCES_DE_FERME)
                .biologicalSeedInoculation(false)
                .chemicalTreatment(false)
                .includedTreatment(false)
                .organic(true).build();

        Collection<DomainSeedSpeciesInputDto> domainSeedSpeciesInputDtos = getCropSpeciesSeedSpeciesInputDtos(
                baulonMelangeVarietesBle.getCroppingPlanSpecies(), priceDto);
        DomainSeedLotInputDto lotDto = DomainInputStockServiceTest.getDomainSeedLotInputDto(
                baulonMelangeVarietesBleDto, domainSeedSpeciesInputDtos, true);

        Collection<DomainInputDto> domainInputStockUnits = Lists.newArrayList(lotDto);

        RefSolArvalis refSolArvalis0 = refSolArvalisDao.forNaturalId("AL0016500").findUnique();
        Ground ground = domainService.newSol();
        ground.setRefSolArvalis(refSolArvalis0);
        ground.setComment("Sol pour test");
        ground.setName("Un sol pour TU");
        ground.setImportance(1.0);
        ground.setValidated(false);
        List<Ground> grounds = Lists.newArrayList(ground);

        // 1: fr.inra.agrosyst.api.entities.referential.RefMaterielOutil_44d39d01-7f1a-4234-9c21-15fad6532cb2
        // 2: fr.inra.agrosyst.api.entities.referential.RefMaterielTraction_7e75192e-58c9-4721-9f8f-24ab193864ff
        // 3: fr.inra.agrosyst.api.entities.referential.RefMaterielTraction_8248b263-db80-4e36-92cb-7a4a17b97f3b
        List<RefMaterielOutil> materielOutils = testDatas.createRefMaterielOutils();


        // 1: fr.inra.agrosyst.api.entities.referential.RefMaterielOutil_44d39d01-7f1a-4234-9c21-15fad6532cb2
        // 2: fr.inra.agrosyst.api.entities.referential.RefMaterielTraction_7e75192e-58c9-4721-9f8f-24ab193864ff
        // 3: fr.inra.agrosyst.api.entities.referential.RefMaterielTraction_8248b263-db80-4e36-92cb-7a4a17b97f3b
        List<RefMaterielTraction> materielTractions = testDatas.createRefMaterielTraction();
        Equipment baulonTractor = domainService.newMateriel();
        baulonTractor.setTopiaId(DomainService.NEW_EQUIPMENT + "_" + UUID.randomUUID());
        baulonTractor.setName("RefMaterielTraction 0");
        baulonTractor.setDescription("RefMaterielTraction de Baulon");
        baulonTractor.setRefMateriel(materielTractions.get(0));

        Equipment baulonMaterielOutil0 = domainService.newMateriel();
        baulonMaterielOutil0.setTopiaId(DomainService.NEW_EQUIPMENT + "_" + UUID.randomUUID());
        baulonMaterielOutil0.setName("RefMaterielOutil 0");
        baulonMaterielOutil0.setDescription("RefMaterielOutil de Baulon");
        baulonMaterielOutil0.setRefMateriel(materielOutils.get(1));

        Equipment baulonMaterielOutil1 = domainService.newMateriel();
        baulonMaterielOutil1.setTopiaId(DomainService.NEW_EQUIPMENT + "_" + UUID.randomUUID());
        baulonMaterielOutil1.setName("RefMaterielTraction 1");
        baulonMaterielOutil1.setDescription("RefMaterielOutil de Baulon");
        baulonMaterielOutil1.setRefMateriel(materielOutils.get(0));

        List<Equipment> equipments = Lists.newArrayList(baulonTractor, baulonMaterielOutil0, baulonMaterielOutil1);

        RefInterventionAgrosystTravailEDI actionSEM = refInterventionAgrosystTravailEDIDao.forReference_codeEquals("SEM").findAnyOrNull();

        ToolsCoupling tc1 = domainService.newToolsCoupling();
        tc1.setMainsActions(Sets.newHashSet(actionSEM));
        tc1.setTractor(baulonTractor);
        tc1.setEquipments(Lists.newArrayList(baulonMaterielOutil0, baulonMaterielOutil1));
        tc1.setToolsCouplingName("TC domainBaulon");

        List<ToolsCoupling> toolsCouplings = Lists.newArrayList(tc1);

        Domain persistedDomain = domainService.createOrUpdateDomain(
                domainBaulon,
                locations.getFirst().getTopiaId(),
                exploitantAgricoleStatus.getTopiaId(),
                domainBaulonGeoPoints,
                croppingPlanEntryDtos,
                otex18.getCode_OTEX_18_postes(),
                null,
                grounds,
                equipments,
                toolsCouplings,
                livestockUnits,
                domainInputStockUnits,
                new ArrayList<>());

        DomainSeedLotInput domainSeedLotInputEntity = domainSeedLotInputDao.forDomainEquals(persistedDomain).findUnique();

        assertThat(domainSeedLotInputEntity.getDomain()).isEqualTo(persistedDomain);
        validateCommonDtoFields(
                lotDto,
                domainSeedLotInputEntity.getUsageUnit(),
                domainSeedLotInputEntity.isOrganic());

        Collection<DomainSeedSpeciesInput> domainSeedSpeciesInputs = domainSeedLotInputEntity.getDomainSeedSpeciesInput();
        assertThat(domainSeedLotInputEntity.sizeDomainSeedSpeciesInput()).isEqualTo(domainSeedSpeciesInputDtos.size());

        for (DomainSeedSpeciesInput domainSeedSpeciesInput : domainSeedSpeciesInputs) {
            SeedPrice seedPrice = domainSeedSpeciesInput.getSeedPrice();
            assertThat(seedPrice.getPrice()).isEqualTo(priceDto.getPrice());
        }

    }

    @Test
    public void testSpeciesArea() {
        String json = "{\"zbb_zbd__\":46,\"zcj_g99__\":32.5,\"zdg_zni__\":2,\"zdg_zni__\":2.5}";
        //            {"zbb_zbd__":46,"zcj_g99__":32.5,"zdg_zni__":2,"zdg_zni__":2.5}
        //            {"zbb_zbd__":46.0,"zcj_g99__":32.5,"zdg_zni__":2.5}
        Map<String, Double> speciesToAreaJson = domainService.safelyConvertDomainSpeciesToAreaJson(json);
        Gson gson = new AgrosystGsonSupplier().get();
        String jsonResult = gson.toJson(speciesToAreaJson);
        Assertions.assertEquals("{\"zbb_zbd__\":46.0,\"zcj_g99__\":32.5,\"zdg_zni__\":2.5}", jsonResult);
    }

    @Test
    public void testPasteCrops() throws IOException {

        testDatas.importRefEspeces();
        testDatas.importVarieteGeves();
        testDatas.createDemoRefLocation();

        RefLocation refLocation = refLocationDao.findAll().getFirst();
        Domain domainBaulon2013 = domainService.newDomain();
        domainBaulon2013.setName(TestDatas.BAULON_DOMAIN_NAME);
        domainBaulon2013.setUpdateDate(LocalDateTime.of(2024, 3, 6, 18, 21));
        domainBaulon2013.setLocation(refLocation);
        domainBaulon2013.setSiret("442 116 703 00046");
        domainBaulon2013.setCode(UUID.randomUUID().toString());
        domainBaulon2013.setCampaign(2013);
        domainBaulon2013.setMainContact("Annie Verssaire");
        domainBaulon2013.setType(DomainType.DOMAINE_EXPERIMENTAL);
        domainBaulon2013.setDescription("Lorem ipsum dolor sit amet");
        domainBaulon2013.setUaaActionPart(100.0);
        domainBaulon2013.setOtherWorkForce(9.0);
        domainBaulon2013.setTemporaryEmployeesWorkForce(1.0);
        domainDao.create(domainBaulon2013);

        Domain domainBaulon2014 = domainService.newDomain();
        domainBaulon2014.setName(TestDatas.BAULON_DOMAIN_NAME);
        domainBaulon2014.setUpdateDate(LocalDateTime.of(2024, 3, 6, 18, 21));
        domainBaulon2014.setLocation(refLocation);
        domainBaulon2014.setCode(domainBaulon2013.getCode());
        domainBaulon2014.setSiret("442 116 703 00046");
        domainBaulon2014.setCampaign(2014);
        domainBaulon2014.setMainContact("Annie Verssaire");
        domainBaulon2014.setType(DomainType.DOMAINE_EXPERIMENTAL);
        domainBaulon2014.setDescription("Lorem ipsum dolor sit amet");
        domainBaulon2014.setUaaActionPart(100.0);
        domainBaulon2014.setOtherWorkForce(9.0);
        domainBaulon2014.setTemporaryEmployeesWorkForce(1.0);
        domainDao.create(domainBaulon2014);


        final RefEspece refBleTendreHiver = refEspeceDao.forNaturalId("ZAR", "", "ZFB", "ZLJ").findUnique();
        final RefVariete refJosselin = refVarieteGevesDao.forDenominationEquals("Josselin").findUnique();

        final RefDestination rd0 = refDestinationDao.create(
                RefDestination.PROPERTY_ACTIVE, true,
                RefDestination.PROPERTY_DESTINATION, "Pour test",
                RefDestination.PROPERTY_SECTOR, Sector.MARAICHAGE,
                RefDestination.PROPERTY_CODE_ESPECE_BOTANIQUE, refBleTendreHiver.getCode_espece_botanique(),
                RefDestination.PROPERTY_CODE_DESTINATION__A, "DEST_CODE_0"
                );

        CroppingPlanEntry cpe = croppingPlanEntryDao.create(
             CroppingPlanEntry.PROPERTY_CODE, UUID.randomUUID().toString(),
             CroppingPlanEntry.PROPERTY_NAME, "Blé",
                CroppingPlanEntry.PROPERTY_TYPE, CroppingEntryType.MAIN,
                CroppingPlanEntry.PROPERTY_DOMAIN, domainBaulon2013
        );

        CroppingPlanSpecies bleTendreHiver = croppingPlanSpeciesDao.create(
                CroppingPlanSpecies.PROPERTY_CODE, UUID.randomUUID().toString(),
                CroppingPlanSpecies.PROPERTY_CROPPING_PLAN_ENTRY, cpe,
                CroppingPlanSpecies.PROPERTY_COMPAGNE, CompagneType.PLANTE_COMPAGNE,
                CroppingPlanSpecies.PROPERTY_DESTINATION, rd0,
                CroppingPlanSpecies.PROPERTY_SPECIES, refBleTendreHiver,
                CroppingPlanSpecies.PROPERTY_VARIETY, refJosselin,
                CroppingPlanSpecies.PROPERTY_SPECIES_AREA, 12
        );

        cpe.addCroppingPlanSpecies(bleTendreHiver);
        croppingPlanEntryDao.update(cpe);

        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);
        i18nService.fillCroppingPlanEntryTranslationMaps(
                Lists.newArrayList(cpe),
                translationMap);

        CroppingPlanEntryDto cpeDto = CroppingPlans.getDtoForCroppingPlanEntry(cpe, translationMap);

        domainService.pasteCrops(domainBaulon2013.getTopiaId(), List.of(domainBaulon2014.getTopiaId()), List.of(cpeDto));

        List<CroppingPlanEntry> copiedOnSameDomainCrop2024 = croppingPlanEntryDao.forDomainEquals(domainBaulon2014).findAll();
        Assertions.assertEquals(1, copiedOnSameDomainCrop2024.size());
        Assertions.assertTrue(isSimilarCrop(cpe, copiedOnSameDomainCrop2024.get(0), domainBaulon2013, domainBaulon2014));

        Assertions.assertTrue(isTopiaAttributSeted(copiedOnSameDomainCrop2024.get(0)));

    }

    private boolean isTopiaAttributSeted(CroppingPlanEntry croppingPlanEntry) {
        boolean speciesValid = CollectionUtils.emptyIfNull(croppingPlanEntry.getCroppingPlanSpecies()).stream().anyMatch(
                te -> te.getTopiaId() != null && te.getTopiaCreateDate() != null
        );
        return speciesValid &&
                croppingPlanEntry.getTopiaId() != null
                && croppingPlanEntry.getTopiaCreateDate() != null;
    }

    protected boolean isSimilarCrop(CroppingPlanEntry from, CroppingPlanEntry to, Domain fromDomain, Domain toDomain) {
        if (fromDomain.getTopiaId().equals(toDomain.getTopiaId())) {
            if (from.getCode().contentEquals(to.getCode())) return false;
        } else if (fromDomain.getCode().contentEquals(to.getCode())) {
            if (!from.getCode().contentEquals(to.getCode())) return false;
        }
        if (CollectionUtils.size(from.getCroppingPlanSpecies()) != CollectionUtils.size(to.getCroppingPlanSpecies())) return false;

        Map<String, CroppingPlanSpecies> fromSpeciesByCode = CollectionUtils.emptyIfNull(from.getCroppingPlanSpecies()).stream().collect(Collectors.toMap((CroppingPlanSpecies::getCode), Function.identity()));
        Map<String, CroppingPlanSpecies> toSpeciesByCode = CollectionUtils.emptyIfNull(to.getCroppingPlanSpecies()).stream().collect(Collectors.toMap((CroppingPlanSpecies::getCode), Function.identity()));

        for (Map.Entry<String, CroppingPlanSpecies> codeToCPS : fromSpeciesByCode.entrySet()) {
            CroppingPlanSpecies toSpecies = toSpeciesByCode.get(codeToCPS.getKey());
            if (!isSimilarSpecies(codeToCPS.getValue(), toSpecies, fromDomain, toDomain)) return false;
        }

        return from.getName().contentEquals(to.getName())
            && from.getCode().contentEquals(to.getCode())
            && Objects.equals(from.getSellingPrice(), to.getSellingPrice())
            && from.isValidated() == to.isValidated()
            && Objects.equals(from.getYealdAverage(), to.getYealdAverage())
            && Objects.equals(from.getAverageIFT(), to.getAverageIFT())
            && Objects.equals(from.getBiocontrolIFT(), to.getBiocontrolIFT())
            && from.getTemporaryMeadow() == to.getTemporaryMeadow()
            && from.isPasturedMeadow() == to.isPasturedMeadow()
            && from.isMowedMeadow() == to.isMowedMeadow()
            && from.isMixSpecies() == to.isMixSpecies()
            && from.getType() == to.getType()
            && from.getYealdUnit() == to.getYealdUnit()
            && from.getEstimatingIftRules() == to.getEstimatingIftRules()
            && from.getIftSeedsType() == to.getIftSeedsType()
            && from.getDoseType() == to.getDoseType();
    }

    protected boolean isSimilarSpecies(CroppingPlanSpecies from, CroppingPlanSpecies to, Domain fromDomain, Domain toDomain) {

        if (from == null && to == null) return true;

        if (from != null && to == null) return false;

        if (fromDomain.getTopiaId().equals(toDomain.getTopiaId())) {
            if (from.getCode().contentEquals(to.getCode())) return false;
        } else if (fromDomain.getCode().contentEquals(to.getCode())) {
            if (!from.getCode().contentEquals(to.getCode())) return false;
        }
        return from.isValidated() == to.isValidated()
                && Objects.equals(from.getSpeciesArea(), to.getSpeciesArea())
                && from.getCompagne() == to.getCompagne()
                && Objects.equals(from.getEdaplosUnknownVariety(), to.getEdaplosUnknownVariety())
                && from.getVariety() == to.getVariety()
                && from.getDestination() == to.getDestination()
                && from.getSpecies() == to.getSpecies();
    }

    private void validateCommonDtoFields(
            DomainSeedLotInputDto lotDto,
            SeedPlantUnit domainSeedLotInputDto,
            boolean domainSeedLotInputDto5) {

        assertThat(domainSeedLotInputDto).isEqualTo(lotDto.getUsageUnit());
        assertThat(domainSeedLotInputDto5).isEqualTo(lotDto.isOrganic());
    }

    private Collection<DomainSeedSpeciesInputDto> getCropSpeciesSeedSpeciesInputDtos(
            Collection<CroppingPlanSpecies> baulonMelangeVarietesBleSpeciesDtos,
            InputPriceDto priceDto) {

        ArrayList<DomainSeedSpeciesInputDto> domainSeedSpeciesInputDtos = new ArrayList<>();
        ReferentialTranslationMap translationMap = new ReferentialTranslationMap(Language.FRENCH);

        for (CroppingPlanSpecies species : baulonMelangeVarietesBleSpeciesDtos) {
            final RefEspece refEspece = species.getSpecies();
            InputPriceDto speciesPriceDto = priceDto.toBuilder()
                    .chemicalTreatment(true)
                    .biologicalSeedInoculation(true)
                    .includedTreatment(true)
                    .seedType(SeedType.SEMENCES_CERTIFIEES)
                    .displayName(refEspece.getLibelle_espece_botanique())
                    .build();
            final String objectId = InputPriceService.GET_SPECIES_DTO_SEED_OBJECT_ID.apply(species, speciesPriceDto);
            speciesPriceDto = speciesPriceDto.toBuilder()
                    .objectId(objectId)
                    .build();
            String varietyName = species.getVariety() != null ? species.getVariety().getLabel() : "";
            final CroppingPlanSpeciesDto croppingPlanSpeciesDto = CroppingPlans.getDtoForCroppingPlanSpecies(species, translationMap);

            DomainSeedSpeciesInputDto domainSeedSpeciesInputDto = DomainSeedSpeciesInputDto.builder()
                    .key(DomainInputStockUnitService.getLotSpeciesInputKey(refEspece.getCode_espece_botanique(), refEspece.getCode_qualifiant_AEE(), false, false, true, varietyName, SeedPlantUnit.KG_PAR_HA, priceDto.getSeedType()))
                    .inputName(refEspece.getLibelle_espece_botanique() + (StringUtils.isNotBlank(varietyName) ? " (" + varietyName + ")" : ""))
                    .speciesSeedDto(croppingPlanSpeciesDto)
                    .inputType(InputType.SEMIS)
                    .organic(true)
                    .seedType(SeedType.SEMENCES_CERTIFIEES)
                    .seedPrice(speciesPriceDto)
                    .build();
            domainSeedSpeciesInputDtos.add(domainSeedSpeciesInputDto);

        }
        return domainSeedSpeciesInputDtos;
    }

    protected CroppingPlanSpecies createCroppingPlanSpecies(RefEspece refEspece, CroppingPlanEntry croppingPlanEntry, RefVariete refVariete) {
        CroppingPlanSpecies cps = domainService.newCroppingSpecies();
        cps.setTopiaId(DomainService.NEW_SPECIES_PREFIX + UUID.randomUUID());
        cps.setSpecies(refEspece);
        cps.setCroppingPlanEntry(croppingPlanEntry);
        cps.setVariety(refVariete);
        return cps;
    }

    protected CroppingPlanEntry createCroppingPlanEntry(String cropName, CroppingEntryType cropType) {
        CroppingPlanEntry cpe = domainService.newCroppingPlanEntry();
        cpe.setTopiaId(DomainService.NEW_CROP_PREFIX + UUID.randomUUID());
        cpe.setCode(UUID.randomUUID().toString());
        cpe.setName(cropName);
        cpe.setType(cropType);
        return cpe;
    }
}
