package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainIrrigationInputTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.IrrigationUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.WeedType;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationActionTopiaDao;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsageTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSpeciesStade;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSpeciesStade;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemSource;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrigTopiaDao;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.indicators.ift.IndicatorLegacyIFTTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nuiton.topia.persistence.TopiaException;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import static fr.inra.agrosyst.api.entities.performance.DecomposedOperatingExpenses.IRRIGATION;
import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorDecomposedIrrigationOperatingExpensesTest extends AbstractIndicatorDecomposedOperatingExpensesTest {
    private DomainIrrigationInputTopiaDao irrigationInputDao;
    private IrrigationInputUsageTopiaDao irrigationInputUsageDao;

    @BeforeEach
    public void setupServices() throws TopiaException, IOException {
        super.setupServices();
        irrigationInputDao = getPersistenceContext().getDomainIrrigationInputDao();
        irrigationInputUsageDao = getPersistenceContext().getIrrigationInputUsageDao();
    }

    protected void createRefPrixIrrig(int year) {
        RefPrixIrrigTopiaDao refPrixIrrigDao = getPersistenceContext().getRefPrixIrrigDao();
        refPrixIrrigDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year - 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3,
                RefInputPrice.PROPERTY_PRICE, 0.08
        );

        refPrixIrrigDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3,
                RefInputPrice.PROPERTY_PRICE, 0.08
        );

        refPrixIrrigDao.create(
                RefInputPrice.PROPERTY_ACTIVE, true,
                RefInputPrice.PROPERTY_CAMPAIGN, year + 1,
                RefInputPrice.PROPERTY_UNIT, PriceUnit.EURO_M3,
                RefInputPrice.PROPERTY_PRICE, 0.08
        );
    }

    @Test
    public void inraeEffectiveWithPriceTest()  throws IOException {
        inraeEffectiveTest(
                0.1d,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles irrigation (€/ha);2013;Baulon;50.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, irrigation (€/ha);2013;Baulon;40.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Irrigation;intervention;03/04/2013;03/04/2014;Irrigation;N/A;;null;2013;Indicateur économique;Charges opérationnelles réelles irrigation (€/ha);50.0;100;;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Irrigation;intervention;03/04/2013;03/04/2014;Irrigation;N/A;;null;2013;Indicateur économique;Charges opérationnelles standardisées, millésimé, irrigation (€/ha);40.0;100;;;");
                });
    }

    @Test
    public void inraeEffectiveWithoutPriceTest()  throws IOException {
        inraeEffectiveTest(
                null,
                (content) -> {
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Plot Baulon 1;Zone principale;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles irrigation (€/ha);2013;Baulon;40.0;100;;");
                    assertThat(content).usingComparator(comparator)
                            .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Irrigation;intervention;03/04/2013;03/04/2014;Irrigation;N/A;;null;2013;Indicateur économique;Charges opérationnelles réelles irrigation (€/ha);40.0;100;;;");
                });
    }

    private void inraeEffectiveTest(Double price, Consumer<String> assertions) throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        testDatas.createRefInputUnitPriceUnitConverter();

        // Irrigation
        RefInterventionAgrosystTravailEDI actionIrrigation = refInterventionAgrosystTravailEDIDao.forNaturalId("SEO").findUnique();

        // "Plot Baulon 1" (blé tendre d'hiver)
        Zone zpPlotBaulon1 = zoneDao.newQueryBuilder().setOrderByArguments(Zone.PROPERTY_PLOT + "." + Plot.PROPERTY_NAME).findFirst();

        createRefPrixIrrig(zpPlotBaulon1.getPlot().getDomain().getCampaign());

        EffectiveCropCyclePhase cropCyclePhase = effectiveCropCyclePhaseDao.create(
                EffectiveCropCyclePhase.PROPERTY_DURATION, 15,
                EffectiveCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION
        );

        // test le blé tendre d'hiver en culture
        List<CroppingPlanEntry> crops = effectiveCropCycleService.getZoneCroppingPlanEntries(zpPlotBaulon1);
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(crops, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();
        Assertions.assertEquals("ZAR", bleTendreHiver.getSpecies().getCode_espece_botanique());

        effectivePerennialCropCycleTopiaDao.create(
                EffectivePerennialCropCycle.PROPERTY_ZONE, zpPlotBaulon1,
                EffectivePerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY, cultureBle,
                EffectivePerennialCropCycle.PROPERTY_PHASE, cropCyclePhase,
                EffectivePerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        EffectiveIntervention intervention = effectiveInterventionDao.create(
                EffectiveIntervention.PROPERTY_EFFECTIVE_CROP_CYCLE_PHASE, cropCyclePhase,
                EffectiveIntervention.PROPERTY_NAME, "intervention",
                EffectiveIntervention.PROPERTY_TYPE, AgrosystInterventionType.IRRIGATION,
                EffectiveIntervention.PROPERTY_START_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign(), 4, 3),
                EffectiveIntervention.PROPERTY_END_INTERVENTION_DATE, LocalDate.of(zpPlotBaulon1.getPlot().getDomain().getCampaign() + 1, 4, 3),
                EffectiveIntervention.PROPERTY_WORK_RATE, 1.0d,
                EffectiveIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                EffectiveIntervention.PROPERTY_TRANSIT_COUNT, 1,
                EffectiveIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        EffectiveSpeciesStade intervention1Stades = effectiveSpeciesStadeDao.create(EffectiveSpeciesStade.PROPERTY_CROPPING_PLAN_SPECIES, bleTendreHiver);
        intervention.addSpeciesStades(intervention1Stades);

        String objectId = InputPriceService.GET_IRRIGATION_OBJECT_ID.apply(zpPlotBaulon1.getPlot().getDomain().getCampaign());
        var irrigationPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.IRRIGATION_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Eau",
                InputPrice.PROPERTY_PRICE, price
        );
        var irrigationInput = irrigationInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, zpPlotBaulon1.getPlot().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Eau",
                DomainIrrigationInput.PROPERTY_USAGE_UNIT, IrrigationUnit.MM,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, irrigationPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var irrigationUsage = irrigationInputUsageDao.create(
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT, irrigationInput,
                AbstractInputUsage.PROPERTY_QT_AVG, 50.0d
        );

        IrrigationActionTopiaDao irrigActionDao = getPersistenceContext().getIrrigationActionDao();
        irrigActionDao.create(
                AbstractAction.PROPERTY_EFFECTIVE_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionIrrigation,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, 50.0d,
                IrrigationAction.PROPERTY_IRRIGATION_INPUT_USAGE, irrigationUsage,
                IrrigationAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(IRRIGATION));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(zpPlotBaulon1.getPlot().getDomain().getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertions.accept(content);
    }

    @Test
    public void inraePracticedTest() throws IOException {
        // required imports
        testDatas.importRefEspeces();
        testDatas.createTestPlots();
        testDatas.importInterventionAgrosystTravailEdi();

        GrowingSystem baulon1 = growingSystemDao.forNameEquals("Systeme de culture Baulon 1").findUnique();
        Domain domain = baulon1.getGrowingPlan().getDomain();

        testDatas.createRefInputUnitPriceUnitConverter();
        createRefPrixIrrig(domain.getCampaign());

        RefInterventionAgrosystTravailEDI actionIrrigation = refInterventionAgrosystTravailEDIDao.forNaturalId("SEO").findUnique();

        PracticedSystem practicedSystem = practicedSystemDao.create(
                PracticedSystem.PROPERTY_TOPIA_ID, "IndicatorOperatingExpenses_PS_SDC_TEST",
                PracticedSystem.PROPERTY_NAME, "épandage",
                PracticedSystem.PROPERTY_GROWING_SYSTEM, baulon1,
                PracticedSystem.PROPERTY_SOURCE, PracticedSystemSource.ENTRETIEN_ENREGISTREMENT,
                PracticedSystem.PROPERTY_CAMPAIGNS, CommonService.ARRANGE_CAMPAIGNS.apply(domain.getCampaign() + " 2017"),
                PracticedSystem.PROPERTY_UPDATE_DATE, LocalDateTime.now(),
                PracticedSystem.PROPERTY_ACTIVE, true
        );

        List<CroppingPlanEntry> croppingPlanEntries = testDatas.findCroppingPlanEntries(domain.getTopiaId());
        Map<String, CroppingPlanEntry> indexedCrops = Maps.uniqueIndex(croppingPlanEntries, IndicatorLegacyIFTTest.GET_CPE_NAME::apply);

        CroppingPlanEntry cultureBle = indexedCrops.get("Blé");
        CroppingPlanSpecies bleTendreHiver = cultureBle.getCroppingPlanSpecies().getFirst();

        PracticedPerennialCropCycle newCycle = practicedPerennialCropCycleDao.create(
                PracticedPerennialCropCycle.PROPERTY_PRACTICED_SYSTEM, practicedSystem,
                PracticedPerennialCropCycle.PROPERTY_CROPPING_PLAN_ENTRY_CODE, cultureBle.getCode(),
                PracticedPerennialCropCycle.PROPERTY_SOL_OCCUPATION_PERCENT, 100.0d,
                PracticedPerennialCropCycle.PROPERTY_WEED_TYPE, WeedType.PAS_ENHERBEMENT
        );

        PracticedCropCyclePhase cropCyclePhase = practicedCropCyclePhaseDao.create(
                PracticedCropCyclePhase.PROPERTY_DURATION, 2,
                PracticedCropCyclePhase.PROPERTY_TYPE, CropCyclePhaseType.PLEINE_PRODUCTION,
                PracticedCropCyclePhase.PROPERTY_PRACTICED_PERENNIAL_CROP_CYCLE, newCycle
        );

        // En théorie ce n'est pas nécessaire. Mais comme l'objet newCycle reste en cache, il n'est pas rechargé de la base et est incomplet
        newCycle.addCropCyclePhases(cropCyclePhase);

        PracticedIntervention intervention = practicedInterventionDao.create(
                PracticedIntervention.PROPERTY_PRACTICED_CROP_CYCLE_PHASE, cropCyclePhase,
                PracticedIntervention.PROPERTY_NAME, "intervention",
                PracticedIntervention.PROPERTY_TYPE, AgrosystInterventionType.EPANDAGES_ORGANIQUES,
                PracticedIntervention.PROPERTY_STARTING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_ENDING_PERIOD_DATE, "03/04",
                PracticedIntervention.PROPERTY_WORK_RATE, 1.0d,
                PracticedIntervention.PROPERTY_WORK_RATE_UNIT, MaterielWorkRateUnit.HA_H,
                PracticedIntervention.PROPERTY_TEMPORAL_FREQUENCY, 1.0d,
                PracticedIntervention.PROPERTY_SPATIAL_FREQUENCY, 1.0d
        );

        // auto select "blé tendre d'hiver"
        PracticedSpeciesStade intervention1Stades = practicedSpeciesStadeDao.create(PracticedSpeciesStade.PROPERTY_SPECIES_CODE, bleTendreHiver.getCode());
        intervention.addSpeciesStades(intervention1Stades);

        String objectId = InputPriceService.GET_IRRIGATION_OBJECT_ID.apply(baulon1.getGrowingPlan().getDomain().getCampaign());
        var irrigationPrice = priceDao.create(
                InputPrice.PROPERTY_OBJECT_ID, objectId,
                InputPrice.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                InputPrice.PROPERTY_PRICE_UNIT, PriceUnit.EURO_M3,
                InputPrice.PROPERTY_CATEGORY, InputPriceCategory.IRRIGATION_INPUT,
                InputPrice.PROPERTY_DISPLAY_NAME, "Eau",
                InputPrice.PROPERTY_PRICE, 0.1d
        );
        var irrigationInput = irrigationInputDao.create(
                AbstractDomainInputStockUnit.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                AbstractDomainInputStockUnit.PROPERTY_DOMAIN, baulon1.getGrowingPlan().getDomain(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_NAME, "Eau",
                DomainIrrigationInput.PROPERTY_USAGE_UNIT, IrrigationUnit.MM,
                AbstractDomainInputStockUnit.PROPERTY_INPUT_PRICE, irrigationPrice,
                AbstractDomainInputStockUnit.PROPERTY_CODE, UUID.randomUUID().toString(),
                AbstractDomainInputStockUnit.PROPERTY_INPUT_KEY, ""
        );
        var irrigationUsage = irrigationInputUsageDao.create(
                AbstractInputUsage.PROPERTY_INPUT_TYPE, InputType.IRRIGATION,
                IrrigationInputUsage.PROPERTY_DOMAIN_IRRIGATION_INPUT, irrigationInput,
                AbstractInputUsage.PROPERTY_QT_AVG, 50.0d
        );

        IrrigationActionTopiaDao irrigActionDao = getPersistenceContext().getIrrigationActionDao();
        irrigActionDao.create(
                AbstractAction.PROPERTY_PRACTICED_INTERVENTION, intervention,
                AbstractAction.PROPERTY_MAIN_ACTION, actionIrrigation,
                IrrigationAction.PROPERTY_WATER_QUANTITY_AVERAGE, 50.0d,
                IrrigationAction.PROPERTY_IRRIGATION_INPUT_USAGE, irrigationUsage,
                IrrigationAction.PROPERTY_OTHER_PRODUCT_INPUT_USAGES, Collections.emptyList()
        );

        IndicatorDecomposedOperatingExpenses indicatorOperatingExpenses = serviceFactory.newInstance(IndicatorDecomposedOperatingExpenses.class);

        Collection<IndicatorFilter> indicatorFilters = new ArrayList<>();
        IndicatorFilter indicatorFilter = createIndicatorFilter(Collections.singletonList(IRRIGATION));
        indicatorOperatingExpenses.init(indicatorFilter);
        indicatorFilters.add(indicatorFilter);

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setPracticed(true);
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(domain.getTopiaId()),
                null,
                null,
                null,
                indicatorFilters,
                null,
                true,
                true,
                false
        );

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorOperatingExpenses);
        String content = out.toString();

        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles réelles irrigation (€/ha);2013, 2017;Baulon;50.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("interventionSheet;Baulon;Systeme de culture Baulon 1;Blé;PLEINE_PRODUCTION;intervention;Indicateur économique;Charges opérationnelles standardisées, millésimé, irrigation (€/ha);2013, 2017;Baulon;40.0;100;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04;03/04;Irrigation;N/A;;null;2013, 2017;Indicateur économique;Charges opérationnelles réelles irrigation (€/ha);50.0;100;;;");
        assertThat(content).usingComparator(comparator)
                .isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;épandage;Blé;;Blé tendre;;Blé tendre;;Pleine production;Épandage organique;intervention;03/04;03/04;Irrigation;N/A;;null;2013, 2017;Indicateur économique;Charges opérationnelles standardisées, millésimé, irrigation (€/ha);40.0;100;;;");
    }
}
