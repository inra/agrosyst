package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.performance.ExportType;
import fr.inra.agrosyst.api.entities.performance.Performance;
import fr.inra.agrosyst.api.entities.performance.PerformanceImpl;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.services.performance.IndicatorMockWriter;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class IndicatorTotalActiveSubstanceAmountTest extends AbstractQSATest {

    @Test
    public void testQuantiteTotaleRealise_12311() {
        testDatas.createEffectiveDataSetForQSAIndicators();

        //
        // Calcul de l'indicateur
        //

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorWriter writer = new IndicatorMockWriter(out);
        IndicatorTotalActiveSubstanceAmount indicatorTotalActiveSubstanceAmount = serviceFactory.newInstance(IndicatorTotalActiveSubstanceAmount.class);
        indicatorTotalActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorTotalActiveSubstanceAmount);

        final String content = out.toString();

        // Vérifications indicateur de QSA Totale
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité totale de substances actives phyto;2013;Baulon;4.32;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité totale de substances actives phyto;2013;Baulon;1.008;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité totale de substances actives phyto;2013;Baulon;8.0;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité totale de substances actives phyto;2013;Baulon;0.8;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);Intervention 5;17/03/2013;17/03/2013;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);MASTOR WG (0.5 kg/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto;0.25;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité totale de substances actives phyto;2013;Baulon;0.252;");
        assertThat(content).usingComparator(comparator).isEqualTo("Quantité totale de substances actives phyto;2013;Baulon;0.025;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 13;26/04/2013;26/04/2013;Semis;OTANA (0.2 kg/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto;0.025;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 13;26/04/2013;26/04/2013;Semis;OTANA (0.2 kg/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto hors traitement de semence;0.0;100;;;");
        //assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 14;26/04/2013;26/04/2013;Semis;OTANA (0.2 kg/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto;0.025;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 14;26/04/2013;26/04/2013;Semis;OTANA (null kg/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto hors traitement de semence;0.0;0;Intrant Traitement de semence : Dose moyenne;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 15;26/04/2013;26/04/2013;Semis;OTANA (0.2 L/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto;0.025;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 15;26/04/2013;26/04/2013;Semis;OTANA (0.2 L/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto hors traitement de semence;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 16;26/04/2013;26/04/2013;Semis;OTANA (0.2 unité/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 16;26/04/2013;26/04/2013;Semis;OTANA (0.2 unité/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto hors traitement de semence;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("inputSheet;Baulon (2013);;;Systeme de culture Baulon 1;HOF21095;Agriculture conventionnelle;Plot Baulon 1;Zone principale;34.0;Blé;;Blé tendre;;Blé tendre;;Pleine production;Semis;Intervention 17;26/04/2013;26/04/2013;Semis;OTANA (200.0 mL/ha);;non;2013;Substances actives;Quantité totale de substances actives phyto hors traitement de semence;0.0;100;;;");
        assertThat(content).usingComparator(comparator).isEqualTo("");
    }

    @Test
    public void testQuantiteTotaleSynthetise_12311() throws IOException {
        testDatas.createPracticedDataSetForQSAIndicators();

        /*
         * Calcul des indicateurs
         */

        // create new performance
        Performance performance = new PerformanceImpl();
        performance.setName("Test");
        performance.setExportType(ExportType.FILE);
        performance.setPracticed(true);
        performance = performanceService.createOrUpdatePerformance(performance,
                "", Collections.singletonList(baulon.getTopiaId()),
                null, null, null, indicatorFilters, null, true, true, false);

        // compute effective
        StringWriter out = new StringWriter();
        IndicatorMockWriter writer = new IndicatorMockWriter(out);
        writer.setUseOriginaleWriterFormat(false);
        writer.setPerformance(performance);

        IndicatorTotalActiveSubstanceAmount indicatorTotalActiveSubstanceAmount = serviceFactory.newInstance(IndicatorTotalActiveSubstanceAmount.class);
        indicatorTotalActiveSubstanceAmount.setConversionRatesByUnit(this.referentialService.getCoeffsConversionVersKgHa());
        performanceService.convertToWriter(performance, writer, indicatorFilters, indicatorTotalActiveSubstanceAmount);
        final String content = out.toString();

        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 1;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 kg/ha);;non;2012, 2013;Substances actives;Quantité totale de substances actives phyto;0.884;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 2;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 L/ha);;non;2012, 2013;Substances actives;Quantité totale de substances actives phyto;0.884;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 3;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3.0 unité/ha);;non;2012, 2013;Substances actives;Quantité totale de substances actives phyto;0.0;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 4;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);LONGRUN (3000.0 g/ha);;non;2012, 2013;Substances actives;Quantité totale de substances actives phyto;0.884;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 5;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);ATHLET (3.6 L/ha);;non;2012, 2013;Substances actives;Quantité totale de substances actives phyto;1.26;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 6;03/04;03/04;Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM);TARAK (0.11 L/ha);;non;2012, 2013;Substances actives;Quantité totale de substances actives phyto;0.055;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 10;Intervention_10_ID;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;Quantité totale de substances actives phyto;0.048;");
        assertThat(content).usingComparator(comparator).isEqualTo(";Intervention 10;Intervention_10_ID;03/04;03/04;Semis;Intrant de semis de test sur culture Blé;1;;;non;;2012, 2013;Synthétisé;Substances actives;Quantité totale de substances actives phyto hors traitement de semence;0.0;");

    }

    @Test
    public void testQuantiteGlyphosateUniteCompatible_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductLongrunLHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(produitLongrun, domainPhytoProductLongrunLHa, 3, "Quantité totale de substances actives phyto;0.884;");
    }

    @Test
    public void testQuantiteGlyphosateUniteNonCompatible_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductLongrunUniteHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.UNITE_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(produitLongrun, domainPhytoProductLongrunUniteHa, 3, "Quantité totale de substances actives phyto;0.0;");
    }

    @Test
    public void testQuantiteGlyphosateUniteCompatibleDifferente_12307() throws IOException {
        RefActaTraitementsProduit produitLongrun = refActaTraitementsProduitsTopiaDao.forNaturalId("7540", 147, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductLongrunGHa = testDatas.createDomainPhytoProductDto(produitLongrun, ProductType.HERBICIDAL, PhytoProductUnit.G_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(produitLongrun, domainPhytoProductLongrunGHa, 3000, "Quantité totale de substances actives phyto;0.884;");
    }

    @Test
    public void testQuantitethiamethoxamUniteCompatible_12311() throws IOException {
        RefActaTraitementsProduit produitTara25Wg = refActaTraitementsProduitsTopiaDao.forNaturalId("7976", 153, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductTara25Wg = testDatas.createDomainPhytoProductDto(produitTara25Wg, ProductType.INSECTICIDAL, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(produitTara25Wg, domainPhytoProductTara25Wg, 0.2, "Quantité totale de substances actives phyto;0.025;", 0.5, 1.0, 100.0, 25d);
    }

    @Test
    public void testQuantitethiamethoxamUniteIncompatible_12311() throws IOException {
        RefActaTraitementsProduit produitTara25Wg = refActaTraitementsProduitsTopiaDao.forNaturalId("7976", 153, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductTara25Wg = testDatas.createDomainPhytoProductDto(produitTara25Wg, ProductType.INSECTICIDAL, PhytoProductUnit.UNITE_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        IndicatorTotalActiveSubstanceAmount totalActiveSubstanceAmount = serviceFactory.newInstance(IndicatorTotalActiveSubstanceAmount.class);
        Map<String, Double> coeffsConversionVersKgHa = this.referentialService.getCoeffsConversionVersKgHa();
        totalActiveSubstanceAmount.setConversionRatesByUnit(coeffsConversionVersKgHa);

        this.testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(produitTara25Wg, domainPhytoProductTara25Wg, 0.2, "Quantité totale de substances actives phyto;0.0;", 0.5, 1.0, 100.0, 25d);
    }

    @Test
    public void testQuantitethiamethoxamUniteCompatibleDifferente_12311() throws IOException {
        RefActaTraitementsProduit produitTara25Wg = refActaTraitementsProduitsTopiaDao.forNaturalId("7976", 153, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductTara25Wg = testDatas.createDomainPhytoProductDto(produitTara25Wg, ProductType.INSECTICIDAL, PhytoProductUnit.ML_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(produitTara25Wg, domainPhytoProductTara25Wg, 200, "Quantité totale de substances actives phyto;0.025;", 0.5, 1.0, 100.0, 25d);
    }

    @Test
    public void testQuantiteImidaclopride_12311() throws IOException {
        RefActaTraitementsProduit produitGaucho = refActaTraitementsProduitsTopiaDao.forNaturalId("6796", 115, france).findUnique();
        DomainPhytoProductInputDto domainPhytoProductGaucho = testDatas.createDomainPhytoProductDto(produitGaucho, ProductType.ASSOCIATIONS, PhytoProductUnit.L_HA, null, InputType.APPLICATION_DE_PRODUITS_PHYTOSANITAIRES);

        this.testPour1ApplicationEnSynthetiseIndicatorTotalActiveSubstance(produitGaucho, domainPhytoProductGaucho, 1, "Quantité totale de substances actives phyto;0.4;");
    }

}
