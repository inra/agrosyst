package fr.inra.agrosyst.services;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.poi.util.TempFileCreationStrategy;
import org.nuiton.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 *     poiVersion>5.3.0: Les versions supérieures à 5.2.3 jusqu'à 5.2.5 entrainent des erreurs de ce type:
 *     java.nio.file.NoSuchFileException: '/builds/03cc2214/0/inra/agrosyst/agrosyst-services/target/surefire-workdir/poifiles/poi-sxssf-sheet8110463425603098115.xml'
 *     parceque dans le déroulement de nos tests après chaque test, on supprime le contenu du dossier temporaire
 *     et que les fichiers temporaires de POI sont placée à la racine de JAVA_IO_TMPDIR
 *     Cette implémentation spécifique isole les fichiers temporaires dans un dossier spécifique au test en cours
 *     pour éviter la suppréssion d'autres fichiers d'autres tests en cours.
 */
public class TestPoiTempFileCreationStrategy implements TempFileCreationStrategy {

    protected final String path;
    protected final File tempDirectoryFile;

    public TestPoiTempFileCreationStrategy(String path) {
        this.path = path;
        try {
            tempDirectoryFile = createTempDirectory(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public File createTempFile(String prefix, String suffix) throws IOException {

        File tmpFile = Files.createTempFile(tempDirectoryFile.toPath(), prefix, suffix).toFile();
        tmpFile.deleteOnExit();

        return tmpFile;
    }

    @Override
    public File createTempDirectory(String prefix) throws IOException {

        File tempDirectoryFile = new File(path);
        FileUtil.createDirectoryIfNecessary(tempDirectoryFile);

        return tempDirectoryFile;
    }
}
