package fr.inra.agrosyst.services.managementmode.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.services.common.export.ExportModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

public class DecisionRuleExportMetadata {

    @Getter
    @Setter
    public static class DecisionRuleBean {

        // Domaine
        String domainName;

        // Règle de décision
        String name;
        int versionNumber;
        AgrosystInterventionType interventionType;
        String decisionRuleCrop;
        BioAgressorType bioAgressorType;
        String groupeCible;
        String bioAgressor;
        String decisionObject;
        String domainValidity;
        String source;
        String usageComment;
        String objective;
        String expectedResult;
        String solution;
        String decisionCriteria;
        String resultCriteria;
        String observation;
        String solutionComment;
        String versionReason;

    }

    public static class DecisionRuleModel extends ExportModel<DecisionRuleBean> {

        @Override
        public String getTitle() {
            return "Généralités";
        }

        public DecisionRuleModel() {
            newColumn("Domaine", DecisionRuleBean::getDomainName);
            newColumn("Nom", DecisionRuleBean::getName);
            newColumn("Version", DecisionRuleBean::getVersionNumber);
            newColumn("Type d'intervention", AgrosystInterventionType.class, DecisionRuleBean::getInterventionType);
            newColumn("Cultures concernées", DecisionRuleBean::getDecisionRuleCrop);
            newColumn("Type de bio-agresseur", BioAgressorType.class, DecisionRuleBean::getBioAgressorType);
            newColumn("Groupe cible", DecisionRuleBean::getGroupeCible);
            newColumn("Bio-agresseur concerné", DecisionRuleBean::getBioAgressor);
            newColumn("Objet de la décision", DecisionRuleBean::getDecisionObject);
            newColumn("Domaine de validité", DecisionRuleBean::getDomainValidity);
            newColumn("Source", DecisionRuleBean::getSource);
            newColumn("Commentaire sur l'utilisation de la règle de décision", DecisionRuleBean::getUsageComment);
            newColumn("Objectif", DecisionRuleBean::getObjective);
            newColumn("Résultat attendu ", DecisionRuleBean::getExpectedResult);
            newColumn("Solution", DecisionRuleBean::getSolution, solution -> {
                String result12 = null;
                if (StringUtils.isNotBlank(solution)) {
                    solution = solution.replaceAll("<[^>]*>","");
                    result12 = StringEscapeUtils.unescapeHtml4(solution);
                }
                return result12;
            });
            newColumn("Critère de décision ou seuil", DecisionRuleBean::getDecisionCriteria);
            newColumn("Critère d'évaluation du résultat attendu", DecisionRuleBean::getResultCriteria);
            newColumn("Observation ou outil", DecisionRuleBean::getObservation);
            newColumn("Commentaire sur la solution", DecisionRuleBean::getSolutionComment);
            newColumn("Motif de changement de version", DecisionRuleBean::getVersionReason);
        }
    }

}
