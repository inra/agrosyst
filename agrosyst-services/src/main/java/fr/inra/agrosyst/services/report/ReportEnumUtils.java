/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.report;

import org.apache.commons.lang3.StringUtils;
import org.nuiton.i18n.I18n;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Class qui traduit les enums par leur traduction dans les rapports freemarker.
 */
public class ReportEnumUtils {

    public String of(Object obj, String name) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        return of(obj, name, null);
    }

    public String of(Object obj, String name, String context) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Method method = obj.getClass().getMethod("get" + StringUtils.capitalize(name));
        Class<?> returnType = method.getReturnType();
        Object value = method.invoke(obj);
        String result = null;
        if (value != null) {
            String key = returnType.getName();
            if (context != null) {
                key += "#" + context;
            }
            key += "." + value;
            result = tr(key);
        }
        return result;
    }

    public String tr(String key) {
        String result = I18n.t(key);
        return result;
    }
}
