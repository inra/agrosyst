package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClassImpl;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefQualityCriteriaClassModel extends AbstractDestinationAndPriceModel<RefQualityCriteriaClass> implements ExportModel<RefQualityCriteriaClass> {

    public RefQualityCriteriaClassModel() {
        super(CSV_SEPARATOR);
    }

    public RefQualityCriteriaClassModel(RefQualityCriteriaTopiaDao dao) {
        super(CSV_SEPARATOR);
        getQualityCriteriaCodes(dao);

        setColumnsForImport();
    }

    protected void setColumnsForImport() {
        newMandatoryColumn("code_classe", RefQualityCriteriaClass.PROPERTY_CODE);
        newMandatoryColumn("code_critères_de_qualité", RefQualityCriteriaClass.PROPERTY_REF_QUALITY_CRITERIA_CODE, CODE_CRITERE_QUALITE_PARSER);
        newMandatoryColumn("classe", RefQualityCriteriaClass.PROPERTY_CLASSE);
        newOptionalColumn(COLUMN_ACTIVE, RefQualityCriteriaClass.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefQualityCriteriaClass, Object>> getColumnsForExport() {
        ModelBuilder<RefQualityCriteria> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_classe", RefQualityCriteriaClass.PROPERTY_CODE);
        modelBuilder.newColumnForExport("code_critères_de_qualité", RefQualityCriteriaClass.PROPERTY_REF_QUALITY_CRITERIA_CODE);
        modelBuilder.newColumnForExport("classe", RefQualityCriteriaClass.PROPERTY_CLASSE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefQualityCriteriaClass.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefQualityCriteriaClass newEmptyInstance() {
        RefQualityCriteriaClass result = new RefQualityCriteriaClassImpl();
        result.setActive(true);
        return result;
    }

}
