package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspeceImpl;
import fr.inra.agrosyst.services.common.CommonService;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixEspeceModel extends AbstractDestinationAndPriceModel<RefPrixEspece> implements ExportModel<RefPrixEspece> {

    public RefPrixEspeceModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Code espèce botanique", RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE);
        newMandatoryColumn("Code qualifiant AEE", RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE);
        newMandatoryColumn("Type de semence/plant", RefPrixEspece.PROPERTY_SEED_TYPE, SEED_TYPE_PARSER);
        newOptionalColumn("Nom de l'espèce", RefPrixEspece.PROPERTY_ESPECE);
        newOptionalColumn("Qualifiant", RefPrixEspece.PROPERTY_QUALIFIANT);
        newMandatoryColumn("Prix", RefPrixEspece.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Unité", RefPrixEspece.PROPERTY_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Code scénario", RefPrixEspece.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixEspece.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixEspece.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Organic", RefPrixEspece.PROPERTY_ORGANIC, CommonService.BOOLEAN_PARSER);
        newMandatoryColumn("Traitement", RefPrixEspece.PROPERTY_TREATMENT, CommonService.BOOLEAN_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixEspece.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("Source", RefPrixEspece.PROPERTY_SOURCE);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixEspece, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixEspece> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Code espèce botanique", RefPrixEspece.PROPERTY_CODE_ESPECE_BOTANIQUE);
        modelBuilder.newColumnForExport("Code qualifiant AEE", RefPrixEspece.PROPERTY_CODE_QUALIFIANT__AEE);
        modelBuilder.newColumnForExport("Type de semence/plant", RefPrixEspece.PROPERTY_SEED_TYPE, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Nom de l'espèce", RefPrixEspece.PROPERTY_ESPECE);
        modelBuilder.newColumnForExport("Qualifiant", RefPrixEspece.PROPERTY_QUALIFIANT);
        modelBuilder.newColumnForExport("Prix", RefPrixEspece.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité", RefPrixEspece.PROPERTY_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Code scénario", RefPrixEspece.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixEspece.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixEspece.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Organic", RefPrixEspece.PROPERTY_ORGANIC, O_N_FORMATTER);
        modelBuilder.newColumnForExport("Traitement", RefPrixEspece.PROPERTY_TREATMENT, T_F_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixEspece.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixEspece.PROPERTY_SOURCE);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixEspece newEmptyInstance() {
        RefPrixEspece result = new RefPrixEspeceImpl();
        result.setActive(true);// default if not set up
        return result;
    }
}
