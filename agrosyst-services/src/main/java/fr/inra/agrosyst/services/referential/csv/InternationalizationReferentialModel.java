package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

import java.util.Map;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public abstract class InternationalizationReferentialModel<E> extends AbstractAgrosystModel<E> {
    
    private static final Log LOGGER = LogFactory.getLog(InternationalizationReferentialModel.class);
    protected Map<String, RefCountry> refCountryByTrigram;
    protected RefCountryTopiaDao refCountryDao;
    
    protected InternationalizationReferentialModel(char separator) {
        super(separator);
    }
    
    protected final ValueParser<RefCountry> TRIGRAM_PARSER = trigram -> {
        RefCountry refCountry = refCountryByTrigram.get(trigram.toLowerCase());
        if (refCountry == null) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(String.format("Country with trigram as:'%s' was not found", trigram));
            }
            throw new AgrosystImportException(String.format("Country with trigram as:'%s' was not found", trigram));
        }
        return refCountry;
    };
    
    protected static final ValueFormatter<RefCountry> TRIGRAM_FORMATTER = refCountry -> refCountry.getTrigram().toLowerCase();
    
}
