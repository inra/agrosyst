package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * <p>
 *     Indicateur d'Unité de Travail Humain nécessaire pour assurer les travaux d'un système de culture.
 * </p>
 *
 * <p>
 *     Pour ce faire, le somme du temps d’utilisation du matériel et du temps de travail manuel est divisé par 1600.
 *     Cet indicateur est calculé à l'échelle de la Parcelle en réalisé et de la culture en Synthétisé et se décline jusqu'au Domaine.
 *     Cet indicateur ne sera pas calculé aux échelles Intrants et Interventions
 * </p>
 *
 * <p>
 *     <pre>Nb UTH nécessaires (unité / ha)= Temps de travail total (h / ha) / 1600 (h / unité).</pre>
 * </p>
 *
 * <p>
 *     Le temps de travail total est calculé dans {@link IndicatorTotalWorkTime}
 * </p>
 *
 * @see fr.inra.agrosyst.api.entities.action.IrrigationAction
 */
public class IndicatorUTH extends AbstractIndicator {

    private final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public static final String COLUMN_NAME = "nombre_UTH_necessaires";

    /** Unité de Travail Humain nécessaire pour assurer les travaux du système de culture, en prenant comme référence 1600 heures/an/UTH */
    public static final double NB_HEURES_PAR_AN_PAR_UTH_REF = 1600.0;

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        final Map<String, String> indicatorNameToDbColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToDbColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToDbColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return newArray(1, 0.0);
        }

        final double uthValue = interventionContext.getTotalWorkTime() / NB_HEURES_PAR_AN_PAR_UTH_REF;
        return new Double[] { uthValue };
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext, PerformanceEffectiveCropExecutionContext cropContext, PerformanceEffectiveInterventionExecutionContext interventionContext) {
        final double uthValue = interventionContext.getTotalWorkTime() / NB_HEURES_PAR_AN_PAR_UTH_REF;
        return new Double[] { uthValue };
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.socialPerformance");
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.uthAmount");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }
}
