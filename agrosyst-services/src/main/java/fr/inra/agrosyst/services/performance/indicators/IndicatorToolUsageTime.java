package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Temps d'utilisation du matériel.
 * 
 * Présentation de l’indicateur
 *
 * Le temps d'utilisation du matériel remplace le temps de travail
 *
 * Le temps de travail est calculé sur la base de l’indicateur de Systerre1. Il correspond au temps
 *
 * @author Yannick Martel (martel@codelutin.com
 */
public class IndicatorToolUsageTime extends AbstractIndicator {
    
    protected static final MaterielWorkRateUnit DEFAULT_WORK_RATE_UNIT = MaterielWorkRateUnit.H_HA;
    @Setter
    protected String[] translatedMonth;

    private boolean detailedByMonth = true;
    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);
    
    public IndicatorToolUsageTime() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "temps_utilisation_du_materiel_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "temps_utilisation_du_materiel_detail_champs_non_renseig");
    }
    
    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        
        for (int i = 0; i <= NB_MONTH; i++) {
            String label = getIndicatorLabel(i);
            String dbColumnName;
            if (i < NB_MONTH) {// 12 months + sum
                dbColumnName = "temps_utilisation_du_materiel_" + MONTHS_FOR_DB_COLUMNS[i];
            } else {
                dbColumnName = "temps_utilisation_du_materiel";
            }
            indicatorNameToColumnName.put(label, dbColumnName);
        }
    
        return indicatorNameToColumnName;
    }
    
    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed &&
                (detailedByMonth || i == NB_MONTH);// 12 months + sum
    }
    
    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }
    
    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_SOCIO_TECHNIC);
    }
    
    @Override
    public String getIndicatorLabel(int i) {
        String result;
        if (0 <= i && i < NB_MONTH) {// 12 months + sum
            result = l(locale, "Indicator.label.toolUsageTimeMonth", translatedMonth[i]);
        } else {
            result = l(locale, "Indicator.label.toolUsageTime");
        }

        return result;
    }
    
    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
    
        if (interventionContext.isFictive()) return newArray(NB_MONTH + 1, 0.0);
        
        double usageTime;
        
        PracticedIntervention intervention = interventionContext.getIntervention();
        
        ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();

        if (toolsCoupling == null || toolsCoupling.isManualIntervention()) {
            // No tools coupling, no tools usage time !
            usageTime = 0.0d;

        } else {
    
            // c'est un débit de chantier
            Pair<Double, MaterielWorkRateUnit> workRate = getPracticedInterventionWorkRate(domainContext, intervention, toolsCoupling, DEFAULT_WORK_RATE_VALUE, DEFAULT_WORK_RATE_UNIT);
    
            String interventionId = intervention.getTopiaId();
    
            // We know we always have one TC max
            double toolPSCi = getToolPSCi(intervention);
    
            // TransitVolume (ne peut être absent)
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            Double transitVolume = ObjectUtils.firstNonNull(intervention.getTransitVolume(), toolsCoupling.getTransitVolume());
            MaterielTransportUnit transitVolumeUnit = ObjectUtils.firstNonNull(intervention.getTransitVolumeUnit(), (toolsCoupling.getTransitVolume() != null ? MaterielTransportUnit.M3 : null));
    
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            Integer nbBalls = intervention.getNbBalls();
    
            Optional<OrganicFertilizersSpreadingAction> organicFertilizersSpreadingAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
            
            Collection<OrganicProductInputUsage> organicProductInputUsages = organicFertilizersSpreadingAction.isPresent() ? organicFertilizersSpreadingAction.get().getOrganicProductInputUsages() : CollectionUtils.emptyCollection();
    
            Optional<HarvestingAction> optionalHarvestingAction = interventionContext.getOptionalHarvestingAction();
    
            usageTime = toolPSCi * getEquipmentsToolsUsage(
                    toolsCoupling,
                    workRate,
                    interventionId,
                    transitVolume,
                    transitVolumeUnit,
                    nbBalls,
                    organicProductInputUsages,
                    optionalHarvestingAction);
        }
    
        interventionContext.setToolsCouplingWorkingTime(usageTime);
    
        // Le temps est reparti en proportion du nombre de jour
        return newResult(getMonthsRatio(intervention, usageTime));
    }
    
    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Double[] usageTimes;
        double usageTime;
        
        Pair<Double, MaterielWorkRateUnit> workRate = getEffectiveInterventionWorkRate(domainContext, interventionContext, DEFAULT_WORK_RATE_VALUE, DEFAULT_WORK_RATE_UNIT);
        
        EffectiveIntervention intervention = interventionContext.getIntervention();

        // c'est un débit de chantier
        ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();
        if (toolsCoupling == null || toolsCoupling.isManualIntervention()) {
            // No tools coupling, no tools usage time !
            usageTime = 0.0d;

        } else {
    
            String interventionId = intervention.getTopiaId();
            double toolPSCi = getToolPSCi(intervention);
    
            // TransitVolume (ne peut être absent)
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            Double transitVolume = intervention.getTransitVolume();
            MaterielTransportUnit transitVolumeUnit = intervention.getTransitVolumeUnit();
    
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            Integer nbBalls = intervention.getNbBalls();
    
            Optional<OrganicFertilizersSpreadingAction> organicFertilizersSpreadingAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();
    
            Collection<OrganicProductInputUsage> organicProductInputUsages = organicFertilizersSpreadingAction.isPresent() ? organicFertilizersSpreadingAction.get().getOrganicProductInputUsages() : CollectionUtils.emptyCollection();
            
            Optional<HarvestingAction> optionalHarvestingAction = interventionContext.getOptionalHarvestingAction();
    
            usageTime = toolPSCi * getEquipmentsToolsUsage(
                    toolsCoupling,
                    workRate,
                    interventionId,
                    transitVolume,
                    transitVolumeUnit,
                    nbBalls,
                    organicProductInputUsages,
                    optionalHarvestingAction);
    
        }
    
        interventionContext.setToolsCouplingWorkingTime(usageTime);
    
        // Le temps est reparti en proportion du nombre de jour
        usageTimes = newResult(getMonthsRatio(intervention, usageTime));
    
        return usageTimes;
    }

    public void init(IndicatorFilter filter, String[] translatedMonth) {
        displayed = filter != null;
        detailedByMonth = displayed && BooleanUtils.isTrue(filter.getDetailedByMonth());
        this.translatedMonth = translatedMonth;
    }
}
