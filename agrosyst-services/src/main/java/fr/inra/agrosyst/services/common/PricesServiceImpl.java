package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisationTopiaDao;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnectionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedInterventionTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycleTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceImpl;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.api.services.action.HarvestingActionValorisationDto;
import fr.inra.agrosyst.api.services.action.HarvestingPriceDto;
import fr.inra.agrosyst.api.services.common.GlobalRefHarvestingPriceFilter;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPriceSummary;
import fr.inra.agrosyst.api.services.common.LoadedPricesResult;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PriceDto;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.RefHarvestingPriceFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.api.utils.DaoUtils;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.export.ExportUtils;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Setter
public class PricesServiceImpl extends AbstractAgrosystService implements PricesService {
    
    public static final String IMPL_SUFFIX = "Impl";
    public static final String INCONNU = "INCONNU";

    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;
    protected EffectiveInterventionTopiaDao effectiveInterventionDao;
    protected EffectivePerennialCropCycleTopiaDao effectivePerennialCropCyclDao;
    protected EffectiveSeasonalCropCycleTopiaDao effectiveSeasonalCropCycleDao;
    protected PracticedCropCycleConnectionTopiaDao practicedCropCycleConnectionDao;
    protected PracticedInterventionTopiaDao practicedInterventionDao;
    protected PracticedPerennialCropCycleTopiaDao practicedPerennialCropCycleDao;
    protected PracticedSeasonalCropCycleTopiaDao practicedSeasonalCropCycleDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected HarvestingPriceTopiaDao harvestingPriceDao;
    protected RefHarvestingPriceTopiaDao refHarvestingPriceDao;

    protected ActionService actionService;
    protected ReferentialService referentialService;
    protected PracticedSystemService practicedSystemService;
    protected EffectiveCropCycleService effectiveCropCycleService;
    protected DomainService domainService;

    public static final Function <PriceDto, LinkedHashSet<Integer>> getRefInputPricesCampaigns = price -> {
        LinkedHashSet<Integer> campaigns;
        if (CommonService.getInstance().areCampaignsValids(price.getCampaigns())) {
            campaigns = CommonService.GET_CAMPAIGNS_SET.apply(price.getCampaigns());
        } else {
            campaigns = new LinkedHashSet<>(CommonService.getInstance().lowerCampaignBound);
        }
        return campaigns;
    };

    private void validValorisation(HarvestingActionValorisation valorisation) {
        Preconditions.checkArgument(
                CommonService.getInstance()
                        .isCampaignValid(valorisation.getBeginMarketingPeriodCampaign()),
                String.format(
                        "la valorisation avec comme ID: '%s' a une valeurs de début de campagne marketing non valide: %d",
                        valorisation.getTopiaId(),
                        valorisation.getBeginMarketingPeriodCampaign()));
    
        Preconditions.checkArgument(
                CommonService.getInstance().isCampaignValid(
                        valorisation.getEndingMarketingPeriodCampaign()),
                String.format("la valorisation avec comme ID: '%s' a une valeurs de fin de campagne marketing non valide: %d",
                        valorisation.getTopiaId(),
                        valorisation.getEndingMarketingPeriodCampaign()));
    }

    protected Map<String, HarvestingValorisationPriceSummary> computeAverageReferencePricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            List<RefHarvestingPrice> refHarvestingPrices,
            String practicedSystemCampaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
    

        // return prices by valorisation. Prices or ordered by increasing price values and units
        Optional<String> optionalPracticedSystemCampaigns = Optional.ofNullable(practicedSystemCampaigns);
        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> refHarvestingPrices0;
        if (optionalPracticedSystemCampaigns.isPresent()) {
            refHarvestingPrices0 =
                    optionalPracticedSystemCampaigns
                            .map(campaigns -> referentialService.getRefHarvestingPricesForPracticedSystemValorisations(
                                    valorisations, campaigns, refHarvestingPrices, codeEspBotCodeQualiBySpeciesCode))
                            .orElse(new LinkedHashMap<>());
        } else {
            refHarvestingPrices0 =
                    referentialService.getRefHarvestingPricesForValorisations(valorisations, refHarvestingPrices, codeEspBotCodeQualiBySpeciesCode);
        }
    
        LinkedHashMap<HarvestingActionValorisation, Map<Integer, List<RefHarvestingPrice>>> pricesByValorisationAndCampaigns
                = getPricesByValorisationAndCampaigns(refHarvestingPrices0);

        LinkedHashMap<HarvestingActionValorisation, List<HarvestingValorisationPriceSummary>> averagePricesForValorisationsByCampaigns = computeAverageReferencesPricesForValorisations(
                pricesByValorisationAndCampaigns);

        LinkedHashMap<String, HarvestingValorisationPriceSummary> averageRefPricesByValorisationIds =
                computeAverageRefPricesForValorisations(
                        valorisations,
                        averagePricesForValorisationsByCampaigns);
    
        return averageRefPricesByValorisationIds;
    }
    
    protected Map<String, HarvestingValorisationPriceSummary> computeMedianUserPricesForValorisations(
            Collection<HarvestingActionValorisation> harvestingActionValorisations, Map<String, HarvestingValorisationPriceSummary> averageReferencePricesForValorisations,
            MultiValuedMap<String, HarvestingPrice> pricesForSimilarCodeEspeceBotanique) {
        
        LinkedHashMap<String, HarvestingValorisationPriceSummary> result = new LinkedHashMap<>();

        for (HarvestingActionValorisation speciesValorisation : harvestingActionValorisations) {
            Collection<HarvestingPrice> relatedPriceForSpecies = pricesForSimilarCodeEspeceBotanique.get(speciesValorisation.getSpeciesCode());
            List<HarvestingPrice> userPrices = findValorisationUserPrices(
                    speciesValorisation,
                    relatedPriceForSpecies
            );
            
            // median prices
            // lover prices
            // higher prices
            // nb prices by campaigns
            HarvestingValorisationPriceSummary refPriceSummary = averageReferencePricesForValorisations.get(speciesValorisation.getTopiaId());
            HarvestingValorisationPriceSummary productPriceSummary = createUserPriceSummaryFromPrices(userPrices, refPriceSummary);

            result.put(speciesValorisation.getTopiaId(), productPriceSummary);

        }
        
        return result;
    }

    public List<HarvestingPrice> findValorisationUserPrices(HarvestingActionValorisation valorisation,
                                                            Collection<HarvestingPrice> relatedPriceForSpecies) {

        if (CollectionUtils.isEmpty(relatedPriceForSpecies)) {
            return new ArrayList<>();
        }

        return relatedPriceForSpecies.stream().filter(rps -> {
            HarvestingActionValorisation harvestingActionValorisation = rps.getHarvestingActionValorisation();
            return harvestingActionValorisation.getDestination().equals(valorisation.getDestination()) &&
                    harvestingActionValorisation.getBeginMarketingPeriod() >= valorisation.getBeginMarketingPeriod() &&
                    harvestingActionValorisation.getEndingMarketingPeriod() <= valorisation.getEndingMarketingPeriod() &&
                    harvestingActionValorisation.isIsOrganicCrop() == valorisation.isIsOrganicCrop();
        }).distinct().collect(Collectors.toList());

    }
    
    protected HarvestingValorisationPriceSummary createUserPriceSummaryFromPrices(
            List<HarvestingPrice> userPrices,
            HarvestingValorisationPriceSummary refPriceSummary) {

        HarvestingValorisationPriceSummary productPriceSummary = new HarvestingValorisationPriceSummary();
        if (!userPrices.isEmpty()) {
            List<PriceAndUnit> priceAndUnits = getSameUnitPriceValues(refPriceSummary, userPrices);
            if (!priceAndUnits.isEmpty()) {
                Double medianPrice = computeMedianPrice(priceAndUnits);
                productPriceSummary.setMedianPrice(medianPrice);
                productPriceSummary.setNbPricesByCampaigns(computeNbPricesByCampaigns(userPrices));
                productPriceSummary.setLowerPrice(priceAndUnits.getFirst().value());
                productPriceSummary.setHigherPrice(priceAndUnits.get(priceAndUnits.size() - 1).value());
                productPriceSummary.setCountedPrices(priceAndUnits.size());
                productPriceSummary.setPriceUnit(priceAndUnits.getFirst().unit());
            }
        }
        return productPriceSummary;
    }

    protected List<PriceAndUnit> getSameUnitPriceValues(
            HarvestingValorisationPriceSummary refPriceSummary,
            List<HarvestingPrice> prices) {
        List<PriceAndUnit> pricesValues = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(prices) && refPriceSummary != null) {


            for (HarvestingPrice price : prices) {
                computeSameUnitPriceValue(refPriceSummary.getPriceUnit(), price).ifPresent(pricesValues::add);
            }

            if (pricesValues.isEmpty()) {// no result found with ref price, force use user price unit
                PriceUnit priceUnit = extractPriceUnit(prices);
                PriceUnit userPriceUnit = priceUnit;
                for (HarvestingPrice price : prices) {
                    computeSameUnitPriceValue(userPriceUnit, price).ifPresent(pricesValues::add);
                }
            }
        }
        return pricesValues.stream().sorted(Comparator.comparingDouble(PriceAndUnit::value)).toList();
    }

    protected static PriceUnit extractPriceUnit(List<HarvestingPrice> prices) {
        PriceUnit refPriceUnitOrUserUnit = PricesService.DEFAULT_PRICE_UNIT;

        Optional<HarvestingPrice> aPrice = prices.stream()
                .filter(p -> p.getPrice() != null && !PricesService.DEFAULT_PRICE_UNIT.equals(p.getPriceUnit()))
                .findAny();
        if (aPrice.isPresent()) {
            refPriceUnitOrUserUnit = aPrice.get().getPriceUnit();
        } else {
            aPrice = prices.stream()
                .filter(p -> !PricesService.DEFAULT_PRICE_UNIT.equals(p.getPriceUnit()))
                .findAny();
        }
        if (aPrice.isPresent()) {
            refPriceUnitOrUserUnit = aPrice.get().getPriceUnit();
        }

        return refPriceUnitOrUserUnit;
    }

    private static Optional<PriceAndUnit> computeSameUnitPriceValue(PriceUnit priceUnit, HarvestingPrice price) {
        PriceAndUnit result = null;
        if (price.getPrice() != null && price.getPrice() != 0) {
            if (price.getPriceUnit() == priceUnit) {
                result = new PriceAndUnit(price.getPrice(), priceUnit);
            } else {
                Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(price.getPriceUnit(), priceUnit));
                if (priceUnitConversionRate != null && priceUnitConversionRate != 0) {
                    result = new PriceAndUnit(price.getPrice() * priceUnitConversionRate, priceUnit);
                } else if (LOGGER.isDebugEnabled() &&
                        (!PricesService.DEFAULT_PRICE_UNIT.equals(price.getPriceUnit()) ||
                                !PricesService.DEFAULT_PRICE_UNIT.equals(priceUnit))) {
                    LOGGER.debug("PRICE WITH DIFFERENT UNIT CAN NOT BE SUM !");
                }
            }
        }
        return Optional.ofNullable(result);
    }

    protected Double computeMedianPrice(List<PriceAndUnit> prices) {

        Double medianPrice = null;
        if (!prices.isEmpty()) {
            List<Double> list = new ArrayList<>(prices.stream().map(PriceAndUnit::value).toList());
            medianPrice = DaoUtils.median(list);
        }
        return medianPrice;
    }

    private Double computeRefAveragePrice(List<RefHarvestingPrice> values) {

        Double averagePrice = null;

        if (CollectionUtils.isNotEmpty(values)) {
            OptionalDouble optAveragePrice = values.stream()
                    .filter(v -> v.getPrice() != null)
                    .mapToDouble(RefHarvestingPrice::getPrice)
                    .average();
            if (optAveragePrice.isPresent()) {
                averagePrice = optAveragePrice.getAsDouble();
            }
        }

        return averagePrice;
    }

    protected Map<Integer, Integer> computeNbPricesByCampaigns(List<HarvestingPrice> prices) {

        Map<Integer, Integer> pricesByCampaigns = new HashMap<>();

        if (CollectionUtils.isNotEmpty(prices)) {
            for (HarvestingPrice price : prices) {
                if (price.getPrice() != null && price.getPrice() != 0) {
                    Integer endingCampaign = price.getHarvestingActionValorisation().getEndingMarketingPeriodCampaign();
                    Integer count = pricesByCampaigns.get(endingCampaign);
                    Integer total = count == null ? 1 : count + 1;
                    pricesByCampaigns.put(endingCampaign, total);
                }
            }
        }

        return pricesByCampaigns;
    }

    protected LinkedHashMap<HarvestingActionValorisation, List<HarvestingValorisationPriceSummary>> computeAverageReferencesPricesForValorisations(
            LinkedHashMap<HarvestingActionValorisation, Map<Integer, List<RefHarvestingPrice>>> refHarvestingPricesByValorisationForCampaigns) {

        LinkedHashMap<HarvestingActionValorisation, List<HarvestingValorisationPriceSummary>> averagePricesForValorisationsByCampaigns = new LinkedHashMap<>();
        if (!refHarvestingPricesByValorisationForCampaigns.isEmpty()) {
            for (Map.Entry<HarvestingActionValorisation, Map<Integer, List<RefHarvestingPrice>>> pricesForValorisation
                    : refHarvestingPricesByValorisationForCampaigns.entrySet()) {
        
                HarvestingActionValorisation valorisation = pricesForValorisation.getKey();
                Map<Integer, List<RefHarvestingPrice>> pricesForCampaigns = pricesForValorisation.getValue();
                for (Map.Entry<Integer, List<RefHarvestingPrice>> priceForACampaign : pricesForCampaigns.entrySet()) {
                    List<RefHarvestingPrice> pricesForCampaign = priceForACampaign.getValue();
                    Map<Integer, Integer> nbPricesByCampaigns = new HashMap<>();
                    nbPricesByCampaigns.put(priceForACampaign.getKey(), pricesForCampaign.size());
            
                    PricesByHarvestingProducts pricesByHarvestingProducts = new PricesByHarvestingProducts(pricesForCampaign).invoke();
                    // price are same unit
                    Map<String, List<RefHarvestingPrice>> pricesByEspece = pricesByHarvestingProducts.getPricesByHarvestingProduct();
                    PriceUnit priceUnit = pricesByHarvestingProducts.getPriceUnit();
    
                    Double averagePrice = computeHarvestingAveragePrice(pricesByEspece, priceUnit);
    
                    List<HarvestingValorisationPriceSummary> allCampaignPricesForValorisation =
                            averagePricesForValorisationsByCampaigns.computeIfAbsent(valorisation, k -> new ArrayList<>());
    
                    HarvestingValorisationPriceSummary currentValorisationPrice = new HarvestingValorisationPriceSummary();
                    currentValorisationPrice.setPriceUnit(priceUnit);
                    currentValorisationPrice.setAveragePrice(averagePrice);
                    currentValorisationPrice.setNbPricesByCampaigns(nbPricesByCampaigns);

                    currentValorisationPrice.addSources(pricesByEspece.values().stream()
                            .flatMap(Collection::stream)
                            .filter(RefHarvestingPrice::isActive)
                            .distinct()
                            .collect(Collectors.toMap(
                                    refHarvestingPrice -> refHarvestingPrice.getCampaign() + " - "
                                            + refHarvestingPrice.getMarketingPeriodDecade() + " - "
                                            + refHarvestingPrice.getMarketingPeriod() + " - "
                                            + refHarvestingPrice.getProduitRecolte(),
                                    refHarvestingPrice -> Objects.toString(refHarvestingPrice.getSource(), ExportUtils.NULL_SOURCE_PLACEHOLDER),
                                    (source0, source1) -> source0)
                            ));

                    allCampaignPricesForValorisation.add(currentValorisationPrice);
                }
            }
        }
        return averagePricesForValorisationsByCampaigns;
    }

    protected LinkedHashMap<String, HarvestingValorisationPriceSummary> computeAverageRefPricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            LinkedHashMap<HarvestingActionValorisation, List<HarvestingValorisationPriceSummary>> averagePricesForValorisationsByCampaigns) {

        LinkedHashMap<String, HarvestingValorisationPriceSummary> averageRefPricesByValorisationIds = new LinkedHashMap<>();
        for (HarvestingActionValorisation valorisation : valorisations) {
            /*
              Prix_culture simple:
              f ( Espèce * Destination * AB ? * Campagne * Période)

             */
            List<HarvestingValorisationPriceSummary> allCampaignPricesForValorisation = averagePricesForValorisationsByCampaigns.get(valorisation);
            if (CollectionUtils.isEmpty(allCampaignPricesForValorisation)) {
                // no prices for this valorisation
                continue;
            }
    
            HarvestingValorisationPriceSummary averagePricesForAllCampaings =
                    computeAverageMedianPriceForCampaigns(allCampaignPricesForValorisation);

            averageRefPricesByValorisationIds.putAll(addReferencePriceToResult(
                    valorisation,
                    averagePricesForAllCampaings,
                    averagePricesForAllCampaings.getMedianPrice(),
                    averagePricesForAllCampaings.getSources()
            ));
        }
        return averageRefPricesByValorisationIds;
    
    }
    
    protected HarvestingValorisationPriceSummary computeAverageMedianPriceForCampaigns(
            List<HarvestingValorisationPriceSummary> allCampaignPricesForValorisation) {
        
        HarvestingValorisationPriceSummary averagePricesForCampaigns = new HarvestingValorisationPriceSummary();
        Map<Integer, Integer> nbPricesForCampaign = new HashMap<>();
        averagePricesForCampaigns.setNbPricesByCampaigns(nbPricesForCampaign);
        
        Double averagePrice = null;
        PriceUnit priceUnit;
        Map<String, String> sources = new HashMap<>();
        
        List<HarvestingValorisationPriceSummary> notNullAveragePricesForValorisation = allCampaignPricesForValorisation.stream()
                .filter(s -> s.getAveragePrice() != null).toList();
        
        if (!notNullAveragePricesForValorisation.isEmpty()) {
            
            final HarvestingValorisationPriceSummary valorisationPrice = notNullAveragePricesForValorisation.getFirst();
            priceUnit = valorisationPrice.getPriceUnit();

            double sum = 0d;
            for (HarvestingValorisationPriceSummary price : notNullAveragePricesForValorisation) {
                
                Map<Integer, Integer> nbPricesForCampaigns = price.getNbPricesByCampaigns();
                for (Map.Entry<Integer, Integer> nbPricesByCampaign : nbPricesForCampaigns.entrySet()) {
                    
                    Integer campaign = nbPricesByCampaign.getKey();
                    
                    Integer allNbPricesForCampaign = nbPricesForCampaign.get(campaign);
                    
                    Integer currentNbPricesForCampaign = allNbPricesForCampaign == null ?
                            nbPricesByCampaign.getValue() :
                            allNbPricesForCampaign +
                                    nbPricesByCampaign.getValue();
                    
                    nbPricesForCampaign.put(campaign, currentNbPricesForCampaign);
                }
                PriceUnit priceUnit1 = price.getPriceUnit();
                if (priceUnit1 == priceUnit) {
                    sum += price.getAveragePrice();
                } else {
                    Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(priceUnit1, priceUnit));
                    if (priceUnitConversionRate != null && priceUnitConversionRate != 0) {
                        sum += price.getAveragePrice() * priceUnitConversionRate;
                    } else if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("PRICE WITH DIFFERENT UNIT CAN NOT BE SUM !");
                    }
                }
                
                sources.putAll(price.getSources());
            }
            int nbCampaignsWithAveragePrice = nbPricesForCampaign.size();
            averagePrice = nbCampaignsWithAveragePrice == 0 ? 0 : sum / nbCampaignsWithAveragePrice;
            
        } else {
            priceUnit = allCampaignPricesForValorisation.getFirst().getPriceUnit();
        }
        // ceci n'est pas ue médiane, mais une moyenne !
        // vue avec INRAE le 16/12/2024, pour l'instant on ne change pas le comportement
        averagePricesForCampaigns.setPriceUnit(priceUnit);
        averagePricesForCampaigns.setMedianPrice(averagePrice);
        averagePricesForCampaigns.addSources(sources);
        
        return averagePricesForCampaigns;
    }

    protected LinkedHashMap<String, HarvestingValorisationPriceSummary> addReferencePriceToResult(HarvestingActionValorisation mainVal,
                                                                                                  HarvestingValorisationPriceSummary price0,
                                                                                                  Double referencePrice,
                                                                                                  Map<String, String> sources) {
        LinkedHashMap<String, HarvestingValorisationPriceSummary> averageRefPricesByValorisationIds = new LinkedHashMap<>();
        HarvestingValorisationPriceSummary mainValorisationReferencePrice = new HarvestingValorisationPriceSummary();
    
        mainValorisationReferencePrice.setAveragePrice(referencePrice);
        mainValorisationReferencePrice.setPriceUnit(price0.getPriceUnit());
        mainValorisationReferencePrice.setNbPricesByCampaigns(price0.getNbPricesByCampaigns());
        mainValorisationReferencePrice.addSources(sources);
        averageRefPricesByValorisationIds.put(mainVal.getTopiaId(), mainValorisationReferencePrice);
        return averageRefPricesByValorisationIds;
    }
    
    
    protected Double computeHarvestingAveragePrice(
            Map<String, List<RefHarvestingPrice>> pricesByEspece,
            PriceUnit priceUnit) {
        
        Double averagePrice = null;
        if (priceUnit != null) {
            
            List<Double> averagePricesForSpecies = new ArrayList<>();
            for (List<RefHarvestingPrice> harvestingPrices : pricesByEspece.values()) {
                
                Double speciesAveragePrice = computeRefAveragePrice(harvestingPrices);
                
                if (speciesAveragePrice != null) {
                    averagePricesForSpecies.add(speciesAveragePrice);
                }
            }
            
            if (!averagePricesForSpecies.isEmpty()) {
                averagePrice = averagePricesForSpecies
                        .stream()
                        .mapToDouble(Double::doubleValue)
                        .average()
                        .orElse(0.0);// should not be 0 as refPrice values must be > 0
            }
        }
        return averagePrice;
    }

    protected LinkedHashMap<HarvestingActionValorisation, Map<Integer, List<RefHarvestingPrice>>> getPricesByValorisationAndCampaigns(
            LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> refHarvestingPrices) {

        LinkedHashMap<HarvestingActionValorisation, Map<Integer, List<RefHarvestingPrice>>> result = new LinkedHashMap<>();

        for (Map.Entry<HarvestingActionValorisation, List<RefHarvestingPrice>> harvestingActionValorisationListEntry : refHarvestingPrices.entrySet()) {
            HarvestingActionValorisation valorisation = harvestingActionValorisationListEntry.getKey();

            Map<Integer, List<RefHarvestingPrice>> pricesByCampaigns = new HashMap<>();

            List<RefHarvestingPrice> pricesForValorisation = harvestingActionValorisationListEntry.getValue();
    
            if (CollectionUtils.isNotEmpty(pricesForValorisation)){
        
                for (RefHarvestingPrice refHarvestingPrice : pricesForValorisation) {
                    Integer campaign = refHarvestingPrice.getCampaign();
                    List<RefHarvestingPrice> priceForCampaign = pricesByCampaigns.computeIfAbsent(campaign, k -> new ArrayList<>());
                    priceForCampaign.add(refHarvestingPrice);
                }
        
            }
            result.put(valorisation, pricesByCampaigns);
        }
        return result;
    }


    @Override
    public HarvestingPrice newHarvestingPriceInstance() {
        final HarvestingPrice harvestingPrice = harvestingPriceDao.newInstance();
        return harvestingPrice;
    }
    
    @Override
    public void persistPrice(HarvestingPrice harvestingPrice) {

        if (harvestingPrice == null) {
            return;
        }
        if (harvestingPrice.isPersisted() && !harvestingPrice.getTopiaId().startsWith(HarvestingActionValorisationTopiaDao.NEW_HARVESTING_VALORISATION)) {
            harvestingPriceDao.update(harvestingPrice);
        } else {
            harvestingPrice.setTopiaId(null);
            harvestingPriceDao.create(harvestingPrice);
        }
        
    }

    @Override
    public HarvestingPrice bindDtoToPrice(
            Domain domain,
            HarvestingActionValorisation valorisation,
            InputPriceDto priceDto,
            HarvestingPrice price,
            String priceObjectId,
            Optional<PracticedSystem> harvestingPracticedSystem,
            Optional<Zone> harvestingZone) {
    
        price.setDomain(domain);
        price.setObjectId(priceObjectId);
        price.setDisplayName(priceDto.getDisplayName());
        price.setPrice(priceDto.getPrice());
        price.setPriceUnit(priceDto.getPriceUnit());
        price.setSourceUnit(priceDto.getSourceUnit());
        price.setHarvestingActionValorisation(valorisation);
        
        Preconditions.checkArgument(harvestingPracticedSystem.isPresent() || harvestingZone.isPresent());
        price.setPracticedSystem(harvestingPracticedSystem.orElse(null));
        price.setZone(harvestingZone.orElse(null));
        
        return price;
    }


    @Override
    public List<HarvestingPrice> loadHarvestingPrices(
            String domainTopiaId) {
        
        List<HarvestingPrice> result;
        // If no domainId specified, return an empty list
        if (StringUtils.isEmpty(domainTopiaId)) {
            result = new LinkedList<>();
        } else {
            result = harvestingPriceDao.findPrices(
                    domainTopiaId,
                    null);
        }
        return result;
    }

    @Override
    public LoadedPricesResult getHarvestingPriceResults(
            String domainId) {

        LoadedPricesResult loadedPricesResult = new LoadedPricesResult();

        if (StringUtils.isNotBlank(domainId)) {
            List<HarvestingPriceDto> harvestingPriceDtos = loadHarvestingDomainPrices(domainId);
            loadedPricesResult.setHarvestingValorisationPrices(harvestingPriceDtos);
        }

        return loadedPricesResult;
    }

    private static List<HarvestingPriceDto> addAllowedUnitsToHarvestingPriceDtos(
            List<HarvestingPriceDto> harvestingPriceDtos,
            Map<HarvestingPrice, Collection<PriceUnit>> allowedUnits) {
        
        List<HarvestingPriceDto> result = new ArrayList<>();
        Map<String, HarvestingPriceDto> harvestingPriceDtoByIds = harvestingPriceDtos.stream().collect(Collectors.toMap(HarvestingPriceDto::getTopiaId, Function.identity()));

        allowedUnits.forEach((hpDto, pus) -> {
                    final HarvestingPriceDto harvestingPriceDto = harvestingPriceDtoByIds.get(hpDto.getTopiaId());
                    if (harvestingPriceDto != null) {
                        result.add(harvestingPriceDto.toBuilder().allowedPriceUnitsForPrice(pus).build());
                    } else {
                        LOGGER.error("Price without zone or domain detected: " + hpDto.getTopiaId() + " - " + hpDto.getDomain().getTopiaId());
                    }
        });
        return result;
    }

    private List<HarvestingPriceDto> getHarvestingPriceDtos(
            List<HarvestingPrice> harvestingPrices) {

        Collection<HarvestingActionValorisation> valorisations = harvestingPrices.stream().map(
                HarvestingPrice::getHarvestingActionValorisation).toList();

        List<HarvestingPriceDto> harvestingPriceDtos = new ArrayList<>();
        Map<HarvestingActionValorisation, String> valorisationsToActionId = actionService.getHarvestingActionValorisationsActions(valorisations);
        harvestingPrices.forEach(
                hp -> {
                    String actionId = valorisationsToActionId.get(hp.getHarvestingActionValorisation());
                    harvestingPriceDtos.add(PricesService.bindHarvestingPriceToHarvestingPriceDto(
                            hp.getDomain(),
                            hp.getPracticedSystem(),
                            hp.getZone(),
                            hp,
                            actionId));
                }
        );
        return harvestingPriceDtos;
    }

    protected Map<HarvestingPrice, Collection<PriceUnit>> getPracticedHarvestingPricesAllowedUnits(
            Collection<HarvestingPrice> harvestingPrices,
            String campaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
        
        Collection<HarvestingActionValorisation> valorisations = harvestingPrices.stream()
                .map(HarvestingPrice::getHarvestingActionValorisation)
                .collect(Collectors.toList());
        
        // return prices by valorisation. Prices or ordered by increasing price values and units
        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> refHarvestingPrices = referentialService.getRefHarvestingPricesForPracticedSystemValorisations(
                valorisations, campaigns, codeEspBotCodeQualiBySpeciesCode);

        Map<HarvestingPrice, Collection<RefHarvestingPrice>> priceRefPrices = harvestingPrices.stream()
                .collect(Collectors.toMap(Function.identity(),
                        price -> CollectionUtils.emptyIfNull(refHarvestingPrices.get(price.getHarvestingActionValorisation()))));

        Map<HarvestingPrice, Collection<PriceUnit>> allowedPriceUnitsByHarvestingPrice = getAllowedPriceUnitsByHarvestingPrice(priceRefPrices);

        return allowedPriceUnitsByHarvestingPrice;
        
    }

    protected Map<HarvestingPrice, Collection<PriceUnit>> getEffectiveHarvestingPricesAllowedUnits(
            Collection<HarvestingPrice> harvestingPrices,
            List<RefHarvestingPrice> refHarvestingPrices0,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        Collection<HarvestingActionValorisation> valorisations = harvestingPrices.stream()
                .map(HarvestingPrice::getHarvestingActionValorisation)
                .collect(Collectors.toList());

        // return prices by valorisation. Prices or ordered by increasing price values and units
        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> refHarvestingPrices = referentialService.getRefHarvestingPricesForValorisations(
                valorisations, refHarvestingPrices0, codeEspBotCodeQualiBySpeciesCode);

        Map<HarvestingPrice, Collection<RefHarvestingPrice>> priceRefPrices = harvestingPrices.stream()
                .collect(Collectors.toMap(Function.identity(),
                        price -> CollectionUtils.emptyIfNull(refHarvestingPrices.get(price.getHarvestingActionValorisation()))));

        Map<HarvestingPrice, Collection<PriceUnit>> allowedPriceUnitsByHarvestingPrice = getAllowedPriceUnitsByHarvestingPrice(priceRefPrices);

        return allowedPriceUnitsByHarvestingPrice;

    }

    protected List<HarvestingPriceDto> loadHarvestingDomainPrices(String domainId) {

        List<HarvestingPrice> harvestingPrices = loadHarvestingPrices(domainId);

        List<HarvestingPriceDto> harvestingPriceDtos = getHarvestingPriceDtos(harvestingPrices);

        List<HarvestingPriceDto> harvestingPriceDto = addValorisationPriceSummary(
                domainId,
                harvestingPrices,
                harvestingPriceDtos
        );

        return harvestingPriceDto;
    }

    private List<HarvestingPriceDto> addValorisationPriceSummary(
            String domainId,
            List<HarvestingPrice> harvestingPrices,
            List<HarvestingPriceDto> harvestingPriceDtos) {

        Domain d0 = domainService.getDomain(domainId);
        Set<String> speciesCodes = harvestingPrices.stream().map(HarvestingPrice::getHarvestingActionValorisation).map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode = referentialService.getCodeEspBotCodeQualiBySpeciesCode(speciesCodes);

        Set<RefHarvestingPriceFilter> refHarvestingPriceFilters = new HashSet<>();
        harvestingPrices.forEach(hp -> {

            HarvestingActionValorisation harvestingActionValorisation = hp.getHarvestingActionValorisation();
            RefDestination destination = harvestingActionValorisation.getDestination();
            boolean isOrganicCrop = harvestingActionValorisation.isIsOrganicCrop();
            int beginMarketingPeriodCampaign = harvestingActionValorisation.getBeginMarketingPeriodCampaign();
            int endingMarketingPeriodCampaign = harvestingActionValorisation.getEndingMarketingPeriodCampaign();
            Set<Pair<String, String>> codeEspBotCodeQualis = codeEspBotCodeQualiBySpeciesCode.get(harvestingActionValorisation.getSpeciesCode());

            for (Pair<String, String> codeEspBotCodeQuali : codeEspBotCodeQualis) {
                RefHarvestingPriceFilter rhf = new RefHarvestingPriceFilter(
                        codeEspBotCodeQuali.getLeft(),
                        codeEspBotCodeQuali.getRight(),
                        destination.getCode_destination_A(),
                        isOrganicCrop,
                        hp.getPriceUnit(),
                        beginMarketingPeriodCampaign,
                        endingMarketingPeriodCampaign
                );
                refHarvestingPriceFilters.add(rhf);
            }

        });
        MultiValuedMap<String, HarvestingPrice> pricesForSimilarCodeEspeceBotanique = getPricesForCodeEspeceBotaniqueCodeQualifiant(codeEspBotCodeQualiBySpeciesCode, d0);


        List<RefHarvestingPrice> refHarvestingPrices = getRefPricesForCodeEspeceBotaniqueCodeQualifiant(refHarvestingPriceFilters);

        Map<String, HarvestingPriceDto> harvestingPriceDtoByValorisationIds = harvestingPriceDtos.stream().collect(Collectors.toMap(HarvestingPriceDto::getValorisationId, Function.identity()));

        List<HarvestingPriceDto> harvestingPriceDto = addPracticedValorisationPriceSummary(
                harvestingPrices,
                refHarvestingPrices,
                harvestingPriceDtoByValorisationIds,
                codeEspBotCodeQualiBySpeciesCode,
                pricesForSimilarCodeEspeceBotanique
        );

        harvestingPriceDto.addAll(addEffectiveValorisationPriceSummary(
                harvestingPrices,
                refHarvestingPrices,
                harvestingPriceDtoByValorisationIds,
                codeEspBotCodeQualiBySpeciesCode,
                pricesForSimilarCodeEspeceBotanique
        ));

        return harvestingPriceDto;
    }

    @NotNull
    private MultiValuedMap<String, HarvestingPrice> getPricesForCodeEspeceBotaniqueCodeQualifiant(
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            Domain domain) {

        MultiValuedMap<String, HarvestingPrice>  pricesForSimilarCodeEspeceBotanique = new HashSetValuedHashMap<>();
        Set<String> allCodeEspeceBotaniques = codeEspBotCodeQualiBySpeciesCode.values().stream().flatMap(Collection::stream).map(Pair::getKey).collect(Collectors.toSet());
        MultiValuedMap<String, HarvestingPrice> harvestingPriceByCodeEspeceBotanique = harvestingPriceDao.loadUserPriceForCodeEspeceBotaniquesAndGivenCampaigns(
                allCodeEspeceBotaniques,
                domain,
                domain.getCampaign()
        );

        for (Map.Entry<String, Set<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiant : codeEspBotCodeQualiBySpeciesCode.entrySet()) {
            String speciesCode = speciesCodeToCodeEspeceBotaniqueCodeQualifiant.getKey();
            Set<Pair<String, String>> codeEspeceBotaniqueCodeQualifants = speciesCodeToCodeEspeceBotaniqueCodeQualifiant.getValue();
            CollectionUtils.emptyIfNull(codeEspeceBotaniqueCodeQualifants)
                    .forEach(cebcq -> {
                        String ceb = cebcq.getKey();
                        Collection<HarvestingPrice> harvestingPrices1 = harvestingPriceByCodeEspeceBotanique.get(ceb);
                        if (CollectionUtils.isNotEmpty(harvestingPrices1)) {
                            pricesForSimilarCodeEspeceBotanique.putAll(speciesCode, harvestingPrices1);
                        }
                    });
        }
        return pricesForSimilarCodeEspeceBotanique;
    }

    private List<RefHarvestingPrice> getRefPricesForCodeEspeceBotaniqueCodeQualifiant(
            Set<RefHarvestingPriceFilter> refHarvestingPriceFilters) {

        Set<String> code_espece_botaniques = new HashSet<>();
        Set<String> code_destination = new HashSet<>();
        int beginMarketingPeriodCampaign = Integer.MAX_VALUE;
        int endingMarketingPeriodCampain = -Integer.MAX_VALUE;
        for (RefHarvestingPriceFilter refHarvestingPriceFilter : refHarvestingPriceFilters) {
            code_espece_botaniques.add(refHarvestingPriceFilter.code_espece_botanique());
            code_destination.add(refHarvestingPriceFilter.code_destination_A());
            beginMarketingPeriodCampaign = Math.min(beginMarketingPeriodCampaign, refHarvestingPriceFilter.beginMarketingPeriodCampaign());
            endingMarketingPeriodCampain = Math.max(endingMarketingPeriodCampain, refHarvestingPriceFilter.endingMarketingPeriodCampaign());
        }
        GlobalRefHarvestingPriceFilter globalRefHarvestingPriceFilter = new GlobalRefHarvestingPriceFilter(
                code_espece_botaniques,
                code_destination,
                beginMarketingPeriodCampaign - 3,
                endingMarketingPeriodCampain
        );
        List<RefHarvestingPrice> refHarvestingPrices = refHarvestingPriceDao.loadRefPriceForCodeEspeceBotaniquesAndGivenCampaigns(globalRefHarvestingPriceFilter);

        return refHarvestingPrices;
    }


    protected List<HarvestingPriceDto> addEffectiveValorisationPriceSummary(
            List<HarvestingPrice> harvestingPrices,
            List<RefHarvestingPrice> refHarvestingPrices,
            Map<String, HarvestingPriceDto> harvestingPriceDtoByValorisationIds,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            MultiValuedMap<String, HarvestingPrice> speciesCodeToSimilarOtherSpeciesCode) {
        List<HarvestingPriceDto> result = new ArrayList<>();
        List<HarvestingPriceDto> zoneHarvestingPriceDto = new ArrayList<>();
        List<HarvestingPrice> zonePrices = harvestingPrices.stream().filter(hp -> hp.getZone() != null).toList();
        for (HarvestingPrice zonePrice : zonePrices) {

            zoneHarvestingPriceDto.add(addValorisationPriceSummary(
                    harvestingPriceDtoByValorisationIds,
                    refHarvestingPrices,
                    null,
                    zonePrice.getHarvestingActionValorisation(),
                    codeEspBotCodeQualiBySpeciesCode,
                    speciesCodeToSimilarOtherSpeciesCode));
        }
        if (!zoneHarvestingPriceDto.isEmpty()) {

            Map<HarvestingPrice, Collection<PriceUnit>> allowedUnits = getEffectiveHarvestingPricesAllowedUnits(
                    zonePrices,
                    refHarvestingPrices,
                    codeEspBotCodeQualiBySpeciesCode);
            result = addAllowedUnitsToHarvestingPriceDtos(zoneHarvestingPriceDto, allowedUnits);
        }
        return result;
    }

    protected List<HarvestingPriceDto> addPracticedValorisationPriceSummary(
            List<HarvestingPrice> harvestingPrices,
            List<RefHarvestingPrice> refHarvestingPrices,
            Map<String, HarvestingPriceDto> harvestingPriceDtoByValorisationIds,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            MultiValuedMap<String, HarvestingPrice> pricesForSimilarCodeEspeceBotanique) {

        MultiValuedMap<PracticedSystem, HarvestingPrice> practicedSystemPrices = new HashSetValuedHashMap<>();
        harvestingPrices.stream().filter(hp -> hp.getPracticedSystem() != null).forEach(hp -> practicedSystemPrices.put(hp.getPracticedSystem(), hp));

        List<HarvestingPriceDto> harvestingPriceDtos = new ArrayList<>();
        for (PracticedSystem ps : practicedSystemPrices.keys()) {
            Collection<HarvestingPrice> harvestingPrices0 = practicedSystemPrices.get(ps);
            List<HarvestingPriceDto> harvestingPriceDtos_ = new ArrayList<>();
            for (HarvestingPrice practicedSystemPrice : harvestingPrices0) {
                HarvestingPriceDto priceDto = addValorisationPriceSummary(
                        harvestingPriceDtoByValorisationIds,
                        refHarvestingPrices,
                        ps.getCampaigns(),
                        practicedSystemPrice.getHarvestingActionValorisation(),
                        codeEspBotCodeQualiBySpeciesCode,
                        pricesForSimilarCodeEspeceBotanique
                );
                harvestingPriceDtos_.add(priceDto);
            }
            Map<HarvestingPrice, Collection<PriceUnit>> allowedUnits = getPracticedHarvestingPricesAllowedUnits(harvestingPrices0, ps.getCampaigns(), codeEspBotCodeQualiBySpeciesCode);
            harvestingPriceDtos.addAll(addAllowedUnitsToHarvestingPriceDtos(harvestingPriceDtos_, allowedUnits));
        }
        return harvestingPriceDtos;
    }

    private HarvestingPriceDto addValorisationPriceSummary(
            Map<String, HarvestingPriceDto> harvestingPriceDtoByValorisationIds,
            List<RefHarvestingPrice> refHarvestingPrices,
            String practicedSystemCampaigns,
            HarvestingActionValorisation harvestingActionValorisation,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            MultiValuedMap<String, HarvestingPrice> pricesForSimilarCodeEspeceBotanique) {

        Collection<HarvestingActionValorisation> valorisations = List.of(harvestingActionValorisation);
        Map<String, HarvestingValorisationPriceSummary> averageReferencePricesForValorisations =
                computeAverageReferencePricesForValorisations(
                        valorisations,
                        refHarvestingPrices,
                        practicedSystemCampaigns,
                        codeEspBotCodeQualiBySpeciesCode);

        Map<String, HarvestingValorisationPriceSummary> userSummaryPriceForValorisations =
                computeMedianUserPricesForValorisations(
                        valorisations,
                        averageReferencePricesForValorisations,
                        pricesForSimilarCodeEspeceBotanique
                );

        HarvestingValorisationPriceSummary averageReferencePricesForValorisation = averageReferencePricesForValorisations.get(harvestingActionValorisation.getTopiaId());
        HarvestingValorisationPriceSummary userSummaryPriceForValorisation = userSummaryPriceForValorisations.get(harvestingActionValorisation.getTopiaId());

        HarvestingPriceDto harvestingPriceDto = harvestingPriceDtoByValorisationIds.get(harvestingActionValorisation.getTopiaId());
        harvestingPriceDto = harvestingPriceDto.toBuilder()
                .averageReferencePricesForValorisations(averageReferencePricesForValorisation)
                .userSummaryPriceForValorisations(userSummaryPriceForValorisation)
                .build();

        return harvestingPriceDto;
    }

    private Map<HarvestingPrice, Collection<PriceUnit>> getAllowedPriceUnitsByHarvestingPrice(Map<HarvestingPrice, Collection<RefHarvestingPrice>> priceRefPrices) {

        Map<HarvestingPrice, Collection<PriceUnit>> result = new HashMap<>();
        if (!priceRefPrices.isEmpty()) {

            for (HarvestingPrice price : priceRefPrices.keySet()) {

                Collection<RefHarvestingPrice> refHarvestingPrices = priceRefPrices.getOrDefault(price, new HashSet<>());

                result.put(price, getAllowedPriceUnitsForHarvestingPrice(price, refHarvestingPrices));
            }
        }
        return result;
    }

    private static Set<PriceUnit> getAllowedPriceUnitsForHarvestingPrice(
            HarvestingPrice price, Collection<RefHarvestingPrice> refHarvestingPrices) {

        Optional<RefDestination> destination = Optional.ofNullable(price)
                .map(HarvestingPrice::getHarvestingActionValorisation)
                .map(HarvestingActionValorisation::getDestination);

        WineValorisation wineValorisation = destination.map(RefDestination::getWineValorisation).orElse(null);
        YealdUnit yealdUnit = destination.map(RefDestination::getYealdUnit).orElse(null);

        Collection<PriceUnit> rpUnits = CollectionUtils.emptyIfNull(refHarvestingPrices).stream()
                .map(RefHarvestingPrice::getPriceUnit)
                .collect(Collectors.toSet());

        Set<PriceUnit> units;
        if (CollectionUtils.isEmpty(rpUnits)) {
            units = new HashSet<>(DEFAULT_PRICE_UNITS);// default value
        } else {
            units = new LinkedHashSet<>(rpUnits);
            if (WineValorisation.WINE.equals(wineValorisation) && YealdUnit.KG_RAISIN_HA.equals(yealdUnit)) {
                units.addAll(WINE_PRICE_UNITS);
            } else {
                if (CollectionUtils.containsAny(WEIGTH_PRICE_UNITS, rpUnits)) {
                    units.addAll(WEIGTH_PRICE_UNITS);
                }
                if (CollectionUtils.containsAny(WINE_PRICE_UNITS, rpUnits)) {
                    units.addAll(WINE_PRICE_UNITS);
                }
                if (CollectionUtils.containsAny(VOL_PRICE_UNITS, rpUnits)) {
                    units.addAll(VOL_PRICE_UNITS);
                }
            }
            units.add(PricesService.DEFAULT_PRICE_UNIT);// default value
        }
        return units;
    }

    @Override
    public void bindZoneOrPracticedHarvestingPrice(
            HarvestingActionValorisationDto valorisationDto,
            HarvestingActionValorisation valorisationEntity,
            HarvestingPrice persistedPrice,
            RefDestination destination,
            CroppingPlanSpecies cps,
            Domain domain,
            PracticedSystem practicedSystem,
            Zone zone,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode,
            Map<HarvestingActionValorisationDto, List<RefHarvestingPrice>> refHarvestingPricesForValorisationDtos) {

        HarvestingPrice harvestingPriceEntity = ObjectUtils.firstNonNull(persistedPrice, newHarvestingPriceInstance());

        harvestingPriceEntity.setPracticedSystem(practicedSystem);
        harvestingPriceEntity.setZone(zone);
        harvestingPriceEntity.setDomain(domain);
        harvestingPriceEntity.setHarvestingActionValorisation(valorisationEntity);
        harvestingPriceEntity.setSourceUnit(valorisationDto.getYealdUnit().name());

        final String objectId = PricesService.GET_HARVESTING_ACTION_VALORISATION_OBJECT_ID.apply(cps, destination);
        final String displayName = PricesService.GET_HARVESTING_PRICE_DISPLAY_NAME.apply(cps);
        harvestingPriceEntity.setObjectId(objectId);
        harvestingPriceEntity.setDisplayName(displayName);

        if(harvestingPriceEntity.getPriceUnit() == null) {
            if (refHarvestingPricesForValorisationDtos != null) {
                List<RefHarvestingPrice> refHarvestingPrices = refHarvestingPricesForValorisationDtos.get(valorisationDto);
                Set<PriceUnit> allowedPriceUnitsByHarvestingPrice = getAllowedPriceUnitsForHarvestingPrice(harvestingPriceEntity, refHarvestingPrices);
                PriceUnit priceUnit = allowedPriceUnitsByHarvestingPrice.stream().filter(pu -> !PricesService.DEFAULT_PRICE_UNIT.equals(pu)).findAny().orElse(PricesService.DEFAULT_PRICE_UNIT);
                harvestingPriceEntity.setPriceUnit(priceUnit);
            } else {
                harvestingPriceEntity.setPriceUnit(PricesService.DEFAULT_PRICE_UNIT);
            }
        }
        // the price value is not update from zone or crop but from domain, do not change it's value there

        persistPrice(harvestingPriceEntity);
    }
    
    @Override
    public void removeValorisationPrices(Collection<HarvestingActionValorisation> valorisations) {
        if (CollectionUtils.isNotEmpty(valorisations)) {
            List<HarvestingPrice> pricesToDelete = harvestingPriceDao.forHarvestingActionValorisationIn(valorisations).findAll();
            removeHarvestingPrices(pricesToDelete);
        }
    }

    @Override
    public HarvestingPrice getPriceForValorisation(HarvestingActionValorisation valorisation) {
        return harvestingPriceDao.forHarvestingActionValorisationEquals(valorisation).findUniqueOrNull();
    }
    
    @Override
    public void updateHarvestingPriceValues(List<HarvestingPriceDto> harvestingPriceDtos) {
        if (CollectionUtils.isNotEmpty(harvestingPriceDtos)) {
            
            final List<String> topiaIds = harvestingPriceDtos.stream().map(HarvestingPriceDto::getTopiaId).toList();
            final Map<String, HarvestingPrice> harvestingPriceByTopiaIds = harvestingPriceDao.forTopiaIdIn(topiaIds).findAll().stream().collect(
                    Collectors.toMap(HarvestingPrice::getTopiaId, Function.identity()));

            if (harvestingPriceByTopiaIds.isEmpty()) return;

            for (HarvestingPriceDto harvestingPriceDto : harvestingPriceDtos) {
                final HarvestingPrice persistedHarvestingPrice = harvestingPriceByTopiaIds.get(harvestingPriceDto.getTopiaId());
                if (persistedHarvestingPrice != null) {
                    persistedHarvestingPrice.setPrice(harvestingPriceDto.getPrice());
                    persistedHarvestingPrice.setPriceUnit(harvestingPriceDto.getPriceUnit());
                }
            }
            harvestingPriceDao.updateAll(harvestingPriceByTopiaIds.values());
        }
    }

    @Override
    public Map<String, HarvestingValorisationPriceSummary> getEffectiveAverageReferencePricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        // return prices by valorisation. Prices or ordered by increasing price values and units
        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> refHarvestingPrices = referentialService.getRefHarvestingPricesForValorisations(
                valorisations, codeEspBotCodeQualiBySpeciesCode);

        return getAverageReferencePricesForValorisations(valorisations, refHarvestingPrices);
    }

    @Override
    public Map<String, HarvestingValorisationPriceSummary> getPracticedAverageReferencePricesForValorisations(
            Collection<HarvestingActionValorisation> valorisations,
            String practicedSystemCampaigns,
            Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {

        // return prices by valorisation. Prices or ordered by increasing price values and units
        LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> refHarvestingPrices= referentialService.getRefHarvestingPricesForPracticedSystemValorisations(
                valorisations, practicedSystemCampaigns, codeEspBotCodeQualiBySpeciesCode);

        return getAverageReferencePricesForValorisations(valorisations, refHarvestingPrices);
    }

    protected LinkedHashMap<String, HarvestingValorisationPriceSummary> getAverageReferencePricesForValorisations(Collection<HarvestingActionValorisation> valorisations, LinkedHashMap<HarvestingActionValorisation, List<RefHarvestingPrice>> refHarvestingPrices) {
        LinkedHashMap<HarvestingActionValorisation, Map<Integer, List<RefHarvestingPrice>>> pricesByValorisationAndCampaigns
                = getPricesByValorisationAndCampaigns(refHarvestingPrices);

        LinkedHashMap<HarvestingActionValorisation, List<HarvestingValorisationPriceSummary>> averagePricesForValorisationsByCampaigns = computeAverageReferencesPricesForValorisations(
                pricesByValorisationAndCampaigns);

        LinkedHashMap<String, HarvestingValorisationPriceSummary> averageRefPricesByValorisationIds =
                computeAverageRefPricesForValorisations(
                        valorisations,
                        averagePricesForValorisationsByCampaigns);

        return averageRefPricesByValorisationIds;
    }

    @Override
    public Map<String, HarvestingPrice> getPriceForValorisations(Collection<HarvestingActionValorisation> harvestingActionValorisations) {
        Map<String, HarvestingPrice> harvestingPriceByValorisationIds = new HashMap<>();
        if (CollectionUtils.isNotEmpty(harvestingActionValorisations)) {
            List<HarvestingPrice> harvestingPrices = harvestingPriceDao.forHarvestingActionValorisationIn(harvestingActionValorisations).findAll();
            harvestingPrices.forEach(hp -> harvestingPriceByValorisationIds.put(hp.getHarvestingActionValorisation().getTopiaId(), hp));
        }
        return harvestingPriceByValorisationIds;
    }

    @Override
    public void removeHarvestingPrices(Collection<HarvestingPrice> harvestingPrices) {
        if (CollectionUtils.isNotEmpty(harvestingPrices)) {
            harvestingPriceDao.deleteAll(harvestingPrices);
        }
    }

    @Override
    public PriceAndUnit getAverageRefHarvestingPriceForValorisation(HarvestingActionValorisation valorisation,
                                                                    Optional<String> optionalPracticedSystemCampaigns,
                                                                    Optional<PriceUnit> optionalPriceUnit,
                                                                    Map<String, Set<Pair<String, String>>> codeEspBotCodeQualiBySpeciesCode) {
        validValorisation(valorisation);

        List<RefHarvestingPrice> pricesForValorisation;
        if (optionalPracticedSystemCampaigns.isPresent()) {
            pricesForValorisation = referentialService.getRefHarvestingPricesForPracticedSystemValorisations(
                    Collections.singletonList(valorisation),
                    optionalPracticedSystemCampaigns.get(),
                    codeEspBotCodeQualiBySpeciesCode
            ).get(valorisation);
        } else {
            pricesForValorisation = referentialService.getRefHarvestingPricesForValorisations(
                    Collections.singletonList(valorisation),
                    codeEspBotCodeQualiBySpeciesCode
            ).get(valorisation);
        }

        // on filtre sur l'unité recherchée, si elle est présente
        if (optionalPriceUnit.isPresent()) {

            boolean requestedPriceUnitFound = pricesForValorisation
                    .stream()
                    .filter(p -> p.getPrice() != null)
                    .anyMatch(p -> p.getPriceUnit().equals(optionalPriceUnit.get()));

            if (requestedPriceUnitFound) {
                pricesForValorisation = pricesForValorisation
                        .stream()
                        .filter(p -> p.getPrice() != null)
                        .filter(p -> p.getPriceUnit().equals(optionalPriceUnit.get()))
                        .toList();
            }
        }

        // on conserve uniquement les prix de même unité pour faire la moyenne
        Optional<PriceUnit> aPriceUnit = pricesForValorisation.stream()
                .filter(rp -> rp.getPrice() != null)
                .map(RefHarvestingPrice::getPriceUnit)
                .findAny();

        OptionalDouble average = OptionalDouble.empty();
        if (aPriceUnit.isPresent()) {
            average = pricesForValorisation.stream()
                    .filter(p -> p.getPrice() != null)
                    .filter(rp -> rp.getPriceUnit().equals(aPriceUnit.get()))
                    .mapToDouble(RefHarvestingPrice::getPrice)
                    .average();
        }

        return average.isEmpty() ? null : new PriceAndUnit(average.getAsDouble(), aPriceUnit.get());

    }
}
