package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefCountryModel extends AbstractAgrosystModel<RefCountry> implements ExportModel<RefCountry> {

    public RefCountryModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Trigramme", RefCountry.PROPERTY_TRIGRAM);
        newMandatoryColumn("Nom français", RefCountry.PROPERTY_FRENCH_NAME);
        newMandatoryColumn("Nom anglais", RefCountry.PROPERTY_ENGLISH_NAME);
        newMandatoryColumn("Nom natif", RefCountry.PROPERTY_NATIVE_NAME);
    }

    @Override
    public Iterable<ExportableColumn<RefCountry, Object>> getColumnsForExport() {
        ModelBuilder<RefCountry> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Trigramme", RefCountry.PROPERTY_TRIGRAM);
        modelBuilder.newColumnForExport("Nom français", RefCountry.PROPERTY_FRENCH_NAME);
        modelBuilder.newColumnForExport("Nom anglais", RefCountry.PROPERTY_ENGLISH_NAME);
        modelBuilder.newColumnForExport("Nom natif", RefCountry.PROPERTY_NATIVE_NAME);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefCountry newEmptyInstance() {
        RefCountry entity = new RefCountryImpl();
        entity.setActive(true);
        return entity;
    }
}
