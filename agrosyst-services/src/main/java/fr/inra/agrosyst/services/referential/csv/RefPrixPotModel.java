package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixPotImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixPotModel extends AbstractAgrosystModel<RefPrixPot> implements ExportModel<RefPrixPot> {

    public RefPrixPotModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Prix (€/pot)", RefPrixPot.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Code scénario", RefPrixPot.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixPot.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixPot.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newOptionalColumn("Source", RefPrixPot.PROPERTY_SOURCE);
        newMandatoryColumn("Caracteristique 1", RefPrixPot.PROPERTY_CARACTERISTIC1);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixPot.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixPot, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixPot> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Prix (€/pot)", RefPrixPot.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Code scénario", RefPrixPot.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixPot.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixPot.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixPot.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("Caracteristique 1", RefPrixPot.PROPERTY_CARACTERISTIC1);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixPot.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixPot newEmptyInstance() {
        RefPrixPot entity = new RefPrixPotImpl();
        entity.setActive(true);
        entity.setUnit(PriceUnit.EURO_UNITE);
        return entity;
    }
}
