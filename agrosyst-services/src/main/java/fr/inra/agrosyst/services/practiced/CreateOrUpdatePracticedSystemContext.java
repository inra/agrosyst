package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedPerennialCropCycleDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSeasonalCropCycleDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 08/04/16.
 */
@Getter
@Setter
public class CreateOrUpdatePracticedSystemContext {
    
    protected final Map<String, AbstractDomainInputStockUnit> domainInputStockByIds;
    private final Map<String, CroppingPlanEntry> croppingPlanEntryMap;
    private final Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap;

    // initial create or update practiced system attributes
    protected PracticedSystem practicedSystem;
    protected List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos;
    protected List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos;
    protected List<String> practicedSystemAvailableCropCodes;

    // related context object
    protected GrowingSystem growingSystem;
    protected Domain domain;

    // create or update PracticedPerennialCropCycle context
    protected PracticedPerennialCropCycle practicedPerennialCropCycle;
    protected List<PracticedPerennialCropCycle> practicedPerennialCropCycles;
    protected List<PracticedSeasonalCropCycle> practicedSeasonalCropCycles;

    // for Valorisation validation
    protected Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    protected Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant;

    CreateOrUpdatePracticedSystemContext(
            PracticedSystem practicedSystem,
            List<PracticedPerennialCropCycleDto> practicedPerennialCropCycleDtos,
            List<PracticedSeasonalCropCycleDto> practicedSeasonalCropCycleDtos,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockByIds,
            Map<String, CroppingPlanEntry> croppingPlanEntryMap,
            Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap) {
        this.practicedSystem = practicedSystem;
        this.practicedPerennialCropCycleDtos = practicedPerennialCropCycleDtos;
        this.practicedSeasonalCropCycleDtos = practicedSeasonalCropCycleDtos;
        growingSystem = practicedSystem.getGrowingSystem();
        domain = growingSystem.getGrowingPlan().getDomain();
        this.speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        this.sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant;
        this.domainInputStockByIds = domainInputStockByIds;
        this.croppingPlanEntryMap = croppingPlanEntryMap;
        this.croppingPlanSpeciesMap = croppingPlanSpeciesMap;
    }
    
    
    public void setPracticedSystem(PracticedSystem practicedSystem) {
        this.practicedSystem = practicedSystem;
        growingSystem = practicedSystem.getGrowingSystem();
        domain = growingSystem.getGrowingPlan().getDomain();
    }

}
