package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static org.nuiton.i18n.I18n.l;

/**
 * #12312
 * Identiques à la QSA totale
 * on ne prend en compte que les substances du référentiel "Substances Actives Commission Européenne" dont les valeurs de la colonne N "SA_faible_risque" prend la valeur "O"
 */
public class IndicatorTotalActiveSubstanceAmount extends AbstractIndicatorQSA {
    public static final String COLUMN_NAME = "qsa_tot";
    public static final String COLUMN_NAME_HTS = "qsa_tot_hts";

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.INPUT,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    protected boolean isWithSeedingTreatment = true;
    protected boolean isWithoutSeedingTreatment = true;

    private final String[] indicators = new String[]{
            "Indicator.label.totalActiveSubstances",        // Quantité totale de substances actives phyto
            "Indicator.label.totalActiveSubstances_hts",    // Quantité totale de substances actives phyto hors traitement de semence
    };

    public IndicatorTotalActiveSubstanceAmount() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {

        if (interventionContext.isFictive()) {
            return newArray(getIndicators().length, 0.0d);
        }

        // just to increment field counter
        double toolPSCi = getToolPSCi(interventionContext.getIntervention());

        final Optional<Pair<Double, Double>> optionalTotalQsa = computeTotalQsa(
                writerContext,
                toolPSCi,
                interventionContext,
                domainContext.getAllDomainSubstancesByAmm());

        return optionalTotalQsa.map(doubleDoublePair -> new Double[]{doubleDoublePair.getLeft(), doubleDoublePair.getRight()}).orElse(null);
    }

    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveCropExecutionContext cropContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        // just to increment field counter
        double toolPSCi = getToolPSCi(interventionContext.getIntervention());

        final Optional<Pair<Double, Double>> optionalTotalQsa = computeTotalQsa(
                writerContext,
                toolPSCi,
                interventionContext,
                domainContext.getAllDomainSubstancesByAmm());

        return optionalTotalQsa.map(doubleDoublePair -> new Double[]{doubleDoublePair.getLeft(), doubleDoublePair.getRight()}).orElse(null);
    }

    private Optional<Pair<Double, Double>> computeTotalQsa(
            WriterContext writerContext,
            double psci,
            PerformanceInterventionContext interventionContext,
            Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm) {

        AtomicBoolean computeDoneFlag = new AtomicBoolean(false);

        AtomicReference<Double> result0 = new AtomicReference<>(0.0);

        interventionContext.getOptionalPesticidesSpreadingAction().ifPresent(abstractAction -> {
            final double treatedSurface = abstractAction.getProportionOfTreatedSurface() / 100;
            final double phytoPsci = psci * treatedSurface;

            Collection<? extends AbstractPhytoProductInputUsage> pesticideProductInputUsages = abstractAction.getPesticideProductInputUsages();
            if (pesticideProductInputUsages != null) {
                pesticideProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(
                                usage -> {
                                    RefActaTraitementsProduit refInput = usage.getDomainPhytoProductInput().getRefInput();
                                    ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                                    final double inputAmount = this.computeAmountUsedInKgHa(abstractAction, usage, phytoPsci, referenceDose);

                                    final String codeAmm = refInput.getCode_AMM();

                                    final List<RefCompositionSubstancesActivesParNumeroAMM> activeSubstances = domainSubstancesByAmm.getOrDefault(codeAmm, List.of());
                                    double sum = computeAndWriteUsage(
                                            writerContext,
                                            interventionContext,
                                            abstractAction,
                                            usage,
                                            activeSubstances,
                                            inputAmount,
                                            false);
                                    result0.updateAndGet(v -> (v + sum));
                                    computeDoneFlag.set(true);
                                }
                        );
            }
        });

        interventionContext.getOptionalBiologicalControlAction().ifPresent(abstractAction -> {
            // Application du psci phyto pour les actions d'application de produit phytosanitaire
            final double treatedSurface = abstractAction.getProportionOfTreatedSurface() / 100;
            final double phytoPsci = psci * treatedSurface;

            // Parmi les produits sans AMM et macro-organismes, certains ont un AMM (qui peut être un vrai AMM
            // ou un numéro de dossier quelconque) que l'on peut retrouver dans refactatraitementsproduit,
            // donc pour ces produits on peut trouver des quantités de substances actives
            Collection<? extends AbstractPhytoProductInputUsage> biologicalProductInputUsages = abstractAction.getBiologicalProductInputUsages();
            if (biologicalProductInputUsages != null) {
                biologicalProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .forEach(
                                usage -> {
                                    ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                                    final double inputAmount = this.computeAmountUsedInKgHa(abstractAction, usage, phytoPsci, referenceDose);

                                    final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();

                                    final List<RefCompositionSubstancesActivesParNumeroAMM> activeSubstances = domainSubstancesByAmm.getOrDefault(codeAmm, List.of());
                                    double sum = computeAndWriteUsage(
                                            writerContext,
                                            interventionContext,
                                            abstractAction,
                                            usage,
                                            activeSubstances,
                                            inputAmount,
                                            false);
                                    result0.updateAndGet(v -> (v + sum));

                                    computeDoneFlag.set(true);
                                }
                        );
            }
        });

        Double tsaWithoutTS = result0.get();

        interventionContext.getOptionalSeedingActionUsage().ifPresent(abstractAction -> abstractAction.getSeedLotInputUsage()
                .stream()
                .filter(u -> u != null && u.getSeedingSpecies() != null)
                .flatMap(u -> u.getSeedingSpecies().stream())
                .filter(u -> u.getSeedProductInputUsages() != null)
                .flatMap(u -> u.getSeedProductInputUsages().stream())
                .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                .forEach(
                        usage -> {
                            ReferenceDoseDTO referenceDose = getReferenceDoseDTO(usage, interventionContext);

                            final double inputAmount = this.computeAmountUsedInKgHa(abstractAction, usage, psci, referenceDose);

                            final String codeAmm = usage.getDomainPhytoProductInput().getRefInput().getCode_AMM();

                            final List<RefCompositionSubstancesActivesParNumeroAMM> activeSubstances = domainSubstancesByAmm.getOrDefault(codeAmm, List.of());
                            double sum = computeAndWriteUsage(
                                    writerContext,
                                    interventionContext,
                                    abstractAction,
                                    usage,
                                    activeSubstances,
                                    inputAmount,
                                    true);
                            result0.updateAndGet(v -> (v + sum));

                            computeDoneFlag.set(true);
                        }
                ));

        if (computeDoneFlag.get()) {
            Double tsaWithTS = result0.get();

            return Optional.of(Pair.of(tsaWithTS, tsaWithoutTS));
        }
        return Optional.empty();
    }

    protected double computeAndWriteUsage(
            WriterContext writerContext,
            PerformanceInterventionContext interventionContext,
            AbstractAction abstractAction,
            AbstractPhytoProductInputUsage usage,
            List<RefCompositionSubstancesActivesParNumeroAMM> activeSubstances,
            double inputAmount,
            boolean seedingTreatment) {

        double sum = getSum(interventionContext, activeSubstances, inputAmount);

        writeInput(writerContext,
                abstractAction,
                usage,
                seedingTreatment,
                sum);

        return sum;
    }

    protected double getSum(PerformanceInterventionContext interventionContext, List<RefCompositionSubstancesActivesParNumeroAMM> activeSubstances, double inputAmount) {
        double sum = CollectionUtils.emptyIfNull(activeSubstances)
                .stream()
                .mapToDouble(activeSubstance -> {
                    final double coefActiveSubstanceAmountToKgHa = this.getCoef(activeSubstance, interventionContext);
                    double result = activeSubstance.getDose_sa() * inputAmount * coefActiveSubstanceAmountToKgHa;
                    return result;
                }).sum();
        return sum;
    }

    protected void writeInput(
            WriterContext writerContext,
            AbstractAction abstractAction,
            final AbstractPhytoProductInputUsage usage,
            final boolean seedingTreatment,
            final double result_) {

        IndicatorWriter writer = writerContext.getWriter();

        for (int i = 0; i < getIndicators().length; i++) {
            String indicatorLabel = getIndicatorLabel(i);
            // si traitement de semence && indicateur avec traitement de semence alors on comptabilise
            double result = !seedingTreatment || i == 0 ? result_ : 0;

            boolean isDisplayed = isDisplayed(ExportLevel.INPUT, i);

            if (isDisplayed) {

                // dose
                String usageTopiaId = usage.getTopiaId();
                incrementAngGetTotalFieldCounterForTargetedId(usageTopiaId);

                if (usage.getQtAvg() == null) {
                    addMissingFieldMessage(
                            usageTopiaId,
                            messageBuilder.getMissingDoseMessage(
                                    usage.getInputType(),
                                    MissingMessageScope.INPUT));
                }

                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);
                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                        usageTopiaId,
                        MissingMessageScope.INPUT);

                // write input sheet

                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        getIndicatorCategory(),
                        indicatorLabel,
                        usage,
                        abstractAction,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        this.getClass(),
                        result,
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        "",
                        "",
                        "",
                        writerContext.getGroupesCiblesByCode());
            }
        }

    }

    protected double getCoef(@NotNull RefCompositionSubstancesActivesParNumeroAMM activeSubstance, PerformanceInterventionContext performanceInterventionContext) {
        return this.conversionRatesByUnit.getOrDefault(activeSubstance.getUnite_sa(), 0.0);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;
    }

    protected String[] getIndicators() {
        return indicators;
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, getIndicators()[i]);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed && (
                i == 0 && isWithSeedingTreatment ||
                        i == 1 && isWithoutSeedingTreatment
        );
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.activeSubstances");
    }

    public void init(IndicatorFilter filter, Map<String, Double> coeffsConversionVersKgHa) {
        displayed = filter != null;
        if (displayed) {
            isWithSeedingTreatment = filter.getWithSeedingTreatment();
            isWithoutSeedingTreatment = filter.getWithoutSeedingTreatment();
        }
        this.conversionRatesByUnit = coeffsConversionVersKgHa;
    }

}
