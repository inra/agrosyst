package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitementImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class RefGroupeCibleTraitementModel extends AbstractAgrosystModel<RefGroupeCibleTraitement> implements ExportModel<RefGroupeCibleTraitement> {

    public RefGroupeCibleTraitementModel() {
        super(CSV_SEPARATOR);
        // code_amm;code_culture_maa;culture_maa;traitement_maa;code_groupe_cible_maa;groupe_cible_maa;campagne;dose_ref_maa;unit_dose_ref_maa;active;code_traitement_maa;correspondance_TraitementAgrosyst_TraitementMAA
        newMandatoryColumn("code_traitement_maa", RefGroupeCibleTraitement.PROPERTY_CODE_TRAITEMENT_MAA);
        newMandatoryColumn("traitement_maa", RefGroupeCibleTraitement.PROPERTY_TRAITEMENT_MAA, STRING_MANDATORY_PARSER);
        newMandatoryColumn("code_groupe_cible_maa", RefGroupeCibleTraitement.PROPERTY_CODE_GROUPE_CIBLE_MAA, STRING_WITH_NULL_PARSER);
        newMandatoryColumn("groupe_cible_maa", RefGroupeCibleTraitement.PROPERTY_GROUPE_CIBLE_MAA);
        newMandatoryColumn("id_traitement", RefGroupeCibleTraitement.PROPERTY_ID_TRAITEMENT, INT_MANDATORY_PARSER);
        newMandatoryColumn("code_traitement", RefGroupeCibleTraitement.PROPERTY_CODE_TRAITEMENT, STRING_MANDATORY_PARSER);
        newMandatoryColumn("nom_traitement", RefGroupeCibleTraitement.PROPERTY_NOM_TRAITEMENT, STRING_MANDATORY_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefGroupeCibleTraitement.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefGroupeCibleTraitement, Object>> getColumnsForExport() {
        ModelBuilder<RefGroupeCibleTraitement> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_traitement_maa", RefGroupeCibleTraitement.PROPERTY_CODE_TRAITEMENT_MAA);
        modelBuilder.newColumnForExport("traitement_maa", RefGroupeCibleTraitement.PROPERTY_TRAITEMENT_MAA);
        modelBuilder.newColumnForExport("code_groupe_cible_maa", RefGroupeCibleTraitement.PROPERTY_CODE_GROUPE_CIBLE_MAA);
        modelBuilder.newColumnForExport("groupe_cible_maa", RefGroupeCibleTraitement.PROPERTY_GROUPE_CIBLE_MAA);
        modelBuilder.newColumnForExport("id_traitement", RefGroupeCibleTraitement.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("code_traitement", RefGroupeCibleTraitement.PROPERTY_CODE_TRAITEMENT);
        modelBuilder.newColumnForExport("nom_traitement", RefGroupeCibleTraitement.PROPERTY_NOM_TRAITEMENT);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefGroupeCibleTraitement.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefGroupeCibleTraitement newEmptyInstance() {
        RefGroupeCibleTraitement refGroupeCibleTraitement = new RefGroupeCibleTraitementImpl();
        refGroupeCibleTraitement.setActive(true);
        return refGroupeCibleTraitement;
    }
}
