package fr.inra.agrosyst.services.domain.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingEntryType;
import fr.inra.agrosyst.api.entities.DomainType;
import fr.inra.agrosyst.api.entities.DoseType;
import fr.inra.agrosyst.api.entities.EstimatingIftRules;
import fr.inra.agrosyst.api.entities.IftSeedsType;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.Zoning;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalTypeTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAlimentTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatusTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOTEXTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.export.ExportModel;
import lombok.Getter;
import lombok.Setter;

public class DomainExportBeansAndModels {

    @Getter
    public static class CommonBean {

        final DomainType domainType;
        final String department;
        final String postalCode;
        final String domainName;
        final int campaign;
        final String mainContact;

        /**
         * Constructeur avec tous les champs pour la première instanciation
         */
        public CommonBean(DomainType domainType,
                          String department,
                          String postalCode,
                          String domainName,
                          int campaign,
                          String mainContact) {
            this.domainType = domainType;
            this.department = department;
            this.postalCode = postalCode;
            this.domainName = domainName;
            this.campaign = campaign;
            this.mainContact = mainContact;
        }

        /**
         * Constructeur par recopie pour faciliter le travail des sous modèles
         */
        public CommonBean(CommonBean source) {
            this(
                    source.domainType,
                    source.department,
                    source.postalCode,
                    source.domainName,
                    source.campaign,
                    source.mainContact);
        }
    }

    public abstract static class CommonModel<T extends CommonBean> extends ExportModel<T> {
        public CommonModel() {
            newColumn("Type de domaine", DomainType.class, CommonBean::getDomainType);
            newColumn("Département", CommonBean::getDepartment);
            newColumn("Code Postal", CommonBean::getPostalCode);
            newColumn("Nom du domaine", CommonBean::getDomainName);
            newColumn("Campagne", CommonBean::getCampaign);
            newColumn("Nom de l'interlocuteur principal", CommonBean::getMainContact);
        }
    }

    /**
     * Current bean info.
     */
    @Getter
    @Setter
    public static class DomainBean extends CommonBean {

        String petiteRegionAgricole;
        Zoning zoning;
        Double uaaVulnarablePart;
        Double uaaStructuralSurplusAreaPart;
        Double uaaActionPart;
        Double uaaNatura2000Part;
        Double uaaErosionRegionPart;
        Double uaaWaterResourceProtectionPart;
        String stakesTourist;

        public DomainBean(CommonBean source) {
            super(source);
        }
    }

    public static class DomainModel extends CommonModel<DomainBean> {
        @Override
        public String getTitle() {
            return "Généralités";
        }

        public DomainModel() {
            super();
            newColumn("Petite région agricole", DomainBean::getPetiteRegionAgricole);
            newColumn("Zonage du domaine", Zoning.class, DomainBean::getZoning);
            newColumn("Part de la SAU en Zone vulnérable", DomainBean::getUaaVulnarablePart);
            newColumn("Part de la SAU en Zone d’excédent structurel", DomainBean::getUaaStructuralSurplusAreaPart);
            newColumn("Part de la SAU en Zone d’actions complémentaires", DomainBean::getUaaActionPart);
            newColumn("Part de la SAU en Zone Natura 2000", DomainBean::getUaaNatura2000Part);
            newColumn("Part de la SAU en Zone d’érosion (arrêté préfectoral)", DomainBean::getUaaErosionRegionPart);
            newColumn("Part de la SAU en Périmètre de protection de captage", DomainBean::getUaaWaterResourceProtectionPart);
            newColumn("Touristique / Autres ", DomainBean::getStakesTourist);
        }
    }

    @Getter
    @Setter
    public static class GpsBean extends CommonBean {

        String geoPointName;
        Double geoPointLatitude;
        Double geoPointLongitude;
        String geoPointDescription;

        public GpsBean(CommonBean source) {
            super(source);
        }

    }

    public static class GpsModel extends CommonModel<GpsBean> {
        @Override
        public String getTitle() {
            return "Données GPS";
        }

        public GpsModel() {
            super();
            newColumn("Nom du centre", GpsBean::getGeoPointName);
            newColumn("Latitude", GpsBean::getGeoPointLatitude);
            newColumn("Longitude", GpsBean::getGeoPointLongitude);
            newColumn("Description", GpsBean::getGeoPointDescription);
        }
    }

    @Getter
    @Setter
    public static class LivestockUnitBean extends CommonBean {

        String ref_animal_type;
        Integer livestock_unit_size;
        Double permanent_grassland_area;
        Double temporary_grassland_area;
        Double permanent_mowed_grassland_area;
        Double temporary_mowed_grassland_area;
        Double forage_crops_area;
        Double producing_food_area;
        Double holding_straw_area;
        Double mass_self_sustaining_percent;
        Double average_straw_for_bedding;
        Double average_manure_produced;
        Double average_liquid_effluent;
        Double average_composted_effluent;
        Double average_methanised_effluent;
        String comment;
        String animal_type;
        Integer number_of_heads;
        Integer starting_half_month;
        Integer ending_half_month;
        String aliment_type;
        Double quantity;
        String aliment_unit;

        public LivestockUnitBean(CommonBean source) {
            super(source);
        }

    }

    public static class LivestockUnitModel extends CommonModel<LivestockUnitBean> {
        @Override
        public String getTitle() {
            return "Ateliers d’élevage";
        }

        public LivestockUnitModel(
                RefAnimalTypeTopiaDao refAnimalTypeTopiaDao,
                RefCattleAnimalTypeTopiaDao refCattleAnimalTypeTopiaDao,
                RefCattleRationAlimentTopiaDao refCattleRationAlimentTopiaDao) {
            super();
            Iterable<String> refAnimalTypeValues = refAnimalTypeTopiaDao.findPropertyDistinctValues(RefAnimalType.PROPERTY_ANIMAL_TYPE);
            newColumn("Type d’atelier", refAnimalTypeValues, LivestockUnitBean::getRef_animal_type);
            newColumn("Taille de l’atelier (approx.)", LivestockUnitBean::getLivestock_unit_size);
            newColumn("Surface de prairie permanente pâturée par les animaux de l’atelier", LivestockUnitBean::getPermanent_grassland_area);
            newColumn("Surface de prairie temporaire pâturée par les animaux de l’atelier", LivestockUnitBean::getTemporary_grassland_area);
            newColumn("Surface de prairie permanente fauchée entrant dans la ration de l’atelier", LivestockUnitBean::getPermanent_mowed_grassland_area);
            newColumn("Surface de prairie temporaire fauchée entrant dans la ration de l’atelier", LivestockUnitBean::getTemporary_mowed_grassland_area);
            newColumn("Surface de cultures fourragères annuelles de l’exploitation entrant dans la ration de l’atelier", LivestockUnitBean::getForage_crops_area);
            newColumn("Surface de l’exploitation produisant des aliments concentrés entrant dans la ration de l’atelier", LivestockUnitBean::getProducing_food_area);
            newColumn("Surface de l’exploitation produisant la paille valorisée par l’atelier", LivestockUnitBean::getHolding_straw_area);
            newColumn("% d’autonomie massique de la ration de l’atelier", LivestockUnitBean::getMass_self_sustaining_percent);
            newColumn("Quantité moyenne de paille utilisée en litière", LivestockUnitBean::getAverage_straw_for_bedding);
            newColumn("Quantité moyenne de fumier produit", LivestockUnitBean::getAverage_manure_produced);
            newColumn("Quantité moyenne d'effluent liquide", LivestockUnitBean::getAverage_liquid_effluent);
            newColumn("Quantité moyenne d'effluent composté", LivestockUnitBean::getAverage_composted_effluent);
            newColumn("Quantité moyenne d'effluent méthanisé", LivestockUnitBean::getAverage_methanised_effluent);
            newColumn("Commentaire", LivestockUnitBean::getComment);
            Iterable<String> animalTypeValues = refCattleAnimalTypeTopiaDao.findPropertyDistinctValues(RefCattleAnimalType.PROPERTY_ANIMAL_TYPE);
            newColumn("Type d'animaux", animalTypeValues, LivestockUnitBean::getAnimal_type);
            newColumn("Nombre de têtes", LivestockUnitBean::getNumber_of_heads);
            newColumn("Quinzaine de début", LivestockUnitBean::getStarting_half_month);
            newColumn("Quinzaine de fin", LivestockUnitBean::getEnding_half_month);
            Iterable<String> alimentTypeValues = refCattleRationAlimentTopiaDao.findPropertyDistinctValues(RefCattleRationAliment.PROPERTY_ALIMENT_TYPE);
            newColumn("Type d'aliment", alimentTypeValues, LivestockUnitBean::getAliment_type);
            newColumn("Quantité", LivestockUnitBean::getQuantity);
            newColumn("Unité", LivestockUnitBean::getAliment_unit);
        }
    }

    @Getter
    @Setter
    public static class GroundBean extends CommonBean {

        String region;
        String solArvalis;
        String localName;
        Double importance;
        String comment;

        public GroundBean(CommonBean source) {
            super(source);
        }

    }

    public static class GroundModel extends CommonModel<GroundBean> {
        @Override
        public String getTitle() {
            return "Sols";
        }

        public GroundModel(RefSolArvalisTopiaDao refSolArvalisTopiaDao) {
            super();
            Iterable<String> regionValues = refSolArvalisTopiaDao.findPropertyDistinctValues(RefSolArvalis.PROPERTY_SOL_REGION);
            newColumn("Region", regionValues, GroundBean::getRegion);
            Iterable<String> solArvalisValues = refSolArvalisTopiaDao.findPropertyDistinctValues(RefSolArvalis.PROPERTY_SOL_NOM);
            newColumn("Type de sol Arvalis", solArvalisValues, GroundBean::getSolArvalis);
            newColumn("Nom local", GroundBean::getLocalName);
            newColumn("Part de la SAU du domaine concernée", GroundBean::getImportance);
            newColumn("Commentaire", GroundBean::getComment);
        }
    }

    @Getter
    @Setter
    public static class StatusBean extends CommonBean {

        String legal_status;
        String status_comment;
        String siret;
        Integer chief_birth_year;
        String otex18;
        String otex70;
        String orientation;
        Integer partners_number;
        String objectives;
        String domain_assets;
        String domain_constraints;
        String description;
        String domain_likely_trends;
        Boolean cooperative_member;
        Boolean development_group_member;
        Boolean cuma_member;
        Double other_work_force;
        Double permanent_employees_work_force;
        Double temporary_employees_work_force;
        Double operator_work_force;
        Double non_seasonal_work_force;
        Double seasonal_work_force;
        Double volunteer_work_force;
        String workforce_comment;
        Double used_agricultural_area;
        Double used_agricultural_area_for_farming;
        Double irrigable_area;
        Double fallow_area;
        Double annual_crop_area;
        Double vineyard_and_orchard_area;
        Double meadow_area;
        Double meadow_always_with_grass_area;
        Double meadow_other_area;
        Double meadow_only_pastured_area;
        Double meadow_only_mowed_area;
        Double meadow_pastured_and_mowed_area;
        Double heathland_and_routes_area;
        Double summer_and_mountain_pasture_area;
        Double collective_heathland_and_routes_area;
        Double collective_summer_and_mountain_pasture_area;
        Double total_other_areas;
        Integer family_work_force_wage;
        Integer wage_costs;
        Double msa_fee;
        Double average_tenant_farming;
        Double decoupled_assistance;

        Double getTotalWorkForce() {
            double sum = 0;
            if (getOther_work_force() != null) {
                sum += getOther_work_force();
            }
            if (getPermanent_employees_work_force() != null) {
                sum += getPermanent_employees_work_force();
            }
            if (getTemporary_employees_work_force() != null) {
                sum += getTemporary_employees_work_force();
            }
            return sum;
        }

        Double getSauPerTotalWorkForce() {
            double res = 0;
            Double workForce = getTotalWorkForce();
            if (getUsed_agricultural_area() != null && workForce != 0) {
                res = getUsed_agricultural_area() / workForce;
            }
            return res;
        }

        public StatusBean(CommonBean source) {
            super(source);
        }

    }

    public static class StatusModel extends CommonModel<StatusBean> {
        @Override
        public String getTitle() {
            return "Statut et Main d'Œuvre";
        }

        public StatusModel(RefOTEXTopiaDao refOTEXTopiaDao, RefLegalStatusTopiaDao refLegalStatusTopiaDao) {
            super();

            // Caractéristiques
            Iterable<String> legalStatusValues = refLegalStatusTopiaDao.findPropertyDistinctValues(RefLegalStatus.PROPERTY_LIBELLE__INSEE);
            newColumn("Statut juridique ou institutionnel", legalStatusValues, StatusBean::getLegal_status);
            newColumn("Commentaire sur le statut", StatusBean::getStatus_comment);
            newColumn("Numéro SIRET", StatusBean::getSiret);
            newColumn("Année de naissance du chef d'exploitation", StatusBean::getChief_birth_year);
            Iterable<String> otex18Values = refOTEXTopiaDao.findPropertyDistinctValues(RefOTEX.PROPERTY_LIBELLE__OTEX_18_POSTES);
            newColumn("Orientation technico-économique de l'exploitation agricole OTEX", otex18Values, StatusBean::getOtex18);
            Iterable<String> otex70Values = refOTEXTopiaDao.findPropertyDistinctValues(RefOTEX.PROPERTY_LIBELLE__OTEX_70_POSTES);
            newColumn("Précision OTEX", otex70Values, StatusBean::getOtex70);
            newColumn("Description de l'orientation", StatusBean::getOrientation);
            newColumn("Nombre d'associés", StatusBean::getPartners_number);
            newColumn("Objectifs (agriculteur, chef de culture)", StatusBean::getObjectives);
            newColumn("Atouts de l’exploitation", StatusBean::getDomain_assets);
            newColumn("Contraintes de l’exploitation", StatusBean::getDomain_constraints);
            newColumn("Description", StatusBean::getDescription);
            newColumn("Perspectives d’évolution", StatusBean::getDomain_likely_trends);
            newColumn("Adhérent coopérative", StatusBean::getCooperative_member);
            newColumn("Adhérent Groupe de développement", StatusBean::getDevelopment_group_member);
            newColumn("Adhérent CUMA", StatusBean::getCuma_member);
            // Main d'œuvre
            newColumn("Main d'œuvre familiale ou main d'œuvre des associés", StatusBean::getOther_work_force);
            newColumn("Main d'œuvre salariée permanente", StatusBean::getPermanent_employees_work_force);
            newColumn("Main d'œuvre salariée temporaire", StatusBean::getTemporary_employees_work_force);
            newColumn("Main d'œuvre totale", StatusBean::getTotalWorkForce);
            newColumn("Main d'œuvre exploitant", StatusBean::getOperator_work_force);
            newColumn("Main d'œuvre salariée non saisonnière", StatusBean::getNon_seasonal_work_force);
            newColumn("Main d'œuvre salariée saisonnière", StatusBean::getSeasonal_work_force);
            newColumn("Main d'œuvre bénévole", StatusBean::getVolunteer_work_force);
            newColumn("Commentaire main d'œuvre", StatusBean::getWorkforce_comment);
            // SAU
            newColumn("Surface agricole utilisée (SAU) totale", StatusBean::getUsed_agricultural_area);
            newColumn("Dont surface en fermage", StatusBean::getUsed_agricultural_area_for_farming);
            newColumn("Surface irrigable", StatusBean::getIrrigable_area);
            newColumn("Surface en jachère", StatusBean::getFallow_area);
            newColumn("Surface en cultures annuelles", StatusBean::getAnnual_crop_area);
            newColumn("Surface de vignes et vergers", StatusBean::getVineyard_and_orchard_area);
            newColumn("Surface en prairie", StatusBean::getMeadow_area);
            newColumn("Dont surface toujours en herbe", StatusBean::getMeadow_always_with_grass_area);
            newColumn("Dont surface autres prairies", StatusBean::getMeadow_other_area);
            newColumn("Surface prairies exclusivement pâturées", StatusBean::getMeadow_only_pastured_area);
            newColumn("Surface prairies exclusivement fauchées", StatusBean::getMeadow_only_mowed_area);
            newColumn("Surface prairies mixtes", StatusBean::getMeadow_pastured_and_mowed_area);
            newColumn("Surface landes & parcours", StatusBean::getHeathland_and_routes_area);
            newColumn("Surface estives & alpages", StatusBean::getSummer_and_mountain_pasture_area);
            newColumn("Surface landes & parcours collectifs", StatusBean::getCollective_heathland_and_routes_area);
            newColumn("Surface estives & alpages collectifs", StatusBean::getCollective_summer_and_mountain_pasture_area);
            newColumn("Total Autres surfaces", StatusBean::getTotal_other_areas);
            newColumn("Surface agricole utilisée (SAU) totale / Main d'œuvre totale", StatusBean::getSauPerTotalWorkForce);
            // Cotisations, fermage, aides découplées
            newColumn("Rémunération de la main d'œuvre familiale", StatusBean::getFamily_work_force_wage);
            newColumn("Charges salariales", StatusBean::getWage_costs);
            newColumn("Cotisations MSA", StatusBean::getMsa_fee);
            newColumn("Fermage moyen", StatusBean::getAverage_tenant_farming);
            newColumn("Aides découplées", StatusBean::getDecoupled_assistance);
        }
    }

    @Getter
    @Setter
    public static class EquipmentBean extends CommonBean {

        RefMateriel refMateriel;
        String name;
        String description;
        boolean materielEta;
        boolean homemadeMaterial;
        boolean jerryRigged;
        boolean weakenedMaterial;

        public EquipmentBean(CommonBean source) {
            super(source);
        }
    }

    public static class EquipmentModel extends CommonModel<EquipmentBean> {
        @Override
        public String getTitle() {
            return "Matériels";
        }

        public EquipmentModel(RefMaterielTopiaDao refMaterielTopiaDao) {
            super();

            Iterable<String> type1Values = refMaterielTopiaDao.findPropertyDistinctValues(RefMateriel.PROPERTY_TYPE_MATERIEL1);
            newColumn("Matériel", type1Values, EquipmentBean::getRefMateriel, RefMateriel::getTypeMateriel1);
            Iterable<String> type2Values = refMaterielTopiaDao.findPropertyDistinctValues(RefMateriel.PROPERTY_TYPE_MATERIEL2);
            newColumn("Caractéristique 1", type2Values, EquipmentBean::getRefMateriel, RefMateriel::getTypeMateriel2);
            Iterable<String> type3Values = refMaterielTopiaDao.findPropertyDistinctValues(RefMateriel.PROPERTY_TYPE_MATERIEL3);
            newColumn("Caractéristique 2", type3Values, EquipmentBean::getRefMateriel, RefMateriel::getTypeMateriel3);
            Iterable<String> type4Values = refMaterielTopiaDao.findPropertyDistinctValues(RefMateriel.PROPERTY_TYPE_MATERIEL4);
            newColumn("Caractéristique 3", type4Values, EquipmentBean::getRefMateriel, RefMateriel::getTypeMateriel4);
            Iterable<String> uniteValues = refMaterielTopiaDao.findPropertyDistinctValues(RefMateriel.PROPERTY_UNITE);
            newColumn("Utilisation annuelle", uniteValues, EquipmentBean::getRefMateriel, RefMateriel::getUnite);
            newColumn("Unité par an", EquipmentBean::getRefMateriel, RefMateriel::getUniteParAn);
            newColumn("Nom", EquipmentBean::getName);
            newColumn("Autres caractéristiques", EquipmentBean::getDescription);
            newColumn("Matériel ETA / CUMA", EquipmentBean::isMaterielEta);
            newColumn("Matériel construit au domaine", EquipmentBean::isHomemadeMaterial);
            newColumn("Matériel bricolé au domaine", EquipmentBean::isJerryRigged);
            newColumn("Matériel amorti", EquipmentBean::isWeakenedMaterial);
        }
    }

    @Getter
    @Setter
    public static class ToolsCouplingBean extends CommonBean {

        String mainsActions;
        String tractor;
        String equipments;
        String tools_coupling_name;
        Double work_rate;
        MaterielWorkRateUnit work_rate_unit;
        Double workforce;
        String comment;
        AgrosystInterventionType agrosystInterventionTypes;

        public ToolsCouplingBean(CommonBean source) {
            super(source);
        }
    }

    public static class ToolsCouplingModel extends CommonModel<ToolsCouplingBean> {
        @Override
        public String getTitle() {
            return "Combinaison d'outils";
        }

        public ToolsCouplingModel() {
            super();
            newColumn("Actions principales", ToolsCouplingBean::getMainsActions);
            newColumn("Matériel de Traction ou Automoteur", ToolsCouplingBean::getTractor);
            newColumn("Outils ou matériel d'irrigation", ToolsCouplingBean::getEquipments);
            newColumn("Nom", ToolsCouplingBean::getTools_coupling_name);
            newColumn("Débit de chantier", ToolsCouplingBean::getWork_rate);
            newColumn("Unité de débit de chantier", ToolsCouplingBean::getWork_rate_unit);
            newColumn("Nombre de personnes mobilisées", ToolsCouplingBean::getWorkforce);
            newColumn("Commentaire", ToolsCouplingBean::getComment);
            newColumn("Type d'action", ToolsCouplingBean::getAgrosystInterventionTypes);

        }
    }

    @Getter
    @Setter
    public static class CroppingBean extends CommonBean {

        String name;
        CroppingEntryType type;
        Boolean temporary_meadow;
        boolean pastured_meadow;
        boolean mowed_meadow;
        String libelle_espece_botanique;
        String libelle_qualifiant__aee;
        String libelle_type_saisonnier__aee;
        String libelle_destination__aee;
        String label;
        Double yeald_average;
        YealdUnit yeald_unit;
        Double average_ift;
        Double biocontrol_ift;
        EstimatingIftRules estimating_ift_rules;
        IftSeedsType ift_seeds_type;
        DoseType dose_type;
        Double selling_price;

        public CroppingBean(CommonBean source) {
            super(source);
        }
    }

    public static class CroppingModel extends CommonModel<CroppingBean> {
        @Override
        public String getTitle() {
            return "Assolements";
        }

        public CroppingModel(RefEspeceTopiaDao refEspeceTopiaDao) {
            super();
            newColumn("Culture", CroppingBean::getName);
            newColumn("Type", CroppingBean::getType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            newColumn("Prairie temporaire", CroppingBean::getTemporary_meadow);
            newColumn("Prairie pâturée", CroppingBean::isPastured_meadow);
            newColumn("Prairie fauchée", CroppingBean::isMowed_meadow);
            Iterable<String> espaceValues = refEspeceTopiaDao.findPropertyDistinctValues(RefEspece.PROPERTY_LIBELLE_ESPECE_BOTANIQUE);
            newColumn("Espèce", espaceValues, CroppingBean::getLibelle_espece_botanique);
            Iterable<String> qualifiantAeeValues = refEspeceTopiaDao.findPropertyDistinctValues(RefEspece.PROPERTY_LIBELLE_QUALIFIANT__AEE);
            newColumn("Qualifiant", qualifiantAeeValues, CroppingBean::getLibelle_qualifiant__aee);
            Iterable<String> typeSaisonnierValues = refEspeceTopiaDao.findPropertyDistinctValues(RefEspece.PROPERTY_LIBELLE_TYPE_SAISONNIER__AEE);
            newColumn("Type saisonnier", typeSaisonnierValues, CroppingBean::getLibelle_type_saisonnier__aee);
            Iterable<String> destinationValues = refEspeceTopiaDao.findPropertyDistinctValues(RefEspece.PROPERTY_LIBELLE_DESTINATION__AEE);
            newColumn("Destination", destinationValues, CroppingBean::getLibelle_destination__aee);
            newColumn("Cépage/Variété", CroppingBean::getLabel);
            newColumn("Rendement moyen", CroppingBean::getYeald_average);
            newColumn("Unité rendement moyen", YealdUnit.class, CroppingBean::getYeald_unit);
            newColumn("IFT moyen (hors biocontrôle) ", CroppingBean::getAverage_ift);
            newColumn("IFT biocontrôle", CroppingBean::getBiocontrol_ift);
            newColumn("Modalités d’estimation de l’IFT", EstimatingIftRules.class, CroppingBean::getEstimating_ift_rules);
            newColumn("Type d’IFT", IftSeedsType.class, CroppingBean::getIft_seeds_type);
            newColumn("Type de doses de référence", DoseType.class, CroppingBean::getDose_type);
            newColumn("Prix de vente (€)", CroppingBean::getSelling_price);
        }
    }

    @Getter
    @Setter
    public static class PlotBean extends CommonBean {

        Integer nb_plot;
        Double furthest_plot_distance;
        Double area_around_hq;
        boolean grouped_plots;
        boolean rather_grouped_plots;
        boolean scattered_plots;
        boolean rather_scattered_plots;
        boolean joined_plots;

        public PlotBean(CommonBean source) {
            super(source);
        }
    }

    public static class PlotModel extends CommonModel<PlotBean> {
        @Override
        public String getTitle() {
            return "Parcellaire";
        }

        public PlotModel() {
            super();
            newColumn("Nombre de parcelles", PlotBean::getNb_plot);
            newColumn("Distance du siège de l’exploitation à la parcelle la plus éloignée (km)", PlotBean::getFurthest_plot_distance);
            newColumn("Surface autour du siège d’exploitation", PlotBean::getArea_around_hq);
            newColumn("Parcellaire bien regroupé autour de l'exploitation", PlotBean::isGrouped_plots);
            newColumn("Parcellaire plutôt regroupé, quelques parcelles dispersées", PlotBean::isRather_grouped_plots);
            newColumn("Parcellaire plutôt dispersé", PlotBean::isScattered_plots);
            newColumn("Parcellaire très dispersé, certaines parcelles éloignées du siège de l'exploitation", PlotBean::isRather_scattered_plots);
            newColumn("Parcellaire constitué de plusieurs ensembles regroupés, éloignés les uns des autres", PlotBean::isJoined_plots);
        }
    }

    @Getter
    @Setter
    public static class InputBean extends CommonBean {

        InputType inputType;

        // concerne tous les intrants sauf Carburant ; Eau d'irrigation ; Autres, correspond au ProductTyp pour les traitements et au type d’engrais pour les engrais/amendements
        String productType;

        String lotName;

        String lotSpeciesName;

        // Pour Carburant et Eau d’irrigation, répéter ces termes. Pour les autres types d’intrants, indiquer le nom du champ de saisie libre.
        String productName;

        String ammNumber;

        // pour intrants du type Engrais/amendement organiques ; Engrais/amendement (organo)minéral, composition concaténée du type ‘N (33%) - K2O (5%)’
        String composition;

        Boolean priceIncludeTreatment;// Oui/Non pour le lot de semence

        Double realPrice;

        String realPriceUnit;

        Boolean chemicalTreatment;

        Boolean biologicalSeedInoculation;

        public String getPriceIncludeTreatment() {
            return priceIncludeTreatment != null ? priceIncludeTreatment ? "Oui" : "Non" : "";
        }

        public String getChemicalTreatment() {
            return chemicalTreatment != null ? chemicalTreatment ? "Oui" : "Non" : "";
        }

        public String getBiologicalSeedInoculation() {
            return biologicalSeedInoculation != null ? biologicalSeedInoculation ? "Oui" : "Non" : "";
        }

        public InputBean(CommonBean source) {
            super(source);
        }
    }

    public static class InputModel extends CommonModel<InputBean> {

        @Override
        public String getTitle() {
            return "Intrants et prix saisis";
        }

        public InputModel() {
            super();
            newColumn("Type d'intrant", InputBean::getInputType);
            newColumn("Type de produit", InputBean::getProductType);
            newColumn("Nom du lot de semence (Culture)", InputBean::getLotName);
            newColumn("Nom de l'espèce du lot de semence", InputBean::getLotSpeciesName);
            newColumn("Nom du Produit", InputBean::getProductName);
            newColumn("Numéro AMM", InputBean::getAmmNumber);
            newColumn("Composition (engrais/amendements)", InputBean::getComposition);
            newColumn("Traitement inclus dans la semence", InputBean::getPriceIncludeTreatment);
            newColumn("Traitement chimique de semence", InputBean::getChemicalTreatment);
            newColumn("Inoculation biologique des semences", InputBean::getBiologicalSeedInoculation);
            newColumn("Prix réel", InputBean::getRealPrice);
            newColumn("Unité prix réel", InputBean::getRealPriceUnit);
        }
    }

    @Getter
    @Setter
    public static class HarvestingPriceBean extends CommonBean {

        boolean isPracticed = false;

        String growingSystemName;

        String plotOrPracticedSystemName;

        String campaigns;

        String cropName;

        String destination;

        Double yealdAverage;

        Double realPrice;

        String realPriceUnit;

        public String isPracticed() {
            return isPracticed ? "Oui" : "Non";
        }

        public HarvestingPriceBean(CommonBean source) {
            super(source);
        }
    }

    public static class HarvestingPriceModel extends CommonModel<HarvestingPriceBean> {

        @Override
        public String getTitle() {
            return "Prix des récoltes";
        }

        public HarvestingPriceModel() {
            super();
            newColumn("Réalisé / Synthétisé", HarvestingPriceBean::isPracticed);
            newColumn("Système de culture", HarvestingPriceBean::getGrowingSystemName);
            newColumn("Nom parcelle / Nom Synthétisé", HarvestingPriceBean::getPlotOrPracticedSystemName);
            newColumn("Campagne(s)", HarvestingPriceBean::getCampaigns);
            newColumn("Culture", HarvestingPriceBean::getCropName);
            newColumn("Destination", HarvestingPriceBean::getDestination);
            newColumn("Rendement", HarvestingPriceBean::getYealdAverage);
            newColumn("Prix réel", HarvestingPriceBean::getRealPrice);
            newColumn("Unité prix réel", HarvestingPriceBean::getRealPriceUnit);
        }
    }
}
