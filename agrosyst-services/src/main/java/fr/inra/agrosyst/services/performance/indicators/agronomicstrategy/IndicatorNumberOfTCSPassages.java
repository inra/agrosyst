package fr.inra.agrosyst.services.performance.indicators.agronomicstrategy;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.TypeTravailSol;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.services.performance.AbstractPerformanceCropContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;


/**
 * Le nombre de passage de TCS est calculé à toutes les échelles hormis l'intrant et concerne les interventions
 * mobilisant une combinaison d'outil avec un outil de type de TCS dans la colonne Type_travail_sol du référentiel
 * de travail du sol.
 *
 * A l'échelle de l'intervention le nombre de passage de TCS correspond au PSCI.
 * Pour les interventions mobilisant une combinaison d'outil avec un outil dont la valeur de la colonne Type_travail_sol du référentiel travail du sol est TCS et la valeur de la colonne désherbage_mécanique est N alors le nombre de passage de TCS correspond au PSCI.
 *
 * Pour les interventions mobilisant une combinaison d'outil avec un outil dont la valeur de la colonne Type_travail_sol du référentiel travail du sol est TCS et la valeur de la colonne désherbage_mécanique est O alors le nombre de passage de TCS est calculé uniquement si l'intervention de travail du sol est réalisée avant le semi. Dans le cas contraire il serait considéré comme intervention de désherbage mécanique.
 *
 * Nb_passage_TCS = PSCI
 *
 * Si le matériel utilisé n'est pas de type TCS alors l'indicateur prend la valeur 0.
 */
public class IndicatorNumberOfTCSPassages extends AbstractIndicator {

    private static final String COLUMN_NAME = "nombre_de_passages_tcs";

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN
    );

    public IndicatorNumberOfTCSPassages() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel == null ||
                interventionContext.isFictive() ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return newArray(1, 0.0d);
        }

        return this.computeTSCPassages(interventionContext, this.getToolPSCi(interventionContext.getIntervention()), cropContext);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext, PerformanceEffectiveCropExecutionContext cropContext, PerformanceEffectiveInterventionExecutionContext interventionContext) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel == null ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return newArray(1, 0.0d);
        }

        return this.computeTSCPassages(interventionContext, this.getToolPSCi(interventionContext.getIntervention()), cropContext);
    }


    private Double[] computeTSCPassages(PerformanceInterventionContext interventionContext, Double interventionPSCi, AbstractPerformanceCropContext cropContext) {
        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();
        final ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();

        TillageType interventionValues = IndicatorTillageType.computeInterventionTillageType(interventionContext);

        final Set<RefCorrespondanceMaterielOutilsTS> refCorrespondanceMaterielOutilsTS = interventionValues == TillageType.TCS ? toolsCoupling.getEquipments().stream()
                        .map(Equipment::getRefMateriel)
                        .filter(correspondanceByRefMateriel::containsKey)
                        .map(correspondanceByRefMateriel::get)
                        .filter(refCorrespondance -> refCorrespondance.getTypeTravailSol() == TypeTravailSol.TCS)
                        .collect(Collectors.toSet()) : new HashSet<>();

        // Cas matériel TCS et desherbage mécanique false
        if (!refCorrespondanceMaterielOutilsTS.isEmpty() && refCorrespondanceMaterielOutilsTS.stream().noneMatch(RefCorrespondanceMaterielOutilsTS::isDesherbage_mecanique)) {
            return new Double[] { interventionPSCi };
        }

        // Cas matériel TCS et desherbage mécanique true. Obligé de gérer comme ça pour les cas en réalisé et synthétisé
        if (interventionContext instanceof PerformanceEffectiveInterventionExecutionContext effectiveInterventionExecutionContext) {
            final PerformanceEffectiveCropExecutionContext effectiveCropContext = (PerformanceEffectiveCropExecutionContext) cropContext;
            final EffectiveIntervention effectiveIntervention = effectiveInterventionExecutionContext.getIntervention();

            if (refCorrespondanceMaterielOutilsTS.stream().anyMatch(RefCorrespondanceMaterielOutilsTS::isDesherbage_mecanique)) {
                final Optional<EffectiveIntervention> mostRecentSeedingIntervention = effectiveCropContext.getInterventionExecutionContexts().stream()
                        .map(PerformanceEffectiveInterventionExecutionContext::getIntervention)
                        .filter(intervention -> !intervention.isIntermediateCrop())
                        .filter(intervention -> intervention.getType() == AgrosystInterventionType.SEMIS)
                        .max((i1, i2) -> computeAverageDay(i1).compareTo(computeAverageDay(i2)));

                if (mostRecentSeedingIntervention.isEmpty() || computeAverageDay(effectiveIntervention).compareTo(computeAverageDay(mostRecentSeedingIntervention.get())) <= 0) {
                    return new Double[] { this.getToolPSCi(effectiveIntervention) };
                }
            }
        } else if (interventionContext instanceof PerformancePracticedInterventionExecutionContext practicedInterventionContext) {
            final PerformancePracticedCropExecutionContext practicedCropContext = (PerformancePracticedCropExecutionContext) cropContext;
            final PracticedIntervention practicedIntervention = practicedInterventionContext.getIntervention();

            if (refCorrespondanceMaterielOutilsTS.stream().anyMatch(RefCorrespondanceMaterielOutilsTS::isDesherbage_mecanique)) {
                final Optional<PracticedIntervention> mostRecentSeedingIntervention = practicedCropContext.getInterventionExecutionContexts().stream()
                        .filter(performanceInterventionExecutionContext -> !performanceInterventionExecutionContext.isFictive())
                        .map(PerformancePracticedInterventionExecutionContext::getIntervention)
                        .filter(intervention -> !intervention.isIntermediateCrop())
                        .filter(intervention -> intervention.getType() == AgrosystInterventionType.SEMIS)
                        .max((i1, i2) -> computeAverageDayOfYear(i1).compareTo(computeAverageDayOfYear(i2)));

                final Double currentInterventionAverageDay = computeAverageDayOfYear(practicedIntervention);
                if (mostRecentSeedingIntervention.isEmpty() || currentInterventionAverageDay <= computeAverageDayOfYear(mostRecentSeedingIntervention.get())) {
                    return new Double[] { this.getToolPSCi(practicedIntervention) };
                }
            }
        }

        // Dans tous les autres cas, l'indicateur ne doit pas être calculé donc on retourne simplement 0
        return newArray(1, 0.0d);
    }


    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorCategory() {
        return INDICATOR_CATEGORY_AGRONOMIC_STRATEGY;
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.nbOfTCSPassages");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }
}
