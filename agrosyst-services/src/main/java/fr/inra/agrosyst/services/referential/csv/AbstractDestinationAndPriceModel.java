package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.WineValorisation;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import fr.inra.agrosyst.api.services.common.PricesService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 10/03/16.
 */
public abstract class AbstractDestinationAndPriceModel<E> extends AbstractAgrosystModel<E> {

    private static final Log LOGGER = LogFactory.getLog(AbstractDestinationAndPriceModel.class);

    protected RefFertiOrgaTopiaDao refFertiOrgaDao;

    protected RefDestinationTopiaDao refDestinationsDao;
    protected RefEspeceTopiaDao refEspeceDao;

    protected static final String EURO_DEG_PURE_ALCOHOL = "€/° d'alcool pur";
    protected static final String EURO_DIFFUSEUR = "€/diffuseur";
    protected static final String EURO_G = "€/g";
    protected static final String EURO_HA = "€/ha";
    protected static final String EURO_HL_WINE = "€/hL vin";
    protected static final String EURO_KG_GRAPE = "€/kg raisin";
    protected static final String EURO_KG = "€/kg";
    protected static final String EURO_HL = "€/hL";
    protected static final String EURO_L = "€/l";
    protected static final String EURO_PLANT = "€/plant";
    protected static final String EURO_POT = "€/pot";
    protected static final String EURO_Q = "€/q";
    protected static final String EURO_T = "€/t";
    protected static final String EURO_T_SUGAR = "€/t sucre";
    protected static final String EURO_UNITE = "€/unité";
    protected static final String EURO_M3 = "€/m3";
    protected static final String EURO_MG = "€/mg";

    protected static final String WINE = "Vin";
    protected static final String GRAPE = "Raisin de table";
    protected static final String BRANDY = "Eau-de-vie";
    
    protected static final String POT_PAR_M2 = "pot/m2";
    
    protected static final String POT_M2 = "pot_m2";
    
    protected static final String M3_HA = "m3/ha";
    protected static final String L_M2 = "l/m2";
    protected static final String KG_HA = "kg/ha";
    protected static final String G_M2 = "g/m2";
    protected static final String T_HA = "t/ha";
    protected static final String KG_M2 = "kg/m2";
    protected static final String L_POT = "l/pot";
    protected static final String G_POT = "g/pot";
    protected static final String KG_POT = "kg/pot";
    
    
    public static final int MARKETING_PERIOD_PARSER_ERROR = -2;

    protected final Map<String, String> destinationLabelByNormalisedDestinationLabel = new HashMap<>();

    protected Map<String, String> upperCodeEspeceBotaniqueTocodeEspeceBotanique = new HashMap<>();

    protected Map<String, RefDestination> destinationByCodes = new HashMap<>();

    protected final java.util.function.Function<RefDestination, String> GET_REF_DESTINATION_CODE_A = RefDestination::getCode_destination_A;

    protected List<String> allQualityCriteriaCodes = new ArrayList<>();

    protected final ValueParser<String> CODE_DESTINATION_A_PARSER = value -> {
        String result = null;
        String from = value.toUpperCase();
        RefDestination to;
        if (StringUtils.isNoneBlank(from)) {
            to = destinationByCodes.get(from);
            if (to == null) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(String.format("le code destination_A:'%s' n'a pas été retrouvé", value));
                }
                throw new AgrosystImportException(String.format("le code destination_A:'%s' n'a pas été retrouvé", value));
            } else {
                result = to.getCode_destination_A();
            }

        }
        return result;
    };

    protected final ValueParser<String> DESTINATION_LABEL_PARSER = value -> {
        String normalizedLabel = AbstractAgrosystModel.LABEL_FORMATTER.parse(value);
        String destinationLabel = destinationLabelByNormalisedDestinationLabel.get(normalizedLabel);
        destinationLabel = StringUtils.isNotBlank(destinationLabel) ? destinationLabel: normalizedLabel;
        return destinationLabel;
    };

    protected final ValueParser<String> CODE_ESPECE_BOTANIQUE_PARSER = value -> {
        String from = value.toUpperCase();
        String to = null;
        if (StringUtils.isNoneBlank(from)) {
            to = upperCodeEspeceBotaniqueTocodeEspeceBotanique.get(from);
            if (Strings.isNullOrEmpty(to)) {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(String.format("le code espèce botanique:'%s' n'a pas été retrouvé", value));
                }
                throw new AgrosystImportException(String.format("le code espèce botanique:'%s' n'a pas été retrouvé", value));
            }
        }
        return to;
    };

    protected final ValueParser<String> CODE_CRITERE_QUALITE_PARSER = value -> {
        String result = null;
        String from = value.toUpperCase();
        if (StringUtils.isNoneBlank(from)) {
            if (allQualityCriteriaCodes.contains(from)) {
                result = from;
            } else {
                if (LOGGER.isErrorEnabled()) {
                    LOGGER.error(String.format("le code critères de qualité :'%s' n'a pas été retrouvé", value));
                }
                throw new AgrosystImportException(String.format("le code critères de qualité:'%s' n'a pas été retrouvé", value));
            }

        }
        return result;
    };

    protected static final ValueParser<Integer> MARKETING_PERIOD_PARSER = value -> {

        Integer result = null;
        if (StringUtils.isEmpty(value) || "TOUTES".equals(StringUtils.upperCase(StringUtils.stripAccents(value)))) return -1;

        String harvestingPeriod = StringUtils.upperCase(StringUtils.stripAccents(value));
        Boolean isNumber = harvestingPeriod.matches("\\d+");

        if (isNumber) {
            result =  Integer.valueOf(harvestingPeriod);

            if (result < Calendar.JANUARY || result > Calendar.DECEMBER) {
                result = MARKETING_PERIOD_PARSER_ERROR;// error
            }
        } else {
            if ("JANVIER".contentEquals(harvestingPeriod)) {
                result = Calendar.JANUARY;
            } else if ("FEVRIER".contentEquals(harvestingPeriod)) {
                result = Calendar.FEBRUARY;
            } else if ("MARS".contentEquals(harvestingPeriod)) {
                result = Calendar.MARCH;
            } else if ("AVRIL".contentEquals(harvestingPeriod)) {
                result = Calendar.APRIL;
            } else if ("MAI".contentEquals(harvestingPeriod)) {
                result = Calendar.MAY;
            } else if ("JUIN".contentEquals(harvestingPeriod)) {
                result = Calendar.JUNE;
            } else if ("JUILLET".contentEquals(harvestingPeriod)) {
                result = Calendar.JULY;
            } else if ("AOUT".contentEquals(harvestingPeriod)) {
                result = Calendar.AUGUST;
            } else if ("SEPTEMBRE".contentEquals(harvestingPeriod)) {
                result = Calendar.SEPTEMBER;
            } else if ("OCTOBRE".contentEquals(harvestingPeriod)) {
                result = Calendar.OCTOBER;
            } else if ("NOVEMBRE".contentEquals(harvestingPeriod)) {
                result = Calendar.NOVEMBER;
            } else if ("DECEMBRE".contentEquals(harvestingPeriod)) {
                result = Calendar.DECEMBER;
            }
        }

        return result;
    };

    protected static final ValueFormatter<Integer> MARKETING_PERIOD_FORMATTER = value -> {

        String result = "TOUTES";

        if (value == Calendar.JANUARY) result = "JANVIER";
        else if (value == Calendar.FEBRUARY) result = "FEVRIER";
        else if (value == Calendar.MARCH) result = "MARS";
        else if (value == Calendar.APRIL) result = "AVRIL";
        else if (value == Calendar.MAY) result = "MAI";
        else if (value == Calendar.JUNE) result = "JUIN";
        else if (value == Calendar.JULY) result = "JUILLET";
        else if (value == Calendar.AUGUST) result = "AOUT";
        else if (value == Calendar.SEPTEMBER) result = "SEPTEMBRE";
        else if (value == Calendar.OCTOBER) result = "OCTOBRE";
        else if (value == Calendar.NOVEMBER) result = "NOVEMBRE";
        else if (value == Calendar.DECEMBER) result = "DECEMBRE";

        return result;
    };

    protected static final ValueParser<PriceUnit> PRICE_UNIT_PARSER = value -> {
        PriceUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(StringUtils.deleteWhitespace(value));
            if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_DEG_PURE_ALCOHOL)))) {
                result = PriceUnit.EURO_DEG_PURE_ALCOHOL;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_DIFFUSEUR)))) {
                result = PriceUnit.EURO_DIFFUSEUR;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_G)))) {
                result = PriceUnit.EURO_G;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_HA)))) {
                result = PricesService.DEFAULT_PRICE_UNIT;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_HL_WINE)))) {
                result = PriceUnit.EURO_HL_WINE;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_HL)))) {
                result = PriceUnit.EURO_HL;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_KG_GRAPE)))) {
                result = PriceUnit.EURO_KG_GRAPE;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_KG)))) {
                result = PriceUnit.EURO_KG;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_L)))) {
                result = PriceUnit.EURO_L;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_PLANT)))) {
                result = PriceUnit.EURO_PLANT;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_POT)))) {
                result = PriceUnit.EURO_POT;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_Q)))) {
                result = PriceUnit.EURO_Q;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_T)))) {
                result = PriceUnit.EURO_T;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_T_SUGAR)))) {
                result = PriceUnit.EURO_T_SUGAR;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_UNITE)))) {
                result = PriceUnit.EURO_UNITE;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_M3)))) {
                result = PriceUnit.EURO_M3;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(EURO_MG)))) {
                result = PriceUnit.EURO_MG;
            } else {
                result = (PriceUnit) getGenericEnumParser(PriceUnit.class, value);
            }
        }
        return result;
    };
    
    protected static final ValueParser<PotInputUnit> POT_INPUT_UNIT_PARSER = value -> {
        PotInputUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(StringUtils.deleteWhitespace(value));
            if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(POT_M2))) || strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(POT_PAR_M2)))) {
                result = PotInputUnit.POTS_M2;
            } else {
                result = (PotInputUnit) getGenericEnumParser(PotInputUnit.class, value);
            }
        }
        return result;
    };
    
    protected static final ValueParser<SubstrateInputUnit> SUBSTRATE_INPUT_UNIT_PARSER = value -> {
        SubstrateInputUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(StringUtils.deleteWhitespace(value));
            if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(M3_HA)))) {
                result = SubstrateInputUnit.M3_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(L_M2)))) {
                result = SubstrateInputUnit.L_M2;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(KG_HA)))) {
                result = SubstrateInputUnit.KG_HA;
            }
            else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(G_M2)))) {
                result = SubstrateInputUnit.G_M2;
            }
            else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(T_HA)))) {
                result = SubstrateInputUnit.T_HA;
            }
            else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(KG_M2)))) {
                result = SubstrateInputUnit.KG_M2;
            }
            else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(L_POT)))) {
                result = SubstrateInputUnit.L_POT;
            }
            else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(G_POT)))) {
                result = SubstrateInputUnit.G_POT;
            }
            else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(KG_POT)))) {
                result = SubstrateInputUnit.KG_POT;
            } else {
                result = (SubstrateInputUnit) getGenericEnumParser(SubstrateInputUnit.class, value);
            }
        }
        return result;
    };

    protected static final ValueFormatter<PriceUnit> PRICE_UNIT_FORMATTER = value -> {
        String result = null;
        if (value != null) {
            result = switch (value) {
                case EURO_DEG_PURE_ALCOHOL -> EURO_DEG_PURE_ALCOHOL;
                case EURO_DIFFUSEUR -> EURO_DIFFUSEUR;
                case EURO_G -> EURO_G;
                case EURO_HA -> EURO_HA;
                case EURO_HL_WINE -> EURO_HL_WINE;
                case EURO_KG_GRAPE -> EURO_KG_GRAPE;
                case EURO_KG -> EURO_KG;
                case EURO_L -> EURO_L;
                case EURO_HL -> EURO_HL;
                case EURO_PLANT, EURO_POT -> EURO_PLANT;
                case EURO_Q -> EURO_Q;
                case EURO_T -> EURO_T;
                case EURO_T_SUGAR -> EURO_T_SUGAR;
                case EURO_UNITE -> EURO_UNITE;
                case EURO_M3 -> EURO_M3;
                case EURO_MG -> EURO_MG;
                default -> value.name().toLowerCase();
            };

        }
        return result;
    };

    protected AbstractDestinationAndPriceModel(char separator) {
        super(separator);
    }

    protected void getRefDestinationLabelByNormalisedLabeled() {
        List<String> destinationLabels = refDestinationsDao.getAllDestinationLibelle();
        for (String destinationLabel : destinationLabels) {
            try {
                String normalisedDestinationLabel = AbstractAgrosystModel.LABEL_FORMATTER.parse(destinationLabel);
                destinationLabelByNormalisedDestinationLabel.put(normalisedDestinationLabel, destinationLabel);
            } catch (ParseException e) {
                LOGGER.warn(String.format("Échec de normalisation du libellé de destination, libellé '%s' ignoré", destinationLabel), e);
            }
        }
    }

    protected void getRefDestinationByCodes() {
        List<RefDestination> allDestinations = refDestinationsDao.findAll();
        destinationByCodes = Maps.uniqueIndex(allDestinations, GET_REF_DESTINATION_CODE_A::apply);
    }

    protected void getUpperCodeEspeceBotaniqueToCodeEspeceBotanique() {
        upperCodeEspeceBotaniqueTocodeEspeceBotanique = refEspeceDao.getUpperCodeEspeceBotaniqueToCodeEspeceBotanique();
    }

    protected void getQualityCriteriaCodes(RefQualityCriteriaTopiaDao refQualityCriteriaTopiaDao) {
        allQualityCriteriaCodes = refQualityCriteriaTopiaDao.findAllRefQualityCriteriaCodes();
    }

    protected static final ValueParser<WineValorisation> VALORISATION_PARSER = value -> {
        WineValorisation result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(value);

            if (strValue.equalsIgnoreCase(WINE)) {
                result = WineValorisation.WINE;
            } else if (strValue.equalsIgnoreCase(GRAPE)) {
                result = WineValorisation.GRAPE;
            } else if (strValue.equalsIgnoreCase(BRANDY)) {
                result = WineValorisation.BRANDY;
            } else {
                result = WineValorisation.valueOf(strValue);
            }
        }
        return result;
    };

    protected static final ValueFormatter<WineValorisation> VALORISATION_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = switch (value) {
                case WINE -> WINE;
                case GRAPE -> GRAPE;
                case BRANDY -> BRANDY;
                default -> value.name().toLowerCase();
            };
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueParser<String> STRING_NOT_NULL_PARSER = value -> {
        value = value == null ? "" : value;
        return value;
    };
}
