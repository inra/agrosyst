package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import org.apache.commons.collections4.MultiValuedMap;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

public class IndicatorLowRiskSubstances extends IndicatorSubstancesCandidateToSubstitution {
    public static final String COLUMN_NAME = "qsa_substances_faible_risque";
    public static final String COLUMN_NAME_HTS = "qsa_substances_faible_risque_hts";

    /**
     * <p>
     * Il faut créer à toutes les échelles 2 indicateurs identiques à la QSA totale (ticket 12311) (avec traitements de semences), pour seulement une sélection de substances actives.
     * QSA Substances à faible risque : on ne prend en compte que les substances du référentiel "Substances Actives Commission Européenne" dont les valeurs de la colonne N "SA_faible_risque" prend la valeur "O"
     * <p>
     * Pour chaque produit avec AMM,
     * on recherche dans le référentiel 'Substances Actives composant les produits AMM' les 'id_sa' qui le composent
     * pour chaque 'id_sa' on recherche dans le référentiel 'Substances Actives Commission Européenne' si 'sa_candidates_substitution' est vrai
     * Si vrai on fait la somme des (dose substance active * qté appliquée * applique la conversion en Kg * psci * surface de traitement)
     * <p>
     */
    protected final String[] indicators = new String[]{
            "Indicator.label.totalLowRiskQSA",        // QSA Substances à faible risque
            "Indicator.label.totalLowRiskQSA_hts",    // QSA Substances à faible risque hors traitement de semence
    };

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    protected @NotNull List<RefCompositionSubstancesActivesParNumeroAMM> getQSA_Substance(Map<String, List<RefCompositionSubstancesActivesParNumeroAMM>> domainSubstancesByAmm, MultiValuedMap<String, RefSubstancesActivesCommissionEuropeenne> allSubstancesActivesCommissionEuropeenneByAmmCode, String codeAmm) {
        List<String> idSaCandidatesToSubstitution = allSubstancesActivesCommissionEuropeenneByAmmCode.get(codeAmm)
                .stream()
                .filter(RefSubstancesActivesCommissionEuropeenne::isSa_faible_risque)
                .toList()
                .stream()
                .map(RefSubstancesActivesCommissionEuropeenne::getId_sa)
                .toList();

        List<RefCompositionSubstancesActivesParNumeroAMM> doseSaCandidatesToSubstitution = domainSubstancesByAmm.getOrDefault(codeAmm, List.of())
                .stream()
                .filter(ref -> idSaCandidatesToSubstitution.contains(ref.getId_sa())).
                toList();
        return doseSaCandidatesToSubstitution;
    }

    @Override
    protected String[] getIndicators() {
        return indicators;
    }
}
