package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Marge directe.
 * <p>
 * La Marge directe est exprimée en €/ha. La Marge directe correspond à la Marge semi-nette à laquelle on soustrait les charges de main d'œuvre totales.
 * Elle est définie seulement à partir de l’échelle du couple culture-précédent, et agrégées aux échelles supérieures ensuite.
 * Cet indicateur doit pouvoir être calculé avec deux méthodes de calculs : Avec autoconsommation et Sans autoconsommation.
 * C'est deux paramètres ne sont maintenant plus des indicateurs, mais bien des méthodes de calculs.
 * <p>
 * Il est calculé suivant la formule suivante :
 * Marge directe 𝑖 = Temps de travail manuel 𝑖 + CMO totale 𝑖
 * <p>
 * Avec :
 * Marge directe 𝑖 (h/ha) : Marge directe de l’intervention i.
 * Marge semi-nette 𝑖 (h/ha) : Marge semi-nette de l’intervention i. Indicateur calculé par Agrosyst.
 * CMO totale 𝑖 (h/ha) : Charges de main d'œuvre totale de l’intervention i. Indicateur calculé par Agrosyst.
 *
 * @author Geoffroy Gley (gley@codelutin.com)
 */
public class IndicatorDirectMargin extends AbstractIndicator {

    public static final String COLUMN_NAME = "marge_directe";
    private static final String MARGE_DIRECTE_REELLE_TX_COMP = COLUMN_NAME + "_reelle_taux_de_completion";
    private static final String MARGE_DIRECTE_STD_MIL_TX_COMP = COLUMN_NAME + "_std_mil_taux_de_completion";
    private static final String MARGE_DIRECTE_REELLE_DETAIL = COLUMN_NAME + "_reelle_detail_champs_non_renseig";
    private static final String MARGE_DIRECTE_STD_MIL_DETAIL = COLUMN_NAME + "_std_mil_detail_champs_non_renseig";


    private static final String[] LABELS = new String[]{
            "Indicator.label.directMarginRealWithoutAC",
            "Indicator.label.directMarginRealWithAC",
            "Indicator.label.directMarginStandardWithoutAC",
            "Indicator.label.directMarginStandardWithAC",
    };

    private boolean isWithAutoConsumed = true;
    private boolean isWithoutAutoConsumed = true;
    private boolean computeReal = true;
    private boolean computeStandardised = true;

    private final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public IndicatorDirectMargin() {
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_DIRECTE_REELLE_TX_COMP,
                MARGE_DIRECTE_REELLE_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_DIRECTE_REELLE_DETAIL,
                MARGE_DIRECTE_REELLE_DETAIL
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_DIRECTE_STD_MIL_TX_COMP,
                MARGE_DIRECTE_STD_MIL_TX_COMP
        );
        extraFields.put(
                OptionalExtraColumnEnumKey.MARGE_DIRECTE_STD_MIL_DETAIL,
                MARGE_DIRECTE_STD_MIL_DETAIL
        );
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale,INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) &&
                displayed && (
                        i == 0 && isWithoutAutoConsumed && computeReal ||
                                i == 1 && isWithAutoConsumed && computeReal ||
                                i == 2 && isWithoutAutoConsumed && computeStandardised ||
                                i == 3 && isWithAutoConsumed && computeStandardised
        );
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        // pour inclure les colonnes extra
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME + "_reelle_sans_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME + "_reelle_avec_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(2), COLUMN_NAME + "_std_mil_sans_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(3), COLUMN_NAME + "_std_mil_avec_autoconso");

        indicatorNameToColumnName.put(MARGE_DIRECTE_REELLE_TX_COMP, MARGE_DIRECTE_REELLE_TX_COMP);
        indicatorNameToColumnName.put(MARGE_DIRECTE_STD_MIL_TX_COMP, MARGE_DIRECTE_STD_MIL_TX_COMP);

        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) return newArray(LABELS.length, 0.0d);
        PracticedIntervention intervention = interventionContext.getIntervention();
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // SemiNetMarginWithAutoconsume
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // SemiNetMarginWithoutAutoconsume
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // TotalWorkforceExpenses
        Pair<Double, Double> semiNetMarginWithAutoconsume = interventionContext.getSemiNetMarginWithAutoconsume();
        Pair<Double, Double> semiNetMarginWithoutAutoconsume = interventionContext.getSemiNetMarginWithoutAutoconsume();
        Pair<Double, Double> totalWorkforceExpenses = interventionContext.getTotalWorkforceExpenses();
        return computeRealDirectMargin(semiNetMarginWithAutoconsume, semiNetMarginWithoutAutoconsume, totalWorkforceExpenses, intervention.getTopiaId());
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        EffectiveIntervention intervention = interventionContext.getIntervention();
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // SemiNetMarginWithAutoconsume
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // SemiNetMarginWithoutAutoconsume
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // TotalWorkforceExpenses
        Pair<Double, Double> semiNetMarginWithAutoconsume = interventionContext.getSemiNetMarginWithAutoconsume();
        Pair<Double, Double> semiNetMarginWithoutAutoconsume = interventionContext.getSemiNetMarginWithoutAutoconsume();
        Pair<Double, Double> totalWorkforceExpenses = interventionContext.getTotalWorkforceExpenses();
        return computeRealDirectMargin(semiNetMarginWithAutoconsume, semiNetMarginWithoutAutoconsume, totalWorkforceExpenses, intervention.getTopiaId());
    }

    private Double[] computeRealDirectMargin(Pair<Double, Double> semiNetMarginWithAutoconsume,
                                                         Pair<Double, Double> semiNetMarginWithoutAutoconsume,
                                                         Pair<Double, Double> totalWorkforceExpenses,
                                                         String interventionId) {
        double realDirectMarginWithAutoConsume = 0.0d;
        double standardizedDirectMarginWithAutoConsume = 0.0d;
        double realDirectMarginWithoutAutoConsume = 0.0d;
        double standardizedDirectMarginWithoutAutoConsume = 0.0d;
        if (totalWorkforceExpenses == null) {
            addMissingIndicatorMessage(interventionId, messageBuilder.getMissingTotalWorkforceExpensesMessage());
        }
        if (semiNetMarginWithAutoconsume != null && totalWorkforceExpenses != null) {
            realDirectMarginWithAutoConsume = semiNetMarginWithAutoconsume.getLeft() - totalWorkforceExpenses.getLeft();
            standardizedDirectMarginWithAutoConsume = semiNetMarginWithAutoconsume.getRight() - totalWorkforceExpenses.getRight();
        } else {
            addMissingIndicatorMessage(interventionId, messageBuilder.getMissingSemiNetMarginWithAutoconsumeMessage());
        }
        if (semiNetMarginWithoutAutoconsume != null && totalWorkforceExpenses != null) {
            realDirectMarginWithoutAutoConsume = semiNetMarginWithoutAutoconsume.getLeft() - totalWorkforceExpenses.getLeft();
            standardizedDirectMarginWithoutAutoConsume = semiNetMarginWithoutAutoconsume.getRight() - totalWorkforceExpenses.getRight();
        } else {
            addMissingIndicatorMessage(interventionId, messageBuilder.getMissingSemiNetMarginWithoutAutoconsumeMessage());
        }
        return newResult(realDirectMarginWithoutAutoConsume, realDirectMarginWithAutoConsume, standardizedDirectMarginWithoutAutoConsume, standardizedDirectMarginWithAutoConsume);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
        if (displayed) {
            isWithAutoConsumed = filter.getWithAutoConsumed() != null && filter.getWithAutoConsumed();
            isWithoutAutoConsumed = filter.getWithoutAutoConsumed() != null && filter.getWithoutAutoConsumed();
            computeReal = filter.getComputeReal() != null && filter.getComputeReal();
            computeStandardised = filter.getComputeStandardized() != null && filter.getComputeStandardized();
        }
    }
}
