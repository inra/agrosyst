package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLeverImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 12/10/16.
 */
public class RefStrategyLeverModel extends AbstractDestinationAndPriceModel<RefStrategyLever> implements ExportModel<RefStrategyLever> {

    public RefStrategyLeverModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Code", RefStrategyLever.PROPERTY_CODE);
        newMandatoryColumn("Filière", RefStrategyLever.PROPERTY_SECTOR, SECTOR_PARSER);
        newMandatoryColumn("Rubrique", RefStrategyLever.PROPERTY_SECTION_TYPE, SECTION_TYPE_PARSER);
        newMandatoryColumn("Type de gestion/levier", RefStrategyLever.PROPERTY_STRATEGY_TYPE, STRATEGY_TYPE_PARSER);
        newMandatoryColumn("Levier", RefStrategyLever.PROPERTY_LEVER);
        newMandatoryColumn("Source", RefStrategyLever.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefStrategyLever.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefStrategyLever, Object>> getColumnsForExport() {
        ModelBuilder<RefStrategyLever> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Code", RefStrategyLever.PROPERTY_CODE);
        modelBuilder.newColumnForExport("Filière", RefStrategyLever.PROPERTY_SECTOR, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Rubrique", RefStrategyLever.PROPERTY_SECTION_TYPE, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Type de gestion/levier", RefStrategyLever.PROPERTY_STRATEGY_TYPE, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("Levier", RefStrategyLever.PROPERTY_LEVER);
        modelBuilder.newColumnForExport("Source", RefStrategyLever.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefStrategyLever.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefStrategyLever newEmptyInstance() {
        RefStrategyLever refStrategyLever = new RefStrategyLeverImpl();
        refStrategyLever.setActive(true);
        return refStrategyLever;
    }
}
