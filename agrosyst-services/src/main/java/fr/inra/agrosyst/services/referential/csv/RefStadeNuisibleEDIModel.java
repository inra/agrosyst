package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDIImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * repository_id;reference_id;reference_param;reference_code;reference_label;source
 * 
 * @author Eric Chatellier
 */
public class RefStadeNuisibleEDIModel extends AbstractAgrosystModel<RefStadeNuisibleEDI> implements ExportModel<RefStadeNuisibleEDI> {

    public RefStadeNuisibleEDIModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("repository_id", RefStadeNuisibleEDI.PROPERTY_REPOSITORY_ID, INT_PARSER);
        newMandatoryColumn("reference_id", RefStadeNuisibleEDI.PROPERTY_REFERENCE_ID, INT_PARSER);
        newMandatoryColumn("reference_param", RefStadeNuisibleEDI.PROPERTY_REFERENCE_PARAM);
        newMandatoryColumn("reference_code", RefStadeNuisibleEDI.PROPERTY_REFERENCE_CODE);
        newMandatoryColumn("reference_label", RefStadeNuisibleEDI.PROPERTY_REFERENCE_LABEL);
        newMandatoryColumn("source", RefStadeNuisibleEDI.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefStadeNuisibleEDI.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefStadeNuisibleEDI, Object>> getColumnsForExport() {
        ModelBuilder<RefStadeNuisibleEDI> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("repository_id", RefStadeNuisibleEDI.PROPERTY_REPOSITORY_ID, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("reference_id", RefStadeNuisibleEDI.PROPERTY_REFERENCE_ID, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("reference_param", RefStadeNuisibleEDI.PROPERTY_REFERENCE_PARAM);
        modelBuilder.newColumnForExport("reference_code", RefStadeNuisibleEDI.PROPERTY_REFERENCE_CODE);
        modelBuilder.newColumnForExport("reference_label", RefStadeNuisibleEDI.PROPERTY_REFERENCE_LABEL);
        modelBuilder.newColumnForExport("source", RefStadeNuisibleEDI.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefStadeNuisibleEDI.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefStadeNuisibleEDI newEmptyInstance() {
        RefStadeNuisibleEDI refStadeNuisibleEDI = new RefStadeNuisibleEDIImpl();
        refStadeNuisibleEDI.setActive(true);
        return refStadeNuisibleEDI;
    }
}
