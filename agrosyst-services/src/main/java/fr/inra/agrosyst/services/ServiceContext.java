package fr.inra.agrosyst.services;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.inra.agrosyst.api.entities.AgrosystTopiaDaoSupplier;
import fr.inra.agrosyst.api.entities.AgrosystTopiaPersistenceContext;
import fr.inra.agrosyst.api.services.AgrosystService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.async.BusinessTasksManager;
import fr.inra.agrosyst.services.internal.InstancesSynchroHelper;
import fr.inra.agrosyst.services.security.SecurityContext;
import org.nuiton.topia.persistence.TopiaTransaction;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

public interface ServiceContext extends AutoCloseable {

    String SERVICE_CONTEXT_PARAMETER = ServiceContext.class.getName();

    /**
     * Méthode à utiliser plutôt que new LocalDate.now() pour une meilleure testabilité
     *
     * @return la date courante
     */
    LocalDate getCurrentDate();

    /**
     * Méthode à utiliser plutôt que new LocalDateTime.now() pour une meilleure testabilité
     *
     * @return la date courante
     */
    LocalDateTime getCurrentTime();

    /**
     * Méthode à utiliser plutôt que new OffsetDateTime.now() pour une meilleure testabilité
     *
     * @return la date courante
     */
    OffsetDateTime getOffsetTime();

    /**
     * Permet à chaque service d'obtenir la transaction en cours.
     * Cette méthode revient à faire {@link #getTransaction(boolean)})} avec la valeur 'true'
     *
     * @return the currentTransaction
     * @see #getTransaction(boolean)
     */
    TopiaTransaction getTransaction();

    /**
     * Permet à chaque service d'obtenir la transaction en cours
     *
     * @param create indique si il faut créer la transaction si elle n'existe pas
     * @return the currentTransaction
     */
    TopiaTransaction getTransaction(boolean create);

    /**
     * Permet à chaque service d'obtenir un fournisseur de Dao
     *
     * @return l'instance de TopiaDaoSupplier
     */
    AgrosystTopiaDaoSupplier getDaoSupplier();

    /**
     * @return l'instance courante du TopiaPersistenceContext
     */
    AgrosystTopiaPersistenceContext getPersistenceContext();

    /**
     * @param create indique si il faut créer la transaction si elle n'existe pas
     * @return l'instance courante du TopiaPersistenceContext
     */
    AgrosystTopiaPersistenceContext getPersistenceContext(boolean create);

    /**
     * Create new service context from current service context with custom lifecycle.
     * 
     * @return new service context
     */
    ServiceContext newServiceContext();

    /**
     * Create new service context from current service context with custom lifecycle for a user identified by the given
     * token.
     *
     * @return new service context configured for a specific user.
     */
    ServiceContext newServiceContext(AuthenticatedUser authenticatedUser);

    /**
     * Build new service instance depending on factory implementation.
     *
     * @param clazz service interface
     * @return service implementation
     * @see fr.inra.agrosyst.api.services.ServiceFactory#newService(Class)
     */
    <E extends AgrosystService> E newService(Class<E> clazz);

    /**
     * Build new {@code clazz} instance with inject objects (Service, Config, DAO...)
     *
     * @param clazz implementation class
     * @return clazz instance
     */
    <I> I newInstance(Class<I> clazz);
    
    <E extends AgrosystService, F extends AgrosystService> F newExpectedService(Class<E> clazz, Class<F> expectedClazz);
    
    /**
     * Let each service get the current configuration
     *
     * @return the AgrosystServiceConfig instance
     */
    AgrosystServiceConfig getConfig();

    /**
     * Returns the current security context. Note : the life cycle of this object is linked to the security context
     * life cycle (request).
     *
     * @return the current security context
     */
    SecurityContext getSecurityContext();

    /**
     * Returns the current security context to operate as specified {@code userId}. Note : the life cycle of this object is linked to the security context
     * life cycle (request).
     *
     * @param userId userId to operate with
     * @return the current security context
     */
    SecurityContext getSecurityContextAsUser(String userId);

    Gson getGson();

    /**
     * @return the {@link InstancesSynchroHelper} instance.
     */
    InstancesSynchroHelper getSynchroHelper();

    BusinessTasksManager getTaskManager();

}
