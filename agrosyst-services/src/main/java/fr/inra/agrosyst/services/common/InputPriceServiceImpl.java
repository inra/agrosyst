package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.Language;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.SeedPriceTopiaDao;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.services.common.InputPriceFilter;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.common.ProductPriceSummary;
import fr.inra.agrosyst.api.services.common.RefInputPriceDto;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static fr.inra.agrosyst.services.common.PricesServiceImpl.*;
import static fr.inra.agrosyst.services.performance.indicators.IndicatorManualWorkforceExpenses.STANDARD_MANUAL_WORK_COST_VALUE;
import static fr.inra.agrosyst.services.performance.indicators.IndicatorMechanizedWorkforceExpenses.STANDARD_MECHANIZED_WORK_COST_VALUE;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Setter
public class InputPriceServiceImpl extends AbstractAgrosystService implements InputPriceService {

    HarvestingPriceTopiaDao harvestingPriceDao;
    InputPriceTopiaDao inputPriceDao;
    
    SeedPriceTopiaDao seedPriceDao;
    RefInputPriceTopiaDao refInputPriceDao;
    RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;

    protected DomainService domainService;

    @Override
    public String getRefFertiMinUnifaDisplayName(Language language, RefFertiMinUNIFA mineralProduct) {
        Map<FertiMinElement, String> frElementNames = new HashMap<>();
        frElementNames.put(FertiMinElement.N, "n");
        frElementNames.put(FertiMinElement.P2_O5, "p2O5");
        frElementNames.put(FertiMinElement.K2_O, "k2O");
        frElementNames.put(FertiMinElement.BORE, "bore");
        frElementNames.put(FertiMinElement.CALCIUM, "calcium");
        frElementNames.put(FertiMinElement.FER, "fer");
        frElementNames.put(FertiMinElement.MANGANESE, "manganese");
        frElementNames.put(FertiMinElement.MOLYBDENE, "molybdene");
        frElementNames.put(FertiMinElement.MG_O, "mgO");
        frElementNames.put(FertiMinElement.OXYDE_DE_SODIUM, "oxyde_de_sodium");
        frElementNames.put(FertiMinElement.S_O3, "sO3");
        frElementNames.put(FertiMinElement.CUIVRE, "cuivre");
        frElementNames.put(FertiMinElement.ZINC, "zinc");

        String valuedPartModel = "%s=(%s%%)";
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        List<String> valuedElements = new ArrayList<>();
        if (mineralProduct.getN() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.N), df.format(mineralProduct.getN())));
        if (mineralProduct.getP2O5() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.P2_O5), df.format(mineralProduct.getP2O5())));
        if (mineralProduct.getK2O() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.K2_O), df.format(mineralProduct.getK2O())));
        if (mineralProduct.getBore() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.BORE), df.format(mineralProduct.getBore())));
        if (mineralProduct.getCalcium() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.CALCIUM), df.format(mineralProduct.getCalcium())));
        if (mineralProduct.getFer() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.FER), df.format(mineralProduct.getFer())));
        if (mineralProduct.getManganese() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.MANGANESE), df.format(mineralProduct.getManganese())));
        if (mineralProduct.getMolybdene() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.MOLYBDENE), df.format(mineralProduct.getMolybdene())));
        if (mineralProduct.getMgO() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.MG_O), df.format(mineralProduct.getMgO())));
        if (mineralProduct.getOxyde_de_sodium() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.OXYDE_DE_SODIUM), df.format(mineralProduct.getOxyde_de_sodium())));
        if (mineralProduct.getsO3() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.S_O3), df.format(mineralProduct.getsO3())));
        if (mineralProduct.getCuivre() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.CUIVRE), df.format(mineralProduct.getCuivre())));
        if (mineralProduct.getZinc() != 0) valuedElements.add(String.format(valuedPartModel, frElementNames.get(FertiMinElement.ZINC), df.format(mineralProduct.getZinc())));

        //amendements-engrais siderurgiques basiques N(1%), P2O5(2.5%), K2O(3%)
        String trad = refFertiMinUNIFADao.findMineralProductTranslatedTypeProduit(language, mineralProduct.getType_produit());
        String typeProduitName = StringUtils.isNotBlank(trad) ? trad : mineralProduct.getType_produit();
        String displayName = typeProduitName + " " + String.join(", ", valuedElements);
        return displayName;
    }

    @Override
    public void deleteHarvestingPrices(Set<HarvestingPrice> harvestingPricesToRemoves) {
        harvestingPriceDao.deleteAll(harvestingPricesToRemoves);
    }
    
    
    @Override
    public void createOrUpdateHarvestingPrices(List<HarvestingPrice> harvestingPricesToCreateOrUpdate) {
        List<HarvestingPrice> harvestingPricesToCreate = new ArrayList<>();
        List<HarvestingPrice> harvestingPricesToUpdate = new ArrayList<>();
        harvestingPricesToCreateOrUpdate.forEach(
                p -> {
                    if (p.isPersisted()) {
                        harvestingPricesToUpdate.add(p);
                    } else {
                        harvestingPricesToCreate.add(p);
                    }
                }
        );
        harvestingPriceDao.updateAll(harvestingPricesToUpdate);
        harvestingPriceDao.createAll(harvestingPricesToCreate);
    }
    
    @Override
    public List<HarvestingPrice> getPracticedSystemPrices(PracticedSystem practicedSystem) {
        return harvestingPriceDao.forPracticedSystemEquals(practicedSystem).findAll();
    }
    
    @Override
    public ProductPriceSummary loadPriceIndications(InputPriceFilter filter, String excludeDomain) {
    
        final ProductPriceSummary userCurrentCampaignsPricesSummary = inputPriceDao.loadUserPriceIndicationFromDomain(filter, excludeDomain);
        final Map<PriceUnit, RefInputPriceDto> refPrices = getDomainInputRefPrices(filter);

        Collection<PriceUnit> allowedPriceUnits = getAllowedPriceUnits(filter, refPrices);
    
        final ProductPriceSummary currentCampaignsRefPricesSummary = new ProductPriceSummary(
                userCurrentCampaignsPricesSummary.lowerPrice(),
                userCurrentCampaignsPricesSummary.higherPrice(),
                userCurrentCampaignsPricesSummary.medianPrice(),
                userCurrentCampaignsPricesSummary.averagePrice(),
                userCurrentCampaignsPricesSummary.countedPrices(),
                refPrices,
                allowedPriceUnits);
        
        return currentCampaignsRefPricesSummary;
    }

    @Override
    public void deleteInputPrice(InputPrice price) {
        InputPriceCategory inputPriceCategory = price.getCategory();
        if (InputPriceCategory.HARVESTING_ACTION.equals(inputPriceCategory)) {
            harvestingPriceDao.delete((HarvestingPrice) price);
        } else if (InputPriceCategory.SEEDING_INPUT.equals(inputPriceCategory) ||
                InputPriceCategory.SEEDING_PLAN_COMPAGNE_INPUT.equals(inputPriceCategory)
        ) {
            seedPriceDao.delete((SeedPrice) price);
        } else {
            inputPriceDao.delete(price);
        }
    }
    @Override
    public InputPrice persistInputPrice(InputPrice inputPrice) {
        if (inputPrice == null) {
            return null;
        }
        InputPrice persitedInputPrice;
        InputPriceCategory inputPriceCategory = inputPrice.getCategory();

        if (InputPriceCategory.SEEDING_INPUT.equals(inputPriceCategory) ||
                InputPriceCategory.SEEDING_PLAN_COMPAGNE_INPUT.equals(inputPriceCategory)) {

            if (inputPrice.isPersisted()) {
                persitedInputPrice = seedPriceDao.update((SeedPrice) inputPrice);
            } else {
                persitedInputPrice = seedPriceDao.create((SeedPrice) inputPrice);
            }

        } else {

            if (inputPrice.isPersisted()) {
                persitedInputPrice = inputPriceDao.update(inputPrice);
            } else {
                persitedInputPrice = inputPriceDao.create(inputPrice);
            }

        }
        return persitedInputPrice;
    }
    
    @Override
    public InputPrice getNewPriceInstance(InputPriceCategory inputPriceCategory) {
        InputPrice price;
        if (InputPriceCategory.SEEDING_INPUT.equals(inputPriceCategory) ||
                InputPriceCategory.SEEDING_PLAN_COMPAGNE_INPUT.equals(inputPriceCategory)
        ) {
            price = seedPriceDao.newInstance();
        } else {
            price = inputPriceDao.newInstance();
        }
        return price;
    }
    
    @Override
    public InputPrice bindDtoToPrice(
            Domain domain,
            InputPriceDto inputPriceDto,
            InputPrice price,
            String priceObjectId,
            Optional<PracticedSystem> harvestingPracticedSystem,
            Optional<Zone> harvestingZone) {
    
        shallowBindPriceDtoToPojo(inputPriceDto, price, priceObjectId, domain);
        
        if (InputPriceCategory.SEEDING_INPUT.equals(inputPriceDto.getCategory())||
                InputPriceCategory.SEEDING_PLAN_COMPAGNE_INPUT.equals(inputPriceDto.getCategory())){
            
            SeedPrice seedPrice = (SeedPrice) price;
            seedPrice.setBiologicalSeedInoculation(inputPriceDto.isBiologicalSeedInoculation());
            seedPrice.setChemicalTreatment(inputPriceDto.isChemicalTreatment());
            seedPrice.setIncludedTreatment(inputPriceDto.isIncludedTreatment());
            seedPrice.setOrganic(inputPriceDto.isOrganic());
            seedPrice.setSeedType(inputPriceDto.getSeedType());
            
        }
        
        return price;
    }
    
    @Override
    public InputPrice bindToNewPrice(
            Domain domain,
            InputPrice fromPrice,
            String priceObjectId,
            Optional<PracticedSystem> harvestingPracticedSystem,
            Optional<Zone> harvestingZone) {
        
        if (fromPrice == null) {
            return null;
        }
        
        InputPrice newPrice = getNewPriceInstance(fromPrice.getCategory());
        
        shallowBindPriceToPojo(fromPrice, newPrice, priceObjectId, domain);
    
        if (InputPriceCategory.SEEDING_INPUT.equals(fromPrice.getCategory())||
                InputPriceCategory.SEEDING_PLAN_COMPAGNE_INPUT.equals(fromPrice.getCategory())){
            SeedPrice origin = (SeedPrice) fromPrice;
            SeedPrice newSeedPrice = (SeedPrice) newPrice;
            newSeedPrice.setBiologicalSeedInoculation(origin.isBiologicalSeedInoculation());
            newSeedPrice.setChemicalTreatment(origin.isChemicalTreatment());
            newSeedPrice.setIncludedTreatment(origin.isIncludedTreatment());
            newSeedPrice.setOrganic(origin.isOrganic());
            newSeedPrice.setSeedType(origin.getSeedType());
        
        } else if (InputPriceCategory.HARVESTING_ACTION.equals(fromPrice.getCategory())) {
            Preconditions.checkArgument(harvestingPracticedSystem.isPresent() || harvestingZone.isPresent());
            HarvestingPrice newHarvestingPrice = (HarvestingPrice) newPrice;
            newHarvestingPrice.setPracticedSystem(harvestingPracticedSystem.orElse(null));
            newHarvestingPrice.setZone(harvestingZone.orElse(null));
        }
    
        return newPrice;
    }
    
    protected void shallowBindPriceDtoToPojo(InputPriceDto priceDto, InputPrice price, String objectId, Domain domain) {
        price.setDomain(domain);
        price.setObjectId(objectId);
        price.setDisplayName(priceDto.getDisplayName());
        price.setPrice(priceDto.getPrice());
        price.setPriceUnit(priceDto.getPriceUnit());
        price.setCategory(priceDto.getCategory());
        price.setSourceUnit(priceDto.getSourceUnit());
    }
    protected void shallowBindPriceToPojo(InputPrice price, InputPrice newPrice, String objectId, Domain domain) {
        newPrice.setDomain(domain);
        newPrice.setObjectId(objectId);
        newPrice.setDisplayName(price.getDisplayName());
        newPrice.setPrice(price.getPrice());
        newPrice.setPriceUnit(price.getPriceUnit());
        newPrice.setCategory(price.getCategory());
        newPrice.setSourceUnit(price.getSourceUnit());
    }

    private Map<PriceUnit, RefInputPriceDto> getDomainInputRefPrices(InputPriceFilter filter) {
        Integer campaign = filter.getCampaign();
        Map<PriceUnit, RefInputPriceDto> result = new HashMap<>();
        InputPriceCategory category = filter.getCategory();
        final String objectId = filter.getObjectId();
        switch (category) {
            case BIOLOGICAL_CONTROL_INPUT, SEEDING_TREATMENT_INPUT, PHYTO_TRAITMENT_INPUT -> {
                var refCountry = domainService.getCountry();
                List<RefPrixPhyto> refPrices = refInputPriceDao.findRefPrixPhytoForCampaign(filter, refCountry);
                refPrices.forEach(rp -> result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice())));
            }
            case FUEL_INPUT -> {
                
                Optional<RefInputPrice> optionalRefPrice = refInputPriceDao.findRefPrixForCampaign(RefPrixCarbu.class, campaign);
                if (optionalRefPrice.isPresent()) {
                    RefInputPrice rp = optionalRefPrice.get();
                    result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice()));
                }
            }
            case IRRIGATION_INPUT -> {
                
                Optional<RefInputPrice> optionalRefPrice = refInputPriceDao.findRefPrixForCampaign(RefPrixIrrig.class, campaign);
                if (optionalRefPrice.isPresent()) {
                    RefInputPrice rp = optionalRefPrice.get();
                    result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice()));
                }
            }
            case MINERAL_INPUT -> {
                
                // categ=109 forme=Granule n=16,000000 p2O5=null k2O=null bore=null calcium=null fer=null manganese=null molybdene=null mgO=6,000000 oxyde_de_sodium=10,000000 sO3=null cuivre=null zinc=null
                String[] elementRes = objectId.replace(',', '.').split(" ");
                int categ = Integer.parseInt(elementRes[0].split("=")[1]);
                String forme = elementRes[1].split("=")[1];
                Map<FertiMinElement, Double> fertiMinElementWithQuantity = OBJECT_ID_TO_FERTI_MIN_ELEMENTS_QUANTITY.apply(objectId);

                Set<FertiMinElement> elements = new HashSet<>();
                for (Map.Entry<FertiMinElement, Double> fertiMinElementToUnitsFor100kg : fertiMinElementWithQuantity.entrySet()) {
                    if(fertiMinElementToUnitsFor100kg.getValue() > 0) {
                        elements.add(fertiMinElementToUnitsFor100kg.getKey());
                    }
                }

                List<RefPrixFertiMin> refPrices = refInputPriceDao.findRefPrixFertiMinForCampaigns(categ, forme, elements, Sets.newHashSet(campaign));

                Map<FertiMinElement, List<PriceAndUnit>> refPricesByElements = new HashMap<>();
                refPrices.forEach(rp -> {

                        List<PriceAndUnit> refInputPriceDtos = refPricesByElements.computeIfAbsent(rp.getElement(), k -> new ArrayList<>());
                        refInputPriceDtos.add(new PriceAndUnit(rp.getPrice(), rp.getUnit()));
                    }
                );

                PriceAndUnit averageFertiMinInputRefPrice = RefFertiMinInputPriceServiceImpl.computeAverageRefPrice(fertiMinElementWithQuantity, refPricesByElements);

                result.put(averageFertiMinInputRefPrice.unit(), new RefInputPriceDto(campaign, averageFertiMinInputRefPrice.unit(), averageFertiMinInputRefPrice.value()));
            }
            case ORGANIQUES_INPUT -> {
                
                List<RefPrixFertiOrga> refPrices = refInputPriceDao.findRefPrixFertiOrgaForCampaign(filter);
                refPrices.forEach(rp -> result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice())));
            }
            case OTHER_INPUT -> {
                
                final String[] characteristics = objectId.split(" - ");
                String inputType_c0 = characteristics.length > 0 ? refInputPriceDao.getParamDbName(characteristics[0], RefPrixAutre.PROPERTY_INPUT_TYPE_C0, RefPrixAutre.class) : null;
                String caracteristic1 = characteristics.length > 1 ? refInputPriceDao.getParamDbName(characteristics[1], RefPrixAutre.PROPERTY_CARACTERISTIC1, RefPrixAutre.class) : null;
                String caracteristic2 = characteristics.length > 2 ? refInputPriceDao.getParamDbName(characteristics[1], RefPrixAutre.PROPERTY_CARACTERISTIC2, RefPrixAutre.class) : null;
                String caracteristic3 = characteristics.length > 3 ? refInputPriceDao.getParamDbName(characteristics[1], RefPrixAutre.PROPERTY_CARACTERISTIC3, RefPrixAutre.class) : null;
                
                List<RefPrixAutre> refPrices = refInputPriceDao.findRefPrixAutreForCampaign(inputType_c0, caracteristic1, caracteristic2, caracteristic3, campaign);
                refPrices.forEach(rp -> result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice())));
            }
            case POT_INPUT -> {

                String caracteristic1 = refInputPriceDao.getParamDbName(objectId, RefPrixPot.PROPERTY_CARACTERISTIC1, RefPrixPot.class);
                List<RefPrixPot> refPrices = refInputPriceDao.findRefPrixPotForCampaign(caracteristic1, campaign);
                refPrices.forEach(rp -> result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice())));
            }
            case SEEDING_INPUT, SEEDING_PLAN_COMPAGNE_INPUT -> {
                
                List<RefPrixEspece> refPrices = refInputPriceDao.findRefPrixEspeceForCampaign(filter);
                refPrices.forEach(rp -> result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice())));
            }
            case SUBSTRATE_INPUT -> {
                
                final String[] characteristics = objectId.split(" - ");
                String caracteristic1 = refInputPriceDao.getParamDbName(characteristics[0], RefPrixSubstrate.PROPERTY_CARACTERISTIC1, RefPrixSubstrate.class);
                String caracteristic2 = refInputPriceDao.getParamDbName(characteristics[1], RefPrixSubstrate.PROPERTY_CARACTERISTIC2, RefPrixSubstrate.class);
    
                List<RefPrixSubstrate> refPrices = refInputPriceDao.findRefPrixSubstratForCampaign(caracteristic1, caracteristic2, campaign);
                refPrices.forEach(rp -> result.put(rp.getUnit(), new RefInputPriceDto(rp.getCampaign(), rp.getUnit(), rp.getPrice())));
            }
            case MAIN_OEUVRE_MANUELLE_INPUT -> result.put(PriceUnit.EURO_H, new RefInputPriceDto(filter.getCampaign(), PriceUnit.EURO_H, STANDARD_MANUAL_WORK_COST_VALUE));
            case MAIN_OEUVRE_TRACTORISTE_INPUT -> result.put(PriceUnit.EURO_H, new RefInputPriceDto(filter.getCampaign(), PriceUnit.EURO_H, STANDARD_MECHANIZED_WORK_COST_VALUE));
        }
        return result;
    }

    private Set<PriceUnit> getAllowedPriceUnits(InputPriceFilter filter, Map<PriceUnit, RefInputPriceDto> refPrices) {
        Set<PriceUnit> result = new HashSet<>();

        if (InputPriceCategory.FUEL_INPUT.equals(filter.getCategory())) {
            result.add(PriceUnit.EURO_L);
        } else if (InputPriceCategory.SEEDING_INPUT.equals(filter.getCategory()) ||
            InputPriceCategory.SEEDING_PLAN_COMPAGNE_INPUT.equals(filter.getCategory())) {

            if (!refPrices.isEmpty()) {
                for (Map.Entry<PriceUnit, RefInputPriceDto> priceRefInputPriceEntry : refPrices.entrySet()) {

                    RefInputPriceDto rp = priceRefInputPriceEntry.getValue();
                    if (rp != null) {
                        PriceUnit rpUnit = rp.priceUnit();
                        if (WEIGTH_PRICE_UNITS.contains(rpUnit)) {
                            result.addAll(WEIGTH_PRICE_UNITS);
                        } else if (SEED_PRICE_UNITS.contains(rpUnit)) {
                            result.addAll(SEED_PRICE_UNITS);
                        }
                    }
                }
            } else {
                result.addAll(WEIGTH_PRICE_UNITS);
                result.addAll(SEED_PRICE_UNITS);
            }

        } else if (InputPriceCategory.MAIN_OEUVRE_TRACTORISTE_INPUT.equals(filter.getCategory()) ||
                InputPriceCategory.MAIN_OEUVRE_MANUELLE_INPUT.equals(filter.getCategory())) {

            result.add(WORKFORCE_PRICE_UNIT);

        } else if (!refPrices.isEmpty()) {
            for (Map.Entry<PriceUnit, RefInputPriceDto> priceRefInputPriceEntry : refPrices.entrySet()) {

                RefInputPriceDto rp = priceRefInputPriceEntry.getValue();

                PriceUnit rpUnit = null;
                if (rp != null) {
                    rpUnit = rp.priceUnit();
                }

                if (rpUnit == null) {
                    Set<PriceUnit> units;
                    if (filter.getCategory() == InputPriceCategory.IRRIGATION_INPUT) {
                        units = new HashSet<>(VOL_PRICE_UNITS);
                    } else {
                        units = new HashSet<>();
                    }
                    result.addAll(units);
                } else {
                    Set<PriceUnit> units = new HashSet<>();
                    units.add(rpUnit);// first same unit has ref prices
                    if (WEIGTH_PRICE_UNITS.contains(rpUnit)) {
                        units.addAll(WEIGTH_PRICE_UNITS);
                    } else if (WINE_PRICE_UNITS.contains(rpUnit)) {
                        units.addAll(WINE_PRICE_UNITS);
                    } else if (VOL_PRICE_UNITS.contains(rpUnit)) {
                        units.addAll(VOL_PRICE_UNITS);
                    }
                    units.add(PricesService.DEFAULT_PRICE_UNIT);// default value
                    result.addAll(units);
                }
            }
        } else {
            // default
            result.addAll(DEFAULT_PRICE_UNITS);
        }

        return result;
    }
}
