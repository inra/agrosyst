package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Map;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Temps de travail total.
 * <p>
 * Le temps de travail total est exprimé en h/ha. Il se calcule en additionnant le temps de travail manuel au temps de travail mécanisé. Il reprend par conséquent le nombre de travailleurs utilisés pour les interventions correspondantes.
 * <p>
 * Il est calculé suivant la formule suivante :
 * Temps de travail total 𝑖 = Temps de travail manuel 𝑖 + Temps de travail mécanisé 𝑖
 * <p>
 * Avec :
 * Temps de travail total 𝑖 (h/ha) : Temps de travail total de l’intervention i.
 * Temps de travail manuel 𝑖 (h/ha) : Temps de travail manuel i. Indicateur calculé par Agrosyst.
 * Temps de travail mécanisé 𝑖 (h/ha) : Temps de travail mécanisé i. Indicateur calculé par Agrosyst.
 *
 * @author Geoffroy Gley (gley@codelutin.com)
 */
public class IndicatorTotalWorkTime extends AbstractIndicator {

    public static final String COLUMN_NAME = "temps_travail_total";
    public static final int NB_MONTH = 12;

    private boolean detailedByMonth = true;
    protected String[] translatedMonth;

    private final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);

    public IndicatorTotalWorkTime() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed && (detailedByMonth || i == NB_MONTH);
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorLabel(int i) {
        String result;
        if (0 <= i && i < NB_MONTH) { // 12 months + sum
            result = l(locale, "Indicator.label.totalWorkTimeMonth", translatedMonth[i]);
        } else {
            result = l(locale, "Indicator.label.totalWorkTime");
        }
        return result;
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i <= NB_MONTH; i++) {
            String label = getIndicatorLabel(i);
            String dbColumnName;
            if (i < NB_MONTH) { // 12 months + sum
                dbColumnName = COLUMN_NAME + "_" + MONTHS_FOR_DB_COLUMNS[i];
            } else {
                dbColumnName = COLUMN_NAME;
            }
            indicatorNameToColumnName.put(label, dbColumnName);
        }

        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        PracticedIntervention intervention = interventionContext.getIntervention();
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // ManualWorkTime
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // MechanizedWorkTime
        if (interventionContext.isFictive()) return newArray(NB_MONTH + 1, 0.0d); // 12 months + sum
        Double manualWorkTime = interventionContext.getManualWorkTime();
        Double mechanizedWorkTime = interventionContext.getMechanizedWorkTime();
        double totalWorkTime = computeTotalWorkTime(manualWorkTime, mechanizedWorkTime, intervention.getTopiaId());
        interventionContext.setTotalWorkTime(totalWorkTime);
        return newResult(getMonthsRatio(intervention, totalWorkTime));
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        EffectiveIntervention intervention = interventionContext.getIntervention();
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // ManualWorkTime
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId()); // MechanizedWorkTime
        Double manualWorkTime = interventionContext.getManualWorkTime();
        Double mechanizedWorkTime = interventionContext.getMechanizedWorkTime();
        double totalWorkTime = computeTotalWorkTime(manualWorkTime, mechanizedWorkTime, intervention.getTopiaId());
        interventionContext.setTotalWorkTime(totalWorkTime);
        return newResult(getMonthsRatio(intervention, totalWorkTime));
    }

    private double computeTotalWorkTime(Double manualWorkTime, Double mechanizedWorkTime, String interventionId) {
        double totalWorkTime = 0.0d;
        if (manualWorkTime != null && mechanizedWorkTime != null) {
            totalWorkTime = manualWorkTime + mechanizedWorkTime;
        } else {
            if (manualWorkTime == null) {
                addMissingIndicatorMessage(interventionId, messageBuilder.getMissingManualWorkTimeMessage());
            }
            if (mechanizedWorkTime == null) {
                addMissingIndicatorMessage(interventionId, messageBuilder.getMissingMechanizedWorkTimeMessage());
            }
        }
        return totalWorkTime;
    }

    public void init(IndicatorFilter filter, String[]  translatedMonth) {
        displayed = filter != null;
        detailedByMonth = displayed && BooleanUtils.isTrue(filter.getDetailedByMonth());
        this.translatedMonth = translatedMonth;
    }
}
