package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefAnimalTypeImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefAnimalTypeModel extends AbstractAgrosystModel<RefAnimalType> implements ExportModel<RefAnimalType> {

    public RefAnimalTypeModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Type d’animaux", RefAnimalType.PROPERTY_ANIMAL_TYPE);
        newMandatoryColumn("Unité (taille de l’atelier d’élevage)", RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS);
        newOptionalColumn(COLUMN_ACTIVE, RefAnimalType.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefAnimalType, Object>> getColumnsForExport() {
        ModelBuilder<RefAnimalType> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Type d’animaux", RefAnimalType.PROPERTY_ANIMAL_TYPE);
        modelBuilder.newColumnForExport("Unité (taille de l’atelier d’élevage)", RefAnimalType.PROPERTY_ANIMAL_POPULATION_UNITS);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefAnimalType.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefAnimalType newEmptyInstance() {
        RefAnimalType refAnimalType = new RefAnimalTypeImpl();
        refAnimalType.setActive(true);
        return refAnimalType;
    }

}
