package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.indicators.fertilization.AbstractIndicatorFertilization;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.nuiton.i18n.I18n.l;

/**
 * Classe calculant l'indicateur QSA Soufre Fertilisation.
 * <p>
 * QSA Soufre fertilisation = PSCI * ( Qorg * Csorg + Qmin * 0.4 * Cso3min) -> rev 09/09/2024 Suite aux analyses de Lucie, pour coller à la méthode de la cellule référence, il faut retirer le facteur 0.4 pour le soufre
 * La formule devient donc : QSA Soufre fertilisation = PSCI * ( Qorg * Csorg + Qmin * Cso3min)
 * Avec :
 *   Qorg = quantité de fertilisant organique appliquée
 *   Csorg = concentration en S dans le fertilisant organique
 *   Qmin = Quantité de fertilisant minéral appliquée
 *   Cso3min = Quantité de SO3 dans le fertilisant minéral
 * On applique un coefficient de 0.4 car le SO3 a une concentration massique en soufre de 40%.
 */
public class IndicatorSulfurFertilisationProduct extends AbstractIndicatorFertilization {

    private static final String COLUMN_NAME = "qsa_soufre_ferti";

    private static final double SO3_MASS_CONCENTRATION_OF_SULFUR = 1.0d;

    public IndicatorSulfurFertilisationProduct() {
        //  il n'y a pas de version d'indicateur hors traitement de semence puisqu'un traitement de semence ne peut pas être un fertilisant
        // labels
        super(new String[] {
                "Indicator.label.sulfurFertiProduct", // QSA Soufre fertilisation
        });
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return newArray(this.labels.length, 0.0);
        }

        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();
        final Optional<OrganicFertilizersSpreadingAction> optionalOrganicFertilizersSpreadingAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();

        Optional<Double[]> optionalResult = this.computeQuantity(writerContext, optionalMineralFertilizersSpreadingAction, optionalOrganicFertilizersSpreadingAction, interventionContext);
        return optionalResult.orElse(null);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();
        final Optional<OrganicFertilizersSpreadingAction> optionalOrganicFertilizersSpreadingAction = interventionContext.getOptionalOrganicFertilizersSpreadingAction();

        Optional<Double[]> optionalResult = this.computeQuantity(writerContext, optionalMineralFertilizersSpreadingAction, optionalOrganicFertilizersSpreadingAction, interventionContext);
        return optionalResult.orElse(null);
    }

    private Optional<Double[]> computeQuantity(WriterContext writerContext,
                                             Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction,
                                             Optional<OrganicFertilizersSpreadingAction> optionalOrganicFertilizersSpreadingAction,
                                             PerformanceInterventionContext interventionContext) {

        AtomicBoolean computeDoneFlag = new AtomicBoolean(false);

        Double[] quantities = newArray(this.labels.length, 0.0d);

        if (optionalMineralFertilizersSpreadingAction.isPresent()) {
            MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = optionalMineralFertilizersSpreadingAction.get();
            for (MineralProductInputUsage mineralProductInputUsage : mineralFertilizersSpreadingAction.getMineralProductInputUsages()) {
                final double sQuantity = interventionContext.getSO3QuantityByInputUsage().getOrDefault(mineralProductInputUsage.getTopiaId(), 0.0d) * SO3_MASS_CONCENTRATION_OF_SULFUR;

                this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, sQuantity, 0);

                interventionContext.addUsageFertiSQuantity(mineralFertilizersSpreadingAction, mineralProductInputUsage, sQuantity);

                quantities[0] += sQuantity;

                computeDoneFlag.set(true);
            }
        }

        if (optionalOrganicFertilizersSpreadingAction.isPresent()) {
            OrganicFertilizersSpreadingAction organicFertilizersSpreadingAction = optionalOrganicFertilizersSpreadingAction.get();
            for (OrganicProductInputUsage organicProductInputUsage : organicFertilizersSpreadingAction.getOrganicProductInputUsages()) {
                final double sQuantity = interventionContext.getSQuantityByInputUsage().getOrDefault(organicProductInputUsage.getTopiaId(), 0.0d);

                this.writeInputUsageForSubstance(writerContext, organicProductInputUsage, organicFertilizersSpreadingAction, sQuantity, 0);

                interventionContext.addUsageFertiSQuantity(organicFertilizersSpreadingAction, organicProductInputUsage, sQuantity);

                quantities[0] += sQuantity;

                computeDoneFlag.set(true);
            }
        }

        if (computeDoneFlag.get()) {
            interventionContext.setFertiSQuantity(quantities[0]);
            return Optional.of(quantities);
        }

        return Optional.empty();
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.activeSubstances");
    }


    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return isRelevant(atLevel) && displayed;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }
}
