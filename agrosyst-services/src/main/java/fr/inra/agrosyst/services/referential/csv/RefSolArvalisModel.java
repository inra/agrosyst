package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisImpl;
import fr.inra.agrosyst.api.services.referential.ImportStatus;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;

/**
 * Sol arvalis import model
 * 
 * Columns:
 * <pre>
 * id_type_sol character varying(20) NOT NULL,;  sol_nom text,;  sol_calcaire text,;
 * sol_hydromorphie text,;  sol_pierrosite text,;  sol_profondeur text,;  sol_texture text,;
 * sol_region text,;Source
 * </pre>
 * 
 * @author Eric Chatellier
 */
public class RefSolArvalisModel extends AbstractAgrosystModel<RefSolArvalis> implements ExportModel<RefSolArvalis> {

    public RefSolArvalisModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("id_type_sol", RefSolArvalis.PROPERTY_ID_TYPE_SOL);
        newMandatoryColumn("sol_nom", RefSolArvalis.PROPERTY_SOL_NOM);
        newMandatoryColumn("sol_calcaire", RefSolArvalis.PROPERTY_SOL_CALCAIRE);
        newMandatoryColumn("sol_calcaire_typeCode", RefSolArvalis.PROPERTY_SOL_CALCAIRE_TYPE_CODE);
        newMandatoryColumn("sol_hydromorphie", RefSolArvalis.PROPERTY_SOL_HYDROMORPHIE);
        newMandatoryColumn("sol_hydromorphie_typeCode", RefSolArvalis.PROPERTY_SOL_HYDROMORPHIE_TYPE_CODE);
        newMandatoryColumn("sol_pierrosite", RefSolArvalis.PROPERTY_SOL_PIERROSITE);
        newMandatoryColumn("sol_pierrosite_typeCode", RefSolArvalis.PROPERTY_SOL_PIERROSITE_TYPE_CODE);
        newMandatoryColumn("sol_profondeur", RefSolArvalis.PROPERTY_SOL_PROFONDEUR);
        newMandatoryColumn("sol_profondeur_typeCode", RefSolArvalis.PROPERTY_SOL_PROFONDEUR_TYPE_CODE);
        newMandatoryColumn("sol_texture", RefSolArvalis.PROPERTY_SOL_TEXTURE);
        newMandatoryColumn("sol_texture_typeCode", RefSolArvalis.PROPERTY_SOL_TEXTURE_TYPE_CODE);
        newMandatoryColumn("sol_region", RefSolArvalis.PROPERTY_SOL_REGION);
        newMandatoryColumn("Source", RefSolArvalis.PROPERTY_SOURCE);
        newIgnoredColumn("id_Agrosyst");
        newIgnoredColumn("status");
        newOptionalColumn(COLUMN_ACTIVE, RefSolArvalis.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefSolArvalis newEmptyInstance() {
        RefSolArvalis refSolArvalis = new RefSolArvalisImpl();
        refSolArvalis.setActive(true);
        return refSolArvalis;
    }

    @Override
    public Iterable<ExportableColumn<RefSolArvalis, Object>> getColumnsForExport() {
        ModelBuilder<RefSolArvalis> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id_type_sol", RefSolArvalis.PROPERTY_ID_TYPE_SOL);
        modelBuilder.newColumnForExport("sol_nom", RefSolArvalis.PROPERTY_SOL_NOM);
        modelBuilder.newColumnForExport("sol_calcaire", RefSolArvalis.PROPERTY_SOL_CALCAIRE);
        modelBuilder.newColumnForExport("sol_calcaire_typeCode", RefSolArvalis.PROPERTY_SOL_CALCAIRE_TYPE_CODE);
        modelBuilder.newColumnForExport("sol_hydromorphie", RefSolArvalis.PROPERTY_SOL_HYDROMORPHIE);
        modelBuilder.newColumnForExport("sol_hydromorphie_typeCode", RefSolArvalis.PROPERTY_SOL_HYDROMORPHIE_TYPE_CODE);
        modelBuilder.newColumnForExport("sol_pierrosite", RefSolArvalis.PROPERTY_SOL_PIERROSITE);
        modelBuilder.newColumnForExport("sol_pierrosite_typeCode", RefSolArvalis.PROPERTY_SOL_PIERROSITE_TYPE_CODE);
        modelBuilder.newColumnForExport("sol_profondeur", RefSolArvalis.PROPERTY_SOL_PROFONDEUR);
        modelBuilder.newColumnForExport("sol_profondeur_typeCode", RefSolArvalis.PROPERTY_SOL_PROFONDEUR_TYPE_CODE);
        modelBuilder.newColumnForExport("sol_texture", RefSolArvalis.PROPERTY_SOL_TEXTURE);
        modelBuilder.newColumnForExport("sol_texture_typeCode", RefSolArvalis.PROPERTY_SOL_TEXTURE_TYPE_CODE);
        modelBuilder.newColumnForExport("sol_region", RefSolArvalis.PROPERTY_SOL_REGION);
        modelBuilder.newColumnForExport("Source", RefSolArvalis.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("id_Agrosyst", RefSolArvalisImpl.PROPERTY_AGROSYST_ID);
        modelBuilder.newColumnForExport("status", RefSolArvalisImpl.PROPERTY_STATUS, STATUS_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefSolArvalisImpl.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    protected static final ValueFormatter<ImportStatus> STATUS_FORMATTER = value -> {
        String status = value == null ? "" : value.toString();
        return status;
    };

}
