package fr.inra.agrosyst.services.internal;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.services.AgrosystServiceConfig;
import fr.inra.agrosyst.services.common.CacheDiscriminator;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.jms.JMSException;
import java.lang.management.ManagementFactory;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class InstancesSynchroHelper {

    private static final Log log = LogFactory.getLog(InstancesSynchroHelper.class);

    private static final String RELOAD_CONFIG_TOPIC = "reloadConfig";
    private static final String CLEAR_CACHE_TOPIC = "clearCache";
    private static final String CLEAR_SINGLE_CACHE_TOPIC = "clearSingleCache";
    private static final String PING_TOPIC = "ping";

    protected final Optional<String> jmsUrl;

    /**
     * Maintien une liste de listeners à notifier de chaque ping reçu.
     * En théorie le set est vidé quasi-immédiatement (cf {@link #ping()}).
     */
    protected final Set<Consumer<String>> pingListeners = Collections.newSetFromMap(new ConcurrentHashMap<>());

    protected final Map<String, Runnable> emptyMessagesWatchers = new HashMap<>();
    protected final Map<String, Consumer<String>> messagesWatchers = new HashMap<>();
    protected final Map<String, Consumer<String>> selfMessagesWatchers = new HashMap<>();

    protected ScheduledExecutorService jmsPingCancelExecutor;

    @Getter
    @Setter
    private static class JmsMessage {
        protected String instanceId;
        protected String content;
    }

    public InstancesSynchroHelper(AgrosystServiceConfig config) {
        this(config.getJmsUrl());
    }

    public InstancesSynchroHelper(Optional<String> jmsUrl) {
        this.jmsUrl = jmsUrl;

        if (jmsUrl.isPresent()) {
            ThreadFactory jmsPingCancelThreadFactory = new ThreadFactoryBuilder()
                    .setNameFormat("jms-ping-cancel-%d")
                    .build();
            // On autorise un maximum de 8 threads pour les fortes sollicitations (en théorie 1 seul suffit)
            this.jmsPingCancelExecutor = Executors.newScheduledThreadPool(8, jmsPingCancelThreadFactory);
        }
    }

    public void shutdown() {
        if (this.jmsUrl.isPresent()) {
            JmsHelper jmsHelper = JmsHelper.getInstance(this.jmsUrl.get());
            try {
                jmsHelper.shutdown();
            } catch (JMSException e) {
                throw new AgrosystTechnicalException("Unable to shutdown JMS", e);
            }

            try {
                if (!jmsPingCancelExecutor.isShutdown()) {
                    jmsPingCancelExecutor.shutdown();
                }
            } catch (Exception e) {
                throw new AgrosystTechnicalException("Unable to shutdown ExecutorService", e);
            }
        }
    }

    public void healthCheck() {
        if (jmsUrl.isPresent()) {
            try {
                JmsHelper jmsHelper = JmsHelper.getInstance(jmsUrl.get());
                if (!jmsHelper.isConnectionAvailable()) {
                    log.warn("Connexion non disponible, on essaye de se reconnecter");
                    emptyMessagesWatchers.forEach((topicName, runnable) -> watchEmptyMessage(jmsUrl.get(), topicName, runnable));
                    messagesWatchers.forEach((topicName, consumer) -> watchMessage(jmsUrl.get(), topicName, consumer));
                    selfMessagesWatchers.forEach((topicName, consumer) -> watchSelfMessage(jmsUrl.get(), topicName, consumer));
                }
            } catch (Exception eee) {
                log.error("Unable to check connection statu", eee);
            }

            // Test de ping pour valider
            Future<Void> pingJms = ping();
            Preconditions.checkState(!pingJms.isCancelled(), "La tâche ne devrait pas tourner puisque JMS est désactivé");
            try {
                pingJms.get(1, TimeUnit.SECONDS);
                log.debug("Test de ping OK");
            } catch (TimeoutException | InterruptedException | ExecutionException eee) {
                log.warn("Test de ping KO : " + eee.getMessage());
            }
        }
    }

    /**
     * @return un identifiant de l'instance. Cet identifiant va changer si la JVM est redémarrée.
     */
    protected String computeInstanceId() {
        String result = ManagementFactory.getRuntimeMXBean().getName();
        return result;
    }

    protected JmsMessage toMessage(String content) {
        JmsMessage result = new JmsMessage();
        result.setInstanceId(computeInstanceId());
        result.setContent(content);
        return result;
    }

    protected JmsMessage toEmptyMessage() {
        JmsMessage result = new JmsMessage();
        result.setInstanceId(computeInstanceId());
        return result;
    }

    protected void emitEmptyMessage(String url, String topicName) {
        try {
            JmsHelper jmsHelper = JmsHelper.getInstance(url);
            JmsMessage jmsMessage = toEmptyMessage();
            String json = new Gson().toJson(jmsMessage);
            jmsHelper.sendMessage(topicName, json);
        } catch (Exception eee) {
            log.error("Unable to emit message", eee);
        }
    }

    protected void emitMessage(String url, String topicName, String content) {
        try {
            JmsHelper jmsHelper = JmsHelper.getInstance(url);
            JmsMessage jmsMessage = toMessage(content);
            String json = new Gson().toJson(jmsMessage);
            jmsHelper.sendMessage(topicName, json);
        } catch (Exception eee) {
            log.error("Unable to emit message", eee);
        }
    }

    protected void registerEmptyMessageWatcher(String url, String topicName, Runnable runnable) {
        emptyMessagesWatchers.put(topicName, runnable);
        watchEmptyMessage(url, topicName, runnable);
    }

    protected void watchEmptyMessage(String url, String topicName, Runnable runnable) {
        final String instanceId = computeInstanceId();
        JmsHelper jmsHelper = JmsHelper.getInstance(url);
        jmsHelper.watchMessage(topicName, message -> {
            JmsMessage jmsMessage = new Gson().fromJson(message, JmsMessage.class);
            if (instanceId.equals(jmsMessage.getInstanceId())) {
                if (log.isTraceEnabled()) {
                    log.trace("Le message est ignoré car il a été émi par l'instance courante");
                }
            } else {
                runnable.run();
            }
        });
    }

    protected void registerMessageWatcher(String url, String topicName, Consumer<String> consumer) {
        messagesWatchers.put(topicName, consumer);
        watchMessage(url, topicName, consumer);
    }

    protected void watchMessage(String url, String topicName, Consumer<String> consumer) {
        final String instanceId = computeInstanceId();
        JmsHelper jmsHelper = JmsHelper.getInstance(url);
        jmsHelper.watchMessage(topicName, message -> {
            JmsMessage jmsMessage = new Gson().fromJson(message, JmsMessage.class);
            if (instanceId.equals(jmsMessage.getInstanceId())) {
                if (log.isTraceEnabled()) {
                    log.trace("Le message est ignoré car il a été émi par l'instance courante");
                }
            } else {
                consumer.accept(jmsMessage.getContent());
            }
        });
    }

    protected void registerSelfMessageWatcher(String url, String topicName, Consumer<String> consumer) {
        selfMessagesWatchers.put(topicName, consumer);
        watchSelfMessage(url, topicName, consumer);
    }

    protected void watchSelfMessage(String url, String topicName, Consumer<String> consumer) {
        final String instanceId = computeInstanceId();
        JmsHelper jmsHelper = JmsHelper.getInstance(url);
        jmsHelper.watchMessage(topicName, message -> {
            JmsMessage jmsMessage = new Gson().fromJson(message, JmsMessage.class);
            if (instanceId.equals(jmsMessage.getInstanceId())) {
                consumer.accept(jmsMessage.getContent());
            } else {
                if (log.isTraceEnabled()) {
                    log.trace("Le message est ignoré car il est destiné à l'instance émettrice");
                }
            }
        });
    }

    public void emitConfigReload() {
        jmsUrl.ifPresent(url -> emitEmptyMessage(url, RELOAD_CONFIG_TOPIC));
    }

    public void watchConfigReload(Runnable runnable) {
        jmsUrl.ifPresent(url -> registerEmptyMessageWatcher(url, RELOAD_CONFIG_TOPIC, runnable));
    }

    public void emitClearCache() {
        jmsUrl.ifPresent(url -> emitEmptyMessage(url, CLEAR_CACHE_TOPIC));
    }

    public void watchClearCache(Runnable runnable) {
        jmsUrl.ifPresent(url -> registerEmptyMessageWatcher(url, CLEAR_CACHE_TOPIC, runnable));
    }

    public void emitClearSingleCache(CacheDiscriminator discriminator) {
        jmsUrl.ifPresent(url -> emitMessage(url, CLEAR_SINGLE_CACHE_TOPIC, discriminator.discriminatorAsString()));
    }

    public void watchClearSingleCache(Consumer<String> consumer) {
        jmsUrl.ifPresent(url -> registerMessageWatcher(url, CLEAR_SINGLE_CACHE_TOPIC, consumer));
    }

    protected void pingReceived(String pingId) {
        if (pingListeners.isEmpty() && log.isWarnEnabled()) {
            // En théorie on ne reçoit aucun ping s'il n'y a pas au moins un listener
            log.warn("Pas de pingListener");
        }
        if (pingListeners.size() > 1 && log.isWarnEnabled()) {
            // Ça peut arriver, mais la taille doit rester basse
            log.warn("Plus d'1 pingListener enregistré. Si ça persiste ou si le nombre augmente, il faut s'inquiéter : " + pingListeners.size());
        }
        for (Consumer<String> pingListener : pingListeners) {
            pingListener.accept(pingId);
        }
    }

    public void watchPing() {
        jmsUrl.ifPresent(url -> registerSelfMessageWatcher(url, PING_TOPIC, this::pingReceived));
    }

    public Future<Void> ping() {
        if (jmsUrl.isPresent()) {
            final String pingContent = UUID.randomUUID().toString();
            final CompletableFuture<Void> result = new CompletableFuture<>();

            // On prépare le listener et on l'ajoute au set
            Consumer<String> listener = (pingId) -> {
                if (pingContent.equals(pingId)) {
                    result.complete(null);
                }
            };
            pingListeners.add(listener);

            // Que le CompletableFuture soit `complete` ou `cancel`, la méthode whenComplete est appelée
            result.whenComplete((a,b) -> pingListeners.remove(listener));

            // On envoie le message
            emitMessage(jmsUrl.get(), PING_TOPIC, pingContent);

            // Si la file JMS n'est pas dispo, on doit annuler l'attente (au bout de 5s), sans quoi ça va tourner éternellement
            Runnable cancelPing = () -> {
                boolean wasRunning = result.cancel(false);
                if (wasRunning && log.isWarnEnabled()) {
                    log.warn("ping interrompu");
                }
            };
            jmsPingCancelExecutor.schedule(cancelPing, 5, TimeUnit.SECONDS);

            return result;
        } else {
            // Pas de JMS configuré, donc on le ping n'a pas de sens
            return Futures.immediateCancelledFuture();
        }
    }

}
