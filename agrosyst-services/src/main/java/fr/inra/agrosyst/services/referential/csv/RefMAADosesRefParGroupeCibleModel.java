package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCibleImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class RefMAADosesRefParGroupeCibleModel extends AbstractAgrosystModel<RefMAADosesRefParGroupeCible> implements ExportModel<RefMAADosesRefParGroupeCible> {

    public RefMAADosesRefParGroupeCibleModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("code_amm", RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM, STRING_MANDATORY_PARSER);
        newMandatoryColumn("type_dose", RefMAADosesRefParGroupeCible.PROPERTY_TYPE_DOSE, TYPE_DOSE_PARSER);
        newMandatoryColumn("code_culture_maa", RefMAADosesRefParGroupeCible.PROPERTY_CODE_CULTURE_MAA, STRING_MANDATORY_PARSER);
        newMandatoryColumn("culture_maa", RefMAADosesRefParGroupeCible.PROPERTY_CULTURE_MAA);
        newMandatoryColumn("traitement_maa", RefMAADosesRefParGroupeCible.PROPERTY_TRAITEMENT_MAA);
        newMandatoryColumn("code_groupe_cible_maa", RefMAADosesRefParGroupeCible.PROPERTY_CODE_GROUPE_CIBLE_MAA, STRING_MANDATORY_PARSER);
        newMandatoryColumn("groupe_cible_maa", RefMAADosesRefParGroupeCible.PROPERTY_GROUPE_CIBLE_MAA);
        newMandatoryColumn("campagne", RefMAADosesRefParGroupeCible.PROPERTY_CAMPAGNE, INT_MANDATORY_PARSER);
        newMandatoryColumn("dose_ref_maa", RefMAADosesRefParGroupeCible.PROPERTY_DOSE_REF_MAA, DOUBLE_PARSER);
        newMandatoryColumn("unit_dose_ref_maa", RefMAADosesRefParGroupeCible.PROPERTY_UNIT_DOSE_REF_MAA, PHYTO_PRODUCT_UNIT_PARSER);
        newMandatoryColumn("code_traitement_maa", RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA, STRING_MANDATORY_PARSER);
        newMandatoryColumn("volume_max_bouillie", RefMAADosesRefParGroupeCible.PROPERTY_VOLUME_MAX_BOUILLIE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("date_correction_maa", RefMAADosesRefParGroupeCible.PROPERTY_DATE_CORRECTION_MAA, LOCAL_DATE_PARSER);
        newMandatoryColumn("description_correction_maa", RefMAADosesRefParGroupeCible.PROPERTY_DESCRIPTION_CORRECTION_MAA);
        newOptionalColumn(COLUMN_ACTIVE, RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefMAADosesRefParGroupeCible, Object>> getColumnsForExport() {
        ModelBuilder<RefMAADosesRefParGroupeCible> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("code_amm", RefMAADosesRefParGroupeCible.PROPERTY_CODE_AMM);
        modelBuilder.newColumnForExport("type_dose", RefMAADosesRefParGroupeCible.PROPERTY_TYPE_DOSE, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("code_culture_maa", RefMAADosesRefParGroupeCible.PROPERTY_CODE_CULTURE_MAA);
        modelBuilder.newColumnForExport("culture_maa", RefMAADosesRefParGroupeCible.PROPERTY_CULTURE_MAA);
        modelBuilder.newColumnForExport("traitement_maa", RefMAADosesRefParGroupeCible.PROPERTY_TRAITEMENT_MAA);
        modelBuilder.newColumnForExport("code_groupe_cible_maa", RefMAADosesRefParGroupeCible.PROPERTY_CODE_GROUPE_CIBLE_MAA);
        modelBuilder.newColumnForExport("groupe_cible_maa", RefMAADosesRefParGroupeCible.PROPERTY_GROUPE_CIBLE_MAA);
        modelBuilder.newColumnForExport("campagne", RefMAADosesRefParGroupeCible.PROPERTY_CAMPAGNE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("dose_ref_maa", RefMAADosesRefParGroupeCible.PROPERTY_DOSE_REF_MAA, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("unit_dose_ref_maa", RefMAADosesRefParGroupeCible.PROPERTY_UNIT_DOSE_REF_MAA, GENERIC_ENUM_FORMATTER);
        modelBuilder.newColumnForExport("code_traitement_maa", RefMAADosesRefParGroupeCible.PROPERTY_CODE_TRAITEMENT_MAA);
        modelBuilder.newColumnForExport("volume_max_bouillie", RefMAADosesRefParGroupeCible.PROPERTY_VOLUME_MAX_BOUILLIE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("date_correction_maa", RefMAADosesRefParGroupeCible.PROPERTY_DATE_CORRECTION_MAA, LOCAL_DATE_FORMATTER);
        modelBuilder.newColumnForExport("description_correction_maa", RefMAADosesRefParGroupeCible.PROPERTY_DESCRIPTION_CORRECTION_MAA);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefMAADosesRefParGroupeCible.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefMAADosesRefParGroupeCible newEmptyInstance() {
        RefMAADosesRefParGroupeCible refMAADosesRefParGroupeCible = new RefMAADosesRefParGroupeCibleImpl();
        refMAADosesRefParGroupeCible.setActive(true);
        return refMAADosesRefParGroupeCible;
    }
}
