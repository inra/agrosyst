package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2017 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.edaplos.EdaplosConstants;
import org.apache.commons.lang3.EnumUtils;

public class TechnicalCharacteristic implements AgroEdiObject {

    public static final String TYPE_CODE_LIMESTONE = "ABG02";
    public static final String TYPE_CODE_STRUCTURE = "ABG03";
    public static final String TYPE_CODE_STONINESS = "ABG04";
    public static final String TYPE_CODE_DEPTH = "ZD2";
    public static final String TYPE_CODE_WATER_LOGGING = "ZD3";
    public static final String TYPE_CODE_VOLUME_BOUILLIE = "T37";
    public static final String TYPE_CODE_DEBIT_CHANTIER = "AAA38";
    // AAA13: import des récoltes -> Caractéristiques techniques du produit récolté
    public static final String TYPE_CODE_PRODUIT_RECOLTE = "AAA13";
    public static final String TYPE_CODE_TYPE_SOL_ARVALIS = "ZD1";
    // ZJ9: import des récoltes -> critère de qualité taux de Matières Sèches Spec Spec_Récolte_EDI_to_Agrosyst_v1-4
    public static final String SUB_TYPE_CODE_TAUX_MS = "ZJ9";

    public static final String AGROSYST_QUALITY_CRITERIA_HUMIDITY = "GCPE_00001";

    /** Type de caractéristique (http://agroedieurope.fr/fr/referentiels/34.html). */
    protected String typeCode;

    /** code de la caractéristique (http://agroedieurope.fr/fr/referentiels/2.html). */
    protected String subordinateTypeCode;

    /** Mesure de la caractéristique. L’unité sera à piocher dans (http://agroedieurope.fr/fr/referentiels/36.html) */
    protected String valueMeasure;

    protected String valueMeasureUnitCode;

    protected String description;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getSubordinateTypeCode() {
        return subordinateTypeCode;
    }

    public void setSubordinateTypeCode(String subordinateTypeCode) {
        this.subordinateTypeCode = subordinateTypeCode;
    }

    public String getValueMeasure() {
        return valueMeasure;
    }

    public void setValueMeasure(String valueMeasure) {
        this.valueMeasure = valueMeasure;
    }

    public EdaplosConstants.UnitCode getValueMeasureUnitCode() {
        return EnumUtils.getEnum(EdaplosConstants.UnitCode.class, valueMeasureUnitCode);
    }

    public void setValueMeasureUnitCode(String valueMeasureUnitCode) {
        this.valueMeasureUnitCode = valueMeasureUnitCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "caractéristique '" + typeCode + "'";
    }
}
