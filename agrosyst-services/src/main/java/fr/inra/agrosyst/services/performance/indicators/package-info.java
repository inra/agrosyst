/**
 * Mode de calcul des indicateurs:
 *
 * <ol>
 * <li>Indicateurs de pression d’utilisation des intrants
 *  <ol>
 *   <li>Indice de Fréquence de Traitement (IFT) et ses déclinaisons</li>
 *   <li>Quantité d’eau apporté (Qeau)</li>
 *   <li>Consommation de carburant</li>
 *   <li>Pressions N, P2O5, K2O</li>
 *  </ol>
 * </li>
 * <li>Indicateurs de résultats socio-techniques
 *  <ol>
 *   <li>Surface par UTH</li>
 *   <li>Temps de travail</li>
 *   <li>Nombre de passages</li>
 *  </ol>
 * </li>
 * <li>Indicateurs de résultats agronomiques
 *  <ol>
 *   <li>Rendement</li>
 *   <li>Balances N, P2O5, K2O</li>
 *   <li>Production d’énergie brute</li>
 *  </ol>
 * </li>
 * <li>Indicateurs de performances technico-économiques
 *  <ol>
 *   <li>Produit brut (PB)</li>
 *   <li>Charges opérationnelles (CO)</li>
 *   <li>Marge brute (MB)</li>
 *   <li>Efficience économique des intrants</li>
 *   <li>Charges de mécanisation (CM)</li>
 *   <li>Charges salariales (CS)</li>
 *   <li>Marge directe (MD)</li>
 *  </ol>
 * </li>
 * <li>Indicateurs de performances environnementales
 *  <ol>
 *   <li>Risque de perte de pesticides dans les eaux profondes, dans les eaux de surface et dans l’air (I-Phy)</li>
 *   <li>Consommation d’énergie (CE)</li>
 *   <li>Efficience énergétique (EEN)</li>
 *   <li>Emissions de gaz à effet de serre (GES)</li>
 *  </ol>
 * </li>
 * </ol>
 * @author Cossé David : cosse@codelutin.com
 */
package fr.inra.agrosyst.services.performance.indicators;
/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
