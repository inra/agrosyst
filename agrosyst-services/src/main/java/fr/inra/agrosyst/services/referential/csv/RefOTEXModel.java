package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOTEXImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefOTEXModel extends AbstractAgrosystModel<RefOTEX> implements ExportModel<RefOTEX> {

    public RefOTEXModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Code OTEX 18 postes", RefOTEX.PROPERTY_CODE__OTEX_18_POSTES, INT_PARSER);
        newMandatoryColumn("libelle OTEX 18 postes", RefOTEX.PROPERTY_LIBELLE__OTEX_18_POSTES);
        newMandatoryColumn("code OTEX 70 postes", RefOTEX.PROPERTY_CODE__OTEX_70_POSTES, INT_PARSER);
        newMandatoryColumn("libelle OTEX 70 postes", RefOTEX.PROPERTY_LIBELLE__OTEX_70_POSTES);
        newMandatoryColumn("Source", RefOTEX.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefOTEX.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefOTEX, Object>> getColumnsForExport() {
        ModelBuilder<RefOTEX> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Code OTEX 18 postes", RefOTEX.PROPERTY_CODE__OTEX_18_POSTES, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("libelle OTEX 18 postes", RefOTEX.PROPERTY_LIBELLE__OTEX_18_POSTES);
        modelBuilder.newColumnForExport("code OTEX 70 postes", RefOTEX.PROPERTY_CODE__OTEX_70_POSTES, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("libelle OTEX 70 postes", RefOTEX.PROPERTY_LIBELLE__OTEX_70_POSTES);
        modelBuilder.newColumnForExport("Source", RefOTEX.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefOTEX.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefOTEX newEmptyInstance() {
        RefOTEXImpl result = new RefOTEXImpl();
        result.setActive(true);
        return result;
    }
}
