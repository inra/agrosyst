package fr.inra.agrosyst.services.users;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserImpl;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;

/**
 * Modele d'import des utilisateurs.
 * 
 * <ul>
 * <li>Nom
 * <li>Prenom
 * <li>Mail
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class UserImportModel extends AbstractAgrosystModel<AgrosystUser> {

    public UserImportModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("email", AgrosystUser.PROPERTY_EMAIL);
        newMandatoryColumn("prenom", AgrosystUser.PROPERTY_FIRST_NAME);
        newMandatoryColumn("nom", AgrosystUser.PROPERTY_LAST_NAME);
        newMandatoryColumn("organisation", AgrosystUser.PROPERTY_ORGANISATION);
        newMandatoryColumn("tele", AgrosystUser.PROPERTY_PHONE_NUMBER);
    }

    @Override
    public AgrosystUser newEmptyInstance() {
        return new AgrosystUserImpl();
    }
}
