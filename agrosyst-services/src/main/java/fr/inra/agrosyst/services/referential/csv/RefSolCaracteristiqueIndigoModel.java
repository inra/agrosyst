package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigoImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel sol texture GEPPA:
 * 
 * <ul>
 * <li>abbreviation INDIGO
 * <li>classe INDIGO
 * <li>Classe de profondeur INDIGO
 * <li>Réserve Utile (mm)
 * <li>Source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class RefSolCaracteristiqueIndigoModel extends AbstractAgrosystModel<RefSolCaracteristiqueIndigo> implements ExportModel<RefSolCaracteristiqueIndigo> {

    public RefSolCaracteristiqueIndigoModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("abbreviation INDIGO", RefSolCaracteristiqueIndigo.PROPERTY_ABBREVIATION__INDIGO);
        newMandatoryColumn("classe INDIGO", RefSolCaracteristiqueIndigo.PROPERTY_CLASSE__INDIGO);
        newMandatoryColumn("Classe de profondeur INDIGO", RefSolCaracteristiqueIndigo.PROPERTY_CLASSE_DE_PROFONDEUR__INDIGO);
        newMandatoryColumn("Réserve Utile (mm)", RefSolCaracteristiqueIndigo.PROPERTY_RESERVE_UTILE, INT_PARSER);
        newMandatoryColumn("Source", RefSolCaracteristiqueIndigo.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefSolCaracteristiqueIndigo.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefSolCaracteristiqueIndigo, Object>> getColumnsForExport() {
        ModelBuilder<RefSolCaracteristiqueIndigo> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("abbreviation INDIGO", RefSolCaracteristiqueIndigo.PROPERTY_ABBREVIATION__INDIGO);
        modelBuilder.newColumnForExport("classe INDIGO", RefSolCaracteristiqueIndigo.PROPERTY_CLASSE__INDIGO);
        modelBuilder.newColumnForExport("Classe de profondeur INDIGO", RefSolCaracteristiqueIndigo.PROPERTY_CLASSE_DE_PROFONDEUR__INDIGO);
        modelBuilder.newColumnForExport("Réserve Utile (mm)", RefSolCaracteristiqueIndigo.PROPERTY_RESERVE_UTILE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefSolCaracteristiqueIndigo.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefSolCaracteristiqueIndigo.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefSolCaracteristiqueIndigo newEmptyInstance() {
        RefSolCaracteristiqueIndigo refSolCaracteristiqueIndigo = new RefSolCaracteristiqueIndigoImpl();
        refSolCaracteristiqueIndigo.setActive(true);
        return refSolCaracteristiqueIndigo;
    }
}
