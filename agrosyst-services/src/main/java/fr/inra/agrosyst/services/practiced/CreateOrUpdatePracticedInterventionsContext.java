package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedInterventionDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 26/04/16.
 */
@Getter
@Setter
public class CreateOrUpdatePracticedInterventionsContext {

    private final Map<String, CroppingPlanEntry> croppingPlanEntryMap;
    private final Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap;
    protected PracticedSystem practicedSystem;
    protected String practicedSystemCampaigns;
    protected Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds;
    protected List<PracticedIntervention> interventions;
    protected PracticedCropCycleConnection connection;
    protected boolean intermediateCrop;
    protected String domainCode;
    protected PracticedCropCycleConnectionDto connectionDto;
    protected PracticedCropCyclePhase phase;

    protected List<PracticedInterventionDto> interventionDtos;

    // For Valorisation validation
    protected Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    protected Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant;

    protected CreateOrUpdatePracticedInterventionsContext(
            PracticedSystem practicedSystem,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            List<PracticedInterventionDto> interventionDtos,
            List<PracticedIntervention> interventions,
            PracticedCropCycleConnection connection,
            PracticedCropCycleConnectionDto connectionDto,
            boolean intermediateCrop,
            String domainCode,
            PracticedCropCyclePhase phase,
            String practicedSystemCampaigns,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, CroppingPlanEntry> croppingPlanEntryMap,
            Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap) {
        this.practicedSystem = practicedSystem;
        this.domainInputStockUnitByIds = domainInputStockUnitByIds;
        this.interventionDtos = interventionDtos;
        this.interventions = interventions;
        this.connection = connection;
        this.connectionDto = connectionDto;
        this.intermediateCrop = intermediateCrop;
        this.domainCode = domainCode;
        this.phase = phase;
        this.practicedSystemCampaigns = practicedSystemCampaigns;
        this.speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        this.sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant;
        this.croppingPlanEntryMap = croppingPlanEntryMap;
        this.croppingPlanSpeciesMap = croppingPlanSpeciesMap;
    }

    static CreateOrUpdatePracticedInterventionsContext createOrUpdateSeasonalPracticedInterventionsContext(
            PracticedSystem practicedSystem,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            List<PracticedIntervention> interventions,
            PracticedCropCycleConnection connection, PracticedCropCycleConnectionDto connectionDto,
            String domainCode, String practicedSystemCampaigns,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, CroppingPlanEntry> croppingPlanEntryMap,
            Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap) {

        boolean intermediateCrop = !Strings.isNullOrEmpty(connectionDto.getIntermediateCroppingPlanEntryCode());
        CreateOrUpdatePracticedInterventionsContext result = new
                CreateOrUpdatePracticedInterventionsContext(
                practicedSystem,
                domainInputStockUnitByIds,
                connectionDto.getInterventions(),
                interventions,
                connection,
                connectionDto,
                intermediateCrop,
                domainCode,
                null,
                practicedSystemCampaigns,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                croppingPlanEntryMap,
                croppingPlanSpeciesMap);
        return result;
    }

    static CreateOrUpdatePracticedInterventionsContext createOrUpdatePerennialPracticedInterventionsContext(
            PracticedSystem practicedSystem,
            Map<String, AbstractDomainInputStockUnit> domainInputStockUnitByIds,
            List<PracticedInterventionDto> interventionDtos,
            List<PracticedIntervention> interventions,
            String domainCode,
            PracticedCropCyclePhase phase,
            String practicedSystemCampaigns,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, CroppingPlanEntry> croppingPlanEntryMap,
            Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap) {

        return new CreateOrUpdatePracticedInterventionsContext(
                practicedSystem,
                domainInputStockUnitByIds,
                interventionDtos,
                interventions,
                null,
                null,
                false,
                domainCode,
                phase,
                practicedSystemCampaigns,
                speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
                sectorByCodeEspceBotaniqueCodeQualifiant,
                croppingPlanEntryMap,
                croppingPlanSpeciesMap);
    }
    
}
