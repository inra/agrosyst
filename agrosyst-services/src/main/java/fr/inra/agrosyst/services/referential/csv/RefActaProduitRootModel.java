package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRootImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RefActaProduitRootModel extends InternationalizationReferentialModel<RefActaProduitRoot> implements ExportModel<RefActaProduitRoot> {

    // for import
    public RefActaProduitRootModel(RefCountryTopiaDao refCountryDao) {
        super(CSV_SEPARATOR);
        
        try(final Stream<RefCountry> refCountryStream = refCountryDao.streamAll()){
            refCountryByTrigram = refCountryStream.collect(
                    Collectors.toMap(refCountry -> refCountry.getTrigram().toLowerCase(), Function.identity()));
        }
        
        newMandatoryColumn("pays", RefActaProduitRoot.PROPERTY_REF_COUNTRY, TRIGRAM_PARSER);
        newMandatoryColumn("idProduit", RefActaProduitRoot.PROPERTY_ID_PRODUIT, EMPTY_TO_NULL);
        newMandatoryColumn("nomProduit", RefActaProduitRoot.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("amm", RefActaProduitRoot.PROPERTY_AMM, EMPTY_TO_NULL);
        newMandatoryColumn("ammUE", RefActaProduitRoot.PROPERTY_AMM_UE, EMPTY_TO_NULL);
        newMandatoryColumn("permis", RefActaProduitRoot.PROPERTY_PERMIS, EMPTY_TO_NULL);
        newOptionalColumn(COLUMN_ACTIVE, RefActaProduitRoot.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("status");
    }
    
    // for export
    public RefActaProduitRootModel() {
        super(CSV_SEPARATOR);
    }
    
    @Override
    public Iterable<ExportableColumn<RefActaProduitRoot, Object>> getColumnsForExport() {
        ModelBuilder<RefActaProduitRoot> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("pays", RefActaProduitRoot.PROPERTY_REF_COUNTRY, TRIGRAM_FORMATTER);
        modelBuilder.newColumnForExport("idProduit", RefActaProduitRoot.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("nomProduit", RefActaProduitRoot.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("amm", RefActaProduitRoot.PROPERTY_AMM);
        modelBuilder.newColumnForExport("ammUE", RefActaProduitRoot.PROPERTY_AMM_UE);
        modelBuilder.newColumnForExport("permis", RefActaProduitRoot.PROPERTY_PERMIS);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaProduitRoot.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }


    @Override
    public RefActaProduitRoot newEmptyInstance() {
        RefActaProduitRoot refActaProduitRoot = new RefActaProduitRootImpl();
        refActaProduitRoot.setActive(true);
        return refActaProduitRoot;
    }
}
