package fr.inra.agrosyst.services.performance.indicators.ift;

/*
 * #%L
 * Agrosyst :: Services
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PhytoProductTarget;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.UsagePerformanceResult;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * IFT à la cible millésimé
 * Pour chaque intrant de type ‘Phytosanitaire’
 * • L’IFT à la cible millésimé ne peut théoriquement pas être calculé pour les campagnes antérieures à 2015. Donc si l’intervention est rattachée à un Domaine d’une campagne (strictement) antérieure à 2015, l’IFT à la cible millésimé récupèrera une dose de référence de 2015 ou plus tard.
 * • Repérer les espèces cultivées de la culture concernée (le plus souvent il n’y en a qu’une, mais il peut y en avoir plusieurs)
 * • Sur la base de la table de correspondance Correspondance_CulturesAgrosyst_CulturesMAA, établir la liste des espèces MAA.
 * Note David: Il doit s'agir du référentiel RefEspece, colonne "code_culture_maa"
 * • Repérer les cibles de l’intrant (0, 1 ou plusieurs cibles)
 * • Sur la base de la table Correspondance_CiblesAgrosyst_GroupesCiblesMAA (RefCiblesAgrosystGroupesCiblesMAA), établir la liste des GroupesCiblesMAA (code_groupe_cible_maa)
 * • Dans le référentiel MAA_Doses_Ref_par_GroupeCibles (RefMAADosesRefParGroupeCible), Repérer la Dose_Ref correspondant au quintuplet Campagne|code_AMM|CultureMAA|GroupesCiblesMAA|Traitement_MAA|
 * Campagne correspond à la campagne rattachée au domaine
 * o S’il y a plusieurs valeurs parce que plusieurs cibles (et donc plusieurs GroupeCibleMAA), retenir la dose de référence la plus élevée comme dose de référence (Dose_REF) pour ce traitement
 * o S’il n’y a pas de cible déclarée (et donc pas de GroupeCiblesMAA), retenir comme dose de référence la plus petite des doses de référence de la table MAA_Doses_Ref_par_GroupeCibles pour le quadruplet Campagne|code_AMM|CultureMAA| Traitement_MAA |
 * o Si on ne trouve pas le quintuplet Campagne|code_AMM|CultureMAA|GroupesCiblesMAA|Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles, alors rechercher si on trouve le quadruplet Campagne|code_AMM|CultureMAA| Traitement_MAA|, et retenir comme Dose_Ref la plus petite des valeurs trouvées.
 * o Si on ne trouve pas le quadruplet Campagne|code_AMM|CultureMAA| Traitement_MAA | Unité de dose. dans MAA_Doses_Ref_par_GroupeCibles, alors : reproduire cette dernière étape sur les campagnes précédentes, puis suivantes
 * o Si on ne trouve pas de résultat, alors alors rechercher dans ACTA_Dosage_SPC_Complet selon le mécanisme présent actuellement, à savoir id_produit*id_traitement*(id_culture+id_culture des groupes culture), en retenant la plus petite valeur comme Dose_REF.
 * o Si on ne trouve pas de résultat, IFT_Cible_Millesimé = PSCI_PHYTO * 1
 * (soit : DOSE_REF = DOSE appliquée)
 * • Si le produit appliqué est identifié comme Biocontrôle = ‘O’ dans la table MAA_Biocontrôle pour la campagne de la dose de référence remontée, alors tous les IFT ift_Cible_Millesimé_total, ift_Cible_Millesimé_tot_hts, ift_Cible_Millesimé_h, ift_Cible_Millesimé_f, ift_Cible_Millesimé_i, ift_Cible_Millesimé_ts, ift_Cible_Millesimé_a, ift_Cible_Millesimé_hh,ci-dessus prennent la valeur 0, et l’IFT_Cible calculé alimente l’IFT_Cible_Biocontrôle, (comme pour le calcul de l’IFT « à l’ancienne »).
 *
 * @author dcosse
 */
@Setter
public class IndicatorVintageTargetIFT extends IndicatorRefMaxYearTargetIFT {

    private static final Log LOGGER = LogFactory.getLog(IndicatorVintageTargetIFT.class);

    protected static final int MINIMAL_CAMPAIGN = 2015;
    /**
     * the corresponding indicator fields
     */
    protected static final String[] FIELDS = new String[]{
            "ift_a_la_cible_mil_ift_chimique_total",             // 0
            "ift_a_la_cible_mil_ift_chimique_tot_hts",           // 1
            "ift_a_la_cible_mil_ift_h",                          // 2
            "ift_a_la_cible_mil_ift_f",                          // 3
            "ift_a_la_cible_mil_ift_i",                          // 4
            "ift_a_la_cible_mil_ift_ts",                         // 5
            "ift_a_la_cible_mil_ift_a",                          // 6
            "ift_a_la_cible_mil_ift_hh",                         // 7
            "ift_a_la_cible_mil_ift_biocontrole"                 // 8
    };

    protected static final String[] LABELS = new String[]{
            "Indicator.label.ift_a_la_cible_mil_ift_chimique_total",            // 0 IFT phytosanitaire chimique (ex total, refs #10576)
            "Indicator.label.ift_a_la_cible_mil_ift_chimique_tot_hts",          // 1 IFT phytosanitaire chimique total hors traitement de semence
            "Indicator.label.ift_a_la_cible_mil_ift_h",                         // 2 IFT herbicide
            "Indicator.label.ift_a_la_cible_mil_ift_f",                         // 3 IFT fongicide
            "Indicator.label.ift_a_la_cible_mil_ift_i",                         // 4 IFT insecticide
            "Indicator.label.ift_a_la_cible_mil_ift_ts",                        // 5 IFT traitement de semence
            "Indicator.label.ift_a_la_cible_mil_ift_a",                         // 6 IFT phytosanitaire autres
            "Indicator.label.ift_a_la_cible_mil_ift_hh",                        // 7 IFT phytosanitaire hors herbicide
            "Indicator.label.ift_a_la_cible_mil_ift_biocontrole"                // 8 IFT vert
    };

    /**
     * Constructeur initialisant la map des champs optionnels
     */
    public IndicatorVintageTargetIFT() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "ift_a_la_cible_mil_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "ift_a_la_cible_mil_detail_champs_non_renseig");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE, "ift_a_la_cible_mil_dose_reference");
        extraFields.put(OptionalExtraColumnEnumKey.DOSE_REFERENCE_UNIT, "ift_a_la_cible_mil_dose_reference_unite");
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_VINTAGE_TARGET_IFT);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {

        HashMap<String, String> indicatorNameToColumnName = new HashMap<>();
        for (Map.Entry<OptionalExtraColumnEnumKey, String> extraField : extraFields.entrySet()) {
            indicatorNameToColumnName.put(indicatorName + "_" + extraField.getKey(), extraField.getValue());
        }

        for (int i = 0; i < LABELS.length; i++) {
            String indicatorName = getIndicatorLabel(i);
            String columnName = FIELDS[i];
            indicatorNameToColumnName.put(indicatorName, columnName);
        }
        return indicatorNameToColumnName;
    }

    @Override
    protected InputIftResult computeIft(
            ComputeInputIftContext computeContext,
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        addVintageTargetIftIfAvailable(
                computeContext.getPhytoProductInputUsage(),
                computeContext,
                doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);

        return super.computeIft(computeContext, doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
    }

    protected void addVintageTargetIftIfAvailable(
            AbstractPhytoProductInputUsage usage,
            ComputeInputIftContext context,
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        Map<AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages = context.getRefVintageTargetIftDoseByUsages();
        UsagePerformanceResult refVintageTargetIftDose = refVintageTargetIftDoseByUsages.get(usage);

        if (refVintageTargetIftDose == null) {
            int domainCampaign = context.getDomain().getCampaign();
            Optional<RefMAADosesRefParGroupeCible> refMAADosesRefParGroupeCible = getVintageRefMAADosesRefParGroupeCible(
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                    usage,
                    domainCampaign);

            UsagePerformanceResult usagePerformanceResult = getUsagePerformanceResult(
                    usage,
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                    refMAADosesRefParGroupeCible,
                    domainCampaign);

            refVintageTargetIftDoseByUsages.put(usage, usagePerformanceResult);
        }
    }

    /**
     * Correspondant au quintuplet Campagne|code_AMM|CultureMAA|GroupesCiblesMAA|traitementMAA,
     * où Campagne est la valeur la plus élevée du champ ‘Campagne’ de la table MAA_Doses_Ref_par_GroupeCibles
     *
     * @param domainCampaign est null pour l'ift à la cible non millésimé
     */
    @Override
    protected Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCible(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            int domainCampaign) {

        Optional<RefMAADosesRefParGroupeCible> refMAADosesRefParGroupeCible = getVintageRefMAADosesRefParGroupeCible(
                doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                phytoProductInputUsage,
                domainCampaign);

        return refMAADosesRefParGroupeCible;
    }


    @Override
    protected Optional<RefMAADosesRefParGroupeCible> getVintageRefMAADosesRefParGroupeCible(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            AbstractPhytoProductInputUsage phytoProductInputUsage,
            int domainCampaign) {

        Set<String> bioAgressorIdentifiantOrReference_id = null;
        Set<String> groupesCibles = null;
        Collection<PhytoProductTarget> targets = phytoProductInputUsage.getTargets();
        if (CollectionUtils.isNotEmpty(targets)) {
            groupesCibles = targets.stream()
                    .map(PhytoProductTarget::getCodeGroupeCibleMaa)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            bioAgressorIdentifiantOrReference_id = getTargetIdsFromAdventicesAndNuisibleEdis(targets);
        }

        return computeVitageTargetIft(
                doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
                domainCampaign,
                bioAgressorIdentifiantOrReference_id,
                groupesCibles);
    }

    public Optional<RefMAADosesRefParGroupeCible> computeVitageTargetIft(
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA,
            int domainCampaign,
            Set<String> bioAgressorIdentifiantOrReference_id,
            Set<String> groupesCibles) {

        Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget =
                groupesCiblesMaaCodesAndGeneric -> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA;

        Collection<RefMAADosesRefParGroupeCible> availableDoseRefs = new HashSet<>();
        if (CollectionUtils.isNotEmpty(bioAgressorIdentifiantOrReference_id)) {
            for (String bioAgressorEdi : bioAgressorIdentifiantOrReference_id) {
                Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = getRefMAADosesRefForTarget(
                        doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
                        RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID,
                        bioAgressorEdi,
                        domainCampaign);
                optionalDoseRef.ifPresent(availableDoseRefs::add);
            }
        }

        if (CollectionUtils.isNotEmpty(groupesCibles)) {
            for (String groupeCible : groupesCibles) {
                Optional<RefMAADosesRefParGroupeCible> optionalDoseRef = getRefMAADosesRefForTarget(
                        doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
                        RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CODE_GROUPE_CIBLE_MAA,
                        groupeCible,
                        domainCampaign);
                optionalDoseRef.ifPresent(availableDoseRefs::add);
            }
        }

        Optional<RefMAADosesRefParGroupeCible> refMAADosesRefParGroupeCible;

        if (!availableDoseRefs.isEmpty()) {
            refMAADosesRefParGroupeCible = availableDoseRefs.stream()
                    .max(Comparator.comparing(d -> Optional.ofNullable(d.getDose_ref_maa()).orElse(-Double.MAX_VALUE)));

        } else {
            refMAADosesRefParGroupeCible = getRefMAADosesRefForTarget(
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
                    RefCiblesAgrosystGroupesCiblesMAA.PROPERTY_CIBLE_EDI_REF_ID,
                    null,
                    domainCampaign);
        }
        return refMAADosesRefParGroupeCible;
    }

    @Override
    protected UsagePerformanceResult getIndicatorReferenceDoseDTO(
            AbstractPhytoProductInputUsage usage,
            ComputeInputIftContext context,
            List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        // previously computed
        Map<AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages = context.getRefVintageTargetIftDoseByUsages();
        UsagePerformanceResult refVintageTargetIftDose = refVintageTargetIftDoseByUsages.get(usage);

        return refVintageTargetIftDose;
    }

    @Override
    protected Integer getStudiedCampaign(ComputeInputIftContext computeContext, List<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {
        return computeContext.getDomain().getCampaign();
    }

    @Override
    protected void addAllTargetedIftRefDosageForPhytoUsages(PerformanceInterventionContext interventionContext, Map<AbstractPhytoProductInputUsage, UsagePerformanceResult> refDoseByUsages) {
        // Affectation de la dose de référence IFT à la cible millésimé
        interventionContext.addAllTargetedIftRefDosageForPhytoUsages(refDoseByUsages);
    }

    protected Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefForTarget(
            Function<Map<String, Boolean>, Collection<RefMAADosesRefParGroupeCible>> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget,
            String property,
            String bioAgressorEdiIdsOrGroupesCible,
            Integer domainCampaign) {

        final Map<String, Boolean> groupesCiblesMaaCodesAndGeneric = referentialService.getGroupesCiblesCodesAndGeneric(property, bioAgressorEdiIdsOrGroupesCible);

        // filtre les doses CodeAMM | CultureMAA | TraitementMAA | SupplierByTarget
        Collection<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA_SupplierByTarget.apply(groupesCiblesMaaCodesAndGeneric);

        if (CollectionUtils.isEmpty(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA)) return Optional.empty();

        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef;

        boolean isTargetPresent = MapUtils.isNotEmpty(groupesCiblesMaaCodesAndGeneric);

        if (isTargetPresent) {
            optionalDoseRef = getRefMAADosesRefParGroupeCible(
                    domainCampaign,
                    groupesCiblesMaaCodesAndGeneric,
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);

            // Si on ne trouve pas le quadruplet
            // Campagne|code_AMM|CultureMAA|Traitement_MAA.
            // alors : reproduire cette dernière étape sur les campagnes précédentes, puis suivantes
            int failDownMinCampaign = domainCampaign - 1;
            while (optionalDoseRef.isEmpty() && failDownMinCampaign >= MINIMAL_CAMPAIGN) {
                int finalFailDownMinCampaign = failDownMinCampaign;
                optionalDoseRef = getRefMAADosesRefParGroupeCible(
                        finalFailDownMinCampaign,
                        groupesCiblesMaaCodesAndGeneric,
                        doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
                failDownMinCampaign--;
            }

            if (optionalDoseRef.isEmpty()) {
                final Integer MAX_CAMPAIGN = ReferentialService.getRefDoseMaxCampaign(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
                int failDownMaxCampaign = domainCampaign + 1;
                while (optionalDoseRef.isEmpty() && failDownMaxCampaign <= MAX_CAMPAIGN) {
                    int finalFailDownMaxCampaign = failDownMaxCampaign;
                    optionalDoseRef = getRefMAADosesRefParGroupeCible(
                            finalFailDownMaxCampaign,
                            groupesCiblesMaaCodesAndGeneric,
                            doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
                    failDownMaxCampaign++;
                }
            }
        } else {

            optionalDoseRef = getRefMAADosesRefParGroupeCibleForNoTargets(
                    domainCampaign,
                    doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);

            // Si on ne trouve pas le quadruplet
            // Campagne|code_AMM|CultureMAA|Traitement_MAA.
            // alors : reproduire cette dernière étape sur les campagnes précédentes, puis suivantes
            int failDownMinCampaign = domainCampaign - 1;
            while (optionalDoseRef.isEmpty() && failDownMinCampaign >= MINIMAL_CAMPAIGN) {
                int finalFailDownMinCampaign = failDownMinCampaign;
                optionalDoseRef = getRefMAADosesRefParGroupeCibleForNoTargets(
                        finalFailDownMinCampaign,
                        doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
                failDownMinCampaign--;
            }

            if (optionalDoseRef.isEmpty()) {
                final Integer MAX_CAMPAIGN = ReferentialService.getRefDoseMaxCampaign(doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
                int failDownMaxCampaign = domainCampaign + 1;
                while (optionalDoseRef.isEmpty() && failDownMaxCampaign <= MAX_CAMPAIGN) {
                    int finalFailDownMaxCampaign = failDownMaxCampaign;
                    optionalDoseRef = getRefMAADosesRefParGroupeCibleForNoTargets(
                            finalFailDownMaxCampaign,
                            doseRefsFor_CodeAMM_CultureMAA_TraitementMAA);
                    failDownMaxCampaign++;
                }
            }
        }
        return optionalDoseRef;
    }

    private @NotNull Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCible(
            Integer vintageIndicatorCampaign,
            Map<String, Boolean> groupesCiblesMaaCodesAndGeneric,
            Collection<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef;
        // S’il y a plusieurs valeurs parce que plusieurs cibles (et donc plusieurs GroupeCibleMAA),
        // retenir la dose de référence la plus élevée comme dose de référence (Dose_REF) pour ce traitement en priorisant les non génériques
        // Campagne|code_AMM|CultureMAA| Traitement_MAA dans MAA_Doses_Ref_par_GroupeCibles
        Comparator<RefMAADosesRefParGroupeCible> maxGroupCibleComparator =
                Comparator.<RefMAADosesRefParGroupeCible, Boolean>comparing(
                                dose -> BooleanUtils.toBoolean(groupesCiblesMaaCodesAndGeneric.get(dose.getCode_groupe_cible_maa()))
                        )
                        .reversed()
                        .thenComparing(d -> Optional.ofNullable(d.getDose_ref_maa()).orElse(-Double.MAX_VALUE));

        // Campagne | CodeAMM | CultureMAA | GroupesCiblesMAA | TraitementMAA
        optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                .filter(dose -> vintageIndicatorCampaign.equals(dose.getCampagne())
                        && groupesCiblesMaaCodesAndGeneric.containsKey(dose.getCode_groupe_cible_maa()))
                .max(maxGroupCibleComparator);

        // IFT à la cible millésimé
        // alors rechercher si on trouve le quadruplet
        //    Campagne|code_AMM|CultureMAA| Traitement_MAA|
        // retenir comme Dose_Ref la plus grande des valeurs trouvées en priorisant les non génériques.
        Comparator<RefMAADosesRefParGroupeCible> maxComparator = Comparator.<RefMAADosesRefParGroupeCible>comparingDouble(
                dose -> Optional.ofNullable(dose.getDose_ref_maa()).orElse(-Double.MAX_VALUE)).reversed();

        if (optionalDoseRef.isEmpty()) {
            optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                    .filter(dose -> vintageIndicatorCampaign.equals(dose.getCampagne()))
                    .max(maxComparator);
        }
        return optionalDoseRef;
    }

    protected static @NotNull Optional<RefMAADosesRefParGroupeCible> getRefMAADosesRefParGroupeCibleForNoTargets(
            Integer vintageIndicatorCampaign,
            Collection<RefMAADosesRefParGroupeCible> doseRefsFor_CodeAMM_CultureMAA_TraitementMAA) {

        Optional<RefMAADosesRefParGroupeCible> optionalDoseRef;
        // retenir la dose de référence la plus petite comme dose de référence
        Comparator<RefMAADosesRefParGroupeCible> minComparator = Comparator.<RefMAADosesRefParGroupeCible>comparingDouble(
                dose -> Optional.ofNullable(dose.getDose_ref_maa()).orElse(Double.MAX_VALUE));

        optionalDoseRef = doseRefsFor_CodeAMM_CultureMAA_TraitementMAA.stream()
                .filter(dose -> vintageIndicatorCampaign.equals(dose.getCampagne()))
                .min(minComparator);

        return optionalDoseRef;
    }

}
