package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * @author David Cossé
 */
public class RefFertiOrgaModel extends AbstractAgrosystModel<RefFertiOrga> implements ExportModel<RefFertiOrga> {

    public RefFertiOrgaModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("idtypeeffluent (PK)", RefFertiOrga.PROPERTY_IDTYPEEFFLUENT);
        newMandatoryColumn("libelle", RefFertiOrga.PROPERTY_LIBELLE);
        newMandatoryColumn("Teneur_Ferti_Orga_N_total", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__N_TOTAL, DOUBLE_PARSER);
        newMandatoryColumn("Teneur_Ferti_Orga_P", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__P, DOUBLE_PARSER);
        newMandatoryColumn("Teneur_Ferti_Orga_K", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__K, DOUBLE_PARSER);
        newMandatoryColumn("Teneur_Ferti_Orga_CA_O", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__CA_O, DOUBLE_PARSER);
        newMandatoryColumn("Teneur_Ferti_Orga_MG_O", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__MG_O, DOUBLE_PARSER);
        newMandatoryColumn("Teneur_Ferti_Orga_S", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__S, DOUBLE_PARSER);
        newMandatoryColumn("Unité_teneur_ferti_orga",RefFertiOrga.PROPERTY_UNITE_TENEUR_FERTI_ORGA,  AGROSYST_FERTI_ORGA_UNIT_PARSER);
        newMandatoryColumn("source", RefFertiOrga.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefFertiOrga.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefFertiOrga, Object>> getColumnsForExport() {
        ModelBuilder<RefFertiOrga> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("idtypeeffluent (PK)", RefFertiOrga.PROPERTY_IDTYPEEFFLUENT);
        modelBuilder.newColumnForExport("libelle", RefFertiOrga.PROPERTY_LIBELLE);
        modelBuilder.newColumnForExport("Teneur_Ferti_Orga_N_total", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__N_TOTAL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Teneur_Ferti_Orga_P", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__P, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Teneur_Ferti_Orga_K", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__K, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Teneur_Ferti_Orga_CA_O", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__CA_O, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Teneur_Ferti_Orga_MG_O", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__MG_O, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Teneur_Ferti_Orga_S", RefFertiOrga.PROPERTY_TENEUR__FERTI__ORGA__S, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité_teneur_ferti_orga", RefFertiOrga.PROPERTY_UNITE_TENEUR_FERTI_ORGA ,AGROSYST_FERTI_ORGA_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("source", RefFertiOrga.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefFertiOrga.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefFertiOrga newEmptyInstance() {
        RefFertiOrga refFertiOrga = new RefFertiOrgaImpl();
        refFertiOrga.setActive(true);
        return refFertiOrga;
    }
}
