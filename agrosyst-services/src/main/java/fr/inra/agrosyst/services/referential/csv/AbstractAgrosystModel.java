package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.BioAgressorType;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.OtherProductInputUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PotInputUnit;
import fr.inra.agrosyst.api.entities.PriceType;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.SubstrateInputUnit;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.entities.UnitType;
import fr.inra.agrosyst.api.entities.action.CapacityUnit;
import fr.inra.agrosyst.api.entities.action.FertiOrgaUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.YealdCategory;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.managementmode.CategoryObjective;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.managementmode.StrategyType;
import fr.inra.agrosyst.api.entities.referential.FeedbackCategory;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.VitesseCouv;
import fr.inra.agrosyst.api.entities.referential.refApi.TypeDose;
import fr.inra.agrosyst.api.entities.security.RoleType;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.common.EmailService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.nuiton.csv.ImportableColumn;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ValueParserFormatter;
import org.nuiton.csv.ext.AbstractImportModel;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;


public abstract class AbstractAgrosystModel<E> extends AbstractImportModel<E> {

    public static final char CSV_SEPARATOR = ';';

    protected static final String DEG_BRIX = "° Brix";
    protected static final String DEG_PURE_ALCOHOL_HA = "°d'alcool pur/ha";
    protected static final String HL_HA = "hl/ha";
    protected static final String HL_VIN_HA = "hL vin/ha";
    protected static final String HL_JUICE_HA = "hl jus/ha";
    protected static final String KG_M2 = "kg/m²";
    protected static final String KG_RAISIN_HA = "kg raisin/ha";
    protected static final String L_HA = "l/ha";
    protected static final String NB_CLUSTERS_HA = "nb régime/ha";
    protected static final String NB_HA = "nb/ha";
    protected static final String Q_HA = "q/ha";
    protected static final String Q_HA_TO_STANDARD_HUMIDITY = "q/ha (humidité ramenée à la norme)";
    protected static final String TONNE_HA = "t/ha";
    protected static final String TONNE_MS_HA = "t MS/ha";
    protected static final String TONNE_SUGAR_HA = "t sucre/ha";
    protected static final String UNITE_HA = "unité/ha";
    protected static final String UNITE_M2 = "unité/m²";

    protected static final String KG_T = "kg/T";
    protected static final String KG_M_CUB = "kg/m3";

    protected static final String COLUMN_ACTIVE = "active";

    protected static final ValueParser<MaterielType> MATERIAL_TYPE_PARSER = value -> {
        MaterielType result = null;
        if (StringUtils.isNotEmpty(value)) {
            String strValue = StringUtils.deleteWhitespace(StringUtils.stripAccents(value));
            result = (MaterielType) getGenericEnumParser(MaterielType.class, strValue);
        }
        return result;
    };

    protected static final ValueParser<YealdUnit> YEALD_UNIT_PARSER = value -> {
        YealdUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.deleteWhitespace(StringUtils.stripAccents(value));
            if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(HL_HA)))) {
                result = YealdUnit.HL_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(KG_M2)))) {
                result = YealdUnit.KG_M2;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(Q_HA)))) {
                result = YealdUnit.Q_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(TONNE_HA)))) {
                result = YealdUnit.TONNE_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(UNITE_HA)))) {
                result = YealdUnit.UNITE_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(UNITE_M2)))) {
                result = YealdUnit.UNITE_M2;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(DEG_BRIX)))) {
                result = YealdUnit.DEG_BRIX;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(DEG_PURE_ALCOHOL_HA)))) {
                result = YealdUnit.DEG_PURE_ALCOHOL_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(L_HA)))) {
                result = YealdUnit.L_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(HL_VIN_HA)))) {
                result = YealdUnit.HL_VIN_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(KG_RAISIN_HA)))) {
                result = YealdUnit.KG_RAISIN_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(NB_CLUSTERS_HA)))) {
                result = YealdUnit.NB_CLUSTERS_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(NB_HA)))) {
                result = YealdUnit.NB_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(TONNE_MS_HA)))) {
                result = YealdUnit.TONNE_MS_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(TONNE_SUGAR_HA)))) {
                result = YealdUnit.TONNE_SUGAR_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(HL_JUICE_HA)))) {
                result = YealdUnit.HL_JUICE_HA;
            } else if (strValue.equalsIgnoreCase(StringUtils.stripAccents(StringUtils.deleteWhitespace(Q_HA_TO_STANDARD_HUMIDITY)))) {
                result = YealdUnit.Q_HA_TO_STANDARD_HUMIDITY;
            } else {
                result = (YealdUnit) getGenericEnumParser(YealdUnit.class, value);
            }
        }
        return result;
    };

    protected static final ValueFormatter<YealdUnit> YEALD_UNIT_FORMATTER = value -> {
        String result = null;
        if (value != null) {
            result = switch (value) {
                case HL_HA -> HL_HA;
                case KG_M2 -> KG_M2;
                case Q_HA -> Q_HA;
                case TONNE_HA -> TONNE_HA;
                case UNITE_HA -> UNITE_HA;
                case UNITE_M2 -> UNITE_M2;
                case DEG_BRIX -> DEG_BRIX;
                case DEG_PURE_ALCOHOL_HA -> DEG_PURE_ALCOHOL_HA;
                case HL_VIN_HA -> HL_VIN_HA;
                case KG_RAISIN_HA -> KG_RAISIN_HA;
                case NB_CLUSTERS_HA -> NB_CLUSTERS_HA;
                case NB_HA -> NB_HA;
                case TONNE_MS_HA -> TONNE_MS_HA;
                case TONNE_SUGAR_HA -> TONNE_SUGAR_HA;
                case Q_HA_TO_STANDARD_HUMIDITY -> Q_HA_TO_STANDARD_HUMIDITY;
                case HL_JUICE_HA -> HL_JUICE_HA;
                default -> value.name().toLowerCase();
            };
        }
        return result;
    };

    protected static final ValueParser<SeedType> SEED_TYPE_PARSER = value -> {
        SeedType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = (SeedType) getGenericEnumParser(SeedType.class, value);
        }
        return result;
    };

    protected static final ValueParser<String> LABEL_FORMATTER = destinationLabel -> {
        String result = StringUtils.stripAccents(destinationLabel);
        result = StringUtils.upperCase(result);
        result = StringUtils.deleteWhitespace(result);
        return result;
    };

    /**
     * String to integer converter.
     */
    protected static final ValueParser<Integer> INT_PARSER = value -> {
        int result = 0;
        if (!Strings.isNullOrEmpty(value)) {
            result = parseInt(value);
        }
        return result;
    };

    /**
     * String to integer converter (null allowed).
     */
    protected static final ValueParser<Integer> INTEGER_WITH_NULL_PARSER = value -> {
        Integer result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = Integer.valueOf(value);
        }
        return result;
    };

    /**
     * String to double converter.
     * Can handle , or . as decimal separator and ' ' as thousand separator
     */
    protected static final ValueParser<Double> DOUBLE_PARSER = value -> {
        double result = 0;
        if (!value.isEmpty() && !value.contains("N/A") && !value.contains("NA")) {
            // " " est un espace insécable, pas un " "
            result = Double.parseDouble(value.replace(',', '.').replace(" ", ""));
        }
        return result;
    };

    /**
     * String to double converter (null allowed).
     * Can handle , or . as decimal separator and ' ' as thousand separator
     */
    protected static final ValueParser<Double> DOUBLE_WITH_NULL_PARSER = value -> {
        Double result = null;
        if (!value.isEmpty() && !value.contains("N/A") && !value.contains("NA")) {
            // " " est un espace insécable, pas un " "
            result = Double.valueOf(value.replace(',', '.').replace(" ", ""));
        }
        return result;
    };

    /**
     * Zero to empty string converter.
     */
    protected static final ValueParser<String> ZERO_TO_EMPTY_PARSER = value -> {
        String result = value.trim();
        result = result.replaceAll("\\s{2,}", " "); // replace multiple spaces
        if (result.equals("0")) {
            result = "";
        }
        return result;
    };

    /**
     * Date converter converter.
     */
    protected static final ValueParser<LocalDate> LOCAL_DATE_PARSER = value -> {
        LocalDate result = null;
        if (!Strings.isNullOrEmpty(value)) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            result = LocalDate.parse(value, format);
        }
        return result;
    };

    protected static final ValueFormatter<Boolean> O_N_FORMATTER = value -> value ? "O" : "N";


    protected static final ValueFormatter<Boolean> T_F_FORMATTER = value -> value ? "t" : "f";

    /**
     * Default TRUE
     */
    protected static final ValueParser<Boolean> ACTIVE_PARSER = value -> {
        Boolean active = true;
        if (!Strings.isNullOrEmpty(value)) {
            active = CommonService.BOOLEAN_PARSER.parse(value);
        }
        return active;
    };

    protected static final ValueFormatter<Integer> INTEGER_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = String.valueOf(value);
        } else {
            result = "";
        }
        return result;
    };

    public static final ValueFormatter<Double> DOUBLE_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = String.valueOf(value);
        } else {
            result = "";
        }
        return result;
    };

    public static final ValueParserFormatter<Double> DOUBLE_PARSER_FORMATTER = new ValueParserFormatter<>() {
        @Override
        public String format(Double aDouble) {
            return DOUBLE_FORMATTER.format(aDouble);
        }

        @Override
        public Double parse(String s) throws ParseException {
            return DOUBLE_PARSER.parse(s);
        }
    };

    protected static final ValueFormatter<TemporalAccessor> LOCAL_DATE_FORMATTER = value -> {
        String result;
        if (value != null) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            result = format.format(value);
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueFormatter<TemporalAccessor> OFFSET_DATE_TIME_FORMATTER = value -> {
        String result;
        if (value != null) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            result = format.format(value);
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueFormatter<AgrosystInterventionType> AGROSYST_INTERVENTION_TYPE_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = value.name().toLowerCase();
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueFormatter<BioAgressorType> AGROSYST_BIO_AGRESSOR_TYPE_FORMATTER = bioAgressorType -> {
        String result;
        if (bioAgressorType != null) {
            result = switch (bioAgressorType) {
                case ACTION_SUR_LE_METABOLISME -> "Action sur le métabolisme";
                case ADVENTICE -> "Adventice";
                case AUXILIAIRE_BIOLOGIQUE -> "Auxiliaire biologique";
                case GENERIQUE -> "Générique";
                case MALADIE -> "Maladie";
                case MALADIE_PHYSIOLOGIQUE -> "Maladie physiologique";
                case PLANTE_PARASITE -> "Plate parasite";
                case PLANTES_ENVAHISSANTES_ORIGINE_EXOTIQUE -> "Plantes envahissantes d'origine exotique";
                case RAVAGEUR -> "Ravageur";
                case VIRUS -> "Virus";
            };
        } else {
            result = "";
        }

        return result;
    };

    protected static final ValueFormatter<List<Sector>> AGROSYST_SECTORS_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = value.stream().map(Enum::name).collect(Collectors.joining(","));
        } else {
            result = "";
        }

        return result;
    };

    protected static final ValueParser<AgrosystInterventionType> AGROSYST_INTERVENTION_TYPE_PARSER = value -> {
        AgrosystInterventionType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = AgrosystInterventionType.valueOf(value.toUpperCase());
        }
        return result;
    };

    protected static final ValueParser<OtherProductInputUnit> AGROSYST_OTHER_PRODUCT_INPUT_UNIT_PARSER = value -> {
        OtherProductInputUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = OtherProductInputUnit.valueOf(value.toUpperCase());
        }
        return result;
    };

    protected static final ValueFormatter<OtherProductInputUnit> AGROSYST_OTHER_PRODUCT_INPUT_UNIT_FORMATTER = value -> {
        String result = value != null ? value.name() : "";
        return result;
    };

    protected static final ValueParser<BioAgressorType> AGROSYST_BIO_AGRESSOR_TYPE_PARSER = value -> {
        BioAgressorType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            List<String> names = Arrays.stream(BioAgressorType.values()).map(Enum::name).toList();
            if (names.contains(value.toUpperCase())) {
                result = BioAgressorType.valueOf(value.toUpperCase());
            }
            if (result == null) {
                if (value.equalsIgnoreCase("Action sur le métabolisme")) {
                    result = BioAgressorType.ACTION_SUR_LE_METABOLISME;
                } else if (value.equalsIgnoreCase("Auxiliaire biologique")) {
                    result = BioAgressorType.AUXILIAIRE_BIOLOGIQUE;
                } else if (value.equalsIgnoreCase("Générique")) {
                    result = BioAgressorType.GENERIQUE;
                } else if (value.equalsIgnoreCase("Maladie")) {
                    result = BioAgressorType.MALADIE;
                } else if (value.equalsIgnoreCase("Maladie physiologique")) {
                    result = BioAgressorType.MALADIE_PHYSIOLOGIQUE;
                } else if (value.equalsIgnoreCase("Plante parasite")) {
                    result = BioAgressorType.PLANTE_PARASITE;
                } else if (value.equalsIgnoreCase("Plantes envahissantes d'origine exotique")) {
                    result = BioAgressorType.PLANTES_ENVAHISSANTES_ORIGINE_EXOTIQUE;
                } else if (value.equalsIgnoreCase("Ravageur")) {
                    result = BioAgressorType.RAVAGEUR;
                } else if (value.equalsIgnoreCase("Virus")) {
                    result = BioAgressorType.VIRUS;
                } else if (value.equalsIgnoreCase("Adventice")) {
                    result = BioAgressorType.ADVENTICE;
                } else {
                    throw new UnsupportedOperationException("type non supporté : " + value);
                }
            }

        }
        return result;
    };

    protected static final ValueParser<Object> MANDATORYAGROSYST_BIO_AGRESSOR_TYPE_PARSER = value -> {
        if (StringUtils.isBlank(value)) {
            throw new AgrosystImportException("La valeur est obligatoire");
        }
        return AGROSYST_BIO_AGRESSOR_TYPE_PARSER.parse(value);
    };

    protected static final ValueParser<Collection<Sector>> AGROSYST_SECTORS_PARSER = value -> {
        Collection<Sector> result = new ArrayList<>();
        if (StringUtils.isNotBlank(value)) {
            Iterable<String> sectorsName = Splitter.on(',').omitEmptyStrings().trimResults().split(value);
            for (String sectorName : sectorsName) {
                result.add(Sector.valueOf(sectorName));
            }
        }
        return result;
    };

    protected static final ValueParser<FertiOrgaUnit> AGROSYST_FERTI_ORGA_UNIT_PARSER = value -> {
        FertiOrgaUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            value = value.trim();
            if (value.equalsIgnoreCase(KG_T)) {
                result = FertiOrgaUnit.KG_T;
            } else if (value.equalsIgnoreCase(KG_M_CUB)) {
                result = FertiOrgaUnit.KG_M_CUB;
            } else {
                throw new UnsupportedOperationException("Unité non supporté : " + value);
            }
        }
        return result;
    };

    protected static final ValueFormatter<FertiOrgaUnit> AGROSYST_FERTI_ORGA_UNIT_FORMATTER = fertiOrgaUnit -> {
        String result;
        if (fertiOrgaUnit != null) {
            if (fertiOrgaUnit == FertiOrgaUnit.KG_M_CUB) {
                result = KG_M_CUB;
            } else if (fertiOrgaUnit == FertiOrgaUnit.KG_T) {
                result = KG_T;
            } else {
                result = fertiOrgaUnit.name().toLowerCase();
            }
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueParser<VitesseCouv> VITESSE_COUV_PARSER = VitesseCouv::valueOf;

    protected static final ValueFormatter<VitesseCouv> VITESSE_COUV_FORMATTER = Enum::name;

    /**
     * Convertit une chaine contenant un pourcentage obligatoire en son equivalent en double.
     * Format valide: 0,0% >= 99,99% ou 0.0% >= 99.99%
     */
    protected static final ValueParser<Double> PERCENT_DOUBLE_PARSER = value -> {
        Double result = null;
        if (value != null) {
            if (value.matches("\\d{1,2}([.,]\\d+)?\\s*%")) {
                String str = StringUtils.removeEnd(value, "%").trim();
                str = str.replace(',', '.');
                result = Double.parseDouble(str) / 100;
            }
        }
        return result;
    };

    /**
     * Convertit un double en une chaine formattant le double en pourcentage.
     */
    protected static final ValueFormatter<Double> PERCENT_DOUBLE_FORMATTER = value -> String.format("%f%%", value);

    protected AbstractAgrosystModel(char separator) {
        super(separator);
    }

    protected Map<String, String> headerMap;

    protected String getHeaderId(String header) {
        return header.toLowerCase();
    }

    /**
     * Override method to build map to manage case insesitive headers.
     */
    @Override
    public void pushCsvHeaderNames(List<String> headerNames) {
        headerMap = Maps.uniqueIndex(headerNames, this::getHeaderId);
    }

    /**
     * Retourne le nom réel de l'header
     *
     * @param header header string
     * @return header in file
     */
    protected String getRealHeader(String header) {
        String result = null;
        if (headerMap != null) { // non dynamic mode
            result = headerMap.get(getHeaderId(header));
        }
        // si il existe pas
        if (result == null) {
            result = header;
        }
        return result;
    }

    protected boolean hasHeader(String header) {
        return headerMap.containsKey(getHeaderId(header));
    }

    @Override
    public <T> ImportableColumn<E, T> newIgnoredColumn(String headerName) {
        return super.newIgnoredColumn(getRealHeader(headerName));
    }

    @Override
    public ImportableColumn<E, String> newMandatoryColumn(String headerName) {
        return super.newMandatoryColumn(getRealHeader(headerName));
    }

    @Override
    public ImportableColumn<E, String> newMandatoryColumn(String headerName, String propertyName) {
        return super.newMandatoryColumn(getRealHeader(headerName), propertyName);
    }

    @Override
    public <T> ImportableColumn<E, T> newMandatoryColumn(String headerName, String propertyName, ValueParser<T> valueParser) {
        return super.newMandatoryColumn(getRealHeader(headerName), propertyName, valueParser);
    }


    protected static final ValueParser<RoleType> ROLE_TYPE_PARSER = value -> {
        RoleType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            result = RoleType.valueOf(value.toUpperCase());
        }
        return result;
    };

    protected static final ValueParser<MaterielWorkRateUnit> AGROSYST_MATERIEL_WORK_RATE_UNIT_PARSER = value -> {
        MaterielWorkRateUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String valueTrim = StringUtils.remove(value, ' ');
            if (valueTrim.equalsIgnoreCase("bal/h")) {
                result = MaterielWorkRateUnit.BAL_H;
            } else if (valueTrim.equalsIgnoreCase("ha/h")) {
                result = MaterielWorkRateUnit.HA_H;
            } else if (valueTrim.equalsIgnoreCase("m3/h")) {
                result = MaterielWorkRateUnit.M3_H;
            } else if (valueTrim.equalsIgnoreCase("qx/h")) {
                result = MaterielWorkRateUnit.QX_H;
            } else if (valueTrim.equalsIgnoreCase("sacs/h")) {
                result = MaterielWorkRateUnit.SACS_H;
            } else if (valueTrim.equalsIgnoreCase("t/benne")) {
                result = MaterielWorkRateUnit.T_BENNE;
            } else if (valueTrim.equalsIgnoreCase("trous/h")) {
                result = MaterielWorkRateUnit.TROUS_H;
            } else if (valueTrim.equalsIgnoreCase("voy/h")) {
                result = MaterielWorkRateUnit.VOY_H;
            } else if (valueTrim.equalsIgnoreCase("t/ha")) {
                result = MaterielWorkRateUnit.T_HA;
            } else if (valueTrim.equalsIgnoreCase("h/ha")) {
                result = MaterielWorkRateUnit.H_HA;
            } else if (valueTrim.equalsIgnoreCase("t/h")) {
                result = MaterielWorkRateUnit.T_H;
            } else if (valueTrim.equalsIgnoreCase("€/h")) {
                result = MaterielWorkRateUnit.EURO_H;
            } else if (valueTrim.equalsIgnoreCase("0")) {
            } else {
                throw new UnsupportedOperationException("Unité non supporté : " + value);
            }
        }
        return result;
    };

    protected static final ValueFormatter<MaterielWorkRateUnit> AGROSYST_MATERIEL_WORK_RATE_UNIT_FORMATTER = materielWorkRateUnit -> {
        String result;
        if (materielWorkRateUnit != null) {
            if (materielWorkRateUnit == MaterielWorkRateUnit.BAL_H) {
                result = "bal/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.HA_H) {
                result = "ha/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.M3_H) {
                result = "m3/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.QX_H) {
                result = "qx/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.SACS_H) {
                result = "sacs/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.T_BENNE) {
                result = "t/benne";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.TROUS_H) {
                result = "trous/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.VOY_H) {
                result = "voy/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.T_HA) {
                result = "t/ha";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.H_HA) {
                result = "h/ha";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.T_H) {
                result = "t/h";
            } else if (materielWorkRateUnit == MaterielWorkRateUnit.EURO_H) {
                result = "€/h";
            } else {
                result = materielWorkRateUnit.name().toLowerCase();
            }
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueParser<MaterielTransportUnit> AGROSYST_MATERIEL_TRANSPORT_UNIT_PARSER = value -> {
        MaterielTransportUnit result = null;
        if (!Strings.isNullOrEmpty(value)) {
            value = StringUtils.remove(value, ' ');
            if (value.equalsIgnoreCase("l") || value.equalsIgnoreCase("litres")) {
                result = MaterielTransportUnit.L;
            } else if (value.equalsIgnoreCase("m3")) {
                result = MaterielTransportUnit.M3;
            } else if (value.equalsIgnoreCase("t")) {
                result = MaterielTransportUnit.T;
            } else {
                throw new UnsupportedOperationException("Unité non supporté : " + value);
            }
        }
        return result;
    };

    protected static final ValueFormatter<MaterielTransportUnit> AGROSYST_MATERIEL_TRANSPORT_UNIT_FORMATTER = materielTransportUnit -> {
        String result;
        if (materielTransportUnit != null) {
            if (materielTransportUnit == MaterielTransportUnit.L) {
                result = "l";
            } else if (materielTransportUnit == MaterielTransportUnit.M3) {
                result = "m3";
            } else if (materielTransportUnit == MaterielTransportUnit.T) {
                result = "T";
            } else {
                result = materielTransportUnit.name().toLowerCase();
            }
        } else {
            result = "";
        }
        return result;
    };

    public static final ValueParser<TypeDEPHY> TYPE_DEPHY_PARSER = value -> {

        TypeDEPHY result = (TypeDEPHY) getSafeGenericEnumParser(TypeDEPHY.class, value);
        // in case not TypeDEPHY value
        if (result == null && !Strings.isNullOrEmpty(value)) {
            if (value.equalsIgnoreCase("DEPHY-FERME")) {
                result = TypeDEPHY.DEPHY_FERME;
            } else if (value.equalsIgnoreCase("DEPHY-EXPE")) {
                result = TypeDEPHY.DEPHY_EXPE;
            } else if (value.equalsIgnoreCase("Hors DEPHY") || value.equalsIgnoreCase("HORS_DEPHY")) {
                result = TypeDEPHY.NOT_DEPHY;
            } else {
                throw new UnsupportedOperationException("Type Dephy non supporté : " + value);
            }
        }
        return result;
    };

    public static final ValueParser<String> PARSE_EMAIL_ADDRESS = value -> {
        String result = null;
        if (StringUtils.isNoneBlank(value) && EmailService.isValidEmailAddress(value.trim().toLowerCase())) {
            result = value.trim().toLowerCase();
        }
        return result;
    };

    public static final ValueParser<FeedbackCategory> FEED_BACK_TYPE_PARSER = value -> {
        FeedbackCategory result;

        result = (FeedbackCategory) getSafeGenericEnumParser(FeedbackCategory.class, value);

        if (result == null && !Strings.isNullOrEmpty(value)) {
            if (value.equalsIgnoreCase("Bug")) {
                result = FeedbackCategory.BUG;
            } else if (value.equalsIgnoreCase("Consignes_saisies")) {
                result = FeedbackCategory.CONSIGNES_SAISIES;
            } else if (value.equalsIgnoreCase("Etat_lieux_saisies")) {
                result = FeedbackCategory.ETAT_LIEUX_SAISIES;
            } else if (value.equalsIgnoreCase("Decalage_donnees")) {
                result = FeedbackCategory.DECALAGE_DONNEES;
            } else if (value.equalsIgnoreCase("Demande_evolution")) {
                result = FeedbackCategory.EVOLUTION;
            } else if (value.equalsIgnoreCase("eDaplos")) {
                result = FeedbackCategory.EDAPLOS;
            } else if (value.equalsIgnoreCase("Fonctionnement_Agrosyst")) {
                result = FeedbackCategory.FONCTIONNEMENT_AGROSYST;
            } else if (value.equalsIgnoreCase("Gestion_comptes")) {
                result = FeedbackCategory.GESTION_COMPTE;
            } else if (value.equalsIgnoreCase("Referentiels")) {
                result = FeedbackCategory.REFERENTIELS;
            } else if (value.equalsIgnoreCase("Local_intrants")) {
                result = FeedbackCategory.LOCAL_INTRANTS;
            } else {
                throw new UnsupportedOperationException("Type de feedback non supporté : " + value);
            }
        }
        return result;
    };

    protected static final ValueParser<PriceUnit> PRICE_UNIT_PARSER = value -> (PriceUnit) getGenericEnumParser(PriceUnit.class, value);

    protected static final ValueParser<PriceType> PRICE_TYPE_PARSER = value -> {
        PriceType result = null;
        if (!Strings.isNullOrEmpty(value)) {
            if (value.equalsIgnoreCase(PriceType.ACTION.toString())) {
                result = PriceType.valueOf(value.toUpperCase());
            } else if (value.equalsIgnoreCase(PriceType.FUEL.toString())) {
                result = PriceType.valueOf(value.toUpperCase());
            } else if (value.equalsIgnoreCase(PriceType.INPUT.toString())) {
                result = PriceType.valueOf(value.toUpperCase());
            }
        }
        return result;
    };

    protected static final ValueParser<Sector> SECTOR_PARSER = value -> (Sector) getGenericEnumParser(Sector.class, value);

    protected static final ValueParser<SectionType> SECTION_TYPE_PARSER = value -> (SectionType) getGenericEnumParser(SectionType.class, value);

    public static final ValueParser<CategoryObjective> CATEGORY_OBJECTIVE_PARSER = value -> (CategoryObjective) getGenericEnumParser(CategoryObjective.class, value);

    public static final ValueParser<StrategyType> STRATEGY_TYPE_PARSER = value -> (StrategyType) getGenericEnumParser(StrategyType.class, value);

    public static final ValueParser<YealdCategory> YEALD_CATEGORY_PARSER = value -> (YealdCategory) getGenericEnumParser(YealdCategory.class, value);

    public static final ValueParser<PhytoProductUnit> PHYTO_PRODUCT_UNIT_PARSER = value -> (PhytoProductUnit) getGenericEnumParser(PhytoProductUnit.class, value);

    public static final ValueParser<CapacityUnit> CAPACITY_UNIT_PARSER = value -> (CapacityUnit) getGenericEnumParser(CapacityUnit.class, value);

    public static final ValueParser<MineralProductUnit> MINERAL_PRODUCT_UNIT_PARSER = value -> (MineralProductUnit) getGenericEnumParser(MineralProductUnit.class, value);

    public static final ValueParser<OrganicProductUnit> ORGANIC_PRODUCT_UNIT_PARSER = value -> (OrganicProductUnit) getGenericEnumParser(OrganicProductUnit.class, value);

    public static final ValueParser<SeedPlantUnit> SEED_PLANT_UNIT_VALUE_UNIT_PARSER = value -> (SeedPlantUnit) getGenericEnumParser(SeedPlantUnit.class, value);

    public static final ValueParser<SubstrateInputUnit> SUBSTRATE_INPUT_UNIT_VALUE_UNIT_PARSER = value -> (SubstrateInputUnit) getGenericEnumParser(SubstrateInputUnit.class, value);

    public static final ValueParser<PotInputUnit> POT_UNIT_UNIT_VALUE_UNIT_PARSER = value -> (PotInputUnit) getGenericEnumParser(PotInputUnit.class, value);

    public static final ValueParser<FertiMinElement> FERTI_MIN_ELEMENT_PARSER = value -> {
        // TODO: 05/10/17 dcosse utiliser la métode de ReferentialService.FERTI_MIN_ELEMENT_NAME_TO_ENUM
        return (FertiMinElement) getGenericEnumParser(FertiMinElement.class, value);
    };

    protected static Enum getGenericEnumParser(Class enumType, String value) {
        Enum result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(value);
            strValue = strValue.replaceAll("\\W", "_"); // non word chars
            strValue = strValue.toUpperCase();
            result = Enum.valueOf(enumType, strValue);
        }
        return result;
    }

    protected static Enum getSafeGenericEnumParser(Class enumType, String value) {
        Enum result = null;
        if (!Strings.isNullOrEmpty(value)) {
            String strValue = StringUtils.stripAccents(value);
            strValue = strValue.replaceAll("\\W", "_"); // non word chars
            strValue = strValue.toUpperCase();
            try {
                result = Enum.valueOf(enumType, strValue);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
        return result;
    }

    protected static final ValueFormatter<Enum> GENERIC_ENUM_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = value.name().toLowerCase();
        } else {
            result = "";
        }
        return result;
    };

    protected static final ValueParser<Object> EMPTY_TO_NULL = value -> {
        String result = null;
        if (value != null) {
            if (NumberUtils.isParsable(value)) {
                Integer val = NumberUtils.toInt(value);
                result = String.valueOf(val);
            } else {
                result = value;
            }
        }
        result = Strings.emptyToNull(result);
        return result;
    };

    protected static final ValueParser<Object> STRING_MANDATORY_PARSER = value -> {
        String result;
        if (StringUtils.isBlank(value)) {
            throw new AgrosystImportException("La valeur est obligatoire");
        }

        if (NumberUtils.isParsable(value)) {
            Integer val = NumberUtils.toInt(value);
            result = String.valueOf(val);
        } else {
            result = value;
        }
        return result;
    };

    protected static final ValueParser<String> STRING_WITH_NULL_PARSER = value -> {
        String result = "";
        if (!Strings.isNullOrEmpty(value)) {
            result = value;
        }
        return result;
    };

    /**
     * String to integer converter.
     */
    protected static final ValueParser<Integer> INT_MANDATORY_PARSER = value -> {
        if (StringUtils.isBlank(value)) {
            throw new AgrosystImportException("La valeur est obligatoire");
        }
        return parseInt(value);
    };

    /**
     * String to double converter for a mandatory value:
     * Can handle , or . as decimal separator and ' ' as thousand separator
     * Throw {@link UnsupportedOperationException} if no value.
     */
    protected static final ValueParser<Double> DOUBLE_MANDATORY_PARSER = value -> {
        if (StringUtils.isBlank(value) || value.contains("N/A") || value.contains("NA")) {
            throw new AgrosystImportException("La valeur est obligatoire");
        }
        // " " est un espace insécable, pas un " "
        return Double.parseDouble(value.replace(',', '.').replace(" ", ""));
    };

    protected static final ValueParser<UnitType> UNIT_TYPE_PARSER = value -> {
        UnitType result = null;
        if (StringUtils.isNotEmpty(value)) {
            String strValue = StringUtils.deleteWhitespace(StringUtils.stripAccents(value));
            result = (UnitType) getGenericEnumParser(UnitType.class, strValue);
        }
        return result;
    };

    protected static final ValueParser<TypeDose> TYPE_DOSE_PARSER = value -> {
        TypeDose result = null;
        if (StringUtils.isNotEmpty(value)) {
            String strValue = StringUtils.deleteWhitespace(StringUtils.stripAccents(value));
            result = (TypeDose) getGenericEnumParser(TypeDose.class, strValue);
        }
        return result;
    };
}
