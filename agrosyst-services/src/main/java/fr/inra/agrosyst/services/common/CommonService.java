package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Splitter;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.csv.ValueParser;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Created by davidcosse on 22/10/14.
 *
 * Singleton
 */
public class CommonService {
    public static final int INVALID_CAMPAIGN = -1;
    public final int lowerCampaignBound;
    public final int upperCampaignBound;
    protected final Pair<Integer, Integer> campaignsBounds;

    public static final String CAMPAIGNS_SEPARATOR = ", ";
    
    private static CommonService instance = null;
    
    private CommonService(int lowerCampaignBound, int upperCampaignBound) {
        this.lowerCampaignBound = lowerCampaignBound;
        this.upperCampaignBound = upperCampaignBound;
        this.campaignsBounds = Pair.of(this.lowerCampaignBound, this.upperCampaignBound);
        instance = this;
    }
    
    public static CommonService getInstance() {
        if (instance == null) {
            throw new AgrosystTechnicalException("Use CommonService.createInstance(lowerCampaignBound, upperCampaignBound) first");
        }
        return instance;
    }
    
    public static CommonService createInstance(int lowerCampaignBound, int upperCampaignBound) {
        if (instance == null) {
            instance = new CommonService(lowerCampaignBound, upperCampaignBound);
        }
        return instance;
    }
    
    public Pair<Integer, Integer> getCampaignsBounds() {
        return campaignsBounds;
    }
    
    public boolean areCampaignsValids(String campaigns) {
        boolean result = false;
        try {
            Set<Integer> set = GET_CAMPAIGNS_SET.apply(campaigns);
            result = set != null && !set.isEmpty();
            if (result) {
                for (Integer integer : set) {
                    result &= isCampaignValid(integer);
                }
            }
        } catch (Exception eee) {
            Log log = LogFactory.getLog(CommonService.class);
            // Parsing exception
            if (log.isWarnEnabled()) {
                log.warn("Invalid campaigns: " + campaigns, eee);
            }
        }
        return result;
    }
    
    public boolean isCampaignValid(Integer integer) {
        
        boolean result = true;
        
        if (integer < lowerCampaignBound || integer > upperCampaignBound) {
            Log log = LogFactory.getLog(CommonService.class);
            if (log.isWarnEnabled()) {
                String format = "Invalid campaign: %d. Must be contained between %d and %d. ";
                String message = String.format(format, integer, lowerCampaignBound, upperCampaignBound);
                log.warn(message);
            }
            result = false;
        }
        return result;
    }
    
    protected static final Function<String, Integer> STRING_TO_INTEGER = input -> {
        int result = INVALID_CAMPAIGN;
        if (NumberUtils.isCreatable(input)) {
            result = NumberUtils.toInt(input, INVALID_CAMPAIGN);
        }
        return result;
    };
    
    public static final Function<String, LinkedHashSet<Integer>> GET_CAMPAIGNS_SET = input -> {
        Objects.requireNonNull(input);
        input = input.replaceAll("\\D+", " ");
        Iterable<String> split = Splitter.on(" ").trimResults().omitEmptyStrings().split(input);
        return StreamSupport.stream(split.spliterator(), false)
                .map(STRING_TO_INTEGER)
                .sorted(Integer::compareTo)
                .collect(Collectors.toCollection(LinkedHashSet::new));
    };
    
    public static final Function<String, Pair<Integer, Integer>> GET_CAMPAINGS_RANGE = campaigns -> {
        List<Integer> allCampaigns = new ArrayList<>(GET_CAMPAIGNS_SET.apply(campaigns));
        Collections.sort(allCampaigns);
        int beginCampaigns = allCampaigns.getFirst();
        int endingCampaigns = allCampaigns.get(allCampaigns.size() - 1);
        return Pair.of(beginCampaigns, endingCampaigns);
    };
    
    public static final Function<Set<Integer>, String> ARRANGE_CAMPAIGNS_SET = input -> {
        Objects.requireNonNull(input);
        return input.stream()
                .sorted()
                .map(Object::toString)
                .collect(Collectors.joining(CAMPAIGNS_SEPARATOR));
    };

    public static final Function<String, String> ARRANGE_CAMPAIGNS = GET_CAMPAIGNS_SET.andThen(ARRANGE_CAMPAIGNS_SET);
    
    public static final ValueParser<Boolean> BOOLEAN_PARSER = value -> {
        Boolean result;
        if ("O".equalsIgnoreCase(value) ||
                "OU".equalsIgnoreCase(value) ||
                "OUI".equalsIgnoreCase(value) ||
                "Y".equalsIgnoreCase(value) ||
                "YE".equalsIgnoreCase(value) ||
                "YES".equalsIgnoreCase(value) ||
                "T".equalsIgnoreCase(value) ||
                "TR".equalsIgnoreCase(value) ||
                "TRU".equalsIgnoreCase(value) ||
                "TRUE".equalsIgnoreCase(value) ||
                "1".equalsIgnoreCase(value)) {
            result = Boolean.TRUE;
        } else {
            result = Boolean.FALSE;
        }
        return result;
    };
    
    // remove spaces, transform to lower case and replace accent chart by regular one
    public static final Function<String, String> NORMALISE = input -> {
    
        final String replace = input == null ? "" : input.toLowerCase();

        return Normalizer.normalize(replace, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .replaceAll("[^a-z0-9]", "");
    };
    
    public static final Function<String, String> NORMALISE_WITH_SPACES = input -> {
        
        final String replace = input == null ? "" : input.toLowerCase();
        
        return Normalizer.normalize(replace, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    };
    
    public static final Function<String, List<String>> EXTRACT_EMAILS = input -> {
        List<String> emails = new ArrayList<>();
        if (input != null) {
            Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(input);
            while (m.find()) {
                emails.add(m.group());
            }
        }
        return emails;
    };

}
