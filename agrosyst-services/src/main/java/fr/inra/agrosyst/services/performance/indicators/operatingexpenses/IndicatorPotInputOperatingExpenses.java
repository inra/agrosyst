package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2020 INRAE, Code Lutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.PotInputUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant de type « pot ».
 * <p>
 *
 * <p>
 * Cette classe est une composante de {@link IndicatorOperatingExpenses} chargée de calculer les charges relatives aux pots.
 * </p>
 * Rappel de la formule globale de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + sum(Q_a * PA_a)) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 * - Autres intrants
 *   - Q_a (diverses unités) : quantité de l'intrant pot. Donnée saisie par l’utilisateur.
 *   - Q_h (diverses unités) : quantité de l’intrant h, h appartenant à la liste des intrants de type « pot » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *   - PA_h (diverses unités) : prix d’achat de l’intrant h, h appartenant à la liste des intrants de type « pot » appliqués au cours de l’intervention i.
 *     Donnée saisie par l’utilisateur.
 *
 *
 * </pre>
 *
 * @author David Cossé (cosse@codelutin.com)
 */
public class IndicatorPotInputOperatingExpenses extends IndicatorInputProductOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorPotInputOperatingExpenses.class);

    public IndicatorPotInputOperatingExpenses(boolean displayed, boolean computeReal, boolean computStandardised, Locale locale) {
        super(displayed, computeReal, computStandardised, locale);
    }

    @Override
    public Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(AbstractInputUsage usage, RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {
        final DomainPotInput input = ((PotInputUsage) usage).getDomainPotInput();
        final InputPrice inputPrice = input.getInputPrice();
        Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.potRefPriceForInput().get(input);
        inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
        return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
    }

    // practiced
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> conversionRate,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        Double[] result = new Double[]{
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};

        PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();

        Optional<List<OtherAction>> optionalOtherActions = interventionContext.getOptionalOtherActions();
        if (optionalOtherActions.isEmpty()) {
            return result;
        }

        List<OtherAction> otherActions = optionalOtherActions.get();
        for (OtherAction otherAction : otherActions) {

            Collection<PotInputUsage> potInputUsages = otherAction.getPotInputUsages();
            PracticedIntervention intervention = interventionContext.getIntervention();

            if (CollectionUtils.isEmpty(potInputUsages)) {
                // no phyto product cost
                continue;
            }

            // inputsCharges for price, inputsCharges for refprice
            double psci = getToolPSCi(intervention);

            final RefCampaignsInputPricesByDomainInput practicedSystemInputRefPricesByDomainInput = practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput();
            Pair<Double, Double> inputsCharges = computeIndicatorForAction(
                    writerContext,
                    otherAction,
                    potInputUsages,
                    practicedSystemInputRefPricesByDomainInput,
                    psci,
                    conversionRate, intervention.getTopiaId(),
                    indicatorClass,
                    labels,
                    new HashMap<>());

            result = GenericIndicator.sum(result, new Double[]{inputsCharges.getLeft(), inputsCharges.getRight()});

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format(
                        Indicator.REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED,
                        "computePotIndicator",
                        intervention.getName(),
                        intervention.getTopiaId(),
                        practicedSystem.getName(),
                        practicedSystem.getCampaigns(),
                        System.currentTimeMillis() - start));
            }

        }

        return result;
    }

    // effective
    protected Double[] computeOperatingExpenses(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            Zone zone,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Collection<RefInputUnitPriceUnitConverter> converters,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        long start = System.currentTimeMillis();

        Double[] result = new Double[]{
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getLeft(),
                IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES.getRight()};

        Optional<List<OtherAction>> optionalOtherAction_ = interventionContext.getOptionalOtherActions();
        if (optionalOtherAction_.isEmpty()) {
            return result;
        }

        List<OtherAction> otherActions = optionalOtherAction_.get();
        for (OtherAction otherAction : otherActions) {

            EffectiveIntervention intervention = interventionContext.getIntervention();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(
                        "computePotIndicator intervention:" +
                                intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                                ", zone:" + zone.getName() +
                                " ) calculé en :" + (System.currentTimeMillis() - start) + "ms");
            }

            Collection<PotInputUsage> potInputUsages = otherAction.getPotInputUsages();

            if (CollectionUtils.isEmpty(potInputUsages)) {
                // no product cost
                continue;
            }

            // inputsCharges for price, inputsCharges for refprice
            double psci = getToolPSCi(intervention);

            final RefCampaignsInputPricesByDomainInput refInputPricesForCampaignsByInput = domainContext.getRefInputPricesForCampaignsByInput();

            Pair<Double, Double> inputsCharges = computeIndicatorForAction(
                    writerContext,
                    otherAction,
                    potInputUsages,
                    refInputPricesForCampaignsByInput,
                    psci,
                    converters, intervention.getTopiaId(),
                    indicatorClass,
                    labels,
                    new HashMap<>());

            Double[] result_ = {
                    inputsCharges.getLeft(),
                    inputsCharges.getRight()};

            result = GenericIndicator.sum(result, result_);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(
                        "computeOrganicProductIndicator intervention:" +
                                intervention.getName() + " (" + intervention.getTopiaId() + ")" +
                                ", zone:" + zone.getTopiaId() +
                                " calculé en :" + (System.currentTimeMillis() - start));
            }

        }

        return result;
    }

}
