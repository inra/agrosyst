package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.services.action.ActionService;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.indicators.agronomicstrategy.TillageType;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public abstract class GenericIndicator extends IndicatorFieldsStatistics {

    private static final Log LOGGER = LogFactory.getLog(GenericIndicator.class);

    public static final String TYPE_MAT_EPANDEURS_ET_ACCESSOIRES = "EPANDEURS & ACCESSOIRES";
    public static final String TYPE_MAT_PRESSES = "PRESSES";
    public static final String TYPE_MAT_RECOLTE_POMMES_CIDRE = "RECOLTE POMMES CIDRE";
    public static final Integer DEFAULT_NB_BALLS = 10;

    // refs #10059 : vol/voy default value depends on the input dose unit : 12t/voy or 18.5m3/voy
    public static final Map<OrganicProductUnit, Double> DEFAULT_TRANSIT_VOLUME =
            ImmutableMap.of(OrganicProductUnit.T_HA, 12.0d, OrganicProductUnit.KG_HA, 12000.0d, OrganicProductUnit.M_CUB_HA, 18.5d);

    public static final Map<Pair<MaterielTransportUnit, OrganicProductUnit>, Double> TRANSIT_VOLUME_INPUT_DOSE_UNIT_CONVERTER =
            ImmutableMap.<Pair<MaterielTransportUnit, OrganicProductUnit>, Double>builder()
                    .put(Pair.of(MaterielTransportUnit.T, OrganicProductUnit.M_CUB_HA), 1.0d)
                    .put(Pair.of(MaterielTransportUnit.T, OrganicProductUnit.T_HA), 1.0d)
                    .put(Pair.of(MaterielTransportUnit.T, OrganicProductUnit.KG_HA), 0.001d)
                    .put(Pair.of(MaterielTransportUnit.M3, OrganicProductUnit.M_CUB_HA), 1.0d)
                    .put(Pair.of(MaterielTransportUnit.M3, OrganicProductUnit.T_HA), 1d)
                    .put(Pair.of(MaterielTransportUnit.M3, OrganicProductUnit.KG_HA), 0.001d)
                    .put(Pair.of(MaterielTransportUnit.L, OrganicProductUnit.M_CUB_HA), 0.001d)
                    .put(Pair.of(MaterielTransportUnit.L, OrganicProductUnit.T_HA), 0.001d)
                    .put(Pair.of(MaterielTransportUnit.L, OrganicProductUnit.KG_HA), 1.0d)
                    .build();

    public static final Map<MaterielTransportUnit, OrganicProductUnit> TRANSIT_VOLUME_INPUT_PRODUCT_UNIT_CONVERTER =
            ImmutableMap.<MaterielTransportUnit, OrganicProductUnit>builder()
                    .put(MaterielTransportUnit.T, OrganicProductUnit.T_HA)
                    .put(MaterielTransportUnit.M3, OrganicProductUnit.M_CUB_HA)
                    .put(MaterielTransportUnit.L, OrganicProductUnit.KG_HA)
                    .build();

    public static final Double DEFAULT_ORGANIC_INPUT_QTE_AVG_DOSE = 20.0d;

    // TODO ymartel 20180830 : for the moment, use same as described in 10060
    // TODO kmorin 20190920 : for the moment, use same as described in 10504
    public static final double DEFAULT_WORK_RATE_VALUE = 1.0d;

    protected static final String RELIABILITY_INDEX_NO_COMMENT = "";

    protected static final Integer DEFAULT_RELIABILITY_INDEX = 100;

    protected static final Double DEFAULT_USED_AGRICULTURAL_AREA = 1.0d;

    protected static final Double DEFAULT_AFFECTED_AREA_RATE = 1.0d;

    public static final Double DEFAULT_IFT = 1.0d;

    // Le volume de bouillie est saisi au niveau de l’intervention en hL/ha.
    // C’est une donnée obligatoire (pas de valeur null) mais la valeur 0 est autorisée.
    // Si VB = 0 hL/ha, alors on définit une valeur par défaut de VB=10 hL/ha.
    protected static final Double DEFAULT_BOILED_QUANTITY = 10.0d;

    protected static final double DEFAULT_PRICE_CONVERTION_RATE = 1.0d;

    /**
     * Pattern des dates de périodes des interventions ITK.
     */
    public static final Pattern PRACTICED_DATE_PATTERN = Pattern.compile("(\\d\\d?)/(\\d\\d?)");

    // indicatorName, interventionName, interventionId, practicedSystemName
    public static final String REPORT_INDICATOR_EXECUTION_TIME_ON_PRACTICED = "%s intervention:'%s (%s)', practicedSystem: '%s (%s)' calculé en : %d ms";

    public static final String REPORT_INDICATOR_EXECUTION_TIME_ON_EFFECTIVE = "%s intervention:'%s (%s)', zone: '%s' calculé en : %d ms";

    protected final static String[] MONTHS_FOR_DB_COLUMNS = new String[] {
            "janvier",
            "fevrier",
            "mars",
            "avril",
            "mai",
            "juin",
            "juillet",
            "aout",
            "septembre",
            "octobre",
            "novembre",
            "decembre",
    };

    public abstract Map<String, String> getIndicatorNameToDbColumnName();

    public abstract Map<OptionalExtraColumnEnumKey, String> getExtraFields();

    public enum OptionalExtraColumnEnumKey {
        TAUX_DE_COMPLETION,
        MARGE_BRUTE_REELLE_TX_COMP,
        MARGE_BRUTE_STD_MIL_TX_COMP,
        MARGE_SEMI_NETTE_REELLE_TX_COMP,
        MARGE_SEMI_NETTE_STD_MIL_TX_COMP,
        MARGE_DIRECTE_REELLE_TX_COMP,
        MARGE_DIRECTE_STD_MIL_TX_COMP,
        CHARGES_MECANISATION_REELLES_TX_COMP,
        CHARGES_MECANISATION_STD_MIL_TX_COMP,
        CHARGES_OPERATIONNELLES_REELLES_TX_COMP,
        CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP,
        CHARGES_MANUELLES_REELLES_TX_COMP,
        CHARGES_MANUELLES_STD_MIL_TX_COMP,
        CHARGES_TRACTORISTES_REELLES_TX_COMP,
        CHARGES_TRACTORISTES_STD_MIL_TX_COMP,
        CHARGES_TOTALES_REELLES_TX_COMP,
        CHARGES_TOTALES_STD_MIL_TX_COMP,
        DETAIL_CHAMPS_NON_RENSEIGNES,
        MARGE_BRUTE_REELLE_DETAIL,
        MARGE_BRUTE_STD_MIL_DETAIL,
        MARGE_SEMI_NETTE_REELLE_DETAIL,
        MARGE_SEMI_NETTE_STD_MIL_DETAIL,
        MARGE_DIRECTE_REELLE_DETAIL,
        MARGE_DIRECTE_STD_MIL_DETAIL,
        CHARGES_OPERATIONNELLES_REELLES_DETAIL,
        CHARGES_OPERATIONNELLES_STD_MIL_DETAIL,
        CHARGES_MECANISATION_REELLES_DETAIL,
        CHARGES_MECANISATION_STD_MIL_DETAIL,
        CHARGES_MANUELLES_REELLES_DETAIL,
        CHARGES_MANUELLES_STD_MIL_DETAIL,
        CHARGES_TRACTORISTES_REELLES_DETAIL,
        CHARGES_TRACTORISTES_STD_MIL_DETAIL,
        CHARGES_TOTALES_REELLES_DETAIL,
        CHARGES_TOTALES_STD_MIL_DETAIL,
        DOSE_REFERENCE,
        DOSE_REFERENCE_UNIT
    }

    protected boolean isEdaplosDefaultValorisations(Collection<HarvestingActionValorisation> valorisations) {
        boolean isEdaplosDefaultValorisations = valorisations.stream()
                .filter(Objects::nonNull)
                .anyMatch(valorisation -> ActionService.DEFAULT_DESTINATION_NAME.equals(valorisation.getDestination().getDestination()));
        return isEdaplosDefaultValorisations;
    }

    protected double getWorkRateValue_H_HA_OrDefault(Pair<Double, MaterielWorkRateUnit> workRate,
                                                     Pair<Double, OrganicProductUnit> dose,
                                                     Double transitVolume,
                                                     Double doseVolumeConverter,
                                                     double defaultWorkRate) {
        double workRateValue = defaultWorkRate;

        if (workRate != null && workRate.getLeft() != null) {

            final MaterielWorkRateUnit workRateUnit = workRate.getRight();
            final Double workRateValue1 = workRate.getLeft();

            if (MaterielWorkRateUnit.H_HA.equals(workRateUnit) || MaterielWorkRateUnit.HA_H.equals(workRateUnit)) {
                if (MaterielWorkRateUnit.HA_H.equals(workRateUnit)) {
                    if (workRateValue1 > 0.0d) {
                        workRateValue = 1 / workRateValue1;
                    } else {
                        workRateValue = DEFAULT_WORK_RATE_VALUE;
                    }
                } else {
                    workRateValue = workRateValue1;
                }

            } else if (MaterielWorkRateUnit.VOY_H.equals(workRateUnit) && dose != null && transitVolume != null) {

                final Double doseValue = dose.getLeft();
                if (doseValue == null || doseVolumeConverter == null) {
                    workRateValue = defaultWorkRate;
                } else {
                    final double divider = transitVolume * workRateValue1;
                    workRateValue = divider == 0 ? 0 : (doseValue * doseVolumeConverter) / divider;
                }

            }
        }

        return workRateValue;
    }

    protected void logNonValidValorisations(Collection<HarvestingActionValorisation> valorisations, String interventionId) {
        long nbNullValorisations = valorisations.stream()
                .filter(Objects::isNull).count();
        if (nbNullValorisations > 0) {
            MissingFieldMessage m = messageBuilder.getInvalidValorisationMessage();
            addMissingFieldMessage(interventionId, m);
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn(String.format("Les valorisations présentes l'intervention '%s' ne sont pas toutes valides.", interventionId));
            }
        }
    }

    /**
     * @return work rate
     */
    protected Pair<Double, MaterielWorkRateUnit> getInterventionWorkRate(Double workRate, MaterielWorkRateUnit workRateUnit, String interventionId) {
        MaterielWorkRateUnit unit = workRateUnit;
        if (MaterielWorkRateUnit.HA_H.equals(unit)) {
            if (workRate > 0.0d) {
                workRate = 1.0d / workRate;
            } else {
                workRate = DEFAULT_WORK_RATE_VALUE;
                addMissingFieldMessage(interventionId, messageBuilder.getMissingWorkRateMessage());
            }
            unit = MaterielWorkRateUnit.H_HA;
        }
        return Pair.of(workRate, unit);
    }

    protected Pair<Double, MaterielWorkRateUnit> getInterventionWorkRate(Pair<Double, MaterielWorkRateUnit> workRate,
                                                                         String interventionId,
                                                                         double defaultWorkRate,
                                                                         MaterielWorkRateUnit defaultWorkRateUnit) {
        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
        if (workRate == null) {
            workRate = Pair.of(defaultWorkRate, defaultWorkRateUnit);
            addMissingFieldMessage(interventionId, messageBuilder.getMissingWorkRateMessage());
        }
        return workRate;
    }

    protected Pair<Double, Double> addLowerHigherWorkRateForWorkRateUnit(
            Pair<Double, Double> lowerHigerWorkRateForUnit,
            double mwr) {

        double lowerWorkRateForUnit = lowerHigerWorkRateForUnit == null ? Double.MAX_VALUE : lowerHigerWorkRateForUnit.getLeft();
        double higerWorkRateForUnit = lowerHigerWorkRateForUnit == null ? -Double.MAX_VALUE : lowerHigerWorkRateForUnit.getRight();

        lowerWorkRateForUnit = Math.min(lowerWorkRateForUnit, mwr);

        higerWorkRateForUnit = Math.max(higerWorkRateForUnit, mwr);

        return Pair.of(lowerWorkRateForUnit, higerWorkRateForUnit);
    }

    protected ImmutableMap<MaterielWorkRateUnit, Pair<Double, Double>> getAutomoteurWorkRateRangeByUnits(
            ToolsCoupling toolsCoupling,
            RefMaterielAutomoteur rma) {

        Map<MaterielWorkRateUnit, Pair<Double, Double>> workRateByUnits = new HashMap<>();
        final MaterielWorkRateUnit autoMoteurMwru = rma.getPerformanceUnite();
        final double autoMoteurMwr = rma.getPerformance();
        if (autoMoteurMwru != null) {
            workRateByUnits.put(autoMoteurMwru, Pair.of(autoMoteurMwr, autoMoteurMwr));
        }
        if (autoMoteurMwru == null || autoMoteurMwr == 0) {
            Collection<Equipment> equipments = toolsCoupling.getEquipments();

            equipments.forEach(
                    equipment -> {
                        final RefMateriel rme = equipment.getRefMateriel();
                        if (rme instanceof RefMaterielOutil rmo) {
                            final MaterielWorkRateUnit emwru = rmo.getPerformanceUnite();
                            final double emwr = rmo.getPerformance();
                            if (emwru != null) {
                                Pair<Double, Double> lowerHigherWorkRateForWorkRateUnit = addLowerHigherWorkRateForWorkRateUnit(workRateByUnits.get(emwru), emwr);
                                workRateByUnits.put(emwru, lowerHigherWorkRateForWorkRateUnit);
                            }
                        }
                    }
            );
        }
        return ImmutableMap.copyOf(workRateByUnits);
    }

    protected ImmutableMap<MaterielWorkRateUnit, Pair<Double, Double>> computeToolsCouplingWorkRateByUnits(
            ToolsCoupling toolsCoupling, Double mwr, MaterielWorkRateUnit mwru) {

        ImmutableMap<MaterielWorkRateUnit, Pair<Double, Double>> workRateByUnits;
        if (mwru != null && mwr != null && mwr > 0.0d) {
            workRateByUnits = ImmutableMap.of(mwru, Pair.of(mwr, mwr));
        } else {
            // get workRate From equipment
            Equipment tractor = toolsCoupling.getTractor();
            if (tractor != null) {
                final RefMateriel rm = tractor.getRefMateriel();
                workRateByUnits = rm instanceof RefMaterielAutomoteur rma ? getAutomoteurWorkRateRangeByUnits(toolsCoupling, rma) : ImmutableMap.of();
            } else {
                workRateByUnits = ImmutableMap.of();
            }
        }
        return workRateByUnits;
    }

    protected Pair<Double, MaterielWorkRateUnit> computeToolsCouplingWorkRate(
            MaterielWorkRateUnit mwru, ImmutableMap<MaterielWorkRateUnit, Pair<Double, Double>> workRateByUnits) {

        Pair<Double, MaterielWorkRateUnit> result;
        Pair<Double, Double> workRateAndUnit = workRateByUnits.get(MaterielWorkRateUnit.HA_H);
        if (workRateAndUnit != null || workRateByUnits.get(MaterielWorkRateUnit.H_HA) != null) {
            Double workRate = null;
            // s’il y a plusieurs outils dans la combinaison d’outil, on retient le
            // débit de chantier le plus lent => le plus élevé en h/ha = le plus faible en ha/h
            if (workRateAndUnit != null) {
                Double workRateValue = workRateAndUnit.getLeft();
                if (workRateValue > 0.0d) {
                    // le plus élevé en h/ha
                    workRate = 1.0d / workRateValue; // convert in H_HA
                } else {
                    workRate = DEFAULT_WORK_RATE_VALUE;
                }
            }
            Pair<Double, Double> hHaWr = workRateByUnits.get(MaterielWorkRateUnit.H_HA);
            if (hHaWr != null) {
                // le plus faible en ha/h
                workRate = workRate == null ? hHaWr.getRight() : Math.min(workRate, hHaWr.getRight());
            }
            result = Pair.of(workRate, MaterielWorkRateUnit.H_HA);
        } else {
            Double workRate = null;
            // pas de conversion on utilise l'unité du matériel de traction
            // on garde la plus petite valeur
            Pair<Double, Double> wr = workRateByUnits.get(mwru);
            if (wr != null) {
                // le plus faible en ha/h
                workRate = wr.getRight();
            }
            result = Pair.of(workRate, mwru);
        }
        return result;
    }

    /**
     * @return work rate in h/ha
     */
    protected Pair<Double, MaterielWorkRateUnit> getToolsCouplingWorkRate(ToolsCoupling toolsCoupling) {

        if (toolsCoupling == null || toolsCoupling.isManualIntervention()) {
            return null;
        }

        final Double mwr = toolsCoupling.getWorkRate();
        final MaterielWorkRateUnit mwru = toolsCoupling.getWorkRateUnit();

        ImmutableMap<MaterielWorkRateUnit, Pair<Double, Double>> workRateByUnits = computeToolsCouplingWorkRateByUnits(toolsCoupling, mwr, mwru);

        Pair<Double, MaterielWorkRateUnit> result = computeToolsCouplingWorkRate(mwru, workRateByUnits);

        return result;
    }

    public Double[] addPerennialCropPart(Double solOccupationPercent, Double totalSolOccupationPercent, Double[] result) {
        Double[] r = new Double[result.length];
        // calcul de la part des culture pérennes dans la surface du SDC
        double part = solOccupationPercent == null ? 1 : solOccupationPercent / 100;
        double sumPart = totalSolOccupationPercent == null ? 1 : totalSolOccupationPercent / 100;

        if (sumPart == 0) {
            // resultat non valid on ignore le calcul des part
            sumPart = 1;
            part = 1;
        }

        for (int i = 0; i < r.length; i++) {
            // prise en compte de la part de la culture dans la surface du SDC
            r[i] = (result[i] * part) / sumPart;
        }
        return r;
    }

    /**
     * Initialize new array with size and default value.
     *
     * @param length       size
     * @param defaultValue defalt value
     * @return initialized array
     */
    public static Double[] newArray(int length, double defaultValue) {
        Double[] array = new Double[length];
        Arrays.fill(array, defaultValue);
        return array;
    }

    /**
     * Initialize new array with size and default value.
     *
     * @param length       size
     * @param defaultValue defalt value
     * @return initialized array
     */
    public static Boolean[] newArray(int length, boolean defaultValue) {
        Boolean[] array = new Boolean[length];
        Arrays.fill(array, defaultValue);
        return array;
    }

    public static TillageType[] newArray(int length, TillageType defaultValue) {
        TillageType[] array = new TillageType[length];
        Arrays.fill(array, defaultValue);
        return array;
    }

    /**
     * Sum two double arrays.
     */
    public static Double[] sum(Double[] arr1, Double[] arr2) {
        Double[] arr3 = new Double[arr1.length];
        for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            arr3[i] = arr1[i] + arr2[i];
        }
        return arr3;
    }

    /**
     * Sum two boolean arrays.
     */
    public static Boolean[] sum(Boolean[] arr1, Boolean[] arr2) {
        Boolean[] arr3 = new Boolean[arr1.length];
        for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            arr3[i] = arr1[i] || arr2[i];
        }
        return arr3;
    }

    /**
     * "Somme" de 2 tableaux de TillageType.
     * <p>
     * Il y a un ordre de priorité à respecter pour les types. La méthode ne prend
     * en compte que 3 types : LABOUR, TCS et SEMIS_DIRECT. Les autres valeurs sont
     * celles qui seront calculées à l'échelle du système de culture, et le seront différemment.
     * <p>
     * L'ordre de priorité est le suivant :
     * <p>
     * - s'il y a au moins 1 Labour, alors on considère que c'est LABOUR
     * - sinon, s'il y a au moins 1 TCS, alors c'est TCS
     * - sinon si ce ne sont que semis direct, alors c'est SEMIS_DIRECT
     */
    public static TillageType sum(TillageType arr1, TillageType arr2) {
        TillageType result;

        if (arr1 == TillageType.LABOUR || arr2 == TillageType.LABOUR) {
            result = TillageType.LABOUR;
        } else if (arr1 == TillageType.TCS || arr2 == TillageType.TCS) {
            result = TillageType.TCS;
        } else {
            result = TillageType.SEMIS_DIRECT;
        }

        return result;
    }

    /**
     * Multiplication du vecteur par un scalaire.
     *
     * @param arr1 array
     * @param s    scalar
     * @return result
     */
    protected Double[] mults(Double[] arr1, double s) {
        Double[] array = new Double[arr1.length];
        for (int i = 0; i < arr1.length; ++i) {
            array[i] = arr1[i] * s;
        }
        return array;
    }

    /**
     * Division du vecteur par un scalaire.
     *
     * @param arr1 array
     * @param s    scalar
     * @return result
     */
    protected Double[] divs(Double[] arr1, double s) {
        Double[] array = new Double[arr1.length];
        for (int i = 0; i < arr1.length; ++i) {
            array[i] = s == 0 ? 0 : arr1[i] / s;
        }
        return array;
    }

    /**
     * Convert result into always array of double (even if there is only one value).
     *
     * @param val val (single double or multiple double, or array of double).
     * @return always array of double
     */
    protected Double[] newResult(Double... val) {
        return val;
    }
}
