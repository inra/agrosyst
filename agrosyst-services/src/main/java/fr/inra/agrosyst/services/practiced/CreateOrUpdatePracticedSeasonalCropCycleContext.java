package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Sector;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleConnectionDto;
import fr.inra.agrosyst.api.services.practiced.PracticedCropCycleNodeDto;
import fr.inra.agrosyst.api.services.practiced.PracticedSeasonalCropCycleDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;

/**
 * Created by davidcosse on 26/04/16.
 */
@Getter
@Setter
public class CreateOrUpdatePracticedSeasonalCropCycleContext {
    
    protected final Map<String, AbstractDomainInputStockUnit> domainInputStockByIds;
    protected final PracticedSeasonalCropCycle cycle;
    protected final PracticedSystem practicedSystem;
    protected final List<PracticedCropCycleNodeDto> nodeDtos;
    protected final List<PracticedCropCycleConnectionDto> connectionDtos;
    protected final PracticedSeasonalCropCycleDto practicedSeasonalCropCycleDto;
    protected final List<String> practicedSystemAvailableCropCodes;
    protected final String domainName;
    protected final String domainCode;

    protected final Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    protected final Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant;
    private final Map<String, CroppingPlanEntry> croppingPlanEntryMap;
    private final Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap;

    public CreateOrUpdatePracticedSeasonalCropCycleContext(
            PracticedSeasonalCropCycle cycle,
            PracticedSystem practicedSystem,
            List<PracticedCropCycleNodeDto> nodeDtos,
            List<PracticedCropCycleConnectionDto> connectionDtos,
            PracticedSeasonalCropCycleDto practicedSeasonalCropCycle,
            List<String> practicedSystemAvailaibleCropCodes,
            Domain domain,
            Map<String, List<Pair<String, String>>> speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee,
            Map<String, List<Sector>> sectorByCodeEspceBotaniqueCodeQualifiant,
            Map<String, AbstractDomainInputStockUnit> domainInputStockByIds,
            Map<String, CroppingPlanEntry> croppingPlanEntryMap,
            Map<String, CroppingPlanSpecies> croppingPlanSpeciesMap) {

        this.cycle = cycle;
        this.practicedSystem = practicedSystem;
        this.nodeDtos = nodeDtos;
        this.connectionDtos = connectionDtos;
        this.practicedSeasonalCropCycleDto = practicedSeasonalCropCycle;
        this.practicedSystemAvailableCropCodes = practicedSystemAvailaibleCropCodes;
        this.domainName = domain.getName();
        this.domainCode = domain.getCode();
        this.speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee = speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
        this.sectorByCodeEspceBotaniqueCodeQualifiant = sectorByCodeEspceBotaniqueCodeQualifiant;
        this.domainInputStockByIds = domainInputStockByIds;
        this.croppingPlanEntryMap = croppingPlanEntryMap;
        this.croppingPlanSpeciesMap = croppingPlanSpeciesMap;
    }

    public PracticedSeasonalCropCycle getCycle() {
        return cycle;
    }

    public PracticedSystem getPracticedSystem() {
        return practicedSystem;
    }

    public List<PracticedCropCycleNodeDto> getNodeDtos() {
        return nodeDtos;
    }

    public List<PracticedCropCycleConnectionDto> getConnectionDtos() {
        return connectionDtos;
    }

    public PracticedSeasonalCropCycleDto getPracticedSeasonalCropCycleDto() {
        return practicedSeasonalCropCycleDto;
    }

    public List<String> getPracticedSystemAvailableCropCodes() {
        return practicedSystemAvailableCropCodes;
    }

    public String getDomainName() {
        return domainName;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public Map<String, List<Pair<String, String>>> getSpeciesCodeToCodeEspeceBotaniqueCodeQualifiantAee() {
        return speciesCodeToCodeEspeceBotaniqueCodeQualifiantAee;
    }

    public Map<String, List<Sector>> getSectorByCodeEspceBotaniqueCodeQualifiant() {
        return sectorByCodeEspceBotaniqueCodeQualifiant;
    }
}
