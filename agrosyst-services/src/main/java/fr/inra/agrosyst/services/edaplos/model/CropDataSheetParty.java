package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.edaplos.EdaplosParsingPayload;
import fr.inra.agrosyst.services.edaplos.annotations.StrongValidSiret;
import fr.inra.agrosyst.services.edaplos.annotations.ValidSiret;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class CropDataSheetParty implements AgroEdiObject {

    @NotBlank(message = "Le SIRET du domaine est une information obligatoire or il est manquant dans le document")
    @ValidSiret(message = "Le SIRET du domaine est une information obligatoire or il n'est pas valide dans le document.", payload = EdaplosParsingPayload.ERROR.class)
    @StrongValidSiret(message = "Le SIRET n'est pas valide et devra être corrigé sur votre domaine.", payload = EdaplosParsingPayload.WARNING.class)
    protected String identification;

    protected String name;

    protected String description;

    @Valid
    @NotNull(message = "Le document ne contient pas les informations relatives à la localisation du domain (balise UnstructuredAddress)")
    protected UnstructuredAddress specifiedUnstructuredAddress;

    protected PartyContact definedPartyContact;

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification.replaceAll(" ","");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UnstructuredAddress getSpecifiedUnstructuredAddress() {
        return specifiedUnstructuredAddress;
    }

    public void setSpecifiedUnstructuredAddress(UnstructuredAddress specifiedUnstructuredAddress) {
        this.specifiedUnstructuredAddress = specifiedUnstructuredAddress;
    }

    public PartyContact getDefinedPartyContact() {
        return definedPartyContact;
    }

    public void setDefinedPartyContact(PartyContact definedPartyContact) {
        this.definedPartyContact = definedPartyContact;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "contact '" + identification + "'";
    }
}
