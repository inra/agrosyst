package fr.inra.agrosyst.services.performance.performancehelper;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.services.practiced.ReferenceDoseDTO;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.immutables.value.Value;

import java.util.Collection;
import java.util.Map;

/**
 * @author Cossé David : cosse@codelutin.com
 */
@Value
@Getter
@SuperBuilder(toBuilder = true)
public class WriterContext {
    
    protected final IndicatorWriter writer;
    protected final String its;
    protected final String irs;
    protected final String campaigns;
    protected final Domain anonymizeDomain;
    protected final GrowingSystem anonymizeGrowingSystem;
    
    protected final CroppingPlanEntry croppingPlanEntry;
    protected final Double solOccupationPercent;
    protected final Collection<String> codeAmmBioControle;
    protected final Map<String, String> groupesCiblesByCode;

    protected final Integer rank;
    protected final CroppingPlanEntry previousPlanEntry;
    protected final CroppingPlanEntry intermediateCrop;
    
    protected final PracticedSystem practicedSystem;
    protected final PracticedCropCyclePhase practicedPhase;
    protected final PracticedIntervention practicedIntervention;
    
    protected final Plot plot;
    protected final Zone zone;
    
    protected final EffectiveCropCyclePhase effectivePhase;
    protected final EffectiveIntervention effectiveIntervention;
    
    protected final ReferenceDoseDTO legacyActaDoseRef;
    
    public static WriterContext createPracticedPerennialWriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            String campaigns,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase phase,
            Double solOccupationPercent,
            PracticedIntervention intervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode
    ) {
        return new WriterContext(
                writer,
                its,
                irs,
                campaigns,
                anonymizeDomain,
                anonymizeGrowingSystem,
                practicedSystem,
                croppingPlanEntry,
                phase,
                solOccupationPercent,
                intervention,
                codeAmmBioControle,
                groupesCiblesByCode,
                null);
    }
    
    private WriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            String campaigns,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            PracticedCropCyclePhase phase,
            Double solOccupationPercent,
            PracticedIntervention intervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode,
            EffectiveIntervention effectiveIntervention) {
    
        this.writer = writer;
        this.its = its;
        this.irs = irs;
        this.campaigns = campaigns;
        this.anonymizeDomain = anonymizeDomain;
        this.anonymizeGrowingSystem = anonymizeGrowingSystem;
        this.practicedSystem = practicedSystem;
        this.croppingPlanEntry = croppingPlanEntry;
        this.practicedPhase = phase;
        this.solOccupationPercent = solOccupationPercent;
        this.practicedIntervention = intervention;
        this.codeAmmBioControle = codeAmmBioControle;
        this.groupesCiblesByCode = groupesCiblesByCode;
        this.effectiveIntervention = effectiveIntervention;
        rank = null;
        previousPlanEntry = null;
        intermediateCrop = null;
        plot = null;
        zone = null;
        effectivePhase = null;
        legacyActaDoseRef = null;
    }
    
    public static WriterContext createPracticedSeasonalWriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            String campaigns,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            Integer rank,
            CroppingPlanEntry previousPlanEntry,
            CroppingPlanEntry intermediateCrop,
            PracticedIntervention practicedIntervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode
    ) {
        return new WriterContext(writer,
                its,
                irs,
                campaigns,
                anonymizeDomain,
                anonymizeGrowingSystem,
                practicedSystem,
                croppingPlanEntry,
                rank,
                previousPlanEntry,
                intermediateCrop,
                practicedIntervention,
                codeAmmBioControle,
                groupesCiblesByCode,
                null);
    }
    
    private WriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            String campaigns,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            PracticedSystem practicedSystem,
            CroppingPlanEntry croppingPlanEntry,
            Integer rank,
            CroppingPlanEntry previousPlanEntry,
            CroppingPlanEntry intermediateCrop,
            PracticedIntervention intervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode,
            EffectiveIntervention effectiveIntervention) {
    
        this.writer = writer;
        this.its = its;
        this.irs = irs;
        this.campaigns = campaigns;
        this.anonymizeDomain = anonymizeDomain;
        this.anonymizeGrowingSystem = anonymizeGrowingSystem;
        this.practicedSystem = practicedSystem;
        this.croppingPlanEntry = croppingPlanEntry;
        this.rank = rank;
        this.previousPlanEntry = previousPlanEntry;
        this.intermediateCrop = intermediateCrop;
        this.practicedIntervention = intervention;
        this.codeAmmBioControle = codeAmmBioControle;
        this.groupesCiblesByCode = groupesCiblesByCode;
        this.effectiveIntervention = effectiveIntervention;
        practicedPhase = null;
        plot = null;
        zone = null;
        effectivePhase = null;
        solOccupationPercent = null;
        legacyActaDoseRef = null;
    }
    
    public static WriterContext createEffectivePerennialWriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            EffectiveCropCyclePhase phase,
            EffectiveIntervention intervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode
    ) {
        return new WriterContext(
                writer,
                its,
                irs,
                anonymizeDomain,
                anonymizeGrowingSystem,
                plot,
                zone,
                croppingPlanEntry,
                phase,
                intervention,
                codeAmmBioControle,
                groupesCiblesByCode,
                null);
    }
    
    private WriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            EffectiveCropCyclePhase phase,
            EffectiveIntervention intervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode,
            PracticedIntervention practicedIntervention) {
    
        this.writer = writer;
        this.its = its;
        this.irs = irs;
        this.anonymizeDomain = anonymizeDomain;
        this.anonymizeGrowingSystem = anonymizeGrowingSystem;
        this.plot = plot;
        this.zone = zone;
        this.croppingPlanEntry = croppingPlanEntry;
        this.effectivePhase = phase;
        this.effectiveIntervention = intervention;
        this.codeAmmBioControle = codeAmmBioControle;
        this.groupesCiblesByCode = groupesCiblesByCode;
        this.practicedIntervention = practicedIntervention;
        campaigns = String.valueOf(anonymizeDomain.getCampaign());
        rank = null;
        previousPlanEntry = null;
        intermediateCrop = null;
        practicedSystem = null;
        practicedPhase = null;
        solOccupationPercent = null;
        legacyActaDoseRef = null;
    }
    
    public static WriterContext createEffectiveSeasonalWriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            CroppingPlanEntry intermediateCrop,
            Integer rank,
            CroppingPlanEntry previousPlanEntry,
            EffectiveIntervention intervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode
    ) {
        return new WriterContext(
                writer,
                its,
                irs,
                anonymizeDomain,
                anonymizeGrowingSystem,
                plot,
                zone,
                croppingPlanEntry,
                intermediateCrop,
                rank,
                previousPlanEntry,
                intervention,
                codeAmmBioControle,
                groupesCiblesByCode,
                null);
    }
    
    private WriterContext(
            IndicatorWriter writer,
            String its,
            String irs,
            Domain anonymizeDomain,
            GrowingSystem anonymizeGrowingSystem,
            Plot plot,
            Zone zone,
            CroppingPlanEntry croppingPlanEntry,
            CroppingPlanEntry intermediateCrop,
            Integer rank,
            CroppingPlanEntry previousPlanEntry,
            EffectiveIntervention intervention,
            Collection<String> codeAmmBioControle,
            Map<String, String> groupesCiblesByCode,
            PracticedIntervention practicedIntervention) {
    
        this.writer = writer;
        this.its = its;
        this.irs = irs;
        this.anonymizeDomain = anonymizeDomain;
        this.anonymizeGrowingSystem = anonymizeGrowingSystem;
        this.plot = plot;
        this.zone = zone;
        this.croppingPlanEntry = croppingPlanEntry;
        this.intermediateCrop = intermediateCrop;
        this.rank = rank;
        this.previousPlanEntry = previousPlanEntry;
        this.effectiveIntervention = intervention;
        this.codeAmmBioControle = codeAmmBioControle;
        this.groupesCiblesByCode = groupesCiblesByCode;
        this.practicedIntervention = practicedIntervention;
        campaigns = String.valueOf(anonymizeDomain.getCampaign());
        practicedSystem = null;
        practicedPhase = null;
        effectivePhase = null;
        solOccupationPercent = null;
        legacyActaDoseRef = null;
    }
    
    public static WriterContext createNotUsedWriterContext() {
        return new WriterContext(null, null);
    }
    
    private WriterContext(PracticedIntervention practicedIntervention, EffectiveIntervention effectiveIntervention) {
        this.practicedIntervention = practicedIntervention;
        this.effectiveIntervention = effectiveIntervention;
        writer = null;
        its = null;
        irs = null;
        campaigns = null;
        anonymizeDomain = null;
        anonymizeGrowingSystem = null;
        croppingPlanEntry = null;
        codeAmmBioControle = null;
        groupesCiblesByCode = null;
        rank = null;
        previousPlanEntry = null;
        intermediateCrop = null;
        practicedSystem = null;
        practicedPhase = null;
        plot = null;
        zone = null;
        effectivePhase = null;
        solOccupationPercent = null;
        legacyActaDoseRef = null;
    }
    
    public String getInterventionId() {
        String interventionId = null;
        if (practicedIntervention != null) {
            interventionId = practicedIntervention.getTopiaId();
        } else if (effectiveIntervention != null) {
            interventionId = effectiveIntervention.getTopiaId();
        }
        return interventionId;
    }
    
    public String getInterventionName() {
        String interventionName = "NA";
        if (practicedIntervention != null) {
            interventionName = practicedIntervention.getName();
        } else if (effectiveIntervention != null) {
            interventionName = effectiveIntervention.getName();
        }
        return interventionName;
    }
    
//    public void setLegacyActaDoseRef(ReferenceDoseDTO legacyActaDoseRef) {
//        this.legacyActaDoseRef = legacyActaDoseRef;
//    }
}

