package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleNode;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMMTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenneTopiaDao;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.PracticedCropPath;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * refs #10240
 * tous les indicateurs sont affichés à tous les niveaux d'agrégation (= tous les onglets),
 * sauf la liste suivante qui ne sont pas affichés à l'échelle intervention :
 * - Produit brut réel sans l’autoconsommation
 * - Produit brut réel avec l’autoconsommation
 * - Produit brut standardisé avec autoconsommation
 * - Produit brut standardisé sans autoconsommation
 * - Produit brut sur scénarios avec autoconsommation
 * - Produit brut sur scénarios sans autoconsommation
 * - Marge brute réelle sans l’autoconsommation
 * - Marge brute réelle avec l’autoconsommation
 * - Marge brute standardisée avec autoconsommation
 * - Marge brute standardisée sans autoconsommation
 * - Marge brute sur scénarios avec autoconsommation
 * - Marge brute sur scénarios sans autoconsommation
 * <p>
 * Modifier Saisir temps Surveiller Copier Supprimer
 */
@Setter
public abstract class AbstractIndicator extends Indicator {

    private static final Log LOGGER = LogFactory.getLog(AbstractIndicator.class);

    protected boolean displayed = true;

    protected HarvestingPriceTopiaDao priceDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;

    protected RefInputUnitPriceUnitConverterTopiaDao refInputUnitPriceUnitConverterDao;
    protected RefCompositionSubstancesActivesParNumeroAMMTopiaDao refCompositionSubstancesActivesParNumeroAMMDao;
    protected RefSubstancesActivesCommissionEuropeenneTopiaDao refSubstancesActivesCommissionEuropeennesDao;

    protected EffectiveCropCycleService effectiveCropCycleService;
    protected PerformanceService performanceService;
    protected PricesService pricesService;
    protected ReferentialService referentialService;

    protected final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> effectiveCropsYealdAverage = new HashMap<>();

    protected final Map<EffectiveItkCropCycleScaleKey, Double[]> effectiveItkCropPrevCropPhaseValues = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkCropPrevCropPhaseTotalCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkCropPrevCropPhaseFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Set<MissingFieldMessage>> effectiveItkCropPrevCropPhaseFieldsErrors = new HashMap<>();

    protected final Map<EffectiveCropCycleScaleKey, Double[]> effectiveCroppingValues = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityTotalCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Set<MissingFieldMessage>> effectiveCroppingFieldsErrors = new HashMap<>();

    protected final Map<Zone, Double[]> effectiveZoneValues = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityTotalCounter = new HashMap<>();
    protected final Map<Zone, Set<MissingFieldMessage>> effectiveZoneFieldsErrors = new HashMap<>();

    protected final Map<Plot, Double[]> effectivePlotValues = new HashMap<>();
    protected final Map<Plot, Integer> effectivePlotReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Plot, Integer> effectivePlotReliabilityTotalCounter = new HashMap<>();

    protected final Map<Optional<GrowingSystem>, Double[]> effectiveGrowingSystemValues = new HashMap<>();
    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityTotalCounter = new HashMap<>();

    //campaigns, anonymizeGrowingSystem, ps
    protected final Map<PracticedSystemScaleKey, Double[]> practicedSystemsValues = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesTotalCounter = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesErrorCounter = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Set<MissingFieldMessage>> practicedSystemFieldsErrors = new HashMap<>();

    public abstract Double[] manageIntervention(WriterContext writerContext,
                                                PerformanceGlobalExecutionContext globalExecutionContext,
                                                PerformancePracticedDomainExecutionContext domainContext,
                                                PerformanceGrowingSystemExecutionContext growingSystemContext,
                                                PerformancePracticedSystemExecutionContext practicedSystemContext,
                                                PerformancePracticedCropExecutionContext cropContext,
                                                PerformancePracticedInterventionExecutionContext interventionContext,
                                                PracticedCropCyclePhase phase);

    public abstract Double[] manageIntervention(WriterContext writerContext,
                                                PerformanceGlobalExecutionContext globalExecutionContext,
                                                PerformanceEffectiveDomainExecutionContext domainContext,
                                                PerformanceZoneExecutionContext zoneContext,
                                                PerformanceEffectiveCropExecutionContext cropContext,
                                                PerformanceEffectiveInterventionExecutionContext interventionContext);

    @Override
    public void computePracticed(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedDomainExecutionContext domainContext) {

        if (growingSystemContext.getAnonymizeGrowingSystem().isEmpty()) {
            return;
        }

        Set<PerformancePracticedSystemExecutionContext> practicedSystemExecutionContexts = growingSystemContext.getPracticedSystemExecutionContexts();

        for (PerformancePracticedSystemExecutionContext practicedSystemExecutionContext : practicedSystemExecutionContexts) {

            computePracticedSeasonal(
                    writer,
                    globalExecutionContext,
                    domainContext,
                    practicedSystemExecutionContext,
                    growingSystemContext
            );

            computePracticedPerennial(
                    writer,
                    globalExecutionContext,
                    domainContext,
                    practicedSystemExecutionContext,
                    growingSystemContext
            );

        }

        writePracticedSystemSheet(writer, domainContext, growingSystemContext);

    }

    private void writePracticedPerennialCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
            Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors,
            GrowingSystem anonymizeGrowingSystem,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            List<Map.Entry<MultiKey<?>, Double[]>> practicedPerennialCroppingValues) {

        for (Map.Entry<MultiKey<?>, Double[]> entry : practicedPerennialCroppingValues) {
            // perennial: cropCode, phase
            MultiKey<?> practicedCropingKey = entry.getKey();
            String croppingPlanEntryCode = (String) practicedCropingKey.getKey(0);
            PracticedCropCyclePhase previousPlanEntryCodeOrPhase = (PracticedCropCyclePhase) practicedCropingKey.getKey(1);

            Double[] values = entry.getValue();

            final int campaign = domainContext.getDomain().getCampaign();
            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode().get(campaign, croppingPlanEntryCode);
            if (croppingPlanEntry == null) {
                final CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }
            final Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = cropsYealdAverage.get(croppingPlanEntryCode);

            final String campaigns = anonymizePracticedSystem.getCampaigns();

            Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCropingKey);
            Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCropingKey);

            Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);

            Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCropingKey);
            String comments = CollectionUtils.isEmpty(missingFields) ?
                    RELIABILITY_INDEX_NO_COMMENT :
                    missingFields.stream().
                            map(MissingFieldMessage::getMessage).
                            collect(Collectors.joining(", "));

            Double perennialCropCyclePercent = perennialCropCyclePercentByCrop.get(croppingPlanEntry);

            for (int i = 0; i < values.length; i++) {
                boolean isDisplayed = isDisplayed(ExportLevel.CROP, i);
                if (isDisplayed) {
                    // write crop sheet
                    writer.writePracticedPerennialCop(
                            its,
                            irs,
                            campaigns,
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            values[i],
                            cropYealdAverage,
                            reliabilityIndexForCrop,
                            comments,
                            domainContext.getAnonymiseDomain(),
                            anonymizeGrowingSystem,
                            anonymizePracticedSystem,
                            croppingPlanEntry,
                            previousPlanEntryCodeOrPhase,
                            perennialCropCyclePercent,
                            this.getClass());
                }
            }
        }
    }

    private void writePracticedSeasonalCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage,
            MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors,
            GrowingSystem anonymizeGrowingSystem,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            List<Map.Entry<MultiKey<?>, Double[]>> practicedSeasonalCroppingValues) {

        for (Map.Entry<MultiKey<?>, Double[]> entry : practicedSeasonalCroppingValues) {
            // seasonal:  cropCode, previousPlanEntryCode, rank, cropConnection
            MultiKey<?> practicedCropingKey = entry.getKey();
            String croppingPlanEntryCode = (String) practicedCropingKey.getKey(0);
            Object previousPlanEntryCode = practicedCropingKey.getKey(1);
            int rank = (Integer) practicedCropingKey.getKey(2);
            PracticedCropCycleConnection cropConnection = (PracticedCropCycleConnection) practicedCropingKey.getKey(3);

            Double[] values = entry.getValue();

            final int campaign = domainContext.getDomain().getCampaign();
            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode().get(campaign, croppingPlanEntryCode);
            if (croppingPlanEntry == null) {
                final CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }
            final Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = cropsYealdAverage.get(croppingPlanEntryCode);

            final String campaigns = anonymizePracticedSystem.getCampaigns();
            CroppingPlanEntry previousCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(campaign, previousPlanEntryCode);

            if (previousCroppingPlanEntry == null) {
                final String cropCode = (String) previousPlanEntryCode;
                final CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(cropCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }

            Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCropingKey);
            Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCropingKey);

            Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);

            Double cummulativeFrequencies = cumulativeFrequenciesByPracticedCropCycleConnectionCode.get(croppingPlanEntry,
                    previousCroppingPlanEntry,
                    rank,
                    cropConnection);
            cummulativeFrequencies = cummulativeFrequencies == null ? 0 : cummulativeFrequencies;

            Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCropingKey);
            Set<String> errors = missingFields != null ? missingFields.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet()) : new HashSet<>();

            String reliabilityComments = CollectionUtils.isEmpty(missingFields) ? RELIABILITY_INDEX_NO_COMMENT :
                    String.join(", ", errors);

            for (int i = 0; i < values.length; i++) {
                boolean isDisplayed = isDisplayed(ExportLevel.CROP, i);
                if (isDisplayed) {
                    // write crop sheet
                    writer.writePracticedSeasonalCrop(
                            its,
                            irs,
                            campaigns,
                            cummulativeFrequencies,
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            values[i],
                            cropYealdAverage,
                            reliabilityIndexForCrop,
                            reliabilityComments,
                            rank,
                            anonymizeGrowingSystem.getGrowingPlan().getDomain(),
                            anonymizeGrowingSystem,
                            anonymizePracticedSystem,
                            croppingPlanEntry,
                            previousCroppingPlanEntry,
                            cropConnection.getTopiaId(),
                            this.getClass(),
                            cropConnection);
                }
            }
        }
    }

    protected void writePracticedSystemSheet(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();
        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        if (optionalGrowingSystem.isEmpty()) return;
        GrowingSystem anonymizeGrowingSystem = optionalGrowingSystem.get();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        for (Map.Entry<PracticedSystemScaleKey, Double[]> entry : practicedSystemsValues.entrySet()) {

            PracticedSystemScaleKey key = entry.getKey();
            String campaigns = key.campaigns();
            GrowingSystem anonymizeGrowingSystem0 = key.growingSystem();
            PracticedSystem anonymizePracticedSystem = key.practicedSystem();

            if (!anonymizeGrowingSystem0.getCode().contentEquals(anonymizeGrowingSystem.getCode())) {
                continue;// filter on practiced system related to the current growing system
            }

            Double[] values = entry.getValue();
            int nbValues = values.length;

            Integer psREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(key);
            Integer psRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(key);
            Set<MissingFieldMessage> fieldsErrorMessages = practicedSystemFieldsErrors.get(key);

            Integer reliability = computeReliabilityIndex(psREC, psRTC);

            String messages;
            if (CollectionUtils.isEmpty(fieldsErrorMessages)) {
                messages = RELIABILITY_INDEX_NO_COMMENT;
            } else {
                messages = StringUtils.join(fieldsErrorMessages, ", ");
            }

            // CA Practice System scale
            for (int i = 0; i < nbValues; i++) {
                boolean isDisplayed = isDisplayed(ExportLevel.PRACTICED_SYSTEM, i);
                if (isDisplayed) {
                    // write practiced system sheet
                    writer.writePracticedSystem(its,
                            irs,
                            campaigns,
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            values[i],
                            reliability,
                            messages,
                            anonymizeDomain,
                            anonymizeGrowingSystem,
                            anonymizePracticedSystem,
                            this.getClass());
                }
            }
        }
    }

    protected void computePracticedPerennial(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {

        long chronoT0 = System.currentTimeMillis();

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedSystemExecutionContext.getPracticedPerennialCropCycles();

        // perennial
        if (CollectionUtils.isEmpty(practicedPerennialCropCycles)) {
            return;
        }

        Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.nonNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performancePracticedCropContextExecutionContexts)) {
            return;// no crop in cycle
        }

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();

        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        if (optionalGrowingSystem.isEmpty()) return;

        GrowingSystem anonymizeGrowingSystem = optionalGrowingSystem.get();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        PracticedSystem practicedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();


        String campaigns = practicedSystem.getCampaigns();
        Set<Integer> psCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(campaigns);

        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, anonymizeGrowingSystem, practicedSystem);

        // surface totale d'occupation des cultures pérennes sur le SDC, devrait-être égal à 100.
        Double totalSolOccupationPercent = 0d;
        for (PracticedPerennialCropCycle cycle : practicedPerennialCropCycles) {
            totalSolOccupationPercent += cycle.getSolOccupationPercent();
        }
        totalSolOccupationPercent = totalSolOccupationPercent > 0d ? totalSolOccupationPercent : null;

        Map<PracticedPerennialCropCycle, Double[]> practicedSystemCycleValues = new HashMap<>();
        for (PerformancePracticedCropExecutionContext cropContext : performancePracticedCropContextExecutionContexts) {

            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();
            final MultiKeyMap<Object, Double[]> practicedCroppingValues = new MultiKeyMap<>();
            final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();
            final Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop = new HashMap<>();

            // crop, previousCrop, rank, cropConnection -> value
            PracticedPerennialCropCycle cycle = cropContext.getPracticedPerennialCropCycle();

            Set<PerformancePracticedInterventionExecutionContext> interventionExecutionContexts = cropContext.getInterventionExecutionContexts();

            CroppingPlanEntry crop = cropContext.getCropWithSpecies().getCroppingPlanEntry();
            String cropCode = crop.getCode();

            // First we try to bring back the crop from the campaign of the domain from the growing plan from the growing system from the practiced system
            CroppingPlanEntry campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(practicedSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign(), cropCode);
            // If none, we iterate over the campaigns of the practiced system to find one
            if (campaignCroppingPlanEntry == null) {
                final Iterator<Integer> iterator = psCampaigns.iterator();
                while (iterator.hasNext() && campaignCroppingPlanEntry == null) {
                    campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(iterator.next(), cropCode);
                }
            }

            if (campaignCroppingPlanEntry != null) {
                crop = campaignCroppingPlanEntry;
            } else {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(String.format("For practicedSystem %s Crop with code %s could not be find into domain code %s for campaigns %s",
                            practicedSystem.getTopiaId(),
                            crop.getCode(),
                            anonymizeDomain.getCode(),
                            campaigns));
                }
            }

            final double solOccupationPercent = cycle.getSolOccupationPercent();
            perennialCropCyclePercentByCrop.put(crop, solOccupationPercent);

            for (PerformancePracticedInterventionExecutionContext interventionContext : interventionExecutionContexts) {

                if (interventionContext.isFictive()) {
                    continue;
                }

                PracticedIntervention intervention = interventionContext.getIntervention();

                if (intervention.getPracticedCropCyclePhase() == null) {
                    continue;
                }

                final String interventionId = intervention.getTopiaId();

                PracticedCropCyclePhase phase = intervention.getPracticedCropCyclePhase();

                WriterContext writerContext = WriterContext.createPracticedPerennialWriterContext(
                        writer,
                        its,
                        irs,
                        campaigns,
                        anonymizeDomain,
                        anonymizeGrowingSystem,
                        practicedSystem,
                        crop,
                        phase,
                        solOccupationPercent,
                        intervention,
                        practicedSystemExecutionContext.getCodeAmmBioControle(),
                        globalExecutionContext.getGroupesCiblesParCode()
                );
                Double[] interValues = manageIntervention(
                        writerContext,
                        globalExecutionContext,
                        domainContext,
                        growingSystemContext,
                        practicedSystemExecutionContext,
                        cropContext,
                        interventionContext,
                        phase);

                if (interValues != null) {

                    Set<MissingFieldMessage> interventionWarnings;
                    Set<MissingFieldMessage> cropMissingFieldMessages;


                    Collection<AbstractInputUsage> inputUsages = getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomain);

                    InterventionFieldCounterGetter interventionFieldCounterGetter =
                            new InterventionFieldCounterGetter(
                                    interventionId,
                                    inputUsages,
                                    interventionContext.getOptionalIrrigationAction(),
                                    interventionContext.getOptionalSeedingActionUsage())
                                    .invoke();

                    AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
                    AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

                    int reliabilityIndexForIntervention =
                            computeReliabilityIndex(
                                    missingFieldCounterValueForIntervention.get(),
                                    totalFieldCounterForIntervention.get());

                    if (isRelevant(ExportLevel.INTERVENTION)) {
                        // sortie résultat courant
                        final List<AbstractAction> actions = interventionContext.getActions();

                        final String reliabilityCommentForIntervention =
                                getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                        final List<String> iftReferencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                        final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                        for (int i = 0; i < interValues.length; i++) {
                            boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, i);
                            if (isDisplayed) {
                                // write intervention sheet
                                writer.writePracticedPerennialIntervention(
                                        its,
                                        irs,
                                        campaigns,
                                        getIndicatorCategory(),
                                        getIndicatorLabel(i),
                                        interValues[i],
                                        reliabilityIndexForIntervention,
                                        reliabilityCommentForIntervention,
                                        iftReferencesDosagesUserInfos,
                                        anonymizeDomain,
                                        anonymizeGrowingSystem,
                                        practicedSystem,
                                        crop,
                                        phase,
                                        solOccupationPercent,
                                        intervention,
                                        actions,
                                        practicedSystemExecutionContext.getCodeAmmBioControle(),
                                        interventionYealdAverage,
                                        globalExecutionContext.getGroupesCiblesParCode(),
                                        this.getClass()
                                );
                            }
                        }

                        // trow up to crop the missing fields warning that must be displayed at crop level
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = interventionWarnings == null ? new HashSet<>() :
                                interventionWarnings.stream()
                                        .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent())
                                        .collect(Collectors.toSet());

                    } else {
                        // trow up to crop all the missing fields warning (as well messages that are not at crop level displayed)
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = Optional.ofNullable(interventionWarnings).orElse(new HashSet<>());
                    }

                    Set<MissingFieldMessage> missingFieldsForCrop = practicedCroppingFieldsErrors.get(practicedSystem, cropCode, phase);

                    // trow up to crop the missing fields warning
                    if (missingFieldsForCrop == null) {
                        missingFieldsForCrop = new HashSet<>();
                    }

                    if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                        missingFieldsForCrop.addAll(cropMissingFieldMessages);
                    }

                    practicedCroppingFieldsErrors.put(practicedSystem, cropCode, phase, cropMissingFieldMessages);

                    // somme pour CC (pour toutes les cultures du cycle)
                    Double[] previous = practicedCroppingValues.get(cropCode, phase);
                    if (previous == null) {
                        practicedCroppingValues.put(cropCode, phase, interValues);
                        if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                            cropsYealdAverage.put(cropCode, cropContext.getIntermediateCropYealds());
                        } else {
                            cropsYealdAverage.put(cropCode, cropContext.getMainCropYealds());
                        }
                    } else {
                        Double[] sum = sum(previous, interValues);
                        practicedCroppingValues.put(cropCode, phase, sum);
                    }

                    // somme pour CC
                    final Integer prevPhaseReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(
                            practicedSystem,
                            cropCode,
                            phase);

                    final int totalFieldCounterValueForCrop =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevPhaseReliabilityTotalCount, 0);

                    practicedCroppingReliabilityTotalCounter.put(
                            practicedSystem,
                            cropCode,
                            phase,
                            totalFieldCounterValueForCrop);

                    final Integer prevPhaseMissingFieldCounter =
                            practicedCroppingReliabilityFieldErrorCounter.get(
                                    practicedSystem,
                                    cropCode,
                                    phase);

                    final int phaseMissingFieldCounter =
                            missingFieldCounterValueForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevPhaseMissingFieldCounter, 0);

                    practicedCroppingReliabilityFieldErrorCounter.put(
                            practicedSystem,
                            cropCode,
                            phase,
                            phaseMissingFieldCounter);

                    Double[] psPreviousValue = practicedSystemCycleValues.get(cycle);
                    if (psPreviousValue == null) {
                        practicedSystemCycleValues.put(cycle, interValues);
                    } else {
                        Double[] sum = sum(psPreviousValue, interValues);
                        practicedSystemCycleValues.put(cycle, sum);
                    }

                    // practicedSystem scale
                    final Integer previousPracticedSystemRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(practicedSystemScaleKey);

                    final int totalFieldCounterValueForPracticedSystem =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(previousPracticedSystemRTC, 0);

                    reliabilityIndexPracticedSystemValuesTotalCounter.put(practicedSystemScaleKey, totalFieldCounterValueForPracticedSystem);

                    Integer previousPracticedSystemREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(practicedSystemScaleKey);

                    final int missingFieldCounterValueForGS = missingFieldCounterValueForIntervention.get() +
                            ObjectUtils.firstNonNull(previousPracticedSystemREC, 0);

                    reliabilityIndexPracticedSystemValuesErrorCounter.put(practicedSystemScaleKey, missingFieldCounterValueForGS);

                    Set<MissingFieldMessage> messagesAtPracticedScope = practicedSystemFieldsErrors.computeIfAbsent(practicedSystemScaleKey, k -> new HashSet<>());

                    // trow up to practiced system the missing fields warning that must be displayed at Practiced System level
                    Set<MissingFieldMessage> practicedMissingFieldMessages = interventionWarnings == null ?
                            new HashSet<>() :
                            interventionWarnings.stream().
                                    filter(missingFieldMessage ->
                                            missingFieldMessage.getMessageForScope(MissingMessageScope.PRACTICED_SYSTEM).isPresent())
                                    .collect(Collectors.toSet());

                    if (CollectionUtils.isNotEmpty(practicedMissingFieldMessages)) {
                        messagesAtPracticedScope.addAll(practicedMissingFieldMessages);
                    }


                }
            } // fin des interventions

            PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();
            List<Map.Entry<MultiKey<?>, Double[]>> practicedPerennialCroppingValues = practicedCroppingValues.entrySet().stream()
                    .filter(entry -> {
                        final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                        return previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase;
                    })
                    .collect(Collectors.toList());

            writePracticedPerennialCroppingValues(
                    writer,
                    domainContext,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingReliabilityTotalCounter,
                    perennialCropCyclePercentByCrop,
                    cropsYealdAverage,
                    practicedSystemExecutionContext,
                    practicedCroppingFieldsErrors,
                    anonymizeGrowingSystem,
                    its,
                    irs,
                    anonymizePracticedSystem,
                    practicedPerennialCroppingValues);

        }// fin des cycle de cultures

        // mise à l'échelle vers Practiced System, on tient compte de la part des cultures pérennes dans le sdc


        for (Map.Entry<PracticedPerennialCropCycle, Double[]> entry : practicedSystemCycleValues.entrySet()) {

            Double[] pspreviousValue = practicedSystemsValues.get(practicedSystemScaleKey);

            Double[] cropPart = addPerennialCropPart(entry.getKey().getSolOccupationPercent(), totalSolOccupationPercent, entry.getValue());

            if (pspreviousValue == null) {
                practicedSystemsValues.put(practicedSystemScaleKey, cropPart);
            } else {
                Double[] sum = sum(pspreviousValue, cropPart);
                practicedSystemsValues.put(practicedSystemScaleKey, sum);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getSimpleName() + ": " + (System.currentTimeMillis() - chronoT0) + " ms");
        }

    }

    protected void computePracticedSeasonal(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext
    ) {

        long chronoT0 = System.currentTimeMillis();

        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSystemExecutionContext.getSeasonalCropCycle();

        // seasonal
        if (practicedSeasonalCropCycle == null) {
            return;
        }

        if (CollectionUtils.isEmpty(practicedSeasonalCropCycle.getCropCycleNodes())) {
            return;
        }

        Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts_ =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.isNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performancePracticedCropContextExecutionContexts_)) {
            return;// no crop in cycle
        }

        List<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts = performancePracticedCropContextExecutionContexts_
                .stream()
                .sorted(Comparator.comparingInt(PerformancePracticedCropExecutionContext::getRank)).toList();

        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        if (optionalGrowingSystem.isEmpty()) return;

        GrowingSystem anonymizeGrowingSystem = optionalGrowingSystem.get();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        PracticedSystem practicedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();

        String campaigns = practicedSystem.getCampaigns();
        Set<Integer> psCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(campaigns);

        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, anonymizeGrowingSystem, practicedSystem);

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();

        boolean seasonalInterventionFound = false;

        Map<PracticedCropCycleConnection, Double[]> cycleValues = new HashMap<>();

        // compute at crop cycle level
        for (PerformancePracticedCropExecutionContext cropContext : performancePracticedCropContextExecutionContexts) {

            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();
            final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();

            // crop, previousCrop, rank, cropConnection -> value
            MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode = new MultiKeyMap<>();

            // seasonal:  cropCode, previousPlanEntryCode, rank, cropConnection
            // perennial: cropCode, phase
            MultiKeyMap<Object, Double[]> practicedCroppingValues = new MultiKeyMap<>();

            Set<PerformancePracticedInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();
            if (CollectionUtils.isEmpty(interventionContexts)) continue; // NOTHING TO DO

            CropWithSpecies intermediateCropWithSpecies = cropContext.getIntermediateCropWithSpecies();
            CroppingPlanEntry crop = cropContext.getCropWithSpecies().getCroppingPlanEntry();
            final String cropCode = crop.getCode();
            {
                // First we try to bring back the crop from the campaign of the domain from the growing plan from the growing system from the practiced system
                CroppingPlanEntry campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(practicedSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign(), cropCode);
                // If none, we iterate over the campaigns of the practiced system to find one
                if (campaignCroppingPlanEntry == null) {
                    final Iterator<Integer> iterator = psCampaigns.iterator();
                    while (iterator.hasNext() && campaignCroppingPlanEntry == null) {
                        campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(iterator.next(), cropCode);
                    }
                }
                if (campaignCroppingPlanEntry != null) {
                    crop = campaignCroppingPlanEntry;
                } else {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(String.format("For practicedSystem %s Crop with code %s could not be find into domain code %s for campaigns %s",
                                practicedSystem.getTopiaId(),
                                cropCode,
                                anonymizeDomain.getCode(),
                                campaigns));
                    }
                }
            }

            CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();
            {
                CroppingPlanEntry previousPlanEntry = cropContext.getSeasonalPreviousCrop();
                if (previousCrop != null) {
                    previousPlanEntry = domainContext.getCropByCampaignAndCode().get(domainContext.getDomain().getCampaign(), previousPlanEntry.getCode());
                }
                if (previousCrop != null) {
                    previousCrop = previousPlanEntry;
                } else {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(String.format("For practicedSystem %s Crop with code %s could not be find into domain code %s for campaigns %s",
                                practicedSystem.getTopiaId(),
                                cropCode,
                                anonymizeDomain.getCode(),
                                campaigns));
                    }
                }
            }
            String previousPlanEntryCode = previousCrop != null ? previousCrop.getCode() : "";

            PracticedCropCycleConnection cropConnection = cropContext.getConnection();
            CroppingPlanEntry intermediateCrop = intermediateCropWithSpecies != null ?
                    intermediateCropWithSpecies.getCroppingPlanEntry() :
                    null;

            int rank = cropContext.getRank();

            // compute at intervention level
            for (PerformancePracticedInterventionExecutionContext interventionContext : interventionContexts) {

                PracticedIntervention intervention = interventionContext.getIntervention();
                String interventionId = null;
                Double[] interValues;

                if (!interventionContext.isFictive()) {

                    if (intervention.getPracticedCropCyclePhase() != null) {
                        continue;
                    }
                    seasonalInterventionFound = true;

                    interventionId = interventionContext.getInterventionId();

                }

                WriterContext writerContext = WriterContext.createPracticedSeasonalWriterContext(
                        writer,
                        its,
                        irs,
                        campaigns,
                        anonymizeDomain,
                        anonymizeGrowingSystem,
                        practicedSystem,
                        crop,
                        rank,
                        previousCrop,
                        intermediateCrop,
                        intervention,
                        practicedSystemExecutionContext.getCodeAmmBioControle(),
                        globalExecutionContext.getGroupesCiblesParCode()
                );

                interValues = manageIntervention(
                        writerContext,
                        globalExecutionContext,
                        domainContext,
                        growingSystemContext,
                        practicedSystemExecutionContext,
                        cropContext,
                        interventionContext,
                        null);

                if (interValues != null) {

                    // sortie résultat courant
                    Set<MissingFieldMessage> cropMissingFieldMessages;
                    Set<MissingFieldMessage> interventionWarnings;

                    Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomain);

                    InterventionFieldCounterGetter interventionFieldCounterGetter =
                            new InterventionFieldCounterGetter(
                                    interventionId,
                                    inputUsages,
                                    interventionContext.getOptionalIrrigationAction(),
                                    interventionContext.getOptionalSeedingActionUsage())
                                    .invoke();

                    AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
                    AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

                    int reliabilityIndexForIntervention =
                            computeReliabilityIndex(
                                    missingFieldCounterValueForIntervention.get(),
                                    totalFieldCounterForIntervention.get());

                    if (!interventionContext.isFictive() &&
                            this.isRelevant(ExportLevel.INTERVENTION)) {

                        CroppingPlanEntry interventionIntermediateCrop = null;
                        if (intervention.isIntermediateCrop()) {
                            interventionIntermediateCrop = intermediateCrop;
                        }

                        List<String> iftReferencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                        final List<AbstractAction> actions = interventionContext.getActions();

                        final String reliabilityCommentForInterventionId =
                                getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                        final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                        for (int i = 0; i < interValues.length; i++) {
                            boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, i);
                            if (isDisplayed) {
                                // write intervention sheet
                                writer.writePracticedSeasonalIntervention(
                                        its,
                                        irs,
                                        campaigns,
                                        getIndicatorCategory(),
                                        getIndicatorLabel(i),
                                        interValues[i],
                                        reliabilityIndexForIntervention,
                                        reliabilityCommentForInterventionId,
                                        iftReferencesDosagesUserInfos,
                                        anonymizeDomain,
                                        anonymizeGrowingSystem,
                                        practicedSystem,
                                        crop,
                                        rank,
                                        previousCrop,
                                        interventionIntermediateCrop,
                                        intervention,
                                        actions,
                                        practicedSystemExecutionContext.getCodeAmmBioControle(),
                                        interventionYealdAverage,
                                        globalExecutionContext.getGroupesCiblesParCode(),
                                        this.getClass()
                                );
                            }
                        }

                        // trow up to crop the missing fields warning that must be displayed at crop level
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = interventionWarnings == null ?
                                new HashSet<>() :
                                interventionWarnings.stream()
                                        .filter(missingFieldMessage ->
                                                missingFieldMessage.getMessageForScope(MissingMessageScope.CROP)
                                                        .isPresent())
                                        .collect(Collectors.toSet());

                    } else {
                        // trow up to crop all the missing fields warning (as well messages that are not at crop level displayed)
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = ObjectUtils.firstNonNull(interventionWarnings, new HashSet<>());
                    }

                    // trow up to crop and practicedSystem the missing fields warning
                    String interventionCroppingPlanEntryCode = crop.getCode();

                    Set<MissingFieldMessage> messagesAtCropScope = practicedCroppingFieldsErrors.get(interventionCroppingPlanEntryCode, previousPlanEntryCode, rank);

                    // trow up to crop the missing fields warning
                    if (messagesAtCropScope == null) {
                        messagesAtCropScope = new HashSet<>();
                    }

                    if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                        messagesAtCropScope.addAll(cropMissingFieldMessages);
                    }

                    practicedCroppingFieldsErrors.put(interventionCroppingPlanEntryCode, previousPlanEntryCode, rank, cropMissingFieldMessages);

                    // somme pour CC le cycle (ensemble des cultures du cycle)
                    Double[] previous = practicedCroppingValues.get(interventionCroppingPlanEntryCode, previousPlanEntryCode, rank, cropConnection);
                    // ici
                    if (previous == null) {
                        practicedCroppingValues.put(cropCode, previousPlanEntryCode, rank, cropConnection, interValues);// ici
                        if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                            cropsYealdAverage.put(interventionCroppingPlanEntryCode, cropContext.getIntermediateCropYealds());
                        } else {
                            cropsYealdAverage.put(interventionCroppingPlanEntryCode, cropContext.getMainCropYealds());
                        }
                    } else {
                        Double[] sum = sum(previous, interValues);
                        practicedCroppingValues.put(interventionCroppingPlanEntryCode, previousPlanEntryCode, rank, cropConnection, sum);//ici
                    }

                    // somme pour la culture seulement (toutes les interventions de la culture)
                    PracticedCropCycleConnection connection = interventionContext.getPracticedCropCycleConnection();
                    Double[] previousValue = cycleValues.get(connection);
                    if (previousValue == null) {
                        cycleValues.put(connection, interValues);
                    } else {
                        Double[] sum = sum(previousValue, interValues);
                        cycleValues.put(connection, sum);
                    }

                    // somme pour CC
                    final Integer prevConnectionReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection);

                    final int totalFieldCounterValueForCrop =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevConnectionReliabilityTotalCount, 0);

                    practicedCroppingReliabilityTotalCounter.put(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            totalFieldCounterValueForCrop);

                    final Integer prevConnectionReliabilityErrorCounter = practicedCroppingReliabilityFieldErrorCounter.get(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection);

                    final int connectionMissingFieldCounter =
                            missingFieldCounterValueForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevConnectionReliabilityErrorCounter, 0);

                    practicedCroppingReliabilityFieldErrorCounter.put(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            connectionMissingFieldCounter);

                    // practicedSystem scale
                    Integer previousPracticedSystemRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(practicedSystemScaleKey);

                    final int totalFieldCounterValueForPracticedSystem =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(previousPracticedSystemRTC, 0);

                    reliabilityIndexPracticedSystemValuesTotalCounter.put(practicedSystemScaleKey, totalFieldCounterValueForPracticedSystem);

                    final Integer previousPracticedSystemREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(practicedSystemScaleKey);

                    final int missingFieldCounterValueForGS = missingFieldCounterValueForIntervention.get() +
                            ObjectUtils.firstNonNull(previousPracticedSystemREC, 0);

                    reliabilityIndexPracticedSystemValuesErrorCounter.put(practicedSystemScaleKey, missingFieldCounterValueForGS);

                    Set<MissingFieldMessage> messagesAtPracticedScope = practicedSystemFieldsErrors.computeIfAbsent(practicedSystemScaleKey, k -> new HashSet<>());

                    // trow up to practiced system the missing fields warning that must be displayed at Practiced System level
                    Set<MissingFieldMessage> practicedMissingFieldMessages = interventionWarnings == null ?
                            new HashSet<>() :
                            interventionWarnings.stream().
                                    filter(missingFieldMessage ->
                                            missingFieldMessage.getMessageForScope(MissingMessageScope.PRACTICED_SYSTEM).isPresent()).
                                    collect(Collectors.toSet());
                    if (CollectionUtils.isNotEmpty(practicedMissingFieldMessages)) {
                        messagesAtPracticedScope.addAll(practicedMissingFieldMessages);
                    }

                }
            } // end of intervention

            cumulativeFrequenciesByPracticedCropCycleConnectionCode.put(
                    crop,
                    previousCrop,
                    rank,
                    cropConnection,
                    cropContext.getCummulativeFrequencyForCrop());

            if (intermediateCrop != null) {
                cumulativeFrequenciesByPracticedCropCycleConnectionCode.put(
                        intermediateCrop,
                        previousCrop,
                        rank,
                        cropConnection,
                        cropContext.getCummulativeFrequencyForCrop());
            }

            List<Map.Entry<MultiKey<?>, Double[]>> practicedSeasonalCroppingValues = practicedCroppingValues.entrySet().stream()
                    .filter(entry -> {
                        final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                        return !(previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase);
                    })
                    .sorted(
                            Comparator.comparingInt(entry -> {
                                final Integer _rank = (Integer) entry.getKey().getKey(2);
                                return _rank;
                            })
                    )
                    .collect(Collectors.toList());

            PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();

            writePracticedSeasonalCroppingValues(
                    writer,
                    domainContext,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingReliabilityTotalCounter,
                    cropsYealdAverage,
                    cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                    practicedSystemExecutionContext,
                    practicedCroppingFieldsErrors,
                    anonymizeGrowingSystem,
                    its,
                    irs,
                    anonymizePracticedSystem,
                    practicedSeasonalCroppingValues
            );

        }// end of cycle

        // compute at practiced system scale
        if (seasonalInterventionFound) {

            Set<PracticedCropPath> practicedCropPaths = practicedSystemExecutionContext.getPracticedCropPaths();

            Map<PracticedCropPath, Double[]> valuesByRoad = computeValuesByRoad(cycleValues, practicedCropPaths);

            if (!valuesByRoad.isEmpty()) {
                Double[] weightedAverageValueSum = computeWeightedAverageValueSum(valuesByRoad);
                double weightedAveragePathSize = computeWeightedAveragePathSize(valuesByRoad);
                computeSeasonalResults(practicedSystemScaleKey, weightedAverageValueSum, weightedAveragePathSize);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getSimpleName() + ": " + (System.currentTimeMillis() - chronoT0) + " ms");
        }

    }

    protected void computeSeasonalResults(PracticedSystemScaleKey practicedSystemScaleKey, Double[] weightedAverageValueSum,
                                          double weightedAveragePathSize) {

        Double[] result;
        if (weightedAverageValueSum != null && weightedAveragePathSize > 0.0d) {
            result = divs(weightedAverageValueSum, weightedAveragePathSize);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(this.getClass().getSimpleName() + " - result: " + result[0]);
            }

            Double[] pspreviousValue = practicedSystemsValues.get(practicedSystemScaleKey);
            if (pspreviousValue == null) {
                practicedSystemsValues.put(practicedSystemScaleKey, result);
            } else {
                Double[] sum = sum(pspreviousValue, result);
                practicedSystemsValues.put(practicedSystemScaleKey, sum);
            }
        }
    }

    protected Double[] computeWeightedAverageValueSum(Map<PracticedCropPath, Double[]> valuesByRoad) {
        Double[] result = null;
        // on multiplie la somme des valeurs par chemin par le poids du chemin
        for (Map.Entry<PracticedCropPath, Double[]> practicedCropPathEntry : valuesByRoad.entrySet()) {
            Double[] practicedCropValues = practicedCropPathEntry.getValue();
            if (practicedCropValues != null) {
                PracticedCropPath path = practicedCropPathEntry.getKey();
                if (result == null) {
                    result = mults(practicedCropValues, path.getFinalFrequency());
                } else {
                    result = sum(result, mults(practicedCropValues, path.getFinalFrequency()));
                }
            }
        }
        return result;
    }

    protected double computeWeightedAveragePathSize(Map<PracticedCropPath, Double[]> valuesByRoad) {
        // Lors du calcul de la longueur de chaque branche (en nombre de campagnes),
        // il faut prendre en compte les cultures qui sont déclarées comme étant "même campagne que la culture précédente"
        return valuesByRoad.keySet().stream()
                .mapToDouble(path -> path.getFinalFrequency() *
                        Math.max(path.getConnections().stream()
                                .filter(connection -> !connection.isNotUsedForThisCampaign())
                                .map(PracticedCropCycleConnection::getTarget)
                                .filter(practicedCropCycleNode -> !practicedCropCycleNode.isSameCampaignAsPreviousNode())
                                .count(), 1))
                .sum();
    }

    protected Map<PracticedCropPath, Double[]> computeValuesByRoad(
            Map<PracticedCropCycleConnection, Double[]> cycleValues,
            Set<PracticedCropPath> practicedCropPaths) {

        Map<PracticedCropPath, Double[]> valueByRoad = new HashMap<>();
        // moyenne pour CA
        for (Map.Entry<PracticedCropCycleConnection, Double[]> entry : cycleValues.entrySet()) {
            Double[] values = entry.getValue();
            PracticedCropCycleConnection connection = entry.getKey();

            List<PracticedCropPath> pathsWithConnection = practicedCropPaths.stream()
                    .filter(path -> path.getConnections().contains(connection)).toList();

            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace(this.getClass().getSimpleName());
                LOGGER.trace(this.getClass().getSimpleName() + " Freq: " + connection.getCroppingPlanEntryFrequency());
                LOGGER.trace(this.getClass().getSimpleName() + " Source: X:" + connection.getSource().getRank() + " Y:" + connection.getSource().getY());
                LOGGER.trace(this.getClass().getSimpleName() + " Source Code: " + (connection.getSource() != null ? connection.getSource().getCroppingPlanEntryCode() : ""));
                LOGGER.trace(this.getClass().getSimpleName() + " Target: X:" + connection.getTarget().getRank() + " Y:" + connection.getTarget().getY());
                LOGGER.trace(this.getClass().getSimpleName() + " Target Code: " + connection.getTarget().getCroppingPlanEntryCode());

                for (PracticedCropPath practicedCropPath : pathsWithConnection) {
                    String path = practicedCropPath.getConnections().stream()
                            .map(PracticedCropCycleConnection::getTarget)
                            .sorted(Comparator.comparing(PracticedCropCycleNode::getRank))
                            .map(PracticedCropCycleNode::getCroppingPlanEntryCode)
                            .collect(Collectors.joining(" -> "));
                    double[] freqs = practicedCropPath.getConnections().stream()
                            .mapToDouble(PracticedCropCycleConnection::getCroppingPlanEntryFrequency)
                            .toArray();
                    List<String> freqs0 = new ArrayList<>();
                    for (double freq : freqs) {
                        freqs0.add(String.valueOf(freq));
                    }
                    LOGGER.trace("freqs: " + StringUtils.join(IndicatorWriter.SEPARATOR, freqs0));
                    LOGGER.trace("Path: " + path + " Path frequency:" + practicedCropPath.getFinalFrequency());
                }
            }

            // on fait la somme des valeurs par chemin
            for (PracticedCropPath practicedCropPath : practicedCropPaths) {
                Double[] previousValueForRoad = valueByRoad.get(practicedCropPath);
                if (previousValueForRoad == null) {
                    valueByRoad.put(practicedCropPath, pathsWithConnection.contains(practicedCropPath) ? values : null);
                } else if (pathsWithConnection.contains(practicedCropPath)) {
                    Double[] sum = sum(previousValueForRoad, values);
                    valueByRoad.put(practicedCropPath, sum);
                }
            }

        }
        return valueByRoad;
    }

    @Override
    public void computePracticed(IndicatorWriter writer, Domain domain) {

        Map<String, Double[]> weightedValueSum = Maps.newLinkedHashMap();
        Map<String, Double[]> weightSum = Maps.newLinkedHashMap();

        Map<String, Pair<Integer, Integer>> domainCampaignRelaibilityCount = Maps.newLinkedHashMap();

        List<String> gstc = new ArrayList<>();

        Double usedAgriculturalArea = domain.getUsedAgriculturalArea();

        final String domainTopiaId = domain.getTopiaId();
        if (usedAgriculturalArea == null) {
            addMissingFieldMessage(domainTopiaId, messageBuilder.getDomainMissingSAUMessage());
        }

        for (Map.Entry<PracticedSystemScaleKey, Double[]> entry : practicedSystemsValues.entrySet()) {

            Double[] values = entry.getValue();
            int nbValues = values.length;

            final PracticedSystemScaleKey key = entry.getKey();
            String campaigns = key.campaigns();
            GrowingSystem growingSystem = key.growingSystem();

            if (growingSystem.getTypeAgriculture() != null)
                gstc.add(growingSystem.getTypeAgriculture().getReference_label());

            Integer psREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(key);
            Integer psRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(key);

            Pair<Integer, Integer> dcrc = domainCampaignRelaibilityCount.computeIfAbsent(campaigns, k -> Pair.of(psREC, psRTC));

            Double affectedAreaRate = growingSystem.getAffectedAreaRate();

            if (affectedAreaRate == null) {
                addMissingFieldMessage(domainTopiaId, messageBuilder.getDomainMissingAffectedAreaMessage());
            }

            Integer ef = dcrc.getLeft() + (usedAgriculturalArea == null ? 1 : 0) + (affectedAreaRate == null ? 1 : 0);
            Integer tf = dcrc.getRight() + 2;

            domainCampaignRelaibilityCount.put(campaigns, Pair.of(ef, tf));

            // somme des valeurs pondérée
            if (!weightedValueSum.containsKey(campaigns)) {
                // init table
                weightedValueSum.put(campaigns, newArray(nbValues, 0.0));
            }

            // somme des poids
            if (!weightSum.containsKey(campaigns)) {
                // init table
                weightSum.put(campaigns, newArray(nbValues, 0.0));
            }

            usedAgriculturalArea = usedAgriculturalArea == null ? DEFAULT_USED_AGRICULTURAL_AREA : usedAgriculturalArea;
            affectedAreaRate = affectedAreaRate == null ? DEFAULT_AFFECTED_AREA_RATE : affectedAreaRate;
            for (int i = 0; i < nbValues; i++) {
                weightedValueSum.get(campaigns)[i] += values[i] * usedAgriculturalArea * affectedAreaRate;
                weightSum.get(campaigns)[i] += usedAgriculturalArea * affectedAreaRate;
            }

        }

        for (Map.Entry<String, Double[]> weightedValueEntry : weightedValueSum.entrySet()) {
            String campaigns = weightedValueEntry.getKey();
            Double[] values = weightedValueEntry.getValue();
            Double[] weightValues = weightSum.get(campaigns);

            Pair<Integer, Integer> reliabilityErrorAndTotal = domainCampaignRelaibilityCount.get(campaigns);
            Integer practicedDomainREC = reliabilityErrorAndTotal.getLeft();
            Integer practicedDomainRTC = reliabilityErrorAndTotal.getRight();

            String comments = getMessagesForScope(MissingMessageScope.DOMAIN);

            Integer reliability = computeReliabilityIndex(practicedDomainREC, practicedDomainRTC);

            for (int i = 0; i < values.length; i++) {
                if (weightValues[i] != 0) {
                    // domain scale
                    double weightedAverage = values[i] / weightValues[i];
                    boolean isDisplayed = isDisplayed(ExportLevel.DOMAIN, i);
                    if (isDisplayed) {
                        // write domain sheet
                        writer.writePracticedDomain(campaigns,
                                getIndicatorCategory(),
                                getIndicatorLabel(i),
                                weightedAverage,
                                reliability,
                                comments,
                                domain,
                                String.join(", ", gstc));
                    }
                } else {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn("Can't compute growing system scale with 0 weigth");
                    }
                }
            }
        }
    }

    @Override
    public void resetPracticed(Domain domain) {
        // cleanup for next iteration
        practicedSystemsValues.clear();
        reliabilityIndexPracticedSystemValuesTotalCounter.clear();
        reliabilityIndexPracticedSystemValuesErrorCounter.clear();
        practicedSystemFieldsErrors.clear();
        targetedErrorFieldMessages.clear();
    }

    /*
     * @see fr.inra.agrosyst.services.performance.indicators.Indicator#computeEffective(
     * fr.inra.agrosyst.services.performance.IndicatorWriter,
     * fr.inra.agrosyst.api.entities.Domain,
     * fr.inra.agrosyst.api.entities.GrowingSystem,
     * fr.inra.agrosyst.api.entities.plot, fr.inra.agrosyst.api.entities.Zone)
     */
    @Override
    public void computeEffective(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        computeEffectivePerennial(writer, globalExecutionContext, domainContext, zoneContext);

        computeEffectiveSeasonal(writer, globalExecutionContext, domainContext, zoneContext);

        // la map effectiveZoneValues est valuée plusieurs fois avec plusieurs zones par plusieurs
        // appels pour la mise à l'échelle de la methode suivante
        // computeEffective(IndicatorWriter writer, Domain domain, GrowingSystem growingSystem, plot plot)
        // on ne génère une ligne dans le fichier de sortie que pour la zone courante (s'il y a des valeurs)
        // et non pour toutes les zones
        writeEffectiveZone(writer, domainContext, zoneContext);

    }

    protected void writeEffectiveZone(
            IndicatorWriter writer,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        Zone anonymizeZone = zoneContext.getAnonymizeZone();
        Double[] values = effectiveZoneValues.get(anonymizeZone);
        if (values != null) {
            Integer rmfzc = effectiveZoneReliabilityFieldErrorCounter.get(anonymizeZone);
            Integer rtzc = effectiveZoneReliabilityTotalCounter.get(anonymizeZone);
            Integer reliabilityIndex = computeReliabilityIndex(rmfzc, rtzc);

            Optional<GrowingSystem> optionalAnoGrowingSystem = zoneContext.getAnonymizeGrowingSystem();

            String speciesName = zoneContext.getZoneSpeciesNames();
            String varietyNames = zoneContext.getZoneVarietiesNames();

            String its = zoneContext.getIts();
            String irs = zoneContext.getIrs();

            final Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald = zoneContext.getZoneAverageYeald();

            final Plot plot = anonymizeZone.getPlot();
            // zone scale

            String comments;
            Set<MissingFieldMessage> fieldMessages = effectiveZoneFieldsErrors.get(anonymizeZone);
            if (CollectionUtils.isNotEmpty(fieldMessages)) {
                Set<String> errors = fieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            } else {
                comments = RELIABILITY_INDEX_NO_COMMENT;
            }

            //    void writeEffective(String irs, String its, int campaign, String indicatorCategory, String indicatorName,
            //    Double value, String printableYealdAverage, Integer reliabilityIndex, String comment, Domain domain, GrowingSystem growingSystem,
            //    Plot plot, Zone zone, String speciesNames, String varietyNames);
            for (int i = 0; i < values.length; i++) {
                boolean isDisplayed = isDisplayed(ExportLevel.ZONE, i);
                if (isDisplayed) {
                    // write zone sheet
                    writer.writeEffectiveZone(
                            its,
                            irs,
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            values[i],
                            zoneAverageYeald,
                            reliabilityIndex,
                            comments,
                            domainContext.getAnonymiseDomain(),
                            domainContext.getCroppingPlanSpecies(),
                            optionalAnoGrowingSystem,
                            plot,
                            anonymizeZone,
                            speciesName,
                            varietyNames,
                            this.getClass());
                }
            }

            for (Map.Entry<EffectiveItkCropCycleScaleKey, Double[]> effectiveItkCropPrevCropPhaseKeyEntry : effectiveItkCropPrevCropPhaseValues.entrySet()) {

                EffectiveItkCropCycleScaleKey key = effectiveItkCropPrevCropPhaseKeyEntry.getKey();
                Double[] itkValues = effectiveItkCropPrevCropPhaseKeyEntry.getValue();
                Integer effectiveItkCropPrevCropPhaseTotalCount = effectiveItkCropPrevCropPhaseTotalCounter.get(key);
                Integer effectiveItkCropPrevCropPhaseFieldErrorCount = effectiveItkCropPrevCropPhaseFieldErrorCounter.get(key);
                Integer effectiveItkCropPrevCropPhaseReliabilityIndex = computeReliabilityIndex(effectiveItkCropPrevCropPhaseFieldErrorCount, effectiveItkCropPrevCropPhaseTotalCount);

                String effectiveItkCropPrevCropPhaseComments;
                Set<MissingFieldMessage> effectiveItkCropPrevCropPhaseFieldMessages = effectiveItkCropPrevCropPhaseFieldsErrors.get(key);
                if (CollectionUtils.isNotEmpty(effectiveItkCropPrevCropPhaseFieldMessages)) {
                    Set<String> errors = effectiveItkCropPrevCropPhaseFieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                    effectiveItkCropPrevCropPhaseComments = String.join(IndicatorWriter.SEPARATOR, errors);
                } else {
                    effectiveItkCropPrevCropPhaseComments = RELIABILITY_INDEX_NO_COMMENT;
                }

                for (int i = 0; i < itkValues.length; i++) {
                    boolean isDisplayed = isDisplayed(ExportLevel.ZONE, i);
                    if (isDisplayed) {
                        // write zone sheet
                        writer.writeEffectiveItk(
                                its,
                                irs,
                                getIndicatorCategory(),
                                getIndicatorLabel(i),
                                itkValues[i],
                                effectiveItkCropPrevCropPhaseReliabilityIndex,
                                effectiveItkCropPrevCropPhaseComments,
                                domainContext.getAnonymiseDomain(),
                                optionalAnoGrowingSystem,
                                plot,
                                anonymizeZone,
                                this.getClass(),
                                key);
                    }
                }

            }
        }

        effectiveItkCropPrevCropPhaseValues.clear();
        effectiveItkCropPrevCropPhaseTotalCounter.clear();
        effectiveItkCropPrevCropPhaseFieldErrorCounter.clear();
        effectiveItkCropPrevCropPhaseFieldsErrors.clear();
    }

    protected void computeEffectiveSeasonal(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        EffectiveSeasonalCropCycle seasonalCropCycle = zoneContext.getSeasonalCropCycle();

        if (seasonalCropCycle == null) {
            return;
        }

        Set<PerformanceEffectiveCropExecutionContext> cropContexts_ = zoneContext.getPerformanceCropContextExecutionContexts()
                .stream()
                .filter(
                        performanceCropExecutionContext ->
                                Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                ))
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(cropContexts_)) {
            return;// no crop in cycle
        }

        List<PerformanceEffectiveCropExecutionContext> cropContexts = cropContexts_
                .stream()
                .sorted(Comparator.comparingInt(PerformanceEffectiveCropExecutionContext::getRank)).toList();

        for (PerformanceEffectiveCropExecutionContext cropContext : cropContexts) {

            Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();

            if (CollectionUtils.isEmpty(interventionContexts)) continue; // NOTHING TO DO

            for (PerformanceEffectiveInterventionExecutionContext interventionContext : interventionContexts) {

                EffectiveIntervention intervention = interventionContext.getIntervention();

                if (intervention.getEffectiveCropCyclePhase() != null) {
                    continue;
                }

                computeEffectiveInterventionSheet(writer, globalExecutionContext, domainContext, zoneContext, cropContext, interventionContext);
            }

        }
    }

    protected void computeEffectivePerennial(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        List<EffectivePerennialCropCycle> perennialCycles = zoneContext.getPerennialCropCycles();

        if (CollectionUtils.isEmpty(perennialCycles)) {
            return;
        }

        Set<PerformanceEffectiveCropExecutionContext> performanceCropContextExecutionContexts = zoneContext.getPerformanceCropContextExecutionContexts()
                .stream()
                .filter(
                        performanceCropExecutionContext ->
                                !Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                ))
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performanceCropContextExecutionContexts)) {
            return;// no crop in cycle
        }

        Optional<GrowingSystem> optionalAnoGrowingSystem = zoneContext.getAnonymizeGrowingSystem();
        Domain anonymizeDomaine = domainContext.getAnonymiseDomain();
        Plot anonymizePlot = zoneContext.getAnonymizePlot();
        Zone anonymizeZone = zoneContext.getAnonymizeZone();

        String its = zoneContext.getIts();
        String irs = zoneContext.getIrs();

        for (PerformanceEffectiveCropExecutionContext cropContext : performanceCropContextExecutionContexts) {

            Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();

            CroppingPlanEntry crop = cropContext.getCrop();

            for (PerformanceEffectiveInterventionExecutionContext interventionContext : interventionContexts) {

                EffectiveIntervention intervention = interventionContext.getIntervention();

                if (intervention.getEffectiveCropCyclePhase() == null) {
                    continue;
                }

                String interventionId = intervention.getTopiaId();

                EffectiveCropCyclePhase phase = intervention.getEffectiveCropCyclePhase();

                WriterContext writerContext = WriterContext.createEffectivePerennialWriterContext(
                        writer,
                        its,
                        irs,
                        anonymizeDomaine,
                        optionalAnoGrowingSystem.orElse(null),
                        anonymizePlot,
                        anonymizeZone,
                        crop,
                        phase,
                        intervention,
                        domainContext.getCodeAmmBioControle(),
                        globalExecutionContext.getGroupesCiblesParCode()
                );
                Double[] interValues = manageIntervention(
                        writerContext,
                        globalExecutionContext,
                        domainContext,
                        zoneContext,
                        cropContext,
                        interventionContext);

                if (interValues != null) {

                    zoneContext.addCropConcernByIndicator(this.getClass(), crop);

                    Set<MissingFieldMessage> interventionWarnings;
                    Set<MissingFieldMessage> cropMissingFieldMessages;

                    Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomaine);

                    InterventionFieldCounterGetter interventionFieldCounterGetter =
                            new InterventionFieldCounterGetter(
                                    interventionId,
                                    inputUsages,
                                    interventionContext.getOptionalIrrigationAction(),
                                    interventionContext.getOptionalSeedingActionUsage())
                                    .invoke();

                    AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
                    AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

                    int reliabilityIndexForIntervention =
                            computeReliabilityIndex(
                                    missingFieldCounterValueForIntervention.get(),
                                    totalFieldCounterForIntervention.get());

                    if (this.isRelevant(ExportLevel.INTERVENTION)) {

                        final List<AbstractAction> actions = interventionContext.getActions();

                        final String reliabilityCommentForIntervention =
                                getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                        final List<String> referencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                        final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                        for (int i = 0; i < interValues.length; i++) {
                            boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, i);
                            if (isDisplayed) {
                                // write intervention sheet
                                writer.writeEffectivePerennialIntervention(
                                        its,
                                        irs,
                                        getIndicatorCategory(),
                                        getIndicatorLabel(i),
                                        interValues[i],
                                        reliabilityIndexForIntervention,
                                        reliabilityCommentForIntervention,
                                        referencesDosagesUserInfos,
                                        anonymizeDomaine,
                                        optionalAnoGrowingSystem,
                                        anonymizePlot,
                                        anonymizeZone,
                                        crop,
                                        phase,
                                        intervention,
                                        actions,
                                        domainContext.getCodeAmmBioControle(),
                                        interventionYealdAverage,
                                        globalExecutionContext.getGroupesCiblesParCode(),
                                        this.getClass());
                            }
                        }

                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = interventionWarnings == null ?
                                new HashSet<>() :
                                interventionWarnings.stream()
                                        .filter(missingFieldMessage ->
                                                missingFieldMessage.getMessageForScope(MissingMessageScope.CROP)
                                                        .isPresent())
                                        .collect(Collectors.toSet());

                    } else {

                        // trow up to crop the missing fields warning
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = ObjectUtils.firstNonNull(interventionWarnings, new HashSet<>());
                    }


                    EffectiveCropCycleScaleKey effectivePerennialCropKey =
                            new EffectiveCropCycleScaleKey(
                                    crop,
                                    Optional.empty(),
                                    Optional.empty(),
                                    Optional.of(phase));

                    aggregateToCropScale(
                            intervention,
                            effectivePerennialCropKey,
                            cropContext,
                            crop,
                            interValues,
                            cropMissingFieldMessages,
                            totalFieldCounterForIntervention,
                            missingFieldCounterValueForIntervention);

                    // somme pour CA
                    effectiveZoneValues.merge(anonymizeZone, interValues, GenericIndicator::sum);

                    final int totalFieldCounterValueForCrop = effectiveCroppingReliabilityTotalCounter.get(effectivePerennialCropKey);
                    effectiveZoneReliabilityTotalCounter.merge(
                            anonymizeZone,
                            totalFieldCounterValueForCrop, Integer::sum);

                    final int phaseMissingFieldCounter = effectiveCroppingReliabilityFieldErrorCounter.get(effectivePerennialCropKey);
                    effectiveZoneReliabilityFieldErrorCounter.merge(
                            anonymizeZone,
                            phaseMissingFieldCounter,
                            Integer::sum);

                    Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings == null ?
                            new HashSet<>() :
                            interventionWarnings.stream().
                                    filter(
                                            missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.ZONE)
                                                    .isPresent())
                                    .collect(Collectors.toSet());

                    Set<MissingFieldMessage> missingFieldsForZone = effectiveZoneFieldsErrors.computeIfAbsent(anonymizeZone, k -> new HashSet<>());
                    missingFieldsForZone.addAll(zoneMissingFieldMessages);

                    EffectiveItkCropCycleScaleKey effectiveItkCropCycleScaleKey =
                            new EffectiveItkCropCycleScaleKey(
                                    optionalAnoGrowingSystem,
                                    anonymizePlot,
                                    anonymizeZone,
                                    crop,
                                    Optional.empty(),
                                    Optional.empty(),
                                    Optional.of(phase),
                                    Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                            );

                    effectiveItkCropPrevCropPhaseValues.merge(
                            effectiveItkCropCycleScaleKey,
                            interValues,
                            GenericIndicator::sum);

                    effectiveItkCropPrevCropPhaseTotalCounter.merge(
                            effectiveItkCropCycleScaleKey,
                            totalFieldCounterValueForCrop,
                            Integer::sum);

                    effectiveItkCropPrevCropPhaseFieldErrorCounter.merge(
                            effectiveItkCropCycleScaleKey,
                            phaseMissingFieldCounter,
                            Integer::sum);

                    // on ajoute ceux de la zone ?
                    Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropCycleScaleKey, k -> new HashSet<>());
                    missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);

                }
            }
        }
    }

    protected void computeEffectiveInterventionSheet(IndicatorWriter writer,
                                                     PerformanceGlobalExecutionContext globalExecutionContext,
                                                     PerformanceEffectiveDomainExecutionContext domainContext,
                                                     PerformanceZoneExecutionContext zoneContext,
                                                     PerformanceEffectiveCropExecutionContext cropContext,
                                                     PerformanceEffectiveInterventionExecutionContext interventionContext) {

        Domain anonymizeDomaine = domainContext.getAnonymiseDomain();

        EffectiveIntervention intervention = interventionContext.getIntervention();

        if (intervention.getEffectiveCropCyclePhase() != null) {
            return;
        }

        Optional<GrowingSystem> optionalAnonGrowingSystem = zoneContext.getAnonymizeGrowingSystem();
        Plot anonymizePlot = zoneContext.getAnonymizePlot();
        Zone anonymizeZone = zoneContext.getAnonymizeZone();

        CroppingPlanEntry crop = cropContext.getCrop();
        CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();
        CroppingPlanEntry intermediateCrop = cropContext.getIntermediateCrop();
        int rank = cropContext.getRank();

        String its = zoneContext.getIts();
        String irs = zoneContext.getIrs();

        String interventionId = intervention.getTopiaId();
        // compute indicator
        WriterContext writerContext = WriterContext.createEffectiveSeasonalWriterContext(
                writer,
                its,
                irs,
                anonymizeDomaine,
                optionalAnonGrowingSystem.orElse(null),
                anonymizePlot,
                anonymizeZone,
                crop,
                intermediateCrop,
                rank,
                previousCrop,
                intervention,
                domainContext.getCodeAmmBioControle(),
                globalExecutionContext.getGroupesCiblesParCode()
        );
        Double[] interValues = manageIntervention(
                writerContext,
                globalExecutionContext,
                domainContext,
                zoneContext,
                cropContext,
                interventionContext);

        if (interValues != null) {

            zoneContext.addCropConcernByIndicator(this.getClass(), crop);

            Set<MissingFieldMessage> interventionWarnings;
            Set<MissingFieldMessage> cropMissingFieldMessages;

            Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomaine);

            InterventionFieldCounterGetter interventionFieldCounterGetter =
                    new InterventionFieldCounterGetter(
                            interventionId,
                            inputUsages,
                            interventionContext.getOptionalIrrigationAction(),
                            interventionContext.getOptionalSeedingActionUsage())
                            .invoke();

            AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
            AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

            int reliabilityIndexForIntervention =
                    computeReliabilityIndex(
                            missingFieldCounterValueForIntervention.get(),
                            totalFieldCounterForIntervention.get());

            if (isRelevant(ExportLevel.INTERVENTION)) {

                final List<AbstractAction> actions = interventionContext.getActions();

                final String reliabilityCommentForIntervention =
                        getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                final List<String> referencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                for (int i = 0; i < interValues.length; i++) {
                    boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, i);
                    if (isDisplayed) {
                        // write intervention sheet
                        writer.writeEffectiveSeasonalIntervention(
                                its,
                                irs,
                                anonymizeDomaine.getCampaign(),
                                getIndicatorCategory(),
                                getIndicatorLabel(i),
                                interValues[i],
                                reliabilityIndexForIntervention,
                                reliabilityCommentForIntervention,
                                referencesDosagesUserInfos,
                                anonymizeDomaine,
                                optionalAnonGrowingSystem,
                                anonymizePlot,
                                anonymizeZone,
                                crop,
                                intermediateCrop,
                                rank,
                                previousCrop,
                                intervention,
                                actions,
                                domainContext.getCodeAmmBioControle(),
                                interventionYealdAverage,
                                globalExecutionContext.getGroupesCiblesParCode(),
                                this.getClass());
                    }
                }

                interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                cropMissingFieldMessages = interventionWarnings == null ?
                        new HashSet<>() :
                        interventionWarnings.stream()
                                .filter(missingFieldMessage ->
                                        missingFieldMessage.getMessageForScope(MissingMessageScope.CROP)
                                                .isPresent())
                                .collect(Collectors.toSet());

            } else {
                // trow up to crop the missing fields warning
                interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                cropMissingFieldMessages = ObjectUtils.firstNonNull(interventionWarnings, new HashSet<>());
            }

            EffectiveCropCycleScaleKey effectiveSeasonalCropKey =
                    new EffectiveCropCycleScaleKey(
                            interventionContext.getCroppingPlanEntry(),
                            Optional.ofNullable(previousCrop),
                            Optional.of(rank),
                            Optional.empty());

            aggregateToCropScale(
                    intervention,
                    effectiveSeasonalCropKey,
                    cropContext,
                    interventionContext.getCroppingPlanEntry(),
                    interValues,
                    cropMissingFieldMessages,
                    totalFieldCounterForIntervention,
                    missingFieldCounterValueForIntervention
            );

            // somme pour CA
            effectiveZoneValues.merge(anonymizeZone, interValues, GenericIndicator::sum);

            final int totalFieldCounterValueForCrop = effectiveCroppingReliabilityTotalCounter.get(effectiveSeasonalCropKey);
            effectiveZoneReliabilityTotalCounter.merge(
                    anonymizeZone,
                    totalFieldCounterValueForCrop, Integer::sum);

            final int rankMissingFieldCounter = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveSeasonalCropKey);
            effectiveZoneReliabilityFieldErrorCounter.merge(
                    anonymizeZone,
                    rankMissingFieldCounter,
                    Integer::sum);

            Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings == null ?
                    new HashSet<>() :
                    interventionWarnings.stream().
                            filter(
                                    missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.ZONE).isPresent())
                            .collect(Collectors.toSet());

            Set<MissingFieldMessage> missingFieldsForZone = effectiveZoneFieldsErrors.computeIfAbsent(anonymizeZone, k -> new HashSet<>());
            missingFieldsForZone.addAll(zoneMissingFieldMessages);

            EffectiveItkCropCycleScaleKey effectiveItkCropCycleScaleKey =
                    new EffectiveItkCropCycleScaleKey(
                            optionalAnonGrowingSystem,
                            anonymizePlot,
                            anonymizeZone,
                            interventionContext.getCroppingPlanEntry(),
                            Optional.ofNullable(previousCrop),
                            Optional.of(rank),
                            Optional.empty(),
                            Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                    );

            effectiveItkCropPrevCropPhaseValues.merge(
                    effectiveItkCropCycleScaleKey,
                    interValues,
                    GenericIndicator::sum);

            effectiveItkCropPrevCropPhaseTotalCounter.merge(
                    effectiveItkCropCycleScaleKey,
                    totalFieldCounterValueForCrop,
                    Integer::sum);

            effectiveItkCropPrevCropPhaseFieldErrorCounter.merge(
                    effectiveItkCropCycleScaleKey,
                    rankMissingFieldCounter,
                    Integer::sum);

            // on ajoute ceux de la zone ?
            Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropCycleScaleKey, k -> new HashSet<>());
            missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);

        }
    }

    private void aggregateToCropScale(
            EffectiveIntervention intervention,
            EffectiveCropCycleScaleKey effectiveCropKey,
            PerformanceEffectiveCropExecutionContext cropContext,
            CroppingPlanEntry crop,
            Double[] interValues,
            Set<MissingFieldMessage> cropMissingFieldMessages,
            AtomicReference<Integer> totalFieldCounterForIntervention,
            AtomicReference<Integer> missingFieldCounterValueForIntervention) {

        Set<MissingFieldMessage> missingFieldsForCrop = effectiveCroppingFieldsErrors
                .computeIfAbsent(effectiveCropKey, k -> new HashSet<>());

        // trow up to crop the missing fields warning
        if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
            missingFieldsForCrop.addAll(cropMissingFieldMessages);
        }

        if (intervention.isIntermediateCrop()) {
            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getIntermediateCropYealds());
        } else {
            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getMainCropYealds());
        }

        // somme pour CC (pour toutes les cultures du cycle)
        effectiveCroppingValues.merge(
                effectiveCropKey,
                interValues,
                GenericIndicator::sum);

        // somme pour CC
        Integer prevRankReliabilityTotalCount = effectiveCroppingReliabilityTotalCounter.get(effectiveCropKey);

        final int totalFieldCounterValue =
                totalFieldCounterForIntervention.get() +
                        ObjectUtils.firstNonNull(prevRankReliabilityTotalCount, 0);

        effectiveCroppingReliabilityTotalCounter.put(effectiveCropKey, totalFieldCounterValue);

        final Integer prevMissingFieldCounter = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCropKey);

        final int missingFieldCounter =
                missingFieldCounterValueForIntervention.get() +
                        ObjectUtils.firstNonNull(prevMissingFieldCounter, 0);

        effectiveCroppingReliabilityFieldErrorCounter.put(effectiveCropKey, missingFieldCounter);
    }

    protected List<String> getReferencesDosagesUserInfos(PerformanceInterventionContext interventionContext) {
        return Collections.emptyList();
    }

    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 Optional<GrowingSystem> optionalGrowingSystem,
                                 PerformancePlotExecutionContext plotContext) {

        Plot anonymisePlot = plotContext.getAnonymizePlot();

        Double[] weightedValueSum = null;
        Double[] weightSum = null;

        Integer reliabilityPlotTotal = 0;
        Integer reliabilityPlotMissingFileld = 0;
        Set<MissingFieldMessage> plotMissingFieldMessages = new HashSet<>();

        Set<String> zoneTopiaIds = new HashSet<>();
        // moyenne pondérée sur la surface
        for (Map.Entry<Zone, Double[]> entry : effectiveZoneValues.entrySet()) {
            Zone zone = entry.getKey();
            zoneTopiaIds.add(zone.getTopiaId());
            Double[] values = entry.getValue();

            reliabilityPlotTotal += effectiveZoneReliabilityTotalCounter.get(zone);
            reliabilityPlotMissingFileld += effectiveZoneReliabilityFieldErrorCounter.get(zone);
            final Set<MissingFieldMessage> fieldMessages = effectiveZoneFieldsErrors.get(zone);
            Set<MissingFieldMessage> zoneMissingFieldMessages = fieldMessages != null ?
                    fieldMessages.stream().
                            filter(
                                    missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.PLOT).isPresent())
                            .collect(Collectors.toSet()) :
                    new HashSet<>();

            plotMissingFieldMessages.addAll(zoneMissingFieldMessages);

            if (weightedValueSum == null) {
                weightedValueSum = newArray(values.length, 0.0);
                weightSum = newArray(values.length, 0.0);
            }

            // zoneArea
            reliabilityPlotTotal += 1;
            for (int i = 0; i < values.length; i++) {
                weightedValueSum[i] += values[i] == null ? 0 : values[i] * zone.getArea();
                weightSum[i] += zone.getArea();
            }
        }

        if (weightSum != null) {
            String zoneIds = String.join(", ", zoneTopiaIds);
            final Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages = plotContext.getPlotYealdAverages();

            String its = plotContext.getIts();
            String irs = plotContext.getIrs();

            Integer plotReliability = computeReliabilityIndex(reliabilityPlotMissingFileld, reliabilityPlotTotal);
            effectivePlotReliabilityFieldErrorCounter.put(anonymisePlot, reliabilityPlotMissingFileld);
            effectivePlotReliabilityTotalCounter.put(anonymisePlot, reliabilityPlotTotal);

            effectivePlotValues.put(anonymisePlot, newArray(weightedValueSum.length, 0.0));

            String comments;
            if (CollectionUtils.isNotEmpty(plotMissingFieldMessages)) {
                Set<String> errors = plotMissingFieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            } else {
                comments = RELIABILITY_INDEX_NO_COMMENT;
            }

            for (int i = 0; i < weightedValueSum.length; i++) {
                double plotValue = weightSum[i] == 0 ? 0 : weightedValueSum[i] / weightSum[i];
                boolean isDisplayed = isDisplayed(ExportLevel.PLOT, i);
                if (isDisplayed) {
                    // write plot sheet
                    writer.writeEffectivePlot(
                            its,
                            irs,
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            plotValue,
                            plotReliability,
                            comments,
                            domainContext.getAnonymiseDomain(),
                            domainContext.getCroppingPlanSpecies(),
                            optionalGrowingSystem,
                            anonymisePlot,
                            plotYealdAverages,
                            this.getClass(),
                            zoneIds);
                }

                effectivePlotValues.get(anonymisePlot)[i] = plotValue;
            }
        }
    }

    @Override
    public void resetEffectiveZones() {
        // cleanup for next iteration
        effectiveZoneValues.clear();
        effectiveZoneReliabilityFieldErrorCounter.clear();
        effectiveZoneReliabilityTotalCounter.clear();
        effectiveZoneFieldsErrors.clear();
    }

    @Override
    public void computeEffectiveCC(IndicatorWriter writer,
                                   Domain domain,
                                   PerformanceGrowingSystemExecutionContext growingSystemContext,
                                   Plot plot) {

        // échelle culture
        // toutes les interventions culturales d’une campagne culturale (CC)

        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();


        final List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> sortedEffectiveCroppingValues = effectiveCroppingValues.entrySet().stream()
                .filter(entry -> entry.getKey().rang().isPresent())
                .sorted(
                        Comparator.comparingInt(entry -> {
                            final Optional<Integer> rang = entry.getKey().rang();
                            final Integer rank = rang.orElse(Integer.MAX_VALUE);
                            return rank;
                        })
                ).toList();

        writeEffectiveSeasonalCroppingValues(
                writer,
                domain,
                growingSystemContext,
                plot,
                its,
                irs,
                sortedEffectiveCroppingValues);

        final List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> perennialEffectiveCroppingValues = effectiveCroppingValues.entrySet().stream()
                .filter(entry -> entry.getKey().phase().isPresent())
                .toList();

        writeEffectivePerennialCroppingValues(
                writer,
                domain,
                growingSystemContext,
                plot,
                its,
                irs,
                perennialEffectiveCroppingValues);
    }

    private void writeEffectivePerennialCroppingValues(
            IndicatorWriter writer,
            Domain domain,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            Plot plot,
            String its,
            String irs,
            List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> perennialEffectiveCroppingValues) {

        for (Map.Entry<EffectiveCropCycleScaleKey, Double[]> entry : perennialEffectiveCroppingValues) {

            Double[] values = entry.getValue();
            EffectiveCropCycleScaleKey effectiveCroppingKey = entry.getKey();

            CroppingPlanEntry crop = effectiveCroppingKey.crop();
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = effectiveCropsYealdAverage.get(crop.getCode());

            EffectiveCropCyclePhase phase = effectiveCroppingKey.phase().orElse(null);

            Integer riec = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCroppingKey);
            Integer ritc = effectiveCroppingReliabilityTotalCounter.get(effectiveCroppingKey);

            Integer reliability = computeReliabilityIndex(riec, ritc);

            String comments = RELIABILITY_INDEX_NO_COMMENT;
            Set<MissingFieldMessage> messages = effectiveCroppingFieldsErrors.get(effectiveCroppingKey);

            if (CollectionUtils.isNotEmpty(messages)) {
                Set<String> errors = messages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            }

            for (int i = 0; i < values.length; i++) {

                boolean isDisplayed = isDisplayed(ExportLevel.CROP, i);

                if (isDisplayed && effectiveCroppingKey.phase().isPresent()) {

                    // write crop sheet
                    writer.writeEffectivePerennialCrop(its,
                            irs,
                            domain.getCampaign(),
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            values[i],
                            cropYealdAverage,
                            reliability,
                            comments,
                            domain,
                            growingSystemContext.getAnonymizeGrowingSystem(),
                            plot,
                            crop,
                            phase,
                            this.getClass());
                }
            }
        }
    }

    private void writeEffectiveSeasonalCroppingValues(IndicatorWriter writer, Domain domain, PerformanceGrowingSystemExecutionContext growingSystemContext, Plot plot, String its, String irs, List<Map.Entry<EffectiveCropCycleScaleKey, Double[]>> sortedEffectiveCroppingValues) {
        for (Map.Entry<EffectiveCropCycleScaleKey, Double[]> entry : sortedEffectiveCroppingValues) {

            Double[] values = entry.getValue();
            EffectiveCropCycleScaleKey effectiveCroppingKey = entry.getKey();

            CroppingPlanEntry crop = effectiveCroppingKey.crop();
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = effectiveCropsYealdAverage.get(crop.getCode());

            CroppingPlanEntry previousCrop = effectiveCroppingKey.previousCrop().orElse(null);
            Integer rank = effectiveCroppingKey.rang().orElse(null);

            Integer riec = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCroppingKey);
            Integer ritc = effectiveCroppingReliabilityTotalCounter.get(effectiveCroppingKey);

            Integer reliability = computeReliabilityIndex(riec, ritc);

            String comments = RELIABILITY_INDEX_NO_COMMENT;
            Set<MissingFieldMessage> messages = effectiveCroppingFieldsErrors.get(effectiveCroppingKey);

            if (CollectionUtils.isNotEmpty(messages)) {
                Set<String> errors = messages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            }

            for (int i = 0; i < values.length; i++) {

                boolean isDisplayed = isDisplayed(ExportLevel.CROP, i);
                if (isDisplayed && effectiveCroppingKey.rang().isPresent()) {

                    // write crop sheet
                    writer.writeEffectiveSeasonalCrop(its,
                            irs,
                            domain.getCampaign(),
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            values[i],
                            cropYealdAverage,
                            reliability,
                            comments,
                            domain,
                            growingSystemContext.getAnonymizeGrowingSystem(),
                            plot,
                            crop,
                            rank,
                            previousCrop,
                            this.getClass());
                }
            }
        }
    }

    @Override
    public void computeEffective(IndicatorWriter writer,
                                 PerformanceEffectiveDomainExecutionContext domainContext,
                                 PerformanceGrowingSystemExecutionContext growingSystemContext) {


        // Parcelle >> SdC
        // plot: moyenne pondérée sur la surface
        Double[] weightedValueSum = null;
        Double[] weightSum = null;

        Integer reliabilityMissingFileldCounter = 0;
        Integer reliabilityTotalCounter = 0;

        Domain anonymiseDomain = domainContext.getAnonymiseDomain();
        Collection<CroppingPlanSpecies> domainAgrosystSpecies = domainContext.getCroppingPlanSpecies();

        for (Map.Entry<Plot, Double[]> entry : effectivePlotValues.entrySet()) {
            Plot plot = entry.getKey();
            Double[] values = entry.getValue();

            reliabilityMissingFileldCounter += effectivePlotReliabilityFieldErrorCounter.get(plot);
            reliabilityTotalCounter += effectivePlotReliabilityTotalCounter.get(plot);

            if (weightedValueSum == null) {
                weightedValueSum = newArray(values.length, 0.0);
                weightSum = newArray(values.length, 0.0);
            }

            // plotArea
            reliabilityTotalCounter += 1;

            for (int i = 0; i < values.length; i++) {
                weightedValueSum[i] += values[i] * plot.getArea();
                weightSum[i] += plot.getArea();
            }
        }

        Optional<GrowingSystem> anonymizeGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();

        if (weightSum != null && anonymizeGrowingSystem.isPresent()) {
            String its = growingSystemContext.getIts();
            String irs = growingSystemContext.getIrs();

            Integer reliability = computeReliabilityIndex(reliabilityMissingFileldCounter, reliabilityTotalCounter);

            effectiveGrowingSystemValues.put(anonymizeGrowingSystem, newArray(weightedValueSum.length, 0.0));

            effectiveGrowingSystemReliabilityFieldErrorCounter.put(anonymizeGrowingSystem, reliabilityMissingFileldCounter);
            effectiveGrowingSystemReliabilityTotalCounter.put(anonymizeGrowingSystem, reliabilityTotalCounter);
            String comments = getMessagesForScope(MissingMessageScope.GROWING_SYSTEM);

            for (int i = 0; i < weightedValueSum.length; i++) {
                double plotValue = weightSum[i] == 0 ? 0 : weightedValueSum[i] / weightSum[i];
                boolean isDisplayed = isDisplayed(ExportLevel.GROWING_SYSTEM, i);
                if (isDisplayed) {
                    // write SDC sheet
                    writer.writeEffectiveGrowingSystem(its,
                            irs,
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            plotValue,
                            reliability,
                            comments,
                            anonymiseDomain,
                            anonymizeGrowingSystem,
                            this.getClass(),
                            domainAgrosystSpecies);
                }
                effectiveGrowingSystemValues.get(anonymizeGrowingSystem)[i] = plotValue;
            }
        }
    }

    @Override
    public void resetEffectiveCC() {
        // cleanup for next iteration
        effectiveCroppingReliabilityTotalCounter.clear();
        effectiveCroppingReliabilityFieldErrorCounter.clear();
        effectiveCroppingValues.clear();
        effectiveCropsYealdAverage.clear();
        effectiveCroppingFieldsErrors.clear();
    }

    @Override
    public void resetEffectivePlots() {
        // cleanup for next iteration
        effectivePlotValues.clear();
        effectivePlotReliabilityFieldErrorCounter.clear();
        effectivePlotReliabilityTotalCounter.clear();
    }

    // domain scale
    @Override
    public void computeEffective(IndicatorWriter writer, Domain domain) {
        Double[] weightedValueSum = null;
        Double[] weightSum = null;

        Integer domaineRmfc = 0;
        Integer domaineRtfc = 0;

        List<String> gst = new ArrayList<>();
        // moyenne pondérée sur la surface
        for (Map.Entry<Optional<GrowingSystem>, Double[]> entry : effectiveGrowingSystemValues.entrySet()) {
            Optional<GrowingSystem> optionalGrowingSystem = entry.getKey();

            if (optionalGrowingSystem.isPresent()) {
                GrowingSystem growingSystem = optionalGrowingSystem.get();

                Double[] values = entry.getValue();
                if (growingSystem.getTypeAgriculture() != null)
                    gst.add(growingSystem.getTypeAgriculture().getReference_label());

                domaineRmfc += effectiveGrowingSystemReliabilityFieldErrorCounter.get(optionalGrowingSystem);
                domaineRtfc += effectiveGrowingSystemReliabilityTotalCounter.get(optionalGrowingSystem);

                // AffectedAreaRate
                domaineRtfc += 1;

                Double affectedAreaRate = growingSystem.getAffectedAreaRate();
                if (affectedAreaRate == null) {
                    final String domainTopiaId = domain.getTopiaId();
                    addMissingFieldMessage(domainTopiaId, messageBuilder.getDomainMissingAffectedAreaMessage());
                    domaineRmfc += 1;
                }
                affectedAreaRate = affectedAreaRate == null ? DEFAULT_AFFECTED_AREA_RATE : affectedAreaRate;

                if (weightedValueSum == null) {
                    weightedValueSum = newArray(values.length, 0.0);
                    weightSum = newArray(values.length, 0.0);
                }
                for (int i = 0; i < values.length; i++) {
                    weightedValueSum[i] += values[i] * affectedAreaRate;
                    weightSum[i] += affectedAreaRate;
                }
            }

        }

        if (weightSum != null) {

            Integer domainReliability = computeReliabilityIndex(domaineRmfc, domaineRtfc);

            for (int i = 0; i < weightedValueSum.length; i++) {
                if (weightSum[i] == 0 && LOGGER.isWarnEnabled()) {
                    LOGGER.warn("Can't compute growing system scale with 0 weigth");
                }
                double domainValue = weightSum[i] == 0 ? 0 : weightedValueSum[i] / weightSum[i];

                boolean isDisplayed = isDisplayed(ExportLevel.DOMAIN, i);
                if (isDisplayed) {

                    String comments = getMessagesForScope(MissingMessageScope.DOMAIN);
                    // write domain sheet
                    writer.writeEffectiveDomain(
                            getIndicatorCategory(),
                            getIndicatorLabel(i),
                            domainValue,
                            domainReliability,
                            comments,
                            domain,
                            String.join(", ", gst));
                }
            }
        }
    }

    @Override
    public void resetEffectiveGrowingSystems() {
        // cleanup for next iteration
        effectiveGrowingSystemValues.clear();
        effectiveGrowingSystemReliabilityFieldErrorCounter.clear();
        effectiveGrowingSystemReliabilityTotalCounter.clear();
        targetedErrorFieldMessages.clear();
    }

    /**
     * From a mouth number (consider start at 1 for January), determine the
     * effective mouth number between 1 and 12.
     * For example, 15 is march year+1, so return 3.
     */
    protected int getEffectiveMonth(int month) {
        return (month - 1) % 12 + 1;
    }

    protected Double[] getMonthsRatio(PracticedIntervention intervention, double usageTime) {

        Double[] usageTimes = newArray(13, 0.0); // 12 months + sum
        usageTimes[12] = usageTime; // total

        String targetId = intervention.getTopiaId();

        incrementAngGetTotalFieldCounterForTargetedId(targetId);
        String startingPeriodDate = intervention.getStartingPeriodDate();

        if (StringUtils.isBlank(startingPeriodDate)) {
            return usageTimes;
        }

        Matcher mStartDate = PRACTICED_DATE_PATTERN.matcher(startingPeriodDate);

        if (!mStartDate.find()) {
            return usageTimes;
        }

        int startMonth = Integer.parseInt(mStartDate.group(2));

        incrementAngGetTotalFieldCounterForTargetedId(targetId);
        String endingPeriodDate = intervention.getEndingPeriodDate();

        // No end mounth : all in starting month !
        if (StringUtils.isBlank(endingPeriodDate)) {
            usageTimes[startMonth - 1] = usageTime;
            return usageTimes;
        }

        Matcher endDateMatcher = PRACTICED_DATE_PATTERN.matcher(endingPeriodDate);
        // cannot match date or starting and ending months are same : all time in this mouth
        if (!endDateMatcher.find() || Integer.parseInt(endDateMatcher.group(2)) == startMonth) {
            usageTimes[startMonth - 1] = usageTime;
            return usageTimes;
        }

        int endMonth = Integer.parseInt(endDateMatcher.group(2));

        int startDay = Integer.parseInt(mStartDate.group(1));
        int endDay = Integer.parseInt(endDateMatcher.group(1));
        int[] theoreticalNbMonthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        int monthDelta = endMonth > startMonth ? endMonth - startMonth : (12 - startMonth + endMonth);
        int daysInFirstMonth = theoreticalNbMonthDays[startMonth - 1] - startDay + 1;

        final int dayInFirstMonth = daysInFirstMonth + endDay;
        if (monthDelta == 1) {

            double usageTimePerDayRatio = dayInFirstMonth == 0 ? 1 : usageTime / dayInFirstMonth;
            usageTimes[startMonth - 1] = usageTimePerDayRatio * daysInFirstMonth;
            usageTimes[endMonth - 1] = usageTimePerDayRatio * endDay;

        } else {

            int totalDays = dayInFirstMonth == 0 ? 1 : dayInFirstMonth;
            // For each month between starting and endind month, add number of days in total days
            for (int i = 1; i < monthDelta; i++) {
                int currentMonth = getEffectiveMonth(startMonth + i);
                totalDays += theoreticalNbMonthDays[currentMonth - 1]; // use modulo to be sure we don't exceed 12 months
            }

            double usageTimePerDayRatio = usageTime / totalDays;
            usageTimes[startMonth - 1] = usageTimePerDayRatio * daysInFirstMonth;

            for (int i = 1; i < monthDelta; i++) {
                int currentMonth = getEffectiveMonth(startMonth + i);
                long lengthOfMonth = theoreticalNbMonthDays[currentMonth - 1];
                usageTimes[currentMonth - 1] = usageTimePerDayRatio * lengthOfMonth;
            }
            usageTimes[endMonth - 1] = usageTimePerDayRatio * endDay;

        }

        return usageTimes;
    }

    protected Double[] getMonthsRatio(EffectiveIntervention intervention, double usageTime) {

        Double[] usageTimes = newArray(13, 0.0); // 12 months + sum
        usageTimes[12] = usageTime; // total

        String interventionId = intervention.getTopiaId();

        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
        if (intervention.getStartInterventionDate() == null) {
            return usageTimes;
        }
        LocalDate startLocalDate = LocalDate.from(intervention.getStartInterventionDate());

        incrementAngGetTotalFieldCounterForTargetedId(interventionId);

        int startMonth = startLocalDate.getMonthValue();
        if (intervention.getEndInterventionDate() == null) {
            // no end date ? All in start month !
            usageTimes[startMonth - 1] = usageTime;
            return usageTimes;
        }

        LocalDate endLocalDate = LocalDate.from(intervention.getEndInterventionDate());
        if (endLocalDate.isBefore(startLocalDate)) {
            MissingFieldMessage missingFieldMessage = new MissingFieldMessage("Date d'intervention erronée", MissingMessageScope.INTERVENTION);
            addMissingFieldMessage(interventionId, missingFieldMessage);
            endLocalDate = startLocalDate;
        }

        long daysDelta = ChronoUnit.DAYS.between(startLocalDate, endLocalDate) + 1;

        if (startLocalDate.getMonthValue() == endLocalDate.getMonthValue() &&
                startLocalDate.getYear() == endLocalDate.getYear()) { // same month, same year, all time in only this month
            //Same month, just get time !
            usageTimes[startMonth - 1] = usageTime;


        } else {
            int startingYear = startLocalDate.getYear();

            // month Delta
            long monthDelta = ChronoUnit.MONTHS.between(startLocalDate, endLocalDate);
            // for example, we have 07/30 and 08/03 : MONTHS.between give 0 month, but we need to know there is one month change
            if (startLocalDate.getDayOfMonth() > endLocalDate.getDayOfMonth()) {
                monthDelta += 1;
            }

            if (monthDelta == 1) {
                int endMonth = endLocalDate.getMonthValue();
                long daysMonthOne = ChronoUnit.DAYS.between(
                        startLocalDate,
                        YearMonth.of(startingYear, startMonth)
                                .atEndOfMonth()) + 1; // +1 is because last day is not counted

                long daysMonthTwo = ChronoUnit.DAYS.between(
                        LocalDate.of(startingYear,
                                endLocalDate.getMonth(),
                                1),
                        endLocalDate) + 1;

                usageTimes[startMonth - 1] = usageTime / daysDelta * daysMonthOne;
                usageTimes[endMonth - 1] = usageTime / daysDelta * daysMonthTwo;

            } else {

                YearMonth yearMonth = YearMonth.of(startingYear, startMonth);
                long daysMonthOne = ChronoUnit.DAYS.between(startLocalDate, yearMonth.atEndOfMonth()) + 1;
                usageTimes[startMonth - 1] = usageTime / daysDelta * daysMonthOne;

                for (int i = 1; i < monthDelta; i++) {
                    int currentMonth = getEffectiveMonth(startMonth + i);
                    int currentYear =
                            currentMonth == (startMonth + i) ?
                                    startingYear :
                                    startingYear + 1; //XXX consider an intervention will never be one 2 years ...

                    long lengthOfMonth = YearMonth.of(currentYear, currentMonth).lengthOfMonth();
                    usageTimes[currentMonth - 1] = usageTime / daysDelta * lengthOfMonth;
                }
                long daysLastMonth = ChronoUnit.DAYS.between(LocalDate.of(endLocalDate.getYear(), endLocalDate.getMonth(), 1), endLocalDate) + 1;
                usageTimes[endLocalDate.getMonthValue() - 1] = usageTime / daysDelta * daysLastMonth;

            }
        }

        return usageTimes;
    }

    protected double computeBoiledQuantityFactor(AbstractAction action, PhytoProductUnit phytoProductUnit) {

        double boiledQuantity = 1;

        if (phytoProductUnit == PhytoProductUnit.G_HL ||
                phytoProductUnit == PhytoProductUnit.KG_HL ||
                phytoProductUnit == PhytoProductUnit.L_HL) {

            final String inputTopiaId = action.getTopiaId();

            // BoiledQuantity
            incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);

            if (action instanceof PesticidesSpreadingAction pesticidesSpreadingAction) {
                //Traitements phytosanitaires : Lutte chimique et biocontrôle (produits avec AMM) / Application de produits avec AMM
                boiledQuantity = getBoiledQuantity(inputTopiaId, pesticidesSpreadingAction);
            } else if (action instanceof BiologicalControlAction biologicalControlAction) {
                // Traitements phytosanitaires : Produits sans AMM et macroorganismes / Application de Produits sans AMM et macroorganismes
                // actuellement (2022) il n'y a pas de dose de référence, ça va être très rare,
                // c'est souvent des macroorganismes pour lesquels il n'y a pas de bouillie,
                // et souvent les produits avec dose de ref et besoin de bouillie ont une AMM
                boiledQuantity = biologicalControlAction.getBoiledQuantity();
            } else {
                addMissingFieldMessage(inputTopiaId, messageBuilder.getMissingBoiledQuantityMessage());
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn("Phyto quantity defined as hl, but not on PesticideProductInput");
                }
            }
        }
        return boiledQuantity;
    }

    protected double getBoiledQuantity(String inputTopiaId, PesticidesSpreadingAction pesticidesSpreadingAction) {
        // BoiledQuantity
        incrementAngGetTotalFieldCounterForTargetedId(inputTopiaId);

        // Le volume de bouillie est saisi au niveau de l’intervention en hL/ha.
        // C’est une donnée obligatoire (pas de valeur null) mais la valeur 0 est autorisée.
        // Si VB = 0 hL/ha, alors on définit une valeur par défaut de VB=10 hL/ha.
        Double boiledQuantity = pesticidesSpreadingAction.getBoiledQuantity();
        if (boiledQuantity == null || boiledQuantity == 0d) {
            boiledQuantity = DEFAULT_BOILED_QUANTITY;
            addMissingFieldMessage(inputTopiaId, messageBuilder.getMissingBoiledQuantityMessage());
        }

        return boiledQuantity;
    }

    protected double getDouble(Double d) {
        return d != null ? d : 0.0d;
    }
}
