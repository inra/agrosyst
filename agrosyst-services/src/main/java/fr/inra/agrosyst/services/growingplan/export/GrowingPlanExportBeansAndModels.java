package fr.inra.agrosyst.services.growingplan.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.services.common.export.ExportModel;
import fr.inra.agrosyst.services.common.export.ExportUtils;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GrowingPlanExportBeansAndModels {

    @Getter
    public static class CommonBean {

        final String department;
        final String domainName;
        final int campaign;
        final String growingPlanName;
        final String typeName;

        /**
         * Constructeur avec tous les champs pour la première instanciation
         */
        public CommonBean(String department,
                          String domainName,
                          int campaign,
                          String growingPlanName,
                          String typeName) {
            this.department = department;
            this.domainName = domainName;
            this.campaign = campaign;
            this.growingPlanName = growingPlanName;
            this.typeName = typeName;
        }

        /**
         * Constructeur par recopie pour faciliter le travail des sous modèles
         */
        public CommonBean(CommonBean source) {
            this(
                    source.department,
                    source.domainName,
                    source.campaign,
                    source.growingPlanName,
                    source.typeName);
        }
    }

    public abstract static class CommonModel<T extends CommonBean> extends ExportModel<T> {
        public CommonModel() {
            newColumn("Département", CommonBean::getDepartment);
            newColumn("Domaine", CommonBean::getDomainName);
            newColumn("Campagne", CommonBean::getCampaign);
            newColumn("Nom du dispositif", CommonBean::getGrowingPlanName);
            List<String> typeValues = Stream.of(TypeDEPHY.DEPHY_EXPE, TypeDEPHY.DEPHY_FERME, TypeDEPHY.NOT_DEPHY)
                    .map(ExportUtils.TYPE_DEPHY_FORMATTER::format)
                    .collect(Collectors.toList());
            newColumn("Type", typeValues, CommonBean::getTypeName);
        }
    }

    @Getter
    @Setter
    public static class GrowingPlanBean extends CommonBean {

        String description;
        String goals;
        String protocolReference;
        String institutionalStructure;

        public GrowingPlanBean(CommonBean source) {
            super(source);
        }
    }

    public static class GrowingPlanModel extends CommonModel<GrowingPlanBean> {
        @Override
        public String getTitle() {
            return "Dispositifs";
        }

        public GrowingPlanModel() {
            super();
            newColumn("Description succincte du dispositif", GrowingPlanBean::getDescription);
            newColumn("Objectifs du dispositif", GrowingPlanBean::getGoals);
            newColumn("Reference du protocole qui gère le dispositif", GrowingPlanBean::getProtocolReference);
            newColumn("Partenaires institutionnels", GrowingPlanBean::getInstitutionalStructure);
        }
    }

    @Getter
    @Setter
    public static class GrowingSystemBean extends CommonBean {

        String name;
        String dephyNumber;
        String networks;

        public GrowingSystemBean(CommonBean source) {
            super(source);
        }
    }

    public static class GrowingSystemModel extends CommonModel<GrowingSystemBean> {
        @Override
        public String getTitle() {
            return "Systèmes de Culture liés";
        }

        public GrowingSystemModel() {
            super();
            newColumn("Système de culture", GrowingSystemBean::getName);
            newColumn("Numéro DEPHY", GrowingSystemBean::getDephyNumber);
            newColumn("Réseaux", GrowingSystemBean::getNetworks);
        }
    }

}
