package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveImpl;
import fr.inra.agrosyst.services.referential.csv.importApi.RefActaProduitRootExportModel;
import fr.inra.agrosyst.services.referential.json.RefApiActaSubstanceActive;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Référentiel Acta substance active.
 * 
 * id_produit;nom_produit;nom_commun_SA;concentration_valeur;concentration_unite;Source;code_amm
 */
public class RefActaSubstanceActiveModel extends AbstractAgrosystModel<RefActaSubstanceActive> implements ExportModel<RefActaSubstanceActive> {

    public RefActaSubstanceActiveModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("id_produit", RefActaSubstanceActive.PROPERTY_ID_PRODUIT);
        newMandatoryColumn("nom_produit", RefActaSubstanceActive.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("nom_commun_SA", RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA);
        newMandatoryColumn("concentration_valeur", RefActaSubstanceActive.PROPERTY_CONCENTRATION_VALEUR, DOUBLE_PARSER);
        newMandatoryColumn("concentration_unite", RefActaSubstanceActive.PROPERTY_CONCENTRATION_UNITE);
        newMandatoryColumn("Source", RefActaSubstanceActive.PROPERTY_SOURCE);
        newMandatoryColumn("code_AMM", RefActaSubstanceActive.PROPERTY_CODE__AMM);
        newMandatoryColumn("Remarques", RefActaSubstanceActive.PROPERTY_REMARQUES);
        newOptionalColumn(COLUMN_ACTIVE, RefActaSubstanceActive.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newIgnoredColumn("Status");
    }

    @Override
    public Iterable<ExportableColumn<RefActaSubstanceActive, Object>> getColumnsForExport() {
        ModelBuilder<RefActaSubstanceActive> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id_produit", RefActaSubstanceActive.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("nom_produit", RefActaSubstanceActive.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("nom_commun_SA", RefActaSubstanceActive.PROPERTY_NOM_COMMUN_SA);
        modelBuilder.newColumnForExport("concentration_valeur", RefActaSubstanceActive.PROPERTY_CONCENTRATION_VALEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("concentration_unite", RefActaSubstanceActive.PROPERTY_CONCENTRATION_UNITE);
        modelBuilder.newColumnForExport("Source", RefActaSubstanceActive.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("code_AMM", RefActaSubstanceActive.PROPERTY_CODE__AMM);
        modelBuilder.newColumnForExport("Remarques", RefActaSubstanceActive.PROPERTY_REMARQUES);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaSubstanceActive.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Status", RefApiActaSubstanceActive.PROPERTY_STATUS, RefActaProduitRootExportModel.STATUS_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefActaSubstanceActive newEmptyInstance() {
        RefActaSubstanceActive refActaSubstanceActive = new RefActaSubstanceActiveImpl();
        refActaSubstanceActive.setActive(true);
        return refActaSubstanceActive;
    }
}
