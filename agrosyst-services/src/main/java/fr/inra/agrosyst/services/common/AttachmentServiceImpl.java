package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AttachmentContent;
import fr.inra.agrosyst.api.entities.AttachmentContentTopiaDao;
import fr.inra.agrosyst.api.entities.AttachmentMetadata;
import fr.inra.agrosyst.api.entities.AttachmentMetadataTopiaDao;
import fr.inra.agrosyst.api.entities.Downloadable;
import fr.inra.agrosyst.api.entities.DownloadableTopiaDao;
import fr.inra.agrosyst.api.entities.security.AgrosystUser;
import fr.inra.agrosyst.api.entities.security.AgrosystUserTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.AttachmentService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import org.hibernate.Session;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

/**
 * Default implementation of {@link AttachmentService}.
 *
 * @author <a href="mailto:sebastien.grimault@makina-corpus.com">S. Grimault</a>
 */
public class AttachmentServiceImpl extends AbstractAgrosystService implements AttachmentService {

    protected BusinessAuthorizationService authorizationService;

    protected AgrosystUserTopiaDao userDao;
    protected AttachmentMetadataTopiaDao attachmentMetadataDao;
    protected AttachmentContentTopiaDao attachmentContentDao;
    protected DownloadableTopiaDao downloadableTopiaDao;

    public void setAuthorizationService(BusinessAuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setUserDao(AgrosystUserTopiaDao userDao) {
        this.userDao = userDao;
    }

    public void setAttachmentMetadataDao(AttachmentMetadataTopiaDao attachmentMetadataDao) {
        this.attachmentMetadataDao = attachmentMetadataDao;
    }

    public void setAttachmentContentDao(AttachmentContentTopiaDao attachmentContentDao) {
        this.attachmentContentDao = attachmentContentDao;
    }

    public void setDownloadableTopiaDao(DownloadableTopiaDao downloadableTopiaDao) {
        this.downloadableTopiaDao = downloadableTopiaDao;
    }

    @Override
    public AttachmentMetadata addAttachment(String objectReferenceId, InputStream inputStream, String fileName, String fileContentType) {

        authorizationService.checkAddAttachment(objectReferenceId);

        AgrosystUser author = userDao.forTopiaIdEquals(getSecurityContext().getUserId()).findUnique();

        AttachmentMetadata attachmentMetadata = attachmentMetadataDao.newInstance();
        attachmentMetadata.setObjectReferenceId(objectReferenceId);
        attachmentMetadata.setAuthor(author);
        attachmentMetadata.setName(fileName);
        attachmentMetadata.setContentType(fileContentType);

        try {
            // copy file into blob
            AttachmentContent attachmentContent = this.attachmentContentDao.newInstance();
            Session hibernateSession = getPersistenceContext().getHibernateSupport().getHibernateSession();
            Blob blob = hibernateSession.getLobHelper().createBlob(inputStream.readAllBytes());
            attachmentContent.setContent(blob);
            attachmentMetadata.setSize(blob.length());

            // persist blob and metadata
            attachmentMetadata = attachmentMetadataDao.create(attachmentMetadata);
            attachmentContent.setAttachmentMetadata(attachmentMetadata);
            attachmentContentDao.create(attachmentContent);
        } catch (IOException | SQLException ex) {
            throw new AgrosystTechnicalException("Can't upload file in database", ex);
        }

        getTransaction().commit();

        return attachmentMetadata;
    }

    @Override
    public void delete(String attachmentMetadataId) {
        authorizationService.checkDeleteAttachment(attachmentMetadataId);

        AttachmentMetadata attachmentMetadata = attachmentMetadataDao.forTopiaIdEquals(attachmentMetadataId).findUnique();
        AttachmentContent attachmentContent = attachmentContentDao.forAttachmentMetadataEquals(attachmentMetadata).findUnique();
        attachmentContentDao.delete(attachmentContent);
        attachmentMetadataDao.delete(attachmentMetadata);

        getTransaction().commit();
    }

    @Override
    public InputStream getAttachmentContent(String attachmentMetadataId) {
        authorizationService.checkReadAttachment(attachmentMetadataId);

        AttachmentMetadata attachmentMetadata = attachmentMetadataDao.forTopiaIdEquals(attachmentMetadataId).findUnique();
        AttachmentContent attachmentContents = this.attachmentContentDao.forAttachmentMetadataEquals(attachmentMetadata).findUnique();

        InputStream is;
        try {
            is = attachmentContents.getContent().getBinaryStream();
        } catch (SQLException ex) {
            throw new AgrosystTechnicalException("Can't get file content from database", ex);
        }
        return is;
    }

    @Override
    public List<AttachmentMetadata> getAttachmentMetadatas(String objectReferenceId) {
        List<AttachmentMetadata> result = attachmentMetadataDao.forObjectReferenceIdEquals(objectReferenceId).findAll();
        return result;
    }

    @Override
    public long getAttachmentMetadatasCount(String objectReferenceId) {
        long result = attachmentMetadataDao.forObjectReferenceIdEquals(objectReferenceId).count();
        return result;
    }

    @Override
    public AttachmentMetadata getAttachmentMetadata(String attachmentTopiaId) {
        AttachmentMetadata result = attachmentMetadataDao.forTopiaIdEquals(attachmentTopiaId).findUnique();
        return result;
    }

    @Override
    public Downloadable getDownloadable(String id) {
        String topiaId = getPersistenceContext().getTopiaIdFactory().newTopiaId(Downloadable.class, id);
        Downloadable result = downloadableTopiaDao.forTopiaIdEquals(topiaId).findAny();
        return result;
    }

    @Override
    public void purgeDownloadables() {
        downloadableTopiaDao.purgeDownloadable();
        getPersistenceContext().commit();
    }

}
