package fr.inra.agrosyst.services.practiced;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Entities;
import fr.inra.agrosyst.api.entities.SolHorizon;
import fr.inra.agrosyst.api.entities.SolHorizonTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlot;
import fr.inra.agrosyst.api.entities.practiced.PracticedPlotTopiaDao;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystemTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinageTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDITopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigoTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppaTopiaDao;
import fr.inra.agrosyst.api.services.plot.SolHorizonDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotDto;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotFilter;
import fr.inra.agrosyst.api.services.practiced.PracticedPlotService;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import lombok.Setter;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.pagination.PaginationResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Setter
public class PracticedPlotServiceImpl extends AbstractAgrosystService implements PracticedPlotService {

    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;
    protected PracticedSystemService practicedSystemService;

    protected PracticedPlotTopiaDao practicedPlotTopiaDao;
    protected PracticedSystemTopiaDao practicedSystemDao;
    protected RefLocationTopiaDao locationDao;
    protected RefParcelleZonageEDITopiaDao parcelleZonageEDIDao;
    protected RefSolTextureGeppaTopiaDao refSolTextureGeppaDao;
    protected RefSolProfondeurIndigoTopiaDao refSolProfondeurIndigoDao;
    protected SolHorizonTopiaDao solHorizonDao;
    protected RefElementVoisinageTopiaDao refElementVoisinageTopiaDao;
    
    @Override
    public PaginationResult<PracticedPlot> getFilteredPracticedPlots(PracticedPlotFilter filter) {
        PaginationResult<PracticedPlot> result = practicedPlotTopiaDao.getFilteredPracticedPlots(filter, getSecurityContext());
        return result;
    }

    @Override
    public PaginationResult<PracticedPlotDto> getFilteredPracticedPlotsDto(PracticedPlotFilter filter) {
        PaginationResult<PracticedPlot> practicedPlots = getFilteredPracticedPlots(filter);
        return practicedPlots.transform(anonymizeService.getPracticedPlotToDtoFunction());
    }

    @Override
    public PracticedPlot getPracticedPlot(String practicedPlotTopiaId) {
        PracticedPlot result;
        if (StringUtils.isBlank(practicedPlotTopiaId)) {
            result = practicedPlotTopiaDao.newInstance();
        } else {
            result = practicedPlotTopiaDao.forTopiaIdEquals(practicedPlotTopiaId).findUnique();
        }
        return result;
    }

    @Override
    public PracticedPlot createOrUpdatePracticedPlot(PracticedPlot practicedPlot,
                                                     String practicedSystemId,
                                                     String locationId,
                                                     String selectedSurfaceTextureId,
                                                     String selectedSubSoilTextureId,
                                                     String selectedSolDepthId,
                                                     List<String> selectedPlotZoningIds,
                                                     List<SolHorizonDto> solHorizons,
                                                     List<String> adjacentElementIds) {

        PracticedSystem practicedSystem = getPracticedSystemForPracticedPlot(practicedPlot, practicedSystemId);

        authorizationService.checkCreateOrUpdatePracticedPlot(practicedPlot.getTopiaId(), practicedSystem.getTopiaId());

        Preconditions.checkArgument(practicedSystemService.isActivated(practicedSystem), "The practiced system ancestor or unactivated");

        RefLocation location = getRefLocation(locationId);

        RefSolTextureGeppa surfaceTexture = getSurfaceTexture(practicedPlot, selectedSurfaceTextureId);

        RefSolTextureGeppa subSoilTexture = getSubSoilTexture(practicedPlot, selectedSubSoilTextureId);

        RefSolProfondeurIndigo solDepth = getSolDepth(practicedPlot, selectedSolDepthId);

        PracticedPlot result = createOrUpdatePracticedPlotWithoutCommit(practicedPlot, practicedSystem, location, surfaceTexture, subSoilTexture, solDepth, selectedPlotZoningIds, solHorizons, adjacentElementIds);

        getTransaction().commit();
        return result;
    }

    protected PracticedPlot createOrUpdatePracticedPlotWithoutCommit(PracticedPlot practicedPlot,
                                                                     PracticedSystem practicedSystem,
                                                                     RefLocation location,
                                                                     RefSolTextureGeppa surfaceTexture,
                                                                     RefSolTextureGeppa subSoilTexture,
                                                                     RefSolProfondeurIndigo solDepth,
                                                                     List<String> selectedPlotZoningIds,
                                                                     List<SolHorizonDto> solHorizons,
                                                                     List<String> adjacentElementIds) {
        validatePracticedPlot(practicedPlot, practicedSystem);

        practicedPlot.setPracticedSystem(Collections.singleton(practicedSystem));

        practicedPlot.setLocation(location);

        // plot's surface texture
        practicedPlot.setSurfaceTexture(surfaceTexture);

        // plot's sub soil texture
        practicedPlot.setSubSoilTexture(subSoilTexture);

        // plot's sol profondeur
        practicedPlot.setSolDepth(solDepth);

        // persist sol horizon
        managePracticePlotSolHorizon(practicedPlot, solHorizons);

        // add zonings entities
        Collection<RefParcelleZonageEDI> plotZonings = practicedPlot.getPlotZonings() == null ? new ArrayList<>() : practicedPlot.getPlotZonings();
        practicedPlot.setPlotZonings(plotZonings);


        Collection<RefParcelleZonageEDI> nonDeletedPlotZonings = getPlotZonings(practicedPlot, selectedPlotZoningIds, plotZonings);
        plotZonings.retainAll(nonDeletedPlotZonings);

        // persist plot
        PracticedPlot result;
        if (practicedPlot.isPersisted()) {
            result = practicedPlotTopiaDao.update(practicedPlot);
        } else {
            result = practicedPlotTopiaDao.create(practicedPlot);
        }

        // adjacent elements
        manageAdjacentElements(adjacentElementIds, result);
        return result;
    }

    protected void manageAdjacentElements(List<String> adjacentElementIds, PracticedPlot result) {
        Collection<RefElementVoisinage> currentAdjacentElements = result.getAdjacentElements();
        if (currentAdjacentElements == null) {
            currentAdjacentElements = new ArrayList<>();
            result.setAdjacentElements(currentAdjacentElements);
        }
        Collection<RefElementVoisinage> nonDeletedAdjacentElements = new ArrayList<>();
        if (adjacentElementIds != null) {
            for (String adjacentElementId : adjacentElementIds) {
                RefElementVoisinage refElementVoisinage = refElementVoisinageTopiaDao.forTopiaIdEquals(adjacentElementId).findUnique();
                if (!currentAdjacentElements.contains(refElementVoisinage)) {
                    currentAdjacentElements.add(refElementVoisinage);
                }
                nonDeletedAdjacentElements.add(refElementVoisinage);
            }
        }
        currentAdjacentElements.retainAll(nonDeletedAdjacentElements);
    }

    protected void managePracticePlotSolHorizon(PracticedPlot practicedPlot, List<SolHorizonDto> solHorizons) {
        if (solHorizons != null) {
            Collection<SolHorizon> currentHorizons = practicedPlot.getSolHorizon();
            if (currentHorizons == null) {
                currentHorizons = new ArrayList<>();
                practicedPlot.setSolHorizon(currentHorizons);
            }
            Map<String, SolHorizon> currentHorizonMap = Maps.uniqueIndex(currentHorizons, Entities.GET_TOPIA_ID::apply);
            Set<SolHorizon> nonDeletedHorizon = Sets.newHashSet();
            for (SolHorizonDto solHorizonDto : solHorizons) {
                String topiaId = solHorizonDto.getTopiaId();

                SolHorizon solHorizon;
                if (StringUtils.isNotBlank(topiaId)) {
                    solHorizon = currentHorizonMap.get(topiaId);
                } else {
                    solHorizon = solHorizonDao.newInstance();
                }

                solHorizon.setComment(solHorizonDto.getComment());
                solHorizon.setLowRating(solHorizonDto.getLowRating());
                solHorizon.setStoniness(solHorizonDto.getStoniness());
                String solTextureId = solHorizonDto.getSolTextureId();
                RefSolTextureGeppa horizonSolTexture = null;
                if (StringUtils.isNotBlank(solTextureId)) {
                    horizonSolTexture = refSolTextureGeppaDao.forTopiaIdEquals(solTextureId).findUnique();
                }
                solHorizon.setSolTexture(horizonSolTexture);

                nonDeletedHorizon.add(solHorizon);
                currentHorizons.add(solHorizon);
            }
            currentHorizons.retainAll(nonDeletedHorizon);
        }
    }

    protected RefSolProfondeurIndigo getSolDepth(PracticedPlot practicedPlot, String selectedSolProfondeurId) {
        RefSolProfondeurIndigo solProfondeur = practicedPlot.getSolDepth();
        if (!Strings.isNullOrEmpty(selectedSolProfondeurId) && (solProfondeur == null || !selectedSolProfondeurId.equals(solProfondeur.getTopiaId()))) {
            solProfondeur = refSolProfondeurIndigoDao.forTopiaIdEquals(selectedSolProfondeurId).findUnique();
        }
        return solProfondeur;
    }

    protected RefSolTextureGeppa getSubSoilTexture(PracticedPlot practicedPlot, String selectedSubSoilTextureId) {
        RefSolTextureGeppa subSoilTexture = practicedPlot.getSubSoilTexture();
        if (!Strings.isNullOrEmpty(selectedSubSoilTextureId) && (subSoilTexture == null || !selectedSubSoilTextureId.equals(subSoilTexture.getTopiaId()))) {
            subSoilTexture = refSolTextureGeppaDao.forTopiaIdEquals(selectedSubSoilTextureId).findUnique();
        }
        return subSoilTexture;
    }

    protected RefSolTextureGeppa getSurfaceTexture(PracticedPlot practicedPlot, String selectedSurfaceTextureId) {
        RefSolTextureGeppa surfaceTexture = practicedPlot.getSurfaceTexture();
        if (!Strings.isNullOrEmpty(selectedSurfaceTextureId) && (surfaceTexture == null || !selectedSurfaceTextureId.equals(surfaceTexture.getTopiaId()))) {
            surfaceTexture = refSolTextureGeppaDao.forTopiaIdEquals(selectedSurfaceTextureId).findUnique();
        }
        return surfaceTexture;
    }

    protected Collection<RefParcelleZonageEDI> getPlotZonings(PracticedPlot practicedPlot, List<String> selectedPlotZoningIds, Collection<RefParcelleZonageEDI> plotZonings) {
        Collection<RefParcelleZonageEDI> nonDeletedPlotZonings = new ArrayList<>();
        if (selectedPlotZoningIds != null) {
            // si la parcelle est hors zonage, on vide la liste
            List<RefParcelleZonageEDI> parcelleZonageEDIs;
            if (practicedPlot.isOutOfZoning()) {
                selectedPlotZoningIds.clear();
                parcelleZonageEDIs = new ArrayList<>();
            } else {
                parcelleZonageEDIs = parcelleZonageEDIDao.forTopiaIdIn(selectedPlotZoningIds).findAll();
                Preconditions.checkArgument(parcelleZonageEDIs.size() == selectedPlotZoningIds.size(), "Parcelles de la zone non retouvées !");
            }

            for (RefParcelleZonageEDI parcelleZonageEDI : parcelleZonageEDIs) {
                if (!plotZonings.contains(parcelleZonageEDI)) {
                    plotZonings.add(parcelleZonageEDI);
                }
                nonDeletedPlotZonings.add(parcelleZonageEDI);
            }
        }
        return nonDeletedPlotZonings;
    }

    protected RefLocation getRefLocation(String locationTopiaId) {
        RefLocation location = null;
        if (StringUtils.isNotBlank(locationTopiaId)) {
            location = locationDao.forTopiaIdEquals(locationTopiaId).findUnique();
        }
        return location;
    }

    protected PracticedSystem getPracticedSystemForPracticedPlot(PracticedPlot practicedPlot, String practicedSystemTopiaId) {
        PracticedSystem practicedSystem;
        // TODO kmorin 20221121 check si practicedPlot AGS ou IpmWorks ?
        if (!practicedPlot.isPersisted()) {
            Preconditions.checkArgument(StringUtils.isNotBlank(practicedSystemTopiaId), "L'identifiant su sytstème synthétisé est requis.");
            practicedSystem = practicedSystemDao.forTopiaIdEquals(practicedSystemTopiaId).findUnique();
            practicedPlot.setPracticedSystem(Collections.singleton(practicedSystem));

        } else {
            practicedSystem = IterableUtils.get(practicedPlot.getPracticedSystem(), 0);
        }
        return practicedSystem;
    }

    protected void validatePracticedPlot(PracticedPlot practicedPlot, PracticedSystem practicedSystem) {
        // valid unique
        if (!practicedPlot.isPersisted()) {
            Preconditions.checkArgument(!practicedPlotTopiaDao.forPracticedSystemContains(practicedSystem).exists());
        }
    }

    @Override
    public List<PracticedSystem> getPracticedSystemsWithoutPracticedPlot(NavigationContext navigationContext) {
        List<PracticedSystem> result = practicedSystemDao.getPracticedSystemsWithoutPlot(navigationContext, getSecurityContext());
        return result;
    }
}
