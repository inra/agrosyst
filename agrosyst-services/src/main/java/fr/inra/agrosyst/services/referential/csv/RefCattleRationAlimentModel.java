package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAlimentImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * aliment_type;aliment_unit
 * 
 * @author Kevin Morin
 */
public class RefCattleRationAlimentModel extends AbstractAgrosystModel<RefCattleRationAliment> implements ExportModel<RefCattleRationAliment> {

    public RefCattleRationAlimentModel() {
        super(CSV_SEPARATOR);
        
        newMandatoryColumn("aliment_type", RefCattleRationAliment.PROPERTY_ALIMENT_TYPE);
        newMandatoryColumn("aliment_unit", RefCattleRationAliment.PROPERTY_ALIMENT_UNIT);
        newOptionalColumn(COLUMN_ACTIVE, RefCattleRationAliment.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefCattleRationAliment, Object>> getColumnsForExport() {
        ModelBuilder<RefCattleRationAliment> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("aliment_type", RefCattleRationAliment.PROPERTY_ALIMENT_TYPE);
        modelBuilder.newColumnForExport("aliment_unit", RefCattleRationAliment.PROPERTY_ALIMENT_UNIT);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefCattleRationAliment.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefCattleRationAliment newEmptyInstance() {
        RefCattleRationAliment refCattleRationAliment = new RefCattleRationAlimentImpl();
        refCattleRationAliment.setActive(true);
        return refCattleRationAliment;
    }
}
