package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.DephygraphConnectorService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DephygraphConnectorServiceImpl extends AbstractAgrosystService implements DephygraphConnectorService {
    
    protected static final Log LOGGER = LogFactory.getLog(DephygraphConnectorServiceImpl.class);

    protected GrowingSystemTopiaDao growingSystemTopiaDao;

    public void setGrowingSystemTopiaDao(GrowingSystemTopiaDao growingSystemTopiaDao) {
        this.growingSystemTopiaDao = growingSystemTopiaDao;
    }

    @Override
    public String getDephygraphConnexionUrl() {

        // On récupère les informations sur l'utilisateur actuellement connecté
        AuthenticatedUser authenticatedUser = getAuthenticatedUser();

        AgrosystInfo userInfo = new AgrosystInfo();
        userInfo.setUserName(authenticatedUser.getFirstName() + " " + authenticatedUser.getLastName());
        List<String> allowedDephyNb = growingSystemTopiaDao.findAllDephyNb(getSecurityContext());
        userInfo.setAllowedDephyNb(allowedDephyNb);

        String userInfoJson = new Gson().toJson(userInfo);

        HttpResponse response;
        String dephyGraphAPIUrl = getConfig().getDephyGraphAPIUrl();
        try {
            final String uri = dephyGraphAPIUrl + "api/v1/agrosyst/prepare";
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Try to connect to:" + uri);
            }
            URI nvUri = URI.create(uri);
            response = Request.Post(nvUri)
                    .bodyForm(Form.form().add("agrosystInfo", userInfoJson).build(), StandardCharsets.UTF_8)
                    .connectTimeout(20 * 1000)
                    .execute()
                    .returnResponse();
        } catch (IOException e) {
            throw new AgrosystTechnicalException("Cannot contact Dephygraph", e);
        }

        if (response.getStatusLine().getStatusCode() == 200) {
            String dephyGraphUrl = StringUtils.firstNonBlank(
                    getConfig().getDephyGraphRedirectionUrl(),
                    dephyGraphAPIUrl);
            HttpEntity responseEntity = response.getEntity();
            String dephyGraphToken;
            try {
                dephyGraphToken = EntityUtils.toString(responseEntity);
                EntityUtils.consume(responseEntity);
            } catch (IOException e) {
                throw new AgrosystTechnicalException("Cannot read Dephygraph result");
            }
            return dephyGraphUrl + "?agrosystToken=" + dephyGraphToken;
        }

        throw new AgrosystTechnicalException("Cannot contact Dephygraph");
    }


    /**
     * Class imported from Dephygraph 1.1.0 on 2018/07/31
     *
     * @author ymartel (martel@codelutin.com)
     */
    public static class AgrosystInfo {

        protected String userName;
        protected List<String> allowedDephyNb;
        protected List<String> allowedIrCode;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public List<String> getAllowedDephyNb() {
            return allowedDephyNb;
        }

        public void setAllowedDephyNb(List<String> allowedDephyNb) {
            this.allowedDephyNb = allowedDephyNb;
        }

        public List<String> getAllowedIrCode() {
            return allowedIrCode;
        }

        public void setAllowedIrCode(List<String> allowedIrCode) {
            this.allowedIrCode = allowedIrCode;
        }
    }
}
