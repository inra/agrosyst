package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrigImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefPrixIrrigModel extends AbstractDestinationAndPriceModel<RefPrixIrrig> implements ExportModel<RefPrixIrrig> {

    public RefPrixIrrigModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Prix", RefPrixIrrig.PROPERTY_PRICE, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("Unité", RefPrixIrrig.PROPERTY_UNIT, PRICE_UNIT_PARSER);
        newMandatoryColumn("Code scénario", RefPrixIrrig.PROPERTY_CODE_SCENARIO);
        newMandatoryColumn("Scénario", RefPrixIrrig.PROPERTY_SCENARIO);
        newMandatoryColumn("Campagne", RefPrixIrrig.PROPERTY_CAMPAIGN, INTEGER_WITH_NULL_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefPrixIrrig.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newOptionalColumn("Source", RefPrixIrrig.PROPERTY_SOURCE);
    }

    @Override
    public Iterable<ExportableColumn<RefPrixIrrig, Object>> getColumnsForExport() {
        ModelBuilder<RefPrixIrrig> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Prix", RefPrixIrrig.PROPERTY_PRICE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("Unité", RefPrixIrrig.PROPERTY_UNIT, PRICE_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("Code scénario", RefPrixIrrig.PROPERTY_CODE_SCENARIO);
        modelBuilder.newColumnForExport("Scénario", RefPrixIrrig.PROPERTY_SCENARIO);
        modelBuilder.newColumnForExport("Campagne", RefPrixIrrig.PROPERTY_CAMPAIGN, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefPrixIrrig.PROPERTY_ACTIVE, T_F_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefPrixIrrig.PROPERTY_SOURCE);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefPrixIrrig newEmptyInstance() {
        RefPrixIrrig result = new RefPrixIrrigImpl();
        result.setActive(true);
        return result;
    }
}
