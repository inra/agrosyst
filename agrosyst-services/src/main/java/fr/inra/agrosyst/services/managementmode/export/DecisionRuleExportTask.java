package fr.inra.agrosyst.services.managementmode.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import fr.inra.agrosyst.services.async.AbstractTask;

/**
 * Classe qui permet de représenter une demande d'export brut de règles de décision.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class DecisionRuleExportTask extends AbstractTask {

    protected final ImmutableSet<String> decisionRuleIds;

    public DecisionRuleExportTask(String userId, String userEmail, Iterable<String> decisionRuleIds) {
        super(userId, userEmail);
        this.decisionRuleIds = ImmutableSet.copyOf(decisionRuleIds);
    }

    @Override
    public String getDescription() {
        final int count = decisionRuleIds.size();
        final String description = String.format("export XLS de %d règle%s de décision", count, count > 1 ? "s" : "");
        return description;
    }

    public ImmutableSet<String> getDecisionRuleIds() {
        return decisionRuleIds;
    }
}
