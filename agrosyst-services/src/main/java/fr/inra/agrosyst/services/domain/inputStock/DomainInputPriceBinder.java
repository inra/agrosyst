package fr.inra.agrosyst.services.domain.inputStock;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2022 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class DomainInputPriceBinder {
    
    public static InputPriceDto bindPriceToDto(InputPrice price) {
        
        if (price == null) return null;
        
        final InputPriceDto.InputPriceDtoBuilder<?,?> builder = InputPriceDto.builder();
        
        builder
                .topiaId(price.getTopiaId())
                .objectId(price.getObjectId())
                .displayName(price.getDisplayName())
                .price(price.getPrice())
                .phytoProductType(price.getPhytoProductType())
                .sourceUnit(price.getSourceUnit())
                .priceUnit(price.getPriceUnit())
                .category(price.getCategory());
        
        if (InputPriceCategory.SEEDING_INPUT.equals(price.getCategory())) {
            SeedPrice seedPrice = (SeedPrice) price;
            builder
                    .biologicalSeedInoculation(seedPrice.isBiologicalSeedInoculation())
                    .chemicalTreatment(seedPrice.isChemicalTreatment())
                    .includedTreatment(seedPrice.isIncludedTreatment())
                    .organic(seedPrice.isOrganic())
                    .seedType(seedPrice.getSeedType());
                    
        }
        
        return builder.build();
    }
}
