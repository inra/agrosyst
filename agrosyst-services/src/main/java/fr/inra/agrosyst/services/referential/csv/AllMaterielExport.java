package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MaterielType;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class AllMaterielExport {

    public static final String PROPERTY_CATEGORY = "category";

    public static final String PROPERTY_TYPE_MATERIEL1 = "typeMateriel1";

    public static final String PROPERTY_TYPE_MATERIEL2 = "typeMateriel2";

    public static final String PROPERTY_TYPE_MATERIEL3 = "typeMateriel3";

    public static final String PROPERTY_TYPE_MATERIEL4 = "typeMateriel4";

    public static final String PROPERTY_MANUAL_TOOL = "manualTool";

    protected MaterielType category;

    protected String typeMateriel1;

    protected String typeMateriel2;

    protected String typeMateriel3;

    protected String typeMateriel4;

    protected boolean manualTool;

    public AllMaterielExport(RefMateriel materiel) {
        this();
        if (materiel instanceof RefMaterielTraction) {
            category = MaterielType.TRACTEUR;
        } else if (materiel instanceof RefMaterielAutomoteur) {
            category = MaterielType.AUTOMOTEUR;
        } else if (materiel instanceof RefMaterielOutil) {
            category = MaterielType.OUTIL;
        } else if (materiel instanceof RefMaterielIrrigation) {
            category = MaterielType.IRRIGATION;
        }
        this.typeMateriel1 = materiel.getTypeMateriel1();
        this.typeMateriel2 = materiel.getTypeMateriel2();
        this.typeMateriel3 = materiel.getTypeMateriel3();
        this.typeMateriel4 = materiel.getTypeMateriel4();
        this.manualTool = materiel instanceof RefMaterielOutil && ((RefMaterielOutil) materiel).isPetitMateriel();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AllMaterielExport that = (AllMaterielExport) o;
        return Objects.equals(category, that.category) &&
                Objects.equals(typeMateriel1, that.typeMateriel1) &&
                Objects.equals(typeMateriel2, that.typeMateriel2) &&
                Objects.equals(typeMateriel3, that.typeMateriel3) &&
                Objects.equals(typeMateriel4, that.typeMateriel4) &&
                Objects.equals(manualTool, that.manualTool);
    }

    @Override
    public int hashCode() {
        return Objects.hash(category, typeMateriel1, typeMateriel2, typeMateriel3, typeMateriel4, manualTool);
    }
}
