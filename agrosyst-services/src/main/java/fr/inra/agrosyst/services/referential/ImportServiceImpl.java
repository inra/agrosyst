package fr.inra.agrosyst.services.referential;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.StandardSystemProperty;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.InputPriceCategory;
import fr.inra.agrosyst.api.entities.InputPriceTopiaDao;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPCTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCultures;
import fr.inra.agrosyst.api.entities.referential.RefActaGroupeCulturesTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActive;
import fr.inra.agrosyst.api.entities.referential.RefActaSubstanceActiveTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCateg;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitsCategTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefAdventice;
import fr.inra.agrosyst.api.entities.referential.RefAgsAmortissement;
import fr.inra.agrosyst.api.entities.referential.RefAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleAnimalType;
import fr.inra.agrosyst.api.entities.referential.RefCattleRationAliment;
import fr.inra.agrosyst.api.entities.referential.RefCiblesAgrosystGroupesCiblesMAA;
import fr.inra.agrosyst.api.entities.referential.RefClonePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMMTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefConversionUnitesQSA;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolAnnuelle;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne;
import fr.inra.agrosyst.api.entities.referential.RefCultureEdiGroupeCouvSol;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAA;
import fr.inra.agrosyst.api.entities.referential.RefCultureMAATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefDepartmentShape;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefDestinationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefEdaplosTypeTraitement;
import fr.inra.agrosyst.api.entities.referential.RefElementVoisinage;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefEspeceOtherTools;
import fr.inra.agrosyst.api.entities.referential.RefEspeceToVariete;
import fr.inra.agrosyst.api.entities.referential.RefEspeceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFeedbackRouter;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFATopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrgaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefGesCarburant;
import fr.inra.agrosyst.api.entities.referential.RefGesEngrais;
import fr.inra.agrosyst.api.entities.referential.RefGesPhyto;
import fr.inra.agrosyst.api.entities.referential.RefGesSemence;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitement;
import fr.inra.agrosyst.api.entities.referential.RefGroupeCibleTraitementTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceConverter;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefInterventionAgrosystTravailEDI;
import fr.inra.agrosyst.api.entities.referential.RefInterventionTypeItemInputEDI;
import fr.inra.agrosyst.api.entities.referential.RefLegalStatus;
import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontroleImpl;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontroleTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCibleImpl;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCibleTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMarketingDestination;
import fr.inra.agrosyst.api.entities.referential.RefMaterielAutomoteur;
import fr.inra.agrosyst.api.entities.referential.RefMaterielIrrigation;
import fr.inra.agrosyst.api.entities.referential.RefMaterielOutil;
import fr.inra.agrosyst.api.entities.referential.RefMaterielTraction;
import fr.inra.agrosyst.api.entities.referential.RefMesure;
import fr.inra.agrosyst.api.entities.referential.RefNrjCarburant;
import fr.inra.agrosyst.api.entities.referential.RefNrjEngrais;
import fr.inra.agrosyst.api.entities.referential.RefNrjGesOutil;
import fr.inra.agrosyst.api.entities.referential.RefNrjPhyto;
import fr.inra.agrosyst.api.entities.referential.RefNrjSemence;
import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefOTEX;
import fr.inra.agrosyst.api.entities.referential.RefOrientationEDI;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefParcelleZonageEDI;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMM;
import fr.inra.agrosyst.api.entities.referential.RefPhrasesRisqueEtClassesMentionDangerParAMMTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefPhytoSubstanceActiveIphy;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixAutre;
import fr.inra.agrosyst.api.entities.referential.RefPrixCarbu;
import fr.inra.agrosyst.api.entities.referential.RefPrixEspece;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefPrixIrrig;
import fr.inra.agrosyst.api.entities.referential.RefPrixPhyto;
import fr.inra.agrosyst.api.entities.referential.RefPrixPot;
import fr.inra.agrosyst.api.entities.referential.RefPrixSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteria;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaClass;
import fr.inra.agrosyst.api.entities.referential.RefQualityCriteriaTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSaActaIphy;
import fr.inra.agrosyst.api.entities.referential.RefSeedUnits;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisImpl;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalisTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolCaracteristiqueIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolProfondeurIndigo;
import fr.inra.agrosyst.api.entities.referential.RefSolTextureGeppa;
import fr.inra.agrosyst.api.entities.referential.RefSpeciesToSector;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeNuisibleEDI;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStrategyLever;
import fr.inra.agrosyst.api.entities.referential.RefSubstancesActivesCommissionEuropeenne;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.RefSupportOrganeEDI;
import fr.inra.agrosyst.api.entities.referential.RefTraitSdC;
import fr.inra.agrosyst.api.entities.referential.RefTypeAgriculture;
import fr.inra.agrosyst.api.entities.referential.RefTypeNotationEDI;
import fr.inra.agrosyst.api.entities.referential.RefUniteEDI;
import fr.inra.agrosyst.api.entities.referential.RefUnitesQualifiantEDI;
import fr.inra.agrosyst.api.entities.referential.RefValeurQualitativeEDI;
import fr.inra.agrosyst.api.entities.referential.RefVarieteGeves;
import fr.inra.agrosyst.api.entities.referential.RefVarietePlantGrape;
import fr.inra.agrosyst.api.entities.referential.RefZoneClimatiqueIphy;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.entities.referential.ReferentialI18nEntry;
import fr.inra.agrosyst.api.entities.referential.Referentials;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoCaseGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoFuzzySetGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesoRulesGroundWater;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesuRunoffPotRulesParc;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRootTopiaDao;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSpcRootTopiaDao;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRootImpl;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaProduitRootTopiaDao;
import fr.inra.agrosyst.api.entities.referential.refApi.TypeDose;
import fr.inra.agrosyst.api.exceptions.AgrosystImportException;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.InputPriceService;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.api.services.referential.ImportStatus;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.CacheService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;
import fr.inra.agrosyst.services.referential.csv.AbstractDestinationAndPriceModel;
import fr.inra.agrosyst.services.referential.csv.CommuneEuropeModel;
import fr.inra.agrosyst.services.referential.csv.CommuneInseeModel;
import fr.inra.agrosyst.services.referential.csv.CommunePostCodeModel;
import fr.inra.agrosyst.services.referential.csv.CommuneRegionAgricoleModel;
import fr.inra.agrosyst.services.referential.csv.RefLocationCommunesFranceImportModel;
import fr.inra.agrosyst.services.referential.csv.RefActaDosageSPCModel;
import fr.inra.agrosyst.services.referential.csv.RefActaGroupeCulturesModel;
import fr.inra.agrosyst.services.referential.csv.RefActaSubstanceActiveModel;
import fr.inra.agrosyst.services.referential.csv.RefActaTraitementsProduitModel;
import fr.inra.agrosyst.services.referential.csv.RefActaTraitementsProduitsCategModel;
import fr.inra.agrosyst.services.referential.csv.RefAdventiceModel;
import fr.inra.agrosyst.services.referential.csv.RefAgsAmortissementModel;
import fr.inra.agrosyst.services.referential.csv.RefAnimalTypeModel;
import fr.inra.agrosyst.services.referential.csv.RefCattleAnimalTypeModel;
import fr.inra.agrosyst.services.referential.csv.RefCattleRationAlimentModel;
import fr.inra.agrosyst.services.referential.csv.RefCiblesAgrosystGroupesCiblesMAAModel;
import fr.inra.agrosyst.services.referential.csv.RefClonePlantGrapeModel;
import fr.inra.agrosyst.services.referential.csv.RefCompositionSubstancesActivesParNumeroAMMModel;
import fr.inra.agrosyst.services.referential.csv.RefConversionUnitesQSAModel;
import fr.inra.agrosyst.services.referential.csv.RefCountryModel;
import fr.inra.agrosyst.services.referential.csv.RefCouvSolAnnuelleModel;
import fr.inra.agrosyst.services.referential.csv.RefCouvSolPerenneModel;
import fr.inra.agrosyst.services.referential.csv.RefCultureEdiGroupeCouvSolModel;
import fr.inra.agrosyst.services.referential.csv.RefDepartmentShapeModel;
import fr.inra.agrosyst.services.referential.csv.RefDestinationModel;
import fr.inra.agrosyst.services.referential.csv.RefEdaplosTypeTraitemenModel;
import fr.inra.agrosyst.services.referential.csv.RefElementVoisinageModel;
import fr.inra.agrosyst.services.referential.csv.RefEspeceDto;
import fr.inra.agrosyst.services.referential.csv.RefEspeceModel;
import fr.inra.agrosyst.services.referential.csv.RefEspeceOtherToolsModel;
import fr.inra.agrosyst.services.referential.csv.RefEspeceToVarieteModel;
import fr.inra.agrosyst.services.referential.csv.RefFeedbackRouterModel;
import fr.inra.agrosyst.services.referential.csv.RefFertiMinUNIFAModel;
import fr.inra.agrosyst.services.referential.csv.RefFertiOrgaModel;
import fr.inra.agrosyst.services.referential.csv.RefGesCarburantModel;
import fr.inra.agrosyst.services.referential.csv.RefGesEngraisModel;
import fr.inra.agrosyst.services.referential.csv.RefGesPhytoModel;
import fr.inra.agrosyst.services.referential.csv.RefGesSemenceModel;
import fr.inra.agrosyst.services.referential.csv.RefGroupeCibleTraitementModel;
import fr.inra.agrosyst.services.referential.csv.RefHarvestingPriceConverterModel;
import fr.inra.agrosyst.services.referential.csv.RefHarvestingPriceModel;
import fr.inra.agrosyst.services.referential.csv.RefInputUnitPriceUnitConverterModel;
import fr.inra.agrosyst.services.referential.csv.RefInterventionAgrosystTravailEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefInterventionTypeItemInputEdiModel;
import fr.inra.agrosyst.services.referential.csv.RefLegalStatusModel;
import fr.inra.agrosyst.services.referential.csv.RefLocationDto;
import fr.inra.agrosyst.services.referential.csv.RefMAABiocontroleModel;
import fr.inra.agrosyst.services.referential.csv.RefMAADosesRefParGroupeCibleModel;
import fr.inra.agrosyst.services.referential.csv.RefMarketingDestinationModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielAutomoteurModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielIrrigationModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielOutilModel;
import fr.inra.agrosyst.services.referential.csv.RefMaterielTracteurModel;
import fr.inra.agrosyst.services.referential.csv.RefMesureModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjCarburantModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjEngraisModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjGesOutilModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjPhytoModel;
import fr.inra.agrosyst.services.referential.csv.RefNrjSemenceModel;
import fr.inra.agrosyst.services.referential.csv.RefNuisibleEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefOTEXModel;
import fr.inra.agrosyst.services.referential.csv.RefOrientationEdiModel;
import fr.inra.agrosyst.services.referential.csv.RefOtherInputModel;
import fr.inra.agrosyst.services.referential.csv.RefParcelleZonageEdiModel;
import fr.inra.agrosyst.services.referential.csv.RefPhrasesRisqueEtClassesMentionDangerParAMMModel;
import fr.inra.agrosyst.services.referential.csv.RefPhytoSubstanceActiveIphyModel;
import fr.inra.agrosyst.services.referential.csv.RefPotModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixAutreModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixCarbuModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixEspeceModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixFertiMinModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixFertiOrgaModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixIrrigModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixPhytoModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixPotModel;
import fr.inra.agrosyst.services.referential.csv.RefPrixSubstrateModel;
import fr.inra.agrosyst.services.referential.csv.RefProtocoleVgObsModel;
import fr.inra.agrosyst.services.referential.csv.RefQualityCriteriaClassModel;
import fr.inra.agrosyst.services.referential.csv.RefQualityCriteriaModel;
import fr.inra.agrosyst.services.referential.csv.RefSaActaIphyModel;
import fr.inra.agrosyst.services.referential.csv.RefSeedUnitsModel;
import fr.inra.agrosyst.services.referential.csv.RefSolArvalisModel;
import fr.inra.agrosyst.services.referential.csv.RefSolCaracteristiqueIndigoModel;
import fr.inra.agrosyst.services.referential.csv.RefSolProfondeurIndigoModel;
import fr.inra.agrosyst.services.referential.csv.RefSolTextureGeppaModel;
import fr.inra.agrosyst.services.referential.csv.RefSpeciesToSectorModel;
import fr.inra.agrosyst.services.referential.csv.RefStadeEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefStadeNuisibleEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefStationMeteoModel;
import fr.inra.agrosyst.services.referential.csv.RefStrategyLeverModel;
import fr.inra.agrosyst.services.referential.csv.RefSubstancesActivesCommissionEuropeenneModel;
import fr.inra.agrosyst.services.referential.csv.RefSubstrateModel;
import fr.inra.agrosyst.services.referential.csv.RefSupportOrganeEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefTraductionModel;
import fr.inra.agrosyst.services.referential.csv.RefTraitSdCModel;
import fr.inra.agrosyst.services.referential.csv.RefTypeAgricultureModel;
import fr.inra.agrosyst.services.referential.csv.RefTypeNotationEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefUniteEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefUnitesQualifiantEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefValeurQualitativeEDIModel;
import fr.inra.agrosyst.services.referential.csv.RefVarieteGevesModel;
import fr.inra.agrosyst.services.referential.csv.RefVarietePlantGrapeModel;
import fr.inra.agrosyst.services.referential.csv.RefZoneClimatiqueIphyModel;
import fr.inra.agrosyst.services.referential.csv.RegionDto;
import fr.inra.agrosyst.services.referential.csv.SolsArvalisRegionsModel;
import fr.inra.agrosyst.services.referential.csv.importApi.RefActaDosageSaRootExportModel;
import fr.inra.agrosyst.services.referential.csv.importApi.RefActaDosageSpcRootExportModel;
import fr.inra.agrosyst.services.referential.csv.importApi.RefActaProduitRootExportModel;
import fr.inra.agrosyst.services.referential.csv.iphy.RefRcesoCaseGroundWaterModel;
import fr.inra.agrosyst.services.referential.csv.iphy.RefRcesoFuzzySetGroundWaterModel;
import fr.inra.agrosyst.services.referential.csv.iphy.RefRcesoRulesGroundWaterModel;
import fr.inra.agrosyst.services.referential.csv.iphy.RefRcesuRunoffPotRulesParcModel;
import fr.inra.agrosyst.services.referential.json.ApiImportResults;
import fr.inra.agrosyst.services.referential.json.Dosages;
import fr.inra.agrosyst.services.referential.json.RefActaDosageSaRootExport;
import fr.inra.agrosyst.services.referential.json.RefActaDosageSpcRootExport;
import fr.inra.agrosyst.services.referential.json.RefActaProduitRootExport;
import fr.inra.agrosyst.services.referential.json.RefApiActaSubstanceActive;
import fr.inra.agrosyst.services.referential.json.RefApiActaTraitementsProduit;
import fr.inra.agrosyst.services.referential.json.Sol;
import fr.inra.agrosyst.services.referential.maa.MaaApiImportTask;
import freemarker.template.utility.StringUtil;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.nuiton.csv.Export;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportModel;
import org.nuiton.topia.persistence.TopiaDao;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.topia.persistence.support.TopiaJpaSupport;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
@Getter
@Setter
public class ImportServiceImpl extends AbstractAgrosystService implements ImportService {

    private static final Log LOGGER = LogFactory.getLog(ImportServiceImpl.class);

    public static final String SANS_DOSE_UNIT = "SANS_DOSE";
    public static final String JAVA_IO_TMPDIR = System.getProperty(StandardSystemProperty.JAVA_IO_TMPDIR.key()) + "/";

    public static final java.util.function.Function<RefLocationDto, String> GET_REF_LOCATION_DTO_CODE_INSEE = input -> {
        String rawCodeInsee = input.getCodeInsee();
        if (rawCodeInsee == null) {
            String codeDepartement = input.getDepartement();
            String codeCommune = input.getCodeCommune();
            if (codeDepartement.length() == 3) { // Cas particulier des DOM/TOM
                codeCommune = Strings.padStart(codeCommune, 2, '0');
            } else {
                codeCommune = Strings.padStart(codeCommune, 3, '0');
            }
            rawCodeInsee = codeDepartement + codeCommune;
        }
        return Strings.padStart(rawCodeInsee, 5, '0');
    };

    public static final java.util.function.Function<RefLocationDto, String> GET_REF_LOCATION_DTO_PRETTY_COMMUNE = input -> {
        String result = input.getCommune();
        String article = input.getArticleCommune();
        if (StringUtils.isNotBlank(article)) {
            article = article.trim();
            if (article.startsWith("(")) {
                article = article.substring(1);
            }
            if (article.endsWith(")")) {
                article = article.substring(0, article.length() - 1);
            }
            if (!article.endsWith("'")) {
                article += " ";
            }
            result = article + result;
        }
        return result;
    };

    protected static final java.util.function.Function<RefActaTraitementsProduit, String> GET_ACTA_TRAITEMENTS_PRODUITS_ID_PRODUIT_CODE_TRAITEMENT =
            input -> String.format(
                    "%s_%s",
                    input.getId_produit().trim(),
                    input.getCode_traitement().trim()
            );

    protected static final java.util.function.Function<RefActaTraitementsProduitDto, String> GET_ACTA_TRAITEMENTS_PRODUITS_ID_PRODUIT_CODE_TRAITEMENT_FROM_DTO =
            input -> String.format(
                    "%s_%s",
                    input.getId_produit().trim(),
                    input.getCode_traitement().trim()
            );

    public static final Function<RefEspeceDto, String> GET_ESPECE_DTO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%s",
                    RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, input.getCode_type_saisonnier_AEE(),
                    RefEspece.PROPERTY_CODE_DESTINATION__AEE, input.getCode_destination_AEE()
            );

    private static final java.util.function.Function<RefEspeceDto, String> GET_ESPECE_MAA_DTO_NATURAL_ID = input ->
            String.format(
                    "%s=%s %s=%s %s=%s %s=%s %s=%s",
                    RefEspece.PROPERTY_CODE_ESPECE_BOTANIQUE, input.getCode_espece_botanique(),
                    RefEspece.PROPERTY_CODE_QUALIFIANT__AEE, input.getCode_qualifiant_AEE(),
                    RefEspece.PROPERTY_CODE_TYPE_SAISONNIER__AEE, input.getCode_type_saisonnier_AEE(),
                    RefEspece.PROPERTY_CODE_DESTINATION__AEE, input.getCode_destination_AEE(),
                    RefCultureMAA.PROPERTY_CODE_CULTURE_MAA, input.getCode_culture_maa()
            );

    public static final int API_IMPORT_TRIES = 10;
    public static final int API_IMPORT_INTERVAL = 5000;
    public static final String SOL_FROM_API_SOURCE = "API";

    protected CacheService cacheService;
    protected HarvestingPriceTopiaDao harvestingPriceDao;
    protected InputPriceTopiaDao inputPriceTopiaDao;
    protected RefActaDosageSaRootTopiaDao refActaDosageSaRootDao;
    protected RefActaDosageSpcRootTopiaDao refActaDosageSpcRootDao;
    protected RefActaDosageSPCTopiaDao refActaDosageSPCDao;
    protected RefActaGroupeCulturesTopiaDao refActaGroupeCulturesDao;
    protected RefActaProduitRootTopiaDao refActaProduitRootDao;
    protected RefActaTraitementsProduitsCategTopiaDao refActaTraitementsProduitsCategDao;
    protected RefActaTraitementsProduitTopiaDao refActaTraitementsProduitDao;
    protected RefCompositionSubstancesActivesParNumeroAMMTopiaDao refCompositionSubstancesActivesParNumeroAMMTopiaDao;
    protected RefCountryTopiaDao refCountryDao;
    protected RefCultureMAATopiaDao refCultureMAADao;
    protected RefDestinationTopiaDao refDestinationDao;
    protected RefEspeceTopiaDao refEspeceDao;
    protected RefFertiMinUNIFATopiaDao refFertiMinUNIFADao;
    protected RefGroupeCibleTraitementTopiaDao refGroupeCibleTraitementDao;
    protected RefHarvestingPriceTopiaDao refHarvestingPriceDao;
    protected RefInputUnitPriceUnitConverterTopiaDao refInputUnitPriceUnitConverterDao;
    protected RefLocationTopiaDao refLocationDao;
    protected RefMAABiocontroleTopiaDao biocontroleDao;
    protected RefMAADosesRefParGroupeCibleTopiaDao refMAADosesRefParGroupeCibles;
    protected RefPhrasesRisqueEtClassesMentionDangerParAMMTopiaDao refPhrasesRisqueEtClassesMentionDangerParAMM;
    protected RefSolArvalisTopiaDao getRefSolArvalisDao;
    protected RefSolArvalisTopiaDao refSolArvalisDao;


    protected RefActaSubstanceActiveTopiaDao refActaSubstanceActiveDao;

    protected Map<String, RefSolArvalis> getAgrosystSolByNaturalId() {
        List<RefSolArvalis> all = getRefSolArvalisDao.findAll();
        return new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_SOL_ARVALIS_NATURAL_ID::apply));
    }

    protected Map<String, RefActaDosageSaRoot> getRefActaDosageSaRootByNaturalId() {
        List<RefActaDosageSaRoot> all = refActaDosageSaRootDao.findAll();
        return new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_REF_ACTA_DOSAGE_SA_NATURAL_ID::apply));
    }

    protected Map<String, RefActaDosageSpcRoot> getRefActaDosageSpcRootByNaturalId() {
        List<RefActaDosageSpcRoot> all = refActaDosageSpcRootDao.findAll();
        return new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_REF_ACTA_DOSAGE_SPC_NATURAL_ID::apply));
    }

    protected Map<String, RefActaProduitRoot> getRefActaProduitRootByNaturalId() {
        List<RefActaProduitRoot> all = refActaProduitRootDao.findAll();
        return new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_REF_ACTA_PRODUIT_NATURAL_ID::apply));
    }

    protected Map<String, RefActaTraitementsProduit> getRefActaTraitementsProduitByNaturalId() {
        List<RefActaTraitementsProduit> all = refActaTraitementsProduitDao.findAll();
        return new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_ACTA_TRAITEMENTS_PRODUITS_NATURAL_ID::apply));
    }

    protected MultiValuedMap<String, RefActaTraitementsProduit> getRefActaTraitementsProduitByIdProduitCodeTraitementMaa() {
        List<RefActaTraitementsProduit> all = refActaTraitementsProduitDao.findAll();
        MultiValuedMap<String, RefActaTraitementsProduit> result = new HashSetValuedHashMap<>();
        for (RefActaTraitementsProduit produit : all) {
            result.put(GET_ACTA_TRAITEMENTS_PRODUITS_ID_PRODUIT_CODE_TRAITEMENT.apply(produit), produit);
        }
        return result;
    }

    protected Map<String, RefActaSubstanceActive> getRefActaSubstanceActiveByNaturalId() {
        List<RefActaSubstanceActive> all = refActaSubstanceActiveDao.findAll();
        return new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_ACTA_SUBSTANCE_ACTIVE_NATURAL_ID::apply));
    }

    protected Map<String, RefMAADosesRefParGroupeCible> getRefDosesRefParGroupeCiblesByNaturalId(int campagne) {
        List<RefMAADosesRefParGroupeCible> all = refMAADosesRefParGroupeCibles.forCampagneEquals(campagne).findAll();
        return Maps.newHashMap(Maps.uniqueIndex(all, Referentials.GET_MAA_DOSES_REF_PAR_GROUPE_CIBLE_NATURAL_ID::apply));
    }

    protected Map<String, RefMAABiocontrole> getRefMAABiocontrolesByNaturalId(int campagne) {
        List<RefMAABiocontrole> all = biocontroleDao.forCampagneEquals(campagne).findAll();
        return new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_MAA_BIOCONTROLE_NATURAL_ID::apply));
    }

    protected <T extends ReferentialEntity> ImportResult runSimpleImport(InputStream input,
                                                                         Class<T> entityClass,
                                                                         ImportModel<T> model,
                                                                         Function<T, String> getNaturalIdFunction) {

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        String entityClassName = entityClass.getSimpleName();
        try {
            // declare dao
            TopiaDao<T> dao = context.getDaoSupplier().getDao(entityClass);

            List<T> all = dao.findAll();
            Map<String, T> persistedEntitiesByNaturalIds = Maps.uniqueIndex(all, getNaturalIdFunction::apply);
            Set<String> alreadyDone = Sets.newHashSet();

            int line = 2;
            try (Import<T> importer = Import.newImport(model, input)) {
                Binder<T, T> binder = BinderFactory.newBinder(entityClass);
                for (T entity : importer) {
                    String id = getNaturalIdFunction.apply(entity);
                    if (alreadyDone.contains(id)) {
                        String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", entityClassName, line, id);
                        if (LOGGER.isErrorEnabled()) {
                            LOGGER.error(message);
                        }
                        result.addError(message);
                    } else {
                        alreadyDone.add(id);
                        if (persistedEntitiesByNaturalIds.containsKey(id)) {
                            T existing = persistedEntitiesByNaturalIds.get(id);
                            binder.copyExcluding(entity, existing,
                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                    TopiaEntity.PROPERTY_TOPIA_VERSION);
                            dao.update(existing);
                            result.incUpdated();
                        } else {
                            dao.create(entity);
                            result.incCreated();
                        }
                    }
                    line++;
                }
            } catch (Exception ee) {
                throw new AgrosystTechnicalException(String.format("Import Failed on line %d, cause: %s", line, ee.getMessage()), ee);
            }

            getTransaction().commit();
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: ", eee);
            }
            result.addError(eee.getMessage() + "\n Import annulé");
            // Reset all counters
            result.setCreated(0);
            result.setUpdated(0);
            result.setIgnored(0);
            result.setDeleted(0);
            result.setUnactivated(0);
            getTransaction().rollback();
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import finished (type=%s). Result: %s", entityClassName, result);
            LOGGER.info(message);
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;
    }

    private ImportResult runImportRefEspece(InputStream input) {

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        RefEspeceModel model = new RefEspeceModel();
        String entityClassName = RefEspece.class.getSimpleName();
        try {
            // declare dao
            List<RefEspece> all = refEspeceDao.findAll();
            Map<String, RefEspece> persistedEntitiesByNaturalIds = new HashMap<>(Maps.uniqueIndex(all, Referentials.GET_ESPECE_NATURAL_ID::apply));
            Set<String> alreadyDone = Sets.newHashSet();

            int line = 2;
            try (Import<RefEspeceDto> importer = Import.newImport(model, input)) {
                Binder<RefEspeceDto, RefEspece> refEspeceBinder = BinderFactory.newBinder(RefEspeceDto.class, RefEspece.class);
                for (RefEspeceDto refEspeceDto : importer) {
                    String id_maa = GET_ESPECE_MAA_DTO_NATURAL_ID.apply(refEspeceDto);
                    if (alreadyDone.contains(id_maa)) {
                        String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", entityClassName, line, id_maa);
                        if (LOGGER.isErrorEnabled()) {
                            LOGGER.error(message);
                        }
                        result.addError(message);
                    } else {
                        alreadyDone.add(id_maa);
                        String entity_nat_id = GET_ESPECE_DTO_NATURAL_ID.apply(refEspeceDto);
                        if (persistedEntitiesByNaturalIds.containsKey(entity_nat_id)) {
                            RefEspece existing = persistedEntitiesByNaturalIds.get(entity_nat_id);
                            refEspeceBinder.copyExcluding(refEspeceDto, existing,
                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                                    RefEspeceDto.PROPERTY_CODE_CULTURE_MAA,
                                    RefEspece.PROPERTY_CULTURES_MAA,
                                    RefEspece.PROPERTY_ACTIVE
                            );
                            if (StringUtils.isNotBlank(refEspeceDto.getCode_culture_maa()) || StringUtils.isNotBlank(refEspeceDto.getCulture_maa())) {
                                Collection<RefCultureMAA> refCultureMAAs = existing.getCulturesMaa();
                                if (refCultureMAAs == null) {
                                    refCultureMAAs = new HashSet<>();
                                    existing.setCulturesMaa(refCultureMAAs);
                                }
                                Optional<RefCultureMAA> existingCultureMaa = refCultureMAAs.stream()
                                        .filter(refCultureMAA -> refEspeceDto.getCode_culture_maa().equals(refCultureMAA.getCode_culture_maa()))
                                        .findAny();
                                if (existingCultureMaa.isPresent()) {
                                    RefCultureMAA eCmaa = existingCultureMaa.get();
                                    eCmaa.setCulture_maa(refEspeceDto.getCulture_maa());
                                    eCmaa.setActive(refEspeceDto.isActive());

                                    refCultureMAADao.update(eCmaa);
                                } else {
                                    RefCultureMAA newCultureMaa = refCultureMAADao.newInstance();
                                    newCultureMaa.setCode_culture_maa(refEspeceDto.getCode_culture_maa());
                                    newCultureMaa.setCulture_maa(refEspeceDto.getCulture_maa());
                                    newCultureMaa.setActive(refEspeceDto.isActive());

                                    refCultureMAADao.create(newCultureMaa);
                                    existing.addCulturesMaa(newCultureMaa);
                                }
                                existing.setActive(CollectionUtils.emptyIfNull(existing.getCulturesMaa()).stream().anyMatch(RefCultureMAA::isActive));

                            } else {
                                existing.setActive(refEspeceDto.isActive());
                            }

                            refEspeceDao.update(existing);
                            result.incUpdated();

                        } else {
                            RefEspece newEspece = refEspeceDao.newInstance();
                            refEspeceBinder.copyExcluding(refEspeceDto, newEspece, TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE, TopiaEntity.PROPERTY_TOPIA_VERSION,
                                    RefEspeceDto.PROPERTY_CODE_CULTURE_MAA, RefEspece.PROPERTY_CULTURES_MAA,
                                    RefEspece.PROPERTY_ACTIVE);

                            newEspece.setActive(true);
                            refEspeceDao.create(newEspece);

                            if (StringUtils.isNotBlank(refEspeceDto.getCode_culture_maa()) || StringUtils.isNotBlank(refEspeceDto.getCulture_maa())) {
                                Collection<RefCultureMAA> refCultureMAAs = new HashSet<>();
                                newEspece.setCulturesMaa(refCultureMAAs);

                                RefCultureMAA newCultureMaa = refCultureMAADao.newInstance();
                                newCultureMaa.setCode_culture_maa(refEspeceDto.getCode_culture_maa());
                                newCultureMaa.setCulture_maa(refEspeceDto.getCulture_maa());
                                newCultureMaa.setActive(refEspeceDto.isActive());
                                refCultureMAADao.create(newCultureMaa);
                                newEspece.addCulturesMaa(newCultureMaa);

                                newEspece.setActive(CollectionUtils.emptyIfNull(newEspece.getCulturesMaa()).stream().anyMatch(RefCultureMAA::isActive));

                            } else {
                                newEspece.setActive(refEspeceDto.isActive());
                            }

                            refEspeceDao.update(newEspece);

                            result.incCreated();
                            persistedEntitiesByNaturalIds.put(entity_nat_id, newEspece);
                        }
                    }
                    line++;
                }
            } catch (Exception ee) {
                throw new AgrosystTechnicalException(String.format("Import Failed on line %d, cause: %s", line, ee.getMessage()), ee);
            }

            getTransaction().commit();
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: ", eee);
            }
            result.addError(eee.getMessage() + "\n Import annulé");
            // Reset all counters
            result.setCreated(0);
            result.setUpdated(0);
            result.setIgnored(0);
            result.setDeleted(0);
            result.setUnactivated(0);
            getTransaction().rollback();
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import finished (type=%s). Result: %s", entityClassName, result);
            LOGGER.info(message);
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;
    }

    protected ImportResult runRefInputUnitPriceUnitConverterImport(InputStream input,
                                                                   ImportModel<RefInputUnitPriceUnitConverter> model) {

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        String entityClassName = RefInputUnitPriceUnitConverter.class.getSimpleName();
        try {
            // declare dao
            List<RefInputUnitPriceUnitConverter> all = refInputUnitPriceUnitConverterDao.findAll();

            Map<String, RefInputUnitPriceUnitConverter> persistedCapacityUnitRefInputUnitPriceUnitConverter = new HashMap<>();
            Map<String, RefInputUnitPriceUnitConverter> persistedMineralProductUnitRefInputUnitPriceUnitConverter = new HashMap<>();
            Map<String, RefInputUnitPriceUnitConverter> persistedOrganicProductUnitRefInputUnitPriceUnitConverter = new HashMap<>();
            Map<String, RefInputUnitPriceUnitConverter> persistedSeedPlantUnitRefInputUnitPriceUnitConverter = new HashMap<>();
            Map<String, RefInputUnitPriceUnitConverter> persistedPhytoProductUnitRefInputUnitPriceUnitConverter = new HashMap<>();
            Map<String, RefInputUnitPriceUnitConverter> persistedPotInputUnitRefInputUnitPriceUnitConverter = new HashMap<>();
            Map<String, RefInputUnitPriceUnitConverter> persistedSubstrateInputUnitRefInputUnitPriceUnitConverter = new HashMap<>();

            mapRefUnitPriceUnitConverterToUniqueIndex(
                    all,
                    persistedCapacityUnitRefInputUnitPriceUnitConverter,
                    persistedMineralProductUnitRefInputUnitPriceUnitConverter,
                    persistedOrganicProductUnitRefInputUnitPriceUnitConverter,
                    persistedSeedPlantUnitRefInputUnitPriceUnitConverter,
                    persistedPhytoProductUnitRefInputUnitPriceUnitConverter,
                    persistedPotInputUnitRefInputUnitPriceUnitConverter,
                    persistedSubstrateInputUnitRefInputUnitPriceUnitConverter);

            Set<String> alreadyDone = Sets.newHashSet();

            int line = 2;
            try (Import<RefInputUnitPriceUnitConverter> importer = Import.newImport(model, input)) {
                Binder<RefInputUnitPriceUnitConverter, RefInputUnitPriceUnitConverter> binder = BinderFactory.newBinder(RefInputUnitPriceUnitConverter.class);
                for (RefInputUnitPriceUnitConverter entity : importer) {


                    List<Pair<String, Map<String, RefInputUnitPriceUnitConverter>>> keysToPersistedEntities = getRefInputPriceUnitConverterByKey(
                            persistedCapacityUnitRefInputUnitPriceUnitConverter,
                            persistedMineralProductUnitRefInputUnitPriceUnitConverter,
                            persistedOrganicProductUnitRefInputUnitPriceUnitConverter,
                            persistedSeedPlantUnitRefInputUnitPriceUnitConverter,
                            persistedPhytoProductUnitRefInputUnitPriceUnitConverter,
                            persistedPotInputUnitRefInputUnitPriceUnitConverter,
                            persistedSubstrateInputUnitRefInputUnitPriceUnitConverter,
                            entity);

                    Iterator<Pair<String, Map<String, RefInputUnitPriceUnitConverter>>> iterator = keysToPersistedEntities.iterator();
                    boolean found = false;
                    while (!found && iterator.hasNext()) {
                        found = alreadyDone.contains(iterator.next().getKey());
                    }

                    if (found) {
                        String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", entityClassName, line, Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_NATURAL_ID.apply(entity));
                        if (LOGGER.isErrorEnabled()) {
                            LOGGER.error(message);
                        }
                        result.addError(message);
                    } else {
                        alreadyDone.addAll(keysToPersistedEntities.stream().map(Pair::getKey).toList());
                        for (Pair<String, Map<String, RefInputUnitPriceUnitConverter>> keysToPersistedEntity : keysToPersistedEntities) {
                            String key = keysToPersistedEntity.getKey();
                            Map<String, RefInputUnitPriceUnitConverter> converters = keysToPersistedEntity.getRight();
                            RefInputUnitPriceUnitConverter existing = converters.get(key);

                            if (existing != null) {
                                binder.copyExcluding(entity, existing,
                                        TopiaEntity.PROPERTY_TOPIA_ID,
                                        TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                        TopiaEntity.PROPERTY_TOPIA_VERSION);
                                refInputUnitPriceUnitConverterDao.update(existing);
                                result.incUpdated();
                            } else {
                                entity.setActive(true);
                                refInputUnitPriceUnitConverterDao.create(entity);
                                result.incCreated();
                            }
                        }
                    }
                    line++;
                }
            } catch (Exception ee) {
                throw new AgrosystTechnicalException(String.format("Import Failed on line %d, cause: %s", line, ee.getMessage()), ee);
            }

            if (result.hasErrors()) {
                getTransaction().rollback();
                result.resetCounterForError();
            } else {
                getTransaction().commit();
            }
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: ", eee);
            }
            result.addError(eee.getMessage());
            result.resetCounterForError();
            getTransaction().rollback();
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import finished (type=%s). Result: %s", entityClassName, result);
            LOGGER.info(message);
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;
    }

    private List<Pair<String, Map<String, RefInputUnitPriceUnitConverter>>> getRefInputPriceUnitConverterByKey(Map<String, RefInputUnitPriceUnitConverter> persistedCapacityUnitRefInputUnitPriceUnitConverter, Map<String, RefInputUnitPriceUnitConverter> persistedMineralProductUnitRefInputUnitPriceUnitConverter, Map<String, RefInputUnitPriceUnitConverter> persistedOrganicProductUnitRefInputUnitPriceUnitConverter, Map<String, RefInputUnitPriceUnitConverter> persistedSeedPlantUnitRefInputUnitPriceUnitConverter, Map<String, RefInputUnitPriceUnitConverter> persistedPhytoProductUnitRefInputUnitPriceUnitConverter, Map<String, RefInputUnitPriceUnitConverter> persistedPotInputUnitRefInputUnitPriceUnitConverter, Map<String, RefInputUnitPriceUnitConverter> persistedSubstrateInputUnitRefInputUnitPriceUnitConverter, RefInputUnitPriceUnitConverter entity) {
        String capacityUnitPriceUnitKey = entity.getCapacityUnit() == null ? null : Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_CAPACITY_UNIT_NATURAL_ID.apply(entity);
        String mineralUnitPriceUnitKey = entity.getMineralProductUnit() == null ? null : Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_MINERAL_UNIT_NATURAL_ID.apply(entity);
        String organicUnitPriceUnitKey = entity.getOrganicProductUnit() == null ? null : Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_ORGANIC_UNIT_NATURAL_ID.apply(entity);
        String seedPlanUnitPriceUnitKey = entity.getSeedPlantUnit() == null ? null : Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_SEED_PLAN_UNIT_NATURAL_ID.apply(entity);
        String phytoProductUnitPriceUnitKey = entity.getPhytoProductUnit() == null ? null : Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_PHYTO_PRODUCT_UNIT_NATURAL_ID.apply(entity);
        String potInputUnitPriceUnitKey = entity.getPotInputUnit() == null ? null : Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_POT_INPUT_UNIT_NATURAL_ID.apply(entity);
        String substrateInputUnitPriceUnitKey = entity.getSubstrateInputUnit() == null ? null : Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_SUBSTRATE_INPUT_UNIT_NATURAL_ID.apply(entity);

        List<Pair<String, Map<String, RefInputUnitPriceUnitConverter>>> keysToPersistedEntities = new ArrayList<>();

        keysToPersistedEntities.add(Pair.of(capacityUnitPriceUnitKey, persistedCapacityUnitRefInputUnitPriceUnitConverter));
        keysToPersistedEntities.add(Pair.of(mineralUnitPriceUnitKey, persistedMineralProductUnitRefInputUnitPriceUnitConverter));
        keysToPersistedEntities.add(Pair.of(organicUnitPriceUnitKey, persistedOrganicProductUnitRefInputUnitPriceUnitConverter));
        keysToPersistedEntities.add(Pair.of(seedPlanUnitPriceUnitKey, persistedSeedPlantUnitRefInputUnitPriceUnitConverter));
        keysToPersistedEntities.add(Pair.of(phytoProductUnitPriceUnitKey, persistedPhytoProductUnitRefInputUnitPriceUnitConverter));
        keysToPersistedEntities.add(Pair.of(potInputUnitPriceUnitKey, persistedPotInputUnitRefInputUnitPriceUnitConverter));
        keysToPersistedEntities.add(Pair.of(substrateInputUnitPriceUnitKey, persistedSubstrateInputUnitRefInputUnitPriceUnitConverter));
        keysToPersistedEntities = keysToPersistedEntities.stream().filter(stringMapPair -> stringMapPair.getLeft() != null).collect(Collectors.toList());
        return keysToPersistedEntities;
    }

    private void mapRefUnitPriceUnitConverterToUniqueIndex(
            List<RefInputUnitPriceUnitConverter> all,
            Map<String, RefInputUnitPriceUnitConverter> persistedCapacityUnitRefInputUnitPriceUnitConverter,
            Map<String, RefInputUnitPriceUnitConverter> persistedMineralProductUnitRefInputUnitPriceUnitConverter,
            Map<String, RefInputUnitPriceUnitConverter> persistedOrganicProductUnitRefInputUnitPriceUnitConverter,
            Map<String, RefInputUnitPriceUnitConverter> persistedSeedPlantUnitRefInputUnitPriceUnitConverter,
            Map<String, RefInputUnitPriceUnitConverter> persistedPhytoProductUnitRefInputUnitPriceUnitConverter,
            Map<String, RefInputUnitPriceUnitConverter> persistedPotInputUnitRefInputUnitPriceUnitConverter,
            Map<String, RefInputUnitPriceUnitConverter> persistedSubstrateInputUnitRefInputUnitPriceUnitConverter) {

        for (RefInputUnitPriceUnitConverter converter : all) {
            if (converter.getCapacityUnit() != null) {
                persistedCapacityUnitRefInputUnitPriceUnitConverter.put(Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_CAPACITY_UNIT_NATURAL_ID.apply(converter), converter);
            }

            if (converter.getMineralProductUnit() != null) {
                persistedMineralProductUnitRefInputUnitPriceUnitConverter.put(Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_MINERAL_UNIT_NATURAL_ID.apply(converter), converter);
            }

            if (converter.getOrganicProductUnit() != null) {
                persistedOrganicProductUnitRefInputUnitPriceUnitConverter.put(Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_ORGANIC_UNIT_NATURAL_ID.apply(converter), converter);
            }

            if (converter.getSeedPlantUnit() != null) {
                persistedSeedPlantUnitRefInputUnitPriceUnitConverter.put(Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_SEED_PLAN_UNIT_NATURAL_ID.apply(converter), converter);
            }

            if (converter.getPhytoProductUnit() != null) {
                persistedPhytoProductUnitRefInputUnitPriceUnitConverter.put(Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_PHYTO_PRODUCT_UNIT_NATURAL_ID.apply(converter), converter);
            }

            if (converter.getPotInputUnit() != null) {
                persistedPotInputUnitRefInputUnitPriceUnitConverter.put(Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_POT_INPUT_UNIT_NATURAL_ID.apply(converter), converter);
            }

            if (converter.getSubstrateInputUnit() != null) {
                persistedSubstrateInputUnitRefInputUnitPriceUnitConverter.put(Referentials.GET_REF_INPUT_UNIT_PRICE_UNIT_SUBSTRATE_INPUT_UNIT_NATURAL_ID.apply(converter), converter);
            }
        }
    }

    protected ImportResult runRefHarvestingPriceImport(InputStream input,
                                                       ImportModel<RefHarvestingPrice> model) {

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        String entityClassName = RefHarvestingPrice.class.getSimpleName();
        try {

            // declare dao
            List<RefHarvestingPrice> all = refHarvestingPriceDao.findAll();

            List<RefHarvestingPrice> persistedRefCampaignPrices = all.stream().filter(refHarvestingPrice -> refHarvestingPrice.getCampaign() > 0).collect(Collectors.toList());
            Map<String, RefHarvestingPrice> persistedRefHarvestingPriceByNaturalIds = new HashMap<>(Maps.uniqueIndex(persistedRefCampaignPrices, Referentials.GET_REF_HARVESTING_PRICE_CAMPAIGN_NATURAL_ID::apply));

            List<RefHarvestingPrice> persistedRefStrategiePrices = all.stream().filter(refHarvestingPrice -> StringUtils.isNotBlank(refHarvestingPrice.getCode_scenario())).collect(Collectors.toList());
            Map<String, RefHarvestingPrice> persistedReScenarioPriceByNaturalIds = new HashMap<>(Maps.uniqueIndex(persistedRefStrategiePrices, Referentials.GET_REF_HARVESTING_PRICE_SCENARIO_NATURAL_ID::apply));

            Set<String> alreadyDone = Sets.newHashSet();


            List<RefHarvestingPrice> refHarvestingPriceToUpdates = new ArrayList<>();
            List<RefHarvestingPrice> refHarvestingPriceToCreate = new ArrayList<>();

            Binder<RefHarvestingPrice, RefHarvestingPrice> binder = BinderFactory.newBinder(RefHarvestingPrice.class);
            int line = 2;
            try (Import<RefHarvestingPrice> importer = Import.newImport(model, input)) {
                for (RefHarvestingPrice entity : importer) {

                    boolean isScenarioRefPrice = StringUtils.isNotBlank(entity.getCode_scenario());
                    boolean isCampaignRefPrice = entity.getCampaign() > 0;

                    if (!isScenarioRefPrice && !isCampaignRefPrice) {
                        String message = String.format("Ni campagne, ni code scénario renseigné (type=%s, L%d)", entityClassName, line);
                        if (LOGGER.isErrorEnabled()) {
                            LOGGER.error(message);
                        }
                        result.addError(message);
                        line++;
                        continue;
                    }

                    if (AbstractDestinationAndPriceModel.MARKETING_PERIOD_PARSER_ERROR == entity.getMarketingPeriod()) {
                        String message = String.format("'Période de commercialisation - Mois' valeur non valide, doit-être compris entre 0 et 11 ou exprimée en literal entre 'JANVIER' et 'DECEMBRE' (type=%s, L%d)", entityClassName, line);
                        if (LOGGER.isErrorEnabled()) {
                            LOGGER.error(message);
                        }
                        result.addError(message);
                        line++;
                        continue;
                    }

                    if (entity.getMarketingPeriodDecade() != null && (entity.getMarketingPeriodDecade() > 3 || entity.getMarketingPeriodDecade() < 1)) {
                        String message = String.format("'Période de commercialisation - Décade' valeur non valide, doit-être compris entre 1 et 3 (type=%s, L%d)", entityClassName, line);
                        if (LOGGER.isErrorEnabled()) {
                            LOGGER.error(message);
                        }
                        result.addError(message);
                        line++;
                        continue;
                    }

                    String refHarvestingPriceForScenarioId = null;
                    String refHarvestingPriceForCampaignId = null;

                    if (isScenarioRefPrice) {
                        refHarvestingPriceForScenarioId = Referentials.GET_REF_HARVESTING_PRICE_SCENARIO_NATURAL_ID.apply(entity);
                    }

                    if (isCampaignRefPrice) {
                        refHarvestingPriceForCampaignId = Referentials.GET_REF_HARVESTING_PRICE_CAMPAIGN_NATURAL_ID.apply(entity);
                    }

                    if (isCampaignRefPrice && alreadyDone.contains(refHarvestingPriceForCampaignId) ||
                            isScenarioRefPrice && alreadyDone.contains(refHarvestingPriceForScenarioId)) {

                        List<String> duplicateValues = new ArrayList<>();

                        if (alreadyDone.contains(refHarvestingPriceForCampaignId))
                            duplicateValues.add(refHarvestingPriceForCampaignId);
                        if (alreadyDone.contains(refHarvestingPriceForScenarioId))
                            duplicateValues.add(refHarvestingPriceForScenarioId);

                        String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", entityClassName, line, StringUtils.join(duplicateValues, ';'));
                        result.addError(message);

                        if (LOGGER.isErrorEnabled()) {
                            LOGGER.error(message);
                        }
                    } else {

                        if (isCampaignRefPrice) alreadyDone.add(refHarvestingPriceForCampaignId);
                        if (isScenarioRefPrice) alreadyDone.add(refHarvestingPriceForScenarioId);

                        RefHarvestingPrice existing =
                                isScenarioRefPrice ? persistedReScenarioPriceByNaturalIds.get(refHarvestingPriceForScenarioId) : persistedRefHarvestingPriceByNaturalIds.get(refHarvestingPriceForCampaignId);

                        if (existing != null) {

                            binder.copyExcluding(entity, existing,
                                    TopiaEntity.PROPERTY_TOPIA_ID,
                                    TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                    TopiaEntity.PROPERTY_TOPIA_VERSION,
                                    RefHarvestingPrice.PROPERTY_CAMPAIGN,
                                    RefHarvestingPrice.PROPERTY_CODE_SCENARIO);

                            if (entity.getCampaign() != 0 && existing.getCampaign() == 0) {
                                existing.setCampaign(entity.getCampaign());
                            }

                            if (StringUtils.isNotBlank(entity.getCode_scenario()) && StringUtils.isBlank(existing.getCode_scenario())) {
                                existing.setCode_scenario(entity.getCode_scenario());
                            }

                            refHarvestingPriceToUpdates.add(existing);

                            persistedRefHarvestingPriceByNaturalIds.replace(refHarvestingPriceForCampaignId, existing);
                            persistedReScenarioPriceByNaturalIds.replace(refHarvestingPriceForScenarioId, existing);

                            result.incUpdated();

                        } else {

                            refHarvestingPriceToCreate.add(entity);
                            result.incCreated();

                        }
                    }
                    line++;
                }
            } catch (Exception ee) {
                throw new AgrosystTechnicalException(String.format("Import Failed on line %d, cause: %s", line, ee.getMessage()), ee);
            }

            if (result.hasErrors()) {
                result.resetCounterForError();
            } else {

                refHarvestingPriceDao.createAll(refHarvestingPriceToCreate);
                refHarvestingPriceDao.updateAll(refHarvestingPriceToUpdates);

                getTransaction().commit();
            }
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: ", eee);
            }
            result.addError(eee.getMessage());
            result.resetCounterForError();
            getTransaction().rollback();
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import finished (type=%s). Result: %s", entityClassName, result);
            LOGGER.info(message);
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;
    }

    /**
     * I must extends T and implements ApiImportResults
     *
     * @param <T>                  Targeted Type class
     * @param <I>                  Imported Type class
     * @param input                csv input stream
     * @param targetedEntityClass  Targeted class
     * @param model                csv model mapping
     * @param getNaturalIdFunction function that return the natural id of persisted entity
     * @return Summary of result
     */
    protected <T extends ReferentialEntity, I extends ReferentialEntity> ImportResult runEntityFromApiSimpleImport(InputStream input,
                                                                                                                   Class<T> targetedEntityClass,
                                                                                                                   ImportModel<I> model,
                                                                                                                   Function<T, String> getNaturalIdFunction) {

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        String entityClassName = targetedEntityClass.getSimpleName();
        try (Import<I> importer = Import.newImport(model, input)) {
            // declare dao
            TopiaDao<T> dao = context.getDaoSupplier().getDao(targetedEntityClass);

            List<T> all = dao.findAll();
            Map<String, T> map = Maps.uniqueIndex(all, getNaturalIdFunction::apply);
            Set<String> alreadyDone = Sets.newHashSet();

            Binder<T, T> binder = BinderFactory.newBinder(targetedEntityClass);
            int line = 2;
            for (I entity : importer) {

                ImportStatus status = ((ApiImportResults) entity).getStatus();

                if (ImportStatus.DUPLICATED_NEW_KEY.equals(status)) {
                    result.incIgnored();
                    line++;
                    continue;
                }

                String id = getNaturalIdFunction.apply((T) entity);
                if (alreadyDone.contains(id)) {
                    String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", entityClassName, line, id);
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(message);
                    }
                    result.addError(message);
                } else {
                    alreadyDone.add(id);

                    if (map.containsKey(id)) {
                        T existing = map.get(id);
                        binder.copyExcluding((T) entity, existing,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION,
                                "status");
                        dao.update(existing);

                        result.incUpdated();

                    } else {
                        T newInstance = dao.newInstance();
                        binder.copyExcluding((T) entity, newInstance, "status");

                        dao.create(newInstance);
                        result.incCreated();
                    }
                }
                line++;
            }

            getTransaction().commit();
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: " + eee.getMessage(), eee);
            }
            result.addError(eee.getMessage());
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import finished (type=%s). Result: %s", entityClassName, result);
            LOGGER.info(message);
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;
    }

    protected RefSolArvalisImpl bindSolToRefSolArvalis(Sol sol) {
        RefSolArvalisImpl refSolArvalis = new RefSolArvalisImpl();
        refSolArvalis.setId_type_sol(sol.getId_type_sol());
        refSolArvalis.setSol_region(sol.getRegion());
        refSolArvalis.setSol_calcaire(sol.getCalcaire());
        refSolArvalis.setSol_nom(sol.getNom_arvalis());
        refSolArvalis.setSol_pierrosite(sol.getPierrosite());
        refSolArvalis.setSol_texture(sol.getTexture());
        refSolArvalis.setSol_profondeur(sol.getProfondeur());
        refSolArvalis.setNom_referenciel_pedologique_francais(sol.getNom_referenciel_pedologique_francais());
        refSolArvalis.setSol_hydromorphie(sol.getHydromorphie());
        refSolArvalis.setSol_nom(sol.getNom_sol());
        return refSolArvalis;
    }

    protected InputStream produceCsvImportResult(String jsonResult, Class<?> klass) {
        InputStream result = null;
        try {
            if (StringUtils.isNoneBlank(jsonResult)) {

                Gson gson = new Gson();

                JsonElement element = JsonParser.parseString(jsonResult);
                if (element.isJsonArray()) {
                    JsonArray jsonArray = JsonParser.parseString(jsonResult).getAsJsonArray();

                    if (RefSolArvalis.class.equals(klass)) {
                        List<RefSolArvalis> solToImports = new ArrayList<>();
                        Map<String, RefSolArvalis> entityByNaturaIds = getAgrosystSolByNaturalId();
                        Set<String> duplicatedSolFromAPI = Sets.newHashSet();

                        for (JsonElement importedSol : jsonArray) {

                            Record item = gson.fromJson(importedSol, Record.class);
                            Sol sol = item.getFields();
                            RefSolArvalisImpl refSolArvalis = bindSolToRefSolArvalis(sol);
                            refSolArvalis.setSource(SOL_FROM_API_SOURCE);
                            solToImports.add(refSolArvalis);

                            String id = sol.getId_type_sol();
                            String solId = String.format("%s=%s", RefSolArvalis.PROPERTY_ID_TYPE_SOL, id);
                            RefSolArvalis existing = entityByNaturaIds.remove(solId);

                            ImportStatus status = getImportStatus(duplicatedSolFromAPI, refSolArvalis, id, existing);
                            sol.setStatus(status);

                            String agrosystSolId = existing != null ? existing.getTopiaId() : null;
                            sol.setAgrosystId(agrosystSolId);
                        }

                        RefSolArvalisModel model = new RefSolArvalisModel();
                        result = getApiAnalyseResult_CSV(klass, model, solToImports);
                    }

                } else if (element.isJsonObject()) {
                    if (RefActaDosageSpcRoot.class.equals(klass)) {
                        Dosages dosages = mapDosageResult(jsonResult);
                        RefActaDosageSpcRootExportModel model = new RefActaDosageSpcRootExportModel();
                        List<RefActaDosageSpcRootExport> contents = dosages.getDosagesSPC();
                        Map<String, RefActaDosageSpcRoot> entityByNaturaIds = getRefActaDosageSpcRootByNaturalId();
                        result = doApiDosagesImport(RefActaDosageSpcRoot.class, RefActaDosageSpcRootExport.class, model, contents, entityByNaturaIds, Referentials.GET_REF_ACTA_DOSAGE_SPC_NATURAL_ID);

                    } else if (RefActaDosageSaRoot.class.equals(klass)) {
                        Dosages dosages = mapDosageResult(jsonResult);
                        RefActaDosageSaRootExportModel model = new RefActaDosageSaRootExportModel();
                        List<RefActaDosageSaRootExport> contents = dosages.getDosagesSA();
                        Map<String, RefActaDosageSaRoot> entityByNaturaIds = getRefActaDosageSaRootByNaturalId();
                        result = doApiDosagesImport(RefActaDosageSaRoot.class, RefActaDosageSaRootExport.class, model, contents, entityByNaturaIds, Referentials.GET_REF_ACTA_DOSAGE_SA_NATURAL_ID);

                    } else if (RefActaProduitRoot.class.equals(klass)) {
                        Dosages dosages = mapDosageResult(jsonResult);
                        RefActaProduitRootExportModel model = new RefActaProduitRootExportModel(refCountryDao);
                        List<RefActaProduitRootExport> contents = dosages.getProduits();
                        Map<String, RefActaProduitRoot> entityByNaturaIds = getRefActaProduitRootByNaturalId();
                        result = doApiDosagesImport(RefActaProduitRoot.class, RefActaProduitRootExport.class, model, contents, entityByNaturaIds, Referentials.GET_REF_ACTA_PRODUIT_NATURAL_ID);

                    } else if (RefActaTraitementsProduit.class.equals(klass)) {
                        Dosages dosages = mapDosageResult(jsonResult);
                        RefActaTraitementsProduitModel model = new RefActaTraitementsProduitModel(refCountryDao);
                        List<RefApiActaTraitementsProduit> contents = dosages.getTraitements();
                        Map<String, RefActaTraitementsProduit> entityByNaturaIds = getRefActaTraitementsProduitByNaturalId();
                        result = doApiDosagesImport(RefActaTraitementsProduit.class, RefApiActaTraitementsProduit.class, model, contents, entityByNaturaIds, Referentials.GET_ACTA_TRAITEMENTS_PRODUITS_NATURAL_ID);

                    } else if (RefActaSubstanceActive.class.equals(klass)) {
                        Dosages dosages = mapDosageResult(jsonResult);
                        RefActaSubstanceActiveModel model = new RefActaSubstanceActiveModel();
                        List<RefApiActaSubstanceActive> contents = dosages.getSubstancesActives();
                        Map<String, RefActaSubstanceActive> entityByNaturaIds = getRefActaSubstanceActiveByNaturalId();
                        result = doApiDosagesImport(RefActaSubstanceActive.class, RefApiActaSubstanceActive.class, model, contents, entityByNaturaIds, Referentials.GET_ACTA_SUBSTANCE_ACTIVE_NATURAL_ID);

                    } else {
                        throw new AgrosystImportException(String.format("Désolé l'import de la classe :%s n'est pas encore géré", klass));
                    }
                }

            }

        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: " + eee.getMessage(), eee);
            }
        }

        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import parse (type=%s).", RefSolArvalis.class.getSimpleName());
            LOGGER.info(message);
        }

        return result;
    }

    // T RefActaDosageSaRootExport
    // E RefActaDosageSaRoot
    private <T extends ReferentialEntity, E extends ReferentialEntity> InputStream doApiDosagesImport(
            Class<E> eClass,
            Class<T> tClass,
            ExportModel model,
            List<T> contents,
            Map<String, E> entityByNaturaIds,
            Function<E, String> getNaturalId) throws InstantiationException, IllegalAccessException {
        InputStream result;
        Set<String> duplicatedImportedEntity = Sets.newHashSet();
        for (T importedEntity_ : contents) {
            ApiImportResults importedEntity = (ApiImportResults) importedEntity_;
            String id = getNaturalId.apply((E) importedEntity);

            E existing = entityByNaturaIds.remove(id);
            // if entity has be set to unactivated let-it unactivated ref #7747
            boolean active = existing == null || existing.isActive();
            importedEntity.setActive(active);

            ImportStatus status = getImportStatus(duplicatedImportedEntity, importedEntity, id, existing);
            importedEntity.setStatus(status);
        }

        List<T> entityToRemoves = new ArrayList<>();
        Binder<E, T> binder = BinderFactory.newBinder(eClass, tClass);
        for (E entityToRemove : entityByNaturaIds.values()) {
            T entityToRemoveExport = tClass.newInstance();
            binder.copy(entityToRemove, entityToRemoveExport);
            entityToRemoves.add(entityToRemoveExport);
        }

        List<T> exportResult = new ArrayList<>();
        exportResult.addAll(contents);
        exportResult.addAll(entityToRemoves);

        result = getApiAnalyseResult_CSV(tClass, model, exportResult);
        return result;
    }

    private ImportStatus getImportStatus(Set<String> duplicatedEntityFromAPI, ReferentialEntity apiEntity, String id, ReferentialEntity agrosystEntity) {
        ImportStatus status;
        if (duplicatedEntityFromAPI.contains(id)) {
            status = ImportStatus.DUPLICATED_NEW_KEY;
        } else {
            duplicatedEntityFromAPI.add(id);
            if (agrosystEntity != null) {
                status = apiEntity.equals(agrosystEntity) ? ImportStatus.IDENTICAL : ImportStatus.UPDATED;
            } else {
                status = ImportStatus.NEW;
            }
        }
        return status;
    }

    protected ImportResult runActaDocageSpcImport(InputStream input,
                                                  ImportModel<RefActaDosageSPC> model) {


        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        try (Import<RefActaDosageSPC> importer = Import.newImport(model, input)) {
            // declare dao
            List<RefActaDosageSPC> allActaDosageSPC = refActaDosageSPCDao.findAll();
            Map<String, RefActaDosageSPC> dosageSpcByNaturalIds = Maps.uniqueIndex(allActaDosageSPC, Referentials.GET_ACTA_DOSAGE_SPC_NATURAL_ID::apply);
            Set<String> alreadyDone = Sets.newHashSet();

            Map<Integer, Set<Integer>> groupCultureIdCultures = getGroupCulturesByCrop();

            // map of id_culture, id_produit, id_traitement => PhytoProductUnit
            // this map is use to check consistence for PhytoProductUnit
            MultiKeyMap<Object, PhytoProductUnit> cropGroupProductTraitement = getAgrosystPhytoProductUnitByCropIdProductIdTreatmentId(result, allActaDosageSPC);

            Binder<RefActaDosageSPC, RefActaDosageSPC> binder = BinderFactory.newBinder(RefActaDosageSPC.class);

            int line = 2;
            for (RefActaDosageSPC entity : importer) {
                int cropId = entity.getId_culture();
                String productId = entity.getId_produit();
                int treatmentId = entity.getId_traitement();

                convertPhytoProductUnitToValidUnits(entity);

                PhytoProductUnit dosage_spc_unite = entity.getDosage_spc_unite();

                // add PhytoProductUnit for current cropId, productId and treatment if not already define
                PhytoProductUnit refPhytoProductUnit = cropGroupProductTraitement.get(cropId, productId, treatmentId);
                if (refPhytoProductUnit == null) {
                    cropGroupProductTraitement.put(cropId, productId, treatmentId, dosage_spc_unite);
                }

                // all crops that needs to be checked
                Set<Integer> matchingCropIds = groupCultureIdCultures.get(cropId);

                // add all units found matching corresponding key
                Set<PhytoProductUnit> phytoProductUnits = getPhytoProductUnitsForCropIdProductIdTreatmentId(
                        cropGroupProductTraitement, entity, cropId, productId, treatmentId, matchingCropIds);

                // if more than on unit is found then product is not valid
                if (phytoProductUnits.size() > 1) {
                    String unitDesc = " '%s',";
                    StringBuilder units = new StringBuilder();
                    for (PhytoProductUnit phytoProductUnit : phytoProductUnits) {
                        units.append(String.format(unitDesc, phytoProductUnit.toString()));
                    }
                    String errorUnits = units.substring(0, units.lastIndexOf(","));
                    String error =
                            String.format("Ligne '%d', Culture '%d', des unités hétérogènes ont été remontées pour le produit '%s' et le traitement '%s', %s.",
                                    line, cropId, productId, treatmentId, errorUnits);
                    result.addError(error);
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(error);
                    }
                    line++;
                    continue;
                }

                String id = Referentials.GET_ACTA_DOSAGE_SPC_NATURAL_ID.apply(entity);
                if (alreadyDone.contains(id)) {
                    String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", RefActaDosageSPC.class.getSimpleName(), line, id);
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(message);
                    }
                    result.addError(message);
                } else {
                    alreadyDone.add(id);
                    if (dosageSpcByNaturalIds.containsKey(id)) {
                        RefActaDosageSPC existing = dosageSpcByNaturalIds.get(id);
                        binder.copyExcluding(entity, existing,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION);
                        refActaDosageSPCDao.update(existing);
                        result.incUpdated();
                    } else {
                        refActaDosageSPCDao.create(entity);
                        result.incCreated();
                    }
                }
                line++;
            }

            getTransaction().commit();

        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: " + eee.getMessage(), eee);
            }
            result.addError(eee.getMessage());
            getTransaction().rollback();
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import finished (type=%s). Result: %s", RefActaDosageSPC.class.getSimpleName(), result);
            LOGGER.info(message);
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;
    }

    protected ImportResult runRefMAADosesRefParGroupeCibleImport(InputStream input,
                                                                 ImportModel<RefMAADosesRefParGroupeCible> model) {
        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        try (Import<RefMAADosesRefParGroupeCible> importer = Import.newImport(model, input)) {
            // declare dao
            List<RefMAADosesRefParGroupeCible> allDosage = refMAADosesRefParGroupeCibles.findAll();

            Map<String, RefMAADosesRefParGroupeCible> persistedEntitiesByNaturalIds =
                    Maps.uniqueIndex(allDosage, Referentials.GET_MAA_DOSES_REF_PAR_GROUPE_CIBLE_NATURAL_ID::apply);
            Set<String> alreadyDone = new HashSet<>();

            int line = 2;

            Binder<RefMAADosesRefParGroupeCible, RefMAADosesRefParGroupeCible> binder = BinderFactory.newBinder(RefMAADosesRefParGroupeCible.class);
            for (RefMAADosesRefParGroupeCible entity : importer) {

                Pair<PhytoProductUnit, Double> validDose = convertToPhytoProductUnitHAunit(entity.getUnit_dose_ref_maa(), entity.getDose_ref_maa(), entity.getVolume_max_bouillie());
                entity.setDose_ref_maa(validDose.getValue());
                entity.setUnit_dose_ref_maa(validDose.getLeft());

                String id = Referentials.GET_MAA_DOSES_REF_PAR_GROUPE_CIBLE_NATURAL_ID.apply(entity);
                if (alreadyDone.contains(id)) {
                    String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", RefMAADosesRefParGroupeCible.class.getSimpleName(), line, id);
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(message);
                    }
                    result.addError(message);
                } else {
                    alreadyDone.add(id);
                    if (persistedEntitiesByNaturalIds.containsKey(id)) {
                        RefMAADosesRefParGroupeCible existing = persistedEntitiesByNaturalIds.get(id);
                        binder.copyExcluding(entity, existing,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION);
                        refMAADosesRefParGroupeCibles.update(existing);
                        result.incUpdated();
                    } else {
                        refMAADosesRefParGroupeCibles.create(entity);
                        result.incCreated();
                    }
                }
                line++;
            }
        } catch (Exception ee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: ", ee);
            }
            result.addError(ee.getMessage() + "\n Import annulé");
            // Reset all counters
            result.setCreated(0);
            result.setUpdated(0);
            result.setIgnored(0);
            result.setDeleted(0);
            result.setUnactivated(0);
            getTransaction().rollback();
        }

        getTransaction().commit();

        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            String message = String.format("Import finished (type=%s). Result: %s", RefMAADosesRefParGroupeCible.class.getSimpleName(), result);
            LOGGER.info(message);
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;

    }

    private void convertPhytoProductUnitToValidUnits(RefActaDosageSPC entity) {
        PhytoProductUnit dosage_spc_unite = entity.getDosage_spc_unite();
        Double doseValue = entity.getDosage_spc_valeur();
        Pair<PhytoProductUnit, Double> doseRef = convertToPhytoProductUnitHAunit(dosage_spc_unite, doseValue, null);
        entity.setDosage_spc_valeur(doseRef.getValue());
        entity.setDosage_spc_unite(doseRef.getLeft());
    }

    private Pair<PhytoProductUnit, Double> convertToPhytoProductUnitHAunit(PhytoProductUnit unit, Double doseValue, Double volumeMaxBouillieLparHa) {
        //1) transformation en dose/hl puis en dose/ha
        //G_L;KG_HL;0.1
        //ML_L;L_HL;1.1
        //L_M3;L_HL;2.1
        //G_M3;G_HL;3.1
        //KG_M3;KG_HL;4.1
        //L_100M3;L_HL;0.001
        //G_100_L_D_EAU;G_HL;0.1
        if (unit != null) {
            double volumeMaxBouillieHlParHa;
            if (volumeMaxBouillieLparHa == null) {
                volumeMaxBouillieHlParHa = 10.0d;
            } else {
                volumeMaxBouillieHlParHa = volumeMaxBouillieLparHa / 100;
            }
            switch (unit) {
                case G_100L_D_EAU -> {
                    doseValue = doseValue == null ? null : (doseValue * 0.1) * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.G_HA;
                }
                case G_M3 -> {
                    doseValue = doseValue == null ? null : (doseValue * 3.1) * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.G_HA;
                }
                case KG_M3 -> {
                    doseValue = doseValue == null ? null : (doseValue * 4.1) * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.KG_HA;
                }
                case L_100M3 -> {
                    doseValue = doseValue == null ? null : (doseValue * 0.001) * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.L_HA;
                }
                case L_M3 -> {
                    doseValue = doseValue == null ? null : (doseValue * 2.1) * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.L_HA;
                }
                case G_L -> {
                    doseValue = doseValue == null ? null : (doseValue * 0.1) * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.KG_HA;
                }
                case ML_L -> {
                    doseValue = doseValue == null ? null : (doseValue * 1.1) * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.L_HA;
                }
                case L_HL -> {
                    doseValue = doseValue == null ? null : doseValue * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.L_HA;
                }
                case KG_HL -> {
                    doseValue = doseValue == null ? null : doseValue * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.KG_HA;
                }
                case G_HL -> {
                    doseValue = doseValue == null ? null : doseValue * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.G_HA;
                }
                case UNITE_HL -> {
                    doseValue = doseValue == null ? null : doseValue * volumeMaxBouillieHlParHa;
                    unit = PhytoProductUnit.UNITE_HA;
                }
            }
        }
        return Pair.of(unit, doseValue);
    }

    private Set<PhytoProductUnit> getPhytoProductUnitsForCropIdProductIdTreatmentId(MultiKeyMap<Object, PhytoProductUnit> cropGroupProductTraitement, RefActaDosageSPC refDose, int cropId, String productId, int treatmentId, Set<Integer> matchingCropIds) {
        Set<PhytoProductUnit> phytoProductUnits = Sets.newHashSet();
        if (refDose.getDosage_spc_unite() != null && refDose.isActive()) {// ignorer les doses non valides
            phytoProductUnits.add(refDose.getDosage_spc_unite());
        }
        if (CollectionUtils.isNotEmpty(matchingCropIds)) {
            matchingCropIds.add(cropId);

            for (Integer groupCropId : matchingCropIds) {
                PhytoProductUnit phytoProductUnit = cropGroupProductTraitement.get(groupCropId, productId, treatmentId);
                if (phytoProductUnit != null) {
                    phytoProductUnits.add(phytoProductUnit);
                }
            }
        }
        return phytoProductUnits;
    }

    private MultiKeyMap<Object, PhytoProductUnit> getAgrosystPhytoProductUnitByCropIdProductIdTreatmentId(ImportResult result, List<RefActaDosageSPC> allActaDosageSPC) {
        MultiKeyMap<Object, PhytoProductUnit> cropGroupProductTraitement = new MultiKeyMap<>();

        for (RefActaDosageSPC refActaDosageSPC : allActaDosageSPC) {
            if (!refActaDosageSPC.isActive()) continue;// ignorer les unités des entités non valides
            int cropId = refActaDosageSPC.getId_culture();
            String productId = refActaDosageSPC.getId_produit();
            int treatmentId = refActaDosageSPC.getId_traitement();
            // add PhytoProductUnit for current cropId, productId and treatment if not already define
            PhytoProductUnit refPhytoProductUnit = cropGroupProductTraitement.get(cropId, productId, treatmentId);
            if (refPhytoProductUnit == null) {
                cropGroupProductTraitement.put(cropId, productId, treatmentId, refActaDosageSPC.getDosage_spc_unite());
            } else if (!refPhytoProductUnit.equals(refActaDosageSPC.getDosage_spc_unite())) {
                String warning =
                        String.format("Depuis les données présentes dans Agrosyst, culture '%d', des unités hétérogènes ont été remontées pour le produit '%s' et le traitement '%s', '%s' '%s'.",
                                cropId, productId, treatmentId, refPhytoProductUnit, refActaDosageSPC.getDosage_spc_unite());
                result.addWarning(warning);
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(warning);
                }
            }
        }
        return cropGroupProductTraitement;
    }

    protected Map<Integer, Set<Integer>> getGroupCulturesByCrop() {
        List<RefActaGroupeCultures> groupeCultures = refActaGroupeCulturesDao.findAll();
        Map<Integer, Set<Integer>> groupCultureIdCultures = new HashMap<>();
        for (RefActaGroupeCultures groupeCulture : groupeCultures) {
            Integer id_culture = groupeCulture.getId_culture();
            Set<Integer> id_groupe_cultures = groupCultureIdCultures.computeIfAbsent(id_culture, k -> Sets.newHashSet());
            id_groupe_cultures.add(groupeCulture.getId_groupe_culture());
        }
        return groupCultureIdCultures;
    }

    @Override
    public ImportResult importMaterielTracteursCSV(InputStream contentStream) {
        // declare nuiton csv import model
        ImportModel<RefMaterielTraction> csvModel = new RefMaterielTracteurModel();

        return runSimpleImport(contentStream, RefMaterielTraction.class, csvModel, Referentials.GET_MATERIEL_TRACTION_NATURAL_ID);
    }

    @Override
    public ImportResult importMaterielAutomoteursCSV(InputStream contentStream) {
        // declare nuiton csv import model
        ImportModel<RefMaterielAutomoteur> csvModel = new RefMaterielAutomoteurModel();

        return runSimpleImport(contentStream, RefMaterielAutomoteur.class, csvModel, Referentials.GET_MATERIEL_AUTOMOTEUR_NATURAL_ID);
    }

    @Override
    public ImportResult importMaterielOutilsCSV(InputStream contentStream) {
        // declare nuiton csv import model
        ImportModel<RefMaterielOutil> csvModel = new RefMaterielOutilModel();

        return runSimpleImport(contentStream, RefMaterielOutil.class, csvModel, Referentials.GET_MATERIEL_OUTIL_NATURAL_ID);
    }

    @Override
    public ImportResult importMaterielIrrigationCSV(InputStream contentStream) {

        ImportModel<RefMaterielIrrigation> csvModel = new RefMaterielIrrigationModel();

        return runSimpleImport(contentStream, RefMaterielIrrigation.class, csvModel, Referentials.GET_MATERIEL_IRRIGATION_NATURAL_ID);
    }

    @Override
    public ImportResult importCommuneFranceCSV(InputStream communesFrancaisesStream) {

        // déclaration du modèle d'import nuiton-csv
        ImportModel<RefLocationDto> csvModel = new RefLocationCommunesFranceImportModel();

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        RefCountry france = refCountryDao.forTrigramEquals(Locale.FRANCE.getISO3Country().toLowerCase()).findUnique();

        try (InputStreamReader isr = new InputStreamReader(communesFrancaisesStream, getConfig().getFileEncoding());
             Import<RefLocationDto> importer = Import.newImport(csvModel, isr)) {

            // declare dao
            List<RefLocation> all = refLocationDao.findAll();
            Map<String, RefLocation> map = Maps.uniqueIndex(all, Referentials.GET_LOCATION_CODE_INSEE::apply);


            for (RefLocationDto dto : importer) {

                // compute the codeInsee
                String codeInsee = GET_REF_LOCATION_DTO_CODE_INSEE.apply(dto);
                dto.setCodeInsee(codeInsee);

                RefLocation location;
                if (map.containsKey(codeInsee)) {
                    location = map.get(codeInsee);
                } else {
                    location = refLocationDao.newInstance();
                }
                location.setCodeInsee(codeInsee);
                location.setRefCountry(france);

                // geoloc
                location.setLatitude(dto.getLatitude());
                location.setLongitude(dto.getLongitude());

                // commune
                String commune = GET_REF_LOCATION_DTO_PRETTY_COMMUNE.apply(dto);
                location.setCommune(commune);

                // region & departement
                int region = dto.getRegion();
                String departement = dto.getDepartement();
                String codePostal = dto.getCodePostal();
                location.setRegion(region);
                location.setDepartement(departement);
                codePostal = codePostal.split(";")[0];
                location.setCodePostal(Strings.padStart(codePostal, 5, '0'));

                // autres infos
                location.setAltitude_moy(dto.getAltitude_moy());
                location.setAire_attr(dto.getAire_attr());
                location.setArrondissement_code(dto.getArrondissement_code());
                location.setBassin_vie(dto.getBassin_vie());
                location.setIntercommunalite_code(dto.getIntercommunalite_code());
                location.setUnite_urbaine(dto.getUnite_urbaine());
                location.setZone_emploi(dto.getZone_emploi());
                location.setBassin_viticole(dto.getBassin_viticole());
                location.setAncienne_region(dto.getAncienne_region());
                location.setPetiteRegionAgricoleCode(dto.getPetiteRegionAgricoleCode());
                location.setPetiteRegionAgricoleNom(dto.getPetiteRegionAgricoleNom());

                location.setActive(dto.isActive());

                if (location.isPersisted()) {
                    refLocationDao.update(location);
                    result.incUpdated();
                } else {
                    refLocationDao.create(location);
                    result.incCreated();
                }
            }

            getTransaction().commit();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e);
            }
            result.addError(e.getMessage());
            getTransaction().rollback();
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Import communes françaises terminé: " + result);
        }
        return result;
    }

    protected ImmutableMap<String, RefLocationDto> importCommunePostCodesCSV(InputStream postCodesStream) {
        // déclaration du modèle d'import nuiton-csv
        CommunePostCodeModel csvModel = new CommunePostCodeModel();

        ImmutableMap<String, RefLocationDto> result = null;

        try (InputStreamReader isr = new InputStreamReader(postCodesStream, getConfig().getFileEncoding());
             Import<RefLocationDto> importer = Import.newImport(csvModel, isr)) {
            // lecture du CSV et création de l'index
            result = Maps.uniqueIndex(importer, GET_REF_LOCATION_DTO_CODE_INSEE::apply);
        } catch (IOException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e);
            }
        }
        return result;
    }

    protected ImmutableMultimap<String, RefLocationDto> importCommuneRegionAgricoleCSV(InputStream raStream) {
        // déclaration du modèle d'import nuiton-csv
        CommuneRegionAgricoleModel csvModel = new CommuneRegionAgricoleModel();


        ImmutableMultimap<String, RefLocationDto> result = null;
        try (InputStreamReader isr = new InputStreamReader(raStream, getConfig().getFileEncoding());
             Import<RefLocationDto> importer = Import.newImport(csvModel, isr)
        ) {
            // lecture du CSV et création de l'index
            result = Multimaps.index(importer, GET_REF_LOCATION_DTO_CODE_INSEE::apply);

        } catch (IOException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e);
            }
        }
        return result;
    }

    @Override
    public ImportResult importCommuneEuropeCSV(FileInputStream stream) {
        ImportModel<RefLocationDto> csvModel = new CommuneEuropeModel();

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        try (InputStreamReader isr = new InputStreamReader(stream, getConfig().getFileEncoding());
             Import<RefLocationDto> importer = Import.newImport(csvModel, isr)) {

            List<RefLocation> allLocations = refLocationDao.findAll();
            Map<String, RefLocation> locationsByCodeInsee = Maps.uniqueIndex(allLocations, Referentials.GET_LOCATION_CODE_INSEE::apply);
            List<RefCountry> allCountries = refCountryDao.findAll();
            ImmutableMap<String, RefCountry> countriesByTrigram = Maps.uniqueIndex(allCountries, RefCountry::getTrigram);

            String franceTrigram = Locale.FRANCE.getISO3Country().toLowerCase();

            for (RefLocationDto dto : importer) {
                // l'import des communes françaises est fait par fr.inra.agrosyst.services.referential.ImportServiceImpl.importCommuneOsmCSV
                if (franceTrigram.equals(dto.getPays())) {
                    continue;
                }

                RefLocation location;
                if (locationsByCodeInsee.containsKey(dto.getCodeInsee())) {
                    location = locationsByCodeInsee.get(dto.getCodeInsee());
                } else {
                    location = refLocationDao.newInstance();
                }

                RefCountry refCountry = countriesByTrigram.get(dto.getPays());
                location.setRefCountry(refCountry);
                location.setCodeInsee(dto.getCodeInsee());
                location.setCommune(dto.getCommune());
                location.setDepartement("");

                if (location.isPersisted()) {
                    refLocationDao.update(location);
                    result.incUpdated();
                } else {
                    location.setActive(true);
                    refLocationDao.create(location);
                    result.incCreated();
                }
            }

            getTransaction().commit();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e);
            }
            result.addError(e.getMessage());
            getTransaction().rollback();
        }

        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Import communes européennes terminé: " + result);
        }
        return result;
    }

    @Override
    public ImportResult importSolArvalisCSV(InputStream solsStream, InputStream regionsStreamRaw) {

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        // import data
        RefSolArvalisModel csvModelSol = new RefSolArvalisModel();

        // first import all region into memory
        SolsArvalisRegionsModel csvModelRegion = new SolsArvalisRegionsModel();

        try (InputStreamReader isr = new InputStreamReader(solsStream, getConfig().getFileEncoding());
             InputStream regionsStream = regionsStreamRaw != null ? regionsStreamRaw : ImportServiceImpl.class.getResourceAsStream("/referentiels/sols_regions.csv");
             Import<RefSolArvalis> importerSol = Import.newImport(csvModelSol, isr);
             Import<RegionDto> importRegion = Import.newImport(csvModelRegion, regionsStream)
        ) {

            Map<String, RegionDto> regionMaps = Maps.uniqueIndex(importRegion, RegionDto::getName);

            List<RefSolArvalis> all = refSolArvalisDao.findAll();
            Map<String, RefSolArvalis> map = Maps.uniqueIndex(all, Referentials.GET_SOL_ARVALIS_NATURAL_ID::apply);
            List<String> addedSolArvalisIds = new ArrayList<>();

            Binder<RefSolArvalis, RefSolArvalis> binder = BinderFactory.newBinder(RefSolArvalis.class);
            for (RefSolArvalis refSolsArvalis : importerSol) {
                RegionDto region = regionMaps.get(refSolsArvalis.getSol_region());
                if (region == null) {
                    throw new AgrosystTechnicalException("No such region " + refSolsArvalis.getSol_region() + " in sols region file");
                }
                refSolsArvalis.setSol_region_code(region.getCode());
                String id = Referentials.GET_SOL_ARVALIS_NATURAL_ID.apply(refSolsArvalis);
                if (map.containsKey(id)) {
                    RefSolArvalis existingSol = map.get(id);
                    binder.copyExcluding(refSolsArvalis, existingSol,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                            RefSolArvalis.PROPERTY_ACTIVE);
                    refSolArvalisDao.update(existingSol);
                    result.incUpdated();
                    addedSolArvalisIds.add(id);
                } else if (addedSolArvalisIds.contains(id)) {
                    result.incIgnored();
                    if (LOGGER.isInfoEnabled()) {
                        LOGGER.info(String.format("Clef dupliquée trouvée '%s' pour l'import des RefSolArvalis", id));
                    }
                } else {
                    refSolArvalisDao.create(refSolsArvalis);
                    result.incCreated();
                    addedSolArvalisIds.add(id);
                }
            }

            getTransaction().commit();
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: " + eee.getMessage(), eee);
            }
            result.addError(eee.getMessage());
            getTransaction().rollback();
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Import Sols Arvalis: " + result);
        }
        return result;
    }

    @Override
    public ImportResult importLegalStatusCSV(InputStream statusStream) {
        return runSimpleImport(
                statusStream,
                RefLegalStatus.class,
                new RefLegalStatusModel(),
                Referentials.GET_LEGAL_STATUS_NATURAL_ID);
    }

    @Override
    public ImportResult importEspeces(InputStream stream) {
        return runImportRefEspece(stream);
    }

    @Override
    public ImportResult importVarietesGeves(InputStream stream) {

        return runSimpleImport(
                stream,
                RefVarieteGeves.class,
                new RefVarieteGevesModel(),
                Referentials.GET_VARIETE_GEVES_NATURAL_ID);
    }

    @Override
    public ImportResult importVarietesPlantGrape(InputStream stream) {
        return runSimpleImport(
                stream,
                RefVarietePlantGrape.class,
                new RefVarietePlantGrapeModel(),
                Referentials.GET_VARIETE_PLANT_GRAPE_NATURAL_ID);
    }

    @Override
    public ImportResult importClonesPlantGrape(InputStream stream) {
        return runSimpleImport(
                stream,
                RefClonePlantGrape.class,
                new RefClonePlantGrapeModel(),
                Referentials.GET_CLONE_PLANT_GRAPE_NATURAL_ID);
    }

    @Override
    public ImportResult importEspecesToVarietes(InputStream stream) {
        return runSimpleImport(
                stream,
                RefEspeceToVariete.class,
                new RefEspeceToVarieteModel(),
                Referentials.GET_ESPECE_TO_VARIETE_NATURAL_ID);
    }

    @Override
    public ImportResult importOtexCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefOTEX.class,
                new RefOTEXModel(),
                Referentials.GET_OTEX_NATURAL_ID);
    }

    @Override
    public ImportResult importOrientationEdiCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefOrientationEDI.class,
                new RefOrientationEdiModel(),
                Referentials.GET_ORIENTATION_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importInterventionAgrosystTravailEdiCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefInterventionAgrosystTravailEDI.class,
                new RefInterventionAgrosystTravailEDIModel(),
                Referentials.GET_INTERVENTION_AGROSYST_TRAVAIL_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importStadesEdiCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefStadeEDI.class,
                new RefStadeEDIModel(),
                Referentials.GET_STADE_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importSolTextureGeppa(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefSolTextureGeppa.class,
                new RefSolTextureGeppaModel(),
                Referentials.GET_SOL_TEXTURE_GEPPA_NATURAL_ID);
    }

    @Override
    public ImportResult importZonageParcelleEdi(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefParcelleZonageEDI.class,
                new RefParcelleZonageEdiModel(),
                Referentials.GET_PARCELLE_ZONAGE_NATURAL_ID);
    }

    @Override
    public ImportResult importSolProfondeurIndigo(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefSolProfondeurIndigo.class,
                new RefSolProfondeurIndigoModel(),
                Referentials.GET_SOL_PROFONDEUR_INDIGO_NATURAL_ID);
    }

    @Override
    public ImportResult importSolCarateristiquesIndigo(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefSolCaracteristiqueIndigo.class,
                new RefSolCaracteristiqueIndigoModel(),
                Referentials.GET_SOL_CARACTERISTIQUES_INDIGO_NATURAL_ID);
    }

    @Override
    public ImportResult importUniteEDI(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefUniteEDI.class,
                new RefUniteEDIModel(),
                Referentials.GET_UNITES_EDI_NATURAL_ID);
    }

    protected Map<String, List<InputPrice>> getRefFertiMinIndexedPrices() {
        Map<String, List<InputPrice>> result = new HashMap<>();
        List<InputPrice> prices = inputPriceTopiaDao.forCategoryEquals(InputPriceCategory.MINERAL_INPUT).findAll();
        for (InputPrice price : prices) {
            List<InputPrice> objectPrices = result.computeIfAbsent(price.getObjectId(), k -> Lists.newArrayList());
            objectPrices.add(price);
        }
        return result;
    }

    @Override
    public ImportResult importFertiMinUNIFA(InputStream contentStream) {

        Import<RefFertiMinUNIFA> importerRefFertiMinUNIFA = null;
        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        // import data
        try (InputStreamReader isr = new InputStreamReader(contentStream, getConfig().getFileEncoding())) {
            RefFertiMinUNIFAModel csvModelRefFertiMinUNIFAModel = new RefFertiMinUNIFAModel();
            importerRefFertiMinUNIFA = Import.newImport(csvModelRefFertiMinUNIFAModel, isr);

            List<RefFertiMinUNIFA> persistedFertiMinUnifas = refFertiMinUNIFADao.findAll();
            Map<String, RefFertiMinUNIFA> fertiMinUNIFAMapWithNull = Maps.newHashMap(Maps.uniqueIndex(persistedFertiMinUnifas, Referentials.GET_FERTI_MIN_UNIFA_NATURAL_ID_WITH_NULL::apply));
            Binder<RefFertiMinUNIFA, RefFertiMinUNIFA> binder = BinderFactory.newBinder(RefFertiMinUNIFA.class);

            List<RefFertiMinUNIFA> productToCreates = new ArrayList<>();
            List<RefFertiMinUNIFA> productToUpdates = new ArrayList<>();
            List<InputPrice> pricesToUpdates = new ArrayList<>();

            Map<String, List<InputPrice>> objectPrices = getRefFertiMinIndexedPrices();

            for (RefFertiMinUNIFA importedData : importerRefFertiMinUNIFA) {
                RefFertiMinUNIFA validProduct = fertiMinUNIFAMapWithNull.get(Referentials.GET_FERTI_MIN_UNIFA_NATURAL_ID_WITH_NULL.apply(importedData));

                if (validProduct != null) {
                    binder.copyExcluding(importedData, validProduct,
                            TopiaEntity.PROPERTY_TOPIA_ID,
                            TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                            TopiaEntity.PROPERTY_TOPIA_VERSION,
                            RefFertiMinUNIFA.PROPERTY_ACTIVE);
                    productToUpdates.add(validProduct);
                    result.incUpdated();

                    List<InputPrice> prices = objectPrices.get(validProduct.getTopiaId());
                    if (prices != null) {
                        for (InputPrice price : prices) {
                            price.setObjectId(InputPriceService.GET_REF_FERTI_MIN_UNIFA_OBJECT_ID.apply(importedData));
                            pricesToUpdates.add(price);
                        }
                    }

                } else {
                    productToCreates.add(importedData);
                    result.incCreated();
                    fertiMinUNIFAMapWithNull.put(Referentials.GET_FERTI_MIN_UNIFA_NATURAL_ID_WITH_NULL.apply(importedData), importedData);
                }
            }

            refFertiMinUNIFADao.updateAll(productToUpdates);
            refFertiMinUNIFADao.createAll(productToCreates);
            inputPriceTopiaDao.updateAll(pricesToUpdates);

            getTransaction().commit();
        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: " + e.getMessage(), e);
            }
            result.addError(e.getMessage());
            getTransaction().rollback();
        } finally {
            if (importerRefFertiMinUNIFA != null) {
                importerRefFertiMinUNIFA.close();
            }
        }
        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Import FertiMinUnifa: " + result);
        }

        return result;
    }

    @Override
    public ImportResult importAdventices(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefAdventice.class,
                new RefAdventiceModel(),
                Referentials.GET_ADVENTICES_NATURAL_ID);
    }

    @Override
    public ImportResult importAgsAmortissement(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefAgsAmortissement.class,
                new RefAgsAmortissementModel(),
                Referentials.GET_AGS_AMORTISSEMENT_NATURAL_ID);
    }

    @Override
    public ImportResult importNuisiblesEDI(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefNuisibleEDI.class,
                new RefNuisibleEDIModel(),
                Referentials.GET_NUISIBLES_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importFertiOrga(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefFertiOrga.class,
                new RefFertiOrgaModel(),
                Referentials.GET_FERTI_ORGA_NATURAL_ID);
    }

    @Override
    public ImportResult importStationMeteo(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefStationMeteo.class,
                new RefStationMeteoModel(),
                Referentials.GET_STATION_METEO_NATURAL_ID);
    }

    @Override
    public ImportResult importTypeAgriculture(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefTypeAgriculture.class,
                new RefTypeAgricultureModel(),
                Referentials.GET_TYPE_AGRICULTURE_NATURAL_ID);
    }

    @Override
    public ImportResult importGesCarburants(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefGesCarburant.class,
                new RefGesCarburantModel(),
                Referentials.GET_GES_CARBURANTS_NATURAL_ID);
    }

    @Override
    public ImportResult importGesEngrais(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefGesEngrais.class,
                new RefGesEngraisModel(),
                Referentials.GET_GES_ENGRAIS_NATURAL_ID);
    }

    @Override
    public ImportResult importGesPhyto(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefGesPhyto.class,
                new RefGesPhytoModel(),
                Referentials.GET_GES_PHYTO_NATURAL_ID);
    }

    @Override
    public ImportResult importGesSemences(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefGesSemence.class,
                new RefGesSemenceModel(),
                Referentials.GET_GES_SEMENCES_NATURAL_ID);
    }

    @Override
    public ImportResult importNrjCarburants(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefNrjCarburant.class,
                new RefNrjCarburantModel(),
                Referentials.GET_NRJ_CARBURANTS_NATURAL_ID);
    }

    @Override
    public ImportResult importNrjEngrais(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefNrjEngrais.class,
                new RefNrjEngraisModel(),
                Referentials.GET_NRJ_ENGRAIS_NATURAL_ID);
    }

    @Override
    public ImportResult importNrjPhyto(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefNrjPhyto.class,
                new RefNrjPhytoModel(),
                Referentials.GET_NRJ_PHYTO_NATURAL_ID);
    }

    @Override
    public ImportResult importNrjSemences(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefNrjSemence.class,
                new RefNrjSemenceModel(),
                Referentials.GET_NRJ_SEMENCES_NATURAL_ID);
    }

    @Override
    public ImportResult importNrjGesOutils(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefNrjGesOutil.class,
                new RefNrjGesOutilModel(),
                Referentials.GET_NRJ_GES_OUTILS_NATURAL_ID);
    }

    @Override
    public ImportResult importMesure(InputStream stream) {
        return runSimpleImport(
                stream,
                RefMesure.class,
                new RefMesureModel(),
                Referentials.GET_MESURE_NATURAL_ID);
    }

    @Override
    public ImportResult importSupportOrganeEDI(InputStream stream) {
        return runSimpleImport(
                stream,
                RefSupportOrganeEDI.class,
                new RefSupportOrganeEDIModel(),
                Referentials.GET_SUPPORT_ORGANE_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importStadeNuisibleEDI(InputStream stream) {
        return runSimpleImport(
                stream,
                RefStadeNuisibleEDI.class,
                new RefStadeNuisibleEDIModel(),
                Referentials.GET_STADE_NUISIBLE_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importTypeNotationEDI(InputStream stream) {
        return runSimpleImport(
                stream,
                RefTypeNotationEDI.class,
                new RefTypeNotationEDIModel(),
                Referentials.GET_TYPE_NOTATION_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importValeurQualitativeEDI(InputStream stream) {
        return runSimpleImport(
                stream,
                RefValeurQualitativeEDI.class,
                new RefValeurQualitativeEDIModel(),
                Referentials.GET_VALEUR_QUALITATIVE_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importUnitesQualifiantEDI(InputStream stream) {
        return runSimpleImport(
                stream,
                RefUnitesQualifiantEDI.class,
                new RefUnitesQualifiantEDIModel(),
                Referentials.GET_UNITES_QUALIFIANT_EDI_NATURAL_ID);
    }

    @Override
    public ImportResult importActaTraitementsProduits(InputStream stream) {
        return runSimpleImport(
                stream,
                RefActaTraitementsProduit.class,
                new RefActaTraitementsProduitModel(refCountryDao),
                Referentials.GET_ACTA_TRAITEMENTS_PRODUITS_NATURAL_ID
        );
    }

    @Override
    public ImportResult importActaTraitementsProduitsCateg(InputStream stream) {
        return runSimpleImport(
                stream,
                RefActaTraitementsProduitsCateg.class,
                new RefActaTraitementsProduitsCategModel(),
                Referentials.GET_ACTA_TRAITEMENTS_PRODUITS_CATEG_NATURAL_ID
        );
    }

    @Override
    public ImportResult importActaSubstanceActive(InputStream stream) {
        return runSimpleImport(
                stream,
                RefActaSubstanceActive.class,
                new RefActaSubstanceActiveModel(),
                Referentials.GET_ACTA_SUBSTANCE_ACTIVE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importProtocoleVgObs(InputStream stream) {
        return runSimpleImport(
                stream,
                RefProtocoleVgObs.class,
                new RefProtocoleVgObsModel(),
                Referentials.GET_PROTOCOLE_VG_OBS_NATURAL_ID
        );
    }

    @Override
    public ImportResult importElementVoisinage(InputStream stream) {
        return runSimpleImport(
                stream,
                RefElementVoisinage.class,
                new RefElementVoisinageModel(),
                Referentials.GET_ELEMENT_VOISINAGE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRcesoRulesGroundWater(InputStream stream) {
        return runSimpleImport(
                stream,
                RefRcesoRulesGroundWater.class,
                new RefRcesoRulesGroundWaterModel(),
                Referentials.GET_RCESO_RULES_GROUND_WATER_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRcesoFuzzySetGroundWater(InputStream stream) {
        return runSimpleImport(
                stream,
                RefRcesoFuzzySetGroundWater.class,
                new RefRcesoFuzzySetGroundWaterModel(),
                Referentials.GET_RCESO_FUZZYSET_GROUND_WATER_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRcesoCaseGroundWater(InputStream stream) {
        return runSimpleImport(
                stream,
                RefRcesoCaseGroundWater.class,
                new RefRcesoCaseGroundWaterModel(),
                Referentials.GET_RCESO_CASE_GROUND_WATER_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRcesuRunoffPotRulesParc(InputStream stream) {
        return runSimpleImport(
                stream,
                RefRcesuRunoffPotRulesParc.class,
                new RefRcesuRunoffPotRulesParcModel(),
                Referentials.GET_RCESU_RUNOFF_POT_RULES_PARC_NATURAL_ID
        );
    }

    @Override
    public ImportResult importPhytoSubstanceActiveIphy(InputStream stream) {
        return runSimpleImport(
                stream,
                RefPhytoSubstanceActiveIphy.class,
                new RefPhytoSubstanceActiveIphyModel(),
                Referentials.GET_PHYTO_SUBSTANCE_ACTIVE_IPHY_NATURAL_ID
        );
    }

    @Override
    public ImportResult importActaDosageSpc(InputStream stream) {
        return runActaDocageSpcImport(
                stream,
                new RefActaDosageSPCModel()
        );
    }

    @Override
    public ImportResult importActaGroupeCultures(InputStream stream) {
        return runSimpleImport(
                stream,
                RefActaGroupeCultures.class,
                new RefActaGroupeCulturesModel(),
                Referentials.GET_ACTA_GROUPE_CULTURES_NATURAL_ID
        );
    }

    @Override
    public ImportResult importSaActaIphy(InputStream stream) {
        return runSimpleImport(
                stream,
                RefSaActaIphy.class,
                new RefSaActaIphyModel(),
                Referentials.GET_SA_ACTA_IPHY_NATURAL_ID
        );
    }

    @Override
    public ImportResult importTraitSdC(InputStream stream) {
        return runSimpleImport(
                stream,
                RefTraitSdC.class,
                new RefTraitSdCModel(),
                Referentials.GET_TRAIT_SDC_NATURAL_ID
        );
    }

    @Override
    public ImportResult importCouvSolAnnuelle(InputStream stream) {
        return runSimpleImport(
                stream,
                RefCouvSolAnnuelle.class,
                new RefCouvSolAnnuelleModel(),
                Referentials.GET_COUV_SOL_ANNUELLE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importCultureEdiGroupeCouvSol(InputStream stream) {
        return runSimpleImport(
                stream,
                RefCultureEdiGroupeCouvSol.class,
                new RefCultureEdiGroupeCouvSolModel(),
                Referentials.GET_CULTURE_EDI_GROUP_COUV_SOL_NATURAL_ID
        );
    }

    @Override
    public ImportResult importCouvSolPerenne(InputStream stream) {
        return runSimpleImport(
                stream,
                RefCouvSolPerenne.class,
                new RefCouvSolPerenneModel(),
                Referentials.GET_COUV_SOL_PERENNE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importDepartmentShapes(InputStream stream) {
        return runSimpleImport(
                stream,
                RefDepartmentShape.class,
                new RefDepartmentShapeModel(),
                Referentials.GET_DEPARTMENT_SHAPE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importZoneClimatiqueIphy(InputStream stream) {
        return runSimpleImport(
                stream,
                RefZoneClimatiqueIphy.class,
                new RefZoneClimatiqueIphyModel(),
                Referentials.GET_ZONE_CLIMATIQUE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefActaDosageSpcRoot(InputStream stream) {
        return runSimpleImport(
                stream,
                RefActaDosageSpcRoot.class,
                new RefActaDosageSpcRootExportModel(),
                Referentials.GET_REF_ACTA_DOSAGE_SPC_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefActaDosageSaRoot(InputStream stream) {
        return runEntityFromApiSimpleImport(
                stream,
                RefActaDosageSaRoot.class,
                new RefActaDosageSaRootExportModel(),
                Referentials.GET_REF_ACTA_DOSAGE_SA_NATURAL_ID);
    }

    @Override
    public ImportResult importRefActaProduitRoot(InputStream stream) {
        return runEntityFromApiSimpleImport(
                stream,
                RefActaProduitRoot.class,
                new RefActaProduitRootExportModel(refCountryDao),
                Referentials.GET_REF_ACTA_PRODUIT_NATURAL_ID);
    }

    @Override
    public ImportResult importRefEdaplosTraitementProduit(InputStream stream) {
        return runSimpleImport(
                stream,
                RefEdaplosTypeTraitement.class,
                new RefEdaplosTypeTraitemenModel(),
                Referentials.GET_REF_EDAPLOS_TYPE_TRAITEMENT_NATURAL_ID);
    }

    @Override
    public InputStream getImportSolFromApiToCSVResult(String url, String apiKey, Class<?> klass, String apiUser, String apiPwd) throws AgrosystImportException {

        InputStream csvStream;

        CloseableHttpResponse response;
        if (url != null && apiUser != null && apiPwd != null) {

            response = getApiServerResponseForAuthentication(url, apiUser, apiPwd);

        } else if (apiKey != null) {

            String dataSet = null;
            if (RefSolArvalis.class.equals(klass)) {
                dataSet = "base-sol";
            }

            if (dataSet != null) {
                response = loadApiResource(apiKey, klass, null);

            } else {
                throw new AgrosystImportException(String.format("L'import de données depuis l'API pour '%s' n'est pas supporté", klass));
            }
        } else if (url != null) {
            response = loadApiResource(null, klass, url);
        } else {
            throw new AgrosystImportException("L'import de données depuis l'API a échoué, l'url ou la clef d'API doivent être présent");
        }

        csvStream = getCSVImportResults(response, klass);


        return csvStream;
    }

    protected CloseableHttpResponse getApiServerResponseForAuthentication(String url, String apiUser, String apiPwd) {

        String uri = getAuthenticationUri(url);

        CloseableHttpClient httpclient = HttpClientBuilder.create().build();

        HttpHost targetHost = new HttpHost("api-agro.opendatasoft.com", 443, "https");

        CredentialsProvider credProvider = getAPICredentialsProvider(apiUser, apiPwd, targetHost);

        AuthCache authCache = getAPIAuthCache(targetHost);

        HttpClientContext context = getAPIHttpClientContext(credProvider, authCache);

        return getAuthenticationCloseableHttpResponse(uri, httpclient, targetHost, context);
    }

    private CloseableHttpResponse getAuthenticationCloseableHttpResponse(String uri, CloseableHttpClient httpclient, HttpHost targetHost, HttpClientContext context) {
        CloseableHttpResponse response;
        HttpGet httpget = new HttpGet(uri);
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(uri);
            }
            response = httpclient.execute(targetHost, httpget, context);
        } catch (IOException ex) {
            throw new AgrosystImportException("Les données non pas pu être récupérées: " + ex.getMessage());
        }
        return response;
    }

    private HttpClientContext getAPIHttpClientContext(CredentialsProvider credProvider, AuthCache authCache) {
        // Add AuthCache to the execution context
        HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credProvider);
        context.setAuthCache(authCache);
        return context;
    }

    private AuthCache getAPIAuthCache(HttpHost targetHost) {
        //Create AuthCache instance
        AuthCache authCache = new BasicAuthCache();
        // Generate BASIC scheme object and add it to the local auth cache
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(targetHost, basicAuth);
        return authCache;
    }

    protected CredentialsProvider getAPICredentialsProvider(String apiUser, String apiPwd, HttpHost targetHost) {
        CredentialsProvider credProvider = new BasicCredentialsProvider();
        credProvider.setCredentials(
                new AuthScope(targetHost.getHostName(), targetHost.getPort()),
                new UsernamePasswordCredentials(apiUser, apiPwd));
        return credProvider;
    }

    private String getAuthenticationUri(String apiUrl) {
        String[] apiUrlParts = apiUrl.split("api-agro.opendatasoft.com");
        if (apiUrlParts.length != 2) {
            throw new AgrosystImportException("Les données non pas pu être récupérées, URI incorrecte.");
        }
        return apiUrlParts[1];
    }

    protected CloseableHttpResponse loadApiResource(String apiKey, Class<?> klass, String userDefineUrl) {

        URI uri = getUri(apiKey, klass, userDefineUrl);

        URL apiUrl = getUrlFromUri(uri);

        getConnection(apiUrl);

        return getResponse(uri);
    }

    private CloseableHttpResponse getResponse(URI uri) {
        CloseableHttpResponse response;
        try (CloseableHttpClient httpclient = HttpClientBuilder.create().build()) {
            HttpGet httpget = new HttpGet(uri);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(uri.toString());
            }
            response = httpclient.execute(httpget);
        } catch (IOException ex) {
            throw new AgrosystImportException("Les données non pas pu être récupérées: " + ex.getMessage());
        }
        return response;
    }

    protected void getConnection(URL apiUrl) {
        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) apiUrl.openConnection();
            connection.connect();
        } catch (IOException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e.getMessage());
            }
            throw new AgrosystImportException("Les données de l'url '" + apiUrl + "' non pas pu être récupérées: La connexion a échouée, " + e.getMessage());
        }
    }

    protected URL getUrlFromUri(URI uri) {
        URL apiUrl;
        try {
            apiUrl = uri.toURL();
        } catch (MalformedURLException e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e.getMessage());
            }
            throw new AgrosystImportException("Les données non pas pu être récupérées: URL malformée, " + e.getMessage());
        }
        return apiUrl;
    }

    protected URI getUri(String apiKey, Class<?> klass, String userDefineUrl) {
        URI uri = null;

        if (apiKey != null) {

            String dataSet = getDataSet(klass);

            try {
                uri = new URIBuilder()
                        .setScheme("https")
                        .setHost("api-agro.opendatasoft.com")
                        .setPath("/api/records/1.0/download")
                        .addParameter("dataset", dataSet)
                        .addParameter("format", "json")
                        .addParameter("rows", "-1")
                        .addParameter("apikey", apiKey)
                        .build();
            } catch (URISyntaxException e) {
                throw new AgrosystImportException("Les données non pas pu être récupérées, URI incorrecte.", e);
            }
        } else if (userDefineUrl != null) {
            uri = getUri(userDefineUrl);
        }

        return uri;
    }

    protected URI getUri(String userDefineUrl) {
        try {
            return new URIBuilder(userDefineUrl).build();
        } catch (URISyntaxException e) {
            throw new AgrosystImportException(String.format("Les données non pas pu être récupérées, URL incorrecte: '%s' .", userDefineUrl), e);
        }
    }

    protected String getDataSet(Class<?> klass) {
        String dataSet = null;
        if (RefSolArvalis.class.equals(klass)) {
            dataSet = "base-sol";
        }

        if (dataSet == null) {
            throw new AgrosystImportException(String.format("L'import de données depuis l'API pour '%s' n'est pas supporté", klass));
        }
        return dataSet;
    }

    protected InputStream getCSVImportResults(CloseableHttpResponse response, Class<?> klass) {
        InputStream csvStream;
        StatusLine statusLine = response.getStatusLine();
        if (statusLine.getStatusCode() == 200) {
            String jsonResult = transformHttpResponseToJson(response);
            csvStream = produceCsvImportResult(jsonResult, klass);
        } else if (statusLine.getStatusCode() == 401) {
            throw new AgrosystImportException("Les données non pas pu être récupérées: Erreur d'authentification");
        } else {
            throw new AgrosystImportException("Les données non pas pu être récupérées: " + statusLine.getReasonPhrase());
        }

        return csvStream;
    }

    protected String transformHttpResponseToJson(CloseableHttpResponse response) {
        String jsonResult;

        HttpEntity entity = response.getEntity();
        try (InputStream stream = entity.getContent();
             InputStreamReader isr = new InputStreamReader(stream, StandardCharsets.UTF_8);
             BufferedReader streamReader = new BufferedReader(isr)) {

            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            jsonResult = responseStrBuilder.toString();

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e.getMessage());
            }
            throw new AgrosystImportException("Les données non pas pu être récupérées: Le mapping Json a échoué, " + e.getMessage(), e);
        }

        return jsonResult;
    }

    protected InputStream getApiAnalyseResult_CSV(Class klass, ExportModel model, List contents) {
        InputStream result = null;
        try {
            File tempFile = File.createTempFile("export-" + klass.getSimpleName() + "-", ".csv");
            tempFile.deleteOnExit();
            Export.exportToFile(model, contents, tempFile, StandardCharsets.UTF_8);
            result = new FileInputStream(tempFile);
            if (LOGGER.isDebugEnabled()) {
                String path = tempFile.getAbsolutePath();
                LOGGER.debug(String.format("File '%s' created", path));
            }
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Exception à l'export CSV: " + eee.getMessage(), eee);
            }
        }
        return result;
    }

    protected Dosages mapDosageResult(String jsonResult) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonResult, Dosages.class);
    }

    @Override
    public ImportResult importRefDestination(InputStream stream) {
        return runSimpleImport(
                stream,
                RefDestination.class,
                new RefDestinationModel(refEspeceDao),
                Referentials.GET_DESTINATION_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefQualityCriteria(InputStream stream) {
        return runSimpleImport(
                stream,
                RefQualityCriteria.class,
                new RefQualityCriteriaModel(refEspeceDao),
                Referentials.GET_QUALITYCRITERIA_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefHarvestingPrice(InputStream stream) {
        RefHarvestingPriceModel model = new RefHarvestingPriceModel(refDestinationDao, refEspeceDao);

        return runRefHarvestingPriceImport(
                stream,
                model
        );
    }

    @Override
    public ImportResult importRefSpeciesToSector(InputStream stream) {
        return runSimpleImport(
                stream,
                RefSpeciesToSector.class,
                new RefSpeciesToSectorModel(refEspeceDao),
                Referentials.GET_REF_SPECIES_TO_SECTOR_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefHarvestingPriceConverter(InputStream stream) {
        RefHarvestingPriceConverterModel model = new RefHarvestingPriceConverterModel(refDestinationDao, refEspeceDao);

        return runSimpleImport(
                stream,
                RefHarvestingPriceConverter.class,
                model,
                Referentials.GET_REF_HARVESTING_PRICE_CONVERTER_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefStrategyLever(InputStream stream) {
        RefStrategyLeverModel model = new RefStrategyLeverModel();

        return runSimpleImport(
                stream,
                RefStrategyLever.class,
                model,
                Referentials.GET_REF_STRATEGIE_LEVER_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefAnimalType(InputStream stream) {
        RefAnimalTypeModel model = new RefAnimalTypeModel();

        return runSimpleImport(
                stream,
                RefAnimalType.class,
                model,
                Referentials.GET_REF_ANIMAL_TYPE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefMarketingDestination(InputStream stream) {
        RefMarketingDestinationModel model = new RefMarketingDestinationModel();

        return runSimpleImport(
                stream,
                RefMarketingDestination.class,
                model,
                Referentials.GET_REF_MARKETING_DESTINATION_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefInternventionTypeItemInputEdi(InputStream stream) {
        RefInterventionTypeItemInputEdiModel model = new RefInterventionTypeItemInputEdiModel();

        return runSimpleImport(
                stream,
                RefInterventionTypeItemInputEDI.class,
                model,
                Referentials.GET_REF_INTERVENTION_TYPE_ITEM_INPUT_EDI_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefQualityCriteriaClass(InputStream stream) {
        RefQualityCriteriaTopiaDao dao = getPersistenceContext().getRefQualityCriteriaDao();
        RefQualityCriteriaClassModel model = new RefQualityCriteriaClassModel(dao);

        return runSimpleImport(
                stream,
                RefQualityCriteriaClass.class,
                model,
                Referentials.GET_REF_QUALITY_CRITERIA_CLASS_NATURAL_ID
        );

    }

    @Override
    public ImportResult importRefPrixCarbuCSV(InputStream stream) {
        RefPrixCarbuModel model = new RefPrixCarbuModel();

        // Fonction required by importRefPrixCSV but not used for this import so the returned result is always true as valid
        java.util.function.Function<RefPrixCarbu, Boolean> ALLAYS_TRUE_FCT = input -> Boolean.TRUE;

        return importRefPrixCSV(
                stream, RefPrixCarbu.class, model, Referentials.GET_REF_PRIX_CARBU_CAMPAIGN_NATURAL_ID, Referentials.GET_REF_PRIX_CARBU_SCENARIO_NATURAL_ID, ALLAYS_TRUE_FCT, "t");

    }

    @Override
    public ImportResult importRefPrixEspeceCSV(InputStream input) {

        long start = System.currentTimeMillis();

        RefEspeceTopiaDao refEspeceTopiaDao = getPersistenceContext().getRefEspeceDao();
        List<String> allRefEspecekeys = refEspeceTopiaDao.findAllKeys();

        RefPrixEspeceModel model = new RefPrixEspeceModel();

        java.util.function.Function<RefPrixEspece, Boolean> VALIDATE_REF_ESPECE_ID = refPrixEspece -> allRefEspecekeys.contains(
                refPrixEspece.getCode_espece_botanique() + "_" +
                        refPrixEspece.getCode_qualifiant_AEE()
        );

        ImportResult result = importRefPrixCSV(
                input,
                RefPrixEspece.class,
                model,
                Referentials.GET_REF_PRIX_ESPECE_CAMPAIGN_NATURAL_ID,
                Referentials.GET_REF_PRIX_ESPECE_SCENARIO_NATURAL_ID,
                VALIDATE_REF_ESPECE_ID,
                "l'espèce n'est pas présente dans le référentiel RefEspece");

        long end = System.currentTimeMillis();
        result.setDuration(end - start);

        return result;
    }

    @Override
    public ImportResult importRefPrixFertiMinCSV(InputStream stream) {
        RefPrixFertiMinModel model = new RefPrixFertiMinModel();

        // Fonction required by importRefPrixCSV but not used for this import so the returned result is always true as valid
        java.util.function.Function<RefPrixFertiMin, Boolean> ALLAYS_TRUE_FCT = input -> Boolean.TRUE;

        return importRefPrixCSV(
                stream, RefPrixFertiMin.class, model, Referentials.GET_REF_PRIX_FERTI_MIN_CAMPAIGN_NATURAL_ID, Referentials.GET_REF_PRIX_FERTI_MIN_SCENARIO_NATURAL_ID, ALLAYS_TRUE_FCT, "");
    }

    @Override
    public ImportResult importRefPrixFertiOrgaCSV(InputStream input) {

        long start = System.currentTimeMillis();

        RefFertiOrgaTopiaDao refFertiOrgaDao = getPersistenceContext().getRefFertiOrgaDao();

        List<String> allRefFertiOrgaKeysForActives = refFertiOrgaDao.finfAllKeysForActive();
        final java.util.function.Function<RefPrixFertiOrga, Boolean> VALIDATE_REF_FERFI_ORGA_ID = input_ -> allRefFertiOrgaKeysForActives.contains(input_.getIdTypeEffluent());

        RefPrixFertiOrgaModel model = new RefPrixFertiOrgaModel();

        ImportResult result = importRefPrixCSV(
                input,
                RefPrixFertiOrga.class,
                model,
                Referentials.GET_REF_PRIX_FERTI_ORGA_CAMPAIGN_NATURAL_ID,
                Referentials.GET_REF_PRIX_FERTI_ORGA_SCENARIO_NATURAL_ID,
                VALIDATE_REF_FERFI_ORGA_ID,
                "le fertilisant organique n'a peu être retrouvé dans le référentiel RefFertiOrga"
        );

        long end = System.currentTimeMillis();
        result.setDuration(end - start);

        return result;
    }


    @Override
    public ImportResult importRefPrixPhytoCSV(InputStream input) {

        long start = System.currentTimeMillis();

        RefActaTraitementsProduitTopiaDao refATPdao = getPersistenceContext().getRefActaTraitementsProduitDao();

        List<String> allRefActaTraitementProduitIds = refATPdao.finfAllKeysForActive();
        java.util.function.Function<RefPrixPhyto, Boolean> VALIDATE_REF_ACTA_TRAITEMENT_PRODUIT_ID = input_ -> allRefActaTraitementProduitIds.contains(input_.getId_produit() + "_" + input_.getId_traitement());

        RefPrixPhytoModel model = new RefPrixPhytoModel();

        ImportResult result = importRefPrixCSV(
                input, RefPrixPhyto.class, model, Referentials.GET_REF_PRIX_PHYTO_CAMPAIGN_NATURAL_ID, Referentials.GET_REF_PRIX_PHYTO_SCENARIO_NATURAL_ID, VALIDATE_REF_ACTA_TRAITEMENT_PRODUIT_ID, "l'intrant phyto sanitaire n'a peu être retrouvé dans le référentiel RefActaTraitementsProduit");

        long end = System.currentTimeMillis();
        result.setDuration(end - start);

        return result;
    }

    @Override
    public ImportResult importRefPrixIrrigCSV(InputStream stream) {
        RefPrixIrrigModel model = new RefPrixIrrigModel();

        // Fonction required by importRefPrixCSV but not used for this import so the returned result is always true as valid
        java.util.function.Function<RefPrixIrrig, Boolean> ALLAYS_TRUE_FCT = input -> Boolean.TRUE;

        return importRefPrixCSV(
                stream, RefPrixIrrig.class, model, Referentials.GET_REF_PRIX_IRRIG_CAMPAIGN_NATURAL_ID, Referentials.GET_REF_PRIX_IRRIG_SCENARIO_NATURAL_ID, ALLAYS_TRUE_FCT, "t");

    }

    @Override
    public ImportResult importRefPrixAutreCSV(InputStream stream) {
        RefPrixAutreModel model = new RefPrixAutreModel();

        // Fonction required by importRefPrixCSV but not used for this import so the returned result is always true as valid
        java.util.function.Function<RefPrixAutre, Boolean> ALLAYS_TRUE_FCT = input -> Boolean.TRUE;

        return importRefPrixCSV(
                stream, RefPrixAutre.class, model, Referentials.GET_REF_PRIX_AUTRE_CAMPAIGN_NATURAL_ID, Referentials.GET_REF_PRIX_AUTRE_SCENARIO_NATURAL_ID, ALLAYS_TRUE_FCT, "t");
    }

    @Override
    public ImportResult importRefInputUnitPriceUnitConverterCSV(InputStream stream) {
        RefInputUnitPriceUnitConverterModel model = new RefInputUnitPriceUnitConverterModel();

        return runRefInputUnitPriceUnitConverterImport(
                stream,
                model
        );
    }

    private <T extends RefInputPrice> ImportResult importRefPrixCSV(
            InputStream input,
            Class<T> entityClass,
            ImportModel<T> model,
            java.util.function.Function<T, String> campaignNaturalIdFunction,
            java.util.function.Function<T, String> scenarioNaturalIdFonction,
            java.util.function.Function<T, Boolean> validateFunction,
            String validateErrorMessage) {

        ImportResult result = new ImportResult();

        try (Import<T> importer = Import.newImport(model, input)) {
            // declare dao
            TopiaDao<T> dao = context.getDaoSupplier().getDao(entityClass);
            List<T> entities = dao.findAll();


            Set<T> refPricesForCampaigns = entities.stream().filter(entity -> entity.getCampaign() != null).collect(Collectors.toSet());
            Map<String, T> entitiesByCampaignNaturalIds = Maps.uniqueIndex(refPricesForCampaigns, campaignNaturalIdFunction::apply);

            Set<T> refPricesForScenarios = entities.stream().filter(entity -> StringUtils.isNotBlank(entity.getCode_scenario())).collect(Collectors.toSet());
            Map<String, T> entitiesByScenarioNaturalIds = Maps.uniqueIndex(refPricesForScenarios, scenarioNaturalIdFonction::apply);

            Set<String> campaignAlreadyDone = Sets.newHashSet();
            Set<String> scenarioAlreadyDone = Sets.newHashSet();

            Binder<T, T> binder = BinderFactory.newBinder(entityClass);

            int line = 2;
            for (T entity : importer) {

                if (!validateFunction.apply(entity)) {
                    String message = String.format("Ligne %d, '%s', %s", line, entityClass.getSimpleName(), validateErrorMessage);
                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(message);
                    }
                    result.addError(message);
                    line++;
                    continue;
                }

                String campaignId = entity.getCampaign() != null ? campaignNaturalIdFunction.apply(entity) : null;

                String scenarioId = StringUtils.isNotBlank(entity.getCode_scenario()) ? scenarioNaturalIdFonction.apply(entity) : null;

                boolean duplicatedValue = campaignId != null ? campaignAlreadyDone.contains(campaignId) : scenarioAlreadyDone.contains(scenarioId);

                if (duplicatedValue) {
                    String message = String.format("Valeur en double trouvée (type=%s, L%d): %s", entityClass.getSimpleName(), line, campaignAlreadyDone.contains(campaignId) ? campaignId : scenarioId);

                    if (LOGGER.isErrorEnabled()) {
                        LOGGER.error(message);
                    }
                    result.addError(message);

                } else if (!result.hasErrors()) {

                    if (campaignId != null) campaignAlreadyDone.add(campaignId);
                    if (scenarioId != null) scenarioAlreadyDone.add(scenarioId);

                    T existing = entitiesByCampaignNaturalIds.get(campaignId) != null ? entitiesByCampaignNaturalIds.get(campaignId) : entitiesByScenarioNaturalIds.get(scenarioId);
                    if (existing != null) {
                        binder.copyExcluding(entity, existing,
                                TopiaEntity.PROPERTY_TOPIA_ID,
                                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                                TopiaEntity.PROPERTY_TOPIA_VERSION,
                                RefInputPrice.PROPERTY_CAMPAIGN,
                                RefInputPrice.PROPERTY_CODE_SCENARIO);

                        if (entity.getCampaign() != null && existing.getCampaign() == null) {
                            existing.setCampaign(entity.getCampaign());
                        }

                        if (StringUtils.isNotBlank(entity.getCode_scenario()) && StringUtils.isBlank(existing.getCode_scenario())) {
                            existing.setCode_scenario(entity.getCode_scenario());
                        }

                        addRefPrixPhytoSpecificField(existing);
                        dao.update(existing);
                        result.incUpdated();
                    } else {
                        addRefPrixPhytoSpecificField(entity);
                        dao.create(entity);
                        result.incCreated();
                    }
                }
                line++;
            }

            if (!result.hasErrors()) {
                getTransaction().commit();
            }
        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: " + eee.getMessage(), eee);
            }
            result.addError(eee.getMessage());
            getTransaction().rollback();
        }

        // At the end of each import, make sure to invalidate caches
        clearCacheAndSync();

        return result;
    }

    protected <T extends RefInputPrice> void addRefPrixPhytoSpecificField(T entity) {
        if (entity instanceof RefPrixPhyto rpp) {
            String phytoObjectId = rpp.getPhytoObjectId();
            if (StringUtils.isBlank(phytoObjectId)) {
                phytoObjectId = rpp.getId_produit() + '_' + rpp.getId_traitement();
            }
            rpp.setPhytoObjectId(phytoObjectId);
        }
    }

    @Override
    public ImportResult importRefEspeceOtherToolsCSV(InputStream stream) {
        RefEspeceOtherToolsModel model = new RefEspeceOtherToolsModel();

        return runSimpleImport(
                stream,
                RefEspeceOtherTools.class,
                model,
                Referentials.GET_REF_ESPECE_OTHERS_TOOLS_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefCattleAnimalTypesCSV(InputStream contentStream, List<RefAnimalType> animalTypes) {
        return runSimpleImport(
                contentStream,
                RefCattleAnimalType.class,
                new RefCattleAnimalTypeModel(animalTypes),
                Referentials.GET_CATTLE_ANIMAL_TYPE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefCattleRationAlimentsCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefCattleRationAliment.class,
                new RefCattleRationAlimentModel(),
                Referentials.GET_CATTLE_RATION_ALIMENT_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefCiblesAgrosystGroupesCiblesMAA(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefCiblesAgrosystGroupesCiblesMAA.class,
                new RefCiblesAgrosystGroupesCiblesMAAModel(true),
                Referentials.GET_CIBLES_AGROSYST_GROUPES_CIBLE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefMAABiocontrole(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefMAABiocontrole.class,
                new RefMAABiocontroleModel(),
                Referentials.GET_MAA_BIOCONTROLE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefMAADosesRefParGroupeCible(InputStream contentStream) {
        return runRefMAADosesRefParGroupeCibleImport(
                contentStream,
                new RefMAADosesRefParGroupeCibleModel()
        );
    }

    @Override
    public ImportResult importRefGroupeCibleTraitement(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefGroupeCibleTraitement.class,
                new RefGroupeCibleTraitementModel(),
                Referentials.GET_GROUPE_CIBLE_TRAITEMENT_NATURAL_ID
        );
    }

    @Override
    public void importMAAReferentialsFromApiAsync(int campagne, Integer numeroAmmIdMetier) {
        AuthenticatedUser user = getAuthenticatedUser();

        MaaApiImportTask task = new MaaApiImportTask(user.getTopiaId(), user.getEmail(), campagne, numeroAmmIdMetier);
        getBusinessTaskManager().schedule(task);
    }

    @Override
    public Map<Class<? extends ReferentialEntity>, ImportResult> importMAAReferentialsFromApi(int campagne, Integer numeroAmmIdMetier) {
        ImportResult refActaTraitementsProduitImportResult = new ImportResult();
        ImportResult refMAADosesRefParGroupeCibleImportResult = new ImportResult();
        ImportResult refMAADBioControleImportResult = new ImportResult();

        Map<Class<? extends ReferentialEntity>, ImportResult> result = new HashMap<>();
        result.put(RefActaTraitementsProduit.class, refActaTraitementsProduitImportResult);
        result.put(RefMAADosesRefParGroupeCible.class, refMAADosesRefParGroupeCibleImportResult);
        result.put(RefMAABiocontrole.class, refMAADBioControleImportResult);

        String maaReferentialUrl = getConfig().getMAAReferentialUrl();
        int nbElementByPageLoaded = getConfig().getMAARefenrentialNbElementLoaded();
        int nbPageToLoad = getConfig().getMAARefenrentialNbPageToLoad();
        String lastApiCall = refMAADosesRefParGroupeCibles
                .findLastDateCorrectionMaa(campagne)
                .map(d -> d.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                .orElse("");

        Map<String, Integer> historicIdTraitementsByCode = refActaTraitementsProduitsCategDao
                .findAll()
                .stream()
                .collect(Collectors.toMap(RefActaTraitementsProduitsCateg::getCode_traitement,
                        RefActaTraitementsProduitsCateg::getId_traitement));

        Collection<RefActaProduitRoot> refActaProduitRoots = refActaProduitRootDao.findAll();
        RefCountry france = refCountryDao.forTrigramEquals(Locale.FRANCE.getISO3Country().toLowerCase()).findUnique();

        Collection<RefGroupeCibleTraitement> refMAACodeTraitementToHistoricCodeTraitements = refGroupeCibleTraitementDao.findAll()
                .stream()
                .filter(RefGroupeCibleTraitement::isActive)
                .toList();

        int page = 0;
        try {
            boolean remainingDataToFetch = true;
            int remainingRetry = API_IMPORT_TRIES;

            while (remainingDataToFetch) {
                String numAmmIdMetier = numeroAmmIdMetier != null ? "&numeroAmmIdMetier=" + numeroAmmIdMetier : "";
                String url = String.format("%s?campagneIdMetier=%s&page=%s&size=%d%s", maaReferentialUrl, campagne, page, nbElementByPageLoaded, numAmmIdMetier);
                if (StringUtils.isNotBlank(lastApiCall) && StringUtils.isBlank(numAmmIdMetier)) {
                    url += "&dateCorrectionDepuis=" + lastApiCall;
                }

                URI uri = getUri(url);
                try (CloseableHttpClient httpclient = HttpClientBuilder.create().build()) {
                    HttpGet httpget = new HttpGet(uri);
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug(uri.toString());
                    }
                    CloseableHttpResponse response = httpclient.execute(httpget);

                    StatusLine statusLine = response.getStatusLine();
                    int statusCode = statusLine.getStatusCode();

                    if (statusCode == HttpStatus.SC_PARTIAL_CONTENT) {
                        String jsonResult = transformHttpResponseToJson(response);
                        if (StringUtils.isNoneBlank(jsonResult)) {
                            handleMAAResponseForImport(
                                    jsonResult,
                                    refActaTraitementsProduitImportResult,
                                    refMAADosesRefParGroupeCibleImportResult,
                                    refMAADBioControleImportResult,
                                    campagne,
                                    historicIdTraitementsByCode,
                                    refMAACodeTraitementToHistoricCodeTraitements,
                                    refActaProduitRoots,
                                    france);
                        }

                        if (LOGGER.isInfoEnabled()) {
                            LOGGER.info("Import de la page " + page + " réalisé avec succès.");
                        }

                        remainingRetry = API_IMPORT_TRIES;
                        page++;


                    } else if (statusCode == HttpStatus.SC_NOT_FOUND || page == nbPageToLoad) {
                        remainingDataToFetch = false;

                    } else if (remainingRetry > 0) {
                        remainingRetry--;
                        Thread.sleep(API_IMPORT_INTERVAL);

                    } else {
                        throw new AgrosystImportException("[Page " + page + "] Les données n'ont pas pu être récupérées : " + statusLine.getReasonPhrase()
                                + " (reçu : " + statusCode + ", attendu : 206) / url : " + url);
                    }
                } catch (IOException ex) {
                    throw new AgrosystImportException("Les données non pas pu être récupérées: " + ex.getMessage());
                }
            }

            updateActaTraitementProduitsTraitementNoCommit();

            getTransaction().commit();

        } catch (Exception eee) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Error during import: " + eee.getMessage(), eee);
            }
            getTransaction().rollback();
            throw new AgrosystTechnicalException("Error during API MAA import: " + eee.getMessage(), eee);
        }

        return result;
    }

    @Override
    public void updateActaTraitementProduitsTraitement() {
        updateActaTraitementProduitsTraitementNoCommit();
        getTransaction().commit();
    }

    private void updateActaTraitementProduitsTraitementNoCommit() {
        Collection<RefActaTraitementsProduit> refActaTraitementsProduitsToSave = new HashSet<>();
        Collection<RefActaTraitementsProduit> refActaTraitementsProduitsToDelete = new HashSet<>();

        // référentiel destiné au Types produits : Autres / Divers
        // dont les produits Segment 6, vocabulaire Ministère = les types de produits "Divers" historique ACTA / Agrosyst
        // Ce segment est trop générique et afin d'avoir un calcul de l'IFT plus fin le référentiel refGroupeCibleTraitement
        // a été créé pour regrouper des types de produit différents (molluscicide, SDN, Substance de croissance) qui donne lieu à des IFT différents
        List<RefGroupeCibleTraitement> allActiveRefGroupeCibleTraitements = refGroupeCibleTraitementDao
                .forActiveEquals(true)
                .findAll();

        Map<String, String> groupesCiblesMaaByCodeAmmAndCodeTraitementMaa = refMAADosesRefParGroupeCibles
                .findAllActiveGroupesCiblesMaaByCodeAmmAndCodeTraitementMaa();

        List<RefActaTraitementsProduit> allRefActaTraitementsProduits = refActaTraitementsProduitDao.findAll();

        MultiKeyMap<String, Collection<RefActaTraitementsProduit>> productsByMaaCodeTraitementGroupeCible = new MultiKeyMap<>();
        MultiKeyMap<Object, RefActaTraitementsProduit> refActaTraitementsProduitsByIdProduitIdTraitement = new MultiKeyMap<>();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("%d RefActaTraitementsProduit en base", allRefActaTraitementsProduits.size()));
        }

        for (RefActaTraitementsProduit refActaTraitementsProduit : allRefActaTraitementsProduits) {

            String codeTraitement = refActaTraitementsProduit.getCode_traitement();
            String idProduit = refActaTraitementsProduit.getId_produit();
            int idTraitement = refActaTraitementsProduit.getId_traitement();

            // may be null
            String groupeCibleMaa = StringUtils.trimToNull(groupesCiblesMaaByCodeAmmAndCodeTraitementMaa
                    .get(refActaTraitementsProduit.getCode_AMM() + refActaTraitementsProduit.getCode_traitement_maa()));

            if (!productsByMaaCodeTraitementGroupeCible.containsKey(codeTraitement, groupeCibleMaa)) {
                productsByMaaCodeTraitementGroupeCible.put(codeTraitement, groupeCibleMaa, new HashSet<>());
            }

            Collection<RefActaTraitementsProduit> productsForCodeTraitementGroupeCible = productsByMaaCodeTraitementGroupeCible.get(codeTraitement, groupeCibleMaa);
            productsForCodeTraitementGroupeCible.add(refActaTraitementsProduit);

            refActaTraitementsProduitsByIdProduitIdTraitement.put(idProduit, idTraitement, refActaTraitementsProduit);
        }

        for (RefGroupeCibleTraitement matchMaaProductWithHistoricalActaTraitementIdAndCode : allActiveRefGroupeCibleTraitements) {

            String codeTraitementMaa = matchMaaProductWithHistoricalActaTraitementIdAndCode.getCode_traitement_maa();
            String groupeCibleMaa = StringUtils.trimToNull(matchMaaProductWithHistoricalActaTraitementIdAndCode.getGroupe_cible_maa());

            Collection<RefActaTraitementsProduit> productsForCodeTraitementGroupeCibles = productsByMaaCodeTraitementGroupeCible.get(codeTraitementMaa, groupeCibleMaa);

            if (CollectionUtils.isNotEmpty(productsForCodeTraitementGroupeCibles)) {
                for (RefActaTraitementsProduit changeProductMaaCodeTraitementToActaIdTraitementCodeTraitement : productsForCodeTraitementGroupeCibles) {

                    RefActaTraitementsProduit product;
                    RefActaTraitementsProduit existingRefActaTraitementsProduit = refActaTraitementsProduitsByIdProduitIdTraitement
                            .get(changeProductMaaCodeTraitementToActaIdTraitementCodeTraitement.getId_produit(), matchMaaProductWithHistoricalActaTraitementIdAndCode.getId_traitement());

                    if (existingRefActaTraitementsProduit != null) {
                        // product imported from MAA API but that are not specific enough
                        refActaTraitementsProduitsToDelete.add(changeProductMaaCodeTraitementToActaIdTraitementCodeTraitement);

                        existingRefActaTraitementsProduit.setNom_traitement_maa(changeProductMaaCodeTraitementToActaIdTraitementCodeTraitement.getNom_traitement_maa());
                        existingRefActaTraitementsProduit.setNodu(changeProductMaaCodeTraitementToActaIdTraitementCodeTraitement.isNodu());
                        existingRefActaTraitementsProduit.setSource(changeProductMaaCodeTraitementToActaIdTraitementCodeTraitement.getSource());
                        existingRefActaTraitementsProduit.setActive(true);

                        product = existingRefActaTraitementsProduit;
                    } else {
                        product = changeProductMaaCodeTraitementToActaIdTraitementCodeTraitement;
                    }

                    product.setId_traitement(matchMaaProductWithHistoricalActaTraitementIdAndCode.getId_traitement());
                    product.setCode_traitement(matchMaaProductWithHistoricalActaTraitementIdAndCode.getCode_traitement());
                    product.setNom_traitement(matchMaaProductWithHistoricalActaTraitementIdAndCode.getNom_traitement());

                    refActaTraitementsProduitsToSave.add(product);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Suppression de %d RefActaTraitementsProduit", refActaTraitementsProduitsToDelete.size()));
        }
        refActaTraitementsProduitDao.deleteAll(refActaTraitementsProduitsToDelete);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(String.format("Mise à jour de %d RefActaTraitementsProduit", refActaTraitementsProduitsToSave.size()));
        }
        refActaTraitementsProduitDao.updateAll(refActaTraitementsProduitsToSave);
    }

    @Override
    public void handleMAAResponseForImport(String jsonData,
                                           ImportResult refActaTraitementsProduitImportResult,
                                           ImportResult refMAADosesRefParGroupeCibleImportResult,
                                           ImportResult refMAADBioControleImportResult,
                                           int campagne,
                                           Map<String, Integer> idTraitementsByCode,
                                           Collection<RefGroupeCibleTraitement> refMAACodeTraitementToHistoricCodeTraitements,
                                           Collection<RefActaProduitRoot> refActaProduitRoots,
                                           RefCountry refCountry) {

        MultiValuedMap<String, RefActaTraitementsProduit> refActaTraitementsProduitsByNaturalIds = getRefActaTraitementsProduitByIdProduitCodeTraitementMaa();

        List<RefActaTraitementsProduit> refActaTraitementsProduitsToSave = new ArrayList<>();
        List<RefActaProduitRoot> refActaProduitRootsToSave = new ArrayList<>();

        Map<String, RefMAADosesRefParGroupeCible> refDosesRefParGroupeCibleByNaturaIds = getRefDosesRefParGroupeCiblesByNaturalId(campagne);
        List<RefMAADosesRefParGroupeCible> refDosesRefParGroupeCiblesToSave = new ArrayList<>();

        Map<String, RefMAABiocontrole> refBioControlesByNaturaId = getRefMAABiocontrolesByNaturalId(campagne);
        List<RefMAABiocontrole> refMAABiocontrolesToSave = new ArrayList<>();

        Gson gson = new Gson();
        JsonElement element = JsonParser.parseString(jsonData);
        JsonArray jsonArray = element.getAsJsonArray();

        for (JsonElement jsonElement : jsonArray) {
            ProduitsDosesReference ref = gson.fromJson(jsonElement, ProduitsDosesReference.class);
            try {
                handleRefActaTraitementsProduit(
                        ref,
                        refActaTraitementsProduitsByNaturalIds,
                        idTraitementsByCode,
                        refActaProduitRoots,
                        refActaTraitementsProduitsToSave,
                        refMAACodeTraitementToHistoricCodeTraitements,
                        refActaProduitRootsToSave,
                        refCountry);
            } catch (Exception e) {
                refActaTraitementsProduitImportResult.addError("Erreur lors de l'import de l'élément "
                        + jsonElement + " (" + e.getMessage() + ")");
                refActaTraitementsProduitImportResult.incIgnored();
            }
            try {
                handleRefDosesRefParGroupeCible(ref,
                        refDosesRefParGroupeCibleByNaturaIds,
                        refDosesRefParGroupeCiblesToSave);
            } catch (Exception e) {
                refMAADosesRefParGroupeCibleImportResult.addError("Erreur lors de l'import de l'élément "
                        + jsonElement + " (" + e.getMessage() + ")");
                refMAADosesRefParGroupeCibleImportResult.incIgnored();
            }
            try {
                handleRefBiocontrole(ref,
                        refBioControlesByNaturaId,
                        refMAABiocontrolesToSave);
            } catch (Exception e) {
                refMAADBioControleImportResult.addError("Erreur lors de l'import de l'élément "
                        + jsonElement + " (" + e.getMessage() + ")");
                refMAADBioControleImportResult.incIgnored();
            }
        }

        List<RefActaTraitementsProduit> refActaTraitementsProduitsToUpdate = refActaTraitementsProduitsToSave.stream()
                .filter(RefActaTraitementsProduit::isPersisted)
                .collect(Collectors.toList());
        refActaTraitementsProduitsToSave.removeAll(refActaTraitementsProduitsToUpdate);

        refActaTraitementsProduitDao.createAll(refActaTraitementsProduitsToSave);
        refActaTraitementsProduitImportResult.setCreated(refActaTraitementsProduitsToSave.size());
        refActaTraitementsProduitDao.updateAll(refActaTraitementsProduitsToUpdate);
        refActaTraitementsProduitImportResult.setUpdated(refActaTraitementsProduitsToUpdate.size());

        List<RefActaProduitRoot> refActaProduitRootsToUpdate = refActaProduitRootsToSave.stream()
                .filter(RefActaProduitRoot::isPersisted)
                .collect(Collectors.toList());
        refActaProduitRootsToSave.removeAll(refActaProduitRootsToUpdate);
        refActaProduitRootDao.createAll(refActaProduitRootsToSave);
        refActaProduitRootDao.updateAll(refActaProduitRootsToUpdate);

        List<RefMAADosesRefParGroupeCible> refDosesRefParGroupeCiblesToUpdate = refDosesRefParGroupeCiblesToSave.stream()
                .filter(RefMAADosesRefParGroupeCible::isPersisted)
                .collect(Collectors.toList());
        refDosesRefParGroupeCiblesToSave.removeAll(refDosesRefParGroupeCiblesToUpdate);
        refMAADosesRefParGroupeCibles.createAll(refDosesRefParGroupeCiblesToSave);
        refMAADosesRefParGroupeCibleImportResult.setCreated(refDosesRefParGroupeCiblesToSave.size());
        refMAADosesRefParGroupeCibles.updateAll(refDosesRefParGroupeCiblesToUpdate);
        int nbRefDosesRefParGroupeCiblesToUpdate = refDosesRefParGroupeCiblesToUpdate.size();
        int nbRefDosesRefParGroupeCiblesActive = (int) refDosesRefParGroupeCiblesToUpdate.stream()
                .filter(RefMAADosesRefParGroupeCible::isActive)
                .count();
        refMAADosesRefParGroupeCibleImportResult.setUpdated(nbRefDosesRefParGroupeCiblesActive);
        refMAADosesRefParGroupeCibleImportResult.setUnactivated(nbRefDosesRefParGroupeCiblesToUpdate - nbRefDosesRefParGroupeCiblesActive);

        List<RefMAABiocontrole> refMAABiocontrolesToUpdate = refMAABiocontrolesToSave.stream()
                .filter(RefMAABiocontrole::isPersisted)
                .collect(Collectors.toList());
        refMAABiocontrolesToSave.removeAll(refMAABiocontrolesToUpdate);
        biocontroleDao.createAll(refMAABiocontrolesToSave);
        refMAADBioControleImportResult.setCreated(refMAABiocontrolesToSave.size());
        biocontroleDao.updateAll(refMAABiocontrolesToUpdate);
        refMAADBioControleImportResult.setUpdated(refMAABiocontrolesToUpdate.size());
    }

    @Override
    public ImportResult importRefFeedbackRouter(InputStream contentStream) {
        Set<String> userEmails = new HashSet<>(getPersistenceContext().getAgrosystUserDao().getActiveUserEmails());
        String feedbackEmail = getConfig().getFeedbackEmail().trim().toLowerCase();
        String supportEmail = getConfig().getSupportEmail().trim().toLowerCase();
        userEmails.add(feedbackEmail);
        userEmails.add(supportEmail);

        return runSimpleImport(
                contentStream,
                RefFeedbackRouter.class,
                new RefFeedbackRouterModel(userEmails),
                Referentials.GET_FEEDBACK_ROUTER_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefSubstrate(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefSubstrate.class,
                new RefSubstrateModel(),
                Referentials.GET_REF_SUBSTRATE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefPrixSubstrate(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefPrixSubstrate.class,
                new RefPrixSubstrateModel(),
                Referentials.GET_REF_PRIX_SUBSTRATE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefPot(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefPot.class,
                new RefPotModel(),
                Referentials.GET_REF_POT_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefPrixPot(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefPrixPot.class,
                new RefPrixPotModel(),
                Referentials.GET_REF_PRIX_POT_NATURAL_ID
        );
    }

    @Override
    public ImportResult importCountriesCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefCountry.class,
                new RefCountryModel(),
                Referentials.GET_REF_COUNTRY_NATURAL_ID
        );
    }

    @Override
    public ImportResult importTraduction(Class<? extends ReferentialI18nEntry> klass, InputStream stream) {
        ImportModel<ReferentialI18nEntry> csvModel = new RefTraductionModel();

        ImportResult result = new ImportResult();
        long start = System.currentTimeMillis();

        try (InputStreamReader isr = new InputStreamReader(stream, getConfig().getFileEncoding());
             Import<ReferentialI18nEntry> importer = Import.newImport(csvModel, isr)) {

            TopiaJpaSupport jpaSupport = getPersistenceContext().getJpaSupport();
            List<ReferentialI18nEntry> entries = jpaSupport.findAll(
                    String.format(" FROM %s refTrad ", klass.getName()), new HashMap<>());
            Map<String, ReferentialI18nEntry> translationsByLangAndKeyNewlyAdded = new HashMap<>();
            Map<String, ReferentialI18nEntry> translationsByLangAndKey = new HashMap<>(Maps.uniqueIndex(
                    entries, Referentials.GET_REF_I18N_ENTRY_NATURAL_ID::apply));

            for (ReferentialI18nEntry csvEntry : importer) {

                String langAndKey = Referentials.GET_REF_I18N_ENTRY_NATURAL_ID.apply(csvEntry);
                boolean isNew = false;
                if (translationsByLangAndKeyNewlyAdded.get(langAndKey) != null) {
                    result.addError("valeur dupliquée pour: " + langAndKey);
                    continue;
                }

                ReferentialI18nEntry entry = translationsByLangAndKey.get(langAndKey);
                if (entry == null) {
                    entry = klass.getConstructor().newInstance();
                    isNew = true;
                }

                entry.setLang(csvEntry.getLang());
                entry.setTradkey(csvEntry.getTradkey());
                entry.setTraduction(csvEntry.getTraduction());
                if (isNew) {
                    jpaSupport.save(entry);
                    result.incCreated();
                    translationsByLangAndKeyNewlyAdded.put(langAndKey, entry);
                } else {
                    jpaSupport.update(entry);
                    result.incUpdated();
                }
            }

            if (!result.hasErrors()) {
                getTransaction().commit();
                clearCacheAndSync();
            } else {
                getTransaction().rollback();
                clearCacheAndSync();
            }

        } catch (Exception e) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error(e, e);
            }
            result.addError(e.getMessage());
            getTransaction().rollback();
            clearCacheAndSync();
        }

        long end = System.currentTimeMillis();
        result.setDuration(end - start);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Import des traductions diverses terminé: " + result);
        }
        return result;
    }

    private void handleRefActaTraitementsProduit(ProduitsDosesReference ref,
                                                 MultiValuedMap<String, RefActaTraitementsProduit> entitiesByNaturalId,
                                                 Map<String, Integer> idTraitementsByCode,
                                                 Collection<RefActaProduitRoot> refActaProduitRoots,
                                                 List<RefActaTraitementsProduit> entitiesToSave,
                                                 Collection<RefGroupeCibleTraitement> refMAACodeTraitementToHistoricCodeTraitements,
                                                 List<RefActaProduitRoot> refActaProduitRootsToSave,
                                                 RefCountry refCountry) {
        String codeTraitementMaa = ref.segment.idMetier;
        String nomTraitementMaa = ref.segment.libelle;
        String numeroAmm = Strings.nullToEmpty(ref.numeroAmm.idMetier).trim();
        String numeroAmmUE = "";// no ammUE from the API
        String nomProduitMaa = ref.produit.libelle;
        boolean nodu = ref.biocontrole;
        String source = ref.campagne.idMetier;

        Optional<RefGroupeCibleTraitement> toHistoricIdTraitment = refMAACodeTraitementToHistoricCodeTraitements
                .stream()
                .filter(refMAACodeTraitementToHistoricCodeTraitement -> StringUtil.emptyToNull(refMAACodeTraitementToHistoricCodeTraitement.getCode_groupe_cible_maa()) == null)
                .filter(refMAACodeTraitementToHistoricCodeTraitement -> refMAACodeTraitementToHistoricCodeTraitement.getCode_traitement_maa().contains(codeTraitementMaa))
                .findAny();

        Integer idTraitement = toHistoricIdTraitment.map(RefGroupeCibleTraitement::getId_traitement).orElse(idTraitementsByCode.get(codeTraitementMaa));
        String codeTraitement = toHistoricIdTraitment.map(RefGroupeCibleTraitement::getCode_traitement).orElse(codeTraitementMaa);
        String nomTraitement = toHistoricIdTraitment.map(RefGroupeCibleTraitement::getNom_traitement).orElse(nomTraitementMaa);

        if (idTraitement == null) {
            LOGGER.error("Not Found ID traitment for:" + codeTraitement);
            return;
        }

        RefActaTraitementsProduitDto refActaTraitementsProduitDto = new RefActaTraitementsProduitDto();
        refActaTraitementsProduitDto.setRefCountry(refCountry);
        refActaTraitementsProduitDto.setCode_AMM(numeroAmm);
        refActaTraitementsProduitDto.setNom_produit(nomProduitMaa);
        refActaTraitementsProduitDto.setCode_traitement(codeTraitement);
        refActaTraitementsProduitDto.setNom_traitement(nomTraitement);
        refActaTraitementsProduitDto.setNom_traitement_maa(nomTraitementMaa);
        refActaTraitementsProduitDto.setCode_traitement_maa(codeTraitementMaa);
        refActaTraitementsProduitDto.setId_traitement(idTraitement);
        refActaTraitementsProduitDto.setNodu(nodu);
        refActaTraitementsProduitDto.setActive(true);
        refActaTraitementsProduitDto.setSource(source);

        String unaccentedTrimedNomProduit = StringUtils.stripAccents(nomProduitMaa).trim();
        StringBuilder idProduit = new StringBuilder();
        Optional<RefActaProduitRoot> refActaProduitRoot = refActaProduitRoots.stream()
                .filter(r -> r.isActive()
                        && StringUtils.equalsIgnoreCase(unaccentedTrimedNomProduit, StringUtils.stripAccents(r.getNomProduit()).trim())
                        && (numeroAmm.equals(Strings.nullToEmpty(r.getAmm()).trim()) || numeroAmm.equals(r.getPermis())))
                .findAny();


        if (refActaProduitRoot.isPresent()) {
            idProduit.append(refActaProduitRoot.get().getIdProduit().trim());
        } else {
            idProduit.append("maaf_").append(numeroAmm);

            List<RefActaProduitRoot> refActaProduitRootStream = refActaProduitRoots.stream()
                    .filter(r -> numeroAmm.equals(r.getAmm()))
                    .toList();

            refActaProduitRoot = refActaProduitRootStream.stream()
                    .max(Comparator.comparing(RefActaProduitRoot::getIdProduit)
                            .thenComparing(RefActaProduitRoot::isActive));

            if (refActaProduitRoot.isPresent()) {
                String lastIdProduit = refActaProduitRoot.get().getIdProduit();
                if (idProduit.toString().equals(lastIdProduit)) {
                    idProduit.append("_A");
                } else if (lastIdProduit.startsWith(idProduit.toString())) {
                    char suffix = lastIdProduit.charAt(lastIdProduit.length() - 1);
                    idProduit.append("_").append((char) (suffix + 1));
                }

                // si le produit existe avec l'identifiant mis à jour alors c'est ce produit que l'on utilise
                // sinon on retourne vide afin de créer un nouveau produit avec l'identifiant mis à jour
                refActaProduitRoot = refActaProduitRootStream.stream()
                        .filter(r -> idProduit.toString().equals(r.getIdProduit()))
                        .max(Comparator.comparing(RefActaProduitRoot::getIdProduit));
            }

            RefActaProduitRoot refActaProduitRootToSave;
            if (refActaProduitRoot.isPresent()) {
                refActaProduitRootToSave = refActaProduitRoot.get();
            } else {
                refActaProduitRootToSave = new RefActaProduitRootImpl();
                refActaProduitRoots.add(refActaProduitRootToSave);
            }
            refActaProduitRootToSave.setRefCountry(refCountry);
            refActaProduitRootToSave.setActive(true);
            refActaProduitRootToSave.setIdProduit(idProduit.toString());
            refActaProduitRootToSave.setNomProduit(nomProduitMaa);
            refActaProduitRootToSave.setAmm(numeroAmm);
            refActaProduitRootToSave.setAmmUE(numeroAmmUE);
            refActaProduitRootsToSave.add(refActaProduitRootToSave);
        }
        refActaTraitementsProduitDto.setId_produit(idProduit.toString());

        String idProduitCodeTraitement = GET_ACTA_TRAITEMENTS_PRODUITS_ID_PRODUIT_CODE_TRAITEMENT_FROM_DTO.apply(refActaTraitementsProduitDto);
        Collection<RefActaTraitementsProduit> savedRefActaTraitementsProduits = entitiesByNaturalId.get(idProduitCodeTraitement);

        if (CollectionUtils.isNotEmpty(savedRefActaTraitementsProduits)) {
            for (RefActaTraitementsProduit savedRefActaTraitementsProduit : savedRefActaTraitementsProduits) {
                if (!entitiesToSave.contains(savedRefActaTraitementsProduit)) {
                    setUpdatedValuesToRefActaTraitementsProduit(refActaTraitementsProduitDto, savedRefActaTraitementsProduit);
                    entitiesToSave.add(savedRefActaTraitementsProduit);
                }
            }
        } else {
            RefActaTraitementsProduit refActaTraitementsProduit = refActaTraitementsProduitDao.newInstance();
            setNewValuesToRefActaTraitementsProduit(refActaTraitementsProduitDto, refActaTraitementsProduit);
            entitiesToSave.add(refActaTraitementsProduit);
            entitiesByNaturalId.put(idProduitCodeTraitement, refActaTraitementsProduit);
        }
    }

    private void setNewValuesToRefActaTraitementsProduit(RefActaTraitementsProduitDto actaTraitementsProduitDto,
                                                         RefActaTraitementsProduit refActaTraitementsProduit) {

        Binder<RefActaTraitementsProduitDto, RefActaTraitementsProduit> binder = BinderFactory.newBinder(RefActaTraitementsProduitDto.class, RefActaTraitementsProduit.class);
        binder.copyExcluding(actaTraitementsProduitDto,
                refActaTraitementsProduit,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION);
    }

    private void setUpdatedValuesToRefActaTraitementsProduit(RefActaTraitementsProduitDto actaTraitementsProduitDto,
                                                             RefActaTraitementsProduit refActaTraitementsProduit) {

        Binder<RefActaTraitementsProduitDto, RefActaTraitementsProduit> binder = BinderFactory.newBinder(RefActaTraitementsProduitDto.class, RefActaTraitementsProduit.class);
        binder.copyExcluding(actaTraitementsProduitDto,
                refActaTraitementsProduit,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT,
                RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT,
                RefActaTraitementsProduit.PROPERTY_CODE__AMM,
                RefActaTraitementsProduit.PROPERTY_ACTIVE,
                RefActaTraitementsProduit.PROPERTY_SOURCE
        );
    }

    private void handleRefDosesRefParGroupeCible(ProduitsDosesReference ref,
                                                 Map<String, RefMAADosesRefParGroupeCible> entitiesByNaturalId,
                                                 List<RefMAADosesRefParGroupeCible> entitiesToSave) {

        RefMAADosesRefParGroupeCible refMAADosesRefParGroupeCible = new RefMAADosesRefParGroupeCibleImpl();
        refMAADosesRefParGroupeCible.setCode_amm(ref.numeroAmm.idMetier);
        refMAADosesRefParGroupeCible.setCode_traitement_maa(ref.segment.idMetier);
        refMAADosesRefParGroupeCible.setCode_groupe_cible_maa(ref.cible.idMetier);
        refMAADosesRefParGroupeCible.setCode_culture_maa(ref.culture.idMetier);
        refMAADosesRefParGroupeCible.setCampagne(Integer.parseInt(ref.campagne.idMetier));
        refMAADosesRefParGroupeCible.setType_dose(ref.typeDose);

        String naturalId = Referentials.GET_MAA_DOSES_REF_PAR_GROUPE_CIBLE_NATURAL_ID.apply(refMAADosesRefParGroupeCible);
        RefMAADosesRefParGroupeCible savedRefMAADosesRefParGroupeCible = entitiesByNaturalId.get(naturalId);
        if (savedRefMAADosesRefParGroupeCible != null) {
            if (!entitiesToSave.contains(savedRefMAADosesRefParGroupeCible)) {
                setNewValuesToRefMAADosesRefParGroupeCible(savedRefMAADosesRefParGroupeCible, ref);
                entitiesToSave.add(savedRefMAADosesRefParGroupeCible);
            }
        } else {
            setNewValuesToRefMAADosesRefParGroupeCible(refMAADosesRefParGroupeCible, ref);
            entitiesToSave.add(refMAADosesRefParGroupeCible);
            entitiesByNaturalId.put(naturalId, refMAADosesRefParGroupeCible);
        }
    }

    private void setNewValuesToRefMAADosesRefParGroupeCible(RefMAADosesRefParGroupeCible refMAADosesRefParGroupeCible,
                                                            ProduitsDosesReference ref) {

        PhytoProductUnit unitDoseRefMAA = null;
        String unite = StringUtils.stripAccents(ref.unite.libelle).toUpperCase().replaceAll("[^A-Z0-9]", "_");
        Double doseRefMAA = ref.dose;
        if (!SANS_DOSE_UNIT.equals(unite)) {
            Pair<PhytoProductUnit, Double> haValidUnitValue = convertToPhytoProductUnitHAunit(PhytoProductUnit.valueOf(unite), ref.dose, ref.volumeMaxBouillie);
            doseRefMAA = haValidUnitValue.getValue();
            unitDoseRefMAA = haValidUnitValue.getLeft();
        }
        refMAADosesRefParGroupeCible.setType_dose(ref.typeDose);
        refMAADosesRefParGroupeCible.setDose_ref_maa(doseRefMAA);
        refMAADosesRefParGroupeCible.setUnit_dose_ref_maa(unitDoseRefMAA);
        refMAADosesRefParGroupeCible.setCulture_maa(ref.culture.libelle);
        refMAADosesRefParGroupeCible.setGroupe_cible_maa(ref.cible.libelle);
        refMAADosesRefParGroupeCible.setTraitement_maa(ref.segment.libelle);
        if (ref.descriptionCorrection != null && !ref.descriptionCorrection.startsWith("Correction étiquette biocontr")) {
            if (StringUtils.isNotEmpty(ref.dateCorrection)) {
                refMAADosesRefParGroupeCible.setDate_correction_maa(LocalDate.parse(ref.dateCorrection, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            }
            refMAADosesRefParGroupeCible.setDescription_correction_maa(ref.descriptionCorrection);
        }
        refMAADosesRefParGroupeCible.setVolume_max_bouillie(ref.volumeMaxBouillie);
        refMAADosesRefParGroupeCible.setActive(true);
    }

    private void handleRefBiocontrole(ProduitsDosesReference ref,
                                      Map<String, RefMAABiocontrole> entitiesByNaturalId,
                                      List<RefMAABiocontrole> entitiesToSave) {
        String numeroAmm = ref.numeroAmm.idMetier;
        String idCampagne = ref.campagne.idMetier;

        RefMAABiocontrole refMAABiocontrole = new RefMAABiocontroleImpl();
        refMAABiocontrole.setCode_amm(numeroAmm);
        refMAABiocontrole.setCampagne(Integer.parseInt(idCampagne));

        String naturalId = Referentials.GET_MAA_BIOCONTROLE_NATURAL_ID.apply(refMAABiocontrole);
        RefMAABiocontrole savedRefMAABiocontrole = entitiesByNaturalId.get(naturalId);
        if (savedRefMAABiocontrole != null) {
            if (!entitiesToSave.contains(savedRefMAABiocontrole)) {
                setNewValuesToRefMAABiocontrole(savedRefMAABiocontrole, ref);
                entitiesToSave.add(savedRefMAABiocontrole);
            }
        } else {
            setNewValuesToRefMAABiocontrole(refMAABiocontrole, ref);
            entitiesToSave.add(refMAABiocontrole);
            entitiesByNaturalId.put(naturalId, refMAABiocontrole);
        }
    }

    private void setNewValuesToRefMAABiocontrole(RefMAABiocontrole refMAABiocontrole, ProduitsDosesReference ref) {
        refMAABiocontrole.setBiocontrol(ref.biocontrole);
        if (ref.descriptionCorrection != null && ref.descriptionCorrection.startsWith("Correction étiquette biocontr")) {
            if (StringUtils.isNotEmpty(ref.dateCorrection)) {
                refMAABiocontrole.setDate_correction_maa(LocalDate.parse(ref.dateCorrection, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            }
            refMAABiocontrole.setDescription_correction_maa(ref.descriptionCorrection);
        }
        refMAABiocontrole.setActive(true);
    }

    @Getter
    @Setter
    public static class BaseSol {
        protected List<Record> records;
    }

    @Getter
    @Setter
    public static class Record {
        protected String datasetid;
        protected String recordid;
        protected Sol fields;
        protected Timestamp record_timestamp;
    }

    private static class ProduitsDosesReference {
        boolean biocontrole;
        Campagne campagne;
        Culture culture;
        Double dose;
        TypeDose typeDose;
        NumeroAmm numeroAmm;
        Segment segment;
        Unite unite;
        Produit produit;
        Cible cible;
        String dateCorrection;
        String descriptionCorrection;
        Double volumeMaxBouillie;
    }

    private static class Campagne {
        String idMetier;
    }

    private static class Culture {
        String idMetier;
        String libelle;
    }

    private static class NumeroAmm {
        String idMetier;
    }

    private static class Segment {
        String idMetier;
        String libelle;
    }

    private static class Unite {
        String libelle;
    }

    private static class Produit {
        String libelle;
    }

    private static class Cible {
        String idMetier;
        String libelle;
    }

    @Override
    public ImportResult importRefOtherInputCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefOtherInput.class,
                new RefOtherInputModel(true),
                Referentials.GET_REF_OTHER_INPUT_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefSubstancesActivesCommissionEuropeenneCSV(InputStream contentStream) {
        return runSimpleImport(
                contentStream,
                RefSubstancesActivesCommissionEuropeenne.class,
                new RefSubstancesActivesCommissionEuropeenneModel(),
                Referentials.GET_REF_SUBSTANCES_ACTIVES_COMMISSION_EUROPEENNE_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefCompositionSubstancesActivesParNumeroAMM(InputStream content) {
        final ImportResult importResult = runSimpleImport(
                content,
                RefCompositionSubstancesActivesParNumeroAMM.class,
                new RefCompositionSubstancesActivesParNumeroAMMModel(),
                Referentials.GET_REF_COMPOSITION_NATURAL_ID
        );

        // Même chose que RefPhrasesRisqueEtClassesMentionDangerParAMM :
        // Signaler par email les codes AMM non connus d'Agrosyst
        final Set<String> nonExistingCodes = refCompositionSubstancesActivesParNumeroAMMTopiaDao.findNonExistingCodeAmm();
        if (!nonExistingCodes.isEmpty()) {
            StringBuilder message = new StringBuilder();
            message.append("Codes AMM non connus  : \n");
            nonExistingCodes.forEach(codeAMM -> message.append(codeAMM).append("\n"));

            importResult.addWarning(message.toString());
        }

        return importResult;
    }

    @Override
    public ImportResult importRefPhrasesRisqueEtClassesMentionDangerParAMM(InputStream content) {
        final ImportResult importResult = runSimpleImport(
                content,
                RefPhrasesRisqueEtClassesMentionDangerParAMM.class,
                new RefPhrasesRisqueEtClassesMentionDangerParAMMModel(),
                Referentials.GET_REF_PHRASES_RISQUES_NATURAL_ID
        );

        // Même chose que RefCompositionSubstancesActivesParNumeroAMM :
        // Signaler par email les codes AMM non connus d'Agrosyst
        final Set<String> nonExistingCodes = refPhrasesRisqueEtClassesMentionDangerParAMM.findNonExistingCodeAmm();
        if (!nonExistingCodes.isEmpty()) {
            StringBuilder message = new StringBuilder();
            message.append("Codes AMM non connus : \n");
            nonExistingCodes.forEach(codeAMM -> message.append(codeAMM).append("\n"));

            importResult.addWarning(message.toString());
        }

        return importResult;
    }

    @Override
    public ImportResult importRefConversionUnitesQSA(InputStream content) {
        return runSimpleImport(
                content,
                RefConversionUnitesQSA.class,
                new RefConversionUnitesQSAModel(),
                Referentials.GET_REF_CONVERSION_UNITES_QSA_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefCorrespondanceMaterielOutilsTS(InputStream content) {
        return runSimpleImport(
                content,
                RefCorrespondanceMaterielOutilsTS.class,
                new RefCorrespondanceMaterielOutilsTSModel(),
                Referentials.GET_REF_CORRESPONDANCE_MATERIEL_OUTILS_TS_NATURAL_ID
        );
    }

    @Override
    public ImportResult importRefSeedUnits(InputStream statusStream) {
        return runSimpleImport(
                statusStream,
                RefSeedUnits.class,
                new RefSeedUnitsModel(),
                Referentials.GET_REF_SEED_UNITS_NATURAL_ID);
    }
}
