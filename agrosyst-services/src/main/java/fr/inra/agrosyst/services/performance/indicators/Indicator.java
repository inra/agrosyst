package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielTransportUnit;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.OrganicProductUnit;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.MaintenancePruningVinesAction;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.OrganicProductInputUsage;
import fr.inra.agrosyst.api.entities.action.OtherAction;
import fr.inra.agrosyst.api.entities.action.OtherProductInputUsage;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public abstract class Indicator extends GenericIndicator {

    public static final String INDICATOR_CATEGORY_ECONOMIC = "Indicator.category.economic";
    public static final String INDICATOR_CATEGORY_SOCIO_TECHNIC = "Indicator.category.socio-technic";
    public static final String INDICATOR_CATEGORY_LEGACY_IFT = "Indicator.category.legacy-ift";
    public static final String INDICATOR_CATEGORY_TARGET_IFT = "Indicator.category.target-ift";
    public static final String INDICATOR_CATEGORY_VINTAGE_TARGET_IFT = "Indicator.category.vintage-target-ift";
    public static final String INDICATOR_CATEGORY_CROP_IFT = "Indicator.category.crop-ift";
    public static final String INDICATOR_CATEGORY_VINTAGE_CROP_IFT = "Indicator.category.vintage-crop-ift";
    public static final String INDICATOR_CATEGORY_AGRONOMIC_STRATEGY = "Indicator.category.agronomicStrategy";

    protected final String indicatorName = this.getClass().getSimpleName();
    
    @Getter
    protected final Map<OptionalExtraColumnEnumKey, String> extraFields = new HashMap<>();
    
    /**
     * Define if indicator must be written
     *
     * @param atLevel the given level where indicator intent to write
     * @return true if indicator must be written at this level
     */
    protected abstract boolean isRelevant(ExportLevel atLevel);
    
    /**
     * Retourne la categories de l'indicateur.
     * Par exemple "Résultats socio-techniques".
     * 
     * @return categorie de l'indicateur
     */
    public abstract String getIndicatorCategory();

    /**
     * Retourne le nom de l'indicateur.
     * <p>
     * Dans le cas ou un indicateur retourne de multiple résultat (Double[]), la méthode est appelée
     * plusieurs fois avec l'indice dans le tableau pour obtenir le nom spécific de l'indicateur
     * pour l'indice demandé.
     *
     * @param i indice dans le cas de résultat multiple (peut être ingoré suivant l'indicateur)
     * @return indicateur label
     */
    public abstract String getIndicatorLabel(int i);
    
    /**
     * Permet de retrouver le nom de la colonne en base de données.
     * Il y a deux type de clefs pour retrouver le nom en bd
     * <p>
     * 1: pour l'indicateur lui même, la clef est son label CSV (se qui permet de rester proche de ce qui était développé dans les précédents exports)
     * Dans ce cas chaque indicateur surcharge cette methode pour y ajouter ses valeurs
     * <p>
     * 2: pour les champs complémentaires représenté par les valeurs de l'enum OptionalExtraColumnEnumKey on applique un formatage particulier
     * <p>
     * Il existe des cas particulier IndicatorGrossIncome par exemple qui ne surcharge pas cette méthode
     */
    public Map<String, String> getIndicatorNameToDbColumnName() {
        HashMap<String, String> indicatorNameToColumnName = new HashMap<>();
        for (Map.Entry<OptionalExtraColumnEnumKey, String> extraField : extraFields.entrySet()) {
            indicatorNameToColumnName.put(indicatorName + "_" + extraField.getKey(), extraField.getValue());
        }
        return indicatorNameToColumnName;
    }
    
    /**
     * Define if indicator must be written
     *
     * @param atLevel the given level where indicator intent to write
     * @param i       the current indicator indice in the result array
     * @return true if indicator must be written at this level
     */
    protected abstract boolean isDisplayed(ExportLevel atLevel, int i);
    
    /**
     * Interventions scales, Crop scales = (Crop Cycles (CC)), PracticedSystem scales
     */
    public abstract void computePracticed(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedDomainExecutionContext domainContext);
    
    /**
     * Compute at domain scale
     */
    public abstract void computePracticed(IndicatorWriter writer, Domain domain);

    /**
     * Reset state for domain.
     * 
     * @param domain domain to reset state
     */
    public abstract void resetPracticed(Domain domain);
    
    /**
     * Interventions and Crop Scales = (Crop Cycles (CC))
     * <p>
     * Optionally Write results at Intervention scale
     * Write results to zones scale
     */
    public abstract void computeEffective(IndicatorWriter writer,
                                          PerformanceGlobalExecutionContext globalExecutionContext,
                                          PerformanceEffectiveDomainExecutionContext domainContext,
                                          PerformanceZoneExecutionContext zoneContext);
    
    /**
     * For all Crop Cycle (CC), write result at crop scale
     */
    public abstract void computeEffectiveCC(IndicatorWriter writer,
                                            Domain domain,
                                            PerformanceGrowingSystemExecutionContext growingSystemContext,
                                            Plot plot);
    
    /**
     * Reset crop cycles results
     */
    public abstract void resetEffectiveCC();
    
    /**
     *
     * Zone -> Plot
     * Compute and write result at plot scale. Apply the plot area weighted.
     *
     */
    public abstract void computeEffective(IndicatorWriter writer,
                                          PerformanceEffectiveDomainExecutionContext domainContext,
                                          Optional<GrowingSystem> optionalGrowingSystem,
                                          PerformancePlotExecutionContext plotContext);
    
    /**
     * Reset zones results.
     *
     */
    public abstract void resetEffectiveZones();
    
    /**
     * Plots -> Growing System
     */
    public abstract void computeEffective(IndicatorWriter writer,
                                          PerformanceEffectiveDomainExecutionContext domainExecutionContext,
                                          PerformanceGrowingSystemExecutionContext growingSystemContext);
    
    /**
     * Reset plots results.
     */
    public abstract void resetEffectivePlots();
    
    /**
     * Growing Systems -> Domain. Apply the growing system affected area weighted.
     */
    public abstract void computeEffective(IndicatorWriter writer,
                                          Domain domain);
    
    /**
     * Reset Growing Systems results
     */
    public abstract void resetEffectiveGrowingSystems();
    
    
    protected double getPhytoActionPSCi(PracticedIntervention intervention, AbstractAction phytoAction) {
        double result = getToolPSCi(intervention);
        String interventionId = intervention.getTopiaId();
    
        result *= getPhytoInputTreatedSurfaceFactor(phytoAction, interventionId);
    
        return result;
    }
    
    protected double getPhytoActionPSCi(EffectiveIntervention intervention, AbstractAction phytoAction) {
        double result = getToolPSCi(intervention);
        String interventionId = intervention.getTopiaId();
        
        result *= getPhytoInputTreatedSurfaceFactor(phytoAction, interventionId);
        
        return result;
    }
    
    protected double getPesticideOrBiologicalControlActionPSCi(PracticedIntervention intervention, AbstractAction phytoAction) {
        double result = getToolPSCi(intervention);
        String interventionId = intervention.getTopiaId();
        
        result *= getPhytoInputTreatedSurfaceFactor(phytoAction, interventionId);
        
        return result;
    }
    
    protected double getPesticideOrBiologicalControlActionPSCi(EffectiveIntervention intervention, AbstractAction phytoAction) {
        double result = getToolPSCi(intervention);
        String interventionId = intervention.getTopiaId();
        
        result *= getPhytoInputTreatedSurfaceFactor(phytoAction, interventionId);
        
        return result;
    }
    
    protected double getPhytoInputTreatedSurfaceFactor(AbstractAction action, String interventionId) {
        double result = 1d;
        if (action instanceof PesticidesSpreadingAction pesticidesSpreadingAction) {
            // ProportionOfTreatedSurface
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
    
            result = pesticidesSpreadingAction.getProportionOfTreatedSurface() / 100;
        } else if (action instanceof BiologicalControlAction biologicalControlAction) {
            // ProportionOfTreatedSurface
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
    
            result = biologicalControlAction.getProportionOfTreatedSurface() / 100;
        }
        return result;
    }
    
    public static Collection<AbstractInputUsage> getValidDomainInputUsages(Collection<AbstractAction> actions, Domain domain) {
        
        Collection<AbstractInputUsage> result = new ArrayList<>();
    
        Collection<AbstractAction> actions_ = CollectionUtils.emptyIfNull(actions);
        for (AbstractAction action : actions_) {
            
            AgrosystInterventionType agrosystInterventionType = action.getMainAction().getIntervention_agrosyst();
            switch (agrosystInterventionType) {
                case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX -> {
                    final MineralFertilizersSpreadingAction action_ = (MineralFertilizersSpreadingAction) action;
                    result.addAll(
                            CollectionUtils.emptyIfNull(action_.getMineralProductInputUsages())
                                    .stream()
                                    .filter(di -> di.getDomainMineralProductInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                                    .toList());

                    if (CollectionUtils.isNotEmpty(action_.getOtherProductInputUsages())) result.addAll(
                            filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                }
                case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES -> {
                    final PesticidesSpreadingAction action_ = (PesticidesSpreadingAction) action;
                    result.addAll(CollectionUtils.emptyIfNull(action_.getPesticideProductInputUsages()).stream()
                            .filter(di -> di.getDomainPhytoProductInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                            .toList());

                    if (CollectionUtils.isNotEmpty(CollectionUtils.emptyIfNull(action_.getOtherProductInputUsages())))
                        result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                }
                case AUTRE -> {
                    final OtherAction action_ = (OtherAction) action;
                    result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                    result.addAll(CollectionUtils.emptyIfNull(action_.getPotInputUsages()).stream()
                            .filter(di -> di.getDomainPotInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                            .toList());
                    result.addAll(CollectionUtils.emptyIfNull(action_.getSubstrateInputUsages()).stream()
                            .filter(di -> di.getDomainSubstrateInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                            .toList());
                }
                case ENTRETIEN_TAILLE_VIGNE_ET_VERGER -> {
                    final MaintenancePruningVinesAction action_ = (MaintenancePruningVinesAction) action;
                    result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                }
                case EPANDAGES_ORGANIQUES -> {
                    final OrganicFertilizersSpreadingAction action_ = (OrganicFertilizersSpreadingAction) action;
                    result.addAll(CollectionUtils.emptyIfNull(action_.getOrganicProductInputUsages()).stream()
                            .filter(di -> di.getDomainOrganicProductInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                            .toList());
                    if (CollectionUtils.isNotEmpty(action_.getOtherProductInputUsages()))
                        result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                }
                case IRRIGATION -> {
                    final IrrigationAction action_ = (IrrigationAction) action;
                    if (CollectionUtils.isNotEmpty(action_.getOtherProductInputUsages()))
                        result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                }
                case LUTTE_BIOLOGIQUE -> {
                    final BiologicalControlAction action_ = (BiologicalControlAction) action;
                    result.addAll(CollectionUtils.emptyIfNull(action_.getBiologicalProductInputUsages()).stream()
                            .filter(di -> di.getDomainPhytoProductInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                            .toList());
                    if (CollectionUtils.isNotEmpty(action_.getOtherProductInputUsages()))
                        result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                }
                case RECOLTE -> {
                    final HarvestingAction action_ = (HarvestingAction) action;
                    if (CollectionUtils.isNotEmpty(action_.getOtherProductInputUsages()))
                        result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));
                }
                case SEMIS -> {
                    final SeedingActionUsage action_ = (SeedingActionUsage) action;
                    result.addAll(CollectionUtils.emptyIfNull(action_.getSeedLotInputUsage()).stream()
                            .filter(di -> di.getDomainSeedLotInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                            .toList());
                    if (CollectionUtils.isNotEmpty(action_.getOtherProductInputUsages()))
                        result.addAll(filterOnValidDomain(action_.getOtherProductInputUsages(), domain));

                    if (CollectionUtils.isNotEmpty(action_.getSubstrateInputUsage()))
                        result.addAll(CollectionUtils.emptyIfNull(action_.getSubstrateInputUsage()).stream()
                                .filter(di -> di.getDomainSubstrateInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId()))
                                .toList());
                }
                case TRANSPORT, TRAVAIL_DU_SOL -> {}
            }
        }
        return result;
    }

    private static List<OtherProductInputUsage> filterOnValidDomain(Collection<OtherProductInputUsage> action_, Domain domain) {
        return CollectionUtils.emptyIfNull(action_)
                .stream()
                .filter(iu -> iu.getDomainOtherInput().getDomain().getTopiaId().contentEquals(domain.getTopiaId())).toList();
    }

    /**
     * @return workRate
     */
    protected Pair<Double, MaterielWorkRateUnit> getPracticedInterventionWorkRate(
            PerformancePracticedDomainExecutionContext domainContext,
            PracticedIntervention intervention,
            ToolsCoupling toolsCoupling,
            double defaultWorkRate,
            MaterielWorkRateUnit defaultWorkRateUnit) {
    
        Pair<Double, MaterielWorkRateUnit> result = null;
        
        Double workRate = intervention.getWorkRate();
        MaterielWorkRateUnit workRateUnit = intervention.getWorkRateUnit();
    
        String interventionId = intervention.getTopiaId();

        if (workRate == null || workRateUnit == null) {
            if (toolsCoupling != null) {
                result = domainContext.getToolsCouplingWorkRate(toolsCoupling);
                if (result == null) {
                    result = getToolsCouplingWorkRate(toolsCoupling);
                    domainContext.setToolsCouplingWorkRate(toolsCoupling, result);
                }
            }
        } else {
            result = getInterventionWorkRate(workRate, workRateUnit, interventionId);
        }

        result = getInterventionWorkRate(result, interventionId, defaultWorkRate,defaultWorkRateUnit);

        return result;
    }
    
    /**
     * @return workRate
     */
    protected Pair<Double, MaterielWorkRateUnit> getEffectiveInterventionWorkRate(
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            double defaultWorkRate,
            MaterielWorkRateUnit defaultWorkRateUnit) {
    
        Pair<Double, MaterielWorkRateUnit> result = null;
        
        final EffectiveIntervention intervention = interventionContext.getIntervention();
        
        Double workRate = intervention.getWorkRate();
        MaterielWorkRateUnit workRateUnit = intervention.getWorkRateUnit();

        String interventionId = intervention.getTopiaId();

        if (workRate == null || workRateUnit == null) {
            ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();
            if (toolsCoupling != null) {
                result = domainContext.getToolsCouplingWorkRate(toolsCoupling);
                if (result == null) {
                    result = getToolsCouplingWorkRate(toolsCoupling);
                    domainContext.setToolsCouplingWorkRate(toolsCoupling, result);
                }
            }
        } else {
            result = getInterventionWorkRate(workRate, workRateUnit, interventionId);
        }

        result = getInterventionWorkRate(result, interventionId, defaultWorkRate, defaultWorkRateUnit);

        return result;
    }
    
    protected Double getEffectiveInterventionInvolvedPeopleCount(EffectiveIntervention intervention,
                                                                 double defaultInvolvedPeople) {
        Double involvedPeople = intervention.getInvolvedPeopleCount();
        String interventionId = intervention.getTopiaId();
        // involvedPeople
        return getInvolvedPeople(involvedPeople, interventionId, defaultInvolvedPeople);
    }

    protected Double getPracticedInterventionInvolvedPeopleNumber(PracticedIntervention intervention,
                                                                  double defaultInvolvedPeople) {
        Double involvedPeople = intervention.getInvolvedPeopleNumber();
        String interventionId = intervention.getTopiaId();
        return getInvolvedPeople(involvedPeople, interventionId, defaultInvolvedPeople);
    }

    private Double getInvolvedPeople(Double involvedPeople, String interventionId, double defaultInvolvedPeople) {
        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
        if (involvedPeople == null) {
            addMissingFieldMessage(interventionId, messageBuilder.getMissingInvolvedMessage());
            involvedPeople = defaultInvolvedPeople;
        }
        return involvedPeople;
    }

    protected Pair<Double, OrganicProductUnit> getOrganicInputUsageQteAvgDose(String interventionId,
                                                                              Collection<? extends AbstractInputUsage> inputs,
                                                                              MaterielTransportUnit transitVolumeUnit) {
        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
        Pair<Double, OrganicProductUnit> dose = null;
        Optional<Pair<Double, OrganicProductUnit>> doseAndUnit;

        if (transitVolumeUnit != null) {
            doseAndUnit = inputs.stream()
                    .map(input -> {
                        OrganicProductUnit usageUnit = ((OrganicProductInputUsage) input).getDomainOrganicProductInput().getUsageUnit();
                        Pair<MaterielTransportUnit, OrganicProductUnit> key = Pair.of(transitVolumeUnit, usageUnit);
                        return Pair.of(input, key);
                    })
                    .filter(inputAndKey -> TRANSIT_VOLUME_INPUT_DOSE_UNIT_CONVERTER.containsKey(inputAndKey.getRight()))
                    .map(inputAndKey -> {
                        Double qtAvg = inputAndKey.getLeft() != null && inputAndKey.getLeft().getQtAvg() != null ? inputAndKey.getLeft().getQtAvg() : DEFAULT_ORGANIC_INPUT_QTE_AVG_DOSE;
                        Double convertedDose = TRANSIT_VOLUME_INPUT_DOSE_UNIT_CONVERTER.get(inputAndKey.getRight()) * qtAvg;
                        OrganicProductUnit unit = TRANSIT_VOLUME_INPUT_PRODUCT_UNIT_CONVERTER.get(transitVolumeUnit);
                        return Pair.of(convertedDose, unit);
                    })
                    .max(Comparator.comparing(Pair::getLeft));

        } else {
            doseAndUnit = inputs.stream().filter((input) -> InputType.EPANDAGES_ORGANIQUES == input.getInputType())
                    .max(Comparator.comparing(AbstractInputUsage::getQtAvg))
                    .map((input) -> Pair.of(input.getQtAvg(), ((OrganicProductInputUsage) input).getDomainOrganicProductInput().getUsageUnit()));
        }

        if (doseAndUnit.isPresent()) {
            dose = doseAndUnit.get();
        }

        return dose;
    }

    protected boolean getDeprecatedOrganicInputUsageValidityStatus(
            Collection<? extends AbstractInputUsage> inputs) {

        boolean deprecatedInputValid = true;
        List<? extends AbstractInputUsage> deprecatedInputs = inputs.stream()
                .filter(i -> InputType.EPANDAGES_ORGANIQUES == i.getInputType())
                .filter(i -> StringUtils.isNotBlank(i.getDeprecatedId()))
                .toList();
        if (CollectionUtils.isNotEmpty(deprecatedInputs)) {
            deprecatedInputValid = deprecatedInputs.stream()
                    .noneMatch(di -> di.getDeprecatedQtAvg() == null || di.getDeprecatedUnit() == null);
        }

        return deprecatedInputValid;
    }
    
    protected double getEquipmentsToolsUsage(ToolsCoupling toolsCoupling,
                                             Pair<Double, MaterielWorkRateUnit> workRate,
                                             String interventionId,
                                             Double transitVolume,
                                             MaterielTransportUnit transitVolumeUnit,
                                             Integer nbBalls,
                                             Collection<OrganicProductInputUsage> inputUsages,
                                             Optional<HarvestingAction> optionalHarvestingAction) {
        
        Double usageTime = null;
        
        MaterielWorkRateUnit workRateUnit = workRate.getRight();
        
        for (Equipment equipment : toolsCoupling.getEquipments()) {
            
            final RefMateriel refMateriel = equipment.getRefMateriel();
            
            if (refMateriel == null) continue;
            
            if (TYPE_MAT_EPANDEURS_ET_ACCESSOIRES.equalsIgnoreCase(refMateriel.getTypeMateriel1())) {
                // (PSCi x dose produit) / (débit de chantier x vol/voy)
                
                Double doseVolumeConverter = 1d;
                
                if (inputUsages == null) {
                    continue;// nothing to do
                }
                // dose
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                
                Pair<Double, OrganicProductUnit> dose = getOrganicInputUsageQteAvgDose(interventionId, inputUsages, transitVolumeUnit);

                boolean isDeprecatedInputValid = getDeprecatedOrganicInputUsageValidityStatus(inputUsages);

                if (!isDeprecatedInputValid) {
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingDeprecateDoseMessage(InputType.EPANDAGES_ORGANIQUES, MissingMessageScope.INTERVENTION));
                }
                
                if (dose == null) {
                    dose = Pair.of(DEFAULT_ORGANIC_INPUT_QTE_AVG_DOSE, OrganicProductUnit.T_HA);
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingDoseMessage(InputType.EPANDAGES_ORGANIQUES, MissingMessageScope.INTERVENTION));
                }
                
                if (transitVolume == null) {
                    // refs #10059 : use dose unit specific default value, or if no dose unit use 12 as default t/v value
                    transitVolume = DEFAULT_TRANSIT_VOLUME.get(dose.getRight());
                    
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingTransitVolumeMessage());
                } else {
                    doseVolumeConverter = TRANSIT_VOLUME_INPUT_DOSE_UNIT_CONVERTER.get(Pair.of(transitVolumeUnit, dose.getRight()));
                    
                    if (doseVolumeConverter == null) {
                        
                        Set<MissingFieldMessage> errorFields = targetedErrorFieldMessages.computeIfAbsent(interventionId, k -> Sets.newHashSet());
                        errorFields.add(
                                messageBuilder.getMissingInputUnitTransitUnitConverterMessage(
                                        InputType.EPANDAGES_ORGANIQUES,
                                        dose.getRight(),
                                        transitVolumeUnit)
                        );
                        
                        usageTime = 0d;
                        break;
                    }
                }
                
                usageTime = getWorkRateValue_H_HA_OrDefault(workRate, dose, transitVolume, doseVolumeConverter, DEFAULT_WORK_RATE_VALUE);
                break;
                
            } else if (TYPE_MAT_PRESSES.equalsIgnoreCase(refMateriel.getTypeMateriel1()) && MaterielWorkRateUnit.BAL_H.equals(workRateUnit)) {
                // (PSCi x bal/ha) / (débit de chantier)
                // nbBalls
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                
                if (nbBalls == null) {
                    // refs #10059 : use 10 as default bal/ha value
                    nbBalls = DEFAULT_NB_BALLS;
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingNbBallsMessage());
                }
                
                Double workRateValue = workRate.getLeft();
                if (workRateValue == null) {
                    usageTime = DEFAULT_WORK_RATE_VALUE;
                    
                } else if (workRateValue == 0) {
                    usageTime = 0d;
                    
                } else {
                    // (PSCi x bal/ha) / (débit de chantier)
                    usageTime = nbBalls / workRateValue;
                }
                break;
                
            } else if (TYPE_MAT_RECOLTE_POMMES_CIDRE.equalsIgnoreCase(refMateriel.getTypeMateriel1())) {
                
                if (optionalHarvestingAction.isEmpty()) {
                    continue;// nothing to do
                }
                HarvestingAction harvestingAction = optionalHarvestingAction.get();
                
                // YealdAverage
                incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                
                Collection<HarvestingActionValorisation> valorisations = harvestingAction.getValorisations();
                
                logNonValidValorisations(valorisations, interventionId);
                
                boolean isEdaplosDefaultValorisations = isEdaplosDefaultValorisations(valorisations);
                
                if (isEdaplosDefaultValorisations) {
                    addMissingFieldMessage(interventionId, messageBuilder.getMissingValorisationDestinationMessage());
                    break;
                }
                
                double yealdsAverageValue;
                
                final Set<YealdUnit> yealdUnits = valorisations.stream()
                        .filter(Objects::nonNull)
                        .map(HarvestingActionValorisation::getYealdUnit)
                        .collect(Collectors.toSet());
                long nbYealdUnits = yealdUnits.size();
                
                if (nbYealdUnits > 1) {
                    addMissingFieldMessage(interventionId, messageBuilder.getHarvestingYealdIncoherenceMessage());
                }
                
                yealdsAverageValue = nbYealdUnits == 1 ? valorisations.stream()
                        .filter(Objects::nonNull)
                        .mapToDouble(HarvestingActionValorisation::getYealdAverage)
                        .average()
                        .orElse(0) : 0;// YealAverage can not be null
                double workRateValue = getWorkRateValue_H_HA_OrDefault(workRate, null, null, null, DEFAULT_WORK_RATE_VALUE);
                usageTime = workRateValue == 0 ? 0 : yealdsAverageValue / workRateValue;
                break;
            }
        }
        
        if (usageTime == null) {
            // default for all toolsCoupling
            usageTime = getWorkRateValue_H_HA_OrDefault(workRate, null, null, null, DEFAULT_WORK_RATE_VALUE);
        }
        return usageTime;
    }

    public class InterventionFieldCounterGetter {
        private final String interventionId;
        private final Collection<AbstractInputUsage> inputs;
        private final Optional<IrrigationAction> optionalIrrigationAction;
        private final Optional<SeedingActionUsage> optionalSeedingActionUsage;
        private AtomicReference<Integer> totalFieldCounterForIntervention;
        private AtomicReference<Integer> missingFieldCounterValueForIntervention;

        public InterventionFieldCounterGetter(
                String interventionId,
                Collection<AbstractInputUsage> inputs,
                Optional<IrrigationAction> optionalIrrigationAction,
                Optional<SeedingActionUsage> optionalSeedingActionUsage) {
            this.interventionId = interventionId;
            this.inputs = inputs;
            this.optionalIrrigationAction = optionalIrrigationAction;
            this.optionalSeedingActionUsage = optionalSeedingActionUsage;
        }

        public AtomicReference<Integer> getTotalFieldCounterForIntervention() {
            return totalFieldCounterForIntervention;
        }

        public AtomicReference<Integer> getMissingFieldCounterValueForIntervention() {
            return missingFieldCounterValueForIntervention;
        }

        public InterventionFieldCounterGetter invoke() {
            totalFieldCounterForIntervention = new AtomicReference<>(
                    getTotalFieldCounterValueForTargetedId(interventionId));

            missingFieldCounterValueForIntervention = new AtomicReference<>(
                    getMissingFieldCounterValueForTargetedId(interventionId));

            inputs.forEach(
                    input ->
                    {
                        totalFieldCounterForIntervention.updateAndGet(
                                v -> v + getTotalFieldCounterValueForTargetedId(input.getTopiaId()));
                        missingFieldCounterValueForIntervention.updateAndGet(
                                v -> v + getMissingFieldCounterValueForTargetedId(input.getTopiaId()));
                    }
            );

            if (optionalIrrigationAction.isPresent()) {
                totalFieldCounterForIntervention.updateAndGet(
                        v -> v + getTotalFieldCounterValueForTargetedId(optionalIrrigationAction.get().getTopiaId()));
                missingFieldCounterValueForIntervention.updateAndGet(
                        v -> v + getMissingFieldCounterValueForTargetedId(optionalIrrigationAction.get().getTopiaId()));
            }

            if (optionalSeedingActionUsage.isPresent()) {
                totalFieldCounterForIntervention.updateAndGet(
                        v -> v + getTotalFieldCounterValueForTargetedId(optionalSeedingActionUsage.get().getTopiaId()));
                missingFieldCounterValueForIntervention.updateAndGet(
                        v -> v + getMissingFieldCounterValueForTargetedId(optionalSeedingActionUsage.get().getTopiaId()));
            }

            return this;
        }
    }

    protected Date computeAverageDay(EffectiveIntervention effectiveIntervention) {
        Date startingDate = new Date(effectiveIntervention.getStartInterventionDate().toEpochDay());
        Date endingDate = new Date(effectiveIntervention.getEndInterventionDate().toEpochDay());
        Date averageDate = new Date((startingDate.getTime() + endingDate.getTime()) / 2);

        return averageDate;
    }

    /**
     * Retourne le numéro du jour dans l'année "moyen" pour une intervention synthétisée.
     *
     * On calcule le numéro du jour dans l'année pour la date de début et la date de fin, et
     * on fait la moyenne des deux.
     *
     * date de début de l'intervention est postérieure à la date de fin de l'intervention:
     * DAM = (stat_inter + end_inter + 366) / 2
     *
     * date de début de l'intervention est en fin du cycle annuel et date de fin de l'intervention est en début de cycle annuel
     * DAM est supérieure à 366 on soustrait 366
     * Si  DAM_condition_1 = (start_inter + end_inter + 366) / 2 > 366 => DAM_conditon_2 = (start_inter + end_inter - 366) / 2
     *
     */
    protected Double computeAverageDayOfYear(PracticedIntervention practicedIntervention) {
        final Calendar startPeriodDate = getCalendarFromInterventionDateString(practicedIntervention.getStartingPeriodDate());
        final Calendar endPeriodDate = getCalendarFromInterventionDateString(practicedIntervention.getEndingPeriodDate());

        int yearDiff = 0;
        if (startPeriodDate.getTime().compareTo(endPeriodDate.getTime()) > 0) {
            yearDiff = 366;
        }

        int startingInterventionDayOfYear = getDayOfYear(startPeriodDate);
        int endingInterventionDayOfYear = getDayOfYear(endPeriodDate);
        double dAM_condition_1 = (startingInterventionDayOfYear + yearDiff + endingInterventionDayOfYear) / 2.0;

        Double dAM_condition_2 = null;
        if (dAM_condition_1 > 366) {
            dAM_condition_2 = (startingInterventionDayOfYear + endingInterventionDayOfYear - 366) / 2.0;
        }
        return ObjectUtils.firstNonNull(dAM_condition_2, dAM_condition_1);
    }

    /**
     * Retourne le numéro du jour dans l'année.
     *
     * 01/01 = 1, 02/01 = 2, 02/02 = 32, et ainsi de suite. 31/12 = 366, y compris pour les années non bissextiles
     * pour que les comparaisons se fassent toujours bien. Donc : le 28/02 est le jour 59, le 29/02 est le 60 et
     * le 01/03 est le 61. Pour les années non bissextiles, il n'y a donc pas de jour 60.
     */
    private int getDayOfYear(final Calendar calendar) {
        /*
         * Bricolage pour s'assurer d'avoir toujours un nombre entre 1 et 366 : on met par défaut une année bissextile.
         * Autrement, si on se réfère seulement à l'année en cours, on n'aura pas un nombre "déterministe" pour la date
         * en paramètre, puisque la valeur de Calendar.get(Calendar.DAY_OF_YEAR) dépend de si l'année est bissextile.
         */
        calendar.set(Calendar.YEAR, 2024);
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    protected Calendar getCalendarFromInterventionDateString(final String interventionDate) {
        if (!interventionDate.matches("\\d{1,2}/\\d{1,2}")) {
            throw new IllegalArgumentException("L'argument '%s' ne correspond pas au format de date attendu dd/MM".formatted(interventionDate));
        }
        final String[] dayAndMonth = interventionDate.split("/");

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayAndMonth[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(dayAndMonth[1]) - 1);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.MILLISECOND, 250);

        return calendar;
    }
}
