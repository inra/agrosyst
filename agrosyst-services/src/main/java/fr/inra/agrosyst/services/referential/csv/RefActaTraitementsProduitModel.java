package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduitImpl;
import fr.inra.agrosyst.api.entities.referential.RefCountry;
import fr.inra.agrosyst.api.entities.referential.RefCountryTopiaDao;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.referential.csv.importApi.RefActaProduitRootExportModel;
import fr.inra.agrosyst.services.referential.json.RefApiActaTraitementsProduit;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * id_produit, nom_produit, id_traitement, code_traitement, nom_traitement, NODU 2013, Source.
 * 
 * @author David Cossé
 */
public class RefActaTraitementsProduitModel extends InternationalizationReferentialModel<RefActaTraitementsProduit> implements ExportModel<RefActaTraitementsProduit> {
    
    // for Export
    public RefActaTraitementsProduitModel() {
        super(CSV_SEPARATOR);
    }
    
    // for Import
    public RefActaTraitementsProduitModel(RefCountryTopiaDao refCountryDao) {
        super(CSV_SEPARATOR);
        
        try(final Stream<RefCountry> refCountryStream = refCountryDao.streamAll()){
            refCountryByTrigram = refCountryStream.collect(
                    Collectors.toMap(refCountry -> refCountry.getTrigram().toLowerCase(), Function.identity()));
        }
        
        newMandatoryColumn("pays", RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, TRIGRAM_PARSER);
        newMandatoryColumn("id_produit", RefActaTraitementsProduit.PROPERTY_ID_PRODUIT);
        newMandatoryColumn("nom_produit", RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("id_traitement", RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, INT_PARSER);
        newMandatoryColumn("code_traitement", RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT);
        newMandatoryColumn("nom_traitement", RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT);
        newMandatoryColumn("NODU 2013", RefActaTraitementsProduit.PROPERTY_NODU, CommonService.BOOLEAN_PARSER);
        newOptionalColumn("etat_usage", RefActaTraitementsProduit.PROPERTY_ETAT_USAGE);
        newOptionalColumn("date_retrait_produit", RefActaTraitementsProduit.PROPERTY_DATE_RETRAIT_PRODUIT, LOCAL_DATE_PARSER);
        newOptionalColumn("date_autorisation_produit", RefActaTraitementsProduit.PROPERTY_DATE_AUTORISATION_PRODUIT, LOCAL_DATE_PARSER);
        newMandatoryColumn("Source", RefActaTraitementsProduit.PROPERTY_SOURCE);
        newMandatoryColumn("code_AMM", RefActaTraitementsProduit.PROPERTY_CODE__AMM);
        newMandatoryColumn("code_traitement_maa", RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA, STRING_MANDATORY_PARSER);
        newOptionalColumn("nom_traitement_maa", RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT_MAA);
        newOptionalColumn(COLUMN_ACTIVE, RefActaTraitementsProduit.PROPERTY_ACTIVE, ACTIVE_PARSER);
        newIgnoredColumn("Status");
    }
    
    @Override
    public Iterable<ExportableColumn<RefActaTraitementsProduit, Object>> getColumnsForExport() {
        ModelBuilder<RefActaTraitementsProduit> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("pays", RefActaTraitementsProduit.PROPERTY_REF_COUNTRY, TRIGRAM_FORMATTER);
        modelBuilder.newColumnForExport("id_produit", RefActaTraitementsProduit.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("nom_produit", RefActaTraitementsProduit.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("id_traitement", RefActaTraitementsProduit.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("code_traitement", RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT);
        modelBuilder.newColumnForExport("nom_traitement", RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT);
        modelBuilder.newColumnForExport("NODU 2013", RefActaTraitementsProduit.PROPERTY_NODU, O_N_FORMATTER);
        modelBuilder.newColumnForExport("etat_usage", RefActaTraitementsProduit.PROPERTY_ETAT_USAGE);
        modelBuilder.newColumnForExport("date_retrait_produit", RefActaTraitementsProduit.PROPERTY_DATE_RETRAIT_PRODUIT, LOCAL_DATE_FORMATTER);
        modelBuilder.newColumnForExport("date_autorisation_produit", RefActaTraitementsProduit.PROPERTY_DATE_AUTORISATION_PRODUIT, LOCAL_DATE_FORMATTER);
        modelBuilder.newColumnForExport("Source", RefActaTraitementsProduit.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("code_AMM", RefActaTraitementsProduit.PROPERTY_CODE__AMM);
        modelBuilder.newColumnForExport("code_traitement_maa", RefActaTraitementsProduit.PROPERTY_CODE_TRAITEMENT_MAA);
        modelBuilder.newColumnForExport("nom_traitement_maa", RefActaTraitementsProduit.PROPERTY_NOM_TRAITEMENT_MAA);
        modelBuilder.newColumnForExport("Status", RefApiActaTraitementsProduit.PROPERTY_STATUS, RefActaProduitRootExportModel.STATUS_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefApiActaTraitementsProduit.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefActaTraitementsProduit newEmptyInstance() {
        RefActaTraitementsProduit refActaTraitementsProduit = new RefActaTraitementsProduitImpl();
        refActaTraitementsProduit.setActive(true);
        return refActaTraitementsProduit;
    }
}
