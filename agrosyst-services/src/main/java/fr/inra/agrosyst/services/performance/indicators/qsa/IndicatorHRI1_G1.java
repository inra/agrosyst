package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * HRI-1 Groupe 1 : Seulement pour le coeff "1"
 */
public class IndicatorHRI1_G1 extends IndicatorHRI1_Group {

    protected static final double RETAIN_COEF_PONDERATION = 1.0;

    public static final String COLUMN_NAME = "hri1_g1_tot";
    public static final String COLUMN_NAME_HTS = "hri1_g1_hts";


    protected final String[] indicators = new String[]{
            "Indicator.label.HRI1_g1",        // HRI-1 groupe 1
            "Indicator.label.HRI1_g1_hts",    // HRI-1 groupe 1 hors traitement de semence
    };


    public IndicatorHRI1_G1() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        indicatorNameToColumnName.put(getIndicatorLabel(1), COLUMN_NAME_HTS);
        return indicatorNameToColumnName;
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, indicators[i]);
    }

    protected double getPonderation() {
        return RETAIN_COEF_PONDERATION;
    }
}
