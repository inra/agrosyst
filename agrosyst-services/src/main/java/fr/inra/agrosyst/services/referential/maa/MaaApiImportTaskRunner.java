package fr.inra.agrosyst.services.referential.maa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefMAABiocontrole;
import fr.inra.agrosyst.api.entities.referential.RefMAADosesRefParGroupeCible;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntity;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.referential.ImportResult;
import fr.inra.agrosyst.api.services.referential.ImportService;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.async.AbstractTaskRunner;
import fr.inra.agrosyst.services.common.EmailService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;
import java.util.Map;

/**
 * Runner capable de traiter une tâche asynchrone de type {@link MaaApiImportTask}.
 */
public class MaaApiImportTaskRunner extends AbstractTaskRunner<MaaApiImportTask, Map<Class<? extends ReferentialEntity>, ImportResult>> {

    private static final Log LOG = LogFactory.getLog(MaaApiImportTaskRunner.class);

    @Override
    protected Map<Class<? extends ReferentialEntity>, ImportResult> executeTask(MaaApiImportTask task, ServiceContext serviceContext) {
        ImportService service = serviceContext.newService(ImportService.class);
        return service.importMAAReferentialsFromApi(task.getCampaign(), task.getNumeroAmmIdMetier());
    }

    @Override
    protected void taskSucceeded(MaaApiImportTask task,
                                 ServiceContext serviceContext,
                                 Map<Class<? extends ReferentialEntity>, ImportResult> importResults,
                                 Duration duration) {

        if (LOG.isInfoEnabled()) {
            String message = String.format(
                    "[t=%s] Task succeeded in %ds",
                    task.getTaskId(),
                    duration.getSeconds()
            );
            LOG.info(message);
        }

        ImportResult refActaTraitementsProduitImportResult = importResults.get(RefActaTraitementsProduit.class);
        ImportResult refMAADosesRefParGroupeCibleImportResult = importResults.get(RefMAADosesRefParGroupeCible.class);
        ImportResult refMAABiocontroleImportResult = importResults.get(RefMAABiocontrole.class);

        sendEmailReportMessages(refActaTraitementsProduitImportResult, RefActaTraitementsProduit.class, task.getUserEmail(), serviceContext);
        sendEmailReportMessages(refMAADosesRefParGroupeCibleImportResult, RefMAADosesRefParGroupeCible.class, task.getUserEmail(), serviceContext);
        sendEmailReportMessages(refMAABiocontroleImportResult, RefMAABiocontrole.class, task.getUserEmail(), serviceContext);
    }

    @Override
    protected void taskFailed(MaaApiImportTask task, ServiceContext serviceContext, Exception eee) {
        if (LOG.isErrorEnabled()) {
            String message = String.format(
                    "[t=%s] Task failed with %s.",
                    task.getTaskId(),
                    eee.getMessage()
            );
            LOG.error(message);
        }
        ImportResult refActaTraitementsProduitImportResult = new ImportResult();
        refActaTraitementsProduitImportResult.addError(eee.getMessage());

        ImportResult refMAADosesRefParGroupeCibleImportResult = new ImportResult();
        refMAADosesRefParGroupeCibleImportResult.addError(eee.getMessage());

        ImportResult refMAABiocontroleImportResult = new ImportResult();
        refMAABiocontroleImportResult.addError(eee.getMessage());

        sendEmailReportMessages(refActaTraitementsProduitImportResult, RefActaTraitementsProduit.class, task.getUserEmail(), serviceContext);
        sendEmailReportMessages(refMAADosesRefParGroupeCibleImportResult, RefMAADosesRefParGroupeCible.class, task.getUserEmail(), serviceContext);
        sendEmailReportMessages(refMAABiocontroleImportResult, RefMAABiocontrole.class, task.getUserEmail(), serviceContext);
    }

    protected void sendEmailReportMessages(ImportResult importResult, Class<?> klass, String userEmail, ServiceContext serviceContext) {
        try {
            InputStream content = logMessagesToStream(importResult.getErrors(), importResult.getWarnings());
            EmailService emailService = serviceContext.newService(EmailService.class);
            emailService.sendImportReferentialReport(userEmail, klass, importResult, content);

        } catch (Exception e) {
            // noting to do
        }
    }

    protected InputStream logMessagesToStream(List<String> errors, List<String> warnings) {
        ByteArrayInputStream input;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             Writer writer = new OutputStreamWriter(out, StandardCharsets.UTF_8)) {
            for (String error : errors) {
                writer.write(error + "\n");
            }
            for (String warning : warnings) {
                writer.write(warning + "\n");
            }
            writer.close();
            input = new ByteArrayInputStream(out.toByteArray());

        } catch (IOException e) {
            throw new AgrosystTechnicalException("Can't export import result", e);
        }
        return input;
    }
}
