package fr.inra.agrosyst.services.network;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.NetworkManager;
import fr.inra.agrosyst.api.services.network.NetworkManagerDto;
import fr.inra.agrosyst.api.services.users.Users;

import java.util.function.Function;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class Networks {

    public static final Function<NetworkManager, NetworkManagerDto> TO_MANAGER_DTO = input -> {
        NetworkManagerDto result = new NetworkManagerDto();
        result.setTopiaId(input.getTopiaId());
        result.setActive(input.isActive());
        result.setFromDate(input.getFromDate());
        result.setToDate(input.getToDate());
        result.setUser(Users.TO_USER_DTO.apply(input.getAgrosystUser()));
        return result;
    };

}
