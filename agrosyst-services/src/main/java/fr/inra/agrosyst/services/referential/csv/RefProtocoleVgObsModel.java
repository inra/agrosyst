package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObs;
import fr.inra.agrosyst.api.entities.referential.RefProtocoleVgObsImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Référentiel protocole VgObs.
 * 
 * Protocole - Code protocole;Protocole - Libellé;Protocole - Statut (code);Protocole - Statut;
 * Protocole - Statué le;Protocole - Modifié le;Protocole - Validé le;Protocole - Statué par;
 * Protocole - Modifié par;Protocole - Validé par;Protocole - Cultures;Ligne - Numéro d'ordre;
 * Ligne - Numéro de ligne;Ligne - Code ligne protocole;Ligne - Organisme vivant (code);
 * Ligne - Organisme vivant;Ligne - Stades de développement;Ligne - Stade phéno Initial (code);
 * Ligne - Stade phéno Initial;Ligne - Stade phéno Final (code);Ligne - Stade phéno Final;
 * Ligne - Début;Ligne - Fin;Ligne - Fréquence;Ligne - Supports/Organes;Ligne - Pièges;
 * Ligne -  Type d'échant. (code);Ligne -  Type d'échant.;Ligne - Echantillonnage;
 * Ligne - Type d'observation (code);Ligne - Type d'observation;Ligne - Mode opératoire;
 * Relevé - Nº de relevé;Relevé - Type de relevé;Relevé - Unité (code);Relevé - Unité;
 * Relevé - Qualifiant de l'unité de mesure (code);Relevé - Qualifiant de l'unité de mesure;
 * Relevé - Valeur qualitative (code);Relevé - Valeur qualitative;Relevé - Borne min.;
 * Relevé - Borne max.;Classe - Nº de classe;Classe - Valeur qualitative (code);
 * Classe - Valeur qualitative
 */
public class RefProtocoleVgObsModel extends AbstractAgrosystModel<RefProtocoleVgObs> implements ExportModel<RefProtocoleVgObs> {

    public RefProtocoleVgObsModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Protocole - Code protocole", RefProtocoleVgObs.PROPERTY_PROTOCOLE_CODE_PROTOCOLE);
        newMandatoryColumn("Protocole - Libellé", RefProtocoleVgObs.PROPERTY_PROTOCOLE_LIBELLE);
        newMandatoryColumn("Protocole - Statut (code)", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUT_CODE);
        newMandatoryColumn("Protocole - Statut", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUT);
        newMandatoryColumn("Protocole - Statué le", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUE_LE);
        newMandatoryColumn("Protocole - Modifié le", RefProtocoleVgObs.PROPERTY_PROTOCOLE_MODIFIE_LE);
        newMandatoryColumn("Protocole - Validé le", RefProtocoleVgObs.PROPERTY_PROTOCOLE_VALIDE_LE);
        newMandatoryColumn("Protocole - Statué par", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUE_PAR);
        newMandatoryColumn("Protocole - Modifié par", RefProtocoleVgObs.PROPERTY_PROTOCOLE_MODIFIE_PAR);
        newMandatoryColumn("Protocole - Validé par", RefProtocoleVgObs.PROPERTY_PROTOCOLE_VALIDE_PAR);
        newMandatoryColumn("Protocole - Cultures", RefProtocoleVgObs.PROPERTY_PROTOCOLE_CULTURES);
        newMandatoryColumn("Ligne - Numéro d'ordre", RefProtocoleVgObs.PROPERTY_LIGNE_NUMERO_ORDRE, INT_PARSER);
        newMandatoryColumn("Ligne - Numéro de ligne", RefProtocoleVgObs.PROPERTY_LIGNE_NUMERO_LIGNE, INT_PARSER);
        newMandatoryColumn("Ligne - Code ligne protocole", RefProtocoleVgObs.PROPERTY_LIGNE_CODE_LIGNE_PROTOCOLE);
        newMandatoryColumn("Ligne - Organisme vivant (code)", RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT_CODE);
        newMandatoryColumn("Ligne - Organisme vivant", RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT);
        newMandatoryColumn("Ligne - Stades de développement", RefProtocoleVgObs.PROPERTY_LIGNE_STADES_DEVELOPPEMENT);
        newMandatoryColumn("Ligne - Stade phéno Initial (code)", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_INITIAL_CODE);
        newMandatoryColumn("Ligne - Stade phéno Initial", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_INITIAL);
        newMandatoryColumn("Ligne - Stade phéno Final (code)", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_FINAL_CODE);
        newMandatoryColumn("Ligne - Stade phéno Final", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_FINAL);
        newMandatoryColumn("Ligne - Début", RefProtocoleVgObs.PROPERTY_LIGNE_DEBUT);
        newMandatoryColumn("Ligne - Fin", RefProtocoleVgObs.PROPERTY_LIGNE_FIN);
        newMandatoryColumn("Ligne - Fréquence", RefProtocoleVgObs.PROPERTY_LIGNE_FREQUENCE);
        newMandatoryColumn("Ligne - Supports/Organes", RefProtocoleVgObs.PROPERTY_LIGNE_SUPPORTS_ORGANES);
        newMandatoryColumn("Ligne - Pièges", RefProtocoleVgObs.PROPERTY_LIGNE_PIEGES);
        newMandatoryColumn("Ligne -  Type d'échant. (code)", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_ECHANT_CODE);
        newMandatoryColumn("Ligne -  Type d'échant.", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_ECHANT);
        newMandatoryColumn("Ligne - Echantillonnage", RefProtocoleVgObs.PROPERTY_LIGNE_ECHANTILLONNAGE);
        newMandatoryColumn("Ligne - Type d'observation (code)", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION_CODE);
        newMandatoryColumn("Ligne - Type d'observation", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION);
        newMandatoryColumn("Ligne - Mode opératoire", RefProtocoleVgObs.PROPERTY_LIGNE_MODE_OPERATOIRE);
        newMandatoryColumn("Relevé - Nº de relevé", RefProtocoleVgObs.PROPERTY_RELEVE_NO_RELEVE);
        newMandatoryColumn("Relevé - Type de relevé", RefProtocoleVgObs.PROPERTY_RELEVE_TYPE_RELEVE);
        newMandatoryColumn("Relevé - Unité (code)", RefProtocoleVgObs.PROPERTY_RELEVE_UNITE_CODE);
        newMandatoryColumn("Relevé - Unité", RefProtocoleVgObs.PROPERTY_RELEVE_UNITE);
        newMandatoryColumn("Relevé - Qualifiant de l'unité de mesure (code)", RefProtocoleVgObs.PROPERTY_RELEVE_QUALIFIANT_UNITE_MESURE_CODE);
        newMandatoryColumn("Relevé - Qualifiant de l'unité de mesure", RefProtocoleVgObs.PROPERTY_RELEVE_QUALIFIANT_UNITE_MESURE);
        newMandatoryColumn("Relevé - Valeur qualitative (code)", RefProtocoleVgObs.PROPERTY_RELEVE_VALEUR_QUALITATIVE_CODE);
        newMandatoryColumn("Relevé - Valeur qualitative", RefProtocoleVgObs.PROPERTY_RELEVE_VALEUR_QUALITATIVE);
        newMandatoryColumn("Relevé - Borne min.", RefProtocoleVgObs.PROPERTY_RELEVE_BORNE_MIN);
        newMandatoryColumn("Relevé - Borne max.", RefProtocoleVgObs.PROPERTY_RELEVE_BORNE_MAX);
        newMandatoryColumn("Classe - Nº de classe", RefProtocoleVgObs.PROPERTY_CLASSE_NO_CLASSE);
        newMandatoryColumn("Classe - Valeur qualitative (code)", RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE_CODE);
        newMandatoryColumn("Classe - Valeur qualitative", RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE);
        newOptionalColumn(COLUMN_ACTIVE, RefProtocoleVgObs.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefProtocoleVgObs, Object>> getColumnsForExport() {
        ModelBuilder<RefProtocoleVgObs> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Protocole - Code protocole", RefProtocoleVgObs.PROPERTY_PROTOCOLE_CODE_PROTOCOLE);
        modelBuilder.newColumnForExport("Protocole - Libellé", RefProtocoleVgObs.PROPERTY_PROTOCOLE_LIBELLE);
        modelBuilder.newColumnForExport("Protocole - Statut (code)", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUT_CODE);
        modelBuilder.newColumnForExport("Protocole - Statut", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUT);
        modelBuilder.newColumnForExport("Protocole - Statué le", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUE_LE);
        modelBuilder.newColumnForExport("Protocole - Modifié le", RefProtocoleVgObs.PROPERTY_PROTOCOLE_MODIFIE_LE);
        modelBuilder.newColumnForExport("Protocole - Validé le", RefProtocoleVgObs.PROPERTY_PROTOCOLE_VALIDE_LE);
        modelBuilder.newColumnForExport("Protocole - Statué par", RefProtocoleVgObs.PROPERTY_PROTOCOLE_STATUE_PAR);
        modelBuilder.newColumnForExport("Protocole - Modifié par", RefProtocoleVgObs.PROPERTY_PROTOCOLE_MODIFIE_PAR);
        modelBuilder.newColumnForExport("Protocole - Validé par", RefProtocoleVgObs.PROPERTY_PROTOCOLE_VALIDE_PAR);
        modelBuilder.newColumnForExport("Protocole - Cultures", RefProtocoleVgObs.PROPERTY_PROTOCOLE_CULTURES);
        modelBuilder.newColumnForExport("Ligne - Numéro d'ordre", RefProtocoleVgObs.PROPERTY_LIGNE_NUMERO_ORDRE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Ligne - Numéro de ligne", RefProtocoleVgObs.PROPERTY_LIGNE_NUMERO_LIGNE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Ligne - Code ligne protocole", RefProtocoleVgObs.PROPERTY_LIGNE_CODE_LIGNE_PROTOCOLE);
        modelBuilder.newColumnForExport("Ligne - Organisme vivant (code)", RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT_CODE);
        modelBuilder.newColumnForExport("Ligne - Organisme vivant", RefProtocoleVgObs.PROPERTY_LIGNE_ORGANISME_VIVANT);
        modelBuilder.newColumnForExport("Ligne - Stades de développement", RefProtocoleVgObs.PROPERTY_LIGNE_STADES_DEVELOPPEMENT);
        modelBuilder.newColumnForExport("Ligne - Stade phéno Initial (code)", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_INITIAL_CODE);
        modelBuilder.newColumnForExport("Ligne - Stade phéno Initial", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_INITIAL);
        modelBuilder.newColumnForExport("Ligne - Stade phéno Final (code)", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_FINAL_CODE);
        modelBuilder.newColumnForExport("Ligne - Stade phéno Final", RefProtocoleVgObs.PROPERTY_LIGNE_STADE_PHENO_FINAL);
        modelBuilder.newColumnForExport("Ligne - Début", RefProtocoleVgObs.PROPERTY_LIGNE_DEBUT);
        modelBuilder.newColumnForExport("Ligne - Fin", RefProtocoleVgObs.PROPERTY_LIGNE_FIN);
        modelBuilder.newColumnForExport("Ligne - Fréquence", RefProtocoleVgObs.PROPERTY_LIGNE_FREQUENCE);
        modelBuilder.newColumnForExport("Ligne - Supports/Organes", RefProtocoleVgObs.PROPERTY_LIGNE_SUPPORTS_ORGANES);
        modelBuilder.newColumnForExport("Ligne - Pièges", RefProtocoleVgObs.PROPERTY_LIGNE_PIEGES);
        modelBuilder.newColumnForExport("Ligne -  Type d'échant. (code)", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_ECHANT_CODE);
        modelBuilder.newColumnForExport("Ligne -  Type d'échant.", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_ECHANT);
        modelBuilder.newColumnForExport("Ligne - Echantillonnage", RefProtocoleVgObs.PROPERTY_LIGNE_ECHANTILLONNAGE);
        modelBuilder.newColumnForExport("Ligne - Type d'observation (code)", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION_CODE);
        modelBuilder.newColumnForExport("Ligne - Type d'observation", RefProtocoleVgObs.PROPERTY_LIGNE_TYPE_OBSERVATION);
        modelBuilder.newColumnForExport("Ligne - Mode opératoire", RefProtocoleVgObs.PROPERTY_LIGNE_MODE_OPERATOIRE);
        modelBuilder.newColumnForExport("Relevé - Nº de relevé", RefProtocoleVgObs.PROPERTY_RELEVE_NO_RELEVE);
        modelBuilder.newColumnForExport("Relevé - Type de relevé", RefProtocoleVgObs.PROPERTY_RELEVE_TYPE_RELEVE);
        modelBuilder.newColumnForExport("Relevé - Unité (code)", RefProtocoleVgObs.PROPERTY_RELEVE_UNITE_CODE);
        modelBuilder.newColumnForExport("Relevé - Unité", RefProtocoleVgObs.PROPERTY_RELEVE_UNITE);
        modelBuilder.newColumnForExport("Relevé - Qualifiant de l'unité de mesure (code)", RefProtocoleVgObs.PROPERTY_RELEVE_QUALIFIANT_UNITE_MESURE_CODE);
        modelBuilder.newColumnForExport("Relevé - Qualifiant de l'unité de mesure", RefProtocoleVgObs.PROPERTY_RELEVE_QUALIFIANT_UNITE_MESURE);
        modelBuilder.newColumnForExport("Relevé - Valeur qualitative (code)", RefProtocoleVgObs.PROPERTY_RELEVE_VALEUR_QUALITATIVE_CODE);
        modelBuilder.newColumnForExport("Relevé - Valeur qualitative", RefProtocoleVgObs.PROPERTY_RELEVE_VALEUR_QUALITATIVE);
        modelBuilder.newColumnForExport("Relevé - Borne min.", RefProtocoleVgObs.PROPERTY_RELEVE_BORNE_MIN);
        modelBuilder.newColumnForExport("Relevé - Borne max.", RefProtocoleVgObs.PROPERTY_RELEVE_BORNE_MAX);
        modelBuilder.newColumnForExport("Classe - Nº de classe", RefProtocoleVgObs.PROPERTY_CLASSE_NO_CLASSE);
        modelBuilder.newColumnForExport("Classe - Valeur qualitative (code)", RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE_CODE);
        modelBuilder.newColumnForExport("Classe - Valeur qualitative", RefProtocoleVgObs.PROPERTY_CLASSE_VALEUR_QUALITATIVE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefProtocoleVgObs.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefProtocoleVgObs newEmptyInstance() {
        RefProtocoleVgObs refProtocoleVgObs = new RefProtocoleVgObsImpl();
        refProtocoleVgObs.setActive(true);
        return refProtocoleVgObs;
    }
}
