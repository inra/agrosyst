package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.async.AbstractTask;
import fr.inra.agrosyst.services.performance.dbPersistence.DbCommonReferentiels;
import fr.inra.agrosyst.services.performance.dbPersistence.DbPerformanceDomainContext;

import java.time.OffsetDateTime;
import java.util.Set;

/**
 * Thread lancé après la creation ou la mise à jour d'une performance pour générer le fichier
 * Excel.
 *
 * @author David Cossé
 */
public class PerformanceDbTask extends AbstractTask implements PerformanceTask {
    
    protected final transient Set<String> scenarioCodes;
    
    protected final transient DbCommonReferentiels dbCommonReferentiels;
    
    protected final transient DbPerformanceDomainContext dbPerformanceDomainContext;
    
    protected final String performanceId;
    
    protected final OffsetDateTime performanceDate;
    
    public PerformanceDbTask(String performanceId,
                             String userId,
                             String userEmail,
                             Set<String> scenarioCodes,
                             DbCommonReferentiels dbCommonReferentiels,
                             DbPerformanceDomainContext dbPerformanceDomainContext,
                             OffsetDateTime performanceDate) {
        super(userId, userEmail);
        this.performanceId = performanceId;
        this.performanceDate = performanceDate;
        
        this.scenarioCodes = scenarioCodes;
        this.dbCommonReferentiels = dbCommonReferentiels;
        this.dbPerformanceDomainContext = dbPerformanceDomainContext;
    }

    @Override
    // TODO AThimel 12/11/2020 Faire une vraie description
    public String getDescription() {
        return dbPerformanceDomainContext.domainId();
    }

    @Override
    public boolean mustBeQueued() {
        return false;
    }

//    @Override
//    public String stringify() {
//        return "";
//    }

    @Override
    public String getPerformanceId() {
        return performanceId;
    }

    @Override
    public boolean isDbPerformanceTask() {
        return true;
    }

    public Set<String> getScenarioCodes() {
        return scenarioCodes;
    }
    
    public DbPerformanceDomainContext getDbPerformanceDomainContext() {
        return dbPerformanceDomainContext;
    }
    
    public DbCommonReferentiels getDbCommonReferentiels() {
        return dbCommonReferentiels;
    }
    
    public OffsetDateTime getPerformanceDate() {
        return performanceDate;
    }
}
