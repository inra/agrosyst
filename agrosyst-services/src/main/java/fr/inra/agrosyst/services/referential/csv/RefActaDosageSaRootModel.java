package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRoot;
import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRootImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Created by davidcosse on 05/01/16.
 */
public class RefActaDosageSaRootModel extends AbstractAgrosystModel<RefActaDosageSaRoot> implements ExportModel<RefActaDosageSaRoot> {

    public RefActaDosageSaRootModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("idProduit", RefActaDosageSaRoot.PROPERTY_ID_PRODUIT);
        newMandatoryColumn("nomProduit", RefActaDosageSaRoot.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("idTraitement", RefActaDosageSaRoot.PROPERTY_ID_TRAITEMENT, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("codeTraitement", RefActaDosageSaRoot.PROPERTY_CODE_TRAITEMENT);
        newMandatoryColumn("id_culture", RefActaDosageSaRoot.PROPERTY_ID_CULTURE, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("nom_culture", RefActaDosageSaRoot.PROPERTY_NOM_CULTURE);
        newMandatoryColumn("remarque_culture", RefActaDosageSaRoot.PROPERTY_REMARQUE_CULTURE);
        newMandatoryColumn("dosage_sa_valeur", RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_VALEUR);
        newMandatoryColumn("dosage_sa_unite", RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_UNITE);
        newMandatoryColumn("dosage_sa_commentaire", RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_COMMENTAIRE);
        newOptionalColumn(COLUMN_ACTIVE, RefActaDosageSaRoot.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefActaDosageSaRoot, Object>> getColumnsForExport() {
        ModelBuilder<RefActaDosageSaRoot> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("idProduit", RefActaDosageSaRoot.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("nomProduit", RefActaDosageSaRoot.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("idTraitement", RefActaDosageSaRoot.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("codeTraitement", RefActaDosageSaRoot.PROPERTY_CODE_TRAITEMENT);
        modelBuilder.newColumnForExport("id_culture", RefActaDosageSaRoot.PROPERTY_ID_CULTURE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("nom_culture", RefActaDosageSaRoot.PROPERTY_NOM_CULTURE);
        modelBuilder.newColumnForExport("remarque_culture", RefActaDosageSaRoot.PROPERTY_REMARQUE_CULTURE);
        modelBuilder.newColumnForExport("dosage_sa_valeur", RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_VALEUR);
        modelBuilder.newColumnForExport("dosage_sa_unite", RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_UNITE);
        modelBuilder.newColumnForExport("dosage_sa_commentaire", RefActaDosageSaRoot.PROPERTY_DOSAGE_SA_COMMENTAIRE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaDosageSaRoot.PROPERTY_ACTIVE, T_F_FORMATTER);
        newIgnoredColumn("status");
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefActaDosageSaRoot newEmptyInstance() {
        RefActaDosageSaRoot refActaDosageSaRoot = new RefActaDosageSaRootImpl();
        refActaDosageSaRoot.setActive(true);
        return refActaDosageSaRoot;
    }
}
