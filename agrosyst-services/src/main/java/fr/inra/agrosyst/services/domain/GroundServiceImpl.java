package fr.inra.agrosyst.services.domain;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2016 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Ground;
import fr.inra.agrosyst.api.entities.GroundTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefSolArvalis;
import fr.inra.agrosyst.api.services.domain.GroundService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.services.AbstractAgrosystService;

import java.util.List;

public class GroundServiceImpl extends AbstractAgrosystService implements GroundService {

    protected GroundTopiaDao groundDao;

    protected BusinessAuthorizationService authorizationService;
    protected AnonymizeService anonymizeService;

    public void setGroundDao(GroundTopiaDao groundDao) {
        this.groundDao = groundDao;
    }

    public void setAuthorizationService(BusinessAuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setAnonymizeService(AnonymizeService anonymizeService) {
        this.anonymizeService = anonymizeService;
    }

    @Override
    public List<Ground> getAllGrounds() {
        List<Ground> grounds = groundDao.findAll();
        return grounds;
    }

    @Override
    public List<Ground> getAllGroundsByDomain(Domain domain) {
        List<Ground> grounds = groundDao.forDomainEquals(domain).findAll();
        return grounds;
    }

    @Override
    public Ground newGround() {
        Ground result = groundDao.newInstance();
        return result;
    }

    @Override
    public Ground getGround(String groundId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(groundId));
        Ground ground = groundDao.forTopiaIdEquals(groundId).findUnique();
        return ground;
    }

    @Override
    public
    Ground getDomainByDomainAndRefSolArvalis(Domain domain, RefSolArvalis refSolArvalis) {
        Ground ground = groundDao.forProperties(Ground.PROPERTY_DOMAIN, domain, Ground.PROPERTY_REF_SOL_ARVALIS, refSolArvalis).findAny();
        return ground;
    }

    @Override
    public Ground createOrUpdateGround(final Ground ground) {

        final Ground persistedGround = createOrUpdateGroundWithoutCommit(ground);

        getTransaction().commit();

        return persistedGround;
    }

    /*
    public Ground importEdaplosGround(final Ground ground) {

        final Ground persistedGround = createOrUpdateGroundWithoutCommit(ground);

        return persistedGround;
    }*/

    protected Ground createOrUpdateGroundWithoutCommit(Ground ground) {

        Ground persistedGround;

        if (ground.isPersisted()) {
            persistedGround = groundDao.update(ground);
        } else {
            persistedGround = groundDao.create(ground);
        }

        return persistedGround;
    }
}
