package fr.inra.agrosyst.services.growingplan;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.TypeDEPHY;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.domain.ExtendContext;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanFilter;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.api.services.users.AuthenticatedUser;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.common.export.EntityExporter;
import fr.inra.agrosyst.services.common.export.ExportModelAndRows;
import fr.inra.agrosyst.services.common.export.ExportUtils;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportBeansAndModels.CommonBean;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportBeansAndModels.GrowingPlanBean;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportBeansAndModels.GrowingPlanModel;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportBeansAndModels.GrowingSystemBean;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportBeansAndModels.GrowingSystemModel;
import fr.inra.agrosyst.services.growingplan.export.GrowingPlanExportTask;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.beans.Binder;
import org.nuiton.util.beans.BinderFactory;
import org.nuiton.util.pagination.PaginationResult;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Setter
public class GrowingPlanServiceImpl extends AbstractAgrosystService implements GrowingPlanService {

    private static final Log LOGGER = LogFactory.getLog(GrowingPlanServiceImpl.class);

    protected GrowingSystemService growingSystemService;
    protected AnonymizeService anonymizeService;
    protected BusinessAuthorizationService authorizationService;

    protected GrowingPlanTopiaDao growingPlanDao;
    protected GrowingSystemTopiaDao growingSystemDao;
    protected DomainTopiaDao domainDao;
    
    @Override
    public PaginationResult<GrowingPlan> getFilteredGrowingPlans(GrowingPlanFilter filter) {
        PaginationResult<GrowingPlan> result = growingPlanDao.getFilteredGrowingPlans(filter, getSecurityContext());
        return result;
    }

    @Override
    public PaginationResult<GrowingPlanDto> getFilteredGrowingPlansDto(GrowingPlanFilter filter) {
        PaginationResult<GrowingPlan> growingPlans = getFilteredGrowingPlans(filter);
        PaginationResult<GrowingPlanDto> result = growingPlans.transform(anonymizeService.getGrowingPlanToDtoFunction(true));
        return result;
    }

    @Override
    public Set<String> getFilteredGrowingPlanIds(GrowingPlanFilter filter) {
        return growingPlanDao.getFilteredGrowingPlanIds(filter, getSecurityContext());
    }

    @Override
    public void unactivateGrowingPlans(List<String> growingPlanIds, boolean activate) {
        if (CollectionUtils.isNotEmpty(growingPlanIds)) {

            for (String growingPlanId : growingPlanIds) {
                GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUnique();
                growingPlan.setActive(activate);
                growingPlan.setUpdateDate(context.getCurrentTime());
                growingPlanDao.update(growingPlan);
            }
            getTransaction().commit();
        }
    }

    @Override
    public GrowingPlan getGrowingPlan(String growingPlanId) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(growingPlanId));
        GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUnique();
        GrowingPlan result = anonymizeService.checkForGrowingPlanAnonymization(growingPlan);
        return result;
    }

    @Override
    public GrowingPlan createOrUpdateGrowingPlan(GrowingPlan growingPlan) {
        String growingPlanId = growingPlan.getTopiaId();

        authorizationService.checkCreateOrUpdateGrowingPlan(growingPlanId);

        checkActivated(growingPlan);

        GrowingPlan result = createOrUpdateGrowingPlanWithoutCommit(growingPlan);

        addUserAuthorization(growingPlanId, result);

        // Now validation is automatic https://forge.codelutin.com/issues/4549
        result = validateAndCommit(result.getTopiaId());

        return result;
    }

    protected void checkActivated(GrowingPlan growingPlan) {
        if (growingPlan.isPersisted()) {
            Preconditions.checkArgument(growingPlan.isActive() && growingPlan.getDomain().isActive(),
                    String.format("The growing plan with id '%s' or domain related to it is unactivated", growingPlan.getTopiaId()));
        }
    }

    protected void addUserAuthorization(String growingPlanId, GrowingPlan result) {
        if (StringUtils.isBlank(growingPlanId)) {
            authorizationService.growingPlanCreated(result);
        }
    }

    protected GrowingPlan createOrUpdateGrowingPlanWithoutCommit(GrowingPlan growingPlan) {
        GrowingPlan result;

        growingPlan.setUpdateDate(context.getCurrentTime());
        if (StringUtils.isBlank(growingPlan.getTopiaId())) {

            // create a random growingPlan code, used to link growingPlans each other
            if (StringUtils.isBlank(growingPlan.getCode())) {
                String code = UUID.randomUUID().toString();
                growingPlan.setCode(code);
            }

            growingPlan.setActive(true);
            result = growingPlanDao.create(growingPlan);

        } else {
            result = growingPlanDao.update(growingPlan);
        }
        return result;
    }

    @Override
    public GrowingPlan newGrowingPlan() {
        GrowingPlan result = growingPlanDao.newInstance();
        return result;
    }

    @Override
    public List<GrowingPlan> findAllByDomain(Domain domain) {
        List<GrowingPlan> result = growingPlanDao.forDomainEquals(domain).findAll();
        return result;
    }

    @Override
    public GrowingPlan duplicateGrowingPlan(String growingPlanId, String duplicateDomainId, boolean duplicateGrowingSystems) {

        Domain domain = domainDao.forTopiaIdEquals(duplicateDomainId).findUnique();
        GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUnique();

        GrowingPlan duplicatedGrowingPlan = duplicateGrowingPlan(new ExtendContext(true), domain, growingPlan, duplicateGrowingSystems);
        duplicatedGrowingPlan.setUpdateDate(context.getCurrentTime());
        growingPlanDao.update(duplicatedGrowingPlan);

        getTransaction().commit();

        return duplicatedGrowingPlan;
    }

    @Override
    public GrowingPlan duplicateGrowingPlan(ExtendContext extendContext, Domain clonedDomain, GrowingPlan growingPlan, boolean duplicateGrowingSystems) {

        // perform clone
        Binder<GrowingPlan, GrowingPlan> growingPlanBinder = BinderFactory.newBinder(GrowingPlan.class);
        GrowingPlan clonedGrowingPlan = growingPlanDao.newInstance();
        growingPlanBinder.copyExcluding(growingPlan, clonedGrowingPlan,
                TopiaEntity.PROPERTY_TOPIA_ID,
                TopiaEntity.PROPERTY_TOPIA_CREATE_DATE,
                TopiaEntity.PROPERTY_TOPIA_VERSION,
                GrowingPlan.PROPERTY_DOMAIN);
        clonedGrowingPlan.setDomain(clonedDomain);

        // if duplication, break code
        if (extendContext.isDuplicateOnly()) {
            clonedGrowingPlan.setCode(UUID.randomUUID().toString());
        }

        // Now the new growing plan is automatically validated https://forge.codelutin.com/issues/4549
        clonedGrowingPlan.setUpdateDate(context.getCurrentTime());
        clonedGrowingPlan.setValidated(true);
        clonedGrowingPlan.setValidationDate(context.getCurrentTime());

        // persist clone
        clonedGrowingPlan = growingPlanDao.create(clonedGrowingPlan);

        // others entities to clone
        if (duplicateGrowingSystems) {
            List<GrowingSystem> growingSystems = growingSystemDao.forGrowingPlanEquals(growingPlan)
                    .addEquals(GrowingSystem.PROPERTY_ACTIVE, true)
                    .findAll();
            for (GrowingSystem growingSystem : growingSystems) {
                growingSystemService.duplicateGrowingSystem(extendContext, clonedGrowingPlan, growingSystem);
            }
        }

        return clonedGrowingPlan;
    }

    @Override
    public LinkedHashMap<Integer, String> getRelatedGrowingPlans(String growingPlanCode) {

        LinkedHashMap<Integer, String> result = growingPlanDao.findAllRelatedGrowingPlans(growingPlanCode);
        return result;
    }

    @Override
    public long getGrowingPlansCount(Boolean active) {
        if (active == null) {
            return growingPlanDao.count();
        }
        return growingPlanDao.forActiveEquals(active).count();
    }

    @Override
    public GrowingPlan validateAndCommit(String growingPlanId) {

        authorizationService.checkValidateGrowingPlan(growingPlanId);

        // AThimel 09/12/13 Perform DB update is more powerful
        growingPlanDao.validateGrowingPlan(growingPlanId, context.getCurrentTime());

        getTransaction().commit();

        // AThimel 09/12/13 The next line makes sure that cache state is synchronized with database state
        getPersistenceContext().getHibernateSupport().getHibernateSession().clear();

        GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUnique();
        return growingPlan;
    }

    @Override
    public List<GrowingSystem> getGrowingPlanGrowingSystems(String growingPlanId) {
        List<GrowingSystem> result = growingSystemDao.forProperties(GrowingSystem.PROPERTY_GROWING_PLAN + "." +
                GrowingPlan.PROPERTY_TOPIA_ID, growingPlanId).setOrderByArguments(GrowingPlan.PROPERTY_NAME).findAll();
        return result;
    }

    @Override
    public List<GrowingPlan> getGrowingPlanWithName(String growingPlanName) {
        Preconditions.checkNotNull(growingPlanName);
        List<GrowingPlan> result = growingPlanDao.forNameEquals(growingPlanName).findAll();
        return result;
    }

    @Override
    public ExportResult exportGrowingPlanAsXls(Collection<String> growingPlanIds) {

        List<GrowingPlanBean> growingPlanBeans = new LinkedList<>();
        List<GrowingSystemBean> growingSystemBeans = new LinkedList<>();

        try {
            if (CollectionUtils.isNotEmpty(growingPlanIds)) {
                long start = System.currentTimeMillis();
                Iterable<List<String>> chunks = Iterables.partition(growingPlanIds, 20);
                for (List<String> chunk : chunks) {

                    growingPlanDao.clear();

                    Iterable<GrowingPlan> growingPlans = growingPlanDao.forTopiaIdIn(chunk).findAllLazy(100);
                    for (GrowingPlan gp : growingPlans) {

                        // anonymize growing plan
                        gp = anonymizeService.checkForGrowingPlanAnonymization(gp);

                        Domain domain = gp.getDomain();

                        CommonBean baseBean = new CommonBean(
                                domain.getLocation().getDepartement(),
                                domain.getName(),
                                domain.getCampaign(),
                                gp.getName(),
                                ExportUtils.TYPE_DEPHY_FORMATTER.format(gp.getType())
                        );

                        // main tab
                        GrowingPlanBean growingPlanBean = new GrowingPlanBean(baseBean);
                        growingPlanBean.setDescription(gp.getDescription());
                        growingPlanBean.setGoals(gp.getGoals());
                        growingPlanBean.setProtocolReference(gp.getProtocolReference());
                        growingPlanBean.setInstitutionalStructure(gp.getInstitutionalStructure());
                        growingPlanBeans.add(growingPlanBean);

                        // growing systems tab
                        List<GrowingSystem> gss = getGrowingPlanGrowingSystems(gp.getTopiaId());
                        for (GrowingSystem growingSystem : gss) {

                            GrowingSystemBean growingSystemBean = new GrowingSystemBean(baseBean);
                            growingSystemBean.setName(growingSystem.getName());
                            growingSystemBean.setDephyNumber(growingSystem.getDephyNumber());
                            String networks = growingSystem.getNetworks().stream()
                                    .filter(Objects::nonNull)
                                    .map(Network::getName)
                                    .distinct()
                                    .collect(Collectors.joining(", "));
                            growingSystemBean.setNetworks(networks);
                            growingSystemBeans.add(growingSystemBean);
                        }
                    }
                }

                if (LOGGER.isInfoEnabled()) {
                    long duration = System.currentTimeMillis() - start;
                    int count = growingPlanIds.size();
                    LOGGER.warn(String.format("%d dispositifs exportés en %dms (%dms/dispositif)", count, duration, duration / count));
                }
            }
        } catch (Exception ex) {
            throw new AgrosystTechnicalException("Can't copy properties", ex);
        }

        ExportModelAndRows<GrowingPlanBean> growingPlanBeanExportModelAndRows = new ExportModelAndRows<>(new GrowingPlanModel(), growingPlanBeans);
        ExportModelAndRows<GrowingSystemBean> growingSystemBeanExportModelAndRows = new ExportModelAndRows<>(new GrowingSystemModel(), growingSystemBeans);

        EntityExporter exporter = new EntityExporter();
        ExportResult result = exporter.exportAsXlsx(
                "dispositifs-export",
                growingPlanBeanExportModelAndRows,
                growingSystemBeanExportModelAndRows
        );

        return result;
    }

    @Override
    public void exportGrowingPlanAsXlsAsync(Collection<String> growingPlanIds) {
        AuthenticatedUser user = getAuthenticatedUser();

        GrowingPlanExportTask exportTask = new GrowingPlanExportTask(user.getTopiaId(), user.getEmail(), growingPlanIds);
        getBusinessTaskManager().schedule(exportTask);
    }

    @Override
    public GrowingPlan getOrCreateDefaultGrowingPlanForDomain(Domain domain) {
        Preconditions.checkNotNull(domain);
        GrowingPlan defaultGrowingPlan;

        boolean defaultGrowingPlanExists = growingPlanDao.forDomainEquals(domain)
                .addEquals(GrowingPlan.PROPERTY_NAME, domain.getName())
                .exists();

        if (!defaultGrowingPlanExists) {
            defaultGrowingPlan = newGrowingPlan();
            defaultGrowingPlan.setDomain(domain);
            defaultGrowingPlan.setName(domain.getName());
            defaultGrowingPlan.setType(TypeDEPHY.NOT_DEPHY);
            defaultGrowingPlan.setActive(true);
            createOrUpdateGrowingPlan(defaultGrowingPlan);
        } else {
            defaultGrowingPlan = growingPlanDao.forDomainEquals(domain)
                    .addEquals(GrowingPlan.PROPERTY_NAME, domain.getName())
                    .findAny();
        }

        return defaultGrowingPlan;
    }

    @Override
    public GrowingPlan getDefaultGrowingPlanForDomain(Domain domain) {
        return getOrCreateDefaultGrowingPlanForDomain(domain);
    }

}
