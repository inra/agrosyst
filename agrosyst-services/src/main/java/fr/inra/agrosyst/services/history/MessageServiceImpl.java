package fr.inra.agrosyst.services.history;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2018 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.entities.history.Message;
import fr.inra.agrosyst.api.entities.history.MessageTopiaDao;
import fr.inra.agrosyst.api.services.history.MessageFilter;
import fr.inra.agrosyst.api.services.history.MessageService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by davidcosse on 14/10/14.
 */
public class MessageServiceImpl extends AbstractAgrosystService implements MessageService {

    protected MessageTopiaDao messageTopiaDao;

    public void setMessageTopiaDao(MessageTopiaDao messageTopiaDao) {
        this.messageTopiaDao = messageTopiaDao;
    }

    @Override
    public void publishMessage(String title, String content) {
        Preconditions.checkNotNull(title);
        Message message = messageTopiaDao.newInstance();
        message.setTitle(title);
        message.setContent(content == null ? "" : content);
        message.setMessageDate(context.getCurrentTime());
        messageTopiaDao.create(message);
        getTransaction().commit();
    }


    @Override
    public PaginationResult<Message> getFilteredMessages(MessageFilter filter) {
        PaginationResult<Message> result = messageTopiaDao.getAllMessageByPage(filter);
        return result;
    }

    @Override
    public List<Message> getMessagesFromDate(LocalDateTime fromDate) {
        List<Message> result;

        if (fromDate == null) {
            result = messageTopiaDao.geAllMessages();
        } else {
            result = messageTopiaDao.getMessageFromDate(fromDate);
        }
        return result;
    }

}
