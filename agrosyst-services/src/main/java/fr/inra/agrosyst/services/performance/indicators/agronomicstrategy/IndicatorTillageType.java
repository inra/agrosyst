package fr.inra.agrosyst.services.performance.indicators.agronomicstrategy;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.CroppingPlanSpeciesTopiaDao;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPriceTopiaDao;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.TypeTravailSol;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.effective.EffectivePerennialCropCycle;
import fr.inra.agrosyst.api.entities.effective.EffectiveSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedPerennialCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSeasonalCropCycle;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverterTopiaDao;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.effective.EffectiveCropCycleService;
import fr.inra.agrosyst.api.services.performance.CropWithSpecies;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePlotExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceService;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import fr.inra.agrosyst.services.common.CommonService;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingFieldMessage;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.EffectiveCropCycleScaleKey;
import fr.inra.agrosyst.services.performance.indicators.EffectiveItkCropCycleScaleKey;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.indicators.PracticedSystemScaleKey;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * Type de travail du sol
 */
public class IndicatorTillageType extends Indicator {

    private static final Log LOGGER = LogFactory.getLog(IndicatorTillageType.class);

    private static final String COLUMN_NAME = "type_de_travail_du_sol";

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM
    );

    protected boolean displayed = true;

    protected HarvestingPriceTopiaDao priceDao;
    protected CroppingPlanSpeciesTopiaDao croppingPlanSpeciesDao;

    protected RefInputUnitPriceUnitConverterTopiaDao refInputUnitPriceUnitConverterDao;

    protected EffectiveCropCycleService effectiveCropCycleService;
    protected PerformanceService performanceService;
    protected PricesService pricesService;
    protected ReferentialService referentialService;

    protected final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> effectiveCropsYealdAverage = new HashMap<>();

    protected final Map<EffectiveItkCropCycleScaleKey, TillageType> effectiveItkCropPrevCropPhaseValues = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkCropPrevCropPhaseTotalCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Integer> effectiveItkCropPrevCropPhaseFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveItkCropCycleScaleKey, Set<MissingFieldMessage>> effectiveItkCropPrevCropPhaseFieldsErrors = new HashMap<>();

    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Integer> effectiveCroppingReliabilityTotalCounter = new HashMap<>();
    protected final Map<EffectiveCropCycleScaleKey, Set<MissingFieldMessage>> effectiveCroppingFieldsErrors = new HashMap<>();

    protected final Map<Zone, TillageType> effectiveZoneValues = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Zone, Integer> effectiveZoneReliabilityTotalCounter = new HashMap<>();
    protected final Map<Zone, Set<MissingFieldMessage>> effectiveZoneFieldsErrors = new HashMap<>();

    protected final Map<Plot, TillageType> effectivePlotValues = new HashMap<>();
    protected final Map<Plot, Integer> effectivePlotReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Plot, Integer> effectivePlotReliabilityTotalCounter = new HashMap<>();

    protected final Map<Optional<GrowingSystem>, TillageType> effectiveGrowingSystemValues = new HashMap<>();
    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityFieldErrorCounter = new HashMap<>();
    protected final Map<Optional<GrowingSystem>, Integer> effectiveGrowingSystemReliabilityTotalCounter = new HashMap<>();

    //campaigns, anonymizeGrowingSystem, ps
    protected final Map<PracticedSystemScaleKey, TillageType> practicedSystemsValues = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesTotalCounter = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Integer> reliabilityIndexPracticedSystemValuesErrorCounter = new HashMap<>();
    protected final Map<PracticedSystemScaleKey, Set<MissingFieldMessage>> practicedSystemFieldsErrors = new HashMap<>();

    public IndicatorTillageType() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToColumnName;
    }

    //
    // Calcul au niveau de l'intervention
    //

    public TillageType manageIntervention(PerformancePracticedInterventionExecutionContext interventionContext, Optional<PracticedIntervention> firstSeedingIntervention) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel == null ||
                interventionContext.isFictive() ||
                interventionContext.getIntervention().isIntermediateCrop() ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return null;
        }
        // pour ne pas avoir 0, car il n'y a pas de données utilisateur sur cet indicateur
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());

        TillageType interventionValues = computeInterventionTillageType(interventionContext);

        if (interventionValues == TillageType.TCS) {
            // un outil qualifié de TCS peut avoir comme sortie "semis-direct" si il est aussi identifié comme désherbage mécanique
            // et que sa date de passage est postérieure à celle du semis
            // À l'échelle intervention dans le cas d'un "Semis-Direct" rien n'est affiché

            final ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();
            final Set<RefCorrespondanceMaterielOutilsTS> correspondancesMaterielOutilsTS = findRefCorrespondanceMaterielOutilsTS(toolsCoupling, correspondanceByRefMateriel);
            boolean isDesherbageMecanic = correspondancesMaterielOutilsTS.stream().allMatch(RefCorrespondanceMaterielOutilsTS::isDesherbage_mecanique);

            final Double currentInterventionAverageDay = computeAverageDayOfYear(interventionContext.getIntervention());
            if (isDesherbageMecanic && (firstSeedingIntervention.isEmpty() || currentInterventionAverageDay > computeAverageDayOfYear(firstSeedingIntervention.get()))) {
                interventionValues = TillageType.SEMIS_DIRECT;
            }
        }
        return interventionValues;
    }

    public TillageType manageIntervention(PerformanceEffectiveInterventionExecutionContext interventionContext, Optional<EffectiveIntervention> firstSeedingIntervention) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel == null ||
                interventionContext.getIntervention().isIntermediateCrop() ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return null;
        }

        // pour ne pas avoir 0, car il n'y a pas de données utilisateur sur cet indicateur
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());

        TillageType interventionValues = computeInterventionTillageType(interventionContext);

        if (interventionValues == TillageType.TCS) {
            // un outil qualifié de TCS peut avoir comme sortie dans le test unitaire "semis-direct" si il est aussi identifié comme désherbage mécanique
            // et que sa date de passage est postérieure à celle du semis
            // À l'échelle intervention dans le cas d'un "Semis-Direct" rien n'est affiché

            final ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();
            final Set<RefCorrespondanceMaterielOutilsTS> correspondancesMaterielOutilsTS = findRefCorrespondanceMaterielOutilsTS(toolsCoupling, correspondanceByRefMateriel);
            boolean isDesherbageMecanic = correspondancesMaterielOutilsTS.stream().allMatch(RefCorrespondanceMaterielOutilsTS::isDesherbage_mecanique);

            if (isDesherbageMecanic && (firstSeedingIntervention.isEmpty() ||
                    computeAverageDay(interventionContext.getIntervention()).compareTo(computeAverageDay(firstSeedingIntervention.get())) > 0)) {
                interventionValues = TillageType.SEMIS_DIRECT;
            }

        }

        return interventionValues;
    }

    protected static TillageType computeInterventionTillageType(PerformanceInterventionContext interventionContext) {

        // A cette échelle, si une intervention peut être qualifiée de semis-direct, alors elle n'est pas prise en compte dans l'indicateur. Seuls le labour et les TCS doivent ressortir. Les semis-directs ne ressortent qu'à partir du premier niveau d'agrégation.
        //
        // S'il y a utilisation d'une intervention de type travail du sol dans l'itinéraire technique, on vérifie d'abord à quelle catégorie l'outil de la combinaison d'outil est rattaché dans le référentiel de travail du sol.
        // Puis en fonction de cette catégorie, on qualifie l'intervention par cette dernière.
        // Le tableau suivant récapitule la marche à suivre :
        //
        // | Utilisation d'une intervention de travail du sol | Labour             | OUI | Labour           |
        // |                                                  | TCS                | OUI | TCS              |
        // |                                                  | ni l'un ni l'autre | OUI | ne rien renvoyer |
        //
        // À cette échelle, l'indicateur ne peut prendre que deux valeurs qualitatives (Labour, TCS).

        TillageType interventionValues;
        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();
        final ToolsCoupling toolsCoupling = interventionContext.getToolsCoupling();

        final Set<RefCorrespondanceMaterielOutilsTS> correspondancesMaterielOutilsTS = findRefCorrespondanceMaterielOutilsTS(toolsCoupling, correspondanceByRefMateriel);

        // À noter : d'un point de vue métier, il ne peut pas y avoir dans une combinaison d'outil à la fois un outil
        // de type TCS et un outil de type Labour.
        if (correspondancesMaterielOutilsTS.stream().anyMatch(refCorrespondance -> refCorrespondance.getTypeTravailSol() == TypeTravailSol.LABOUR)) {
            interventionValues = TillageType.LABOUR;
        } else if (correspondancesMaterielOutilsTS.stream().anyMatch(refCorrespondance -> refCorrespondance.getTypeTravailSol() == TypeTravailSol.TCS)) {
            interventionValues = TillageType.TCS;
        } else {
            interventionValues = TillageType.AUCUN;
        }
        return interventionValues;
    }

    private static Set<RefCorrespondanceMaterielOutilsTS> findRefCorrespondanceMaterielOutilsTS(ToolsCoupling toolsCoupling, Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel) {
        return toolsCoupling.getEquipments().stream()
                .map(Equipment::getRefMateriel)
                .filter(correspondanceByRefMateriel::containsKey)
                .map(correspondanceByRefMateriel::get)
                .collect(Collectors.toSet());
    }


    //
    // Calculs aux échelles supérieures
    //

    @Override
    public void computePracticed(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedDomainExecutionContext domainContext) {
        if (growingSystemContext.getAnonymizeGrowingSystem().isEmpty()) {
            return;
        }

        Set<PerformancePracticedSystemExecutionContext> practicedSystemExecutionContexts = growingSystemContext.getPracticedSystemExecutionContexts();

        for (PerformancePracticedSystemExecutionContext practicedSystemExecutionContext : practicedSystemExecutionContexts) {

            computePracticedSeasonal(
                    writer,
                    globalExecutionContext,
                    domainContext,
                    practicedSystemExecutionContext,
                    growingSystemContext
            );

            computePracticedPerennial(
                    writer,
                    globalExecutionContext,
                    domainContext,
                    practicedSystemExecutionContext,
                    growingSystemContext
            );

        }

        writePracticedSystemSheet(writer, domainContext, growingSystemContext);
    }

    protected void computePracticedPerennial(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {

        long chronoT0 = System.currentTimeMillis();

        List<PracticedPerennialCropCycle> practicedPerennialCropCycles = practicedSystemExecutionContext.getPracticedPerennialCropCycles();

        // perennial
        if (CollectionUtils.isEmpty(practicedPerennialCropCycles)) {
            return;
        }

        Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.nonNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performancePracticedCropContextExecutionContexts)) {
            return;// no crop in cycle
        }

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();

        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        if (optionalGrowingSystem.isEmpty()) return;

        GrowingSystem anonymizeGrowingSystem = optionalGrowingSystem.get();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        PracticedSystem practicedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();


        String campaigns = practicedSystem.getCampaigns();
        Set<Integer> psCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(campaigns);

        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, anonymizeGrowingSystem, practicedSystem);

        Map<PracticedPerennialCropCycle, TillageType> practicedSystemCycleValues = new HashMap<>();
        for (PerformancePracticedCropExecutionContext cropContext : performancePracticedCropContextExecutionContexts) {

            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();
            final MultiKeyMap<Object, List<TillageType>> practicedCroppingValues = new MultiKeyMap<>();
            final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();
            final Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop = new HashMap<>();

            // crop, previousCrop, rank, cropConnection -> value
            PracticedPerennialCropCycle cycle = cropContext.getPracticedPerennialCropCycle();

            Set<PerformancePracticedInterventionExecutionContext> interventionExecutionContexts = cropContext.getInterventionExecutionContexts();

            CroppingPlanEntry crop = cropContext.getCropWithSpecies().getCroppingPlanEntry();
            String cropCode = crop.getCode();

            // First we try to bring back the crop from the campaign of the domain from the growing plan from the growing system from the practiced system
            CroppingPlanEntry campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(practicedSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign(), cropCode);
            // If none, we iterate over the campaigns of the practiced system to find one
            if (campaignCroppingPlanEntry == null) {
                final Iterator<Integer> iterator = psCampaigns.iterator();
                while (iterator.hasNext() && campaignCroppingPlanEntry == null) {
                    campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(iterator.next(), cropCode);
                }
            }

            if (campaignCroppingPlanEntry != null) {
                crop = campaignCroppingPlanEntry;
            } else {
                if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(String.format("For practicedSystem %s Crop with code %s could not be find into domain code %s for campaigns %s",
                            practicedSystem.getTopiaId(),
                            crop.getCode(),
                            anonymizeDomain.getCode(),
                            campaigns));
                }
            }

            final double solOccupationPercent = cycle.getSolOccupationPercent();
            perennialCropCyclePercentByCrop.put(crop, solOccupationPercent);


            final Optional<PracticedIntervention> mostRecentSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                    .map(PerformancePracticedInterventionExecutionContext::getIntervention)
                    .filter(intervention0 -> intervention0.getType() == AgrosystInterventionType.SEMIS)
                    .filter(intervention0 -> !intervention0.isIntermediateCrop())
                    .max((i1, i2) -> computeAverageDayOfYear(i1).compareTo(computeAverageDayOfYear(i2)));

            for (PerformancePracticedInterventionExecutionContext interventionContext : interventionExecutionContexts) {

                if (interventionContext.isFictive()) {
                    continue;
                }

                PracticedIntervention intervention = interventionContext.getIntervention();

                if (intervention.getPracticedCropCyclePhase() == null) {
                    continue;
                }

                final String interventionId = intervention.getTopiaId();

                PracticedCropCyclePhase phase = intervention.getPracticedCropCyclePhase();

                TillageType interValues = manageIntervention(interventionContext, mostRecentSeedingIntervention);

                if (interValues != null) {

                    Set<MissingFieldMessage> interventionWarnings;
                    Set<MissingFieldMessage> cropMissingFieldMessages;

                    Collection<AbstractInputUsage> inputUsages = getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomain);

                    InterventionFieldCounterGetter interventionFieldCounterGetter =
                            new AbstractIndicator.InterventionFieldCounterGetter(
                                    interventionId,
                                    inputUsages,
                                    interventionContext.getOptionalIrrigationAction(),
                                    interventionContext.getOptionalSeedingActionUsage())
                                    .invoke();

                    AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
                    AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

                    int reliabilityIndexForIntervention =
                            computeReliabilityIndex(
                                    missingFieldCounterValueForIntervention.get(),
                                    totalFieldCounterForIntervention.get());

                    if (isRelevant(ExportLevel.INTERVENTION)) {
                        // sortie résultat courant
                        final List<AbstractAction> actions = interventionContext.getActions();

                        final String reliabilityCommentForIntervention =
                                getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                        final List<String> iftReferencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                        final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                        boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, 0) && !TillageType.SEMIS_DIRECT.equals(interValues) && !TillageType.AUCUN.equals(interValues);
                        if (isDisplayed) {
                            // write intervention sheet
                            writer.writePracticedPerennialIntervention(
                                    its,
                                    irs,
                                    campaigns,
                                    getIndicatorCategory(),
                                    getIndicatorLabel(0),
                                    interValues.name(),
                                    reliabilityIndexForIntervention,
                                    reliabilityCommentForIntervention,
                                    iftReferencesDosagesUserInfos,
                                    anonymizeDomain,
                                    anonymizeGrowingSystem,
                                    practicedSystem,
                                    crop,
                                    phase,
                                    solOccupationPercent,
                                    intervention,
                                    actions,
                                    practicedSystemExecutionContext.getCodeAmmBioControle(),
                                    interventionYealdAverage,
                                    globalExecutionContext.getGroupesCiblesParCode(),
                                    this.getClass()
                            );
                        }

                        // trow up to crop the missing fields warning that must be displayed at crop level
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = interventionWarnings == null ? new HashSet<>() :
                                interventionWarnings.stream()
                                        .filter(missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.CROP).isPresent())
                                        .collect(Collectors.toSet());

                    } else {
                        // trow up to crop all the missing fields warning (as well messages that are not at crop level displayed)
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = Optional.ofNullable(interventionWarnings).orElse(new HashSet<>());
                    }

                    Set<MissingFieldMessage> missingFieldsForCrop = practicedCroppingFieldsErrors.get(practicedSystem, cropCode, phase);

                    // trow up to crop the missing fields warning
                    if (missingFieldsForCrop == null) {
                        missingFieldsForCrop = new HashSet<>();
                    }

                    if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                        missingFieldsForCrop.addAll(cropMissingFieldMessages);
                    }

                    practicedCroppingFieldsErrors.put(practicedSystem, cropCode, phase, cropMissingFieldMessages);

                    // somme pour CC (pour toutes les cultures du cycle)
                    List<TillageType> previous = practicedCroppingValues.get(cropCode, phase);
                    if (previous == null) {
                        final List<TillageType> valuesForCrop = new LinkedList<>();
                        valuesForCrop.add(interValues);

                        practicedCroppingValues.put(cropCode, phase, valuesForCrop);
                        if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                            cropsYealdAverage.put(cropCode, cropContext.getIntermediateCropYealds());
                        } else {
                            cropsYealdAverage.put(cropCode, cropContext.getMainCropYealds());
                        }
                    } else {
                        previous.add(interValues);
                    }

                    // somme pour CC
                    final Integer prevPhaseReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(
                            practicedSystem,
                            cropCode,
                            phase);

                    final int totalFieldCounterValueForCrop =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevPhaseReliabilityTotalCount, 0);

                    practicedCroppingReliabilityTotalCounter.put(
                            practicedSystem,
                            cropCode,
                            phase,
                            totalFieldCounterValueForCrop);

                    final Integer prevPhaseMissingFieldCounter =
                            practicedCroppingReliabilityFieldErrorCounter.get(
                                    practicedSystem,
                                    cropCode,
                                    phase);

                    final int phaseMissingFieldCounter =
                            missingFieldCounterValueForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevPhaseMissingFieldCounter, 0);

                    practicedCroppingReliabilityFieldErrorCounter.put(
                            practicedSystem,
                            cropCode,
                            phase,
                            phaseMissingFieldCounter);

                    // practicedSystem scale
                    final Integer previousPracticedSystemRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(practicedSystemScaleKey);

                    final int totalFieldCounterValueForPracticedSystem =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(previousPracticedSystemRTC, 0);

                    reliabilityIndexPracticedSystemValuesTotalCounter.put(practicedSystemScaleKey, totalFieldCounterValueForPracticedSystem);

                    Integer previousPracticedSystemREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(practicedSystemScaleKey);

                    final int missingFieldCounterValueForGS = missingFieldCounterValueForIntervention.get() +
                            ObjectUtils.firstNonNull(previousPracticedSystemREC, 0);

                    reliabilityIndexPracticedSystemValuesErrorCounter.put(practicedSystemScaleKey, missingFieldCounterValueForGS);

                    Set<MissingFieldMessage> messagesAtPracticedScope = practicedSystemFieldsErrors.computeIfAbsent(practicedSystemScaleKey, k -> new HashSet<>());

                    // trow up to practiced system the missing fields warning that must be displayed at Practiced System level
                    Set<MissingFieldMessage> practicedMissingFieldMessages = interventionWarnings == null ?
                            new HashSet<>() :
                            interventionWarnings.stream().
                                    filter(missingFieldMessage ->
                                            missingFieldMessage.getMessageForScope(MissingMessageScope.PRACTICED_SYSTEM).isPresent())
                                    .collect(Collectors.toSet());

                    if (CollectionUtils.isNotEmpty(practicedMissingFieldMessages)) {
                        messagesAtPracticedScope.addAll(practicedMissingFieldMessages);
                    }
                }
            } // fin des interventions


            PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();
            List<Map.Entry<MultiKey<?>, List<TillageType>>> practicedPerennialValuesByCrops = practicedCroppingValues.entrySet().stream()
                    .filter(entry -> {
                        final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                        return previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase;
                    })
                    .toList();

            List<Map.Entry<MultiKey<?>, TillageType>> practicedPerennialCroppingValues = new LinkedList<>();

            // Calcul de la valeur de l'indicateur à l'échelle de la culture
            for (Map.Entry<MultiKey<?>, List<TillageType>> entry : practicedPerennialValuesByCrops) {

                TillageType cropValues = getTillageTypeAtCropScale(entry.getValue());

                if (cropValues == null) continue;

                practicedPerennialCroppingValues.add(Map.entry(entry.getKey(), cropValues));

                TillageType psPreviousValue = practicedSystemCycleValues.get(cycle);
                if (psPreviousValue == null) {
                    practicedSystemCycleValues.put(cycle, cropValues);
                } else {
                    final TillageType sum = sum(psPreviousValue, cropValues);
                    practicedSystemCycleValues.put(cycle, sum);
                }
            }

            writePracticedPerennialCroppingValues(
                    writer,
                    domainContext,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingReliabilityTotalCounter,
                    perennialCropCyclePercentByCrop,
                    cropsYealdAverage,
                    practicedSystemExecutionContext,
                    practicedCroppingFieldsErrors,
                    anonymizeGrowingSystem,
                    its,
                    irs,
                    anonymizePracticedSystem,
                    practicedPerennialCroppingValues);

        }// fin des cycle de cultures

        // mise à l'échelle vers Practiced System
        final List<TillageType> tillageTypes = practicedSystemCycleValues.values()
                .stream()
                .filter(tt -> tt != TillageType.AUCUN)
                .toList();

        final double interventionsWithTillageCount = tillageTypes.size();
        // les valeurs LABOUR_SYSTEMATIQUE, LABOUR_FREQUENT, LABOUR_OCCASIONNEL ne sont utilisées qu'à l'échelle système de culture et pas avant,
        final double ploughingPercentage = interventionsWithTillageCount == 0 ? 0 : tillageTypes.stream().filter(tt -> tt == TillageType.LABOUR).count() / interventionsWithTillageCount;

        if (interventionsWithTillageCount > 0) {
            TillageType sdcValue;

            if (ploughingPercentage == 1.0) {
                sdcValue = TillageType.LABOUR_SYSTEMATIQUE;
            } else if (ploughingPercentage > 0.66) {
                sdcValue = TillageType.LABOUR_FREQUENT;
            } else if (ploughingPercentage > 0) {
                sdcValue = TillageType.LABOUR_OCCASIONNEL;
            } else {
                // Dans ce cas, il n'y a aucun LABOUR. Donc il y a soit TCS, soit SEMIS_DIRECT
                // comme valeurs possibles. S'il y a au moins 1 TCS alors TCS, sinon c'est SEMIS_DIRECT
                if (tillageTypes.stream().anyMatch(tt -> tt == TillageType.TCS)) {
                    sdcValue = TillageType.TCS;
                } else {
                    sdcValue = TillageType.SEMIS_DIRECT;
                }
            }

            practicedSystemsValues.put(practicedSystemScaleKey, sdcValue);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getSimpleName() + ": " + (System.currentTimeMillis() - chronoT0) + " ms");
        }
    }

    protected void computePracticedSeasonal(IndicatorWriter writer,
                                            PerformanceGlobalExecutionContext globalExecutionContext,
                                            PerformancePracticedDomainExecutionContext domainContext,
                                            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
                                            PerformanceGrowingSystemExecutionContext growingSystemContext) {

        long chronoT0 = System.currentTimeMillis();

        PracticedSeasonalCropCycle practicedSeasonalCropCycle = practicedSystemExecutionContext.getSeasonalCropCycle();

        // seasonal
        if (practicedSeasonalCropCycle == null) {
            return;
        }

        if (CollectionUtils.isEmpty(practicedSeasonalCropCycle.getCropCycleNodes())) {
            return;
        }

        Set<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts_ =
                practicedSystemExecutionContext.getPerformancePracticedCropContextExecutionContexts().stream()
                        .filter(
                                performanceCropExecutionContext ->
                                        Objects.isNull(performanceCropExecutionContext.getPracticedPerennialCropCycle()))
                        .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performancePracticedCropContextExecutionContexts_)) {
            return;// no crop in cycle
        }

        List<PerformancePracticedCropExecutionContext> performancePracticedCropContextExecutionContexts = performancePracticedCropContextExecutionContexts_
                .stream()
                .sorted(Comparator.comparingInt(PerformancePracticedCropExecutionContext::getRank)).toList();

        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        if (optionalGrowingSystem.isEmpty()) return;

        GrowingSystem anonymizeGrowingSystem = optionalGrowingSystem.get();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        PracticedSystem practicedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();

        String campaigns = practicedSystem.getCampaigns();
        Set<Integer> psCampaigns = CommonService.GET_CAMPAIGNS_SET.apply(campaigns);

        PracticedSystemScaleKey practicedSystemScaleKey = new PracticedSystemScaleKey(campaigns, anonymizeGrowingSystem, practicedSystem);

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();

        List<TillageType> practicedSystemCycleValues = new ArrayList<>();

        // compute at crop cycle level
        for (PerformancePracticedCropExecutionContext cropContext : performancePracticedCropContextExecutionContexts) {

            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter = new MultiKeyMap<>();
            final MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors = new MultiKeyMap<>();
            final Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage = new HashMap<>();

            // crop, previousCrop, rank, cropConnection -> value
            MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode = new MultiKeyMap<>();

            // seasonal:  cropCode, previousPlanEntryCode, rank, cropConnection
            // perennial: cropCode, phase
            MultiKeyMap<Object, List<TillageType>> practicedCroppingValues = new MultiKeyMap<>();

            Set<PerformancePracticedInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();
            if (CollectionUtils.isEmpty(interventionContexts)) continue; // NOTHING TO DO

            CropWithSpecies intermediateCropWithSpecies = cropContext.getIntermediateCropWithSpecies();
            CroppingPlanEntry crop = cropContext.getCropWithSpecies().getCroppingPlanEntry();
            final String cropCode = crop.getCode();
            {
                // First we try to bring back the crop from the campaign of the domain from the growing plan from the growing system from the practiced system
                CroppingPlanEntry campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(practicedSystem.getGrowingSystem().getGrowingPlan().getDomain().getCampaign(), cropCode);
                // If none, we iterate over the campaigns of the practiced system to find one
                if (campaignCroppingPlanEntry == null) {
                    final Iterator<Integer> iterator = psCampaigns.iterator();
                    while (iterator.hasNext() && campaignCroppingPlanEntry == null) {
                        campaignCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(iterator.next(), cropCode);
                    }
                }
                if (campaignCroppingPlanEntry != null) {
                    crop = campaignCroppingPlanEntry;
                } else {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(String.format("For practicedSystem %s Crop with code %s could not be find into domain code %s for campaigns %s",
                                practicedSystem.getTopiaId(),
                                cropCode,
                                anonymizeDomain.getCode(),
                                campaigns));
                    }
                }
            }

            CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();
            {
                CroppingPlanEntry previousPlanEntry = cropContext.getSeasonalPreviousCrop();
                if (previousCrop != null) {
                    previousPlanEntry = domainContext.getCropByCampaignAndCode().get(domainContext.getDomain().getCampaign(), previousPlanEntry.getCode());
                }
                if (previousCrop != null) {
                    previousCrop = previousPlanEntry;
                } else {
                    if (LOGGER.isWarnEnabled()) {
                        LOGGER.warn(String.format("For practicedSystem %s Crop with code %s could not be find into domain code %s for campaigns %s",
                                practicedSystem.getTopiaId(),
                                cropCode,
                                anonymizeDomain.getCode(),
                                campaigns));
                    }
                }
            }
            String previousPlanEntryCode = previousCrop != null ? previousCrop.getCode() : "";

            PracticedCropCycleConnection cropConnection = cropContext.getConnection();
            CroppingPlanEntry intermediateCrop = intermediateCropWithSpecies != null ?
                    intermediateCropWithSpecies.getCroppingPlanEntry() :
                    null;

            int rank = cropContext.getRank();

            final Optional<PracticedIntervention> mostRecentSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                    .filter(pic -> !pic.isFictive())
                    .map(PerformancePracticedInterventionExecutionContext::getIntervention)
                    .filter(intervention0 -> intervention0.getType() == AgrosystInterventionType.SEMIS)
                    .filter(intervention0 -> !intervention0.isIntermediateCrop())
                    .max((i1, i2) -> computeAverageDayOfYear(i1).compareTo(computeAverageDayOfYear(i2)));


            for (PerformancePracticedInterventionExecutionContext interventionContext : interventionContexts) {
                PracticedIntervention intervention = interventionContext.getIntervention();
                String interventionId = null;

                if (!interventionContext.isFictive()) {

                    if (intervention.getPracticedCropCyclePhase() != null) {
                        continue;
                    }

                    interventionId = interventionContext.getInterventionId();

                }

                TillageType interValues = manageIntervention(interventionContext, mostRecentSeedingIntervention);

                if (interValues != null) {

                    // sortie résultat courant
                    Set<MissingFieldMessage> cropMissingFieldMessages;
                    Set<MissingFieldMessage> interventionWarnings;

                    Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomain);

                    AbstractIndicator.InterventionFieldCounterGetter interventionFieldCounterGetter =
                            new AbstractIndicator.InterventionFieldCounterGetter(
                                    interventionId,
                                    inputUsages,
                                    interventionContext.getOptionalIrrigationAction(),
                                    interventionContext.getOptionalSeedingActionUsage())
                                    .invoke();

                    AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
                    AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

                    int reliabilityIndexForIntervention =
                            computeReliabilityIndex(
                                    missingFieldCounterValueForIntervention.get(),
                                    totalFieldCounterForIntervention.get());

                    if (!interventionContext.isFictive() &&
                            this.isRelevant(ExportLevel.INTERVENTION)) {

                        CroppingPlanEntry interventionIntermediateCrop = null;
                        if (intervention.isIntermediateCrop()) {
                            interventionIntermediateCrop = intermediateCrop;
                        }

                        List<String> iftReferencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                        final List<AbstractAction> actions = interventionContext.getActions();

                        final String reliabilityCommentForInterventionId =
                                getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                        final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                        boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, 0) && !TillageType.SEMIS_DIRECT.equals(interValues) && !TillageType.AUCUN.equals(interValues);
                        if (isDisplayed) {
                            // write intervention sheet
                            writer.writePracticedSeasonalIntervention(
                                    its,
                                    irs,
                                    campaigns,
                                    getIndicatorCategory(),
                                    getIndicatorLabel(0),
                                    interValues.name(),
                                    reliabilityIndexForIntervention,
                                    reliabilityCommentForInterventionId,
                                    iftReferencesDosagesUserInfos,
                                    anonymizeDomain,
                                    anonymizeGrowingSystem,
                                    practicedSystem,
                                    crop,
                                    rank,
                                    previousCrop,
                                    interventionIntermediateCrop,
                                    intervention,
                                    actions,
                                    practicedSystemExecutionContext.getCodeAmmBioControle(),
                                    interventionYealdAverage,
                                    globalExecutionContext.getGroupesCiblesParCode(),
                                    this.getClass()
                            );
                        }

                        // trow up to crop the missing fields warning that must be displayed at crop level
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = interventionWarnings == null ?
                                new HashSet<>() :
                                interventionWarnings.stream()
                                        .filter(missingFieldMessage ->
                                                missingFieldMessage.getMessageForScope(MissingMessageScope.CROP)
                                                        .isPresent())
                                        .collect(Collectors.toSet());

                    } else {
                        // trow up to crop all the missing fields warning (as well messages that are not at crop level displayed)
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = ObjectUtils.firstNonNull(interventionWarnings, new HashSet<>());
                    }

                    // trow up to crop and practicedSystem the missing fields warning
                    String interventionCroppingPlanEntryCode = crop.getCode();

                    Set<MissingFieldMessage> messagesAtCropScope = practicedCroppingFieldsErrors.get(interventionCroppingPlanEntryCode, previousPlanEntryCode, rank);

                    // trow up to crop the missing fields warning
                    if (messagesAtCropScope == null) {
                        messagesAtCropScope = new HashSet<>();
                    }

                    if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                        messagesAtCropScope.addAll(cropMissingFieldMessages);
                    }

                    practicedCroppingFieldsErrors.put(interventionCroppingPlanEntryCode, previousPlanEntryCode, rank, cropMissingFieldMessages);

                    // somme pour CC le cycle (ensemble des cultures du cycle)
                    List<TillageType> previous = practicedCroppingValues.get(interventionCroppingPlanEntryCode, previousPlanEntryCode, rank, cropConnection);
                    if (previous == null) {
                        final List<TillageType> valuesForCrop = new LinkedList<>();
                        valuesForCrop.add(interValues);

                        practicedCroppingValues.put(cropCode, previousPlanEntryCode, rank, cropConnection, valuesForCrop);// ici
                        if (!interventionContext.isFictive() && intervention.isIntermediateCrop()) {
                            cropsYealdAverage.put(interventionCroppingPlanEntryCode, cropContext.getIntermediateCropYealds());
                        } else {
                            cropsYealdAverage.put(interventionCroppingPlanEntryCode, cropContext.getMainCropYealds());
                        }
                    } else {
                        previous.add(interValues);
                    }

                    // somme pour CC
                    final Integer prevConnectionReliabilityTotalCount = practicedCroppingReliabilityTotalCounter.get(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection);

                    final int totalFieldCounterValueForCrop =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevConnectionReliabilityTotalCount, 0);

                    practicedCroppingReliabilityTotalCounter.put(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            totalFieldCounterValueForCrop);

                    final Integer prevConnectionReliabilityErrorCounter = practicedCroppingReliabilityFieldErrorCounter.get(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection);

                    final int connectionMissingFieldCounter =
                            missingFieldCounterValueForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevConnectionReliabilityErrorCounter, 0);

                    practicedCroppingReliabilityFieldErrorCounter.put(
                            interventionCroppingPlanEntryCode,
                            previousPlanEntryCode,
                            rank,
                            cropConnection,
                            connectionMissingFieldCounter);

                    // practicedSystem scale
                    Integer previousPracticedSystemRTC = reliabilityIndexPracticedSystemValuesTotalCounter.get(practicedSystemScaleKey);

                    final int totalFieldCounterValueForPracticedSystem =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(previousPracticedSystemRTC, 0);

                    reliabilityIndexPracticedSystemValuesTotalCounter.put(practicedSystemScaleKey, totalFieldCounterValueForPracticedSystem);

                    final Integer previousPracticedSystemREC = reliabilityIndexPracticedSystemValuesErrorCounter.get(practicedSystemScaleKey);

                    final int missingFieldCounterValueForGS = missingFieldCounterValueForIntervention.get() +
                            ObjectUtils.firstNonNull(previousPracticedSystemREC, 0);

                    reliabilityIndexPracticedSystemValuesErrorCounter.put(practicedSystemScaleKey, missingFieldCounterValueForGS);

                    Set<MissingFieldMessage> messagesAtPracticedScope = practicedSystemFieldsErrors.computeIfAbsent(practicedSystemScaleKey, k -> new HashSet<>());

                    // trow up to practiced system the missing fields warning that must be displayed at Practiced System level
                    Set<MissingFieldMessage> practicedMissingFieldMessages = interventionWarnings == null ?
                            new HashSet<>() :
                            interventionWarnings.stream().
                                    filter(missingFieldMessage ->
                                            missingFieldMessage.getMessageForScope(MissingMessageScope.PRACTICED_SYSTEM).isPresent()).
                                    collect(Collectors.toSet());
                    if (CollectionUtils.isNotEmpty(practicedMissingFieldMessages)) {
                        messagesAtPracticedScope.addAll(practicedMissingFieldMessages);
                    }

                }
            } // end of intervention

            cumulativeFrequenciesByPracticedCropCycleConnectionCode.put(
                    crop,
                    previousCrop,
                    rank,
                    cropConnection,
                    cropContext.getCummulativeFrequencyForCrop());

            if (intermediateCrop != null) {
                cumulativeFrequenciesByPracticedCropCycleConnectionCode.put(
                        intermediateCrop,
                        previousCrop,
                        rank,
                        cropConnection,
                        cropContext.getCummulativeFrequencyForCrop());
            }

            List<Map.Entry<MultiKey<?>, List<TillageType>>> practicedSeasonalValuesByCrops = practicedCroppingValues.entrySet().stream()
                    .filter(entry -> {
                        final Object previousPlanEntryCodeOrPhase = entry.getKey().getKey(1);
                        return !(previousPlanEntryCodeOrPhase instanceof PracticedCropCyclePhase);
                    })
                    .sorted(
                            Comparator.comparingInt(entry -> (Integer) entry.getKey().getKey(2))
                    )
                    .toList();

            List<Map.Entry<MultiKey<?>, TillageType>> practicedSeasonalCroppingValues = new LinkedList<>();

            // Calcul de la valeur de l'indicateur à l'échelle de la culture
            for (Map.Entry<MultiKey<?>, List<TillageType>> entry : practicedSeasonalValuesByCrops) {

                TillageType cropValues = getTillageTypeAtCropScale(entry.getValue());

                if (cropValues == null) continue;

                practicedSeasonalCroppingValues.add(Map.entry(entry.getKey(), cropValues));

                practicedSystemCycleValues.add(cropValues);
            }

            PracticedSystem anonymizePracticedSystem = practicedSystemExecutionContext.getAnonymizePracticedSystem();

            writePracticedSeasonalCroppingValues(
                    writer,
                    domainContext,
                    practicedCroppingReliabilityFieldErrorCounter,
                    practicedCroppingReliabilityTotalCounter,
                    cropsYealdAverage,
                    cumulativeFrequenciesByPracticedCropCycleConnectionCode,
                    practicedSystemExecutionContext,
                    practicedCroppingFieldsErrors,
                    anonymizeGrowingSystem,
                    its,
                    irs,
                    anonymizePracticedSystem,
                    practicedSeasonalCroppingValues);

        }// end of cycle

        // mise à l'échelle vers Practiced System
        final List<TillageType> tillageTypes = practicedSystemCycleValues
                .stream()
                .filter(tt -> tt != TillageType.AUCUN)
                .toList();

        final double interventionsWithTillageCount = tillageTypes.size();
        // les valeurs LABOUR_SYSTEMATIQUE, LABOUR_FREQUENT, LABOUR_OCCASIONNEL ne sont utilisées qu'à l'échelle système de culture et pas avant,
        final double ploughingPercentage = interventionsWithTillageCount == 0 ? 0 : tillageTypes.stream().filter(tt -> tt == TillageType.LABOUR).count() / interventionsWithTillageCount;

        if (interventionsWithTillageCount > 0) {
            TillageType sdcValue;

            if (ploughingPercentage == 1.0) {
                sdcValue = TillageType.LABOUR_SYSTEMATIQUE;
            } else if (ploughingPercentage > 0.66) {
                sdcValue = TillageType.LABOUR_FREQUENT;
            } else if (ploughingPercentage > 0) {
                sdcValue = TillageType.LABOUR_OCCASIONNEL;
            } else {
                // Dans ce cas, il n'y a aucun LABOUR. Donc il y a soit TCS, soit SEMIS_DIRECT
                // comme valeurs possibles. S'il y a au moins 1 TCS alors TCS, sinon c'est SEMIS_DIRECT
                if (tillageTypes.stream().anyMatch(tt -> tt == TillageType.TCS)) {
                    sdcValue = TillageType.TCS;
                } else {
                    sdcValue = TillageType.SEMIS_DIRECT;
                }
            }

            practicedSystemsValues.put(practicedSystemScaleKey, sdcValue);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getSimpleName() + ": " + (System.currentTimeMillis() - chronoT0) + " ms");
        }

    }

    protected List<String> getReferencesDosagesUserInfos(PerformanceInterventionContext interventionContext) {
        return Collections.emptyList();
    }

    @Override
    public void computePracticed(
            IndicatorWriter writer,
            Domain domain) {
        // Méthode pour calcul à l'échelle du domaine, mais rien à faire car l'indicateur n'est pas calculé à l'échelle du domaine
    }

    /**
     * Interventions and Crop Scales = (Crop Cycles (CC))
     */
    @Override
    public void computeEffective(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        computeEffectivePerennial(writer, globalExecutionContext, domainContext, zoneContext);

        computeEffectiveSeasonal(writer, globalExecutionContext, domainContext, zoneContext);

        // la map effectiveZoneValues est valuée plusieurs fois avec plusieurs zones par plusieurs
        // appels pour la mise à l'échelle de la methode suivante
        // computeEffective(IndicatorWriter writer, Domain domain, GrowingSystem growingSystem, plot plot)
        // on ne génère une ligne dans le fichier de sortie que pour la zone courante (s'il y a des valeurs)
        // et non pour toutes les zones
        writeEffectiveZone(writer, domainContext, zoneContext);
    }

    protected void computeEffectivePerennial(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        List<EffectivePerennialCropCycle> perennialCycles = zoneContext.getPerennialCropCycles();

        if (CollectionUtils.isEmpty(perennialCycles)) {
            return;
        }

        Set<PerformanceEffectiveCropExecutionContext> performanceCropContextExecutionContexts = zoneContext.getPerformanceCropContextExecutionContexts()
                .stream()
                .filter(
                        performanceCropExecutionContext ->
                                !Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                ))
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(performanceCropContextExecutionContexts)) {
            return;// no crop in cycle
        }

        Optional<GrowingSystem> optionalAnoGrowingSystem = zoneContext.getAnonymizeGrowingSystem();
        Domain anonymizeDomaine = domainContext.getAnonymiseDomain();
        Plot anonymizePlot = zoneContext.getAnonymizePlot();
        Zone anonymizeZone = zoneContext.getAnonymizeZone();

        String its = zoneContext.getIts();
        String irs = zoneContext.getIrs();

        Map<EffectivePerennialCropCycle, TillageType> effectiveSystemCycleValues = new HashMap<>();
        for (PerformanceEffectiveCropExecutionContext cropContext : performanceCropContextExecutionContexts) {
            final Map<EffectiveCropCycleScaleKey, List<TillageType>> effectiveValuesByCrop = new HashMap<>();

            Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();

            CroppingPlanEntry crop = cropContext.getCrop();

            final Optional<EffectiveIntervention> mostRecentSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                    .map(PerformanceEffectiveInterventionExecutionContext::getIntervention)
                    .filter(intervention0 -> intervention0.getType() == AgrosystInterventionType.SEMIS)
                    .filter(intervention0 -> !intervention0.isIntermediateCrop())
                    .max((i1, i2) -> computeAverageDay(i1).compareTo(computeAverageDay(i2)));

            for (PerformanceEffectiveInterventionExecutionContext interventionContext : interventionContexts) {

                EffectiveIntervention intervention = interventionContext.getIntervention();

                if (intervention.getEffectiveCropCyclePhase() == null) {
                    continue;
                }

                String interventionId = intervention.getTopiaId();

                EffectiveCropCyclePhase phase = intervention.getEffectiveCropCyclePhase();

                TillageType interValues = manageIntervention(interventionContext, mostRecentSeedingIntervention);

                if (interValues != null) {

                    zoneContext.addCropConcernByIndicator(this.getClass(), crop);

                    Set<MissingFieldMessage> interventionWarnings;
                    Set<MissingFieldMessage> cropMissingFieldMessages;

                    Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomaine);

                    AbstractIndicator.InterventionFieldCounterGetter interventionFieldCounterGetter =
                            new AbstractIndicator.InterventionFieldCounterGetter(
                                    interventionId,
                                    inputUsages,
                                    interventionContext.getOptionalIrrigationAction(),
                                    interventionContext.getOptionalSeedingActionUsage())
                                    .invoke();

                    AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
                    AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

                    int reliabilityIndexForIntervention =
                            computeReliabilityIndex(
                                    missingFieldCounterValueForIntervention.get(),
                                    totalFieldCounterForIntervention.get());

                    if (this.isRelevant(ExportLevel.INTERVENTION)) {
                        final List<AbstractAction> actions = interventionContext.getActions();

                        final String reliabilityCommentForIntervention =
                                getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                        final List<String> referencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                        final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                        boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, 0) && !TillageType.SEMIS_DIRECT.equals(interValues) && !TillageType.AUCUN.equals(interValues);
                        if (isDisplayed) {
                            // write intervention sheet
                            writer.writeEffectivePerennialIntervention(
                                    its,
                                    irs,
                                    getIndicatorCategory(),
                                    getIndicatorLabel(0),
                                    interValues.name(),
                                    reliabilityIndexForIntervention,
                                    reliabilityCommentForIntervention,
                                    referencesDosagesUserInfos,
                                    anonymizeDomaine,
                                    optionalAnoGrowingSystem,
                                    anonymizePlot,
                                    anonymizeZone,
                                    crop,
                                    phase,
                                    intervention,
                                    actions,
                                    domainContext.getCodeAmmBioControle(),
                                    interventionYealdAverage,
                                    globalExecutionContext.getGroupesCiblesParCode(),
                                    this.getClass());
                        }

                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = interventionWarnings == null ?
                                new HashSet<>() :
                                interventionWarnings.stream()
                                        .filter(missingFieldMessage ->
                                                missingFieldMessage.getMessageForScope(MissingMessageScope.CROP)
                                                        .isPresent())
                                        .collect(Collectors.toSet());
                    } else {
                        // trow up to crop the missing fields warning
                        interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                        cropMissingFieldMessages = ObjectUtils.firstNonNull(interventionWarnings, new HashSet<>());
                    }


                    EffectiveCropCycleScaleKey effectivePerennialCropKey =
                            new EffectiveCropCycleScaleKey(
                                    crop,
                                    Optional.empty(),
                                    Optional.empty(),
                                    Optional.of(phase));

                    Set<MissingFieldMessage> missingFieldsForCrop = effectiveCroppingFieldsErrors
                            .computeIfAbsent(effectivePerennialCropKey, k -> new HashSet<>());

                    // trow up to crop the missing fields warning
                    if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                        missingFieldsForCrop.addAll(cropMissingFieldMessages);
                    }

                    List<TillageType> previous = effectiveValuesByCrop.get(effectivePerennialCropKey);
                    if (previous == null) {
                        final List<TillageType> valuesForCrop = new LinkedList<>();
                        valuesForCrop.add(interValues);
                        effectiveValuesByCrop.put(effectivePerennialCropKey, valuesForCrop);
                        if (intervention.isIntermediateCrop()) {
                            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getIntermediateCropYealds());
                        } else {
                            effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getMainCropYealds());
                        }
                    } else {
                        previous.add(interValues);
                    }

                    Integer prevRankReliabilityTotalCount = effectiveCroppingReliabilityTotalCounter.get(effectivePerennialCropKey);

                    final int totalFieldCounterValue =
                            totalFieldCounterForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevRankReliabilityTotalCount, 0);

                    effectiveCroppingReliabilityTotalCounter.put(effectivePerennialCropKey, totalFieldCounterValue);

                    final Integer prevMissingFieldCounter = effectiveCroppingReliabilityFieldErrorCounter.get(effectivePerennialCropKey);

                    final int missingFieldCounter =
                            missingFieldCounterValueForIntervention.get() +
                                    ObjectUtils.firstNonNull(prevMissingFieldCounter, 0);

                    effectiveCroppingReliabilityFieldErrorCounter.put(effectivePerennialCropKey, missingFieldCounter);

                    final int totalFieldCounterValueForCrop = effectiveCroppingReliabilityTotalCounter.get(effectivePerennialCropKey);
                    effectiveZoneReliabilityTotalCounter.merge(
                            anonymizeZone,
                            totalFieldCounterValueForCrop, Integer::sum);

                    final int phaseMissingFieldCounter = effectiveCroppingReliabilityFieldErrorCounter.get(effectivePerennialCropKey);
                    effectiveZoneReliabilityFieldErrorCounter.merge(
                            anonymizeZone,
                            phaseMissingFieldCounter,
                            Integer::sum);

                    Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings == null ?
                            new HashSet<>() :
                            interventionWarnings.stream().
                                    filter(
                                            missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.ZONE)
                                                    .isPresent())
                                    .collect(Collectors.toSet());

                    Set<MissingFieldMessage> missingFieldsForZone = effectiveZoneFieldsErrors.computeIfAbsent(anonymizeZone, k -> new HashSet<>());
                    missingFieldsForZone.addAll(zoneMissingFieldMessages);

                    EffectiveItkCropCycleScaleKey effectiveItkCropCycleScaleKey =
                            new EffectiveItkCropCycleScaleKey(
                                    optionalAnoGrowingSystem,
                                    anonymizePlot,
                                    anonymizeZone,
                                    crop,
                                    Optional.empty(),
                                    Optional.empty(),
                                    Optional.of(phase),
                                    Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                            );

                    effectiveItkCropPrevCropPhaseValues.merge(
                            effectiveItkCropCycleScaleKey,
                            interValues,
                            GenericIndicator::sum);

                    effectiveItkCropPrevCropPhaseTotalCounter.merge(
                            effectiveItkCropCycleScaleKey,
                            totalFieldCounterValueForCrop,
                            Integer::sum);

                    effectiveItkCropPrevCropPhaseFieldErrorCounter.merge(
                            effectiveItkCropCycleScaleKey,
                            phaseMissingFieldCounter,
                            Integer::sum);

                    // on ajoute ceux de la zone ?
                    Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropCycleScaleKey, k -> new HashSet<>());
                    missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);
                }
            } // fin des interventions


            List<Map.Entry<EffectiveCropCycleScaleKey, TillageType>> effectivePerennialCroppingValues = new LinkedList<>();
            for (Map.Entry<EffectiveCropCycleScaleKey, List<TillageType>> entry : effectiveValuesByCrop.entrySet()) {

                TillageType cropValues = getTillageTypeAtCropScale(entry.getValue());

                if (cropValues == null) continue;

                effectivePerennialCroppingValues.add(Map.entry(entry.getKey(), cropValues));

                final EffectivePerennialCropCycle cycle = cropContext.getPerennialCropCycle();
                TillageType psPreviousValue = effectiveSystemCycleValues.get(cycle);
                if (psPreviousValue == null) {
                    effectiveSystemCycleValues.put(cycle, cropValues);
                } else {
                    final TillageType sum = sum(psPreviousValue, cropValues);
                    effectiveSystemCycleValues.put(cycle, sum);
                }
            }

            writeEffectivePerennialCroppingValues(
                    writer,
                    domainContext.getAnonymiseDomain(),
                    domainContext.getGrowingSystemContextByGrowingSystems().get(zoneContext.getGrowingSystem()),
                    zoneContext.getPlot(),
                    its,
                    irs,
                    effectivePerennialCroppingValues);
        } // fin des cycles de culture

        // Mise à l'échelle vers la zone
        final Zone zone = zoneContext.getZone();
        for (Map.Entry<EffectivePerennialCropCycle, TillageType> entry : effectiveSystemCycleValues.entrySet()) {
            TillageType psPreviousValue = effectiveZoneValues.get(zone);

            if (psPreviousValue == null) {
                effectiveZoneValues.put(zone, entry.getValue());
            } else {
                final TillageType sum = sum(psPreviousValue, entry.getValue());
                effectiveZoneValues.put(zone, sum);
            }
        }
    }

    private static @Nullable TillageType getTillageTypeAtCropScale(List<TillageType> entry) {
        List<TillageType> typeList = entry;
        final List<TillageType> tillageTypes = typeList.stream().filter(tt -> tt != TillageType.AUCUN).toList();
        if (tillageTypes.isEmpty()) {
            return null;
        }
        final boolean isTCS = typeList.stream().allMatch(tt -> tt == TillageType.TCS);

        final double interventionCountWithTillage = tillageTypes.size();
        // les valeurs LABOUR_SYSTEMATIQUE, LABOUR_FREQUENT, LABOUR_OCCASIONNEL ne sont utilisées qu'à l'échelle système de culture et pas avant,
        final double interventionCountWithPloughing = tillageTypes.stream().filter(tt -> tt == TillageType.LABOUR).count();

        final double ploughingPercentage = interventionCountWithPloughing / interventionCountWithTillage;

        // À cette échelle l'indicateur ne peut prendre que trois valeurs qualitatives (Labour, TCS, Semis direct).
        // | Pourcentage d'intervention de type Labour | >0 | Labour                             |     |              |
        // |                                           | =0 | Date de semis avant l'intervention | OUI | TCS          |
        // |                                           |    |                                    | NON | Semis direct |

        // Pour résumer:
        // Il y a un ordre de priorité à respecter:
        // 1 -> s'il y a au moins 1 labour, alors on considère que c'est "Labour"
        // 2 -> sinon, s'il y a au moins 1 TCS, alors c'est TCS
        // 3 -> sinon si ce n'est que semis-direct, alors c'est "Semis-direct"

        TillageType cropValues;
        if (ploughingPercentage > 0) {
            cropValues = TillageType.LABOUR;
        } else if (isTCS) {
            cropValues = TillageType.TCS;
        } else {
            cropValues = TillageType.SEMIS_DIRECT;
        }
        return cropValues;
    }

    protected void computeEffectiveSeasonal(
            IndicatorWriter writer,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        EffectiveSeasonalCropCycle seasonalCropCycle = zoneContext.getSeasonalCropCycle();

        if (seasonalCropCycle == null) {
            return;
        }

        Set<PerformanceEffectiveCropExecutionContext> cropContexts_ = zoneContext.getPerformanceCropContextExecutionContexts()
                .stream()
                .filter(
                        performanceCropExecutionContext ->
                                Objects.isNull(performanceCropExecutionContext.getPerennialCropCycle()
                                ))
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(cropContexts_)) {
            return;// no crop in cycle
        }

        List<PerformanceEffectiveCropExecutionContext> cropContexts = cropContexts_
                .stream()
                .sorted(Comparator.comparingInt(PerformanceEffectiveCropExecutionContext::getRank)).toList();

        TillageType effectiveSystemCycleValues = null;
        for (PerformanceEffectiveCropExecutionContext cropContext : cropContexts) {
            final Map<EffectiveCropCycleScaleKey, List<TillageType>> effectiveValuesByCrop = new HashMap<>();

            Set<PerformanceEffectiveInterventionExecutionContext> interventionContexts = cropContext.getInterventionExecutionContexts();

            if (CollectionUtils.isEmpty(interventionContexts)) continue; // NOTHING TO DO

            final Optional<EffectiveIntervention> mostRecentSeedingIntervention = cropContext.getInterventionExecutionContexts().stream()
                    .map(PerformanceEffectiveInterventionExecutionContext::getIntervention)
                    .filter(intervention0 -> intervention0.getType() == AgrosystInterventionType.SEMIS)
                    .filter(intervention0 -> !intervention0.isIntermediateCrop())
                    .max((i1, i2) -> computeAverageDay(i1).compareTo(computeAverageDay(i2)));

            for (PerformanceEffectiveInterventionExecutionContext interventionContext : interventionContexts) {

                EffectiveIntervention intervention = interventionContext.getIntervention();

                if (intervention.getEffectiveCropCyclePhase() != null) {
                    continue;
                }

                computeEffectiveInterventionSheet(
                        writer,
                        globalExecutionContext,
                        domainContext,
                        zoneContext,
                        cropContext,
                        interventionContext,
                        effectiveValuesByCrop,
                        mostRecentSeedingIntervention);
            }

            List<Map.Entry<EffectiveCropCycleScaleKey, TillageType>> effectiveSaisonnalCroppingValues = new LinkedList<>();
            for (Map.Entry<EffectiveCropCycleScaleKey, List<TillageType>> entry : effectiveValuesByCrop.entrySet()) {

                TillageType cropValues = getTillageTypeAtCropScale(entry.getValue());

                if (cropValues == null) continue;

                effectiveSaisonnalCroppingValues.add(Map.entry(entry.getKey(), cropValues));

                TillageType psPreviousValue = effectiveSystemCycleValues;
                if (psPreviousValue == null) {
                    effectiveSystemCycleValues = cropValues;
                } else {
                    effectiveSystemCycleValues = sum(psPreviousValue, cropValues);
                }
            }

            writeEffectiveSeasonalCroppingValues(
                    writer,
                    domainContext.getAnonymiseDomain(),
                    domainContext.getGrowingSystemContextByGrowingSystems().get(zoneContext.getGrowingSystem()),
                    zoneContext.getPlot(),
                    zoneContext.getIts(),
                    zoneContext.getIrs(),
                    effectiveSaisonnalCroppingValues);
        }

        // Mise à l'échelle vers la zone
        final Zone zone = zoneContext.getZone();
        TillageType psPreviousValue = effectiveZoneValues.get(zone);

        if (effectiveSystemCycleValues != null) {
            if (psPreviousValue == null) {
                effectiveZoneValues.put(zone, effectiveSystemCycleValues);
            } else {
                final TillageType sum = sum(psPreviousValue, effectiveSystemCycleValues);
                effectiveZoneValues.put(zone, sum);
            }
        }
    }

    protected void computeEffectiveInterventionSheet(IndicatorWriter writer,
                                                                      PerformanceGlobalExecutionContext globalExecutionContext,
                                                                      PerformanceEffectiveDomainExecutionContext domainContext,
                                                                      PerformanceZoneExecutionContext zoneContext,
                                                                      PerformanceEffectiveCropExecutionContext cropContext,
                                                                      PerformanceEffectiveInterventionExecutionContext interventionContext,
                                                                      Map<EffectiveCropCycleScaleKey, List<TillageType>> effectiveValuesByCrop,
                                                                      Optional<EffectiveIntervention> firstSeedingIntervention) {
        Domain anonymizeDomaine = domainContext.getAnonymiseDomain();

        EffectiveIntervention intervention = interventionContext.getIntervention();

        if (intervention.getEffectiveCropCyclePhase() != null) {
            return;
        }

        Optional<GrowingSystem> optionalAnonGrowingSystem = zoneContext.getAnonymizeGrowingSystem();
        Plot anonymizePlot = zoneContext.getAnonymizePlot();
        Zone anonymizeZone = zoneContext.getAnonymizeZone();

        CroppingPlanEntry crop = cropContext.getCrop();
        CroppingPlanEntry previousCrop = cropContext.getSeasonalPreviousCrop();
        CroppingPlanEntry intermediateCrop = cropContext.getIntermediateCrop();
        int rank = cropContext.getRank();

        String its = zoneContext.getIts();
        String irs = zoneContext.getIrs();

        String interventionId = intervention.getTopiaId();

        TillageType interValues = manageIntervention(interventionContext, firstSeedingIntervention);

        if (interValues != null) {

            zoneContext.addCropConcernByIndicator(this.getClass(), crop);

            Set<MissingFieldMessage> interventionWarnings;
            Set<MissingFieldMessage> cropMissingFieldMessages;

            Collection<AbstractInputUsage> inputUsages = Indicator.getValidDomainInputUsages(interventionContext.getActions(), anonymizeDomaine);

            AbstractIndicator.InterventionFieldCounterGetter interventionFieldCounterGetter =
                    new AbstractIndicator.InterventionFieldCounterGetter(
                            interventionId,
                            inputUsages,
                            interventionContext.getOptionalIrrigationAction(),
                            interventionContext.getOptionalSeedingActionUsage())
                            .invoke();

            AtomicReference<Integer> totalFieldCounterForIntervention = interventionFieldCounterGetter.getTotalFieldCounterForIntervention();
            AtomicReference<Integer> missingFieldCounterValueForIntervention = interventionFieldCounterGetter.getMissingFieldCounterValueForIntervention();

            int reliabilityIndexForIntervention =
                    computeReliabilityIndex(
                            missingFieldCounterValueForIntervention.get(),
                            totalFieldCounterForIntervention.get());

            if (isRelevant(ExportLevel.INTERVENTION)) {

                final List<AbstractAction> actions = interventionContext.getActions();

                final String reliabilityCommentForIntervention =
                        getReliabilityCommentForTargetedId(interventionId, MissingMessageScope.INTERVENTION);

                final List<String> referencesDosagesUserInfos = getReferencesDosagesUserInfos(interventionContext);

                final Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage = interventionContext.getYealdAveragesByDestinations();

                boolean isDisplayed = isDisplayed(ExportLevel.INTERVENTION, 0) && !TillageType.SEMIS_DIRECT.equals(interValues) && !TillageType.AUCUN.equals(interValues);
                if (isDisplayed) {
                    // write intervention sheet
                    writer.writeEffectiveSeasonalIntervention(
                            its,
                            irs,
                            anonymizeDomaine.getCampaign(),
                            getIndicatorCategory(),
                            getIndicatorLabel(0),
                            interValues.name(),
                            reliabilityIndexForIntervention,
                            reliabilityCommentForIntervention,
                            referencesDosagesUserInfos,
                            anonymizeDomaine,
                            optionalAnonGrowingSystem,
                            anonymizePlot,
                            anonymizeZone,
                            crop,
                            intermediateCrop,
                            rank,
                            previousCrop,
                            intervention,
                            actions,
                            domainContext.getCodeAmmBioControle(),
                            interventionYealdAverage,
                            globalExecutionContext.getGroupesCiblesParCode(),
                            this.getClass());
                }

                interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                cropMissingFieldMessages = interventionWarnings == null ?
                        new HashSet<>() :
                        interventionWarnings.stream()
                                .filter(missingFieldMessage ->
                                        missingFieldMessage.getMessageForScope(MissingMessageScope.CROP)
                                                .isPresent())
                                .collect(Collectors.toSet());

            } else {
                // trow up to crop the missing fields warning
                interventionWarnings = targetedErrorFieldMessages.get(interventionId);
                cropMissingFieldMessages = ObjectUtils.firstNonNull(interventionWarnings, new HashSet<>());
            }

            EffectiveCropCycleScaleKey effectiveSeasonalCropKey =
                    new EffectiveCropCycleScaleKey(
                            interventionContext.getCroppingPlanEntry(),
                            Optional.ofNullable(previousCrop),
                            Optional.of(rank),
                            Optional.empty());

            Set<MissingFieldMessage> missingFieldsForCrop = effectiveCroppingFieldsErrors
                    .computeIfAbsent(effectiveSeasonalCropKey, k -> new HashSet<>());

            // trow up to crop the missing fields warning
            if (CollectionUtils.isNotEmpty(cropMissingFieldMessages)) {
                missingFieldsForCrop.addAll(cropMissingFieldMessages);
            }

            List<TillageType> previous = effectiveValuesByCrop.get(effectiveSeasonalCropKey);
            if (previous == null) {
                final List<TillageType> valuesForCrop = new LinkedList<>();
                valuesForCrop.add(interValues);
                effectiveValuesByCrop.put(effectiveSeasonalCropKey, valuesForCrop);
                if (intervention.isIntermediateCrop()) {
                    effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getIntermediateCropYealds());
                } else {
                    effectiveCropsYealdAverage.computeIfAbsent(crop.getCode(), k -> cropContext.getMainCropYealds());
                }
            } else {
                previous.add(interValues);
            }

            Integer prevRankReliabilityTotalCount = effectiveCroppingReliabilityTotalCounter.get(effectiveSeasonalCropKey);

            final int totalFieldCounterValue =
                    totalFieldCounterForIntervention.get() +
                            ObjectUtils.firstNonNull(prevRankReliabilityTotalCount, 0);

            effectiveCroppingReliabilityTotalCounter.put(effectiveSeasonalCropKey, totalFieldCounterValue);

            final Integer prevMissingFieldCounter = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveSeasonalCropKey);

            final int missingFieldCounter =
                    missingFieldCounterValueForIntervention.get() +
                            ObjectUtils.firstNonNull(prevMissingFieldCounter, 0);

            effectiveCroppingReliabilityFieldErrorCounter.put(effectiveSeasonalCropKey, missingFieldCounter);

            final int totalFieldCounterValueForCrop = effectiveCroppingReliabilityTotalCounter.get(effectiveSeasonalCropKey);
            effectiveZoneReliabilityTotalCounter.merge(
                    anonymizeZone,
                    totalFieldCounterValueForCrop, Integer::sum);

            final int rankMissingFieldCounter = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveSeasonalCropKey);
            effectiveZoneReliabilityFieldErrorCounter.merge(
                    anonymizeZone,
                    rankMissingFieldCounter,
                    Integer::sum);

            Set<MissingFieldMessage> zoneMissingFieldMessages = interventionWarnings == null ?
                    new HashSet<>() :
                    interventionWarnings.stream().
                            filter(
                                    missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.ZONE).isPresent())
                            .collect(Collectors.toSet());

            Set<MissingFieldMessage> missingFieldsForZone = effectiveZoneFieldsErrors.computeIfAbsent(anonymizeZone, k -> new HashSet<>());
            missingFieldsForZone.addAll(zoneMissingFieldMessages);

            EffectiveItkCropCycleScaleKey effectiveItkCropCycleScaleKey =
                    new EffectiveItkCropCycleScaleKey(
                            optionalAnonGrowingSystem,
                            anonymizePlot,
                            anonymizeZone,
                            interventionContext.getCroppingPlanEntry(),
                            Optional.ofNullable(previousCrop),
                            Optional.of(rank),
                            Optional.empty(),
                            Optional.ofNullable(intervention.getEffectiveCropCycleNode())
                    );

            effectiveItkCropPrevCropPhaseValues.merge(
                    effectiveItkCropCycleScaleKey,
                    interValues,
                    GenericIndicator::sum);

            effectiveItkCropPrevCropPhaseTotalCounter.merge(
                    effectiveItkCropCycleScaleKey,
                    totalFieldCounterValueForCrop,
                    Integer::sum);

            effectiveItkCropPrevCropPhaseFieldErrorCounter.merge(
                    effectiveItkCropCycleScaleKey,
                    rankMissingFieldCounter,
                    Integer::sum);

            // on ajoute ceux de la zone ?
            Set<MissingFieldMessage> missingFieldsForItkCropPrevCropPhase = effectiveItkCropPrevCropPhaseFieldsErrors.computeIfAbsent(effectiveItkCropCycleScaleKey, k -> new HashSet<>());
            missingFieldsForItkCropPrevCropPhase.addAll(zoneMissingFieldMessages);
        }
    }

    // write result at crop scale
    @Override
    public void computeEffectiveCC(
            IndicatorWriter writer,
            Domain domain,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            Plot plot) {
        // fait dans les méthodes parentes
    }

    // Calcul à l'échelle de la parcelle
    @Override
    public void computeEffective(
            IndicatorWriter writer,
            PerformanceEffectiveDomainExecutionContext domainContext,
            Optional<GrowingSystem> optionalGrowingSystem,
            PerformancePlotExecutionContext plotContext) {

        Plot anonymisePlot = plotContext.getAnonymizePlot();

        TillageType weightedValueSum = null;

        int reliabilityPlotTotal = 0;
        int reliabilityPlotMissingFileld = 0;
        Set<MissingFieldMessage> plotMissingFieldMessages = new HashSet<>();

        Set<String> zoneTopiaIds = new HashSet<>();

        for (Map.Entry<Zone, TillageType> entry : effectiveZoneValues.entrySet()) {
            Zone zone = entry.getKey();
            zoneTopiaIds.add(zone.getTopiaId());
            TillageType values = entry.getValue();

            reliabilityPlotTotal += Optional.ofNullable(effectiveZoneReliabilityTotalCounter.get(zone)).orElse(0);
            reliabilityPlotMissingFileld += Optional.ofNullable(effectiveZoneReliabilityFieldErrorCounter.get(zone)).orElse(0);
            final Set<MissingFieldMessage> fieldMessages = effectiveZoneFieldsErrors.get(zone);
            Set<MissingFieldMessage> zoneMissingFieldMessages = fieldMessages != null ?
                    fieldMessages.stream().
                            filter(
                                    missingFieldMessage -> missingFieldMessage.getMessageForScope(MissingMessageScope.PLOT).isPresent())
                            .collect(Collectors.toSet()) :
                    new HashSet<>();

            plotMissingFieldMessages.addAll(zoneMissingFieldMessages);

            if (weightedValueSum == null) {
                weightedValueSum = TillageType.AUCUN;
            }

            // zoneArea
            reliabilityPlotTotal += 1;
            weightedValueSum = sum(weightedValueSum, values);
        }

        if (weightedValueSum != null) {
            String zoneIds = String.join(", ", zoneTopiaIds);
            final Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages = plotContext.getPlotYealdAverages();

            String its = plotContext.getIts();
            String irs = plotContext.getIrs();

            Integer plotReliability = computeReliabilityIndex(reliabilityPlotMissingFileld, reliabilityPlotTotal);
            effectivePlotReliabilityFieldErrorCounter.put(anonymisePlot, reliabilityPlotMissingFileld);
            effectivePlotReliabilityTotalCounter.put(anonymisePlot, reliabilityPlotTotal);
            
            String comments;
            if (CollectionUtils.isNotEmpty(plotMissingFieldMessages)) {
                Set<String> errors = plotMissingFieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            } else {
                comments = RELIABILITY_INDEX_NO_COMMENT;
            }

            TillageType plotValue = weightedValueSum;
            boolean isDisplayed = isDisplayed(ExportLevel.PLOT, 0) && !TillageType.AUCUN.equals(plotValue);
            if (isDisplayed) {
                // write plot sheet
                writer.writeEffectivePlot(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        plotValue.name(),
                        plotReliability,
                        comments,
                        domainContext.getAnonymiseDomain(),
                        domainContext.getCroppingPlanSpecies(),
                        optionalGrowingSystem,
                        anonymisePlot,
                        plotYealdAverages,
                        this.getClass(),
                        zoneIds);
            }

            effectivePlotValues.put(anonymisePlot, plotValue);

        }
    }

    // Calcul à l'échelle du système de culture
    @Override
    public void computeEffective(
            IndicatorWriter writer,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {
        Integer reliabilityMissingFiledCounter = 0;
        Integer reliabilityTotalCounter = 1;

        Domain anonymiseDomain = domainContext.getAnonymiseDomain();
        Collection<CroppingPlanSpecies> domainAgrosystSpecies = domainContext.getCroppingPlanSpecies();

        final List<TillageType> tillageTypes = effectivePlotValues.values()
                .stream()
                .filter(tt -> tt != TillageType.AUCUN)
                .toList();

        final double interventionsWithTillageCount = tillageTypes.size();
        // les valeurs LABOUR_SYSTEMATIQUE, LABOUR_FREQUENT, LABOUR_OCCASIONNEL ne sont utilisées qu'à l'échelle système de culture et pas avant,
        final double ploughingPercentage = interventionsWithTillageCount == 0 ? 0 : tillageTypes.stream().filter(tt -> tt == TillageType.LABOUR).count() / interventionsWithTillageCount;

        Optional<GrowingSystem> anonymizeGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();

        if (interventionsWithTillageCount > 0 && anonymizeGrowingSystem.isPresent()) {
            TillageType weightedValueSum;

            //|Pourcentage d'intervention de type *Labour*| =1        | Labour systématique                |     |              |
            //|                                           | >0.66     | Labour fréquent                    |     |              |
            //|                                           | 0.66>%>0  | Labour occasionnel                 |     |              |
            //|                                           | =0        | Date de semis avant l'intervention | OUI | TCS          |
            //|                                           |           |                                    | NON | Semis direct |
            if (ploughingPercentage == 1.0) {
                weightedValueSum = TillageType.LABOUR_SYSTEMATIQUE;
            } else if (ploughingPercentage > 0.66) {
                weightedValueSum = TillageType.LABOUR_FREQUENT;
            } else if (ploughingPercentage > 0) {
                weightedValueSum = TillageType.LABOUR_OCCASIONNEL;
            } else {
                // Dans ce cas, il n'y a aucun LABOUR. Donc il y a soit TCS, soit SEMIS_DIRECT
                // comme valeurs possibles. S'il y a au moins 1 TCS alors TCS, sinon c'est SEMIS_DIRECT
                if (tillageTypes.stream().anyMatch(tt -> tt == TillageType.TCS)) {
                    weightedValueSum = TillageType.TCS;
                } else {
                    weightedValueSum = TillageType.SEMIS_DIRECT;
                }
            }

            String its = growingSystemContext.getIts();
            String irs = growingSystemContext.getIrs();

            Integer reliability = computeReliabilityIndex(reliabilityMissingFiledCounter, reliabilityTotalCounter);

            effectiveGrowingSystemValues.put(anonymizeGrowingSystem, TillageType.AUCUN);

            effectiveGrowingSystemReliabilityFieldErrorCounter.put(anonymizeGrowingSystem, reliabilityMissingFiledCounter);
            effectiveGrowingSystemReliabilityTotalCounter.put(anonymizeGrowingSystem, reliabilityTotalCounter);
            String comments = getMessagesForScope(MissingMessageScope.GROWING_SYSTEM);


            TillageType plotValue = weightedValueSum;
            boolean isDisplayed = isDisplayed(ExportLevel.GROWING_SYSTEM, 0);
            if (isDisplayed) {
                // write SDC sheet
                writer.writeEffectiveGrowingSystem(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        plotValue.name(),
                        reliability,
                        comments,
                        anonymiseDomain,
                        anonymizeGrowingSystem,
                        this.getClass(),
                        domainAgrosystSpecies);
            }
            effectiveGrowingSystemValues.put(anonymizeGrowingSystem, plotValue);

        }
    }


    @Override
    public void computeEffective(
            IndicatorWriter writer,
            Domain domain) {
        // Méthode pour calcul à l'échelle du domaine, mais rien à faire car l'indicateur n'est pas calculé à l'échelle du domaine
    }


    //
    // Méthodes pour écrire les données
    //

    private void writePracticedPerennialCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
            Map<CroppingPlanEntry, Double> perennialCropCyclePercentByCrop,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors,
            GrowingSystem anonymizeGrowingSystem,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            List<Map.Entry<MultiKey<?>, TillageType>> practicedPerennialCroppingValues) {

        for (Map.Entry<MultiKey<?>, TillageType> entry : practicedPerennialCroppingValues) {

            TillageType values = entry.getValue();

            if (values == null) continue;

            // perennial: cropCode, phase
            MultiKey<?> practicedCropingKey = entry.getKey();
            String croppingPlanEntryCode = (String) practicedCropingKey.getKey(0);
            PracticedCropCyclePhase previousPlanEntryCodeOrPhase = (PracticedCropCyclePhase) practicedCropingKey.getKey(1);

            final int campaign = domainContext.getDomain().getCampaign();
            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode().get(campaign, croppingPlanEntryCode);
            if (croppingPlanEntry == null) {
                final CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }
            final Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = cropsYealdAverage.get(croppingPlanEntryCode);

            final String campaigns = anonymizePracticedSystem.getCampaigns();

            Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCropingKey);
            Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCropingKey);

            Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);

            Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCropingKey);
            String comments = CollectionUtils.isEmpty(missingFields) ?
                    RELIABILITY_INDEX_NO_COMMENT :
                    missingFields.stream().
                            map(MissingFieldMessage::getMessage).
                            collect(Collectors.joining(", "));

            Double perennialCropCyclePercent = perennialCropCyclePercentByCrop.get(croppingPlanEntry);

            boolean isDisplayed = isDisplayed(ExportLevel.CROP, 0) && !TillageType.AUCUN.equals(values);
            if (isDisplayed) {
                // write crop sheet
                writer.writePracticedPerennialCop(
                        its,
                        irs,
                        campaigns,
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        values.name(),
                        cropYealdAverage,
                        reliabilityIndexForCrop,
                        comments,
                        domainContext.getAnonymiseDomain(),
                        anonymizeGrowingSystem,
                        anonymizePracticedSystem,
                        croppingPlanEntry,
                        previousPlanEntryCodeOrPhase,
                        perennialCropCyclePercent,
                        this.getClass());
            }

        }
    }

    private void writePracticedSeasonalCroppingValues(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityFieldErrorCounter,
            MultiKeyMap<Object, Integer> practicedCroppingReliabilityTotalCounter,
            Map<String, Map<Pair<RefDestination, YealdUnit>, Double>> cropsYealdAverage,
            MultiKeyMap<Object, Double> cumulativeFrequenciesByPracticedCropCycleConnectionCode,
            PerformancePracticedSystemExecutionContext practicedSystemExecutionContext,
            MultiKeyMap<Object, Set<MissingFieldMessage>> practicedCroppingFieldsErrors,
            GrowingSystem anonymizeGrowingSystem,
            String its,
            String irs,
            PracticedSystem anonymizePracticedSystem,
            List<Map.Entry<MultiKey<?>, TillageType>> practicedSeasonalCroppingValues) {

        for (Map.Entry<MultiKey<?>, TillageType> entry : practicedSeasonalCroppingValues) {

            TillageType values = entry.getValue();

            if (values == null) continue;

            // seasonal:  cropCode, previousPlanEntryCode, rank, cropConnection
            MultiKey<?> practicedCropingKey = entry.getKey();
            String croppingPlanEntryCode = (String) practicedCropingKey.getKey(0);
            Object previousPlanEntryCode = practicedCropingKey.getKey(1);
            int rank = (Integer) practicedCropingKey.getKey(2);
            PracticedCropCycleConnection cropConnection = (PracticedCropCycleConnection) practicedCropingKey.getKey(3);

            final int campaign = domainContext.getDomain().getCampaign();
            CroppingPlanEntry croppingPlanEntry = domainContext.getCropByCampaignAndCode().get(campaign, croppingPlanEntryCode);
            if (croppingPlanEntry == null) {
                final CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(croppingPlanEntryCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }
            final Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = cropsYealdAverage.get(croppingPlanEntryCode);

            final String campaigns = anonymizePracticedSystem.getCampaigns();
            CroppingPlanEntry previousCroppingPlanEntry = domainContext.getCropByCampaignAndCode().get(campaign, previousPlanEntryCode);

            if (previousCroppingPlanEntry == null) {
                final String cropCode = (String) previousPlanEntryCode;
                final CropWithSpecies cropWithSpecies = practicedSystemExecutionContext.getCropByCodeWithSpecies().get(cropCode);
                croppingPlanEntry = cropWithSpecies != null ? cropWithSpecies.getCroppingPlanEntry() : null;
            }

            Integer reliabilityErrorForCrop = practicedCroppingReliabilityFieldErrorCounter.get(practicedCropingKey);
            Integer totalFieldCounter = practicedCroppingReliabilityTotalCounter.get(practicedCropingKey);

            Integer reliabilityIndexForCrop = computeReliabilityIndex(reliabilityErrorForCrop, totalFieldCounter);

            Double cummulativeFrequencies = cumulativeFrequenciesByPracticedCropCycleConnectionCode.get(croppingPlanEntry,
                    previousCroppingPlanEntry,
                    rank,
                    cropConnection);
            cummulativeFrequencies = cummulativeFrequencies == null ? 0 : cummulativeFrequencies;

            Set<MissingFieldMessage> missingFields = practicedCroppingFieldsErrors.get(practicedCropingKey);
            Set<String> errors = missingFields != null ? missingFields.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet()) : new HashSet<>();

            String reliabilityComments = CollectionUtils.isEmpty(missingFields) ? RELIABILITY_INDEX_NO_COMMENT :
                    String.join(", ", errors);

            boolean isDisplayed = isDisplayed(ExportLevel.CROP, 0) && !TillageType.AUCUN.equals(values);
            if (isDisplayed) {
                // write crop sheet
                writer.writePracticedSeasonalCrop(
                        its,
                        irs,
                        campaigns,
                        cummulativeFrequencies,
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        values.name(),
                        cropYealdAverage,
                        reliabilityIndexForCrop,
                        reliabilityComments,
                        rank,
                        anonymizeGrowingSystem.getGrowingPlan().getDomain(),
                        anonymizeGrowingSystem,
                        anonymizePracticedSystem,
                        croppingPlanEntry,
                        previousCroppingPlanEntry,
                        cropConnection.getTopiaId(),
                        this.getClass(),
                        cropConnection);
            }

        }
    }

    protected void writePracticedSystemSheet(
            IndicatorWriter writer,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext) {

        Domain anonymizeDomain = domainContext.getAnonymiseDomain();
        final Optional<GrowingSystem> optionalGrowingSystem = growingSystemContext.getAnonymizeGrowingSystem();
        if (optionalGrowingSystem.isEmpty()) return;
        GrowingSystem anonymizeGrowingSystem = optionalGrowingSystem.get();
        String its = growingSystemContext.getIts();
        String irs = growingSystemContext.getIrs();

        for (Map.Entry<PracticedSystemScaleKey, TillageType> entry : practicedSystemsValues.entrySet()) {

            TillageType values = entry.getValue();

            if (values == null) continue;

            PracticedSystemScaleKey key = entry.getKey();
            String campaigns = key.campaigns();
            GrowingSystem anonymizeGrowingSystem0 = key.growingSystem();
            PracticedSystem anonymizePracticedSystem = key.practicedSystem();

            if (!anonymizeGrowingSystem0.getCode().contentEquals(anonymizeGrowingSystem.getCode())) {
                continue;// filter on practiced system related to the current growing system
            }

            Integer psREC = Optional.ofNullable(reliabilityIndexPracticedSystemValuesErrorCounter.get(key)).orElse(0);
            Integer psRTC = Optional.ofNullable(reliabilityIndexPracticedSystemValuesTotalCounter.get(key)).orElse(0);
            Set<MissingFieldMessage> fieldsErrorMessages = practicedSystemFieldsErrors.get(key);

            Integer reliability = computeReliabilityIndex(psREC, psRTC);

            String messages;
            if (CollectionUtils.isEmpty(fieldsErrorMessages)) {
                messages = RELIABILITY_INDEX_NO_COMMENT;
            } else {
                messages = StringUtils.join(fieldsErrorMessages, ", ");
            }

            // CA Practice System scale
            boolean isDisplayed = isDisplayed(ExportLevel.PRACTICED_SYSTEM, 0) && !TillageType.AUCUN.equals(values);
            if (isDisplayed) {
                // write practiced system sheet
                writer.writePracticedSystem(
                        its,
                        irs,
                        campaigns,
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        values.name(),
                        reliability,
                        messages,
                        anonymizeDomain,
                        anonymizeGrowingSystem,
                        anonymizePracticedSystem,
                        this.getClass());
            }

        }
    }

    protected void writeEffectiveZone(
            IndicatorWriter writer,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext) {

        // Échelles, zones et parcelle : L'agrégation se fait comme cela :
        //
        //    pour une zone, on regarde tous les résultats de l'indicateur au niveau culture.
        //    S'il y a au moins un Labour,
        //    alors au niveau de la zone, on retient la valeur Labour.
        //    Sinon, s'il y a au moins # TCS on retient TCS,
        //    sinon on est dans un cas qui est forcément Semis Direct.
        //    C'est en quelque sorte un ordre de priorité : Labour est prioritaire sur TCS qui est lui-même prioritaire sur Semis Direct.
        //    C'est ce qui est fait dans la méthode TillageType sum(TillageType arr1, TillageType arr2) dans GenericIndicator.
        //    Et on procède de la même manière pour le niveau parcelle, on récupère toutes les valeurs au niveau des zones de la culture et on agrège de la même façon.
        //    À l'échelle du système de culture : la description est OK sur la façon dont le calcul est fait, à part sur un détail :
        //    on récupère les valeurs de l'échelle inférieure (donc culture ou parcelle selon le cas synthétisé ou réalisé).
        //    À noter : il n'y a pas d'échelle plus petite que l'intervention, il n'y a pas d'échelle au-dessus du système de culture.

        Zone anonymizeZone = zoneContext.getAnonymizeZone();
        TillageType values = effectiveZoneValues.get(anonymizeZone);
        if (values != null) {
            Integer rmfzc = Optional.ofNullable(effectiveZoneReliabilityFieldErrorCounter.get(anonymizeZone)).orElse(0);
            Integer rtzc = Optional.ofNullable(effectiveZoneReliabilityTotalCounter.get(anonymizeZone)).orElse(0);
            Integer reliabilityIndex = computeReliabilityIndex(rmfzc, rtzc);

            Optional<GrowingSystem> optionalAnoGrowingSystem = zoneContext.getAnonymizeGrowingSystem();

            String speciesName = zoneContext.getZoneSpeciesNames();
            String varietyNames = zoneContext.getZoneVarietiesNames();

            String its = zoneContext.getIts();
            String irs = zoneContext.getIrs();

            final Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald = zoneContext.getZoneAverageYeald();

            final Plot plot = anonymizeZone.getPlot();
            // zone scale

            String comments;
            Set<MissingFieldMessage> fieldMessages = effectiveZoneFieldsErrors.get(anonymizeZone);
            if (CollectionUtils.isNotEmpty(fieldMessages)) {
                Set<String> errors = fieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            } else {
                comments = RELIABILITY_INDEX_NO_COMMENT;
            }

            boolean isDisplayed = isDisplayed(ExportLevel.ZONE, 0) && !TillageType.AUCUN.equals(values);
            if (isDisplayed) {
                // write zone sheet
                writer.writeEffectiveZone(
                        its,
                        irs,
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        values.name(),
                        zoneAverageYeald,
                        reliabilityIndex,
                        comments,
                        domainContext.getAnonymiseDomain(),
                        domainContext.getCroppingPlanSpecies(),
                        optionalAnoGrowingSystem,
                        plot,
                        anonymizeZone,
                        speciesName,
                        varietyNames,
                        this.getClass());
            }

            for (Map.Entry<EffectiveItkCropCycleScaleKey, TillageType> effectiveItkCropPrevCropPhaseKeyEntry : effectiveItkCropPrevCropPhaseValues.entrySet()) {

                EffectiveItkCropCycleScaleKey key = effectiveItkCropPrevCropPhaseKeyEntry.getKey();
                TillageType itkValues = effectiveItkCropPrevCropPhaseKeyEntry.getValue();
                Integer effectiveItkCropPrevCropPhaseTotalCount = effectiveItkCropPrevCropPhaseTotalCounter.get(key);
                Integer effectiveItkCropPrevCropPhaseFieldErrorCount = effectiveItkCropPrevCropPhaseFieldErrorCounter.get(key);
                Integer effectiveItkCropPrevCropPhaseReliabilityIndex = computeReliabilityIndex(effectiveItkCropPrevCropPhaseFieldErrorCount, effectiveItkCropPrevCropPhaseTotalCount);

                String effectiveItkCropPrevCropPhaseComments;
                Set<MissingFieldMessage> effectiveItkCropPrevCropPhaseFieldMessages = effectiveItkCropPrevCropPhaseFieldsErrors.get(key);
                if (CollectionUtils.isNotEmpty(effectiveItkCropPrevCropPhaseFieldMessages)) {
                    Set<String> errors = effectiveItkCropPrevCropPhaseFieldMessages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                    effectiveItkCropPrevCropPhaseComments = String.join(IndicatorWriter.SEPARATOR, errors);
                } else {
                    effectiveItkCropPrevCropPhaseComments = RELIABILITY_INDEX_NO_COMMENT;
                }

                boolean zoneIsDisplayed = isDisplayed(ExportLevel.ZONE, 0) && !TillageType.AUCUN.equals(itkValues);
                if (zoneIsDisplayed) {
                    // write zone sheet
                    writer.writeEffectiveItk(
                            its,
                            irs,
                            getIndicatorCategory(),
                            getIndicatorLabel(0),
                            itkValues.name(),
                            effectiveItkCropPrevCropPhaseReliabilityIndex,
                            effectiveItkCropPrevCropPhaseComments,
                            domainContext.getAnonymiseDomain(),
                            optionalAnoGrowingSystem,
                            plot,
                            anonymizeZone,
                            this.getClass(),
                            key);
                }

            }
        }

        effectiveItkCropPrevCropPhaseValues.clear();
        effectiveItkCropPrevCropPhaseTotalCounter.clear();
        effectiveItkCropPrevCropPhaseFieldErrorCounter.clear();
        effectiveItkCropPrevCropPhaseFieldsErrors.clear();
    }

    private void writeEffectivePerennialCroppingValues(
            IndicatorWriter writer,
            Domain domain,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            Plot plot,
            String its,
            String irs,
            List<Map.Entry<EffectiveCropCycleScaleKey, TillageType>> perennialEffectiveCroppingValues) {
        for (Map.Entry<EffectiveCropCycleScaleKey, TillageType> entry : perennialEffectiveCroppingValues) {

            TillageType values = entry.getValue();
            EffectiveCropCycleScaleKey effectiveCroppingKey = entry.getKey();

            CroppingPlanEntry crop = effectiveCroppingKey.crop();
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = effectiveCropsYealdAverage.get(crop.getCode());

            EffectiveCropCyclePhase phase = effectiveCroppingKey.phase().orElse(null);

            Integer riec = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCroppingKey);
            Integer ritc = effectiveCroppingReliabilityTotalCounter.get(effectiveCroppingKey);

            Integer reliability = computeReliabilityIndex(riec, ritc);

            String comments = RELIABILITY_INDEX_NO_COMMENT;
            Set<MissingFieldMessage> messages = effectiveCroppingFieldsErrors.get(effectiveCroppingKey);

            if (CollectionUtils.isNotEmpty(messages)) {
                Set<String> errors = messages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            }

            // À cette échelle l'indicateur ne peux prendre que trois valeurs qualitatives (Labour, TCS, Semis direct).
            boolean isDisplayed = isDisplayed(ExportLevel.CROP, 0) && !TillageType.AUCUN.equals(values);

            if (isDisplayed && effectiveCroppingKey.phase().isPresent()) {

                // write crop sheet
                writer.writeEffectivePerennialCrop(its,
                        irs,
                        domain.getCampaign(),
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        values.name(),
                        cropYealdAverage,
                        reliability,
                        comments,
                        domain,
                        growingSystemContext.getAnonymizeGrowingSystem(),
                        plot,
                        crop,
                        phase,
                        this.getClass());
            }

        }
    }

    private void writeEffectiveSeasonalCroppingValues(
            IndicatorWriter writer,
            Domain domain,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            Plot plot,
            String its,
            String irs,
            List<Map.Entry<EffectiveCropCycleScaleKey, TillageType>> sortedEffectiveCroppingValues) {
        for (Map.Entry<EffectiveCropCycleScaleKey, TillageType> entry : sortedEffectiveCroppingValues) {

            TillageType values = entry.getValue();
            EffectiveCropCycleScaleKey effectiveCroppingKey = entry.getKey();

            CroppingPlanEntry crop = effectiveCroppingKey.crop();
            Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage = effectiveCropsYealdAverage.get(crop.getCode());

            CroppingPlanEntry previousCrop = effectiveCroppingKey.previousCrop().orElse(null);
            Integer rank = effectiveCroppingKey.rang().orElse(null);

            Integer riec = effectiveCroppingReliabilityFieldErrorCounter.get(effectiveCroppingKey);
            Integer ritc = effectiveCroppingReliabilityTotalCounter.get(effectiveCroppingKey);

            Integer reliability = computeReliabilityIndex(riec, ritc);

            String comments = RELIABILITY_INDEX_NO_COMMENT;
            Set<MissingFieldMessage> messages = effectiveCroppingFieldsErrors.get(effectiveCroppingKey);

            if (CollectionUtils.isNotEmpty(messages)) {
                Set<String> errors = messages.stream().map(MissingFieldMessage::getMessage).collect(Collectors.toSet());
                comments = String.join(IndicatorWriter.SEPARATOR, errors);
            }

            // À cette échelle l'indicateur ne peux prendre que trois valeurs qualitatives (Labour, TCS, Semis direct).
            boolean isDisplayed = isDisplayed(ExportLevel.CROP, 0) && !TillageType.AUCUN.equals(values);
            if (isDisplayed && effectiveCroppingKey.rang().isPresent()) {

                // write crop sheet
                writer.writeEffectiveSeasonalCrop(its,
                        irs,
                        domain.getCampaign(),
                        getIndicatorCategory(),
                        getIndicatorLabel(0),
                        values.name(),
                        cropYealdAverage,
                        reliability,
                        comments,
                        domain,
                        growingSystemContext.getAnonymizeGrowingSystem(),
                        plot,
                        crop,
                        rank,
                        previousCrop,
                        this.getClass());
            }
        }
    }


    @Override
    public void resetPracticed(Domain domain) {
        // cleanup for next iteration
        practicedSystemsValues.clear();
        reliabilityIndexPracticedSystemValuesTotalCounter.clear();
        reliabilityIndexPracticedSystemValuesErrorCounter.clear();
        practicedSystemFieldsErrors.clear();
        targetedErrorFieldMessages.clear();
    }

    @Override
    public void resetEffectiveCC() {
        // cleanup for next iteration
        effectiveCroppingReliabilityTotalCounter.clear();
        effectiveCroppingReliabilityFieldErrorCounter.clear();
        effectiveCropsYealdAverage.clear();
        effectiveCroppingFieldsErrors.clear();
    }

    @Override
    public void resetEffectiveZones() {
        // cleanup for next iteration
        effectiveZoneValues.clear();
        effectiveZoneReliabilityFieldErrorCounter.clear();
        effectiveZoneReliabilityTotalCounter.clear();
        effectiveZoneFieldsErrors.clear();
    }

    @Override
    public void resetEffectivePlots() {
        // cleanup for next iteration
        effectivePlotValues.clear();
        effectivePlotReliabilityFieldErrorCounter.clear();
        effectivePlotReliabilityTotalCounter.clear();
    }

    @Override
    public void resetEffectiveGrowingSystems() {
        // cleanup for next iteration
        effectiveGrowingSystemValues.clear();
        effectiveGrowingSystemReliabilityFieldErrorCounter.clear();
        effectiveGrowingSystemReliabilityTotalCounter.clear();
        targetedErrorFieldMessages.clear();
    }


    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return this.exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorCategory() {
        return INDICATOR_CATEGORY_AGRONOMIC_STRATEGY;
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.tillageType");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return this.displayed && this.exportLevels.contains(atLevel);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }

}
