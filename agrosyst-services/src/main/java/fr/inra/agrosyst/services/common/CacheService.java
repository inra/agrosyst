package fr.inra.agrosyst.services.common;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.security.AgrosystRuntimeException;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 * @author Arnaud Thimel (Code Lutin)
 */
public class CacheService extends AbstractAgrosystService {

    private static final Log log = LogFactory.getLog(CacheService.class);

    // instance is static to be shared between several service instances
    protected static Cache<String, Serializable> cache;

    protected static Cache<String, Serializable> shortCache;

    protected static final Set<CacheDiscriminator> shortCacheDiscriminators = Sets.newHashSet(CacheDiscriminator.hasRole(), CacheDiscriminator.networksHierarchy());

    protected Cache<String, Serializable> getCacheInstance(CacheDiscriminator discriminator) {
        Cache<String, Serializable> result;
        if (CacheService.shortCacheDiscriminators.contains(discriminator)) {
            if (CacheService.shortCache == null && getConfig().isBusinessCachingEnabled()) {
                long duration = getConfig().getBusinessCachingShortDuration();
                CacheService.shortCache = CacheBuilder.newBuilder().expireAfterWrite(duration, TimeUnit.SECONDS).build();
            }
            result = CacheService.shortCache;
        } else {
            if (CacheService.cache == null && getConfig().isBusinessCachingEnabled()) {
                long duration = getConfig().getBusinessCachingDuration();
                CacheService.cache = CacheBuilder.newBuilder().expireAfterWrite(duration, TimeUnit.MINUTES).build();
            }
            result = CacheService.cache;
        }
        return result;
    }

    public <V extends Serializable> V get(CacheDiscriminator discriminator, Callable<? extends V> loader) {
        V result = get(discriminator, null, loader);
        return result;
    }

    public <K, V extends Serializable> V get(CacheDiscriminator discriminator, K key, Callable<? extends V> loader) {
        try {
            V result;
            if (getConfig().isBusinessCachingEnabled()) {
                String cacheKey = String.format("%s#%s", discriminator.discriminatorAsString(), key);
                result = (V) getCacheInstance(discriminator).get(cacheKey, loader);
            } else {
                result = loader.call();
            }
            return result;
        } catch (Exception eee) {
            if (eee.getCause() instanceof AgrosystRuntimeException) {
                throw (AgrosystRuntimeException) eee.getCause();
            } else if (eee instanceof RuntimeException) {
                throw (RuntimeException) eee;
            } else {
                throw new AgrosystTechnicalException("Unable to load type from cache", eee);
            }
        }
    }

    public void clear() {
        if (log.isDebugEnabled()) {
            log.debug("Purge des caches");
        }
        if (CacheService.cache != null) {
            CacheService.cache.invalidateAll();
        }
        if (CacheService.shortCache != null) {
            CacheService.shortCache.invalidateAll();
        }
    }

    public void clearSingle(CacheDiscriminator discriminator) {
        if (log.isDebugEnabled()) {
            log.debug("Purge des caches avec discriminant: " + discriminator);
        }
        Cache<String, Serializable> instance = getCacheInstance(discriminator);
        if (instance != null) {
            instance.invalidateAll();
        }
    }

}
