package fr.inra.agrosyst.services.performance.indicators.fertilization;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.IrrigationAction;
import fr.inra.agrosyst.api.entities.action.IrrigationInputUsage;
import fr.inra.agrosyst.api.entities.action.MineralFertilizersSpreadingAction;
import fr.inra.agrosyst.api.entities.action.MineralProductInputUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.performance.MineralFertilization;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.nuiton.i18n.I18n.l;

/**
 * Calcule les indicateurs de quantités de fertilisants minéraux utilisés.
 *
 * Méthode de calcul générale : on cherche la quantité de substance appliquée dans l'action d'irrigation,
 * et on cherche la quantité de substance appliquée dans l'action d'application de produits fertilisants minéraux.
 * On fait donc le calcul de base à l'échelle de l'intervention.
 *
 * <p>Par exemple pour l'azote (N), on doit faire le calcul de cette façon :</p>
 *
 * Pour les actions d' Application de produits minéraux / Fertilisation minérale :
 * <pre>N minéral (kg/ha) (kg/ha)= Dose de produit (kg/ha) * Unités fertilisantes (kg/100kg) * PSCI</pre>
 *
 * Pour les actions d' Irrigation (Fertirrigation) :
 * <pre>N minéral (kg/ha) (kg/ha)= Unités d’azote minéral apportées (kg/ha) * PSCI</pre>
 *
 * Le total de la quantité d'azote étant simplement la somme des deux :
 *
 * N minéral total (kg/ha) = N minéral Irrigation + N minéral Fertilisation minérale
 *
 * On voit donc que l'on peut aussi faire le calcul de la quantité d'azote pour les interventions sans appliquer
 * le PSCI et l'appliquer une seule fois au total.
 */
public class IndicatorMineralFertilization extends AbstractIndicatorFertilization {

    private static final Double DEFAULT_COEF = 1.0;

    private static final List<String> FIELDS = List.of(
            "ferti_n_mineral",
            "ferti_p2o5_mineral",
            "ferti_k2o_mineral",
            "ferti_b_mineral",
            "ferti_ca_mineral",
            "ferti_fe_mineral",
            "ferti_mn_mineral",
            "ferti_mo_mineral",
            "ferti_na2o_mineral",
            "ferti_so3_mineral",
            "ferti_cu_mineral",
            "ferti_zn_mineral",
            "ferti_mgo_mineral"
    );

    private boolean[] substanceToDisplay = new boolean[] {
            true, // N
            true, // P2O5
            true, // K2O
            true, // B
            true, // Ca
            true, // Fe
            true, // Mn
            true, // Mo
            true, // Na2O
            true, // SO3
            true, // Cu
            true, // Zn
            true, // MgO
    };

    public IndicatorMineralFertilization() {
        // labels
        super(new String[] {
                "Indicator.label.mineralFertilization.N",
                "Indicator.label.mineralFertilization.P2O5",
                "Indicator.label.mineralFertilization.K2O",
                "Indicator.label.mineralFertilization.B",
                "Indicator.label.mineralFertilization.Ca",
                "Indicator.label.mineralFertilization.Fe",
                "Indicator.label.mineralFertilization.Mn",
                "Indicator.label.mineralFertilization.Mo",
                "Indicator.label.mineralFertilization.Na2O",
                "Indicator.label.mineralFertilization.SO3",
                "Indicator.label.mineralFertilization.Cu",
                "Indicator.label.mineralFertilization.Zn",
                "Indicator.label.mineralFertilization.MgO",
        });
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();

        for (int i = 0; i < this.labels.length; i++) {
            String indicatorName = getIndicatorLabel(i);
            String columnName = FIELDS.get(i);
            indicatorNameToColumnName.put(indicatorName, columnName);
        }
        return indicatorNameToColumnName;
    }


    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return null;
        }

        final Optional<IrrigationAction> optionalIrrigationAction = interventionContext.getOptionalIrrigationAction();
        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();
        final double psci = this.getToolPSCi(interventionContext.getIntervention());

        Optional<Double[]> optionalResult = this.computeAmounts(writerContext, optionalIrrigationAction, optionalMineralFertilizersSpreadingAction, psci, interventionContext);

        return optionalResult.orElse(null);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {
        final Optional<IrrigationAction> optionalIrrigationAction = interventionContext.getOptionalIrrigationAction();
        final Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction = interventionContext.getOptionalMineralFertilizersSpreadingAction();
        final double psci = this.getToolPSCi(interventionContext.getIntervention());

        Optional<Double[]> optionalResult = this.computeAmounts(writerContext, optionalIrrigationAction, optionalMineralFertilizersSpreadingAction, psci, interventionContext);

        return optionalResult.orElse(null);
    }

    private Optional<Double[]> computeAmounts(WriterContext writerContext,
                                    Optional<IrrigationAction> optionalIrrigationAction,
                                    Optional<MineralFertilizersSpreadingAction> optionalMineralFertilizersSpreadingAction,
                                    double psci,
                                    PerformanceInterventionContext interventionContext) {

        AtomicBoolean computeDoneFlag = new AtomicBoolean(false);

        Double[] amounts = newArray(this.labels.length, 0.0d);
        Map<Pair<AbstractAction, AbstractInputUsage>, Double[]> fertilizationAmountsByInput = new HashMap<>();

        // Pour l'action d'irrigation, on ne peut calculer que la quantité d'azote.
        if (optionalIrrigationAction.isPresent()) {
            final IrrigationAction irrigationAction = optionalIrrigationAction.get();
            final Integer azoteQuantity = irrigationAction.getAzoteQuantity();
            final double nitrogenAmount = (azoteQuantity != null ? azoteQuantity : 0) * psci;

            final IrrigationInputUsage irrigationInputUsage = irrigationAction.getIrrigationInputUsage();
            this.writeInputUsageForSubstance(writerContext, irrigationInputUsage, irrigationAction, nitrogenAmount, 0);

            final Double[] amountsForInput = newArray(labels.length, 0.0d);
            amountsForInput[0] += nitrogenAmount;

            fertilizationAmountsByInput.put(Pair.of(irrigationAction, irrigationInputUsage), amountsForInput);

            amounts = sum(amounts, amountsForInput);

            computeDoneFlag.set(true);
        }

        if (optionalMineralFertilizersSpreadingAction.isPresent()) {
            MineralFertilizersSpreadingAction mineralFertilizersSpreadingAction = optionalMineralFertilizersSpreadingAction.get();
            for (MineralProductInputUsage mineralProductInputUsage : mineralFertilizersSpreadingAction.getMineralProductInputUsages()) {
                final DomainMineralProductInput domainMineralProductInput = mineralProductInputUsage.getDomainMineralProductInput();

                final Double qtProductUsed = mineralProductInputUsage.getQtAvg();
                if (qtProductUsed != null) {
                    final Double coefToKgHa = COEF_CONVERT_MINERAL_PRODUCT_USAGE_TO_KG_HA.getOrDefault(domainMineralProductInput.getUsageUnit(), DEFAULT_COEF);

                    // dans reffertiminunifa, les substances sont indiquées en proportion de la masse de produit appliqué en kg/ha,
                    // donc si on a 35 dans la colonne n, cela signifie 35%, d'où ici la division par 100.
                    final RefFertiMinUNIFA refInput = domainMineralProductInput.getRefInput();
                    final double nitrogenProportion = refInput.getN() / 100;
                    final double p2O5Proportion = refInput.getP2O5() / 100;
                    final double k2OProportion = refInput.getK2O() / 100;
                    final double bProportion = refInput.getBore() / 100;
                    final double caProportion = refInput.getCalcium() / 100;
                    final double feProportion = refInput.getFer() / 100;
                    final double mnProportion = refInput.getManganese() / 100;
                    final double moProportion = refInput.getMolybdene() / 100;
                    final double na2OProportion = refInput.getOxyde_de_sodium() / 100;
                    final double sO3Proportion = refInput.getsO3() / 100;
                    final double cuProportion = refInput.getCuivre() / 100;
                    final double znProportion = refInput.getZinc() / 100;
                    final double mgOProportion = refInput.getMgO() / 100;

                    final double productAmount = qtProductUsed * coefToKgHa;

                    final double nitrogenAmount = productAmount * nitrogenProportion * psci;
                    final double p2O5Amount     = productAmount * p2O5Proportion * psci;
                    final double k20Amount      = productAmount * k2OProportion * psci;
                    final double bAmount        = productAmount * bProportion * psci;
                    final double caAmount       = productAmount * caProportion * psci;
                    final double feAmount       = productAmount * feProportion * psci;
                    final double mnAmount       = productAmount * mnProportion * psci;
                    final double moAmount       = productAmount * moProportion * psci;
                    final double na2OAmount     = productAmount * na2OProportion * psci;
                    final double sO3Amount      = productAmount * sO3Proportion * psci;
                    final double cuAmount       = productAmount * cuProportion * psci;
                    final double znAmount       = productAmount * znProportion * psci;
                    final double mgOAmount      = productAmount * mgOProportion * psci;

                    interventionContext.getCuQuantityByInputUsage().put(mineralProductInputUsage.getTopiaId(), cuAmount);
                    interventionContext.getSO3QuantityByInputUsage().put(mineralProductInputUsage.getTopiaId(), sO3Amount);

                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, nitrogenAmount, 0);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, p2O5Amount, 1);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, k20Amount, 2);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, bAmount, 3);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, caAmount, 4);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, feAmount, 5);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, mnAmount, 6);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, moAmount, 7);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, na2OAmount, 8);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, sO3Amount, 9);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, cuAmount, 10);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, znAmount, 11);
                    this.writeInputUsageForSubstance(writerContext, mineralProductInputUsage, mineralFertilizersSpreadingAction, mgOAmount, 12);

                    final Double[] amountsForInput = newArray(labels.length, 0.0d);
                    amountsForInput[0] = nitrogenAmount;
                    amountsForInput[1] = p2O5Amount;
                    amountsForInput[2] = k20Amount;
                    amountsForInput[3] = bAmount;
                    amountsForInput[4] = caAmount;
                    amountsForInput[5] = feAmount;
                    amountsForInput[6] = mnAmount;
                    amountsForInput[7] = moAmount;
                    amountsForInput[8] = na2OAmount;
                    amountsForInput[9] = sO3Amount;
                    amountsForInput[10] = cuAmount;
                    amountsForInput[11] = znAmount;
                    amountsForInput[12] = mgOAmount;

                    fertilizationAmountsByInput.put(Pair.of(mineralFertilizersSpreadingAction, mineralProductInputUsage), amountsForInput);

                    amounts = sum(amounts, amountsForInput);

                    computeDoneFlag.set(true);
                }
            }
        }

        if (computeDoneFlag.get()) {
            interventionContext.setMineralFertilizationAmountsByInput(fertilizationAmountsByInput);
            return Optional.of(amounts);
        }

        return Optional.empty();
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.mineralFertilization");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return displayed && isRelevant(atLevel) && substanceToDisplay[i];
    }

    public void init(IndicatorFilter filter, Collection<MineralFertilization> mineralFertilizations) {
        displayed = filter != null;
        this.substanceToDisplay = new boolean[] {
                false, // N
                false, // P2O5
                false, // K2O
                false, // B
                false, // Ca
                false, // Fe
                false, // Mn
                false, // Mo
                false, // Na2O
                false, // SO3
                false, // Cu
                false, // Zn
                false, // MgO
        };

        for (MineralFertilization mineralFertilization : mineralFertilizations) {
            switch (mineralFertilization) {
                case N -> substanceToDisplay[0] = true;
                case P2O5 -> substanceToDisplay[1] = true;
                case K2O -> substanceToDisplay[2] = true;
                case B -> substanceToDisplay[3] = true;
                case CA -> substanceToDisplay[4] = true;
                case FE -> substanceToDisplay[5] = true;
                case MN -> substanceToDisplay[6] = true;
                case MO -> substanceToDisplay[7] = true;
                case NA2O -> substanceToDisplay[8] = true;
                case SO3 -> substanceToDisplay[9] = true;
                case CU -> substanceToDisplay[10] = true;
                case ZN -> substanceToDisplay[11] = true;
                case MGO -> substanceToDisplay[12] = true;
                default -> {} // par défaut : ne rien faire
            }
        }
    }
}
