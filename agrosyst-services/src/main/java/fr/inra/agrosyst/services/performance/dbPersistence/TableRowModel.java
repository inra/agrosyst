package fr.inra.agrosyst.services.performance.dbPersistence;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.services.performance.indicators.Indicator;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Model de table  pour les exports de performance
 * @author Cossé David : cosse@codelutin.com
 */
public class TableRowModel {
    
    protected final String tableName;
    // id de l'entité étudiée
    protected final String id;
    // Map des colonnes à renseigner par nom de colomne
    protected final HashMap<String, Column> allColumns = new HashMap<>();
    // liste des colomnes renseignées
    protected final Set<Column> columnsToPersists = new HashSet<>();
    
    public TableRowModel(String id, String tableName) {
        this.id = id;
        this.tableName = tableName;
    }
    
    public Column getColumnForDbName(String dbColumnName) {
        return allColumns.get(dbColumnName);
    }
    
    public void addIndicatorsColumns(Map<String, String> indicatorsNameToColumns) {
        for (Map.Entry<String, String> indicatorNameToColumn : indicatorsNameToColumns.entrySet()) {
            addColumn(Column.createIndicatorColumn(indicatorNameToColumn.getValue()));
        }
    }
    
    public void addColumn(Column column) {
        allColumns.put(column.getDbColumnName(), column);
        if (column.getValue().isPresent()) {
            columnsToPersists.add(column);
        }
    }
    
    public void addIndicatorValue(String columnName, Object value) {
        Column c = this.getColumnForDbName(columnName);
        if (c == null) {
            throw new AgrosystTechnicalException(String.format("Colonne '%s' non disponible", columnName));
        }
        if (value instanceof Double) {
            c.setIndicatorValue(value, Double.class);
        } else if (value instanceof String) {
            c.setIndicatorValue(value, String.class);
        } else if (value instanceof Boolean) {
            c.setIndicatorValue(value, Boolean.class);
        }
        this.addValue(c);
    }
    
    public void addExtraValues(Integer reliabilityIndex, IndicatorModel indicatorModel, String comment, String doseRef, String doseRefUnit) {
        
        for (Map.Entry<Indicator.OptionalExtraColumnEnumKey, String> extraColumnToIndicatorColumnName : indicatorModel.getExtraFieldsNameToColumn().entrySet()) {
            Object value0;
            Type type;
            switch (extraColumnToIndicatorColumnName.getKey()) {
                case TAUX_DE_COMPLETION,
                        MARGE_BRUTE_REELLE_TX_COMP, MARGE_BRUTE_STD_MIL_TX_COMP,
                        MARGE_SEMI_NETTE_REELLE_TX_COMP, MARGE_SEMI_NETTE_STD_MIL_TX_COMP,
                        MARGE_DIRECTE_REELLE_TX_COMP, MARGE_DIRECTE_STD_MIL_TX_COMP,
                        CHARGES_MECANISATION_REELLES_TX_COMP, CHARGES_MECANISATION_STD_MIL_TX_COMP,
                        CHARGES_OPERATIONNELLES_REELLES_TX_COMP, CHARGES_OPERATIONNELLES_STD_MIL_TX_COMP,
                        CHARGES_MANUELLES_REELLES_TX_COMP, CHARGES_MANUELLES_STD_MIL_TX_COMP,
                        CHARGES_TRACTORISTES_REELLES_TX_COMP, CHARGES_TRACTORISTES_STD_MIL_TX_COMP,
                        CHARGES_TOTALES_REELLES_TX_COMP, CHARGES_TOTALES_STD_MIL_TX_COMP -> {
                    value0 = reliabilityIndex;
                    type = Integer.class;
                }
                case DETAIL_CHAMPS_NON_RENSEIGNES,
                        MARGE_BRUTE_REELLE_DETAIL, MARGE_BRUTE_STD_MIL_DETAIL,
                        MARGE_SEMI_NETTE_REELLE_DETAIL, MARGE_SEMI_NETTE_STD_MIL_DETAIL,
                        MARGE_DIRECTE_REELLE_DETAIL, MARGE_DIRECTE_STD_MIL_DETAIL,
                        CHARGES_MECANISATION_REELLES_DETAIL, CHARGES_MECANISATION_STD_MIL_DETAIL,
                        CHARGES_OPERATIONNELLES_REELLES_DETAIL, CHARGES_OPERATIONNELLES_STD_MIL_DETAIL,
                        CHARGES_MANUELLES_REELLES_DETAIL, CHARGES_MANUELLES_STD_MIL_DETAIL,
                        CHARGES_TRACTORISTES_REELLES_DETAIL, CHARGES_TRACTORISTES_STD_MIL_DETAIL,
                        CHARGES_TOTALES_REELLES_DETAIL, CHARGES_TOTALES_STD_MIL_DETAIL -> {
                    value0 = comment;
                    type = String.class;
                }
                case DOSE_REFERENCE -> {
                    value0 = doseRef;
                    type = String.class;
                }
                case DOSE_REFERENCE_UNIT -> {
                    value0 = doseRefUnit;
                    type = String.class;
                }
                default -> throw new AgrosystTechnicalException("Unexpected value: " + extraColumnToIndicatorColumnName.getKey());
            }
            
            String extraFieldDbName = extraColumnToIndicatorColumnName.getValue();
            addExtraValue(value0, type, extraFieldDbName);
        }
    }
    
    protected void addExtraValue(Object value0, Type type, String extraFieldDbName) {
        Column cExtra = getColumnForDbName(extraFieldDbName);
        if (cExtra == null) {
            throw new AgrosystTechnicalException(String.format("Colonne '%s' non disponible", extraFieldDbName));
        }
        if (value0 != null) {
            cExtra.setIndicatorValue(value0, type);
            addValue(cExtra);
        }
    }
    
    public void addValue(Column column) {
        columnsToPersists.add(column);
    }
    
    public Set<Column> getColumnsToPersists() {
        return columnsToPersists;
    }
    
    public String getTableName() {
        return tableName;
    }
}
