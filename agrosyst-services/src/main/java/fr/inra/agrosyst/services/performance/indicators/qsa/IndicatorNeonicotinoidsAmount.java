package fr.inra.agrosyst.services.performance.indicators.qsa;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.BiologicalControlAction;
import fr.inra.agrosyst.api.entities.action.PesticidesSpreadingAction;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * Quantité de substances actives de la famille des néonicotinoïdes.
 *
 * Le calcul est le suivant :
 *
 * QSA Néonicotinoïdes = QSA Clothianidine + QSA Imidaclopride + QSA Thiamethoxam + QSA Acétamipride + QSA Thiaclopride
 */
public class IndicatorNeonicotinoidsAmount extends AbstractIndicatorQSA {

    private static final String[] LABELS = new String[] {
            "Indicator.label.neonicotinoids",
            "Indicator.label.neonicotinoids_hts"
    };

    private boolean[] indicatorsToDisplay = new boolean[] { false, false };

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {
        if (interventionContext.isFictive()) {
            return newArray(LABELS.length, 0.0d);
        }

        return this.computeNeonicotinoidsAmounts(writerContext, interventionContext);
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext, PerformanceEffectiveCropExecutionContext cropContext, PerformanceEffectiveInterventionExecutionContext interventionContext) {
        return this.computeNeonicotinoidsAmounts(writerContext, interventionContext);
    }

    private Double[] computeNeonicotinoidsAmounts(WriterContext writerContext,
                                                  PerformanceInterventionContext interventionContext) {
        double totalAmount = 0.0d;

        if (interventionContext.getOptionalPesticidesSpreadingAction().isPresent()) {
            final PesticidesSpreadingAction pesticidesSpreadingAction = interventionContext.getOptionalPesticidesSpreadingAction().get();

            Collection<? extends AbstractPhytoProductInputUsage> pesticideProductInputUsages = pesticidesSpreadingAction.getPesticideProductInputUsages();
            if (pesticideProductInputUsages != null) {
                final Double amountForPesticideProduct = pesticideProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .map(
                                usage -> {
                                    final String usageTopiaId = usage.getTopiaId();
                                    Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);
                                    String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                                            usageTopiaId,
                                            MissingMessageScope.INPUT);
                                    final Double neonicotinoidAmount = interventionContext.getNeonicotinoidAmounts().getOrDefault(Pair.of(pesticidesSpreadingAction, usage), 0.0);

                                    writeIndicatorValue(writerContext, usage, pesticidesSpreadingAction, neonicotinoidAmount, reliabilityIndexForInputId, reliabilityCommentForInputId, 0);
                                    writeIndicatorValue(writerContext, usage, pesticidesSpreadingAction, neonicotinoidAmount, reliabilityIndexForInputId, reliabilityCommentForInputId, 1);

                                    return neonicotinoidAmount;
                                }
                        ).reduce(0.0, Double::sum);
                totalAmount += amountForPesticideProduct;
            }
        }

        if (interventionContext.getOptionalBiologicalControlAction().isPresent()) {
            final BiologicalControlAction biologicalControlAction = interventionContext.getOptionalBiologicalControlAction().get();

            Collection<? extends AbstractPhytoProductInputUsage> biologicalProductInputUsages = biologicalControlAction.getBiologicalProductInputUsages();
            if (biologicalProductInputUsages != null) {
                final Double amountForBiologicalProduct = biologicalProductInputUsages.stream()
                        .filter(u -> u.getDomainPhytoProductInput() != null)
                        .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                        .map(
                                usage -> {
                                    final String usageTopiaId = usage.getTopiaId();
                                    Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);
                                    String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                                            usageTopiaId,
                                            MissingMessageScope.INPUT);
                                    final Double neonicotinoidAmount = interventionContext.getNeonicotinoidAmounts().getOrDefault(Pair.of(biologicalControlAction, usage), 0.0);

                                    writeIndicatorValue(writerContext, usage, biologicalControlAction, neonicotinoidAmount, reliabilityIndexForInputId, reliabilityCommentForInputId, 0);
                                    writeIndicatorValue(writerContext, usage, biologicalControlAction, neonicotinoidAmount, reliabilityIndexForInputId, reliabilityCommentForInputId, 1);

                                    return neonicotinoidAmount;
                                }
                        ).reduce(0.0, Double::sum);
                totalAmount += amountForBiologicalProduct;
            }
        }

        final double amountWithoutSeedingTreatment = totalAmount;

        if (interventionContext.getOptionalSeedingActionUsage().isPresent()) {
            final SeedingActionUsage seedingActionUsage = interventionContext.getOptionalSeedingActionUsage().get();

            final Double amountForSeedingAction = seedingActionUsage.getSeedLotInputUsage()
                    .stream()
                    .filter(u -> u != null && u.getSeedingSpecies() != null)
                    .flatMap(u -> u.getSeedingSpecies().stream())
                    .filter(u -> u.getSeedProductInputUsages() != null)
                    .flatMap(u -> u.getSeedProductInputUsages().stream())
                    .filter(u -> u.getDomainPhytoProductInput().getRefInput().getCode_AMM() != null)
                    .map(
                            usage -> {
                                final String usageTopiaId = usage.getTopiaId();
                                Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(usageTopiaId);
                                String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(
                                        usageTopiaId,
                                        MissingMessageScope.INPUT);
                                final Double neonicotinoidAmount = interventionContext.getNeonicotinoidAmounts().getOrDefault(Pair.of(seedingActionUsage, usage), 0.0);

                                writeIndicatorValue(writerContext, usage, seedingActionUsage, neonicotinoidAmount, reliabilityIndexForInputId, reliabilityCommentForInputId, 0);
                                writeIndicatorValue(writerContext, usage, seedingActionUsage, 0.0, reliabilityIndexForInputId, reliabilityCommentForInputId, 1);

                                return neonicotinoidAmount;
                            }
                    ).reduce(0.0, Double::sum);
            totalAmount += amountForSeedingAction;
        }

        return new Double[] { totalAmount, amountWithoutSeedingTreatment };
    }

    private void writeIndicatorValue(WriterContext writerContext, AbstractPhytoProductInputUsage usage, AbstractAction action, Double neonicotinoidAmount, Integer reliabilityIndexForInputId, String reliabilityCommentForInputId, int indicatorIndex) {
        if (isDisplayed(ExportLevel.INPUT, indicatorIndex)) {
            writerContext.getWriter()
                    .writeInputUsage(
                            writerContext.getIts(),
                            writerContext.getIrs(),
                            getIndicatorCategory(),
                            getIndicatorLabel(indicatorIndex),
                            usage,
                            action,
                            writerContext.getEffectiveIntervention(),
                            writerContext.getPracticedIntervention(),
                            writerContext.getCodeAmmBioControle(),
                            writerContext.getAnonymizeDomain(),
                            writerContext.getAnonymizeGrowingSystem(),
                            writerContext.getPlot(),
                            writerContext.getZone(),
                            writerContext.getPracticedSystem(),
                            writerContext.getCroppingPlanEntry(),
                            writerContext.getPracticedPhase(),
                            writerContext.getSolOccupationPercent(),
                            writerContext.getEffectivePhase(),
                            writerContext.getRank(),
                            writerContext.getPreviousPlanEntry(),
                            writerContext.getIntermediateCrop(),
                            this.getClass(),
                            neonicotinoidAmount,
                            reliabilityIndexForInputId,
                            reliabilityCommentForInputId,
                            "",
                            "",
                            "",
                            writerContext.getGroupesCiblesByCode()
                    );
        }
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), "qsa_neonicotinoides");
        indicatorNameToColumnName.put(getIndicatorLabel(1), "qsa_neonicotinoides_hts");
        return indicatorNameToColumnName;
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.specificActiveSubstances");
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return this.indicatorsToDisplay[i];
    }

    public void init(IndicatorFilter filter) {
        boolean displayed = filter != null;
        if (displayed) {
            this.indicatorsToDisplay[0] = filter.getWithSeedingTreatment();
            this.indicatorsToDisplay[1] = filter.getWithoutSeedingTreatment();
        }
    }
}
