package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefNuisibleEDI;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel NuisiblesEDI.
 * 
 * <ul>
 * <li>reference_param
 * <li>reference_label
 * <li>filieres
 * <li>source
 * </ul>
 * 
 * @author Eric Chatellier
 */
public class ExportActiveRefNuisibleEDIModel implements ExportModel<RefNuisibleEDI> {

    @Override
    public char getSeparator() {
        return AbstractAgrosystModel.CSV_SEPARATOR;
    }

    @Override
    public Iterable<ExportableColumn<RefNuisibleEDI, Object>> getColumnsForExport() {
        ModelBuilder<RefNuisibleEDI> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("reference_param", RefNuisibleEDI.PROPERTY_REFERENCE_PARAM, AbstractAgrosystModel.AGROSYST_BIO_AGRESSOR_TYPE_FORMATTER);
        modelBuilder.newColumnForExport("reference_label", RefNuisibleEDI.PROPERTY_REFERENCE_LABEL);
        modelBuilder.newColumnForExport("filieres", RefNuisibleEDI.PROPERTY_SECTORS, AbstractAgrosystModel.AGROSYST_SECTORS_FORMATTER);
        modelBuilder.newColumnForExport("source", RefNuisibleEDI.PROPERTY_SOURCE);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
