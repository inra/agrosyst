package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenne;
import fr.inra.agrosyst.api.entities.referential.RefCouvSolPerenneImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * Vitesse couv PHASE   Début Inter Fin Inter   Couv
 * 
 * @author Eric Chatellier
 */
public class RefCouvSolPerenneModel extends AbstractAgrosystModel<RefCouvSolPerenne> implements ExportModel<RefCouvSolPerenne> {

    protected static final ValueParser<CropCyclePhaseType> PHASE_PARSER = value -> {
        CropCyclePhaseType result = CropCyclePhaseType.valueOf(value);
        return result;
    };

    protected static final ValueFormatter<CropCyclePhaseType> PERIODE_SEMIS_FORMATTER = Enum::name;

    public RefCouvSolPerenneModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("Vitesse couv", RefCouvSolPerenne.PROPERTY_VITESSE_COUV, VITESSE_COUV_PARSER);
        newMandatoryColumn("PHASE", RefCouvSolPerenne.PROPERTY_PHASE, PHASE_PARSER);
        newMandatoryColumn("Début Inter", RefCouvSolPerenne.PROPERTY_DEBUT_INTER);
        newMandatoryColumn("Fin Inter", RefCouvSolPerenne.PROPERTY_FIN_INTER);
        newMandatoryColumn("Couv", RefCouvSolPerenne.PROPERTY_COUV, DOUBLE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefCouvSolPerenne.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefCouvSolPerenne, Object>> getColumnsForExport() {
        ModelBuilder<RefCouvSolPerenne> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Vitesse couv", RefCouvSolPerenne.PROPERTY_VITESSE_COUV, VITESSE_COUV_FORMATTER);
        modelBuilder.newColumnForExport("PHASE", RefCouvSolPerenne.PROPERTY_PHASE, PERIODE_SEMIS_FORMATTER);
        modelBuilder.newColumnForExport("Début Inter", RefCouvSolPerenne.PROPERTY_DEBUT_INTER);
        modelBuilder.newColumnForExport("Fin Inter", RefCouvSolPerenne.PROPERTY_FIN_INTER);
        modelBuilder.newColumnForExport("Couv", RefCouvSolPerenne.PROPERTY_COUV, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefCouvSolPerenne.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
    
    @Override
    public RefCouvSolPerenne newEmptyInstance() {
        RefCouvSolPerenne result = new RefCouvSolPerenneImpl();
        result.setActive(true);
        return result;
    }
}
