package fr.inra.agrosyst.services.referential.csv;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMM;
import fr.inra.agrosyst.api.entities.referential.RefCompositionSubstancesActivesParNumeroAMMImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefCompositionSubstancesActivesParNumeroAMMModel extends AbstractAgrosystModel<RefCompositionSubstancesActivesParNumeroAMM> implements ExportModel<RefCompositionSubstancesActivesParNumeroAMM> {

    public RefCompositionSubstancesActivesParNumeroAMMModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("numero_AMM", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM);
        newMandatoryColumn("ID_SA", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ID_SA);
        newMandatoryColumn("nom_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA);
        newMandatoryColumn("variant_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_VARIANT_SA);
        newMandatoryColumn("dose_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_DOSE_SA, DOUBLE_PARSER);
        newMandatoryColumn("unite_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_UNITE_SA);
        newMandatoryColumn("source", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_SOURCE);
        newMandatoryColumn("active", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }
    @Override
    public Iterable<ExportableColumn<RefCompositionSubstancesActivesParNumeroAMM, Object>> getColumnsForExport() {
        ModelBuilder<RefCompositionSubstancesActivesParNumeroAMM> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("numero_AMM", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NUMERO__AMM);
        modelBuilder.newColumnForExport("ID_SA", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ID_SA);
        modelBuilder.newColumnForExport("nom_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_NOM_SA);
        modelBuilder.newColumnForExport("variant_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_VARIANT_SA);
        modelBuilder.newColumnForExport("dose_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_DOSE_SA, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("unite_sa", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_UNITE_SA);
        modelBuilder.newColumnForExport("source", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("active", RefCompositionSubstancesActivesParNumeroAMM.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefCompositionSubstancesActivesParNumeroAMM newEmptyInstance() {
        final RefCompositionSubstancesActivesParNumeroAMM referential = new RefCompositionSubstancesActivesParNumeroAMMImpl();
        referential.setActive(true);
        return referential;
    }
}
