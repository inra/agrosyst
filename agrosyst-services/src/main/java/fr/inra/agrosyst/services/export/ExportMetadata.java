package fr.inra.agrosyst.services.export;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AgrosystInterventionType;
import fr.inra.agrosyst.api.entities.CropCyclePhaseType;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.MaterielWorkRateUnit;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.action.CapacityUnit;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedType;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.referential.PastureType;
import fr.inra.agrosyst.api.entities.referential.ProductType;
import fr.inra.agrosyst.api.entities.referential.RefEspece;
import fr.inra.agrosyst.api.entities.referential.RefVariete;
import fr.inra.agrosyst.services.common.AgrosystI18nService;
import fr.inra.agrosyst.services.common.export.ExportModel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.i18n.I18n;

import java.time.LocalDate;

public class ExportMetadata {

    @Getter
    @Setter
    public static class Bean {
        protected String department;
        protected String networkNames;
        protected String growingSystemName;
        protected String growingSystemTypeAgriculture;
        protected String dephyNb;
        protected String growingPlanName;
        protected String domainName;

        protected InputType inputType;
        protected String productName;

        protected Double qtMin;
        protected Double qtAvg;
        protected Double qtMed;
        protected Double qtMax;
        protected String inputUnit;

        protected AgrosystInterventionType actionType;
        protected String action;
        protected boolean mainAction;
        protected String actionComment;

        protected String itkCycle ;
        protected String itkCrop;
        protected String itkPreviousCrop;
        protected Integer itkRank;
        protected CropCyclePhaseType itkPhase;
        protected AgrosystInterventionType type;
        protected String interventionName;
        protected String interventionId;
        protected String itkStart;
        protected String itkEnd;
        protected boolean intermediateCrop;


        protected String toolsCouplingName;
        protected Double psci;
        protected String speciesName;
        protected String itkSpecies;
        protected String itkVariety;
        protected String stadeMin;
        protected String stadeMax;

        protected double spatialFrequency;
        protected Integer progressionSpeed;
        protected Double involvedPeopleNumber;
        protected Double workRate ;
        protected MaterielWorkRateUnit workRateUnit;
        protected String comment;

        protected String zoneName;
        protected String plotName;
        protected Integer campaign;

        // Practiced
        protected String practicedSystemName;
        protected String campaigns;

        public Bean(String departement,
                    String networkNames,
                    String growingSystemName,
                    String typeAgricultureLabel,
                    String dephyNumber,
                    String growingPlanName,
                    String domainName,
                    Integer campaign) {
            this.department = departement;
            this.networkNames = networkNames;
            this.growingSystemName = growingSystemName;
            this.growingSystemTypeAgriculture = typeAgricultureLabel;
            this.dephyNb = dephyNumber;
            this.growingPlanName = growingPlanName;
            this.domainName = domainName;
            this.campaign = campaign;
        }

        public Bean(Bean baseBean) {
            this.department = baseBean.department;
            this.networkNames = baseBean.networkNames;
            this.growingSystemName = baseBean.growingSystemName;
            this.growingSystemTypeAgriculture = baseBean.growingSystemTypeAgriculture;
            this.dephyNb = baseBean.dephyNb;
            this.growingPlanName = baseBean.growingPlanName;
            this.domainName = baseBean.domainName;
            this.inputType = baseBean.inputType;
            this.productName = baseBean.productName;
            this.qtMin = baseBean.qtMin;
            this.qtAvg = baseBean.qtAvg;
            this.qtMed = baseBean.qtMed;
            this.qtMax = baseBean.qtMax;
            this.inputUnit = baseBean.inputUnit;
            this.actionType = baseBean.actionType;
            this.action = baseBean.action;
            this.mainAction = baseBean.mainAction;
            this.actionComment = baseBean.actionComment;
            this.itkCycle = baseBean.itkCycle;
            this.itkCrop = baseBean.itkCrop;
            this.itkPreviousCrop = baseBean.itkPreviousCrop;
            this.itkRank = baseBean.itkRank;
            this.itkPhase = baseBean.itkPhase;
            this.type = baseBean.type;
            this.interventionName = baseBean.interventionName;
            this.interventionId = baseBean.interventionId;
            this.itkStart = baseBean.itkStart;
            this.itkEnd = baseBean.itkEnd;
            this.intermediateCrop = baseBean.intermediateCrop;
            this.toolsCouplingName = baseBean.toolsCouplingName;
            this.psci = baseBean.psci;
            this.speciesName = baseBean.speciesName;
            this.itkSpecies = baseBean.itkSpecies;
            this.itkVariety = baseBean.itkVariety;
            this.stadeMin = baseBean.stadeMin;
            this.stadeMax = baseBean.stadeMax;
            this.spatialFrequency = baseBean.spatialFrequency;
            this.progressionSpeed = baseBean.progressionSpeed;
            this.involvedPeopleNumber = baseBean.involvedPeopleNumber;
            this.workRate = baseBean.workRate;
            this.workRateUnit = baseBean.workRateUnit;
            this.comment = baseBean.comment;
            this.zoneName = baseBean.zoneName;
            this.plotName = baseBean.plotName;
            this.campaign = baseBean.campaign;
            this.practicedSystemName = baseBean.practicedSystemName;
            this.campaigns = baseBean.campaigns;
        }

    }

    protected static <T extends Bean> void baseColumnsWithCampaign(ExportModel<T> m) {
        baseColumns(m, true);
    }

    protected static <T extends Bean> void baseColumnsWithoutCampaign(ExportModel<T> m) {
        baseColumns(m, false);
    }


    private static <T extends Bean> void baseColumns(ExportModel<T> m, boolean withCampaign) {
        m.newColumn("Département", Bean::getDepartment);
        m.newColumn("Réseau(x)", Bean::getNetworkNames);
        m.newColumn("Système De culture", Bean::getGrowingSystemName);
        m.newColumn("Type de conduite", Bean::getGrowingSystemTypeAgriculture);
        m.newColumn("N° DEPHY", Bean::getDephyNb);
        m.newColumn("Dispositif", Bean::getGrowingPlanName);
        m.newColumn("Domaine", Bean::getDomainName);
        m.newColumn("Campagne", Bean::getCampaign);
    }

    protected static <T extends Bean> void productInputColumns(ExportModel<T> m) {
        m.newColumn("Type d'intrant", Bean::getInputType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
        m.newColumn("Nom du produit", Bean::getProductName);

        m.newColumn("Quantité minimale d'intrant", Bean::getQtMin);
        m.newColumn("Quantité moyenne d'intrant", Bean::getQtAvg);
        m.newColumn("Quantité médiane d'intrant", Bean::getQtMed);
        m.newColumn("Quantité maximale d'intrant", Bean::getQtMax);
        m.newColumn("Unité de produit", Bean::getInputUnit);
    }

    protected static <T extends Bean> void interventionColumns(ExportModel<T> m) {
        m.newColumn("Cycle", Bean::getItkCycle);
        m.newColumn("Culture", Bean::getItkCrop);
        m.newColumn("Culture précédente", Bean::getItkPreviousCrop);
        // ITK
        m.newColumn("Rang", Bean::getItkRank);
        m.newColumn("Phase", CropCyclePhaseType.class, Bean::getItkPhase);
        m.newColumn("Type d'intervention", Bean::getType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
        m.newColumn("Nom de l'intervention", Bean::getInterventionName);
        m.newColumn("Identifiant d'intervention", Bean::getInterventionId);
        m.newColumn("Date de début d'intervention", Bean::getItkStart);
        m.newColumn("Date de fin d'intervention ", Bean::getItkEnd);
        m.newColumn("Affectation à la culture intermédiaire", Bean::isIntermediateCrop);

    }

    protected static <T extends Bean> void interventionDetailsColumns(ExportModel<T> m) {
        m.newColumn("Fréquence spatiale", Bean::getSpatialFrequency);
        m.newColumn("PSCi", Bean::getPsci);
        m.newColumn("Combinaison d'outils", Bean::getToolsCouplingName);
        m.newColumn("Vitesse d'avancement", Bean::getProgressionSpeed);

        m.newColumn("Espèce", Bean::getSpeciesName);
        m.newColumn("Espèces de l'action", Bean::getItkSpecies);
        m.newColumn("Variétés de l'action", Bean::getItkVariety);
        m.newColumn("Stade de culture minimum", Bean::getStadeMin);
        m.newColumn("Stade de culture maximum", Bean::getStadeMax);

        m.newColumn("Débit de chantier", Bean::getWorkRate);
        m.newColumn("Unité du débit de chantier", Bean::getWorkRateUnit);
        m.newColumn("Nombre de personnes mobilisées", Bean::getInvolvedPeopleNumber);
        m.newColumn("Commentaire", Bean::getComment);
    }

    protected static <T extends Bean> void actionColumns(ExportModel<T> m) {
        m.newColumn("Type d'action", Bean::getActionType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
        m.newColumn("Action", Bean::getAction);
        m.newColumn("Action principale", Bean::isMainAction);
        m.newColumn("Commentaire (action)", Bean::getActionComment);
    }

    @Getter
    @Setter
    public static class PerennialCropCycleSpeciesBean extends Bean {
        String croppingPlanEntryName;
        CropCyclePhaseType phase;
        String speciesEspece;
        String speciesQualifiant;
        String speciesTypeSaisonnier;
        String speciesDestination;
        String varietyLibelle;
        String profil_vegetatif_BBCH;
        String graftSupport;
        String graftClone;
        boolean plantCertified;
        LocalDate overGraftDate;

        public PerennialCropCycleSpeciesBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Espèces des phases de production des cultures pérennes";
//        }

        public static <T extends PerennialCropCycleSpeciesBean> void columnsWithProfilVegetatifBBCH(ExportModel<T> m) {
            columns(m, true);
        }

        public static <T extends PerennialCropCycleSpeciesBean> void columnsWithoutProfilVegetatifBBCH(ExportModel<T> m) {
            columns(m, false);
        }

        private static <T extends PerennialCropCycleSpeciesBean> void columns(ExportModel<T> m, boolean withProfilVegetatifBBCH) {

            // remind the crop from perennial cycle
            m.newColumn("Culture", PerennialCropCycleSpeciesBean::getCroppingPlanEntryName);
            m.newColumn("Phase", CropCyclePhaseType.class, PerennialCropCycleSpeciesBean::getPhase);

            // species characteristics
            m.newColumn("Espèce", PerennialCropCycleSpeciesBean::getSpeciesEspece);
            m.newColumn("Qualifiant", PerennialCropCycleSpeciesBean::getSpeciesQualifiant);
            m.newColumn("Type saisonnier", PerennialCropCycleSpeciesBean::getSpeciesTypeSaisonnier);
            m.newColumn("Destination", PerennialCropCycleSpeciesBean::getSpeciesDestination);
            m.newColumn("Cépage / Variété", PerennialCropCycleSpeciesBean::getVarietyLibelle);
            if (withProfilVegetatifBBCH) {
                m.newColumn("Profil vegetatif BBCH", PerennialCropCycleSpeciesBean::getProfil_vegetatif_BBCH);
            }
            m.newColumn("Porte-greffe", PerennialCropCycleSpeciesBean::getGraftSupport);
            m.newColumn("Clone de la greffe", PerennialCropCycleSpeciesBean::getGraftClone);
            m.newColumn("Certification des plants", PerennialCropCycleSpeciesBean::isPlantCertified);
            m.newColumn("Date de sur-greffage", PerennialCropCycleSpeciesBean::getOverGraftDate);
        }
    }

    @Getter
    @Setter
    public static class ItkActionApplicationProduitsMinerauxBean extends Bean {
        boolean burial;
        boolean localizedSpreading;
        String typeProduit;
        String forme;
        Boolean phytoEffect;

        Double N;
        Double P2_O5;
        Double K2_O;
        Double bore;
        Double calcium;
        Double fer;
        Double manganese;
        Double molybdene;
        Double MG_O;
        Double oxydeDeSodium;
        Double S_O3;
        Double cuivre;
        Double zinc;

        Double uN;
        Double uP2_O5;
        Double uK2_O;
        Double uBore;
        Double uCalcium;
        Double uFer;
        Double uManganese;
        Double uMolybdene;
        Double uMG_O;
        Double uOxydeDeSodium;
        Double uS_O3;
        Double uCuivre;
        Double uZinc;

        public ItkActionApplicationProduitsMinerauxBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Application de produits minéraux";
//        }

        public static <T extends ItkActionApplicationProduitsMinerauxBean> void columns(ExportModel<T> m) {

            // intervention
            interventionColumns(m);

            // action
            actionColumns(m);
            m.newColumn("Enfouissement dans les 24h", ItkActionApplicationProduitsMinerauxBean::isBurial);
            m.newColumn("Apport localisé sur la ligne de culture", ItkActionApplicationProduitsMinerauxBean::isLocalizedSpreading);

            // inputs (Engrais/amendement (organo)minéral)
            productInputColumns(m);
            m.newColumn("Type de produit fertilisant", ItkActionApplicationProduitsMinerauxBean::getTypeProduit);
            m.newColumn("Forme de l'engrais", ItkActionApplicationProduitsMinerauxBean::getForme);
            m.newColumn("Effet SDN / phytosanitaire attendu", ItkActionApplicationProduitsMinerauxBean::getPhytoEffect);

            m.newColumn("% N", ItkActionApplicationProduitsMinerauxBean::getN);
            m.newColumn("% P2O5", ItkActionApplicationProduitsMinerauxBean::getP2_O5);
            m.newColumn("% K2O", ItkActionApplicationProduitsMinerauxBean::getK2_O);
            m.newColumn("% B", ItkActionApplicationProduitsMinerauxBean::getBore);
            m.newColumn("% Ca", ItkActionApplicationProduitsMinerauxBean::getCalcium);
            m.newColumn("% Fe", ItkActionApplicationProduitsMinerauxBean::getFer);
            m.newColumn("% Mn", ItkActionApplicationProduitsMinerauxBean::getManganese);
            m.newColumn("% Mo", ItkActionApplicationProduitsMinerauxBean::getMolybdene);
            m.newColumn("% MgO", ItkActionApplicationProduitsMinerauxBean::getMG_O);
            m.newColumn("% Na2O", ItkActionApplicationProduitsMinerauxBean::getOxydeDeSodium);
            m.newColumn("% SO3", ItkActionApplicationProduitsMinerauxBean::getS_O3);
            m.newColumn("% Cu", ItkActionApplicationProduitsMinerauxBean::getCuivre);
            m.newColumn("% Zn", ItkActionApplicationProduitsMinerauxBean::getZinc);

            m.newColumn("u N(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUN);
            m.newColumn("u P2O5(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUP2_O5);
            m.newColumn("u K2O(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUK2_O);
            m.newColumn("u B(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUBore);
            m.newColumn("u Ca(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUCalcium);
            m.newColumn("u Fe(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUFer);
            m.newColumn("u Mn(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUManganese);
            m.newColumn("u Mo(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUMolybdene);
            m.newColumn("u MgO(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUMG_O);
            m.newColumn("u Na2O(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUOxydeDeSodium);
            m.newColumn("u SO3(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUS_O3);
            m.newColumn("u Cu(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUCuivre);
            m.newColumn("u Zn(kg/ha)", ItkActionApplicationProduitsMinerauxBean::getUZinc);
        }

    }

    @Getter
    @Setter
    public static class ItkApplicationProduitsPhytoBean extends Bean {

        double proportionOfTreatedSurface;
        Double boiledQuantity;
        Double boiledQuantityPerTrip;
        boolean antiDriftNozzle;
        Double tripFrequency;

        ProductType productType;
        String groupesCibles;
        String targets;

        Double referenceDoseIFTancienne;
        PhytoProductUnit referenceDoseUnitIFTancienne;
        Double referenceDoseIFTcibleNonMillesime;
        PhytoProductUnit referenceDoseUnitIFTcibleNonMillesime;

        public ItkApplicationProduitsPhytoBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Application de phyto avec AMM";
//        }
//
        public static <T extends ItkApplicationProduitsPhytoBean> void columns(ExportModel<T> m) {
            // intervention
            interventionColumns(m);

            // action
            actionColumns(m);
            m.newColumn("Proportion de surface traitée au sein de la surface de l'intervention", ItkApplicationProduitsPhytoBean::getProportionOfTreatedSurface);
            m.newColumn("Volume moyen de bouillie par hectare", ItkApplicationProduitsPhytoBean::getBoiledQuantity);
            m.newColumn("Volume moyen de bouillie chargée à chaque voyage", ItkApplicationProduitsPhytoBean::getBoiledQuantityPerTrip);
            m.newColumn("Présence de buse anti-dérive sur le pulvérisateur", ItkApplicationProduitsPhytoBean::isAntiDriftNozzle);
            m.newColumn("Nombre de voyages par heure", ItkApplicationProduitsPhytoBean::getTripFrequency);

            // inputs
            productInputColumns(m);
            m.newColumn("Type de phyto", ItkApplicationProduitsPhytoBean::getProductType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            m.newColumn("Groupes cibles", ItkApplicationProduitsPhytoBean::getGroupesCibles);
            m.newColumn("Cibles du traitement", ItkApplicationProduitsPhytoBean::getTargets);

            m.newColumn("Dose de référence IFT à l’ancienne", ItkApplicationProduitsPhytoBean::getReferenceDoseIFTancienne);
            m.newColumn("Unité de dose de référence IFT à l’ancienne", ItkApplicationProduitsPhytoBean::getReferenceDoseUnitIFTancienne);
            m.newColumn("Dose de référence IFT à la cible non millésimé", ItkApplicationProduitsPhytoBean::getReferenceDoseIFTcibleNonMillesime);
            m.newColumn("Unité de dose de référence IFT à la cible non millésimé", ItkApplicationProduitsPhytoBean::getReferenceDoseUnitIFTcibleNonMillesime);
        }
    }

    public static class ItkActionAutreBean extends Bean{

        public ItkActionAutreBean(Bean baseBean) {
            super(baseBean);
        }
//        @Override
//        public String getTitle() {
//            return "Autre";
//        }

        public static <T extends Bean> void columns(ExportModel<T> m) {
            interventionColumns(m);
            actionColumns(m);
            productInputColumns(m);
        }
    }

    public static class ItkActionEntretienTailleVigneBean extends Bean {

        public ItkActionEntretienTailleVigneBean(Bean baseBean) {
            super(baseBean);
        }

        //        @Override
//        public String getTitle() {
//            return "Entretien-Taille de vigne et verger";
//        }
//
        public static <T extends Bean> void columns(ExportModel<T> m) {
            interventionColumns(m);
            actionColumns(m);
            productInputColumns(m);
        }
    }

    @Getter
    @Setter
    public static class ItkActionEpandageOrganiqueBean extends Bean {

        boolean landfilledWaste;

        String typeDeProduitFertilisant;

        Double n;
        Double p2O5;
        Double k2O;
        Double caO;
        Double mgO;
        Double s;

        Double uN;
        Double uP2O5;
        Double uK2O;
        Double uCaO;
        Double uMgO;
        Double uS;

        public ItkActionEpandageOrganiqueBean(Bean baseBean) {
            super(baseBean);
        }

        public String getTitle() {
            return "Épandage organique";
        }


        public static <T extends ItkActionEpandageOrganiqueBean> void columns(ExportModel<T> m) {
            // intervention
            interventionColumns(m);
            
            // action
            actionColumns(m);
            m.newColumn("Enfouissement dans les 24h", ItkActionEpandageOrganiqueBean::isLandfilledWaste);

            // inputs
            productInputColumns(m);

            m.newColumn("Type de produit fertilisant", ItkActionEpandageOrganiqueBean::getTypeDeProduitFertilisant); //RefFertiOrga.PROPERTY_LIBELLE

            m.newColumn("% N", ItkActionEpandageOrganiqueBean::getN);
            m.newColumn("% P2O5", ItkActionEpandageOrganiqueBean::getP2O5);
            m.newColumn("% K2O", ItkActionEpandageOrganiqueBean::getK2O);
            m.newColumn("% CaO", ItkActionEpandageOrganiqueBean::getCaO);
            m.newColumn("% MgO", ItkActionEpandageOrganiqueBean::getMgO);
            m.newColumn("% S", ItkActionEpandageOrganiqueBean::getS);

            // Since #10450 : add m.new calculated columns
            m.newColumn("u N(kg/ha)", ItkActionEpandageOrganiqueBean::getUN);
            m.newColumn("u P2O5(kg/ha)", ItkActionEpandageOrganiqueBean::getUP2O5);
            m.newColumn("u K2O(kg/ha)", ItkActionEpandageOrganiqueBean::getUK2O);
            m.newColumn("u CaO(kg/ha)", ItkActionEpandageOrganiqueBean::getUCaO);
            m.newColumn("u MgO(kg/ha)", ItkActionEpandageOrganiqueBean::getUMgO);
            m.newColumn("u S(kg/ha)", ItkActionEpandageOrganiqueBean::getUS);
        }
    }

    @Getter
    @Setter
    public static class ItkActionIrrigationBean extends Bean {

        Double waterQuantityMin;
        Double waterQuantityMax;
        Double waterQuantityAverage;
        Double waterQuantityMedian;
        Integer azoteQuantity;

        public ItkActionIrrigationBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Irrigation";
//        }

        public static <T extends ItkActionIrrigationBean> void columns(ExportModel<T> m) {
            // intervention
            interventionColumns(m);

            // action
            actionColumns(m);

            m.newColumn("Quantité minimale d'eau", ItkActionIrrigationBean::getWaterQuantityMin);
            m.newColumn("Quantité moyenne d'eau", ItkActionIrrigationBean::getWaterQuantityAverage);
            m.newColumn("Quantité médiane d'eau", ItkActionIrrigationBean::getWaterQuantityMedian);
            m.newColumn("Quantité maximale d'eau", ItkActionIrrigationBean::getWaterQuantityMax);
            m.newColumn("Unités d'azote minéral apportées", ItkActionIrrigationBean::getAzoteQuantity);

            // other input
            productInputColumns(m);
        }
    }

    @Getter
    @Setter
    public static class ItkActionLutteBiologiqueBean extends Bean {

        Double proportionOfTreatedSurface;
        Double boiledQuantity;
        Double boiledQuantityPerTrip;
        Double tripFrequency;

        String groupesCibles;
        String targets;
        String phytoProduct;

        Double referenceDoseIFTancienne;
        PhytoProductUnit referenceDoseUnitIFTancienne;
        Double referenceDoseIFTcibleNonMillesime;
        PhytoProductUnit referenceDoseUnitIFTcibleNonMillesime;

        public ItkActionLutteBiologiqueBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Application de phyto sans AMM";
//        }
//

        public static <T extends ItkActionLutteBiologiqueBean> void columns(ExportModel<T> m) {
            // intervention
            interventionColumns(m);

            // action
            actionColumns(m);
            m.newColumn("Proportion de surface traitée au sein de la surface de l'intervention", ItkActionLutteBiologiqueBean::getProportionOfTreatedSurface);
            m.newColumn("Volume moyen de bouillie par hectare", ItkActionLutteBiologiqueBean::getBoiledQuantity);
            m.newColumn("Volume moyen de bouillie chargée à chaque voyage", ItkActionLutteBiologiqueBean::getBoiledQuantityPerTrip);
            m.newColumn("Nombre de voyages par heure", ItkActionLutteBiologiqueBean::getTripFrequency);

            // inputs (Lutte biologique)
            productInputColumns(m);

            m.newColumn("Groupes cibles", ItkActionLutteBiologiqueBean::getGroupesCibles);
            m.newColumn("Cibles du traitement", ItkActionLutteBiologiqueBean::getTargets);
            m.newColumn("Type de produit", ItkActionLutteBiologiqueBean::getPhytoProduct);

            m.newColumn("Dose de référence IFT à l’ancienne", ItkActionLutteBiologiqueBean::getReferenceDoseIFTancienne);
            m.newColumn("Unité de dose de référence IFT à l’ancienne", ItkActionLutteBiologiqueBean::getReferenceDoseUnitIFTancienne);
            m.newColumn("Dose de référence IFT à la cible non millésimé", ItkActionLutteBiologiqueBean::getReferenceDoseIFTcibleNonMillesime);
            m.newColumn("Unité de dose de référence IFT à la cible non millésimé", ItkActionLutteBiologiqueBean::getReferenceDoseUnitIFTcibleNonMillesime);
        }
    }

    @Getter
    @Setter
    public static class ItkActionRecolteBean extends Bean {

        boolean organicCrop;
        String actionSpecies;
        String actionVarieties;
        String destination;
        Double yieldMin;
        Double yieldAverage;
        Double yieldMedian;
        Double yieldMax;
        YealdUnit yieldUnit;
        Integer salesPercent;
        Integer selfConsumedPercent;
        Integer noValorisationPercent;
        String qualityCriteriaLabel;
        Object quantitativeValue; // soit Double soit Boolean
        String cattleName;
        Integer pastureLoad;
        PastureType pastureType;
        boolean pasturingAtNight;

        public ItkActionRecolteBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Récolte";
//        }
//

        public static <T extends ItkActionRecolteBean> void columns(ExportModel<T> m) {
            // intervention
            interventionColumns(m);

            // action
            actionColumns(m);

            m.newColumn("Culture biologique", ItkActionRecolteBean::isOrganicCrop);
            m.newColumn("Espèces de l'action", ItkActionRecolteBean::getActionSpecies);
            m.newColumn("Variétés de l'action", ItkActionRecolteBean::getActionVarieties);
            m.newColumn("Destination", ItkActionRecolteBean::getDestination);
            m.newColumn("Rendement min", ItkActionRecolteBean::getYieldMin);
            m.newColumn("Rendement moyen", ItkActionRecolteBean::getYieldAverage);
            m.newColumn("Rendement median", ItkActionRecolteBean::getYieldMedian);
            m.newColumn("Rendement max", ItkActionRecolteBean::getYieldMax);
            m.newColumn("Unite", ItkActionRecolteBean::getYieldUnit, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            m.newColumn("Valorisation commercialisé", ItkActionRecolteBean::getSalesPercent);
            m.newColumn("Valorisation autoconsommé", ItkActionRecolteBean::getSelfConsumedPercent);
            m.newColumn("Valorisation non valorisé", ItkActionRecolteBean::getNoValorisationPercent);
            m.newColumn("Critères de qualité", ItkActionRecolteBean::getQualityCriteriaLabel);
            m.newColumn("Critères de qualité (valeur)", ItkActionRecolteBean::getQuantitativeValue);
            m.newColumn("Troupeau", ItkActionRecolteBean::getCattleName);
            m.newColumn("Chargement (têtes par ha)", ItkActionRecolteBean::getPastureLoad);
            m.newColumn("Type de pâturage", ItkActionRecolteBean::getPastureType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            m.newColumn("Présence la nuit", ItkActionRecolteBean::isPasturingAtNight);

            // other input
            productInputColumns(m);
        }
    }

    @Getter
    @Setter
    public static class ItkActionSemiBean extends Bean {

        SeedType seedType;
        Double yieldTarget;
        YealdUnit yealdUnit;
        String actionSpecies;
        String actionVarieties;
        Boolean treatment;
        Boolean biologicalSeedInoculation;
        Double quantity;
        SeedPlantUnit seedPlantUnit;
        Double deepness;
        String seedingsActionSpeciesComment;

        String groupesCibles;
        String targets;
        String phytoProduct;

        Double referenceDose;
        PhytoProductUnit referenceDoseUnit;

        public ItkActionSemiBean(Bean baseBean) {
            super(baseBean);
        }

        public static <T extends ItkActionSemiBean> void columns(ExportModel<T> m) {
            // intervention
            interventionColumns(m);

            // action
            actionColumns(m);

            m.newColumn("Type de semence/plant",  ItkActionSemiBean::getSeedType, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            m.newColumn("Objectif de rendement", ItkActionSemiBean::getYieldTarget);
            m.newColumn("Unité objectif de rendement", ItkActionSemiBean::getYealdUnit, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            m.newColumn("Espèces de l'action", ItkActionSemiBean::getActionSpecies);
            m.newColumn("Variétés de l'action", ItkActionSemiBean::getActionVarieties);
            m.newColumn("Traitement chimique des semences / plants", ItkActionSemiBean::getTreatment);
            m.newColumn("Inoculation biologique des semences / plants", ItkActionSemiBean::getBiologicalSeedInoculation);
            m.newColumn("Quantité semée", ItkActionSemiBean::getQuantity);
            m.newColumn("Unité quantité semée", ItkActionSemiBean::getSeedPlantUnit, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
            m.newColumn("Profondeur de semis", ItkActionSemiBean::getDeepness);
            m.newColumn("Commentaire", ItkActionSemiBean::getSeedingsActionSpeciesComment);

            // traitements semences
            m.newColumn("Groupes cibles", ItkActionSemiBean::getGroupesCibles);
            m.newColumn("Cibles du traitement", ItkActionSemiBean::getTargets);
            m.newColumn("Type de produit", ItkActionSemiBean::getPhytoProduct);

            // other input
            productInputColumns(m);

            m.newColumn("Dose de référence", ItkActionSemiBean::getReferenceDose);
            m.newColumn("Unité de dose de référence", ItkActionSemiBean::getReferenceDoseUnit, AgrosystI18nService::getEnumTraductionWithDefaultLocale);
        }
    }

    @Getter
    @Setter
    public static class ItkActionTransportBean extends Bean {

        Double loadCapacity;
        Double tripFrequency;
        CapacityUnit capacityUnit;

        public ItkActionTransportBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Transport";
//        }
//

        public static <T extends ItkActionTransportBean> void columns(ExportModel<T> m) {
            // intervention
            interventionColumns(m);

            // action
            actionColumns(m);
            m.newColumn("Volume de chargement par voyage", ItkActionTransportBean::getLoadCapacity);
            m.newColumn("Unité", ItkActionTransportBean::getCapacityUnit);
            m.newColumn("Nombre de voyage par heure", ItkActionTransportBean::getTripFrequency);
        }
    }

    @Getter
    @Setter
    public static class ItkActionTravailSolBean extends Bean {

        Double tillageDepth;
        String otherSettingTool;

        public ItkActionTravailSolBean(Bean baseBean) {
            super(baseBean);
        }

//        @Override
//        public String getTitle() {
//            return "Travail du sol";
//        }
//

        public static <T extends ItkActionTravailSolBean> void columns(ExportModel<T> m) {
            // action
            actionColumns(m);
            m.newColumn("Profondeur de travail du sol", ItkActionTravailSolBean::getTillageDepth);
            m.newColumn("Autres réglages de l'outil", ItkActionTravailSolBean::getOtherSettingTool);
        }
    }

    public record SpeciesAndVariety(RefEspece species, RefVariete variete, String edaplosUnknownVariety) {
        public String getSpeciesName() {
            return species.getLibelle_espece_botanique_Translated();
        }

        public String getVarietyName() {
                if (variete != null) {
                    return variete.getLabel();
                } else if (StringUtils.isNotBlank(edaplosUnknownVariety)) {
                    return I18n.t("fr.inra.agrosyst.api.services.edaplos.unknownVariety", edaplosUnknownVariety);
                } else {
                    return "";
                }
            }
        }
}
