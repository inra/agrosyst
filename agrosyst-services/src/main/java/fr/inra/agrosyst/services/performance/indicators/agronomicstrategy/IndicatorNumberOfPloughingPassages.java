package fr.inra.agrosyst.services.performance.indicators.agronomicstrategy;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.ToolsCoupling;
import fr.inra.agrosyst.api.entities.TypeTravailSol;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefCorrespondanceMaterielOutilsTS;
import fr.inra.agrosyst.api.entities.referential.RefMateriel;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.indicators.AbstractIndicator;
import fr.inra.agrosyst.services.performance.indicators.ExportLevel;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Nombre de passages de Labour.
 * <p>
 * Présentation de l’indicateur
 * <p>
 * Le nombre de passages de Labour est calculé à toutes les échelles hormis l'intrant et concerne les interventions
 * mobilisant une combinaison d'outil avec un outil de type de Labour selon le référentiel de travail du sol.
 * <p>
 * Le type de travail du sol est défini dans la colonne Type_travail_sol du référentiel de travail du sol.
 * <p>
 * A l'échelle de l'intervention le nombre de passages de labour correspond au PSCI.
 * <p>
 * Nb_passage_labour = PSCI
 * <p>
 * Si le matériel utilisé n'est pas de type Labour alors l'indicateur prend la valeur 0.
 * <p>
 * De la culture à la parcelle, l'indicateur se calcule en faisant l'addition des valeurs obtenues à l'échelle inférieure.
 * <p>
 * À l'échelle du SdC, il s'agit de faire une agrégation classique en faisant la moyenne des valeurs l'indicateur à
 * l'échelle de l'intervention de type Labour pour l'échelle SdC.
 * <p>
 * Formule :
 * <p>
 * Nb_passage_labour = (Σ (indicateur_echelle_intervention_labour))/Nb_intervention_labour
 * <p>
 * Avec Nb_intervention_labour = le nombre d'interventions de type labour dans le SdC.
 *
 * @author Geoffroy Gley (gley@codelutin.com)
 */
public class IndicatorNumberOfPloughingPassages extends AbstractIndicator {

    private static final String COLUMN_NAME = "nombre_de_passages_labour";

    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.INTERVENTION,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN
    );

    public IndicatorNumberOfPloughingPassages() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToColumnName;
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayed;
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_AGRONOMIC_STRATEGY);
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.nbOfPloughingPassages");
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformancePracticedDomainExecutionContext domainContext,
                                       PerformanceGrowingSystemExecutionContext growingSystemContext,
                                       PerformancePracticedSystemExecutionContext practicedSystemContext,
                                       PerformancePracticedCropExecutionContext cropContext,
                                       PerformancePracticedInterventionExecutionContext interventionContext,
                                       PracticedCropCyclePhase phase) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel == null ||
                interventionContext.isFictive() ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return newArray(1, 0.0);
        }

        // pour ne pas avoir 0, car il n'y a pas de données utilisateur sur cet indicateur
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());

        PracticedIntervention intervention = interventionContext.getIntervention();
        return computeNumberOfPassages(interventionContext.getToolsCoupling(), correspondanceByRefMateriel, this.getToolPSCi(intervention));
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext,
                                       PerformanceGlobalExecutionContext globalExecutionContext,
                                       PerformanceEffectiveDomainExecutionContext domainContext,
                                       PerformanceZoneExecutionContext zoneContext,
                                       PerformanceEffectiveCropExecutionContext cropContext,
                                       PerformanceEffectiveInterventionExecutionContext interventionContext) {

        final Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel = interventionContext.getCorrespondanceByRefMateriel();

        if (correspondanceByRefMateriel ==null ||
                interventionContext.getToolsCoupling() == null ||
                interventionContext.getToolsCoupling().getEquipments().stream().filter(Objects::nonNull).noneMatch(e -> correspondanceByRefMateriel.containsKey(e.getRefMateriel()))) {
            return newArray(1, 0.0);
        }

        // pour ne pas avoir 0, car il n'y a pas de données utilisateur sur cet indicateur
        incrementAngGetTotalFieldCounterForTargetedId(interventionContext.getInterventionId());

        EffectiveIntervention intervention = interventionContext.getIntervention();
        return computeNumberOfPassages(interventionContext.getToolsCoupling(), correspondanceByRefMateriel, this.getToolPSCi(intervention));
    }

    private Double[] computeNumberOfPassages(ToolsCoupling toolsCoupling, Map<RefMateriel, RefCorrespondanceMaterielOutilsTS> correspondanceByRefMateriel, double psci) {
        var hasLabourEquipment = false;

        if (toolsCoupling.getEquipments() != null) {
            hasLabourEquipment = toolsCoupling.getEquipments().stream()
                    .map(Equipment::getRefMateriel)
                    .filter(correspondanceByRefMateriel::containsKey)
                    .map(correspondanceByRefMateriel::get)
                    .anyMatch(refCorrespondance -> TypeTravailSol.LABOUR == refCorrespondance.getTypeTravailSol());
        }

        if (!hasLabourEquipment) {
            return newArray(1, 0.0);
        }
        return newArray(1,  psci);
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }
}
