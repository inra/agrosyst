package fr.inra.agrosyst.services.referential.json;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2016 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.refApi.RefActaDosageSaRootImpl;
import fr.inra.agrosyst.api.services.referential.ImportStatus;

import java.io.Serial;

/**
 * Created by davidcosse on 06/01/16.
 */
public class RefActaDosageSaRootExport extends RefActaDosageSaRootImpl implements ApiImportResults {

    @Serial
    private static final long serialVersionUID = -2839123887026039532L;

    public static final String PROPERTY_STATUS = "status";
    protected ImportStatus status;

    @Override
    public void setStatus(ImportStatus status) {
        this.status = status;
    }

    @Override
    public ImportStatus getStatus() {
        return status;
    }
}
