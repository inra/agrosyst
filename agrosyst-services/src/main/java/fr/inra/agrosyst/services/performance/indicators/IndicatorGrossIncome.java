package fr.inra.agrosyst.services.performance.indicators;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2024 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.action.HarvestingAction;
import fr.inra.agrosyst.api.entities.action.HarvestingActionValorisation;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceInterventionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.utils.PriceConverterKeysToRate;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.nuiton.i18n.I18n.l;

/**
 * Created by davidcosse on 28/09/16.

 * Formule de calcul:
 *
 * <pre>
 * Produit Brut réel sans autoconsommation _i:
 *
 * = PSCi * sum (Rendement(EVD) * Prix de vente(EVD) *  (% 𝑐𝑜𝑚𝑚𝑒𝑟𝑐𝑖𝑎𝑙𝑖𝑠é) * π )
 *
 * Avec :
 * - PB réel sans autoconso_i (€.ha-1) : Produit brut réel sans l’autoconsommation de l’intervention i.
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *     PSCi est calculé sur la base de données saisies par l’utilisateur.
 * - EVD : Couple Espèce*Variété*Destination appartenant à la liste des couples Espèce*Variété*Destination
 *     pour lesquels il a été déclaré un rendement dans l’intervention i.
 * - Rendement(EVD) (diverses unités) : Rendement déclaré pour le couple EVD de l’action de type Récolte de l’intervention i.
 * - Prix de vente(EVD) (diverses unités) : Prix de vente du couple EVD. Donnée saisie par l’utilisateur.
 * - (commercialisé): Part du rendement du couple EVD commercialisée. Donnée saisie par l’utilisateur.
 * _ π (unités diverses): Taux de conversion à appliquer en cas d’incohérence entre l’unité de rendement et l’unité de prix.
 *
 * </pre>
 *
 * <pre>
 * Produit Brut réel avec autoconsommation _i:
 *
 * = PSCi * sum (Rendement(EVD) * Prix de vente(EVD) *  (% 𝑐𝑜𝑚𝑚𝑒𝑟𝑐𝑖𝑎𝑙𝑖𝑠é + % autoconsommé) * π )
 *
 * Avec :
 * - PB réel sans autoconso_i (€.ha-1) : Produit brut réel sans l’autoconsommation de l’intervention i.
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *     PSCi est calculé sur la base de données saisies par l’utilisateur.
 * - EVD : Couple Espèce*Variété*Destination appartenant à la liste des couples Espèce*Variété*Destination
 *     pour lesquels il a été déclaré un rendement dans l’intervention i.
 * - Rendement(EVD) (diverses unités) : Rendement déclaré pour le couple EVD de l’action de type Récolte de l’intervention i.
 * - Prix de vente(EVD) (diverses unités) : Prix de vente du couple EVD. Donnée saisie par l’utilisateur.
 * - commercialisé (%): Part du rendement du couple EVD commercialisée. Donnée saisie par l’utilisateur.
 * - autoconsommé (%): Part du rendement du couple EVD autoconsommée. Donnée saisie par l’utilisateur.
 * _ π (unités diverses): Taux de conversion à appliquer en cas d’incohérence entre l’unité de rendement et l’unité de prix.
 *
 * </pre>
 *
 */
public class IndicatorGrossIncome extends AbstractIndicator {
    
    private static final Log LOGGER = LogFactory.getLog(IndicatorGrossIncome.class);

    private static final String[] LABELS = new String[]{
            "Indicator.label.grossIncomeWithoutAutoConsume",
            "Indicator.label.grossIncomeWithAutoConsume",
    };

    private boolean displayReal = true;
    private boolean isWithAutoConsumed = true;
    private boolean isWithoutAutoConsumed = true;
    
    protected final Set<ExportLevel> exportLevels = Sets.newHashSet(
            ExportLevel.PRICE,
            ExportLevel.CROP,
            ExportLevel.ZONE,
            ExportLevel.PLOT,
            ExportLevel.PRACTICED_SYSTEM,
            ExportLevel.GROWING_SYSTEM,
            ExportLevel.DOMAIN);
    
    public IndicatorGrossIncome() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, "produit_brut_reel_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, "produit_brut_reel_detail_champs_non_renseig");
    }
    
    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
    
        indicatorNameToColumnName.put(getIndicatorLabel(0), "produit_brut_reel_sans_autoconso");
        indicatorNameToColumnName.put(getIndicatorLabel(1), "produit_brut_reel_avec_autoconso");
    
        return indicatorNameToColumnName;
    }
    
    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return exportLevels.contains(atLevel) && displayReal && (i == 0 && isWithoutAutoConsumed || i == 1 && isWithAutoConsumed);
    }
    
    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return exportLevels.contains(atLevel);
    }
    
    @Override
    public String getIndicatorCategory() {
        return l(locale, INDICATOR_CATEGORY_ECONOMIC);
    }
    
    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, LABELS[i]);
    }
    
    /**
     * Formule de calcul:
     *
     * <pre>
     * Produit brut réel sans l’autoconsommation (€.ha-1):
     *
     * = PSCi * sum ( Rendement(EVD) * Prix de vente(EVD) * % commercialisé * TxUnitConverter )
     *
     * PSCi: (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
     * PSCi est calculé sur la base de données saisies par l’utilisateur.
     * Rendement(EVD): (diverses unités) : Rendement déclaré pour le couple EVD de l’action de type Récolte de l’intervention i.
     * Prix de vente(EVD): (diverses unités) : Prix de vente du couple EVD. Donnée saisie par l’utilisateur.
     *
     * Produit brut réel avec l’autoconsommation
     *
     * = PSCi * sum ( Rendement(EVD) * Prix de vente(EVD) * (% commercialisé + % non commercialisé )* TxUnitConverter )
     *
     * </pre>
     */
    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformancePracticedDomainExecutionContext domainContext,
            PerformanceGrowingSystemExecutionContext growingSystemContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedCropExecutionContext cropContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            PracticedCropCyclePhase phase) {
    
        if (interventionContext.isFictive()) return newArray(LABELS.length, 0.0d);
    
        Optional<HarvestingAction> optionalHarvestingAction = interventionContext.getOptionalHarvestingAction();
    
        if (optionalHarvestingAction.isEmpty()) {
            return null;
        }
    
        final HarvestingAction harvestingAction = optionalHarvestingAction.get();
        Collection<HarvestingActionValorisation> valorisations = CollectionUtils.emptyIfNull(harvestingAction.getValorisations())
                .stream().filter(Objects::nonNull).toList();
        if (CollectionUtils.isEmpty(valorisations)) {
            return null;
        }
    
        PracticedIntervention intervention = interventionContext.getIntervention();
        Double psci = getToolPSCi(intervention);
    
        List<HarvestingPrice> harvestingPrices = interventionContext.getHarvestingPrices();
        PracticedSystem practicedSystem = practicedSystemContext.getPracticedSystem();
        String practicedSystemCampaigns = practicedSystem.getCampaigns();
    
        // cet indicateur n'est pas écrit à l'échelle de l'intervention (n'a pas de sens)
        CroppingPlanEntry crop = interventionContext.getCropWithSpecies().getCroppingPlanEntry();
        Double[] withoutWithAutoConsumeValues = computeIndicatorForIntervention(
                harvestingAction,
                Optional.of(practicedSystemCampaigns),
                crop,
                interventionContext,
                psci,
                harvestingPrices);
    
        if (withoutWithAutoConsumeValues != null) {
            interventionContext.addProductWithoutAutoconsume(withoutWithAutoConsumeValues[0]);
            interventionContext.addProductWithAutoconsume(withoutWithAutoConsumeValues[1]);
        }
    
        return withoutWithAutoConsumeValues;
    }

    /*
     * @return gross product income with and without autoconsumed
     */
    @Override
    public Double[] manageIntervention(
            WriterContext writerContext,
            PerformanceGlobalExecutionContext globalExecutionContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveCropExecutionContext cropContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext) {

        Double[] withoutWithAutoConsumeValues;

        Optional<HarvestingAction> optionalHarvestingAction = interventionContext.getOptionalHarvestingAction();
        if (optionalHarvestingAction.isPresent() && optionalHarvestingAction.get().isValorisationsNotEmpty()) {
    
            EffectiveIntervention intervention = interventionContext.getIntervention();
            Double psci = getToolPSCi(intervention);

            List<HarvestingPrice> harvestingPrices = interventionContext.getHarvestingPrices();
            // main or intermediate crop
            CroppingPlanEntry crop = interventionContext.getCroppingPlanEntry();
            withoutWithAutoConsumeValues = computeIndicatorForIntervention(
                    optionalHarvestingAction.get(),
                    Optional.empty(),
                    crop,
                    interventionContext,
                    psci,
                    harvestingPrices
            );
    
            if (withoutWithAutoConsumeValues != null) {
                interventionContext.addProductWithoutAutoconsume(withoutWithAutoConsumeValues[0]);
                interventionContext.addProductWithAutoconsume(withoutWithAutoConsumeValues[1]);
            }
        } else {
            withoutWithAutoConsumeValues = newArray(LABELS.length, 0.0d);
        }
    
        return withoutWithAutoConsumeValues;
    }

    protected Double[] computeIndicatorForIntervention(
            HarvestingAction harvestingAction,
            Optional<String> optionalPracticedSystemCampaigns,
            CroppingPlanEntry crop,
            PerformanceInterventionContext interventionContext,
            Double psci,
            List<HarvestingPrice> prices) {

        double currentValueWithoutAutoConsume = 0d;
        double currentValueWithAutoConsume = 0d;

        String interventionId = interventionContext.getInterventionId();

        Collection<HarvestingActionValorisation> valorisations = harvestingAction.getValorisations();

        logNonValidValorisations(valorisations, interventionId);

        boolean isEdaplosDefaultValorisations = isEdaplosDefaultValorisations(valorisations);

        if (isEdaplosDefaultValorisations) {
            // yealdAverage
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            addMissingFieldMessage(interventionId, messageBuilder.getMissingValorisationDestinationMessage());
            return newResult(currentValueWithoutAutoConsume, currentValueWithAutoConsume);
        }

        Map<String, HarvestingActionValorisation> valorisationByIds = valorisations.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(HarvestingActionValorisation::getTopiaId, Function.identity()));

        boolean averageYealdsInsteadOfSum = crop.isCroppingPlanSpeciesNotEmpty()
                && crop.sizeCroppingPlanSpecies() > 1
                && !crop.isMixSpecies()
                && !crop.isMixVariety();

        Set<String> actionSpeciesCodes = valorisations.stream()
                .filter(Objects::nonNull)
                .map(HarvestingActionValorisation::getSpeciesCode)
                .collect(Collectors.toSet());

        double totalAreaRatio;
        Map<String, Integer> speciesAreasByCode;

        if (averageYealdsInsteadOfSum && crop.isCroppingPlanSpeciesNotEmpty()) {

            speciesAreasByCode = crop.getCroppingPlanSpecies().stream()
                    .filter(species -> actionSpeciesCodes.contains(species.getCode()))
                    .collect(Collectors.toMap(CroppingPlanSpecies::getCode, croppingPlanSpecies -> {
                        Integer speciesArea = croppingPlanSpecies.getSpeciesArea();
                        incrementAngGetTotalFieldCounterForTargetedId(interventionId);
                        if (speciesArea == null) {
                            addMissingFieldMessage(interventionId, messageBuilder.getMissingSurfaceMessage(croppingPlanSpecies));
                            speciesArea = 100 / actionSpeciesCodes.size();
                        }
                        return speciesArea;
                    }));

            int areaSum = speciesAreasByCode.values().stream()
                    .mapToInt(a -> a)
                    .sum();
            totalAreaRatio = areaSum == 0 ? 0 : (double) 100 / areaSum;

        } else {
            totalAreaRatio = 1;
            speciesAreasByCode = new HashMap<>();
        }
        // filter prices for current valorisations

        prices = CollectionUtils.emptyIfNull(prices).stream().filter(p -> valorisationByIds.get(p.getHarvestingActionValorisation().getTopiaId()) != null).toList();

        Set<String> speciesCodes = valorisationByIds.values().stream().map(HarvestingActionValorisation::getSpeciesCode).collect(Collectors.toSet());
        Map<String, Set<Pair<String, String>>>  codeEspBotCodeQualiBySpeciesCode = croppingPlanSpeciesDao.loadCodeEspeceBotaniqueCodeQualifantAeeForSpeciesCodes(speciesCodes);

        LinkedHashMap<HarvestingActionValorisation, HarvestingPrice> pricesByValorisation = new LinkedHashMap<>();

        prices.forEach(p -> pricesByValorisation.put(p.getHarvestingActionValorisation(), p));

        CollectionUtils.emptyIfNull(valorisationByIds.values()).forEach(
                val -> pricesByValorisation.putIfAbsent(val, null)
        );

        Map<RefDestination, PriceUnit> priceUnitForDestination = new HashMap<>();
        for (Map.Entry<HarvestingActionValorisation, HarvestingPrice> valToPrice : pricesByValorisation.entrySet()) {
            HarvestingPrice price = valToPrice.getValue();

            HarvestingActionValorisation valorisation = valToPrice.getKey();

            // to be sure to have the effective valorisation
            valorisation = valorisationByIds.get(valorisation.getTopiaId());

            // price
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            double sellingPrice;
            PriceUnit currentPriceUnit;
            if (price == null || price.getPrice() == null) {
                addMissingFieldMessage(interventionId, messageBuilder.getMissingHarvestingPriceMessage(crop));
                currentPriceUnit = priceUnitForDestination.get(valorisation.getDestination());
                // Taux de conversion à appliquer en cas d’incohérence entre l’unité de rendement et l’unité de prix.
                PriceAndUnit averageRefSellingPriceForPriceUnit = null;

                addMissingFieldMessage(interventionId, messageBuilder.getMissingHarvestingPriceMessage(crop));
                // depuis le #10240 on utilise le prix moyen des prix de références en remplacement du prix médian
                Pair<Boolean, PriceAndUnit> optionalAverageRefSellingPriceForPriceUnit =  interventionContext.getValorisationRefPrice(valorisation);

                if (!optionalAverageRefSellingPriceForPriceUnit.getLeft()) {// le prix de référence n'a pas été encore cherché
                    try {
                        // on essaie de trouver un prix avec la même unité que les valorisations aux mêmes destinations précédentes
                        averageRefSellingPriceForPriceUnit = performanceService.loadHarvestingPriceWithUnitRefPriceForValorisation(
                                optionalPracticedSystemCampaigns,
                                Optional.ofNullable(currentPriceUnit),
                                interventionId,
                                valorisation,
                                codeEspBotCodeQualiBySpeciesCode);

                        if (averageRefSellingPriceForPriceUnit == null) {
                            averageRefSellingPriceForPriceUnit = performanceService.loadHarvestingPriceWithUnitRefPriceForValorisation(
                                    optionalPracticedSystemCampaigns,
                                    Optional.empty(),
                                    interventionId,
                                    valorisation,
                                    codeEspBotCodeQualiBySpeciesCode);
                        }

                        interventionContext.addValorisationRefPrice(valorisation, averageRefSellingPriceForPriceUnit);

                    } catch (Exception e) {
                        if (LOGGER.isWarnEnabled()) {
                            LOGGER.warn(
                                    String.format("ÉCHEC DU CALCUL DU PRIX DE RÉFÉRENCE POUR LA VALORISATION '%s' ET L'INTERVENTION %s",
                                            valorisation.getTopiaId(),
                                            interventionId));

                            LOGGER.warn(e);
                        }
                    }
                } else {
                    averageRefSellingPriceForPriceUnit = optionalAverageRefSellingPriceForPriceUnit.getRight();
                }

                PriceUnit priceUnit;
                if (averageRefSellingPriceForPriceUnit != null) {
                    priceUnit = averageRefSellingPriceForPriceUnit.unit();
                    sellingPrice = averageRefSellingPriceForPriceUnit.value();

                } else {
                    priceUnit = PricesService.DEFAULT_PRICE_UNIT;
                    sellingPrice = 0d;
                }
                currentPriceUnit = priceUnit;
            } else {
                sellingPrice = price.getPrice();
                currentPriceUnit = price.getPriceUnit();
            }

            priceUnitForDestination.putIfAbsent(valorisation.getDestination(), currentPriceUnit);

            // salesPercent
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            double salesPercent = valorisation.getSalesPercent() / 100.0d;

            //selfConsumePersent
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);
            double selfConsumePersent = valorisation.getSelfConsumedPersent() / 100.0d;

            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            PriceConverterKeysToRate priceConverterKeysToRate = interventionContext.getPriceConverterKeysToRate();
            Optional<Double> conversionRate = priceConverterKeysToRate.getPriceConverterRate(
                    valorisation.getDestination().getCode_destination_A(),
                    currentPriceUnit);

            if (conversionRate.isEmpty() && !PricesService.DEFAULT_PRICE_UNIT.equals(currentPriceUnit)) {
                addMissingRefConversionRateMessageMessage(interventionId);
            }

            double unitRateConverterValue = conversionRate.orElse(Indicator.DEFAULT_PRICE_CONVERTION_RATE);

            // yealdAverage
            incrementAngGetTotalFieldCounterForTargetedId(interventionId);

            // part relative du couple espèce*variété
            double yealdAverage = valorisation.getYealdAverage();
            if (averageYealdsInsteadOfSum &&  speciesAreasByCode.containsKey(valorisation.getSpeciesCode())) {
                Integer speciesArea = speciesAreasByCode.get(valorisation.getSpeciesCode());
                yealdAverage = yealdAverage * speciesArea * totalAreaRatio / 100.0d;
            }
            // #10856 multiplier par 1 si €/ha et pas par le rendement moyen
            // Lorsqu'un utilisateur saisit un prix en €/ha pour une récolte,
            // la quantité récoltée déclarée ne doit pas être prise en compte,
            // on doit considérer, comme pour les intrants, une quantité de 1.
            if (PricesService.DEFAULT_PRICE_UNIT.equals(currentPriceUnit)) {
                double amount = sellingPrice * unitRateConverterValue;
                currentValueWithoutAutoConsume += amount;
                currentValueWithAutoConsume += amount;
            } else {
                currentValueWithoutAutoConsume += yealdAverage * sellingPrice * salesPercent * unitRateConverterValue;
                currentValueWithAutoConsume += yealdAverage * sellingPrice * (salesPercent + selfConsumePersent) * unitRateConverterValue;
            }

        }
        double resWithoutAutoConsume = psci * currentValueWithoutAutoConsume;
        double resWithAutoConsume = psci * currentValueWithAutoConsume;

        return newResult(resWithoutAutoConsume, resWithAutoConsume);
    }

    public void init(IndicatorFilter grossIncomeIndicatorFilter) {
        displayReal = grossIncomeIndicatorFilter != null && grossIncomeIndicatorFilter.getComputeReal();
        if (displayReal) {
            isWithoutAutoConsumed = grossIncomeIndicatorFilter.getWithoutAutoConsumed() != null && grossIncomeIndicatorFilter.getWithoutAutoConsumed();
            isWithAutoConsumed = grossIncomeIndicatorFilter.getWithAutoConsumed() != null && grossIncomeIndicatorFilter.getWithAutoConsumed();
        }
    }
}
