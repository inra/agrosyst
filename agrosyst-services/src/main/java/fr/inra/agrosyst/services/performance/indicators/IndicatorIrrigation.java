package fr.inra.agrosyst.services.performance.indicators;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.performance.IndicatorFilter;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGlobalExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceGrowingSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedCropExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;

import java.util.Map;

import static org.nuiton.i18n.I18n.l;

/**
 * <p>
 *     Indicateur d'irrigation.
 * </p>
 *
 * <p>
 *     <pre>Consommation d'eau (mm) = Quantité moyenne d'eau apportée (mm) * PSCI</pre>
 * </p>
 *
 * <p>
 *     La quantité moyenne d'eau est renseignée par les utilisateurs au niveau de la saisie d'une intervention d'irrigation.
 * </p>
 *
 * @see fr.inra.agrosyst.api.entities.action.IrrigationAction
 */
public class IndicatorIrrigation extends AbstractIndicator {
    public static final String COLUMN_NAME = "consommation_eau";

    public IndicatorIrrigation() {
        extraFields.put(OptionalExtraColumnEnumKey.TAUX_DE_COMPLETION, COLUMN_NAME + "_taux_de_completion");
        extraFields.put(OptionalExtraColumnEnumKey.DETAIL_CHAMPS_NON_RENSEIGNES, COLUMN_NAME + "_detail_champs_non_renseig");
    }

    @Override
    public Map<String, String> getIndicatorNameToDbColumnName() {
        Map<String, String> indicatorNameToColumnName = super.getIndicatorNameToDbColumnName();
        indicatorNameToColumnName.put(getIndicatorLabel(0), COLUMN_NAME);
        return indicatorNameToColumnName;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformancePracticedDomainExecutionContext domainContext, PerformanceGrowingSystemExecutionContext growingSystemContext, PerformancePracticedSystemExecutionContext practicedSystemContext, PerformancePracticedCropExecutionContext cropContext, PerformancePracticedInterventionExecutionContext interventionContext, PracticedCropCyclePhase phase) {
        Double[] result = newArray(1, 0.0);

        if (interventionContext.isFictive()) {
            return result;
        }

        interventionContext.getOptionalIrrigationAction().ifPresent(irrigationAction -> {
            final double psci = getPhytoActionPSCi(interventionContext.getIntervention(), irrigationAction);
            final double waterConsumption = irrigationAction.getWaterQuantityAverage() * psci;
            result[0] = waterConsumption;
        });

        return result;
    }

    @Override
    public Double[] manageIntervention(WriterContext writerContext, PerformanceGlobalExecutionContext globalExecutionContext, PerformanceEffectiveDomainExecutionContext domainContext, PerformanceZoneExecutionContext zoneContext, PerformanceEffectiveCropExecutionContext cropContext, PerformanceEffectiveInterventionExecutionContext interventionContext) {
        Double[] result = newArray(1, 0.0);

        interventionContext.getOptionalIrrigationAction().ifPresent(irrigationAction -> {
            final double psci = getPhytoActionPSCi(interventionContext.getIntervention(), irrigationAction);
            final double waterConsumption = irrigationAction.getWaterQuantityAverage() * psci;
            result[0] = waterConsumption;
        });

        return result;
    }

    @Override
    protected boolean isRelevant(ExportLevel atLevel) {
        return true;
    }

    @Override
    public String getIndicatorCategory() {
        return l(locale, "Indicator.category.socialPerformance");
    }

    @Override
    public String getIndicatorLabel(int i) {
        return l(locale, "Indicator.label.waterConsumption");
    }

    @Override
    protected boolean isDisplayed(ExportLevel atLevel, int i) {
        return displayed;
    }

    public void init(IndicatorFilter filter) {
        displayed = filter != null;
    }
}
