package fr.inra.agrosyst.services.context;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.inra.agrosyst.api.NavigationContext;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingPlan;
import fr.inra.agrosyst.api.entities.GrowingPlanTopiaDao;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.GrowingSystemTopiaDao;
import fr.inra.agrosyst.api.entities.Network;
import fr.inra.agrosyst.api.entities.NetworkTopiaDao;
import fr.inra.agrosyst.api.services.context.NavigationContextService;
import fr.inra.agrosyst.api.services.domain.DomainDto;
import fr.inra.agrosyst.api.services.domain.DomainFilter;
import fr.inra.agrosyst.api.services.domain.DomainService;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanDto;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanFilter;
import fr.inra.agrosyst.api.services.growingplan.GrowingPlanService;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemDto;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemFilter;
import fr.inra.agrosyst.api.services.growingsystem.GrowingSystemService;
import fr.inra.agrosyst.api.services.network.NetworkFilter;
import fr.inra.agrosyst.api.services.network.NetworkService;
import fr.inra.agrosyst.api.services.security.AnonymizeService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationResult;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;

/**
 * @author Arnaud Thimel : thimel@codelutin.com
 */
public class NavigationContextServiceImpl extends AbstractAgrosystService implements NavigationContextService {

    private static final Log LOGGER = LogFactory.getLog(NavigationContextServiceImpl.class);

    protected NetworkService networkService;
    protected DomainService domainService;
    protected GrowingPlanService growingPlanService;
    protected GrowingSystemService growingSystemService;
    protected AnonymizeService anonymizeService;

    protected NetworkTopiaDao networkDao;
    protected DomainTopiaDao domainDao;
    protected GrowingPlanTopiaDao growingPlanDao;
    protected GrowingSystemTopiaDao growingSystemDao;

    public void setNetworkService(NetworkService networkService) {
        this.networkService = networkService;
    }

    public void setDomainService(DomainService domainService) {
        this.domainService = domainService;
    }

    public void setGrowingPlanService(GrowingPlanService growingPlanService) {
        this.growingPlanService = growingPlanService;
    }

    public void setGrowingSystemService(GrowingSystemService growingSystemService) {
        this.growingSystemService = growingSystemService;
    }

    public void setAnonymizeService(AnonymizeService anonymizeService) {
        this.anonymizeService = anonymizeService;
    }

    public void setNetworkDao(NetworkTopiaDao networkDao) {
        this.networkDao = networkDao;
    }

    public void setDomainDao(DomainTopiaDao domainDao) {
        this.domainDao = domainDao;
    }

    public void setGrowingPlanDao(GrowingPlanTopiaDao growingPlanDao) {
        this.growingPlanDao = growingPlanDao;
    }

    public void setGrowingSystemDao(GrowingSystemTopiaDao growingSystemDao) {
        this.growingSystemDao = growingSystemDao;
    }

    @Override
    public List<Integer> getAllCampaigns() {
        return domainDao.getAllCampaigns();
    }

    @Override
    public PaginationResult<Network> getAllNetworks(int nbElements) {
        NetworkFilter activeFilter = new NetworkFilter();
        activeFilter.setPageSize(nbElements);
        activeFilter.setActive(true);
        return networkService.getFilteredNetworks(activeFilter);
    }

    @Override
    public PaginationResult<DomainDto> getAllDomainsForCampaign(Set<Integer> campaigns, Set<String> networkIds, int nbElements) {

        NavigationContext navigationContext = new NavigationContext(campaigns, networkIds, null, null, null);

        DomainFilter filter = new DomainFilter();
        filter.setPageSize(nbElements);
        filter.setNavigationContext(navigationContext);
        filter.setActive(true);

        return domainService.getFilteredDomainsDto(filter);
    }

    @Override
    public PaginationResult<GrowingPlanDto> getAllGrowingPlansForDomains(Set<Integer> campaigns, Set<String> domainIds, Set<String> networkIds, int nbElements) {
        NavigationContext navigationContext = new NavigationContext(campaigns, networkIds, domainIds, null, null);

        GrowingPlanFilter filter = new GrowingPlanFilter();
        filter.setPageSize(nbElements);
        filter.setNavigationContext(navigationContext);
        filter.setActive(true);

        return growingPlanService.getFilteredGrowingPlansDto(filter);
    }

    @Override
    public PaginationResult<GrowingSystemDto> getAllGrowingSystemsForGrowingPlans(
            Set<Integer> campaigns, Set<String> domainIds, Set<String> growingPlansIds, Set<String> networkIds, int nbElements) {
        
        NavigationContext navigationContext = new NavigationContext(campaigns, networkIds, domainIds, growingPlansIds, null);

        GrowingSystemFilter filter = new GrowingSystemFilter();
        filter.setPageSize(nbElements);
        filter.setNavigationContext(navigationContext);
        filter.setActive(true);

        return growingSystemService.getFilteredGrowingSystemsDto(filter);
    }

    @Override
    public NavigationContext verify(NavigationContext navigationContext) {
        Preconditions.checkArgument(navigationContext != null);
        
        final Set<Integer> campaigns = new HashSet<>();
        final Set<String> networks = new HashSet<>();
        final Set<String> domains = new HashSet<>();
        final Set<String> growingPlans = new HashSet<>();
        final Set<String> growingSystems = new HashSet<>();
    
        final Set<String> networksIds = new HashSet<>();
    
        if (navigationContext.getNetworksCount() > 0) {
            for (String networkId : navigationContext.getNetworks()) {
                Network network = networkDao.forTopiaIdEquals(networkId).findUniqueOrNull();
                if (network != null) {
                    networks.add(networkId);
                } else if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(String.format("The network (%s) does not exist.", networkId));
                }
            }
            networksIds.addAll(domainDao.networksToDomains(networks));
        }
        
        final Predicate<Domain> isValidDomain = ((Predicate<Domain>) Objects::nonNull).and(domain -> {
            boolean result13 = campaigns.isEmpty() || campaigns.contains(domain.getCampaign());
            result13 &= networksIds.isEmpty() || networksIds.contains(domain.getTopiaId());
            
            return result13;
        });

        final Predicate<GrowingPlan> isValidGrowingPlan = ((Predicate<GrowingPlan>) Objects::nonNull).and(growingPlan -> {
            Domain domain = growingPlan.getDomain();
            boolean result12 = isValidDomain.test(domain);
            result12 &= domains.isEmpty() || domains.contains(domain.getTopiaId());
            return result12;
        });

        final Predicate<GrowingSystem> isValidGrowingSystem = ((Predicate<GrowingSystem>) Objects::nonNull).and(growingSystem -> {
            GrowingPlan growingPlan = growingSystem.getGrowingPlan();
            boolean result1 = isValidGrowingPlan.test(growingPlan);
            result1 &= growingPlans.isEmpty() || growingPlans.contains(growingPlan.getTopiaId());
            return result1;
        });

        if (navigationContext.getCampaigns() != null) {
            campaigns.addAll(navigationContext.getCampaigns());
        }

        if (navigationContext.getDomainsCount() > 0) {
            for (String domainId : navigationContext.getDomains()) {
                Domain domain = domainDao.forTopiaIdEquals(domainId).findUniqueOrNull();
                if (isValidDomain.test(domain)) {
                    domains.add(domainId);
                } else if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(String.format("The domain (%s) does not exist or does not match any of the campaigns %s.", domainId, campaigns));
                }
            }
        }

        if (navigationContext.getGrowingPlansCount() > 0) {
            for (String growingPlanId : navigationContext.getGrowingPlans()) {
                GrowingPlan growingPlan = growingPlanDao.forTopiaIdEquals(growingPlanId).findUniqueOrNull();
                if (isValidGrowingPlan.test(growingPlan)) {
                    growingPlans.add(growingPlanId);
                } else if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(String.format("The growingPlan (%s) does not exist or does not match any of the campaigns %s or domains %s.", growingPlanId, campaigns, domains));
                }
            }
        }

        if (navigationContext.getGrowingSystemsCount() > 0) {
            for (String growingSystemId : navigationContext.getGrowingSystems()) {
                GrowingSystem growingSystem = growingSystemDao.forTopiaIdEquals(growingSystemId).findUniqueOrNull();
                if (isValidGrowingSystem.test(growingSystem)) {
                    growingSystems.add(growingSystemId);
                } else if (LOGGER.isWarnEnabled()) {
                    LOGGER.warn(String.format("The growingSystem (%s) does not exist or does not match any of the campaigns %s, domains %s or growingPlans %s.", growingSystemId, campaigns, domains, growingPlans));
                }
            }
        }

        return new NavigationContext(campaigns, networks, domains, growingPlans, growingSystems);
    }

    @Override
    public NavigationContext verify(NavigationContext navigationContext, TopiaEntity newEntity) {
        Preconditions.checkArgument(navigationContext != null);
        String entityId = newEntity.getTopiaId();
        Preconditions.checkNotNull(entityId);
        Integer campaign = null;
        if (newEntity instanceof Network) {
            if (navigationContext.getNetworksCount() >= 1) {
                navigationContext.getNetworks().add(entityId);
            }
        } else if (newEntity instanceof Domain) {
            if (navigationContext.getDomainsCount() >= 1) {
                navigationContext.getDomains().add(entityId);
            }
            campaign = ((Domain) newEntity).getCampaign();
        } else if (newEntity instanceof GrowingPlan) {
            if (navigationContext.getGrowingPlansCount() >= 1) {
                navigationContext.getGrowingPlans().add(entityId);
            }
            campaign = ((GrowingPlan) newEntity).getDomain().getCampaign();

        } else if (newEntity instanceof GrowingSystem) {
            if (navigationContext.getGrowingSystemsCount() >= 1) {
                navigationContext.getGrowingSystems().add(entityId);
            }
            campaign = ((GrowingSystem) newEntity).getGrowingPlan().getDomain().getCampaign();
        } else {
            String message = "This method is supposed to be used with new " +
                    "Domain/GrowingPlan/GrowingSystem/Network entities only";
            throw new UnsupportedOperationException(message);
        }
        if (campaign != null && navigationContext.getCampaignsCount() >= 1) {
            navigationContext.getCampaigns().add(campaign);
        }
        return verify(navigationContext);
    }

    @Override
    public Map<String, String> getNetworks(Set<String> networkIds, int maxCount) {

        Map<String, String> result = new LinkedHashMap<>();
        if (networkIds != null && !networkIds.isEmpty()) {
            int max = maxCount >= 0 ? maxCount : -1;
            List<Network> networks = networkDao.forTopiaIdIn(networkIds)
                    .setOrderByArguments(Network.PROPERTY_NAME)
                    .find(0, max);

            for (Network network : networks) {
                result.put(network.getTopiaId(), network.getName());
            }
        }
        return result;
    }

    @Override
    public Map<String, String> getDomains(Set<String> domainsIds, int maxCount) {
        // TODO AThimel 26/02/14 Introduce cache (user dependent)

        Map<String, String> result = new LinkedHashMap<>();
        if (domainsIds != null && !domainsIds.isEmpty()) {
            int max = Math.max(maxCount, -1);
            List<Domain> domains = domainDao.forTopiaIdIn(domainsIds)
                    .setOrderByArguments(Domain.PROPERTY_NAME)
                    .find(0, max);

            Map<String, String> domainsMap = anonymizeService.getDomainsAsMap(domains);
            result.putAll(domainsMap);
        }
        return result;
    }

    @Override
    public Map<String, String> getGrowingPlans(Set<String> growingPlansIds, int maxCount) {
        // TODO AThimel 26/02/14 Introduce cache (user dependent)

        Map<String, String> result = new LinkedHashMap<>();
        if (growingPlansIds != null && !growingPlansIds.isEmpty()) {
            int max = Math.max(maxCount, -1);
            List<GrowingPlan> growingPlans = growingPlanDao.forTopiaIdIn(growingPlansIds)
                    .setOrderByArguments(GrowingPlan.PROPERTY_NAME)
                    .find(0, max);

            Map<String, String> growingPlansMap = anonymizeService.getGrowingPlansAsMap(growingPlans);
            result.putAll(growingPlansMap);
        }
        return result;
    }

    @Override
    public Map<String, String> getGrowingSystems(Set<String> growingSystemsIds, int maxCount) {
        // TODO AThimel 26/02/14 Introduce cache (user dependent)

        Map<String, String> result = new LinkedHashMap<>();

        if (growingSystemsIds != null && !growingSystemsIds.isEmpty()) {
            int max = Math.max(maxCount, -1);
            List<GrowingSystem> growingSystems = growingSystemDao.forTopiaIdIn(growingSystemsIds)
                    .setOrderByArguments(GrowingSystem.PROPERTY_NAME)
                    .find(0, max);

            for (GrowingSystem growingSystem : growingSystems) {
                result.put(growingSystem.getTopiaId(),
                        String.format("%s (%s)", growingSystem.getName(), growingSystem.getGrowingPlan().getDomain().getCampaign()));
            }
        }
        return result;
    }

}
