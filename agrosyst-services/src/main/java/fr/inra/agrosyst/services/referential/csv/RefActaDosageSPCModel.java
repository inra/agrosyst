package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPC;
import fr.inra.agrosyst.api.entities.referential.RefActaDosageSPCImpl;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author Eric Chatellier
 */
public class RefActaDosageSPCModel extends AbstractAgrosystModel<RefActaDosageSPC> implements ExportModel<RefActaDosageSPC> {

    protected static final Map<String, PhytoProductUnit> PHYTO_PRODUCT_UNIT_TO_ENUM = new LinkedHashMap<>();
    protected static final Map<PhytoProductUnit, String> PHYTO_PRODUCT_UNIT_ENUM_TO_STRING = new HashMap<>();

    static {
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("Aa/ha", PhytoProductUnit.AA_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("adulte/m²", PhytoProductUnit.ADULTES_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("adultes/m²", PhytoProductUnit.ADULTES_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("Ana-line/semaine/ha", PhytoProductUnit.ANA_LINE_SEMAINE_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("auxiliaire/m²", PhytoProductUnit.AUXILIAIRES_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("auxiliaires/m²", PhytoProductUnit.AUXILIAIRES_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("cartonette/ha", PhytoProductUnit.CARTONETTES_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("cartonettes/ha", PhytoProductUnit.T_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("diffuseurs/ha", PhytoProductUnit.DIFFUSEURS_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("Di/ha", PhytoProductUnit.DI_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("dose/200 m²", PhytoProductUnit.DOSES_200M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("doses/200 m²", PhytoProductUnit.DOSES_200M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("Ds/ha", PhytoProductUnit.DS_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("Ds/m²", PhytoProductUnit.DS_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("élevage/500 m²", PhytoProductUnit.ELEVAGES_500M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("élevages/500 m²", PhytoProductUnit.ELEVAGES_500M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/100 kg", PhytoProductUnit.G_100KG);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/100 L d'eau", PhytoProductUnit.G_100L_D_EAU);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/100 m2", PhytoProductUnit.G_100M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/100 m²", PhytoProductUnit.G_100M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/10 m²", PhytoProductUnit.G_10M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/160 m²", PhytoProductUnit.G_160M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/bouture", PhytoProductUnit.G_BOUTURE);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/ha", PhytoProductUnit.G_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/hl", PhytoProductUnit.G_HL);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/kg", PhytoProductUnit.G_KG);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/L/10 m²", PhytoProductUnit.G_L_10M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/L", PhytoProductUnit.G_L);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/m2", PhytoProductUnit.G_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/m²", PhytoProductUnit.G_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/m3", PhytoProductUnit.G_M3);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/m³", PhytoProductUnit.G_M3);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/palme", PhytoProductUnit.G_PALME);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g", PhytoProductUnit.G);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/pied", PhytoProductUnit.G_PIED);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/plant", PhytoProductUnit.G_PLANT);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/q", PhytoProductUnit.G_Q);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/t", PhytoProductUnit.G_T);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("g/unité de semences", PhytoProductUnit.G_UNITE_SEMENCES);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("Hm/m²", PhytoProductUnit.HM_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("individus/arbre", PhytoProductUnit.INDIVIDUS_ARBRE);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("individus/foyer", PhytoProductUnit.INDIVIDUS_FOYER);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("individus/ha", PhytoProductUnit.INDIVIDUS_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("individus/m²", PhytoProductUnit.INDIVIDUS_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ind/m²", PhytoProductUnit.IND_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/100 m²", PhytoProductUnit.KG_100M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/ha", PhytoProductUnit.KG_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/hl", PhytoProductUnit.KG_HL);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/hL", PhytoProductUnit.KG_HL);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/m2", PhytoProductUnit.KG_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/m²", PhytoProductUnit.KG_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/m³", PhytoProductUnit.KG_M3);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/q", PhytoProductUnit.KG_Q);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/t", PhytoProductUnit.KG_T);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("kg/unité", PhytoProductUnit.KG_UNITE);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/100000 graines", PhytoProductUnit.L_100000_GRAINES);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("l/1000m²", PhytoProductUnit.L_1000M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/1000 plants", PhytoProductUnit.L_1000PLANTS);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/100 m²", PhytoProductUnit.L_100M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/100m²", PhytoProductUnit.L_100M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/100 m³", PhytoProductUnit.L_100M3);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/10 m²", PhytoProductUnit.L_10M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("larve/50 pucerons", PhytoProductUnit.LARVES_50PUCERONS);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("larves/50 pucerons", PhytoProductUnit.LARVES_50PUCERONS);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("larves/5 à 10 m²", PhytoProductUnit.LARVES_5_A_10M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("larves d'Ab/colonie de pucerons", PhytoProductUnit.LARVES_D_AB_COLONIE_DE_PUCERONS);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("larves/m²", PhytoProductUnit.LARVES_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/ha", PhytoProductUnit.L_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/hl", PhytoProductUnit.L_HL);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/hL", PhytoProductUnit.L_HL);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/kg appat", PhytoProductUnit.L_KG_APPAT);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/kg", PhytoProductUnit.L_KG);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/m2", PhytoProductUnit.L_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/m²", PhytoProductUnit.L_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/m³", PhytoProductUnit.L_M3);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/palmier", PhytoProductUnit.L_PALMIER);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/q", PhytoProductUnit.L_Q);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/t", PhytoProductUnit.L_T);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/T", PhytoProductUnit.L_T);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/unité de semences", PhytoProductUnit.L_UNITE_SEMENCES);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L/unité", PhytoProductUnit.L_UNITE);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("L ou kg/kg", PhytoProductUnit.L_OU_KG_KG);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("/m²", PhytoProductUnit.PER_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("mg/plant", PhytoProductUnit.MG_PLANT);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("milliard/ha", PhytoProductUnit.MILLIARDS_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("milliards/ha", PhytoProductUnit.MILLIARDS_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("million/arbre", PhytoProductUnit.MILLIONS_ARBRE);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("million/m²", PhytoProductUnit.MILLIONS_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("millions/100 m²", PhytoProductUnit.MILLIONS_100M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("millions/arbre", PhytoProductUnit.MILLIONS_ARBRE);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("millions/L de bouillie", PhytoProductUnit.MILLIONS_L_BOUILLIE);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("millions/m²", PhytoProductUnit.MILLIONS_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/100 m²", PhytoProductUnit.ML_100M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/10 m²", PhytoProductUnit.ML_10M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/5000 graines", PhytoProductUnit.ML_5000GRAINES);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/ha", PhytoProductUnit.ML_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/kg", PhytoProductUnit.ML_KG);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/L", PhytoProductUnit.ML_L);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/m2", PhytoProductUnit.ML_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/m²", PhytoProductUnit.ML_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/t", PhytoProductUnit.ML_T);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("ml/unité", PhytoProductUnit.ML_UNIT);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("momie/m²", PhytoProductUnit.MOMIES_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("momies/500 m²", PhytoProductUnit.MOMIES_500M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("momies/m²", PhytoProductUnit.MOMIES_M2);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("%", PhytoProductUnit.PERCENT);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("pièges/ha", PhytoProductUnit.PIEGES_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("sachet/ha", PhytoProductUnit.SACHETS_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("sachets/ha", PhytoProductUnit.SACHETS_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("tablettes/ha", PhytoProductUnit.TABLETTES_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("Ta/ha", PhytoProductUnit.TA_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("t/ha", PhytoProductUnit.T_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("unité/ha", PhytoProductUnit.UNITE_HA);
        PHYTO_PRODUCT_UNIT_TO_ENUM.put("unité/hL", PhytoProductUnit.UNITE_HL);
    }

    protected static final ValueParser<PhytoProductUnit> PHYTO_PRODUCT_UNIT_PARSER = value -> {

        PhytoProductUnit result = null;
        if (StringUtils.isNotEmpty(value)) {
            result = PHYTO_PRODUCT_UNIT_TO_ENUM.get(value);
            result = result == null ? (PhytoProductUnit) getGenericEnumParser(PhytoProductUnit.class, value) : result;
            if (result == null) {
                throw new UnsupportedOperationException("Unexpected value: " + value);
            }
        }
        return result;
    };

    public static final ValueFormatter<PhytoProductUnit> PHYTO_PRODUCT_UNIT_FORMATTER = value -> {
        String result;
        if (value != null) {
            result = PHYTO_PRODUCT_UNIT_ENUM_TO_STRING.get(value);
            if (StringUtils.isEmpty(result)) {
                Optional<Map.Entry<String, PhytoProductUnit>> optional = PHYTO_PRODUCT_UNIT_TO_ENUM.entrySet().stream()
                        .filter(input -> value.equals(input.getValue()))
                        .findFirst();
                if (optional.isPresent()) {
                    result = optional.get().getKey();
                    PHYTO_PRODUCT_UNIT_ENUM_TO_STRING.put(value, result);
                }
            }
            if (result == null) {
                result = value.name().toLowerCase();
            }
        } else {
            result = "";
        }
        return result;
    };

    public RefActaDosageSPCModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("id_produit", RefActaDosageSPC.PROPERTY_ID_PRODUIT);
        newMandatoryColumn("nom_produit", RefActaDosageSPC.PROPERTY_NOM_PRODUIT);
        newMandatoryColumn("id_traitement", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, INT_PARSER);
        newMandatoryColumn("code_traitement", RefActaDosageSPC.PROPERTY_CODE_TRAITEMENT);
        newMandatoryColumn("id_culture", RefActaDosageSPC.PROPERTY_ID_CULTURE, INT_PARSER);
        newMandatoryColumn("nom_culture", RefActaDosageSPC.PROPERTY_NOM_CULTURE);
        newMandatoryColumn("remarque_culture", RefActaDosageSPC.PROPERTY_REMARQUE_CULTURE);
        newMandatoryColumn("dosage_spc_valeur", RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR, DOUBLE_WITH_NULL_PARSER);
        newMandatoryColumn("dosage_spc_unite", RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE, PHYTO_PRODUCT_UNIT_PARSER);
        newMandatoryColumn("dosage_spc_commentaire", RefActaDosageSPC.PROPERTY_DOSAGE_SPC_COMMENTAIRE);
        newMandatoryColumn("source", RefActaDosageSPC.PROPERTY_SOURCE);
        newMandatoryColumn("code_amm", RefActaDosageSPC.PROPERTY_CODE_AMM);
        newOptionalColumn(COLUMN_ACTIVE, RefActaDosageSPC.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefActaDosageSPC, Object>> getColumnsForExport() {
        ModelBuilder<RefActaDosageSPC> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("id_produit", RefActaDosageSPC.PROPERTY_ID_PRODUIT);
        modelBuilder.newColumnForExport("nom_produit", RefActaDosageSPC.PROPERTY_NOM_PRODUIT);
        modelBuilder.newColumnForExport("id_traitement", RefActaDosageSPC.PROPERTY_ID_TRAITEMENT, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("code_traitement", RefActaDosageSPC.PROPERTY_CODE_TRAITEMENT);
        modelBuilder.newColumnForExport("id_culture", RefActaDosageSPC.PROPERTY_ID_CULTURE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("nom_culture", RefActaDosageSPC.PROPERTY_NOM_CULTURE);
        modelBuilder.newColumnForExport("remarque_culture", RefActaDosageSPC.PROPERTY_REMARQUE_CULTURE);
        modelBuilder.newColumnForExport("dosage_spc_valeur", RefActaDosageSPC.PROPERTY_DOSAGE_SPC_VALEUR, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("dosage_spc_unite", RefActaDosageSPC.PROPERTY_DOSAGE_SPC_UNITE, PHYTO_PRODUCT_UNIT_FORMATTER);
        modelBuilder.newColumnForExport("dosage_spc_commentaire", RefActaDosageSPC.PROPERTY_DOSAGE_SPC_COMMENTAIRE);
        modelBuilder.newColumnForExport("source", RefActaDosageSPC.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport("code_amm", RefActaDosageSPC.PROPERTY_CODE_AMM);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefActaDosageSPC.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefActaDosageSPC newEmptyInstance() {
        RefActaDosageSPC refActaDosageSPC = new RefActaDosageSPCImpl();
        refActaDosageSPC.setActive(true);
        return refActaDosageSPC;
    }
}
