package fr.inra.agrosyst.services.domain.inputStock;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainFuelInput;
import fr.inra.agrosyst.api.entities.DomainIrrigationInput;
import fr.inra.agrosyst.api.entities.DomainManualWorkforceInput;
import fr.inra.agrosyst.api.entities.DomainMechanizedWorkforceInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainOrganicProductInput;
import fr.inra.agrosyst.api.entities.DomainOtherInput;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainPotInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.DomainSubstrateInput;
import fr.inra.agrosyst.api.entities.InputType;
import fr.inra.agrosyst.api.entities.referential.RefActaTraitementsProduit;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefFertiOrga;
import fr.inra.agrosyst.api.entities.referential.RefOtherInput;
import fr.inra.agrosyst.api.entities.referential.RefPot;
import fr.inra.agrosyst.api.entities.referential.RefSubstrate;
import fr.inra.agrosyst.api.entities.referential.ReferentialEntityTranslation;
import fr.inra.agrosyst.api.entities.referential.ReferentialTranslationMap;
import fr.inra.agrosyst.api.exceptions.AgrosystTechnicalException;
import fr.inra.agrosyst.api.services.domain.CropPersistResult;
import fr.inra.agrosyst.api.services.domain.CroppingPlanEntryDto;
import fr.inra.agrosyst.api.services.domain.CroppingPlanSpeciesDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainFuelInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainInputStockUnitService;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainIrrigationInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainManualWorkforceInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMechanizedWorkforceInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainMineralProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOrganicProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainOtherProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPhytoProductInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainPotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedLotInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSeedSpeciesInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.DomainSubstrateInputDto;
import fr.inra.agrosyst.api.services.domain.inputStock.InputPriceDto;
import fr.inra.agrosyst.api.services.referential.DomainReferentialInputs;
import fr.inra.agrosyst.api.services.referential.ReferentialService;
import lombok.NonNull;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static org.nuiton.i18n.I18n.getDefaultLocale;
import static org.nuiton.i18n.I18n.l;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class DomainInputBinder {

    public static final String KEY_SEPARATOR = ";";
    public static final String MISSING_REQUIRED_PARAM = "Missing require Param %s:%s for farm with id '%s' :";
    public static final String ENTITY_NOT_FOUND = "Entity not found:";

    public static DomainInputDto bindToDto(AbstractDomainInputStockUnit domainInput,
                                           List<CroppingPlanEntryDto> cropDtos,
                                           ReferentialTranslationMap translationMap) {

        DomainInputDto result = null;
        InputType inputType = domainInput.getInputType();
        switch (inputType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                    result = domainMineralProductInputEntityToDtoBinder(domainInput, translationMap);
            case EPANDAGES_ORGANIQUES -> result = domainOrganicProductInputDtoBinder(domainInput, translationMap);
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE ->
                    result = domainPhytoProductInputDtoBinder(domainInput);
            case CARBURANT -> result = domainFuelInputDtoBinder(domainInput);
            case MAIN_OEUVRE_MANUELLE -> result = domainManualWorkforceInputDtoBinder(domainInput);
            case MAIN_OEUVRE_TRACTORISTE -> result = domainMechanizedWorkforceInputDtoBinder(domainInput);
            case IRRIGATION -> result = domainIrrigationInputDtoBinder(domainInput);
            case AUTRE -> result = domainOtherProductInputDtoBinder(domainInput, translationMap);
            case SUBSTRAT -> result = domainSubstrateInputDtoBinder(domainInput, translationMap);
            case POT -> result = domainPotInputDtoBinder(domainInput, translationMap);
            case SEMIS, PLAN_COMPAGNE -> result = bindToSeedingDto(domainInput, cropDtos);
            case TRAITEMENT_SEMENCE -> {
            }//nothing to do, done into SEMIS...
        }

        return result;
    }

    public static DomainSeedLotInputDto bindToSeedingDto(AbstractDomainInputStockUnit domainInput, Collection<CroppingPlanEntryDto> cropDtos) {
        DomainSeedLotInputDto result = null;
        if (domainInput instanceof DomainSeedLotInput) {
            result = domainSeedLotInputDtoBinder(domainInput, cropDtos);
        }
        return result;
    }

    public static DomainMineralProductInputDto domainMineralProductInputEntityToDtoBinder(
            AbstractDomainInputStockUnit abstractInput) {
        return domainMineralProductInputEntityToDtoBinder(abstractInput, null);
    }

    public static DomainMineralProductInputDto domainMineralProductInputEntityToDtoBinder(
            AbstractDomainInputStockUnit abstractInput, ReferentialTranslationMap translationMap) {

        DomainMineralProductInput input = (DomainMineralProductInput) abstractInput;

        DomainMineralProductInputDto.DomainMineralProductInputDtoBuilder<?, ?> builder = DomainMineralProductInputDto.builder();

        RefFertiMinUNIFA fertiMin = input.getRefInput();

        String typeProduit = fertiMin.getType_produit();
        String typeProduitTranslated = null;
        String formeTranslated = null;
        String forme = fertiMin.getForme();
        if (translationMap != null) {
            ReferentialEntityTranslation entityTranslation = translationMap.getEntityTranslation(fertiMin.getTopiaId());
            typeProduitTranslated = entityTranslation.getPropertyTranslation(RefFertiMinUNIFA.PROPERTY_TYPE_PRODUIT, typeProduit);
            formeTranslated = entityTranslation.getPropertyTranslation(RefFertiMinUNIFA.PROPERTY_FORME, forme);
        }
        DomainMineralProductInputDto domainInputDto = builder
                // for DomainInputDto
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(input.getInputType())
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .tradeName(typeProduit)
                .tradeNameTranslated(typeProduitTranslated)
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .usageUnit(input.getUsageUnit())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice()))
                // for DomainMineralProductInputDto
                .refInputId(fertiMin.getTopiaId())
                .phytoEffect(input.isPhytoEffect())
                .categ(fertiMin.getCateg())
                .forme(forme)
                .formeTranslated(formeTranslated)
                .n(fertiMin.getN())
                .p2o5(fertiMin.getP2O5())
                .k2o(fertiMin.getK2O())
                .bore(fertiMin.getBore())
                .calcium(fertiMin.getCalcium())
                .fer(fertiMin.getFer())
                .manganese(fertiMin.getManganese())
                .molybdene(fertiMin.getMolybdene())
                .mgo(fertiMin.getMgO())
                .oxyde_de_sodium(fertiMin.getOxyde_de_sodium())
                .so3(fertiMin.getsO3())
                .cuivre(fertiMin.getCuivre())
                .zinc(fertiMin.getZinc())
                .type_produit(typeProduit)
                .type_produit_Translated(typeProduitTranslated)
                .build();

        return domainInputDto;
    }

    public static DomainOrganicProductInputDto domainOrganicProductInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        return domainOrganicProductInputDtoBinder(abstractInput, null);
    }

    public static DomainOrganicProductInputDto domainOrganicProductInputDtoBinder(AbstractDomainInputStockUnit abstractInput, ReferentialTranslationMap translationMap) {
        DomainOrganicProductInput input = (DomainOrganicProductInput) abstractInput;

        final DomainOrganicProductInputDto.DomainOrganicProductInputDtoBuilder<?, ?> builder = DomainOrganicProductInputDto.builder();

        final RefFertiOrga refInput = input.getRefInput();
        String libelle = refInput.getLibelle();
        if (translationMap != null) {
            ReferentialEntityTranslation entityTranslation = translationMap.getEntityTranslation(refInput.getTopiaId());
            libelle = entityTranslation.getPropertyTranslation(RefFertiOrga.PROPERTY_LIBELLE, libelle);
        }

        DomainOrganicProductInputDto domainInputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(input.getInputType())
                .key(DomainInputStockUnitService.getOrganicProductInputKey(
                        refInput.getIdtypeeffluent(),
                        input.getUsageUnit(),
                        input.getN(),
                        input.getP2O5(),
                        input.getK2O(),
                        input.getCaO(),
                        input.getMgO(),
                        input.getS(),
                        input.isOrganic()
                ))
                .inputName(input.getInputName())
                .tradeName(libelle)
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .refInputId(refInput.getTopiaId())
                .n(input.getN())
                .p2O5(input.getP2O5())
                .k2O(input.getK2O())
                .caO(input.getCaO())
                .mgO(input.getMgO())
                .s(input.getS())
                .organic(input.isOrganic())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice())).build();

        return domainInputDto;
    }

    public static DomainPhytoProductInputDto domainPhytoProductInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        DomainPhytoProductInput input = (DomainPhytoProductInput) abstractInput;

        final DomainPhytoProductInputDto.DomainPhytoProductInputDtoBuilder<?, ?> builder = DomainPhytoProductInputDto.builder();

        RefActaTraitementsProduit refInput = input.getRefInput();
        DomainPhytoProductInputDto domainInputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(input.getInputType())
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .tradeName(refInput.getNom_produit())
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .refInputId(refInput.getTopiaId())
                .ammCode(refInput.getCode_AMM())
                .productType(input.getProductType())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice()))
                .build();

        return domainInputDto;
    }

    protected static DomainFuelInputDto domainFuelInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        DomainFuelInput input = (DomainFuelInput) abstractInput;

        final DomainFuelInputDto.DomainFuelInputDtoBuilder<?, ?> builder = DomainFuelInputDto.builder();

        DomainFuelInputDto domainInputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(InputType.CARBURANT)
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice())).build();

        return domainInputDto;
    }

    protected static DomainManualWorkforceInputDto domainManualWorkforceInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        DomainManualWorkforceInput input = (DomainManualWorkforceInput) abstractInput;

        final DomainManualWorkforceInputDto.DomainManualWorkforceInputDtoBuilder<?, ?> builder = DomainManualWorkforceInputDto.builder();

        DomainManualWorkforceInputDto domainInputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(InputType.MAIN_OEUVRE_MANUELLE)
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice())).build();

        return domainInputDto;
    }

    protected static DomainMechanizedWorkforceInputDto domainMechanizedWorkforceInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        DomainMechanizedWorkforceInput input = (DomainMechanizedWorkforceInput) abstractInput;

        final DomainMechanizedWorkforceInputDto.DomainMechanizedWorkforceInputDtoBuilder<?, ?> builder = DomainMechanizedWorkforceInputDto.builder();

        DomainMechanizedWorkforceInputDto domainInputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(InputType.MAIN_OEUVRE_TRACTORISTE)
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice())).build();

        return domainInputDto;
    }

    public static DomainIrrigationInputDto domainIrrigationInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        DomainIrrigationInput input = (DomainIrrigationInput) abstractInput;

        final DomainIrrigationInputDto.DomainIrrigationInputDtoBuilder<?, ?> builder = DomainIrrigationInputDto.builder();

        DomainIrrigationInputDto inputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(input.getInputType())
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice())).build();

        return inputDto;
    }

    public static DomainSeedSpeciesInputDto domainSeedSpeciesInputDtoBinder(
            List<CroppingPlanSpeciesDto> species,
            DomainSeedSpeciesInput domainSeedSpeciesInput) {

        Map<String, CroppingPlanSpeciesDto> speciesDtoByIds = Maps.uniqueIndex(species, CroppingPlanSpeciesDto::getTopiaId);

        CroppingPlanSpecies speciesSeed = domainSeedSpeciesInput.getSpeciesSeed();
        CroppingPlanSpeciesDto speciesSeedDto = speciesDtoByIds.get(speciesSeed.getTopiaId());
        if (speciesSeedDto == null) {
            throw new AgrosystTechnicalException(String.format(ENTITY_NOT_FOUND + "cps with id '%s'", speciesSeed.getTopiaId()));
        }

        return getDomainSeedSpeciesInputDto(domainSeedSpeciesInput, speciesSeedDto);

    }

    public static DomainSeedSpeciesInputDto getDomainSeedSpeciesInputDto(
            DomainSeedSpeciesInput domainSeedSpeciesInput,
            CroppingPlanSpeciesDto speciesSeedDto) {

        Collection<DomainPhytoProductInputDto> seedProductDtos = new ArrayList<>();

        CollectionUtils.emptyIfNull(domainSeedSpeciesInput.getSpeciesPhytoInputs())
                .forEach(seedProduct ->
                {
                    DomainPhytoProductInputDto inputDto = domainPhytoProductInputDtoBinder(seedProduct);
                    seedProductDtos.add(inputDto);
                });

        final InputPriceDto seedPrice = DomainInputPriceBinder.bindPriceToDto(domainSeedSpeciesInput.getSeedPrice());
        final DomainSeedSpeciesInputDto domainSeedSpeciesInputDto = getDomainSeedSpeciesInputDto(
                domainSeedSpeciesInput, speciesSeedDto, seedProductDtos, seedPrice);

        return domainSeedSpeciesInputDto;
    }

    private static DomainSeedSpeciesInputDto getDomainSeedSpeciesInputDto(
            DomainSeedSpeciesInput domainSeedSpeciesInput,
            CroppingPlanSpeciesDto speciesSeedDto,
            Collection<DomainPhytoProductInputDto> seedProductDtos,
            InputPriceDto seedPrice) {

        final DomainSeedSpeciesInputDto domainSeedSpeciesInputDto = DomainSeedSpeciesInputDto.builder()
                .domainId(domainSeedSpeciesInput.getDomain().getTopiaId())
                .inputType(domainSeedSpeciesInput.getInputType())
                .key(domainSeedSpeciesInput.getInputKey())
                .inputName(domainSeedSpeciesInput.getInputName())
                .topiaId(domainSeedSpeciesInput.getTopiaId())
                .code(domainSeedSpeciesInput.getCode())
                .speciesSeedDto(speciesSeedDto) //must not be null
                .seedPrice(seedPrice)
                .biologicalSeedInoculation(domainSeedSpeciesInput.isBiologicalSeedInoculation())
                .chemicalTreatment(domainSeedSpeciesInput.isChemicalTreatment())
                .organic(domainSeedSpeciesInput.isOrganic())
                .seedType(domainSeedSpeciesInput.getSeedType())
                .speciesPhytoInputDtos(seedProductDtos).build();
        return domainSeedSpeciesInputDto;
    }

    protected static DomainSeedLotInputDto domainSeedLotInputDtoBinder(AbstractDomainInputStockUnit abstractInput, Collection<CroppingPlanEntryDto> cropDtos) {
        DomainSeedLotInput input = (DomainSeedLotInput) abstractInput;

        final DomainSeedLotInputDto.DomainSeedLotInputDtoBuilder<?, ?> builder = DomainSeedLotInputDto.builder();

        Map<String, CroppingPlanEntryDto> cropDtoByIds = Maps.uniqueIndex(cropDtos, CroppingPlanEntryDto::getTopiaId);
        CroppingPlanEntry cropSeed = input.getCropSeed();
        CroppingPlanEntryDto seedDto = cropDtoByIds.get(cropSeed.getTopiaId());
        if (seedDto == null) {
            throw new AgrosystTechnicalException(String.format(ENTITY_NOT_FOUND + "crop with id '%s'", cropSeed.getTopiaId()));
        }

        Collection<DomainSeedSpeciesInputDto> speciesInputDtos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(input.getDomainSeedSpeciesInput())) {
            input.getDomainSeedSpeciesInput().forEach(domainSeedSpeciesInput ->
                    speciesInputDtos.add(domainSeedSpeciesInputDtoBinder(seedDto.getSpecies(), domainSeedSpeciesInput)));
        }

        DomainSeedLotInputDto inputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(input.getInputType())
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .cropSeedDto(seedDto)// must not be null
                .speciesInputs(ImmutableList.copyOf(speciesInputDtos))
                .organic(input.isOrganic())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getSeedPrice()))
                .seedCoatedTreatment(input.isSeedCoatedTreatment())
                .build();

        return inputDto;
    }

    public static DomainOtherProductInputDto domainOtherProductInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        return domainOtherProductInputDtoBinder(abstractInput, null);
    }

    public static DomainOtherProductInputDto domainOtherProductInputDtoBinder(AbstractDomainInputStockUnit abstractInput, ReferentialTranslationMap translationMap) {
        DomainOtherInput input = (DomainOtherInput) abstractInput;

        final DomainOtherProductInputDto.DomainOtherProductInputDtoBuilder<?, ?> builder = DomainOtherProductInputDto.builder();

        final RefOtherInput refInput = input.getRefInput();

        String inputTypeC0 = refInput.getInputType_c0();
        String caracteristic1 = refInput.getCaracteristic1();
        String caracteristic2 = refInput.getCaracteristic2();
        String caracteristic3 = refInput.getCaracteristic3();
        Locale locale = getDefaultLocale();
        if (translationMap != null) {
            ReferentialEntityTranslation entityTranslation = translationMap.getEntityTranslation(refInput.getTopiaId());
            inputTypeC0 = entityTranslation.getPropertyTranslation(RefOtherInput.PROPERTY_INPUT_TYPE_C0, inputTypeC0);
            caracteristic1 = entityTranslation.getPropertyTranslation(RefOtherInput.PROPERTY_CARACTERISTIC1, caracteristic1);
            caracteristic2 = entityTranslation.getPropertyTranslation(RefOtherInput.PROPERTY_CARACTERISTIC2, caracteristic2);
            caracteristic3 = entityTranslation.getPropertyTranslation(RefOtherInput.PROPERTY_CARACTERISTIC3, caracteristic3);
            locale = translationMap.getLocale();
        }

        String emptyString = l(locale, DomainInputBinder.class.getName() + "." + "DEFAULT_EMPTY_VIDE");

        DomainOtherProductInputDto domainInputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(input.getInputType())
                .inputName(input.getInputName())
                .refInputId(input.getRefInput().getTopiaId())
                .key(input.getInputKey())
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .inputType_c0(StringUtils.defaultIfEmpty(inputTypeC0, emptyString))
                .caracteristic1(StringUtils.defaultIfEmpty(caracteristic1, emptyString))
                .caracteristic2(StringUtils.defaultIfEmpty(caracteristic2, emptyString))
                .caracteristic3(StringUtils.defaultIfEmpty(caracteristic3, emptyString))
                .lifetime(refInput.getLifetime())
                .comment(input.getComment())
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice()))
                .deprecated(refInput.getTopiaId()
                        .contentEquals(DomainInputStockUnitService.DEPRECATED_REF_OTHER_INPUT_ID))
                .build();

        return domainInputDto;
    }

    public static DomainSubstrateInputDto domainSubstrateInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        return domainSubstrateInputDtoBinder(abstractInput, null);
    }

    public static DomainSubstrateInputDto domainSubstrateInputDtoBinder(AbstractDomainInputStockUnit abstractInput, ReferentialTranslationMap translationMap) {
        DomainSubstrateInput input = (DomainSubstrateInput) abstractInput;

        final DomainSubstrateInputDto.DomainSubstrateInputDtoBuilder<?, ?> builder = DomainSubstrateInputDto.builder();

        final RefSubstrate refInput = input.getRefInput();

        String caracteristic1 = refInput.getCaracteristic1();
        String caracteristic2 = refInput.getCaracteristic2();
        String caracteristic1Translated = null;
        String caracteristic2Translated = null;
        if (translationMap != null) {
            ReferentialEntityTranslation entityTranslation = translationMap.getEntityTranslation(refInput.getTopiaId());
            caracteristic1Translated = entityTranslation.getPropertyTranslation(RefSubstrate.PROPERTY_CARACTERISTIC1, caracteristic1);
            caracteristic2Translated = entityTranslation.getPropertyTranslation(RefSubstrate.PROPERTY_CARACTERISTIC2, caracteristic2);
        }

        DomainSubstrateInputDto domainInputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(InputType.SUBSTRAT)
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .refInputId(refInput.getTopiaId())
                .characteristic1(caracteristic1)
                .characteristic1Translated(StringUtils.firstNonBlank(caracteristic1Translated, caracteristic1))
                .characteristic2(caracteristic2)
                .characteristic2Translated(StringUtils.firstNonBlank(caracteristic2Translated, caracteristic2))
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice())).build();

        return domainInputDto;
    }

    public static DomainPotInputDto domainPotInputDtoBinder(AbstractDomainInputStockUnit abstractInput) {
        return domainPotInputDtoBinder(abstractInput, null);
    }

    public static DomainPotInputDto domainPotInputDtoBinder(AbstractDomainInputStockUnit abstractInput, ReferentialTranslationMap translationMap) {
        DomainPotInput input = (DomainPotInput) abstractInput;

        final DomainPotInputDto.DomainPotInputDtoBuilder<?, ?> builder = DomainPotInputDto.builder();

        final RefPot refInput = input.getRefInput();

        String caracteristic1 = refInput.getCaracteristic1();
        String caracteristic1Translated = null;
        if (translationMap != null) {
            ReferentialEntityTranslation entityTranslation = translationMap.getEntityTranslation(refInput.getTopiaId());
            caracteristic1Translated = entityTranslation.getPropertyTranslation(RefPot.PROPERTY_CARACTERISTIC1, caracteristic1);
        }

        DomainPotInputDto inputDto = builder
                .domainId(abstractInput.getDomain().getTopiaId())
                .inputType(InputType.POT)
                .key(input.getInputKey())
                .inputName(input.getInputName())
                .usageUnit(input.getUsageUnit())
                .topiaId(input.getTopiaId())
                .code(input.getCode())
                .refInputId(refInput.getTopiaId())
                .caracteristic1(caracteristic1)
                .caracteristic1Translated(StringUtils.firstNonBlank(caracteristic1Translated, caracteristic1))
                .price(DomainInputPriceBinder.bindPriceToDto(input.getInputPrice())).build();

        return inputDto;
    }

    protected static void bindDtoToEntity(
            Domain domain,
            DomainInputDto dto,
            AbstractDomainInputStockUnit entity,
            ReferentialService referentialService,
            DomainReferentialInputs domainInputReferential,
            boolean isInputUsed) {

        InputType domainInputType = dto.getInputType();
        switch (domainInputType) {
            case APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX ->
                    domainMineralProductInputDtoToEntityBinder(domain, (DomainMineralProductInputDto) dto, (DomainMineralProductInput) entity, isInputUsed, referentialService);
            case EPANDAGES_ORGANIQUES ->
                    domainOrganicProductInputDtoToEntityBinder(domain, (DomainOrganicProductInputDto) dto, (DomainOrganicProductInput) entity, isInputUsed, domainInputReferential);
            case APPLICATION_DE_PRODUITS_PHYTOSANITAIRES, LUTTE_BIOLOGIQUE, TRAITEMENT_SEMENCE ->
                    domainPhytoProductInputDtoToEntityBinder(domain, (DomainPhytoProductInputDto) dto, (DomainPhytoProductInput) entity, isInputUsed, domainInputReferential);
            case CARBURANT ->
                    domainFuelInputDtoToEntityBinder(domain, (DomainFuelInputDto) dto, (DomainFuelInput) entity, isInputUsed);
            case MAIN_OEUVRE_MANUELLE ->
                    domainManualWorkforceInputDtoToEntityBinder(domain, (DomainManualWorkforceInputDto) dto, (DomainManualWorkforceInput) entity, isInputUsed);
            case MAIN_OEUVRE_TRACTORISTE ->
                    domainMechanizedWorkforceInputDtoToEntityBinder(domain, (DomainMechanizedWorkforceInputDto) dto, (DomainMechanizedWorkforceInput) entity, isInputUsed);
            case IRRIGATION ->
                    domainIrrigationDtoToEntityBinder(domain, (DomainIrrigationInputDto) dto, (DomainIrrigationInput) entity, isInputUsed);
            case SEMIS, PLAN_COMPAGNE -> {
            }// NOTING TO DO done by bindSeedLotInputDtoToEntity()
            case AUTRE ->
                    domainOtherDtoToEntityBinder(domain, (DomainOtherProductInputDto) dto, (DomainOtherInput) entity, isInputUsed, domainInputReferential);
            case SUBSTRAT ->
                    domainSubstratDtoToEntityBinder(domain, (DomainSubstrateInputDto) dto, (DomainSubstrateInput) entity, isInputUsed, domainInputReferential);
            case POT ->
                    domainPotDtoToEntityBinder(domain, (DomainPotInputDto) dto, (DomainPotInput) entity, isInputUsed, domainInputReferential);
        }
    }

    protected static void domainOrganicProductInputDtoToEntityBinder(
            Domain domain,
            DomainOrganicProductInputDto dto,
            DomainOrganicProductInput entity,
            boolean isInputUsed,
            DomainReferentialInputs domainInputReferential) {

        RefFertiOrga refInput = domainInputReferential.refFertiOrgaByIds().get(dto.getRefInputId());
        if (refInput == null) {
            throw new AgrosystTechnicalException(String.format(MISSING_REQUIRED_PARAM, "RefFertiOrga", dto.getRefInputId(), domain.getTopiaId()));
        }
        entity.setInputType(InputType.EPANDAGES_ORGANIQUES);
        entity.setInputName(dto.getInputName());
        entity.setN(dto.getN());
        entity.setK2O(dto.getK2O());
        entity.setP2O5(dto.getP2O5());
        entity.setCaO(dto.getCaO());
        entity.setMgO(dto.getMgO());
        entity.setS(dto.getS());
        entity.setInputKey(DomainInputStockUnitService.getOrganicProductInputKey(
                refInput.getIdtypeeffluent(), dto.getUsageUnit(), dto.getN(),
                dto.getP2O5(),
                dto.getK2O(),
                dto.getCaO(),
                dto.getMgO(),
                dto.getS(),
                dto.isOrganic()
        ));
        if (!isInputUsed) {
            entity.setOrganic(dto.isOrganic());
            entity.setRefInput(refInput);
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
        }

    }

    protected static void domainPotDtoToEntityBinder(
            Domain domain,
            DomainPotInputDto dto,
            DomainPotInput entity,
            boolean isInputUsed,
            DomainReferentialInputs domainInputReferential) {

        entity.setInputType(InputType.POT);
        entity.setInputName(dto.getInputName());

        if (!isInputUsed) {
            RefPot refInput = domainInputReferential.refPotByIds().get(dto.getRefInputId());
            if (refInput == null) {
                throw new AgrosystTechnicalException(String.format(MISSING_REQUIRED_PARAM, "RefPot", dto.getRefInputId(), domain.getTopiaId()));
            }
            entity.setRefInput(refInput);
            entity.setInputKey(DomainInputStockUnitService.getPotInputKey(refInput, dto.getUsageUnit()));
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
        }

    }

    private static void domainSubstratDtoToEntityBinder(
            Domain domain,
            DomainSubstrateInputDto dto,
            DomainSubstrateInput entity,
            boolean isInputUsed,
            DomainReferentialInputs domainInputReferential) {

        entity.setInputType(InputType.SUBSTRAT);
        entity.setInputName(dto.getInputName());

        if (!isInputUsed) {
            RefSubstrate refInput = domainInputReferential.refSubstrateByIds().get(dto.getRefInputId());
            if (refInput == null) {
                throw new AgrosystTechnicalException(String.format(MISSING_REQUIRED_PARAM, "RefSubstrate", dto.getRefInputId(), domain.getTopiaId()));
            }
            entity.setRefInput(refInput);
            entity.setInputKey(DomainInputStockUnitService.getSubstratInputKey(refInput, dto.getUsageUnit()));
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
        }
    }

    private static void domainOtherDtoToEntityBinder(
            Domain domain,
            DomainOtherProductInputDto dto,
            DomainOtherInput entity,
            boolean isInputUsed,
            DomainReferentialInputs domainInputReferential) {

        entity.setInputType(InputType.AUTRE);
        entity.setInputName(dto.getInputName());
        entity.setComment(dto.getComment());

        if (!isInputUsed) {
            RefOtherInput refInput = domainInputReferential.refOtherInputByIds().get(dto.getRefInputId());
            if (refInput == null) {
                throw new AgrosystTechnicalException(String.format(MISSING_REQUIRED_PARAM, "RefOtherInput", dto.getRefInputId(), domain.getTopiaId()));
            }
            entity.setRefInput(refInput);
            entity.setInputKey(DomainInputStockUnitService.getOtherInputKey(refInput, dto.getUsageUnit()));
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
        }
    }

    private static void domainIrrigationDtoToEntityBinder(
            Domain domain,
            DomainIrrigationInputDto dto,
            DomainIrrigationInput entity,
            boolean isInputUsed) {

        entity.setInputType(InputType.IRRIGATION);
        entity.setInputName(dto.getInputName());
        if (!isInputUsed) {
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
            entity.setInputKey(DomainInputStockUnitService.getIrrigInputKey(dto.getUsageUnit()));
        }

        //bindInputPrice(entity, priceEntity, dto.getPrice(), null);
    }

    private static void domainFuelInputDtoToEntityBinder(
            Domain domain,
            DomainFuelInputDto dto,
            DomainFuelInput entity,
            boolean isInputUsed) {

        entity.setInputType(InputType.CARBURANT);
        entity.setInputName(dto.getInputName());
        if (!isInputUsed) {
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
            entity.setInputKey(DomainInputStockUnitService.getFuelInputKey(dto.getUsageUnit()));
        }
    }

    private static void domainManualWorkforceInputDtoToEntityBinder(
            Domain domain,
            DomainManualWorkforceInputDto dto,
            DomainManualWorkforceInput entity,
            boolean isInputUsed) {

        entity.setInputType(InputType.MAIN_OEUVRE_MANUELLE);
        entity.setInputName(dto.getInputName());
        if (!isInputUsed) {
            entity.setDomain(domain);
            entity.setInputKey(DomainInputStockUnitService.getManualWorkforceInputKey(domain));
        }
    }

    private static void domainMechanizedWorkforceInputDtoToEntityBinder(
            Domain domain,
            DomainMechanizedWorkforceInputDto dto,
            DomainMechanizedWorkforceInput entity,
            boolean isInputUsed) {

        entity.setInputType(InputType.MAIN_OEUVRE_TRACTORISTE);
        entity.setInputName(dto.getInputName());
        if (!isInputUsed) {
            entity.setDomain(domain);
            entity.setInputKey(DomainInputStockUnitService.getMechanizedWorkforceInputKey(domain));
        }
    }

    private static void domainPhytoProductInputDtoToEntityBinder(
            Domain domain,
            DomainPhytoProductInputDto dto,
            DomainPhytoProductInput entity,
            boolean isInputUsed,
            DomainReferentialInputs domainInputReferential) {

        entity.setInputType(dto.getInputType());
        entity.setInputName(dto.getInputName());

        if (!entity.isPersisted() || !isInputUsed) {
            entity.setProductType(dto.getProductType());
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
            RefActaTraitementsProduit refInput = domainInputReferential.refActaTraitementsProduitByIds().get(dto.getRefInputId());
            if (refInput == null) {
                throw new AgrosystTechnicalException(String.format(MISSING_REQUIRED_PARAM, "RefActaTraitementsProduit", dto.getRefInputId(), domain.getTopiaId()));
            }
            entity.setRefInput(refInput);
            entity.setInputKey(DomainInputStockUnitService.getPhytoInputKey(refInput, dto.getUsageUnit()));
        }
    }

    protected static void domainMineralProductInputDtoToEntityBinder(
            Domain domain,
            DomainMineralProductInputDto dto,
            DomainMineralProductInput entity,
            boolean isInputUsed,
            ReferentialService referentialService) {

        entity.setInputType(InputType.APPLICATION_DE_PRODUITS_FERTILISANTS_MINERAUX);

        if (!isInputUsed) {
            RefFertiMinUNIFA product = referentialService.createOrUpdateRefMineralProductInput(dto);
            entity.setRefInput(product);
            entity.setPhytoEffect(dto.isPhytoEffect());
            String mineralInputKey = DomainInputStockUnitService.getMineralInputKey(product, entity.isPhytoEffect(), entity.getUsageUnit());
            entity.setInputKey(mineralInputKey);
            entity.setDomain(domain);
            entity.setUsageUnit(dto.getUsageUnit());
        }

        entity.setInputName(dto.getInputName());

    }

    protected static void shallowBindDomainSeedingLotDtoToEntity(
            Domain domain,
            DomainSeedLotInputDto lotDto,
            DomainSeedLotInput seedLotInput,
            Map<String, CropPersistResult> cropsByIds,
            boolean isInputUsed) {

        seedLotInput.setInputName(lotDto.getInputName());

        if (!isInputUsed) {
            seedLotInput.setDomain(domain);
            CropPersistResult persistResult = cropsByIds.get(lotDto.getCropSeedDto().getTopiaId());
            CroppingPlanEntry crop = persistResult.crop();
            seedLotInput.setCropSeed(crop);
            boolean isCompagne = CollectionUtils.isNotEmpty(crop.getCroppingPlanSpecies()) && crop.getCroppingPlanSpecies().stream()
                    .map(CroppingPlanSpecies::getCompagne)
                    .anyMatch(Objects::nonNull);
            InputType inputType = isCompagne ? InputType.PLAN_COMPAGNE : InputType.SEMIS;
            seedLotInput.setInputType(inputType);
            seedLotInput.setUsageUnit(lotDto.getUsageUnit());
            seedLotInput.setOrganic(lotDto.isOrganic());
            seedLotInput.setInputKey(DomainInputStockUnitService.getLotCropSeedInputKey(crop, lotDto.isOrganic(), seedLotInput.getUsageUnit()));
        }
        seedLotInput.setSeedCoatedTreatment(lotDto.isSeedCoatedTreatment());

    }

    public static void shallowBindDomainSeedSpeciesInputDtoToEntity(
            Domain domain,
            DomainSeedSpeciesInputDto domainSeedSpeciesInputDto,
            DomainSeedSpeciesInput speciesInputEntity,
            CroppingPlanSpecies cps,
            Boolean speciesUsed,
            String speciesInputName,
            String lotSpeciesInputKey,
            @NonNull InputType inputType) {

        if (!(speciesInputEntity.isPersisted() && speciesUsed)) {
            speciesInputEntity.setDomain(domain);
        }
        speciesInputEntity.setInputType(inputType);
        speciesInputEntity.setInputName(speciesInputName);
        speciesInputEntity.setInputKey(lotSpeciesInputKey);
        speciesInputEntity.setSpeciesSeed(cps);
        speciesInputEntity.setSeedType(domainSeedSpeciesInputDto.getSeedType());
        speciesInputEntity.setBiologicalSeedInoculation(domainSeedSpeciesInputDto.isBiologicalSeedInoculation());
        speciesInputEntity.setChemicalTreatment(domainSeedSpeciesInputDto.isChemicalTreatment());
        speciesInputEntity.setOrganic(domainSeedSpeciesInputDto.isOrganic());


    }
}
