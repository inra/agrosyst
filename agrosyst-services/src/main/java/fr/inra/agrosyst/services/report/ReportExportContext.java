/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package fr.inra.agrosyst.services.report;

import fr.inra.agrosyst.api.entities.managementmode.ManagementMode;
import fr.inra.agrosyst.api.entities.managementmode.SectionType;
import fr.inra.agrosyst.api.entities.report.ReportGrowingSystem;
import fr.inra.agrosyst.api.services.report.ReportExportOption;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ReportExportContext extends ReportExportOption {

    protected ReportGrowingSystem reportGrowingSystem;

    protected ManagementMode managementMode;

    protected Map<String, String> groupesCiblesParCode;

    public ReportExportContext(ReportExportOption options) {
        super(options);
    }

    public ReportExportContext(ReportExportOption options,
                               ReportGrowingSystem reportGrowingSystem,
                               ManagementMode managementMode,
                               Map<String, String> groupesCiblesParCode) {
        this(options);
        this.reportGrowingSystem = reportGrowingSystem;
        this.managementMode = managementMode;
        this.groupesCiblesParCode = groupesCiblesParCode;
    }

    protected ReportExportContext(boolean includeReportRegional,
                                  Map<String, Collection<String>> reportGrowingSystemSections,
                                  List<SectionType> managementSections,
                                  ReportGrowingSystem reportGrowingSystem, ManagementMode managementMode,
                                  Map<String, String> groupesCiblesParCode) {
        this(new ReportExportOption(includeReportRegional, reportGrowingSystemSections, managementSections),
                reportGrowingSystem, managementMode, groupesCiblesParCode);
    }

    public ReportGrowingSystem getReportGrowingSystem() {
        return reportGrowingSystem;
    }

    public ManagementMode getManagementMode() {
        return managementMode;
    }

    public Map<String, String> getGroupesCiblesParCode() {
        return groupesCiblesParCode;
    }
}
