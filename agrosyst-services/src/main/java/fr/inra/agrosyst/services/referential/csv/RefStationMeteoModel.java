package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefStationMeteo;
import fr.inra.agrosyst.api.entities.referential.RefStationMeteoImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

/**
 * Modele d'import du référentiel station meteo.
 *
 * @author Eric Chatellier
 */
public class RefStationMeteoModel extends AbstractAgrosystModel<RefStationMeteo> implements ExportModel<RefStationMeteo> {

    public RefStationMeteoModel() {
        super(CSV_SEPARATOR);

        newMandatoryColumn("commune-site", RefStationMeteo.PROPERTY_COMMUNE_SITE);
        newMandatoryColumn("code postal", RefStationMeteo.PROPERTY_CODE_POSTAL);
        newMandatoryColumn("commune", RefStationMeteo.PROPERTY_COMMUNE);
        newMandatoryColumn("site", RefStationMeteo.PROPERTY_SITE);
        newMandatoryColumn("affectation", RefStationMeteo.PROPERTY_AFFECTATION);
        newMandatoryColumn("point GPS", RefStationMeteo.PROPERTY_POINT_GPS);
        newMandatoryColumn("Source", RefStationMeteo.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefStationMeteo.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefStationMeteo, Object>> getColumnsForExport() {
        ModelBuilder<RefStationMeteo> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("commune-site", RefStationMeteo.PROPERTY_COMMUNE_SITE);
        modelBuilder.newColumnForExport("code postal", RefStationMeteo.PROPERTY_CODE_POSTAL);
        modelBuilder.newColumnForExport("commune", RefStationMeteo.PROPERTY_COMMUNE);
        modelBuilder.newColumnForExport("site", RefStationMeteo.PROPERTY_SITE);
        modelBuilder.newColumnForExport("affectation", RefStationMeteo.PROPERTY_AFFECTATION);
        modelBuilder.newColumnForExport("point GPS", RefStationMeteo.PROPERTY_POINT_GPS);
        modelBuilder.newColumnForExport("Source", RefStationMeteo.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefStationMeteo.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefStationMeteo newEmptyInstance() {
        RefStationMeteo refStationMeteo = new RefStationMeteoImpl();
        refStationMeteo.setActive(true);
        return refStationMeteo;
    }
}
