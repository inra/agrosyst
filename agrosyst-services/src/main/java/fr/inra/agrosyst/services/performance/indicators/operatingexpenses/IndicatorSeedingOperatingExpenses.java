package fr.inra.agrosyst.services.performance.indicators.operatingexpenses;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.DomainPhytoProductInput;
import fr.inra.agrosyst.api.entities.DomainSeedLotInput;
import fr.inra.agrosyst.api.entities.DomainSeedSpeciesInput;
import fr.inra.agrosyst.api.entities.InputPrice;
import fr.inra.agrosyst.api.entities.PhytoProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.SeedPrice;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.AbstractPhytoProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedLotInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedPlantUnit;
import fr.inra.agrosyst.api.entities.action.SeedProductInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedSpeciesInputUsage;
import fr.inra.agrosyst.api.entities.action.SeedingActionUsage;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.common.PricesService;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveDomainExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceEffectiveInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedInterventionExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformancePracticedSystemExecutionContext;
import fr.inra.agrosyst.api.services.performance.PerformanceZoneExecutionContext;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.UsagePerformanceResult;
import fr.inra.agrosyst.services.performance.IndicatorWriter;
import fr.inra.agrosyst.services.performance.MissingMessageScope;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;
import fr.inra.agrosyst.services.performance.performancehelper.WriterContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Les charges opérationnelles réelles sont exprimées en €/ha.
 * Elles correspondent aux dépenses liées à l’achat des intrants (semences et plants,
 * produits fertilisants minéraux  et  organiques, traitement de semences et plants,
 * irrigation, produits phytosanitaires, produits de lutte biologique, autres intrants, substrats, pots).
 * <p>
 * Le calcul se fait sur la base des prix saisis par l’utilisateur.
 * <p>
 * Les interventions concernées par cet indicateur sont toutes les interventions
 * contenant un intrant et/ou une action de type « Semis » et/ou une action de
 * type « Irrigation ».
 * <p>
 *
 * <p>
 * Cette classe est une composante de {@link IndicatorOperatingExpenses} chargée de calculer les charges relatives au semis.
 * </p>
 * Rappel de la formule globale de calcul:
 *
 * <pre>
 * CI réel_i:
 *
 * = PSCi * ( sum(Q_ev * PA_ev) + sum(Q_j * PA_j) + sum(Q_e * PA_e) + sum(Q_a * PA_a)) + PSCi_phyto * sum(Q_k * PA_k) + PSCi_luttebio * sum(Q_h * PA_h)
 *
 * Avec :
 * - CI réel_i : charges intrants de l’intervention i
 * - PSCi (sans unité) : Proportion de surface concernée par le passage de la combinaison d’outils ou l’opération manuelle de l’intervention i.
 *   PSCi est calculé sur la base des données saisies par l’utilisateur.
 *
 *  - Semis :
 *   - Q_ev (diverses unités) : quantité semée du couple EV, EV appartenant à la liste des couples EV semés
 *     dans l’action semis de l’intervention i. Donnée saisie par l’utilisateur.
 *   - PA_ev (diverses unités) : prix d’achat du couple EV pour ce type de semence (de ferme, certifiées ...),
 *     EV appartenant à la liste des couples EV semés au cours de l’intervention i. Donnée saisie par l’utilisateur.
 *      - ATTENTION :  dès lors que la case « traitement inclus ... » est cochée, ne plus réaliser l’étape décrite dans
 *        le paragraphe ci-dessous pour ce qui est du traitement de semence (par contre, réaliser l’opération pour les
 *        intrants de fertilisation). Car il ne faut pas compter cet intrant 2 fois.
 *
 * </pre>
 *
 * @author Yannick Martel (martel@codelutin.com)
 */
public class IndicatorSeedingOperatingExpenses extends IndicatorInputProductOperatingExpenses {

    protected static final Log LOGGER = LogFactory.getLog(IndicatorSeedingOperatingExpenses.class);

    public IndicatorSeedingOperatingExpenses(
            boolean displayed,
            boolean computeReal,
            boolean computStandardised,
            Locale locale) {
        super(displayed, computeReal, computStandardised, locale);
    }

    @Override
    public Pair<Optional<InputPrice>, Optional<InputRefPrice>> getDomainInputPrice(AbstractInputUsage usage, RefCampaignsInputPricesByDomainInput refCampaignsInputPricesByDomainInputAndCampaigns) {
        if (usage instanceof SeedProductInputUsage) {
            final DomainPhytoProductInput input = ((AbstractPhytoProductInputUsage) usage).getDomainPhytoProductInput();
            final InputPrice inputPrice = input.getInputPrice();
            Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.phytoRefPriceForInput().get(input);
            inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
            return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
        } else if (usage instanceof SeedSpeciesInputUsage) {
            final DomainSeedSpeciesInput input = ((SeedSpeciesInputUsage) usage).getDomainSeedSpeciesInput();
            final InputPrice inputPrice = input.getSeedPrice();
            Optional<InputRefPrice> inputRePrice = refCampaignsInputPricesByDomainInputAndCampaigns.speciesRefPriceForInput().get(input);
            inputRePrice = inputRePrice == null ? Optional.empty() : inputRePrice;
            return Pair.of(Optional.ofNullable(inputPrice), inputRePrice);
        }
        return null;
    }

    /**
     * Q EV (diverses unités) : quantité semée du couple EV, EV appartenant à la liste des couples EV semés
     * dans l’action semis de l’intervention i. Donnée saisie par l’utilisateur.
     * PA EV (diverses unités) : prix d’achat du couple EV pour ce type de semence (de ferme, certifiées ...),
     * EV appartenant à la liste des couples EV semés au cours de l’intervention i. Donnée saisie par
     * l’utilisateur.
     * On considère une semence comme étant un intrant
     * return ci for price, ci for refprice
     */
    protected Pair<Double, Double> computeCropOperatingExpenses(
            final WriterContext writerContext,
            final SeedingActionUsage seedingActionUsage,
            RefCampaignsInputPricesByDomainInput inputRefPricesByDomainInputs,
            Set<SeedingActionUsage> actionCropPricesIncludedTreatment,
            Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingProductUnit,
            final double psci,
            String interventionId,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        Pair<Double, Double> ci = IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;

        Domain domain = writerContext.getAnonymizeDomain();

        final Collection<SeedLotInputUsage> seedLotInputUsages = seedingActionUsage.getSeedLotInputUsage();

        final Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> speciesRefPriceByDomainInputs = inputRefPricesByDomainInputs.speciesRefPriceForInput();

        for (SeedLotInputUsage seedLotInputUsage : seedLotInputUsages) {
            final DomainSeedLotInput domainSeedLotInput = seedLotInputUsage.getDomainSeedLotInput();

            // refs #8507
            // Ne pas considérer les unités des espèces dont la quantité semée est 0 car celle si sont a ignorer
            SeedPlantUnit seedPlantUnit = domainSeedLotInput.getUsageUnit();

            // soit on a un prix en €/quantité pour l'ensemble du lot ou par détail par espèce
            final SeedPrice cropSeedPrice = domainSeedLotInput.getSeedPrice();

            boolean isPriceIncludeTreatment = domainSeedLotInput.isSeedCoatedTreatment();// par défaut le prix inclus le prix des traitements

            // le prix de référence ne peut être connu à l'échelle de la culture, car il manque la proportion de l'espèce dans la culture
            final Collection<SeedSpeciesInputUsage> seedingSpeciesUsages = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies());

            // calcul du prix pour chaque espèce du lot
            for (SeedSpeciesInputUsage seedSpeciesInputUsage : seedingSpeciesUsages) {
                final String seedSpeciesInputUsageTopiaId = seedSpeciesInputUsage.getTopiaId();

                incrementAngGetTotalFieldCounterForTargetedId(seedSpeciesInputUsageTopiaId); // quantité + unit
                incrementAngGetTotalFieldCounterForTargetedId(seedSpeciesInputUsageTopiaId); // price

                final DomainSeedSpeciesInput domainSeedSpeciesInput = seedSpeciesInputUsage.getDomainSeedSpeciesInput();
                final SeedPrice speciesSeedPrice = cropSeedPrice != null ? cropSeedPrice : domainSeedSpeciesInput.getSeedPrice();

                Double price = null;
                PriceUnit priceUnit = null;

                // s'il y a un traitement sur la semence, le prix de référence inclus le traitement
                if (speciesSeedPrice == null || speciesSeedPrice.getPrice() == null) {
                    if (cropSeedPrice != null) {
                        addMissingFieldMessage(seedSpeciesInputUsageTopiaId, messageBuilder.getMissingSeedingPriceMessage(domainSeedLotInput.getCropSeed().getName()));
                    } else {
                        addMissingFieldMessage(seedSpeciesInputUsageTopiaId, messageBuilder.getMissingSeedingSpeciesPriceMessage(domainSeedSpeciesInput.getSpeciesSeed().getSpecies().getLibelle_espece_botanique_Translated()));
                    }
                } else {
                    price = speciesSeedPrice.getPrice();
                    priceUnit = speciesSeedPrice.getPriceUnit();
                }

                final Optional<PriceAndUnit> speciesRefPriceValue = getSpeciesRefPriceValue(
                        domain,
                        seedPlantUnit,
                        speciesRefPriceByDomainInputs,
                        seedSpeciesInputUsage,
                        interventionId);

                if (speciesRefPriceValue.isEmpty()) {
                    addMissingFieldMessage(seedSpeciesInputUsageTopiaId, messageBuilder.getMissingRefStandardizedPriceMessage());
                }

                if (isPriceIncludeTreatment) {
                    actionCropPricesIncludedTreatment.add(seedingActionUsage);
                }

                Pair<Double, Double> ci_ = computeCi(
                        seedSpeciesInputUsage.getQtAvg(),
                        speciesRefPriceValue,
                        price,
                        priceUnit,
                        seedPlantUnit,
                        convertersBySeedingProductUnit);

                final double realOperatingExpense = psci * ci_.getLeft();
                final double standardisedOperatingExpense = psci * ci_.getRight();
                Double[] seedingCropResult = {realOperatingExpense, standardisedOperatingExpense};

                writeSeedingAction(seedingActionUsage, seedSpeciesInputUsage, seedSpeciesInputUsageTopiaId, writerContext, seedingCropResult, indicatorClass, labels);

                ci = Pair.of(ci.getLeft() + realOperatingExpense, ci.getRight() + standardisedOperatingExpense);
            }
        }

        final double realOperatingExpense = ci.getLeft();
        final double standardisedOperatingExpense = ci.getRight();

        return Pair.of(realOperatingExpense, standardisedOperatingExpense);
    }

    private Pair<Double, Double> computeCi(
            double lot_da,
            Optional<PriceAndUnit> optionalRefPrice,
            Double price,
            PriceUnit priceUnit,
            SeedPlantUnit seedPlantUnit,
            Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingProductUnit) {

        Pair<Double, Double> ci = IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;

        if (lot_da != 0) {
            // attributs utilisés pour filter les prix de semence d'une culture
            // comme le prix est global à la culture, les informations de traitement et d'inoculation biologique sont également globales à la culture.
            double userCi;
            double refCi;

            //Si aucun prix n’est saisi par l’utilisateur, on prendra le prix de référence de l’intrant pour le calcul de
            //l’indicateur. Cf. bien se référer pour l’identification de ces prix de ref à la SPEC des charges
            //opérationnelles standardisées. Car l’identification des prix de référence est spécifique à chaque type
            //d’intrant.

            double refPriceValue = 0;
            PriceUnit refPriceUnit = PricesService.DEFAULT_PRICE_UNIT;

            if (optionalRefPrice.isPresent()) {
                final PriceAndUnit refPriceAndUnit = optionalRefPrice.get();
                refPriceValue = refPriceAndUnit.value();
                refPriceUnit = refPriceAndUnit.unit();
            }

            refCi = IndicatorOperatingExpenses.computeCi(lot_da, refPriceValue, refPriceUnit, 1);// conversionRate already applied to refPrices

            if (price == null) {

                // refs #10063 Si aucun prix de référence n’est disponible pour le prix de l’intrant,
                // alors on prend la valeur « 0 » comme référence
                userCi = refCi;

            } else {

                Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters = CollectionUtils.emptyIfNull(convertersBySeedingProductUnit.get(seedPlantUnit));
                double conversionRate = refInputUnitPriceUnitConverters.stream()
                        .filter(rpu -> rpu.getPriceUnit().equals(priceUnit))
                        .findAny()
                        .map(RefInputUnitPriceUnitConverter::getConvertionRate)
                        .orElse(0d);

                //L’utilisateur peut saisir un prix en €/ha pour les intrants. Dans ce cas-là, on considère que la dose
                //d’application de l’intrant vaut 1.
                userCi = IndicatorOperatingExpenses.computeCi(lot_da, price, priceUnit, conversionRate);

            }

            ci = Pair.of(ci.getLeft() + userCi, ci.getRight() + refCi);

        }

        return ci;
    }

    private Optional<PriceAndUnit> getSpeciesRefPriceValue(
            Domain domain,
            SeedPlantUnit seedPlantUnit,
            Map<AbstractDomainInputStockUnit, Optional<InputRefPrice>> domainSpeciesToRefPrice,
            SeedSpeciesInputUsage seedSpeciesInputUsage,
            String interventionId) {

        PriceAndUnit priceAndUnit = null;
        //domainSpeciesToRefPrice may be null
        final Optional<InputRefPrice> optionalSpeciesRefPrice = domainSpeciesToRefPrice.get(seedSpeciesInputUsage.getDomainSeedSpeciesInput());

        if (optionalSpeciesRefPrice == null) {
            seedSpeciesInputUsage.getTopiaId();
            if (seedSpeciesInputUsage.getDomainSeedSpeciesInput() != null) {
                String usageDomainId = seedSpeciesInputUsage.getDomainSeedSpeciesInput().getDomain().getTopiaId();
                if (!usageDomainId.contentEquals(domain.getTopiaId())) {
                    LOGGER.warn(String.format("ERREUR: Sur l'intervention '%s', usage '%s', le domaine référencé par l'usage de semis '%s' ne correspond pas au domaine étudié '%s' ",
                            interventionId,
                            seedSpeciesInputUsage.getTopiaId(),
                            usageDomainId,
                            domain.getTopiaId()));
                }
            }
        }
        if (optionalSpeciesRefPrice != null && optionalSpeciesRefPrice.isPresent()) {
            final InputRefPrice speciesRefPrice = optionalSpeciesRefPrice.get();
            final PriceAndUnit priceAndUnit_ = speciesRefPrice.averageRefPrice();
            final double refPriceValue = priceAndUnit_.value();
            final PriceUnit priceUnit = priceAndUnit_.unit();
            priceAndUnit = new PriceAndUnit(refPriceValue, priceUnit);

            if (speciesRefPrice.fallbackRefPrice()) {
                boolean organic = seedSpeciesInputUsage.getDomainSeedSpeciesInput().isOrganic();// valeur recherchée mais non obtenue
                addMissingFieldMessage(interventionId, messageBuilder.getNotAccurateRefStandardizedPriceMessage("Prix de référence " + (organic ? "non biologique" : "biologique")));
            }

        } else {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("Prix de référence non trouvé avec comme unité d'utilisation:" + seedPlantUnit);
            }
        }
        return Optional.ofNullable(priceAndUnit);
    }

    /**
     * return ci for price, ci for refprice
     */
    protected Pair<Double, Double> computeProductOperatingExpenses(
            WriterContext writerContext,
            SeedingActionUsage seedingActionUsage,
            RefCampaignsInputPricesByDomainInput inputRefPricesByDomainInputs,
            Set<SeedingActionUsage> actionCropPricesIncludedTreatment,
            double psci,
            Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingPhytoProductUnit,
            String interventionId,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Map<? extends AbstractInputUsage, UsagePerformanceResult> refDoseByUsages) {

        Pair<Double, Double> ci = IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;

        if (actionCropPricesIncludedTreatment.contains(seedingActionUsage)) {
            // product is incorporate into the seeding, there is no extra product to use
            // or no seeding product cost
            return ci;
        }

        final Collection<SeedLotInputUsage> seedLotInputUsages = seedingActionUsage.getSeedLotInputUsage();

        for (SeedLotInputUsage seedLotInputUsage : seedLotInputUsages) {

            final Collection<SeedSpeciesInputUsage> seedingSpecieUsages = CollectionUtils.emptyIfNull(seedLotInputUsage.getSeedingSpecies());
            for (SeedSpeciesInputUsage seedingSpecieUsage : seedingSpecieUsages) {
                final boolean speciesSeedPriceIncludedTreatment = seedLotInputUsage.getDomainSeedLotInput().isSeedCoatedTreatment();
                if (!speciesSeedPriceIncludedTreatment) {
                    Collection<RefInputUnitPriceUnitConverter> converters = convertersBySeedingPhytoProductUnit != null ? convertersBySeedingPhytoProductUnit.values().stream().flatMap(Collection::stream).toList() : new ArrayList<>();
                    // Ne pas prendre en compte les prix de produit de semis si le prix de la culture inclue le traitement
                    final Collection<SeedProductInputUsage> seedProductInputUsages = CollectionUtils.emptyIfNull(seedingSpecieUsage.getSeedProductInputUsages());

                    Pair<Double, Double> speciesSeedProductInputUsages = computeIndicatorForAction(
                            writerContext,
                            seedingActionUsage,
                            seedProductInputUsages,
                            inputRefPricesByDomainInputs,
                            psci,
                            converters,
                            interventionId,
                            indicatorClass,
                            labels,
                            refDoseByUsages);
                    ci = Pair.of(speciesSeedProductInputUsages.getLeft() + ci.getLeft(), speciesSeedProductInputUsages.getRight() + ci.getRight());

                }
            }

        }

        final Pair<Double, Double> result = Pair.of(ci.getLeft(), ci.getRight());

        return result;
    }

    private void writeSeedingAction(
            AbstractAction action,
            SeedSpeciesInputUsage seedSpeciesInputUsage,
            String targetId,
            WriterContext writerContext,
            Double[] inputResult,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        IndicatorWriter writer = writerContext.getWriter();

        String reliabilityCommentForInputId = getReliabilityCommentForTargetedId(targetId, MissingMessageScope.INPUT);
        Integer reliabilityIndexForInputId = getReliabilityIndexForTargetedId(targetId);

        write(
                action,
                seedSpeciesInputUsage,
                writerContext,
                inputResult,
                writer,
                reliabilityCommentForInputId,
                reliabilityIndexForInputId,
                indicatorClass,
                labels);
    }

    private void write(
            AbstractAction action,
            SeedSpeciesInputUsage seedSpeciesInputUsage,
            WriterContext writerContext,
            Double[] inputResult,
            IndicatorWriter writer,
            String reliabilityCommentForInputId,
            Integer reliabilityIndexForInputId,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels) {

        Locale locale = writer.getLocale();
        for (int i = 0; i < inputResult.length; i++) {

            boolean isDisplayed = isDisplayed(i);

            if (isDisplayed) {
                // write input sheet
                writer.writeInputUsage(
                        writerContext.getIts(),
                        writerContext.getIrs(),
                        l(locale, Indicator.INDICATOR_CATEGORY_ECONOMIC),
                        l(locale, labels[i]),
                        seedSpeciesInputUsage,
                        action,
                        writerContext.getEffectiveIntervention(),
                        writerContext.getPracticedIntervention(),
                        writerContext.getCodeAmmBioControle(),
                        writerContext.getAnonymizeDomain(),
                        writerContext.getAnonymizeGrowingSystem(),
                        writerContext.getPlot(),
                        writerContext.getZone(),
                        writerContext.getPracticedSystem(),
                        writerContext.getCroppingPlanEntry(),
                        writerContext.getPracticedPhase(),
                        writerContext.getSolOccupationPercent(),
                        writerContext.getEffectivePhase(),
                        writerContext.getRank(),
                        writerContext.getPreviousPlanEntry(),
                        writerContext.getIntermediateCrop(),
                        indicatorClass,
                        inputResult[i],
                        reliabilityIndexForInputId,
                        reliabilityCommentForInputId,
                        null,
                        null,
                        null,
                        writerContext.getGroupesCiblesByCode());
            }
        }
    }

    Pair<Double, Double> computeOperatingExpensesForCrop(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingProductUnit,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        PracticedIntervention intervention = interventionContext.getIntervention();

        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        CroppingPlanEntry crop = interventionContext.getCropWithSpecies().getCroppingPlanEntry();
        if (crop == null) {
            // no counter to increment
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        incrementAngGetTotalFieldCounterForTargetedId(intervention.getTopiaId());// seeding price

        final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
        double psci = getToolPSCi(intervention);

        return computeCropOperatingExpenses(
                writerContext,
                seedingActionUsage,
                practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput(),
                actionCropPricesIncludedTreatment,
                convertersBySeedingProductUnit,
                psci,
                intervention.getTopiaId(),
                indicatorClass,
                labels);
    }

    Pair<Double, Double> computeOperatingExpensesForCrop(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingProductUnit,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        EffectiveIntervention intervention = interventionContext.getIntervention();

        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        CroppingPlanEntry crop = interventionContext.getCroppingPlanEntry();
        if (crop == null) {
            // no counter to increment
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        incrementAngGetTotalFieldCounterForTargetedId(intervention.getTopiaId());// seeding price
        double psci = getToolPSCi(intervention);
        final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();

        return computeCropOperatingExpenses(
                writerContext,
                seedingActionUsage,
                domainContext.getRefInputPricesForCampaignsByInput(),
                actionCropPricesIncludedTreatment,
                convertersBySeedingProductUnit,
                psci,
                intervention.getTopiaId(),
                indicatorClass,
                labels);
    }

    Pair<Double, Double> computeOperatingExpensesForProduct(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingPhytoProductUnit,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        PracticedIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
        double psci = getToolPSCi(intervention);

        Map<? extends AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages = interventionContext.getRefVintageTargetIftDoseByUsages();
        return computeProductOperatingExpenses(
                writerContext,
                seedingActionUsage,
                practicedSystemContext.getPracticedSystemInputRefPricesByDomainInput(),
                actionCropPricesIncludedTreatment,
                psci,
                convertersBySeedingPhytoProductUnit,
                interventionId,
                indicatorClass,
                labels,
                refVintageTargetIftDoseByUsages);
    }

    Pair<Double, Double> computeOperatingExpensesForProduct(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingPhytoProductUnit,
            Class<? extends GenericIndicator> indicatorClass,
            String[] labels,
            Set<SeedingActionUsage> actionCropPricesIncludedTreatment) {
        EffectiveIntervention intervention = interventionContext.getIntervention();
        final String interventionId = intervention.getTopiaId();

        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
        double psci = getToolPSCi(intervention);

        Map<? extends AbstractInputUsage, UsagePerformanceResult> refVintageTargetIftDoseByUsages = interventionContext.getRefVintageTargetIftDoseByUsages();
        return computeProductOperatingExpenses(
                writerContext,
                seedingActionUsage,
                domainContext.getRefInputPricesForCampaignsByInput(),
                actionCropPricesIncludedTreatment,
                psci,
                convertersBySeedingPhytoProductUnit,
                interventionId,
                indicatorClass,
                labels,
                refVintageTargetIftDoseByUsages);
    }

    // practiced
    protected Pair<Double, Double> computeOperatingExpenses(
            WriterContext writerContext,
            PerformancePracticedSystemExecutionContext practicedSystemContext,
            PerformancePracticedInterventionExecutionContext interventionContext,
            Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingProductUnit,
            Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingPhytoProductUnit) {

        long start = System.currentTimeMillis();

        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        CroppingPlanEntry crop = interventionContext.getCropWithSpecies().getCroppingPlanEntry();
        if (crop == null) {
            // no counter to increment
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        Set<SeedingActionUsage> actionCropPricesIncludedTreatment = new HashSet<>();

        Pair<Double, Double> ci0 = computeOperatingExpensesForCrop(
                writerContext,
                practicedSystemContext,
                interventionContext,
                convertersBySeedingProductUnit,
                IndicatorOperatingExpenses.class,
                IndicatorOperatingExpenses.LABELS,
                actionCropPricesIncludedTreatment
        );

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("computeSeedingCropIndicator intervention:"
                    + interventionContext.getIntervention().getName()
                    + " (" + interventionContext.getIntervention().getTopiaId() + ")"
                    + ", practicedSystem:" + practicedSystemContext.getPracticedSystem().getName()
                    + "(" + practicedSystemContext.getPracticedSystem().getCampaigns() + " ) calculé en :"
                    + (System.currentTimeMillis() - start) + "ms");
        }

        start = System.currentTimeMillis();

        Pair<Double, Double> ci1 = computeOperatingExpensesForProduct(
                writerContext,
                practicedSystemContext,
                interventionContext,
                convertersBySeedingPhytoProductUnit,
                IndicatorOperatingExpenses.class,
                IndicatorOperatingExpenses.LABELS,
                actionCropPricesIncludedTreatment
        );

        var result = Pair.of(
                ci0.getLeft() + ci1.getLeft(),
                ci0.getRight() + ci1.getRight()
        );

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("computeSeedingProductIndicator intervention:"
                    + interventionContext.getIntervention().getName()
                    + " (" + interventionContext.getIntervention().getTopiaId() + ")"
                    + ", practicedSystem:" + practicedSystemContext.getPracticedSystem().getTopiaId()
                    + " calculé en :" + (System.currentTimeMillis() - start));
        }
        return result;
    }

    // effective
    protected Pair<Double, Double> computeOperatingExpenses(
            WriterContext writerContext,
            PerformanceEffectiveDomainExecutionContext domainContext,
            PerformanceZoneExecutionContext zoneContext,
            PerformanceEffectiveInterventionExecutionContext interventionContext,
            Map<SeedPlantUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingProductUnit,
            Map<PhytoProductUnit, List<RefInputUnitPriceUnitConverter>> convertersBySeedingPhytoProductUnit) {

        long start = System.currentTimeMillis();

        final Optional<SeedingActionUsage> optionalSeedingActionUsage = interventionContext.getOptionalSeedingActionUsage();
        if (optionalSeedingActionUsage.isEmpty()) {
            // return 0 to not take consideration of this indicator
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        CroppingPlanEntry crop = interventionContext.getCroppingPlanEntry();
        if (crop == null) {
            // no counter to increment
            return IndicatorOperatingExpenses.DEFAULT_REAL_AND_STANDARDIZED_VALUES;
        }

        final SeedingActionUsage seedingActionUsage = optionalSeedingActionUsage.get();
        Set<SeedingActionUsage> actionCropPricesIncludedTreatment = new HashSet<>();

        Pair<Double, Double> ci0 = computeOperatingExpensesForCrop(
                writerContext,
                domainContext,
                interventionContext,
                convertersBySeedingProductUnit,
                IndicatorOperatingExpenses.class,
                IndicatorOperatingExpenses.LABELS,
                actionCropPricesIncludedTreatment
        );

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("computeSeedingCropIndicator intervention:"
                    + interventionContext.getIntervention().getName()
                    + " (" + interventionContext.getIntervention().getTopiaId() + ")"
                    + ", zone:" + zoneContext.getZone().getTopiaId()
                    + " calculé en :" + (System.currentTimeMillis() - start));
        }

        start = System.currentTimeMillis();

        if (actionCropPricesIncludedTreatment.contains(seedingActionUsage)) {
            // product is incorporate into the seeding, there is no extra product to use
            return ci0;
        }

        // to be continued
        Pair<Double, Double> ci1 = computeOperatingExpensesForProduct(
                writerContext,
                domainContext,
                interventionContext,
                convertersBySeedingPhytoProductUnit,
                IndicatorOperatingExpenses.class,
                IndicatorOperatingExpenses.LABELS,
                actionCropPricesIncludedTreatment
        );

        var result = Pair.of(
                ci0.getLeft() + ci1.getLeft(),
                ci0.getRight() + ci1.getRight()
        );

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("computeSeedingProductIndicator intervention:"
                    + interventionContext.getIntervention().getName()
                    + " (" + interventionContext.getIntervention().getTopiaId() + ")"
                    + ", zone:" + zoneContext.getZone().getTopiaId()
                    + " calculé en :" + (System.currentTimeMillis() - start));
        }

        return result;
    }
}
