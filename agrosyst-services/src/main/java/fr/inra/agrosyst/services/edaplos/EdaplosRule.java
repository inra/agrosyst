package fr.inra.agrosyst.services.edaplos;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2017 - 2019 INRA, CodeLutin
 * Copyright (C) 2021 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.edaplos.model.PlotAgriculturalProcess;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.digester3.Rule;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.Attributes;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Digester rule to parse edaplos file.
 */
public class EdaplosRule extends Rule {

    private static final Log log = LogFactory.getLog(EdaplosRule.class);
    private static final String E_DAPLOS_PACKAGE_PATH = PlotAgriculturalProcess.class.getPackageName();//fr.inra.agrosyst.services.edaplos.model

    @Override
    public void begin(String namespace, String nameX, Attributes attributes) throws Exception {
        Object parent = getDigester().peek(0);
        String objectName = StringUtils.remove(nameX, '_');

        Method method = getSetOrAddMethod(parent, objectName);
        if (method == null) {
            if (log.isDebugEnabled()) {
                log.debug("Missing property " + parent.getClass().getName() + "#" + objectName);
            }
        } else {
            Class<?> aClass = method.getParameterTypes()[0];
            if (aClass.getName().startsWith(E_DAPLOS_PACKAGE_PATH)) {
                Object instance = aClass.getDeclaredConstructor().newInstance();

                if (log.isDebugEnabled()) {
                    log.debug("create " + instance.getClass().getName());
                }

                // marckup attributes
                for (int i = 0; i < attributes.getLength(); i++) {
                    String name = attributes.getLocalName(i);
                    String propName = objectName + StringUtils.capitalize(name);
                    String value = attributes.getValue(i);

                    Method propertyMethod = getSetOrAddMethod(instance, propName);
                    if (propertyMethod == null) {
                        if (log.isDebugEnabled() && !"schemaLocation".equals(name) /* faux positif */) {
                            log.debug("Missing property " + instance.getClass().getName() + "#" + propName);
                        }
                    } else {
                        propertyMethod.invoke(instance, value);
                        if (log.isDebugEnabled()) {
                            log.debug(" call " + instance.getClass().getName() + "#set" + propName + "(" + value + ")");
                        }
                    }
                }

                getDigester().push(instance);
            } else {
                // markup attributes
                for (int i = 0; i < attributes.getLength(); i++) {
                    String name = attributes.getLocalName(i);
                    String propName = objectName + StringUtils.capitalize(name);
                    String value = attributes.getValue(i);

                    Method propertyMethod = getSetOrAddMethod(parent, propName);
                    if (propertyMethod == null) {
                        if (log.isDebugEnabled()) {
                            log.debug("Missing property " + parent.getClass().getName() + "#" + propName);
                        }
                    } else {
                        propertyMethod.invoke(parent, value);
                        if (log.isDebugEnabled()) {
                            log.debug(" call " + parent.getClass().getName() + "#set" + propName + "(" + value + ")");
                        }
                    }
                }
            }
        }
    }

    protected Method getSetOrAddMethod(Object parent, String objectName) {
        Method[] methods = parent.getClass().getMethods();
        return Arrays.stream(methods)
                .filter(m -> m.getName().equals("set" + objectName) || m.getName().equals("add" + objectName))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void body(String namespace, String nameX, String text) throws Exception {
        String objectName = StringUtils.remove(nameX, '_');
        // /!\ 03/04/2020 Grosse verrue pour prendre en compte un mauvais format systerre
        // ne pas supprimer tant que systerre n'a pas corrigé
        if ("ApplicableTechnicalCharacteristics".equals(objectName)) {
            objectName = "ApplicableTechnicalCharacteristic";
        }
        String propertyName = StringUtils.uncapitalize(objectName);
        Object current = getDigester().peek(0);
        if (PropertyUtils.isWriteable(current, propertyName)) {
            PropertyUtils.setProperty(current, propertyName, text);
            if (log.isDebugEnabled()) {
                log.debug(" call " + current.getClass().getName() + "#set" + objectName + "(" + text + ")");
            }
        } else if (log.isDebugEnabled()) {
            String text2 = text.replaceAll("\\s", "");
            if (!text2.isEmpty()) {
                log.debug("Missing property " + current.getClass().getName() + "#" + objectName);
            }
        }
    }

    @Override
    public void end(String namespace, String nameX) throws Exception {
        Object current = getDigester().peek(0);
        Object parent = getDigester().peek(1);
        String objectName = StringUtils.remove(nameX, '_');

        Method method = getSetOrAddMethod(parent, objectName);
        if (method != null) {
            Class<?> aClass = method.getParameterTypes()[0];
            if (aClass.getName().startsWith(E_DAPLOS_PACKAGE_PATH)) {
                method.invoke(parent, current);
                if (log.isDebugEnabled()) {
                    log.debug(" call " + parent.getClass().getName() + "#" + method.getName() + "(" + current + ")");
                }

                Object instance = getDigester().pop();
                if (log.isDebugEnabled()) {
                    log.debug("remove (" + nameX + ") " + instance.getClass().getName());
                }
            }
        }
    }
}
