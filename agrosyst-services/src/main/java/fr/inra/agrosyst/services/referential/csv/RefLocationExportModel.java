package fr.inra.agrosyst.services.referential.csv;

import fr.inra.agrosyst.api.entities.referential.RefLocation;
import fr.inra.agrosyst.api.entities.referential.RefLocationImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;

public class RefLocationExportModel extends InternationalizationReferentialModel<RefLocation> implements ExportModel<RefLocation> {

    public RefLocationExportModel() {
        super(CSV_SEPARATOR);
    }

    @Override
    public RefLocation newEmptyInstance() {
        RefLocation refLocation = new RefLocationImpl();
        refLocation.setActive(true);
        return refLocation;
    }

    @Override
    public Iterable<ExportableColumn<RefLocation, Object>> getColumnsForExport() {
        ModelBuilder<RefLocation> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("codeInsee", RefLocation.PROPERTY_CODE_INSEE);
        modelBuilder.newColumnForExport("commune", RefLocation.PROPERTY_COMMUNE);
        modelBuilder.newColumnForExport("region", RefLocation.PROPERTY_REGION, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("departement", RefLocation.PROPERTY_DEPARTEMENT);
        modelBuilder.newColumnForExport("codePostal", RefLocation.PROPERTY_CODE_POSTAL);
        modelBuilder.newColumnForExport("petiteRegionAgricoleCode", RefLocation.PROPERTY_PETITE_REGION_AGRICOLE_CODE);
        modelBuilder.newColumnForExport("petiteRegionAgricoleNom", RefLocation.PROPERTY_PETITE_REGION_AGRICOLE_NOM);
        modelBuilder.newColumnForExport("pays", RefLocation.PROPERTY_REF_COUNTRY, TRIGRAM_FORMATTER);
        modelBuilder.newColumnForExport("latitude", RefLocation.PROPERTY_LATITUDE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("longitude", RefLocation.PROPERTY_LONGITUDE, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport("altitudeMoyenne", RefLocation.PROPERTY_ALTITUDE_MOY, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("aireAttraction", RefLocation.PROPERTY_AIRE_ATTR);
        modelBuilder.newColumnForExport("arrondissementCode", RefLocation.PROPERTY_ARRONDISSEMENT_CODE);
        modelBuilder.newColumnForExport("bassinVie", RefLocation.PROPERTY_BASSIN_VIE);
        modelBuilder.newColumnForExport("intercommunaliteCode", RefLocation.PROPERTY_INTERCOMMUNALITE_CODE);
        modelBuilder.newColumnForExport("uniteUrbaine", RefLocation.PROPERTY_UNITE_URBAINE);
        modelBuilder.newColumnForExport("zoneEmploi", RefLocation.PROPERTY_ZONE_EMPLOI);
        modelBuilder.newColumnForExport("bassinViticole", RefLocation.PROPERTY_BASSIN_VITICOLE);
        modelBuilder.newColumnForExport("regionPre2016", RefLocation.PROPERTY_ANCIENNE_REGION);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefLocation.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
