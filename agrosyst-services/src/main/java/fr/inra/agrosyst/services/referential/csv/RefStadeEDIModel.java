package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.referential.RefStadeEDI;
import fr.inra.agrosyst.api.entities.referential.RefStadeEDIImpl;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;


public class RefStadeEDIModel extends AbstractAgrosystModel<RefStadeEDI> implements ExportModel<RefStadeEDI> {

    public RefStadeEDIModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Famille de culture", RefStadeEDI.PROPERTY_FAMILLE_DE_CULTURE);
        newMandatoryColumn("2n", RefStadeEDI.PROPERTY_SECOND_N, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("3n", RefStadeEDI.PROPERTY_THIRD_N, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("4n", RefStadeEDI.PROPERTY_FOURTH_N, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Colonne1", RefStadeEDI.PROPERTY_COLONNE1);
        newMandatoryColumn("stade autre", RefStadeEDI.PROPERTY_STADE_AUTRE);
        newMandatoryColumn("stade producteur", RefStadeEDI.PROPERTY_STADE_PRODUCTEUR);
        newMandatoryColumn("Codes familles AGRICOMMAND", RefStadeEDI.PROPERTY_CODES_FAMILLES__AGRICOMMAND, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("Profil vegetatif", RefStadeEDI.PROPERTY_PROFIL_VEGETATIF, INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("AEE", RefStadeEDI.PROPERTY_AEE);
        newMandatoryColumn("Colonne2", RefStadeEDI.PROPERTY_COLONNE2);
        newMandatoryColumn("Plages de stades retenues (libellés)", RefStadeEDI.PROPERTY_PLAGES_DE_STADES_RETENUES_LIBELLES);
        newMandatoryColumn("Proposition APCA", RefStadeEDI.PROPERTY_PROPOSITION__APCA);
        newMandatoryColumn("Proposition DGAL (Vigne et blé)", RefStadeEDI.PROPERTY_PROPOSITION__DGAL__VIGNE_ET_BLE);
        newMandatoryColumn("ARVALIS/ACTA", RefStadeEDI.PROPERTY_ARVALIS_VS_ACTA);
        newMandatoryColumn("libéllé court institut du Lin", RefStadeEDI.PROPERTY_LIBELLE_COURT_INSTITUT_DU__LIN);
        newMandatoryColumn("source", RefStadeEDI.PROPERTY_SOURCE);
        newOptionalColumn(COLUMN_ACTIVE, RefStadeEDI.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public Iterable<ExportableColumn<RefStadeEDI, Object>> getColumnsForExport() {
        ModelBuilder<RefStadeEDI> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Famille de culture", RefStadeEDI.PROPERTY_FAMILLE_DE_CULTURE);
        modelBuilder.newColumnForExport("2n", RefStadeEDI.PROPERTY_SECOND_N, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("3n", RefStadeEDI.PROPERTY_THIRD_N, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("4n", RefStadeEDI.PROPERTY_FOURTH_N, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Colonne1", RefStadeEDI.PROPERTY_COLONNE1);
        modelBuilder.newColumnForExport("stade autre", RefStadeEDI.PROPERTY_STADE_AUTRE);
        modelBuilder.newColumnForExport("stade producteur", RefStadeEDI.PROPERTY_STADE_PRODUCTEUR);
        modelBuilder.newColumnForExport("Codes familles AGRICOMMAND", RefStadeEDI.PROPERTY_CODES_FAMILLES__AGRICOMMAND, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Profil vegetatif", RefStadeEDI.PROPERTY_PROFIL_VEGETATIF, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("AEE", RefStadeEDI.PROPERTY_AEE);
        modelBuilder.newColumnForExport("Colonne2", RefStadeEDI.PROPERTY_COLONNE2);
        modelBuilder.newColumnForExport("Plages de stades retenues (libellés)", RefStadeEDI.PROPERTY_PLAGES_DE_STADES_RETENUES_LIBELLES);
        modelBuilder.newColumnForExport("Proposition APCA", RefStadeEDI.PROPERTY_PROPOSITION__APCA);
        modelBuilder.newColumnForExport("Proposition DGAL (Vigne et blé)", RefStadeEDI.PROPERTY_PROPOSITION__DGAL__VIGNE_ET_BLE);
        modelBuilder.newColumnForExport("ARVALIS/ACTA", RefStadeEDI.PROPERTY_ARVALIS_VS_ACTA);
        modelBuilder.newColumnForExport("libéllé court institut du Lin", RefStadeEDI.PROPERTY_LIBELLE_COURT_INSTITUT_DU__LIN);
        modelBuilder.newColumnForExport("source", RefStadeEDI.PROPERTY_SOURCE);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefStadeEDI.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }

    @Override
    public RefStadeEDI newEmptyInstance() {
        RefStadeEDI refStadeEDI = new RefStadeEDIImpl();
        refStadeEDI.setActive(true);
        return refStadeEDI;
    }
}
