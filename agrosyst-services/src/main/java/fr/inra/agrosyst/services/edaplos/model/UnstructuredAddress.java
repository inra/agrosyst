package fr.inra.agrosyst.services.edaplos.model;

/*-
 * #%L
 * Agrosyst :: API
 * %%
 * Copyright (C) 2018 INRA, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * <pre>
 * {@code
 * <ram:PostcodeCode>31000</ram:PostcodeCode>
 * <ram:LineOne>rue Louis 14</ram:LineOne>
 * <ram:LineTwo>BP 14</ram:LineTwo>
 * }
 * </pre>
 */
public class UnstructuredAddress implements AgroEdiObject {

    protected String postcodeCode;

    protected String lineOne;

    protected String lineTwo;

    protected String cityName;

    protected String cityID;

    public String getPostcodeCode() {
        return postcodeCode;
    }

    public void setPostcodeCode(String postcodeCode) {
        this.postcodeCode = postcodeCode;
    }

    public String getLineOne() {
        return lineOne;
    }

    public void setLineOne(String lineOne) {
        this.lineOne = lineOne;
    }

    public String getLineTwo() {
        return lineTwo;
    }

    public void setLineTwo(String lineTwo) {
        this.lineTwo = lineTwo;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    @Override
    public String getLocalizedIdentifier() {
        return "";
    }
}
