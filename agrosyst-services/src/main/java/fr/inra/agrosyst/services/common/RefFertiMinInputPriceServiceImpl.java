package fr.inra.agrosyst.services.common;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2024 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.inra.agrosyst.api.entities.AbstractDomainInputStockUnit;
import fr.inra.agrosyst.api.entities.DomainMineralProductInput;
import fr.inra.agrosyst.api.entities.DomainMineralProductInputImpl;
import fr.inra.agrosyst.api.entities.MineralProductUnit;
import fr.inra.agrosyst.api.entities.PriceUnit;
import fr.inra.agrosyst.api.entities.referential.FertiMinElement;
import fr.inra.agrosyst.api.entities.referential.RefFertiMinUNIFA;
import fr.inra.agrosyst.api.entities.referential.RefInputPrice;
import fr.inra.agrosyst.api.entities.referential.RefInputPriceImpl;
import fr.inra.agrosyst.api.entities.referential.RefInputUnitPriceUnitConverter;
import fr.inra.agrosyst.api.entities.referential.RefPrixFertiMin;
import fr.inra.agrosyst.api.services.common.PriceAndUnit;
import fr.inra.agrosyst.api.services.performance.InputRefPrice;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class RefFertiMinInputPriceServiceImpl {
    
    Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> getRefInputPriceForScenarios(
            Collection<String> scenarioCodes,
            Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
            Collection<AbstractDomainInputStockUnit> inputs,
            Collection<RefPrixFertiMin> refPrixForFertiMinElements) {
        
        // forme, categ, element -> RefPrixFertiMin
        Map<AbstractDomainInputStockUnit, Map<String, Optional<InputRefPrice>>> mineralInputRefPrice = new HashMap<>();
        Map<RefPrixFertiMinKey, Set<RefPrixFertiMin>> refPriceForFormeCategAndElements = getRefPrixFertiMinByFormeCategAndElement(refPrixForFertiMinElements);
        
        Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByMineralProductUnit = getPriceConvertersByMineralProductUnits(refInputUnitPriceUnitConverters);
        
        for (AbstractDomainInputStockUnit input : inputs) {
            final DomainMineralProductInput domainMineralProductInput = (DomainMineralProductInput) input;
            RefFertiMinUNIFA mineralProduct = (domainMineralProductInput.getRefInput());
            Optional<Set<RefPrixFertiMin>> fertiMinElementRefPrices = getMineralProductRefFertiMinElementPrices(refPriceForFormeCategAndElements, mineralProduct);
            
            // no refPrices found, return no results
            if (fertiMinElementRefPrices.isEmpty()) {
                for (String scenarioCode : scenarioCodes) {
                    // add no average ref input price for scenario
                    Map<String, Optional<InputRefPrice>> mineralInputRePriceForScenarioCode = mineralInputRefPrice.computeIfAbsent(input, k -> new HashMap<>());
                    mineralInputRePriceForScenarioCode.put(scenarioCode, Optional.empty());
                }
            }
            
            Map<String, Set<RefPrixFertiMin>> inputRefPricesByScenarioCode = getInputRefPrixFertiMinByScenarioCode(fertiMinElementRefPrices);
    
            final MineralProductUnit usageUnit = domainMineralProductInput.getUsageUnit();
            
            List<RefInputUnitPriceUnitConverter> doseUnitConverters = convertersByMineralProductUnit.get(usageUnit);
            
            Map<FertiMinElement, Double> fertilizingUnitsFor100kgByElements = getFertilizingUnitsFor100kgByElements(mineralProduct);
            
            for (String scenarioCode : scenarioCodes) {
                
                Set<RefPrixFertiMin> refScenarioPrices = inputRefPricesByScenarioCode.get(scenarioCode);
                
                // no refPrices found for this scenario code, set no result then continue to next scenario code
                if (CollectionUtils.isEmpty(refScenarioPrices)) {
    
                    Map<String, Optional<InputRefPrice>> mineralInputRePriceForScenarioCode = mineralInputRefPrice.computeIfAbsent(input, k -> new HashMap<>());
                    mineralInputRePriceForScenarioCode.put(scenarioCode, Optional.empty());
                    continue;
                    
                }
                
                // add refPrices by elements
                Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement = getRefPricesByElements(
                        input, doseUnitConverters, fertilizingUnitsFor100kgByElements, refScenarioPrices);
    
                // comput average scenario price for elements
                PriceAndUnit averagePrice = computeAverageRefPrice(fertilizingUnitsFor100kgByElements, refPricesByElement);
                // add average ref input price for scenario
                InputRefPrice mineralInputRePrice = new InputRefPrice(
                        input.getTopiaId(),
                        Optional.of(scenarioCode),
                        Optional.empty(),
                        Optional.empty(),
                        averagePrice,
                        false);
                
                Map<String, Optional<InputRefPrice>> mineralInputRePriceForScenarioCode = mineralInputRefPrice.computeIfAbsent(input, k -> new HashMap<>());
                mineralInputRePriceForScenarioCode.put(scenarioCode, Optional.of(mineralInputRePrice));
                
            }
        }
        return mineralInputRefPrice;
    }
    
    private Map<FertiMinElement, Double> getFertilizingUnitsFor100kgByElements(RefFertiMinUNIFA mineralProduct) {
        Map<FertiMinElement, Double> fertilizingUnitsFor100kgByElements = new HashMap<>();
        
        if (mineralProduct.getN() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.N, mineralProduct.getN());
        if (mineralProduct.getP2O5() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.P2_O5, mineralProduct.getP2O5());
        if (mineralProduct.getK2O() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.K2_O, mineralProduct.getK2O());
        if (mineralProduct.getBore() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.BORE, mineralProduct.getBore());
        if (mineralProduct.getCalcium() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.CALCIUM, mineralProduct.getCalcium());
        if (mineralProduct.getFer() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.FER, mineralProduct.getFer());
        if (mineralProduct.getManganese() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.MANGANESE, mineralProduct.getManganese());
        if (mineralProduct.getMolybdene() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.MOLYBDENE, mineralProduct.getMolybdene());
        if (mineralProduct.getMgO() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.MG_O, mineralProduct.getMgO());
        if (mineralProduct.getOxyde_de_sodium() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.OXYDE_DE_SODIUM, mineralProduct.getOxyde_de_sodium());
        if (mineralProduct.getsO3() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.S_O3, mineralProduct.getsO3());
        if (mineralProduct.getCuivre() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.CUIVRE, mineralProduct.getCuivre());
        if (mineralProduct.getZinc() != 0) fertilizingUnitsFor100kgByElements.put(FertiMinElement.ZINC, mineralProduct.getZinc());
        return fertilizingUnitsFor100kgByElements;
    }
    
    private Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> getPriceConvertersByMineralProductUnits(Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters) {
        Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByMineralProductUnit = new HashMap<>();
        Collection<RefInputUnitPriceUnitConverter> mineralRefInputUnitPriceUnitConverters = new ArrayList<>(refInputUnitPriceUnitConverters);
        mineralRefInputUnitPriceUnitConverters.removeIf(ripc -> ripc.getMineralProductUnit() == null);
        for (RefInputUnitPriceUnitConverter mineralProductUnit : mineralRefInputUnitPriceUnitConverters) {
            List<RefInputUnitPriceUnitConverter> convertersForUnit = convertersByMineralProductUnit.computeIfAbsent(mineralProductUnit.getMineralProductUnit(), k -> new ArrayList<>());
            convertersForUnit.add(mineralProductUnit);
        }
        return convertersByMineralProductUnit;
    }
    
    protected Optional<Pair<MineralProductUnit, PriceAndUnit>> getDoseUnitToPriceUnitConversionRate(
            Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> priceUnitToConverters,
            RefInputPrice refInputPrice,
            AbstractDomainInputStockUnit input) {
        
        Pair<MineralProductUnit, PriceAndUnit> conversionRateForDoseUnit = null;
        
        if (priceUnitToConverters != null) {
            
            final PriceUnit priceUnit = refInputPrice.getUnit();
            final Collection<RefInputUnitPriceUnitConverter> convertersForPriceUnit = CollectionUtils.emptyIfNull(priceUnitToConverters.get(priceUnit));
            final DomainMineralProductInputImpl mineralProductInput = (DomainMineralProductInputImpl) input;
            final MineralProductUnit mProductUnit = mineralProductInput.getUsageUnit();
    
            PriceUnit refPriceUnit;

            Optional<RefInputUnitPriceUnitConverter> converterForInputUnit = convertersForPriceUnit.stream()
                    .filter(refConverter -> refConverter.getMineralProductUnit() != null && refConverter.getMineralProductUnit().equals(mProductUnit))
                    .findAny();

            Double conversionRate = null;
            if (converterForInputUnit.isPresent()) {
                final RefInputUnitPriceUnitConverter refInputUnitPriceUnitConverter = converterForInputUnit.get();
                refPriceUnit = refInputUnitPriceUnitConverter.getPriceUnit();
                conversionRate = refInputUnitPriceUnitConverter.getConvertionRate();
                conversionRateForDoseUnit = Pair.of(mProductUnit, new PriceAndUnit(conversionRate, refPriceUnit));
            } else {
                // si pas trouvé, essayer avec une unité cohérente

                final Iterator<PriceUnit> priceUnitIterator = priceUnitToConverters.keySet().iterator();
                while (priceUnitIterator.hasNext() && conversionRate == null) {
                    PriceUnit currentPriceUnit = priceUnitIterator.next();
                    final List<RefInputUnitPriceUnitConverter> currentDoseUnitToPriceUnitConverters = priceUnitToConverters.get(currentPriceUnit);
                    Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(priceUnit, currentPriceUnit));
                    if (priceUnitConversionRate != null
                            && priceUnitConversionRate > 0 &&
                            currentDoseUnitToPriceUnitConverters != null &&
                            !currentDoseUnitToPriceUnitConverters.isEmpty()) {
                        
                        final Optional<RefInputUnitPriceUnitConverter> valuatedRefInputUnitPriceUnitConverter  =
                                currentDoseUnitToPriceUnitConverters
                                        .stream()
                                        .filter(
                                                converter -> converter.getMineralProductUnit() != null
                                                        && converter.getMineralProductUnit().equals(mProductUnit)
                                                        && converter.getConvertionRate() > 0.0)
                                        .findAny();
    
                        if (valuatedRefInputUnitPriceUnitConverter.isPresent()) {
                            final RefInputUnitPriceUnitConverter refInputUnitPriceUnitConverter = valuatedRefInputUnitPriceUnitConverter.get();
                            conversionRate = refInputUnitPriceUnitConverter.getConvertionRate() * priceUnitConversionRate;
                            refPriceUnit = refInputUnitPriceUnitConverter.getPriceUnit();
                            conversionRateForDoseUnit = Pair.of(mProductUnit, new PriceAndUnit(conversionRate, refPriceUnit));
                        }
                    }
                }
                
            }
        }
        
        return Optional.ofNullable(conversionRateForDoseUnit);
    }
    
    protected Map<String, Set<RefPrixFertiMin>> getInputRefPrixFertiMinByScenarioCode(Optional<Set<RefPrixFertiMin>> inputRefPrices) {
        Map<String, Set<RefPrixFertiMin>> inputRefPricesByScenarioCode = new HashMap<>();
        if (inputRefPrices.isPresent()) {
            for (RefPrixFertiMin inputRefPrice : inputRefPrices.get()) {
                String scenarioCode = inputRefPrice.getCode_scenario();
                Set<RefPrixFertiMin> inputRefPriceForScenarioCode = inputRefPricesByScenarioCode.computeIfAbsent(scenarioCode, k -> new HashSet<>());
                inputRefPriceForScenarioCode.add(inputRefPrice);
            }
        }
        return inputRefPricesByScenarioCode;
    }
    
    /**
     * Return all refPrices found for the given RefProduct
     * @param refPriceForFormeCategAndElements ref prices for ref input
     * @param mineralProduct the given RefProduct
     * @return Optional.empty if all the elements ref prices are not found
     */
    protected Optional<Set<RefPrixFertiMin>> getMineralProductRefFertiMinElementPrices(
            Map<RefPrixFertiMinKey, Set<RefPrixFertiMin>> refPriceForFormeCategAndElements,
            RefFertiMinUNIFA mineralProduct) {
        
        String forme = StringUtils.lowerCase(StringUtils.stripAccents(mineralProduct.getForme()));
        Integer categ = mineralProduct.getCateg();
        Set<FertiMinElement> elements = getProductElements(mineralProduct);
        
        Set<RefPrixFertiMin> inputRefPrices = new HashSet<>();
        for (FertiMinElement element : elements) {
            RefPrixFertiMinKey key = new RefPrixFertiMinKey(forme,categ,element);
            Set<RefPrixFertiMin> refPrices = refPriceForFormeCategAndElements.get(key);
            if (refPrices == null) {
                inputRefPrices = null;
                break;
            }
            inputRefPrices.addAll(refPrices);
        }
        return Optional.ofNullable(inputRefPrices);
    }
    
    protected static Set<FertiMinElement> getProductElements(RefFertiMinUNIFA product) {
        Set<FertiMinElement> productElements = new HashSet<>();
        if (product.getN() != 0) {
            productElements.add(FertiMinElement.N);
        }
        if (product.getP2O5() != 0) {
            productElements.add(FertiMinElement.P2_O5);
        }
        if (product.getK2O() != 0) {
            productElements.add(FertiMinElement.K2_O);
        }
        if (product.getBore() != 0) {
            productElements.add(FertiMinElement.BORE);
        }
        if (product.getCalcium() != 0) {
            productElements.add(FertiMinElement.CALCIUM);
        }
        if (product.getFer() != 0) {
            productElements.add(FertiMinElement.FER);
        }
        if (product.getManganese() != 0) {
            productElements.add(FertiMinElement.MANGANESE);
        }
        if (product.getMolybdene() != 0) {
            productElements.add(FertiMinElement.MOLYBDENE);
        }
        if (product.getMgO() != 0) {
            productElements.add(FertiMinElement.MG_O);
        }
        if (product.getOxyde_de_sodium() != 0) {
            productElements.add(FertiMinElement.OXYDE_DE_SODIUM);
        }
        if (product.getsO3() != 0) {
            productElements.add(FertiMinElement.S_O3);
        }
        if (product.getCuivre() != 0) {
            productElements.add(FertiMinElement.CUIVRE);
        }
        if (product.getZinc() != 0) {
            productElements.add(FertiMinElement.ZINC);
        }
        return productElements;
    }
    
    protected Map<RefPrixFertiMinKey, Set<RefPrixFertiMin>> getRefPrixFertiMinByFormeCategAndElement(Collection<RefPrixFertiMin> pricesResult) {
        Map<RefPrixFertiMinKey, Set<RefPrixFertiMin>> refPriceForFormeCategAndElements = new HashMap<>();
        for (RefPrixFertiMin refPrixFertiMin : pricesResult) {
            String forme =  StringUtils.lowerCase(StringUtils.stripAccents(refPrixFertiMin.getForme()));
            Integer categ = refPrixFertiMin.getCateg();
            FertiMinElement element = refPrixFertiMin.getElement();
            RefPrixFertiMinKey key = new RefPrixFertiMinKey(forme, categ, element);
            Set<RefPrixFertiMin> refPricesForKey = refPriceForFormeCategAndElements.computeIfAbsent(key, k -> new HashSet<>());
            refPricesForKey.add(refPrixFertiMin);
        }
        return refPriceForFormeCategAndElements;
    }
    
    record RefPrixFertiMinKey(String forme, Integer categ, FertiMinElement element){}
    
    public Map<? extends AbstractDomainInputStockUnit, ? extends Map<Integer, Optional<InputRefPrice>>> getRefInputPriceForCampaigns(//ICI
                                                                                                                                     Collection<Integer> campaigns,
                                                                                                                                     Collection<RefInputUnitPriceUnitConverter> refInputUnitPriceUnitConverters,
                                                                                                                                     List<AbstractDomainInputStockUnit> inputs,
                                                                                                                                     Collection<RefPrixFertiMin> refPrixForFertiMinElements) {
    
        // forme, categ, element -> RefPrixFertiMin
        Map<AbstractDomainInputStockUnit, Map<Integer, Optional<InputRefPrice>>> mineralInputRefPrice = new HashMap<>();
        Map<RefPrixFertiMinKey, Set<RefPrixFertiMin>> refPriceForFormeCategAndElements = getRefPrixFertiMinByFormeCategAndElement(refPrixForFertiMinElements);
    
        Map<MineralProductUnit, List<RefInputUnitPriceUnitConverter>> convertersByMineralProductUnit = getPriceConvertersByMineralProductUnits(refInputUnitPriceUnitConverters);
        
        for (AbstractDomainInputStockUnit input : inputs) {
            
            final DomainMineralProductInput domainMineralProductInput = (DomainMineralProductInput) input;
            RefFertiMinUNIFA mineralProduct = domainMineralProductInput.getRefInput();
            
            Optional<Set<RefPrixFertiMin>> fertiMinElementRefPrices = getMineralProductRefFertiMinElementPrices(refPriceForFormeCategAndElements, mineralProduct);
        
            // no refPrices found, return no results
            if (fertiMinElementRefPrices.isEmpty()) {
                for (Integer campaign : campaigns) {
                    // add no average ref input price for campaign
                    Map<Integer, Optional<InputRefPrice>> mineralInputRePriceForKey = mineralInputRefPrice.computeIfAbsent(input, k -> new HashMap<>());
                    mineralInputRePriceForKey.put(campaign, Optional.empty());
                }
            }
        
            Map<Integer, Set<RefPrixFertiMin>> inputRefPricesByKey = getInputRefPrixFertiMinByCampaign(fertiMinElementRefPrices);
        
            List<RefInputUnitPriceUnitConverter> doseUnitConverters = convertersByMineralProductUnit.get(domainMineralProductInput.getUsageUnit());
        
            Map<FertiMinElement, Double> fertilizingUnitsFor100kgByElements = getFertilizingUnitsFor100kgByElements(mineralProduct);
        
            for (Integer campaign : campaigns) {
            
                Set<RefPrixFertiMin> refCampaignsPrices = inputRefPricesByKey.get(campaign);
            
                // no refPrices found for this scenario code, set no result then continue to next scenario code
                if (CollectionUtils.isEmpty(refCampaignsPrices)) {
                    Map<Integer, Optional<InputRefPrice>> mineralInputRePriceForCampaign = mineralInputRefPrice.computeIfAbsent(input, k -> new HashMap<>());
                    mineralInputRePriceForCampaign.put(campaign, Optional.empty());
                    continue;
                
                }
            
                // add refPrices by elements
                Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement = getRefPricesByElements(
                        input,
                        doseUnitConverters,
                        fertilizingUnitsFor100kgByElements,
                        refCampaignsPrices);
    
                // comput average campaign price for elements
                PriceAndUnit averagePriceForElement = computeAverageRefPrice(fertilizingUnitsFor100kgByElements, refPricesByElement);
    
                // add average ref input price for scenario
                InputRefPrice mineralInputRePrice = new InputRefPrice(
                        input.getTopiaId(),
                        Optional.empty(),
                        Optional.of(campaign),
                        Optional.empty(),
                        averagePriceForElement,
                        false);
                
                Map<Integer, Optional<InputRefPrice>> mineralInputRePriceForCampaign = mineralInputRefPrice.computeIfAbsent(input, k -> new HashMap<>());
                mineralInputRePriceForCampaign.put(campaign, Optional.of(mineralInputRePrice));
            
            }
        }
        return mineralInputRefPrice;
    }
    
    protected static PriceAndUnit computeAverageRefPrice(
            Map<FertiMinElement, Double> fertilizingUnitsFor100kgByElements,
            Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement) {

        double averageRefPrice = 0.0;

        Map<FertiMinElement, PriceAndUnit> pricesByElements = getAverageRefPriceCommonUnitByElement(refPricesByElement);

        if (pricesByElements.isEmpty()) {
            return new PriceAndUnit(averageRefPrice, PriceUnit.EURO_HA);
        }

        PriceUnit unit = null;
        for (Map.Entry<FertiMinElement, PriceAndUnit> refPriceForElement : pricesByElements.entrySet()) {
            FertiMinElement element = refPriceForElement.getKey();
            PriceAndUnit refPrice = refPriceForElement.getValue();
            unit = refPrice.unit();
            double fertilizingUnitsFor100kg = fertilizingUnitsFor100kgByElements.get(element);
            double weightRefPrice = (refPrice.value() * fertilizingUnitsFor100kg) / 100;
            averageRefPrice += weightRefPrice;
        }

        return new PriceAndUnit(averageRefPrice, unit);
    }

    private static Map<FertiMinElement, PriceAndUnit> getAverageRefPriceCommonUnitByElement(Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement) {

        Map<PriceUnit, AtomicInteger> distinctUnitCountForEachElements = countDistinctPriceUnitByElements(refPricesByElement);

        Optional<Map.Entry<PriceUnit, AtomicInteger>> optionalPriceUnitAtomicIntegerEntry = distinctUnitCountForEachElements.entrySet()
                .stream().filter(dcfee -> dcfee.getValue().get() == refPricesByElement.keySet().size()).findAny();

        Map<FertiMinElement, PriceAndUnit> averageRefPriceByElement;
        if (optionalPriceUnitAtomicIntegerEntry.isPresent()) {
            averageRefPriceByElement = computeResultWithCommonMostUsedUnit(refPricesByElement, optionalPriceUnitAtomicIntegerEntry);
        } else {
            averageRefPriceByElement = computResultWithConvertedUnit(refPricesByElement, distinctUnitCountForEachElements);
        }

        Map<FertiMinElement, PriceAndUnit> result = computGlobalFertiMinElementPriceAndUnit(refPricesByElement, averageRefPriceByElement);

        return result;
    }

    protected static Map<FertiMinElement, PriceAndUnit> computGlobalFertiMinElementPriceAndUnit(Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement, Map<FertiMinElement, PriceAndUnit> averageRefPriceByElement) {
        boolean refPriceFoundForAllElements = averageRefPriceByElement.keySet().equals(refPricesByElement.keySet());
        if (!refPriceFoundForAllElements) {
            averageRefPriceByElement = new HashMap<>();
        }
        return averageRefPriceByElement;
    }

    protected static Map<FertiMinElement, PriceAndUnit> computResultWithConvertedUnit(
            Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement,
            Map<PriceUnit, AtomicInteger> distinctUnitCountForEachElements) {

        Map<FertiMinElement, PriceAndUnit> averageRefPriceByElement = new HashMap<>();
        Set<PriceUnit> refPriceUnitFounds = distinctUnitCountForEachElements.keySet();

        PriceUnit refPu;

        for (PriceUnit refPriceUnitFound : refPriceUnitFounds) {
            refPu = refPriceUnitFound;

            Map<FertiMinElement, PriceAndUnit> resultForCurentPriceUnit = new HashMap<>();
            for (Map.Entry<FertiMinElement, List<PriceAndUnit>> fertiMinElementListEntry : refPricesByElement.entrySet()) {
                FertiMinElement element = fertiMinElementListEntry.getKey();
                List<PriceAndUnit> value = fertiMinElementListEntry.getValue();
                for (PriceAndUnit priceAndUnit : value) {
                    PriceUnit currentPriceUnit = priceAndUnit.unit();
                    Double priceUnitConversionRate = RefInputPriceImpl.GET_PRICE_UNIT_TO_OTHER_PRICE_UNIT_CONVERSION_RATE.apply(Pair.of(refPu, currentPriceUnit));
                    if (priceUnitConversionRate != null && priceUnitConversionRate > 0) {
                        // converter found for this unit and element;
                        resultForCurentPriceUnit.put(element, new PriceAndUnit(priceUnitConversionRate * priceAndUnit.value(), refPu));
                        break;
                    }

                }
            }
            if (resultForCurentPriceUnit.keySet().equals(refPricesByElement.keySet())) {
                // price found for all elements
                averageRefPriceByElement = resultForCurentPriceUnit;
                break;
            }

        }
        return averageRefPriceByElement;
    }

    protected static Map<FertiMinElement, PriceAndUnit> computeResultWithCommonMostUsedUnit(
            Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement,
            Optional<Map.Entry<PriceUnit, AtomicInteger>> optionalPriceUnitAtomicIntegerEntry) {
        Map<FertiMinElement, PriceAndUnit> averageRefPriceByElement = new HashMap<>();
        if (optionalPriceUnitAtomicIntegerEntry.isPresent()) {
            Map.Entry<PriceUnit, AtomicInteger> priceUnitAtomicIntegerEntry = optionalPriceUnitAtomicIntegerEntry.get();
            PriceUnit refPu = priceUnitAtomicIntegerEntry.getKey();
            for (Map.Entry<FertiMinElement, List<PriceAndUnit>> fertiMinElementListEntry : refPricesByElement.entrySet()) {
                FertiMinElement element = fertiMinElementListEntry.getKey();
                double averagePrices = fertiMinElementListEntry.getValue().stream().filter(rp -> refPu.equals(rp.unit())).mapToDouble(PriceAndUnit::value).average().orElse(0);
                averageRefPriceByElement.put(element, new PriceAndUnit(averagePrices, refPu));
            }
        }
        return averageRefPriceByElement;
    }

    protected static Map<PriceUnit, AtomicInteger> countDistinctPriceUnitByElements(Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement) {
        Map<PriceUnit, AtomicInteger> priceUnitCount = new HashMap<>();
        for (Map.Entry<FertiMinElement, List<PriceAndUnit>> priceAndUnitForElement : refPricesByElement.entrySet()) {
            Set<PriceUnit> priceUnitsForCurentElement = priceAndUnitForElement.getValue().stream().map(PriceAndUnit::unit).collect(Collectors.toSet());
            priceUnitsForCurentElement.forEach(
                    u -> {
                        AtomicInteger counter = priceUnitCount.computeIfAbsent(u, k -> new AtomicInteger(0));
                        counter.addAndGet(1);
                    }
            );
        }
        return priceUnitCount;
    }

    protected Map<FertiMinElement, List<PriceAndUnit>> getRefPricesByElements(
            AbstractDomainInputStockUnit input,
            List<RefInputUnitPriceUnitConverter> doseUnitConverters,
            Map<FertiMinElement, Double> fertilizingUnitsFor100kgByElements,
            Set<RefPrixFertiMin> refPrixFertiMins) {
        
        Map<FertiMinElement, List<PriceAndUnit>> refPricesByElement = new HashMap<>();
        for (RefPrixFertiMin refPrice : refPrixFertiMins) {
        
            final FertiMinElement element = refPrice.getElement();
            Double fertilizingUnitsFor100kg = fertilizingUnitsFor100kgByElements.get(element);
            // si une quantité d'unités fertilisantes/100 kg est renseignée pour l'élément
            if (fertilizingUnitsFor100kg != null) {
    
                Map<PriceUnit, List<RefInputUnitPriceUnitConverter>> doseUnitToPriceUnitConverterByPriceUnit = RefInputPriceServiceImpl.getConvertersByPriceUnits(doseUnitConverters);

                Optional<Pair<MineralProductUnit, PriceAndUnit>> converter = getDoseUnitToPriceUnitConversionRate(
                        doseUnitToPriceUnitConverterByPriceUnit,
                        refPrice,
                        input);
    
                if (converter.isPresent()) {
                    final PriceAndUnit conversion = converter.get().getValue();
                    double currentRefPrice = refPrice.getPrice() * conversion.value();
                    final PriceAndUnit convertedRefPriceForElement = new PriceAndUnit(currentRefPrice, conversion.unit());
                    
                    refPricesByElement.computeIfAbsent(element, k -> Lists.newArrayList())
                            .add(convertedRefPriceForElement);
                }
            }
        
        }
        return refPricesByElement;
    }

    protected Map<Integer, Set<RefPrixFertiMin>> getInputRefPrixFertiMinByCampaign(Optional<Set<RefPrixFertiMin>> inputRefPrices) {
        Map<Integer, Set<RefPrixFertiMin>> inputRefPricesByScenarioCode = new HashMap<>();
        if (inputRefPrices.isPresent()) {
            for (RefPrixFertiMin inputRefPrice : inputRefPrices.get()) {
                Integer key = inputRefPrice.getCampaign();
                Set<RefPrixFertiMin> inputRefPriceForScenarioCode = inputRefPricesByScenarioCode.computeIfAbsent(key, k -> new HashSet<>());
                inputRefPriceForScenarioCode.add(inputRefPrice);
            }
        }
        return inputRefPricesByScenarioCode;
    }
}
