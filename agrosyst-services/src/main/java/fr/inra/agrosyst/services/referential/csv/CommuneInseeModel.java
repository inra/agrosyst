package fr.inra.agrosyst.services.referential.csv;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2014 INRA
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Locale;

/**
 * CommuneInseeModel import model
 * 
 * Columns:
 * <ul>
 * <li>CDC
 * <li>CHEFLIEU
 * <li>REG
 * <li>DEP
 * <li>COM
 * <li>AR
 * <li>CT
 * <li>TNCC
 * <li>ARTMAJ
 * <li>NCC
 * <li>ARTMIN
 * <li>NCCENR
 * </ul>
 * @author David Cossé
 */
public class CommuneInseeModel extends AbstractAgrosystModel<RefLocationDto> {

    public CommuneInseeModel() {
        super(CSV_SEPARATOR);

        newIgnoredColumn("CDC");
        newIgnoredColumn("CHEFLIEU");
        newMandatoryColumn("REG", "region", INTEGER_WITH_NULL_PARSER);
        newMandatoryColumn("DEP", "departement");
        newMandatoryColumn("COM", "codeCommune");
        newIgnoredColumn("AR");
        newIgnoredColumn("CT");
        newIgnoredColumn("TNCC");
        newIgnoredColumn("ARTMAJ");
        newIgnoredColumn("NCC");
        newMandatoryColumn("ARTMIN", "articleCommune");
        newMandatoryColumn("NCCENR", "commune");
    }

    @Override
    public RefLocationDto newEmptyInstance() {
        RefLocationDto instance = new RefLocationDto();
        instance.setPays(Locale.FRANCE.getISO3Country().toLowerCase());
        return instance;
    }

}
