package fr.inra.agrosyst.services.referential.csv.iphy;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.MaxSlope;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesuRunoffPotRulesParc;
import fr.inra.agrosyst.api.entities.referential.iphy.RefRcesuRunoffPotRulesParcImpl;
import fr.inra.agrosyst.services.referential.csv.AbstractAgrosystModel;
import org.nuiton.csv.ExportModel;
import org.nuiton.csv.ExportableColumn;
import org.nuiton.csv.ModelBuilder;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ValueParser;

/**
 * Id règle;Soil texture;Slope;battance;hydromorphisms;Runoff potential
 * 
 * @author Eric Chatellier
 */
public class RefRcesuRunoffPotRulesParcModel extends AbstractAgrosystModel<RefRcesuRunoffPotRulesParc> implements ExportModel<RefRcesuRunoffPotRulesParc> {

    protected static final ValueParser<MaxSlope> MAX_SLOPE_PARSER = value -> {
        MaxSlope result;
        if ("0%".equals(value)) {
            result = MaxSlope.ZERO;
        } else if ("0-2%".equals(value)) {
            result = MaxSlope.ZERO_TO_TWO;
        } else if ("2-5%".equals(value)) {
            result = MaxSlope.TWO_TO_FIVE;
        } else {
            // >5%
            result = MaxSlope.MORE_THAN_FIVE;
        }
        return result;
    };

    protected static final ValueFormatter<MaxSlope> MAX_SLOPE_FORMATTER = value -> {
        String result = switch (value) {
            case ZERO -> "0%";
            case ZERO_TO_TWO -> "0-2%";
            case TWO_TO_FIVE -> "2-5%";
            default -> ">5%";
        };
        return result;
    };

    protected static final ValueParser<Boolean> OUI_NON_PARSER = value -> {
        boolean result = "oui".equalsIgnoreCase(value);
        return result;
    };

    protected static final ValueFormatter<Boolean> OUI_NON_FORMATTER = value -> {
        String result = value != null && value ? "oui" : "non";
        return result;
    };

    public RefRcesuRunoffPotRulesParcModel() {
        super(CSV_SEPARATOR);
        newMandatoryColumn("Id règle", RefRcesuRunoffPotRulesParc.PROPERTY_ID_REGLE, INT_PARSER);
        newMandatoryColumn("Soil texture", RefRcesuRunoffPotRulesParc.PROPERTY_SOIL_TEXTURE);
        newMandatoryColumn("Slope", RefRcesuRunoffPotRulesParc.PROPERTY_SLOPE, MAX_SLOPE_PARSER);
        newMandatoryColumn("battance", RefRcesuRunoffPotRulesParc.PROPERTY_BATTANCE, OUI_NON_PARSER);
        newMandatoryColumn("hydromorphisms", RefRcesuRunoffPotRulesParc.PROPERTY_HYDROMORPHISMS, OUI_NON_PARSER);
        newMandatoryColumn("Runoff potential", RefRcesuRunoffPotRulesParc.PROPERTY_RUNOFF_POTENTIAL, DOUBLE_PARSER);
        newOptionalColumn(COLUMN_ACTIVE, RefRcesuRunoffPotRulesParc.PROPERTY_ACTIVE, ACTIVE_PARSER);
    }

    @Override
    public RefRcesuRunoffPotRulesParc newEmptyInstance() {
        RefRcesuRunoffPotRulesParc result = new RefRcesuRunoffPotRulesParcImpl();
        result.setActive(true);
        return result;
    }

    @Override
    public Iterable<ExportableColumn<RefRcesuRunoffPotRulesParc, Object>> getColumnsForExport() {
        ModelBuilder<RefRcesuRunoffPotRulesParc> modelBuilder = new ModelBuilder<>();
        modelBuilder.newColumnForExport("Id règle", RefRcesuRunoffPotRulesParc.PROPERTY_ID_REGLE, INTEGER_FORMATTER);
        modelBuilder.newColumnForExport("Soil texture", RefRcesuRunoffPotRulesParc.PROPERTY_SOIL_TEXTURE);
        modelBuilder.newColumnForExport("Slope", RefRcesuRunoffPotRulesParc.PROPERTY_SLOPE, MAX_SLOPE_FORMATTER);
        modelBuilder.newColumnForExport("battance", RefRcesuRunoffPotRulesParc.PROPERTY_BATTANCE, OUI_NON_FORMATTER);
        modelBuilder.newColumnForExport("hydromorphisms", RefRcesuRunoffPotRulesParc.PROPERTY_HYDROMORPHISMS, OUI_NON_FORMATTER);
        modelBuilder.newColumnForExport("Runoff potential", RefRcesuRunoffPotRulesParc.PROPERTY_RUNOFF_POTENTIAL, DOUBLE_FORMATTER);
        modelBuilder.newColumnForExport(COLUMN_ACTIVE, RefRcesuRunoffPotRulesParc.PROPERTY_ACTIVE, T_F_FORMATTER);
        return (Iterable) modelBuilder.getColumnsForExport();
    }
}
