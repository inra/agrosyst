package fr.inra.agrosyst.services.performance;

/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 - 2022 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.entities.CroppingPlanEntry;
import fr.inra.agrosyst.api.entities.CroppingPlanSpecies;
import fr.inra.agrosyst.api.entities.Domain;
import fr.inra.agrosyst.api.entities.Equipment;
import fr.inra.agrosyst.api.entities.GrowingSystem;
import fr.inra.agrosyst.api.entities.HarvestingPrice;
import fr.inra.agrosyst.api.entities.Plot;
import fr.inra.agrosyst.api.entities.Zone;
import fr.inra.agrosyst.api.entities.action.AbstractAction;
import fr.inra.agrosyst.api.entities.action.AbstractInputUsage;
import fr.inra.agrosyst.api.entities.action.YealdUnit;
import fr.inra.agrosyst.api.entities.effective.EffectiveCropCyclePhase;
import fr.inra.agrosyst.api.entities.effective.EffectiveIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCycleConnection;
import fr.inra.agrosyst.api.entities.practiced.PracticedCropCyclePhase;
import fr.inra.agrosyst.api.entities.practiced.PracticedIntervention;
import fr.inra.agrosyst.api.entities.practiced.PracticedSystem;
import fr.inra.agrosyst.api.entities.referential.RefDestination;
import fr.inra.agrosyst.api.entities.referential.RefHarvestingPrice;
import fr.inra.agrosyst.api.services.common.HarvestingValorisationPriceSummary;
import fr.inra.agrosyst.api.services.performance.RefCampaignsInputPricesByDomainInput;
import fr.inra.agrosyst.api.services.performance.RefScenariosInputPricesByDomainInput;
import fr.inra.agrosyst.services.performance.indicators.EffectiveItkCropCycleScaleKey;
import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IndicatorWriter extends DataFormatter {

    /**
     * Init writer if needed.
     */
    void init();
    
    void addComputedIndicators(GenericIndicator... indicators);
    
    void writePracticedSeasonalIntervention(String its,
                                            String irs,
                                            String campaigns,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object value,
                                            Integer reliabilityIndex,
                                            String comment,
                                            List<String> referenceDosages,
                                            Domain domain,
                                            GrowingSystem growingSystem,
                                            PracticedSystem practicedSystem,
                                            CroppingPlanEntry croppingPlanEntry,
                                            int rank,
                                            CroppingPlanEntry previousPlanEntry,
                                            CroppingPlanEntry intermediateCrop,
                                            PracticedIntervention intervention,
                                            Collection<AbstractAction> actions,
                                            Collection<String> codeAmmBioControle,
                                            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
                                            Map<String, String> groupesCiblesByCode,
                                            Class<? extends GenericIndicator> indicatorClass);
    
    void writeInputUsage(String its,
                        String irs,
                        String indicatorCategory,
                        String indicatorName,
                        AbstractInputUsage usage,
                        AbstractAction action,
                        EffectiveIntervention effectiveIntervention,
                        PracticedIntervention practicedIntervention,
                        Collection<String> codeAmmBioControle,
                        Domain domain,
                        GrowingSystem growingSystem,
                        Plot plot,
                        Zone zone,
                        PracticedSystem practicedSystem,
                        CroppingPlanEntry croppingPlanEntry,
                        PracticedCropCyclePhase practicedCropCyclePhase,
                        Double solOccupationPercent,
                        EffectiveCropCyclePhase effectiveCropCyclePhase,
                        Integer rank,
                        CroppingPlanEntry previousCrop,
                        CroppingPlanEntry intermediateCrop, Class<? extends GenericIndicator> indicatorClass,
                        Object value,
                        Integer reliabilityIndexForInputId,
                        String reliabilityCommentForInputId,
                        String iftRefDoseUserInfos,
                        String iftRefDoseValue,
                        String iftRefDoseUnit,
                        Map<String, String> groupesCiblesByCode);
    
    void writePracticedPerennialIntervention(String its,
                                             String irs,
                                             String campaigns,
                                             String indicatorCategory,
                                             String indicatorName,
                                             Object value,
                                             Integer reliabilityIndex,
                                             String comment,
                                             List<String> referenceDosages,
                                             Domain domain,
                                             GrowingSystem growingSystem,
                                             PracticedSystem practicedSystem,
                                             CroppingPlanEntry croppingPlanEntry,
                                             PracticedCropCyclePhase phase,
                                             Double perennialCropCyclePercent,
                                             PracticedIntervention intervention,
                                             Collection<AbstractAction> actions,
                                             Collection<String> codeAmmBioControle,
                                             Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
                                             Map<String, String> groupesCiblesByCode,
                                             Class<? extends GenericIndicator> indicatorClass);
    /**
     * Practiced seasonal crop
     */
    void writePracticedSeasonalCrop(String its,
                                    String irs,
                                    String campaigns,
                                    Double frequency,
                                    String indicatorCategory,
                                    String indicatorName,
                                    Object value,
                                    Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                    Integer reliabilityIndex,
                                    String comment,
                                    Integer rank,
                                    Domain domain,
                                    GrowingSystem growingSystem,
                                    PracticedSystem practicedSystem,
                                    CroppingPlanEntry croppingPlanEntry,
                                    CroppingPlanEntry previousPlanEntry,
                                    String culturePrecedentRangId,
                                    Class<? extends GenericIndicator> indicatorClass,
                                    PracticedCropCycleConnection practicedCropCycleConnection);
    
    /**
     * Practiced perennial crop
     */
    void writePracticedPerennialCop(String its,
                                    String irs,
                                    String campaigns,
                                    String indicatorCategory,
                                    String indicatorName,
                                    Object value,
                                    Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                    Integer reliabilityIndex,
                                    String comment,
                                    Domain domain,
                                    GrowingSystem growingSystem,
                                    PracticedSystem practicedSystem,
                                    CroppingPlanEntry croppingPlanEntry,
                                    PracticedCropCyclePhase phase,
                                    Double perennialCropCyclePercent,
                                    Class<? extends GenericIndicator> indicatorClass);
    
    /**
     * Practice System
     */
    void writePracticedSystem(String its,
                              String irs,
                              String campaigns,
                              String indicatorCategory,
                              String indicatorName,
                              Object value,
                              Integer reliabilityIndex,
                              String comment,
                              Domain domain,
                              GrowingSystem growingSystem,
                              PracticedSystem practicedSystem,
                              Class<? extends GenericIndicator> indicatorClass);
    
    /**
     * Practiced Domain
     */
    void writePracticedDomain(String campaigns,
                              String indicatorCategory,
                              String indicatorName,
                              Object value,
                              Integer reliabilityIndex,
                              String comment,
                              Domain domain,
                              String growingSystemsTypeAgricultureLabels);
    
    /**
     * Prices
     */
    void writePrices(
            RefCampaignsInputPricesByDomainInput domainInputPriceRefPrices,
            List<HarvestingPrice> harvestingPrices,
            Map<String, HarvestingValorisationPriceSummary> refHarvestingPrices,
            List<Equipment> equipments,
            Domain domain,
            PracticedSystem practicedSystem,
            RefScenariosInputPricesByDomainInput refScenariosInputPricesByDomainInput,
            MultiKeyMap<String, List<RefHarvestingPrice>> refHarvestingPricesByCodeEspeceAndCodeDestination,
            List<CroppingPlanSpecies> croppingPlanSpecies);
    
    void writeEffectiveSeasonalIntervention(String its,
                                            String irs,
                                            int campaign,
                                            String indicatorCategory,
                                            String indicatorName,
                                            Object value,
                                            Integer reliabilityIndex,
                                            String reliabilityComment,
                                            List<String> referenceDosages,
                                            Domain domain,
                                            Optional<GrowingSystem> optionalGrowingSystem,
                                            Plot plot,
                                            Zone zone,
                                            CroppingPlanEntry croppingPlanEntry,
                                            CroppingPlanEntry intermediateCrop,
                                            int rank,
                                            CroppingPlanEntry previousPlanEntry,
                                            EffectiveIntervention intervention,
                                            Collection<AbstractAction> actions,
                                            Collection<String> codeAmmBioControle,
                                            Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverages,
                                            Map<String, String> groupesCiblesByCode,
                                            Class<? extends GenericIndicator> indicatorClass);
    
    void writeEffectivePerennialIntervention(String its,
                                             String irs,
                                             String indicatorCategory,
                                             String indicatorName,
                                             Object value,
                                             Integer reliabilityIndex,
                                             String comment,
                                             List<String> referenceDosages,
                                             Domain domain,
                                             Optional<GrowingSystem> optionalGrowingSystem,
                                             Plot plot,
                                             Zone zone,
                                             CroppingPlanEntry croppingPlanEntry,
                                             EffectiveCropCyclePhase phase,
                                             EffectiveIntervention intervention,
                                             Collection<AbstractAction> actions,
                                             Collection<String> codeAmmBioControle,
                                             Map<Pair<RefDestination, YealdUnit>, Double> interventionYealdAverage,
                                             Map<String, String> groupesCiblesByCode,
                                             Class<? extends GenericIndicator> indicatorClass);
    
    /**
     * Effective seasonal crop
     */
    void writeEffectiveSeasonalCrop(String its,
                                    String irs,
                                    int campaign,
                                    String indicatorCategory,
                                    String indicatorName,
                                    Object value,
                                    Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverage,
                                    Integer reliabilityIndex,
                                    String comment,
                                    Domain domain,
                                    Optional<GrowingSystem> optionalGrowingSystem,
                                    Plot plot,
                                    CroppingPlanEntry croppingPlanEntry,
                                    Integer rank,
                                    CroppingPlanEntry previousPlanEntry,
                                    Class<? extends GenericIndicator> indicatorClass);
    
    /**
     * Effective perennial crop
     */
    void writeEffectivePerennialCrop(String its,
                                     String irs,
                                     int campaign,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Map<Pair<RefDestination, YealdUnit>, Double> cropYealdAverages,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     Optional<GrowingSystem> optionalGrowingSystem,
                                     Plot plot,
                                     CroppingPlanEntry croppingPlanEntry,
                                     EffectiveCropCyclePhase phase,
                                     Class<? extends GenericIndicator> indicatorClass);
    
    /**
     * Zone
     */
    void writeEffectiveZone(String its,
                            String irs,
                            String indicatorCategory,
                            String indicatorName,
                            Object value,
                            Map<Pair<RefDestination, YealdUnit>, Double> zoneAverageYeald,
                            Integer reliabilityIndex,
                            String comment,
                            Domain anonymiseDomain,
                            List<CroppingPlanSpecies> domainCroppingPlanSpecies,
                            Optional<GrowingSystem> optionalGrowingSystem,
                            Plot plot,
                            Zone zone,
                            String speciesNames,
                            String varietyNames,
                            Class<? extends GenericIndicator> indicatorClass);
    
    /**
     * Plot
     */
    void writeEffectivePlot(String its,
                            String irs,
                            String indicatorCategory,
                            String indicatorName,
                            Object value,
                            Integer reliabilityIndex,
                            String comment,
                            Domain anonymiseDomain,
                            Collection<CroppingPlanSpecies> domainCroppingPlanSpecies,
                            Optional<GrowingSystem> optionalGrowingSystem,
                            Plot plot,
                            Map<Pair<RefDestination, YealdUnit>, Double> plotYealdAverages,
                            Class<? extends GenericIndicator> indicatorClass,
                            String zoneIds);
    
    /**
     * Effective SDC
     */
    void writeEffectiveGrowingSystem(String its,
                                     String irs,
                                     String indicatorCategory,
                                     String indicatorName,
                                     Object value,
                                     Integer reliabilityIndex,
                                     String comment,
                                     Domain domain,
                                     Optional<GrowingSystem> optionalGrowingSystem,
                                     Class<? extends GenericIndicator> indicatorClass,
                                     Collection<CroppingPlanSpecies> agrosystSpecies);
    
    /**
     * Effective Domain
     */
    void writeEffectiveDomain(String indicatorCategory,
                              String indicatorName,
                              Object value,
                              Integer reliabilityIndex,
                              String comment,
                              Domain domain,
                              String growingSystemsTypeAgricultureLabels);

    /**
     * ITK
     * key: Sdc_id * Parcelle_id * Zone_id * Phase_id * Culture_id * Precedent_id * rank
     */
    void writeEffectiveItk(
            String its,
            String irs,
            String indicatorCategory,
            String indicatorLabel,
            Object itkValue,
            Integer effectiveItkCropPrevCropPhaseReliabilityIndex,
            String effectiveItkCropPrevCropPhaseComments,
            Domain anonymiseDomain,
            Optional<GrowingSystem> optionalAnoGrowingSystem,
            Plot plot,
            Zone anonymizeZone,
            Class<? extends GenericIndicator> aClass,
            EffectiveItkCropCycleScaleKey key);
    
    void afterAllIndicatorsAreWritten();
    
    /**
     * Finalize writer if needed.
     */
    void finish();


}
