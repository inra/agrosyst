package fr.inra.agrosyst.services.practiced.export;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2021 INRAE, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.api.services.common.ExportResult;
import fr.inra.agrosyst.api.services.practiced.PracticedSystemService;
import fr.inra.agrosyst.services.ServiceContext;
import fr.inra.agrosyst.services.common.export.AbstractExportTaskRunner;

/**
 * Runner capable de traiter une tâche asynchrone de type {@link PracticedSystemExportTask}.
 *
 * @author Arnaud Thimel (Code Lutin)
 * @since 2.61
 */
public class PracticedSystemExportTaskRunner extends AbstractExportTaskRunner<PracticedSystemExportTask> {

    @Override
    protected ExportResult executeTask(PracticedSystemExportTask task, ServiceContext serviceContext) {
        PracticedSystemService service = serviceContext.newService(PracticedSystemService.class);
        ExportResult result = service.exportPracticedSystemsAsXls(task.getPracticedSystemIds());
        return result;
    }

}
