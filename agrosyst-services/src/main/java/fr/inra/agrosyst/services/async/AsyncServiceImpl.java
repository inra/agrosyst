package fr.inra.agrosyst.services.async;
/*
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import fr.inra.agrosyst.api.services.async.AsyncService;
import fr.inra.agrosyst.api.services.async.ScheduledTaskDto;
import fr.inra.agrosyst.api.services.async.Task;
import fr.inra.agrosyst.api.services.async.TaskFilter;
import fr.inra.agrosyst.api.services.security.BusinessAuthorizationService;
import fr.inra.agrosyst.services.AbstractAgrosystService;
import fr.inra.agrosyst.services.performance.PerformanceTask;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class AsyncServiceImpl extends AbstractAgrosystService implements AsyncService {
    
    protected BusinessAuthorizationService authorizationService;
    
    public void setAuthorizationService(BusinessAuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    public PaginationResult<ScheduledTaskDto> getRunningAndPendingTasks(TaskFilter filter) {
    
        authorizationService.checkIsAdmin();
        
        BusinessTasksManager businessTasksManager = getBusinessTaskManager();
        final ImmutableList<ScheduledTask> runningAndPendingTasks = businessTasksManager.getRunningAndPendingTasks();
        
        int page = filter != null ? filter.getPage() : 0;
        int count = filter != null ? filter.getPageSize() : 10;
    
        PaginationParameter pager = PaginationParameter.of(page, count);
        
        List<ScheduledTaskDto> taskDtos = new ArrayList<>();
        for (ScheduledTask runningAndPendingTask : runningAndPendingTasks) {
            ScheduledTaskDto dto = new ScheduledTaskDto();
            final LocalDateTime startAt = (LocalDateTime) runningAndPendingTask.getStartedAt().orElse(null);
            final LocalDateTime finishAt = (LocalDateTime) runningAndPendingTask.getFinishedAt().orElse(null);
            Task task = runningAndPendingTask.getTask();
            dto.setTaskId(task.getTaskId().toString());
            dto.setDescription(task.getDescription());
            dto.setStartedAt(startAt);
            dto.setFinishedAt(finishAt);
            dto.setRunning(runningAndPendingTask.getRunningInThread().isPresent());
            dto.setUserEmail(task.getUserEmail());
            if (task instanceof PerformanceTask) {
                dto.setPerformanceId(((PerformanceTask)task).getPerformanceId());
            }
            
            taskDtos.add(dto);
        }
        return PaginationResult.fromFullList(taskDtos, pager);
    }

    @Override
    public Pair<Integer, Integer> countRunningAndPendingTasks() {
        BusinessTasksManager businessTasksManager = getBusinessTaskManager();
        int running = businessTasksManager.getRunningTasks().size();
        int pending = businessTasksManager.getPendingTasks().size();
        return Pair.of(running, pending);
    }

    @Override
    public void cancelTask(String taskId) {
        authorizationService.checkIsAdmin();
    
        BusinessTasksManager businessTasksManager = getBusinessTaskManager();
        businessTasksManager.cancelTask(taskId);
    }
    
    @Override
    public void cancelAllDbTasks() {
        authorizationService.checkIsAdmin();
        BusinessTasksManager businessTasksManager = getBusinessTaskManager();
        businessTasksManager.cancelAllDbTasks();
    }

    @Override
    public Future<Void> pingJms() {
        Future<Void> result = context.getSynchroHelper().ping();
        return result;
    }

}
