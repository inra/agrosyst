package fr.inra.agrosyst.services.performance.dbPersistence;

/*-
 * #%L
 * Agrosyst :: Services
 * %%
 * Copyright (C) 2013 - 2019 INRA, CodeLutin
 * Copyright (C) 2020 INRAE, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.inra.agrosyst.services.performance.indicators.GenericIndicator;
import fr.inra.agrosyst.services.performance.indicators.Indicator;

import java.util.Map;

/**
 * @author Cossé David : cosse@codelutin.com
 */
public class IndicatorModel {
    
    protected final Class<?extends GenericIndicator> indicatorClass;
    
    protected final Map<String, String> indicatorsNameToColumns;
    protected final Map<Indicator.OptionalExtraColumnEnumKey, String> extraFieldsNameToColumn;
    
    public IndicatorModel(GenericIndicator indicator) {
        this.indicatorClass = indicator.getClass();
        this.indicatorsNameToColumns = indicator.getIndicatorNameToDbColumnName();
        this.extraFieldsNameToColumn = indicator.getExtraFields();
    }
    
    public Class<? extends GenericIndicator> getIndicatorClass() {
        return indicatorClass;
    }
    
    public Map<String, String> getIndicatorsNameToColumns() {
        return indicatorsNameToColumns;
    }
    
    public Map<Indicator.OptionalExtraColumnEnumKey, String> getExtraFieldsNameToColumn() {
        return extraFieldsNameToColumn;
    }
}
